/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrrtsm.c,v 1.41.2.1 2018/04/16 13:17:05 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Role Transition State Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortRoleTrMachine                                 */
/*                                                                           */
/* Description        : This is the main routine for the Port Role Transition*/
/*                      State Machine.                                       */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortRoleTrMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2State;
    INT4                i4RetVal = RST_SUCCESS;

    u2State = (UINT2) pPerStPortInfo->u1ProleTrSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Role Tr Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_RTSM][u2Event],
                  gaaau1AstSemState[AST_RTSM][u2State]);

    AST_DBG_ARG3 (AST_RTSM_DBG,
                  "RTSM: Port %s: Role Tr Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_RTSM][u2Event],
                  gaaau1AstSemState[AST_RTSM][u2State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PROLETRSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_RTSM_DBG,
                 "RTSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_PORT_ROLE_TR_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_RTSM_DBG, "RTSM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_ROLE_TR_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                 "RTSM: Event routine returned FAILURE !!!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmUpdtInfoReset                            */
/*                                                                           */
/* Description        : This routine is called whenever the SELECTED variable*/
/*                      gets set and the Port role is Blocked, Root or       */
/*                      Designated.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmUpdtInfoReset (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected, Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    if (pPerStPortInfo->u1PortRole != pPerStPortInfo->u1SelectedPortRole)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RTSM: Port %s: Role Change Detected... taking action ... \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Role Change Detected... taking action ... \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return (RstProleTrSmRoleChanged (pPerStPortInfo));
    }

    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT)
    {
        /* Try all conditions out of the ROOT_PORT state */
        if (RstProleTrSmRootPortTransitions (pPerStPortInfo, RST_FALSE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: RootPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
    {
        /* Try all conditions out of the ALTERNATE_PORT state */
        if (RstProleTrSmAltPortTransitions (pPerStPortInfo, RST_FALSE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: AltPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
    {
        /* Try all conditions out of the DESIGNATED_PORT state */
        if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    /* After BEGIN is cleared, and roles selected, the UCT transition from 
     * INIT state to DISABLE state, for any Disabled port is handled below */
    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED) &&
        (pPerStPortInfo->u1ProleTrSmState == RST_PROLETRSM_STATE_INIT_PORT))
    {
        return RstProleTrSmMakeDisablePort (pPerStPortInfo);
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: UpdateInfo reset event handled successfully\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRerootSetDesgPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the reRoot variable is set  */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRerootSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmSyncSetDesgPort                          */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Sync variable is set in */
/*                      the DesignatedPort state.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmSyncSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmFwdExpRootPort                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Forward Delay timer     */
/*                      expires in the RootPort state.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmFwdExpRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    if (pRstPortInfo->bLearn == RST_FALSE)
    {
        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_RootLearn: Port %s: Learn = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstProleTrSmMakeLearn (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }
    else if (pRstPortInfo->bForward == RST_FALSE)
    {
        /* Root-port forwarding is restricted when the BPDU is not received 
         * till the expiry of edge-delaywhile timer if the loop-guard is enabled
         * on that port */
        if ((pRstPortEntry->bLoopGuard == RST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
            && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
            && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
            pRstPortEntry->bLoopInconsistent = RST_TRUE;
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            UtlGetTimeStr (au1TimeStr);
            AstLoopIncStateChangeTrap ((UINT2)
                                       AST_GET_IFINDEX (pPerStPortInfo->
                                                        u2PortNo),
                                       pRstPortEntry->bLoopInconsistent,
                                       RST_DEFAULT_INSTANCE,
                                       (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1TimeStr);
            if (RstPortStateTrMachine
                (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                 RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

        }
        else
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootForward: Port %s: Forward = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            if (pRstPortEntry->bLoopInconsistent == RST_TRUE)
            {
                pRstPortEntry->bLoopInconsistent = RST_FALSE;
            }
            if (RstProleTrSmMakeForward (pPerStPortInfo) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeForward function returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            /* After calling Make Forward, this Port would have transitioned to
             * the Forwarding state (i.e.) bForward would be True */

            /* Shifting to ReRooted state */
            if (pRstPortInfo->bReRoot == RST_TRUE)
            {
                pRstPortInfo->bReRoot = RST_FALSE;

                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RTSM_Rerooted: Port %s: ReRoot = FALSE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Moved to state REROOTED \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            }
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstProleTrSmFwdExpDesgPort                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Forward Delay timer     */
/*                      expires in the DesignatedPort state.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmFwdExpDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    if (pRstPortEntry->bRootInconsistent != RST_TRUE)
    {
        if (((pRstPortInfo->pRrWhileTmr == NULL) ||
             (pRstPortInfo->bReRoot == RST_FALSE)) &&
            (pRstPortInfo->bSync == RST_FALSE))
        {
            /* Designated-port forwarding is restricted when forward delay 
             * timer is expired and the loop-guard is enabled
             * on that port */
            if ((pRstPortEntry->bLoopGuard == RST_TRUE) &&
                (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                && (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
                (pRstPortEntry->bLoopInconsistent != RST_TRUE))
            {
                /* LoopInconsitent state is set when the port transtions
                 * to Designated/Discarding because of loop-guard feature*/
                pRstPortEntry->bLoopInconsistent = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pRstPortEntry->bLoopInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);

                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine "
                                  "returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine "
                                  "returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
            else if ((pRstPortInfo->bLearn == RST_FALSE))
            {
                if (RstProleTrSmMakeLearn (pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
            else if (pRstPortInfo->bForward == RST_FALSE)
            {
                /* Designated-port forwarding is restricted when the BPDU is not received 
                 * till the expiry of edge-delaywhile timer if the loop-guard is enabled
                 * on that port */
                if ((pRstPortEntry->bLoopGuard == RST_TRUE)
                    && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                    && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
                    && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
                {
                    /* LoopInconsitent state is set when the port transtions
                     * to Designated/Discarding because of loop-guard feature*/
                    pRstPortEntry->bLoopInconsistent = RST_TRUE;
                    pRstPortInfo->bLearn = RST_FALSE;
                    pRstPortInfo->bForward = RST_FALSE;
                    UtlGetTimeStr (au1TimeStr);
                    AstLoopIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pRstPortEntry->
                                                                u2PortNo),
                                               pRstPortEntry->bLoopInconsistent,
                                               RST_DEFAULT_INSTANCE,
                                               (INT1 *) AST_BRG_TRAPS_OID,
                                               AST_BRG_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      au1TimeStr);

                    if (RstPortStateTrMachine
                        (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                         RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }

                }
                else
                {
                    if (RstProleTrSmMakeForward (pPerStPortInfo) != RST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: MakeForward function returned FAILURE\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                }
            }
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRrWhileExpDesgPort                       */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the rrWhile timer expires   */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRrWhileExpDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pRootPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4ReRooted = AST_INIT_VAL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pRootPortInfo = AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                             RST_DEFAULT_INSTANCE);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    pRstPortInfo->bReRoot = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_DesgRetired: Port %s: ReRoot = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_RETIRED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /* If this port could not go to forwarding because of rrWhile start the 
     * transition now */
    if (((pRstPortInfo->pFdWhileTmr == NULL) ||
         (pRstPortInfo->bAgreed == RST_TRUE) ||
         (pPortInfo->bOperEdgePort == RST_TRUE)) &&
        (pRstPortInfo->bSync == RST_FALSE))
    {
        if ((pRstPortInfo->bLearn == RST_FALSE) &&
            (pPortInfo->bRootInconsistent != RST_TRUE))
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_DesgLearn: Port %s: Learn = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstProleTrSmMakeLearn (pPerStPortInfo) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }

        if ((pRstPortInfo->bForward == RST_FALSE) &&
            (pRstPortInfo->bLearn == RST_TRUE))
        {
            /* Designated-port forwarding is restricted when the BPDU is not received 
             * till the expiry of edge-delaywhile timer if the loop-guard is enabled
             * on that port */
            if ((pPortInfo->bLoopGuard == RST_TRUE)
                && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                pPortInfo->bLoopInconsistent = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPortInfo->bLoopInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);

                if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                           pPerStPortInfo,
                                           RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RTSM_DesgForward: Port %s: Forward = TRUE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (RstProleTrSmMakeForward (pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeForward function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }

    /* Check if RrWhile Timer is not running for all other ports. 
     * If so, trigger the Root Port with Rerooted event */
    if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
    {
        i4ReRooted = RstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort);

        if (i4ReRooted == RST_SUCCESS)
        {
            if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOTED_SET,
                                      pRootPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }                        /* All Ports are synced */
    }                            /* Root Port exists */

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmProposedSetRootPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the RootPort state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmProposedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the ROOT_PORT state */
    if (RstProleTrSmRootPortTransitions (pPerStPortInfo, RST_FALSE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmProposedSetAlternatePort                 */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the AlternatePort state.                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmProposedSetAlternatePort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (RstProleTrSmAltPortTransitions (pPerStPortInfo, RST_FALSE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: AltPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmAgreedSetDesgPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when agreed Variable is set      */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmAgreedSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRbWhileExpRootPort                       */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the rbWhile timer expires   */
/*                      in the RootPort state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRbWhileExpRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    INT4                i4ReRooted = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Since Rapid Transition is Possible only if the version is >= 2 
     * check for it.
     */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_2)
    {
        i4ReRooted = RstProleTrSmIsReRooted (u2PortNum);

        if (i4ReRooted == RST_SUCCESS)
        {
            return RstProleTrSmTransitionToRootFwding (pPerStPortInfo);
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmDisputedSetDesgPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Disputed variable is set*/
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmDisputedSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmIsReRooted                               */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      rrWhile timer is not running for any of the other    */
/*                      ports except this RootPort.                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of this Port             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmIsReRooted (UINT2 u2PortNum)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2Count = 1;

    if (AST_IS_PROVIDER_EDGE_PORT (u2PortNum) == RST_TRUE)
    {
        i4RetVal = RstPbCvlanIsReRooted ();

        return i4RetVal;
    }

    if (AST_IS_VIRTUAL_INST_PORT (u2PortNum) == RST_TRUE)
    {
        i4RetVal = RstPbbIsReRooted (u2PortNum);
        return i4RetVal;
    }

    AST_GET_NEXT_PORT_ENTRY (u2Count, pPortEntry)
    {
        if (pPortEntry != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Count,
                                                      RST_DEFAULT_INSTANCE);
            pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2Count,
                                                        RST_DEFAULT_INSTANCE);
            if (u2Count == u2PortNum)
            {
                continue;
            }
            if ((pPerStPortInfo->u1SelectedPortRole !=
                 pPerStPortInfo->u1PortRole) ||
                (pRstPortInfo->pRrWhileTmr != NULL))
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: RrWhileTmr Still Running...\n",
                              AST_GET_IFINDEX_STR (u2Count));
                return RST_FAILURE;
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRoleChanged                              */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state as per the role changed          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRoleChanged (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tMrpInfo            MrpStpInfo;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2Duration;
    UINT1               u1CurrPortRole = 0;

    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    u1CurrPortRole = pPerStPortInfo->u1PortRole;

    switch (pPerStPortInfo->u1SelectedPortRole)
    {
        case AST_PORT_ROLE_DISABLED:

            /* If this Port Role is changing from Root Port Role to Disabled Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Root->Disable: Port %s: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            i4RetVal = RstProleTrSmMakeDisablePort (pPerStPortInfo);
            break;

        case AST_PORT_ROLE_ALTERNATE:
            /* If this Port Role is changing from Backup Port Role 
             * to Alternate Port Role, Start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer of RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Alternate: Port %s: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            /* Fall through and continue processing for Backup Role */
        case AST_PORT_ROLE_BACKUP:

            /* If this Port Role is changing from Root Port Role to Alternate/Backup Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Root->Alternate: Port %s: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            i4RetVal = RstProleTrSmMakeBlockPort (pPerStPortInfo);
            break;

        case AST_PORT_ROLE_ROOT:

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: New Port Role selected is ROOT\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            /* If this Port Role is changing from Backup Port Role to Root Port
             * Role, Start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Root: Port %s: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
                && (pPortInfo->bLoopGuard == AST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE))
            {
                pPerStPortInfo->bLoopGuardStatus = AST_TRUE;
            }

            /* If this Port Role is changing from Backup or Alternate or Disabled
             * Port Roles to Root Port Role, Start the Forward Delay timer.
             */
            if ((pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_ROOT)
                &&
                !((pPerStPortInfo->u1PortRole ==
                   (UINT1) AST_PORT_ROLE_DESIGNATED)
                  && pPortInfo->bLoopGuard != AST_TRUE))
            {

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DISABLED)
                {
                    /* DISABLED -> ROOT - Will never happen in our 
                     * implementation */

                    u2Duration = pPortInfo->DesgTimes.u2MaxAge;
                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Disable->Root: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }
                else
                {
                    /* ALTERNATE-> ROOT */

                    /* On an RSTP link, the ports connected to shared LANs 
                     * can be put to forwarding hop by hop (wait until 
                     * neighbouring bridge receives bpdu i.e. wait for a 
                     * multiple of hello time) rather than waiting for the 
                     * entire network to converge (i.e. wait for FwdDelay).
                     * This is because even shared links act on proposals 
                     * received on root ports and sync (block) their 
                     * designated ports.
                     */

                    /* In 802.1D 2004, alternate ports seem to maintain the
                     * fdWhile timer at FwdDelay instead of at forwardDelay.
                     * But this is a bug since it defeats the above 
                     * optimisation. 802.1Q-REV/D3 has the correct value. */

                    /* As per IEEE 802.1D 2004( section 17.2 RSTP and P2P links),
                     * A newly selected Root Port can transition to Forwarding rapidly,
                     * even if attached to shared media.*/

                    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->
                        bSendRstp == RST_TRUE)
                    {
                        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                    }
                    else
                    {
                        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                    }

                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Alternate->Root: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is DISABLED/BACKUP/ALTERNATE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for FdWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }

            i4RetVal = RstProleTrSmMakeRootPort (pPerStPortInfo);
            break;

        case AST_PORT_ROLE_DESIGNATED:

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: New Port Role selected is DESIGNATED\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            /* If this Port Role is changing from Root Port Role to Designated Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Root->Designated: Port %s: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            /* If this Port Role is changing from Backup Port Role to Designated
             * Port Role, then start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Designated: Port %s: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2Duration);
            }

            /* If this Port Role is changing from Backup or Alternate or 
             * Disabled Port Role to Designated Port Role, then start 
             * Forward Delay While Timer.
             */
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DISABLED)
                || (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP))
            {

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DISABLED)
                {
                    /* DISABLED -> DESIGNATED */

                    /* If the port is newly enabled then it should go to 
                     * learning only after MaxAge since this is the maximum 
                     * time taken by the neighbouring bridge to age out it's 
                     * old information if it is a legacy STP bridge */

                    u2Duration = pPortInfo->DesgTimes.u2MaxAge;

                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Disable->Designated: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }
                else
                {
                    /* ALTERNATE-> DESIGNATED */

                    if (pAstBridgeEntry->u4RstOptStatus == RST_DISABLED)
                    {
                        /* As per IEEE802.1D2004 the fdWhile is assigned
                           with FwdDelay(15 sec). So transition from
                           ALTERNATE to DESIGNATED/FORWARDING takes 23 seconds. */

                        if ((AST_GET_COMM_PORT_INFO
                             (pPerStPortInfo->u2PortNo))->bSendRstp == RST_TRUE)
                        {
                            if (AST_FORCE_VERSION == AST_VERSION_2)
                            {
                                u2Duration =
                                    pPortInfo->DesgTimes.u2ForwardDelay;
                            }
                            else
                            {
                                u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                            }
                        }
                        else
                        {
                            u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                        }

                        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                      "RTSM_Alternate->Designated: Port %s: fdWhile = %u\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2Duration);
                    }

                    else
                    {

                        /* On an RSTP link, the ports connected to shared LANs 
                         * can be put to forwarding hop by hop (wait until 
                         * neighbouring bridge receives bpdu i.e. wait for a 
                         * multiple of hello time) rather than waiting for the 
                         * entire network to converge (i.e. wait for FwdDelay).
                         * This is because even shared links act on proposals 
                         * received on root ports and sync (block) their 
                         * designated ports.
                         */

                        /* In 802.1D 2004, alternate ports seem to maintain the
                         * fdWhile timer at FwdDelay instead of at forwardDelay.
                         * But this is a bug since it defeats the above 
                         * optimisation. 802.1Q-REV/D3 has the correct value */

                        if ((AST_GET_COMM_PORT_INFO
                             (pPerStPortInfo->u2PortNo))->bSendRstp == RST_TRUE)
                        {
                            u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                        }
                        else
                        {
                            u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                        }

                        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                      "RTSM_Alternate->Designated: Port %s: fdWhile = %u\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2Duration);
                    }
                }

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is DISABLED/BACKUP/ALTERNATE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for FdWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
            i4RetVal = RstProleTrSmMakeDesgPort (pPerStPortInfo);
            break;

        default:
            /* Selected Port Role is unknown, so return failure */
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Unknown Port Role selected\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            i4RetVal = RST_FAILURE;
            break;
    }

    if (i4RetVal == RST_SUCCESS)
    {
        MrpStpInfo.u1Flag = MSG_PORT_ROLE_CHANGE;
        MrpStpInfo.u4IfIndex = AST_IFENTRY_IFINDEX (pPortInfo);
        MrpStpInfo.u2MapId = RST_DEFAULT_INSTANCE;
        MrpStpInfo.u1OldRole = u1CurrPortRole;
        MrpStpInfo.u1NewRole = pPerStPortInfo->u1SelectedPortRole;

        AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeInitPort                             */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to INIT_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeInitPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Port Role Made as -> DISABLED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Port Role Made as -> DISABLED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_InitPort: Port %s: Role = Disabled Learn = Forward = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bSynced = RST_FALSE;
    MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bSync = RST_TRUE;
    pRstPortInfo->bReRoot = RST_TRUE;

    /* If Recent Backup While timer is running stop it */
    if (pRstPortInfo->pRbWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_RBWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for RbWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    /* If Forward Delay While timer is running stop it.
     * This timer will again be started only when the PortRole changes to 
     * RootPort or DesignatedPort.
     */
    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_FDWHILE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for FdWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    /* If Recent Root While timer is running stop it */
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_RRWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_InitPort: Port %s: Synced = FALSE Sync = ReRoot = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_InitPort: Port %s: rbWhile = rrWhile = 0\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_INIT_PORT;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state INIT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if ((AST_CURR_CONTEXT_INFO ())->bBegin == RST_FALSE)
    {
        return (RstProleTrSmMakeDisablePort (pPerStPortInfo));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeDisablePort                          */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DISABLE_PORT.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeDisablePort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Port Role Made as -> DISABLED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_DisablePort: Port %s: Role = DISABLED Learn = Forward = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_DISABLE_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DISABLE_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstTopoChMachine (RST_TOPOCHSM_EV_NOT_DESG_ROOT,
                          pPerStPortInfo) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if ((pRstPortInfo->bLearning == RST_FALSE) &&
        (pRstPortInfo->bForwarding == RST_FALSE))
    {
        return (RstProleTrSmMakeDisabledPort (pPerStPortInfo));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeDisabledPort                         */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DISABLED_PORT.                */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeDisabledPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAlreadySynced;
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    INT4                i4ReRooted = RST_FAILURE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    bAlreadySynced = pRstPortInfo->bSynced;
    pRstPortInfo->bSynced = RST_TRUE;
    MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSYNCED_UPDATE);

    /* If Forward Delay While timer is running stop it.
     * This timer will again be started only when the PortRole changes to 
     * RootPort or DesignatedPort.
     */
    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_FDWHILE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for FdWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    pRstPortInfo->bSync = RST_FALSE;
    pRstPortInfo->bReRoot = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_DisabledPort: Port %s: Synced = TRUE fdWhile = rrWhile = 0 Sync = ReRoot = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1ProleTrSmState =
        (UINT1) RST_PROLETRSM_STATE_DISABLED_PORT;

    /* If Recent Root While timer is running stop it */
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_RRWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
        /* Check if RrWhile Timer is not running for all other ports. 
         * If so, trigger the Root Port with Rerooted event */
        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {
            pPerStRootPortInfo =
                AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                         RST_DEFAULT_INSTANCE);

            /* If the port role is not same as the selected role then Role transition
             * has not run for this port. */
            if (pPerStRootPortInfo->u1SelectedPortRole ==
                pPerStRootPortInfo->u1PortRole)
            {
                i4ReRooted = RstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort);

                if (i4ReRooted == RST_SUCCESS)
                {
                    if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOTED_SET,
                                              pPerStRootPortInfo) !=
                        RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                }                /* All Ports are synced */
            }                    /* Root Port exists */
        }
    }

    if (bAlreadySynced == RST_FALSE)
    {
        if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
        {
            if (RstPbCvlanHdlSyncedChangeForPort (pPerStPortInfo->u2PortNo) ==
                RST_FAILURE)
            {
                return RST_FAILURE;
            }
        }
        else
        {
            if ((RstIsTreeAllSynced (pPerStPortInfo->u2PortNo)) == RST_TRUE)
            {
                /* Trigger all Root/Alt/Backup ports except this port with 
                 * AllSynced set event. AllSynced for this port will be handled 
                 * below. */
                if (RstProleTrSmIndicateAllSyncedSet (pPerStPortInfo->u2PortNo)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DISABLED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeBlockPort                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to BLOCK_PORT.                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeBlockPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Port Role Made as -> ALTERNATE/BACKUP\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_BlockPort: Port %s: Role = ALT/BACKUP Learn = Forward = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state BLOCK_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_BLOCK_PORT;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ALTERNATE_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstTopoChMachine (RST_TOPOCHSM_EV_NOT_DESG_ROOT,
                          pPerStPortInfo) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if ((pRstPortInfo->bLearning == RST_FALSE) &&
        (pRstPortInfo->bForwarding == RST_FALSE))
    {
        return RstProleTrSmMakeAlternatePort (pPerStPortInfo);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeAlternatePort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ALTERNATE_PORT.               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeAlternatePort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstBoolean         bAlreadySynced = RST_FALSE;
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4ReRooted = RST_FAILURE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;

    bAlreadySynced = pRstPortInfo->bSynced;
    pRstPortInfo->bSynced = RST_TRUE;
    MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bSync = RST_FALSE;
    pRstPortInfo->bReRoot = RST_FALSE;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstPortEntry == NULL)
    {
        return RST_FAILURE;
    }

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_AlternatePort: Port %s: rrWhile = 0 Synced = TRUE Sync = ReRoot = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /* If Recent Root While timer is running stop it */
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_RRWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
        /* Check if RrWhile Timer is not running for all other ports. 
         * If so, trigger the Root Port with Rerooted event */
        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {
            pPerStRootPortInfo =
                AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                         RST_DEFAULT_INSTANCE);
            /* If the port role is not same as the selected role then Role transition
             * has not run for this port. */
            if (pPerStRootPortInfo->u1SelectedPortRole ==
                pPerStRootPortInfo->u1PortRole)
            {
                i4ReRooted = RstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort);

                if (i4ReRooted == RST_SUCCESS)
                {
                    if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOTED_SET,
                                              pPerStRootPortInfo) !=
                        RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                }                /* All Ports are synced */
            }                    /* Root Port exists */
        }
    }

    pPerStPortInfo->u1ProleTrSmState =
        (UINT1) RST_PROLETRSM_STATE_ALTERNATE_PORT;
    /* If this port was already synced then don't bother with allSynced */
    if (bAlreadySynced == RST_FALSE)
    {
        if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
        {
            if (RstPbCvlanHdlSyncedChangeForPort (u2PortNum) == RST_FAILURE)
            {
                return RST_FAILURE;
            }
        }
        else
        {
            if ((RstIsTreeAllSynced (u2PortNum)) == RST_TRUE)
            {
                /* Trigger all Root/Alt/Backup ports except this port with 
                 * AllSynced set event. AllSynced for this port will be handled 
                 * below. */
                if (RstProleTrSmIndicateAllSyncedSet (u2PortNum) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (RstProleTrSmAltPortTransitions (pPerStPortInfo, RST_FALSE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: AltPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }
    if (pAstPortEntry->bLoopGuard == RST_TRUE)
    {
        pPerStPortInfo->bLoopGuardStatus = RST_TRUE;
    }

    AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, pPerStPortInfo->u2PortNo) =
        RST_TRUE;
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmAltPortTransitions                       */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state ALTERNATE_PORT and executes those that         */
/*                      evaluate to TRUE                                     */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmAltPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                tAstBoolean bAllSyncedEvent)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = RST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* When this function is called to handle the AllSynced set event,
     * the parameter bAllSyncedEvent will be set to TRUE. Hence allSynced 
     * can be considered true even without calling IsTreeAllSynced () */
    if (bAllSyncedEvent == RST_TRUE)
    {
        bAllSynced = RST_TRUE;
    }
    /* Otherwise calculate allSynced only if Agree is false */
    else if (pRstPortInfo->bAgree == RST_FALSE)
    {
        bAllSynced = RstIsTreeAllSynced (u2PortNum);
    }

    /* If Agree is TRUE directly move to the ALTERNATE_AGREED state */
    if ((pRstPortInfo->bProposed == RST_TRUE) &&
        (pRstPortInfo->bAgree == RST_FALSE))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state ALTERNATE_PROPOSED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        /* If AllSynced is TRUE directly move to the ALTERNATE_AGREED state */
        if (bAllSynced == RST_FALSE)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: Moved to state ALTERNATE_PROPOSED \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            AST_DBG (AST_SM_VAR_DBG,
                     "RTSM_AlternateProposed: Sync = TRUE for ALL ports\n");

            pRstPortInfo->bProposed = RST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_AlternateProposed: Port %s: Proposed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstProleTrSmSetSyncTree (pPerStPortInfo->u2PortNo) !=
                RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: SetSyncTree returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            /* Recompute allSynced now that sync has been processed by all 
             * ports*/
            bAllSynced = RstIsTreeAllSynced (pPerStPortInfo->u2PortNo);

        }
    }

    if (((pRstPortInfo->bProposed == RST_TRUE) &&
         (pRstPortInfo->bAgree == RST_TRUE))
        || ((pRstPortInfo->bAgree == RST_FALSE) && (bAllSynced == RST_TRUE)))
    {
        pRstPortInfo->bProposed = RST_FALSE;
        pRstPortInfo->bAgree = RST_TRUE;

        pCommPortInfo->bNewInfo = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_AlternateAgreed: Port %s: Proposed = FALSE Agree = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state ALTERNATE_AGREED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, RST_DEFAULT_INSTANCE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if ((pRstPortInfo->bSynced == RST_FALSE) ||
        (pRstPortInfo->bSync == RST_TRUE) ||
        (pRstPortInfo->bReRoot == RST_TRUE) ||
        (pRstPortInfo->pRrWhileTmr != NULL))
    {
        pRstPortInfo->bSynced = RST_TRUE;
        MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        pRstPortInfo->bSync = RST_FALSE;
        pRstPortInfo->bReRoot = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_AlternatePort: Port %s: rrWhile = 0 Synced = TRUE Sync = ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        /* If Recent Root While timer is running stop it */
        if (pRstPortInfo->pRrWhileTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Stop Timer for RrWhileTmr Failed !!!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
    }

    /* LoopInconsitent state is reset when the port transtions  
     * from Designated/Discarding to Alternate/Discarding state */
    if ((pPortInfo->bLoopGuard == RST_TRUE)
        && (pPortInfo->bOperPointToPoint == RST_TRUE))
    {
        if (pPortInfo->bLoopInconsistent != RST_FALSE)
        {
            pPortInfo->bLoopInconsistent = RST_FALSE;
            AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1TimeStr);
        }

    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ALTERNATE_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeRootPort                             */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2PortNum = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_ROOT;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG1 (AST_SM_VAR_DBG, "RTSM_RootPort: Port %s: Role = ROOT\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Port Role Made as -> ROOT\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_ROOT_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* Try all conditions out of the ROOT_PORT state */
    if (RstProleTrSmRootPortTransitions (pPerStPortInfo, RST_FALSE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }
    AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, pPerStPortInfo->u2PortNo) =
        RST_TRUE;
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRootPortTransitions                      */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state ROOT_PORT and executes those that evaluate     */
/*                      TRUE                                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRootPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                 tAstBoolean bAllSyncedEvent)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = RST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    INT4                i4ReRooted = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* When this function is called to handle the AllSynced set event,
     * the parameter bAllSyncedEvent will be set to TRUE. Hence allSynced 
     * can be considered true even without calling IsTreeAllSynced () */
    if (bAllSyncedEvent == RST_TRUE)
    {
        bAllSynced = RST_TRUE;
    }
    /* Otherwise calculate allSynced only if Agree is false */
    else if (pRstPortInfo->bAgree == RST_FALSE)
    {
        bAllSynced = RstIsTreeAllSynced (u2PortNum);
    }

    /* If Agree is TRUE directly move to the ROOT_AGREED state */
    if ((pRstPortInfo->bProposed == RST_TRUE) &&
        (pRstPortInfo->bAgree == RST_FALSE))
    {
        /* If AllSynced is TRUE directly move to the ROOT_AGREED state */
        if (bAllSynced == RST_FALSE)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: Moved to state ROOT_PROPOSED \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            AST_DBG (AST_SM_VAR_DBG,
                     "RTSM_RootProposed: Sync = TRUE for ALL ports\n");

            pRstPortInfo->bProposed = RST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootProposed: Port %s: Proposed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstProleTrSmSetSyncTree (pPerStPortInfo->u2PortNo) !=
                RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: SetSyncTree returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            /* Recompute allSynced now that sync has been processed by all 
             * ports*/
            bAllSynced = RstIsTreeAllSynced (pPerStPortInfo->u2PortNo);

        }
    }

    if (((pRstPortInfo->bProposed == RST_TRUE) &&
         (pRstPortInfo->bAgree == RST_TRUE))
        || ((pRstPortInfo->bAgree == RST_FALSE) && (bAllSynced == RST_TRUE)))
    {
        pRstPortInfo->bProposed = RST_FALSE;
        pRstPortInfo->bSync = RST_FALSE;
        pRstPortInfo->bAgree = RST_TRUE;

        pCommPortInfo->bNewInfo = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_RootAgreed: Port %s: Proposed = Sync = FALSE Agree = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state ROOT_AGREED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        /* TXSM will be triggered after the the transition to ROOT_FORWARDING
         * happens because the bpdu will then have the forwarding flag and the 
         * topology change flag set. */
    }

    if (pRstPortInfo->bForward == RST_FALSE)
    {
        i4ReRooted = RstProleTrSmIsReRooted (u2PortNum);

        /* If already ReRooted directly move to the REROOTED state */
        if ((i4ReRooted != RST_SUCCESS) && (pRstPortInfo->bReRoot == RST_FALSE))
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: Moved to state REROOT \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstProleTrSmSetReRootTree (u2PortNum) != RST_SUCCESS)
            {
                return RST_FAILURE;
            }

            i4ReRooted = RstProleTrSmIsReRooted (u2PortNum);
        }

        if (i4ReRooted == RST_SUCCESS)
        {
            RstProleTrSmRerootedSetRootPort (pPerStPortInfo);
        }
    }
    else if (pRstPortInfo->bReRoot == RST_TRUE)
    {
        pRstPortInfo->bReRoot = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_Rerooted: Port %s: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state REROOTED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    if (pCommPortInfo->bNewInfo == RST_TRUE)
    {
        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, RST_DEFAULT_INSTANCE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstIsTreeAllSynced                                   */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      Synced variable has been set for all the Ports other */
/*                      than this Port.                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*****************************************************************************/
tAstBoolean
RstIsTreeAllSynced (UINT2 u2PortNum)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2Count = 1;

    if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
    {
        /* In case of C-VLAN component of provider edge bridge,
         * the functionality is different. Hence call the
         * respective function here. */
        return (RstPbCvlanIsTreeAllSynced (u2PortNum));
    }

    if (AST_IS_I_COMPONENT () == RST_TRUE)
    {
        /* If the bridge is an I-Bridge or Component is I-Component, 
         * then the functionality needs to be changed.
         * Hence call the respective function here
         * */
        return (RstPbbIsTreeAllSynced (u2PortNum));
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    AST_GET_NEXT_PORT_ENTRY (u2Count, pPortEntry)
    {
        if (pPortEntry == NULL)
        {
            continue;
        }

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

        if (pRstPortInfo->bPortEnabled != RST_TRUE)
        {
            continue;
        }

        if ((pRstPortInfo->bSelected != RST_TRUE) ||
            (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole)
            || (pRstPortInfo->bUpdtInfo != RST_FALSE))
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: IsTreeAllSynced: Port %s: RoleSelection/Updation not complete",
                          AST_GET_IFINDEX_STR (u2Count));
            /* RoleSelection/Updation is not yet complete for all ports */
            return RST_FALSE;
        }

        if ((pPerStBrgInfo->u2RootPort == u2Count) ||
            (pRstPortInfo->bSynced == RST_TRUE))
        {
            continue;
        }
        else
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: IsTreeAllSynced: Port %s: Synced NOT set",
                          AST_GET_IFINDEX_STR (u2Count));
            return RST_FALSE;
        }
    }
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmAllSyncedSetAlternatePort                */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Alternate Port state i.e. all the other ports are in */
/*                      sync with the current spanning tree information and  */
/*                      the Alternate Port may now signal an agreement to    */
/*                      the neighbouring Bridge.                             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmAllSyncedSetAlternatePort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                RST_DEFAULT_INSTANCE);

    /* If the port role is not same as the selected role then Role transition
     * has not run for this port. */
    if ((RST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (RstProleTrSmAltPortTransitions (pPerStPortInfo, RST_TRUE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: AlternatePortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmAllSyncedSetRootPort                     */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. all the other ports are in sync */
/*                      with the current spanning tree information and the   */
/*                      Root Port may now signal an agreement to the         */
/*                      neighbouring Bridge.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmAllSyncedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                RST_DEFAULT_INSTANCE);

    if ((RST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the ROOT_PORT state */
    if (RstProleTrSmRootPortTransitions (pPerStPortInfo, RST_TRUE)
        != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmRerootedSetRootPort                      */
/*                                                                           */
/* Description        : This routine is called whenever the Rerooted event   */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. none of the other ports have the*/
/*                      RrWhile Timer running and hence the Root Port may now*/
/*                      transition to Forwarding.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmRerootedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                RST_DEFAULT_INSTANCE);

    if ((RST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    if ((AST_FORCE_VERSION >= (UINT1) AST_VERSION_2) &&
        (pRstPortInfo->pRbWhileTmr == NULL))
    {
        return RstProleTrSmTransitionToRootFwding (pPerStPortInfo);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmTransitionToRootFwding                   */
/*                                                                           */
/* Description        : This routine is called to transition the root port   */
/*                      to LEARN and FORWARD states. It is called when       */
/*                      ReRooted evaluates to TRUE or when rbWhile timer     */
/*                      expires.                                             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmTransitionToRootFwding (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                RST_DEFAULT_INSTANCE);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    if (pRstPortInfo->bLearn == RST_FALSE)
    {
        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_RootLearn: Port %s: Learn = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstProleTrSmMakeLearn (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if ((pRstPortInfo->bForward == RST_FALSE) &&
        (pRstPortInfo->bLearn == RST_TRUE))
    {
        /* Root-port forwarding is restricted when the BPDU is not received 
         * till the expiry of edge-delaywhile timer if the loop-guard is enabled
         * on that port */
        if ((pRstPortEntry->bLoopGuard == RST_TRUE)
            && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
            && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
            && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
            pRstPortEntry->bLoopInconsistent = RST_TRUE;
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            UtlGetTimeStr (au1TimeStr);
            AstLoopIncStateChangeTrap ((UINT2)
                                       AST_GET_IFINDEX (pPerStPortInfo->
                                                        u2PortNo),
                                       pRstPortEntry->bLoopInconsistent,
                                       RST_DEFAULT_INSTANCE,
                                       (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1TimeStr);

            if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                       pPerStPortInfo,
                                       RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

        }
        else
        {
            /* LoopInconsitent state is reset when the port transtions
             * from Designated/Discarding to Root/Forwarding state */
            if ((pRstPortEntry->bLoopGuard == RST_TRUE)
                && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                && (pRstPortEntry->bOperPointToPoint == RST_TRUE))
            {
                if (pRstPortEntry->bLoopInconsistent == RST_TRUE)
                {
                    pRstPortEntry->bLoopInconsistent = RST_FALSE;
                    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      au1TimeStr);
                }
            }

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootForward: Port %s: Forward = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstProleTrSmMakeForward (pPerStPortInfo) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeForward function returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

        }
    }

    if ((pRstPortInfo->bForward == RST_TRUE) &&
        (pRstPortInfo->bReRoot == RST_TRUE))
    {
        pRstPortInfo->bReRoot = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_Rerooted: Port %s: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state REROOTED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeDesgPort                             */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DESG_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               u1PrevPortRole = AST_PORT_ROLE_DISABLED;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u1PrevPortRole = AST_GET_PORT_ROLE (RST_DEFAULT_INSTANCE,
                                        pPerStPortInfo->u2PortNo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DESIGNATED;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG1 (AST_SM_VAR_DBG | AST_RTSM_DBG,
                  "RTSM_DesignatedPort: Port %s: Role = DESIGNATED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
    {
        AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, pPerStPortInfo->u2PortNo) =
            RST_TRUE;
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Port Role Made as -> DESIGNATED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_DESG_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmDesgPortTransitions                      */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state DESG_PORT and executes those that evaluate to  */
/*                      TRUE                                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmDesgPortTransitions (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAlreadySynced = RST_FALSE;
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    INT4                i4ReRooted = RST_FAILURE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT2               u2Duration;
    UINT2               u2PortNum;
    UINT2               u2InstIndexFound = 0;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2PortNum = pPerStPortInfo->u2PortNo;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Optimization done to reduce the convergence time
     * when Bridge priroity is changed. This is effective when a port
     * transitions from Alternate/Discarding to Designated/Discarding
     * state on further reception of inferior messages will clear
     * the Dispute Flag.*/

    if ((pRstPortInfo->bDisputed == RST_TRUE) &&
        ((pRstPortInfo->bLearn == RST_FALSE) ||
         (pRstPortInfo->bForward == RST_FALSE)))
    {
        if (RstProleTrSmMakeDesignatedDiscard (pPerStPortInfo) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }

    }
    if ((((pRstPortInfo->bSync == RST_TRUE) &&
          (pRstPortInfo->bSynced == RST_FALSE))
         ||
         ((pRstPortInfo->bReRoot == RST_TRUE) &&
          (pRstPortInfo->pRrWhileTmr != NULL))
         ||
         (pRstPortInfo->bDisputed == RST_TRUE))
        &&
        (pPortInfo->bOperEdgePort == RST_FALSE)
        &&
        ((pRstPortInfo->bLearn == RST_TRUE) ||
         (pRstPortInfo->bForward == RST_TRUE)))
    {
        if (RstProleTrSmMakeDesignatedDiscard (pPerStPortInfo) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    if (((((pRstPortInfo->bLearning == RST_FALSE) &&
           (pRstPortInfo->bForwarding == RST_FALSE)) ||
          (pRstPortInfo->bAgreed == RST_TRUE) ||
          (pPortInfo->bOperEdgePort == RST_TRUE))
         &&
         (pRstPortInfo->bSynced == RST_FALSE))
        ||
        ((pRstPortInfo->bSync == RST_TRUE) &&
         (pRstPortInfo->bSynced == RST_TRUE)))
    {
        bAlreadySynced = pRstPortInfo->bSynced;
        pRstPortInfo->bSynced = RST_TRUE;
        MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        pRstPortInfo->bSync = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_DesgSynced: Port %s: rrWhile = 0 Synced = TRUE Sync = FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state DESIGNATED_SYNCED \n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        /***************************************/
        /* Stop rrWhile Timer if it is running */
        /***************************************/
        if (pRstPortInfo->pRrWhileTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Stop Timer for RrWhileTmr Failed !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
            /* Check if RrWhile Timer is not running for all other ports. 
             * If so, trigger the Root Port with Rerooted event */
            if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
            {
                pPerStRootPortInfo =
                    AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                             RST_DEFAULT_INSTANCE);
                /* If the port role is not same as the selected role then Role transition
                 * has not run for this port. */
                if (pPerStRootPortInfo->u1SelectedPortRole ==
                    pPerStRootPortInfo->u1PortRole)
                {
                    i4ReRooted =
                        RstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort);

                    if (i4ReRooted == RST_SUCCESS)
                    {
                        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOTED_SET,
                                                  pPerStRootPortInfo) !=
                            RST_SUCCESS)
                        {
                            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                          AST_ALL_FAILURE_TRC,
                                          "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo));
                            AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                          AST_ALL_FAILURE_DBG,
                                          "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo));
                            return RST_FAILURE;
                        }
                    }            /* All Ports are synced */
                }                /* Root Port exists */
            }
        }

        /* If this port was already synced then don't bother with allSynced */
        if (bAlreadySynced == RST_FALSE)
        {
            if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
            {
                if (RstPbCvlanHdlSyncedChangeForPort (u2PortNum) == RST_FAILURE)
                {
                    return RST_FAILURE;
                }
            }
            else
            {
                if ((RstIsTreeAllSynced (u2PortNum)) == RST_TRUE)
                {
                    if (RstProleTrSmIndicateAllSyncedSet (u2PortNum) !=
                        RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        return RST_FAILURE;
                    }
                }
            }
        }
    }

    if ((pRstPortInfo->pRrWhileTmr == NULL) &&
        (pRstPortInfo->bReRoot == RST_TRUE))
    {
        pRstPortInfo->bReRoot = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RTSM_DesgRetired: Port %s: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state DESIGNATED_RETIRED \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    if (((pRstPortInfo->pFdWhileTmr == NULL) ||
         (pRstPortInfo->bAgreed == RST_TRUE) ||
         (pPortInfo->bOperEdgePort == RST_TRUE))
        &&
        ((pRstPortInfo->bReRoot == RST_FALSE) ||
         (pRstPortInfo->pRrWhileTmr == NULL))
        && (pRstPortInfo->bSync == RST_FALSE))
    {
        /*Learning should happen in this port only if the root 
         *inconsistency recovery timer is not running*/
        if (((pRstPortInfo->bLearn == RST_FALSE) &&
             (pRstPortInfo->pRootIncRecTmr == NULL)) &&
            (pPortInfo->bLoopInconsistent != RST_TRUE))
        {
            if ((pPortInfo->bLoopGuard == RST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE)
                && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                pPortInfo->bLoopInconsistent = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPortInfo->bLoopInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                if (RstProleTrSmMakeLearn (pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return RST_FAILURE;
                }
            }
        }
        if ((pRstPortInfo->bForward == RST_FALSE) &&
            (pRstPortInfo->bLearn == RST_TRUE))
        {
            /* Designated-port forwarding is restricted when the BPDU is not received 
             * till the expiry of edge-delaywhile timer if the loop-guard is enabled
             * on that port */
            if ((pPortInfo->bLoopGuard == RST_TRUE)
                && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                pPortInfo->bLoopInconsistent = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPortInfo->bLoopInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);
                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                if (pPortInfo->bLoopInconsistent == RST_TRUE)
                {
                    pPortInfo->bLoopInconsistent = RST_FALSE;
                }
                u2InstIndexFound = 1;
                if (RstProleTrSmMakeForward (pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeForward function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return RST_FAILURE;
                }

                if ((pRstPortInfo->bAgreed == RST_TRUE) &&
                    (pRstPortInfo->bSynced == RST_FALSE))
                {
                    /* Agreed is now set since the port has gone to 
                     * forwarding */
                    pRstPortInfo->bSynced = RST_TRUE;
                    MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE,
                                           pPerStPortInfo,
                                           MST_SYNC_BSYNCED_UPDATE);
                    pRstPortInfo->bSync = RST_FALSE;

                    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                                  "RTSM_DesgSynced: Port %s: Synced = TRUE Sync = FALSE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    /* Need not stop rrWhile here since the port would never
                     * have gone to forwarding unless it was not running */

                    if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
                    {
                        if (RstPbCvlanHdlSyncedChangeForPort
                            (pPerStPortInfo->u2PortNo) == RST_FAILURE)
                        {
                            return RST_FAILURE;
                        }
                    }
                    else
                    {
                        if ((RstIsTreeAllSynced (pPerStPortInfo->u2PortNo))
                            == RST_TRUE)
                        {
                            if (RstProleTrSmIndicateAllSyncedSet
                                (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
                            {
                                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                              AST_ALL_FAILURE_TRC,
                                              "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));
                                AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                              AST_ALL_FAILURE_DBG,
                                              "RTSM: Port %s: IndicateAllSyncedSet returned FAILURE!\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));
                                return RST_FAILURE;
                            }
                        }        /* AllSynced */
                    }

                }                /* Not Synced */
            }
        }
    }

    if ((pRstPortInfo->bForward == RST_FALSE) &&
        (pRstPortInfo->bAgreed == RST_FALSE) &&
        (pRstPortInfo->bProposing == RST_FALSE) &&
        (pPortInfo->bOperEdgePort == RST_FALSE))
    {
        AST_DBG (AST_RTSM_DBG | AST_BDSM_DBG | AST_TMR_DBG |
                 AST_ALL_FAILURE_DBG, "MSG: Proposing is TRUE!\n");
        pRstPortInfo->bProposing = RST_TRUE;

        /* Start the EdgeDelayWhileTimer */
        if (pPortInfo->bOperPointToPoint == RST_TRUE)
        {
            u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
        }
        else
        {
            u2Duration = (UINT2) pPortInfo->DesgTimes.u2MaxAge;
        }

        if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                           (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                           u2Duration) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
            AST_DBG (AST_RTSM_DBG | AST_BDSM_DBG | AST_TMR_DBG |
                     AST_ALL_FAILURE_DBG,
                     "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
            return RST_FAILURE;
        }

        /* Need not trigger BridgeDetectionMachine with ProposingSet 
         * event since EdgeDelayWhile timer has just been started */

        pCommPortInfo->bNewInfo = RST_TRUE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_DesgPropose: Port %s: edgeDelayWhile = %u Proposing = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2Duration);

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state DESIGNATED_PROPOSE \n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
    }
    if (u2InstIndexFound == 0)
    {
        if ((pPerStPortInfo->bLoopGuardStatus == AST_TRUE) &&
            (pRstPortInfo->bForward == RST_TRUE) &&
            (pPortInfo->bOperPointToPoint == RST_TRUE))

        {
            pPerStPortInfo->bLoopIncStatus = RST_TRUE;
            pPortInfo->bLoopInconsistent = RST_TRUE;
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            UtlGetTimeStr (au1TimeStr);
            AstLoopIncStateChangeTrap ((UINT2)
                                       AST_GET_IFINDEX (pPerStPortInfo->
                                                        u2PortNo),
                                       pPortInfo->bLoopInconsistent,
                                       RST_DEFAULT_INSTANCE,
                                       (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d  at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              RST_DEFAULT_INSTANCE, au1TimeStr);

            if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                       pPerStPortInfo,
                                       RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
    }

    if ((pRstPortInfo->bLearn == RST_TRUE) &&
        (pRstPortInfo->bForward == RST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == AST_TRUE))
    {
        pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeForward                              */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_FORWARD or DESG_FORWARD  */
/*                      if the present state is ROOT_PORT or DESG_PORT       */
/*                      respectively.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeForward (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (pAstPortEntry == NULL)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u DOES NOT exist..."
                      "Returned Failure \n", pPerStPortInfo->u2PortNo);
        return RST_FAILURE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moving to state FORWARD\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_FDWHILE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for FdWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
    {
        if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp
            == RST_TRUE)
        {
            pRstPortInfo->bAgreed = RST_TRUE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_DesgForward: Port %s: Agreed = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        }
        else
        {
            pRstPortInfo->bAgreed = RST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_DesgForward: Port %s: Agreed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        }
    }

    pRstPortInfo->bForward = RST_TRUE;
    pPerStPortInfo->i4NpPortStateStatus = RST_SUCCESS;

    /* Disabling transmission of inferior info in case the blocking the port
       had failed earlier.
       Now that the port can be forwarding there is no need to maintain the
       peer port in discarding state by sending inferior BPDUs any longer. */
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "RTSM_Forward: Port %s: fdWhile = 0 Forward = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state FORWARD \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_FORWARD, pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstTopoChMachine (RST_TOPOCHSM_EV_FORWARD, pPerStPortInfo)
        != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    /* RootInconsistent reset when the port transtions to Designated/Forwarding
     * on expiry of rcvdInfoWhile timer.*/
    if ((AST_PORT_ROOT_GUARD (pAstPortEntry) == RST_TRUE)
        && (pAstPortEntry->bRootInconsistent != RST_FALSE))
    {
        if (pRstPortInfo->pRootIncRecTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for Root Inconsistency"
                         " Recovery timer FAILED!\n");
                return RST_FAILURE;
            }
        }
        UtlGetTimeStr (au1TimeStr);
        pAstPortEntry->bRootInconsistent = RST_FALSE;
        AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          au1TimeStr);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeLearn                                */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_LEARN or DESG_LEARN      */
/*                      if the present state is ROOT_PORT or DESG_PORT       */
/*                      respectively.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeLearn (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Duration;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moving to state LEARN\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    pRstPortInfo->bLearn = RST_TRUE;

    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp
        == RST_TRUE)
    {
        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
    }
    else
    {
        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
    }

    if (AstStartTimer ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Start Timer for FdWhileTmr Failed !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_Learn: Port %s: fdWhile = %u Learn = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2Duration);

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state LEARN \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /* Before triggering STSM and TCSM check whether Disputed is set.
     * If so do not allow the port to move to learning */
    if ((pRstPortInfo->bDisputed == RST_TRUE) &&
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
        (pPortInfo->bOperEdgePort == RST_FALSE))
    {
        if (RstProleTrSmMakeDesignatedDiscard (pPerStPortInfo) == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MakeDesignatedDiscard returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            return RST_FAILURE;
        }
        /* Need not check the other transitions from DESG_PORT since the port
         * would already have been in blocking if Disputed was set */
        return RST_SUCCESS;
    }

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN, pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstTopoChMachine (RST_TOPOCHSM_EV_LEARN_SET, pPerStPortInfo)
        != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmMakeDesignatedDiscard                    */
/*                                                                           */
/* Description        : This routine performs the action for the state       */
/*                      machine state DESG_LISTEN.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmMakeDesignatedDiscard (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Duration;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moving to state DESIGNATED_LISTEN\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;
    pRstPortInfo->bDisputed = RST_FALSE;

    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp
        == RST_TRUE)
    {
        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
    }
    else
    {
        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
    }

    if (AstStartTimer ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Start Timer for FdWhileTmr Failed !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return RST_FAILURE;
    }

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_DesgListen: Port %s: fdWhile = %u Learn = Forward = Disputed = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2Duration);

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_LISTEN \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmSetSyncTree                              */
/*                                                                           */
/* Description        : This routine performs the setSyncBridge() procedure  */
/*                      of the Port Role Transition State Machine. Calls the */
/*                      RstPortRoleTrMachine() routine for all the ports with*/
/*                      'SYNC_SET'.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number from which the sync set      */
/*                                  is initiated.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmSetSyncTree (UINT2 u2PortNum)
{
    UINT2               u2Count = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    if ((AST_IS_PROVIDER_EDGE_PORT (u2PortNum) == RST_TRUE) ||
        (AST_IS_VIRTUAL_INST_PORT (u2PortNum) == RST_TRUE))
    {
        i4RetVal = RstProviderSetSyncTree (u2PortNum);
        return i4RetVal;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    for (u2Count = 1; u2Count <= AST_MAX_NUM_PORTS; u2Count++)
    {
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

        if (pPerStPortInfo != NULL)
        {
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bSync = RST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG, "RTSM: Port %s: Sync Set...\n",
                          AST_GET_IFINDEX_STR (u2Count));

            /* For all Ports other than the Root Port, call the Role Transition
             * Machine with the SYNC_SET event */

            if (u2Count != pPerStBrgInfo->u2RootPort)
            {
                if (RstPortRoleTrMachine (RST_PROLETRSM_EV_SYNC_SET,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmSetReRootTree                            */
/*                                                                           */
/* Description        : This routine sets the reRoot variable to TRUE for all*/
/*                      Ports of the Bridge.                                 */
/*                                                                           */
/* Input(s)           : u2InPort - Port Invoking this procedure.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmSetReRootTree (UINT2 u2InPort)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    if ((AST_IS_PROVIDER_EDGE_PORT (u2InPort) == RST_TRUE) ||
        (AST_IS_VIRTUAL_INST_PORT (u2InPort) == RST_TRUE))
    {
        i4RetVal = RstProviderSetReRootTree (u2InPort);
        return i4RetVal;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

        if (pPerStPortInfo != NULL)
        {
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bReRoot = RST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: ReRoot Set...\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (u2PortNum != pPerStBrgInfo->u2RootPort)
            {
                if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOT_SET,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmOperEdgeSetDesgPort                      */
/*                                                                           */
/* Description        : This routine is called whenever the operEdge variable*/
/*                      is set to TRUE for any port on the bridge            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmOperEdgeSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmIndicateAllSyncedSet                     */
/*                                                                           */
/* Description        : This routine is called when allSynced becomes TRUE.  */
/*                      It triggers the allSynced event to all Root and      */
/*                      Alternate/Backup ports.                              */
/*                                                                           */
/* Input(s)           : u2ThisPort - The port which caused this trigger      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmIndicateAllSyncedSet (UINT2 u2ThisPort)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    /* If there are no root ports then there will be no alternate 
     * ports and so no port needs be triggered */
    if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
    {
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            /* AllSynced for this port will be handled as part of the port's
             * role transitions */
            if (u2PortNum == u2ThisPort)
            {
                continue;
            }

            if ((pPerStPortInfo =
                 AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE))
                == NULL)
            {
                continue;
            }

            if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
                (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
            {
                if (RstPortRoleTrMachine (RST_PROLETRSM_EV_ALLSYNCED_SET,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }                            /* Root Port exists */
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleTrSmLearningFwdingReset                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Learning and Forwarding */
/*                      variables have been reset after the Hardware port    */
/*                      state is put to Blocking.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
    {
        return RstProleTrSmMakeAlternatePort (pPerStPortInfo);
    }
    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED))
    {
        return RstProleTrSmMakeDisabledPort (pPerStPortInfo);
    }

    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
             (pRstPortInfo->bSynced == RST_FALSE))
    {
        if (RstProleTrSmDesgPortTransitions (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
        return RST_SUCCESS;

    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitPortRoleTrMachine                             */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortRoleTrMachine                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortRoleTrMachine                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstInitPortRoleTrMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PROLETRSM_MAX_EVENTS][20] = {
        "BEGIN", "FDWHILE_EXP", "RRWHILE_EXP",
        "RBWHILE_EXP", "SYNC_SET", "REROOT_SET", "SELECTED_SET",
        "PROPOSED_SET", "AGREED_SET", "ALLSYNCED_SET", "REROOTED_SET",
        "OPEREDGE_SET", "DISPUTED_SET", "LRNING_FWDING_RESET"
    };
    UINT1               aau1SemState[RST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "DISABLE_PORT",
        "DISABLED_PORT", "ROOT_PORT", "DESG_PORT", "ALTERNATE_PORT",
        "BLOCK_PORT"
    };

    for (i4Index = 0; i4Index < RST_PROLETRSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_RTSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_RTSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < RST_PROLETRSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_RTSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_RTSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* BEGIN Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = RstProleTrSmMakeInitPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_BEGIN]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmMakeInitPort;

    /* FDWHILE_EXP Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = RstProleTrSmFwdExpRootPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_FDWHILE_EXP]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmFwdExpDesgPort;

    /* RRWHILE_EXP Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RRWHILE_EXP]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction =
        RstProleTrSmRrWhileExpDesgPort;

    /* RBWHILE_EXP Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction =
        RstProleTrSmRbWhileExpRootPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_RBWHILE_EXP]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* SYNC_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction =
        RstProleTrSmMakeDisabledPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        RstProleTrSmMakeAlternatePort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SYNC_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmSyncSetDesgPort;

    /* REROOT_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction =
        RstProleTrSmMakeDisabledPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        RstProleTrSmMakeAlternatePort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOT_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmRerootSetDesgPort;

    /* SELECTED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = RstProleTrSmUpdtInfoReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_SELECTED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmUpdtInfoReset;

    /* PROPOSED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        RstProleTrSmProposedSetAlternatePort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction =
        RstProleTrSmProposedSetRootPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_PROPOSED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* AGREED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_AGREED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = RstProleTrSmAgreedSetDesgPort;

    /* ALLSYNCED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        RstProleTrSmAllSyncedSetAlternatePort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction =
        RstProleTrSmAllSyncedSetRootPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_ALLSYNCED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* REROOTED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction =
        RstProleTrSmRerootedSetRootPort;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_REROOTED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* OPEREDGE_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_OPEREDGE_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction =
        RstProleTrSmOperEdgeSetDesgPort;

    /* DISPUTED_SET Event */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_DISPUTED_SET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction =
        RstProleTrSmDisputedSetDesgPort;

    /* RST_PROLETRSM_EV_LEARNING_FWDING_RESET */
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_DISABLE_PORT].pAction =
        RstProleTrSmLearningFwdingReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_BLOCK_PORT].pAction =
        RstProleTrSmLearningFwdingReset;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    RST_PORT_ROLE_TR_MACHINE[RST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [RST_PROLETRSM_STATE_DESG_PORT].pAction =
        RstProleTrSmLearningFwdingReset;
    AST_DBG (AST_RTSM_DBG, "RTSM: Loaded RTSM SEM successfully\n");

    return;
}
