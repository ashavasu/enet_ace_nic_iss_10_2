/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpsys.c,v 1.44 2017/11/20 13:12:25 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              AST Module.
 *
 *******************************************************************/

#ifdef RSTP_WANTED
#define _ASTPSYS_C

#include "asthdrs.h"
#include "mux.h"

/*****************************************************************************/
/* Function Name      : AstGlobalMemoryInit                                  */
/*                                                                           */
/* Description        : This function is called at the time of task creation */
/*                      The following are global and not specific to any     */
/*                      context:                                             */
/*                       a) mempools for the protocol data structures        */
/*                       b) timer lists and mempools for timer nodes         */
/*                      This routine creates and initialises the above.      */
/*                                                                           */
/*                      It also creates the global IfIndex based RBTree and  */
/*                      allocates memory for the Default Context.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstGlobalMemoryInit (VOID)
{
    UINT4               u4ContextId;
    UINT4               u4MinBlkAdjusted;
    UINT4               u4MaxBlkAdjusted;
    tAstContextInfo    *pAstContext = NULL;
    tAstMsgDigestsize  *pAstMsgDigestPtr = NULL;
    tAstRedPerPortInstInfoArray *pAstRedPerPortInstInfoPtr = NULL;

#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    INT4                i4MemPoolId;
#endif
    UNUSED_PARAM (pAstMsgDigestPtr);
    UNUSED_PARAM (pAstRedPerPortInstInfoPtr);

    gAstGlobalInfo.u4BufferFailureCount = AST_INIT_VAL;
    gAstGlobalInfo.u4MemoryFailureCount = AST_INIT_VAL;

    MEMSET (&gFsRstpSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsRstpSizingInfo.ModName, "RSTP", STRLEN ("RSTP"));
    gFsRstpSizingInfo.u4ModMemPreAllocated = 0;
    gFsRstpSizingInfo.ModSizingParams = gFsRstpSizingParams;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        gau1AstSystemControl[u4ContextId] = AST_SHUTDOWN;
        gau1AstModuleStatus[u4ContextId] = RST_DISABLED;
    }

#ifdef PVRST_WANTED
    /* Call Utility Routine to calculate Adjustment values for minimum 
       and Max Block size before creating Buddy Memory Pool   */

    UtilCalculateBuddyMemPoolAdjust ((UINT4)
                                     (AST_MAX_MST_INSTANCES *
                                      sizeof (tAstPerStPortInfo *)),
                                     (UINT4) (AST_MAX_PVRST_INSTANCES *
                                              sizeof (tAstPerStPortInfo *)),
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);
#else
    /* Call Utility Routine to calculate Adjustment values for minimum 
       and Max Block size before creating Buddy Memory Pool   */

    UtilCalculateBuddyMemPoolAdjust ((UINT4)
                                     (AST_MAX_MST_INSTANCES *
                                      sizeof (tAstPerStPortInfo *)),
                                     (UINT4) (AST_MAX_MST_INSTANCES *
                                              sizeof (tAstPerStPortInfo *)),
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);
#endif

    /* Create Mempool for PerStInfos. */
    if ((AST_PERST_TBL_MEMPOOL_ID =
         MemBuddyCreate (u4MaxBlkAdjusted, u4MinBlkAdjusted,
                         (AST_SIZING_CONTEXT_COUNT),
                         BUDDY_CONT_BUF)) == BUDDY_FAILURE)
    {
        AST_TRC (AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: Memory Pool Creation Failure for AST module\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Memory Pool Creation Failure for AST module\n");

        AstGlobalMemoryDeinit ();
        return RST_FAILURE;
    }
#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    if ((i4MemPoolId =
         MemBuddyCreate (u4MaxBlkAdjusted, u4MinBlkAdjusted,
                         AST_SIZING_CONTEXT_COUNT,
                         BUDDY_CONT_BUF)) == BUDDY_FAILURE)
    {
        AST_TRC (AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: Memory Pool Creation Failure for AST module\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Memory Pool Creation Failure for AST module\n");
        AstGlobalMemoryDeinit ();
        return RST_FAILURE;
    }
    else
    {
        AST_INSTANCE_UPCOUNT_MEMPOOL_ID = (UINT4) i4MemPoolId;
    }
#endif

    if (AstTimerInit () == RST_FAILURE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                        AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                        "TMR: Timer Init FAILED!\n");

        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_TMR_DBG |
                        AST_ALL_FAILURE_DBG, "TMR: Timer Init FAILED!\n");

        AstGlobalMemoryDeinit ();
        return RST_FAILURE;
    }

    gAstGlobalInfo.GlobalIfIndexTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAstPortEntry,
                                             AstGlobalIfIndexNode),
                              (tRBCompareFn) AstIfIndexTblCmpFn);

    if (gAstGlobalInfo.GlobalIfIndexTable == NULL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                        AST_ALL_FAILURE_TRC,
                        "SYS: Global IfIndex RBTree creation failed !!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "SYS: Global IfIndex RBTree creation failed !!!\n");

        AstGlobalMemoryDeinit ();

        return RST_FAILURE;
    }

    /* Allocate and initialize the default context */
    AST_ALLOC_CONTEXT_MEM_BLOCK (pAstContext);

    if (pAstContext == NULL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Memory Pool allocation for Default Context failed!!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Memory Pool allocation for Default Context failed!!!\n");

        AstGlobalMemoryDeinit ();

        return RST_FAILURE;
    }

    /* Select the newly allocated context Info as the current context */
    AST_CURR_CONTEXT_INFO () = pAstContext;
    AstRedSelectContext (AST_DEFAULT_CONTEXT);

    if (AstInitContextInfo (AST_DEFAULT_CONTEXT) == RST_FAILURE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Initialization of Default Context failed!!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Initialization of Default Context failed!!!\n");

        AstReleaseContext ();

        AST_RELEASE_CONTEXT_MEM_BLOCK (pAstContext);

        AstGlobalMemoryDeinit ();

        return RST_FAILURE;
    }

    AstReleaseContext ();
    KW_FALSEPOSITIVE_FIX (pAstContext);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGlobalMemoryDeinit                                */
/*                                                                           */
/* Description        : This function is called at the time of task creation */
/*                      if Global memory initialisation fails.               */
/*                      The following are global and not specific to any     */
/*                      context:                                             */
/*                       a) mempools for the protocol data structures        */
/*                       b) timer lists and mempools for timer nodes         */
/*                      This routine frees the above.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstGlobalMemoryDeinit (VOID)
{
    INT4                i4RetVal = RST_SUCCESS;

    if (AST_GLOBAL_IFINDEX_TREE () != NULL)
    {
        RBTreeDestroy (AST_GLOBAL_IFINDEX_TREE (),
                       (tRBKeyFreeFn) AstFreePortEntry, 0);
        AST_GLOBAL_IFINDEX_TREE () = NULL;
    }

    if (AST_PERST_TBL_MEMPOOL_ID != AST_INIT_VAL)
    {
        MemBuddyDestroy ((UINT1) AST_PERST_TBL_MEMPOOL_ID);
        AST_PERST_TBL_MEMPOOL_ID = AST_INIT_VAL;
    }

#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    if (AST_INSTANCE_UPCOUNT_MEMPOOL_ID != AST_INIT_VAL)
    {
        MemBuddyDestroy ((UINT1) AST_INSTANCE_UPCOUNT_MEMPOOL_ID);
        AST_INSTANCE_UPCOUNT_MEMPOOL_ID = AST_INIT_VAL;
    }
#endif
    if (AstTimerDeInit () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Timer Deinit failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Timer Deinit failed !!!\n");
        i4RetVal = RST_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstSelectContext                                     */
/*                                                                           */
/* Description        : This function selects a virtual context as the       */
/*                      current context pointer before processing an event   */
/*                      for the context                                      */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
AstSelectContext (UINT4 u4ContextId)
{
    INT4                i4RetVal = RST_SUCCESS;

    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
    {
        return RST_FAILURE;
    }

    AST_CURR_CONTEXT_INFO () = AST_CONTEXT_INFO (u4ContextId);

    if (AST_CURR_CONTEXT_INFO () == NULL)
    {
        return RST_FAILURE;
    }

    i4RetVal = AstRedSelectContext (u4ContextId);

    gCurrContextId = u4ContextId;
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : AstReleaseContext                                    */
/*                                                                           */
/* Description        : This function releases the virtual context           */
/*                      previously selected as current after an event has    */
/*                      been processed for the context.                      */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
AstReleaseContext (VOID)
{
    AST_CURR_CONTEXT_INFO () = NULL;

    AstRedReleaseContext ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleCreateContext                               */
/*                                                                           */
/* Description        : This routine allocates memory for the ASTP module    */
/*                      data structures for the given context and intialises */
/*                      them.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstHandleCreateContext (UINT4 u4ContextId)
{
    tAstContextInfo    *pAstContext = NULL;
    INT4                i4Mode = AST_INIT_VAL;
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
    {
        return RST_FAILURE;
    }

    if (AST_CONTEXT_INFO (u4ContextId) != NULL)
    {
        return RST_SUCCESS;
    }

    AST_ALLOC_CONTEXT_MEM_BLOCK (pAstContext);

    if (pAstContext == NULL)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Memory Pool allocation for Context failed!!!\n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Memory Pool allocation for Context failed!!!\n");

        return RST_FAILURE;
    }
    KW_FALSEPOSITIVE_FIX (pAstContext);
    AST_INIT_PORT_BUF (&pAstContext->PortList);
    AST_INIT_INST_BUF (&pAstContext->MstiList);
    /* Select the newly allocated context Info as the current context */
    AST_CURR_CONTEXT_INFO () = pAstContext;
    AstRedSelectContext (u4ContextId);

    if (AstInitContextInfo (u4ContextId) == RST_FAILURE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Initialization of Context failed!!!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_INIT_SHUT_DBG | AST_MEM_DBG |
                        AST_ALL_FAILURE_DBG,
                        "MSG: Initialization of Context failed!!!\n");

        AstReleaseContext ();

        AST_RELEASE_CONTEXT_MEM_BLOCK (pAstContext);

        return RST_FAILURE;
    }

    /* If the L2IWF has invalid bridge mode, ie) bridge mode has not been
     * configured by administrator then no need to initialise the modules
     * This check is not applicable to the Default context because it always
     * takes the mode mentoned in the issnvram.txt.
     */
    if ((u4ContextId != AST_DEFAULT_CONTEXT) &&
        (AST_BRIDGE_MODE () == AST_INVALID_BRIDGE_MODE))
    {
        return RST_SUCCESS;
    }

    if (AstDeriveOperatingMode (&i4Mode) == RST_FAILURE)
    {
        AstHandleDeleteContext (u4ContextId);
        return RST_FAILURE;
    }

    /* MSTP or RSTP is started by default when a 
     * new virtual context is created */

#ifdef MSTP_WANTED
    if (i4Mode == MST_START)
    {
        if (MstModuleInit () == MST_FAILURE)
        {
            AstHandleDeleteContext (u4ContextId);
            return RST_FAILURE;
        }

        if (MstModuleEnable () == MST_FAILURE)
        {
            AstHandleDeleteContext (u4ContextId);

            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (RstModuleInit () == RST_FAILURE)
        {
            AstHandleDeleteContext (u4ContextId);

            return RST_FAILURE;
        }

        if (RstModuleEnable () == RST_FAILURE)
        {
            AstHandleDeleteContext (u4ContextId);

            return RST_FAILURE;
        }
    }

    AstReleaseContext ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleDeleteContext                               */
/*                                                                           */
/* Description        : This routine de-initialises the AST module data      */
/*                      structures and frees the allocated memory for the    */
/*                      given context                                        */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstHandleDeleteContext (UINT4 u4ContextId)
{
#ifdef MSTP_WANTED
    if (AST_IS_MST_STARTED ())
    {
        MstModuleShutdown ();
    }
    else
#endif
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_STARTED ())
    {
        PvrstModuleShutdown ();
    }
    else
#endif
    {
        RstModuleShutdown ();
    }

    if (AST_PER_CONTEXT_IFINDEX_TREE () != NULL)
    {
        RBTreeDestroy (AST_PER_CONTEXT_IFINDEX_TREE (),
                       (tRBKeyFreeFn) AstFreePortEntry, 0);
        AST_PER_CONTEXT_IFINDEX_TREE () = NULL;
    }

#ifdef L2RED_WANTED
    AstRedReleasePortAndPerPortInstInfoBlock ();
#endif
    AST_RELEASE_CONTEXT_MEM_BLOCK (AST_CURR_CONTEXT_INFO ());

    AstReleaseContext ();

    AST_CONTEXT_INFO (u4ContextId) = NULL;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleUpdateContextName                           */
/*                                                                           */
/* Description        : This routine updates the name of the context         */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstHandleUpdateContextName (UINT4 u4ContextId)
{
    MEMSET (AST_CONTEXT_STR (), 0, AST_CONTEXT_STR_LEN);
    if (AstVcmGetAliasName (u4ContextId, AST_CONTEXT_STR ()) != VCM_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstInitContextInfo                                   */
/*                                                                           */
/* Description        : This routine initialises the context info immediately*/
/*                      after it has been allocated.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - The Context ID to be filled in the     */
/*                                    structure                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstInitContextInfo (UINT4 u4ContextId)
{
    MEMSET (AST_CURR_CONTEXT_INFO (), 0, sizeof (tAstContextInfo));
    AST_INIT_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList);
    AST_INIT_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList);
    AST_CURR_CONTEXT_ID () = u4ContextId;

    AST_CURR_CONTEXT_IFTABLE () =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAstPortEntry,
                                             AstContextIfIndexNode),
                              (tRBCompareFn) AstIfIndexTblCmpFn);

    AST_FORCE_VERSION = (UINT1) AST_VERSION_2;
    AST_TRACE_OPTION = (UINT4) 0;
    AST_DEBUG_OPTION = (UINT4) 0;
    AST_FLUSH_INTERVAL = (UINT4) AST_INIT_VAL;
    AST_ADMIN_STATUS = (UINT1) RST_DISABLED;
    AST_GBL_BPDUGUARD_STATUS = AST_BPDUGUARD_DISABLE;

    MEMSET (AST_CONTEXT_STR (), 0, AST_CONTEXT_STR_LEN);
#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_MI_MODE)
    {
        VcmGetAliasName (u4ContextId, AST_CONTEXT_STR ());
    }
#endif

    AST_CONTEXT_INFO (u4ContextId) = AST_CURR_CONTEXT_INFO ();

#ifdef L2RED_WANTED
    if (AstRedAllocPortAndPerPortInstInfoBlock () != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                 AST_ALL_FAILURE_TRC,
                 "SYS: Mem alloc failed for PortTable/PerPortInstInfo Table "
                 "during RED Initialization. \r\n");
        return RST_FAILURE;
    }
#endif

    /*In MI Case, the context will be created and ASTP Module will be 
     * shutdown. In time of context creation, the L2IWF will have invalid
     * bridge mode unless it was configured by User. So the ASTP Module
     * should not be started unless there is a valid bridge mode*/
    if (AstL2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &(AST_BRIDGE_MODE ()))
        != L2IWF_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MEM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "MSG: Getting the Bridge Mode from l2IWf for"
                      "the context %d failed!!!\n", u4ContextId);

        return RST_FAILURE;
    }

    /* If the L2IWF has invalid bridge mode, ie) bridge mode has not been
     * configured by administrator then no need to initialise the modules
     * This check is not applicable to the Default context because it always
     * takes the mode mentoned in the issnvram.txt.
     */
    if ((AST_CURR_CONTEXT_ID () != AST_DEFAULT_CONTEXT) &&
        (AST_BRIDGE_MODE () == AST_INVALID_BRIDGE_MODE))
    {
        return RST_SUCCESS;
    }

#ifndef L2RED_WANTED            /*L2 RED Moved to Idle To Active */
#ifdef NPAPI_WANTED
    RstpFsMiStpNpHwInit (u4ContextId);
#endif
#endif
    return RST_SUCCESS;
}

#ifdef SNMP_2_WANTED
/*****************************************************************************/
/*    Function Name       : STPRegisterMIBS                                  */
/*                                                                           */
/*    Description         : This function registers the RSTP/MSTP mibs with  */
/*                          the SNMP agent                                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                      .                      */
/*****************************************************************************/
VOID
STPRegisterMIBS (VOID)
{
    RegisterFSMPRS ();
    RegisterFSMPBR ();
    RegisterSTD1W1 ();
#ifdef RSTP_WANTED
    RegisterFSMSRS ();
#endif
#ifdef MSTP_WANTED
    RegisterFSMPMS ();
    RegisterSTD1S1 ();
#endif
#ifdef PVRST_WANTED
    RegisterFSMPPV ();
#endif
    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE)
    {
        RegisterFSRST ();
        RegisterFSPBRS ();
        RegisterSTD1W1 ();
#ifdef MSTP_WANTED
        RegisterFSMST ();
        RegisterSTD1S1 ();
#endif
#ifdef PVRST_WANTED
        RegisterFSPVRS ();
#endif
    }
}
#endif /* SNMP_2_WANTED */

#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstGetAliasName                                      */
/*                                                                           */
/* Description        : This function to get context name from ID            */
/*                                                                           */
/* Input (s)          : u4ContextId - Context Id                             */
/*                                                                           */
/* Output (s)         : pu1Alias - Context Name                              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_SUCCESS)
    {
        return RST_SUCCESS;
    }
    return RST_FAILURE;
}
#endif

/*****************************************************************************/
/* Function Name      : AstDeriveOperatingMode                               */
/*                                                                           */
/* Description        : This function will update the bridge mode for the    */
/*                      current context from L2IWF and the component type    */
/*                      based on the same. If this function is invoked during*/
/*                      module initialisation, then it will also output the  */
/*                      mode, MSTP or RSTP in which the default context      */
/*                      should come up.                                      */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : pi4Mode  - Operating mode                            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstDeriveOperatingMode (INT4 *pi4Mode)
{

#ifdef MSTP_WANTED
    /* If this function is called during ASTP Module Init, it will decide
     * whether MSTP or RSTP should be started in the default context based on
     * the bridge mode
     * */
    *pi4Mode = MST_START;
#else
    *pi4Mode = RST_START;
#endif

    /*Updating the Bridge Mode from L2IWF */
    if (AstL2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &(AST_BRIDGE_MODE ()))
        != L2IWF_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                 AST_ALL_FAILURE_TRC,
                 "SYS: Getting the Bridge mode from l2IWF failed\r\n");
        return RST_FAILURE;
    }

    if (AST_BRIDGE_MODE () == AST_INVALID_BRIDGE_MODE)
    {
        AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                 AST_ALL_FAILURE_TRC,
                 "SYS: Bridge Mode is Invalid. It should be"
                 " Properly configured before starting AST" "Module\r\n");
        return RST_FAILURE;
    }

    /*Updating the Component type of the context */
    AstUpdateCompType ();

    /* Donot allow the module to spawn MSTP, if the bridge I Bridge */
    if ((AST_BRIDGE_MODE ()) == AST_ICOMPONENT_BRIDGE_MODE)
    {
        *pi4Mode = RST_START;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRegisterWithPacketHandler                         */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      Spanning tree control packets.                       */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstRegisterWithPacketHandler ()
{
    tProtocolInfo       StpProtocolInfo;

    /* The last Byte is 0x0f in the L2Iwf code, but the standard and the packet
     * says that it is 00. So does the RSTP packets transmitted*/
    static tMacAddr     BrgResvAddr = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    static tMacAddr     ProviderStpAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x08 };
#ifdef PVRST_WANTED
    /* ISL Destination Addresses */
    static tMacAddr     CiscoISLAddr1 = { 0x01, 0x00, 0x0C, 0x00, 0x00, 0x00 };
    static tMacAddr     CiscoISLAddr2 = { 0x03, 0x00, 0x0C, 0x00, 0x00, 0x00 };

    /* Dot1q Destination Address */
    static tMacAddr     Dot1qAddr = { 0x01, 0x00, 0x0C, 0xCC, 0xCC, 0xCD };
#endif

    /* Register with the packet handler for receiving packets */
    MEMSET (&StpProtocolInfo, 0, sizeof (tProtocolInfo));

    /* The same call back function has to be called for all STP packets
     * so, we need not copy this each time */
    StpProtocolInfo.ProtocolFnPtr = NULL;
    StpProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;
    StpProtocolInfo.i4Command = MUX_PROT_REG_HDLR;

    MEMCPY (&(StpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), BrgResvAddr,
            sizeof (tMacAddr));
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_RSTP_BRG, &StpProtocolInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }

    MEMCPY (&(StpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), ProviderStpAddr,
            sizeof (tMacAddr));
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_PROVIDER_STP, &StpProtocolInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }

#ifdef PVRST_WANTED
    MEMCPY (&(StpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), CiscoISLAddr1,
            sizeof (tMacAddr));
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0,
            ETHERNET_ADDR_SIZE);
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE - 1);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_PVRST_CISCO_ISL1, &StpProtocolInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }

    MEMCPY (&(StpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), CiscoISLAddr2,
            sizeof (tMacAddr));
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0,
            ETHERNET_ADDR_SIZE);
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE - 1);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_PVRST_CISCO_ISL2, &StpProtocolInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }

    MEMCPY (&(StpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), Dot1qAddr,
            sizeof (tMacAddr));
    MEMSET (StpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_PVRST_DOT1Q, &StpProtocolInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }
#endif

    return RST_SUCCESS;
}

#endif
