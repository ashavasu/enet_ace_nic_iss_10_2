/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpred.c,v 1.121 2016/09/13 12:56:29 siva Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for AST module.   
 *
 *******************************************************************/
#ifdef L2RED_WANTED
#include "asthdrs.h"
#include "rmgr.h"
#include "cli.h"
#ifdef MSTP_WANTED
#include "astmred.h"
#endif
#ifdef PVRST_WANTED
#include "astvred.h"
#endif
static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
tAstRedGlobalInfo   gAstRedGlobalInfo;
tAstRedContextInfo *gpAstRedContextInfo = NULL;
tAstRedContextInfo *gpAstRedParentContextInfo = NULL;
/*****************************************************************************/
/* Function Name      : AstRedRmInit                                         */
/*                                                                           */
/* Description        : Initialise the RED Global variables pertaining to STP*/
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*****************************************************************************/
INT4
AstRedRmInit (VOID)
{
    AST_BULK_REQ_RECD () = AST_FALSE;
    AST_NODE_STATUS () = RED_AST_IDLE;

    gAstRedGlobalInfo.u4BulkUpdNextContext = AST_INIT_VAL;
    gAstRedGlobalInfo.u4BulkUpdNextPort = AST_MIN_NUM_PORTS;
    gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
    gAstRedGlobalInfo.u4HwAuditTskId = AST_INIT_VAL;
    gAstRedGlobalInfo.AstRedTimesMemPoolId = AST_INIT_VAL;
    gAstRedGlobalInfo.AstRedDataMemPoolId = AST_INIT_VAL;
    gAstRedGlobalInfo.u1NumPeerPresent = AST_INIT_VAL;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedAllocPduMemPools                               */
/*                                                                           */
/* Description        : Allocate Memory pools to Store Pdus in Active Node   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstRedAllocPduMemPools ()
{
    /* Check if Pool Already created */
    if (AST_RED_DATA_MEMPOOL_ID != AST_INIT_VAL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                        "RED : Memory Pool Already created\n");
        return;
    }
    if (AST_CREATE_MEM_POOL
        ((UINT4) AST_RED_DATA_MEMBLK_SIZE,
         gFsRstpSizingParams[AST_RED_DATA_SIZING_ID].u4PreAllocatedUnits,
         AST_MEMORY_TYPE, &(AST_RED_DATA_MEMPOOL_ID)) == AST_MEM_FAILURE)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                        AST_ALL_FAILURE_TRC,
                        "RED: Memory Pool Creation failed !!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "RED: Memory Pool Creation failed !!!\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedRelPduMemPools                                 */
/* Description        : Releases PDU Mempools                                */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstRedRelPduMemPools ()
{
    if (AST_RED_DATA_MEMPOOL_ID == AST_INIT_VAL)
    {
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "RED: Memory Pool Deletion failed !!!\n");
        return;
    }

    AST_DELETE_MEM_POOL (AST_RED_DATA_MEMPOOL_ID);

    AST_RED_DATA_MEMPOOL_ID = AST_INIT_VAL;
}

/*****************************************************************************/
/* Function Name      : AstRedRelStoreMemPools                               */
/*                                                                           */
/* Description        : Releases Storage Memory back to the Pools            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstRedRelStoreMemPools ()
{
    if (AST_RED_TIMES_MEMPOOL_ID != AST_INIT_VAL)
    {
        AST_DELETE_MEM_POOL (AST_RED_TIMES_MEMPOOL_ID);
        AST_RED_TIMES_MEMPOOL_ID = AST_INIT_VAL;
    }
}

/*****************************************************************************/
/* Function Name      : AstRedSelectContext                                  */
/*                                                                           */
/* Description        : Selects a context for Spanning red information.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Id                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if select red context is success then RST_SUCCESS    */
/*                      Otherwise RST_FAILURE                                */
/*****************************************************************************/
INT4
AstRedSelectContext (UINT4 u4ContextId)
{
    /* As Non Red Context is selected successfully, Red Context will also
     * be selected successfully. Hence no check is done to verify that. */
    AST_RED_CURR_CONTEXT_INFO () =
        &(gAstRedGlobalInfo.aAstRedContextInfo[u4ContextId]);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedReleaseContext                                 */
/*                                                                           */
/* Description        : Relases the context for Spanning red information.    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedReleaseContext (VOID)
{
    AST_RED_CURR_CONTEXT_INFO () = NULL;

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Creates a queue for receiving messages from RM and   */
/*                      registers RSTP/MSTP/PVRST with RM.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then RST_SUCCESS          */
/*                      Otherwise RST_FAILURE                                 */
/*****************************************************************************/
INT4
AstRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_RSTPMSTP_APP_ID;
    RmRegParams.pFnRcvPkt = AstHandleUpdateEvents;
    if (AstRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                        "AstRegisterWithRM: Registration with RM FAILED\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedDeRegisterWithRM                               */
/*                                                                           */
/* Description        : Deregisters RSTP/MSTP/PVRST with RM.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then RST_SUCCESS        */
/*                      Otherwise RST_FAILURE                                */
/*****************************************************************************/
INT4
AstRedDeRegisterWithRM (VOID)
{
    if (AstRmDeRegisterProtocols () == RM_FAILURE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                        "AstRedDeRegisterWithRM: Deregistration with RM FAILED\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleUpdateEvents()                              */
/* Description        : RM processing Entry point                            */
/* Input(s)           : u1Event - The Event from RM to be handled            */
/*                      pData - The RM message                               */
/*                      u2DataLen - Length of Message                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tAstQMsg           *pMsg = NULL;
    tAstRmCtrlMsg      *pCtrlMsg = NULL;

    /* If RM_MESSAGE/RM_STANDBY_UP/RM_STANDBY_DOWN, pData cannot be NULL */
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent */
        return RM_FAILURE;
    }

    if (AST_IS_INITIALISED () != RST_TRUE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    if (AST_ALLOC_CFGQ_MSG_MEM_BLOCK (pMsg) == NULL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AstHandleUpdateEvents: Ctrl Message ALLOC_MEM_BLOCK FAILED\n");
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    if ((pCtrlMsg = (tAstRmCtrlMsg *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AstHandleUpdateEvents: Control Message ALLOC_MEM_BLOCK FAILED\n");
        AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pMsg);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    AST_MEMSET (pMsg, AST_INIT_VAL, sizeof (tAstQMsg));
    AST_MEMSET (pCtrlMsg, AST_INIT_VAL, sizeof (tAstRmCtrlMsg));

    AST_QMSG_TYPE (pMsg) = AST_RM_QMSG;

    pMsg->uQMsg.pRmMsg = pCtrlMsg;

    pMsg->uQMsg.pRmMsg->pFrame = pData;

    pMsg->uQMsg.pRmMsg->u2Length = u2DataLen;

    pMsg->uQMsg.pRmMsg->u1Event = u1Event;

    if (AST_SEND_TO_QUEUE (AST_CFG_QID,
                           (UINT1 *) &pMsg, AST_DEF_MSG_LEN)
        != AST_OSIX_SUCCESS)

    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                        "AstHandleUpdateEvents: Rm Message enqueue FAILED\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pMsg);
        AST_RELEASE_LOCALMSG_MEM_BLOCK (pCtrlMsg);

        return RM_FAILURE;
    }

    if (AST_SEND_EVENT (AST_TASK_ID, AST_MSG_EVENT) != OSIX_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                        "AstHandleUpdateEvents: Rm Event send FAILED\n");

        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstRedProtocolRestart                                */
/*                                                                           */
/* Description        : Restart protocol on a particular Port                */
/* Input(s)           : u2Port - Port                                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstRedProtocolRestart (UINT2 u2Port)
{

    if (RstDisablePort (u2Port, AST_STP_PORT_DOWN) != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Disable Port Failed \n");
        return;
    }
    if (RstEnablePort (u2Port, AST_STP_PORT_UP) != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Enabling Port Failed \n");
        return;
    }

}

/*****************************************************************************/
/* Function Name      : AstRedProtocolRestart                                */
/*                                                                           */
/* Description        : Restart protocol on a particular Port                */
/* Input(s)           : u2InstanceId - Instance in which Protocol needs to be*/
/*                                     restarted.                            */
/*                      u2Port - Port on which protocol needs to be restarted*/
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedProtocolRestart (UINT2 u2InstanceId, UINT2 u2Port)
{
#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        MstRedProtocolRestart (u2Port, u2InstanceId);
        return;
    }
#endif
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        PvrstRedProtocolRestart (u2Port, u2InstanceId);
        return;
    }
#endif
#ifdef RSTP_WANTED
    if (AST_IS_RST_ENABLED ())
    {
        RstRedProtocolRestart (u2Port);
        UNUSED_PARAM (u2InstanceId);
        return;
    }
#endif
}

/*****************************************************************************/
/* Function Name      : AstRedHwAudit                                        */
/* Description        : Audit Module to make sure if port states in H/W and  */
/*                      S/W are in sync                                      */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
/* Software State          Forwarding        Learning        Blocking        */
/* /Hardware State                                                           */
/*****************************************************************************/
/* Forwarding           Do Nothing        Forwarding       Blocking &        */
/*                                                          Protocol Restart */
/* Learning               Learning        Do Nothing       Blocking &        */
/*                                                          Protocol Restart */
/* Blocking               Blocking        Blocking         Do Nothing        */
/*****************************************************************************/
VOID
AstRedHwAudit (INT1 *pi1Param)
{
#ifdef NPAPI_WANTED
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2LocalPort;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_CONTROL_PATH_TRC,
                    "RED: Auditing sw and hw data.\n");

    AST_LOCK ();

    /*Clear all sync up data in the database */
    AstRedClearAllContextSyncUpData ();

    /* Release all stored Synced Data */
    AstRedRelStoreMemPools ();

    AST_UNLOCK ();

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        AST_LOCK ();
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            AST_UNLOCK ();
            continue;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_STARTED ())
        {
            AST_UNLOCK ();
            for (u2InstIndex = 1; u2InstIndex < AST_MAX_PVRST_INSTANCES;
                 u2InstIndex++)
            {
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    /* HwAudit is low priority task.We need to take & Release 
                     * lock for every loop to make other high priority task run 
                     * uninteruptedly */
                    AST_LOCK ();

                    /* Some other Task have the chance of deleting the Global 
                     * Context. So select the context before doing process */
                    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
                    {
                        u2InstIndex = AST_MAX_PVRST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
                    {
                        AST_DBG (AST_RED_DBG,
                                 "RED: PVRST Module not enabled\n");
                        u2InstIndex = AST_MAX_PVRST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if ((AST_IS_PROVIDER_EDGE_BRIDGE () == RST_FALSE) &&
                        (!(AST_IS_PVRST_ENABLED ())))
                    {
                        AST_DBG (AST_RED_DBG,
                                 "RED: PVRST Module not enabled\n");
                        u2InstIndex = AST_MAX_PVRST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if ((u2InstanceId =
                         PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
                    {
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }
                    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
                    {
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    /* Is Port Created ? */
                    pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
                    if (pPortEntry == NULL)
                    {
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        continue;
                    }

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE)
                    {
                        /* TBD */
                    }
                    else
                    {
                        /* Is Port Oper Up ? */
                        if (!AST_IS_PORT_UP (u2LocalPort))
                        {
                            AstReleaseContext ();
                            AST_UNLOCK ();
                            continue;
                        }

                        if (!(AST_IS_PVRST_ENABLED ()))
                        {
                            AST_DBG (AST_RED_DBG,
                                     "RED: PVRST Module not enabled\n");
                            u2InstIndex = AST_MAX_PVRST_INSTANCES;
                            AstReleaseContext ();
                            AST_UNLOCK ();
                            break;
                        }

                        if (AstRedPortInstHwAudit (u2InstanceId, pPortEntry) ==
                            RST_FAILURE)
                        {
                            /* In case of failure AstRedPortInstHwAudit releases
                             * the context and then unlocks. */
                            AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
                            return;
                        }
                    }
                    AstReleaseContext ();
                    AST_UNLOCK ();
                }
            }
        }
        else
#endif
        {
            AstReleaseContext ();
            AST_UNLOCK ();
#ifdef MSTP_WANTED
            for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                 u2InstanceId++)
            {
#endif
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    /* HwAudit is low priority task.We need to take & Release lock  
                     * for every loop to make other high priority task run 
                     * uninteruptedly */
                    AST_LOCK ();

                    /* Some other Task have the chance of deleting the Global 
                     * Context. So select the context before doing process */
                    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
                    {
                        u2InstanceId = AST_MAX_MST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
                    {
                        AST_DBG (AST_RED_DBG,
                                 "RED: RSTP/MSTP Module not enabled\n");
                        u2InstanceId = AST_MAX_MST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if ((AST_IS_PROVIDER_EDGE_BRIDGE () == RST_FALSE) &&
                        (!(AST_IS_RST_ENABLED ()) && !(AST_IS_MST_ENABLED ())))
                    {
                        AST_DBG (AST_RED_DBG,
                                 "RED: RSTP/MSTP Module not enabled\n");
                        u2InstanceId = AST_MAX_MST_INSTANCES;
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    if ((pPerStInfo =
                         AST_GET_PERST_INFO (u2InstanceId)) == NULL)
                    {
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        break;
                    }

                    /* Is Port Created ? */
                    pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
                    if (pPortEntry == NULL)
                    {
                        AstReleaseContext ();
                        AST_UNLOCK ();
                        continue;
                    }

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE)
                    {
                        if (u2InstanceId == 0)
                        {
                            AstReleaseContext ();
                            AST_UNLOCK ();

                            if (AstRedPbCvlanHwAudit (u4ContextId, u2LocalPort)
                                == RST_FAILURE)
                            {
                                AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
                                return;
                            }
                            continue;
                        }
                    }
                    else
                    {
                        /* Is Port Oper Up ? */
                        if (!AST_IS_PORT_UP (u2LocalPort))
                        {
                            AstReleaseContext ();
                            AST_UNLOCK ();
                            continue;
                        }

                        if (!(AST_IS_RST_ENABLED ())
                            && !(AST_IS_MST_ENABLED ()))
                        {
                            AST_DBG (AST_RED_DBG,
                                     "RED: RSTP/MSTP Module not enabled\n");
                            u2InstanceId = AST_MAX_MST_INSTANCES;
                            AstReleaseContext ();
                            AST_UNLOCK ();
                            break;
                        }

                        if (AstRedPortInstHwAudit (u2InstanceId, pPortEntry) ==
                            RST_FAILURE)
                        {
                            /* In case of failure AstRedPortInstHwAudit releases
                             * the context and then unlocks. */
                            AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
                            return;
                        }
                    }
                    AstReleaseContext ();
                    AST_UNLOCK ();
                }
#ifdef MSTP_WANTED
            }
#endif
        }
    }
#endif
    AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
    UNUSED_PARAM (pi1Param);

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedMakeNodeActiveFromIdle                         */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                      in Idle state and is requested to go to active state */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Node status .                             */
/* Global Variables                                                          */
/* Modified           : AST Module Node status .                             */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedMakeNodeActiveFromIdle ()
{
    UINT4               u4ContextId;

    AST_NODE_STATUS () = RED_AST_ACTIVE;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }
#if NPAPI_WANTED
        RstpFsMiStpNpHwInit (AST_CURR_CONTEXT_ID ());
#endif

#ifdef PVRST_WANTED
        if (AST_IS_PVRST_STARTED ())
        {
            if (AST_ADMIN_STATUS == PVRST_ENABLED)
            {
                PvrstModuleEnable ();
            }
        }
#endif
#ifdef MSTP_WANTED
        if (AST_IS_MST_STARTED ())
        {
            if (AST_ADMIN_STATUS == MST_ENABLED)
            {
                MstModuleEnable ();
            }
        }
#endif
#ifdef RSTP_WANTED
        if (AST_IS_RST_STARTED ())
        {
            if (AST_ADMIN_STATUS == RST_ENABLED)
            {
                RstModuleEnable ();
            }
        }
#endif
        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedApplyLatestPduInfo                             */
/*                                                                           */
/* Description        : Validates the latest PDU to be applied to the SEMs   */
/*                      and apply them when relevent                         */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id                           */
/*                      u2LocalPort - Port Number                            */
/*                      pAstRedPdu  - Received Bpdu                          */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstRedApplyLatestPduInfo (UINT2 u2InstanceId, UINT2 u2LocalPort,
                          tAstBpdu * pAstRedPdu)
{
    UNUSED_PARAM (u2InstanceId);

    if (RstHandleInBpdu (pAstRedPdu, u2LocalPort) != RST_SUCCESS)
    {
        AST_DBG (AST_RED_DBG, "RED : Applying Latest BPDU failed\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedUpdatePortStates                               */
/*                                                                           */
/* Description        : Update synced Up Port state                          */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id                           */
/*                      u2LocalPort - Port Id to update                      */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedUpdatePortStates (UINT2 u2InstanceId, UINT2 u2LocalPort)
{
    UINT1               u1PortState;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT4                i4Retval = RST_FAILURE;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            return;
        }
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex);
        if (NULL == pPerStPortInfo)
        {
            return;
        }
        u1PortState = AST_RED_PORT_STATE (u2InstIndex, u2LocalPort);
        if (u1PortState == 0)
        {
            /* FDWhile never started */
            return;
        }
    }
    else
#endif
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId);
        if (NULL == pPerStPortInfo)
        {
            return;
        }

        u1PortState = AST_RED_PORT_STATE (u2InstanceId, u2LocalPort);
        if (u1PortState == 0)
        {
            /* FDWhile never started */
            return;
        }
    }

    if (u1PortState == AST_PORT_STATE_FORWARDING)
    {
        if (AST_IS_RST_ENABLED ())
        {
            i4Retval = RstProleTrSmMakeLearn (pPerStPortInfo);
            i4Retval = RstProleTrSmMakeForward (pPerStPortInfo);
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            MstPRoleTrSmMakeLearn (pPerStPortInfo, u2InstanceId);
            i4Retval = MstPRoleTrSmMakeForward (pPerStPortInfo, u2InstanceId);
        }
#endif
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            i4Retval = PvrstProleTrSmMakeLearn (pPerStPortInfo);
            i4Retval = PvrstProleTrSmMakeForward (pPerStPortInfo);
        }
#endif
    }
    else if (u1PortState == AST_PORT_STATE_LEARNING)
    {
        if (AST_IS_RST_ENABLED ())
        {
            RstProleTrSmMakeLearn (pPerStPortInfo);
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            MstPRoleTrSmMakeLearn (pPerStPortInfo, u2InstanceId);
        }
#endif
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            i4Retval = PvrstProleTrSmMakeLearn (pPerStPortInfo);
        }
#endif
    }
    UNUSED_PARAM (i4Retval);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedUpdateAllPortStates                            */
/*                                                                           */
/* Description        : Update all the synced Up Port state                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedUpdateAllPortStates ()
{
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortIfIndex;
    UINT4               u4ContextId;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
        {
            AST_DBG (AST_RED_DBG, "RED: Spanning Tree is not enabled in this "
                     "Context.\n");
            AstReleaseContext ();
            continue;
        }

        /* In case of Provider Edge Bridge, update the C-VLAN synced up
         * port states first. */
        if (AST_IS_PROVIDER_EDGE_BRIDGE () == RST_TRUE)
        {
            for (u2PortIfIndex = 1; u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                 u2PortIfIndex++)
            {
                /* Is Port Created ? */
                if (AST_GET_PORTENTRY (u2PortIfIndex) == NULL)
                    continue;

                if (AST_IS_CUSTOMER_EDGE_PORT (u2PortIfIndex) == RST_TRUE)
                {
                    AstRedPbUpdtCvlanPortStates (u2PortIfIndex);
                }
            }
        }

        /* Update synced up port states. In case of PEB, update S-VLAN
         * component synced up port states. */

        if (!(AST_IS_RST_ENABLED ()) && !(AST_IS_MST_ENABLED ()) &&
            !(AST_IS_PVRST_ENABLED ()))
        {
            AST_DBG (AST_RED_DBG, "RED: RSTP/MSTP/PVRST Module not enabled\n");
            AstReleaseContext ();
            continue;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
                 u2InstIndex++)
            {
                if ((u2InstanceId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) ==
                    AST_INIT_VAL)
                {
                    continue;
                }
                if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
                {
                    continue;
                }
                for (u2PortIfIndex = 1;
                     u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                     u2PortIfIndex++)
                {
                    /* Is Port Created ? */
                    if (AST_GET_PORTENTRY (u2PortIfIndex) == NULL)
                        continue;

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortIfIndex) == RST_FALSE)
                    {
                        /* Oper UP ? */
                        if (!AST_IS_PORT_UP (u2PortIfIndex))
                            continue;

                        AstRedUpdatePortStates (u2InstanceId, u2PortIfIndex);
                    }

                }
            }
        }
        else
#endif
        {
#ifdef MSTP_WANTED
            for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                 u2InstanceId++)
            {
                if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
                {
                    continue;
                }
#endif
                for (u2PortIfIndex = 1;
                     u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                     u2PortIfIndex++)
                {
                    /* Is Port Created ? */
                    if (AST_GET_PORTENTRY (u2PortIfIndex) == NULL)
                        continue;

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortIfIndex) == RST_FALSE)
                    {
                        /* Oper UP ? */
                        if (!AST_IS_PORT_UP (u2PortIfIndex))
                            continue;

                        AstRedUpdatePortStates (u2InstanceId, u2PortIfIndex);
                    }
                }
#ifdef MSTP_WANTED
            }
#endif
        }
        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedGetNextTimer                                   */
/*                                                                           */
/* Description        : Get the next timer to be applied and the duration    */
/*                      for which it has to be run                           */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Identifier                   */
/*                      u2PortIfIndex - Port Index                           */
/*                      u1TimerType - Type of the Timer                      */
/*                                                                           */
/* Output(s)          : pu1TimerType - Pointer to the Timer Structure        */
/*                      pu2Duration - Duration of Timer                      */
/*                      ppPortPtr - Pointer to the corresponding Port entry  */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedGetNextTimer (UINT2 u2InstanceId, UINT2 u2PortIfIndex,
                    UINT1 u1TimerType, UINT1 *pu1TimerType,
                    UINT2 *pu2Duration, tAstTimer ** ppPortPtr)
{
    tAstBridgeEntry    *pBrgEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstRedTimes       *pRedTimes = NULL;
    tOsixSysTime        CurrTime = AST_INIT_VAL;
    tOsixSysTime        StoredTime = AST_INIT_VAL;
    UINT4               u4MaxTime = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstTimer          *pAstTimer = NULL;
    INT4                i4Retval = RST_FAILURE;

    pRedTimes = AST_RED_TIMES_PTR (u2InstanceId, u2PortIfIndex);

    if (NULL == pRedTimes)
        return RST_FAILURE;

    AST_GET_TIME_STAMP (&CurrTime);
    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            *pu1TimerType = AST_TMR_TYPE_RBWHILE;
            StoredTime = pRedTimes->u4FdWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = pBrgEntry->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId))->
                pFdWhileTmr;
            break;

        case AST_TMR_TYPE_RBWHILE:
            *pu1TimerType = AST_TMR_TYPE_RCVDINFOWHILE;
            StoredTime = pRedTimes->u4RbWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = 2 * pBrgEntry->RootTimes.u2HelloTime;
            *ppPortPtr =
                (AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId))->
                pRbWhileTmr;
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            *pu1TimerType = AST_TMR_TYPE_RRWHILE;
            StoredTime = pRedTimes->u4RcvdInfoWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = (pBrgEntry->RootTimes.u2HelloTime);
            *ppPortPtr =
                (AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId))->
                pRcvdInfoTmr;
            break;

        case AST_TMR_TYPE_RRWHILE:
            *pu1TimerType = AST_TMR_TYPE_TCWHILE;
            StoredTime = pRedTimes->u4RrWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = pBrgEntry->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId))->
                pRrWhileTmr;
            break;

        case AST_TMR_TYPE_TCWHILE:
            if (u2InstanceId == 0)
            {
                *pu1TimerType = AST_TMR_TYPE_MDELAYWHILE;
            }
            else
            {                    /* Remaining timers are per Port Timers */
                *pu1TimerType = AST_RED_TIMER_TYPE_INVALID;
            }
            StoredTime = pRedTimes->u4TcWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = pBrgEntry->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId))->
                pTcWhileTmr;
            break;

        case AST_TMR_TYPE_MDELAYWHILE:

            *pu1TimerType = AST_TMR_TYPE_EDGEDELAYWHILE;
            StoredTime = pRedTimes->u4MdelayWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = pBrgEntry->u1MigrateTime;
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            *ppPortPtr = pCommPortInfo->pMdWhileTmr;
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:

            *pu1TimerType = AST_TMR_TYPE_RAPIDAGE_DURATION;
            StoredTime = pRedTimes->u4EdgeDelayWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = pBrgEntry->u1MigrateTime;
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            *ppPortPtr = pCommPortInfo->pEdgeDelayWhileTmr;
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            *pu1TimerType = AST_TMR_TYPE_HOLD;
            StoredTime = pRedTimes->u4RapidAgeDurtnExpTime;
            pPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
            u4MaxTime = pPortEntry->DesgTimes.u2ForwardDelay + 1;
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            *ppPortPtr = pCommPortInfo->pRapidAgeDurtnTmr;
            break;

        case AST_TMR_TYPE_HOLD:
            *pu1TimerType = AST_TMR_TYPE_HELLOWHEN;

            /* Release the allocated memory for Hold Timer.
             * Hold Timer will start in Tx State Machine */
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            if (pCommPortInfo->pHoldTmr != NULL)
            {
                if (pCommPortInfo->pHoldTmr->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pCommPortInfo->
                                          pHoldTmr->AstAppTimer)) ==
                        AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Hold Timer FAILED!\n");
                    }
                    pCommPortInfo->pHoldTmr->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pCommPortInfo->pHoldTmr)
                    != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Hold Timer Memory Release FAILED!\n");
                }
                pCommPortInfo->pHoldTmr = NULL;
            }
            *pu2Duration = 0;
            *ppPortPtr = pCommPortInfo->pHoldTmr;
            return RST_SUCCESS;

        case AST_TMR_TYPE_HELLOWHEN:
            *pu1TimerType = AST_TMR_TYPE_PSEUDOINFOHELLOWHEN;

            /* Release the allocated memory for Hello Timer.
             * Hello Timer will start in Tx State Machine */
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            if (pCommPortInfo->pHelloWhenTmr != NULL)
            {
                if (pCommPortInfo->pHelloWhenTmr->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pCommPortInfo->pHelloWhenTmr->
                                          AstAppTimer)) == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Hello Timer FAILED!\n");
                    }
                    pCommPortInfo->pHelloWhenTmr->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pCommPortInfo->pHelloWhenTmr)
                    != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Hold Timer Memory Release FAILED!\n");
                }
                pCommPortInfo->pHelloWhenTmr = NULL;
            }
            *pu2Duration = 0;
            *ppPortPtr = pCommPortInfo->pHelloWhenTmr;

            /* Send RST_PTXSM_EV_BEGIN Event To PortTx state machine to 
             * start sending Hello Bpdus */
            pPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
            if (pPortEntry != NULL)
            {
                RstPortTransmitMachine (RST_PTXSM_EV_BEGIN,
                                        pPortEntry, u2InstanceId);
            }

            return RST_SUCCESS;

        case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:
            *pu1TimerType = AST_RED_TIMER_TYPE_INVALID;

            /* Release the allocated memory for PseudoHelloWhen Timer.
             * PseudoHelloWhen Timer will start in PseudoInfo State Machine */
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            if (pCommPortInfo->pPseudoInfoHelloWhenTmr != NULL)
            {
                if (pCommPortInfo->pPseudoInfoHelloWhenTmr->u1IsTmrStarted
                    == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pCommPortInfo->
                                          pPseudoInfoHelloWhenTmr->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Pseudo Info Hello "
                                 "Timer FAILED!\n");
                    }
                    pCommPortInfo->pPseudoInfoHelloWhenTmr->u1IsTmrStarted
                        = RST_FALSE;
                }
                if (AST_RELEASE_TMR_MEM_BLOCK
                    (pCommPortInfo->pPseudoInfoHelloWhenTmr) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PseudoHelloWhen Timer Memory Release FAILED!\n");
                }
                pCommPortInfo->pPseudoInfoHelloWhenTmr = NULL;
            }
            *pu2Duration = 0;
            *ppPortPtr = pCommPortInfo->pPseudoInfoHelloWhenTmr;

            /* Send RST_PSEUDO_INFO_EV_BEGIN Event To PseudoInfo state
             * machine to start sending PseudoInfo BPDU*/
            pPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
            if (pPortEntry != NULL)
            {
                i4Retval =
                    RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_BEGIN,
                                          u2PortIfIndex, NULL);
            }
            UNUSED_PARAM (i4Retval);
            return RST_SUCCESS;

        case AST_RED_TIMER_TYPE_INVALID:

            if (NULL != pRedTimes)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_TIMES_MEMPOOL_ID, pRedTimes);
                AST_RED_TIMES_PTR (u2InstanceId, u2PortIfIndex) = NULL;
            }
            return RST_FAILURE;

            break;

        default:
            break;
    }

    if (0 == StoredTime)
    {
        *pu2Duration = 0;
    }
    else
    {
        if (StoredTime > CurrTime)
        {
            /* Expected Expiry Time - current Time 
               will give the duration for which the timer 
               has to be run in standby Node */
            *pu2Duration = (UINT2) (StoredTime - CurrTime);
            *pu2Duration =
                (UINT2) (*pu2Duration % (u4MaxTime * SYS_TIME_TICKS_IN_A_SEC));
        }
        else
        {
            /* This can happen if the timer Stop/Expiry packet 
               has been Missed */
            *pu2Duration = 0;

            /* This case comes only when FD_While expiry has been missed 
             * during SyncUp. FD_While Timer needs to be started in this case. 
             * So send an expiry event to State Machine to restart the timer. */

            if (u1TimerType == AST_TMR_TYPE_FDWHILE)
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortIfIndex,
                                                          u2InstanceId);

                pAstTimer = pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr;
                if ((pAstTimer != NULL)
                    && (pAstTimer->u1IsTmrStarted == RST_TRUE))
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping FD While Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }
                pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;

                if (AST_IS_RST_ENABLED ())
                {
                    i4Retval =
                        RstPortRoleTrMachine ((UINT2)
                                              RST_PROLETRSM_EV_FDWHILE_EXP,
                                              pPerStPortInfo);
                }
#ifdef MSTP_WANTED
                else if (AST_IS_MST_ENABLED ())
                {
                    i4Retval =
                        MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_FDWHILE_EXPIRED, pPerStPortInfo,
                         u2InstanceId);
                }
#endif
                if (pAstTimer != NULL)
                {
                    if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Expired Timer Memory Block Release FAILED!\n");
                    }
                }
            }
        }
    }

    UNUSED_PARAM (i4Retval);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedApplyPerPortTimers                             */
/*                                                                           */
/* Description        : Timer Updations when relevent .Philosophy is to      */
/*                      run the timers for the remaining time for the given  */
/*                      port and for given MST instance.                     */
/*                       Remaining time                                      */
/*                        = [Expected Expiry time - Current time]            */
/*                                                                           */
/* Input(s)           : pAstPortEntry - Pointer to port.                     */
/*                      u2InstanceId - Mst Instance Id.                      */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedApplyPerPortTimers (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstTimer          *pAstTimer = NULL;
    tAstTimer          *pPortTimer = NULL;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT1               u1TimerType = AST_TMR_TYPE_FDWHILE;
    UINT1               u1NextTimerType = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    u2LocalPort = pAstPortEntry->u2PortNo;

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        return;
    }
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            return;
        }
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex);
        if (NULL == pPerStPortInfo)
        {
            return;
        }
    }
    else
#endif
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId);

        if (pPerStPortInfo == NULL)
        {
            return;
        }
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* STP enabled for this Instance */
    if (pRstPortInfo->bPortEnabled == AST_TRUE)
    {
        /* When doing a force switchover, the protocol is disabled and enabled
         * on new standby (previous active). This may cause the TcWhile timer
         * to be started on the standby alone, even if it is not started on the
         * Active. But on the standby, the timer is not actually started in
         * FSAP. Only the timer memory-block is allocated. This means that this
         * timer will never expire, since it is not started in the Active node.
         * Hence we have to forcefully free these memory-blocks, when the
         * Standby becomes Active.
         *
         * The timer memory block should be released, only when the timer is 
         * not running. Because there are possibilities, where the TC-while
         * timer can be started by the Protocol-state-machine once the node
         * becomes active. Hence u1IsTmrStarted flag is verified before
         * releasing the memory.
         */
        if ((pRstPortInfo->pTcWhileTmr != NULL) &&
            (pRstPortInfo->pTcWhileTmr->u1IsTmrStarted != RST_TRUE))
        {
            if (AST_RELEASE_TMR_MEM_BLOCK (pRstPortInfo->pTcWhileTmr)
                != AST_MEM_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Memory Block Release FAILED!\n");
            }
            pRstPortInfo->pTcWhileTmr = NULL;
        }

#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            while ((PvrstRedGetNextTimer
                    (u2InstanceId, u2LocalPort, u1TimerType,
                     &u1NextTimerType, &u2Duration,
                     &pPortTimer)) != PVRST_FAILURE)
            {
                pAstTimer = pPortTimer;

                if (u2Duration != 0)
                {
                    if (pAstTimer == NULL)
                    {
                        if (u1TimerType == AST_TMR_TYPE_EDGEDELAYWHILE)
                        {
                            if (PvrstStartTimer ((VOID *) pAstPortEntry,
                                                 u2InstIndex,
                                                 u1TimerType,
                                                 u2Duration) != PVRST_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: PvrstStartTimer FAILED!\n");
                            }
                        }
                        else
                        {
                            if (PvrstStartTimer ((VOID *) pPerStPortInfo,
                                                 u2InstIndex,
                                                 u1TimerType,
                                                 u2Duration) != PVRST_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: PvrstStartTimer FAILED!\n");
                            }
                        }
                    }
                    else if (pAstTimer != NULL)
                    {
                        if (AST_START_TIMER
                            (AST_TMR_LIST_ID,
                             &(pAstTimer->AstAppTimer),
                             u2Duration) != AST_TMR_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                     "RED: Starting Timer FAILED!\n");

                            if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer)
                                != AST_MEM_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: MemBlock Release FAILED!\n");
                            }
                        }
                        else
                        {
                            pAstTimer->u1IsTmrStarted = RST_TRUE;
                        }
                    }
                }
                u1TimerType = u1NextTimerType;
            }
        }
        else
#endif
        {
            while ((AstRedGetNextTimer
                    (u2InstanceId, u2LocalPort, u1TimerType,
                     &u1NextTimerType, &u2Duration,
                     &pPortTimer)) != RST_FAILURE)
            {
                pAstTimer = pPortTimer;

                if (u2Duration != 0)
                {
                    if (pAstTimer == NULL)
                    {
                        if (u1TimerType == AST_TMR_TYPE_MDELAYWHILE ||
                            u1TimerType == AST_TMR_TYPE_EDGEDELAYWHILE
                            || u1TimerType == AST_TMR_TYPE_RAPIDAGE_DURATION)
                        {
                            if (AstStartTimer ((VOID *) pAstPortEntry,
                                               u2InstanceId,
                                               u1TimerType,
                                               u2Duration) != RST_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: AstStartTimer FAILED!\n");
                            }
                        }
                        else
                        {
                            if (AstStartTimer ((VOID *) pPerStPortInfo,
                                               u2InstanceId,
                                               u1TimerType,
                                               u2Duration) != RST_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: AstStartTimer FAILED!\n");
                            }
                        }
                    }
                    else if (pAstTimer != NULL)
                    {
                        if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                        {
                            /* Stop the Timer */
                            if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                                &(pAstTimer->AstAppTimer))
                                == AST_TMR_FAILURE)
                            {
                                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                         "TMR: Stopping Timer FAILED!\n");
                            }
                            pAstTimer->u1IsTmrStarted = RST_FALSE;
                        }

                        if (AST_START_TIMER
                            (AST_TMR_LIST_ID,
                             &(pAstTimer->AstAppTimer),
                             u2Duration) != AST_TMR_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                     "RED: Starting Timer FAILED!\n");

                            if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer)
                                != AST_MEM_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "RED: MemBlock Release FAILED!\n");
                            }
                        }
                        else
                        {
                            pAstTimer->u1IsTmrStarted = RST_TRUE;
                        }
                    }
                }
                else
                {
                    if (u1TimerType == AST_TMR_TYPE_RCVDINFOWHILE)
                    {
                        if (pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr !=
                            NULL)
                        {
                            AstRedHandleRcvdInfoWhileTmrExp (pPerStPortInfo);
                        }
                    }
                }
                u1TimerType = u1NextTimerType;
            }
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedApplyTimers                                    */
/*                                                                           */
/* Description        : Timer Updations when relevent .Philosophy is to      */
/*                      run the timers for the remaining time                */
/*                       Remaining time                                      */
/*                        = [Expected Expiry time - Current time]            */
/*                                                                           */
/* Input(s)           : AuditFlag                                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedApplyTimers (UINT1 *pu1AuditFlag)
{
#ifdef MSTP_WANTED
    tAstPerStInfo      *pPerStInfo = NULL;
#endif
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortIfIndex = AST_INIT_VAL;
    UINT4               u4ContextId;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
        {
            AstReleaseContext ();
            continue;
        }

        *pu1AuditFlag = RST_TRUE;

        if (AST_IS_PROVIDER_EDGE_BRIDGE () == RST_TRUE)
        {
            for (u2PortIfIndex = 1; u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                 u2PortIfIndex++)
            {
                pAstPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);

                if ((pAstPortEntry != NULL) &&
                    (AST_GET_BRG_PORT_TYPE (pAstPortEntry) ==
                     AST_CUSTOMER_EDGE_PORT))
                {
                    AstRedPbApplyCvlanTimers (pAstPortEntry);
                }
            }
        }

        if (!(AST_IS_RST_ENABLED ()) && !(AST_IS_MST_ENABLED ()) &&
            !(AST_IS_PVRST_ENABLED ()))
        {
            AstReleaseContext ();
            continue;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
                 u2InstIndex++)
            {
                if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
                {
                    continue;
                }
                if ((u2InstanceId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) ==
                    AST_INIT_VAL)
                {
                    continue;
                }

                for (u2PortIfIndex = 1;
                     u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                     u2PortIfIndex++)
                {
                    /* Is Port Created ? */
                    pAstPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
                    if (pAstPortEntry == NULL)
                        continue;

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortIfIndex) == RST_FALSE)
                    {
                        AstRedApplyPerPortTimers (pAstPortEntry, u2InstanceId);
                    }
                }
            }
        }
        else
#endif
        {
#ifdef MSTP_WANTED
            for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                 u2InstanceId++)
            {
                if ((pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)) == NULL)
                {
                    continue;
                }
#endif
                for (u2PortIfIndex = 1;
                     u2PortIfIndex <= AST_MAX_PORTS_PER_CONTEXT;
                     u2PortIfIndex++)
                {
                    /* Is Port Created ? */
                    pAstPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
                    if (pAstPortEntry == NULL)
                        continue;

                    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortIfIndex) == RST_FALSE)
                    {
                        AstRedApplyPerPortTimers (pAstPortEntry, u2InstanceId);
                    }
                }
#ifdef MSTP_WANTED
            }
#endif
        }
        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedTriggerHigherLayer                             */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module     */
/*                      intimates the Higer Layer for sequencing             */
/* Input(s)           : u1TrigType - Type of Trigger given to higher layer   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedTriggerHigherLayer (UINT1 u1TrigType)
{
#ifdef VLAN_WANTED
    AstVlanRedHandleUpdateEvents (u1TrigType, 0, 0);
#elif LLDP_WANTED
    AstLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((AST_NODE_STATUS () == RED_AST_ACTIVE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                            AST_OS_RESOURCE_TRC | AST_ALL_FAILURE_TRC,
                            "AST send event L2_SYNC_UP_COMPLETED to RM failed\n");
        }
    }
#endif
    /* IGS is not intimated as for IGS to exist Vlan 
     * is very much a dependency  */
}

/*****************************************************************************/
/* Function Name      : AstRedMakeNodeActiveFromStandby                      */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                      in Standby Mode and is requested to go to ACTIVE     */
/*                      mode                                                 */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Node Status.                              */
/* Global Variables                                                          */
/* Modified           : AST Module Node Status.                              */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedMakeNodeActiveFromStandby ()
{
#ifdef MSTP_WANTED
    UINT4               u4ContextId;
#endif
    UINT1               u1AuditFlag = RST_FALSE;
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_RED_DBG,
                    "RED: RSTP/MSTP SWITCHOVER Started\n");

    /* Enable Tx */
    AST_NODE_STATUS () = RED_AST_ACTIVE;

    /* Apply the synchronised Timers */
    AstRedApplyTimers (&u1AuditFlag);
#ifdef MSTP_WANTED 
    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }
        if (AST_IS_MST_ENABLED ())
        {    
            if (MstHwReInit () != MST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                        " Hardware Reinitialization failed !!!\n");
            }
        }
        AstReleaseContext (); 
    }
#endif
    /* Initiate Hardware Audit */
    if (u1AuditFlag == RST_TRUE)
    {
        AST_RM_IS_HW_AUDIT_REQ () = AST_TRUE;
    }
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_RED_DBG,
                    "RED: RSTP/MSTP/PVRST SWITCHOVER Complete\n");
}

/*****************************************************************************/
/* Function Name      : AstRedMakeNodeStandbyFromIdle                        */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                       requested to go from IDLE state to STANDBY state    */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Node Status.                              */
/* Global Variables                                                          */
/* Modified           : AST Module Node Status.                              */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedMakeNodeStandbyFromIdle (VOID)
{
    UINT4               u4ContextId;

    AST_NODE_STATUS () = RED_AST_STANDBY;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }
        if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED () ||
            AST_IS_PVRST_ENABLED ())
        {
            AstReleaseContext ();
            continue;
        }
#ifdef MSTP_WANTED
        if (AST_IS_MST_STARTED ())
        {
            if (AST_ADMIN_STATUS == MST_ENABLED)
            {
                MstModuleEnable ();
            }
        }
#endif
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_STARTED ())
        {
            if (AST_ADMIN_STATUS == PVRST_ENABLED)
            {
                PvrstModuleEnable ();
            }
        }
#endif
#ifdef RSTP_WANTED
        if (AST_IS_RST_STARTED ())
        {
            if (AST_ADMIN_STATUS == RST_ENABLED)
            {
                RstModuleEnable ();
            }
        }
#endif
        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedAllocStoreMemPools                             */
/*                                                                           */
/* Description        :Memory allocated for storage of synched up data in    */
/*                     Standby in "Store and Apply" Design.In apply          */
/*                     Immediately we do not need this storage space         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedAllocStoreMemPools (VOID)
{
    if (AST_RED_TIMES_MEMPOOL_ID == AST_INIT_VAL)
    {
        if (AST_CREATE_MEM_POOL
            ((UINT4) AST_RED_TIMES_MEMBLK_SIZE,
             gFsRstpSizingParams[AST_RED_TIMES_SIZING_ID].u4PreAllocatedUnits,
             AST_MEMORY_TYPE, &(AST_RED_TIMES_MEMPOOL_ID)) == AST_MEM_FAILURE)
        {
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                            AST_ALL_FAILURE_TRC,
                            "SYS: Memory Pool Creation failed !!!\n");
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "SYS: Memory Pool Creation failed !!!\n");
            return;
        }
    }
}

/*****************************************************************************/
/* Function Name      : AstRedMakeNodeStandbyFromActive                      */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                      in ACTIVE state and requested to go to STANDBY state */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Node Status.                              */
/* Global Variables                                                          */
/* Modified           : AST Module Node Status.                              */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedMakeNodeStandbyFromActive (VOID)
{
    UINT4               u4ContextId;

    /* Clear PDUs on all the port */
    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }
        if (AST_IS_RST_ENABLED ())
        {
            if (AstRedClearAllPduOnActive () != RST_SUCCESS)
            {
                AST_DBG (AST_RED_DBG, "Memory release failed\n");
            }
        }
#ifdef MSTP_WANTED
        if (AST_IS_MST_ENABLED ())
        {
            if (MstRedClearAllBpdusOnActive () != MST_SUCCESS)
            {
                AST_DBG (AST_RED_DBG, "Memory release failed\n");
            }
        }
#endif
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            if (PvrstRedClearAllBpdusOnActive () != PVRST_SUCCESS)
            {
                AST_DBG (AST_RED_DBG, "Memory release failed\n");
            }
        }
#endif
        AstReleaseContext ();
    }

    /* In this transition, Protocol timers started in the active node must be
     * stopped, but NPAPI's should not be invoked. Dynamic protocol changes
     * should be flushed.
     * This state is introduced to stop the protocol timers.
     */
    AST_NODE_STATUS () = RED_AST_FORCE_SWITCHOVER_INPROGRESS;

    AST_NUM_STANDBY_NODES () = 0;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }
        if (AST_ADMIN_STATUS == RST_ENABLED)
        {
            if (AST_IS_RST_ENABLED ())
            {
                /* Disabling RSTP Module to flush dynamic protocol changes */
                if (RstModuleDisable () != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: RstModule Disable function returned FAILURE\n");
                    AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: RstModule Disable function returned FAILURE\n");
                }
                AST_TRC (AST_INIT_SHUT_TRC,
                         "MSG: RSTP Module Shutdown Successfully...\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                         "MSG: RSTP Module Disable Successfully...\n");

                if (RstModuleEnable () != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: RstModuleEnable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: RstModuleEnable function returned FAILURE\n");

                }
            }
#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {
                if (MstModuleDisable () != MST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: MstModule Disable function returned FAILURE\n");
                    AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: MstModule Disable function returned FAILURE\n");
                }
                AST_TRC (AST_INIT_SHUT_TRC,
                         "MSG: MSTP Module Shutdown Successfully...\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                         "MSG: MSTP Module Shutdown Successfully...\n");

                if (MstModuleEnable () != MST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: MstModuleEnable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: MstModuleEnable function returned FAILURE\n");

                }
            }
#endif
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                if (PvrstModuleDisable () != PVRST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: PvrstModule Disable function returned FAILURE\n");
                    AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: PvrstModule Disable function returned FAILURE\n");
                }
                AST_TRC (AST_INIT_SHUT_TRC,
                         "MSG: PVRST Module Shutdown Successfully...\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                         "MSG: PVRST Module Shutdown Successfully...\n");

                if (PvrstModuleEnable () != PVRST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: PvrstModuleEnable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: PvrstModuleEnable function returned FAILURE\n");
                }
            }
#endif
        }
        AstReleaseContext ();
    }

    AST_NODE_STATUS () = RED_AST_STANDBY;

    AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
}

/*****************************************************************************/
/* Function Name      : AstRedSendBulkRequest                                */
/*                                                                           */
/* Description        : This function is invoked whenever Bulk Request       */
/*                       needs to be send                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedSendBulkRequest (VOID)
{
    /* Initialte a Bulk Request to the ACTIVE */
    AstRedSendSyncMessages (0, 0, RED_AST_BULK_REQ, 0);
}

/*****************************************************************************/
/* Function Name      : AstRedHandleBulkRequest                              */
/*                                                                           */
/* Description        : This function is invoked whenever AST module         */
/*                      requests for Bulk Update                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : Constructs Bulk Update                               */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleBulkRequest (VOID)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2LocalPort;
    UINT4               u4Offset = AST_INIT_VAL;
    UINT4               u4BulkUpdPortCnt = AST_INIT_VAL;
    VOID               *pBuf = NULL;
    UINT4               u4ContextId;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (!(AST_IS_INITIALISED ()))
    {
        /* AST completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        AstRmSetBulkUpdatesStatus (RM_RSTPMSTP_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        AstRedSendBulkUpdateTailMsg ();

        return;
    }
    u4BulkUpdPortCnt = AST_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u4ContextId = gAstRedGlobalInfo.u4BulkUpdNextContext;
         u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            gAstRedGlobalInfo.u4BulkUpdNextContext++;
            continue;
        }

        /* In case of 1ad bridges, there are three module status present.
         * 1. Global module status per Virtual context.
         * 2. Provider spanning tree module status per virtual context.
         * 3. C-VLAN spanning tree status per C-VLAN context.
         * If Global module status is disabled, then Provider spanning tree
         * and all C-VLAN spanning tree in that context will be operationally
         * disable.
         * Otherwise Provider Spanning tree status and C-VLAN spanning tree
         * status are independent. 
         * So based on the above criteria decide when to skip the context. */
        if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
        {
            /* Global spanning tree status for this context is disabled. */
            AstReleaseContext ();
            gAstRedGlobalInfo.u4BulkUpdNextContext++;
            continue;
        }

        /* C-VLAN component will be present only in Provider Edge Bridges.
         * If the bridge is not a PEB, then the only spanning tree will 
         * run in the bridge. If that is disabled then skip. */
        if ((AST_BRIDGE_MODE () != AST_PROVIDER_EDGE_BRIDGE_MODE) &&
            (!(AST_IS_MST_ENABLED ()) && !(AST_IS_RST_ENABLED ()) &&
             !(AST_IS_PVRST_ENABLED ())))
        {
            AstReleaseContext ();
            gAstRedGlobalInfo.u4BulkUpdNextContext++;
            continue;
        }

        for (u2LocalPort = (UINT2) gAstRedGlobalInfo.u4BulkUpdNextPort;
             u2LocalPort <= (UINT2) AST_MAX_PORTS_PER_CONTEXT
             && u4BulkUpdPortCnt > 0; u2LocalPort++, u4BulkUpdPortCnt--)
        {
            /* Update u4BulkUpdNextPort, to resume the next sub bulk update
             * from where the previous sub bulk update left it out.
             */
            gAstRedGlobalInfo.u4BulkUpdNextPort++;

            /* Is Port Created ? */
            if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
                continue;

            if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE)
            {
                AstRedPbHandleBulkUptOnCep (u2LocalPort, &pBuf, &u4Offset,
                                            &u4BulkUpdPortCnt);
                if (u4BulkUpdPortCnt == 0)
                {
                    break;
                }
            }
            else
            {

                /* Oper UP ? */
                if (!AST_IS_PORT_UP (u2LocalPort))
                    continue;

                if (!(AST_IS_MST_ENABLED ()) && !(AST_IS_RST_ENABLED ()) &&
                    !(AST_IS_PVRST_ENABLED ()))
                {
                    /* This is a PEB and the provider spanning tree status
                     * is disabled. So skip all S-VLAN ports. */
                    continue;

                }

                AstRedHandleBulkUpdatePerPort (u2LocalPort, &pBuf, &u4Offset);
            }
        }                        /* End of Port Count */
        AstReleaseContext ();

        if (u4BulkUpdPortCnt == 0)
            break;
        else if (u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            gAstRedGlobalInfo.u4BulkUpdNextContext++;
            gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
            gAstRedGlobalInfo.u4BulkUpdNextPort = AST_MIN_NUM_PORTS;
        }
    }                            /*End of Context loop */

    /*Send the last Message */
    if (pBuf != NULL)
    {
        if (AstRedSendUpdateToRM ((tRmMsg *) pBuf,
                                  (UINT2) u4Offset) != RST_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AstRmHandleProtocolEvent (&ProtoEvt);
            return;
        }
    }

    if (gAstRedGlobalInfo.u4BulkUpdNextContext < AST_SIZING_CONTEXT_COUNT)
    {
        if (AST_SEND_EVENT (AST_TASK_ID, AST_RED_BULK_UPD_EVENT)
            != AST_OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AstRmHandleProtocolEvent (&ProtoEvt);
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: Sending AST_RED_BULK_UPD_EVENT to "
                            "ASTP Task FAILED!!!\n");
        }
    }
    else
    {
        /* AST completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        AstRmSetBulkUpdatesStatus (RM_RSTPMSTP_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        AstRedSendBulkUpdateTailMsg ();

    }
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedStorePortInfo                                  */
/*                                                                           */
/* Description        : Stores Port Roles/Port states per Instance           */
/*                      External Interface should use this value till Audit  */
/*                      mechanism is completed                               */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedStorePortInfo (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2InstanceId;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort = 0;
    UINT1               u1PortRole;
    UINT1               u1PortState;
#ifdef PVRST_WANTED
    tAstPortEntry      *pPortEntry;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    tVlanId             Pvid = RST_DEFAULT_INSTANCE;
    UINT4               u4IfIndex = 0;
#endif
    if (u2Len != RED_AST_PORT_INFO_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        AST_DBG (AST_RED_DBG, "AstRedStorePortInfo: Incorrect length \n");
        return RST_FAILURE;
    }

    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);

    /* Get Port Role/ Port State */
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortRole);
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortState);

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId >= VLAN_MAX_VLAN_ID
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }

    /* Is Port Created ? */
    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedStorePortInfo: "
                      "Port entry is not present for the local port %d\n",
                      u2LocalPort);
        return RST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort, u2ProtocolPort,
                                                    &u2LocalPort);

        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "PB C-VLAN selection failed for port %s \n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            return RST_FAILURE;
        }
    }

    /* Is Port Oper Up ? */
    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        return RST_SUCCESS;
    }
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
        if (pPortEntry == NULL)
        {
            return RST_FAILURE;
        }
        u4IfIndex = pPortEntry->u4IfIndex;

        AstL2IwfGetVlanPortType ((UINT2) u4IfIndex, &u1PortType);
        if (u1PortType == VLAN_ACCESS_PORT)
        {
            AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &Pvid);
            if (Pvid != u2InstanceId)
            {
                /* Pvid has been changed but it is still not updated in PVRST */
                return RST_SUCCESS;
            }
        }
        if (u2InstIndex == AST_INIT_VAL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "Vlan instance %d mapping is not present \n",
                          u2InstanceId);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "PVRST Perst info is not present for instance %d\n",
                          u2InstIndex);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "PVRST Perst port info is not present for "
                          "port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstIndex);
            return RST_FAILURE;
        }

        AST_RED_PORT_STATE (u2InstIndex, u2LocalPort) = u1PortState;
        AST_RED_PORT_ROLE (u2InstIndex, u2LocalPort) = u1PortRole;
    }
    else
#endif
    {
        if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "ASTP Perst info is not present for instance %d\n",
                          u2InstanceId);
            return RST_FAILURE;
        }
        if (AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStorePortInfo: "
                          "ASTP Perst port info is not present for "
                          "port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
            return RST_FAILURE;
        }

        AST_RED_PORT_STATE (u2InstanceId, u2LocalPort) = u1PortState;
        AST_RED_PORT_ROLE (u2InstanceId, u2LocalPort) = u1PortRole;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedApplyOperStatus                                */
/*                                                                           */
/* Description        : Apply Oper Status                                    */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedApplyOperStatus (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort = 0;
    UINT2               u2InstanceId;
    UINT1               u1MessageSubType;

    if (u2Len != RED_AST_OPER_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        AST_DBG (AST_RED_DBG, "AstRedApplyOperStatus: "
                 "Incorrect packet length \n");
        return RST_FAILURE;
    }

    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1MessageSubType);

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId >= VLAN_MAX_VLAN_ID
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedApplyOperStatus: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedApplyOperStatus: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }

    if ((u1MessageSubType == AST_EXT_PORT_UP) ||
        (u1MessageSubType == AST_STP_PORT_UP))
    {
        if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG,
                          "AstRedApplyOperStatus: No Port Entry in "
                          "port up handling for local port %d\n", u2LocalPort);
            return MST_FAILURE;
        }

        if (u2ProtocolPort != AST_INIT_VAL)
        {
            /* This is C-VLAN component message. So u2LocalPort points to the
             * CEP local port number in the parent context. */

            /* Select C-VLAN context (including Red C-VLAN context) and update
             * the u2LocalPort so that it points the local port of the port
             * whose protocol port is u2ProtocolPort. */
            i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                        u2ProtocolPort,
                                                        &u2LocalPort);
            if (i4RetVal == RST_FAILURE)
            {
                AST_DBG_ARG1 (AST_RED_DBG, "AstRedApplyOperStatus: PB C-VLAN "
                              "selection failed in port up handling for "
                              "port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
                return RST_FAILURE;
            }
        }

        if (AST_IS_PORT_UP (u2LocalPort))
        {
            return RST_SUCCESS;
        }

        if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE)
        {
            /* For CEP, handling oper status or stp port up is different. 
             * For CEP, we need to switch to the corresponding C-VLAN 
             * component, and then enable the port there. */
            i4RetVal = AstPbEnableCepPort (u2LocalPort, u2InstanceId,
                                           u1MessageSubType);
            return i4RetVal;
        }
        else
        {
            i4RetVal = AstEnablePort (u2LocalPort, u2InstanceId,
                                      u1MessageSubType);
            return i4RetVal;
        }
    }
    else if ((u1MessageSubType == AST_EXT_PORT_DOWN) ||
             (u1MessageSubType == AST_STP_PORT_DOWN))
    {
        if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
        {
            return MST_SUCCESS;
        }

        if (u2ProtocolPort != AST_INIT_VAL)
        {
            /* This is C-VLAN component message. So u2LocalPort points to the
             * CEP local port number in the parent context. */

            /* Select C-VLAN context (including Red C-VLAN context) and update
             * the u2LocalPort so that it points the local port of the port
             * whose protocol port is u2ProtocolPort. */
            i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                        u2ProtocolPort,
                                                        &u2LocalPort);
            if (i4RetVal == RST_FAILURE)
            {
                AST_DBG_ARG1 (AST_RED_DBG, "AstRedApplyOperStatus: "
                              "PB C-VLAN selection failed "
                              "in port down handling for port %s\n",
                              AST_GET_IFINDEX_STR (u2LocalPort));
                return RST_FAILURE;
            }
        }

        if (!AST_IS_PORT_UP (u2LocalPort))
        {
            return RST_SUCCESS;
        }
        /* Clear Sync up data for the particular port 
         * before Disabling the port*/
        AstRedClearSyncUpDataOnPort (u2LocalPort);

        if (AST_IS_RST_STARTED ())
        {
            /* Disable Port */
            RstDisablePort (u2LocalPort, u1MessageSubType);
        }
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_STARTED ())
        {
            i4RetVal =
                PvrstPerStDisablePort (u2LocalPort, u2InstanceId,
                                       u1MessageSubType);
            UNUSED_PARAM (i4RetVal);
        }
#endif
#ifdef MSTP_WANTED
        else if (AST_IS_MST_STARTED ())
        {
            MstDisablePort (u2LocalPort, u2InstanceId, u1MessageSubType);
        }
#endif
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearPduOnActive                               */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           : u2PortIfIndex - Port If index                        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedClearPduOnActive (UINT2 u2PortIfIndex)
{
    tAstBpdu           *pPdu = NULL;

    if (u2PortIfIndex > AST_MAX_NUM_PORTS)
    {
        AST_ASSERT ();
        return RST_FAILURE;
    }

    if (AST_RED_CURR_CONTEXT_INFO () == NULL)
    {
        return RST_FAILURE;
    }

    if (AST_RED_CONTEXT_PORT_INFO (u2PortIfIndex) == NULL)
    {
        return RST_SUCCESS;
    }

    /* Free Memory for the particular port */
    pPdu = AST_RED_RST_PDU_PTR (u2PortIfIndex);
    if (pPdu != NULL)
    {
        AST_FREE_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pPdu);
        AST_RED_RST_PDU_PTR (u2PortIfIndex) = NULL;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearAllPduOnActive                            */
/* Description        : Invalidates all the PDUs in Active Node              */
/* Input(s)           : u2PortIfIndex - Port If index                        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedClearAllPduOnActive (VOID)
{
    UINT2               u2PortNum = AST_INIT_VAL;

    for (u2PortNum = 1; u2PortNum < AST_MAX_NUM_PORTS; u2PortNum++)
    {
        AstRedClearPduOnActive (u2PortNum);
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstRedStoreAstPdu                                    */
/*                                                                           */
/* Description        : Stores the latest PDUs in Standby                    */
/* Input(s)           : u2LocalPort - Local port if of the port              */
/*                    : pData - Pointer to BPDU Data                         */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstRedStoreAstPdu (UINT2 u2LocalPort, VOID *pData, UINT2 u2Len)
{
    tAstBpdu           *pPdu = NULL;

    if (u2Len > AST_RED_DATA_MEMBLK_SIZE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Incorrect Len !\n");
        AST_ASSERT ();
        return RST_FAILURE;
    }

    pPdu = AST_RED_RST_PDU_PTR (u2LocalPort);
    if (NULL == pPdu)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pPdu, tAstBpdu);

        if (pPdu == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Message Memory Block Allocation FAILED!\n");
            return RST_FAILURE;
        }
        AST_RED_RST_PDU_PTR (u2LocalPort) = pPdu;
    }

    AST_MEMSET (pPdu, AST_INIT_VAL, AST_RED_DATA_MEMBLK_SIZE);

    /* Store the Data Buffer */
    AST_MEMCPY (pPdu, pData, u2Len);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedCallBackSuccess                                */
/*                                                                           */
/* Description        : Handles the activities in standby,when               */
/*                        sync message with callback status success          */
/*                        is received                                        */
/* Input(s)           : pData - Pointer to BPDU Data                         */
/*                      pu4Offset - pointer to offest for Data               */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedCallBackSuccess (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    UINT2               u2InstanceId;
    tAstPortEntry      *pPortEntry;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort;
    UINT1               u1PortRole;
    UINT1               u1PortState;
    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);
    /* Get Port Role/ Port State */
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortRole);
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortState);
    UNUSED_PARAM (u2Len);
    pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedCallBackSuccess: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return RST_FAILURE;
    }
    AstHwPortStateUpdtSuccess (pPortEntry, u2InstanceId, (UINT2) u1PortState);
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstRedCallBackFailure                                */
/*                                                                           */
/* Description        : Handles the activities in standby,when               */
/*                        sync message with callback status success          */
/*                        is received                                        */
/* Input(s)           : pData - Pointer to BPDU Data                         */
/*                      pu4Offset - pointer to offest for Data               */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedCallBackFailure (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    UINT2               u2InstanceId;
    tAstPortEntry      *pPortEntry;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort;
    UINT1               u1PortRole;
    UINT1               u1PortState;
    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);
    /* Get Port Role/ Port State */
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortRole);
    AST_RM_GET_1_BYTE (pData, pu4Offset, u1PortState);
    UNUSED_PARAM (u2Len);
    pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedCallBackFailure: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return RST_FAILURE;
    }
    AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId, u1PortState);
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstRedStoreAllTimes                                  */
/*                                                                           */
/* Description        : Stores all timers + the system time left to expire.  */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedStoreAllTimes (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tAstRedTimes       *pRedTimes;
    tAstTimeStamp       TimeStamp;
    UINT4               u4FdWhileExpTime;
    UINT4               u4RcvdInfoWhileExpTime;
    UINT4               u4TcWhileExpTime;
    UINT4               u4RrWhileExpTime;
    UINT4               u4RbWhileExpTime;
    UINT4               u4MdelayWhileExpTime;
    UINT4               u4EdgeDelayWhileExpTime;
    UINT4               u4RapidAgeDurtnExpTime;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2InstanceId;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort = 0;
#ifdef PVRST_WANTED
    tAstPortEntry      *pPortEntry;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    tVlanId             Pvid = RST_DEFAULT_INSTANCE;
    UINT4               u4IfIndex = 0;
#endif

    if (u2Len != RED_AST_ALL_TIMES_LEN &&
        u2Len != RED_AST_ALL_TIMES_INST_ZERO_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        AST_DBG (AST_RED_DBG, "AstRedStoreAllTimes: Incorrect "
                 "packet length\n");
        return RST_FAILURE;
    }

    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);

    AST_RM_GET_4_BYTE (pData, pu4Offset, u4FdWhileExpTime);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4RcvdInfoWhileExpTime);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4TcWhileExpTime);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4RrWhileExpTime);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4RbWhileExpTime);
    if (u2Len == RED_AST_ALL_TIMES_LEN)
    {
        AST_RM_GET_4_BYTE (pData, pu4Offset, u4MdelayWhileExpTime);
        AST_RM_GET_4_BYTE (pData, pu4Offset, u4EdgeDelayWhileExpTime);
        AST_RM_GET_4_BYTE (pData, pu4Offset, u4RapidAgeDurtnExpTime);
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId >= VLAN_MAX_VLAN_ID
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }

    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return RST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort, u2ProtocolPort,
                                                    &u2LocalPort);
        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "PB C-VLAN selection failed for "
                          "port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
            return RST_FAILURE;
        }
    }

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: Port Down for "
                      "port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
        return RST_FAILURE;
    }
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        pPortEntry = AST_GET_PORTENTRY (u2LocalPort);
        if (pPortEntry == NULL)
        {
            return RST_FAILURE;
        }
        u4IfIndex = pPortEntry->u4IfIndex;

        AstL2IwfGetVlanPortType ((UINT2) u4IfIndex, &u1PortType);
        if (u1PortType == VLAN_ACCESS_PORT)
        {
            AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &Pvid);
            if (Pvid != u2InstanceId)
            {
                /* Pvid has been changed but it is still not updated in PVRST */
                return RST_SUCCESS;
            }
        }
        if (u2InstIndex == AST_INIT_VAL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "PVRST Vlan instance mapping is not "
                          "present for instance %d\n", u2InstIndex);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "PVRST Perst info is not present for instance %d\n",
                          u2InstIndex);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "PVRST Perst port info is not present for "
                          "port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstIndex);
            return RST_FAILURE;
        }
        /* If Allocated Update the fields */
        pRedTimes = AST_RED_TIMES_PTR (u2InstIndex, u2LocalPort);
    }
    else
#endif
    {
        if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "ASTP Perst info is not present "
                          "for instance %d\n", u2InstanceId);
            return RST_FAILURE;
        }
        if (AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreAllTimes: "
                          "ASTP Perst port info is not present "
                          "for port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
            return RST_FAILURE;
        }

        /* If Allocated Update the fields */
        pRedTimes = AST_RED_TIMES_PTR (u2InstanceId, u2LocalPort);
    }
    if (NULL == pRedTimes)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_TIMES_MEMPOOL_ID, pRedTimes,
                                 tAstRedTimes);

        if (pRedTimes == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC | AST_RED_DBG,
                     "AstRedStoreAllTimes: "
                     "Message Memory Block Allocation FAILED!\n");
            return RST_FAILURE;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            AST_RED_TIMES_PTR (u2InstIndex, u2LocalPort) = pRedTimes;
        }
        else
#endif
        {
            AST_RED_TIMES_PTR (u2InstanceId, u2LocalPort) = pRedTimes;
        }
    }
    AST_MEMSET (pRedTimes, AST_INIT_VAL, sizeof (tAstRedTimes));

    pRedTimes->u4FdWhileExpTime = u4FdWhileExpTime;
    pRedTimes->u4RcvdInfoWhileExpTime = u4RcvdInfoWhileExpTime;
    pRedTimes->u4TcWhileExpTime = u4TcWhileExpTime;
    pRedTimes->u4RrWhileExpTime = u4RrWhileExpTime;
    pRedTimes->u4RbWhileExpTime = u4RbWhileExpTime;

    if (u2Len == RED_AST_ALL_TIMES_LEN)
    {
        pRedTimes->u4MdelayWhileExpTime = u4MdelayWhileExpTime;
        pRedTimes->u4EdgeDelayWhileExpTime = u4EdgeDelayWhileExpTime;
        pRedTimes->u4RapidAgeDurtnExpTime = u4RapidAgeDurtnExpTime;
    }

    AST_GET_TIME_STAMP (&TimeStamp);

    if (pRedTimes->u4FdWhileExpTime != 0)
    {
        AST_ADD_TIME_STAMP (pRedTimes->u4FdWhileExpTime, TimeStamp);
    }

    if (pRedTimes->u4RcvdInfoWhileExpTime != 0)
    {
        AST_ADD_TIME_STAMP (pRedTimes->u4RcvdInfoWhileExpTime, TimeStamp);
    }

    if (pRedTimes->u4TcWhileExpTime != 0)
    {
        AST_ADD_TIME_STAMP (pRedTimes->u4TcWhileExpTime, TimeStamp);
    }

    if (pRedTimes->u4RrWhileExpTime != 0)
    {
        AST_ADD_TIME_STAMP (pRedTimes->u4RrWhileExpTime, TimeStamp);
    }

    if (pRedTimes->u4RbWhileExpTime != 0)
    {
        AST_ADD_TIME_STAMP (pRedTimes->u4RbWhileExpTime, TimeStamp);
    }

    if (u2Len == RED_AST_ALL_TIMES_LEN)
    {
        if (pRedTimes->u4MdelayWhileExpTime != 0)
        {
            AST_ADD_TIME_STAMP (pRedTimes->u4MdelayWhileExpTime, TimeStamp);
        }
        else if (pRedTimes->u4MdelayWhileExpTime == 0)
        {
            /* Case in which Standby misses MDELAYWHILE_EXP, 
             * so Port Migration SEM remains in CHECKING_RSTP State */
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                if ((PVRST_GET_PERST_PVRST_RST_PORT_INFO
                     (u2LocalPort, u2InstIndex))->pMdWhilePvrstTmr != NULL)
                {
                    (PVRST_GET_PERST_PVRST_RST_PORT_INFO
                     (u2LocalPort, u2InstIndex))->pMdWhilePvrstTmr = NULL;
                }
            }
            else
#endif
            {
                if ((AST_GET_COMM_PORT_INFO (u2LocalPort))->pMdWhileTmr != NULL)
                {
                    RstPortMigrationMachine
                        (RST_PMIGSM_EV_MDELAYWHILE_EXP, u2LocalPort);
                }
            }
        }

        if (pRedTimes->u4EdgeDelayWhileExpTime != 0)
        {
            AST_ADD_TIME_STAMP (pRedTimes->u4EdgeDelayWhileExpTime, TimeStamp);
        }

        if (pRedTimes->u4RapidAgeDurtnExpTime != 0)
        {
            AST_ADD_TIME_STAMP (pRedTimes->u4RapidAgeDurtnExpTime, TimeStamp);
        }
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleTimers                                    */
/*                                                                           */
/* Description        : Handles the Timer Expiry and if not expired          */
/*                      saves the remaining time.                            */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedHandleTimers (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstTimer          *pAstTimer = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT4               u4ExpireTime = 0;
    UINT4               u4TempOffset = 0;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2InstanceId;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort = 0;
    UINT1               u1TimerInfo;
    UINT1               u1TimerType;
    UINT1               u1TimerFlag = 0;
#ifdef PVRST_WANTED
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    u4TempOffset = *pu4Offset;

    if (u2Len != RED_AST_TIMES_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        AST_DBG (AST_RED_DBG, "AstRedHandleTimers: Incorrect length \n");
        return RST_FAILURE;
    }

    AST_RM_GET_1_BYTE (pData, pu4Offset, u1TimerInfo);
    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4ExpireTime);

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId >= VLAN_MAX_VLAN_ID
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleTimers: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleTimers: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);
    if (pAstPortEntry == NULL)
    {
        return RST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                    u2ProtocolPort,
                                                    &u2LocalPort);
        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleTimers: "
                          "PB C-VLAN selection is failed for"
                          "port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
            return RST_FAILURE;
        }

        /* Also update the pAstPortEntry to point to the C-VLAN port whose
         * protocol port is u2ProtocolPort. */
        pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);
    }

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleTimers: "
                      "Port %s is not operationally UP\n",
                      AST_GET_IFINDEX_STR (u2LocalPort));
        return RST_FAILURE;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleTimers: "
                          "PVRST Vlan instance %d mapping is not present \n",
                          u2InstanceId);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleTimers: "
                          "PVRST Perst info is not present for instance %d\n",
                          u2InstIndex);
            return RST_FAILURE;
        }
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex);
        if (pPerStPortInfo == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleTimers: "
                          "PVRST Perst port info is not present for port %s"
                          "instance %d\n", AST_GET_IFINDEX_STR (u2LocalPort),
                          u2InstIndex);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleTimers: "
                          "ASTP Perst info is not present for instance %d\n",
                          u2InstanceId);
            return RST_FAILURE;
        }
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId);
        if (pPerStPortInfo == NULL)
        {
            if ((AST_GET_SISP_STATUS (u2LocalPort)) == RST_TRUE)
            {
                AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleTimers: "
                              "ASTP Perst port info is not present "
                              "on SISP enabled port %s instance %d, ignore it\n",
                              AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
                return RST_SUCCESS;

            }
            else
            {
                AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleTimers: "
                              "ASTP Perst port info is not present "
                              "for port %s instance %d\n",
                              AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
                return RST_FAILURE;
            }
        }
    }
    /* Timer Expiry sync Handling */
    u1TimerType = (UINT1) (u1TimerInfo & ~(AST_RED_TIMER_EXPIRED));

    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            pAstTimer = pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr;
            pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;

            if (AST_IS_RST_ENABLED ())
            {
                i4RetVal =
                    RstPortRoleTrMachine ((UINT2) RST_PROLETRSM_EV_FDWHILE_EXP,
                                          pPerStPortInfo);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                i4RetVal =
                    MstPortRoleTransitMachine (MST_PROLETRSM_EV_FDWHILE_EXPIRED,
                                               pPerStPortInfo, u2InstanceId);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                i4RetVal = 
                PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_FDWHILE_EXP,
                                        pPerStPortInfo);
            }
#endif
            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping FD While Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");
                }
            }
            u1TimerFlag = AST_TMR_TYPE_FDWHILE;
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            pAstTimer = pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr;
            pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr = NULL;

            AstRedHandleRcvdInfoWhileTmrExp (pPerStPortInfo);
            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Receive Info Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");

                }
            }
            u1TimerFlag = AST_TMR_TYPE_RCVDINFOWHILE;
            break;

        case AST_TMR_TYPE_TCWHILE:
            pAstTimer = pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr;
            pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr = NULL;

            u1TimerFlag = AST_TMR_TYPE_TCWHILE;
            break;

        case AST_TMR_TYPE_RRWHILE:
            pAstTimer = pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr;
            pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr = NULL;

            if (AST_IS_RST_ENABLED ())
            {
                i4RetVal =
                    RstPortRoleTrMachine ((UINT2) RST_PROLETRSM_EV_RRWHILE_EXP,
                                          pPerStPortInfo);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                i4RetVal = 
                PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_RRWHILE_EXP,
                                        pPerStPortInfo);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                i4RetVal =
                    MstPortRoleTransitMachine (MST_PROLETRSM_EV_RRWHILE_EXPIRED,
                                               pPerStPortInfo, u2InstanceId);
            }
#endif

            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping RR while Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");
                }
            }
            u1TimerFlag = AST_TMR_TYPE_RRWHILE;
            break;

        case AST_TMR_TYPE_RBWHILE:
            pAstTimer = pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr;
            pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr = NULL;

            if (AST_IS_RST_ENABLED ())
            {
                i4RetVal =
                    RstPortRoleTrMachine ((UINT2) RST_PROLETRSM_EV_RBWHILE_EXP,
                                          pPerStPortInfo);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                i4RetVal = 
                PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_RBWHILE_EXP,
                                        pPerStPortInfo);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                i4RetVal =
                    MstPortRoleTransitMachine (MST_PROLETRSM_EV_RBWHILE_EXPIRED,
                                               pPerStPortInfo, u2InstanceId);
            }
#endif

            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping RB while Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");

                }
            }
            u1TimerFlag = AST_TMR_TYPE_RBWHILE;
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2LocalPort,
                                                         u2InstIndex);

                pAstTimer = pPerStPvrstRstPortInfo->pMdWhilePvrstTmr;
                pPerStPvrstRstPortInfo->pMdWhilePvrstTmr = NULL;

/*                PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_MDELAYWHILE_EXP,
                                            u2LocalPort);*/
            }
            else
#endif
            {
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstTimer = pAstCommPortInfo->pMdWhileTmr;
                pAstCommPortInfo->pMdWhileTmr = NULL;

                RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_MDELAYWHILE_EXP,
                                         u2LocalPort);
            }

            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping MDelay while Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");

                }
            }
            u1TimerFlag = AST_TMR_TYPE_MDELAYWHILE;
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            pAstTimer = pAstCommPortInfo->pEdgeDelayWhileTmr;
            pAstCommPortInfo->pEdgeDelayWhileTmr = NULL;

#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                PvrstBrgDetectionMachine ((UINT2)
                                          PVRST_BRGDETSM_EV_EDGEDELAYWHILE_EXP,
                                          u2LocalPort);
            }
            else
#endif
            {
                RstBrgDetectionMachine ((UINT2)
                                        RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP,
                                        u2LocalPort);
            }
            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Edge delay Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");

                }
            }
            u1TimerFlag = AST_TMR_TYPE_EDGEDELAYWHILE;
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            pAstTimer = pAstCommPortInfo->pRapidAgeDurtnTmr;
            pAstCommPortInfo->pRapidAgeDurtnTmr = NULL;

            if (AST_FORCE_VERSION == AST_VERSION_0)
            {
                AstVlanResetShortAgeoutTime (pAstPortEntry);
            }
            if (pAstTimer != NULL)
            {
                if (pAstTimer->u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Rapid Age Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Expired Timer Memory Block Release FAILED!\n");

                }
            }
            u1TimerFlag = AST_TMR_TYPE_RAPIDAGE_DURATION;
            break;

        case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:

            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            pAstTimer = pAstCommPortInfo->pPseudoInfoHelloWhenTmr;
            pAstCommPortInfo->pPseudoInfoHelloWhenTmr = NULL;

            if (RstPseudoInfoMachine
                ((UINT2) RST_PSEUDO_INFO_EV_HELLO_EXPIRY,
                 pAstPortEntry->u2PortNo, NULL) != RST_SUCCESS)
            {
                AST_DBG (AST_PSSM_DBG | AST_PSSM_DBG |
                         AST_ALL_FAILURE_DBG,
                         "TMR: Port PSEUDOINFO SEM returned FAILURE!\n");
                i4RetVal = MST_FAILURE;
            }

            u1TimerFlag = AST_TMR_TYPE_PSEUDOINFOHELLOWHEN;

            break;

        default:
            break;
    }

    /* Stop Timer sync handling */
    u1TimerType = (UINT1) (u1TimerInfo & ~(AST_RED_TIMER_STOPPED));

    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            i4RetVal = AstStopTimer ((VOID *) pPerStPortInfo, u1TimerType);
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            i4RetVal = AstStopTimer ((VOID *) pAstPortEntry, u1TimerType);
            break;

        case AST_TMR_TYPE_RBWHILE:
            i4RetVal = AstStopTimer ((VOID *) pPerStPortInfo, u1TimerType);
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            i4RetVal = AstStopTimer ((VOID *) pPerStPortInfo, u1TimerType);
            break;

        case AST_TMR_TYPE_RRWHILE:
            i4RetVal = AstStopTimer ((VOID *) pPerStPortInfo, u1TimerType);
            break;

        case AST_TMR_TYPE_TCWHILE:
            i4RetVal = AstStopTimer ((VOID *) pPerStPortInfo, u1TimerType);
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            i4RetVal = AstStopTimer ((VOID *) pAstPortEntry, u1TimerType);
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            i4RetVal = AstStopTimer ((VOID *) pAstPortEntry, u1TimerType);
            break;

        default:
            break;

    }

    if (u1TimerFlag == 0)
    {                            /* Start timer sync Handling */
        if (u2ProtocolPort != AST_INIT_VAL)
        {
            AstPbRestoreContext ();
        }

        AstRedStoreTimes (pData, &u4TempOffset, u2Len);
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        AstPbRestoreContext ();
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedStoreTimes                                     */
/*                                                                           */
/* Description        : Stores the system time left to expire.               */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedStoreTimes (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tAstRedTimes       *pRedTimes = NULL;
    tAstTimeStamp       TimeStamp;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4ExpireTime;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2InstanceId;
    UINT2               u2LocalPort = 0;
    UINT2               u2ProtocolPort = 0;
    UINT1               u1TimerInfo;
    UINT1               u1TimerType;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    if (u2Len != RED_AST_TIMES_LEN)
    {
        AST_ASSERT ();
        *pu4Offset = *pu4Offset + u2Len;
        AST_DBG (AST_RED_DBG, "AstRedStoreTimes: Incorrect length \n");
        return RST_FAILURE;
    }

    AST_RM_GET_1_BYTE (pData, pu4Offset, u1TimerInfo);
    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);
    AST_RM_GET_4_BYTE (pData, pu4Offset, u4ExpireTime);

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId >= VLAN_MAX_VLAN_ID
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreTimes: "
                          "Instance id %d or local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPort > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreTimes: "
                          "Instance id %d local port %d invalid \n",
                          u2InstanceId, u2LocalPort);
            return RST_FAILURE;
        }
    }

    /* If u2ProtocolPort is non-zero, then the current context is a 
     * C-VLAN context. */

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                    u2ProtocolPort,
                                                    &u2LocalPort);
        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreTimes: "
                          "PB C-VLAN selection failed for local port %d\n",
                          u2LocalPort);
            return RST_FAILURE;
        }

    }

    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreTimes: "
                      "Port is not present for the local port %d\n",
                      u2LocalPort);
        return RST_FAILURE;
    }
    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreTimes: "
                      "Port %s is not operationally UP\n",
                      AST_GET_IFINDEX_STR (u2LocalPort));
        return RST_FAILURE;
    }
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedStoreTimes: "
                          "Instance index %d is invalid \n", u2InstanceId);
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreTimes: "
                          "PVRST Perst info is not present "
                          "for port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstIndex);
            return RST_FAILURE;
        }
        /* If Allocated Update the fields */
        pRedTimes = AST_RED_TIMES_PTR (u2InstIndex, u2LocalPort);
    }
    else
#endif
    {
        if (AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId) == NULL)
        {
            AST_DBG_ARG2 (AST_RED_DBG, "AstRedStoreTimes: "
                          "ASTP Perst port info is not present "
                          "for port %s instance %d\n",
                          AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
            return RST_FAILURE;
        }
        /* If Allocated Update the fields */
        pRedTimes = AST_RED_TIMES_PTR (u2InstanceId, u2LocalPort);
    }
    if (NULL == pRedTimes)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_TIMES_MEMPOOL_ID, pRedTimes,
                                 tAstRedTimes);

        if (pRedTimes == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC | AST_RED_DBG,
                     "AstRedStoreTimes: Message Memory Block Allocation FAILED!\n");
            return RST_FAILURE;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            AST_RED_TIMES_PTR (u2InstIndex, u2LocalPort) = pRedTimes;
        }
        else
#endif
        {
            AST_RED_TIMES_PTR (u2InstanceId, u2LocalPort) = pRedTimes;
        }
    }

    u1TimerType = (UINT1) (u1TimerInfo & (~(AST_RED_TIMER_STOPPED |
                                            AST_RED_TIMER_STARTED)));

    AST_GET_TIME_STAMP (&TimeStamp);

    /* In case of starting/restarting timer, it is essential
     * that the timer block memory is allocated to
     * corresponding timers. 
     *
     * Throughout the spanning-tree protocol checks are 
     * present in such a way that if the timer node is null, it
     * is considered as timer expiry.
     * For eg: If fdwhile timer block is null, immediately at a
     * particular moment in RTSM it maves that port to the
     * forwarding state. But in standby node, if we don't allocate timer 
     * block during timer start sync up, the protocol will treat it as timer 
     * expiry even if timer start sync up only has happened.
     *
     * In case of stop, we are releasing the memory, and in case of
     * start we are just storing the time stamp in the standby node.
     * Instead, the timer node has to be allocated on receving time start 
     * sync message. 
     *
     * If (u4ExpireTime == 0), then the sync up message received is for 
     * start time sync.
     *
     * Hence calling the AstStartTimer while storing the timer values for start 
     * timer sync up.
     *
     * NOTE: The above comment is applicable for all timers.
     */
    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            pRedTimes->u4FdWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4FdWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4FdWhileExpTime, TimeStamp);

                i4RetVal = AstStartTimer ((VOID *) pPerStPortInfo, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_FDWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_RCVDINFOWHILE:
            pRedTimes->u4RcvdInfoWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4RcvdInfoWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4RcvdInfoWhileExpTime,
                                    TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pPerStPortInfo, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_RCVDINFOWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_TCWHILE:
            pRedTimes->u4TcWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4TcWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4TcWhileExpTime, TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pPerStPortInfo, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_TCWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_RRWHILE:
            pRedTimes->u4RrWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4RrWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4RrWhileExpTime, TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pPerStPortInfo, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_RRWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_RBWHILE:
            pRedTimes->u4RbWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4RbWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4RbWhileExpTime, TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pPerStPortInfo, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_RBWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_MDELAYWHILE:
            pRedTimes->u4MdelayWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4MdelayWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4MdelayWhileExpTime, TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_MDELAYWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pRedTimes->u4EdgeDelayWhileExpTime = u4ExpireTime;
            if (pRedTimes->u4EdgeDelayWhileExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4EdgeDelayWhileExpTime,
                                    TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_EDGEDELAYWHILE_TMR_DEF_VAL);
            }
            break;
        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pRedTimes->u4RapidAgeDurtnExpTime = u4ExpireTime;
            if (pRedTimes->u4RapidAgeDurtnExpTime != 0)
            {
                AST_ADD_TIME_STAMP (pRedTimes->u4RapidAgeDurtnExpTime,
                                    TimeStamp);
                i4RetVal = AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                          u1TimerType,
                                          RED_AST_RAPIDAGE_DURATION_TMR_DEF_VAL);
            }
            break;

        default:
            AST_ASSERT ();
            AST_DBG (AST_RED_DBG, "AstRedStoreTimes: "
                     "Invalid Timer type handling\n");
            return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearSyncUpDataOnPort                          */
/* Description        : Clears stale Data On Port                            */
/* Input(s)           : u2InstanceId - Instance Id                           */
/*                    : u2PortIfIndex - Port If index                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearSyncUpDataOnPort (UINT2 u2PortIfIndex)
{
    tAstRedTimes       *pRedTimes = NULL;
    tAstBpdu           *pAstPdu = NULL;
#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    tAstPerStInfo      *pPerStInfo = NULL;
#endif
#ifdef MSTP_WANTED
    tAstMstRedPdu      *pMstPdu = NULL;
#endif
#ifdef PVRST_WANTED
    tAstPvrstRedPdu    *pPvrstPdu = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2MaxInstances = 0;

    if (u2PortIfIndex > AST_MAX_NUM_PORTS)
    {
        AST_ASSERT ();
        return;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2MaxInstances = AST_MAX_PVRST_INSTANCES;
    }
    else
#endif
    {
        u2MaxInstances = AST_MAX_MST_INSTANCES;
    }

    if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
    {
        u2MaxInstances = RST_MAX_TREE_INSTANCES;
    }

    /*Get All the stored data and release the memory to the MemPool */
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
             u2InstIndex++)
        {
            if ((pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex)) == NULL)
            {
                continue;
            }
            /* Red context port info. is allocated upon create port indication
             * and deallocated upon delete port indication, 
             * so check the port info. existenece before proceeding 
             * for the port related operations. */
            if (AST_RED_CONTEXT_PORT_INFO (u2PortIfIndex) != NULL)
            {
                if (AST_RED_CONTEXT_PERPORT_INST_ENTRY
                    (u2InstIndex, u2PortIfIndex) != NULL)
                {
                    AST_RED_PORT_ROLE (u2InstIndex, u2PortIfIndex) =
                        AST_INIT_VAL;

                    AST_RED_PORT_STATE (u2InstIndex, u2PortIfIndex) =
                        AST_INIT_VAL;

                    /* Release Timer Memory */
                    if (AST_NODE_STATUS () == RED_AST_IDLE)
                    {
                      if (AstRmGetNodeState () == RM_STANDBY)
                      {

                                if (NULL != (pRedTimes =
                                             (AST_RED_TIMES_PTR
                                              (u2InstIndex, u2PortIfIndex))))
                                {
                                    if (AST_FREE_RED_MEM_BLOCK
                                        (AST_RED_TIMES_MEMPOOL_ID,
                                         pRedTimes) == AST_MEM_FAILURE)
                                    {
                                        AST_TRC (AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                                                 AST_ALL_FAILURE_TRC,
                                                 "SYS: Memory Pool Allocation failed !!!\n");
                                        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG |
                                                 AST_ALL_FAILURE_DBG,
                                                 "SYS: Memory Pool Allocation failed !!!\n");

                                    }
                                    AST_RED_TIMES_PTR (u2InstIndex, u2PortIfIndex) = NULL;
                                }
                       }
                     }
        }
    }
  }
}
    else
#endif
    {
#ifdef MSTP_WANTED
        for (u2InstanceId = 0; u2InstanceId < u2MaxInstances; u2InstanceId++)
        {
            if ((pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)) == NULL)
            {
                continue;
            }
#endif
            /* Red context port info. is allocated upon create port indication
             * and deallocated upon delete port indication, 
             * so check the port info. existenece before proceeding 
             * for the port related operations. */

            if (AST_RED_CONTEXT_PORT_INFO (u2PortIfIndex) != NULL)
            {
                if (AST_RED_CONTEXT_PERPORT_INST_ENTRY (u2InstanceId,
                                                        u2PortIfIndex) != NULL)
                {

                    AST_RED_PORT_ROLE (u2InstanceId, u2PortIfIndex) =
                        AST_INIT_VAL;

                    AST_RED_PORT_STATE (u2InstanceId, u2PortIfIndex) =
                        AST_INIT_VAL;

                    /* Release Timer Memory */
                    if (NULL != (pRedTimes =
                                 (AST_RED_TIMES_PTR
                                  (u2InstanceId, u2PortIfIndex))))
                    {
                        if (AST_FREE_RED_MEM_BLOCK
                            (AST_RED_TIMES_MEMPOOL_ID,
                             pRedTimes) == AST_MEM_FAILURE)
                        {
                            AST_TRC (AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                                     AST_ALL_FAILURE_TRC,
                                     "SYS: Memory Pool Allocation failed !!!\n");
                            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG |
                                     AST_ALL_FAILURE_DBG,
                                     "SYS: Memory Pool Allocation failed !!!\n");

                        }
                        AST_RED_TIMES_PTR (u2InstanceId, u2PortIfIndex) = NULL;
                    }
                }
            }
#ifdef MSTP_WANTED
        }
#endif
    }

    /* Red context port info. is allocated upon create port indication
     * and deallocated upon delete port indication, 
     * so check the port info. existenece before proceeding 
     * for the port related operations. */

    /* Free Pdu Memory for the particular port 
     * While SwitchOver,H/w Audit should not clear synced up Pdu in database*/
    if ((AST_NODE_STATUS () == RED_AST_STANDBY) &&
        (AST_RED_CONTEXT_PORT_INFO (u2PortIfIndex) != NULL))
    {
        if (AST_IS_RST_ENABLED ())
        {
            pAstPdu = AST_RED_RST_PDU_PTR (u2PortIfIndex);
            if (pAstPdu != NULL)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pAstPdu);
                AST_RED_RST_PDU_PTR (u2PortIfIndex) = NULL;
            }
        }
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            pPvrstPdu = AST_RED_PVRST_PDU_PTR (u2PortIfIndex);
            if (pPvrstPdu != NULL)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pPvrstPdu);
                AST_RED_PVRST_PDU_PTR (u2PortIfIndex) = NULL;
            }
        }
#endif
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            pMstPdu = AST_RED_MST_PDU_PTR (u2PortIfIndex);
            if (pMstPdu != NULL)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pMstPdu);
                AST_RED_MST_PDU_PTR (u2PortIfIndex) = NULL;
            }
        }
#endif
    }
}

/*****************************************************************************/
/* Function Name      : AstRedClearAllSyncUpData                             */
/* Description        : Clears stale Data On all Ports                       */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearAllSyncUpData (VOID)
{
    UINT2               u2LocalPort;
    tAstPortEntry      *pAstPortEntry = NULL;

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);

        if (pAstPortEntry == NULL)
        {
            continue;
        }

        AstRedClearSyncUpDataOnPort (u2LocalPort);
    }
}

/*****************************************************************************/
/* Function Name      : AstRedClearAllContextSyncUpData                      */
/* Description        : Clears stale Data On all Ports                       */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearAllContextSyncUpData (VOID)
{
    UINT4               u4ContextId;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        if (AST_BRIDGE_MODE () == AST_PROVIDER_EDGE_BRIDGE_MODE)
        {
            if (AST_IS_ENABLED_IN_CTXT () == RST_FALSE)
            {
                /* Global spanning tree status for this context is disabled. */
                AstReleaseContext ();
                continue;
            }

            AstRedClearAllCvlanSyncUpData ();
        }

        if (!(AST_IS_MST_ENABLED ()) && !(AST_IS_RST_ENABLED ()) &&
            !(AST_IS_PVRST_ENABLED ()))
        {
            AstReleaseContext ();
            continue;
        }

        AstRedClearAllSyncUpData ();

        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHandleSyncUpMessage                            */
/*                                                                           */
/* Description        : This function is invoked whenever AST module         */
/*                      receives a sync up request Bulk as well as update    */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleSyncUpMessage (tAstRmCtrlMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;
    VOID               *pData = NULL;
    UINT4               u4Offset = AST_INIT_VAL;
    UINT4               u4ContextId;
    INT4                i4RetVal = RST_SUCCESS;
    INT4                i4Return = RST_SUCCESS;
    UINT2               u2Len;
    UINT2               u2InstanceId;
    UINT2               u2LocalPort;
    UINT2               u2ProtocolPort = 0;
    UINT1               u1MessageType = 0;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    pData = (VOID *) pMsg->pFrame;

    while (1)
    {
        if (u4Offset >= pMsg->u2Length)
            break;

        AST_RM_GET_1_BYTE ((tRmMsg *) pData, &u4Offset, u1MessageType);
        /* Length of the Individual Message */
        AST_RM_GET_2_BYTE ((tRmMsg *) pData, &u4Offset, u2Len);

        if (u1MessageType != RED_AST_BULK_UPD_TAIL_MSG &&
            u1MessageType != RED_AST_BULK_REQ &&
            u1MessageType != AST_HR_STDY_ST_PKT_REQ)
        {
            AST_RM_GET_4_BYTE ((tRmMsg *) pData, &u4Offset, u4ContextId);

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_RED_DBG,
                                "AstRedHandleSyncUpMessage: "
                                "Invalid Context Id\n");
                u4Offset = u4Offset + u2Len - 7;
                continue;
            }
            if (!(AstIsRstStartedInContext (u4ContextId) ||
                  AstIsMstStartedInContext (u4ContextId) ||
                  AstIsPvrstStartedInContext (u4ContextId)))
            {
                AstReleaseContext ();
                u4Offset = u4Offset + u2Len - 7;
                continue;
            }
        }

        /* After Redundancy Forceswitchover, messages in Queue should 
         * be discarded*/
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            if ((u1MessageType != RED_AST_BULK_REQ) &&
                (u1MessageType != AST_HR_STDY_ST_PKT_REQ))
            {
                AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_RED_DBG,
                                "AstRedHandleSyncUpMessage: "
                                "Message is not bulk request\n");
                break;
            }
        }

        /* While Loop to get all messages and store them */
        /* Decode Message */
        switch (u1MessageType)
        {
            case RED_AST_PORT_INFO:
                /* Store Port Roles/States for Software Audit */
                i4Return = AstRedStorePortInfo (pData, &u4Offset, u2Len);
                break;

            case RED_AST_TIMES:
                AstRedHandleTimers (pData, &u4Offset, u2Len);
                break;

            case RED_AST_PDU:
                /* Apply PDUs immediately 
                 * without storing in sync up database */
                i4Return = AstRedHandleAstPdus (pData, &u4Offset, u2Len);
                break;
#ifdef MSTP_WANTED
            case RED_MST_PDU:
                i4Return = MstRedHandleMstPdus (pData, &u4Offset, u2Len);
                break;

#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            case RED_PVRST_PDU:
                i4Return = PvrstRedHandlePvrstPdus (pData, &u4Offset, u2Len);
                break;

#endif /* PVRST_WANTED */
            case RED_AST_OPER_STATUS:
                i4Return = AstRedApplyOperStatus (pData, &u4Offset, u2Len);
                break;
            case RED_AST_CALLBACK_SUCCESS:
                i4Return = AstRedCallBackSuccess (pData, &u4Offset, u2Len);
                break;
            case RED_AST_CALLBACK_FAILURE:
                i4Return = AstRedCallBackFailure (pData, &u4Offset, u2Len);
                i4Return = AstRedStoreAllTimes (pData, &u4Offset, u2Len);
                break;

            case RED_AST_ALL_TIMES:
                i4Return = AstRedStoreAllTimes (pData, &u4Offset, u2Len);
                break;

            case RED_AST_CLEAR_SYNCUP_DATA_ON_PORT:
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2InstanceId);
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2LocalPort);
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2ProtocolPort);

                if (u2ProtocolPort != AST_INIT_VAL)
                {
                    /* It is a C-VLAN component red message. So
                     * select C-VLAN component and then handle it. */

                    i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                                u2ProtocolPort,
                                                                &u2LocalPort);

                    if (i4RetVal == RST_FAILURE)
                    {
                        return;
                    }
                    AstRedClearSyncUpDataOnPort (u2LocalPort);

                    AstPbRestoreContext ();
                }
                else
                {
                    AstRedClearSyncUpDataOnPort (u2LocalPort);
                }
                break;

#ifdef PVRST_WANTED
            case RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT:
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2LocalPort);
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2ProtocolPort);

                if (u2ProtocolPort != AST_INIT_VAL)
                {
                    /* It is a C-VLAN component red message. So
                     * select C-VLAN component and then handle it. */
                    i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                                u2ProtocolPort,
                                                                &u2LocalPort);

                    if (i4RetVal == RST_FAILURE)
                    {
                        return;
                    }
                    AstRedClearSyncUpDataOnPort (u2LocalPort);

                    AstPbRestoreContext ();
                }
                else
                {
                    AstRedClearSyncUpDataOnPort (u2LocalPort);
                }
                break;
#endif
#ifdef MSTP_WANTED
            case RED_MST_CLEAR_SYNCUP_DATA_ON_PORT:
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2LocalPort);
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2ProtocolPort);

                if (u2ProtocolPort != AST_INIT_VAL)
                {
                    /* It is a C-VLAN component red message. So
                     * select C-VLAN component and then handle it. */
                    i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                                u2ProtocolPort,
                                                                &u2LocalPort);

                    if (i4RetVal == RST_FAILURE)
                    {
                        return;
                    }
                    AstRedClearSyncUpDataOnPort (u2LocalPort);

                    AstPbRestoreContext ();
                }
                else
                {
                    AstRedClearSyncUpDataOnPort (u2LocalPort);
                }
                break;
#endif

            case RED_AST_CLEAR_ALL_SYNCUP_DATA:
                AST_RM_GET_2_BYTE (pData, &u4Offset, u2LocalPort);
                AstRedClearAllSyncUpData ();
                break;

            case RED_AST_BULK_UPD_TAIL_MSG:
                /* On receiving BULK_UPD_TAIL_MSG, give indication to RM
                 * RM gives the trigger to higher 
                 * layers (VLAN) to send the Bulk Req msg.
                 */
                AstRedUpdateAllPortStates ();
                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                ProtoEvt.u4Error = RM_NONE;
                AstRmHandleProtocolEvent (&ProtoEvt);
                break;
            case RED_AST_BULK_REQ:
                if (AST_IS_STANDBY_UP () == AST_FALSE)
                {
                    /* This is a special case, where bulk request msg from
                     * standby is coming before RM_STANDBY_UP. So no need to
                     * process the bulk request now. Bulk updates will be send
                     * on RM_STANDBY_UP event.
                     */
                    AST_BULK_REQ_RECD () = AST_TRUE;
                    break;
                }

                AST_BULK_REQ_RECD () = AST_FALSE;
                /* On recieving RED_AST_BULK_REQ, Bulk updation process 
                 * should be restarted.
                 */
                gAstRedGlobalInfo.u4BulkUpdNextContext = 0;
                gAstRedGlobalInfo.u4BulkUpdNextPort = AST_MIN_NUM_PORTS;
                gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
                AstRedHandleBulkUpdateEvent ();
                break;

            case RED_AST_HR_STDY_ST_PKT_REQ:
                AstRedHRProcStdyStPktReq ();
                break;

            default:
                AST_ASSERT ();
                /* Invalid Message */
                break;
        }
        if (i4Return == RST_FAILURE)
        {
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            AstRmHandleProtocolEvent (&ProtoEvt);
        }
        if (u1MessageType != RED_AST_BULK_UPD_TAIL_MSG &&
            u1MessageType != RED_AST_BULK_REQ &&
            u1MessageType != AST_HR_STDY_ST_PKT_REQ)

        {
            AstReleaseContext ();
        }
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHandleGoActive                                 */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                       has been given a GO_ACTIVE trigger                  */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Status .                                  */
/* Global Variables                                                          */
/* Modified           : AST Module Status .                                  */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleGoActive (VOID)
{
    if (AST_NODE_STATUS () == RED_AST_IDLE)
    {
        AstRedMakeNodeActiveFromIdle ();
        AstRedAllocPduMemPools ();
    }
    else if (AST_NODE_STATUS () == RED_AST_STANDBY)
    {
        AstRedMakeNodeActiveFromStandby ();
    }
    else
    {
        /* Ignore the GO_ACTIVE trigger */
    }

    AST_NUM_STANDBY_NODES () = AstRmGetStandbyNodeCount ();

    if (AST_BULK_REQ_RECD () == AST_TRUE)
    {
        AST_BULK_REQ_RECD () = AST_FALSE;
        gAstRedGlobalInfo.u4BulkUpdNextContext = 0;
        gAstRedGlobalInfo.u4BulkUpdNextPort = AST_MIN_NUM_PORTS;
        gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
        AstRedHandleBulkUpdateEvent ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHandleGoStandby                                */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                      has got a GO_STANDBY indication                      */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Status.                                   */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (AST_NODE_STATUS () == RED_AST_IDLE)
    {
        /* Discard this event. Node will become standby
         * on RM_CONFIG_RESTORE_COMPLETE */
    }
    else if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        /* Can process this event to become standby
         * from Active */
        if (AST_RED_HW_AUDIT_TASKID != AST_INIT_VAL)
        {
            AST_DELETE_TASK (AST_RED_HW_AUDIT_TASKID);
            AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
        }
        AstRedMakeNodeStandbyFromActive ();

        /* Allocate for storage Types */
        AstRedAllocStoreMemPools ();

        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        AstRmHandleProtocolEvent (&ProtoEvt);

    }
    else
    {
        /* Already in STANDBY state .
           Ignore the GO_STANDBY trigger */
    }

}

/*****************************************************************************/
/* Function Name      : AstRedHandleRestoreComplete                          */
/*                                                                           */
/* Description        : This function is invoked whenever the AST module is  */
/*                      has got a CONFIG_RESTORE_COMPLETE indication         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : AST Module Status.                                   */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleRestoreComplete (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    if (AST_NODE_STATUS () == RED_AST_IDLE)
    {
        if (AstRmGetNodeState () == RM_STANDBY)
        {
            /* Recieved GO_STANDBY event earlier */
            AstRedMakeNodeStandbyFromIdle ();

            /* Store Bpdus in Standby */
            AstRedAllocPduMemPools ();

            /* Allocate for storage Types */
            AstRedAllocStoreMemPools ();

        }

        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        AstRmHandleProtocolEvent (&ProtoEvt);
    }
}

/*****************************************************************************/
/* Function Name      : AstProcessRmEvent                                    */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstProcessRmEvent (tAstRmCtrlMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tAstRedStates       AstPrevNodeState = RED_AST_IDLE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->u1Event)
    {
        case GO_ACTIVE:

            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                break;
            }
            AstPrevNodeState = AST_NODE_STATUS ();
            AstRedHandleGoActive ();

            if (AstPrevNodeState == RED_AST_IDLE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            AstRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:
            if (AST_NODE_STATUS () == RED_AST_STANDBY)
            {
                break;
            }
            AstRedHandleGoStandby ();
            AST_BULK_REQ_RECD () = AST_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* Initiate Hardware Audit */
            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                if (AST_RM_IS_HW_AUDIT_REQ () == AST_TRUE)
                {
                    AST_RM_IS_HW_AUDIT_REQ () = AST_FALSE;
                    if (AstRedHwAuditTask () != RST_SUCCESS)
                    {
                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_RED_DBG,
                                        "RED: Audit Task Failure\n");
                    }
                }
            }
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pMsg->pFrame;
            AST_NUM_STANDBY_NODES () = pData->u1NumStandby;
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (AST_BULK_REQ_RECD () == AST_TRUE)
            {
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED)
                AST_BULK_REQ_RECD () = AST_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gAstRedGlobalInfo.u4BulkUpdNextContext = 0;
                gAstRedGlobalInfo.u4BulkUpdNextPort = AST_MIN_NUM_PORTS;
                gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
                AstRedHandleBulkUpdateEvent ();
#endif
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsg->pFrame;
            AST_NUM_STANDBY_NODES () = pData->u1NumStandby;
            AstRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->pFrame, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->pFrame, pMsg->u2Length);

            ProtoAck.u4AppId = RM_RSTPMSTP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (AST_NODE_STATUS () != RED_AST_IDLE)
            {
                AstRedHandleSyncUpMessage (pMsg);
            }
            /* Processing of message is over, hence free the RM message. */
            RM_FREE (pMsg->pFrame);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            AstRedHandleRestoreComplete ();
            break;

        case L2_INITIATE_BULK_UPDATES:
            AstRedSendBulkRequest ();
            break;

        case RM_COMPLETE_SYNC_UP:
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED)
            AstRedTriggerHigherLayer (L2_COMPLETE_SYNC_UP);
#else
            /* Ignoring this Event */
            AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_ALL_FAILURE_TRC,
                            "Ignoring RM_COMPLETE_SYNC_UP if lower layers are"
                            "present \n");
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            AstRedTriggerHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : AstRedSendUpdateToRM                                 */
/*                                                                           */
/* Description        : This function sends Update to RM                     */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                      u2Len - Length of RM message                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedSendUpdateToRM (tRmMsg * pMsg, UINT2 u2Len)
{

    /* Call the API provided by RM to send the data to RM */
    if (AstRmEnqMsgToRmFromAppl
        (pMsg, u2Len, RM_RSTPMSTP_APP_ID, RM_RSTPMSTP_APP_ID) == RM_FAILURE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_ALL_FAILURE_TRC,
                        "Enq to RM from appl failed\n");
        /* pMsg is reed only in failure case. RM will free the buf
         * in success case. */

        RM_FREE (pMsg);
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedChangeGetNextMessage                           */
/*                                                                           */
/* Description        : This Functions Identifies the  next Changed          */
/*                      Message to be sent                                   */
/*                                                                           */
/* Input(s)           : u1MessageType - Current Message                      */
/*                      pu1NextMessageType - Points to the next Message      */
/*                      pu2Len             -Points to the Length             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedChangeGetNextMessage (UINT1 u1MessageType,
                            UINT1 *pu1MessageType, UINT2 *pu2Len)
{
    if (u1MessageType == 0)
    {
        *pu1MessageType = RED_AST_PORT_INFO;
        *pu2Len = RED_AST_PORT_INFO_LEN;
    }
    else if (u1MessageType == RED_AST_PORT_INFO)
    {
        *pu1MessageType = RED_AST_ALL_TIMES;
        *pu2Len = RED_AST_ALL_TIMES_LEN;
    }
    else
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedGetNextMessage                                 */
/*                                                                           */
/* Description        : This Functions Identifies the  next Message          */
/*                      to be sent                                           */
/*                                                                           */
/* Input(s)           : u1MessageType - Current Message                      */
/*                      pu1MessageType - Points to the next Message      */
/*                      pu2Len             -Points to the Length             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedGetNextMessage (UINT1 u1MessageType, UINT1 *pu1MessageType, UINT2 *pu2Len)
{

    switch (u1MessageType)
    {
        case RED_AST_GET_FIRST:
            *pu1MessageType = RED_AST_OPER_STATUS;
            *pu2Len = RED_AST_OPER_LEN;
            break;

        case RED_AST_OPER_STATUS:
            *pu1MessageType = RED_AST_PORT_INFO;
            *pu2Len = RED_AST_PORT_INFO_LEN;
            break;

        case RED_AST_PORT_INFO:
            *pu1MessageType = RED_AST_ALL_TIMES;
            *pu2Len = RED_AST_ALL_TIMES_LEN;
            break;

        case RED_AST_ALL_TIMES:
            if (AST_IS_RST_ENABLED ())
            {
                *pu1MessageType = RED_AST_PDU;
                *pu2Len = RED_AST_PDU_LEN;
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                *pu1MessageType = RED_MST_PDU;
                *pu2Len = RED_AST_MST_BPDU_LEN;
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                *pu1MessageType = RED_PVRST_PDU;
                *pu2Len = RED_AST_PVRST_BPDU_LEN;
            }
#endif
            break;

        case RED_AST_PDU:
#ifdef PVRST_WANTED
        case RED_PVRST_PDU:
#endif
        case RED_MST_PDU:
            *pu1MessageType = 0;
            *pu2Len = 0;
            return RST_FAILURE;

        default:
            *pu1MessageType = 0;
            *pu2Len = 0;
            return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSendUpdate                                     */
/*                                                                           */
/* Description        : This function is invoked to  Update RM Buffers  to   */
/*                      send the Update packet via the RM                    */
/*                      from RM module Based on the changed flags all the    */
/*                      data is sent or only the changed data is sent        */
/*                      Here Getnext is used to find the next Messages that  */
/*                      has to be appended in the Buffer.The getNext         */
/*                      messages will return the message type and Length of  */
/*                      messages for which allocation is to be done          */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tAstGlobalInfo.                                      */
/*                      tAstRedGlobalInfo                                    */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedSendUpdate (VOID)
{
    VOID               *pBuf = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT4               u4Offset = AST_INIT_VAL;
    UINT2               u2Len = AST_INIT_VAL;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum;
    UINT2               u2MaxInstances = 0;
    UINT1               u1MessageType = AST_INIT_VAL;
    UINT1               u1NextMessage = AST_INIT_VAL;
    UINT1               u1MessageSubType = AST_INIT_VAL;
#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    tAstPerStInfo      *pPerStInfo = NULL;
#endif

    /* Not Required to send Any Updates */
    if (!(AST_IS_MST_ENABLED ()) && !(AST_IS_RST_ENABLED ()) &&
        !(AST_IS_PVRST_ENABLED ()))
        return RST_SUCCESS;

    if (AST_NUM_STANDBY_NODES () == 0)
    {
        AST_TRC (AST_CONTROL_PATH_TRC, "None of the Peer Nodes  are up \
                 No Need to send Dynamic Updates\n");
        return RST_SUCCESS;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2MaxInstances = AST_MAX_PVRST_INSTANCES;
    }
    else
#endif
    {
        u2MaxInstances = AST_MAX_MST_INSTANCES;
    }

    /* In C-VLAN component, only one instance will be present.
     * Access to another instance (pPerStInfo) may crash. 
     * Also this approach saves time when RST is running. */
    if (AST_IS_RST_STARTED ())
    {
        u2MaxInstances = RST_MAX_TREE_INSTANCES;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        for (u2InstanceId = 1; u2InstanceId <= AST_MAX_PVRST_INSTANCES;
             u2InstanceId++)
        {
            if ((pPerStInfo = PVRST_GET_PERST_INFO (u2InstanceId)) == NULL)
            {
                continue;
            }
            for (u2PortNum = 1; u2PortNum <= AST_MAX_PORTS_PER_CONTEXT;
                 u2PortNum++)
            {
                /* Is Port Created ? */
                if (AST_GET_PORTENTRY (u2PortNum) == NULL)
                    continue;

                /* Oper UP ? */
                if (!AST_IS_PORT_UP (u2PortNum))
                    continue;

                pPerStPortInfo =
                    PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

                if (pPerStPortInfo != NULL)
                {
                    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                    /* STP enabled for this Instance */
                    if (pRstPortInfo->bPortEnabled == TRUE)
                    {
                        /* All Messages Loop Should come here */
                        while (1)
                        {
                            /* Check if Port state/Port Roles for this port is changed 
                             * If so send all Updates on this port */
                            if (PVRST_FALSE ==
                                PVRST_GET_CHANGED_FLAG (u2InstanceId,
                                                        u2PortNum))
                                break;

                            if (RST_FAILURE ==
                                AstRedChangeGetNextMessage (u1MessageType,
                                                            &u1NextMessage,
                                                            &u2Len))
                            {
                                /* All Messages are sent on this Port,
                                 * Now we can Move on to the Next Port/Instance */
                                u1MessageType = 0;
                                break;
                            }

                            u1MessageType = u1NextMessage;
                            /* Send all messages for this Port */
                            /* Cant encode */
                            if ((AST_MAX_RM_BUF_SIZE - u4Offset) < u2Len)
                            {
                                if (pBuf != NULL)
                                {
                                    /* Send and Allocate */
                                    AstRedSendUpdateToRM ((tRmMsg *) pBuf,
                                                          (UINT2) u4Offset);
                                }
                                u4Offset = 0;
                                /* Buffer Not available */
                                if ((pBuf =
                                     (VOID *)
                                     RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) ==
                                    NULL)
                                {
                                    AST_TRC (AST_ALL_FAILURE_TRC,
                                             "RM Allocation Failed\n");
                                    return RST_FAILURE;
                                }
                            }
                            else
                            {
                                if (pBuf == NULL)
                                {
                                    u4Offset = 0;
                                    /* Buffer Not available */
                                    if ((pBuf =
                                         (VOID *)
                                         RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE))
                                        == NULL)
                                    {
                                        AST_TRC (AST_ALL_FAILURE_TRC,
                                                 "RM Allocation Failed\n");
                                        return RST_FAILURE;
                                    }

                                }
                            }
                            if (AstRedFormMessage
                                (u2InstanceId, u2PortNum, u1MessageType,
                                 u1MessageSubType, pBuf,
                                 &u4Offset) != RST_SUCCESS)
                            {
                                RM_FREE (pBuf);
                                return RST_FAILURE;
                            }
                        }
                        /* Reset the changed Flag */
                        PVRST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                            PVRST_FALSE;
                    }            /* STP not enabled for this Instance/Port */
                }                /* if Port is not Initialised */

            }                    /* End of Port Count */
        }
    }
    else
#endif
    {
#ifdef MSTP_WANTED
        for (u2InstanceId = 0; u2InstanceId < u2MaxInstances; u2InstanceId++)
        {
            if ((pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)) == NULL)
            {
                continue;
            }
#endif
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {

                /* Is Port Created ? */
                if (AST_GET_PORTENTRY (u2PortNum) == NULL)
                    continue;

                /* Oper UP ? */
                if (!AST_IS_PORT_UP (u2PortNum))
                    continue;

                pPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

                if (pPerStPortInfo != NULL)
                {
                    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                    /* STP enabled for this Instance */
                    if (pRstPortInfo->bPortEnabled == TRUE)
                    {
                        /* All Messages Loop Should come here */
                        while (1)
                        {
                            /* Check if Port state/Port Roles for this port is changed 
                             * If so send all Updates on this port */
                            if (RST_FALSE ==
                                AST_GET_CHANGED_FLAG (u2InstanceId, u2PortNum))
                                break;

                            if (RST_FAILURE ==
                                AstRedChangeGetNextMessage (u1MessageType,
                                                            &u1NextMessage,
                                                            &u2Len))
                            {
                                /* All Messages are sent on this Port,
                                 * Now we can Move on to the Next Port/Instance */
                                u1MessageType = 0;
                                break;
                            }

                            u1MessageType = u1NextMessage;
                            /* Send all messages for this Port */
                            /* Cant encode */
                            if ((AST_MAX_RM_BUF_SIZE - u4Offset) < u2Len)
                            {
                                if (pBuf != NULL)
                                {
                                    /* Send and Allocate */
                                    AstRedSendUpdateToRM ((tRmMsg *) pBuf,
                                                          (UINT2) u4Offset);
                                }
                                u4Offset = 0;
                                /* Buffer Not available */
                                if ((pBuf =
                                     (VOID *)
                                     RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) ==
                                    NULL)
                                {
                                    AST_TRC (AST_ALL_FAILURE_TRC,
                                             "RM Allocation Failed\n");
                                    return RST_FAILURE;
                                }
                            }
                            else
                            {
                                if (pBuf == NULL)
                                {
                                    u4Offset = 0;
                                    /* Buffer Not available */
                                    if ((pBuf =
                                         (VOID *)
                                         RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE))
                                        == NULL)
                                    {
                                        AST_TRC (AST_ALL_FAILURE_TRC,
                                                 "RM Allocation Failed\n");
                                        return RST_FAILURE;
                                    }

                                }
                            }
                            if (AstRedFormMessage
                                (u2InstanceId, u2PortNum, u1MessageType,
                                 u1MessageSubType, pBuf,
                                 &u4Offset) != RST_SUCCESS)
                            {
                                RM_FREE (pBuf);
                                return RST_FAILURE;
                            }
                        }
                        /* Reset the changed Flag */
                        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                            RST_FALSE;
                    }            /* STP not enabled for this Instance/Port */
                }                /* if Port is not Initialised */

            }                    /* End of Port Count */

#ifdef MSTP_WANTED
        }                        /* End of Instance Count */
#endif
    }
    /*Send the last Message */
    if (pBuf != NULL)
    {
        if (AstRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Offset)
            != RST_SUCCESS)
        {
            return RST_FAILURE;
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedGetExpireTime                                 */
/*                                                                           */
/* Description        : This function is invoked to Get Remaining Time for   */
/*                       the timers                                          */
/*                                                                           */
/* Input(s)           : u1TimerInfo - Info on which timer , and if started   */
/*                                    stopped or expired                     */
/*                    : u2InstanceId - Instance Id                           */
/*                    : u2PortIfIndex - Port If index                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo,tAstGlobalInfo.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Time left for the timer to fire                      */
/*****************************************************************************/
UINT4
AstRedGetExpireTime (UINT1 u1TimerInfo, UINT2 u2InstanceId, UINT2 u2PortIfIndex)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstTimer          *pTmr = NULL;
    tAstAppTimer       *pAppTmr = NULL;
    UINT4               u4TimeLeft = AST_INIT_VAL;
    UINT1               u1TimerType;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
#endif

    u1TimerType = (UINT1) (u1TimerInfo & (~(AST_RED_TIMER_EXPIRED |
                                            AST_RED_TIMER_STARTED |
                                            AST_RED_TIMER_STOPPED)));

#ifdef  PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            return u4TimeLeft;
        }
        if (u1TimerType != AST_TMR_TYPE_EDGEDELAYWHILE)
        {
            pPerStRstPortInfo =
                PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex);
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortIfIndex,
                                                     u2InstIndex);
        }
    }
    else
#endif
    {
        pPerStRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstanceId);
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);

    if ((NULL == pPerStRstPortInfo) || (pCommPortInfo == NULL))
    {
        return u4TimeLeft;
    }

    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            pTmr = pPerStRstPortInfo->pFdWhileTmr;
            break;

        case AST_TMR_TYPE_RBWHILE:
            pTmr = pPerStRstPortInfo->pRbWhileTmr;
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            pTmr = pPerStRstPortInfo->pRcvdInfoTmr;
            break;

        case AST_TMR_TYPE_RRWHILE:
            pTmr = pPerStRstPortInfo->pRrWhileTmr;
            break;

        case AST_TMR_TYPE_TCWHILE:
            pTmr = pPerStRstPortInfo->pTcWhileTmr;
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
#ifdef  PVRST_WANTED
            if (AST_IS_PVRST_ENABLED () && (pPerStPvrstRstPortInfo != NULL))
            {
                pTmr = pPerStPvrstRstPortInfo->pMdWhilePvrstTmr;
            }
            else
#endif
            {
                pTmr = pCommPortInfo->pMdWhileTmr;
            }
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pTmr = pCommPortInfo->pEdgeDelayWhileTmr;
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pTmr = pCommPortInfo->pRapidAgeDurtnTmr;
            break;

        default:
            break;

    }
    if (pTmr == NULL)
    {
        return u4TimeLeft;
    }

    pAppTmr = &(pTmr->AstAppTimer);
    if (pAppTmr == NULL)
    {
        return u4TimeLeft;
    }
    if (AST_GET_REMAINING_TIME (AST_TMR_LIST_ID, pAppTmr, &u4TimeLeft) !=
        OSIX_SUCCESS)
    {
        u4TimeLeft = 0;
    }
    return u4TimeLeft;
}

/*****************************************************************************/
/* Function Name      : AstRedFormMessage                                    */
/*                                                                           */
/* Description        : This function is invoked to  Update RM Buffers  to   */
/*                      send the Update packet via the RM                    */
/*                      from RM module:-                                     */
/*                                                                           */
/* Input(s)           : Instance Id - Instance Information                   */
/*                      u2LocalPortNum - LocalPort number.                   */
/*                      MessageType - Type of Messages                       */
/*                      MessageSubTypes -  Pointer to the Input Buffer       */
/*                                         Valid only for Timer Updates      */
/*                      pOutput-      Pointer to the  O/p Buffer.            */
/*                      pu4Offset -   Pointer to the O/P Bufs valid Offset   */
/*                                                                           */
/* Output(s)          : pRmMsg ,pu4Offset                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedFormMessage (UINT2 u2InstanceId, UINT2 u2LocalPortNum,
                   UINT1 u1MessageType, UINT1 u1MessageSubType,
                   tRmMsg * pOutput, UINT4 *pu4Offset)
{
    tAstBpdu           *pPdu = NULL;
    tAstPortEntry      *pPortEntry = NULL;
#ifdef MSTP_WANTED
    tAstMstRedPdu      *pMstPdu = NULL;
#endif /* MSTP_WANTED */
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#ifdef PVRST_WANTED
    tAstPvrstRedPdu    *pPvrstPdu = NULL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    tVlanId             Pvid = RST_DEFAULT_INSTANCE;
    UINT4               u4IfIndex = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2Len;
    UINT2               u2ProtocolPort = 0;
    UINT1               u1PortState = 0;

    /* Input Checks */
    if (pOutput == NULL)
    {
        return RST_FAILURE;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        if (u2InstanceId > VLAN_MAX_VLAN_ID
            || u2LocalPortNum > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            return RST_FAILURE;
        }
        u2InstIndex = L2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                 u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (u2InstanceId > AST_MAX_MST_INSTANCES
            || u2LocalPortNum > AST_MAX_PORTS_PER_CONTEXT)
        {
            AST_ASSERT ();
            return RST_FAILURE;
        }
    }

    pPortEntry = AST_GET_PORTENTRY (u2LocalPortNum);
    if (pPortEntry == NULL)
    {
        return RST_FAILURE;
    }
#ifdef PVRST_WANTED
    /* synch messages are sent only for the pvid that is set
     * instead of sending for all u2InstanceId incase of ACCESS port*/
    if (AST_IS_PVRST_ENABLED ())
    {
        u4IfIndex = pPortEntry->u4IfIndex;

        AstL2IwfGetVlanPortType ((UINT2) u4IfIndex, &u1PortType);
        if (u1PortType == VLAN_ACCESS_PORT)
        {
            AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &Pvid);
            if (Pvid != u2InstanceId)
            {
                /* Pvid has been changed but it is still not updated in PVRST */
                return RST_FAILURE;
            }
        }
    }
#endif

    /* 
     * If the component type is not a C-VLAN component, then the
     * u2ProtocolPort will be sent as zero. 
     *
     * CAUTION: Below this point for macros accessing data structure 
     * u2LocalPortNum should not be used, as u2LocalPortNum is 
     * getting modified for a C-VLAN component. Instead (pPortEntry->u2PortNo) 
     * shoule be used. Similary for function calls with local port number as
     * an argument.
     */
    if (AST_COMP_TYPE () == AST_PB_C_VLAN)
    {
        /* It is a C-VLAN Component. */

        /* In case of C-VLAN component:
         *    - u2LocaPortNum will represent the CEP local port 
         *      number in the parent context.
         *    - u2ProtocolPort represent the S-VLAN Id. */
        u2ProtocolPort = pPortEntry->u2ProtocolPort;
        u2LocalPortNum = AST_RED_CTXT_CEP_LOCAL_NO ();
    }

    /* Form Message based on Type */
    switch (u1MessageType)
    {
        case RED_AST_PORT_INFO:
            u2Len = RED_AST_PORT_INFO_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                AST_RM_PUT_1_BYTE (pOutput, pu4Offset,
                                   (AST_GET_PORT_ROLE
                                    (u2InstIndex, (pPortEntry->u2PortNo))));
                u1PortState = AST_GET_PORT_STATE (u2InstIndex,
                                                  (pPortEntry->u2PortNo));
            }
            else
#endif
            {
                AST_RM_PUT_1_BYTE (pOutput, pu4Offset,
                                   (AST_GET_PORT_ROLE
                                    (u2InstanceId, (pPortEntry->u2PortNo))));
                u1PortState = AST_GET_PORT_STATE (u2InstanceId,
                                                  (pPortEntry->u2PortNo));
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1PortState);
            break;
        case RED_AST_TIMES:
            u2Len = RED_AST_TIMES_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageSubType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (u1MessageSubType, u2InstanceId,
                                 pPortEntry->u2PortNo)));
            break;
        case RED_AST_PDU:
            u2Len = (UINT2) RED_AST_PDU_LEN;
            pPdu = AST_RED_RST_PDU_PTR (pPortEntry->u2PortNo);
            if (pPdu == NULL)
            {
                return RST_SUCCESS;
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_N_BYTE (pOutput, pPdu, pu4Offset, sizeof (tAstBpdu));
            break;
        case RED_AST_OPER_STATUS:
            u2Len = RED_AST_OPER_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageSubType);
            break;
        case RED_AST_CALLBACK_SUCCESS:
            u2Len = RED_AST_PORT_INFO_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                AST_RM_PUT_1_BYTE (pOutput, pu4Offset,
                                   (AST_GET_PORT_ROLE
                                    (u2InstIndex, (pPortEntry->u2PortNo))));
                u1PortState = AST_GET_PORT_STATE (u2InstIndex,
                                                  (pPortEntry->u2PortNo));
            }
            else
#endif
            {
                AST_RM_PUT_1_BYTE (pOutput, pu4Offset,
                                   (AST_GET_PORT_ROLE
                                    (u2InstanceId, (pPortEntry->u2PortNo))));
                u1PortState = AST_GET_PORT_STATE (u2InstanceId,
                                                  (pPortEntry->u2PortNo));
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1PortState);
            break;
        case RED_AST_CALLBACK_FAILURE:
            u2Len = RED_AST_PORT_INFO_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset,
                               (AST_GET_PORT_ROLE
                                (u2InstanceId, (pPortEntry->u2PortNo))));
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo, u2InstanceId);
            if (pPerStPortInfo == NULL)
            {
                AST_DBG (AST_RED_DBG, "RED: No PerStPortInfo\n");
                return RST_FAILURE;
            }
            if (pPerStPortInfo->i4NpPortStateStatus == AST_FORWARD_FAILURE)
            {
                u1PortState = AST_PORT_STATE_FORWARDING;
            }
            else if (pPerStPortInfo->i4NpPortStateStatus == AST_LEARN_FAILURE)
            {
                u1PortState = AST_PORT_STATE_LEARNING;
            }
            else
            {
                u1PortState = AST_PORT_STATE_DISCARDING;
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1PortState);
            break;

        case RED_AST_ALL_TIMES:
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                u2Len = RED_AST_ALL_TIMES_LEN;
            }
            else
#endif
            {
                if (u2InstanceId == 0)
                    u2Len = RED_AST_ALL_TIMES_LEN;
                else
                    u2Len = RED_AST_ALL_TIMES_INST_ZERO_LEN;
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (AST_TMR_TYPE_FDWHILE, u2InstanceId,
                                 pPortEntry->u2PortNo)));
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (AST_TMR_TYPE_RCVDINFOWHILE, u2InstanceId,
                                 pPortEntry->u2PortNo)));
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (AST_TMR_TYPE_TCWHILE, u2InstanceId,
                                 pPortEntry->u2PortNo)));
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (AST_TMR_TYPE_RRWHILE, u2InstanceId,
                                 pPortEntry->u2PortNo)));
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                               (AstRedGetExpireTime
                                (AST_TMR_TYPE_RBWHILE, u2InstanceId,
                                 pPortEntry->u2PortNo)));
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                   (AstRedGetExpireTime
                                    (AST_TMR_TYPE_MDELAYWHILE, u2InstanceId,
                                     pPortEntry->u2PortNo)));
                AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                   (AstRedGetExpireTime
                                    (AST_TMR_TYPE_EDGEDELAYWHILE, u2InstanceId,
                                     pPortEntry->u2PortNo)));
                AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                   (AstRedGetExpireTime
                                    (AST_TMR_TYPE_RAPIDAGE_DURATION,
                                     u2InstanceId, pPortEntry->u2PortNo)));
            }
            else
#endif
            {
                if (u2InstanceId == 0)
                {
                    AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                       (AstRedGetExpireTime
                                        (AST_TMR_TYPE_MDELAYWHILE, u2InstanceId,
                                         pPortEntry->u2PortNo)));
                    AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                       (AstRedGetExpireTime
                                        (AST_TMR_TYPE_EDGEDELAYWHILE,
                                         u2InstanceId, pPortEntry->u2PortNo)));
                    AST_RM_PUT_4_BYTE (pOutput, pu4Offset,
                                       (AstRedGetExpireTime
                                        (AST_TMR_TYPE_RAPIDAGE_DURATION,
                                         u2InstanceId, pPortEntry->u2PortNo)));
                }
            }
            break;

        case RED_AST_CLEAR_SYNCUP_DATA_ON_PORT:

            u2Len = (UINT2) RED_AST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            break;

        case RED_AST_CLEAR_ALL_SYNCUP_DATA:

            u2Len = (UINT2) RED_AST_CLEAR_ALL_SYNCUP_DATA_LEN;
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            /* In case of C-VLAN component, u2LocalPortNum will give the
             * corresponding CEP local port number.
             * Otherwise u2LocalPortNum will be zero here. */
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            break;

#ifdef PVRST_WANTED
        case RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT:

            u2Len = (UINT2) RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;

            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            break;

        case RED_PVRST_PDU:
            u2Len = (UINT2) RED_AST_PVRST_BPDU_LEN;
            /* To access data structure, use local port no is the port entry.
             * The u2LocalPortNum is the CEP port no in case of C-VLAN 
             * component. */
            if (NULL ==
                (pPvrstPdu = AST_RED_PVRST_PDU_PTR (pPortEntry->u2PortNo)))
            {
                /* PDU not present */
                return RST_SUCCESS;
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_N_BYTE (pOutput, &pPvrstPdu->PvrstRedPdu,
                               pu4Offset, sizeof (tPvrstBpdu));
            break;

#endif /* PVRST_WANTED */

#ifdef MSTP_WANTED
        case RED_MST_CLEAR_SYNCUP_DATA_ON_PORT:

            u2Len = (UINT2) RED_MST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;

            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            break;
        case RED_MST_PDU:
            u2Len = (UINT2) RED_AST_MST_BPDU_LEN;
            /* To access data structure, use local port no is the port entry.
             * The u2LocalPortNum is the CEP port no in case of C-VLAN 
             * component. */
            if (NULL == (pMstPdu = AST_RED_MST_PDU_PTR (pPortEntry->u2PortNo)))
            {
                /* PDU not present */
                return MST_SUCCESS;
            }
            AST_RM_PUT_1_BYTE (pOutput, pu4Offset, u1MessageType);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2Len);
            AST_RM_PUT_4_BYTE (pOutput, pu4Offset, AST_CURR_CONTEXT_ID ());
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2InstanceId);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2LocalPortNum);
            AST_RM_PUT_2_BYTE (pOutput, pu4Offset, u2ProtocolPort);
            AST_RM_PUT_N_BYTE (pOutput, &pMstPdu->MstRedPdu,
                               pu4Offset, sizeof (tMstBpdu));
            break;

#endif /* MSTP_WANTED */

        default:
            /* Invalid Message */

            AST_ASSERT ();
            break;
    }

    if ((u1MemEstFlag == 1) && (AST_HR_STATUS () != AST_HR_STATUS_DISABLE))
    {
        /* This is to find the memory required for syncing AstFormMessage */
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstContextInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPerStInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize =
            sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT1) + sizeof (UINT1);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPortEntry",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstContextInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPerStInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT2) + sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPortEntry",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstContextInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPerStInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = sizeof (UINT2) + sizeof (UINT2);
        IssSzUpdateSizingInfoForHR ((CHR1 *) "AST", (CHR1 *) "tAstPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done , so changing the flag to next bulk message  */
        u1MemEstFlag = 0;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSendSyncMessages                               */
/*                                                                           */
/* Description        : This function is invoked to  Alloc and Send Bulk     */
/*                      Sync Up Messages                                     */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id                           */
/*                    : u2LocalPortNum - Local port number                   */
/*                    : u1MessageType - Type of Sync Message                 */
/*                    : u1MessageSubType- SubType of SyncMessage             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedSendSyncMessages (UINT2 u2InstanceId, UINT2 u2LocalPortNum,
                        UINT1 u1MessageType, UINT1 u1MessageSubType)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Len;
    VOID               *pBuf = NULL;
    UINT4               u4Offset = AST_INIT_VAL;
    INT4                i4RetVal = RST_FAILURE;
    UINT4               u4IfIndex = 0;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Handle Bulk Request separately */
    if (u1MessageType == RED_AST_BULK_REQ)
    {
        u4Len = RED_AST_BULK_REQ_LEN;
        if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
        {
            AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_ALL_FAILURE_TRC,
                            "RM Allocation Failed\n");
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            AstRmHandleProtocolEvent (&ProtoEvt);
            return RST_FAILURE;
        }
        AST_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
        AST_RM_PUT_2_BYTE (pBuf, &u4Offset, u4Len);
        i4RetVal = RST_SUCCESS;
    }
    else
    {
        /* else all messages are from Active to Standby */
        if (AST_NUM_STANDBY_NODES () == 0)
        {
            AST_TRC (AST_CONTROL_PATH_TRC, "None of the Peer Nodes  are up \
                     No Need to send Dynamic Updates\n");
            return RST_SUCCESS;
        }

        switch (u1MessageType)
        {
            case RED_AST_PORT_INFO:
                u4Len = RED_AST_PORT_INFO_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }

                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);
                break;
            case RED_AST_TIMES:
                u4Len = RED_AST_TIMES_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);
                break;
            case RED_AST_PDU:
                if (AST_RED_RST_PDU_PTR (u2LocalPortNum) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "No PDU to Send \n");
                    return RST_FAILURE;
                }

                u4Len = RED_AST_PDU_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }

                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                /* In All ports which has caused a changes its 
                 * Port Role/State send the changed Information */
                AstRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Len);
                if (AstRedSendUpdate () == RST_FAILURE)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "AST Bulk request Failed\n");
                    return RST_FAILURE;
                }
                else
                {
                    return RST_SUCCESS;
                }
                break;

            case RED_AST_OPER_STATUS:

                u4IfIndex = AST_GET_IFINDEX (u2LocalPortNum);
                if ((u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
                    && (u4IfIndex <=
                        (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
                {
                    /* Oper status for port channels will be updated
                     * during addition of a active ports to port channel */
                    return RST_SUCCESS;
                }

                u4Len = RED_AST_OPER_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }

                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);
                break;
            case RED_AST_CALLBACK_SUCCESS:
                u4Len = RED_AST_PORT_INFO_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);
                break;
            case RED_AST_CALLBACK_FAILURE:
                u4Len = RED_AST_PORT_INFO_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);
                break;

            case RED_AST_CLEAR_SYNCUP_DATA_ON_PORT:
                u4Len = RED_AST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                break;

#ifdef MSTP_WANTED
            case RED_MST_CLEAR_SYNCUP_DATA_ON_PORT:
                u4Len = RED_MST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                break;
            case RED_MST_PDU:
                u4Len = RED_AST_MST_BPDU_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                /* In All ports which has caused a changes its 
                 * Port Role/State send the changed Information*/
                AstRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Len);
                if (AstRedSendUpdate () == RST_FAILURE)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "AST Bulk request Failed\n");
                    return RST_FAILURE;
                }
                else
                    return RST_SUCCESS;
                break;
#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
            case RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT:
                u4Len = RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                break;
            case RED_PVRST_PDU:
                u4Len = RED_AST_PVRST_BPDU_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                i4RetVal = AstRedFormMessage (u2InstanceId, u2LocalPortNum,
                                              u1MessageType, u1MessageSubType,
                                              pBuf, &u4Offset);

                /* In All ports which has caused a changes its 
                 * Port Role/State send the changed Information*/
                AstRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Len);
                if (AstRedSendUpdate () == RST_FAILURE)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "AST Bulk request Failed\n");
                    return RST_FAILURE;
                }
                else
                    return RST_SUCCESS;
                break;
#endif /* PVRST_WANTED */
            case RED_AST_CLEAR_ALL_SYNCUP_DATA:
                u4Len = RED_AST_CLEAR_ALL_SYNCUP_DATA_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u4Len)) == NULL)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    return RST_FAILURE;
                }
                AST_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
                AST_RM_PUT_2_BYTE (pBuf, &u4Offset, u4Len);
                AST_RM_PUT_4_BYTE (pBuf, &u4Offset, AST_CURR_CONTEXT_ID ());
                u2LocalPortNum = 0;
                /* In case of C-VLAN component, send the local port number of
                 * CEP too. */
                if (AST_COMP_TYPE () == AST_PB_C_VLAN)
                {
                    u2LocalPortNum = AST_RED_CTXT_CEP_LOCAL_NO ();
                }
                AST_RM_PUT_2_BYTE (pBuf, &u4Offset, u2LocalPortNum);
                break;

            default:
                /* Invalid Message */
                AST_ASSERT ();
                return RST_FAILURE;
        }
    }

    if (i4RetVal == RST_SUCCESS)
    {
        AstRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Len);
        return RST_SUCCESS;
    }
    else
    {
        RM_FREE (pBuf);
        return RST_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHandleAstPdus                                  */
/* Description        : Stores the latest PDUs in Standby                    */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2TotalLen     - Length of Data                      */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedHandleAstPdus (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tAstRedPdu          AstRedPdu;
    INT4                i4RetVal = RST_FAILURE;
    UINT2               u2ProtocolPort = 0;
    UINT2               u2LocalPort;
    UINT2               u2InstanceId = 0;

    if (u2Len != RED_AST_PDU_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC
                 | AST_RED_DBG, "AstRedHandleAstPdus: Incorrect Len !\n");
        AST_ASSERT ();
        return RST_FAILURE;
    }
    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);

    /* Store the Data Buffer */
    AST_MEMSET (AST_RED_PDU_DATA (AstRedPdu), AST_INIT_VAL, sizeof (tAstBpdu));

    AST_RM_GET_N_BYTE (pData, AST_RED_PDU_DATA (AstRedPdu),
                       pu4Offset, sizeof (tAstBpdu));

    if ((u2InstanceId > AST_MAX_MST_INSTANCES)
        || (u2LocalPort > AST_MAX_PORTS_PER_CONTEXT))
    {
        AST_ASSERT ();
        AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleAstPdus: "
                      "Instance id %d or local port %d invalid \n",
                      u2InstanceId, u2LocalPort);
        return RST_FAILURE;
    }
    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleAstPdus: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return RST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort, u2ProtocolPort,
                                                    &u2LocalPort);
        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleAstPdus: "
                          "PB C-VLAN selection failed for "
                          "port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
            return RST_FAILURE;
        }
    }

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleAstPdus: Port Down "
                      "for port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
        return RST_FAILURE;
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "AstRedHandleAstPdus: "
                      "ASTP Perst info is not present for instance %d\n",
                      u2InstanceId);
        return RST_FAILURE;
    }
    if (AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId) == NULL)
    {
        AST_DBG_ARG2 (AST_RED_DBG, "AstRedHandleAstPdus: "
                      "ASTP Perst port info is not present for "
                      "port %s instance %d\n",
                      AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
        return RST_FAILURE;
    }

    if (AST_IS_RST_ENABLED ())
    {
        /*Store Bpdu in Standby */
        RstRedStoreAstPdu (u2LocalPort, AST_RED_PDU_DATA (AstRedPdu),
                           sizeof (tAstBpdu));

        /* Apply PDUs */
        AstRedApplyLatestPduInfo (u2InstanceId, u2LocalPort,
                                  AST_RED_PDU_DATA (AstRedPdu));
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedGetPortState                                   */
/*                                                                           */
/* Description        : Gets Synced Up Port State                            */
/*                      Should not be called when Active                     */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Synced Up Port State                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Synced Up Port State.                                */
/*****************************************************************************/
INT4
AstRedGetPortState (UINT2 u2Port)
{
    UINT1               u1PortState = AST_INIT_VAL;
    /* Give from synced up Port States for external 
       Interfaces before switchover is complete */
    u1PortState = AST_RED_PORT_STATE (RST_DEFAULT_INSTANCE, u2Port);
    if (u1PortState == AST_INIT_VAL)
    {
        return AST_PORT_STATE_DISABLED;
    }
    else
    {
        return (u1PortState);
    }

}

/*****************************************************************************/
/* Function Name      : AstRedSyncUpPdu                                      */
/*                                                                           */
/* Description        : Stores and Send RSTP sync up Pdu to Standby Node     */
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/
INT4
AstRedSyncUpPdu (UINT2 u2PortNum, tAstBpdu * pData, UINT2 u2Len)
{
    UNUSED_PARAM (u2Len);

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        return RST_SUCCESS;
    }

    if (AST_RED_SYNC_FLAG (RST_DEFAULT_INSTANCE) == OSIX_TRUE)
    {
        /* Store Pdus in Active node */
        if (pData != NULL)
        {
            RstRedStoreAstPdu (u2PortNum, pData, sizeof (tAstBpdu));
        }

        AstRedSendSyncMessages (RST_DEFAULT_INSTANCE, u2PortNum, RED_AST_PDU,
                                0);

        AST_RED_RESET_SYNC_FLAG (RST_DEFAULT_INSTANCE);
    }
    return RST_SUCCESS;
}

/******************************************************************************/
/* Function           : AstRedHwAuditTask                                     */
/* Input(s)           : None                                                  */
/* Output(s)          : None.                                                 */
/* Returns            : RM_SUCCESS/RM_FAILURE                                 */
/* Action             : Routine to create Audit Task.                         */
/******************************************************************************/
INT4
AstRedHwAuditTask (VOID)
{
    UINT4               u4RetVal = RST_SUCCESS;

    /* Task to check whether Standby and H/W Port Roles/States are same */

    if (AST_RED_HW_AUDIT_TASKID == AST_INIT_VAL)
    {
        if (OsixTskCrt ((UINT1 *) AST_HARDWARE_AUDIT,
                        AST_HARDWARE_AUDIT_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) AstRedHwAudit,
                        0, &AST_RED_HW_AUDIT_TASKID) != OSIX_SUCCESS)

        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_RED_DBG,
                            "RED: Audit Task creation failed\n");
            u4RetVal = RST_FAILURE;

        }
        else
        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_RED_DBG,
                            "RED: Audit Task creation successful\n");
            u4RetVal = RST_SUCCESS;
        }
    }
    return (u4RetVal);
}

/*****************************************************************************/
/* Function Name      : RedDumpSyncedUpData                                  */
/*                                                                           */
/* Description        : Dumped Synced Up times on Standby on all             */
/*                      Ports Instance                                       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RedDumpSyncedUpData (tCliHandle CliHandle)
{
    UINT4               u4Context;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2LocalPort = 0;
    /* 
     * CAUTION: If CliPrintf is to be used then we need to use nmh routines.
     * Otherwise in more, cli may may the global context pointer as null.
     * So the code may crash in between.
     * Hence normal AST_PRINT is used to show redundancy output. 
     */
    UNUSED_PARAM (CliHandle);

    for (u4Context = 0; u4Context < AST_SIZING_CONTEXT_COUNT; u4Context++)
    {
        if (AstSelectContext (u4Context) != RST_SUCCESS)
        {
            continue;
        }

         AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d : Timer Synced Updata.\n", u4Context);

#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            for (u2InstanceId = 1; u2InstanceId <= AST_MAX_PVRST_INSTANCES;
                 u2InstanceId++)
            {
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    if (AST_GET_PORTENTRY (u2LocalPort) != NULL)
                    {
                        if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) ==
                            RST_FALSE)
                        {
                            AstRedDumpSyncUpTmrsPerPortInst (u2InstanceId,
                                                             u2LocalPort);
                        }

                    }
                }
            }
        }
        else
#endif
        {
            for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                 u2InstanceId++)
            {
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    if (AST_GET_PORTENTRY (u2LocalPort) != NULL)
                    {
                        if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) ==
                            RST_FALSE)
                        {
                            AstRedDumpSyncUpTmrsPerPortInst (u2InstanceId,
                                                             u2LocalPort);
                        }

                    }
                }
            }
        }

        AstRedDumpSyncUpAllCvlanTmrData ();

        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : RedDumpSyncedUpPdus                                  */
/*                                                                           */
/* Description        : Dump PDU on all ports in the system.                 */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RedDumpSyncedUpPdus (tCliHandle CliHandle)
{
    UINT4               u4Context;
    UINT2               u2Port;

    /* 
     * CAUTION: If CliPrintf is to be used then we need to use nmh routines.
     * Otherwise in more, cli may may the global context pointer as null.
     * So the code may crash in between.
     * Hence normal AST_PRINT is used to show redundancy output. 
     */
    UNUSED_PARAM (CliHandle);

    for (u4Context = 0; u4Context < AST_SIZING_CONTEXT_COUNT; u4Context++)
    {
        if (AstSelectContext (u4Context) != RST_SUCCESS)
        {
            continue;
        }

        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d : PDUs Synced Updata.\n", u4Context);

        for (u2Port = 1; u2Port < AST_MAX_PORTS_PER_CONTEXT; u2Port++)
        {

            if ((AST_GET_PORTENTRY (u2Port) != NULL) &&
                (AST_IS_CUSTOMER_EDGE_PORT (u2Port) == RST_FALSE))
            {
                RedDumpSyncedUpPduonPort (u2Port);

            }

        }

        AstRedDumpAllCvlanSyncUpPdus ();
        AstReleaseContext ();

    }

    return;
}

/*****************************************************************************/
/* Function Name      : RedDumpSynced Up Outputs                             */
/*                                                                           */
/* Description        : Dump PDU on a particular Port                        */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncedUpOutputs (tCliHandle CliHandle)
{
    UINT4               u4Context;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2LocalPort = 0;

    /* 
     * CAUTION: If CliPrintf is to be used then we need to use nmh routines.
     * Otherwise in more, cli may may the global context pointer as null.
     * So the code may crash in between.
     * Hence normal AST_PRINT is used to show redundancy output. 
     */
    UNUSED_PARAM (CliHandle);

    for (u4Context = 0; u4Context < AST_SIZING_CONTEXT_COUNT; u4Context++)
    {
        if (AstSelectContext (u4Context) != RST_SUCCESS)
        {
            continue;
        }

        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d : Output Synced Updata.\n", u4Context);

#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            for (u2InstanceId = 1; u2InstanceId <= AST_MAX_PVRST_INSTANCES;
                 u2InstanceId++)
            {
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    if (AST_GET_PORTENTRY (u2LocalPort) != NULL)
                    {
                        if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) ==
                            RST_FALSE)
                        {
                            AstRedDumpSyncUpOutputPerPort (u2InstanceId,
                                                           u2LocalPort);
                        }
                    }
                }
            }
        }
        else
#endif
        {
#ifdef MSTP_WANTED
            for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                 u2InstanceId++)
            {
#endif
                for (u2LocalPort = 1; u2LocalPort <= AST_MAX_PORTS_PER_CONTEXT;
                     u2LocalPort++)
                {
                    if (AST_GET_PORTENTRY (u2LocalPort) != NULL)
                    {
                        if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) ==
                            RST_FALSE)
                        {
                            AstRedDumpSyncUpOutputPerPort (u2InstanceId,
                                                           u2LocalPort);
                        }
                    }
                }
#ifdef MSTP_WANTED
            }
#endif
        }

        AstRedDumpAllCvlanSyncUpOutputs ();
        AstReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : RedDumpSyncedUpPduonPort                             */
/*                                                                           */
/* Description        : Dump PDU on a particular Port                        */
/*                                                                           */
/* Input(s)           : u2Port - Local port id of the port where pdu needs   */
/*                               to be dumped.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RedDumpSyncedUpPduonPort (UINT2 u2Port)
{
    tAstBpdu           *pPdu = NULL;
    UINT1               au1MacAddr[21];

    MEMSET (au1MacAddr, 0, 20);

    if (NULL != (pPdu = AST_RED_RST_PDU_PTR (u2Port)))
    {
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dumping Data On Port %d\n", u2Port);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"------------------------\n");
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"RootId %x:", (pPdu->RootId.u2BrgPriority));
        PrintMacAddress ((UINT1 *) &(pPdu->RootId.BridgeAddr), au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Designated BrId %x:", (pPdu->DesgBrgId.u2BrgPriority));
        PrintMacAddress ((UINT1 *) &(pPdu->DesgBrgId.BridgeAddr), au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Root path Cost %x\n", (int) pPdu->u4RootPathCost);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Length %x\n", pPdu->u2Length);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Protocol Id %x\n", pPdu->u2ProtocolId);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Port Id %x\n", pPdu->u2PortId);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Message Age %x\n", pPdu->u2MessageAge);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Max Age %x\n", pPdu->u2MaxAge);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Hello Time %x\n", pPdu->u2HelloTime);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Fwd Delay Time %x \n", pPdu->u2FwdDelay);
        PrintMacAddress ((UINT1 *) &(pPdu->DestAddr), au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dest Address %s \n", au1MacAddr);
        PrintMacAddress ((UINT1 *) &(pPdu->SrcAddr), au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Src Address %s \n", au1MacAddr);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version Length %x \n", pPdu->u1Version1Len);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version %x \n", pPdu->u1Version);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"BPDU Type %x \n", pPdu->u1BpduType);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Flags %x \n", pPdu->u1Flags);
    }
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncUpOutputPerPort                        */
/*                                                                           */
/* Description        : This function dumps outputs synced up per port and   */
/*                      and per instance.                                    */
/*                                                                           */
/* Input(s)           : CliHandle - CliHandle                                */
/*                      u2InstanceId - Mst Instance Id.                      */
/*                      u2Port - Local port id of the port where synced      */
/*                               output to be dumped.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpOutputPerPort (UINT2 u2InstanceId, UINT2 u2Port)
{
    UINT1               u1PortRole;
    UINT1               u1PortState;

    u1PortRole = AST_RED_PORT_ROLE (u2InstanceId, u2Port);
    u1PortState = AST_RED_PORT_STATE (u2InstanceId, u2Port);

    if (u1PortRole != 0 && u1PortState != 0)
    {
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Port Role/State for Instance %d Port %d\n",
                u2InstanceId, u2Port);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"======================\r\n");
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Port Role %d Port State %d\n", u1PortRole, u1PortState);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncUpTmrsPerPortInst                      */
/*                                                                           */
/* Description        : This function dumps synced up timer vals per Port &  */
/*                      per instance.                                        */
/*                                                                           */
/* Input(s)           : u2InstanceId - Mst Instance Id.                      */
/*                      u2Port - Local port id of the port where synced      */
/*                               output to be dumped.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpTmrsPerPortInst (UINT2 u2InstanceId, UINT2 u2Port)
{
    tAstRedTimes       *pTimes = NULL;

    if (NULL != (pTimes = AST_RED_TIMES_PTR (u2InstanceId, u2Port)))
    {
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Instance %d Port %d\n", u2InstanceId, u2Port);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"======================\r\n");
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected FdWile expiry time %x\n",
                (int) (pTimes->u4FdWhileExpTime));
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected rcvdInfo exp Time %x\n",
                (int) (pTimes->u4RcvdInfoWhileExpTime));
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected rrWhile exp Time %x\n",
                (int) (pTimes->u4RrWhileExpTime));
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected rbWhile exp Time %x\n",
                (int) (pTimes->u4RbWhileExpTime));
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected tcWhile exp Time %x\n",
                (int) (pTimes->u4TcWhileExpTime));
        if (u2InstanceId == 0)
        {
            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected MdelayWhile exp Time %x\n",
                    (int) (pTimes->u4MdelayWhileExpTime));
            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected EdgeDelayWhile exp Time %x\n",
                    (int) (pTimes->u4EdgeDelayWhileExpTime));
            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Expected RapidAgeDuration exp Time %x\n",
                    (int) (pTimes->u4RapidAgeDurtnExpTime));
        }
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHandleBulkUpdateEvent                          */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      AstRedHandleBulkRequest is triggered.                */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHandleBulkUpdateEvent (VOID)
{
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        AstRedHandleBulkRequest ();
    }
    else
    {
        AST_BULK_REQ_RECD () = AST_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : AstRedSendBulkUpdateTailMsg                          */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = AST_INIT_VAL;
    UINT2               u2BufLen;

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_ALL_FAILURE_TRC,
                        "AST: Node is not active."
                        "Bulk update tail message not sent.\n");
        return RST_SUCCESS;
    }

    /* Form a bulk update tail message. 

     *        <-----------1 Byte----------><----2 Byte------>
     ******************************************************* 
     *        *                           *                *
     * RM Hdr * RED_AST_BULK_UPD_TAIL_MSG * Msg Length     *
     * *      *                           *                *
     *******************************************************

     * The RM Hdr shall be included by RM.
     */

    u2BufLen = RED_AST_BULK_UPD_TAIL_MSG_LEN;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufLen)) == NULL)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT, ALL_FAILURE_TRC,
                        "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AstRmHandleProtocolEvent (&ProtoEvt);
        return (RST_FAILURE);
    }

    /* Fill the message type. */
    AST_RM_PUT_1_BYTE (pMsg, &u4Offset, RED_AST_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs */
    AST_RM_PUT_2_BYTE (pMsg, &u4Offset, RED_AST_BULK_UPD_TAIL_MSG);

    if (AstRedSendUpdateToRM (pMsg, u2BufLen) == RST_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        AstRmHandleProtocolEvent (&ProtoEvt);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRestartModule                                     */
/*                                                                           */
/* Description        : This function will restart RSTP/MSTP module.         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstRestartModule (VOID)
{
    UINT4               u4ContextId;

    AST_LOCK ();
    /*Clear all the sync up data before Restarting Modules */
    AstRedClearAllContextSyncUpData ();

    /* Release all the MemPools of sync-up database */
    AstRedRelPduMemPools ();

    AstRedRelStoreMemPools ();


    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            continue;
        }
#ifdef MSTP_WANTED
        if (AST_IS_MST_STARTED ())
        {
            if (MstModuleShutdown () != MST_SUCCESS)
            {
                AST_TRC (ALL_FAILURE_TRC,
                         "MSTP shutdown failed during restart for "
                         "applying configuration database\n");
                AstReleaseContext ();
                continue;
            }
        }
#endif

        if (AST_IS_RST_STARTED ())
        {
            if (RstModuleShutdown () != RST_SUCCESS)
            {
                AST_TRC (ALL_FAILURE_TRC,
                         "RSTP shutdown failed during restart for "
                         "applying configuration database\n");
                AstReleaseContext ();
                continue;
            }
        }

#ifdef MSTP_WANTED
        if (MstModuleInit () != MST_SUCCESS)
        {
            AST_TRC (ALL_FAILURE_TRC,
                     "MSTP start failed during restart for "
                     "applying configuration database\n");
        }
#else
        if (RstModuleInit () != RST_SUCCESS)
        {
            AST_TRC (ALL_FAILURE_TRC,
                     "RSTP start failed during restart for "
                     "applying configuration database\n");
        }
#endif
        AstReleaseContext ();
    }

    AST_UNLOCK ();

    /* Initialize the Redundancy information */
    /* Node status will be set as IDLE in this function */
    AstRedRmInit ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedInitOperTimes                                  */
/*                                                                           */
/* Description        : This function will initalize the timers values in    */
/*                      sync up database that are started during             */
/*                      Port Enable Msg                                      */
/*                                                                           */
/* Input(s)           : Instance Id - Instance Information                   */
/*                      PortIfIndex - Port Interface Index                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstRedInitOperTimes (UINT2 u2InstanceId, UINT2 u2PortIfIndex)
{
    tAstTimeStamp       TimeStamp;
    tAstRedTimes       *pRedTimes = NULL;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    /* Initalize FdWhile Time in SyncUp Database */
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            return RST_FAILURE;
        }
        pRedTimes = AST_RED_TIMES_PTR (u2InstIndex, u2PortIfIndex);
    }
    else
#endif
    {
        pRedTimes = AST_RED_TIMES_PTR (u2InstanceId, u2PortIfIndex);
    }
    if (pRedTimes == NULL)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_TIMES_MEMPOOL_ID, pRedTimes,
                                 tAstRedTimes);

        if (pRedTimes == NULL)
        {
            return RST_FAILURE;
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            AST_RED_TIMES_PTR (u2InstIndex, u2PortIfIndex) = pRedTimes;
        }
        else
#endif
        {
            AST_RED_TIMES_PTR (u2InstanceId, u2PortIfIndex) = pRedTimes;
        }
    }
    AST_GET_TIME_STAMP (&TimeStamp);

    pRedTimes->u4FdWhileExpTime =
        RST_DEFAULT_BRG_FWD_DELAY * SYS_TIME_TICKS_IN_A_SEC;

    AST_ADD_TIME_STAMP (pRedTimes->u4FdWhileExpTime, TimeStamp);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedAllocPortAndPerPortInstInfoBlock               */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      Context PortInfo Array and Context perportinst Array.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedAllocPortAndPerPortInstInfoBlock ()
{
    if (AST_RED_CONTEXT_PORT_TBL () == NULL)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_PORTINFO_TBL_POOL_ID,
                                 AST_RED_CONTEXT_PORT_TBL (),
                                 tAstRedPortInfo *);

        if (AST_RED_CONTEXT_PORT_TBL () == NULL)
        {
            AST_TRC (ALL_FAILURE_TRC, "MemBlock Allocation FAILED.\n");
            return RST_FAILURE;
        }
    }

    MEMSET (AST_RED_CONTEXT_PORT_TBL (), AST_INIT_VAL,
            (sizeof (tAstRedPortInfo *)) * AST_MAX_PORTS_PER_CONTEXT);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedReleasePortAndPerPortInstInfoBlock             */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                      Context PortEntry Array and Context perportInst Array*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedReleasePortAndPerPortInstInfoBlock ()
{
    if (AST_RED_CONTEXT_PORT_TBL () != NULL)
    {
        AST_FREE_RED_MEM_BLOCK (AST_RED_PORTINFO_TBL_POOL_ID,
                                AST_RED_CONTEXT_PORT_TBL ());
        AST_RED_CONTEXT_PORT_TBL () = NULL;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleBulkUpdatePerPort                        */
/*                                                                           */
/* Description        : This functions takes care of the bulk update part    */
/*                      per port.                                            */
/*                                                                           */
/* Input(s)           : u2LocalPort - Local port id                          */
/*                                                                           */
/* In and Out Params  : ppBuf - Pointer to buffer where the information      */
/*                              needs to be put. If this is null, then a     */
/*                              new buffer will be allocated.                */
/*                      pu4Offset - Offset the above buffer from where the   */
/*                              next write operation needs to be started.    */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
AstRedHandleBulkUpdatePerPort (UINT2 u2LocalPort, VOID **ppBuf,
                               UINT4 *pu4Offset)
{
#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    tAstPerStInfo      *pPerStInfo = NULL;
#endif
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2InstanceId = 0;
    UINT2               u2Len = AST_INIT_VAL;
    UINT2               u2MaxInstances = AST_INIT_VAL;
    UINT1               u1MessageType = AST_INIT_VAL;
    UINT1               u1NextMessage = AST_INIT_VAL;
    UINT1               u1MessageSubType = AST_INIT_VAL;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    ProtoEvt.u4AppId = RM_RSTPMSTP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* In C-VLAN component, only one instance will be present.
     * Access to another instance (pPerStInfo) may crash. 
     * Also this approach saves time when RST is running. */
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2MaxInstances = AST_MAX_PVRST_INSTANCES;
    }
    else
#endif
    {
        u2MaxInstances = AST_MAX_MST_INSTANCES;
    }

    if (AST_IS_RST_STARTED ())
    {
        u2MaxInstances = RST_MAX_TREE_INSTANCES;
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        for (u2InstIndex = 1; u2InstIndex <= u2MaxInstances; u2InstIndex++)
        {
            if ((u2InstanceId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) ==
                AST_INIT_VAL)
            {
                continue;
            }
            if ((pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex)) == NULL)
            {
                continue;
            }
            pPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex);

            if (pPerStPortInfo != NULL)
            {
                /* All Messages Loop Should come here */
                while (1)
                {
                    /* Get Next Message to be sent */
                    if (RST_FAILURE ==
                        AstRedGetNextMessage (u1MessageType,
                                              &u1NextMessage, &u2Len))
                    {
                        /* All Messages are sent on this Port,
                         * Now we can Move on to the Next Port/Instance */
                        u1MessageType = 0;
                        break;
                    }

                    u1MessageType = u1NextMessage;
                    /* Send all messages for this Port */
                    /* Cant encode */
                    if ((AST_MAX_RM_BUF_SIZE - *pu4Offset) < u2Len)
                    {
                        if (*ppBuf != NULL)
                        {
                            /* Send and Allocate */
                            if (AstRedSendUpdateToRM ((tRmMsg *) * ppBuf,
                                                      (UINT2) *pu4Offset)
                                == RST_FAILURE)
                            {
                                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                                AstRmHandleProtocolEvent (&ProtoEvt);
                            }
                        }
                        *pu4Offset = 0;
                        /* Buffer Not available */
                        if ((*ppBuf =
                             (VOID *)
                             RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) == NULL)
                        {
                            AST_TRC (AST_ALL_FAILURE_TRC,
                                     "RM Allocation Failed\n");
                            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                            AstRmHandleProtocolEvent (&ProtoEvt);
                            return;
                        }
                    }
                    else
                    {
                        if (*ppBuf == NULL)
                        {
                            *pu4Offset = 0;
                            /* Buffer Not available */
                            if ((*ppBuf =
                                 (VOID *)
                                 RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) == NULL)
                            {
                                AST_TRC (AST_ALL_FAILURE_TRC,
                                         "RM Allocation Failed\n");
                                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                                AstRmHandleProtocolEvent (&ProtoEvt);
                                return;
                            }

                        }
                    }
                    if (u1MessageType == RED_AST_OPER_STATUS)
                    {
                        u1MessageSubType =
                            AST_GET_PORTENTRY (u2LocalPort)->u1EntryStatus;
                        if (u1MessageSubType == AST_PORT_OPER_UP)
                            u1MessageSubType = AST_EXT_PORT_UP;
                        else if (u1MessageSubType == AST_PORT_OPER_DOWN)
                            u1MessageSubType = AST_EXT_PORT_DOWN;
                    }
                    if (AstRedFormMessage
                        (u2InstanceId, u2LocalPort, u1MessageType,
                         u1MessageSubType, *ppBuf, pu4Offset) != RST_SUCCESS)
                    {
                        RM_FREE (*ppBuf);
                        *ppBuf = NULL;
                        return;
                    }
                }
                /* Reset the changed Flag */
                PVRST_SET_CHANGED_FLAG (u2InstIndex, u2LocalPort) = PVRST_FALSE;
            }                    /* if Port is not Initialised */
        }
    }
    else
#endif
    {
#ifdef MSTP_WANTED
        for (u2InstanceId = 0; u2InstanceId < u2MaxInstances; u2InstanceId++)
        {
            if ((pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)) == NULL)
            {
                continue;
            }
#endif
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId);

            if (pPerStPortInfo != NULL)
            {
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                /* All Messages Loop Should come here */
                while (1)
                {
                    /* Get Next Message to be sent */
                    if (RST_FAILURE ==
                        AstRedGetNextMessage (u1MessageType,
                                              &u1NextMessage, &u2Len))
                    {
                        /* All Messages are sent on this Port,
                         * Now we can Move on to the Next Port/Instance */
                        u1MessageType = 0;
                        break;
                    }

                    u1MessageType = u1NextMessage;
                    if ((u1MessageType != RED_AST_OPER_STATUS) &&
                        (pRstPortInfo->bPortEnabled != AST_TRUE))
                    {
                        continue;
                    }

                    /* Send all messages for this Port */
                    /* Cant encode */
                    if ((AST_MAX_RM_BUF_SIZE - *pu4Offset) < u2Len)
                    {
                        if (*ppBuf != NULL)
                        {
                            /* Send and Allocate */
                            if (AstRedSendUpdateToRM ((tRmMsg *) * ppBuf,
                                                      (UINT2) *pu4Offset)
                                == RST_FAILURE)
                            {
                                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                                AstRmHandleProtocolEvent (&ProtoEvt);
                            }
                        }
                        *pu4Offset = 0;
                        /* Buffer Not available */
                        if ((*ppBuf =
                             (VOID *)
                             RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) == NULL)
                        {
                            AST_TRC (AST_ALL_FAILURE_TRC,
                                     "RM Allocation Failed\n");
                            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                            AstRmHandleProtocolEvent (&ProtoEvt);
                            return;
                        }
                    }
                    else
                    {
                        if (*ppBuf == NULL)
                        {
                            *pu4Offset = 0;
                            /* Buffer Not available */
                            if ((*ppBuf =
                                 (VOID *)
                                 RM_ALLOC_TX_BUF (AST_MAX_RM_BUF_SIZE)) == NULL)
                            {
                                AST_TRC (AST_ALL_FAILURE_TRC,
                                         "RM Allocation Failed\n");
                                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                                AstRmHandleProtocolEvent (&ProtoEvt);
                                return;
                            }
                        }
                    }
                    if (u1MessageType == RED_AST_OPER_STATUS)
                    {
                        u1MessageSubType =
                            AST_GET_PORTENTRY (u2LocalPort)->u1EntryStatus;
                        if (u1MessageSubType == AST_PORT_OPER_UP)
                            u1MessageSubType = AST_EXT_PORT_UP;
                        else if (u1MessageSubType == AST_PORT_OPER_DOWN)
                            u1MessageSubType = AST_EXT_PORT_DOWN;
                    }
                    if (AstRedFormMessage
                        (u2InstanceId, u2LocalPort, u1MessageType,
                         u1MessageSubType, *ppBuf, pu4Offset) != RST_SUCCESS)
                    {
                        RM_FREE (*ppBuf);
                        *ppBuf = NULL;
                        return;
                    }

                }
                /* Reset the changed Flag */
                AST_SET_CHANGED_FLAG (u2InstanceId, u2LocalPort) = RST_FALSE;
            }                    /* if Port is not Initialised */
#ifdef MSTP_WANTED
        }                        /* End of Instance Count */
#endif
    }

    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstRedPortInstHwAudit                                */
/*                                                                           */
/* Description        : This functions does the hw audit for the given port  */
/*                      and for the given MST instance.                      */
/*                                                                           */
/* Input(s)           : u2InstanceId - Mst Instance Id.                      */
/*                      pPortEntry - Pointer to the port entry.              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedPortInstHwAudit (UINT2 u2InstanceId, tAstPortEntry * pPortEntry)
{
    INT4                i4RetVal = FNP_FAILURE;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1CtrlPortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1FinalPortState = AST_PORT_STATE_DISCARDING;
#ifdef PVRST_WANTED
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif

    /* In case the hw does not support get functions (ex: C-VLAN
     * ports get port state), then the Get NPAPIs doesn't do
     * anything but returns success. In this case the 
     * port state returned from NPAPI will be discarding always.
     * This will trigger a protocol restart on all ports that are in
     * forwarding state in sw.
     * To avoid this update the u1PortState with the current value in 
     * sw.*/

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == AST_INIT_VAL)
        {
            AST_UNLOCK ();
            return RST_FAILURE;
        }
        u1PortState = AST_GET_PORT_STATE (u2InstIndex, (pPortEntry->u2PortNo));
    }
    else
#endif
    {
        u1PortState = AST_GET_PORT_STATE (u2InstanceId, (pPortEntry->u2PortNo));
    }

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        i4RetVal = AstMiMstpNpGetInstancePortState (pPortEntry->u2PortNo,
                                                    u2InstanceId, &u1PortState);
    }
    else
#endif
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        i4RetVal = AstMiPvrstNpGetVlanPortState (pPortEntry->u2PortNo,
                                                 u2InstanceId, &u1PortState);
    }
    else
#endif
    if (AST_IS_RST_ENABLED ())
    {
        i4RetVal = AstMiRstpNpGetPortState (pPortEntry->u2PortNo, &u1PortState);
    }

    if (i4RetVal == RST_SUCCESS)
    {
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            u1CtrlPortState = AST_GET_PORT_STATE (u2InstIndex,
                                                  (pPortEntry->u2PortNo));
        }
        else
#endif
        {
            u1CtrlPortState = AST_GET_PORT_STATE (u2InstanceId,
                                                  (pPortEntry->u2PortNo));
        }
        if (u1PortState != u1CtrlPortState)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                     "RED: Audit Mismatch\n");
            switch (u1CtrlPortState)
            {
                case AST_PORT_STATE_DISCARDING:
                case AST_PORT_STATE_DISABLED:
                    u1FinalPortState = u1CtrlPortState;
                    break;
                case AST_PORT_STATE_LEARNING:
                    if (u1PortState == AST_PORT_STATE_FORWARDING)
                    {
                        u1FinalPortState = AST_PORT_STATE_LEARNING;
                    }
                    else
                    {
                        u1FinalPortState = AST_PORT_STATE_DISCARDING;
                        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                                 "MSG: Protocol restart \n");
                        AstRedProtocolRestart (u2InstanceId,
                                               pPortEntry->u2PortNo);
                        /*Allow to set H/w PortState to 
                         * u1FinalPortState*/
                    }
                    break;
                case AST_PORT_STATE_FORWARDING:
                    if (u1PortState == AST_PORT_STATE_LEARNING)
                    {
                        u1FinalPortState = u1CtrlPortState;;
                    }
                    else
                    {
                        u1FinalPortState = AST_PORT_STATE_DISCARDING;
                        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                                 "MSG: Protocol restart \n");
                        AstRedProtocolRestart (u2InstanceId,
                                               pPortEntry->u2PortNo);
                    }
                    break;
                default:
                    break;
            }
            if (AST_IS_RST_ENABLED ())
            {
                if (AstMiRstpNpSetPortState (pPortEntry->u2PortNo,
                                             u1FinalPortState) != RST_SUCCESS)
                {
                    if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
                    {
                        AstPbRestoreContext ();
                    }
                    else
                    {
                        AstReleaseContext ();
                    }

                    AST_UNLOCK ();
                    return RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {

                if (AstMiMstpNpSetInstancePortState (pPortEntry->u2PortNo,
                                                     u2InstanceId,
                                                     u1FinalPortState) !=
                    RST_SUCCESS)
                {
                    AstReleaseContext ();
                    AST_UNLOCK ();
                    return RST_FAILURE;
                }
            }
#endif
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {

                if (AstMiPvrstNpSetVlanPortState (pPortEntry->u2PortNo,
                                                  u2InstanceId,
                                                  u1FinalPortState) !=
                    RST_SUCCESS)
                {
                    AstReleaseContext ();
                    AST_UNLOCK ();
                    return RST_FAILURE;
                }
            }
#endif
        }
    }
    else
    {
        AST_DBG (AST_RED_DBG, "RED: Get PortState from Hardware Failed\n");
        AST_ASSERT ();

        if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
        {
            AstPbRestoreContext ();
        }
        else
        {
            AstReleaseContext ();
        }

        AST_UNLOCK ();
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}
#endif /* NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : AstRedCreatePortAndPerPortInst                       */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      Red PortInfo and Red PerPortInfo                     */
/*                                                                           */
/* Input(s)           : u2Port - Local PortNumber                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedCreatePortAndPerPortInst (UINT2 u2Port)
{
    tAstRedPortInfo    *pAstRedPortEntry = NULL;
    tAstRedPerPortInstInfo *pAstRedPerPortInstInfo = NULL;

    pAstRedPortEntry = AST_RED_CONTEXT_PORT_INFO (u2Port);

    if (pAstRedPortEntry == NULL)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_PORT_INFO_MEMPOOL_ID,
                                 pAstRedPortEntry, tAstRedPortInfo);

        if (pAstRedPortEntry == NULL)
        {
            AST_TRC (ALL_FAILURE_TRC, "MemBlock Allocation FAILED.\n");
            return RST_FAILURE;
        }

        MEMSET (pAstRedPortEntry, AST_INIT_VAL, sizeof (tAstRedPortInfo));

        AST_RED_CONTEXT_PORT_INFO (u2Port) = pAstRedPortEntry;
    }

    pAstRedPerPortInstInfo = AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port);
    if (pAstRedPerPortInstInfo == NULL)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_PERPORT_INST_INFO_MEMPOOL_ID,
                                 pAstRedPerPortInstInfo,
                                 tAstRedPerPortInstInfo);

        if (pAstRedPerPortInstInfo == NULL)
        {
            AST_TRC (ALL_FAILURE_TRC, "MemBlock Allocation FAILED.\n");
            return RST_FAILURE;
        }

        AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port) = pAstRedPerPortInstInfo;

        pAstRedPerPortInstInfo = NULL;
    }
#ifdef PVRST_WANTED
    MEMSET (AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port), AST_INIT_VAL,
            AST_RED_PERPORT_INST_INFO_MEMBLK_SIZE * AST_MAX_PVRST_INSTANCES);
#else
    MEMSET (AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port), AST_INIT_VAL,
            AST_RED_PERPORT_INST_INFO_MEMBLK_SIZE * AST_MAX_MST_INSTANCES);
#endif

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedDeletePortAndPerPortInst                       */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                      PortEntry and perportInst Array                      */
/*                                                                           */
/* Input(s)           : u2Port - Local PortNumber                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedDeletePortAndPerPortInst (UINT2 u2Port)
{
    tAstRedPerPortInstInfo *pAstRedPerPortInstInfo = NULL;

    pAstRedPerPortInstInfo = AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port);

    if (pAstRedPerPortInstInfo != NULL)
    {
        AST_FREE_RED_MEM_BLOCK (AST_RED_PERPORT_INST_INFO_MEMPOOL_ID,
                                pAstRedPerPortInstInfo);

        AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port) = NULL;
        pAstRedPerPortInstInfo = NULL;
    }

    if (AST_RED_CONTEXT_PORT_INFO (u2Port) != NULL)
    {
        AST_FREE_RED_MEM_BLOCK (AST_RED_PORT_INFO_MEMPOOL_ID,
                                AST_RED_CONTEXT_PORT_INFO (u2Port));

        AST_RED_CONTEXT_PORT_INFO (u2Port) = NULL;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleRcvdInfoWhileTmrExp                      */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - If the timer exp count >=                     */
/*                                  AST_NUM_TMR_INTERVAL_SPLITS, treat it as */
/*                                  Receive info while timer expiry.         */
/*                             Else                                          */
/*                                  Calls Update receive info while fn.      */
/*                      for given port                                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to port information         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
VOID
AstRedHandleRcvdInfoWhileTmrExp (tAstPerStPortInfo * pPerStPortInfo)
{

    UINT2               u2InstanceId = AST_INIT_VAL;

    u2InstanceId = pPerStPortInfo->u2Inst;

    pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount++;
    if (pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount >=
        AST_NUM_TMR_INTERVAL_SPLITS)
    {
        AST_DBG (AST_RED_DBG, "RvcdInfoWhile Timer Expiry Handling\n");
        if (AST_IS_RST_ENABLED ())
        {
            RstPortInfoMachine ((UINT2)
                                RST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                                pPerStPortInfo, NULL);
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {

            MstPortInfoMachine (MST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                                pPerStPortInfo, NULL, u2InstanceId);
        }
#endif
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            PvrstPortInfoMachine (PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                                  pPerStPortInfo, NULL);
        }
#endif
        pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount = 0;
    }
    else
    {
        if (AST_IS_RST_ENABLED ())
        {
            RstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo);
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            PvrstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo);
        }
#endif
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            MstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo, NULL, u2InstanceId);
        }
#endif
    }

    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : AstRedHRProcStdyStPktReq                             */
/*                                                                           */
/* Description        : This function is called whenever there is a steady   */
/*                      state packet request from RM                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRProcStdyStPktReq (VOID)
{
    UINT4               u4ContextId = 0;
    UINT1               u1TailFlag = 0;
    UINT4               u4NextContextId = 0;

    /* If the Module Status is disabled, send Steady State Tail Msg and move to
     * the next module */

#if !defined (RSTP_WANTED) && defined (MSTP_WANTED)
    if (AST_IS_MST_ENABLED () == AST_FALSE)
    {
        AstRedHRSendStdyStTailMsg ();
        return;
    }
#elif defined (RSTP_WANTED) && !defined (MSTP_WANTED)
    if (AST_IS_RST_ENABLED () == AST_FALSE)
    {
        AstRedHRSendStdyStTailMsg ();
        return;
    }
#elif defined (RSTP_WANTED) && defined (MSTP_WANTED)
    if ((AST_IS_MST_ENABLED () == AST_FALSE)
        && (AST_IS_RST_ENABLED () == AST_FALSE))
    {
        AstRedHRSendStdyStTailMsg ();
        return;
    }
#endif
    AST_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;
    /* Get the first Active Context */
    if (AstGetFirstActiveContext (&u4NextContextId) == RST_FAILURE)
    {
        return;
    }

    /* Loop through the contexts */
    do
    {
        u4ContextId = u4NextContextId;
        AstRedHRSetSSPTmrValInCxt (u4ContextId, &u1TailFlag);

    }
    while (AstGetNextActiveContext (u4ContextId, &u4NextContextId) !=
           RST_FAILURE);
    if (u1TailFlag != AST_HR_STDY_ST_PKT_TAIL)
    {
        AstRedHRSendStdyStTailMsg ();
    }
}

/*****************************************************************************/
/* Function Name      : AstRedHRSetSSPTmrValInCxt                            */
/*                                                                           */
/* Description        : This function loops through the ports in the context */
/*                      and calls the function AstRedHRRestartTmrForSSP to   */
/*                      restart the timer                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRSetSSPTmrValInCxt (UINT4 u4ContextId, UINT1 *u1TailFlag)
{
    INT4                i4IfIndex = 0;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pAstNextPortEntry = NULL;

    /* Get the first Active port in the context */
    if (AstGetFirstPortInContext (u4ContextId, &i4IfIndex) == RST_FAILURE)
    {
        return;
    }

    /* Get the port entry structure for the port */
    pAstNextPortEntry = AstGetIfIndexEntry (i4IfIndex);

    if (pAstNextPortEntry == NULL)
    {
        return;
    }

    do
    {
        pAstPortEntry = pAstNextPortEntry;
        if ((pAstPortEntry->CommPortInfo.pHelloWhenTmr) != NULL)
        {
            AstRedHRRestartTmrForSSP (pAstPortEntry);
            if (*u1TailFlag == 0)
            {
                *u1TailFlag = AST_HR_STDY_ST_PKT_TAIL;
            }
        }
    }
    while ((pAstNextPortEntry =
            AstGetNextIfIndexEntry (pAstPortEntry)) != NULL);

}

/*****************************************************************************/
/* Function Name      : AstRedHRRestartTmrForSSP                             */
/*                                                                           */
/* Description        : This function stops the timer for each port if it is */
/*                      running and restart it with zero duration to trigger */
/*                      a steady state packet to RM                          */
/*                                                                           */
/* Input(s)           : pAstPortEntry - Pointer to port entry structure.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRRestartTmrForSSP (tAstPortEntry * pAstPortEntry)
{
    /* Check if the Timer is running for the port */
    if (pAstPortEntry->CommPortInfo.pHelloWhenTmr->u1IsTmrStarted == RST_TRUE)
    {
        if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                            &(pAstPortEntry->CommPortInfo.pHelloWhenTmr->
                              AstAppTimer)) == AST_TMR_FAILURE)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "AstRedHRSSPRestartTimer: Stopping Hello When  Timer "
                     "FAILED!\n");
            return;
        }

        /* Restart the Timer with 0 duration  */
        if (AST_START_TIMER (AST_TMR_LIST_ID,
                             &(pAstPortEntry->CommPortInfo.pHelloWhenTmr->
                               AstAppTimer), 0) == AST_TMR_FAILURE)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "RstpMstpRedHrSSPRestartTimer: Starting Hello When  Timer FAILED!\n");

        }

    }
}

/*****************************************************************************/
/* Function Name      : AstRedHRSendStdyStPkt                                */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/
INT1
AstRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pPkt, UINT4 u4PktLen,
                       UINT2 u2Port, UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT4               u4BufSize = 0;

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        AST_TRC (AST_INIT_SHUT_TRC, "Steady state packet cannot be sent from "
                 "Standby node\r\n");
        return RST_SUCCESS;
    }

    /* Forming  steady state packet

       <- 24B --><--------- 1B ---------><-- 2B -->< 2B -><-- 4B --><STP Pkt len>
       _________________________________________________________________________
       |        |                        |         |      |         |           | 
       | RM Hdr | AST_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer    |
       |________|________________________|_________|______|_________|___________|

       * The RM Hdr shall be included by RM.
     */

    u4BufSize = AST_RED_MSG_TYPE_SIZE + AST_RED_MSG_LEN_SIZE + sizeof (UINT2)    /* For port */
        + sizeof (UINT4)        /* For TimeOut */
        + u4PktLen;                /* Packet Length */

    if ((pMsg = RM_ALLOC_TX_BUF (u4BufSize)) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_OS_RESOURCE_TRC, "STP: RM Memory "
                 "Allocation failed!!\r\n");
        return RST_FAILURE;
    }

    /* Fill the message type. */
    u4MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    AST_RM_PUT_1_BYTE (pMsg, &u4Offset, AST_HR_STDY_ST_PKT_MSG);
    AST_RM_PUT_2_BYTE (pMsg, &u4Offset, (UINT2) u4MsgLen);
    AST_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);
    AST_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimeOut);

    CRU_BUF_Concat_MsgBufChains (pMsg, pPkt);

    if (AstRedSendUpdateToRM (pMsg, (UINT2) u4MsgLen) == RST_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC, "STP: steady state packet to RM "
                 "failed\r\n");
        return (RST_FAILURE);
    }

    AST_TRC (AST_INIT_SHUT_TRC, "STP: Steady state packet sent to RM\r\n");
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstRedHRSendStdyStTailMsg                            */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/

INT1
AstRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2BufSize = 0;

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        AST_TRC (AST_INIT_SHUT_TRC,
                 "STP: Steady state packet cannot be sent from "
                 "Standby node\r\n");
        return RST_SUCCESS;
    }

    u2BufSize = AST_RED_MSG_TYPE_SIZE + AST_RED_MSG_LEN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
       by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC, "STP: RM alloc failed\n");
        return (RST_FAILURE);
    }

    /* Form a steady state tail message.

       <--------1 Byte--------> <---2 Byte--->
       _________________________________________________
       |        |                         |             |
       | RM Hdr | AST_HR_STDY_ST_PKT_TAIL | Msg Length  |
       |________|________________________ |_____________|

       The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    AST_RM_PUT_1_BYTE (pMsg, &u4Offset, AST_HR_STDY_ST_PKT_TAIL);
    AST_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (AstRedSendUpdateToRM (pMsg, u2BufSize) == RST_FAILURE)
    {

        AST_TRC (AST_ALL_FAILURE_TRC,
                 "STP: steady state tail message sending is " "failed\n");
        return (RST_FAILURE);
    }
    return RST_SUCCESS;
}

/******************************************************************************
 * Function           : AstRedGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/

UINT1
AstRedGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;

    u1HRFlag = RmGetHRFlag ();

    return (u1HRFlag);
}

#endif /* L2RED_WANTED */
