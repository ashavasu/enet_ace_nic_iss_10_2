/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpmsg.c,v 1.187 2018/01/30 09:25:15 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Message Processing Module which includes enqueued
 *              BPDUs and local SNMP messages and Task Initilisation
 *              Routines.
 *
 *******************************************************************/

#define _ASTPMSG_C_

#include "asthdrs.h"
#ifdef PVRST_WANTED
#include "astvinc.h"
#endif
#ifdef MSTP_WANTED
#include "astmred.h"
#endif
#include "mux.h"

/*****************************************************************************/
/* Function Name      : AstInit                                              */
/*                                                                           */
/* Description        : This function is called during system startup.       */
/*                      This initialises the important protocol variables    */
/*                      pertaining to the RSTP/MSTP Module and invokes the   */
/*                      the routine to spawn the ASTP task.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : BridgeInitializeProtocol                             */
/*****************************************************************************/
INT4
AstInit (VOID)
{
    /* Initialize protocol memory 
     * 1) Create mempools for protocol data structures like 
     *    Port entry, PerStPort entry, PerSt info etc. (for MI alone)
     * 2) Create the default context (for both SI and MI)
     */
    AST_ASYNC_MODE () = AstIssGetAsyncMode (L2_PROTO_STP);
    if (AstGlobalMemoryInit () != RST_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                        AST_MGMT_TRC,
                        "STAP: ASTP Task Initialization FAILED!!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MGMT_DBG |
                        AST_ALL_FAILURE_DBG,
                        "STAP: ASTP Task Initialization FAILED!!!\n");
        return RST_FAILURE;
    }

    gAstGlobalInfo.i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "AST", SYSLOG_ALERT_LEVEL);

    if (gAstGlobalInfo.i4SysLogId <= 0)
    {
        AST_DBG (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "Registration with Syslog FAILED\n");
        return RST_FAILURE;
    }

    AstRedRmInit ();

    if (AstRedRegisterWithRM () != RST_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                        "MSG: AstRedRmInit Failed !!!\n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                        "MSG:AST registration with RM failed !!!\n");
        return RST_FAILURE;
    }

    /* This will be called when the module is initializes.
     * From Ast Start module*/
    if (AstRegisterWithPacketHandler () != RST_SUCCESS)
    {
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                        "MSG:AST registration with Packet Handler failed !!!\n");

        return RST_FAILURE;
    }

    /* Cfa has already read the Default Vlan Id read from issnvram.txt
     * and updated L2Iwf.AST gets the default vlan id from L2Iwf.
     */
    AstL2IwfGetDefaultVlanId (&(AST_DEF_VLAN_ID ()));

    AST_IS_INITIALISED () = RST_TRUE;
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstTaskInit                                          */
/*                                                                           */
/* Description        : This function initialises the AST Module. It spawns  */
/*                      the AST Task, creates the AST Queue and creates the  */
/*                      Local Message Memory Pool.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstTaskInit (VOID)
{
    UINT4               u4RetVal = 0xffff;

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_INIT_SHUT_TRC,
                    "MSG: Spawning AST Task...\n");
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_INIT_SHUT_DBG,
                    "MSG: Spawning AST Task...\n");

    AST_MEMSET (&gAstGlobalInfo, AST_INIT_VAL, sizeof (tAstGlobalInfo));
    AST_MEMSET (gAstGlobalInfo.gau1AstBpdu, AST_INIT_VAL,
                AST_MAX_ETH_FRAME_SIZE);
    AST_MEMSET (gAstGlobalInfo.gau1PseudoRstBpdu, AST_INIT_VAL,
                AST_PSEUDO_BPDU_SIZE);
#ifdef MSTP_WANTED
    AST_MEMSET (gAstGlobalInfo.gau1PseudoMstBpdu, AST_INIT_VAL,
                MST_PSEUDO_BPDU_SIZE);
#endif

    if (AST_CREATE_SEM (AST_MUT_EXCL_SEM_NAME, AST_SEM_INIT_COUNT, 0,
                        &gAstGlobalInfo.SemId) != AST_OSIX_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Failed to Create AST Mutual Exclusion Sema4 \n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_MEM_DBG | AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Failed to Create AST Mutual Exclusion Sema4 \n");
        return RST_FAILURE;
    }

    if (AstSizingMemCreateMemPools () != AST_OSIX_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Failed To Create Mempools in AST  \n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Failed To Create Mempools in AST  \n");
        return RST_FAILURE;
    }

    /* Assign MempoolIds created in sz.c to global MempoolIds */
    AstAssignMempoolIds ();

    if ((u4RetVal = AST_CREATE_QUEUE (AST_TASK_INPUT_QNAME,
                                      OSIX_MAX_Q_MSG_LEN,
                                      AST_QUEUE_DEPTH,
                                      &(AST_INPUT_QID))) != AST_OSIX_SUCCESS)

    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Failed To Create Message AST Module Input Queue \n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Failed To Create Message AST Module Input Queue \n");
        return RST_FAILURE;
    }

    if ((u4RetVal = AST_CREATE_QUEUE (AST_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                                      AST_QUEUE_DEPTH, &(AST_CFG_QID)))
        != AST_OSIX_SUCCESS)

    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC |
                        AST_ALL_FAILURE_TRC,
                        "MSG: Failed To Create Message AST Module CFG Queue \n");
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Failed To Create Message AST Module CFG Queue \n");
        return RST_FAILURE;
    }

    gAstGlobalInfo.gpu1AstBpdu = gAstGlobalInfo.gau1AstBpdu;
    gAstGlobalInfo.gpPseudoRstBpdu = gAstGlobalInfo.gau1PseudoRstBpdu;
#ifdef MSTP_WANTED
    gAstGlobalInfo.gpPseudoMstBpdu = gAstGlobalInfo.gau1PseudoMstBpdu;
#endif
    /* HITLESS RESTART */
    AST_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstTaskDeinit                                        */
/*                                                                           */
/* Description        : This function de-initialises the AST Module. It      */
/*                      deletes the Local Message Memory Pool, the AST Queue */
/*                      and the AST Task.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstTaskDeinit (VOID)
{
    tAstBufChainHeader *pBpdu = NULL;
    tAstQMsg           *pQMsg = NULL;

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_INIT_SHUT_TRC,
                    "MSG: De-Initializing AST Task module...\n");
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_INIT_SHUT_DBG,
                    "MSG: De-Initializing AST Task module...\n");

    if (AST_INPUT_QID != (tAstQId) AST_INIT_VAL)
    {

        /* Before DeInitializing the task, remove and release 
         * all messages in the Queue */
        while (AST_GET_BPDU_FROM_AST_QUEUE (AST_INPUT_QID, (UINT1 *) &pQMsg,
                                            AST_DEF_MSG_LEN, AST_OSIX_NO_WAIT)
               == AST_OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }
            /* Processing of BPDUs */
            if (AST_QMSG_TYPE (pQMsg) == AST_BPDU_RCVD_QMSG)
            {
                pBpdu = pQMsg->uQMsg.pBpduInQ;
                if (AST_RELEASE_CRU_BUF (pBpdu, RST_FALSE) == AST_CRU_FAILURE)
                {
                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_EVENT_HANDLING_DBG | AST_MEM_DBG |
                                    AST_ALL_FAILURE_DBG,
                                    "MSG: Received CRU Buffer Release FAILED -");
                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_EVENT_HANDLING_DBG | AST_MEM_DBG |
                                    AST_ALL_FAILURE_DBG,
                                    "in AST Task DeInit!\n");
                }
            }

            if (AST_RELEASE_QMSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
            {
                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                "MSG: Release of Local Message Memory Block FAILED!\n");
                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                                AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                                "MSG: Release of Local Message Memory Block FAILED!\n");
            }
        }

        AST_DELETE_QUEUE (AST_INPUT_QID);

        AST_INPUT_QID = (tAstQId) AST_INIT_VAL;

    }

    if (AST_CFG_QID != (tAstQId) AST_INIT_VAL)
    {
        /* Before DeInitializing the task, remove and release 
         * all messages in the Configuration Queue */
        while (AST_RECV_FROM_QUEUE (AST_CFG_QID, (UINT1 *) &pQMsg,
                                    AST_DEF_MSG_LEN, AST_OSIX_NO_WAIT)
               == AST_OSIX_SUCCESS)

        {
            if (pQMsg == NULL)
            {
                continue;
            }

            switch (AST_QMSG_TYPE (pQMsg))
            {
                case AST_SNMP_CONFIG_QMSG:

                    if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                        (pQMsg->uQMsg.pMsgNode) != AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of Local Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Local Message Memory Block FAILED!\n");
                    }

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");
                    }
                    break;

#ifdef L2RED_WANTED
                case AST_RM_QMSG:

                    if ((pQMsg->uQMsg.pRmMsg->u1Event == RM_MESSAGE) &&
                        (pQMsg->uQMsg.pRmMsg->pFrame != NULL))
                    {
                        RM_FREE (pQMsg->uQMsg.pRmMsg->pFrame);
                    }
                    else if (((pQMsg->uQMsg.pRmMsg->u1Event == RM_STANDBY_UP) ||
                              (pQMsg->uQMsg.pRmMsg->u1Event == RM_STANDBY_DOWN))
                             && (pQMsg->uQMsg.pRmMsg->pFrame != NULL))
                    {
                        AstRmReleaseMemoryForMsg ((UINT1 *) pQMsg->uQMsg.
                                                  pRmMsg->pFrame);
                    }

                    if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                        (pQMsg->uQMsg.pRmMsg) != AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of Local Message Memory \
                                 Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Local Message Memory \
                                 Block FAILED!\n");
                    }

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFGQ Message Memory Block "
                                        "FAILED!\n");
                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Q Message Memory Block "
                                        "FAILED!\n");
                    }
                    break;
#endif

#ifdef MBSM_WANTED
                case MBSM_MSG_CARD_INSERT:
                case MBSM_MSG_CARD_REMOVE:

                    MEM_FREE (pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg);

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");
                    }
                    break;
#endif

            }
        }

        AST_DELETE_QUEUE (AST_CFG_QID);

        AST_CFG_QID = (tAstQId) AST_INIT_VAL;
    }

    AstSizingMemDeleteMemPools ();

    AST_DELETE_SEM (gAstGlobalInfo.SemId);
    gAstGlobalInfo.SemId = 0;

    SYS_LOG_DEREGISTER (gAstGlobalInfo.i4SysLogId);
    gAstGlobalInfo.i4SysLogId = 0;
    if (gAstGlobalInfo.gpu1AstBpdu != NULL)
    {
        gAstGlobalInfo.gpu1AstBpdu = NULL;
    }

    if (gAstGlobalInfo.gpPseudoRstBpdu != NULL)
    {
        gAstGlobalInfo.gpPseudoRstBpdu = NULL;
    }

#ifdef MSTP_WANTED
    if (gAstGlobalInfo.gpPseudoMstBpdu != NULL)
    {
        gAstGlobalInfo.gpPseudoMstBpdu = NULL;
    }
#endif

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstModuleInit                                        */
/*                                                                           */
/* Description        : This function will initialise RSTP/MSTP module.      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstModuleInit (VOID)
{
    INT4                i4Mode = AST_INIT_VAL;

    /* Create Sems, Queues, Mempools, the default context and 
     * the default vlan id*/
    if (AstInit () == RST_FAILURE)
    {
        AstGlobalMemoryDeinit ();
        AstRedDeRegisterWithRM ();
        return RST_FAILURE;
    }

    if (AstSelectContext (AST_DEFAULT_CONTEXT) == RST_FAILURE)
    {
        AstGlobalMemoryDeinit ();
        AstRedDeRegisterWithRM ();
        return RST_FAILURE;
    }

    if (AstDeriveOperatingMode (&i4Mode) == RST_FAILURE)
    {
        AstGlobalMemoryDeinit ();
        AstRedDeRegisterWithRM ();
        return RST_FAILURE;
    }

#ifdef MSTP_WANTED
    if (i4Mode == MST_START)
    {
        if (MstModuleInit () != MST_SUCCESS)
        {
            AstReleaseContext ();
            AstGlobalMemoryDeinit ();
            AstRedDeRegisterWithRM ();
            return RST_FAILURE;
        }
    }
    else
#endif
    {
        if (RstModuleInit () != RST_SUCCESS)
        {
            AstReleaseContext ();
            AstGlobalMemoryDeinit ();
            AstRedDeRegisterWithRM ();
            return RST_FAILURE;
        }
    }

    AstReleaseContext ();
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstTaskMain                                          */
/*                                                                           */
/* Description        : This is the main entry point function for the AST    */
/*                      Task. This waits continuously in a loop to receive   */
/*                      events that are posted to this task and it then calls*/
/*                      the corresponding functions to process the events.   */
/*                                                                           */
/* Input(s)           : pi1Param - Pointer to the parameter value that can be*/
/*                                 passed to this task entry point function. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

VOID
AstTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = AST_INIT_VAL;
    tAstQMsg           *pQMsg = NULL;
#ifdef L2RED_WANTED
    VOID               *pRmMsg;
#endif
#ifdef MBSM_WANTED
    INT4                i4ProtoCookie = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    INT4                i4RetVal = RST_FAILURE;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif
    AST_UNUSED (pi1Param);

    if (AstTaskInit () != (INT4) RST_SUCCESS)
    {
        AstTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (AstModuleInit () != (INT4) RST_SUCCESS)
    {
        AstTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (AST_GET_TASK_ID (AST_SELF, AST_TASK_NAME, &(AST_TASK_ID)) !=
        AST_OSIX_SUCCESS)
    {
        AstGlobalMemoryDeinit ();
        AstTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    STPRegisterMIBS ();
#endif

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_INIT_SHUT_TRC,
                    "MSG: AST Task context running ...\n");
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_INIT_SHUT_DBG,
                    "MSG: AST Task context running ...\n");

    while (RST_TRUE)
    {
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_EVENT_HANDLING_DBG,
                        "MSG: Waiting for an Event to be received ...\n");

        if (AST_RECEIVE_EVENT (AST_TASK_ID, AST_BPDU_EVENT | AST_MSG_EVENT |
                               AST_TMR_EXPIRY_EVENT | AST_RED_BULK_UPD_EVENT,
                               (UINT4) OSIX_WAIT,
                               &u4Events) == AST_OSIX_SUCCESS)

        {
            if (u4Events & AST_BPDU_EVENT)
            {
                while (AST_GET_BPDU_FROM_AST_QUEUE (AST_INPUT_QID,
                                                    (UINT1 *) &pQMsg,
                                                    AST_DEF_MSG_LEN,
                                                    AST_OSIX_NO_WAIT) ==
                       AST_OSIX_SUCCESS)
                {
                    /* Processing of BPDUs */
                    if ((pQMsg == NULL) ||
                        (AST_QMSG_TYPE (pQMsg) != AST_BPDU_RCVD_QMSG))
                    {
                        continue;
                    }

                    AST_LOCK ();

                    AstInitializeTxBuf ();
                    AstHandleBpduMsg (pQMsg->uQMsg.pBpduInQ);
                    AstTransmitMstBpdu ();

                    AST_UNLOCK ();

                    if (AST_RELEASE_QMSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of Local Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_INIT_SHUT_DBG |
                                        AST_EVENT_HANDLING_DBG |
                                        AST_ALL_FAILURE_DBG,
                                        "MSG: Release of Local Message Memory "
                                        "Block FAILED!\n");
                    }

                    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_INIT_SHUT_TRC,
                                    "MSG: BPDU Received Event processed.\n");
                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_BPDU_DBG | AST_EVENT_HANDLING_DBG,
                                    "MSG: BPDU Received Event processed.\n");
                }
            }

            if (u4Events & AST_MSG_EVENT)
            {
                while (AST_RECV_FROM_QUEUE (AST_CFG_QID, (UINT1 *) &pQMsg,
                                            AST_DEF_MSG_LEN, AST_OSIX_NO_WAIT)
                       == AST_OSIX_SUCCESS)
                {
                    switch (AST_QMSG_TYPE (pQMsg))
                    {
                        case AST_SNMP_CONFIG_QMSG:

                            /* Processing of Local Messages */
                            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                            AST_MGMT_DBG |
                                            AST_EVENT_HANDLING_DBG,
                                            "MSG: SNMP CONFIG Event received ...\n");

                            AST_LOCK ();
                            AstInitializeTxBuf ();
                            AstHandleSnmpCfgMsg (pQMsg->uQMsg.pMsgNode);
                            AstTransmitMstBpdu ();

                            AST_UNLOCK ();

                            if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                                (pQMsg->uQMsg.pMsgNode) != AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of Local Message Memory "
                                                "Block FAILED!\n");

                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of Local Message Memory "
                                                "Block FAILED!\n");
                            }

                            if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of Q Message Memory "
                                                "Block FAILED!\n");

                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of Q Message Memory "
                                                "Block FAILED!\n");
                            }

                            AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                            AST_CONTROL_PATH_TRC,
                                            "MSG: Message Received Event processed.\n");
                            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                            AST_EVENT_HANDLING_DBG |
                                            AST_MGMT_DBG,
                                            "MSG: Message Received Event processed.\n");
                            break;

#ifdef L2RED_WANTED
                        case AST_RM_QMSG:

                            AST_LOCK ();
                            pRmMsg = pQMsg->uQMsg.pRmMsg;
                            AstProcessRmEvent (pRmMsg);
                            AST_UNLOCK ();

                            AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                            AST_CONTROL_PATH_TRC,
                                            "MSG: Message Received Event processed.\n");
                            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                            AST_EVENT_HANDLING_DBG |
                                            AST_MGMT_DBG,
                                            "MSG: Message Received Event processed.\n");

                            if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                                (pQMsg->uQMsg.pRmMsg) != AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of Local Message Memory \
                                 Block FAILED!\n");

                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of Local Message Memory \
                                         Block FAILED!\n");
                            }

                            if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of CFGQ Message Memory Block "
                                                "FAILED!\n");
                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of CFGQ Message Memory Block  "
                                                "FAILED!\n");
                            }
                            break;
#endif

#ifdef MBSM_WANTED
                        case MBSM_MSG_CARD_INSERT:
                            i4ProtoCookie =
                                pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg->
                                i4ProtoCookie;
                            pSlotInfo =
                                &(pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmSlotInfo);
                            pPortInfo =
                                &(pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmPortInfo);

                            AST_LOCK ();
                            AstInitializeTxBuf ();
                            i4RetVal = AstMbsmUpdateCardInsertion
                                (pPortInfo, pSlotInfo);
                            AstTransmitMstBpdu ();
                            AST_UNLOCK ();

                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                            MbsmSendAckFromProto (&MbsmProtoAckMsg);

                            MEM_FREE (pQMsg->uQMsg.MbsmCardUpdate.
                                      pMbsmProtoMsg);

                            if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of CFGQ Message Memory Block "
                                                "FAILED!\n");
                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of Q Message Memory Block "
                                                "FAILED!\n");
                            }
                            break;

                        case MBSM_MSG_CARD_REMOVE:
                            i4ProtoCookie =
                                pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg->
                                i4ProtoCookie;
                            pSlotInfo =
                                &(pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmSlotInfo);
                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                            MbsmSendAckFromProto (&MbsmProtoAckMsg);

                            MEM_FREE (pQMsg->uQMsg.MbsmCardUpdate.
                                      pMbsmProtoMsg);

                            if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                AST_MEM_SUCCESS)
                            {
                                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_TRC |
                                                AST_MGMT_TRC,
                                                "MSG: Release of CFGQ Message Memory Block "
                                                "FAILED!\n");
                                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                                AST_ALL_FAILURE_DBG |
                                                AST_MGMT_DBG |
                                                AST_EVENT_HANDLING_DBG,
                                                "MSG: Release of Q Message Memory Block "
                                                "FAILED!\n");
                            }

                            break;
#endif
                    }
                }
            }

            /****************************************************/
            /*              AST_TMR_EXPIRY_EVENT                */
            /****************************************************/
            if (u4Events & AST_TMR_EXPIRY_EVENT)
            {
                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                                AST_TMR_DBG,
                                "MSG: Timer Expiry event obtained ...\n");

                AST_LOCK ();
                AstInitializeTxBuf ();
                if (AstTmrExpiryHandler () != RST_SUCCESS)
                {
                    AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                    AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: RstTmrExpiryHandler function "
                                    "returned FAILURE!\n");

                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_TMR_DBG | AST_EVENT_HANDLING_DBG |
                                    AST_ALL_FAILURE_DBG,
                                    "MSG: RstTmrExpiryHandler function "
                                    "returned FAILURE!\n");
                }
                AstTransmitMstBpdu ();
                AST_UNLOCK ();

                AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_CONTROL_PATH_TRC,
                                "MSG: Timer Expiry Event processed...\n");
                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                AST_TMR_DBG | AST_EVENT_HANDLING_DBG |
                                AST_EVENT_HANDLING_DBG,
                                "MSG: Timer Expiry Event processed...\n");
            }

#ifdef L2RED_WANTED
            if (u4Events & AST_RED_BULK_UPD_EVENT)
            {
                AST_LOCK ();
                AstRedHandleBulkUpdateEvent ();
                AST_UNLOCK ();
            }
#endif

            AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_EVENT_HANDLING_DBG,
                            "MSG: Completed processing the event(s).\n");

        }                        /* End of Receive Event If Loop */
    }

}

VOID
AstHandleBpduMsg (tAstBufChainHeader * pBpdu)
{
    tAstInterface       IfaceId;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT2               u2TmpLength = 0;

    IfaceId = AST_BUF_GET_INTERFACEID (pBpdu);

    u4IfIndex = (UINT2) IfaceId.u4IfIndex;

    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        /* Select the context to which the port belongs */
        if (AstSelectContext (u4ContextId) != RST_FAILURE)
        {
            u2TmpLength = (UINT2) AST_BUF_GET_CHAIN_VALIDBYTECOUNT (pBpdu);

            AST_DBG (AST_EVENT_HANDLING_DBG | AST_BPDU_DBG,
                     "MSG: BPDU RCVD Event received ...\n");
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Valid Bytes in Rxd BPDU: %u \n", u2TmpLength);

            /* Checking Global Module status, If the Module status is disabled,
             * the discard the BPDU*/
            if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_ENABLED) ||
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED) ||
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_ENABLED))
            {

                if (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPortId) == RST_TRUE)
                {
                    /* If the Port is CEP, then Switch to CVLAN context */
                    if (AstPbSwitchCVlanContext (&IfaceId, &u2LocalPortId)
                        == RST_SUCCESS)
                    {
                        /* Check whether the CVLAN module status is enabled for
                         * processing the STP BPDU*/
                        if (AST_IS_RST_ENABLED ())
                        {
                            if (AstReceivedBpdu (pBpdu, u2LocalPortId) !=
                                RST_SUCCESS)
                            {
                                AST_TRC (AST_CONTROL_PATH_TRC |
                                         AST_ALL_FAILURE_TRC,
                                         "MSG: AstReceived BPDU returned "
                                         "failure. DISCARDING BPDU !!!\n");

                                AST_DBG (AST_BPDU_DBG |
                                         AST_EVENT_HANDLING_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "MSG: AstReceivedBpdu function "
                                         "returned FAILURE\n");

                                AST_DBG (AST_BPDU_DBG |
                                         AST_EVENT_HANDLING_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "MSG: DISCARDING BPDU !!!\n");
                            }
                            /* Once the received BPDU is processed, reset the BPDU received port */
                            gu2AstRecvPort = AST_INIT_VAL;
                        }
                        /* Restoring the Context */
                        AstPbRestoreContext ();
                    }

                }
                else
                {
                    /* Check for SVLAN Module Status for BPDU Processing */
                    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ())
                        || (AST_IS_PVRST_ENABLED ()))
                    {
                        if (AstReceivedBpdu (pBpdu, u2LocalPortId) !=
                            RST_SUCCESS)
                        {
                            AST_TRC (AST_CONTROL_PATH_TRC |
                                     AST_ALL_FAILURE_TRC,
                                     "MSG: AstReceived BPDU returned "
                                     "failure. DISCARDING BPDU !!!\n");

                            AST_DBG (AST_BPDU_DBG |
                                     AST_EVENT_HANDLING_DBG |
                                     AST_ALL_FAILURE_DBG,
                                     "MSG: AstReceivedBpdu function "
                                     "returned FAILURE\n");

                            AST_DBG (AST_BPDU_DBG |
                                     AST_EVENT_HANDLING_DBG |
                                     AST_ALL_FAILURE_DBG,
                                     "MSG: DISCARDING BPDU !!!\n");
                        }
                        /* Once the received BPDU is processed, reset the BPDU received port */
                        gu2AstRecvPort = AST_INIT_VAL;
                    }
                }
            }
            else
            {
                AST_DBG (AST_INIT_SHUT_DBG | AST_BPDU_DBG |
                         AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: RSTP/MSTP/PVRST Module is Not Enabled "
                         "Cannot Process Bpdu\n");
            }

            AstReleaseContext ();
        }
    }

    if (AST_RELEASE_CRU_BUF (pBpdu, RST_FALSE) == AST_CRU_FAILURE)
    {
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG |
                        AST_EVENT_HANDLING_DBG,
                        "MSG: Received BPDU CRU Buffer Release " "FAILED!\n");
    }

}

VOID
AstHandleSnmpCfgMsg (tAstMsgNode * pMsgNode)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId;

    u4ContextId = pMsgNode->u4ContextId;

    /* Assumption: All config messages have the context ID passed
     *             to them */

    if (pMsgNode->MsgType == AST_CREATE_CONTEXT_MSG)
    {
        if (AstHandleCreateContext (pMsgNode->u4ContextId) == RST_FAILURE)
        {
            AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                            AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                            "MSG: AstHandleCreateContext "
                            "function returned FAILURE!\n");
        }

        L2MI_SYNC_GIVE_SEM ();
    }
    else if (AstSelectContext (u4ContextId) != RST_FAILURE)
    {
        /* 
         * If the message is NOT a create or map port message
         * and if it has a valid IfIndex, then the IfIndex
         * should be converted to the Local port number in
         * the message
         */
        if (!((pMsgNode->MsgType == AST_CREATE_PORT_MSG) ||
              (pMsgNode->MsgType == AST_MAP_PORT_MSG)))
        {
            if (pMsgNode->u4PortNo != AST_MSG_INVALID_PORT)
            {
                pAstPortEntry = AstGetIfIndexEntry (pMsgNode->u4PortNo);

                if (NULL != pAstPortEntry)
                {
                    /* Overwrite the Global IfIndex in the message 
                     * with the corresponding LocalPort ID */
                    pMsgNode->u4PortNo = pAstPortEntry->u2PortNo;
                }
                else
                {
                    /* Should reach here only if the module is shutdown */
                    if (AST_IS_RST_STARTED () || AST_IS_MST_STARTED ()
                        || AST_IS_PVRST_STARTED ())
                    {
                        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG
                                 | AST_EVENT_HANDLING_DBG,
                                 "MSG: Unknown port received in message !!\n");

                        AstDefaultCfgMsgHandler (pMsgNode);

                        AstReleaseContext ();
                        return;
                    }
                }
            }
        }

        AstHandleLocalMsgReceived (pMsgNode);
        AstReleaseContext ();
    }
    else
    {
        AstDefaultCfgMsgHandler (pMsgNode);
    }
}

/*****************************************************************************/
/* Function Name      : AstReceivedBpdu                                      */
/*                                                                           */
/* Description        : This handles the received Bpdu and hands over the    */
/*                      Bpdu to the respective module depending on which     */
/*                      module is enabled.                                   */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the CRU Buffer of the received     */
/*                             Bpdu                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstReceivedBpdu (tAstBufChainHeader * pBuf, UINT2 u2PortNum)
{
    tCfaIfInfo          IfInfo;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstInterface       IfaceId;
    tAstBpduType       *pAstBpduType = NULL;
    UINT4               u4Offset = AST_INIT_VAL;
    UINT4               u4ByteCount = AST_INIT_VAL;
    UINT2               u2DataLength = AST_INIT_VAL;
    UINT1              *pu1Bpdu = NULL;
    UINT1               u1IfType;

#ifdef PVRST_WANTED
    /* Defining ISL Destination Addresses */
    tMacAddr            BrgPvrstResvAddr1 = { 0x01, 0x00, 0x0C, 0x00, 0x00 };
    tMacAddr            BrgPvrstResvAddr2 = { 0x03, 0x00, 0x0C, 0x00, 0x00 };
    tMacAddr            BrgRstpDestAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    tMacAddr            DestAddr;
    tVlanId             PVID = AST_INIT_VAL;
    UINT2               u2VlanTag = AST_INIT_VAL;
    UINT2               u2LLCHeader = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType;
    UINT1               u1RcvdRstp = AST_FALSE;
#endif
    /* Assumptions: 
     * 1) STP Lock already taken.
     * 2) Appropriate context already selected. 
     * 3) u2PortNum is the Local PerContext port ID 
     * */

#ifdef RSTP_TRACE_WANTED
    UINT2               u2TmpLength = 0;
#endif

#ifdef RSTP_TRACE_WANTED
    u2TmpLength = (UINT2) AST_BUF_GET_CHAIN_VALIDBYTECOUNT (pBuf);

    AST_TRC (AST_CONTROL_PATH_TRC, "MSG: Receiving BPDU ...\n");
    AST_DBG (AST_BPDU_DBG, "MSG: Receiving BPDU ...\n");
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "Valid Bytes in Rxd Packet: %u \n", u2TmpLength);
    AST_PKT_DUMP (AST_DUMP_TRC, pBuf, u2TmpLength,
                  ">************ Dumping received frame **********<\n");
    AST_TRC (AST_CONTROL_PATH_TRC,
             ">*******************************************************<\n");
#endif
    AST_MEMSET (&IfaceId, AST_INIT_VAL, sizeof (tAstInterface));

    gu2AstRecvPort = u2PortNum;    /* Port in which current BPDU is received */

    IfaceId = AST_BUF_GET_INTERFACEID (pBuf);

    u1IfType = IfaceId.u1_InterfaceType;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /* Modified for Attachment Circuit interface. Check it */
    if (AstCfaGetIfInfo (IfaceId.u4IfIndex, &IfInfo) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    if (u2PortNum == AST_INVALID_PORT_NUM)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "Not processing BPDU due to invalid Port in the packet\n");
        AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                 "Not processing BPDU due to invalid Port in the packet\n");
        return RST_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pPortEntry == NULL)
    {
        AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                      "Port %u DOES NOT exist...Discarding BPDU\n", u2PortNum);
        return RST_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if ((pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN)
        && ((pPortEntry->bPVIDInconsistent != AST_TRUE)
            && (pPortEntry->bPTypeInconsistent != AST_TRUE)))
    {
        if (!AST_IS_PVRST_STARTED ())
        {
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Port %s Not Enabled...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_SUCCESS;
        }
    }

    if (!AST_IS_PVRST_STARTED ())
    {
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
        if (pPerStPortInfo == NULL)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "Port %s DOES NOT exist in default instance context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "Port %s DOES NOT exist in default instance context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }

        pPerStRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

        if (pPerStRstPortInfo->bPortEnabled == RST_FALSE)
        {
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Port %s Not Enabled...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_SUCCESS;
        }
    }
    if (AST_ALLOC_BPDU_TYPE_MEM_BLOCK (pAstBpduType) == NULL)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "Memory Allocation Failed \n\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pAstBpduType, AST_MEMSET_VAL, sizeof (tAstBpduType));

    /* Modified for Attachmen Circuit interface */
    if ((u1IfType == AST_ENET_PHYS_INTERFACE_TYPE) ||
        (u1IfType == AST_LAGG_INTERFACE_TYPE) ||
        (u1IfType == AST_BRIDGED_INTERFACE) ||
        (u1IfType == AST_PSEUDO_WIRE) ||
        ((u1IfType == AST_PROP_VIRTUAL_INTERFACE) &&
         (IfInfo.u1IfSubType == AST_ATTACHMENT_CIRCUIT_INTERFACE)))

    {
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_STARTED ())
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0,
                                       ETHERNET_ADDR_SIZE);
            /*Check For ISL Dest Mac Address */
            if ((MEMCMP (DestAddr, BrgPvrstResvAddr1, (ETHERNET_ADDR_SIZE - 1))
                 == 0)
                ||
                (MEMCMP (DestAddr, BrgPvrstResvAddr2, (ETHERNET_ADDR_SIZE - 1))
                 == 0))
            {
                /* ISL Encapsulated packet recieved,check if encapsulation 
                   configured is ISL if no then discard PDU */
                if ((AST_GET_COMM_PORT_INFO (u2PortNum))->eEncapType !=
                    PVRST_ISL)
                {
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }

                pAstBpduType->uBpdu.PvrstBpdu.u1IsPvrstPDU = AST_TRUE;
                /*Extract VLan ID Here */
                if (AST_COPY_FROM_CRU_BUF
                    (pBuf, (UINT1 *) &u2VlanTag, AST_PVRST_VLAN_OFFSET,
                     2) == AST_CRU_FAILURE)
                {
                    AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: Copy From Received BPDU CRU Buffer FAILED!\n");
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }
                u2VlanTag = (UINT2) AST_NTOHS (u2VlanTag);
                u2VlanTag = (UINT2) (u2VlanTag >> 1);
                pAstBpduType->uBpdu.PvrstBpdu.VlanId = u2VlanTag;

                /* Skipping the ISL Header fields */
                AST_BUF_MOVE_VALID_OFFSET (pBuf, ISL_HEADER_SIZE);
            }
        }

#endif
        /* Skipping the Destination and Source Address fields to extract the
         * Length field */
        AST_BUF_MOVE_VALID_OFFSET (pBuf, AST_ENET_DEST_SRC_ADDR_SIZE);

        /* Copying the Length field */
        u4Offset = 0;
        if (AST_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u2DataLength, u4Offset, 2)
            == AST_CRU_FAILURE)
        {
            AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: Copy From Received BPDU CRU Buffer FAILED!\n");
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_FAILURE;
        }
        u2DataLength = (UINT2) AST_NTOHS (u2DataLength);
        if (u2DataLength == VLAN_PROTOCOL_ID)
        {
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                /* Dot1q Encapsulated packet recieved,check if encapsulation 
                   configured is Dot1q if no then discard PDU */
                if ((AST_GET_COMM_PORT_INFO (u2PortNum))->eEncapType !=
                    PVRST_DOT1Q)
                {
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }

                u4Offset += 2;
                if (AST_COPY_FROM_CRU_BUF
                    (pBuf, (UINT1 *) &u2VlanTag, u4Offset,
                     2) == AST_CRU_FAILURE)
                {
                    AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: Copy From Received BPDU CRU Buffer FAILED!\n");
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }
                u2VlanTag = (UINT2) AST_NTOHS (u2VlanTag);
                u2VlanTag &= VLAN_ID_MASK;
                pAstBpduType->uBpdu.PvrstBpdu.u1IsPvrstPDU = AST_TRUE;
                pAstBpduType->uBpdu.PvrstBpdu.VlanId = u2VlanTag;
                u4Offset += 2;
                /* Now skipping Vlan Type and tag Field */
                AST_BUF_MOVE_VALID_OFFSET (pBuf, u4Offset);
                u4Offset = 0;
                if (AST_COPY_FROM_CRU_BUF
                    (pBuf, (UINT1 *) &u2DataLength, u4Offset,
                     2) == AST_CRU_FAILURE)
                {
                    AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: Copy From Received BPDU CRU Buffer FAILED!\n");
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }
                u2DataLength = (UINT2) AST_NTOHS (u2DataLength);

                u1RcvdRstp = AST_FALSE;
                AstL2IwfGetVlanPortType ((UINT2) pPortEntry->u4IfIndex,
                                         &u1PortType);
                AstL2IwfGetVlanPortPvid ((UINT2) pPortEntry->u4IfIndex,
                                         &(PVID));
            }
            else
#endif
            {
#ifdef NPAPI_WANTED
                /* Dot1q Encapsulated packet recieved,check if encapsulation
                   configured is Dot1q if no then discard PDU */
                if (VlanPbPortEncapTypeIsDot1q (IfaceId.u4IfIndex) ==
                    VLAN_FAILURE)
                {
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }
                u4Offset += 4;
                /* Now skipping Vlan Type and tag Field */
                AST_BUF_MOVE_VALID_OFFSET (pBuf, u4Offset);
                u4Offset = 0;
                if (AST_COPY_FROM_CRU_BUF
                    (pBuf, (UINT1 *) &u2DataLength, u4Offset,
                     2) == AST_CRU_FAILURE)
                {
                    AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                             "MSG: Copy From Received BPDU CRU Buffer FAILED!\n");
                    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                    return RST_FAILURE;
                }
                u2DataLength = (UINT2) AST_NTOHS (u2DataLength);
#endif
            }
        }
        else
        {
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                AstL2IwfGetVlanPortType ((UINT2) pPortEntry->u4IfIndex,
                                         &u1PortType);

                if (pAstBpduType->uBpdu.PvrstBpdu.VlanId == AST_INIT_VAL)
                {
                    /* Classify the packet to PVID VLAN of the port */
                    AstL2IwfGetVlanPortPvid ((UINT2) pPortEntry->u4IfIndex,
                                             &(pAstBpduType->uBpdu.PvrstBpdu.
                                               VlanId));
                    if ((MEMCMP
                         (DestAddr, BrgRstpDestAddr,
                          (ETHERNET_ADDR_SIZE) - 1) == 0))
                    {
                        pAstBpduType->uBpdu.PvrstBpdu.u1IsPvrstPDU = AST_FALSE;
                        u1RcvdRstp = AST_TRUE;
                    }
                    else
                    {
                        pAstBpduType->uBpdu.PvrstBpdu.u1IsPvrstPDU = AST_TRUE;
                        u1RcvdRstp = AST_FALSE;
                    }

                    if (u1PortType != VLAN_ACCESS_PORT)
                    {
                        /* If the port is access and frame is RSTP frame 
                           classify it to Default Vlan Id 1 */

                        if (u1RcvdRstp == AST_TRUE)
                        {
                            pAstBpduType->uBpdu.PvrstBpdu.VlanId =
                                AST_DEF_VLAN_ID ();
                        }
                    }
                    /* else port is not an access port and it will
                       be classified to PVID vlan by default. */
                }
            }
#endif
        }
        /* Subtracting the length of the LLC Header */
        u2DataLength = (UINT2) (u2DataLength - AST_LLC_HEADER_SIZE);

#ifdef PVRST_WANTED
        if (AST_IS_PVRST_STARTED ())
        {
            if (AST_COPY_FROM_CRU_BUF
                (pBuf, (UINT1 *) &u2LLCHeader, AST_PVRST_LLC_OFFSET,
                 2) != AST_CRU_FAILURE)
            {
                if (u2LLCHeader == 0xaaaa)
                {
                    /* Now skipping Organization specific data */
                    AST_BUF_MOVE_VALID_OFFSET (pBuf, 5);
                    /* Subtracting the length of the LLC Header */
                    u2DataLength = (UINT2) (u2DataLength - AST_LLC_HEADER_SIZE);

                }
            }

        }
#endif
        /* Now skipping the Length and Llc Header fields */
        AST_BUF_MOVE_VALID_OFFSET (pBuf, AST_BPDU_OFFSET_FROM_ENETLEN);

    }
    else if (u1IfType == AST_PPP_PHYS_INTERFACE_TYPE)
    {
        u4Offset = 0;
    }
    else if ((u1IfType == AST_X25_PHYS_INTERFACE_TYPE)
             || (u1IfType == AST_FRMRL_PHYS_INTERFACE_TYPE))
    {
        u4Offset = AST_ENET_HEADER_SIZE + AST_LLC_HEADER_SIZE;
        AST_BUF_MOVE_VALID_OFFSET (pBuf, AST_ENET_HEADER_SIZE
                                   + AST_LLC_HEADER_SIZE);
    }
    else
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "MSG: Port %s: Not processing BPDU due to -\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "Unknown Physical IfType\n");
        AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                      "MSG: Port %s: Not processing BPDU due to -\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                 "Unknown Physical IfType\n");
        AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
        return RST_FAILURE;
    }

    u4ByteCount = AST_BUF_GET_CHAIN_VALIDBYTECOUNT (pBuf);
    pu1Bpdu = AST_GET_DATA_PTR_IF_LINEAR (pBuf, u4Offset, u4ByteCount);
    if (pu1Bpdu == NULL)
    {
        pu1Bpdu = gAstGlobalInfo.gpu1AstBpdu;
        AST_MEMSET (pu1Bpdu, AST_INIT_VAL, AST_MAX_ETH_FRAME_SIZE);
        if (AST_COPY_FROM_CRU_BUF (pBuf, pu1Bpdu, u4Offset, u4ByteCount)
            == AST_CRU_FAILURE)
        {
            AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: Received BPDU Copy From CRU Buffer FAILED!\n");
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_FAILURE;
        }
    }

#ifdef PVRST_WANTED
    if (AST_IS_PVRST_STARTED ())
    {
        if (pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN)
        {
            if (pAstCommPortInfo->bPortPvrstStatus != AST_SNMP_FALSE)
            {
                if (((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE) &&
                     (AST_GBL_BPDUGUARD_STATUS == AST_TRUE)) ||
                    (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE))
                {
                    /* When BPDU Guard feature is enabled and port is not enabled
                     * to process the pdu, restart the timer */
                    u2Duration = AST_DEFAULT_ERROR_RECOVERY;
                    u2InstIndex =
                        AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pAstBpduType->uBpdu.
                                                      PvrstBpdu.VlanId);
                    if (u2InstIndex != INVALID_INDEX)
                    {
                        if (PvrstStartTimer
                            ((VOID *) pPortEntry, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                             u2Duration) != PVRST_SUCCESS)
                        {
                            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG |
                                     AST_ALL_FAILURE_DBG,
                                     "AstStartTimer for disable recovery duration FAILED!\n");
                            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
                            return RST_FAILURE;
                        }
                    }
                }
            }
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Port %s Not Enabled...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_SUCCESS;

        }
    }
#endif
    if (AstValidateReceivedBpdu (u2PortNum, u2DataLength, pu1Bpdu, pAstBpduType)
        != RST_SUCCESS)
    {
        AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Received BPDU Validations FAILED!\n");
        AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
        return RST_FAILURE;
    }

    /*Verify the valid length of CRU Buffer */
    if (u2DataLength > (UINT2) u4ByteCount)
    {
        AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: BPDU CRU Buffer Length MISMATCH!\n");
        u2DataLength = (UINT2) u4ByteCount;
    }

    if (AST_IS_RST_ENABLED ())
    {
        if (RstHandleInBpdu (&(pAstBpduType->uBpdu.RstBpdu), u2PortNum) !=
            RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstHandleInBpdu function FAILED!\n");
            AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: RstHandleInBpdu function FAILED!\n");
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_FAILURE;
        }
        AstRedSyncUpPdu (u2PortNum, &(pAstBpduType->uBpdu.RstBpdu),
                         u2DataLength);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        if (MstHandleInBpdu (&(pAstBpduType->uBpdu.MstBpdu), u2PortNum) !=
            RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MstHandleInBpdu function FAILED!\n");
            AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: MstHandleInBpdu function FAILED!\n");
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_FAILURE;
        }
        MstRedSyncUpPdu (u2PortNum, &(pAstBpduType->uBpdu.MstBpdu),
                         u2DataLength);
    }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_ENABLED ())
    {
        if (PvrstHandleInBpdu (&(pAstBpduType->uBpdu.PvrstBpdu), u2PortNum) !=
            PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PvrstHandleInBpdu function FAILED!\n");
            AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: PvrstHandleInBpdu function FAILED!\n");
            AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
            return RST_FAILURE;
        }
        PvrstRedSyncUpPdu (u2PortNum, &(pAstBpduType->uBpdu.PvrstBpdu),
                           u2DataLength);
    }
#endif /* PVRST_WANTED */
    else
    {
        AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: No Advanced Spanning Tree Protocol is Enabled\n");
        AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
        return RST_FAILURE;
    }

    AST_DBG (AST_BPDU_DBG, "MSG: BPDU received successfully\n");
    AST_RELEASE_BPDU_TYPE_MEM_BLOCK (pAstBpduType);
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstHandleInBdpu                                      */
/*                                                                           */
/* Description        : It handles the received BPDUs (Config Bpdu, TCN      */
/*                      Bpdu, RST Bpdu or MST Bpdu) when RSTP Module is      */
/*                      enabled.                                             */
/*                                                                           */
/* Input(s)           : pRstBpdu - Pointer to the received message Bpdu      */
/*                                 structure                                 */
/*                      u2PortNum - The port number on which the Bpdu was    */
/*                                  received                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstHandleInBpdu (tAstBpdu * pRstBpdu, UINT2 u2PortNum)
{
    tAstBpduType        AstBpduType;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT2               u2Duration = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE)
    {
        if ((AST_GBL_BPDUGUARD_STATUS == AST_TRUE)
            && (pAstPortEntry->bAdminEdgePort == AST_TRUE))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);
            if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
            {
                if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
                {
                    NotifyProtoToApp.STPNotify.u1AdminStatus =
                        AST_PORT_ADMIN_DOWN;
                    NotifyProtoToApp.STPNotify.u4IfIndex =
                        AST_GET_IFINDEX (u2PortNum);
                    STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                    pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 ((UINT4) AST_CONTROL_PATH_TRC,
                                      "Port:%s  BPDUInconsistent occur at : %s\r\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      au1TimeStr);
                    AstBPDUIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pAstPortEntry->
                                                                u2PortNo),
                                               pAstCommPortInfo->
                                               bBpduInconsistent,
                                               RST_DEFAULT_INSTANCE,
                                               (INT1 *) AST_BRG_TRAPS_OID,
                                               AST_BRG_TRAPS_OID_LEN);
                }
            }
            else
            {
                /*Disable Spanning-tree on the port */
                if (RstDisablePort (u2PortNum, AST_EXT_PORT_DOWN) !=
                    RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "SYS: RstDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    AST_DBG_ARG1 (AST_BPDU_DBG,
                                  "SYS: RstDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    return RST_FAILURE;
                }
                /*Setting BpduInconsistent flag to true */
                if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
                {
                    pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Port:%s  BPDUInconsistent occur at : %s\r\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      au1TimeStr);
                    AstBPDUIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pAstPortEntry->
                                                                u2PortNo),
                                               pAstCommPortInfo->
                                               bBpduInconsistent,
                                               RST_DEFAULT_INSTANCE,
                                               (INT1 *) AST_BRG_TRAPS_OID,
                                               AST_BRG_TRAPS_OID_LEN);
                }
                u2Duration = (UINT2) pAstPortEntry->u4ErrorRecovery;
                if (AstStartTimer ((VOID *) pAstPortEntry, RST_DEFAULT_INSTANCE,
                                   (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                                   u2Duration) != RST_SUCCESS)
                {
                    AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: AstStartTimer for disable recovery duration FAILED!\n");
                    return RST_FAILURE;
                }
            }

            return RST_SUCCESS;
        }

    }
    else if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        AST_DBG_ARG1 (AST_BPDU_DBG,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
        {
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_DOWN;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u2PortNum);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent occur at : %s\r\n",
                                  AST_GET_IFINDEX (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pAstPortEntry->
                                                            u2PortNo),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
            }
        }
        else
        {
            if (RstDisablePort (u2PortNum, AST_EXT_PORT_DOWN) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                              "SYS: RstDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                AST_DBG_ARG1 (AST_BPDU_DBG,
                              "SYS: RstDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                return RST_FAILURE;
            }
            /*Setting BpduInconsistent flag to true */
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent occur at : %s\r\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pAstPortEntry->
                                                            u2PortNo),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
            }
            u2Duration = (UINT2) pAstPortEntry->u4ErrorRecovery;
            if (AstStartTimer ((VOID *) pAstPortEntry, RST_DEFAULT_INSTANCE,
                               (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                               u2Duration) != RST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: AstStartTimer for disable recovery duration FAILED!\n");
                return RST_FAILURE;
            }

            return RST_SUCCESS;
        }
    }
    if (pAstPortEntry->bRootGuard == RST_TRUE)
    {
        if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT)
            || (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE))
        {
            return RST_SUCCESS;
        }
    }

    pAstCommPortInfo->bRcvdBpdu = RST_TRUE;

    AST_MEMSET (&AstBpduType, AST_INIT_VAL, sizeof (tAstBpduType));

    AstBpduType.BpduType = AST_RST_MODE;

    AST_MEMCPY (&AstBpduType.uBpdu.RstBpdu, pRstBpdu, sizeof (tAstBpdu));

    if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_RCVD_BPDU,
                              u2PortNum, &AstBpduType) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RECEIVE: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                      "RECEIVE: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;

    }

    if (RstPortReceiveMachine (RST_PRCVSM_EV_RCVD_BPDU, pRstBpdu, u2PortNum)
        != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: RstPortReceiveMachine function returned FAILURE!\n");
        AST_DBG (AST_BPDU_DBG | AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: RstPortReceiveMachine function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleConfigMsg                                   */
/*                                                                           */
/* Description        : This function decodes the type of config message     */
/*                      and calls the corresponding message handler.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstLocalMsgMemPoolId                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHandleConfigMsg (tAstMsgNode * pMsgNode)
{
    INT4                i4RetVal = (INT4) RST_SUCCESS;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "MSG: Handling SNMP config message ...\n");
    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
             "MSG: Handling SNMP config message ...\n");

    if ((pMsgNode->MsgType != AST_START_RST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_RST_MSG) &&
        (pMsgNode->MsgType != AST_START_MST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_MST_MSG) &&
        (pMsgNode->MsgType != AST_START_PVRST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_PVRST_MSG))
    {

        if (!
            (AST_IS_RST_STARTED () || AST_IS_MST_STARTED ()
             || AST_IS_PVRST_STARTED ()))
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MSG: Module not started to Process Local Message!\n");
            return (INT4) RST_FAILURE;
        }

    }

    switch (pMsgNode->MsgType)
    {
        case AST_START_RST_MSG:
        case AST_STOP_RST_MSG:
        case AST_START_MST_MSG:
        case AST_STOP_MST_MSG:
        case AST_START_PVRST_MSG:
        case AST_STOP_PVRST_MSG:
        case AST_ENABLE_PORT_MSG:
        case AST_DISABLE_PORT_MSG:
        case AST_ENABLE_RST_MSG:
        case AST_ENABLE_MST_MSG:
        case AST_DISABLE_RST_MSG:
        case AST_DISABLE_MST_MSG:
        case AST_ENABLE_PVRST_MSG:
        case AST_DISABLE_PVRST_MSG:
        case AST_SVLAN_ENABLE_MSG:
        case AST_SVLAN_DISABLE_MSG:
        case AST_RST_CONFIG_BPDUGUARD_PORT_MSG:
        case AST_MST_CONFIG_BPDUGUARD_PORT_MSG:
        case AST_GBL_BPDUGUARD_ENABLE_MSG:
        case AST_GBL_BPDUGUARD_DISABLE_MSG:
        case AST_RST_BPDUGUARD_ACTION_MSG:
        case AST_PVRST_BPDUGUARD_ACTION_MSG:
        case AST_MST_BPDUGUARD_ACTION_MSG:

            if (AstHandleLocalMsgReceived (pMsgNode) != RST_SUCCESS)
            {
                AST_TRC (AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: AstHandleConfigMsg function returned FAILURE!\n");
                i4RetVal = RST_FAILURE;
            }
            break;

        case AST_MAXAGE_MSG:
            AstSetMaxAge (pMsgNode->u2InstanceId, pMsgNode->uMsg.u2MaxAge);
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Bridge Max Age as %d Success\n",
                          pMsgNode->uMsg.u2MaxAge);
            break;

        case AST_HELLOTIME_MSG:
            AstSetHelloTime (pMsgNode->u2InstanceId,
                             pMsgNode->uMsg.u2HelloTime);
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Hello Time as %d Success\n",
                          pMsgNode->uMsg.u2HelloTime);
            break;

        case AST_FORWARDDELAY_MSG:
            AstSetForwardDelay (pMsgNode->u2InstanceId,
                                pMsgNode->uMsg.u2ForwardDelay);
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Forward Delay as %d Success\n",
                          pMsgNode->uMsg.u2ForwardDelay);
            break;

        case AST_DYNAMIC_PATHCOST_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetDynamicPathcostCalc
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetDynamicPathcostCalc function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetDynamicPathcostCalc
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetDynamicPathcostCalc function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetDynamicPathcostCalc
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetDynamicPathcostCalc function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                     "MSG: Management SET of Dynamic Pathcost Calc Success\n");
            break;
        case AST_ENABLE_PORT_SPEED_CHG_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetDynaPathcostCalcLagg
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetDynaPathcostCalcLagg function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetDynaPathcostCalcLagg
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetDynaPathcostCalcLagg function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetDynaPathcostCalcLagg
                    (pMsgNode->uMsg.u1DynamicPathcostCalculation) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetDynaPathcostCalcLagg function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */

            AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                     "MSG: Management SET of Dynamic Pathcost On Speed Change"
                     " Success\n");
            break;
        case AST_PORT_PRIORITY_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetPortPriority ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.u1PortPriority)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetPortPriority function returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetPortPriority (pMsgNode->u2InstanceId,
                                        (UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.u1PortPriority)
                    != MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetPortPriority function returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetPortPriority (pMsgNode->u2InstanceId,
                                          (UINT2) pMsgNode->u4PortNo,
                                          pMsgNode->uMsg.u1PortPriority)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetPortPriority function returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Port %s Priority as %d Success\n",
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo),
                          pMsgNode->uMsg.u1PortPriority);
            break;

        case AST_BRIDGE_PRIORITY_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetBridgePriority (pMsgNode->uMsg.u2BridgePriority)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetBridgePriority function returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetBridgePriority (pMsgNode->u2InstanceId,
                                          pMsgNode->uMsg.u2BridgePriority)
                    != MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetBridgePriority function returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetBridgePriority (pMsgNode->u2InstanceId,
                                            pMsgNode->uMsg.u2BridgePriority)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetBridgePriority function returned FAILURE!\n");
                    i4RetVal = (INT4) PVRST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of BridgePriority as %d Success\n",
                          pMsgNode->uMsg.u2BridgePriority);
            break;

        case AST_PATH_COST_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetPortPathCost ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.u4PathCost)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetPortPathCost ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->u2InstanceId,
                                        pMsgNode->uMsg.u4PathCost)
                    != MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetInstPortPathCost (pMsgNode->u2InstanceId,
                                              (UINT2) pMsgNode->u4PortNo,
                                              pMsgNode->uMsg.u4PathCost) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Port %s Path Cost as %d Success\n",
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo),
                          pMsgNode->uMsg.u4PathCost);
            break;
        case AST_ZERO_PATHCOST_MSG:
            if (AST_IS_RST_STARTED ())

            {
                if (RstSetZeroPortPathCost ((UINT2) pMsgNode->u4PortNo)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetZeroPortPathCost ((UINT2) pMsgNode->u4PortNo,
                                            pMsgNode->u2InstanceId)
                    != MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetInstZeroPortPathCost (pMsgNode->u2InstanceId,
                                                  (UINT2) pMsgNode->u4PortNo)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetZeroPortPathCost function "
                             "returned FAILURE!\n");
                    i4RetVal = (INT4) RST_FAILURE;

                }
            }
#endif

            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Port %s Path Cost as %d Success\n",
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo),
                          pMsgNode->uMsg.u4PathCost);
            break;

        case AST_FORCE_VERSION_MSG:
            if (AstSetForceVersion (pMsgNode->uMsg.u1ForceVersion)
                != RST_SUCCESS)
            {
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: AstSetForceVersion function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
            }
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET for Force Version as %d Success\n",
                          pMsgNode->uMsg.u1ForceVersion);
            break;

        case AST_PROTOCOL_MIGRATION_MSG:
            if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
            {
                if (AstSetProtocolMigration ((UINT2) pMsgNode->u4PortNo) !=
                    RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: AstSetProtocolMigration function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET for Protocol Migration "
                              "of Port %s Success\n",
                              AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            }
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetProtocolMigration (pMsgNode->u4PortNo,
                                               pMsgNode->u2InstanceId)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: AstSetProtocolMigration function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
                else
                {
                    AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                                  "MSG: Management SET for Protocol Migration "
                                  "of Port %s Success\n",
                                  AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
                }
            }
#endif
            break;

        case AST_ADMIN_EDGEPORT_MSG:
            if (AstSetAdminEdgePort ((UINT2) pMsgNode->u4PortNo,
                                     pMsgNode->uMsg.bAdminEdgePort)
                != RST_SUCCESS)
            {
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: AstSetAdminEdgePort function returned "
                         "FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
            }

            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET for AdminEdgePort Status %d "
                          "of Port %s Success\n",
                          pMsgNode->uMsg.bAdminEdgePort,
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            break;

        case AST_ADMIN_PTOP_MSG:

            if (AST_IS_RST_STARTED ())
            {
                if (RstSetAdminPointToPoint ((UINT2) pMsgNode->u4PortNo,
                                             pMsgNode->uMsg.u1AdminPToP)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetAdminPointToPoint function returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetAdminPointToPoint ((UINT2) pMsgNode->u4PortNo,
                                             pMsgNode->uMsg.u1AdminPToP)
                    != MST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetAdminPointToPoint function returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetAdminPointToPoint ((UINT2) pMsgNode->u4PortNo,
                                               pMsgNode->uMsg.u1AdminPToP)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetAdminPointToPoint function returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET for AdminPointToPoint Status %d "
                          "of Port %s Success\n",
                          pMsgNode->uMsg.u1AdminPToP,
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            break;

        case AST_TX_HOLDCOUNT_MSG:
            AstSetTxHoldCount (pMsgNode->uMsg.u1TxHoldCount);
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET of Tx Hold Count as %d Success\n",
                          pMsgNode->uMsg.u1TxHoldCount);
            break;

        case AST_PORT_PSEUDO_ROOTID_CONF_MSG:
            if (AstSetPseudoRootId ((UINT2) pMsgNode->u4PortNo,
                                    pMsgNode->u2InstanceId,
                                    pMsgNode->uMsg.BridgeId.BridgeAddr,
                                    pMsgNode->uMsg.BridgeId.u2BrgPriority)
                != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET Pseudo RootId of port %d Failed\n",
                              pMsgNode->u4PortNo);

                i4RetVal = (INT4) RST_FAILURE;

            }

            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET Pseudo RootId of port %d Success\n",
                          pMsgNode->u4PortNo);

            break;

        case AST_PORT_L2GP_CONF_MSG:
            if (AstSetPortL2gpStatus ((UINT2) pMsgNode->u4PortNo,
                                      pMsgNode->uMsg.bPortL2gp) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET L2gp status   of port %d Failed\n",
                              pMsgNode->u4PortNo);

                i4RetVal = (INT4) RST_FAILURE;

            }

            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET L2gp status   of port %d Success\n",
                          pMsgNode->u4PortNo);

            break;

        case AST_PORT_BPDU_RX_CONF_MSG:
            if (AstSetPortBpduRxStatus ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.bEnableBPDURx)
                != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET BPDU Rx status of port %d Failed\n",
                              pMsgNode->u4PortNo);

                i4RetVal = (INT4) RST_FAILURE;

            }

            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET BPDU Rx status of port %d Success\n",
                          pMsgNode->u4PortNo);

            break;

        case AST_PORT_BPDU_TX_CONF_MSG:
            if (AstSetPortBpduTxStatus ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.bEnableBPDUTx)
                != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET BPDU Tx status of port %d Failed\n",
                              pMsgNode->u4PortNo);

                i4RetVal = (INT4) RST_FAILURE;

            }

            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET BPDU Tx status of port %d Success\n",
                          pMsgNode->u4PortNo);

            break;

        case AST_LOOP_GUARD_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetPortLoopGuard ((UINT2) pMsgNode->u4PortNo,
                                         pMsgNode->uMsg.bLoopGuard)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetPortLoopGuard returned " "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstSetPortLoopGuard ((UINT2) pMsgNode->u4PortNo,
                                         pMsgNode->uMsg.bLoopGuard)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstSetPortLoopGuard returned " "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetPortLoopGuard ((UINT2) pMsgNode->u4PortNo,
                                           pMsgNode->uMsg.bLoopGuard)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetPortLoopGuard returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif
            AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                          "MSG: Management SET for LoopGuard Status %d "
                          "of Port %s Success\n",
                          pMsgNode->uMsg.bLoopGuard,
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            break;

#ifdef MSTP_WANTED
        case MST_MAX_INST_NUMBER_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetMaxInstance (pMsgNode->uMsg.u2MaxInstNumber);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Max Instance Numer %d Success\n",
                              pMsgNode->uMsg.u2MaxInstNumber);
            }
            break;

        case MST_MAX_HOP_COUNT_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetMaxHopCount (pMsgNode->uMsg.u1MaxHopCount);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Max Hop Count %d Success\n",
                              pMsgNode->uMsg.u1MaxHopCount);
            }
            break;

        case MST_CONFIG_ID_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetConfigId (pMsgNode->uMsg.u1ConfigIdSel);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Configuration Id  %d Success\n",
                              pMsgNode->uMsg.u1ConfigIdSel);
            }
            break;

        case MST_REGION_NAME_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetRegionName (pMsgNode->uMsg.ConfigName.au1Octets,
                                  pMsgNode->uMsg.ConfigName.u2OctLen);
            }
            break;

        case MST_REGION_VERSION_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetRegionVersion (pMsgNode->uMsg.u2ConfigRevLevel);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of  Region Version %d Success\n",
                              pMsgNode->uMsg.u2ConfigRevLevel);
            }
            break;

        case MST_INST_VLANID_MAP_MSG:
            if (AST_IS_MST_STARTED ())
            {
                if (MstVlanIdInstMap (pMsgNode->u2InstanceId,
                                      pMsgNode->uMsg.VlanId) == MST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management SET of Vlan Id to Instance Success\n");
                }
                else
                {
                    i4RetVal = (INT4) RST_FAILURE;
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management SET of Vlan Id to Instance Failed\n");
                }
            }
            break;

        case MST_INST_VLANID_UNMAP_MSG:
            if (AST_IS_MST_STARTED ())
            {
                if (MstVlanIdInstUnMap (pMsgNode->u2InstanceId,
                                        pMsgNode->uMsg.VlanId) == MST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management RESET of Vlan Id to Instance Success\n");
                }
                else
                {
                    i4RetVal = (INT4) RST_FAILURE;
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management RESET of Vlan Id to Instance Success\n");
                }
            }
            break;

        case MST_INST_VLANLIST_MAP_MSG:
            if (AST_IS_MST_STARTED ())
            {
                /* Post Message to GARP about Instance VLAN map inside this function */
                if (MstMapVlanList (pMsgNode->uMsg.MstVlanList,
                                    pMsgNode->u2InstanceId) == MST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management SET of Vlan Id to Instance Success\n");
                }
                else
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management SET of Vlan Id to Instance Failed\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        case MST_INST_VLANLIST_UNMAP_MSG:
            if (AST_IS_MST_STARTED ())
            {
                /* Post Message to GARP about Instance VLAN map inside this function */
                if (MstUnMapVlanList (pMsgNode->uMsg.MstVlanList,
                                      pMsgNode->u2InstanceId) == MST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management Disable of Vlan Id to Instance Success\n");
                }
                else
                {
                    AST_TRC (AST_CONTROL_PATH_TRC,
                             "MSG: Management Disable of Vlan Id to Instance Failed\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        case MST_PORT_HELLO_TIME_MSG:
            if (AST_IS_MST_STARTED ())
            {
                MstSetPortHelloTime ((UINT2) pMsgNode->u4PortNo,
                                     pMsgNode->uMsg.u2HelloTime);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Port Hello Time %d Success\n",
                              pMsgNode->uMsg.u2HelloTime);
            }
            break;

#endif /* MSTP_WANTED */

        case AST_PORT_ERROR_RECOVERY_MSG:

            if (AST_IS_RST_STARTED ())
            {
                RstSetPortErrorRecovery ((UINT2) pMsgNode->u4PortNo,
                                         pMsgNode->uMsg.u4ErrorRecovery);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Port Error Recovery %d Success\n",
                              pMsgNode->uMsg.u4ErrorRecovery);
            }
#ifdef MSTP_WANTED
            if (AST_IS_MST_STARTED ())
            {
                MstSetPortErrorRecovery ((UINT2) pMsgNode->u4PortNo,
                                         pMsgNode->uMsg.u4ErrorRecovery);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Port Error Recovery %d Success\n",
                              pMsgNode->uMsg.u4ErrorRecovery);
            }
#endif /* MSTP_WANTED */
            break;

        case AST_AUTO_EDGEPORT_MSG:
            if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
            {
                if (AstSetAutoEdgePort ((UINT2) pMsgNode->u4PortNo,
                                        pMsgNode->uMsg.bAutoEdgePort)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: AstSetAutoEdgePortfunction returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }

                AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET for AutoEdgePort Status %d "
                              "of Port %s Success\n",
                              pMsgNode->uMsg.bAutoEdgePort,
                              AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            }
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstSetAutoEdgePort ((UINT2) pMsgNode->u4PortNo,
                                          pMsgNode->uMsg.bAutoEdgePort)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstSetAutoEdgePort returned " "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
                AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET for AutoEdgePort Status %d "
                              "of Port %s Success\n",
                              pMsgNode->uMsg.bAutoEdgePort,
                              AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));
            }

            break;
        case AST_PVRST_CONFIG_STATUS_PORT_MSG:

        case AST_PVRST_SET_ENCAP_TYPE_PORT_MSG:

        case AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG:

        case AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG:

        case AST_PVRST_CONFIG_INST_STATUS_PORT_MSG:

        case AST_PVRST_HELLOTIME_MSG:

        case AST_PVRST_TX_HOLDCOUNT_MSG:

        case AST_PVRST_FORWARDDELAY_MSG:

        case AST_PVRST_BRG_MAXAGE_MSG:

            if (AstHandleLocalMsgReceived (pMsgNode) != RST_SUCCESS)
            {
                AST_TRC (AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: AstHandleConfigMsg function returned FAILURE!\n");
                i4RetVal = (INT4) RST_FAILURE;
            }
            break;
#endif
        case AST_DOT1W_ENABLED_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstSetPortDot1WEnable ((UINT2) pMsgNode->u4PortNo,
                                           pMsgNode->uMsg.bDot1wEnabled)
                    != (INT4) RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstSetPortDot1WEnable returned "
                             "FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        default:
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                     AST_ALL_FAILURE_DBG, "MSG: Invalid Message Type\n");
            break;

    }                            /* End of Switch Block */

    if (i4RetVal != (INT4) RST_SUCCESS)
    {
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: SNMP Config Message Processing FAILED !!!\n");
    }
    else
    {
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                 "MSG: Successfully processed SNMP Config Message !!!\n");
    }
    return (i4RetVal);

}

/*****************************************************************************/
/* Function Name      : AstDefaultCfgMsgHandler                              */
/*                                                                           */
/* Description        : This function does the default processing (like      */
/*                      releasing locks) when the port number or context ID  */
/*                      that is encoded in the message is not known to       */
/*                      RSTP/MSTP/PVRST                                      */
/*                                                                           */
/* Input(s)           : pMsgNode - pointer to the message node               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstDefaultCfgMsgHandler (tAstMsgNode * pMsgNode)
{
    AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                    AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                    "MSG: Unkonwn port or context in message. Message is being"
                    "handled by the default handler!\n");
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                    AST_EVENT_HANDLING_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                    "MSG: Unknown port or context in message. Message is being"
                    "handled by the default handler!\n");

    switch (pMsgNode->MsgType)
    {

        case AST_CREATE_PORT_MSG:
        case AST_CREATE_PORT_FROM_LA_MSG:
            L2_SYNC_GIVE_SEM ();
            break;
        case AST_CREATE_PEP_PORT_MSG:
        case AST_DELETE_PEP_PORT_MSG:
        case AST_ENABLE_PEP_PORT_MSG:
        case AST_DISABLE_PEP_PORT_MSG:
        case AST_HW_PEP_CREATE_MSG:
            /*No need to give any sem for the logical port creation and
             * deletion*/
            break;
        case AST_DELETE_CONTEXT_MSG:
        case AST_MAP_PORT_MSG:
        case AST_UNMAP_PORT_MSG:
            L2MI_SYNC_GIVE_SEM ();
            break;

            /* Handle Enable/Disable port message alone even though 
             * module is shutdown */
        case AST_ENABLE_PORT_MSG:
            if (AST_CURR_CONTEXT_INFO () != NULL)
            {
#ifdef PVRST_WANTED
#ifdef NPAPI_WANTED
                /* Don't program the HW for SISP interfaces */
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    PvrstFsMiPvrstNpSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                      pMsgNode->u4PortNo, 0,
                                                      AST_PORT_STATE_FORWARDING);
                }
#endif /*NPAPI_WANTED */
#else
                AstL2IwfSetInstPortState (RST_DEFAULT_INSTANCE,
                                          (UINT2) pMsgNode->u4PortNo,
                                          AST_PORT_STATE_FORWARDING);
#ifdef NPAPI_WANTED
                if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE) &&
                    (SISP_IS_LOGICAL_PORT (pMsgNode->u4PortNo) == VCM_FALSE))
                {
                    RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                                pMsgNode->u4PortNo,
                                                AST_PORT_STATE_FORWARDING);
                }
#endif /*NPAPI_WANTED */
#endif
            }
            break;

        case AST_DISABLE_PORT_MSG:
            if (AST_CURR_CONTEXT_INFO () != NULL)
            {
#ifdef PVRST_WANTED
#ifdef NPAPI_WANTED
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    PvrstFsMiPvrstNpSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                      pMsgNode->u4PortNo, 0,
                                                      AST_PORT_STATE_DISCARDING);
                }
#endif /*NPAPI_WANTED */
#else
                AstL2IwfSetInstPortState (RST_DEFAULT_INSTANCE,
                                          (UINT2) pMsgNode->u4PortNo,
                                          AST_PORT_STATE_DISCARDING);
#ifdef NPAPI_WANTED
                if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE) &&
                    (SISP_IS_LOGICAL_PORT (pMsgNode->u4PortNo) == VCM_FALSE))
                {
                    RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                                pMsgNode->u4PortNo,
                                                AST_PORT_STATE_DISCARDING);
                }
#endif /*NPAPI_WANTED */
#endif
                if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) ==
                    VLAN_TRUE)
                {
                    AstVlanDeleteFdbEntries (pMsgNode->u4PortNo,
                                             VLAN_NO_OPTIMIZE);
                }
#ifdef BRIDGE_WANTED
                else
                {
                    /* Inform the bridge to delete in its MAC table
                     * all the entries learned for this port.
                     */
                    BrgDeleteFwdEntryForPort (pMsgNode->u4PortNo);
                }
#endif
            }
            break;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleLocalMsgReceived                            */
/*                                                                           */
/* Description        : This function decodes the type of local message      */
/*                      posted and calls the corresponding message handler.  */
/*                                                                           */
/* Input(s)           : pMsgNode - pointer to the message node               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstLocalMsgMemPoolId                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHandleLocalMsgReceived (tAstMsgNode * pMsgNode)
{
    INT4                i4RetVal = (INT4) RST_SUCCESS;

#ifdef PVRST_WANTED
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    UINT1               u4PortListArray[MAX_PORT_NUM];
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    UINT1               u1NumPorts = 0;
    UINT1               u1Index = 0;
    INT4                i4Val = 0;
#endif
    UINT2               u2Port = 0;
#ifdef PB_WANTED
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPortEntry      *pTemp = NULL;
    UINT1               u1TunnelStatus;
#endif
#ifdef NPAPI_WANTED
    tAstPortEntry      *pPortEntry = NULL;
    tAsNpwFsMiRstpNpSetPortState FsMiRstpNpSetPortStateVal;
    tAsNpwFsMiMstpNpSetInstancePortState FsMiMstpNpSetInstancePortStateVal;
#ifdef PVRST_WANTED
    tAsNpwFsMiPvrstNpSetVlanPortState FsMiPvrstNpSetVlanPortStateVal;
    MEMSET (u4PortListArray, 0, MAX_PORT_NUM);
#endif
#endif /*NPAPI_WANTED */

    /* Assumptions:
     * 1) STP Lock already taken
     * 2) Appropriate context already selected. 
     * 3) The u4PortNo field inside the MsgNode 
     *    refers to the LocalPortId EXCEPT if the message 
     *    type is CreatePort in which case the LocalPortId is 
     *    found in the u2LocalPortId field instead.*/

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "MSG: Handling received local message ...\n");
    AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
             "MSG: Handling received local message ...\n");

    if ((pMsgNode->MsgType != AST_START_RST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_RST_MSG) &&
        (pMsgNode->MsgType != AST_START_MST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_MST_MSG) &&
        (pMsgNode->MsgType != AST_START_PVRST_MSG) &&
        (pMsgNode->MsgType != AST_STOP_PVRST_MSG) &&
        (pMsgNode->MsgType != AST_DELETE_CONTEXT_MSG) &&
        (pMsgNode->MsgType != AST_UPDATE_CONTEXT_NAME))
    {
        if (!
            (AST_IS_RST_STARTED () || AST_IS_MST_STARTED ()
             || AST_IS_PVRST_STARTED ()))
        {
            AST_DBG (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MGMT: Module not started to Process Local Message!\n");

            AstDefaultCfgMsgHandler (pMsgNode);
            return (INT4) RST_SUCCESS;
        }
    }

    switch (pMsgNode->MsgType)
    {
        case AST_START_RST_MSG:

            if (AST_IS_PVRST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PVRST must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: PVRST must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_MST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MSTP must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: MSTP must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: RSTP is already Started.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: RSTP is already Started.\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

            /* Starting RSTP Module */
            if (RstModuleInit () != RST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RstModuleInit function returned FAILURE!!!\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: RstModuleInit function returned FAILURE!!!\n");
                i4RetVal = (INT4) RST_FAILURE;
                break;
            }
            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: RSTP Module Initialized Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: RSTP Module Initialized Successfully...\n");
            break;

        case AST_STOP_RST_MSG:

            if (!AST_IS_RST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: RSTP Module not started..No shutdown to perform !\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: RSTP Module not started..No shutdown to perform !\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

            if (AST_NODE_STATUS () == RED_AST_STANDBY)
            {
                AstRedClearAllSyncUpData ();
            }

            /* Shutting Down RSTP Module */
            RstModuleShutdown ();

            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: RSTP Module Shutdown Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: RSTP Module Shutdown Successfully...\n");
            break;

        case AST_START_MST_MSG:

            if (AST_IS_PVRST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PVRST must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: PVRST must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RSTP must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: RSTP must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_MST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: MSTP is already Started.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: MSTP is already Started.\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef MSTP_WANTED
            if (MstModuleInit () != MST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MstModuleInit function returned FAILURE\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: MstModuleInit function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                break;
            }

            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: MSTP Module Initialized Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: MSTP Module Initialized Successfully...\n");
#else
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MSTP Module Not Present !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: MSTP Module Not Present !!!\n");
#endif /* MSTP_WANTED */
            break;

        case AST_STOP_MST_MSG:

            if (!AST_IS_MST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: MSTP Module not started..No shutdown to perform !\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: MSTP Module not started..No shutdown to perform !\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef MSTP_WANTED

            if (AST_NODE_STATUS () == RED_AST_STANDBY)
            {
                AstRedClearAllSyncUpData ();
            }

            if (MstModuleShutdown () != MST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MstModuleShutdown function returned FAILURE\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: MstModuleShutdown function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
            }
            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: MSTP Module Shutdown Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: MSTP Module Shutdown Successfully...\n");
#else
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MSTP Module Not Present !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: MSTP Module Not Present !!!\n");
            i4RetVal = (INT4) RST_FAILURE;
#endif /* MSTP_WANTED */
            break;
        case AST_START_PVRST_MSG:

            if (AST_IS_RST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RSTP must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: RSTP must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_MST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MST must be shutdown first.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: MST must be shutdown first.\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (AST_IS_PVRST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: PVRST is already Started.\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: PVRST is already Started.\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef PVRST_WANTED
            if (PvrstModuleInit () != PVRST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PvrstModuleInit function returned FAILURE\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: PvrstModuleInit function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                break;
            }

            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: PVRST Module Initialized Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: PVRST Module Initialized Successfully...\n");
#else
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PVRST Module Not Present !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: PVRST Module Not Present !!!\n");
#endif /* PVRST_WANTED */
            break;

        case AST_STOP_PVRST_MSG:

            if (!AST_IS_PVRST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: PVRST Module not started..No shutdown to perform !\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: PVRST Module not started..No shutdown to perform !\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef PVRST_WANTED

            if (AST_NODE_STATUS () == RED_AST_STANDBY)
            {
                AstRedClearAllSyncUpData ();
            }

            if (PvrstModuleShutdown () != PVRST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PvrstModuleShutdown function returned FAILURE\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: PvrstModuleShutdown function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                break;
            }
            AST_TRC (AST_INIT_SHUT_TRC,
                     "MSG: PVRST Module Shutdown Successfully...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: PVRST Module Shutdown Successfully...\n");
#else
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PVRST Module Not Present !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: PVRST Module Not Present !!!\n");
            i4RetVal = (INT4) RST_FAILURE;
#endif /* PVRST_WANTED */
            break;

        case AST_CREATE_PORT_MSG:
            i4RetVal = AstHandleCreatePort (pMsgNode);
            L2_SYNC_GIVE_SEM ();
            break;

        case AST_CREATE_PORT_FROM_LA_MSG:
            i4RetVal = AstHandleCreatePortFromLa (pMsgNode);
            L2_SYNC_GIVE_SEM ();
            break;
        case AST_CREATE_PEP_PORT_MSG:
            i4RetVal = AstHandleCreatePepPort (pMsgNode);
            break;

        case AST_MAP_PORT_MSG:
            i4RetVal = AstHandleCreatePort (pMsgNode);
            L2MI_SYNC_GIVE_SEM ();
            break;

        case AST_ENABLE_PORT_MSG:
            u2Port = (UINT2) (pMsgNode->u4PortNo);
#ifdef PB_WANTED
            /*Send an indication to vlan module regarding the stp disable event for updating tunnel status */
            if (pMsgNode->uMsg.u1TrigType != (UINT1) AST_EXT_PORT_UP)
            {
                AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX
                                                       (pMsgNode->u4PortNo),
                                                       L2_PROTO_STP,
                                                       &u1TunnelStatus);
                if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {

                    AstIndicateSTPStatus (pMsgNode->u4PortNo, RST_TRUE);
                }
            }
#endif
            i4RetVal = AstHandleEnablePortMsg (u2Port,
                                               pMsgNode->u2InstanceId,
                                               pMsgNode->uMsg.u1TrigType);
            break;

        case AST_ENABLE_PEP_PORT_MSG:
        case AST_DISABLE_PEP_PORT_MSG:
            i4RetVal = AstHandlePepOperStatus (pMsgNode);
            break;

        case AST_DISABLE_PORT_MSG:
            u2Port = (UINT2) (pMsgNode->u4PortNo);
#ifdef PB_WANTED
            /*Send an indication to vlan module regarding the stp disable event for updating tunnel status */
            if (pMsgNode->uMsg.u1TrigType != (UINT1) AST_EXT_PORT_DOWN)
            {
                AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX
                                                       (pMsgNode->u4PortNo),
                                                       L2_PROTO_STP,
                                                       &u1TunnelStatus);
#ifdef MSTP_WANTED
                if (pMsgNode->u2InstanceId == MST_CIST_CONTEXT)
                {
#endif
                    if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_TUNNEL)
                    {

                        /*Send an indication to vlan module regarding the stp disable event for updating tunnel status */
                        AstIndicateSTPStatus (pMsgNode->u4PortNo, RST_FALSE);
                    }
#ifdef MSTP_WANTED
                }
#endif
            }
#endif
            if (AST_IS_CUSTOMER_EDGE_PORT (u2Port) == RST_TRUE)
            {
                /* Process the STP down and oper down for Customer Edge Port. */
                if (AstPbHandleCcompCepPortStatus (u2Port,
                                                   pMsgNode->uMsg.u1TrigType)
                    != RST_SUCCESS)
                {
                    return RST_FAILURE;
                }
            }

            /* Don't update the port oper down in the S-VLAN component CEP,
             * but update STP up in the S-VLAN component CEP.
             * For other ports update port oper up/stp up in the S-VLAN
             * ports. */
            if (!((pMsgNode->uMsg.u1TrigType == AST_EXT_PORT_UP) &&
                  (AST_IS_CUSTOMER_EDGE_PORT (pMsgNode->u4PortNo) == RST_TRUE)))
            {
                /*To start measuring the time when Port is disabled */
                STP_PBB_PERF_MARK_START_TIME ();
                if (AstDisablePort (pMsgNode) != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: AstEnablePort function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                             | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: AstEnablePort function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }

            if (AST_IS_RST_STARTED ())
            {
                if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                {
                    AstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);
                    AstRedSendSyncMessages (RST_DEFAULT_INSTANCE,
                                            (UINT2) pMsgNode->u4PortNo,
                                            RED_AST_OPER_STATUS,
                                            pMsgNode->uMsg.u1TrigType);
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                {
                    MstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);
                    AstRedSendSyncMessages (pMsgNode->u2InstanceId,
                                            (UINT2) pMsgNode->u4PortNo,
                                            RED_AST_OPER_STATUS,
                                            pMsgNode->uMsg.u1TrigType);
                }

            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                {
                    pPortInfo = AST_GET_PORTENTRY ((UINT2) pMsgNode->u4PortNo);
                    PvrstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);

                    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
                    {
                        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                    VlanId) == OSIX_FALSE)
                        {
                            continue;
                        }
                        AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex,
                                                 &u1PortType);

                        if (u1PortType == VLAN_HYBRID_PORT)
                        {
                            if (AST_DEF_VLAN_ID () != VlanId)
                            {
                                continue;
                            }
                            else
                            {
                                AstRedSendSyncMessages (VlanId,
                                                        (UINT2) pMsgNode->
                                                        u4PortNo,
                                                        RED_AST_OPER_STATUS,
                                                        pMsgNode->uMsg.
                                                        u1TrigType);
                                break;
                            }
                        }
                        if (u1PortType == VLAN_ACCESS_PORT)
                        {
                            AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->
                                                     u4IfIndex, &PVID);
                            if (PVID != VlanId)
                            {
                                continue;
                            }
                            else
                            {
                                AstRedSendSyncMessages (VlanId,
                                                        (UINT2) pMsgNode->
                                                        u4PortNo,
                                                        RED_AST_OPER_STATUS,
                                                        pMsgNode->uMsg.
                                                        u1TrigType);
                                break;
                            }
                        }
                        AstRedSendSyncMessages (VlanId,
                                                (UINT2) pMsgNode->u4PortNo,
                                                RED_AST_OPER_STATUS,
                                                pMsgNode->uMsg.u1TrigType);
                    }
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                          AST_MGMT_DBG,
                          "MSG: Management Disabling of Port %s Success\n",
                          AST_GET_IFINDEX_STR (pMsgNode->u4PortNo));

            break;

        case AST_DELETE_PORT_MSG:
            i4RetVal = AstHandleDeletePort (pMsgNode);
            break;

        case AST_DELETE_PORT_FROM_LA_MSG:
            i4RetVal = AstHandleDeletePortFromLa (pMsgNode);
            break;

        case AST_DELETE_PEP_PORT_MSG:
            i4RetVal = AstHandleDeletePepPort (pMsgNode);
            break;

        case AST_UNMAP_PORT_MSG:
            i4RetVal = AstHandleDeletePort (pMsgNode);
            L2MI_SYNC_GIVE_SEM ();
            break;

        case AST_ENABLE_RST_MSG:

            if (!AST_IS_RST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RSTP System Shutdown; CANNOT Enable RSTP\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: RSTP System Shutdown; CANNOT Enable RSTP\n");
                i4RetVal = RST_FAILURE;
                break;
            }
            if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: RSTP Module is already Enabled\n");
                AST_TRC (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: RSTP Module is already Enabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

            if (RstModuleEnable () != RST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RstModuleEnable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: RstModuleEnable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                RstModuleDisable ();
#ifdef NPAPI_WANTED
                RstDisableStpInHw ();
#endif /* NPAPI_WANTED */
            }
            else
            {
#ifdef PB_WANTED
                u2PortNum = 1;
                AST_GET_NEXT_PORT_ENTRY (u2PortNum, pTemp)
                {
                    if ((pTemp) == NULL)
                    {
                        continue;
                    }

                    AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX
                                                           (u2PortNum),
                                                           L2_PROTO_STP,
                                                           &u1TunnelStatus);
                    if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_TUNNEL)
                    {
                        /*Send an indication to vlan module regarding stp disable event for updating tunnel status */
                        AstIndicateSTPStatus ((UINT4) u2PortNum, RST_TRUE);
                    }

                }
#endif
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Enabling of RSTP Module Success\n");
            }
            break;

        case AST_SVLAN_ENABLE_MSG:
        case AST_SVLAN_DISABLE_MSG:
            i4RetVal = AstHandleSVlanModuleStatus (pMsgNode);
            break;

        case AST_ENABLE_MST_MSG:

            if (!AST_IS_MST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MSTP System Shutdown; CANNOT Enable MSTP\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: MSTP System Shutdown; CANNOT Enable MSTP\n");
                i4RetVal = RST_FAILURE;
                break;
            }
            if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_ENABLED)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: MSTP Module is already Enabled\n");
                AST_TRC (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: MSTP Module is already Enabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef MSTP_WANTED
            if (MstModuleEnable () != MST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MstModuleEnable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: MstModuleEnable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                MstModuleDisable ();
#ifdef NPAPI_WANTED
                RstDisableStpInHw ();
#endif /* NPAPI_WANTED */
            }
            else
            {
#ifdef PB_WANTED
                u2PortNum = 1;
                AST_GET_NEXT_PORT_ENTRY (u2PortNum, pTemp)
                {
                    if ((pTemp) == NULL)
                    {
                        continue;
                    }

                    AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX
                                                           (u2PortNum),
                                                           L2_PROTO_STP,
                                                           &u1TunnelStatus);
                    if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_TUNNEL)
                    {
                        /*Send an indication to vlan module regarding stp disable event for updating tunnel status */
                        AstIndicateSTPStatus ((UINT4) u2PortNum, MST_TRUE);
                    }

                }
#endif
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Enabling of MSTP Module Success\n");
            }
#else
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_MGMT_DBG, "MSG: MSTP module not present !!!\n");
#endif /* MSTP_WANTED */
            break;
        case AST_ENABLE_PVRST_MSG:
            if (!AST_IS_PVRST_STARTED ())
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PVRST System Shutdown; CANNOT Enable PVRST\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: PVRST System Shutdown; CANNOT Enable PVRST\n");
                i4RetVal = RST_FAILURE;
                break;
            }
            if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_ENABLED)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: PVRST Module is already Enabled\n");
                AST_TRC (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: PVRST Module is already Enabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef PVRST_WANTED
            if (PvrstModuleEnable () != PVRST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PvrstModuleEnable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: PvrstModuleEnable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                PvrstModuleDisable ();
#ifdef NPAPI_WANTED
                RstDisableStpInHw ();
#endif /* NPAPI_WANTED */
            }
            else
            {
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Enabling of PVRST Module Success\n");
            }
#else
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_MGMT_DBG, "MSG: PVRST module not present !!!\n");
#endif /* PVRST_WANTED */
            break;
        case AST_DISABLE_RST_MSG:

            if (!(AST_IS_RST_STARTED ()))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: RSTP System Shutdown; CANNOT Disable RSTP\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: RSTP System Shutdown; CANNOT Disable RSTP\n");
                i4RetVal = RST_SUCCESS;
                break;
            }
            if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_DISABLED) &&
                (AST_ADMIN_STATUS == (UINT1) RST_DISABLED))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: RSTP Module is already Disabled\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: RSTP Module is already Disabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

            if (RstModuleDisable () != RST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RstModuleDisable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: RstModuleDisable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;

            }
            else
            {
#ifdef PB_WANTED
                u2PortNum = 1;
                AST_GET_NEXT_PORT_ENTRY (u2PortNum, pTemp)
                {
                    if ((pTemp) == NULL)
                    {
                        continue;
                    }
                    /*Send an indication to vlan module regarding stp disable event for updating tunnel status */
                    AstIndicateSTPStatus ((UINT4) u2PortNum, RST_FALSE);
                }
#endif
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Disabling of RSTP Module Success\n");
            }

            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                /* Release memory blocks on all ports */
                AstRedClearAllPduOnActive ();

                AstRedSendSyncMessages (RST_DEFAULT_INSTANCE, 0,
                                        RED_AST_CLEAR_ALL_SYNCUP_DATA, 0);
            }

            break;

        case AST_DISABLE_MST_MSG:

            if (!(AST_IS_MST_STARTED ()))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: MSTP System Shutdown; CANNOT Disable MSTP\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: MSTP System Shutdown; CANNOT Disable MSTP\n");
                i4RetVal = RST_SUCCESS;
                break;
            }
            if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_DISABLED) &&
                (AST_ADMIN_STATUS == (UINT1) MST_DISABLED))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: MSTP Module is already Disabled\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: MSTP Module is already Disabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef MSTP_WANTED
            if (MstModuleDisable () != MST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MstModuleDisable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: MstModuleDisable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
            }
            else
            {
#ifdef PB_WANTED
                u2PortNum = 1;
                AST_GET_NEXT_PORT_ENTRY (u2PortNum, pTemp)
                {
                    if ((pTemp) == NULL)
                    {
                        continue;
                    }
                    /*Send an indication to vlan module regarding stp disable event for updating tunnel status */
                    AstIndicateSTPStatus ((UINT4) u2PortNum, MST_FALSE);
                }
#endif
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Disabling of MSTP Module Success\n");
            }

            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                /* Release memory blocks on all ports */
                MstRedClearAllBpdusOnActive ();

                AstRedSendSyncMessages (RST_DEFAULT_INSTANCE, 0,
                                        RED_AST_CLEAR_ALL_SYNCUP_DATA, 0);
            }

#else
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_MGMT_DBG, "MSG: MSTP module not present !!!\n");
#endif /* MSTP_WANTED */
            break;
        case AST_DISABLE_PVRST_MSG:

            if (!(AST_IS_PVRST_STARTED ()))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: PVRST System Shutdown; CANNOT Disable PVRST\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: PVRST System Shutdown; CANNOT Disable PVRST\n");
                i4RetVal = RST_SUCCESS;
                break;
            }
            if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_DISABLED)
                && (AST_ADMIN_STATUS == (UINT1) PVRST_DISABLED))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                         "MSG: PVRST Module is already Disabled\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                         "MSG: PVRST Module is already Disabled\n");
                i4RetVal = RST_SUCCESS;
                break;
            }

#ifdef PVRST_WANTED
            if (PvrstModuleDisable () != PVRST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PvrstModuleDisable function returned FAILURE\n");
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                         "MSG: PvrstModuleDisable function returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
            }
            else
            {
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                         AST_MGMT_DBG,
                         "MSG: Management Disabling of PVRST Module Success\n");
            }

            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                /* Release memory blocks on all ports */
                PvrstRedClearAllBpdusOnActive ();

                AstRedSendSyncMessages (1, 0, RED_AST_CLEAR_ALL_SYNCUP_DATA, 0);
            }
#else
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_MGMT_DBG, "MSG: PVRST module not present !!!\n");
#endif /* PVRST_WANTED */
            break;
        case AST_ALL_PORT_DISABLE_MSG:
            if (AST_IS_RST_STARTED ())
            {
                if (RstModuleDisable () != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: RstModuleDisable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstModuleDisable function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstModuleDisable () != MST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: MstModuleDisable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstModuleDisable function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstModuleDisable () != PVRST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: PvrstModuleDisable function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstModuleDisable function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
#endif /* PVRST_WANTED */
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                     "MSG: Management SET for All Ports Disable Success\n");
            break;

        case AST_DELETE_CONTEXT_MSG:

            AstHandleDeleteContext (pMsgNode->u4ContextId);

            L2MI_SYNC_GIVE_SEM ();

            AST_GLOBAL_TRC (pMsgNode->u4ContextId, AST_INIT_SHUT_TRC,
                            "MSG: Context deleted successfully...\n");
            AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                            AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                            "MSG: Context deleted successfully...\n");

            break;

#ifdef PVRST_WANTED
        case AST_PVRST_VLAN_CREATED_MSG:
        {
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstCreateInstance (pMsgNode->u2InstanceId) !=
                    PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Create Instance function returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Create Instance function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
                AST_GLOBAL_TRC (pMsgNode->u4ContextId, AST_INIT_SHUT_TRC,
                                "MSG: Instance Created successfully...\n");
                AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                                "MSG: Instance Created successfully...\n");
            }
        }
            break;
        case AST_PVRST_VLAN_DELETE_MSG:
        {
            if (AST_IS_PVRST_STARTED ())
            {
                u2InstIndex = AstL2IwfMiGetVlanInstMapping
                    (AST_CURR_CONTEXT_ID (), pMsgNode->u2InstanceId);

                if (u2InstIndex == INVALID_INDEX)
                {
                    i4RetVal = (INT4) RST_FAILURE;
                    break;
                }
                if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
                {
                    i4RetVal = (INT4) RST_FAILURE;
                    break;
                }
                if (PvrstDeleteInstance (pMsgNode->u2InstanceId) !=
                    PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Delete Instance function returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Delete Instance function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
                AST_GLOBAL_TRC (pMsgNode->u4ContextId, AST_INIT_SHUT_TRC,
                                "MSG: Instance Created successfully...\n");
                AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                                "MSG: Instance Created successfully...\n");
            }
        }
            break;
        case AST_PVRST_ADD_TRUNK_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstAddTrunkPort (pMsgNode->uMsg.u2LocalPortId) !=
                    PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Enable port function returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Enable port function returned FAILURE\n");

                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;
        case AST_PVRST_ADD_ACCESS_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstAddAccessPort
                    (pMsgNode->uMsg.u2LocalPortId,
                     pMsgNode->u2InstanceId) != PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Disable port function returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Disable port function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        case AST_PVRST_CONFIG_STATUS_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstConfigPvrstStatusPerPort
                    (pMsgNode->u4PortNo,
                     pMsgNode->uMsg.bStatus) != PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Disable port function returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Disable port function returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        case AST_PVRST_SET_ENCAP_TYPE_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                {
                    tAstCommPortInfo   *pAstCommPortInfo =
                        AST_GET_COMM_PORT_INFO (pMsgNode->u4PortNo);

                    pAstCommPortInfo->eEncapType = pMsgNode->uMsg.eEncapType;
                }
            }
            break;

        case AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstSetPortBPDUGuard (pMsgNode->u4PortNo,
                                                  AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG,
                                                  pMsgNode->uMsg.
                                                  u4BpduGuardStatus);
            }
            break;
        case AST_PVRST_BPDUGUARD_ACTION_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstSetPortBPDUGuardAction (pMsgNode->u4PortNo,
                                                        pMsgNode->uMsg.
                                                        u1BpduGuardAction);
            }
            break;

        case AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstSetPortGuards (pMsgNode->u4PortNo,
                                    AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG,
                                    pMsgNode->uMsg.bStatus);
            }
            break;

        case AST_PVRST_CONFIG_INST_STATUS_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstConfigPvrstInstStatusPerPort
                    (pMsgNode->u2InstanceId, pMsgNode->u4PortNo,
                     pMsgNode->uMsg.bStatus) != PVRST_SUCCESS)
                {
                    AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                    "MSG: Configuration of InstStatusPerPort returned FAILURE\n");
                    AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                    AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                    "MSG: Configuration of InstStatusPerPort returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }
            break;

        case AST_PVRST_HELLOTIME_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstSetHelloTime (pMsgNode->u2InstanceId,
                                   pMsgNode->uMsg.u2HelloTime);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Hello Time as %d Success\n",
                              pMsgNode->uMsg.u2HelloTime);
            }
            break;

        case AST_PVRST_TX_HOLDCOUNT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstSetTxHoldCount (pMsgNode->uMsg.u1TxHoldCount,
                                     pMsgNode->u2InstanceId);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Tx Hold Count as %d Success\n",
                              pMsgNode->uMsg.u1TxHoldCount);
            }
            break;

        case AST_PVRST_FORWARDDELAY_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstSetForwardDelay (pMsgNode->u2InstanceId,
                                      pMsgNode->uMsg.u2ForwardDelay);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Forward Delay as %d Success\n",
                              pMsgNode->uMsg.u2ForwardDelay);
            }
            break;

        case AST_PVRST_BRG_MAXAGE_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstSetBrgMaxAge (pMsgNode->u2InstanceId,
                                   pMsgNode->uMsg.u2MaxAge);
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Management SET of Bridge Max Age as %d Success\n",
                              pMsgNode->uMsg.u2MaxAge);
            }
            break;
        case AST_PVRST_PVID_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                if (PvrstChangePVID (pMsgNode->u4PortNo, pMsgNode->u2InstanceId,
                                     pMsgNode->uMsg.VlanId) == PVRST_FAILURE)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: PvrstChangePVID function returned FAILURE\n");
                    i4RetVal = RST_FAILURE;
                }
                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG,
                              "MSG: Change of PVID on Port %d Success\n",
                              pMsgNode->uMsg.IfaceId);
            }
            break;

        case AST_PVRST_ALL_VLAN_DELETE_MSG:
        {
            if (AST_IS_PVRST_STARTED ())
            {
                for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
                {
                    if (AstL2IwfMiIsVlanActive (pMsgNode->u4ContextId,
                                                VlanId) == OSIX_FALSE)
                    {
                        continue;
                    }
                    if (PvrstDeleteInstance (pMsgNode->u2InstanceId) !=
                        PVRST_SUCCESS)
                    {
                        AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                        AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                        "MSG: Delete Instance function returned FAILURE\n");
                        AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                        AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                        "MSG: Delete Instance function returned FAILURE\n");
                        i4RetVal = (INT4) RST_FAILURE;
                    }
                }
                AST_GLOBAL_TRC (pMsgNode->u4ContextId, AST_INIT_SHUT_TRC,
                                "MSG: Instances Deleted successfully...\n");
                AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                                "MSG: Instance Deleted successfully...\n");
            }
        }
            break;
#endif
        case AST_UPDATE_CONTEXT_NAME:

            if (AstHandleUpdateContextName
                (pMsgNode->u4ContextId) != RST_SUCCESS)
            {
                AST_GLOBAL_TRC (pMsgNode->u4ContextId,
                                AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                "MSG: UpdateContextName function "
                                "returned FAILURE\n");
                AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                                AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                "MSG: UpdateContextName function "
                                "returned FAILURE\n");
                i4RetVal = (INT4) RST_FAILURE;
                break;
            }

            AST_GLOBAL_TRC (pMsgNode->u4ContextId, AST_INIT_SHUT_TRC,
                            "MSG: Context name updated successfully...\n");
            AST_GLOBAL_DBG (pMsgNode->u4ContextId,
                            AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                            "MSG: Context name updated successfully...\n");

            break;

        case AST_HW_PEP_CREATE_MSG:
            i4RetVal = AstRedHandleHwPepCreate (pMsgNode);
            break;

#ifdef NPAPI_WANTED
        case AST_HW_PORTSTATE_UPDATED_MSG:
            pPortEntry = AST_GET_PORTENTRY (pMsgNode->u4PortNo);
            if (pPortEntry == NULL)
            {
                AST_TRC (AST_ALL_FAILURE_TRC,
                         "MGMT:HW Port Status Received for Invalid Port!\n");
                i4RetVal = RST_FAILURE;
                break;
            }

            if (pMsgNode->uMsg.AstNpapiCbParams.CallId ==
                AS_FS_MI_RSTP_NP_SET_PORT_STATE)
            {
                FsMiRstpNpSetPortStateVal =
                    pMsgNode->uMsg.AstNpapiCbParams.AstNpapiParams.
                    FsMiRstpNpSetPortState;

                if (AstHwPortStateUpdtInd (pPortEntry,
                                           RST_DEFAULT_INSTANCE,
                                           FsMiRstpNpSetPortStateVal.u1Status,
                                           FsMiRstpNpSetPortStateVal.rval)
                    != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }

                AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                              AST_MGMT_DBG,
                              "MSG: Management Enabling of Port %u Success\n",
                              (pMsgNode->u4PortNo));
                break;
            }

            else if (pMsgNode->uMsg.AstNpapiCbParams.CallId ==
                     AS_FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE)
            {
                FsMiMstpNpSetInstancePortStateVal =
                    pMsgNode->uMsg.AstNpapiCbParams.AstNpapiParams.
                    FsMiMstpNpSetInstancePortState;

                if (AstHwPortStateUpdtInd (pPortEntry,
                                           FsMiMstpNpSetInstancePortStateVal.
                                           u2InstanceId,
                                           FsMiMstpNpSetInstancePortStateVal.
                                           u1PortState,
                                           FsMiMstpNpSetInstancePortStateVal.
                                           rval) != RST_SUCCESS)

                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;
                }
            }

#ifdef PVRST_WANTED
            else

            {
                FsMiPvrstNpSetVlanPortStateVal =
                    pMsgNode->uMsg.AstNpapiCbParams.AstNpapiParams.
                    FsMiPvrstNpSetVlanPortState;
                u2InstIndex =
                    AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                  FsMiPvrstNpSetVlanPortStateVal.
                                                  VlanId);

                if (AstHwPortStateUpdtInd (pPortEntry, u2InstIndex,
                                           FsMiPvrstNpSetVlanPortStateVal.
                                           u1PortState,
                                           FsMiPvrstNpSetVlanPortStateVal.rval)
                    != RST_SUCCESS)

                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: AstHwPortStateUpdtInd returned FAILURE\n");
                    i4RetVal = (INT4) RST_FAILURE;

                }
            }
#endif /* PVRST_WANTED */
#endif

#ifdef PBB_WANTED
        case AST_UPDT_PORT_TYPE_MSG:
            i4RetVal = AstHandleUpdateIntfType (pMsgNode);
            break;
#endif /* PBB_WANTED */

#ifdef MSTP_WANTED
        case AST_SISP_STATUS_MSG:

            i4RetVal = MstSispHandleUpdateSispStatus (pMsgNode);
            break;

        case AST_SISP_VLAN_UPDT_MSG:

            i4RetVal = MstSispHandleUpdateVlanPortList (pMsgNode);
            break;
#endif

        case AST_PORT_SPEED_CHANGE_MSG:
            AstHandlePortSpeedChgIndication (pMsgNode->u4PortNo);
            break;
        case AST_RST_CONFIG_BPDUGUARD_PORT_MSG:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstSetPortBPDUGuard (pMsgNode->u4PortNo,
                                                AST_RST_CONFIG_BPDUGUARD_PORT_MSG,
                                                pMsgNode->uMsg.
                                                u4BpduGuardStatus);
            }
            break;
        case AST_RST_BPDUGUARD_ACTION_MSG:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstSetPortBPDUGuardAction (pMsgNode->u4PortNo,
                                                      pMsgNode->uMsg.
                                                      u1BpduGuardAction);
            }
            break;
#ifdef MSTP_WANTED
        case AST_MST_CONFIG_BPDUGUARD_PORT_MSG:

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstSetPortBPDUGuard (pMsgNode->u4PortNo,
                                                AST_MST_CONFIG_BPDUGUARD_PORT_MSG,
                                                pMsgNode->uMsg.
                                                u4BpduGuardStatus);
            }
            break;
        case AST_MST_BPDUGUARD_ACTION_MSG:

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstSetPortBPDUGuardAction (pMsgNode->u4PortNo,
                                                      pMsgNode->uMsg.
                                                      u1BpduGuardAction);
            }
            break;

#endif
        case AST_GBL_BPDUGUARD_ENABLE_MSG:

            if (!
                (AST_IS_RST_STARTED () || AST_IS_MST_STARTED ()
                 || AST_IS_PVRST_STARTED ()))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: Spanning-tree in shutdown state; cannot enable Global BPDUGuard\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: Spanning-tree in shutdown state; cannot enable Global BPDUGuard\n");
                i4RetVal = RST_FAILURE;
                break;
            }
            i4RetVal = AstSetGlobalBPDUGuard (pMsgNode->uMsg.u4BpduGuardStatus);
            break;

        case AST_GBL_BPDUGUARD_DISABLE_MSG:

            if ((!AST_IS_RST_STARTED ()) && (!AST_IS_MST_STARTED ())
                && (!AST_IS_PVRST_STARTED ()))
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: Spanning-tree in shutdown state; cannot disable Global BPDUGuard\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "MSG: Spanning-tree in shutdown state; cannot disable Global BPDUGuard\n");
                i4RetVal = RST_FAILURE;
                break;
            }
            i4RetVal = AstSetGlobalBPDUGuard (pMsgNode->uMsg.u4BpduGuardStatus);
            break;
#ifdef PVRST_WANTED
        case AST_PVRST_ADD_TAGGED_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                /* Iterate over member ports and add it to vlan for each port */
                AstConvertPortListToArray (pMsgNode->TaggedPorts,
                                           u4PortListArray, &u1NumPorts);
                for (u1Index = 0; u1Index < u1NumPorts; u1Index++)
                {
                    PvrstAddTaggedPort (u4PortListArray[u1Index],
                                        pMsgNode->u2InstanceId);
                }
            }
            break;
        case AST_PVRST_DELETE_TAGGED_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {

                /* Iterate over member ports and delete if from vlan for each port */
                AstConvertPortListToArray (pMsgNode->TaggedPorts,
                                           u4PortListArray, &u1NumPorts);
                for (u1Index = 0; u1Index < u1NumPorts; u1Index++)
                {
                    /*If the port type is hybrid port and vlan is default vlan,
                     *block the deletion. This is done to be complaince with CISCO
                     *Scenario:If no ports is issued for default vlan, PVRST instance
                     *running for hybrid port need not be deleted */
                    AstL2IwfGetVlanPortType ((UINT2) u4PortListArray[u1Index],
                                             &u1PortType);
                    if (u1PortType == VLAN_HYBRID_PORT)
                    {
                        if (AST_DEF_VLAN_ID () == pMsgNode->u2InstanceId)
                        {
                            continue;
                        }
                    }
                    if (PvrstPerStPortDelete
                        (u4PortListArray[u1Index],
                         pMsgNode->u2InstanceId) == PVRST_FAILURE)
                    {
                        return RST_FAILURE;
                    }
                }
            }
            break;
        case AST_PVRST_HYBRID_PORT_MSG:
            if (AST_IS_PVRST_STARTED ())
            {
                PvrstAddHybridPort (pMsgNode->uMsg.u2LocalPortId);
            }
            break;
#endif
        default:
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                     AST_ALL_FAILURE_DBG, "MSG: Invalid Message Type\n");
            break;

    }                            /* End of Switch Block */

    if (i4RetVal == (INT4) RST_SUCCESS)
    {
        if (AST_CURR_CONTEXT_INFO ())
        {
            AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
                     "MSG: Successfully processed received local message ...\n");
        }
        else
        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MGMT_DBG |
                            AST_EVENT_HANDLING_DBG,
                            "MSG: Successfully processed received local message ...\n");
        }
    }
#ifdef PVRST_WANTED
    UNUSED_PARAM (i4Val);
#endif
    return (i4RetVal);

}

/*****************************************************************************/
/* Function Name      : AstValidateReceivedBpdu                              */
/*                                                                           */
/* Description        : This function validates the received BPDU and thus   */
/*                      approves it for further processing.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstValidateReceivedBpdu (UINT2 u2PortNum, UINT2 u2DataLength, UINT1 *pu1Bpdu,
                         tAstBpduType * pBpdu)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstBridgeEntry    *pBrgEntry = NULL;
    tAstBridgeId        OwnBrgId;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT2               u2PortId = (UINT2) AST_INIT_VAL;
    UINT2               u2BpduPortId = (UINT2) AST_INIT_VAL;
    UINT2               u2ProtocolId = (UINT2) AST_INIT_VAL;
    UINT2               u2MaxAge = (UINT2) AST_INIT_VAL;
    UINT2               u2FwdDelay = (UINT2) AST_INIT_VAL;
    UINT2               u2HelloTime = (UINT2) AST_INIT_VAL;
#ifdef PVRST_WANTED
    UINT2               u2InstanceId = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif
    UINT1               u1Version = (UINT1) AST_INIT_VAL;
    UINT1               u1BpduType = (UINT1) AST_INIT_VAL;

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "MSG: Port %s: Validating received bpdu ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                  "MSG: Port %s: Validating received BPDU ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_STARTED ())
    {
        u2InstanceId = pBpdu->uBpdu.PvrstBpdu.VlanId;
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    u2InstanceId);
        if (u2InstIndex == INVALID_INDEX)
        {
            return RST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            return RST_FAILURE;
        }

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (pPerStPortInfo == NULL)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "Port %s DOES NOT exist in spanning tree context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "Port %s DOES NOT exist in spanning tree  context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }

        if (pPerStPortInfo->PerStRstPortInfo.bPortEnabled == PVRST_FALSE)
        {
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Port %s Not Enabled...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_SUCCESS;
        }
    }
#endif

    /* Copying the Protocol Id, Protocol Version Id and BPDU Type */
    AST_GET_2BYTE (u2ProtocolId, pu1Bpdu);
    AST_GET_1BYTE (u1Version, pu1Bpdu);
    AST_GET_1BYTE (u1BpduType, pu1Bpdu);

/* Check for valid octet count in Length/Type field of Ethernet Header*/
    if (AST_IS_RST_STARTED () || AST_IS_MST_STARTED ())
    {
        if (u1BpduType == AST_BPDU_TYPE_CONFIG)
        {
            AST_DBG (AST_BPDU_DBG, "MSG: STP Config BPDU Received !\n");
        }
        else if (u1BpduType == AST_BPDU_TYPE_RST)
        {
            AST_DBG (AST_BPDU_DBG, "MSG: RST BPDU Received !\n");
        }
        else if (u1BpduType == AST_BPDU_TYPE_TCN)
        {
            AST_DBG (AST_BPDU_DBG, "MSG: TCN BPDU Received !\n");
        }
#ifdef MSTP_WANTED
        if ((u2DataLength >= AST_MIN_BPDU_LENGTH_MST) &&
            (u2DataLength <= MST_MAX_BPDU_SIZE))

        {
            AST_DBG (AST_BPDU_DBG, "MSG: MST BPDU Received !\n");
        }
#endif
        else if ((u2DataLength < AST_BPDU_LENGTH_CONFIG)
                 && (u2DataLength != AST_BPDU_LENGTH_TCN))
        {
            AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                     "MSG:Invalid Length field in Ethernet Header..Discarding BPDU\n");

            AstIncrInvalidBpduCounter (u2PortNum, u1BpduType, u1Version);
            AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_PROTOCOL_ID,
                                   (UINT2) AST_INIT_VAL,
                                   (INT1 *) AST_BRG_TRAPS_OID,
                                   AST_BRG_TRAPS_OID_LEN);

            return RST_FAILURE;
        }
    }

    /* Validation for allowed valid values of Protocol Identifier and 
     * Protocol Version fields */
    if (u2ProtocolId != AST_PROTOCOL_ID)
    {
        AstIncrInvalidBpduCounter (u2PortNum, u1BpduType, u1Version);
        AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_PROTOCOL_ID,
                               u2ProtocolId, (INT1 *) AST_BRG_TRAPS_OID,
                               AST_BRG_TRAPS_OID_LEN);
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "MSG: Port %s: BPDU with Invalid Protocol & -\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "Version received\n");
        AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                      "MSG: Port %s: BPDU with Invalid Protocol & -\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG (AST_BPDU_DBG | AST_ALL_FAILURE_DBG, "Version received\n");
        return RST_FAILURE;
    }

    /* Validations for Protocol Version and BPDU Type Combinations */
    switch (u1BpduType)
    {
        case AST_BPDU_TYPE_CONFIG:

            /* The check for version identifier has been removed as part of standard IEEE
             * 802.1D 2004 clause 9.3.4 and IEEE 802.1Q 2005 clause 14.4 where protocol
             * version identifier is not considered while validating the received
             * CONFIG BPDU */
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "MSG: Port %s: Config BPDU received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "MSG: Port %s: Config BPDU received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            break;

        case AST_BPDU_TYPE_TCN:

            /* The check for version identifier has been removed as part of standard IEEE
             * 802.1D 2004 clause 9.3.4 and IEEE 802.1Q 2005 clause 14.4 where protocol
             * version identifier is not considered while validating the received
             * TCN BPDU */

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "MSG: Port %s: TCN BPDU received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "MSG: Port %s: TCN BPDU received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            break;

        case AST_BPDU_TYPE_RST:
            if ((u1Version < AST_VERSION_2)
                || (AST_FORCE_VERSION < AST_VERSION_2))
            {
                /* Incrementing the Invalid RST BPDU Received Count */
                if (AST_IS_RST_ENABLED () || AST_IS_PVRST_ENABLED ())
                {
                    AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                }
#ifdef MSTP_WANTED
                else if (AST_IS_MST_ENABLED ())
                {
                    if (AST_FORCE_VERSION <= AST_VERSION_2)
                    {
                        /* All Bpdus, irrespective of version, to be considered 
                         * as RST bpdus */
                        AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                    }
                    else
                    {
                        AST_INCR_INVALID_MST_BPDU_COUNT (RST_DEFAULT_INSTANCE,
                                                         u2PortNum);
                    }
                }
#endif
                AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_PROTOCOL_ID,
                                       u2ProtocolId,
                                       (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "MSG: Port %s: Rst BPDU with Invalid Version received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: Port %s: Rst BPDU with Invalid Version received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            break;
    }

    /* Validations for BPDU Length */
    switch (u1BpduType)
    {
        case AST_BPDU_TYPE_CONFIG:

            if (u2DataLength < AST_BPDU_LENGTH_CONFIG)
            {
                /* Incrementing the Invalid Config BPDU Received Count */
                AST_INCR_INVALID_CONFIG_BPDU_RXD_COUNT (u2PortNum);

                AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_CONFIG_LENGTH,
                                       u2DataLength, (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "MSG: Port %s: Config BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: Port %s: Config BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            u1Version = (UINT1) AST_INIT_VAL;
            break;

        case AST_BPDU_TYPE_TCN:

            if (u2DataLength < AST_BPDU_LENGTH_TCN)
            {
                /* Incrementing the Invalid TCN BPDU Received Count */
                AST_INCR_INVALID_TCN_BPDU_RXD_COUNT (u2PortNum);

                AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_TCN_LENGTH,
                                       u2DataLength, (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "MSG: Port %s: Tcn BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: Port %s: Tcn BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            u1Version = (UINT1) AST_INIT_VAL;
            break;

        case AST_BPDU_TYPE_RST:

            /* Both Version 2 BPDUs and Version 3 BPDUs (MST BPDUs) will fall
             * under this category */

            if (u2DataLength < AST_BPDU_LENGTH_RST)
            {
#ifdef MSTP_WANTED
                if (AST_IS_MST_ENABLED ())
                {
                    if (AST_FORCE_VERSION <= AST_VERSION_2)
                    {
                        /* All Bpdus, irrespective of version, to be considered 
                         * as RST bpdus */
                        AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                    }
                    else
                    {
                        if (u1Version == AST_VERSION_2)
                        {
                            /* Incrementing the Invalid RST BPDU Received 
                             * Count */
                            AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                        }
                        else
                        {
                            if (u2DataLength < AST_BPDU_LENGTH_CONFIG)
                            {
                                AST_INCR_INVALID_MST_BPDU_COUNT
                                    (RST_DEFAULT_INSTANCE, u2PortNum);
                            }
                            else
                            {
                                /* Version 3 bpdus with length as 35 octets
                                 * is considered valid and processed as an 
                                 * Rst bpdu */
                                break;
                            }
                        }
                    }
                }
#endif
                if (AST_IS_RST_ENABLED ())
                {
                    /* All Bpdus, irrespective of version, to be considered 
                     * as RST bpdus */
                    AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                }

#ifdef PVRST_WANTED
                if (AST_IS_PVRST_ENABLED ())
                {
                    /* Incrementing the Invalid PVRST BPDU Received Count */
                    AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                }
#endif
                AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_RST_LENGTH,
                                       u2DataLength, (INT1 *) AST_BRG_TRAPS_OID,
                                       AST_BRG_TRAPS_OID_LEN);

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "MSG: Port %s: Rst/Mst BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: Port %s: Rst/Mst BPDU of Invalid Length received\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }

            break;

        default:

            if (AST_IS_RST_ENABLED ())
            {
                /* Incrementing the Invalid RST BPDU Received Count */
                AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
            }
#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {
                if (AST_FORCE_VERSION <= AST_VERSION_2)
                {
                    /* All Bpdus, irrespective of version, to be considered 
                     * as RST bpdus */
                    AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                }
                else
                {
                    /* Incrementing the Invalid MST BPDU Received Count */
                    AST_INCR_INVALID_MST_BPDU_COUNT (RST_DEFAULT_INSTANCE,
                                                     u2PortNum);
                }
            }
#endif
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                /* Incrementing the Invalid PVRST BPDU Received Count */
                AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
            }
#endif
            AstInvalidBpduRxdTrap (u2PortNum, AST_INVALID_BPDU_TYPE,
                                   u1BpduType, (INT1 *) AST_BRG_TRAPS_OID,
                                   AST_BRG_TRAPS_OID_LEN);

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "MSG: Port %s: BPDU with Invalid BPDU Type received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "MSG: Port %s: BPDU with Invalid BPDU Type received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;

    }                            /* End of switch */

    /* With the above checks, all validations for Invalid BPDUs have been 
     * performed.
     *
     * Henceforth, the only checks that need to be done are those which may
     * result in the classification of the received BPDUs as those of a lower 
     * version. */

    /* First, we extract all the relevant fields from the BPDU */
    /* Checks resulting in the classification of the received BPDUs as those
     * of a lower version are done inside this function */
    AST_DBG_ARG1 (AST_BPDU_DBG,
                  "MSG: Port %s: Extracting Info from BPDU ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AstExtractReceivedBpdu (u2ProtocolId, u1Version, u1BpduType, u2DataLength,
                            pu1Bpdu, pBpdu);

    /* Now performing validations for Loopback BPDUs and Times values
     * present in the BPDUs */

    /* Getting the value of u1Version and u1BpduType again because its value
     * may have changed in the AstExtractReceivedBpdu function */
    if (AST_IS_RST_ENABLED ())
    {
        u1Version = pBpdu->uBpdu.RstBpdu.u1Version;
        u1BpduType = pBpdu->uBpdu.RstBpdu.u1BpduType;
        u2MaxAge = pBpdu->uBpdu.RstBpdu.u2MaxAge;
        u2FwdDelay = pBpdu->uBpdu.RstBpdu.u2FwdDelay;
        u2HelloTime = pBpdu->uBpdu.RstBpdu.u2HelloTime;
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        u1Version = pBpdu->uBpdu.MstBpdu.u1Version;
        u1BpduType = pBpdu->uBpdu.MstBpdu.u1BpduType;
        u2MaxAge = pBpdu->uBpdu.MstBpdu.u2MaxAge;
        u2FwdDelay = pBpdu->uBpdu.MstBpdu.u2FwdDelay;
        u2HelloTime = pBpdu->uBpdu.MstBpdu.u2HelloTime;
    }
#endif
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_ENABLED ())
    {
        u1Version = pBpdu->uBpdu.PvrstBpdu.u1Version;
        u1BpduType = pBpdu->uBpdu.PvrstBpdu.u1BpduType;
        u2MaxAge = pBpdu->uBpdu.PvrstBpdu.u2MaxAge;
        u2FwdDelay = pBpdu->uBpdu.PvrstBpdu.u2FwdDelay;
        u2HelloTime = pBpdu->uBpdu.PvrstBpdu.u2HelloTime;
    }
#endif

    /* Check for loopback, Message Age > Max Age should be done as part of
     * bpdu validation for Config bpdus */
    if (u1BpduType == AST_BPDU_TYPE_CONFIG)
    {
        pBrgEntry = AST_GET_BRGENTRY ();
        if (AST_IS_PVRST_ENABLED ())
        {
#ifdef PVRST_WANTED
            u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                        u2InstanceId);
            if (INVALID_INDEX == u2InstIndex)
            {
                return RST_FAILURE;
            }
            pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
#endif
        }
        else
        {
            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
        }
        if (pPerStBrgInfo == NULL)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "Port %s DOES NOT exist in spanning tree context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                          "Port %s DOES NOT exist in spanning tree  context ...Discarding BPDU\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }

        /* Getting the Bridge Id of this Bridge */
        AST_MEMCPY (&(OwnBrgId.BridgeAddr), &(pBrgEntry->BridgeAddr),
                    AST_MAC_ADDR_SIZE);
        OwnBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

        /* Checking if the Bridge Id parameter from the received 
         * BPDU matches the Bridge Id that would be transmitted 
         * in a BPDU from this Port */
        if (AST_IS_RST_ENABLED ())
        {
            i4RetVal =
                RstCompareBrgId (&(pBpdu->uBpdu.RstBpdu.DesgBrgId),
                                 &(OwnBrgId));
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            i4RetVal =
                RstCompareBrgId (&(pBpdu->uBpdu.MstBpdu.CistBridgeId),
                                 &(OwnBrgId));
        }
#endif
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            /* Getting the Bridge Id of this Bridge */
            AST_MEMCPY (&(OwnBrgId.BridgeAddr), &(pBrgEntry->BridgeAddr),
                        AST_MAC_ADDR_SIZE);
            OwnBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

            i4RetVal =
                RstCompareBrgId (&(pBpdu->uBpdu.PvrstBpdu.DesgBrgId),
                                 &(OwnBrgId));
        }
#endif
        if (i4RetVal == RST_BRGID1_SAME)
        {
            /* Checking if the Port Id parameter from the received 
             * BPDU matches the Port Id that would be transmitted 
             * in a BPDU from this Port */

            if (pPerStPortInfo == NULL)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "Port %s DOES NOT exist in spanning tree context ...Discarding BPDU\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "Port %s DOES NOT exist in spanning tree  context ...Discarding BPDU\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            u2PortId = (UINT2) pPerStPortInfo->u1PortPriority;
            u2PortId = (UINT2) (u2PortId << AST_PORTPRIORITY_BIT_OFFSET);

            u2PortId = (UINT2) (u2PortId |
                                (AST_GET_PROTOCOL_PORT
                                 (pPerStPortInfo->
                                  u2PortNo) & AST_PORTNUM_MASK));

            if (AST_IS_RST_ENABLED ())
            {
                u2BpduPortId = pBpdu->uBpdu.RstBpdu.u2PortId;
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                u2BpduPortId = pBpdu->uBpdu.MstBpdu.u2CistPortId;
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                u2BpduPortId = pBpdu->uBpdu.PvrstBpdu.u2PortId;
            }
#endif

            if (u2BpduPortId == u2PortId)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "MSG: Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }

    }

    if ((u1BpduType == AST_BPDU_TYPE_CONFIG)
        || (u1BpduType == AST_BPDU_TYPE_RST))
    {
        AST_MEMSET (&OwnBrgId, AST_MEMSET_VAL, sizeof (tAstBridgeId));

        /* Checking if the Relationship between Forward Delay and Max Age has been
         * ensured in the Forward Delay and Max Age values that have been 
         * received in the BPDU. If the relationship is not maintained, a message
         * alone will be displayed and the processing of the BPDU will continue. */

        if (!(AST_BRG_FWDDELAY_MAXAGE_STD (u2FwdDelay, u2MaxAge)))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "MSG: Port %s BPDU: Relationship b/w Forward Delay -\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_TRC (AST_CONTROL_PATH_TRC, "and Max Age NOT MAINTAINED\n");
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "MSG: Port %s BPDU: Relationship b/w Forward Delay -\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG (AST_BPDU_DBG, "and Max Age NOT MAINTAINED\n");
        }

        /* Checking if the Relationship between Hello Time and Max Age has been
         * ensured in the Hello Time and Max Age values that have been 
         * received in the BPDU. If the relationship is not maintained, a message
         * alone will be displayed and the processing of the BPDU will continue. */

        if (!(AST_BRG_HELLOTIME_MAXAGE_STD (u2HelloTime, u2MaxAge)))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "MSG: Port %s BPDU: Relationship b/w Hello Time and -\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_TRC (AST_CONTROL_PATH_TRC, "Max Age NOT MAINTAINED\n");
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "MSG: Port %s BPDU: Relationship b/w Hello Time and -\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG (AST_BPDU_DBG, "Max Age NOT MAINTAINED\n");
        }
    }
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                  "MSG: Port %s: Received BPDU successfully validated...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* If Valid bpdu received and enableBPDURx is false for this port
     * then do not increment bpdu count.
     */
    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ())
        || (AST_IS_PVRST_ENABLED ()))
    {
        if (AST_PORT_ENABLE_BPDU_RX (pPortInfo) == RST_FALSE)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                          "MSG: Port %s: BPDU RX Disabled/BPDU Filter Enabled.. Discarding BPDU..\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
    }
    switch (u1BpduType)
    {
        case AST_BPDU_TYPE_CONFIG:
            /* Incrementing the Config BPDU received count */
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                PVRST_INCR_RX_CONFIG_BPDU_COUNT (u2PortNum, u2InstIndex);
            }
            else
#endif
            {
                AST_INCR_RX_CONFIG_BPDU_COUNT (u2PortNum);
            }
            break;

        case AST_BPDU_TYPE_TCN:
            /* Incrementing the TCN BPDU received count */
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                PVRST_INCR_RX_TCN_BPDU_COUNT (u2PortNum, u2InstIndex);
            }
            else
#endif
            {
                AST_INCR_RX_TCN_BPDU_COUNT (u2PortNum);
            }
            break;

        case AST_BPDU_TYPE_RST:
            /* Both Version 2 BPDUs and Version 3 BPDUs (MST BPDUs) will fall
             * under this category */
#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {
                /* In the specific case of STP (version 0) and RSTP (version 2),
                 * as there is only a single RST BPDU type defined in version 2,
                 * and as the RST BPDU type is undefined in version 0, a version 0
                 * implementation will ignore all RST BPDUs. Sec. 9.3.4 in 802.1D,2004 */

                if (AST_FORCE_VERSION == AST_VERSION_0)
                {
                    return RST_SUCCESS;
                }
                else
                {
                    if (u1Version == AST_VERSION_2)
                    {
                        /* Incrementing the RST BPDU received count */
                        AST_INCR_RX_RST_BPDU_COUNT (u2PortNum);
                    }
                    else
                    {
                        /* Incrementing the MST BPDU received count */
                        AST_INCR_RX_MST_BPDU_COUNT (RST_DEFAULT_INSTANCE,
                                                    u2PortNum);
                    }
                }
            }
#endif
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                /* Incrementing the PVRST BPDU Received Count */
                PVRST_INCR_RX_INST_BPDU_COUNT (u2PortNum, u2InstIndex);
            }
#endif
            if (AST_IS_RST_ENABLED ())
            {
                if (AST_FORCE_VERSION == AST_VERSION_0)
                {
                    return RST_SUCCESS;
                }
                else
                {
                    /* Incrementing the RST BPDU received count */
                    AST_INCR_RX_RST_BPDU_COUNT (u2PortNum);
                }
            }
            break;

    }                            /* End of switch */

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstExtractReceivedBpdu                               */
/*                                                                           */
/* Description        : This function extracts all the fields from the       */
/*                      received BPDU and stores it in the RST BPDU structure*/
/*                      or the MST BPDU structure or the PVRST BPDU structure*/
/*                      depending on which module is enabled.                */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Id in the received BPDU      */
/*                      u1Version - Protocol Version Id in the received BPDU */
/*                      u1BpduType - BPDU Type in the received BPDU          */
/*                      u2DataLength - Length of the received BPDU           */
/*                      pBpdu - Pointer to the BPDU Union where received     */
/*                              information is to be stored                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstExtractReceivedBpdu (UINT2 u2ProtocolId, UINT1 u1Version, UINT1 u1BpduType,
                        UINT2 u2DataLength, UINT1 *pu1Bpdu,
                        tAstBpduType * pBpdu)
{
#ifdef MSTP_WANTED
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    tMstBpdu           *pMstBpdu = NULL;
    UINT2               u2Inst = AST_INIT_VAL;
    UINT2               u2BrgPriority = AST_INIT_VAL;
    UINT2               u2ActualVersion3Len = AST_INIT_VAL;
    UINT2               u2Value = AST_INIT_VAL;
    UINT1               u1MstiFlags = AST_INIT_VAL;
#endif
#ifdef PVRST_WANTED
    tPvrstBpdu         *pPvrstBpdu = NULL;
#endif
    tAstBpdu           *pRstBpdu = NULL;
    UINT1               u1Value = AST_INIT_VAL;
    UINT1               u1RcvdPortRole = AST_INIT_VAL;
    UINT2               u2Result = AST_INIT_VAL;

    if (AST_IS_RST_ENABLED ())
    {
        pBpdu->BpduType = AST_RST_MODE;

        pRstBpdu = &(pBpdu->uBpdu.RstBpdu);

        pRstBpdu->u2ProtocolId = u2ProtocolId;
        pRstBpdu->u1Version = u1Version;
        pRstBpdu->u1BpduType = u1BpduType;

        if (u1BpduType == AST_BPDU_TYPE_TCN)
        {
            return;
        }

        /* As mentioned in Sec 9.3.4 of 802.1D 2004, all bpdus with higher 
         * version to be processed as if they carry the current supported 
         * version number */
        if (pRstBpdu->u1Version > AST_VERSION_2)
        {
            pRstBpdu->u1Version = AST_VERSION_2;
        }

        /* BPDU is either of Type Config or Type Rst (i.e. either Config
         * or RST BPDU). Hence extracting necessary information */

        /* Copying all the Flags information */
        AST_GET_1BYTE (pRstBpdu->u1Flags, pu1Bpdu);

        /* Copying the Root Id */
        AST_GET_2BYTE (pRstBpdu->RootId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pRstBpdu->RootId.BridgeAddr, pu1Bpdu);

        AST_GET_4BYTE (pRstBpdu->u4RootPathCost, pu1Bpdu);

        /* Copying the Bridge Id */
        AST_GET_2BYTE (pRstBpdu->DesgBrgId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pRstBpdu->DesgBrgId.BridgeAddr, pu1Bpdu);

        /* Copying the Port Id */
        AST_GET_2BYTE (pRstBpdu->u2PortId, pu1Bpdu);

        /* Copying all the Times parameters */
        /* Extracting 2 bytes and converting into centi-seconds
         * for Message age, Max age, Hello time and Fwd Delay.
         */
        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pRstBpdu->u2MessageAge = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pRstBpdu->u2MaxAge = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pRstBpdu->u2HelloTime = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pRstBpdu->u2FwdDelay = u2Result;
        /* Consider any RST BPDU received with Unknown Port Role as a 
         * Configuration BPDU */
        if ((pRstBpdu->u1Version == AST_VERSION_2) &&
            (pRstBpdu->u1BpduType == AST_BPDU_TYPE_RST))
        {
            u1RcvdPortRole =
                (UINT1) (pRstBpdu->u1Flags & RST_FLAG_MASK_PORT_ROLE);
            if (u1RcvdPortRole == RST_SET_UNKNOWN_PROLE_FLAG)
            {
                pRstBpdu->u1Version = AST_VERSION_0;
                pRstBpdu->u1BpduType = AST_BPDU_TYPE_CONFIG;
            }
        }

        if (pRstBpdu->u1BpduType == AST_BPDU_TYPE_RST)
        {
            /* Copying the Version 1 Length */
            AST_GET_1BYTE (pRstBpdu->u1Version1Len, pu1Bpdu);
        }
    }                            /* End of RST Module Enabled check */

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        pBpdu->BpduType = AST_MST_MODE;

        pMstBpdu = &(pBpdu->uBpdu.MstBpdu);

        pMstBpdu->u2ProtocolId = u2ProtocolId;
        pMstBpdu->u1Version = u1Version;
        pMstBpdu->u1BpduType = u1BpduType;

        if (u1BpduType == AST_BPDU_TYPE_TCN)
        {
            return;
        }

        /* As mentioned in Sec 9.3.4 of 802.1D 2004, all bpdus with higher 
         * version to be processed as if they carry the current supported 
         * version number */
        if (pMstBpdu->u1Version > AST_VERSION_3)
        {
            pMstBpdu->u1Version = AST_VERSION_3;
        }

        if ((u1Version == AST_VERSION_3) && (u1BpduType == AST_BPDU_TYPE_RST))
        {
            if ((u2DataLength >= AST_BPDU_LENGTH_CONFIG) &&
                (u2DataLength < AST_MIN_BPDU_LENGTH_MST))
            {
                pMstBpdu->u1Version = AST_VERSION_2;
                pMstBpdu->u1BpduType = AST_BPDU_TYPE_RST;
            }
        }

        /* BPDU is either of Type Config or Type Rst (i.e. either Config,
         * RST or MST BPDU). Hence extracting necessary information */

        /* Copying all the Flags information */
        AST_GET_1BYTE (pMstBpdu->u1CistFlags, pu1Bpdu);

        /* Consider any RST BPDU received with Unknown Port Role as a 
         * Configuration BPDU */
        if ((pMstBpdu->u1Version == AST_VERSION_2) &&
            (pMstBpdu->u1BpduType == AST_BPDU_TYPE_RST))
        {
            u1RcvdPortRole =
                (UINT1) (pMstBpdu->u1CistFlags & MST_FLAG_MASK_PORT_ROLE);
            if (u1RcvdPortRole == RST_SET_UNKNOWN_PROLE_FLAG)
            {
                pMstBpdu->u1Version = AST_VERSION_0;
                pMstBpdu->u1BpduType = AST_BPDU_TYPE_CONFIG;
            }
        }
        /* Copying the Root Id */
        AST_GET_2BYTE (pMstBpdu->CistRootId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pMstBpdu->CistRootId.BridgeAddr, pu1Bpdu);

        AST_GET_4BYTE (pMstBpdu->u4CistExtPathCost, pu1Bpdu);

        if (pMstBpdu->u1Version < AST_VERSION_3)
        {
            AST_GET_2BYTE (pMstBpdu->CistBridgeId.u2BrgPriority, pu1Bpdu);
            AST_GET_MAC_ADDR (pMstBpdu->CistBridgeId.BridgeAddr, pu1Bpdu);
            pMstBpdu->CistRegionalRootId = pMstBpdu->CistBridgeId;
        }
        else                    /* Version 3 (MSTP) BPDUs */
        {
            /* MSTP BPDU - Copying CIST Regional Root Id */
            AST_GET_2BYTE (pMstBpdu->CistRegionalRootId.u2BrgPriority, pu1Bpdu);
            AST_GET_MAC_ADDR (pMstBpdu->CistRegionalRootId.BridgeAddr, pu1Bpdu);
        }

        /* Copying the Cist Port Id */
        AST_GET_2BYTE (pMstBpdu->u2CistPortId, pu1Bpdu);

        /* Copying all the Times parameters */
        /* Extracting 2 bytes and converting into centi-seconds
         * for Message age, Max age, Hello time and Fwd Delay.
         */
        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pMstBpdu->u2MessageAge = u2Result;
        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pMstBpdu->u2MaxAge = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pMstBpdu->u2HelloTime = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pMstBpdu->u2FwdDelay = u2Result;

        if (u2DataLength > AST_BPDU_LENGTH_CONFIG)
        {
            /* Copying the Version 1 Length */
            AST_GET_1BYTE (pMstBpdu->u1Version1Len, pu1Bpdu);
        }
        else
        {
            pMstBpdu->u1Version1Len = AST_INIT_VAL;
        }
        if ((u1Version >= AST_VERSION_3) && (u1BpduType == AST_BPDU_TYPE_RST))
        {
            if (pMstBpdu->u1Version1Len != AST_INIT_VAL)
            {
                pMstBpdu->u1Version = AST_VERSION_2;
                pMstBpdu->u1BpduType = AST_BPDU_TYPE_RST;
            }
        }
        if (pMstBpdu->u1Version != AST_VERSION_3)
        {
            /* According to recordTimes procedure & msgTimes definition in IEEE 802.1q 
             * 2005, the value of CistRemainingHops needs to be updated as MaxHops in case 
             * of reception of Config BPDU or RST BPDU without MST parameters*/
            pMstBpdu->u1CistRemainingHops = MST_DEFAULT_MAX_HOPCOUNT;
            return;
        }

        /* The received BPDU is a Version 3 MST BPDU. Hence copying additional
         * fields */
        /* Copying the Version 3 Length */
        AST_GET_2BYTE (pMstBpdu->u2Version3Len, pu1Bpdu);

        /* Copying MST Configuration Identifier */
        AST_GET_1BYTE (pMstBpdu->MstConfigIdInfo.u1ConfigurationId, pu1Bpdu);

        /* Copying MST Configuration Name */
        AST_GET_CONFIG_NAME (pMstBpdu->MstConfigIdInfo.au1ConfigName, pu1Bpdu);

        /* Copying MST Configuration Revision Level */
        AST_GET_2BYTE (pMstBpdu->MstConfigIdInfo.u2ConfigRevLevel, pu1Bpdu);

        /* Copying MST Configuration Digest */
        AST_GET_CONFIG_DIGEST (pMstBpdu->MstConfigIdInfo.
                               au1ConfigDigest, pu1Bpdu);

        /* Copying CIST Internal Root Path Cost */
        AST_GET_4BYTE (pMstBpdu->u4CistIntRootPathCost, pu1Bpdu);

        /* Copying CIST Bridge Id */
        AST_GET_2BYTE (pMstBpdu->CistBridgeId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pMstBpdu->CistBridgeId.BridgeAddr, pu1Bpdu);

        /* Copying CIST Remaining Hops */
        AST_GET_1BYTE (u1Value, pu1Bpdu);
        pMstBpdu->u1CistRemainingHops = u1Value;

        if ((pMstBpdu->u2Version3Len < MST_BPDU_CONFIG_INFO_LENGTH) ||
            (pMstBpdu->u2Version3Len > (MST_BPDU_CONFIG_INFO_LENGTH +
                                        (MST_BPDU_MSTI_LENGTH * AST_MAX_MSTI))))
        {
            pMstBpdu->u1Version = AST_VERSION_2;
            pMstBpdu->u1BpduType = AST_BPDU_TYPE_RST;
            /* Version 3 length should be atleast 64 and should not be 
             * greater than 1088 bytes. */
            AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: Version3Length is Small/Big! BPDU considered as RST BPDU\n");
            return;
        }

        if (pMstBpdu->u2Version3Len == MST_BPDU_CONFIG_INFO_LENGTH)
        {
            /* No MSTI Configuration Messages present in the received BPDU */
            AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: Version3Length is 64 bytes\n");
            return;
        }

        u2ActualVersion3Len = (UINT2) (u2DataLength - AST_BPDU_LENGTH_RST);
        /* Subtract the Version3Length field size */
        u2ActualVersion3Len -= MST_VERSION3LENGTH_FIELD_SIZE;

        /* MSTI Configuration Messages present in received BPDU */
        u2Value = (UINT2) (pMstBpdu->u2Version3Len -
                           MST_BPDU_CONFIG_INFO_LENGTH);

        u2Value = (UINT2) (u2Value % MST_BPDU_MSTI_LENGTH);

        if ((u2Value != AST_INIT_VAL) ||
            (pMstBpdu->u2Version3Len != u2ActualVersion3Len))
        {
            /* Integral Number of MSTI Config Msgs not present */
            pMstBpdu->u1Version = AST_VERSION_2;
            pMstBpdu->u1BpduType = AST_BPDU_TYPE_RST;
            AST_DBG (AST_BPDU_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: Version3Length is not VALID! BPDU considered as RST BPDU\n");
            return;
        }

        /* The received BPDU has an integral number of MSTI 
         * Configuration Messages, so extract the Instance 
         * information from each Configuration message */
        pMstBpdu->u2Version3Len = (UINT2) (pMstBpdu->u2Version3Len -
                                           MST_BPDU_CONFIG_INFO_LENGTH);
        while (pMstBpdu->u2Version3Len >= MST_BPDU_MSTI_LENGTH)
        {
            /* Copying MSTI Flags */
            AST_GET_1BYTE (u1MstiFlags, pu1Bpdu);

            /* Copying MSTI Regional Root Id */
            AST_GET_2BYTE (u2BrgPriority, pu1Bpdu);

            u2Inst = MST_GET_INSTANCE_ID (u2BrgPriority);

            if ((MST_IS_INSTANCE_VALID (u2Inst) == MST_FALSE)
                || (u2Inst >= AST_MAX_MST_INSTANCES))
            {
                pMstBpdu->u2Version3Len = (UINT2) (pMstBpdu->u2Version3Len -
                                                   MST_BPDU_MSTI_LENGTH);
                AST_DBG_ARG1 (AST_BPDU_DBG | AST_ALL_FAILURE_DBG,
                              "Instance %u: is INVALID Skipping...\n", u2Inst);
                continue;
            }

            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "Instance %u: is VALID Continuing...\n", u2Inst);

            /* Copying MSTI Flags */
            pMstBpdu->aMstConfigMsg[u2Inst].u1MstiFlags = u1MstiFlags;

            pMstBrgEntry = AST_GET_MST_BRGENTRY ();

            if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
            {
                MST_GET_EXT_BRIDGE_PRIORITY (u2BrgPriority,
                                             pMstBpdu->aMstConfigMsg[u2Inst].
                                             MstiRegionalRootId.u2BrgPriority);
            }
            else
            {
                MST_GET_BRIDGE_PRIORITY (u2BrgPriority,
                                         pMstBpdu->aMstConfigMsg[u2Inst].
                                         MstiRegionalRootId.u2BrgPriority);
            }
            AST_GET_MAC_ADDR (pMstBpdu->
                              aMstConfigMsg[u2Inst].
                              MstiRegionalRootId.BridgeAddr, pu1Bpdu);

            /* Copying MSTI Internal Root Path Cost */
            AST_GET_4BYTE (pMstBpdu->aMstConfigMsg[u2Inst].
                           u4MstiIntRootPathCost, pu1Bpdu);

            /* Copying MSTI Bridge Priority */
            AST_GET_1BYTE (pMstBpdu->aMstConfigMsg[u2Inst].
                           u1MstiBrgPriority, pu1Bpdu);

            /* Copying MSTI Port Priority */
            AST_GET_1BYTE (pMstBpdu->aMstConfigMsg[u2Inst].
                           u1MstiPortPriority, pu1Bpdu);

            /* Copying MSTI Remaining Hops */
            AST_GET_1BYTE (u1Value, pu1Bpdu);
            pMstBpdu->aMstConfigMsg[u2Inst].u1MstiRemainingHops = u1Value;
            pMstBpdu->aMstConfigMsg[u2Inst].u1ValidFlag = (UINT1) RST_TRUE;

            pMstBpdu->u2Version3Len = (UINT2) (pMstBpdu->u2Version3Len -
                                               MST_BPDU_MSTI_LENGTH);

        }                        /* End of while loop */
    }                            /* End of MST Module Enabled check */
#else
    UNUSED_PARAM (u2DataLength);
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_ENABLED ())
    {
        pBpdu->BpduType = AST_RST_MODE;

        pPvrstBpdu = &(pBpdu->uBpdu.PvrstBpdu);

        pPvrstBpdu->u2ProtocolId = u2ProtocolId;
        pPvrstBpdu->u1Version = u1Version;
        pPvrstBpdu->u1BpduType = u1BpduType;

        if (u1BpduType == AST_BPDU_TYPE_TCN)
        {
            return;
        }

        /* As mentioned in Sec 9.3.4 of 802.1D 2004, all bpdus with higher 
         * version to be processed as if they carry the current supported 
         * version number */
        if (pPvrstBpdu->u1Version > AST_VERSION_2)
        {
            pPvrstBpdu->u1Version = AST_VERSION_2;
        }

        /* BPDU is either of Type Config or Type Rst (i.e. either Config
         * or RST BPDU). Hence extracting necessary information */

        /* Copying all the Flags information */
        AST_GET_1BYTE (pPvrstBpdu->u1Flags, pu1Bpdu);

        /* Copying the Root Id */
        AST_GET_2BYTE (pPvrstBpdu->RootId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pPvrstBpdu->RootId.BridgeAddr, pu1Bpdu);

        AST_GET_4BYTE (pPvrstBpdu->u4RootPathCost, pu1Bpdu);

        /* Copying the Bridge Id */
        AST_GET_2BYTE (pPvrstBpdu->DesgBrgId.u2BrgPriority, pu1Bpdu);
        AST_GET_MAC_ADDR (pPvrstBpdu->DesgBrgId.BridgeAddr, pu1Bpdu);

        /* Copying the Port Id */
        AST_GET_2BYTE (pPvrstBpdu->u2PortId, pu1Bpdu);

        /* Copying all the Times parameters */
        /* Extracting 2 bytes and converting into centi-seconds
         * for Message age, Max age, Hello time and Fwd Delay.
         */
        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pPvrstBpdu->u2MessageAge = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pPvrstBpdu->u2MaxAge = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pPvrstBpdu->u2HelloTime = u2Result;

        AST_BPDU_TO_PROT_CENTI_SEC (u1Value, pu1Bpdu, u2Result);
        pPvrstBpdu->u2FwdDelay = u2Result;
        /* Consider any RST BPDU received with Unknown Port Role as a 
         * Configuration BPDU */
        if ((pPvrstBpdu->u1Version == AST_VERSION_2) &&
            (pPvrstBpdu->u1BpduType == AST_BPDU_TYPE_RST))
        {
            u1RcvdPortRole = (pPvrstBpdu->u1Flags & RST_FLAG_MASK_PORT_ROLE);
            if (u1RcvdPortRole == RST_SET_UNKNOWN_PROLE_FLAG)
            {
                pPvrstBpdu->u1Version = AST_VERSION_0;
                pPvrstBpdu->u1BpduType = AST_BPDU_TYPE_CONFIG;
            }
        }

        if (pPvrstBpdu->u1BpduType == AST_BPDU_TYPE_RST)
        {
            /* Copying the Version 1 Length */
            AST_GET_1BYTE (pPvrstBpdu->u1Version1Len, pu1Bpdu);
        }
    }                            /* End of RST Module Enabled check */
#endif /*PVRST_WANTED */
    UNUSED_PARAM (u2DataLength);
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstHwPortStateUpdtInd                                */
/*                                                                           */
/* Description        : This function calls the appropriate handling         */
/*                      function based on the status of the Npapi call       */
/*                                                                           */
/* Input(s)           : AstPortEntry - poniter to PortEntry                  */
/*                      AstContextInfo - pointer to Context Information      */
/*                      u2InstanceId - Id of the current Instance            */
/*                      u2HwPortState - Status of the Hardware               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHwPortStateUpdtInd (tAstPortEntry * pPortEntry, UINT2 u2InstanceId,
                       UINT2 u2HwPortState, UINT1 u1Status)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNo;

    u2PortNo = pPortEntry->u2PortNo;

    if (!(AST_IS_RST_ENABLED ()) && !(AST_IS_MST_ENABLED ()) &&
        !(AST_IS_PVRST_ENABLED ()))
    {
        /* If none of the spanning tree modules are enabled then do not handle
         * the callback */
        return RST_SUCCESS;
    }

    if (u2InstanceId == AST_TE_MSTID)
    {
        /* All ports will be statically in forwarding state for this instance.
         * So nothing to do in case of PTEID callback since -
         * 1) Neither will any state machine be running for this instance
         * 2) Nor will the L2Iwf contain information about this instance.
         */
        if (u1Status == FNP_FAILURE)
        {
            /* No failure retries for the PTEID instance */
            AstHwFailTrap (u2PortNo, u2InstanceId, u2HwPortState,
                           (INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gAstGlobalInfo.i4SysLogId,
                          "Setting of port state in hardware"
                          "failed for port %u instance %d",
                          AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId));
        }
        return RST_SUCCESS;
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return RST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    if (u1Status == FNP_SUCCESS)
    {
        AstHwPortStateUpdtSuccess (pPortEntry, u2InstanceId, u2HwPortState);
    }
    else
    {
        AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId, u2HwPortState);
    }

    return RST_SUCCESS;
}

#endif /*NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : AstHwPortStateUpdtSuccess                            */
/*                                                                           */
/* Description        : This function is called when the setting of portstate*/
/*                      in hardware is success it updtaes the status based on*/
/*                      the port state                                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : AstPortEntry - poniter to PortEntry                  */
/*                      u2InstanceId - Id of the current Instance            */
/*                      u2HwPortState - Status of the Hardware               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHwPortStateUpdtSuccess (tAstPortEntry * pPortEntry, UINT2 u2InstanceId,
                           UINT2 u2HwPortState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNo;

    u2PortNo = pPortEntry->u2PortNo;

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return RST_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    if (pRstPortInfo->bPortEnabled == RST_FALSE)
    {
        /* If the port is disabled in spanning tree then the Sem variables
         * and L2Iwf/GARP would have already been updated from STSM.
         * So nothing to do here */
        return RST_SUCCESS;
    }

    if (u2HwPortState == AST_PORT_STATE_DISCARDING)
    {
#ifdef MSTP_WANTED
        if ((AST_IS_MST_STARTED ()) &&
            (pPortEntry->u1EntryStatus == (UINT1) AST_PORT_OPER_UP))
        {
            MstTopologyChSmFlushFdb (pPerStPortInfo, u2InstanceId,
                                     VLAN_OPTIMIZE);
        }
#endif
        if ((AST_IS_RST_STARTED ()) &&
            (pPortEntry->u1EntryStatus == (UINT1) AST_PORT_OPER_UP))
        {
            RstTopoChSmFlushFdb (u2PortNo, VLAN_OPTIMIZE);
        }

        pRstPortInfo->bLearning = RST_FALSE;
        pRstPortInfo->bForwarding = RST_FALSE;
        pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;

        AST_DBG_ARG2 (AST_STSM_DBG | AST_SM_VAR_DBG,
                      "STSM: Port %s: Inst %d: Hardware port state "
                      "successfully set as Discarding \n",
                      AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);

        AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNo,
                                    (UINT1) AST_PORT_STATE_DISCARDING);
        AstRedSendSyncMessages (u2InstanceId, u2PortNo,
                                RED_AST_CALLBACK_SUCCESS, 0);

        if (AST_IS_RST_ENABLED ())
        {
            if (RstPortRoleTrMachine ((UINT2)
                                      RST_PROLETRSM_EV_LEARNING_FWDING_RESET,
                                      pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Role Transition Machine Returned"
                              "failure  !!!\n", AST_GET_IFINDEX_STR (u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Role Transition"
                              "Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNo));
                return RST_FAILURE;
            }
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            if (MstPortRoleTransitMachine ((UINT2)
                                           MST_PROLETRSM_EV_LEARNING_FWDING_RESET,
                                           pPerStPortInfo, u2InstanceId)
                != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %d: Port Role Transition Machine"
                              " from AstHwPortStateUpdtSuccess "
                              "Returned failure !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %d: Port Role Transition Machine"
                              " from AstHwPortStateUpdtSuccess "
                              "Returned failure !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);

                return RST_FAILURE;
            }
        }
#endif
    }

    else if (u2HwPortState == AST_PORT_STATE_LEARNING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_LEARN_SUCCESS;
        pRstPortInfo->bLearning = RST_TRUE;

        AST_DBG_ARG2 (AST_STSM_DBG | AST_SM_VAR_DBG,
                      "STSM: Port %s: Inst %d: Hardware port state "
                      "successfully set as Learning \n",
                      AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);

        AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNo,
                                    (UINT1) AST_PORT_STATE_LEARNING);
        AstRedSendSyncMessages (u2InstanceId, u2PortNo,
                                RED_AST_CALLBACK_SUCCESS, 0);
    }
    else
    {

        pRstPortInfo->bForwarding = RST_TRUE;
        /* Setting Block status as port state has been programmed in 
         * hardware successfully, this ensures BPDU s are sent properly*/
        pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_SUCCESS;

        AST_DBG_ARG2 (AST_STSM_DBG | AST_SM_VAR_DBG,
                      "STSM: Port %s: Inst %d: Hardware port state "
                      "successfully set as Forwarding \n",
                      AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);

        AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNo,
                                    (UINT1) AST_PORT_STATE_FORWARDING);

        AST_GET_NUM_FWD_TRANSITIONS (pPerStPortInfo)++;
        AstRedSendSyncMessages (u2InstanceId, u2PortNo,
                                RED_AST_CALLBACK_SUCCESS, 0);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHwPortStateUpdtFailure                            */
/*                                                                           */
/* Description        : This is function is called when the setting of       */
/*                      portstate in hardware failed it invokes the failure  */
/*                      handling of th Npapi                                 */
/*                                      .                                    */
/*                                                                           */
/* Input(s)           : AstPortEntry - poniter to PortEntry                  */
/*                      u2InstanceId - Id of the current Instance            */
/*                      u2HwPortState - Status of the Hardware               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHwPortStateUpdtFailure (tAstPortEntry * pPortEntry, UINT2 u2InstanceId,
                           UINT2 u2HwPortState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
#ifdef PVRST_WANTED
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
#endif
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2PortNo;
    UINT1               aaSysLogPortState[][11]
        = { "", "", "Discarding", "", "Learning", "Forwarding" };

    u2PortNo = pPortEntry->u2PortNo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNo);

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return RST_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                  "STSM: Port %s: Inst %d: Setting of %s port state in Hardware Failed !!\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId,
                  aaSysLogPortState[u2HwPortState]);

    AST_DBG_ARG3 (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                  "STSM: Port %s: Inst %d: Setting of %s port state in Hardware Failed !!\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId,
                  aaSysLogPortState[u2HwPortState]);

    /* Send SNMP trap and log the error if 
     *  1) the max number of retries has been exhausted or
     *  2) if the port is disabled in which case there will be no retries
     *     (because no hello timer is running -
     *      retry happens in hello timer expiry) */
    if ((pPerStPortInfo->i4NpFailRetryCount == AST_MAX_RETRY_COUNT) ||
        (pRstPortInfo->bPortEnabled == RST_FALSE) ||
        (pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN))
    {
        AstHwFailTrap (u2PortNo, u2InstanceId, u2HwPortState,
                       (INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gAstGlobalInfo.i4SysLogId,
                      "Setting of port state as %s in hardware "
                      "failed for port %s instance %d",
                      aaSysLogPortState[u2HwPortState],
                      AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId));

        pPerStPortInfo->i4NpFailRetryCount++;
    }

    if (u2HwPortState == AST_PORT_STATE_FORWARDING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_FAILURE;
        AstRedSendSyncMessages (u2InstanceId, pPerStPortInfo->u2PortNo,
                                RED_AST_CALLBACK_FAILURE, 0);

    }
    else if (u2HwPortState == AST_PORT_STATE_LEARNING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_LEARN_FAILURE;
        AstRedSendSyncMessages (u2InstanceId, pPerStPortInfo->u2PortNo,
                                RED_AST_CALLBACK_FAILURE, 0);
    }
    else if (u2HwPortState == AST_PORT_STATE_DISCARDING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_FAILURE;
        AstRedSendSyncMessages (u2InstanceId, pPerStPortInfo->u2PortNo,
                                RED_AST_CALLBACK_FAILURE, 0);

        if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
            (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
        {
            /* Sending an inferior BPDU, immediately, to block the peer port */
            pPerStPortInfo->i4TransmitSelfInfo = RST_TRUE;
            if (AST_IS_RST_ENABLED ())
            {
                pCommPortInfo->bNewInfo = RST_TRUE;
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_ENABLED ())
            {
                if (u2InstanceId == MST_CIST_CONTEXT)
                {
                    (AST_GET_CIST_MSTI_PORT_INFO (u2PortNo)->bNewInfo) =
                        MST_TRUE;
                }
                else
                {
                    (AST_GET_CIST_MSTI_PORT_INFO (u2PortNo)->bNewInfoMsti) =
                        MST_TRUE;
                }
            }
#endif
#ifdef PVRST_WANTED
            else if AST_IS_PVRST_ENABLED
                ()
            {
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNo,
                                                         u2InstanceId);
                pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

                if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                              pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Transmit machine"
                                  "returneFAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Transmit machine"
                                  "returnedFAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    return RST_FAILURE;
                }
            }
#endif

            if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
            {
                if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                            pPortEntry, RST_DEFAULT_INSTANCE)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "TXSM: Port %s: Port Transmit machine returned"
                                  " FAILURE!!! \n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                  "TXSM: Port %s: Port Transmit machine returned"
                                  " FAILURE!!! \n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSendEventToAstTask                                */
/*                                                                           */
/* Description        : This function receives indications and BPDUs from    */
/*                      external modules, forms a Queue message that contains*/
/*                      either the pointer to the indication message received*/
/*                      or the pointer to the CRU buffer i.e. BPDU. It then  */
/*                      posts the message into the queue and sends an event  */
/*                      to the Astp Task.                                    */
/*                                                                           */
/* Input(s)           : pNode - Pointer to the indication message received   */
/*                              from external modules                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstSendEventToAstTask (tAstMsgNode * pNode, UINT4 u4ContextId)
{
    INT4                i4RetVal = RST_SUCCESS;
    tAstQMsg           *pQMsg = NULL;

    if (!(AST_IS_INITIALISED ()))
    {
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "AST is not initialized!\n");
        }
        return RST_FAILURE;
    }
    if (AST_ALLOC_CFGQ_MSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "STAP: Message Memory Block Allocation FAILED!!!\n");

        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Message Memory Block FAILED!\n");
        }
        return RST_FAILURE;
    }

    AST_MEMSET (pQMsg, AST_MEMSET_VAL, sizeof (tAstQMsg));

    AST_QMSG_TYPE (pQMsg) = AST_SNMP_CONFIG_QMSG;
    pQMsg->uQMsg.pMsgNode = pNode;

    if (AST_SEND_TO_QUEUE (AST_CFG_QID,
                           (UINT1 *) &pQMsg, AST_DEF_MSG_LEN)
        == AST_OSIX_SUCCESS)

    {
        if (AST_TASK_ID == AST_INIT_VAL)
        {

            if (AST_GET_TASK_ID (AST_SELF, AST_TASK_NAME, &(AST_TASK_ID)) !=
                AST_OSIX_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "MGMT: Getting Task Id of AST Task FAILED!\n");
            }
        }

        AST_SEND_EVENT (AST_TASK_ID, AST_MSG_EVENT);
    }
    else
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Posting Message to Ast Queue FAILED!\n");
        i4RetVal = RST_FAILURE;
    }

    if (i4RetVal == RST_FAILURE)
    {
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Message Memory Block FAILED!\n");
            i4RetVal = RST_FAILURE;
        }

        if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of CFGQ Message Memory Block FAILED!\n");
            i4RetVal = RST_FAILURE;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstHandleCreatePort                                  */
/*                                                                           */
/* Description        : This function creates port in RSTP/MSTP/PVRST. Based */
/*                      on the module started, it call RstCreatePort or      */
/*                      MstCreatePort or PvrstCreatePort. Once the creation  */
/*                      of port is done, this function call                  */
/*                      AstPbCheckPortTypeChange to update the port type for */
/*                      the given port.                                      */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to msg node containing the create */
/*                                 port message.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHandleCreatePort (tAstMsgNode * pMsgNode)
{
    INT4                i4RetVal = RST_SUCCESS;
    UINT4               u4PortsCreated = AST_INIT_VAL;

    /* Port creation should be blocked if the number of ports created has reached
     * max ports in system. */

    AstGetNumOfPortsCreated (&u4PortsCreated);

    if (u4PortsCreated >= AST_SIZING_PORT_COUNT)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: RstCreatePort function MAX Ports count reached\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                 AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MSG: RstCreatePort function MAX Ports count reached\n");
        return RST_SUCCESS;
    }

    if (AST_IS_RST_STARTED ())
    {
        if (RstCreatePort (pMsgNode->u4PortNo,
                           pMsgNode->uMsg.u2LocalPortId) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstCreatePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: RstCreatePort function returned FAILURE\n");
            return RST_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (MstCreatePort (pMsgNode->u4PortNo,
                           pMsgNode->uMsg.u2LocalPortId) != MST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MstCreatePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: MstCreatePort function returned FAILURE\n");
            return RST_FAILURE;
        }
    }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_STARTED ())
    {
        if (PvrstCreatePort (pMsgNode->u4PortNo,
                             pMsgNode->uMsg.u2LocalPortId) != MST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PvrstCreatePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: PvrstCreatePort function returned FAILURE\n");
            return RST_FAILURE;
        }
    }
#endif /* PVRST_WANTED */

#ifdef L2RED_WANTED
    if (AstRedCreatePortAndPerPortInst (pMsgNode->uMsg.u2LocalPortId)
        == RST_FAILURE)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstRedCreatePortAndPerPortInst function returned FAILURE\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                 AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MSG: AstRedCreatePortAndPerPortInst function returned FAILURE\n");
        return RST_FAILURE;
    }
#endif

    AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                  AST_MGMT_DBG,
                  "MSG: Management Creation of New Port %d Success\n",
                  pMsgNode->u4PortNo);

    i4RetVal = AstCheckBrgPortTypeChange (pMsgNode->uMsg.u2LocalPortId);

    /* Associate the MAC Address depending upon the port
     * Type 
     * */

    AstDeriveMacAddrFromPortType (pMsgNode->uMsg.u2LocalPortId);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstHandleDeletePort                                  */
/*                                                                           */
/* Description        : This function deletes port in RSTP/MSTP/PVRST. Based */
/*                      on the module started, it call RstDeletePort or      */
/*                      MstDeletePort or PvrstDeletePort.                    */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to msg node containing the delete */
/*                                 port message.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstHandleDeletePort (tAstMsgNode * pMsgNode)
{
    INT4                i4RetVal = RST_SUCCESS;
    UINT4               u4IfIndex = AST_GET_IFINDEX (pMsgNode->u4PortNo);

    gbIsPortDeleted = OSIX_TRUE;

    if (AST_IS_RST_STARTED ())
    {

        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            AstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);
        }
        /* Clear Sync up data for the particular port
         * before deleting the port*/
        AstRedClearSyncUpDataOnPort ((UINT2) pMsgNode->u4PortNo);
        if (RstDeletePort ((UINT2) pMsgNode->u4PortNo) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstDeletePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: RstDeletePort function returned FAILURE\n");
            i4RetVal = (INT4) RST_FAILURE;
        }

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            MstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);
        }

        /* Clear Sync up data for the particular port
         * before deleting the port*/
        AstRedClearSyncUpDataOnPort ((UINT2) pMsgNode->u4PortNo);
        if (MstDeletePort ((UINT2) pMsgNode->u4PortNo) == MST_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MstDeletePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: MstDeletePort function returned FAILURE\n");
            i4RetVal = (INT4) RST_FAILURE;
        }
    }
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_STARTED ())
    {
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            PvrstRedClearPduOnActive ((UINT2) pMsgNode->u4PortNo);
        }

        /* Clear Sync up data for the particular port
         * before deleting the port*/
        AstRedClearSyncUpDataOnPort ((UINT2) pMsgNode->u4PortNo);
        if (PvrstDeletePort ((UINT2) pMsgNode->u4PortNo) == PVRST_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PvrstDeletePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: PvrstDeletePort function returned FAILURE\n");
            i4RetVal = (INT4) RST_FAILURE;
        }
    }
#endif /* PVRST_WANTED */

#ifdef L2RED_WANTED
    if (AstRedDeletePortAndPerPortInst ((UINT2) pMsgNode->u4PortNo)
        == RST_FAILURE)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstRedDeletePortAndPerPortInst function returned FAILURE\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                 AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MSG: AstRedDeletePortAndPerPortInst function returned FAILURE\n");
        i4RetVal = (INT4) RST_FAILURE;
    }
#endif

    gbIsPortDeleted = OSIX_FALSE;
    if (i4RetVal == RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                      AST_MGMT_DBG,
                      "MSG: Management Invalidation of Port %u Success\n",
                      u4IfIndex);
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstHandleEnablePortMsg                               */
/*                                                                           */
/* Description        : This function handles the Enable port message.       */
/*                                                                           */
/* Input(s)           : u2Port - Port for which the enable port msg has come */
/*                      u2InstanceId - Instance id in case if the spanning   */
/*                                     tree is enabled or disabled on that   */
/*                                     port.                                 */
/*                      u1TrigType - AST_EXT_PORT_UP / AST_STP_PORT_DOWN     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstHandleEnablePortMsg (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType)
{
    INT4                i4RetVal = (INT4) RST_SUCCESS;
#ifdef PVRST_WANTED
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    INT4                i4Val = RST_SUCCESS;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    INT4                i4Value = RST_SUCCESS;
#ifdef NPAPI_WANTED
    tAstCommPortInfo   *pCommPortInfo = NULL;
#endif
#endif

    if (AST_IS_RST_STARTED ())
    {
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            AstRedSendSyncMessages (RST_DEFAULT_INSTANCE,
                                    u2Port, RED_AST_OPER_STATUS, u1TrigType);
        }

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            AstRedSendSyncMessages (u2InstanceId,
                                    u2Port, RED_AST_OPER_STATUS, u1TrigType);
        }

    }
#endif /* MSTP_WANTED */
    if (AST_IS_CUSTOMER_EDGE_PORT (u2Port) == RST_TRUE)
    {
        /* For CEP, handling oper status or stp port up is different. 
         * For CEP, we need to switch to the corresponding C-VLAN component, 
         * and then enable the port there. */
        i4RetVal = AstPbEnableCepPort (u2Port, u2InstanceId, u1TrigType);
        return i4RetVal;
    }
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_STARTED ())
    {
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            if ((pPortInfo = AST_GET_PORTENTRY (u2Port)) == NULL)
            {
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                              "MSG: AstHandleEnablePortMsg: Port =%d does not exist !!!\n",
                              u2Port);
                i4RetVal = (INT4) RST_FAILURE;
                return i4RetVal;
            }

            /* Calculate the pathcost here itself. This is done because incase of
               port-channel, speed can be updated in LA module for multiple ports
               when the loop counts for Maximum vlans(i.e 1-4094) which will result
               in wrong path cost when dynamic pathcost on lag speed is disabled  */

            if ((pPortInfo->bPathCostInitialized == PVRST_FALSE) ||
                ((AST_GET_BRGENTRY ())->u1DynamicPathcostCalculation ==
                 PVRST_TRUE))
            {
                pPortInfo->u4PathCost = AstCalculatePathcost (u2Port);
                pPortInfo->bPathCostInitialized = PVRST_TRUE;
            }

            for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                            VlanId) == OSIX_FALSE)
                {
                    continue;
                }
#ifdef NPAPI_WANTED
                pCommPortInfo = AST_GET_COMM_PORT_INFO (u2Port);
                if (pCommPortInfo != NULL)
                {
                    if (pCommPortInfo->bPortPvrstStatus == AST_FALSE)
                    {
                        PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 pPortInfo->u4IfIndex,
                                                 VlanId,
                                                 AST_PORT_STATE_FORWARDING);
                    }
                }
#endif
                AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex,
                                         &u1PortType);

                if (u1PortType == VLAN_HYBRID_PORT)
                {
                    if (AST_DEF_VLAN_ID () != VlanId)
                    {
                        /* If port is a tagged port enable it */
                        i4Val = VlanCheckPortType (VlanId, u2Port);
                        if (i4Val != 0)
                        {
                            i4Value =
                                PvrstPerStEnablePort (u2Port, VlanId,
                                                      u1TrigType);
                        }

                    }
                    else
                    {
                        AstRedSendSyncMessages (VlanId,
                                                u2Port,
                                                RED_AST_OPER_STATUS,
                                                u1TrigType);
                    }
                }
                if (u1PortType == VLAN_ACCESS_PORT)
                {
                    AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->
                                             u4IfIndex, &PVID);
                    if (PVID != VlanId)
                    {
                        continue;
                    }
                    else
                    {
                        AstRedSendSyncMessages (VlanId,
                                                u2Port,
                                                RED_AST_OPER_STATUS,
                                                u1TrigType);
                        break;
                    }
                }
                AstRedSendSyncMessages (VlanId,
                                        u2Port,
                                        RED_AST_OPER_STATUS, u1TrigType);
            }
        }
    }
#endif /* PVRST_WANTED */

    if (AstEnablePort (u2Port, u2InstanceId, u1TrigType) != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstEnablePort function returned FAILURE\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                 | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MSG: AstEnablePort function returned FAILURE\n");
        i4RetVal = (INT4) RST_FAILURE;
    }

    AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                  AST_MGMT_DBG,
                  "MSG: Management Enabling of Port %s Success\n",
                  AST_GET_IFINDEX_STR (u2Port));
#ifdef PVRST_WANTED
    UNUSED_PARAM (i4Value);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstEnablePort                                        */
/*                                                                           */
/* Description        : This function is a wrapper function which enables the*/
/*                      Port in the RSTP/MSTP depending upon the Module status*/
/*                                                                           */
/* Input(s)           : pMsgNode - Message contains StpPortUp and PortOperUp */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstEnablePort (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType)
{
#ifdef PVRST_WANTED
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
#endif

    if (AST_IS_RST_STARTED ())
    {
        if (RstEnablePort (u2Port, u1TrigType) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstEnablePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                     | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: RstEnablePort function returned FAILURE\n");
            return RST_FAILURE;
        }

        if (AST_NODE_STATUS () == RED_AST_STANDBY)
        {
            /* The Timers that are started in RstEnablePort fn wont get synced
             * in standby because port is in oper down state at that time.
             * So initalise the timer values in sync up db after port enable*/
            AstRedInitOperTimes (0, u2Port);

        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (MstEnablePort (u2Port, u2InstanceId, u1TrigType) != MST_SUCCESS)

        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: MstEnablePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                     | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: MstEnablePort function returned FAILURE\n");
            return RST_FAILURE;
        }

        if (AST_NODE_STATUS () == RED_AST_STANDBY)
        {
            /* The Timers that are started in MstEnablePort fn wont get synced
             * in standby because port is in oper down state at that time.
             * So initalise the timer values in sync up db after port enable*/
            AstRedInitOperTimes (u2InstanceId, u2Port);

        }
    }
#endif
#ifdef PVRST_WANTED
    /* While syncing vlanid is sent, and earlier during RedApplyOperStatus,
     * PvrstPerStEnablePort was called with that vlan id.
     * But in active node, PvrstPerStEnablePort is called with
     * RST_DEFAULT_INSTANCE.............
     * Now made both in sync.*/
    else if (AST_IS_PVRST_STARTED ())
    {
        if (PvrstPerStEnablePort (u2Port, VlanId, u1TrigType) != PVRST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PvrstPerStEnablePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                     | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: PvrstPerStEnablePort function returned FAILURE\n");
            return RST_FAILURE;
        }

        if (AST_NODE_STATUS () == RED_AST_STANDBY)
        {
            AstRedInitOperTimes (VlanId, u2Port);

        }
    }
#endif

#ifndef MSTP_WANTED
    UNUSED_PARAM (u2InstanceId);
#endif
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDisablePort                                       */
/*                                                                           */
/* Description        : This function is a wrapper function which disables the*/
/*                      Port in the RSTP/MSTP depending upon the Module status*/
/*                                                                           */
/* Input(s)           : pMsgNode - Message contains StpPortDown and          */
/*                                 PortOperDown                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstDisablePort (tAstMsgNode * pMsgNode)
{
    INT4                i4RetValue = 0;
#ifdef PVRST_WANTED
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
#endif
    if (AST_IS_RST_STARTED ())
    {
        i4RetValue =
            RstDisablePort ((UINT2) pMsgNode->u4PortNo,
                            pMsgNode->uMsg.u1TrigType);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetValue = MstDisablePort ((UINT2) pMsgNode->u4PortNo,
                                     pMsgNode->u2InstanceId,
                                     pMsgNode->uMsg.u1TrigType);

    }
#endif
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_STARTED ())
    {
        if (PvrstPerStDisablePort ((UINT2) pMsgNode->u4PortNo, VlanId,
                                   pMsgNode->uMsg.u1TrigType) != PVRST_SUCCESS)

        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: PvrstPerStDisablePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                     | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: PvrstPerStDisablePort function returned FAILURE\n");
            return RST_FAILURE;
        }
    }
#endif
    UNUSED_PARAM (i4RetValue);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleUpdateIntfType                              */
/*                                                                           */
/* Description        : This function changes the Interface type to the      */
/*                      specified one. This should be only used to update    */
/*                      the port type from CNP S-Tagged to CNP C-Tagged in   */
/*                      I Components.                                        */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to the Msg passed to ASTP.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstHandleUpdateIntfType (tAstMsgNode * pMsgNode)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;

    if (pMsgNode->uMsg.u1BrgPortType != AST_C_INTERFACE_TYPE)
    {
        return RST_SUCCESS;
    }

    /* Scan through all the ports, & then change for the corresponding
     * CNPs alone. */
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if (pPortEntry == NULL)
        {
            continue;
        }

        if (AST_GET_BRG_PORT_TYPE (pPortEntry) != AST_CNP_STAGGED_PORT)
        {
            continue;
        }

        AstHandlePortTypeChange (pPortEntry, AST_CNP_CTAGGED_PORT);

        AstDeriveMacAddrFromPortType (u2PortNum);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandlePortSpeedChgIndication                      */
/*                                                                           */
/* Description        : This calls feature spanning tree specific API to     */
/*                      recalculate Path cost for the given port.            */
/*                                                                           */
/* Input(s)           : u4IfIdex - Interface number for which path cost      */
/*                      is to be calculated                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
VOID
AstHandlePortSpeedChgIndication (UINT4 u4IfIndex)
{
    if (AST_IS_RST_STARTED ())
    {
        RstReCalculatePathcost (u4IfIndex);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        MstReCalculatePathcost (u4IfIndex);
    }
#endif
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_STARTED ())
    {
        PvrstReCalculatePathcost (u4IfIndex);
    }
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : AstAssignMempoolIds                                  */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      ASTMemPoolIds to global mempoolIds.                  */
/*                                                                           */
/* Input(s)           :  None                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
AstAssignMempoolIds (VOID)
{

    /*tAstQMsg */
    AST_CFG_QMSG_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_CONFIG_MESG_SIZING_ID];

    /*tAstContextInfo */
    AST_CONTEXT_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_CONTEXTS_SIZING_ID];

    /*tAstMsgNode */
    AST_LOCALMSG_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_LOCAL_MESG_SIZING_ID];
#ifdef MSTP_WANTED
    /*tAstMsgDigestsize */
    AST_MST_DIGEST_INPUT_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_MST_DIGEST_MESG_SIZING_ID];
#endif
    /*tAstContextInfo */
    AST_CVLAN_CONTEXT_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PB_CONTEXT_INFO_SIZING_ID];

    /*tAstPbCVlanInfo */
    AST_CVLAN_INFO_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_PB_CVLAN_INFO_SIZING_ID];

    /*tAstPerStInfo */
    AST_CVLAN_PERST_TBL_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PB_PERST_INFO_SIZING_ID];

    /*tAstPort2IndexMap */
    AST_PB_PORT2INDEX_MAP_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PB_PORT_INDEX_MAP_SIZING_ID];

    /*tAstPbPortInfo */
    AST_PB_PORT_INFO_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_PB_PORT_INFO_SIZING_ID];

    /*tAstPerStInfo */
    AST_PERST_INFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PERST_INFO_IN_SYS_SIZING_ID];

    /*tAstPerStPortArray */
    AST_PERST_PORT_TBL_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PERST_PORT_ARRAY_SIZING_ID];

    /*tAstPerStPortInfo */
    AST_PERST_PORT_INFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PERST_PORT_INFO_IN_SYS_SIZING_ID];

    /*tAstPortArray */
    AST_PORT_TBL_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_PORT_ARRAY_SIZING_ID];

    /*tAstFrameSize */
    AST_ETHFRAME_SIZE_MEMPOOL_ID =
        ASTMemPoolIds[MAX_AST_ETHFRAME_SIZE_SIZING_ID];

    /*tAstPortEntry */
    AST_PORT_INFO_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_PORTS_IN_SYS_SIZING_ID];

    /*tRstPortInfo */
    RST_PORTINFO_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_PORTS_IN_SYSTEM_SIZING_ID];

    /*tMstInstInfo */
    MST_INSTINFO_MEMPOOL_ID = ASTMemPoolIds[MAX_MST_INSTANCES_SIZING_ID];

#ifdef MSTP_WANTED
    /*tMstVlanMap */
    MST_VLANMAP_MEMPOOL_ID = ASTMemPoolIds[MAX_MST_VLANMAP_SIZING_ID];

    /*tMstVlanList */
    MST_VLANLIST_MEMPOOL_ID = ASTMemPoolIds[MAX_MST_VLANLIST_SIZING_ID];

#endif

#ifdef PVRST_WANTED
    /*tPerStPvrstBridgeInfo */
    AST_PERST_PVRST_BRGINFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PVRST_BRIGE_INFO_SIZING_ID];

    /*tPerStPvrstRstPortInfo */
    AST_PERST_PVRST_RST_PORTINFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PVRST_RST_PORT_INFO_SIZING_ID];

#endif /*PVRST_WANTED */

    /*tAstQMsg */
    AST_QMSG_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_Q_MESG_SIZING_ID];

#ifdef L2RED_WANTED
    /*tAstRedPerPortInstInfoArray */
    AST_RED_PERPORT_INST_INFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_RED_PER_PORT_INST_INFO_SIZING_ID];

    /*tAstRedPortInfoArray */
    AST_RED_PORTINFO_TBL_POOL_ID
        = ASTMemPoolIds[MAX_AST_RED_PORT_INFO_ARRAY_SIZING_ID];

    /*tAstRedPortInfo */
    AST_RED_PORT_INFO_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_RED_PORT_INFO_SIZING_ID];

    /*tAstRedContextInfo */
    AST_RED_CVLAN_COMP_POOL_ID ()
        = ASTMemPoolIds[MAX_AST_RED_CVLAN_CONTEXT_INFO_SIZING_ID];

    /*tAstRedPerPortInstInfo */
    AST_RED_CVLAN_PERST_PORTINFO_POOL_ID ()
        = ASTMemPoolIds[MAX_AST_RED_CVLAN_PER_PORT_INST_INFO_SIZING_ID];
#endif

    /*tAstTimer */
    AST_TMR_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_TIMER_BLOCKS_SIZING_ID];

#ifdef PVRST_WANTED
    /* UINT1 */
    AST_PVRST_VLAN_TO_INDEX_MAPPING_MEMPOOL_ID
        = ASTMemPoolIds[MAX_AST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT_SIZING_ID];
#endif /*PVRST_WANTED */

    AST_BPDU_TYPE_MEMPOOL_ID = ASTMemPoolIds[MAX_AST_BPDU_TYPE_SIZING_ID];

    return;
}

/*****************************************************************************/
/* Function Name      : AstHandleCreatePortFromLa                            */
/*                                                                           */
/* Description        : This function updates the Row Status for             */
/*                       Automatic port create feature when the indication   */
/*                       is from LA.                                         */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to the Msg passed to ASTP.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstHandleCreatePortFromLa (tAstMsgNode * pMsgNode)
{
    UINT1               u1OperStatus = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = AST_GET_PORTENTRY (pMsgNode->uMsg.u2LocalPortId);

    if (pPortEntry != NULL)
    {
        pPortEntry->i4PortRowStatus = ACTIVE;
        if (AstCfaGetIfOperStatus (pPortEntry->u4IfIndex, &u1OperStatus) !=
            CFA_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: Cannot Get IfOperStatus for Port %u\n",
                          pMsgNode->u4PortNo);
            return RST_FAILURE;
        }
        if (u1OperStatus == AST_UP)
        {
            AstUpdateOperStatus (pPortEntry->u4IfIndex, AST_UP);
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleCreatePortFromLa                            */
/*                                                                           */
/* Description        : This function updates the Row Status for             */
/*                       Automatic port create feature when the indication   */
/*                       is from LA.                                         */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to the Msg passed to ASTP.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstHandleDeletePortFromLa (tAstMsgNode * pMsgNode)
{

    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = AST_GET_PORTENTRY (pMsgNode->u4PortNo);
    if (pPortEntry != NULL)
    {
        pPortEntry->i4PortRowStatus = NOT_IN_SERVICE;
        AstUpdateOperStatus (pPortEntry->u4IfIndex, AST_DOWN);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstInitializeTxBuf                                      */
/*                                                                           */
/* Description        : This function initializes the global variables       */
/*                      for transmitting MSTP BPDU                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                 */
/*****************************************************************************/
VOID
AstInitializeTxBuf (VOID)
{
#ifdef MSTP_WANTED
    gi1MstTxAllowed = MST_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name      : AstTransmitMstBpdu                                     */
/*                                                                           */
/* Description        : This function transmits the consolidated MST         */
/*                      BPDU by calling the function MstPortTxSmTxMstpWr     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                 */
/*****************************************************************************/
VOID
AstTransmitMstBpdu (VOID)
{
#ifdef MSTP_WANTED

    UINT2               u2PortNum = 0;
    UINT4               u4CurrContextId = 0;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;

    u4CurrContextId = gCurrContextId;
    gi1MstTxAllowed = MST_TRUE;

    if (AstSelectContext (u4CurrContextId) != RST_FAILURE)
    {
        pAstBridgeEntry = AST_GET_BRGENTRY ();
        if (AST_IS_MST_STARTED ())
        {
            AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                               pRstPortEntry, tRstPortInfo *)
            {
                u2PortNum = pRstPortEntry->u2PortNum;

                if (u2PortNum == 0)
                {
                    continue;
                }
                if ((pAstPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
                {
                    continue;
                }
                if (pAstPortEntry->bTransmitBpdu == MST_TRUE)
                {
                    pAstPortEntry->bTransmitBpdu = MST_FALSE;
                    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
                    if (pAstCommPortInfo->u1TxCount <
                        pAstBridgeEntry->u1TxHoldCount)
                    {
                        if (MstPortTxSmTxMstpWr
                            (pAstPortEntry, MST_CIST_CONTEXT) != MST_SUCCESS)
                        {
                            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                     "TXSM: MstPortTxSmMstpWr function returned FAILURE!\n");
                        }
                        (pAstCommPortInfo->u1TxCount)++;
                    }
                    else
                    {
                        AST_DBG_ARG1 (AST_ALL_FLAG | AST_ALL_FAILURE_DBG,
                                      "TXSM: Port %s: Hold Rate EXCEEDED, Not Sending BPDU...\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_TXSM_DBG,
                                      "TXSM: Port %s: Hold Rate EXCEEDED, Not Sending BPDU...\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                    }
                }

            }
        }

    }
#endif

}
