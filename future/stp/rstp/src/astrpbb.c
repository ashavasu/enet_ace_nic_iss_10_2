/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrpbb.c,v 1.6 2012/08/08 10:36:29 siva Exp $
 *
 * Description: This file contains all utility routines used by the
 *              RSTP Module to realise the PBB Functionality.
 *
 *******************************************************************/

#include "asthdrs.h"
#ifdef MSTP_WANTED
#include "astmtrap.h"
#endif

/*****************************************************************************/
/* Function Name      : RstPbbIsTreeAllSynced                                */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstIsTreeAllSynced function, for I-component ports.  */
/*                      If the port invoking this function is a CNP, then    */
/*                      it returns true if synced is set for all other ports */
/*                      present in this component.                           */
/*                      If the port invoking this function is a VIP, then    */
/*                      it returns true if synced is set for all the CNPs.   */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstIsTreeAllSynced function and         */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/ RST_FALSE.                                 */
/*****************************************************************************/
tAstBoolean
RstPbbIsTreeAllSynced (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPortEntry      *pInputPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2Count = 1;

    pInputPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pInputPortInfo == NULL)
    {
        /* Calling port cannot be NULL. Safely exit */
        return RST_FALSE;
    }
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    AST_GET_NEXT_PORT_ENTRY (u2Count, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

        if (pRstPortInfo->bPortEnabled != RST_TRUE)
        {
            continue;
        }

        /* If the calling port is VIP, then do not calculate synced
         * for another VIP */
        if ((AST_GET_BRG_PORT_TYPE (pInputPortInfo) ==
             AST_VIRTUAL_INSTANCE_PORT)
            && (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_VIRTUAL_INSTANCE_PORT))
        {
            continue;
        }
        /* Calling port is a CNP, Operate normally */

        if ((pRstPortInfo->bSelected != RST_TRUE) ||
            (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole)
            || (pRstPortInfo->bUpdtInfo != RST_FALSE))
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: IsTreeAllSynced: Port %s: RoleSelection/Updation not complete",
                          AST_GET_IFINDEX_STR (u2Count));
            /* RoleSelection/Updation is not yet complete for all ports */
            return RST_FALSE;
        }

        if ((pPerStBrgInfo->u2RootPort == u2Count) ||
            (pRstPortInfo->bSynced == RST_TRUE))
        {
            continue;
        }
        else
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: IsTreeAllSynced: Port %s: Synced NOT set",
                          AST_GET_IFINDEX_STR (u2Count));
            return RST_FALSE;
        }
    }
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : RstPbbIsReRooted                                     */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmIsReRooted function, if the port invoking*/
/*                      is a virtual instance port.                          */
/*                      If rrwhile timer is zero for all the CNPs in this    */
/*                      component, then this function will return success    */
/*                      other wise failure.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstPbbIsReRooted (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2Count = 1;

    /* If rrwhile is zero for all CNPs, then
     * rerooted is true for this VIP. */
    AST_GET_NEXT_PORT_ENTRY (u2Count, pPortInfo)
    {
        if (pPortInfo != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Count,
                                                      RST_DEFAULT_INSTANCE);
            pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2Count,
                                                        RST_DEFAULT_INSTANCE);
            if (u2Count == u2PortNum)
            {
                continue;
            }
            if (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_VIRTUAL_INSTANCE_PORT)
            {
                continue;
            }
            /* Port is a CNP */
            if ((pPerStPortInfo->u1SelectedPortRole !=
                 pPerStPortInfo->u1PortRole) ||
                (pRstPortInfo->pRrWhileTmr != NULL))
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: RrWhileTmr Still Running...\n",
                              AST_GET_IFINDEX_STR (u2Count));
                return RST_FAILURE;
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRestoreVipDefaults                                */
/*                                                                           */
/* Description        : This function will be called by during port          */
/*                      initialisation,if the port created is a VIP. This    */
/*                      function restores the defaults as mentioned by IEEE  */
/*                      802.1ah Draft 4.2 Section 13.40.                     */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
AstRestoreVipDefaults (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT4               u4Isid = AST_INIT_VAL;

    pAstPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                 RST_DEFAULT_INSTANCE);

    /* IEEE 802.1ah Draft 4.2 section 6.10 urges to configure the value of
     * AdminP2P status as AUTO and OperP2P Status as ForceFalse
     * */
    pAstPortInfo->u1AdminPointToPoint = (UINT1) RST_P2P_AUTO;
    pAstPortInfo->bOperPointToPoint = RST_FALSE;
    pAstPortInfo->u1EntryStatus = CFA_IF_DOWN;

    /* Admin Edge and OperEdge are already set to false in
     * RstInitGlobalPortInfo function. So no need to that here. */
    pAstPortInfo->bAutoEdge = RST_TRUE;
    pAstPortInfo->u4PathCost = AST_PB_PEP_PORT_PATH_COST;

    /* The patch cost is configured here, so that when port oper up
     * comes up, RSTP will not calculate path cost based on speed
     * */
    pAstPerStPortInfo->u4PortAdminPathCost = AST_PB_PEP_PORT_PATH_COST;

    pAstPortInfo->bPathCostInitialized = RST_TRUE;

    pAstPerStPortInfo->u1PortPriority = AST_PB_CVLAN_PORT_PRIORITY;

    /* Here some of the constants defined for 802.1ad is used, as
     * both 1ad and 1ah are having the same values for these constants
     * IEEE 802.1ah Section 13.40
     * */

#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    MEMSET (pAstPortInfo->au1StrIfIndex, 0, AST_IFINDEX_STR_LEN);
    /* ISID should be obtained from L2IWF
     * */
    if (AstL2IwfGetIsid (pAstPortInfo->u4IfIndex, &u4Isid) != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }
    SNPRINTF ((CHR1 *) pAstPortInfo->au1StrIfIndex, AST_IFINDEX_STR_LEN,
              "%s: %d", "ISID", u4Isid);
#endif

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: VIP Parameters configured successfully...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name             : AstMiRstNpHwServiceInstancePtToPtStatus    */
/*                                                                           */
/*    Description               : This API is used to program the hardware   */
/*                                regarding the P2P status of a VIP.         */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Context Id                   */
/*                                u4VipIndex  - VIP IfIndex                  */
/*                                u4Isid      - ISID value for this VIP.     */
/*                                u1OperPointToPoint - point to point status */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS or RST_FAILURE.                */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstNpHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                         UINT4 u4VipIndex, UINT4 u4Isid,
                                         UINT1 u1PointToPointStatus)
{
    u1PointToPointStatus =
        (u1PointToPointStatus == RST_TRUE) ? FNP_TRUE : FNP_FALSE;

    if (RstpFsMiPbbRstHwServiceInstancePtToPtStatus (u4ContextId, u4VipIndex,
                                                 u4Isid, u1PointToPointStatus)
        != FNP_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

#endif
