/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrprsm.c,v 1.17 2015/02/11 10:38:37 siva Exp $
 *
 * Description: This file contains RST Port Receive SEM           
 *              related routines.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstPortReceiveMachine                      */
/*                                                                           */
/*    Description               : This function implements the Port Receive  */
/*                                State Machine. Called when a BPDU is       */
/*                                received on the port.                      */
/*                                                                           */
/*    Input(s)                  : u1Event        - Event occured related to  */
/*                                                 this Machine.             */
/*                                pRcvBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstPortReceiveMachine (UINT2 u2Event, tAstBpdu * pRcvBpdu, UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               u1State = 0;
    INT4                i4RetVal = 0;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (NULL == pRcvBpdu)
    {
        AST_DBG_ARG2 (AST_PRSM_DBG,
                      "PRSM: Event %s: Port %s: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_PRSM][u2Event],
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Event %s: Port %s: Port does not exist\n",
                      gaaau1AstSemEvent[AST_PRSM][u2Event],
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    u1State = (AST_GET_COMM_PORT_INFO (u2PortNum))->u1PortRcvSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  " Port Receive Machine Called with Event: %s, State: %s for Port: %s\n",
                  gaaau1AstSemEvent[AST_PRSM][u2Event],
                  gaaau1AstSemState[AST_PRSM][u1State],
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_DBG_ARG3 (AST_PRSM_DBG,
                  " Port Receive Machine Called with Event: %s, State: %s for Port: %s\n",
                  gaaau1AstSemEvent[AST_PRSM][u2Event],
                  gaaau1AstSemState[AST_PRSM][u1State],
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PRCVSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_PRSM_DBG,
                 "PRSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_PORT_RECEIVE_MACHINE[u2Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_PRSM_DBG,
                 "RECV: PortReceiveStateMachine No Action to Perform!\n");

        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_RECEIVE_MACHINE[u2Event][u1State].pAction))
        (pRcvBpdu, u2PortNum);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "RECV: PortReceiveStateMachine Action returned FAILURE!\n");
        AST_DBG (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                 "RECV: PortReceiveStateMachine Action returned FAILURE!\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstPortRcvSmMakeDiscard                    */
/*                                                                           */
/*    Description               : This function changes the Port Receive     */
/*                                State Machine to DISCARD state.            */
/*                                                                           */
/*    Input(s)                    pRcvBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstPortRcvSmMakeDiscard (tAstBpdu * pRcvBpdu, UINT2 u2PortNum)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    AST_UNUSED (pRcvBpdu);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    pAstCommPortInfo->bRcvdStp = RST_FALSE;
    pAstCommPortInfo->bRcvdRstp = RST_FALSE;
    pAstCommPortInfo->bRcvdBpdu = RST_FALSE;
    pRstPortInfo->bRcvdMsg = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PRSM_Discard: Port %s: RcvdBpdu = RcvdStp = RcvdRstp = RcvdMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pAstCommPortInfo->u1PortRcvSmState = (UINT1) RST_PRCVSM_STATE_DISCARD;

    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PRSM: Port %s: Moved to state DISCARD\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstPortRcvSmChkRcvdBpdu                    */
/*                                                                           */
/*    Description               : This function processes the RCVD_BPDU      */
/*                                Event when the Port Receive State Machine  */
/*                                is in Discard State or Receive State.      */
/*                                                                           */
/*    Input(s)                  : pRcvBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstPortRcvSmChkRcvdBpdu (tAstBpdu * pRcvBpdu, UINT2 u2PortNum)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if ((pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN) ||
        (pPerStRstPortInfo->bPortEnabled == RST_FALSE))
    {
        AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Port %s: Received BPDU is discarded Port is Disabled\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        RstPortRcvSmMakeDiscard (pRcvBpdu, u2PortNum);
    }
    else
    {
        if ((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_TRUE) &&
            (AST_PORT_IS_L2GP (pPortEntry) == RST_FALSE))
        {
            if (RstPortRcvSmMakeReceive (pRcvBpdu, u2PortNum) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RECV: Port %s: MstPortRcvSmMakeReceive function returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstPortRcvSmMakeReceive                    */
/*                                                                           */
/*    Description               : This function changes the Port Receive     */
/*                                State Machine to RECEIVE state.            */
/*                                                                           */
/*    Input(s)                    pRcvBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstPortRcvSmMakeReceive (tAstBpdu * pRcvBpdu, UINT2 u2PortNum)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bPrevSendRstp;
    UINT2               u2Duration = AST_INIT_VAL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    RstPortRcvUpdtBpduVersion (pRcvBpdu, u2PortNum);

    pAstCommPortInfo->bRcvdBpdu = RST_FALSE;

    /* Setting OperEdge in L2Iwf will happen in BDSM */
    pPortEntry->bOperEdgePort = RST_FALSE;

    pRstPortInfo->bRcvdMsg = RST_TRUE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PRSM_Receive: Port %s: RcvdBpdu = OperEdge = FALSE RcvdMsg = TRUE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pAstCommPortInfo->u1PortRcvSmState = (UINT1) RST_PRCVSM_STATE_RECEIVE;

    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PRSM: Port %s: Moved to state RECEIVE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    u2Duration = (UINT2)
        (((AST_GET_BRGENTRY ())->u1MigrateTime) * AST_CENTI_SECONDS);
    /*Start the EdgeDelayWhileTimer */
    if (AstStartTimer ((VOID *) pPortEntry, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                       (UINT2) u2Duration) != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        return RST_FAILURE;
    }
    /*Call the BDSM with RCVD bpdu event */
    RstBrgDetectionMachine (RST_BRGDETSM_EV_BPDU_RCVD, u2PortNum);

    bPrevSendRstp = pAstCommPortInfo->bSendRstp;

    if (pAstCommPortInfo->bRcvdStp == RST_TRUE)
    {
        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_STP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_STP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_STP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }
    else if (pAstCommPortInfo->bRcvdRstp == RST_TRUE)
    {
        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_RSTP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }

    if (bPrevSendRstp != pAstCommPortInfo->bSendRstp)
    {
        AST_RED_SET_SYNC_FLAG (RST_DEFAULT_INSTANCE);
    }

    if (RstPortInfoMachine (RST_PINFOSM_EV_RCVD_BPDU, pPerStPortInfo,
                            pRcvBpdu) != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: RstPortInfoMachine function returned FAILURE!\n");
        AST_DBG (AST_BPDU_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: RstPortInfoMachine function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstPortRcvUpdtBpduVersion                  */
/*                                                                           */
/*    Description               : This function updates the flags by the     */
/*                                type of BPDU received.                     */
/*                                                                           */
/*    Input(s)                  : pRcvBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
VOID
RstPortRcvUpdtBpduVersion (tAstBpdu * pRcvBpdu, UINT2 u2PortNum)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PISM: Port %s: Updating BPDU version\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if ((((pRcvBpdu->u1Version == (UINT1) AST_VERSION_0) ||
          (pRcvBpdu->u1Version == (UINT1) AST_VERSION_1)) &&
         (pRcvBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN))
        || (pRcvBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG))
    {
        pAstCommPortInfo->bRcvdStp = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "UpdtBPDUVersion: Port %s: RcvdStp = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    else if (pRcvBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)
    {
        pAstCommPortInfo->bRcvdRstp = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "UpdtBPDUVersion: Port %s: RcvdRstp = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
}

/*****************************************************************************/
/* Function Name      : RstInitPortReceiveMachine                            */
/*                                                                           */
/* Description        : Initialises the Port Received State Machine.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstInitPortRxStateMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PRCVSM_MAX_EVENTS][20] = {
        "BEGIN", "RCVD_BPDU", "PORT_DISABLED"
    };
    UINT1               aau1SemState[RST_PRCVSM_MAX_STATES][20] = {
        "DISCARD", "RECEIVE"
    };
    for (i4Index = 0; i4Index < RST_PRCVSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PRSM][i4Index],
                    aau1SemEvent[i4Index], STRLEN(aau1SemEvent[i4Index]));
	gaaau1AstSemEvent[AST_PRSM][i4Index][STRLEN(aau1SemEvent[i4Index])] = '\0';
    }
    for (i4Index = 0; i4Index < RST_PRCVSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PRSM][i4Index],
                    aau1SemState[i4Index], STRLEN(aau1SemState[i4Index]));
	gaaau1AstSemState[AST_PRSM][i4Index][STRLEN(aau1SemState[i4Index])] = '\0';
    }

    /* Event 0 - RST_PRCVSM_EV_BEGIN */

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_BEGIN][RST_PRCVSM_STATE_DISCARD].
        pAction = (AST_PRXSM_FUNC) RstPortRcvSmMakeDiscard;

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_BEGIN][RST_PRCVSM_STATE_RECEIVE].
        pAction = (AST_PRXSM_FUNC) RstPortRcvSmMakeDiscard;

    /* Event 1 - RST_PRCVSM_EV_RCVD_BPDU */

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_RCVD_BPDU][RST_PRCVSM_STATE_DISCARD].
        pAction = (AST_PRXSM_FUNC) RstPortRcvSmChkRcvdBpdu;

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_RCVD_BPDU][RST_PRCVSM_STATE_RECEIVE].
        pAction = (AST_PRXSM_FUNC) RstPortRcvSmChkRcvdBpdu;

    /* Event 2 - RST_PRCVSM_EV_PORT_DISABLED */

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_PORT_DISABLED]
        [RST_PRCVSM_STATE_DISCARD].pAction = (AST_PRXSM_FUNC) NULL;

    RST_PORT_RECEIVE_MACHINE[RST_PRCVSM_EV_PORT_DISABLED]
        [RST_PRCVSM_STATE_RECEIVE].pAction =
        (AST_PRXSM_FUNC) RstPortRcvSmMakeDiscard;
}
