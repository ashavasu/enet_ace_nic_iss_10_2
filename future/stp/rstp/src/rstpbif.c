/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: rstpbif.c,v 1.68 2017/11/27 13:39:31 siva Exp $
 *
 * Description: This file contains routines which are called from the 
 *              core stp Module.
 *
 *******************************************************************/

#include "asthdrs.h"
#include "stpcli.h"
#include "stpclipt.h"

/*NPAPI_ROUTINES*/
#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstMiStpNpHwInit                                     */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI initialisation for STP.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiStpNpHwInit (VOID)
{

    /* The NPAPI Functions are wrapped up in RSTP module of STP. This is done
     * bcoz while CVLAN component of Provider Edge Bridge initialized, these
     * NPAPI routines should not be called for programming hardware */

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        if (RstpFsMiStpNpHwInit (AST_CURR_CONTEXT_ID ()) != FNP_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiRstpNpHwInit                                    */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI intilisation when RSTP is enabled              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiRstpNpInitHw (VOID)
{
    /* NPAPI Routines should not be called for CVLAN component */

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        RstpFsMiRstpNpInitHw (AST_CURR_CONTEXT_ID ());
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiNpDeleteAllFdbEntries                           */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for deleting All Fdb Entries of this context   */
/*                      when RSTP Module is enabled                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiNpDeleteAllFdbEntries (VOID)
{
    /* In case of CVLAN component, if this function is called, 
     * Flush only for CEP.
     * In case of SVLAN component, call the Normal NPAPI*/
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        VlanMiDeleteAllFdbEntries (AST_CURR_CONTEXT_ID ());

        return RST_SUCCESS;
    }

    /* Flush CEP for the CVLAN Context */
    AstVlanDeleteFdbEntries (AST_CURR_CONTEXT_INFO ()->u4CepIfIndex,
                             VLAN_NO_OPTIMIZE);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiRstpNpSetPortState                              */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Port State in RSTP Module per  */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      u1PortState - Port State for the HL Port Id          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstpNpSetPortState (UINT2 u2PortNum, UINT1 u1PortState)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPbPortInfo     *pPbPortInfo = NULL;
    INT4                i4RetVal = FNP_SUCCESS;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* If the component type is CVLAN and Port type is only PEP, then call
     * the PbRstpNpSetPortState. For CEP in CVLAN context call the Normal
     * FsMiRstpNpSetPortState*/

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT))
    {
        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        /* We dont have proper context id for CVLAN context, so we are passing
           the SVLAN context Id to the NPAPI */
        i4RetVal = RstpFsMiPbRstpNpSetPortState
            (AST_PB_CVLAN_INFO ()->pParentContext->u4ContextId,
             pPbPortInfo->u4CepIfIndex, pPbPortInfo->Svid, u1PortState);
    }
    else
    {
        /* NP programming for CEP, should be done only by the C-VLAN
         * component spanning tree. */
        if ((AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_CUSTOMER_EDGE_PORT) &&
            (AST_IS_CVLAN_COMPONENT () == RST_FALSE))
        {
            return RST_SUCCESS;
        }

        i4RetVal = RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                               AST_GET_PHY_PORT (pPortInfo->
                                                                 u2PortNo),
                                               u1PortState);
    }

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstMiMstpNpSetInstancePortState                      */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Inst Port State in RSTP Module */
/*                      per context.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      u2MstInst   - MstpInstance to which this port state  */
/*                      belongs                                              */
/*                      u1PortState - Port State for the HL Port Id          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiMstpNpSetInstancePortState (UINT2 u2PortNum,
                                 UINT2 u2MstInst, UINT1 u1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /*No need to Check for CVLAN context Since CVLAN component does not run
     * MSTP*/
    i4RetVal = FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 u2MstInst, u1PortState);

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;

}
#endif

#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstMiPvrstNpSetVlanPortState                         */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Vlan Port State in PVRST Module*/
/*                      per context.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      VlanId   - VlanId to which this port state belongs   */
/*                      u1PortState - Port State for the HL Port Id          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiPvrstNpSetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 u1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /*No need to Check for CVLAN context Since CVLAN component does not run
     * PVRST*/
    i4RetVal = PvrstFsMiPvrstNpSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 VlanId, u1PortState);

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function Name      : AstMiRstpNpGetPortState                              */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      RSTP.                                                */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the given port.        */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstpNpGetPortState (UINT2 u2PortNum, UINT1 *pu1PortState)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPbPortInfo     *pPbPortInfo = NULL;
    INT4                i4RetVal = FNP_SUCCESS;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* If the component type is CVLAN and Port type is only PEP, then call
     * the PbRstpNpSetPortState. For CEP in CVLAN context call the Normal
     * FsMiRstpNpSetPortState*/

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT))
    {
        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        /* We dont have proper context id for CVLAN context, so we are passing
           the SVLAN context Id to the NPAPI */
        i4RetVal = RstpFsMiPbRstNpGetPortState
            (AST_PB_CVLAN_INFO ()->pParentContext->u4ContextId,
             pPbPortInfo->u4CepIfIndex, pPbPortInfo->Svid, pu1PortState);
    }
    else
    {
        /* NP programming for CEP, should be done only by the C-VLAN
         * component spanning tree. */
        if ((AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_CUSTOMER_EDGE_PORT) &&
            (AST_IS_CVLAN_COMPONENT () == RST_FALSE))
        {
            *pu1PortState = AST_PORT_STATE_DISCARDING;
            return RST_SUCCESS;
        }
        i4RetVal = RstpFsMiRstNpGetPortState (AST_CURR_CONTEXT_ID (),
                                              AST_IFENTRY_IFINDEX (pPortInfo),
                                              pu1PortState);
    }

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstMiMstpNpGetInstancePortState                      */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      MSTP.                                                */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      u2MstInst   - MstpInstance to which this port state  */
/*                      belongs                                              */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the Port Id            */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiMstpNpGetInstancePortState (UINT2 u2PortNum,
                                 UINT2 u2MstInst, UINT1 *pu1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /* No need to Check for CVLAN context Since CVLAN component does not run
     * MSTP*/
    i4RetVal = MstpFsMiMstNpGetPortState (AST_CURR_CONTEXT_ID (),
                                          u2MstInst,
                                          AST_GET_PHY_PORT (u2PortNum),
                                          pu1PortState);

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }

    return RST_FAILURE;

}
#endif
#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstMiPvrstNpGetVlanPortState                         */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      PVRST.                                               */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      VlanId   - VlanId to which this port state           */
/*                      belongs                                              */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the Port Id            */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiPvrstNpGetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 *pu1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /* No need to Check for CVLAN context Since CVLAN component does not run
     * PVRST*/
    i4RetVal = PvrstFsMiPvrstNpGetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 VlanId,
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 pu1PortState);

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }

    return RST_FAILURE;

}
#endif
#endif

/*L2IWF_ROUTINES*/

/*****************************************************************************/
/* Function Name      : AstCreateSpanningTreeInstanceInL2Iwf                 */
/*                                                                           */
/* Description        : This Function is used to create the Spanning tree    */
/*                      instance in L2IWF. The STP instance should not be    */
/*                      created in L2IWF for CVLAN component in PEB          */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be created in L2IWF       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstCreateSpanningTreeInstanceInL2Iwf (UINT2 u2MstInst)
{
    /* If the component is CVLAN, do not call L2IWF function for
     * creating the separate spanning tree instance.*/
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        L2IwfCreateSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteMstInstanceFromL2Iwf                        */
/*                                                                           */
/* Description        : This Function is used to delete the Spanning tree    */
/*                      instance in L2IWF. The STP instance should not be    */
/*                      delete in L2IWF for CVLAN component in PEB          */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be created in L2IWF       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstDeleteMstInstanceFromL2Iwf (UINT2 u2MstInst)
{
    /* If the component is CVLAN, do not call L2IWF function for
     * deleting the spanning tree instance.*/
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        if (L2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst)
            != L2IWF_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetPortOperStatusFromL2Iwf                        */
/*                                                                           */
/* Description        : This Function is used to get the physical port OR    */
/*                      logical Port (PEP) oper status from l2iwf            */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstGetPortOperStatusFromL2Iwf (UINT1 u1Module, UINT2 u2PortNum,
                               UINT1 *pu1OperStatus)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPbPortInfo     *pPbPortInfo = NULL;
    INT4                i4RetVal = L2IWF_SUCCESS;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo == NULL)
    {

        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MSG: PortInfo not present for port %d\n", u2PortNum);
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                      "MSG: PortInfo not present for port %d\n", u2PortNum);
        return RST_FAILURE;
    }

    /* If the component type is CVLAN and Port type is only PEP, then call
     * the PbPortOperStatus. For CEP in CVLAN context call the Normal
     * L2IwfGetPortOperStatus */

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT))
    {
        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        i4RetVal =
            AstL2IwfGetPbPortOperStatus (u1Module, pPbPortInfo->u4CepIfIndex,
                                         pPbPortInfo->Svid, pu1OperStatus);

    }
    else
    {
        i4RetVal =
            AstL2IwfGetPortOperStatus (u1Module, pPortInfo->u4IfIndex,
                                       pu1OperStatus);

    }

    if (i4RetVal == L2IWF_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstGetInstPortStateFromL2Iwf                         */
/*                                                                           */
/* Description        : This Function is used to get the instance port state */
/*                      of physical OR logical Port (PEP)from l2iwf          */
/*                                                                           */
/* Input(s)           : u2MstInst - MST instance ID                          */
/*                      u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_PORT_STATE_FORWARDING / AST_PORT_STATE_DISCARDING*/
/*****************************************************************************/

UINT1
AstGetInstPortStateFromL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum)
{
    tAstPbPortInfo     *pPbPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               u1State = 0;

    /* CVLAN Component Should be updated only if the port is CEP and
     * Instance is default spanning tree instance*/
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* The CVLAN component Port state should be get only for the Provider Edge
     * port and instance is default instance. For CEP and other port types use
     * normal L2IwfGetInstPortState*/
    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT) &&
        (u2MstInst == RST_DEFAULT_INSTANCE))
    {
        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        u1State = AstL2IwfGetPbPortState (pPbPortInfo->u4CepIfIndex,
                                          pPbPortInfo->Svid);
    }
    else
    {
        u1State =
            AstL2IwfGetInstPortState (u2MstInst, AST_GET_IFINDEX (u2PortNum));
    }
    return u1State;
}

/*****************************************************************************/
/* Function Name      : AstSetInstPortStateFromL2Iwf                         */
/*                                                                           */
/* Description        : This Function is used to set the instance port state */
/*                      of physical OR logical Port (PEP)from l2iwf          */
/*                                                                           */
/* Input(s)           : u2MstInst - MST instance ID                          */
/*                      u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/

INT4
AstSetInstPortStateToL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum, UINT1 u1PortState)
{
    tAstPbPortInfo     *pPbPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT4               u4Ticks = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    /* CVLAN Component Should be updated only if the port is CEP and
     * Instance is default spanning tree instance*/
    if (AstGetContextInfoFromIfIndex
        ((UINT4) AST_GET_IFINDEX (u2PortNum), &u4ContextId,
         &u2LocalPortId) != RST_FAILURE)
    {
        AstGetSysTime (&u4Ticks);

        /* Update global Port State Change Time Stamp */
        pBrgInfo = AST_GET_BRGENTRY ();
        if (pBrgInfo != NULL)
        {
            pBrgInfo->u4PortStateChangeTimeStamp = u4Ticks;
        }
        else
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MSG: BrgInfo not present\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: BrgInfo not present\n");

        }

        /* Update Per Port State Chnage Time Stamp */
        pPortInfo = AST_GET_PORTENTRY (u2LocalPortId);
        if (pPortInfo != NULL)
        {
            pPortInfo->u4PortStateChangeTimeStamp = u4Ticks;
        }
        else
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                          "MSG: PortInfo not present for port %d\n", u2PortNum);
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                          "MSG: PortInfo not present for port %d\n", u2PortNum);
        }

        /* Update Per Instance Per Port State Change Time Stamp */
        if (u2MstInst != AST_TE_MSTID)
        {
            if (AST_GET_PERST_INFO (u2MstInst) != NULL)
            {
                pAstPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2LocalPortId, u2MstInst);
                if (pAstPerStPortInfo != NULL)
                {
                    pAstPerStPortInfo->u4PortStateChangeTimeStamp = u4Ticks;
                }
                else
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                  "MSG: InstanceInfo not present for port %d instance %d\n",
                                  u2PortNum, u2MstInst);
                    AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                                  "MSG: InstanceInfo not present for port %d instance %d\n",
                                  u2PortNum, u2MstInst);
                }
            }
        }
    }

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    /* In CVLAN comp, only for PEP in Default instance update, PB Port state
     * in L2IWF. Ideally for PEP, other than default instance no other
     * instance should not be calling this function.For CEP in CVLAN comp
     * and other SVLAN ports maintain the L2IwfSetInstPortState*/

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT) &&
        (u2MstInst == RST_DEFAULT_INSTANCE))
    {
        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        i4RetVal = AstL2IwfSetPbPortState (pPbPortInfo->u4CepIfIndex,
                                           pPbPortInfo->Svid, u1PortState);
    }
    else
    {
        /* For Customer Edge port, only the C-VLAN component can program 
         * the state in L2Iwf. */

        if (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_CUSTOMER_EDGE_PORT)
        {

            if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
            {
                if (gau1AstSystemControl[AST_CURR_CONTEXT_ID ()]
                    == (UINT1) MST_START)
                {
                    /* In case of MSTP running on SVLAN comp, the CEP 
                     * Port state determined by CVLAN comp should be prog.
                     * to all MST Instances. This is done bcoz all external
                     * modules will ask the portstate for CEP based on VLAN*/

                    i4RetVal =
                        AstL2IwfPbSetInstCepPortState
                        (AST_GET_IFINDEX (u2PortNum), u1PortState);
                }
                else
                {
                    /*In case of RSTP running in SVLAN comp, it is 
                     * enough to program the CEP portstate in the default
                     * instance itself*/
                    i4RetVal =
                        AstL2IwfSetInstPortState (u2MstInst,
                                                  (UINT2)
                                                  AST_GET_IFINDEX (u2PortNum),
                                                  u1PortState);
                }
            }
        }
        else
        {
            i4RetVal = AstL2IwfSetInstPortState (u2MstInst,
                                                 (UINT2)
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 u1PortState);
        }
    }

    if (i4RetVal != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPortOperEdgeStatusToL2Iwf                      */
/*                                                                           */
/* Description        : This Function is used to set the Port Oper Edge Status*/
/*                      of physical OR logical Port (PEP)to  l2iwf           */
/*                                                                           */
/* Input(s)           : u2PortNum -  CEP Port Number / Physical Port Number  */
/*                      bOperEdge - OSIX_TRUE/OSIX_FALSE                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

INT4
AstSetPortOperEdgeStatusToL2Iwf (UINT2 u2PortNum, BOOL1 bOperEdge)
{
    tAstPbPortInfo     *pPbPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    INT4                i4RetVal = L2IWF_SUCCESS;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* If the component type is CVLAN and Port type is only PEP, then call
     * the PbPortOperEdgeStatus. For CEP in CVLAN context call the Normal
     * L2IwfSetPortOperEdgeStatus */

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT))
    {

        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        i4RetVal = AstL2IwfSetPbPortOperEdgeStatus (pPbPortInfo->u4CepIfIndex,
                                                    pPbPortInfo->Svid,
                                                    bOperEdge);
    }
    else
    {
        i4RetVal = AstL2IwfSetPortOperEdgeStatus (AST_CURR_CONTEXT_ID (),
                                                  u2PortNum, bOperEdge);
    }

    if (i4RetVal == L2IWF_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstGetPortOperEdgeStatusFromL2Iwf                    */
/*                                                                           */
/* Description        : This Function is used to get the Port Oper Edge Status*/
/*                      of physical OR logical Port (PEP)from  l2iwf         */
/*                                                                           */
/* Input(s)           : u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : pbOperEdge - OSIX_TRUE/OSIX_FALSE                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

INT4
AstGetPortOperEdgeStatusFromL2Iwf (UINT2 u2PortNum, BOOL1 * pbOperEdge)
{
    tAstPbPortInfo     *pPbPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    INT4                i4RetVal = L2IWF_SUCCESS;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* If the component type is CVLAN and Port type is only PEP, then call
     * the PbPortOperEdgeStatus. For CEP in CVLAN context call the Normal
     * L2IwfGetPortOperEdgeStatus */

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT))
    {

        pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);

        i4RetVal = AstL2IwfGetPbPortOperEdgeStatus (pPbPortInfo->u4CepIfIndex,
                                                    pPbPortInfo->Svid,
                                                    pbOperEdge);
    }
    else
    {
        i4RetVal =
            AstL2IwfGetPortOperEdgeStatus ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                           pbOperEdge);
    }

    if (i4RetVal == L2IWF_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*PROTOCOL_ROUTINES*/

/*****************************************************************************/
/* Function Name      : AstPbAllCVlanCompShutdown                            */
/*                                                                           */
/* Description        : This Function takes care of shutdown of all the      */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompShutdown (VOID)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pAstPortEntry)
    {
        if (pAstPortEntry != NULL)
        {
            /* If this port is customer edge port, then the cvlan component 
             * should be shutdown*/
            if (AstPbCVlanCompShutdown (pAstPortEntry) != RST_SUCCESS)
            {
                return RST_FAILURE;
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompShutdown                               */
/*                                                                           */
/* Description        : This Function takes care of shutdown of CVLAN component*/
/*                      of a Port.                                           */
/*                                                                           */
/* Input(s)           : pPortEntry - CEP Port Entry                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstPbCVlanCompShutdown (tAstPortEntry * pAstPortEntry)
{

    if (AST_PB_IS_PORT_SVLAN_CEP (pAstPortEntry) == RST_TRUE)
    {
        if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: CVlan Select context failed for"
                          "CEP Port %d \r\n", pAstPortEntry->u4IfIndex);
            return RST_FAILURE;
        }

        /* This function will take care of restoring the cvlan context
         * to the SVlan context. So No need to restore the context
         * if this function succeeds*/
        if (RstPbDeInitCVlanComponent () != RST_SUCCESS)
        {
            AstPbRestoreContext ();
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstPbAllCVlanCompDisable                             */
/*                                                                           */
/* Description        : This Function takes care of disabling   all the      */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompDisable (VOID)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pAstPortEntry)
    {

        if (pAstPortEntry != NULL)
        {
            /* If this port is customer edge port, then the cvlan component 
             * should be shutdown*/
            if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
            {
                AstPbCVlanCompDisable (pAstPortEntry);
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbAllCVlanCompEnable                              */
/*                                                                           */
/* Description        : This Function takes care of enabling all the         */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompEnable (VOID)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pAstPortEntry)
    {
        if (pAstPortEntry != NULL)
        {
            /* If this port is customer edge port, then the cvlan component 
             * should be shutdown*/
            if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
            {
                if (AstPbCVlanCompEnable (pAstPortEntry) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC
                                  | AST_ALL_FAILURE_DBG,
                                  "SYS: C-VLAN component with CEP - %d : "
                                  "RSTP Module Enable Failed !!!\n",
                                  (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);

                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_DBG,
                                  "SYS: C-VLAN component with CEP - %d : "
                                  "RSTP Module Enable Failed !!!\n",
                                  (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);
                    return RST_FAILURE;
                }
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompEnable                                 */
/*                                                                           */
/* Description        : This Function takes care of enabling the CVLAN       */
/*                      component in a context                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanCompEnable (tAstPortEntry * pAstPortEntry)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    /* If the spanning tree status is disabled on the given port in S-VLAN
     * Context, then don't enable C-VLAN spanning tree corresponding to 
     * that port. */
    pRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (pAstPortEntry->u2PortNo,
                                     RST_DEFAULT_INSTANCE);
    if (pRstPortInfo->bPortEnabled != (tAstBoolean) RST_TRUE)
    {
        return RST_SUCCESS;
    }

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: CVlan Select context failed for"
                      "CEP Port %d \r\n", pAstPortEntry->u4IfIndex);
        return RST_FAILURE;
    }

    if (RstComponentEnable () != RST_SUCCESS)
    {
        AstPbRestoreContext ();
        return RST_FAILURE;
    }

    AstPbRestoreContext ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbEnableCepPort                                   */
/*                                                                           */
/* Description        : This Function takes care of handling Oper status or  */
/*                      stp status for a CEP port.                           */
/*                                                                           */
/* Input(s)           : u2Port - Local port number of CEP port (in parent    */
/*                               context).                                   */
/*                      u2InstanceId - RST_DEFAULT_INSTANCE                  */
/*                      u1TrigType - AST_EXT_PORT_UP / AST_STP_PORT_UP       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstPbEnableCepPort (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType)
{
    /* For CEP, handling oper status or stp port up is different. 
     * For CEP, we need to switch to the corresponding C-VLAN component, 
     * and then enable the port there. */
    if (AstPbHandleCcompCepPortStatus (u2Port, u1TrigType) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    /* Don't update the port oper up in the S-VLAN component CEP,
     * but update STP up in the S-VLAN component CEP. */
    if (!(u1TrigType == AST_EXT_PORT_UP))
    {
        if (AstEnablePort (u2Port, u2InstanceId, u1TrigType) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstEnablePort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG
                     | AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstEnablePort function returned FAILURE\n");
            return RST_FAILURE;
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompDisable                                */
/*                                                                           */
/* Description        : This Function takes care of enabling the CVLAN       */
/*                      component in a context                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanCompDisable (tAstPortEntry * pAstPortEntry)
{
    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: CVlan Select context failed for"
                      "CEP Port %d \r\n", pAstPortEntry->u4IfIndex);

        return RST_FAILURE;
    }

    if (RstComponentDisable () != RST_SUCCESS)
    {
        AstPbRestoreContext ();
        return RST_FAILURE;
    }

    AstPbRestoreContext ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUpdatePortType                                    */
/*                                                                           */
/* Description        : This Function gets the port type from L2Iwf and fills*/
/*                      in the given port entry.                             */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type to be updated  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstUpdatePortType (tAstPortEntry * pPortEntry, UINT1 u1BrgPortType)
{
    AST_GET_BRG_PORT_TYPE (pPortEntry) = u1BrgPortType;
    return;

}

/*****************************************************************************/
/* Function Name      : AstUpdateCompType                                    */
/*                                                                           */
/* Description        : This Function takes care of updating the comp type   */
/*                      on basis of different bridge modes                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstUpdateCompType (VOID)
{

    /* This function should not be called in CVLAN context */
    /* In case of Normal customer Bridges and Q-in-Q bridges, the component
     * type is initialised to AST_C_VLAN. In case of Provider Bridges, the
     * component type is initialised to AST_PB_S_VLAN. This function is 
     * called only for initialisation.
     * This component type is initialised for smooth functioning of the code*/

    if (AST_IS_1AD_BRIDGE () == RST_FALSE)
    {
        AST_COMP_TYPE () = AST_C_VLAN;
    }
    else if (AST_BRIDGE_MODE () == AST_ICOMPONENT_BRIDGE_MODE)
    {
        AST_COMP_TYPE () = AST_PB_I_COMP;
    }
    else
    {
        AST_COMP_TYPE () = AST_PB_S_VLAN;
    }

    return;
}

/*OTHER MODULE ROUTINES*/

/*****************************************************************************/
/* Function Name      : AstVlanResetShortAgeoutTime                          */
/*                                                                           */
/* Description        : This Function takes care of updating the short ageout*/
/*                      time in the VLAN module                              */
/*                                                                           */
/* Input(s)           : pPortEntry - Physical Port Entry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
AstVlanResetShortAgeoutTime (tAstPortEntry * pPortInfo)
{
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        VlanResetShortAgeoutTime ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo));
        return RST_SUCCESS;

    }
    /* In case of CVLAN component, only for CEP, we need to set short
     * ageout time. This is done bcoz we are learning on CEP but not on
     * PEP*/
    if (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_CUSTOMER_EDGE_PORT)
    {
        VlanResetShortAgeoutTime ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsVlanEnabledInContext                            */
/*                                                                           */
/* Description        : This Function checks whether VLAN is enabled in the  */
/*                      current context                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*****************************************************************************/

INT4
AstIsVlanEnabledInContext ()
{
    /* In case of CVLAN component, VLAN is VLAN Enabled should not be called */

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
        {
            return RST_TRUE;
        }

        return RST_FALSE;

    }

    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstVlanSetShortAgeoutTime                            */
/*                                                                           */
/* Description        : This Function sets the short age out time in VLAN    */
/*                      module for a given port                              */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                            */
/*****************************************************************************/

INT4
AstVlanSetShortAgeoutTime (tAstPortEntry * pPortInfo)
{
    UINT2               u2FwdDelay = AST_INIT_VAL;

    u2FwdDelay =
        (UINT2) (pPortInfo->DesgTimes.u2ForwardDelay / AST_CENTI_SECONDS);
    if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
    {
        /* Set the short age out time only if the port is not a PEP. */
        VlanSetShortAgeoutTime ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo),
                                u2FwdDelay);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstBrgDelFwdEntryForPort                             */
/*                                                                           */
/* Description        : This Function deletes all fdb entries learned on the */
/*                      given port                                           */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                            */
/*****************************************************************************/

INT4
AstBrgDelFwdEntryForPort (tAstPortEntry * pPortInfo)
{
    /* Delete the fwd entry, only when the port is not a provider 
     * edge port. */
    /* If Vlan is disabled, the PEPs will be disabled. Hence this
     * function will not be called for PEPs. */
    if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
    {
#ifdef BRIDGE_WANTED
        BrgDeleteFwdEntryForPort (AST_IFENTRY_IFINDEX (pPortInfo));
#endif
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleOutgoingPktOnPort                           */
/*                                                                           */
/* Description        : This function will be called by RSTP/MSTP module for */
/*                      sending a protocol port on a physical port / PEP     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be sended out                       */
/*                      pAstPortInfo - Port Info on which pkt should be send */
/*                      u4PktSize - Size of the Packet                       */
/*                      u2Protocol - Protocol Id to be sended to CFA         */
/*                      u1EncapType - Encap type of the pkt                  */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
AstHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tAstPortEntry * pAstPortInfo, UINT4 u4PktSize, UINT2
                            u2Protocol, UINT1 u1EncapType)
{
    UINT4               u4CepIfIndex = 0;
    UINT1               u1BPDUFlags = 0;
    UINT4               u2Port;
    UINT4               i4Compatible = AST_TRUE;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pAstPortInfo->u2PortNo);

/* HITLESS RESTART */
    if (AST_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1BPDUFlags,
                                   AST_TC_BIT_OFFSET, AST_TC_LENGTH);
        if (AST_HR_CHECK_STDY_ST_PKT (u1BPDUFlags))
        {
            AstRedHRSendStdyStPkt (pBuf, u4PktSize,
                                   (UINT2) pAstPortInfo->u4PhyPortIndex,
                                   pAstPortInfo->u2HelloTime);
            return RST_SUCCESS;
        }
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pAstPortInfo->u2PortNo);
    if (pAstCommPortInfo->bSendRstp == AST_TRUE)
    {
        if (AST_FORCE_VERSION != AST_VERSION_2)
        {
            i4Compatible = AST_MST_MODE;
        }
    }
    if (AST_IS_MST_STARTED () && (i4Compatible == AST_MST_MODE))
    {
        if ((gi1MstTxAllowed == AST_FALSE)
            && (AST_IS_PVRST_ENABLED () == AST_FALSE))
        {

            u2Port = pAstPortInfo->u2PortNo;
            if (u2Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                pAstPortInfo->bTransmitBpdu = AST_TRUE;
            }
            else
            {
                AST_TRC (AST_TXSM_DBG | AST_ALL_FAILURE_TRC,
                         "MSG: Port greater than Max Interfaces " "!!!\n");
            }
            AST_RELEASE_CRU_BUF (pBuf, RST_FALSE);
            return RST_SUCCESS;
        }

    }

    if ((AST_IS_I_COMPONENT () == RST_TRUE) &&
        (pAstPortInfo->u1PortType == AST_VIRTUAL_INSTANCE_PORT))
    {
        /* VIP. Indicate the L2IWF API Provided.
         * That will take care of encapsulating appropriately, depending
         * on the bridge mode
         * */
        return (AstL2IwfTransmitBpduOnVip (AST_CURR_CONTEXT_ID (),
                                           AST_IFENTRY_IFINDEX (pAstPortInfo),
                                           pBuf));
    }

    if ((AST_IS_CVLAN_COMPONENT () == RST_FALSE) ||
        (pAstPortInfo->u1PortType == AST_CUSTOMER_EDGE_PORT))
    {
        /*For SVLAN context and for sending Packets on CEP (ie) For sending
         * Bpdus on the Physical Ports, this L2Iwf function
         * should be called */

        AstL2IwfHandleOutgoingPktOnPort (pBuf,
                                         AST_GET_PHY_PORT
                                         (pAstPortInfo->u2PortNo),
                                         u4PktSize, u2Protocol, u1EncapType);

    }
    else
    {
        /* For Sending BPDUs on PEP, this function is called. The Logical Port
         * BPDUs will be processed by VLAN module*/
        u4CepIfIndex = AST_CURR_CONTEXT_INFO ()->u4CepIfIndex;

        AstL2IwfSendCustBpduOnPep (u4CepIfIndex, pAstPortInfo->u2ProtocolPort,
                                   pBuf, u4PktSize);

    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstHandleCreatePepPort                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to create a PEP Port in the CVLAN component   */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstHandleCreatePepPort (tAstMsgNode * pMsgNode)
{
    tAstPortEntry      *pAstPortInfo = NULL;

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()) ||
        (AST_IS_PVRST_STARTED ()))
    {
        pAstPortInfo = AST_GET_PORTENTRY (pMsgNode->u4PortNo);

        if (pAstPortInfo == NULL)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Port Entry not present in Global IfIndex "
                     "Table!!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Port Entry not present in Global IfIndex "
                     "Table!!!\n");
            return RST_FAILURE;
        }

#ifdef NPAPI_WANTED
        /* First set the port state for the given pep as
         * discarding. */
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            RstpFsMiPbRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                          AST_GET_IFINDEX (pMsgNode->u4PortNo),
                                          pMsgNode->uMsg.VlanId,
                                          AST_PORT_STATE_DISCARDING);
        }
#endif

        if (AstPbSelectCvlanContext (pAstPortInfo) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            return RST_FAILURE;
        }

        if (RstPbCreateCvlanPort (AST_IFENTRY_IFINDEX (pAstPortInfo),
                                  pMsgNode->uMsg.VlanId, NULL,
                                  NULL) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstPbCreateCvlanPort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: RstPbCreateCvlanPort function returned FAILURE\n");
            AstPbRestoreContext ();
            return RST_FAILURE;
        }

        AstPbRestoreContext ();

    }

    AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                  AST_MGMT_DBG,
                  "MSG: Management Creation of Pep Port (%u, %u) Success\n",
                  (pMsgNode->u4PortNo), (pMsgNode->uMsg.VlanId));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandlePepOperStatus                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to update a PEP Port status in the CVLAN      */
/*                      component                                            */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstHandlePepOperStatus (tAstMsgNode * pNode)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    tAstPort2IndexMap  *pPort2IndexMap = NULL;

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()) ||
        (AST_IS_PVRST_STARTED ()))
    {
        pAstPortInfo = AST_GET_PORTENTRY (pNode->u4PortNo);

        if (pAstPortInfo == NULL)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                          "MSG: CEP Port :%u entry does not exist. \n",
                          pNode->u4PortNo);
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                          AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                          "MSG: CEP Port :%u entry does not exist. \n",
                          pNode->u4PortNo);
            return RST_FAILURE;
        }

        if (AstPbSelectCvlanContext (pAstPortInfo) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            return RST_FAILURE;
        }

        pPort2IndexMap =
            AstPbCVlanGetPort2IndexMapBasedOnProtoPort (pNode->uMsg.VlanId);

        if (pPort2IndexMap == NULL)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Port Not Created in CVLAN context !!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Port Not Created in CVLAN context !!!\n");
            AstPbRestoreContext ();
            return RST_FAILURE;
        }

        switch (pNode->MsgType)
        {
            case AST_ENABLE_PEP_PORT_MSG:

                if (RstUpdatePepOperPtoP (pPort2IndexMap->u2PortIndex,
                                          AST_EXT_PORT_UP) != RST_SUCCESS)
                {
                    AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                             "MSG: RstUpdatePepOperPtoP function returned FAILURE\n");
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                             AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                             "MSG: RstUpdatePepOperPtoP function returned FAILURE\n");
                    AstPbRestoreContext ();
                    return RST_FAILURE;
                }
                if (AST_IS_RST_STARTED ())
                {
                    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                    {
                        AstRedSendSyncMessages (RST_DEFAULT_INSTANCE,
                                                pPort2IndexMap->u2PortIndex,
                                                RED_AST_OPER_STATUS,
                                                AST_EXT_PORT_UP);
                    }
                }
                if (RstEnablePort (pPort2IndexMap->u2PortIndex,
                                   AST_EXT_PORT_UP) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                  "MSG: RstEnablePort function returned FAILURE "
                                  "for port %s \n",
                                  AST_GET_IFINDEX_STR (pPort2IndexMap->
                                                       u2PortIndex));
                    AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                                  AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                                  "MSG: RstEnablePort function returned FAILURE "
                                  "for port %s \n",
                                  AST_GET_IFINDEX_STR (pPort2IndexMap->
                                                       u2PortIndex));
                    AstPbRestoreContext ();
                    return RST_FAILURE;
                }
                break;

            case AST_DISABLE_PEP_PORT_MSG:

                if (RstDisablePort (pPort2IndexMap->u2PortIndex,
                                    AST_EXT_PORT_DOWN) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                  "MSG: RstDisablePort function returned "
                                  "FAILURE for port %s \n",
                                  AST_GET_IFINDEX_STR
                                  (pPort2IndexMap->u2PortIndex));
                    AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                                  AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                                  "MSG: RstDisablePort function returned "
                                  "FAILURE for port %s \n",
                                  AST_GET_IFINDEX_STR
                                  (pPort2IndexMap->u2PortIndex));
                    AstPbRestoreContext ();
                    return RST_FAILURE;
                }
                if (AST_IS_RST_STARTED ())
                {
                    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                    {
                        AstRedClearPduOnActive (pPort2IndexMap->u2PortIndex);
                        AstRedSendSyncMessages (RST_DEFAULT_INSTANCE,
                                                pPort2IndexMap->u2PortIndex,
                                                RED_AST_OPER_STATUS,
                                                AST_EXT_PORT_DOWN);
                    }
                }
                break;
        }

        if (pPort2IndexMap != NULL)
        {
            AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                          AST_MGMT_DBG,
                          "MSG: Management Oper Status handling of PEP Port %s\n",
                          AST_GET_IFINDEX_STR (pPort2IndexMap->u2PortIndex));
        }

        AstPbRestoreContext ();
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleDeletePepPort                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to delete a PEP Port status in the CVLAN      */
/*                      component                                            */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstHandleDeletePepPort (tAstMsgNode * pNode)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    tAstPort2IndexMap  *pPort2IndexMap = NULL;
    UINT4               u4CepIfIndex = 0;

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()) ||
        (AST_IS_PVRST_STARTED ()))
    {
        pAstPortInfo = AST_GET_PORTENTRY (pNode->u4PortNo);

        if (pAstPortInfo == NULL)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Port Entry not present in Global IfIndex Table!!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Port Entry not present in Global IfIndex Table!!!\n");
            return RST_FAILURE;
        }

        u4CepIfIndex = AST_IFENTRY_IFINDEX (pAstPortInfo);

        if (AstPbSelectCvlanContext (pAstPortInfo) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Switching to CVLAN context FAILED !!!\n");
            return RST_FAILURE;
        }

        pPort2IndexMap =
            AstPbCVlanGetPort2IndexMapBasedOnProtoPort (pNode->uMsg.VlanId);

        if (pPort2IndexMap == NULL)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: Port Not Created in CVLAN context !!!\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: Port Not Created in CVLAN context !!!\n");
            AstPbRestoreContext ();
            return RST_FAILURE;
        }

        if (RstPbDeleteCvlanPort (u4CepIfIndex,
                                  pPort2IndexMap->u2PortIndex,
                                  pPort2IndexMap->u2ProtPort,
                                  NULL) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: RstPbDeleteCvlanPort function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: RstPbDeleteCvlanPort function returned FAILURE\n");
            AstPbRestoreContext ();
            return RST_FAILURE;
        }

        AstPbRestoreContext ();
    }

    AST_DBG_ARG2 (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                  AST_MGMT_DBG,
                  "MSG: Management Deletion of PEP Port %u  VlanId %u "
                  "Success\n", (pNode->u4PortNo), (pNode->uMsg.VlanId));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSwitchCVlanContext                              */
/*                                                                           */
/* Description        : This function is  called from the BPDU Processing    */
/*                      to find out whether BPDU is received on a PEP. If    */
/*                      a BPDU is received on a PEP, then it will switch the */
/*                      context to CVLAN comp and changes the protocol port  */
/*                      to HL Port                                           */
/*                                                                           */
/* Input(s)           : pIfaceId - Interface Identifier                      */
/*                                                                           */
/* Output(s)          : *pu2PortNum - HL Port Id                             */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/* Returns            : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbSwitchCVlanContext (tAstInterface * pIfaceId, UINT2 *pu2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPort2IndexMap  *pPort2IndexMap = NULL;

    pAstPortEntry = AstGetIfIndexEntry (pIfaceId->u4IfIndex);

    if (pAstPortEntry == NULL)
    {
        /* The Port is not present in the Global IfIndex Table. */
        return RST_FAILURE;
    }

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        /* FATAL ERROR !!!!! This is Impossible...Port type is CEP
         * and the Port has no cvlan context*/
        return RST_FAILURE;
    }

    /* Sub reference is not invalid and it is not CEP, So it must be
     * a PEP*/

    if (pIfaceId->u2_SubReferenceNum != 0)
    {
        /* Convert Protocol Port to HLPort */
        pPort2IndexMap =
            AstPbCVlanGetPort2IndexMapBasedOnProtoPort (pIfaceId->
                                                        u2_SubReferenceNum);

        if (pPort2IndexMap == NULL)
        {
            /* PEP Not Created in CVLAN Component */
            AstPbRestoreContext ();

            /* The Message Has Sub Reference Number, But no PEP exists in
             * the CVLAN context. So BPDU came for invalid PEP. So discard
             * the BPDU*/
            return RST_FAILURE;

        }

        *pu2PortNum = pPort2IndexMap->u2PortIndex;

        /* PEP's Local Port has been found and the u2Local Port has been
         * changed to PEP*/
        return RST_SUCCESS;
    }

    /*For CEP, the u2PortNum is always 1 irrespective of any CVLAN context.
     * Watch out, This is ASSUMPTION.*/
    if (pIfaceId->u2_SubReferenceNum == 0)
    {
        *pu2PortNum = AST_PB_CEP_CVLAN_LOCAL_PORT_NUM;

    }
    /* BPDU came for CEP in the CVLAN context */
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbRestoreContext                                  */
/*                                                                           */
/* Description        : This Function is responsible for restoring the context*/
/*                      pointer which is already stored in gpAstParentCtxtInfo*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
AstPbRestoreContext (VOID)
{
    /* WARNING : This function should be called in CVLAN context only.
       It should be used in CVLAN component of Provider Edge Bridge */
    AST_CURR_CONTEXT_INFO () = AST_PREV_CONTEXT_INFO ();

    AST_PREV_CONTEXT_INFO () = NULL;

    /* Restore Red Context too. */
    AstRedPbRestoreContext ();

    return;
}

/*****************************************************************************/
/* Function Name      : AstHandleSVlanModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to handle the SVLAN module     */
/*                      status (Enable/Disable)                              */
/*                                                                           */
/* Input(s)           : pNode    - message Node                              */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstHandleSVlanModuleStatus (tAstMsgNode * pNode)
{
    switch (pNode->MsgType)
    {
        case AST_SVLAN_ENABLE_MSG:
        {
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                /* Update the S-VLAN admin status. */
                AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

                /* If the global module status is disabled, then
                 * don't enable s-vlan spanning tree. */
                if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] ==
                    PVRST_DISABLED)
                {
                    return RST_FAILURE;
                }

                if (AST_MODULE_STATUS != PVRST_ENABLED)
                {
                    if (PvrstComponentEnable () != PVRST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }

                return RST_SUCCESS;
            }
#endif /*PVRST_WANTED */
#ifdef MSTP_WANTED
            if (AST_IS_MST_STARTED ())
            {
                /* Update the S-VLAN admin status. */
                AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

                /* If the global module status is disabled, then
                 * don't enable s-vlan spanning tree. */
                if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_DISABLED)
                {
                    return RST_FAILURE;
                }

                if (AST_MODULE_STATUS != MST_ENABLED)
                {
                    if (MstComponentEnable () != MST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }

                return RST_SUCCESS;
            }
#endif /*MSTP_WANTED */
            if (AST_IS_RST_STARTED ())
            {
                /* Update the S-VLAN admin status. */
                AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

                /* If the global module status is disabled, then
                 * don't enable s-vlan spanning tree. */
                if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_DISABLED)
                {
                    return RST_FAILURE;
                }

                if (AST_MODULE_STATUS != RST_ENABLED)
                {
                    if (RstComponentEnable () != RST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }

                return RST_SUCCESS;
            }

            break;
        }

        case AST_SVLAN_DISABLE_MSG:
        {
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_STARTED ())
            {
                if (AST_MODULE_STATUS != PVRST_DISABLED)
                {
                    if (PvrstComponentDisable () != PVRST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return RST_SUCCESS;
            }
#endif /*PVRST_WANTED */
#ifdef MSTP_WANTED
            if (AST_IS_MST_STARTED ())
            {
                if (AST_MODULE_STATUS != MST_DISABLED)
                {
                    if (MstComponentDisable () != MST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return RST_SUCCESS;
            }
#endif /*MSTP_WANTED */
            if (AST_IS_RST_STARTED ())
            {
                if (AST_MODULE_STATUS != RST_DISABLED)
                {
                    if (RstComponentDisable () != RST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }
                }
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return RST_SUCCESS;
            }

            break;
        }
        default:
            return RST_FAILURE;
            break;
    }

    return RST_FAILURE;

}

/*****************************************************************************/
/* Function Name      : AstPbSetPortPathCostInCvlanComp                      */
/*                                                                           */
/* Description        : This function is used to configure the Path cost for */
/*                     CEP in a CVLAN component                              */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      u4PathCost - Configured PathCost                     */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetPortPathCostInCvlanComp (tAstPortEntry * pParentPortEntry,
                                 UINT4 u4PortPathCost)
{
    INT4                i4RetVal = RST_SUCCESS;

    /* Switch to C-VLAN component. Set the port path cost for the CEP
     * in that C-VLAN component. After that trigger Role Selection
     * SEM. */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {
        if (u4PortPathCost == AST_INIT_VAL)
        {
            RstSetZeroPortPathCost (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);
        }

        else
        {
            RstUpdatePortPathCost (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                   u4PortPathCost);
        }
        if (AST_IS_RST_ENABLED ())
        {
            i4RetVal =
                AstTriggerRoleSeletionMachine
                (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM, RST_DEFAULT_INSTANCE);
        }

        AstPbRestoreContext ();

        return i4RetVal;
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbSetPortAdminEdgeInCvlanComp                     */
/*                                                                           */
/* Description        : This function is used to configure the Admin Edge for*/
/*                      CEP in a CVLAN component.                            */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bStatus - Admin Edge status to be set.               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetPortAdminEdgeInCvlanComp (tAstPortEntry * pParentPortEntry,
                                  tAstBoolean bStatus)
{
    tAstPortEntry      *pPortEntry = NULL;

    /* Switch to C-VLAN component. Set the port Admin edge status for the CEP
     * in that C-VLAN component. After that trigger Role Selection SEM. */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {
        pPortEntry = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);
        pPortEntry->bAdminEdgePort = bStatus;

        if (AST_IS_RST_ENABLED ())
        {
            RstBrgDetectionMachine (RST_BRGDETSM_EV_ADMIN_SET,
                                    AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);
        }

        AstPbRestoreContext ();

        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetAdminPToP                                 */
/*                                                                           */
/* Description        : This function is used to configure the Admin Point To*/
/*                      Point in CVLAN component                             */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      u1AdminPToP -ForceTrue/ False/Auto                   */
/*                      bOperPToP - True/False                               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbCVlanSetAdminOperPtoP (UINT2 u2Port, UINT1 u1AdminPToP,
                            tAstBoolean bOperPToP)
{

    tAstPortEntry      *pPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2Port);

    if (AST_COMP_TYPE () != AST_PB_C_VLAN)
    {
        if (AstPbSelectCvlanContext (pPortInfo) == RST_SUCCESS)
        {
            pPortInfo = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

            pPortInfo->u1AdminPointToPoint = u1AdminPToP;

            pPortInfo->bOperPointToPoint = bOperPToP;

            AstPbRestoreContext ();

        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetAutoEdge                                  */
/*                                                                           */
/* Description        : This function is used to configure the Auto Edge     */
/*                      status in CVLAN component                            */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      bAutoEdge - True/False                               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstCVlanSetAutoEdge (UINT2 u2Port, tAstBoolean bAutoEdge)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RetVal = RST_FAILURE;

    pAstPortEntry = AST_GET_PORTENTRY (u2Port);

    if (AstPbSelectCvlanContext (pAstPortEntry) == RST_SUCCESS)
    {
        /*Auto Edge Status should be updated for CEP in CVLAN component */
        /* Now the get the port entry in the C-VLAN component. */
        pAstPortEntry = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);
        pAstPortEntry->bAutoEdge = bAutoEdge;

        i4RetVal = AstTrigAutoEdgeToBrgDetSem (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                               bAutoEdge);
        AstPbRestoreContext ();

        return i4RetVal;
    }

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : AstPbHandleCcompCepPortStatus                        */
/*                                                                           */
/* Description        : This function is used to handle the enable/disable   */
/*                      Port (PORT_OPER_UP/ PORT_OPER_DOWN/ STP_PORT_UP/     */
/*                      STP_PORT_DOWN) for CEP only                          */
/*                                                                           */
/* Input(s)           : u2Port - Local port id of the customer edge port.    */
/*                      u1TrigType - PORT_OPER_UP/PORT_OPER_DOWN/STP_PORT_UP */
/*                      STP_PORT_DOWN                                        */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstPbHandleCcompCepPortStatus (UINT2 u2Port, UINT1 u1TrigType)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT1               u1ContextModuleStatus = RST_ENABLED;

    /* Get the global module status. */
#ifdef MSTP_WANTED
    if ((AST_IS_MST_STARTED ()) &&
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] != MST_ENABLED))
    {
        u1ContextModuleStatus = RST_DISABLED;
    }
#endif
#ifdef PVRST_WANTED
    if ((AST_IS_PVRST_STARTED ()) &&
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] != PVRST_ENABLED))
    {
        u1ContextModuleStatus = RST_DISABLED;
    }
#endif
    if ((AST_IS_RST_STARTED ()) &&
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] != RST_ENABLED))
    {
        u1ContextModuleStatus = RST_DISABLED;
    }

    /* C-VLAN module cannot be enabled operationally when the global
     * module status is disabled. */
    if ((u1ContextModuleStatus == RST_DISABLED) &&
        (u1TrigType == AST_STP_PORT_UP))
    {
        return RST_SUCCESS;
    }

    pAstPortInfo = AST_GET_PORTENTRY (u2Port);

    if (AstPbSelectCvlanContext (pAstPortInfo) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    /*If there is no change in Module Status  just return success */
    if (((u1TrigType == AST_STP_PORT_UP) && (AST_MODULE_STATUS == RST_ENABLED))
        || ((u1TrigType == AST_STP_PORT_DOWN)
            && (AST_MODULE_STATUS == RST_DISABLED)))
    {
        AstPbRestoreContext ();
        return RST_SUCCESS;
    }

    /* Get CVLAN context CEP port Entry */
    pAstPortInfo = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

    if (pAstPortInfo == NULL)
    {
        AstPbRestoreContext ();
        return RST_FAILURE;
    }

    if (((pAstPortInfo->u1EntryStatus == AST_PORT_OPER_UP) &&
         (u1TrigType == AST_EXT_PORT_UP)) ||
        ((pAstPortInfo->u1EntryStatus == AST_PORT_OPER_DOWN)
         && (u1TrigType == AST_EXT_PORT_DOWN)))
    {
        /*NO CHANGE in the CEP PORT OPER STATUS */
        AstPbRestoreContext ();
        return RST_SUCCESS;
    }

    switch (u1TrigType)
    {
            /*case AST_EXT_PORT_UP: */
        case AST_PORT_OPER_UP:
        {
            /* Needs to call AstEnable Port here. AstEnable Port
             * will call RstEnable port, as well as initialize the
             * red times for that cep. */
            if (AstEnablePort (pAstPortInfo->u2PortNo, RST_DEFAULT_INSTANCE,
                               AST_EXT_PORT_UP) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG, " Enabling of PEP Failed for"
                              "port %s \n",
                              AST_GET_IFINDEX_STR (pAstPortInfo->u2PortNo));

                i4RetVal = RST_FAILURE;

            }

            break;
        }

            /*case AST_EXT_PORT_DOWN */
        case AST_PORT_OPER_DOWN:
        {
            if (RstDisablePort (pAstPortInfo->u2PortNo,
                                AST_EXT_PORT_DOWN) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG, " Disabling of PEP"
                              "Failed for port %s \n",
                              AST_GET_IFINDEX_STR (pAstPortInfo->u2PortNo));

                i4RetVal = RST_FAILURE;

            }

            break;
        }

        case AST_STP_PORT_UP:
        {
            if (RstComponentEnable () != RST_SUCCESS)
            {
                AST_DBG (AST_ALL_FAILURE_DBG, "RSTP Enable Failed");

                i4RetVal = RST_FAILURE;

            }
            break;
        }

        case AST_STP_PORT_DOWN:
        {
            if (RstComponentDisable () != RST_SUCCESS)
            {
                AST_DBG (AST_ALL_FAILURE_DBG, "RSTP Disable Failed");

                i4RetVal = RST_FAILURE;
            }
            break;
        }
        default:
            i4RetVal = RST_FAILURE;
            break;

    }

    AstPbRestoreContext ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanFillBrgMacAddr                             */
/*                                                                           */
/* Description        : This function is responsible for filling up the CVLAN*/
/*                      comp Bridge Mac address. The CVLAN comp Bridge Mac   */
/*                      will be same as the CEP Port Mac Address             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pAstBridgeMacAddr - Mac Address to be filled         */
/*                                                                           */
/* Return Value(s)    : None .                                               */
/*****************************************************************************/
VOID
AstPbCVlanFillBrgMacAddr (tAstMacAddr pAstBridgeMacAddr)
{
    tCfaIfInfo          CfaIfInfo;

    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    /* CVLAN component Bridge Mac address is CEP Port's Mac Address */
    if (AstCfaGetIfInfo (AST_CURR_CONTEXT_INFO ()->u4CepIfIndex, &CfaIfInfo) !=
        CFA_SUCCESS)
    {
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 " FATAL ERROR, CFA" "does not contain the CEP Port \r\n");

        return;
    }

    AST_MEMCPY (pAstBridgeMacAddr, CfaIfInfo.au1MacAddr, AST_MAC_ADDR_SIZE);

    return;
}

/*****************************************************************************/
/* Function Name      : RstUpdatePepOperPtoP                                 */
/*                                                                           */
/* Description        : This Function Updates the Oper PtoP status of PEP    */
/*                                                                           */
/* Input(s)           : u2PortNum - HL Port Id / Local Port Id of PEP        */
/*                      u1TrigType - AST_EXT_PORT_UP                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstUpdatePepOperPtoP (UINT2 u2PortNum, UINT1 u1TrigType)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    UINT1               u1ServiceType = AST_ELINE;

    pAstPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    if ((u1TrigType == AST_EXT_PORT_UP) || (u1TrigType == AST_UP))
    {
        if (AstL2IwfPbGetVlanServiceType (AST_PB_CVLAN_INFO ()->
                                          pParentContext->u4ContextId,
                                          pAstPortInfo->u2ProtocolPort,
                                          &u1ServiceType) != L2IWF_SUCCESS)
        {
            return RST_FAILURE;
        }

        if (u1ServiceType == AST_ELINE)
        {
            pAstPortInfo->bOperPointToPoint = RST_TRUE;
        }
        else
        {
            pAstPortInfo->bOperPointToPoint = RST_FALSE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanRemovePort2IndexMapBasedOnProtPort         */
/*                                                                           */
/* Description        : This Function removes the port to index mapping from */
/*                      the RBTree and takes care of counters used for       */
/*                       generating index.                                   */
/*                                                                           */
/* Input(s)           : u2PortNum - Protocol Port Number                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstPbCVlanRemovePort2IndexMapBasedOnProtPort (UINT2 u2ProtocolPort)
{

    tAstPort2IndexMap  *pPort2IndexEntry = NULL;
    tAstPort2IndexMap   TempPort2IndexEntry;
    UINT2               u2PortIndex = 0;

    AST_MEMSET (&TempPort2IndexEntry, AST_INIT_VAL, sizeof (tAstPort2IndexMap));

    TempPort2IndexEntry.u2ProtPort = u2ProtocolPort;

    pPort2IndexEntry = (tAstPort2IndexMap *)
        RBTreeRem (AST_PB_CVLAN_PORT2INDEX_TREE (),
                   (tRBElem *) & TempPort2IndexEntry);

    if (pPort2IndexEntry == NULL)
    {
        AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC, "No Entry Found in"
                      "RBTree for Protocol Port (%u, %u) \n",
                      (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex, u2ProtocolPort);

        return RST_FAILURE;
    }

    u2PortIndex = pPort2IndexEntry->u2PortIndex;

    if (AST_PB_RELEASE_PORT2INDEX_MAP_BLOCK (pPort2IndexEntry) ==
        AST_MEM_FAILURE)
    {
        AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG |
                      AST_ALL_FAILURE_DBG, "Release of Port2Index"
                      "Entry Block failed for Protocol port (%u, %u) \n",
                      (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex, u2ProtocolPort);

        return RST_FAILURE;
    }

    AST_PB_CVLAN_INFO ()->u2NumPorts--;

    /* If the Deleted Port is same as the High  Port Index, then decrease 
     * High port index of this cvlan context by 1. If it is not as same high
     * port index, then dont do anything*/
    if (u2PortIndex == AST_PB_CVLAN_INFO ()->u2HighPortIndex)
    {
        AST_PB_CVLAN_INFO ()->u2HighPortIndex--;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCvlanHdlSyncedChangeForPort                     */
/*                                                                           */
/* Description        : This function will be called whenever synced changes */
/*                      for a port. If synced becomes true for CEP, then:    */
/*                           - it means AllSynced is true for all PEPs.      */
/*                           - give AllSynced trigger to all PEPs.           */
/*                      If synced becomes true for a PEP, then :             */
/*                           - it checks synced variable for all other PEPs  */
/*                           - if synced is true for all PEPs, then AllSynced*/
/*                             trigger will be given to the corresponding    */
/*                             CEP.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port invoking this function.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS in case of success                       */
/*                      otherwise RST_FAILURE.                               */
/*****************************************************************************/
INT4
RstPbCvlanHdlSyncedChangeForPort (UINT2 u2Port)
{
    tAstBoolean         bAllSynced;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (AST_IS_CUSTOMER_EDGE_PORT (u2Port) == RST_TRUE)
    {
        /* This means AllSynced true for all PEPs.
         * Hence indicate this to all PEPs. */

        if (RstProleTrSmIndicateAllSyncedSet (u2Port) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: IndicateAllSyncedSet returned "
                          "FAILURE!\n", AST_GET_IFINDEX_STR (u2Port));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: IndicateAllSyncedSet returned "
                          "FAILURE!\n", AST_GET_IFINDEX_STR (u2Port));
            return RST_FAILURE;
        }
    }
    else
    {
        /* Synced has become true for this PEP. Now compute AllSynced for
         * the CEP. */
        bAllSynced =
            RstPbCvlanIsTreeAllSynced (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

        if (bAllSynced == RST_TRUE)
        {
            /* AllSynced is true for CEP. Indicate this to CEP, it's role
             * is root/alternate/backup. */
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         RST_DEFAULT_INSTANCE);

            if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
                (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
            {
                if (RstPortRoleTrMachine (RST_PROLETRSM_EV_ALLSYNCED_SET,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port Role Transition Machine "
                                  "returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR
                                  (pPerStPortInfo->u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port Role Transition Machine "
                                  "returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR
                                  (pPerStPortInfo->u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCvlanIsReRooted                                 */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmIsReRooted function, if the port invoking*/
/*                      RstProleTrSmIsReRooted function is a provider edge   */
/*                      port.                                                */
/*                      If rrwhile timer is zero for a CEP, then this        */
/*                      function will return success other wise failure.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstPbCvlanIsReRooted (VOID)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    /* If rrwhile is zero for customer edge port, then
     * rerooted is true for this PEP. */
    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO
        (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM, RST_DEFAULT_INSTANCE);

    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %s: RrWhileTmr Still Running...\n",
                      AST_GET_IFINDEX_STR (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCvlanIsTreeAllSynced                            */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstIsTreeAllSynced function, for C-VLAN component    */
/*                      ports.                                               */
/*                      If the port invoking this function is a CEP, then    */
/*                      it returns true if synced is set for all PEPs.       */
/*                      If the port invoking this function is a PEP, then    */
/*                      it returns true if synced is set for the CEP.        */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstIsTreeAllSynced function and         */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/ RST_FALSE.                                 */
/*****************************************************************************/
tAstBoolean
RstPbCvlanIsTreeAllSynced (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2Count = 0;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_PROVIDER_EDGE_PORT)
    {
        /* This is Provider edge port (PEP). */
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                     RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         RST_DEFAULT_INSTANCE);

        if (pRstPortInfo->bPortEnabled != RST_TRUE)
        {
            /* This condition is not possible. */
            return RST_FALSE;
        }

        if ((pRstPortInfo->bSelected != RST_TRUE) ||
            (pPerStPortInfo->u1SelectedPortRole
             != pPerStPortInfo->u1PortRole)
            || (pRstPortInfo->bUpdtInfo != RST_FALSE))
        {
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: IsTreeAllSynced: Port %s: RoleSelection/Upd"
                          "ation not complete",
                          AST_GET_IFINDEX_STR (u2PortNum));
            /* RoleSelection/Updation is not yet complete for all ports */
            return RST_FALSE;
        }

        /* The port for which the condition (allSynced) is being evaluated
         * is PEP. So allSynced value will be equal to the value of synced
         * for corresponding CEP. */
        if (pRstPortInfo->bSynced == RST_TRUE)
        {
            return RST_TRUE;
        }

        return RST_FALSE;
    }
    else
    {
        /* It is a Customer Edge Port(CEP). */

        u2Count = 1;
        AST_GET_NEXT_PORT_ENTRY (u2Count, pPortInfo)
        {
            /* The port for which the condition (allSynced) is being evaluated
             * is CEP. So allSynced shall be true if and only if the synced is 
             * true for all PEPs. (ref: section 13.38.4 of 802.1ad/D6) */
            if ((pPortInfo == NULL) ||
                (AST_GET_BRG_PORT_TYPE (pPortInfo) == AST_CUSTOMER_EDGE_PORT))
            {
                continue;
            }

            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

            pRstPortInfo =
                AST_GET_PERST_RST_PORT_INFO (u2Count, RST_DEFAULT_INSTANCE);

            if (pRstPortInfo->bPortEnabled != RST_TRUE)
            {
                continue;
            }

            if ((pRstPortInfo->bSelected != RST_TRUE) ||
                (pPerStPortInfo->u1SelectedPortRole !=
                 pPerStPortInfo->u1PortRole)
                || (pRstPortInfo->bUpdtInfo != RST_FALSE))
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: IsTreeAllSynced: Port %s: "
                              "RoleSelection/Updation not complete",
                              AST_GET_IFINDEX_STR (u2Count));
                /* RoleSelection/Updation is not yet complete for all ports */
                return RST_FALSE;
            }

            if ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ROOT) ||
                (pRstPortInfo->bSynced == RST_TRUE))
            {
                continue;
            }
            else
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: IsTreeAllSynced: Port %s: Synced NOT set",
                              AST_GET_IFINDEX_STR (u2Count));
                return RST_FALSE;
            }
        }
    }
    return RST_TRUE;

}

/*****************************************************************************/
/* Function Name      : AstPbCheckAllPortTypes                               */
/*                                                                           */
/* Description        : This function handles the port type changes during   */
/*                      spanning tree initialization.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : RstModuleInit or MstModuleInit                       */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbCheckAllPortTypes (VOID)
{
    UINT2               u2PortNum = 0;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        /* Check for the port type change and handle it. */
        if (AstCheckBrgPortTypeChange (u2PortNum) == RST_FAILURE)
        {
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCheckBrgPortTypeChange                            */
/*                                                                           */
/* Description        : This function get the port type for the given port   */
/*                      from L2IWF. If there is any change in the port type  */
/*                      then it call AstHandlePortTypeChange to handle the   */
/*                      port type.                                           */
/*                                                                           */
/* Input(s)           : u2Port - Local port id for the given port.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstCheckBrgPortTypeChange (UINT2 u2Port)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    UINT1               u1NewPbPortType = AST_PROVIDER_NETWORK_PORT;

    /* Will be applicable for the following components only.
     * 1. C-VLAN Component
     * 2. I-Component
     * 3. B-Component
     * */
    if ((AST_IS_1AD_BRIDGE () != RST_TRUE) &&
        (AST_IS_1AH_BRIDGE () != RST_TRUE))
    {
        return RST_SUCCESS;
    }

    pAstPortInfo = AST_GET_PORTENTRY (u2Port);

    if (pAstPortInfo != NULL)
    {
        /* Get the Pb Port type for the port from L2Iwf */
        if (AstGetPbPortType (AST_GET_IFINDEX (u2Port),
                              &u1NewPbPortType) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }

        if (AstHandlePortTypeChange (pAstPortInfo, u1NewPbPortType) !=
            RST_SUCCESS)
        {
            return RST_FAILURE;
        }
        AstDeriveMacAddrFromPortType (u2Port);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSelectCvlanContext                              */
/*                                                                           */
/* Description        : This Function is responsible for switching the context*/
/*                      from the SVLAN to the CVLAN. It also stores the SVLAN */
/*                      context in the gpAstParentCtxtInfo and then changes   */
/*                      the current context pointer to CVLAN context          */
/*                                                                           */
/* Input(s)           : pAstPortInfo- CEP Port Entry (index of CVLAN comp)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbSelectCvlanContext (tAstPortEntry * pPortEntry)
{
    if (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        if ((pPortEntry->pPbPortInfo != NULL)
            && (AST_PB_PORT_CVLAN_CTXT (pPortEntry) != NULL))
        {
            /* If Red C-VLAN context select fails, then return
             * failure. */
            if (AstRedPbSelectCVlanContext (pPortEntry->u2PortNo)
                == RST_FAILURE)
            {
                return RST_FAILURE;
            }

            /* Storing SVLAN context in Parent Ctxt */
            AST_PREV_CONTEXT_INFO () = AST_CURR_CONTEXT_INFO ();
            /*Storing CVLAN context as current Ctxt */
            AST_CURR_CONTEXT_INFO () = AST_PB_PORT_CVLAN_CTXT (pPortEntry);
            return RST_SUCCESS;
        }
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbSetPseudoRootIdInCvlanComp                      */
/*                                                                           */
/* Description        : This function is used to configure PseudoRootId  for */
/*                     CEP in a CVLAN component                              */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      BrgMacAddr - PseudoRoot MacAddress                   */
/*                      u2BrgPriority - PseudoRoot Priority                  */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetPseudoRootIdInCvlanComp (tAstPortEntry * pParentPortEntry,
                                 tAstMacAddr BrgMacAddr, UINT2 u2BrgPriority)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    /* Switch to C-VLAN component. Set the PseudoRootId for the CEP
     * in that C-VLAN component. After that trigger PseudoInfo 
     * SEM. */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {

        pAstPerStPortInfo = AST_GET_PERST_PORT_INFO
            (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM, RST_DEFAULT_INSTANCE);

        pAstPerStPortInfo->PseudoRootId.u2BrgPriority = u2BrgPriority;

        AST_MEMCPY (pAstPerStPortInfo->PseudoRootId.BridgeAddr,
                    BrgMacAddr, AST_MAC_ADDR_SIZE);

        if (AST_IS_RST_ENABLED ())
        {
            /* 
             * Trigger Pseudo Info State Machine
             */
            if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_BEGIN,
                                      AST_PB_CEP_CVLAN_LOCAL_PORT_NUM, NULL)
                != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RstPseudoInfoMachine returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RstPseudoInfoMachine returned FAILURE!\n");

                AstPbRestoreContext ();
                return RST_FAILURE;
            }

        }

        AstPbRestoreContext ();

        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbSetL2gpInCvlanComp                              */
/*                                                                           */
/* Description        : This function is used to configure L2gp for CEP in a */
/*                      CVLAN component                                      */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bIsL2Gp - L2gp Status of Port                        */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetL2gpInCvlanComp (tAstPortEntry * pParentPortEntry, tAstBoolean bIsL2gp)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT1               u1Event;

    /* Switch to C-VLAN component. Set the L2gp for the CEP
     * in that C-VLAN component. After that trigger PseudoInfo 
     * SEM. */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {
        pPortEntry = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

        AST_PORT_IS_L2GP (pPortEntry) = bIsL2gp;

        if (bIsL2gp == RST_TRUE)
        {
            u1Event = RST_PSEUDO_INFO_EV_L2GP_ENABLED;
        }
        else
        {
            u1Event = RST_PSEUDO_INFO_EV_L2GP_DISABLED;
        }

        if (AST_IS_RST_ENABLED ())
        {
            /* 
             * Trigger Pseudo Info State Machine
             */
            if (RstPseudoInfoMachine (u1Event, AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                      NULL) != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RstPseudoInfoMachine returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RstPseudoInfoMachine returned FAILURE!\n");

                AstPbRestoreContext ();
                return RST_FAILURE;
            }

        }

        AstPbRestoreContext ();

        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbSetBpduRxInCvlanComp                            */
/*                                                                           */
/* Description        : This function is used to configure BPDU receive      */
/*                      status for CEP in a CVLAN component                  */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bEnableBPDURx - BPDU Receive Status of Port          */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetBpduRxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDURx)
{
    tAstPortEntry      *pPortEntry = NULL;

    /* Switch to C-VLAN component. Set the BPDURx for the CEP
     * in that C-VLAN component.                               
     * */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {
        pPortEntry = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

        AST_PORT_ENABLE_BPDU_RX (pPortEntry) = bEnableBPDURx;

        AstPbRestoreContext ();

        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbSetBpduTxInCvlanComp                            */
/*                                                                           */
/* Description        : This function is used to configure BPDU transmit     */
/*                      status for CEP in a CVLAN component                  */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bEnableBPDUTx - BPDU Receive Status of Port          */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetBpduTxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDUTx)
{
    tAstPortEntry      *pPortEntry = NULL;

    /* Switch to C-VLAN component. Set the BPDUTx for the CEP
     * in that C-VLAN component.                               
     * */
    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_SUCCESS)
    {
        pPortEntry = AST_GET_PORTENTRY (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM);

        AST_PORT_ENABLE_BPDU_TX (pPortEntry) = bEnableBPDUTx;

        /* Stop HellWhenTimer, if Bpdu transmission is disable on port and,
         * Start the Timer, if Bpdu transmission is enabled on port.
         */

        /* 
         * Trigger RTSM for change in enableBPDUTx status.
         */
        if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ()))
        {
            if (bEnableBPDUTx == RST_TRUE)
            {
                if (RstPortTransmitMachine (RST_PTXSM_EV_TX_ENABLED, pPortEntry,
                                            RST_DEFAULT_INSTANCE) !=
                    RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "SYS: C-Vlan Comp: Port Transmit Machine Returned failure!\n");
                    AST_DBG (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                             "SYS: C-Vlan Comp: Port Transmit Machine Returned failure!\n");
                    return RST_FAILURE;

                }
            }
            else
            {
                if (RstPortTransmitMachine
                    (RST_PTXSM_EV_TX_DISABLED, pPortEntry,
                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "SYS: C-Vlan Comp: Port Transmit Machine Returned failure!\n");
                    AST_DBG (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                             "SYS: C-Vlan Comp: Port Transmit Machine Returned failure!\n");
                    return RST_FAILURE;

                }
            }
        }

        AstPbRestoreContext ();

        return RST_SUCCESS;
    }

    return RST_FAILURE;
}
