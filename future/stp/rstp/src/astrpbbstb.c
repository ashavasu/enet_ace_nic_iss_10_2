/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * Description: This file contains the stub functions used by the
 *              RSTP Module to realise the PBB Functionality. This
 *              will be used when PBB is not defined.
 *
 *******************************************************************/

#include "asthdrs.h"
#ifdef MSTP_WANTED
#include "astmtrap.h"
#endif

/*****************************************************************************/
/* Function Name      : RstPbbIsTreeAllSynced                                */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstIsTreeAllSynced function, for I-component ports.  */
/*                      If the port invoking this function is a CNP, then    */
/*                      it returns true if synced is set for all other ports */
/*                      present in this component.                           */
/*                      If the port invoking this function is a VIP, then    */
/*                      it returns true if synced is set for all the CNPs.   */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstIsTreeAllSynced function and         */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/ RST_FALSE.                                 */
/*****************************************************************************/
tAstBoolean
RstPbbIsTreeAllSynced (UINT2 u2PortNum)
{
    UNUSED_PARAM (u2PortNum);
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : RstPbbCvlanIsReRooted                                */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmIsReRooted function, if the port invoking*/
/*                      is a virtual instance port.                          */
/*                      If rrwhile timer is zero for all the CNPs in this    */
/*                      component, then this function will return success    */
/*                      other wise failure.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstPbbIsReRooted (UINT2 u2PortNum)
{
    UNUSED_PARAM (u2PortNum);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRestoreVipDefaults                                */
/*                                                                           */
/* Description        : This function will be called by during port          */
/*                      initialisation,if the port created is a VIP. This    */
/*                      function restores the defaults as mentioned by IEEE  */
/*                      802.1ah Draft 4.2 Section 13.40.                     */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
AstRestoreVipDefaults (UINT2 u2PortNum)
{
    UNUSED_PARAM (u2PortNum);
    return RST_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name             : AstMiRstNpHwServiceInstancePtToPtStatus    */
/*                                                                           */
/*    Description               : This API is used to program the hardware   */
/*                                regarding the P2P status of a VIP.         */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Context Id                   */
/*                                u4VipIndex  - VIP IfIndex                  */
/*                                u4Isid      - ISID value for this VIP.     */
/*                                u1OperPointToPoint - point to point status */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS or RST_FAILURE.                */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstNpHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                         UINT4 u4VipIndex, UINT4 u4Isid,
                                         UINT1 u1PointToPointStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1PointToPointStatus);

    return RST_SUCCESS;
}

#endif
