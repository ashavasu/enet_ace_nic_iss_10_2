/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrsnmp.c,v 1.71 2017/11/23 14:15:12 siva Exp $
 *
 * Description: This file contains interface functions between the 
 *              SNMP low-level functions and the RSTP Module.
 *
 *******************************************************************/

#include "asthdrs.h"

#ifdef NPAPI_WANTED
extern INT4         gi4AstInitProcess;
#endif

/*****************************************************************************/
/* Function Name      : RstSetBridgePriority                                 */
/*                                                                           */
/* Description        : This function is the event handler whenever Bridge   */
/*                      Priority value is set through management and the     */
/*                      event is generated.                                  */
/*                                                                           */
/* Input(s)           : u2BrgPriority - The priority value to be set for the */
/*                                      Bridge                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetBridgePriority (UINT2 u2BrgPriority)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    if (pAstPerStBridgeInfo->u2BrgPriority == u2BrgPriority)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Bridge Priority\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Bridge Priority\n");
        return RST_SUCCESS;
    }
    pAstPerStBridgeInfo->u2BrgPriority = u2BrgPriority;

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Bridge Priority Set as :%u\n", u2BrgPriority);
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Bridge Priority Set as :%u\n", u2BrgPriority);

    AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
             "SNMP: ...Recomputing roles..\n");
    AST_DBG (AST_MGMT_DBG, "SNMP: ...Recomputing roles..\n");
    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);
        if (pPerStPortInfo != NULL)
        {
            pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
            pPerStRstPortInfo->bSelected = RST_FALSE;
            MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pPerStRstPortInfo->bReSelect = RST_TRUE;
            /* Do not overwrite configured PseudoRootId Priority. */
            if (pPerStPortInfo->bIsPseudoRootIdConfigured == RST_FALSE)
            {
                pPerStPortInfo->PseudoRootId.u2BrgPriority = u2BrgPriority;
            }

        }                        /* if (pPerStPortInfo != NULL) */
    }                            /* End for (u2PortNum = 1; ...) */

    if (AST_IS_RST_ENABLED ())
    {
        if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                         (UINT2) RST_DEFAULT_INSTANCE)
            != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RoleSelectionMachine function returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RoleSelectionMachine function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetDynamicPathcostCalc                            */
/*                                                                           */
/* Description        : This function is the event handler whenever dynamic  */
/*                      pathcost calculation is set or reset by management   */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u1DynamicPathcostCalc - RST_FALSE/RST_TRUE           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetDynamicPathcostCalc (UINT1 u1DynamicPathcostCalc)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT4               u4PathCost = 0;
    UINT1               u1ChangeFlag = RST_FALSE;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == u1DynamicPathcostCalc)
    {
        return (INT1) RST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalculation = u1DynamicPathcostCalc;

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == RST_TRUE)
    {
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if ((pPortEntry) == NULL)
            {
                continue;
            }

            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            u4PathCost = pPortEntry->u4PathCost;

            if (AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
                == RST_FALSE)
            {
                pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
            }

            if (u4PathCost != pPortEntry->u4PathCost)
            {
                u1ChangeFlag = RST_TRUE;
                pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
                MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);
                pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;
            }
        }
        if (AST_IS_RST_ENABLED ())
        {
            if (u1ChangeFlag == RST_TRUE)
            {

                if (RstPortRoleSelectionMachine
                    ((UINT1) RST_PROLESELSM_EV_RESELECT,
                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                    return RST_FAILURE;
                }
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetDynaPathcostCalcLagg                           */
/*                                                                           */
/* Description        : This function is the event handler whenever dynamic  */
/*                      pathcost calculation on speed change is set or reset */
/*                      by management and the corresponding event is         */
/*                      generated. It will take care to recalculate          */
/*                      pathcost for Link Aggregated Port. In case pathcost  */
/*                      calculated is different from the existing value then */
/*                      Spnnaing Tree to which lagg belong will be triggered */
/*                      to re-convergede                                     */
/*                                                                           */
/* Input(s)           : u1DynamicPathcostCalcLagg - RST_FALSE/RST_TRUE       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetDynaPathcostCalcLagg (UINT1 u1DynamicPathcostCalcLagg)
{
    tAstCfaIfInfo       CfaIfInfo;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4PathCost = 0;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1ChangeFlag = RST_FALSE;

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalcLagg == u1DynamicPathcostCalcLagg)
    {
        return (INT1) RST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalcLagg = u1DynamicPathcostCalcLagg;
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if ((pPortEntry) == NULL)
        {
            continue;
        }

        if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pPortEntry),
                             &CfaIfInfo) == CFA_FAILURE)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                          AST_MGMT_TRC,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }

        if (CfaIfInfo.u1IfType != CFA_LAGG)
        {
            continue;
        }

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        u4PathCost = pPortEntry->u4PathCost;

        if (AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
            == RST_FALSE)
        {
            pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
            if (u4PathCost != pPortEntry->u4PathCost)
            {
                u1ChangeFlag = RST_TRUE;
                pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
                MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);
                pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;
            }
        }
    }
    if (AST_IS_RST_ENABLED ())
    {
        if (u1ChangeFlag == RST_TRUE)
        {

            if (RstPortRoleSelectionMachine
                ((UINT1) RST_PROLESELSM_EV_RESELECT,
                 RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetForceVersion                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Stp  */
/*                      Force Version is set through management and the      */
/*                      corresponding event is generated.                    */
/*                                                                           */
/* Input(s)           : u1ForceVersion - Version of Spanning Tree Protocol   */
/*                                       the Bridge should run               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetForceVersion (UINT1 u1ForceVersion)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#ifdef MSTP_WANTED
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
#endif

    if (AST_FORCE_VERSION == u1ForceVersion)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Force Version\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Force Version\n");
        return RST_SUCCESS;
    }

    AST_FORCE_VERSION = u1ForceVersion;
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Force Version Set as :%u\n", u1ForceVersion);
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Force Version Set as :%u\n", u1ForceVersion);

#ifdef MSTP_WANTED
    pAstBridgeEntry = AST_GET_BRGENTRY ();
#endif

    if (AST_IS_RST_ENABLED ())
    {
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if ((pPortEntry) == NULL)
            {
                continue;
            }

            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

            if (pCommPortInfo->pRapidAgeDurtnTmr != NULL)
            {
                AstVlanResetShortAgeoutTime (pPortEntry);

                if (AstStopTimer (pPortEntry, AST_TMR_TYPE_RAPIDAGE_DURATION)
                    != RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Stop Timer for RapidAgeDuration timer Failed !!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                }
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      RST_DEFAULT_INSTANCE);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
                == RST_FALSE)
            {
                pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
            }
        }                        /* End of For Loop */

        /* gi4AstInitProcess is used to disable Hw flush while each port is
         * being initialised, and then flush out entries learnt on all ports
         * at once */
#ifdef NPAPI_WANTED
        gi4AstInitProcess = 1;
#endif

        if (AstAssertBegin () != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC, "SNMP: Asserting BEGIN failed!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: Asserting BEGIN failed!\n");

#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif
            return RST_FAILURE;
        }

#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            AstMiNpDeleteAllFdbEntries ();

        }
        gi4AstInitProcess = 0;
#endif
    }

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            /* If this bridge is the root bridge: update Root Times
             * with the Bridge's helloTime */
            if (u1ForceVersion != AST_VERSION_3)
            {
                pAstBridgeEntry->RootTimes.u2HelloTime =
                    pAstBridgeEntry->BridgeTimes.u2HelloTime;
            }
        }

        /* Updating the Desg Times and Port Times for all ports */
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if (pPortEntry != NULL)
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                          MST_CIST_CONTEXT);
                if (u1ForceVersion == AST_VERSION_3)
                {
                    pPortEntry->DesgTimes.u2HelloTime = pPortEntry->u2HelloTime;

                    /* Update the port times only if the port
                     * is a designated port */
                    if (pPerStPortInfo->PerStRstPortInfo.u1InfoIs ==
                        MST_INFOIS_MINE)
                    {
                        pPortEntry->PortTimes.u2HelloTime =
                            pPortEntry->u2HelloTime;
                    }
                }
                else
                {
                    pPortEntry->DesgTimes.u2HelloTime =
                        pAstBridgeEntry->BridgeTimes.u2HelloTime;

                    /* Update the port times only if the port
                     * is a designated port */
                    if (pPerStPortInfo->PerStRstPortInfo.u1InfoIs ==
                        MST_INFOIS_MINE)
                    {
                        pPortEntry->PortTimes.u2HelloTime =
                            pAstBridgeEntry->BridgeTimes.u2HelloTime;
                    }
                }
            }
        }

        /* Force Version can take values of 'stpCompatible (0) and 'rstp' (2)
         * for RSTP protocol and an additional value of 'mstp' (3) for MSTP
         * protocol. Whenever the value is changed through management action,
         * the value gets updated in the global structure variable. The
         * spanning tree protocol entity will be reinitialised if the Force
         * Version parameter is modified.*/
#ifdef NPAPI_WANTED
        gi4AstInitProcess = 1;
#endif

        if (AstAssertBegin () != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC, "SNMP: Asserting BEGIN failed!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: Asserting BEGIN failed!\n");

#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif
            return RST_FAILURE;
        }

#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            AstMiNpDeleteAllFdbEntries ();

        }
        gi4AstInitProcess = 0;
#endif

    }
#endif

#ifdef PVRST_WANTED

    if (AST_IS_PVRST_ENABLED ())
    {
#ifdef NPAPI_WANTED
        gi4AstInitProcess = 1;
#endif
        if (PvrstAssertBegin () != PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC, "SNMP: Asserting BEGIN failed!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: Asserting BEGIN failed!\n");

#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif
            return RST_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            AstMiNpDeleteAllFdbEntries ();

        }
        gi4AstInitProcess = 0;
#endif
    }
#endif

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortPriority                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      Priority value is set through management and the     */
/*                      corresponding event is generated.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port for which the*/
/*                                  priority is to be set                    */
/*                      u1PortPriority - The priority value to be set        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetPortPriority (UINT2 u2PortNum, UINT1 u1PortPriority)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo->u1PortPriority == u1PortPriority)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port Priority\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port Priority\n");
        return RST_SUCCESS;
    }
    pPerStPortInfo->u1PortPriority = u1PortPriority;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Priority Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u1PortPriority);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Priority Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u1PortPriority);

    if (AST_IS_RST_ENABLED ())
    {
        pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
        pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;

        if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                         RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "SNMP: RoleSelectionMachine function returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RoleSelectionMachine function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortPathCost                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      Path Cost value is set through management and the    */
/*                      corresponding event is generated.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port for which the*/
/*                                  path cost is to be set                   */
/*                      u4PortPathCost - The path cost value to be set       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetPortPathCost (UINT2 u2PortNum, UINT4 u4PortPathCost)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE) == NULL)
    {
        return RST_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    if (pAstPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    if ((pAstPortEntry->u4PathCost == u4PortPathCost) &&
        ((pAstPerStPortInfo->u4PortAdminPathCost == u4PortPathCost) ||
         (pAstPerStPortInfo->u4PortAdminPathCost == 0)))

    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port PathCost\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port PathCost\n");
        return RST_SUCCESS;
    }
    /* Now update the path cost in the port entry. */
    RstUpdatePortPathCost (u2PortNum, u4PortPathCost);

    /* Incase of customer edge port, update the port path cost
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        i4RetVal = AstPbSetPortPathCostInCvlanComp (pAstPortEntry,
                                                    u4PortPathCost);

        return i4RetVal;
    }

    if (AST_IS_RST_ENABLED ())
    {
        i4RetVal = AstTriggerRoleSeletionMachine (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);
    }

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : RstSetZeroPortPathCost                               */
/*                                                                           */
/* Description        : This function is to set the AdminPortPathCost to Zero*/
/*                      and to calculate the pathcost dynamically            */
/*                                                                           */
/* Input(s)           :  U2PortNum- The port fpr which the cost has to be set*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstSetZeroPortPathCost (UINT2 u2PortNum)
{
    INT1                i1RetVal = RST_SUCCESS;

    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT4               u4PathCost = AST_INIT_VAL;
    UINT1               u1ChangeFlag = RST_FALSE;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    u4PathCost = AstCalculatePathcost (u2PortNum);

    AstSetPortPathcostConfigured (u2PortNum, RST_DEFAULT_INSTANCE, 0);

    if (u4PathCost != pPortEntry->u4PathCost)
    {
        pPortEntry->u4PathCost = u4PathCost;
        u1ChangeFlag = RST_TRUE;
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);
        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
        pPerStRstPortInfo->bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
        pPerStRstPortInfo->bReSelect = RST_TRUE;
    }
    if (AST_IS_RST_ENABLED ())
    {
        if (u1ChangeFlag == RST_TRUE)
        {
            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             (UINT2) RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                return (INT1) SNMP_FAILURE;
            }
        }
    }
    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AstSetPortPathcostConfigured                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Configured Admin Path Cost  */
/*                        parameter                                          */
/*                                                                           */
/*     INPUT            : u4PortNum - PortNumber                             */
/*                        u4Val - Admin Path Cost                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : RST_SUCCESS/RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstSetPortPathcostConfigured (UINT2 u2PortNum, UINT2 u2InstanceId, UINT4 u4Val)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pAstPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pAstPerStPortInfo->u4PortAdminPathCost = u4Val;
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstUpdatePortPathCost                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port path cost in port      */
/*                        entry and also sets the port path cost configured  */
/*                        as true.                                           */
/*                                                                           */
/*     INPUT            : u2PortNum - Port identifer.                        */
/*                        u4PortPathCost - Path cost to be set.              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : RST_SUCCESS/RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstUpdatePortPathCost (UINT2 u2PortNum, UINT4 u4PortPathCost)
{

    tAstPortEntry      *pPortEntry = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    pPortEntry->u4PathCost = u4PortPathCost;
    i4RetVal = AstSetPortPathcostConfigured (u2PortNum, RST_DEFAULT_INSTANCE,
                                             u4PortPathCost);

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %u: Path Cost Set as :%u...Recomputing roles..\n",
                  u2PortNum, u4PortPathCost);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %u: Path Cost Set as :%u...Recomputing roles..\n",
                  u2PortNum, u4PortPathCost);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstSetProtocolMigration                              */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      Protocol Migration value is set through management   */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port for which the*/
/*                                  protocol migration is to be set          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetProtocolMigration (UINT2 u2PortNum)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: BPDU Migration Check Forced...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Port %s: BPDU Migration Check Forced...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
    {
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

        pAstCommPortInfo->bMCheck = RST_TRUE;
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);

        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PMSM: Port %s: MCheck = TRUE",
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP: Port %s: Forcing BPDU Migration Check to Rstp Mode\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        if (RstPortMigrationMachine ((UINT1) RST_PMIGSM_EV_MCHECK,
                                     pPerStPortInfo->u2PortNo) != RST_SUCCESS)
        {

            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RstPortMigrationMachine function returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RstPortMigrationMachine function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetAdminEdgePort                                  */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      AdminEdgePort value is set through management        */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which is to  */
/*                                  be made as an Edge Port                  */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should function as an Edge Port or not     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetAdminEdgePort (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT1               aau1EdgePortStatus[2][10] = {
        "FALSE", "TRUE"
    };

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry->bAdminEdgePort == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in AdminEdge Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in AdminEdge Status\n");
        return RST_SUCCESS;
    }
    /* Incase of customer edge port, update the port Admin edge status
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        i4RetVal = AstPbSetPortAdminEdgeInCvlanComp (pAstPortEntry, bStatus);
        return i4RetVal;
    }

    pAstPortEntry->bAdminEdgePort = bStatus;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set Edge Port Status as :%s...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), aau1EdgePortStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Set Edge Port Status as :%s...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), aau1EdgePortStatus[bStatus]);

    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ()
        || AST_IS_PVRST_ENABLED ())
    {

        if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
        {
            RstBrgDetectionMachine (RST_BRGDETSM_EV_ADMIN_SET, u2PortNum);
        }
#ifdef PVRST_WANTED
        if (AST_IS_PVRST_ENABLED ())
        {
            pAstPortEntry->bOperEdgePort = bStatus;
            PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_ADMIN_SET, u2PortNum);
        }
#endif
    }

    /* Flush the port if the port admin status is down */
    if ((pAstPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN) &&
        (bStatus == RST_FALSE))
    {
        if (AST_IS_RST_ENABLED ())
        {
            RstTopoChSmFlushFdb (u2PortNum, VLAN_NO_OPTIMIZE);
        }
        else if (AST_IS_MST_ENABLED ())
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

            if (pPerStPortInfo == NULL)
            {
                return RST_FAILURE;
            }

#ifdef MSTP_WANTED
            MstTopologyChSmFlushPort (pPerStPortInfo);
#endif
        }
    }

#ifdef MSTP_WANTED
    if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
    {
        MstSispCopyPortPropToSispLogical (AST_GET_IFINDEX (u2PortNum),
                                          MST_ADMIN_EDGE, bStatus);
    }
#endif
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetAdminPointToPoint                              */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      AdminPointToPoint value is set through management    */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which is to  */
/*                                  be made as an Edge Port                  */
/*                      u1AdminPToP- The point-to-point status of the LAN    */
/*                                   segment attached to this Port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstSetAdminPointToPoint (UINT2 u2PortNum, UINT1 u1AdminPToP)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCfaIfInfo       CfaIfInfo;
#ifdef NPAPI_WANTED
    UINT4               u4Isid = AST_INIT_VAL;
#endif
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBoolean         bPrevOperStatus = AST_INIT_VAL;
#if defined MSTP_WANTED || PVRST_WANTED
    UINT2               u2Inst = AST_INIT_VAL;
#endif
#ifdef PVRST_WANTED
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
#endif
#ifdef MSTP_WANTED
    tAstPerStInfo      *pPerStInfo = NULL;
#endif
    UINT1               aau1AdminPToPStatus[3][15] = {
        "FORCE_TRUE", "FORCE_FALSE", "AUTO"
    };

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE)
    {
        /* 
         * The configured value of Admin P2P and calculated Oper P2P in
         * SVLAN context are updated in the CVLAN context in this function.
         */
        if (u1AdminPToP == (UINT1) RST_P2P_FORCETRUE)
        {
            AstPbCVlanSetAdminOperPtoP (u2PortNum, u1AdminPToP, RST_TRUE);
        }
        else
        {
            if ((u1AdminPToP == (UINT1) RST_P2P_FORCEFALSE) ||
                (u1AdminPToP == (UINT1) RST_P2P_AUTO))
            {
                AstPbCVlanSetAdminOperPtoP (u2PortNum, u1AdminPToP, RST_FALSE);
            }
        }
        return RST_SUCCESS;
    }
    pAstPortEntry->u1AdminPointToPoint = u1AdminPToP;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set Admin PToP Port Status as :%s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AdminPToPStatus[u1AdminPToP]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Set Admin PToP Port Status as :%s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AdminPToPStatus[u1AdminPToP]);

    /* In case of provider edge port, oper p2p status depends on the service
     * type of corresponding s-vlan. So don't do any thing here. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_PROVIDER_EDGE_PORT)
    {
        return RST_SUCCESS;
    }

    bPrevOperStatus = pAstPortEntry->bOperPointToPoint;
    if (u1AdminPToP == (UINT1) RST_P2P_FORCETRUE)
    {
        pAstPortEntry->bOperPointToPoint = RST_TRUE;
    }
    else if (u1AdminPToP == (UINT1) RST_P2P_FORCEFALSE)
    {
        pAstPortEntry->bOperPointToPoint = RST_FALSE;
    }
    else                        /* u1AdminPToP is set as AUTO */
    {
        pAstPortEntry->bOperPointToPoint = RST_FALSE;

        /* For logical VIPs, if the value of AdminP2P is AUTO, then the value
         * of OperP2P should be maintained as False and should not be 
         * calculated based on the value of duplexity
         * */
        if (AST_IS_VIRTUAL_INST_PORT (u2PortNum) == RST_FALSE)
        {
            if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pAstPortEntry),
                                 &CfaIfInfo) == CFA_FAILURE)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                              AST_MGMT_TRC,
                              "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                              "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return RST_FAILURE;
            }

            if (CfaIfInfo.u1IfType == CFA_LAGG)
            {
                pAstPortEntry->bOperPointToPoint = RST_TRUE;
                AST_DBG (AST_MGMT_DBG,
                         "SNMP: INTERFACE TYPE Indicated as PortChannel \n");
            }
            else
            {
                /* This function checks for Link Duplexity from CFA, CFA 
                 * internally checks if auto negotiation is enabled on the 
                 * interface or not and returns the corresponding duplexity
                 * either as a result of auto negotiation or the Configured 
                 * value.
                 */
#ifndef NPAPI_WANTED
                if (AST_IS_PROVIDER_EDGE_BRIDGE () == RST_TRUE)
                {
                    CfaIfInfo.u1DuplexStatus = AST_LINK_FULL_DUPLEX;
                }
#endif
                if (CfaIfInfo.u1DuplexStatus == AST_LINK_FULL_DUPLEX)
                {
                    pAstPortEntry->bOperPointToPoint = RST_TRUE;
                }

            }                    /* PHYSICAL PORT */

        }                        /* NON VIP PORT */

    }                            /* AdminP2P is Auto */

    /* Change in value of P2P. Update the same */
    AstL2IwfSetPortOperPointToPointStatus (AST_CURR_CONTEXT_ID (),
                                           u2PortNum,
                                           (BOOL1) pAstPortEntry->
                                           bOperPointToPoint);

#ifdef NPAPI_WANTED
    if (AST_IS_VIRTUAL_INST_PORT (u2PortNum) == RST_TRUE)
    {
        if (AstL2IwfGetIsid (pAstPortEntry->u4IfIndex, &u4Isid) ==
            L2IWF_FAILURE)
        {
            return RST_FAILURE;
        }

        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            if (AstMiRstNpHwServiceInstancePtToPtStatus (AST_CURR_CONTEXT_ID (),
                                                         pAstPortEntry->
                                                         u4IfIndex,
                                                         u4Isid,
                                                         (UINT1) pAstPortEntry->
                                                         bOperPointToPoint)
                == RST_FAILURE)
            {
                return RST_FAILURE;
            }
        }
    }
#endif
    /* If bOperPointToPoint is TRUE(link is p2p), no need to do state transition.
     * loopguard should be applicable only for non-designated port */
    if ((pAstPortEntry->bLoopGuard == RST_TRUE) &&
        (bPrevOperStatus != pAstPortEntry->bOperPointToPoint))
    {
        if (AST_IS_RST_ENABLED ())
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      RST_DEFAULT_INSTANCE);
            if (pPerStPortInfo == NULL)
            {
                return RST_FAILURE;
            }
            if ((pAstPortEntry->bLoopInconsistent == RST_TRUE) &&
                (pAstPortEntry->bOperPointToPoint == RST_FALSE))
            {
                if (RstPortRoleTrMachine (RST_PROLETRSM_EV_FDWHILE_EXP,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                if ((pPerStPortInfo->bLoopIncStatus == RST_TRUE) &&
                    (pAstPortEntry->bOperPointToPoint == RST_FALSE))
                {
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_FDWHILE_EXPIRED, pPerStPortInfo,
                         u2Inst) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                    pPerStPortInfo->bLoopIncStatus = RST_FALSE;
                }
            }
        }
#endif
#ifdef PVRST_WANTED
        else                    /* PVRST enabled */
        {
            for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                    OSIX_FALSE)
                {
                    continue;
                }
                u2Inst = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                       VlanId);
                if (u2Inst == INVALID_INDEX)
                {
                    return RST_FAILURE;
                }
                pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                if ((pPerStPortInfo->bLoopIncStatus == RST_TRUE) &&
                    (pAstPortEntry->bOperPointToPoint == RST_FALSE))
                {
                    if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_FDWHILE_EXP,
                                                pPerStPortInfo) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "SNMP: Port %s: Port Role Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                    pPerStPortInfo->bLoopIncStatus = RST_FALSE;
                }
            }
        }
#endif
        pAstPortEntry->bLoopInconsistent = RST_FALSE;
    }
    if (pAstPortEntry->bOperPointToPoint == RST_TRUE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                      "SNMP: Port %s: Set Oper PToP Port Status as TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP: Port %s: Set Oper PToP Port Status as TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    else
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                      "SNMP: Port %s: Set Oper PToP Port Status as FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP: Port %s: Set Oper PToP Port Status as FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    pAstPortEntry->bIEEE8021apAdminP2P = RST_TRUE;
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetMaxAge                                         */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Maximum Age for the Bridge information is set through*/
/*                      management and the corresponding event is generated. */
/*                                                                           */
/* Input(s)           : u2InstanceId - The Id of the Spanning Tree Instance  */
/*                      u2MaxAge - The maximum age value to be set           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstSetMaxAge (UINT2 u2InstanceId, UINT2 u2MaxAge)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pBrgInfo = AST_GET_BRGENTRY ();
    if (pBrgInfo->BridgeTimes.u2MaxAge == u2MaxAge)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Maxage value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in MaxAge value\n");
        return;
    }
    pBrgInfo->BridgeTimes.u2MaxAge = u2MaxAge;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Instance %u: Set Max Age as :%u...\n",
                  u2InstanceId, u2MaxAge);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Instance %u: Set Max Age as :%u...\n",
                  u2InstanceId, u2MaxAge);

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
    {
        if ((!(AST_IS_RST_ENABLED ())) && (!(AST_IS_MST_ENABLED ())))
        {
            pBrgInfo->RootTimes.u2MaxAge = pBrgInfo->BridgeTimes.u2MaxAge;
        }
    }

    /* Updating the Root Times, Desg Times and Port Times if this Bridge 
     * is the Root Bridge */
    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ()
        || AST_IS_PVRST_ENABLED ())
    {
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            /* This Bridge is the Root Bridge */
            pBrgInfo->RootTimes.u2MaxAge = pBrgInfo->BridgeTimes.u2MaxAge;

            u2PortNum = 1;
            AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
            {
                if ((pPortEntry) == NULL)
                {
                    continue;
                }
                pPortEntry->DesgTimes.u2MaxAge =
                    pPortEntry->PortTimes.u2MaxAge =
                    pBrgInfo->RootTimes.u2MaxAge;
            }                    /* End of For Loop */
        }                        /* End of this Bridge being the Root Bridge */
    }
}

/*****************************************************************************/
/* Function Name      : AstSetHelloTime                                      */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Hello Time interval is set through management and    */
/*                      the corresponding event is generated.                */
/*                                                                           */
/* Input(s)           : u2InstanceId - The Id of the Spanning Tree Instance  */
/*                      u2HelloTime - The hello time value to be set         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstSetHelloTime (UINT2 u2InstanceId, UINT2 u2HelloTime)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pBrgInfo = AST_GET_BRGENTRY ();
    if (pBrgInfo->BridgeTimes.u2HelloTime == u2HelloTime)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in HelloTime value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in HelloTime value\n");
        return;
    }
    pBrgInfo->BridgeTimes.u2HelloTime = u2HelloTime;
    pBrgInfo->u1MigrateTime =
        (UINT1) ((u2HelloTime / AST_CENTI_SECONDS) +
                 ((u2HelloTime / 2) / AST_CENTI_SECONDS));

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Instance %u: Set Hello Time as :%u...\n",
                  u2InstanceId, u2HelloTime);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Instance %u: Set Hello Time as :%u...\n",
                  u2InstanceId, u2HelloTime);

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
    {
        if ((!(AST_IS_RST_ENABLED ())) && (!(AST_IS_MST_ENABLED ())))
        {
            pBrgInfo->RootTimes.u2HelloTime = pBrgInfo->BridgeTimes.u2HelloTime;
        }
    }

    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
    {
        /* Updating the Root Times, only if this Bridge is the Root Bridge */
        /* Updating the Desg Times and Port Times always */
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            pBrgInfo->RootTimes.u2HelloTime = pBrgInfo->BridgeTimes.u2HelloTime;
        }

        /* Both root and non-root bridges will have their hello times
         * updated */
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if ((pPortEntry) == NULL)
            {
                continue;
            }

            if (AST_IS_MST_ENABLED ())
            {
                pPortEntry->u2HelloTime = pBrgInfo->BridgeTimes.u2HelloTime;
            }

            pPortEntry->DesgTimes.u2HelloTime =
                pBrgInfo->BridgeTimes.u2HelloTime;

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      RST_DEFAULT_INSTANCE);

            /* Update the port times only if the port
             * is a designated port */
            if (pPerStPortInfo->PerStRstPortInfo.u1InfoIs == RST_INFOIS_MINE)
            {
                pPortEntry->PortTimes.u2HelloTime =
                    pPortEntry->DesgTimes.u2HelloTime;
            }
        }                        /* End of For Loop */
    }

}

/*****************************************************************************/
/* Function Name      : AstSetForwardDelay                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      forward delay time is set through management and the */
/*                      corresponding event is generated.                    */
/*                                                                           */
/* Input(s)           : u2InstanceId - The Id of the Spanning Tree Instance  */
/*                      u2ForwardDelay - The forward delay value to be set   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstSetForwardDelay (UINT2 u2InstanceId, UINT2 u2ForwardDelay)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pBrgInfo = AST_GET_BRGENTRY ();
    if (pBrgInfo->BridgeTimes.u2ForwardDelay == u2ForwardDelay)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in ForwardDelay value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in ForwardDelay value\n");
        return;
    }
    pBrgInfo->BridgeTimes.u2ForwardDelay = u2ForwardDelay;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Instance %u: Set Forward Delay as :%u...\n",
                  u2InstanceId, u2ForwardDelay);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Instance %u: Set Forward Delay as :%u...\n",
                  u2InstanceId, u2ForwardDelay);

    if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
    {
        if ((!(AST_IS_RST_ENABLED ())) && (!(AST_IS_MST_ENABLED ())))
        {
            pBrgInfo->RootTimes.u2ForwardDelay =
                pBrgInfo->BridgeTimes.u2ForwardDelay;
        }
    }

    /* Updating the Root Times, Desg Times and Port Times if this Bridge 
     * is the Root Bridge */
    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
    {
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            /* This Bridge is the Root Bridge */
            pBrgInfo->RootTimes.u2ForwardDelay =
                pBrgInfo->BridgeTimes.u2ForwardDelay;

            u2PortNum = 1;

            AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
            {
                if ((pPortEntry) == NULL)
                {
                    continue;
                }
                pPortEntry->DesgTimes.u2ForwardDelay =
                    pPortEntry->PortTimes.u2ForwardDelay =
                    pBrgInfo->RootTimes.u2ForwardDelay;
            }                    /* End of For Loop */
        }                        /* End of this Bridge being the Root Bridge */
    }
}

/*****************************************************************************/
/* Function Name      : AstSetTxHoldCount                                    */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Transmit Hold Count value is set through management  */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u1TxHoldCount - The value of the Transmit Hold Count */
/*                                      to be set                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstSetTxHoldCount (UINT1 u1TxHoldCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();
    pAstBridgeEntry->u1TxHoldCount = u1TxHoldCount;
    if (AST_IS_RST_STARTED ())
    {
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if (pPortEntry == NULL)
            {
                continue;
            }
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
            if (pCommPortInfo != NULL)
            {
                pCommPortInfo->u1TxCount = AST_INIT_VAL;
            }
        }
    }
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Set Tx Hold Count as :%u...\n", u1TxHoldCount);
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Set Tx Hold Count as :%u...\n", u1TxHoldCount);

}

/*****************************************************************************/
/* Function Name      : AstSetAutoEdgePort                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      AutoEdgePort value is set through management         */
/*                      it means automatic dectection of Edge port is made   */
/*                      possible                                             */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which is to  */
/*                                  be made as an Edge Port                  */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should function as an Edge Port or not     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    UINT1               aau1AutoEdgePortStatus[2][10] = {
        "FALSE", "TRUE"
    };

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set AutoEdgeStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AutoEdgePortStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG, "SNMP: Port %s: Set AutoEdgeStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AutoEdgePortStatus[bStatus]);

    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE)
    {
        return (AstCVlanSetAutoEdge (u2PortNum, bStatus));
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry->bAutoEdge == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in AutoEdge Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in AutoEdge Status\n");
        return RST_SUCCESS;
    }
    pAstPortEntry->bAutoEdge = bStatus;

#ifdef MSTP_WANTED
    if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
    {
        MstSispCopyPortPropToSispLogical (AST_GET_IFINDEX (u2PortNum),
                                          MST_AUTO_EDGE, bStatus);
    }
#endif

    return (AstTrigAutoEdgeToBrgDetSem (u2PortNum, bStatus));
}

/*****************************************************************************/
/* Function Name      : AstTrigAutoEdgeToBrgDetSem                           */
/*                                                                           */
/* Description        : This function updates the auto edge status for the   */
/*                      given port. It also triggers the bridge detection    */
/*                      machine.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which is to  */
/*                                  be made as an Edge Port                  */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should function as an Edge Port or not     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstTrigAutoEdgeToBrgDetSem (UINT2 u2PortNum, tAstBoolean bStatus)
{
    if (AST_IS_RST_ENABLED () || AST_IS_MST_ENABLED ())
    {
        if (bStatus == RST_TRUE)
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG, "BDSM: Port %s: AutoEdge = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstBrgDetectionMachine (RST_BRGDETSM_EV_AUTOEDGE_SET,
                                        u2PortNum) != RST_SUCCESS)
            {
                AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: BridgeDetection Machine returned failure!\n");
                return RST_FAILURE;
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortRestrictedRole                             */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      restrictedRole (RootGuard) value is set through      */
/*                      management. It sets/resets restrictedRole field for  */
/*                      given port and takes action accoridingly.            */
/*                      If restrictedRole is set, then this port should not  */
/*                      become a root port. Hence check that this port is a  */
/*                      root port. If so then trigger role selection.        */
/*                      If restrictedRole is reset, then check that the port */
/*                      is an alternate port. If so then trigger role        */
/*                      selection.                                           */
/*                                                                           */
/* Input(s)           : pAstPortEntry  - Pointer to Port Entry               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Root Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstSetPortRestrictedRole (tAstPortEntry * pPortEntry, tAstBoolean bStatus)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    UINT1               aau1PortRootGuardStatus[2][10] = {
        "FALSE", "TRUE"
    };

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %u: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo,
                                              RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    AST_PORT_RESTRICTED_ROLE (pPortEntry) = bStatus;

    if (AST_IS_RST_ENABLED ())
    {
        if (bStatus == RST_TRUE)
        {
            if (pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ROOT)
            {
                return RST_SUCCESS;
            }

            pRstPortInfo->bSelected = RST_FALSE;
            MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pRstPortInfo->bReSelect = RST_TRUE;

            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             (UINT2) RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
        else
        {
            if (pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ALTERNATE)
            {
                return RST_SUCCESS;
            }

            pRstPortInfo->bSelected = RST_FALSE;
            MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pRstPortInfo->bReSelect = RST_TRUE;

            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             (UINT2) RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RoleSelectionMachine returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortRootGuard                                  */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      RootGuard value is set through                       */
/*                      management. It sets/resets RootGuard field for       */
/*                      given port and takes action accoridingly.            */
/*                      If RootGuard is set, then this port should not       */
/*                      become a root port. Hence check that this port is a  */
/*                      root port. If so then trigger role selection.        */
/*                      If RootGuard is reset, then check that the port      */
/*                      is an alternate port. If so then trigger role        */
/*                      selection.                                           */
/*                                                                           */
/* Input(s)           : pAstPortEntry  - Pointer to Port Entry               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Root Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstSetPortRootGuard (tAstPortEntry * pPortEntry, tAstBoolean bStatus)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    UINT1               aau1PortRootGuardStatus[2][10] = {
        "FALSE", "TRUE"
    };

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %u: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo,
                                              RST_DEFAULT_INSTANCE);
    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pRstPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    AST_PORT_ROOT_GUARD (pPortEntry) = bStatus;
    /* RootInconsistent reset when root guard is disabled */

    if ((bStatus == RST_FALSE) && (pPortEntry->bRootInconsistent != RST_FALSE))
    {
        /*Stop the root inconsistency recovery timer if it is running */
        if (pRstPortInfo->pRootIncRecTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for Root Inconsistency"
                         " Recovery timer FAILED!\n");
                return RST_FAILURE;
            }
        }
        UtlGetTimeStr (au1TimeStr);
        pPortEntry->bRootInconsistent = RST_FALSE;
        AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          au1TimeStr);
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPseudoRootId                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      PseudoRootId is configured through management.       */
/*                      If Port is Customer Edge Port then configuration     */
/*                      will be done on Customer Edge Port and               */
/*                      PseudoInfo state machine will get triggetered for    */
/*                      CVLAN component.                                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which Pseudo */
/*                                  RootId is configured                     */
/*                      u2InstanceId - InstanceId for which Pseudo RootId is */
/*                                to be configured.                          */
/*                      BrgMacAddr - Pseudo Root Mac Address                 */
/*                      u2BrgPriority - Pseudo Root Priority                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetPseudoRootId (UINT2 u2PortNum, UINT2 u2InstanceId,
                    tAstMacAddr BrgMacAddr, UINT2 u2BrgPriority)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstMacAddr         au1SwitchMac;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    AST_MEMSET (au1SwitchMac, AST_INIT_VAL, AST_MAC_ADDR_LEN);
    /* Incase of customer edge port, update the pseudo Root Id
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        return (AstPbSetPseudoRootIdInCvlanComp (pAstPortEntry,
                                                 BrgMacAddr, u2BrgPriority));
    }
    pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

    pAstPerStPortInfo->PseudoRootId.u2BrgPriority = u2BrgPriority;

    AST_MEMCPY (pAstPerStPortInfo->PseudoRootId.BridgeAddr,
                BrgMacAddr, AST_MAC_ADDR_SIZE);

    /* Dont set pseudo Id Configured Flag if it is Switch Mac */
    CfaGetSysMacAddress (au1SwitchMac);
    if (AST_MEMCMP (au1SwitchMac, BrgMacAddr, AST_MAC_ADDR_SIZE) == 0)
    {
        pAstPerStPortInfo->bIsPseudoRootIdConfigured = RST_FALSE;
    }
    else
    {
        pAstPerStPortInfo->bIsPseudoRootIdConfigured = RST_TRUE;
    }

    /* 
     * Trigger Pseudo Info State Machine
     */
    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ()))
    {
        if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_BEGIN, u2PortNum, NULL)
            != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RstPseudoInfoMachine returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RstPseudoInfoMachine returned FAILURE!\n");

            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPortL2gpStatus                                 */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      L2gp status configured through management.           */
/*                      If Port is Customer Edge Port then configuration     */
/*                      will be done on Customer Edge Port and               */
/*                      PseudoInfo State Machine will get triggered for      */
/*                      L2gp enable/disabled accordingly.                    */
/*                      if Port is CEP then PseudoInfo state machine will get*/
/*                      triggetered for CVLAN component.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which Pseudo */
/*                                  RootId is configured                     */
/*                      bIsL2gp - if bIsL2gp is TRUE, then port is conf-     */
/*                                  igured as Gateway port.                  */
/*                                  if bIsL2gp is FALSE, then port is conf-  */
/*                                  igured as Gateway Port.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetPortL2gpStatus (UINT2 u2PortNum, tAstBoolean bIsL2gp)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT1               u1Event;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /* Incase of customer edge port, update the pseudo Root Id
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        return (AstPbSetL2gpInCvlanComp (pPortEntry, bIsL2gp));
    }

    AST_PORT_IS_L2GP (pPortEntry) = bIsL2gp;

    if (bIsL2gp == RST_TRUE)
    {
        u1Event = RST_PSEUDO_INFO_EV_L2GP_ENABLED;
    }
    else
    {
        u1Event = RST_PSEUDO_INFO_EV_L2GP_DISABLED;
    }

    /* 
     * Trigger Pseudo Info State Machine
     */
    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ()))
    {
        if (RstPseudoInfoMachine (u1Event, u2PortNum, NULL) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RstPseudoInfoMachine returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RstPseudoInfoMachine returned FAILURE!\n");

            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPortBpduRxStatus                               */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      BPDURx status configured through management.         */
/*                      If Port is Customer Edge Port the configuration      */
/*                      will be done on Customer Edge Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which BPDU   */
/*                                  Receive status to be configured          */
/*                      bEnableBPDURx - if bEnableBPDURx is TRUE, then port  */
/*                                      configured to receive BPDU on port.  */
/*                                      if bEnableBPDURx is FALSE, then port */
/*                                      configured to ignored BPDU received  */
/*                                      on this port.                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetPortBpduRxStatus (UINT2 u2PortNum, tAstBoolean bEnableBPDURx)
{
    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    AST_PORT_ENABLE_BPDU_RX (pPortEntry) = bEnableBPDURx;
    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ())
        || AST_IS_PVRST_ENABLED ())
    {
        /* Incase of customer edge port, update the BPDU Rx Status
         * in the C-VLAN component CEP. */
        if (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT)
        {
            return (AstPbSetBpduRxInCvlanComp (pPortEntry, bEnableBPDURx));
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPortBpduTxStatus                               */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      BPDUTx status configured through management          */
/*                      If Port is Customer Edge Port the configuration      */
/*                      will be done on Customer Edge Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which BPDU   */
/*                                  Transmit status to be configured         */
/*                      bEnableBPDUTx - if bEnableBPDUTx is TRUE, then port  */
/*                                      configured to transmit BPDU on port. */
/*                                      if bEnableBPDUTx is FALSE, then port */
/*                                      configured  not to transmit BPDU     */
/*                                      from this port.                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstSetPortBpduTxStatus (UINT2 u2PortNum, tAstBoolean bEnableBPDUTx)
{
    tAstPortEntry      *pPortEntry = NULL;
#ifdef PVRST_WANTED
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2Inst = 0;
#endif

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    AST_PORT_ENABLE_BPDU_TX (pPortEntry) = bEnableBPDUTx;

    /* Incase of customer edge port, update the BPDU Tx Status
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        return (AstPbSetBpduTxInCvlanComp (pPortEntry, bEnableBPDUTx));
    }
    /* Stop HellWhenTimer, if Bpdu transmission is disable on port and,
     * Start the Timer, if Bpdu transmission is enabled on port.
     */
    /* 
     * Trigger RTSM for change in enableBPDUTx status.
     */

    if ((AST_IS_RST_ENABLED ()) || (AST_IS_MST_ENABLED ()))
    {
        if (bEnableBPDUTx == RST_TRUE)
        {
            if (RstPortTransmitMachine (RST_PTXSM_EV_TX_ENABLED, pPortEntry,
                                        RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;

            }
        }
        else
        {
            if (RstPortTransmitMachine (RST_PTXSM_EV_TX_DISABLED, pPortEntry,
                                        RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;

            }
        }
    }
#ifdef PVRST_WANTED
    else
    {
        for (u2Inst = 0; u2Inst < AST_MAX_PVRST_INSTANCES; ++u2Inst)
        {
            if (AST_GET_PERST_INFO (u2Inst) == NULL)
            {
                continue;
            }
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (bEnableBPDUTx == RST_TRUE)
            {
                if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_TX_ENABLED,
                                              pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            else
            {
                if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_TX_DISABLED,
                                              pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }

            }
        }

    }
#endif

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortLoopGuard                                  */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      loopguard value is set through management.  It       */
/*                      sets/resets port state and role field for given port */
/*                      and takes action accoridingly.                       */
/*                      If loopGuard is set, then this port should not       */
/*                      become forwarding for any instacne then trigger the  */
/*                      bridge detection state machine with auto edge false  */
/*                      to restrict the port state to go forwarding.         */
/*                      If loopGuard is reset, then check that the port      */
/*                      can go to forwarding state                           */
/*                                                                           */
/* Input(s)           : PortNum  - Port Number                               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Root Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry->bLoopGuard == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in LoopGuard  Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in LoopGuard Status\n");
        return RST_SUCCESS;
    }

    pAstPortEntry->bLoopGuard = bStatus;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pPerStPortInfo->bLoopGuardStatus = bStatus;

    if (((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) ||
         (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED))
        && (bStatus == RST_TRUE))
    {
        pPerStPortInfo->bLoopIncStatus = RST_FALSE;
        pPerStPortInfo->bLoopGuardStatus = RST_FALSE;
    }

    if (bStatus == RST_FALSE)
    {
        if (pAstPortEntry->bLoopInconsistent == RST_TRUE)
        {

            RstPortInfoSmMakeAged (pPerStPortInfo, (tAstBpdu *) NULL);
        }
        if (pAstPortEntry->bLoopInconsistent == RST_TRUE)
        {
            AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1TimeStr);

            pAstPortEntry->bLoopInconsistent = RST_FALSE;
        }
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Loop Guard Status as: %u...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), bStatus);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Loop Guard Status as: %u...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), bStatus);

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstSetPortErrorRecovery                    */
/*                                                                           */
/*    Description               : This function sets the Error Recovery Time */
/*                                for Port                                   */
/*                                                                           */
/*    Input(s)                  : u2PortNo - Port Number for which the Hello */
/*                                           to be set.                      */
/*                                u4ErrorRecovery - Error Recovery Time value*/
/*                                to be set.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS on Success.                    */
/*                                RST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
RstSetPortErrorRecovery (UINT2 u2PortNo, UINT4 u4ErrorRecovery)
#else
INT4
RstSetPortErrorRecovery (u2PortNo, u4ErrorRecovery)
     UINT2               u2PortNo;
     UINT2               u4ErrorRecovery;
#endif
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);

    if (pAstPortEntry == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return RST_FAILURE;
    }

    if (pAstPortEntry->u4ErrorRecovery == u4ErrorRecovery)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Error Recovery Time value\n");
        AST_DBG (AST_MGMT_DBG,
                 "SNMP: No change in Error Recovery Time value\n");
        return RST_SUCCESS;
    }
    pAstPortEntry->u4ErrorRecovery = u4ErrorRecovery;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Error Recovery time set as :%u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u4ErrorRecovery);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstSetPortDot1WEnable                                */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      dot1W status is set.                                 */
/*                                                                           */
/* Input(s)           : PortNum  - Port Number                               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have 802.1W mode enabled            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstSetPortDot1WEnable (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstPortEntry == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return RST_FAILURE;
    }

    if (pAstPortEntry->bDot1wEnabled == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: There is no change in 802.1W  Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: There is no change in 802.1W Status\n");
        return RST_SUCCESS;
    }

    pAstPortEntry->bDot1wEnabled = bStatus;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: 802.1W Status as: %u...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), bStatus);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: 802.1W Status as: %u...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), bStatus);

    return RST_SUCCESS;
}

/* End of file */
