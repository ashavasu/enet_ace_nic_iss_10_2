/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmirslw.c,v 1.7 2008/08/20 15:27:55 iss Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmsrslw.h"
# include   "vcm.h"
# include  "asthdrs.h"
# include  "stpcli.h"

/* LOW LEVEL Routines for Table : FsDot1dStpExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStpExtTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStpExtTable (INT4 i4FsDot1dStpContextId)
{
    if (AST_IS_VC_VALID (i4FsDot1dStpContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStpExtTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStpExtTable (INT4 *pi4FsDot1dStpContextId)
{

    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1dStpContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStpExtTable
 Input       :  The Indices
                FsDot1dStpContextId
                nextFsDot1dStpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dStpExtTable (INT4 i4FsDot1dStpContextId,
                                   INT4 *pi4NextFsDot1dStpContextId)
{

    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsDot1dStpContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1dStpContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStpVersion
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpVersion (INT4 i4FsDot1dStpContextId,
                         INT4 *pi4RetValFsDot1dStpVersion)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpVersion (pi4RetValFsDot1dStpVersion);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpTxHoldCount
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpTxHoldCount (INT4 i4FsDot1dStpContextId,
                             INT4 *pi4RetValFsDot1dStpTxHoldCount)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpTxHoldCount (pi4RetValFsDot1dStpTxHoldCount);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStpVersion
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpVersion (INT4 i4FsDot1dStpContextId,
                         INT4 i4SetValFsDot1dStpVersion)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpVersion (i4SetValFsDot1dStpVersion);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpTxHoldCount
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpTxHoldCount (INT4 i4FsDot1dStpContextId,
                             INT4 i4SetValFsDot1dStpTxHoldCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpTxHoldCount (i4SetValFsDot1dStpTxHoldCount);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpVersion
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpVersion (UINT4 *pu4ErrorCode, INT4 i4FsDot1dStpContextId,
                            INT4 i4TestValFsDot1dStpVersion)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpVersion (pu4ErrorCode, i4TestValFsDot1dStpVersion);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpTxHoldCount
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpTxHoldCount (UINT4 *pu4ErrorCode, INT4 i4FsDot1dStpContextId,
                                INT4 i4TestValFsDot1dStpTxHoldCount)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpTxHoldCount (pu4ErrorCode,
                                      i4TestValFsDot1dStpTxHoldCount);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStpExtTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStpExtTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDot1dStpExtPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStpExtPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStpExtPortTable (INT4 i4FsDot1dStpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1dStpExtPortTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStpExtPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStpExtPortTable (INT4 *pi4FsDot1dStpPort)
{
    return nmhGetNextIndexFsDot1dStpExtPortTable (0, pi4FsDot1dStpPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStpExtPortTable
 Input       :  The Indices
                FsDot1dStpPort
                nextFsDot1dStpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStpExtPortTable (INT4 i4FsDot1dStpPort,
                                       INT4 *pi4NextFsDot1dStpPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsDot1dStpPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsDot1dStpPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsDot1dStpPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                && (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                              (pAstPortEntry))))
            {
                *pi4NextFsDot1dStpPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortProtocolMigration
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot1dStpPortProtocolMigration (INT4 i4FsDot1dStpPort,
                                       INT4
                                       *pi4RetValFsDot1dStpPortProtocolMigration)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortProtocolMigration ((INT4) u2LocalPortId,
                                                    pi4RetValFsDot1dStpPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortAdminEdgePort
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortAdminEdgePort (INT4 i4FsDot1dStpPort,
                                   INT4 *pi4RetValFsDot1dStpPortAdminEdgePort)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortAdminEdgePort ((INT4) u2LocalPortId,
                                                pi4RetValFsDot1dStpPortAdminEdgePort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortOperEdgePort
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortOperEdgePort (INT4 i4FsDot1dStpPort,
                                  INT4 *pi4RetValFsDot1dStpPortOperEdgePort)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortOperEdgePort ((INT4) u2LocalPortId,
                                               pi4RetValFsDot1dStpPortOperEdgePort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortAdminPointToPoint (INT4 i4FsDot1dStpPort,
                                       INT4
                                       *pi4RetValFsDot1dStpPortAdminPointToPoint)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortAdminPointToPoint ((INT4) u2LocalPortId,
                                                    pi4RetValFsDot1dStpPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortOperPointToPoint
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortOperPointToPoint (INT4 i4FsDot1dStpPort,
                                      INT4
                                      *pi4RetValFsDot1dStpPortOperPointToPoint)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortOperPointToPoint ((INT4) u2LocalPortId,
                                                   pi4RetValFsDot1dStpPortOperPointToPoint);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortAdminPathCost
 Input       :  The Indices
                FsDot1dStpPort
                The Object 
                retValFsDot1dStpPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot1dStpPortAdminPathCost (INT4 i4FsDot1dStpPort,
                                   INT4 *pi4RetValFsDot1dStpPortAdminPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1dStpPortAdminPathCost ((INT4) u2LocalPortId,
                                                pi4RetValFsDot1dStpPortAdminPathCost);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortProtocolMigration
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortProtocolMigration (INT4 i4FsDot1dStpPort,
                                       INT4
                                       i4SetValFsDot1dStpPortProtocolMigration)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortProtocolMigration ((INT4) u2LocalPortId,
                                                    i4SetValFsDot1dStpPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortAdminEdgePort
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortAdminEdgePort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortAdminEdgePort (INT4 i4FsDot1dStpPort,
                                   INT4 i4SetValFsDot1dStpPortAdminEdgePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortAdminEdgePort ((INT4) u2LocalPortId,
                                                i4SetValFsDot1dStpPortAdminEdgePort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortAdminPointToPoint (INT4 i4FsDot1dStpPort,
                                       INT4
                                       i4SetValFsDot1dStpPortAdminPointToPoint)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortAdminPointToPoint ((INT4) u2LocalPortId,
                                                    i4SetValFsDot1dStpPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortAdminPathCost
 Input       :  The Indices
                FsDot1dStpPort
                The Object 
                setValFsDot1dStpPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot1dStpPortAdminPathCost (INT4 i4FsDot1dStpPort,
                                   INT4 i4SetValFsDot1dStpPortAdminPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortAdminPathCost ((INT4) u2LocalPortId,
                                                i4SetValFsDot1dStpPortAdminPathCost);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortProtocolMigration
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortProtocolMigration (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDot1dStpPort,
                                          INT4
                                          i4TestValFsDot1dStpPortProtocolMigration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortProtocolMigration (pu4ErrorCode,
                                                (INT4) u2LocalPortId,
                                                i4TestValFsDot1dStpPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortAdminEdgePort
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortAdminEdgePort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortAdminEdgePort (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dStpPort,
                                      INT4 i4TestValFsDot1dStpPortAdminEdgePort)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortAdminEdgePort (pu4ErrorCode, (INT4) u2LocalPortId,
                                            i4TestValFsDot1dStpPortAdminEdgePort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortAdminPointToPoint (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDot1dStpPort,
                                          INT4
                                          i4TestValFsDot1dStpPortAdminPointToPoint)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortAdminPointToPoint (pu4ErrorCode,
                                                (INT4) u2LocalPortId,
                                                i4TestValFsDot1dStpPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortAdminPathCost
 Input       :  The Indices
                FsDot1dStpPort
                The Object 
                testValFsDot1dStpPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortAdminPathCost (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dStpPort,
                                      INT4 i4TestValFsDot1dStpPortAdminPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dStpPortAdminPathCost (pu4ErrorCode,
                                                   (INT4) u2LocalPortId,
                                                   i4TestValFsDot1dStpPortAdminPathCost);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStpExtPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStpExtPortTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
