/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astprdstb.c,v 1.15 2011/09/06 06:54:07 siva Exp $
 *
 * Description: This file contains the Dummy functions to support High 
 *              Availability for AST module.   
 *
 *******************************************************************/
#ifndef L2RED_WANTED
#include "astprdstb.h"
/*****************************************************************************/
/* Function Name      : AstRedRmInit                                         */
/*                                                                           */
/* Description        : Initialises the Memory Pools and Registers with RM   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : if registration is success then RST_SUCCESS          */
/*                      Otherwise RST_FAILURE                                */
/*****************************************************************************/

INT4
AstRedRmInit (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSelectContext                                  */
/*                                                                           */
/* Description        : Selects a context for Spanning red information.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Id                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if select red context is success then RST_SUCCESS    */
/*                      Otherwise RST_FAILURE                                */
/*****************************************************************************/
INT4
AstRedSelectContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedReleaseContext                                 */
/*                                                                           */
/* Description        : Relases the context for Spanning red information.    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedReleaseContext (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Creates a queue for receiving messages from RM and   */
/*                      registers RSTP/MSTP with RM.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then RST_SUCCESS          */
/*                      Otherwise RST_FAILURE                                 */
/*****************************************************************************/
INT4
AstRedRegisterWithRM (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedDeRegisterWithRM                               */
/*                                                                           */
/* Description        : Deregisters RSTP/MSTP/PVRST with RM.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then RST_SUCCESS        */
/*                      Otherwise RST_FAILURE                                */
/*****************************************************************************/
INT4
AstRedDeRegisterWithRM (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSendUpdate                                     */
/*                                                                           */
/* Description        : This function is invoked to  Update RM Buffers  to   */
/*                      send the Update packet via the RM                    */
/*                      from RM module Based on the changed flags all the    */
/*                      data is sent or only the changed data is sent        */
/*                      Here Getnext is used to find the next Messages that  */
/*                      has to be appended in the Buffer.The getNext         */
/*                      messages will return the message type and Length of  */
/*                      messages for which allocation is to be done          */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tAstGlobalInfo.                                      */
/*                      tAstRedGlobalInfo                                    */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedSendUpdate (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSendSyncMessages                               */
/*                                                                           */
/* Description        : This function is invoked to  Alloc and Send Bulk     */
/*                      Sync Up Messages                                     */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id                           */
/*                    : u2PortIfIndex - Port If index                        */
/*                    : u1MessageType - Type of Sync Message                 */
/*                    : u1MessageSubType- SubType of SyncMessage             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE.                             */
/*****************************************************************************/
INT4
AstRedSendSyncMessages (UINT2 u2InstanceId, UINT2 u2PortIfIndex,
                        UINT1 u1MessageType, UINT1 u1MessageSubType)
{
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u2PortIfIndex);
    UNUSED_PARAM (u1MessageType);
    UNUSED_PARAM (u1MessageSubType);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearPduOnActive                               */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           : u2PortIfIndex - Port If index                        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedClearPduOnActive (UINT2 u2PortIfIndex)
{
    UNUSED_PARAM (u2PortIfIndex);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearSyncUpDataOnPort                          */
/* Description        : Clears stale Data On Port                            */
/* Input(s)           : u2PortIfIndex - Port If index                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearSyncUpDataOnPort (UINT2 u2PortIfIndex)
{
    UNUSED_PARAM (u2PortIfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedClearAllPduOnActive                            */
/* Description        : Invalidates all the PDUs in Active Node              */
/* Input(s)           : u2PortIfIndex - Port If index                        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedClearAllPduOnActive (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedSyncUpPdu                                      */
/*                                                                           */
/* Description        : Stores and Send RSTP sync up Pdu to Standby Node     */
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/
INT4
AstRedSyncUpPdu (UINT2 u2PortNum, tAstBpdu * pData, UINT2 u2Len)
{
    AST_UNUSED (u2PortNum);
    AST_UNUSED (pData);
    AST_UNUSED (u2Len);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedGetPortState                                   */
/*                                                                           */
/* Description        : Gets Synced Up Port State                            */
/*                      Should not be called when Active                     */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Synced Up Port State                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Synced Up Port State.                                */
/*****************************************************************************/

INT4
AstRedGetPortState (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return AST_PORT_STATE_DISABLED;
}

/*****************************************************************************/
/* Function Name      : AstRedGetOperEdgeStatus                              */
/*                                                                           */
/* Description        : Gets Synced Up Oper Edge Status                      */
/*                      Should not be called when Active                     */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Oper Edge Status                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/RST_FALSE.                                  */
/*****************************************************************************/

INT4
AstRedGetOperEdgeStatus (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_FALSE;
}

/*****************************************************************************/
/* Function Name      : RedDumpSynced Up Outputs                             */
/*                                                                           */
/* Description        : Dump PDU on a particular Port                        */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncedUpOutputs (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    return;
}

/*****************************************************************************/
/* Function Name      : RedDumpSyncedUpData                                 */
/*                                                                           */
/* Description        : Dumped Synced Up times & Variables on Standby on all */
/*                      Ports Instance                                       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RedDumpSyncedUpData (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    return;
}

/*****************************************************************************/
/* Function Name      : RedDumpSyncedUpPdus                                     */
/*                                                                           */
/* Description        : Dump PDU on a particular Port                        */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RedDumpSyncedUpPdus (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleRcvdInfoWhileTmrExp                      */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - If the timer exp count >=                     */
/*                                  AST_NUM_TMR_INTERVAL_SPLITS, treat it as */
/*                                  Receive info while timer expiry.         */
/*                             Else                                          */
/*                                  Calls Update receive info while fn.      */
/*                      for given port                                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to port information         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
VOID
AstRedHandleRcvdInfoWhileTmrExp (tAstPerStPortInfo * pPerStPortInfo)
{
    UNUSED_PARAM (pPerStPortInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedInitOperTimes                                  */
/*                                                                           */
/* Description        : This function will initalize the timers values in    */
/*                      sync up database that are started during             */
/*                      Port Enable Msg                                      */
/*                                                                           */
/* Input(s)           : Instance Id - Instance Information                   */
/*                      PortIfIndex - Port Interface Index                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstRedInitOperTimes (UINT2 u2InstanceId, UINT2 u2PortIfIndex)
{
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u2PortIfIndex);
    return RST_SUCCESS;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : AstRedHRProcStdyStPktReq                             */
/*                                                                           */
/* Description        : This function is called whenever there is a steady   */
/*                      state packet request from RM                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRProcStdyStPktReq (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedHRSetSSPTmrValInCxt                            */
/*                                                                           */
/* Description        : This function loops through the ports in the context */
/*                      and calls the function AstRedHRRestartTmrForSSP to   */
/*                      restart the timer                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRSetSSPTmrValInCxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
}

/*****************************************************************************/
/* Function Name      : AstRedHRRestartTmrForSSP                             */
/*                                                                           */
/* Description        : This function stops the timer for each port if it is */
/*                      running and restart it with zero duration to trigger */
/*                      a steady state packet to RM                          */
/*                                                                           */
/* Input(s)           : pAstPortEntry - Pointer to port entry structure.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedHRRestartTmrForSSP (tAstPortEntry * pAstPortEntry)
{
    UNUSED_PARAM (pAstPortEntry);
}

/*****************************************************************************/
/* Function Name      : AstRedHRSendStdyStPkt                                */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/
INT1
AstRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pu1LinBuf, UINT4 u4PktLen,
                       UINT2 u2Port, UINT4 u4TimeOut)
{
    UNUSED_PARAM (pu1LinBuf);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4TimeOut);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedHRSendStdyStTailMsg                            */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/
INT1
AstRedHRSendStdyStTailMsg (VOID)
{
    return RST_SUCCESS;
}

/******************************************************************************/
/*  Function           : AstRedGetHRFlag                                      */
/*  Input(s)           : None                                                 */
/*  Output(s)          : None                                                 */
/*  Returns            : Hitless restart flag value.                          */
/*  Action             : This API returns the hitless restart flag value.     */
/******************************************************************************/
UINT1
AstRedGetHRFlag (VOID)
{
    return RM_HR_STATUS_DISABLE;
}

#endif
