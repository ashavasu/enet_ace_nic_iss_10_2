/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rstpbstb.c,v 1.24 
*
********************************************************************/
/*MODULE ROUTINES*/

#include "asthdrs.h"
#include "stpcli.h"
#include "stpclipt.h"
#include "astpbcli.h"

/*NPAPI_ROUTINES*/
#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstMiStpNpHwInit                                     */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI initialisation for STP.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiStpNpHwInit (VOID)
{
    if (RstpFsMiStpNpHwInit (AST_CURR_CONTEXT_ID ()) != FNP_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiRstpNpHwInit                                    */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI intilisation when RSTP is enabled              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiRstpNpInitHw (VOID)
{
    RstpFsMiRstpNpInitHw (AST_CURR_CONTEXT_ID ());
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiNpDeleteAllFdbEntries                           */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for deleting All Fdb Entries of this context   */
/*                      when RSTP Module is enabled                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiNpDeleteAllFdbEntries (VOID)
{
    VlanMiDeleteAllFdbEntries (AST_CURR_CONTEXT_ID ());

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiRstpNpSetPortState                              */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Port State in RSTP Module per  */
/*                      context. This function is called when RSTP is disabled*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstpNpSetPortState (UINT2 u2PortNum, UINT1 u1PortState)
{

    INT4                i4RetVal = FNP_FAILURE;
    tAstPortEntry      *pPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (NULL == pPortInfo)
    {
        return RST_FAILURE;
    }

    i4RetVal = RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                           AST_IFENTRY_IFINDEX (pPortInfo),
                                           u1PortState);

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMiRstpNpGetPortState                              */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      RSTP.                                                */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the given port.        */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstMiRstpNpGetPortState (UINT2 u2PortNum, UINT1 *pu1PortState)
{
    tAstPortEntry      *pPortInfo = NULL;
    INT4                i4RetVal = FNP_FAILURE;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    i4RetVal = RstpFsMiRstNpGetPortState (AST_CURR_CONTEXT_ID (),
                                          AST_IFENTRY_IFINDEX (pPortInfo),
                                          pu1PortState);

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }

    return RST_FAILURE;
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstMiMstpNpSetInstancePortState                      */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Inst Port State in RSTP Module per*/
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      u2MstInst   - MstpInstance to which this port state  */
/*                      belongs                                              */
/*                      u1PortState - Port State for the HL Port Id          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiMstpNpSetInstancePortState (UINT2 u2PortNum,
                                 UINT2 u2MstInst, UINT1 u1PortState)
{

    INT4                i4RetVal = FNP_FAILURE;

    /*No need to Check for CVLAN context Since CVLAN component does not run
     * MSTP*/
    i4RetVal = FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 u2MstInst, u1PortState);

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstMiMstpNpGetInstancePortState                      */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      MSTP.                                                */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      u2MstInst   - MstpInstance to which this port state  */
/*                      belongs                                              */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the Port Id            */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiMstpNpGetInstancePortState (UINT2 u2PortNum,
                                 UINT2 u2MstInst, UINT1 *pu1PortState)
{

    INT4                i4RetVal = FNP_FAILURE;

    /* No need to Check for CVLAN context Since CVLAN component does not run
     * MSTP*/
    i4RetVal = MstpFsMiMstNpGetPortState (AST_CURR_CONTEXT_ID (),
                                          u2MstInst,
                                          AST_GET_PHY_PORT (u2PortNum),
                                          pu1PortState);

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }
    return RST_FAILURE;
}
#endif
#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstMiPvrstNpSetVlanPortState                         */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for setting the Vlan Port State in PVRST Module*/
/*                      per context.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      VlanId   - VlanId to which this port state belongs   */
/*                      u1PortState - Port State for the HL Port Id          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiPvrstNpSetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 u1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /*No need to Check for CVLAN context Since CVLAN component does not run
       PVRST */
    i4RetVal = PvrstFsMiPvrstNpSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 VlanId, u1PortState);

    if (i4RetVal == FNP_FAILURE)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstMiPvrstNpGetVlanPortState                         */
/*                                                                           */
/* Description        : This function is a wrapper function Provided for the.*/
/*                      NPAPI for getting the Port State from Hw in case of  */
/*                      PVRST.                                               */

/*                                                                           */
/* Input(s)           : u2PortNum   - HL Port Id                             */
/*                      VlanId   - VlanId to which this port state           */
/*                      belongs                                              */
/*                                                                           */
/* Output(s)          : pu1PortState - Port State for the Port Id            */
/*                                                                           */
/* Returns            : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstMiPvrstNpGetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 *pu1PortState)
{
    INT4                i4RetVal = FNP_SUCCESS;

    /* No need to Check for CVLAN context Since CVLAN component does not run
     * PVRST*/
    i4RetVal = PvrstFsMiPvrstNpGetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 VlanId,
                                                 AST_GET_IFINDEX (u2PortNum),
                                                 pu1PortState);

    if (i4RetVal == FNP_SUCCESS)
    {
        return RST_SUCCESS;
    }

    return RST_FAILURE;

}
#endif /*PVRST_WANTED */
#endif /*NPAPI_WANTED */

#ifdef SNMP_2_WANTED
/*****************************************************************************/
/* Function Name      : RegisterFSMPBR                                       */
/*                                                                           */
/* Description        : This Function is used to register MI provider Bridging*/
/*                      MIB.                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RegisterFSMPBR (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : RegisterFSPBRS                                       */
/*                                                                           */
/* Description        : This Function is used to register provider Bridging  */
/*                      MIB.                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RegisterFSPBRS (VOID)
{
    return;
}
#endif /*SNMP_2_WANTED */

/*L2IWF_ROUTINES*/
/*****************************************************************************/
/* Function Name      : AstCreateSpanningTreeInstanceInL2Iwf                 */
/*                                                                           */
/* Description        : This Function is used to create the Spanning tree    */
/*                      instance in L2IWF. The STP instance should not be    */
/*                      created in L2IWF for CVLAN component in PEB          */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be created in L2IWF       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstCreateSpanningTreeInstanceInL2Iwf (UINT2 u2MstInst)
{
    /* Stub Routines for L2IWF calls from RSTP Module */

    L2IwfCreateSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteMstInstanceFromL2Iwf                        */
/*                                                                           */
/* Description        : This Function is used to delete the Spanning tree    */
/*                      instance in L2IWF. The STP instance should not be    */
/*                      delete in L2IWF for CVLAN component in PEB          */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be created in L2IWF       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstDeleteMstInstanceFromL2Iwf (UINT2 u2MstInst)
{
    if (L2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst)
        != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetPortOperStatusFromL2Iwf                        */
/*                                                                           */
/* Description        : This Function is used to get the physical port OR    */
/*                      logical Port (PEP) oper status from l2iwf            */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstGetPortOperStatusFromL2Iwf (UINT1 u1Module, UINT2 u2PortNum,
                               UINT1 *pu1OperStatus)
{
    INT4                i4RetVal;

    i4RetVal =
        AstL2IwfGetPortOperStatus
        (u1Module, (UINT2) AST_GET_IFINDEX (u2PortNum), pu1OperStatus);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstGetInstPortStateFromL2Iwf                         */
/*                                                                           */
/* Description        : This Function is used to get the instance port state */
/*                      of physical port from l2iwf                          */
/*                                                                           */
/* Input(s)           : u2MstInst - MST instance ID                          */
/*                      u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_PORT_STATE_FORWARDING / AST_PORT_STATE_DISCARDING*/
/*****************************************************************************/

UINT1
AstGetInstPortStateFromL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum)
{
    UINT1               u1State;

    u1State = AstL2IwfGetInstPortState (u2MstInst, AST_GET_IFINDEX (u2PortNum));

    return u1State;
}

/*****************************************************************************/
/* Function Name      : AstSetInstPortStateFromL2Iwf                         */
/*                                                                           */
/* Description        : This Function is used to set the instance port state */
/*                      of physical OR logical Port (PEP)from l2iwf          */
/*                                                                           */
/* Input(s)           : u2MstInst - MST instance ID                          */
/*                      u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/

INT4
AstSetInstPortStateToL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum, UINT1 u1PortState)
{
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT4               u4Ticks = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    if (AstGetContextInfoFromIfIndex
        ((UINT4) AST_GET_IFINDEX (u2PortNum), &u4ContextId,
         &u2LocalPortId) != RST_FAILURE)
    {
        AstGetSysTime (&u4Ticks);

        /* Update global Port State Change Time Stamp */
        pBrgInfo = AST_GET_BRGENTRY ();
        if (pBrgInfo != NULL)
        {
            pBrgInfo->u4PortStateChangeTimeStamp = u4Ticks;
        }
        else
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MSG: BrgInfo not present\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: BrgInfo not present\n");

        }

        /* Update Per Port State Chnage Time Stamp */
        pPortInfo = AST_GET_PORTENTRY (u2LocalPortId);
        if (pPortInfo != NULL)
        {
            pPortInfo->u4PortStateChangeTimeStamp = u4Ticks;
        }
        else
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                          "MSG: PortInfo not present for port %d\n", u2PortNum);
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                          "MSG: PortInfo not present for port %d\n", u2PortNum);
        }

        /* Update Per Instance Per Port State Change Time Stamp */
        if (u2MstInst != AST_TE_MSTID)
        {
            if (AST_GET_PERST_INFO (u2MstInst) != NULL)
            {
                pAstPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2LocalPortId, u2MstInst);
                if (pAstPerStPortInfo != NULL)
                {
                    pAstPerStPortInfo->u4PortStateChangeTimeStamp = u4Ticks;
                }
                else
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                  "MSG: InstanceInfo not present for port %d instance %d\n",
                                  u2PortNum, u2MstInst);
                    AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                                  "MSG: InstanceInfo not present for port %d instance %d\n",
                                  u2PortNum, u2MstInst);
                }
            }

        }
    }
    i4RetVal = AstL2IwfSetInstPortState (u2MstInst,
                                         (UINT2) AST_GET_IFINDEX (u2PortNum),
                                         u1PortState);

    if (i4RetVal != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSetPortOperEdgeStatusToL2Iwf                      */
/*                                                                           */
/* Description        : This Function is used to set the Port Oper Edge Status*/
/*                      of physical OR logical Port (PEP)to  l2iwf           */
/*                                                                           */
/* Input(s)           : u2PortNum -  CEP Port Number / Physical Port Number  */
/*                      bOperEdge - OSIX_TRUE/OSIX_FALSE                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

INT4
AstSetPortOperEdgeStatusToL2Iwf (UINT2 u2PortNum, BOOL1 bOperEdge)
{
    if (AstL2IwfSetPortOperEdgeStatus (AST_CURR_CONTEXT_ID (),
                                       u2PortNum, bOperEdge) != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstGetPortOperEdgeStatusFromL2Iwf                    */
/*                                                                           */
/* Description        : This Function is used to get the Port Oper Edge Status*/
/*                      of physical OR logical Port (PEP)from  l2iwf         */
/*                                                                           */
/* Input(s)           : u2PortNum -  CEP Port Number / Physical Port Number  */
/*                                                                           */
/* Output(s)          : pbOperEdge - OSIX_TRUE/OSIX_FALSE                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

INT4
AstGetPortOperEdgeStatusFromL2Iwf (UINT2 u2PortNum, BOOL1 * pbOperEdge)
{
    if (AstL2IwfGetPortOperEdgeStatus
        ((UINT2) AST_GET_IFINDEX (u2PortNum), pbOperEdge) != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbHandleCcompCepPortStatus                        */
/*                                                                           */
/* Description        : This function is used to handle the enable/disable   */
/*                      Port (PORT_OPER_UP/ PORT_OPER_DOWN/ STP_PORT_UP/     */
/*                      STP_PORT_DOWN) for CEP only                          */
/*                                                                           */
/* Input(s)           : u2Port - Local port id of the customer edge port.    */
/*                      u1TrigType - PORT_OPER_UP/PORT_OPER_DOWN/STP_PORT_UP */
/*                      STP_PORT_DOWN                                        */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstPbHandleCcompCepPortStatus (UINT2 u2Port, UINT1 u1TrigType)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1TrigType);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbAllCVlanCompShutdown                            */
/*                                                                           */
/* Description        : This Function takes care of shutdown of all the      */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompShutdown (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompShutdown                               */
/*                                                                           */
/* Description        : This Function takes care of shutdown of CVLAN component*/
/*                      of a Port.                                           */
/*                                                                           */
/* Input(s)           : pPortEntry - CEP Port Entry                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstPbCVlanCompShutdown (tAstPortEntry * pAstPortEntry)
{
    UNUSED_PARAM (pAstPortEntry);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbAllCVlanCompDisable                             */
/*                                                                           */
/* Description        : This Function takes care of disabling   all the      */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompDisable (VOID)
{
    return RST_SUCCESS;
}

/****************************************************************************/
/* Function Name      : AstPbAllCVlanCompEnable                              */
/*                                                                           */
/* Description        : This Function takes care of enabling all the         */
/*                      CVLAN component in a context                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbAllCVlanCompEnable (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompEnable                                 */
/*                                                                           */
/* Description        : This Function takes care of enabling the CVLAN       */
/*                      component in a context                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanCompEnable (tAstPortEntry * pAstPortEntry)
{
    UNUSED_PARAM (pAstPortEntry);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanCompDisable                                */
/*                                                                           */
/* Description        : This Function takes care of enabling the CVLAN       */
/*                      component in a context                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanCompDisable (tAstPortEntry * pAstPortEntry)
{
    UNUSED_PARAM (pAstPortEntry);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUpdatePortType                                    */
/*                                                                           */
/* Description        : This Function takes care of updating the pbporttype  */
/*                      on basis of different bridge modes                   */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type to be updated  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstUpdatePortType (tAstPortEntry * pPortEntry, UINT1 u1BrgPortType)
{
    pPortEntry->u1PortType = AST_CUSTOMER_BRIDGE_PORT;
    UNUSED_PARAM (u1BrgPortType);

    return;
}

/*****************************************************************************/
/* Function Name      : AstUpdateCompType                                    */
/*                                                                           */
/* Description        : This Function takes care of updating the comp type   */
/*                      on basis of different bridge modes                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstUpdateCompType (VOID)
{
    AST_COMP_TYPE () = AST_C_VLAN;

    return;
}

/*OTHER PROTOCOL ROUTINES*/

/*****************************************************************************/
/* Function Name      : AstVlanResetShortAgeoutTime                          */
/*                                                                           */
/* Description        : This Function takes care of updating the short ageout*/
/*                      time in the VLAN module                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
AstVlanResetShortAgeoutTime (tAstPortEntry * pPortInfo)
{
    VlanResetShortAgeoutTime ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsVlanEnabledInContext                            */
/*                                                                           */
/* Description        : This Function checks whether VLAN is enabled in the  */
/*                      current context                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*****************************************************************************/

INT4
AstIsVlanEnabledInContext ()
{
    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        return RST_TRUE;
    }

    return RST_FALSE;
}

/*****************************************************************************/
/* Function Name      : AstVlanSetShortAgeoutTime                            */
/*                                                                           */
/* Description        : This Function sets the short age out time in VLAN    */
/*                      module for a given port                              */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                            */
/*****************************************************************************/

INT4
AstVlanSetShortAgeoutTime (tAstPortEntry * pPortInfo)
{
    UINT2               u2FwdDelay = AST_INIT_VAL;

    u2FwdDelay =
        (UINT2) (pPortInfo->DesgTimes.u2ForwardDelay / AST_CENTI_SECONDS);
    VlanSetShortAgeoutTime ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo),
                            u2FwdDelay);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstBrgDelFwdEntryForPort                             */
/*                                                                           */
/* Description        : This Function deletes all fdb entries learned on the */
/*                      given port                                           */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                            */
/*****************************************************************************/

INT4
AstBrgDelFwdEntryForPort (tAstPortEntry * pPortInfo)
{
#ifdef BRIDGE_WANTED
    BrgDeleteFwdEntryForPort (AST_IFENTRY_IFINDEX (pPortInfo));
#else
    UNUSED_PARAM (pPortInfo);
#endif

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleOutgoingPktOnPort                           */
/*                                                                           */
/* Description        : This function will be called by RSTP/MSTP module for */
/*                      sending a protocol port on a physical port / PEP     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be sended out                       */
/*                      pAstPortInfo - Port Info on which pkt should be send */
/*                      u4PktSize - Size of the Packet                       */
/*                      u2Protocol - Protocol Id to be sended to CFA         */
/*                      u1EncapType - Encap type of the pkt                  */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/

INT4
AstHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tAstPortEntry * pAstPortInfo, UINT4 u4PktSize, UINT2
                            u2Protocol, UINT1 u1EncapType)
{
    UINT1               u1BPDUFlags = 0;
    UINT4               u2Port;
    UINT4               i4Compatible = AST_TRUE;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pAstPortInfo->u2PortNo);

    if (AST_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1BPDUFlags,
                                   AST_TC_BIT_OFFSET, AST_TC_LENGTH);
        if (AST_HR_CHECK_STDY_ST_PKT (u1BPDUFlags))
        {
            AstRedHRSendStdyStPkt (pBuf, u4PktSize,
                                   (UINT2) pAstPortInfo->u4PhyPortIndex,
                                   pAstPortInfo->u2HelloTime);
            return RST_SUCCESS;
        }
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pAstPortInfo->u2PortNo);
    if (pAstCommPortInfo->bSendRstp == AST_TRUE)
    {
        if (AST_FORCE_VERSION != AST_VERSION_2)
        {
            i4Compatible = AST_MST_MODE;
        }
    }
    if (AST_IS_MST_STARTED () && (i4Compatible == AST_MST_MODE))
    {
        if ((gi1MstTxAllowed == AST_FALSE)
            && (AST_IS_PVRST_ENABLED () == AST_FALSE))
        {
            u2Port = pAstPortInfo->u2PortNo;
            if (u2Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                pAstPortInfo->bTransmitBpdu = AST_TRUE;
            }

            else
            {
                AST_TRC (AST_TXSM_DBG | AST_ALL_FAILURE_TRC,
                         "MSG: Port greater than Max Interfaces " "!!!\n");
            }
            AST_RELEASE_CRU_BUF (pBuf, RST_FALSE);
            return RST_SUCCESS;
        }

    }
    AstL2IwfHandleOutgoingPktOnPort (pBuf,
                                     AST_GET_PHY_PORT (pAstPortInfo->
                                                       u2PortNo), u4PktSize,
                                     u2Protocol, u1EncapType);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleCreatePepPort                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to create a PEP Port in the CVLAN component   */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstHandleCreatePepPort (tAstMsgNode * pMsgNode)
{
    UNUSED_PARAM (pMsgNode);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleDeletePepPort                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to delete a PEP Port status in the CVLAN      */
/*                      component                                            */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstHandleDeletePepPort (tAstMsgNode * pNode)
{
    UNUSED_PARAM (pNode);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandlePepOperStatus                               */
/*                                                                           */
/* Description        : This function is  called from the Message processing */
/*                      thread to update a PEP Port status in the CVLAN      */
/*                      component                                            */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstHandlePepOperStatus (tAstMsgNode * pNode)
{
    UNUSED_PARAM (pNode);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSwitchCVlanContext                                */
/*                                                                           */
/* Description        : This function is  called from the BPDU Processing    */
/*                      to find out whether BPDU is received on a PEP        */
/*                                                                           */
/* Input(s)           : pIfaceId - Interface Identifier                      */
/*                                                                           */
/* Output(s)          : *pu2PortNum - HL Port Id                             */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbSwitchCVlanContext (tAstInterface * pIfaceId, UINT2 *pu2PortNum)
{
    UNUSED_PARAM (pIfaceId);
    UNUSED_PARAM (pu2PortNum);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbRestoreContext                                  */
/*                                                                           */
/* Description        : This Function is responsible for restoring the context*/
/*                      pointer which is already stored in gpAstParentCtxtInfo*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
AstPbRestoreContext (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstHandleSVlanModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to handle the SVLAN module     */
/*                      status (Enable/Disable)                              */
/*                                                                           */
/* Input(s)           : pNode    - message Node                              */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstHandleSVlanModuleStatus (tAstMsgNode * pNode)
{
    UNUSED_PARAM (pNode);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetAdminPToP                                 */
/*                                                                           */
/* Description        : This function is used to configure the Admin Point To*/
/*                      Point in CVLAN component                             */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      u1AdminPToP -ForceTrue/ False/Auto                   */
/*                      bOperPToP - True/False                               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbCVlanSetCalculatedPtoP (UINT2 u2Port, UINT1 u1AdminPToP,
                             tAstBoolean bOperPToP)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1AdminPToP);
    UNUSED_PARAM (bOperPToP);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanFillBrgMacAddr                             */
/*                                                                           */
/* Description        : This function is responsible for filling up the CVLAN*/
/*                      comp Bridge Mac address. The CVLAN comp Bridge Mac   */
/*                      will be same as the CEP Port Mac Address             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pAstBridgeMacAddr - Mac Address to be filled         */
/*                                                                           */
/* Return Value(s)    : None .                                               */
/*****************************************************************************/
VOID
AstPbCVlanFillBrgMacAddr (tAstMacAddr pAstBridgeMacAddr)
{
    AST_MEMSET (pAstBridgeMacAddr, 0, sizeof (tAstMacAddr));

    return;
}

/*****************************************************************************/
/* Function Name      : RstUpdatePepOperPtoP                                 */
/*                                                                           */
/* Description        : This Function Updates the Oper PtoP status of PEP    */
/*                                                                           */
/* Input(s)           : u2PortNum - HL Port Id / Local Port Id of PEP        */
/*                      u1TrigType - AST_EXT_PORT_UP                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstUpdatePepOperPtoP (UINT2 u2PortNum, UINT1 u1TrigType)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (u1TrigType);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanRemovePort2IndexMapBasedOnProtPort         */
/*                                                                           */
/* Description        : This Function removes the port to index mapping from */
/*                      the RBTree and takes care of counters used for       */
/*                       generating index.                                   */
/*                                                                           */
/* Input(s)           : u2PortNum - Protocol Port Number                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanRemovePort2IndexMapBasedOnProtPort (UINT2 u2ProtocolPort)
{
    UNUSED_PARAM (u2ProtocolPort);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetPortPathCost                              */
/*                                                                           */
/* Description        : This function is used to configure the Path cost for */
/*                     CEP in a CVLAN component                              */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      u2Inst - Instance to which CEP belongs               */
/*                      u4PathCost - Configured PathCost                     */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetPortPathCostInCvlanComp (tAstPortEntry * pParentPortEntry,
                                 UINT4 u4PortPathCost)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (u4PortPathCost);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetAdminPToP                                 */
/*                                                                           */
/* Description        : This function is used to configure the Admin Point To*/
/*                      Point in CVLAN component                             */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      u1AdminPToP -ForceTrue/ False/Auto                   */
/*                      bOperPToP - True/False                               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbCVlanSetAdminOperPtoP (UINT2 u2Port, UINT1 u1AdminPToP,
                            tAstBoolean bOperPToP)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1AdminPToP);
    UNUSED_PARAM (bOperPToP);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCVlanSetAutoEdge                                  */
/*                                                                           */
/* Description        : This function is used to configure the Auto Edge     */
/*                      status in CVLAN component                            */
/*                                                                           */
/* Input(s)           : u2Port - CEP Port Number.                            */
/*                      bAutoEdge - True/False                               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstCVlanSetAutoEdge (UINT2 u2Port, tAstBoolean bAutoEdge)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (bAutoEdge);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSetPortAdminEdgeInCvlanComp                     */
/*                                                                           */
/* Description        : This function is used to configure the Admin Edge for*/
/*                      CEP in a CVLAN component.                            */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bStatus - Admin Edge status to be set.               */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/
INT4
AstPbSetPortAdminEdgeInCvlanComp (tAstPortEntry * pParentPortEntry,
                                  tAstBoolean bStatus)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (bStatus);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSetPseudoRootIdInCvlanComp                      */
/*                                                                           */
/* Description        : This function is used to configure PseudoRootId  for */
/*                     CEP in a CVLAN component                              */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      BrgMacAddr - PseudoRoot MacAddress                   */
/*                      u2BrgPriority - PseudoRoot Priority                  */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetPseudoRootIdInCvlanComp (tAstPortEntry * pParentPortEntry,
                                 tAstMacAddr BrgMacAddr, UINT2 u2BrgPriority)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (BrgMacAddr);
    UNUSED_PARAM (u2BrgPriority);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSetL2gpInCvlanComp                              */
/*                                                                           */
/* Description        : This function is used to configure L2gp for CEP in a */
/*                      CVLAN component                                      */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bIsL2Gp - L2gp Status of Port                        */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetL2gpInCvlanComp (tAstPortEntry * pParentPortEntry, tAstBoolean bIsL2gp)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (bIsL2gp);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSetBpduRxInCvlanComp                            */
/*                                                                           */
/* Description        : This function is used to configure BPDU receive      */
/*                      status for CEP in a CVLAN component                  */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bEnableBPDURx - BPDU Receive Status of Port          */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetBpduRxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDURx)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (bEnableBPDURx);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSetBpduTxInCvlanComp                            */
/*                                                                           */
/* Description        : This function is used to configure BPDU transmit     */
/*                      status for CEP in a CVLAN component                  */
/*                                                                           */
/* Input(s)           : pParentPortEntry - CEP Port entry in S-VLAN context. */
/*                      bEnableBPDUTx - BPDU Receive Status of Port          */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstPbSetBpduTxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDUTx)
{
    UNUSED_PARAM (pParentPortEntry);
    UNUSED_PARAM (bEnableBPDUTx);

    return RST_SUCCESS;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : RstpPbPrintModuleStatus                              */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Spanning tree                                    */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                      u4ContextId - Virtual Context Id                     */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
RstpPbPrintModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstCliPrintStpModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Global Spanning tree and SVLAN Spanning Tree     */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                      u4ContextId -Virtual Context Id                      */
/*                      i4GlobalModStatus - GlobalModuleStatus               */
/*                      i4Version - Protocol Version                         */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
RstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4GlobalModStatus, INT4 i4Version)
{

    if (i4GlobalModStatus == RST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nSpanning tree Protocol has been disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nSpanning tree Protocol Enabled. \r\n");

        if (i4Version == AST_VERSION_0)
        {
            CliPrintf (CliHandle,
                       "\r\nBridge is executing the stp compatible "
                       "Rapid Spanning Tree Protocol\r\n");
        }
        else if (i4Version == AST_VERSION_2)
        {
            CliPrintf (CliHandle,
                       "\r\nBridge is executing the rstp compatible "
                       "Rapid Spanning Tree Protocol\r\n");
        }
    }

    UNUSED_PARAM (u4ContextId);
    return CLI_SUCCESS;

}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : MstCliPrintStpModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Global Spanning tree and SVLAN Spanning Tree     */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                      u4ContextId -Virtual Context Id                      */
/*                      i4GlobalModStatus - GlobalModuleStatus               */
/*                      i4Version - Protocol Version                         */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
MstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4GlobalModStatus, INT4 i4Version)
{

    if (i4GlobalModStatus == RST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nSpanning tree Protocol has been disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nSpanning tree Protocol has been enabled\r\n");
        if (i4Version == AST_VERSION_0)
        {
            CliPrintf (CliHandle,
                       "\r\nMST00 is executing the stp compatible "
                       "Multiple Spanning Tree Protocol\r\n");
        }
        else if (i4Version == AST_VERSION_2)
        {
            CliPrintf (CliHandle,
                       "\r\nMST00 is executing the rstp compatible "
                       "Multiple Spanning Tree Protocol\r\n");
        }
        else if (i4Version == AST_VERSION_3)
        {
            CliPrintf (CliHandle,
                       "\r\nMST00 is executing the mstp compatible "
                       "Multiple Spanning Tree Protocol\r\n");
        }

    }
    UNUSED_PARAM (u4ContextId);
    return CLI_SUCCESS;
}
#endif /*MSTP_WANTED */
#endif /*CLI_WANTED */

/*****************************************************************************/
/* Function Name      : RstPbCvlanHdlSyncedChangeForPort                          */
/*                                                                           */
/* Description        : This function will be called whenever synced changes */
/*                      for a port. If synced becomes true for CEP, then:    */
/*                           - it means AllSynced is true for all PEPs.      */
/*                           - give AllSynced trigger to all PEPs.           */
/*                      If synced becomes true for a PEP, then :             */
/*                           - it checks synced variable for all other PEPs  */
/*                           - if synced is true for all PEPs, then AllSynced*/
/*                             trigger will be given to the corresponding    */
/*                             CEP.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port invoking this function.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS in case of success                       */
/*                      otherwise RST_FAILURE.                               */
/*****************************************************************************/
INT4
RstPbCvlanHdlSyncedChangeForPort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCvlanIsReRooted                                 */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmIsReRooted function, if the port invoking*/
/*                      RstProleTrSmIsReRooted function is a provider edge   */
/*                      port.                                                */
/*                      If rrwhile timer is zero for CEP, then this          */
/*                      function will return success other wise failure.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstPbCvlanIsReRooted (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCvlanIsTreeAllSynced                            */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstIsTreeAllSynced function, for C-VLAN component    */
/*                      ports.                                               */
/*                      If the port invoking this function is a CEP, then    */
/*                      it returns true if synced is set for all PEPs.       */
/*                      If the port invoking this function is a PEP, then    */
/*                      it returns true if synced is set for the CEP.        */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstIsTreeAllSynced function and         */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/ RST_FALSE.                                 */
/*****************************************************************************/
tAstBoolean
RstPbCvlanIsTreeAllSynced (UINT2 u2PortNum)
{
    UNUSED_PARAM (u2PortNum);
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : RstProvCheckAndAlterLogicalIfPortRole                */
/*                                                                           */
/* Description        : This function implements the enhancement to RSTP to  */
/*                      PB & PBB functionalities. This function will be      */
/*                      called if the a port (u2PortNum) is selected as an   */
/*                      alternate port. This function does the following:    */
/*                          - If the root port is PEP, and the current port  */
/*                            is also a PEP, then set the port role as root  */
/*                            for the current port.                          */
/*                                                                           */
/*                          - If the root port is VIP, and the current port  */
/*                            is also a VIP, then set the port role as root  */
/*                            for the current port.                          */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port whose port role is       */
/*                                   selected as Alternate port in the Update*/
/*                                   roles tree function.                    */
/*                      u2RootPort - Id of the port whose role is selected   */
/*                                   as root port in UpdateRoles tree        */
/*                                   function.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstProvCheckAndAlterLogicalIfPortRole (UINT2 u2PortNum, UINT2 u2RootPort)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (u2RootPort);
}

/*****************************************************************************/
/* Function Name      : RstProviderSetSyncTree                               */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmSetSyncTree function, when a VIP/PEP     */
/*                      invokes setSyncTree Procedure. This routine will set */
/*                      the value of sync as true for CEP if the calling port*/
/*                      is PEP & sync as true for all the CNPs if the calling*/
/*                      port is VIP.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstProleTrSmSetSyncTree function  and   */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetSyncTree (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProviderSetReRootTree                             */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmSetReRootTree function, when a VIP/PEP   */
/*                      invokes setReRootTreeProcedure. This routine will set*/
/*                      the value of reRoot as true for CEP if calling port  */
/*                      is PEP & sync as true for all the CNPs if the calling*/
/*                      port is VIP.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstProleTrSmSetReRootTree function and  */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetReRootTree (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProviderSetTcPropTree                             */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstTopoChSmSetTcPropTree function, when a VIP/PEP    */
/*                      invokes this function. This function sets tcProp for */
/*                      the all the CNPs in this I-Component if the calling  */
/*                      port is VIP and sets tcProp for CEP, if the calling  */
/*                      port is PEP.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstTopoChSmSetTcPropTree function  and  */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetTcPropTree (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstResetCepPortCounters                              */
/*                                                                           */
/* Description        : This function will be called by RstResetCounters     */
/*                      and RstResetPortCounters function when the port type */
/*                      is Customer Edge Port.This function will clear       */
/*                      all the counters associated with the Customer Edge   */
/*                      ports.                                               */
/*                                                                           */
/* Input(s)           : pCepPortInfo - CEP Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/

INT4
RstResetCepPortCounters (tAstPortEntry * pCepPortEntry)
{
    UNUSED_PARAM (pCepPortEntry);
    return RST_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    EXTERNAL APIS TO BE CALLED BY L2IWF FUNCTIONS FOR PROVIDER BRIDGE      */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : AstCreateProviderEdgePort                            */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      in context of VLAN to indicate the creation of a PEP.*/
/*                                                                           */
/* Input(s)           : u4IfIndex- CEP Port's Interface Index                */
/*                      tMstVlanId  - Service VLAN id for this PEP           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfCreateProviderEdgePort                          */
/*****************************************************************************/
INT4
AstCreateProviderEdgePort (UINT4 u4IfIndex, tMstVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteProviderEdgePort                            */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module in ctxt*/
/*                      of VLAN to indicate the deletion of a PEP.           */
/*                                                                           */
/* Input(s)           : u2PortNum - CEP Port's Interface Index               */
/*                      SVlanId   - Vlan Id indicating this PEP              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfDeleteProviderEdgePort                          */
/*****************************************************************************/
INT4
AstDeleteProviderEdgePort (UINT4 u4IfIndex, tMstVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbHandleInFrame                                   */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module in ctxt*/
/*                      of VLAN to handover the BPDUs to ASTP Task by posting*/
/*                      the BDPUs to the AST Queue and sending an event      */
/*                      to ASTP Task. This function is called when the BPDU  */
/*                      received on a PEP port                               */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the BPDU CRU Buffer             */
/*                      u2IfIndex - CEP Port's Interface Index               */
/*                      SVlanId  - VlanId denoting the PEP Port Num          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingFrameOnPEP                        */
/*****************************************************************************/
INT4
AstPbHandleInFrame (tAstBufChainHeader * pCruBuf, UINT2 u2IfIndex,
                    tMstVlanId SVlanId)
{
    UNUSED_PARAM (pCruBuf);
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (SVlanId);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPepOperStatusIndication                           */
/*                                                                           */
/* Description        : This function is called from the L2iwf Module to     */
/*                      indicate the operational status of the provider edge */
/*                      port status.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Global IfIndex of the CEP belonging  */
/*                                  to this C-VLAN component.                */
/*                      SVlanId   - Svid associated with the given PEP.      */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*                                                                           */
/*****************************************************************************/
INT4
AstPepOperStatusIndication (UINT4 u4IfIndex, tMstVlanId SVlanId, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1Status);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCheckAllPortTypes                               */
/*                                                                           */
/* Description        : This function handles the port type changes during   */
/*                      spanning tree initialization.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : RstModuleInit or MstModuleInit                       */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbCheckAllPortTypes (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCheckPortTypeChange                             */
/*                                                                           */
/* Description        : This function get the port type for the given port   */
/*                      from L2IWF. If there is any change in the port type  */
/*                      then it call AstPbHandlePortTypeChange to handle the */
/*                      port type.                                           */
/*                                                                           */
/* Input(s)           : u2Port - Local port id for the given port.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstCheckBrgPortTypeChange (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSelectCvlanContext                              */
/*                                                                           */
/* Description        : This Function is responsible for switching the context*/
/*                      from the SVLAN to the CVLAN. It also stores the SVLAN */
/*                      context in the gpAstParentCtxtInfo and then changes   */
/*                      the current context pointer to CVLAN context          */
/*                                                                           */
/* Input(s)           : pAstPortInfo- CEP Port Entry (index of CVLAN comp)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbSelectCvlanContext (tAstPortEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);
    return RST_FAILURE;
}

#ifdef MBSM_WANTED

/*****************************************************************************/
/* Function Name      : AstPbMbsmSetCvlanPortStatesInHw                      */
/*                                                                           */
/* Description        : This function programs the HW with the STP software  */
/*                      configuration.                                       */
/*                                                                           */
/*                      This function will be called from the STP module     */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  pInPortEntry -  PortEntry of the CEP belonging to   */
/*                                       the card that is getting inserted.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gpAstContextInfo                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstPbMbsmSetCvlanPortStatesInHw (tAstPortEntry * pInPortEntry,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pInPortEntry);
    UNUSED_PARAM (pSlotInfo);

    return;
}
#endif /* MBSM_WANTED */

/*****************************************************************************/
/* Function Name      : AstHandlePortTypeChange                              */
/*                                                                           */
/* Description        : This Function handles the Provider Bridge Port type  */
/*                      Changes for a Port.                                  */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstHandlePortTypeChange (tAstPortEntry * pPortEntry, UINT1 u1NewPortType)
{

    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (u1NewPortType);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbEnableCepPort                                   */
/*                                                                           */
/* Description        : This Function takes care of handling Oper status or  */
/*                      stp status for a CEP port.                           */
/*                                                                           */
/* Input(s)           : u2Port - Local port number of CEP port (in parent    */
/*                               context).                                   */
/*                      u2InstanceId - RST_DEFAULT_INSTANCE                  */
/*                      u1TrigType - AST_EXT_PORT_UP / AST_STP_PORT_UP       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstPbEnableCepPort (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1TrigType);
    return RST_SUCCESS;
}

/*  END OF EXTERNAL APIS TO BE CALLED BY L2IWF FUNCTIONS FOR PROVIDER BRIDGE  */
