/* $Id: fspbrsnc.c,v 1.1 2016/06/18 11:40:00 siva Exp $
   ISS Wrapper module
   module ARICENT-PB-RSTP-MIB

*/

# include  "lr.h"
# include  "fssnmp.h"
# include  "asthdrs.h"
#include   "fspbrsnc.h"


/********************************************************************
 * FUNCTION NcFsPbProviderStpStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbProviderStpStatusSet (
		INT4 i4FsPbProviderStpStatus )
{

	INT1 i1RetVal;
	AST_LOCK();
	i1RetVal = nmhSetFsPbProviderStpStatus(
			i4FsPbProviderStpStatus);
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbProviderStpStatusSet */

/********************************************************************
 * FUNCTION NcFsPbProviderStpStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbProviderStpStatusTest (UINT4 *pu4Error,
		INT4 i4FsPbProviderStpStatus )
{

	INT1 i1RetVal;
	AST_LOCK();

	i1RetVal = nmhTestv2FsPbProviderStpStatus(pu4Error,
			i4FsPbProviderStpStatus);

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbProviderStpStatusTest */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeIdGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeIdGet (
		INT4 i4FsPbRstPort,
		UINT1 *pau1FsPbRstCVlanBridgeId )
{
	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsPbRstCVlanBridgeId;

	MEMSET (&FsPbRstCVlanBridgeId, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsPbRstCVlanBridgeId.pu1_OctetList = pau1FsPbRstCVlanBridgeId;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeId(
			i4FsPbRstPort,
			&FsPbRstCVlanBridgeId);
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeIdGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeDesignatedRootGet (
		INT4 i4FsPbRstPort,
		UINT1 *pau1FsPbRstCVlanBridgeDesignatedRoot )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsPbRstCVlanBridgeDesignatedRoot;

	MEMSET (&FsPbRstCVlanBridgeDesignatedRoot, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsPbRstCVlanBridgeDesignatedRoot.pu1_OctetList = pau1FsPbRstCVlanBridgeDesignatedRoot;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeDesignatedRoot(
			i4FsPbRstPort,
			&FsPbRstCVlanBridgeDesignatedRoot);
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeDesignatedRootGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeRootCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeRootCostGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeRootCost )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeRootCost(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeRootCost );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeRootCostGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeMaxAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeMaxAgeGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeMaxAge )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeMaxAge(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeMaxAge );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeMaxAgeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeHelloTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeHelloTimeGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeHelloTime )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeHelloTime(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeHelloTime );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeHelloTimeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeHoldTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeHoldTimeGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeHoldTime )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeHoldTime(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeHoldTime );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeHoldTimeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeForwardDelayGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeForwardDelayGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeForwardDelay )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeForwardDelay(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeForwardDelay );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeForwardDelayGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanBridgeTxHoldCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanBridgeTxHoldCountGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanBridgeTxHoldCount )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanBridgeTxHoldCount(
			i4FsPbRstPort,
			pi4FsPbRstCVlanBridgeTxHoldCount );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanBridgeTxHoldCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanStpHelloTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanStpHelloTimeGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanStpHelloTime )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanStpHelloTime(
			i4FsPbRstPort,
			pi4FsPbRstCVlanStpHelloTime );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanStpHelloTimeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanStpMaxAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanStpMaxAgeGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanStpMaxAge )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanStpMaxAge(
			i4FsPbRstPort,
			pi4FsPbRstCVlanStpMaxAge );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanStpMaxAgeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanStpForwardDelayGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanStpForwardDelayGet (
		INT4 i4FsPbRstPort,
		INT4 *pi4FsPbRstCVlanStpForwardDelay )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanStpForwardDelay(
			i4FsPbRstPort,
			pi4FsPbRstCVlanStpForwardDelay );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanStpForwardDelayGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanStpTopChangesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanStpTopChangesGet (
		INT4 i4FsPbRstPort,
		UINT4 *pu4FsPbRstCVlanStpTopChanges )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanStpTopChanges(
			i4FsPbRstPort,
			pu4FsPbRstCVlanStpTopChanges );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanStpTopChangesGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanStpTimeSinceTopologyChangeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanStpTimeSinceTopologyChangeGet (
		INT4 i4FsPbRstPort,
		UINT4 *pu4FsPbRstCVlanStpTimeSinceTopologyChange )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanBridgeTable(
				i4FsPbRstPort) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanStpTimeSinceTopologyChange(
			i4FsPbRstPort,
			pu4FsPbRstCVlanStpTimeSinceTopologyChange );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanStpTimeSinceTopologyChangeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortPriorityGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortPriorityGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortPriority )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortPriority(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortPriority );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortPriorityGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortPathCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortPathCostGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortPathCost )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortPathCost(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortPathCost );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortPathCostGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortRoleGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortRoleGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortRole )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortRole(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortRole );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortRoleGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortState )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortState );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortAdminEdgePortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortAdminEdgePortGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortAdminEdgePort )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();	
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortAdminEdgePort(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortAdminEdgePort );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortAdminEdgePortGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortOperEdgePortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortOperEdgePortGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortOperEdgePort )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortOperEdgePort(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortOperEdgePort );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortOperEdgePortGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortAdminPointToPointGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortAdminPointToPointGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortAdminPointToPoint )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortAdminPointToPoint(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortAdminPointToPoint );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortAdminPointToPointGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortOperPointToPointGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortOperPointToPointGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortOperPointToPoint )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortOperPointToPoint(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortOperPointToPoint );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortOperPointToPointGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortAutoEdgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortAutoEdgeGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortAutoEdge )
{

	INT1 i1RetVal;
	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{	
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortAutoEdge(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortAutoEdge );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortAutoEdgeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortDesignatedRootGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT1 *pau1FsPbRstCVlanPortDesignatedRoot )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsPbRstCVlanPortDesignatedRoot;

	MEMSET (&FsPbRstCVlanPortDesignatedRoot, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsPbRstCVlanPortDesignatedRoot.pu1_OctetList = pau1FsPbRstCVlanPortDesignatedRoot;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{	
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortDesignatedRoot(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			&FsPbRstCVlanPortDesignatedRoot );
	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortDesignatedRootGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortDesignatedCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortDesignatedCostGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortDesignatedCost )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortDesignatedCost(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortDesignatedCost );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortDesignatedCostGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortDesignatedBridgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortDesignatedBridgeGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT1 *pau1FsPbRstCVlanPortDesignatedBridge )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsPbRstCVlanPortDesignatedBridge;

	MEMSET (&FsPbRstCVlanPortDesignatedBridge, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsPbRstCVlanPortDesignatedBridge.pu1_OctetList = pau1FsPbRstCVlanPortDesignatedBridge;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortDesignatedBridge(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			&FsPbRstCVlanPortDesignatedBridge );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortDesignatedBridgeGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortDesignatedPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortDesignatedPortGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT1 *pau1FsPbRstCVlanPortDesignatedPort )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	FsPbRstCVlanPortDesignatedPort;

	MEMSET (&FsPbRstCVlanPortDesignatedPort, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsPbRstCVlanPortDesignatedPort.pu1_OctetList = pau1FsPbRstCVlanPortDesignatedPort;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortDesignatedPort(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			&FsPbRstCVlanPortDesignatedPort );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortDesignatedPortGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortForwardTransitionsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortForwardTransitionsGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortForwardTransitions )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortForwardTransitions(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortForwardTransitions );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortForwardTransitionsGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortInfoSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortInfoSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortInfoSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortInfoSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortInfoSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortInfoSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortMigSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortMigSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortMigSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortMigSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortMigSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortMigSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortRoleTransSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortRoleTransSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortRoleTransSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortRoleTransSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortRoleTransSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortRoleTransSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortStateTransSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortStateTransSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortStateTransSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortStateTransSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortStateTransSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortStateTransSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortTopoChSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortTopoChSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortTopoChSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortTopoChSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortTopoChSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortTopoChSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortTxSmStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortTxSmStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortTxSmState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortSmTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortTxSmState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortTxSmState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortTxSmStateGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortRxRstBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortRxRstBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortRxRstBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortRxRstBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortRxRstBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortRxRstBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortRxConfigBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortRxConfigBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortRxConfigBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortRxConfigBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortRxConfigBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortRxConfigBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortRxTcnBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortRxTcnBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortRxTcnBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}
	i1RetVal = nmhGetFsPbRstCVlanPortRxTcnBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortRxTcnBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortRxTcnBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortTxRstBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortTxRstBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortTxRstBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortTxRstBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortTxRstBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortTxRstBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortTxConfigBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortTxConfigBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortTxConfigBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortTxConfigBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortTxConfigBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortTxConfigBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortTxTcnBpduCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortTxTcnBpduCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortTxTcnBpduCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortTxTcnBpduCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortTxTcnBpduCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortTxTcnBpduCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortInvalidRstBpduRxCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortInvalidRstBpduRxCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortInvalidRstBpduRxCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{

		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortInvalidRstBpduRxCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortInvalidRstBpduRxCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortInvalidRstBpduRxCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortInvalidConfigBpduRxCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortInvalidConfigBpduRxCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortInvalidConfigBpduRxCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortInvalidConfigBpduRxCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortInvalidConfigBpduRxCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortInvalidConfigBpduRxCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortInvalidTcnBpduRxCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortInvalidTcnBpduRxCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortInvalidTcnBpduRxCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortInvalidTcnBpduRxCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortInvalidTcnBpduRxCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortInvalidTcnBpduRxCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortProtocolMigrationCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortProtocolMigrationCountGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		UINT4 *pu4FsPbRstCVlanPortProtocolMigrationCount )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortProtocolMigrationCount(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pu4FsPbRstCVlanPortProtocolMigrationCount );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortProtocolMigrationCountGet */

/********************************************************************
 * FUNCTION NcFsPbRstCVlanPortEffectivePortStateGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsPbRstCVlanPortEffectivePortStateGet (
		INT4 i4FsPbRstPort,
		INT4 i4FsPbRstCepSvid,
		INT4 *pi4FsPbRstCVlanPortEffectivePortState )
{

	INT1 i1RetVal;

	AST_LOCK();
	if (nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable(
				i4FsPbRstPort,
				i4FsPbRstCepSvid) == SNMP_FAILURE)

	{
		AST_UNLOCK();
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetFsPbRstCVlanPortEffectivePortState(
			i4FsPbRstPort,
			i4FsPbRstCepSvid,
			pi4FsPbRstCVlanPortEffectivePortState );

	AST_UNLOCK();
	return i1RetVal;


} /* NcFsPbRstCVlanPortEffectivePortStateGet */

/* END i_ARICENT_PB_RSTP_MIB.c */
