/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsminpwr.c,v 1.12 2011/09/15 05:34:48 siva Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/

#include "asthdrs.h"

/******************************************************************************
 * FsMiStpNpHwInit 
 * 
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * None
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 ******************************************************************************/
INT1
FsMiStpNpHwInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FsStpNpHwInit ();
}

/*****************************************************************************
 * FsRstpNpInitHw 
 *
 * DESCRIPTION:
 * This function performs any necessary RSTP related initialisation in 
 * the Hardware
 *
 * INPUTS:
 * None
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/
VOID
FsMiRstpNpInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    FsRstpNpInitHw ();
    return;
}

/*****************************************************************************/
/* Function Name      : FsMiRstpNpSetPortState                               */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      MI-unaware hardware.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2PortNum - Port Number.                             */
/*                      u1Status  - Status to be set.                        */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsRstpNpSetPortState (u4IfIndex, u1Status);
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : FsMiRstpMbsmNpSetPortState                               */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      MI-unaware hardware.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2PortNum - Port Number.                             */
/*                      u1Status  - Status to be set.                        */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsRstpMbsmNpSetPortState (u4IfIndex, u1Status, pSlotInfo);
}

/*****************************************************************************/
/*  Function Name                  :FsMiRstpMbsmNpInitHw                      */
/*                                                                            */
/*  DESCRIPTION                    :This function performs any necessary      */
/*                                  RSTP related initialisation in the        */
/*                                  Hardware                                  */
/*                                                                            */
/*  INPUTS                         : pSlotInfo - Slot Information             */
/*                                 : u4ContextId - Context ID                 */
/*                                                                            */
/*  OUTPUTS                        : None                                     */
/*                                                                            */
/*  RETURNS                        : FNP_SUCCESS - On Success                 */
/*                                   FNP_FAILURE - On Failure                 */
/*****************************************************************************/
INT1
FsMiRstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsRstpMbsmNpInitHw (pSlotInfo));
}
#endif

/*****************************************************************************/
/* Function Name      : FsMiRstNpGetPortState                                */
/*                                                                           */
/* Description        : Gets the RSTP Port State in the Hardware.            */
/*                      When RSTP is working in RSTP mode                    */
/*                      or RSTP in STP compatible                            */
/*                      mode, porting should be done for this API.           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u4IfIndex - Port Number.                             */
/*                      pu1Status  - Status returned from Hardware.          */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/

INT1
FsMiRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsRstNpGetPortState (u4IfIndex, pu1Status);
}

#ifdef PB_WANTED
/*****************************************************************************/
/* Function Name      : FsMiPbRstpNpSetPortState                             */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      MI-unaware hardware.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2PortNum - CEP Port Number.                         */
/*                      SVid      - Service VLAN Identifier                  */
/*                      u1Status  - Status to be set.                        */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiPbRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVid,
                          UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsPbRstpNpSetPortState (u4IfIndex, SVid, u1Status);
}

/*****************************************************************************/
/* Function Name      : FsMiPbRstNpGetPortState                              */
/*                                                                           */
/* Description        : This function is used to get the PEP Port State.     */
/*                      PEP is Part of the CVLAN component.This NPAPI is used*/
/*                      only when the bridge is operation in provider bridge */
/*                      Mode.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u4IfIndex   - CEP Port IfIndex.                      */
/*                      SVlanId     - Service VLAN id                        */
/*                                                                           */
/* Output(s)          : pu1Status  - Status returned from Hardware.          */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful get (or)                 */
/*                      FNP_FAILURE - Error during getting                   */
/*****************************************************************************/
INT1
FsMiPbRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                         UINT1 *pu1Status)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4ContextId);

    i4RetVal = FsPbRstNpGetPortState (u4IfIndex, SVlanId, pu1Status);
    return i4RetVal;
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : FsMiPbRstpMbsmNpSetPortState                         */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      MI-unaware hardware.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2PortNum - CEP Port Number.                         */
/*                      SVid      - Service VLAN Identifier                  */
/*                      u1Status  - Status to be set.                        */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                      pSlotInfo - Info of the slot inserted.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiPbRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId Svid,
                              UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return FsPbRstpMbsmNpSetPortState (u4IfIndex, Svid, u1Status, pSlotInfo);
}
#endif
#endif /*PB_WANTED */
