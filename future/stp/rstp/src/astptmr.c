/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astptmr.c,v 1.78 2018/02/05 10:29:35 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Timer Module and other timer functionalities of
 *              RSTP and MSTP Modules.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstTimerInit                                         */
/*                                                                           */
/* Description        : This function is called by the initialisation module */
/*                      inorder to initialise all the timer related entities.*/
/*                      This function creates a Memory Pool for all the      */
/*                      module timers and also creates a Timer List.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.AstTmrMemPoolId,                      */
/*                      gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstTimerInit (VOID)
{

    /* Creating the Timer List */
    if (AST_CREATE_TMR_LIST (AST_TASK_NAME, AST_TMR_EXPIRY_EVENT, NULL,
                             &AST_TMR_LIST_ID) != AST_TMR_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                        AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                        "TMR: Timer List Creation FAILED!\n");

        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_TMR_DBG |
                        AST_ALL_FAILURE_DBG,
                        "TMR: Timer List Creation FAILED!\n");

        return RST_FAILURE;
    }

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                    AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                    AST_CONTROL_PATH_TRC,
                    "TMR: Timer List created successfully\n");

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                    AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                    AST_CONTROL_PATH_TRC,
                    "TMR: Timer Module Initialized successfully\n");

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstTimerDeInit                                       */
/*                                                                           */
/* Description        : This function is called by the shutdown module in    */
/*                      order to de-initialise all the timer related         */
/*                      entities.This function deletes the Memory Pool and   */
/*                      the Timer List. This routine is also called when any */
/*                      failure occurs during Timer Initialisation.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.AstTmrMemPoolId,                      */
/*                      gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstTimerDeInit (VOID)
{
    INT4                i4RetVal = RST_SUCCESS;

    /* Deleting the Timer List */
    if (AST_TMR_LIST_ID != AST_INIT_VAL)
    {
        if (AST_DELETE_TMR_LIST (AST_TMR_LIST_ID) != AST_TMR_SUCCESS)
        {
            AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                            AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                            "TMR: Timer List Deletion FAILED!\n");

            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_TMR_DBG |
                            AST_ALL_FAILURE_DBG,
                            "TMR: Timer List Deletion FAILED!\n");
            i4RetVal = RST_FAILURE;
        }
        AST_TMR_LIST_ID = AST_INIT_VAL;
    }

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                    AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                    AST_CONTROL_PATH_TRC,
                    "TMR: Timer Module De-Initialized successfully\n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstStartTimer                                        */
/*                                                                           */
/* Description        : This function is called whenever any timer needs to  */
/*                      be started by the Module(s). This function allocates */
/*                      a memory block for the timer node and then starts    */
/*                      the timer of the specified type for the specified    */
/*                      duration.                                            */
/*                                                                           */
/* Input(s)           : pPortPtr - A Void Pointer which may point to the Port */
/*                                Entry structure or to the Per Instance Port*/
/*                                Info structure.                            */
/*                      u2InstanceId - The Id of the spanning tree instance  */
/*                      u1TimerType - The type of the timer that is to be    */
/*                                    started                                */
/*                      u2Duration - The duration for which the timer needs  */
/*                                   to be started                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrMemPoolId                       */
/*                      gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstStartTimer (VOID *pPortPtr, UINT2 u2InstanceId,
               UINT1 u1TimerType, UINT2 u2Duration)
{
    tAstTimer          *pAstTimer = NULL;
    tAstTimer         **ppTimer = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstContextInfo    *pAstContextInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1SyncFlag = RST_FALSE;
    UINT1               u1TimerInfo;

    UINT1               aau1TimerName[21][20] = { " ",
        "FDWHILE", "HELLOWHEN", "MDELAYWHILE", "RBWHILE", "RCVDINFOWHILE",
        "RRWHILE", "TCWHILE", "HOLD", "EDGEDELAYWHILE", "RESTART",
        "RAPIDAGE DURATION",
        "PSEUDOINFOHELLOWHEN", "TCDETECTED", "FLUSHTGR", " ", " ",
        "ERROR_RECOVERY", "ROOT_INC_REC", "INST_CREATE"
    };

#ifdef MRP_WANTED
    tMrpInfo            MrpStpInfo;
    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));
#endif

    if (pPortPtr == NULL)
    {
        AST_DBG_ARG3 (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Duration %d: AstStartTimer for timer %s"
                      " called with Null Pointer\n", u2InstanceId, u2Duration,
                      aau1TimerName[u1TimerType]);
        return RST_FAILURE;
    }
    if (u2Duration == 0)
    {
        AST_DBG_ARG2 (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Trying to Start Timer %s for Zero"
                      " Duration!!\n", u2InstanceId,
                      aau1TimerName[u1TimerType]);
        return RST_FAILURE;
    }

    /* Allocate the timer node from the Timer Memory Pool */
    if (AST_ALLOC_TMR_MEM_BLOCK (pAstTimer) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG_ARG3 (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Duration %d: Timer Memory Block"
                      "Allocation for timer %s FAILED!\n",
                      u2InstanceId, u2Duration, aau1TimerName[u1TimerType]);
        return RST_FAILURE;
    }

    AST_MEMSET (pAstTimer, AST_INIT_VAL, sizeof (tAstTimer));

    pAstTimer->pEntry = pPortPtr;

    pAstTimer->pAstContext = AST_CURR_CONTEXT_INFO ();
    pAstTimer->u2InstanceId = u2InstanceId;
    pAstTimer->u1TimerType = u1TimerType;

    /* Store the pointer to the timer node in the corresponding timer pointer
     * and also validate the Timer Type */

    switch (u1TimerType)
    {

        case AST_TMR_TYPE_HELLOWHEN:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pHelloWhenTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running HELLOWHEN Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pHelloWhenTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u1SyncFlag = RST_TRUE;

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pRapidAgeDurtnTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running HELLOWHEN Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pRapidAgeDurtnTmr);

            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_FDWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running FDWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u2PortNum = pAstPortEntry->u2PortNo;
            u1SyncFlag = RST_TRUE;

            if (pAstCommPortInfo->pMdWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running MDELAYWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pMdWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_RBWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running RBWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;

            /* Syncing up stop and start Receive info while timer
             * will result in synching up one/two messages for every bpdu  
             * received. This will lead to perfomance issues in HA
             * environement. Hence we should avoid synching information during 
             * receive info while timer restart.
             * But when u1RcvdExpTimerCount becomes zero, we need to
             * synch up stop and start of receive info while tmr with
             * standby node, so that standby also makes the
             * u1RcvdExpTimerCount as zero. 
             * So, receive infor while timer sync up will happen in the
             * following scenarios:
             *        - When receive info while tiemr is started first time.
             *        - When receive info while timer is stopped (no start
             *        asap)
             *        - When receive info while timer expiry count becomes
             *        then stop and start to be synced up.*/

            if (pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr != NULL)
            {
                if (pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount != 0)
                {
                    if (AstStopTimer (pPortPtr,
                                      AST_TMR_TYPE_RCVDINFOWHILE) !=
                        RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Unable to stop the running RCVDINFOWHILE Timer!\n");
                        i4RetVal = RST_FAILURE;
                        break;
                    }
                    u1SyncFlag = RST_TRUE;
                }
                else
                {
                    if (AstStopTimer (pPortPtr,
                                      AST_TMR_TYPE_RCVDINFOWHILE_NO_SYNC) !=
                        RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Unable to stop the running RCVDINFOWHILE Timer!\n");
                        i4RetVal = RST_FAILURE;
                        break;
                    }
                }
            }
#ifdef L2RED_WANTED
            else
            {
                u1SyncFlag = RST_TRUE;
            }
#endif
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_RRWHILE:

            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running RRWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }

            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_TCWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running TCWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_HOLD:

            /* As per IEEE Std 802.1s Hold Timer will be running per port.
             * In feature if the Hold timer needs to be run per Instance then
             * Instance should be passed as it is.
             * */
            pAstTimer->u2InstanceId = RST_DEFAULT_INSTANCE;
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u2PortNum = pAstPortEntry->u2PortNo;

            if (pAstCommPortInfo->pHoldTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running HOLD Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pHoldTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u1SyncFlag = RST_TRUE;

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pEdgeDelayWhileTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, AST_TMR_TYPE_EDGEDELAYWHILE_NO_SYNC)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running EDGEDELAYWHILE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pEdgeDelayWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            /* During ISSU Maintenance Mode 
             * blocking the timer sync with standby node */
            if (IssuGetMaintModeOperation () != OSIX_TRUE)
            {
                u1SyncFlag = RST_TRUE;
            }

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pDisableRecoveryTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, AST_TMR_TYPE_ERROR_DISABLE_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ERROR DISABLE RECOVERY Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pDisableRecoveryTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);
            break;

        case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pPseudoInfoHelloWhenTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running PSEUDO HELLOWHEN Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pPseudoInfoHelloWhenTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          u2InstanceId, u2Duration);

            break;

#ifdef MRP_WANTED
        case AST_TMR_TYPE_TCDETECTED:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;

            if (pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running TCDETECTED "
                             "Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;
#endif
        case AST_TMR_TYPE_FLUSHTGR:
            pPerStInfo = (tAstPerStInfo *) pPortPtr;
            u2InstanceId = pPerStInfo->u2InstanceId;

            if (pPerStInfo->pFlushTgrTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running FLUSHTGR "
                             "Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStInfo->pFlushTgrTmr);
            AST_DBG_ARG3 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType], u2InstanceId, u2Duration);

            break;

        case AST_TMR_TYPE_RESTART:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;

            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            if (pAstContextInfo->pRestartTimer != NULL)
            {
                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Memory Block Release FAILED!\n");
                    return RST_FAILURE;
                }
                return RST_SUCCESS;

            }

            ppTimer = &(pAstContextInfo->pRestartTimer);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for duration %d centi-seconds\n",
                          aau1TimerName[u1TimerType], u2Duration);

            break;

        case AST_TMR_TYPE_ROOT_INC_RECOVERY:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;

            if (pPerStPortInfo->PerStRstPortInfo.pRootIncRecTmr != NULL)
            {
                /*stop the running timer first */
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running Root Inconsistenty "
                             "Recovery Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRootIncRecTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId, u2Duration);
            break;
        case AST_TMR_TYPE_INST_CREATE:
            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            ppTimer = &(pAstContextInfo->pInstCreate);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for duration %d centi-seconds\n",
                          aau1TimerName[u1TimerType], u2Duration);

            break;

        default:
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Invalid Timer Type\n");
            i4RetVal = RST_FAILURE;
            break;
    }

    if (i4RetVal == RST_FAILURE)
    {
        if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Memory Block Release FAILED!\n");
        }
        return i4RetVal;
    }

    if ((AST_NODE_STATUS () == RED_AST_ACTIVE)
        || (u1TimerType == AST_TMR_TYPE_INST_CREATE))
    {
        if (AST_START_TIMER (AST_TMR_LIST_ID, &(pAstTimer->AstAppTimer),
                             u2Duration) != AST_TMR_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Starting Timer FAILED!\n");
            i4RetVal = RST_FAILURE;

            if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Memory Block Release FAILED!\n");
                i4RetVal = RST_FAILURE;
            }
            return i4RetVal;
        }

        pAstTimer->u1IsTmrStarted = RST_TRUE;
#ifdef MRP_WANTED
        if (u1TimerType == AST_TMR_TYPE_TCDETECTED)
        {
            MrpStpInfo.u1Flag = MSG_TC_DETECTED_TMR_STATUS;
            MrpStpInfo.u4IfIndex =
                AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY (u2PortNum));
            MrpStpInfo.u2MapId = u2InstanceId;
            MrpStpInfo.b1TruthVal = OSIX_TRUE;

            AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
        }
#endif
        if (u1TimerType == AST_TMR_TYPE_TCWHILE)
        {
            pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
            if (pPerStInfo == NULL)
            {
                AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                              "TCSM: Port %s: Inst %d: No Such Instance Exist!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                return MST_FAILURE;
            }
            /* Topology change count incremented during timer start when tcWhile timer
             * is running for a particular port in the instance. */
            pPerStInfo->u4TcWhileCount++;
        }
    }
    else
    {
        pAstTimer->u1IsTmrStarted = RST_FALSE;
    }

    *ppTimer = pAstTimer;

    if ((AST_IS_RST_ENABLED ()) && (u1SyncFlag == RST_TRUE) &&
        (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        /*Send the indication to standby only if the port has not been deleted */
        if (gbIsPortDeleted == OSIX_FALSE)
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STARTED);
            u2InstanceId = RST_DEFAULT_INSTANCE;
            AstRedSendSyncMessages (RST_DEFAULT_INSTANCE, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, u2PortNum) = RST_FALSE;
        }
    }
#ifdef MSTP_WANTED
    else if ((AST_IS_MST_ENABLED ()) && (u1SyncFlag == RST_TRUE) &&
             (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        /*Send the indication to standby only if the port has not been deleted */
        if (gbIsPortDeleted == OSIX_FALSE)
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STARTED);
            AstRedSendSyncMessages (u2InstanceId, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_FALSE;
        }
    }
#endif
    if (u1TimerType == AST_TMR_TYPE_FLUSHTGR)
    {
        AST_DBG_ARG3 (AST_TMR_DBG,
                      "TMR: Inst %d: Started Timer %s for duration %u\n",
                      u2InstanceId,
                      aau1TimerName[u1TimerType],
                      (u2Duration / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    }
    else if (u1TimerType == AST_TMR_TYPE_RESTART)
    {
        AST_DBG_ARG2 (AST_TMR_DBG,
                      "TMR: Started Timer %s for duration %u centi-seconds\n",
                      aau1TimerName[u1TimerType], u2Duration);
    }
    else if (u1TimerType == AST_TMR_TYPE_INST_CREATE)
    {
        AST_DBG_ARG2 (AST_TMR_DBG,
                      "TMR: Started Timer %s for duration %u centi-seconds\n",
                      aau1TimerName[u1TimerType], u2Duration);
    }
    else
    {
        AST_DBG_ARG4 (AST_TMR_DBG,
                      "TMR: Port %s: Inst %d: Started Timer %s for duration %u\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId,
                      aau1TimerName[u1TimerType],
                      (u2Duration / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : AstStopTimer                                         */
/*                                                                           */
/* Description        : This function is called whenever any timer needs to  */
/*                      be stopped by the Module(s). This function stops the */
/*                      timer of the specified duration.                     */
/*                                                                           */
/* Input(s)           : pPortPtr - A Void Pointer which may point to the Port */
/*                                Entry structure or to the Per Instance Port*/
/*                                Info structure.                            */
/*                      u1TimerType - The type of the timer that is to be    */
/*                                    started                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrMemPoolId                       */
/*                      gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstStopTimer (VOID *pPortPtr, UINT1 u1TimerType)
{

    tAstTimer         **ppAstTimer = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstContextInfo    *pAstContextInfo = NULL;
    UINT1               u1TimerInfo;
    UINT2               u2PortNum = 0;
    UINT2               u1SyncFlag = RST_FALSE;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT1               aau1TimerName[20][20] = { " ",
        "FDWHILE", "HELLOWHEN", "MDELAYWHILE", "RBWHILE", "RCVDINFOWHILE",
        "RRWHILE", "TCWHILE", "HOLD", "EDGEDELAYWHILE", "RESTART",
        "RAPIDAGE_DURATION",
        "PSEUDOINFOHELLOWHEN", "TCDETECTED", "FLUSHTGR", " ", " ",
        "ERROR_RECOVERY", "ROOT_INC_REC", "INST_CREATE"
    };

#ifdef MRP_WANTED
    tMrpInfo            MrpStpInfo;
    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));
#endif

    if (pPortPtr == NULL)
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: AstStopTimer called with Null Pointer\n");
        return RST_FAILURE;
    }

    switch (u1TimerType)
    {

        case AST_TMR_TYPE_HELLOWHEN:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pHelloWhenTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pRapidAgeDurtnTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            u1SyncFlag = RST_TRUE;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_FDWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr);
            u2InstanceId = pPerStPortInfo->u2Inst;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pMdWhileTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            u1SyncFlag = RST_TRUE;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RBWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr);
            u2InstanceId = pPerStPortInfo->u2Inst;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = RST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
        case AST_TMR_TYPE_RCVDINFOWHILE_NO_SYNC:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u2InstanceId = pPerStPortInfo->u2Inst;
            pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount = 0;
            if (u1TimerType != AST_TMR_TYPE_RCVDINFOWHILE_NO_SYNC)
            {
                u1SyncFlag = RST_TRUE;
                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: Port %s: Stopping Timer %s \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              aau1TimerName[u1TimerType]);
            }
            break;

        case AST_TMR_TYPE_RRWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u2InstanceId = pPerStPortInfo->u2Inst;
            u1SyncFlag = RST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_TCWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u2InstanceId = pPerStPortInfo->u2Inst;
            u1SyncFlag = RST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_HOLD:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pHoldTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
        case AST_TMR_TYPE_EDGEDELAYWHILE_NO_SYNC:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pEdgeDelayWhileTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            if (u1TimerType != AST_TMR_TYPE_EDGEDELAYWHILE_NO_SYNC)
            {
                u1SyncFlag = RST_TRUE;
                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: Port %s: Stopping Timer %s \n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              aau1TimerName[u1TimerType]);
            }

            break;

        case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pDisableRecoveryTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);

            break;

        case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pPseudoInfoHelloWhenTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);

            break;
#ifdef MRP_WANTED
        case AST_TMR_TYPE_TCDETECTED:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u2InstanceId = pPerStPortInfo->u2Inst;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;
#endif
        case AST_TMR_TYPE_FLUSHTGR:
            pPerStInfo = (tAstPerStInfo *) pPortPtr;
            ppAstTimer = &(pPerStInfo->pFlushTgrTmr);
            u2InstanceId = pPerStInfo->u2InstanceId;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Instance %u: Stopping Timer %s \n",
                          u2InstanceId, aau1TimerName[u1TimerType]);
            break;
        case AST_TMR_TYPE_RESTART:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;

            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            ppAstTimer = &(pAstContextInfo->pRestartTimer);
            AST_DBG_ARG1 (AST_TMR_DBG,
                          "TMR: Stopping Timer %s \n",
                          aau1TimerName[u1TimerType]);
            break;
        case AST_TMR_TYPE_ROOT_INC_RECOVERY:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRootIncRecTmr);
            u2InstanceId = pPerStPortInfo->u2Inst;
            u2PortNum = pPerStPortInfo->u2PortNo;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_INST_CREATE:
            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            ppAstTimer = &(pAstContextInfo->pInstCreate);
            AST_DBG_ARG1 (AST_TMR_DBG,
                          "TMR: Stopping Timer %s \n",
                          aau1TimerName[u1TimerType]);
            break;

        default:
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Invalid Timer Type\n");
            return RST_FAILURE;
    }

    if (*ppAstTimer == NULL)
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Timer not running, cannot stop\n");
        return RST_FAILURE;
    }

    if ((AST_NODE_STATUS () == RED_AST_ACTIVE) ||
        (AST_NODE_STATUS () == RED_AST_FORCE_SWITCHOVER_INPROGRESS) ||
        (AST_NODE_STATUS () == RED_AST_SHUT_START_INPROGRESS))
    {
        if ((*ppAstTimer)->u1IsTmrStarted == RST_TRUE)
        {
            /* Stop the Timer */
            if (AST_STOP_TIMER (AST_TMR_LIST_ID, &((*ppAstTimer)->AstAppTimer))
                == AST_TMR_FAILURE)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Stopping Timer FAILED!\n");
                return RST_FAILURE;
            }
            (*ppAstTimer)->u1IsTmrStarted = RST_FALSE;
#ifdef MRP_WANTED
            if (u1TimerType == AST_TMR_TYPE_TCDETECTED)
            {

                MrpStpInfo.u1Flag = MSG_TC_DETECTED_TMR_STATUS;
                MrpStpInfo.u4IfIndex =
                    AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY (u2PortNum));
                MrpStpInfo.u2MapId = u2InstanceId;
                MrpStpInfo.b1TruthVal = OSIX_FALSE;

                AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
            }
#endif
            if (u1TimerType == AST_TMR_TYPE_TCWHILE)
            {
                pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
                if (pPerStInfo == NULL)
                {
                    AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "TCSM: Port %s: Inst %d: No Such Instance Exist!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstanceId);

                    return MST_FAILURE;
                }

                pPerStInfo->u4TcWhileCount--;
            }

        }
    }

    /* Free the Timer Memory Block to the Memory Pool */
    if (AST_RELEASE_TMR_MEM_BLOCK (*ppAstTimer) != AST_MEM_SUCCESS)
    {
        AST_DBG (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Memory Block Release FAILED!\n");
        return RST_FAILURE;
    }

    *ppAstTimer = NULL;

    if ((AST_IS_RST_ENABLED ()) && (u1SyncFlag == RST_TRUE) &&
        (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        /*Send the indication to standby only if the port has not been deleted */
        if (gbIsPortDeleted == OSIX_FALSE)
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STOPPED);
            AstRedSendSyncMessages (RST_DEFAULT_INSTANCE, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, u2PortNum) = RST_FALSE;
        }
    }
    else if ((AST_IS_MST_ENABLED ()) && (u1SyncFlag == RST_TRUE) &&
             (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        /*Send the indication to standby only if the port has not been deleted */
        if (gbIsPortDeleted == OSIX_FALSE)
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STOPPED);
            AstRedSendSyncMessages (u2InstanceId, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_FALSE;
        }
    }

    AST_DBG (AST_TMR_DBG, "TMR: Timer Stopped successfully\n");
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstSelectContextFromTmrInfo                          */
/*                                                                           */
/* Description        : This function selects the C-VLAN red context from    */
/*                      the C-VLAN context pointer.                          */
/*                                                                           */
/* Input(s)           : pCvlanContext - Pointer to C-VLAN context.           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstSelectContextFromTmrInfo (tAstContextInfo * pContext)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    UINT2               u2CepParentPortNo = 0;

    if (pContext->u2CompType == AST_PB_C_VLAN)
    {
        u2CepParentPortNo = pContext->u2CepPortNo;

        /* Select Parent context (S-VLAN context) */
        AstSelectContext (pContext->u4ContextId);

        /* Get the CEP port entry in S-VLAN context and
         * then select C-VLAN context.
         * By this Red C-VLAN context will also be selected. */
        pAstPortInfo = AST_GET_PORTENTRY (u2CepParentPortNo);

        if (AstPbSelectCvlanContext (pAstPortInfo) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstPbSelectCvlanContext function "
                     "returned FAILURE!\n");
            return;
        }

    }
    else
    {
        AstSelectContext (pContext->u4ContextId);
    }
}

/*****************************************************************************/
/* Function Name      : AstTmrExpiryHandler                                  */
/*                                                                           */
/* Description        : This function is called whenever any timer expires.  */
/*                      This extracts all the expired timers at any instant  */
/*                      of time and depending on the type of timer, it       */
/*                      performs the necessary processing.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstTmrExpiryHandler (VOID)
{
    tAstTimer          *pAstTimer = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;
    UINT1               u1TimerType = 0;

    AST_GLOBAL_TRC (AST_INVALID_CONTEXT, AST_CONTROL_PATH_TRC,
                    "TMR: Handling Timer Expiry event obtained ...\n");
    AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_TMR_DBG,
                    "TMR: Handling Timer Expiry event obtained ...\n");

    if ((!(AST_IS_INITIALISED ())) || (AST_TMR_LIST_ID == AST_INIT_VAL))
    {
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT, AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                        "TMR: Invalid Timer List\n");
        return RST_FAILURE;
    }

    while ((pAstTimer =
            (tAstTimer *) AST_GET_NEXT_EXPIRED_TMR (AST_TMR_LIST_ID)) != NULL)
    {
        u1TimerType = pAstTimer->u1TimerType;

        /* Select the ContextInfo encoded in the timer node as the
         * current context */
        AstSelectContextFromTmrInfo (pAstTimer->pAstContext);

        if (AST_IS_RST_ENABLED ())
        {
            if (RstTmrExpiryHandler (pAstTimer) == RST_FAILURE)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: RstTmrExpiryHandler function "
                         "returned FAILURE!\n");

                AST_DBG (AST_TMR_DBG | AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: RstTmrExpiryHandler function "
                         "returned FAILURE!\n");

                i4RetVal = RST_FAILURE;
            }
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            if (MstTmrExpiryHandler (pAstTimer) == MST_FAILURE)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: MstTmrExpiryHandler function "
                         "returned FAILURE!\n");

                AST_DBG (AST_TMR_DBG | AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: MstTmrExpiryHandler function "
                         "returned FAILURE!\n");
                i4RetVal = RST_FAILURE;
            }
        }
#endif
#ifdef PVRST_WANTED
        else if (AST_IS_PVRST_ENABLED ())
        {
            if (PvrstTmrExpiryHandler (pAstTimer) == PVRST_FAILURE)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: PvrstTmrExpiryHandler function "
                         "returned FAILURE!\n");

                AST_DBG (AST_TMR_DBG | AST_EVENT_HANDLING_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: PvrstTmrExpiryHandler function "
                         "returned FAILURE!\n");
                i4RetVal = RST_FAILURE;
            }
        }
#endif
        if (AST_COMP_TYPE () == AST_PB_C_VLAN)
        {
            /*In case of CVLAN context, restore the curr context
             * info to the parent context,so that in SI case the 
             * curr context information will be restored. In MI case
             * the curr context information will be taken cared in
             * release context*/
            AstPbRestoreContext ();
        }
        /*HITLESS RESTART */
        if ((AST_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) && (u1HRTmrFlag == 0)
            && (u1TimerType == AST_TMR_TYPE_HELLOWHEN))
        {
            /* Periodic timer has been made to expire after processing
             * steady state pkt request from RM. This flag indicates
             * to send steady tail msg after sending all the steady
             * state packets of STP to RM. */
            u1HRTmrFlag = 1;
        }
        AstReleaseContext ();
    }

    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        /* sending steady state tail msg to RM */
        AstRedHRSendStdyStTailMsg ();
        AST_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstTmrExpiryHandler                                  */
/*                                                                           */
/* Description        : This function is called whenever any timer expires.  */
/*                      This extracts all the expired timers at any instant  */
/*                      of time and depending on the type of timer, it       */
/*                      performs the necessary processing.                   */
/*                                                                           */
/* Input(s)           : pAstTimer - Timer node pointer                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstTmrExpiryHandler (tAstTimer * pAstTimer)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstContextInfo    *pAstContextInfo = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT2               u2InstanceId = AST_INIT_VAL;
    UINT1               u1TimerType = (UINT1) AST_INIT_VAL;
    VOID               *pEntryPtr = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    INT1                u1TimerInfo;
    UINT2               u2PortNum = 0;
    UINT2               u1SyncFlag = RST_FALSE;
    UINT2               u2Inst = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
#ifdef MRP_WANTED
    tMrpInfo            MrpStpInfo;
    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));
#endif

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    AST_DBG (AST_CONTROL_PATH_TRC,
             "TMR: Handling Timer Expiry event obtained ...\n");
    AST_DBG (AST_TMR_DBG, "TMR: Handling Timer Expiry event obtained ...\n");

    u2InstanceId = pAstTimer->u2InstanceId;
    u1TimerType = pAstTimer->u1TimerType;
    pEntryPtr = pAstTimer->pEntry;

    if (pEntryPtr != NULL)
    {
        /* 
         * Calling the respective State machines to handle 
         * the Timer expiry 
         */

        switch (u1TimerType)
        {
            case AST_TMR_TYPE_HELLOWHEN:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pHelloWhenTmr = NULL;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: HELLOWHEN Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);

                if (RstPortTransmitMachine
                    ((UINT2) RST_PTXSM_EV_HELLOWHEN_EXP, pAstPortEntry,
                     u2InstanceId) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Tx SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Tx SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_RAPIDAGE_DURATION:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pRapidAgeDurtnTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: RAPIDAGE DURATION Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);

                if (AST_FORCE_VERSION == AST_VERSION_0)
                {
                    AstVlanResetShortAgeoutTime (pAstPortEntry);
                }
                break;

            case AST_TMR_TYPE_FDWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = RST_TRUE;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: FDWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                if (RstPortRoleTrMachine
                    ((UINT2) RST_PROLETRSM_EV_FDWHILE_EXP,
                     pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }
                AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;

                break;

            case AST_TMR_TYPE_MDELAYWHILE:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pMdWhileTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_PMSM_DBG,
                              "TMR: Port %s: MDWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);

                if (RstPortMigrationMachine
                    ((UINT2) RST_PMIGSM_EV_MDELAYWHILE_EXP,
                     pAstPortEntry->u2PortNo) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Migration SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_PMSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Migration SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_RBWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: RBWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                if (RstPortRoleTrMachine
                    ((UINT2) RST_PROLETRSM_EV_RBWHILE_EXP,
                     pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM returned FAILURE!\n\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }
                AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;

                break;

            case AST_TMR_TYPE_RCVDINFOWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr = NULL;
                pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

#ifdef L2RED_WANTED

                pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount++;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = RST_TRUE;
                if (pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount >= 3)
                {

                    AST_DBG_ARG2 (AST_TMR_DBG | AST_PISM_DBG,
                                  "TMR: Port %s: RCVDINFOWHILE Timer EXPIRED for Instance: %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstanceId);

                    if (RstPortInfoMachine
                        ((UINT2) RST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                         pPerStPortInfo, NULL) != RST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        i4RetVal = RST_FAILURE;
                    }
                    pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount = 0;
                }
                else
                {
                    RstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo);
                }
#else
                if (RstPortInfoMachine
                    ((UINT2) RST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                     pPerStPortInfo, NULL) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Information SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Information SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

#endif
                break;

            case AST_TMR_TYPE_RRWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: RRWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                if (RstPortRoleTrMachine
                    ((UINT2) RST_PROLETRSM_EV_RRWHILE_EXP,
                     pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }
                AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;

                break;

            case AST_TMR_TYPE_TCWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr = NULL;
                u2Inst = pPerStPortInfo->u2Inst;
                pPerStInfo = AST_GET_PERST_INFO (u2Inst);
                if (pPerStInfo != NULL)
                {
                    /* Topology change count decremented during timer expiry when tcWhile timer
                     * is not running for a particular port in the instance. */
                    pPerStInfo->u4TcWhileCount--;
                }

                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR: Port %s: TCWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                break;

            case AST_TMR_TYPE_HOLD:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;

                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pHoldTmr = NULL;

                pAstBridgeEntry = AST_GET_BRGENTRY ();
                if (pAstCommPortInfo->u1TxCount > 0)
                {
                    (pAstCommPortInfo->u1TxCount)--;
                }

                AST_DBG_ARG3 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: HOLD Timer EXPIRED for Instance: %u, TxCount: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId, pAstCommPortInfo->u1TxCount);

                if (pAstCommPortInfo->u1TxCount ==
                    (pAstBridgeEntry->u1TxHoldCount - (UINT1) 1))
                {
                    if (RstPortTransmitMachine
                        ((UINT2) RST_PTXSM_EV_HOLDTMR_EXP, pAstPortEntry,
                         u2InstanceId) != RST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "TMR: Port Transmit SEM returned FAILURE!!!\n");
                        AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Transmit SEM returned FAILURE!!!\n");
                        i4RetVal = RST_FAILURE;
                    }
                }

                if (pAstCommPortInfo->u1TxCount > (UINT1) AST_INIT_VAL)
                {
                    if (pAstCommPortInfo->pHoldTmr == NULL)
                    {
                        if (AstStartTimer
                            ((VOID *) pAstPortEntry, u2InstanceId,
                             (UINT1) AST_TMR_TYPE_HOLD,
                             (UINT2) AST_HOLD_TIME) != RST_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                     "TMR: AstStartTimer routine returned FAILURE!!!\n");
                            i4RetVal = RST_FAILURE;
                        }
                    }
                }
                break;

            case AST_TMR_TYPE_EDGEDELAYWHILE:
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pEdgeDelayWhileTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %s: EDGEDELAYWHILE Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);
                RstBrgDetectionMachine ((UINT2)
                                        RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP,
                                        pAstPortEntry->u2PortNo);

                break;

            case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pDisableRecoveryTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %s: ERROR DISABLE RECOVERY"
                              "Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);
                /* During ISSU Maintenace Mode 
                 * restart the Error Disable recovery Timer */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                       u1TimerType,
                                       (UINT2) (pAstPortEntry->u4ErrorRecovery
                                                + ISSU_TIMER_VALUE))
                        != RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "RST_TMR: ISSU is in progress,"
                                 "Restarting AstStartTimer returned FAILURE!\n");
                        return RST_FAILURE;
                    }
                    AST_DBG (AST_TMR_DBG,
                             "RST_TMR: ISSU is in progress,"
                             "AstStartTimer Restarted!\n");
                    return RST_SUCCESS;
                }
                AstEnablePort (u2PortNum, RST_DEFAULT_INSTANCE,
                               AST_EXT_PORT_UP);
                /* BpduInconsitent state is reset when the port transtions 
                 * happens */
                if ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE) ||
                    ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE) &&
                     (AST_GBL_BPDUGUARD_STATUS == AST_TRUE)))
                {
                    if (pAstCommPortInfo->bBpduInconsistent == AST_TRUE)
                    {
                        pAstCommPortInfo->bBpduInconsistent = AST_FALSE;
                        AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                          "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                          AST_GET_IFINDEX_STR (pAstPortEntry->
                                                               u2PortNo),
                                          au1TimeStr);
                    }
                }

                break;

            case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pPseudoInfoHelloWhenTmr = NULL;

                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: PSEUDO HELLOWHEN Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pAstPortEntry->u2PortNo);

                if (RstPseudoInfoMachine
                    ((UINT2) RST_PSEUDO_INFO_EV_HELLO_EXPIRY,
                     pAstPortEntry->u2PortNo, NULL) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_PSSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port PSEUDOINFO SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

                break;
#ifdef MRP_WANTED
            case AST_TMR_TYPE_TCDETECTED:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR: Port %s: TCDETECTED Timer EXPIRED for "
                              "Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                MrpStpInfo.u1Flag = MSG_TC_DETECTED_TMR_STATUS;
                MrpStpInfo.u4IfIndex =
                    AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY (u2PortNum));
                MrpStpInfo.u2MapId = u2InstanceId;
                MrpStpInfo.b1TruthVal = OSIX_FALSE;

                AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
                break;
#endif

            case AST_TMR_TYPE_FLUSHTGR:

                pPerStInfo = (tAstPerStInfo *) pEntryPtr;
                pPerStInfo->pFlushTgrTmr = NULL;
                u2InstanceId = pPerStInfo->u2InstanceId;

                /* When the timer FlushInterval fires, then check the flag 
                 * PENDING FLUSHES.. Based on that take decisions as follows
                 *
                 * -- If (PENDING FLUSHES == TRUE) . 
                 * (a) Call the flushing call per instance . Flush (Instance)
                 *
                 * -- If (PENDING FLUSHES == FLASE) . 
                 * (a) No need for any action
                 */

                if (pPerStInfo->FlushFlag.u1PendingFlushes == AST_TRUE)
                {
                    AstFlushFdbOnInstance (u2InstanceId);
                    pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
                    (pPerStInfo->PerStBridgeInfo.u4TotalFlushCount)++;
                }
                pPerStInfo->PerStBridgeInfo.u4FlushIndCount = AST_INIT_VAL;

                AST_DBG_ARG1 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR:  FLUSHTGR Timer EXPIRED for "
                              "Instance: %u\n", u2InstanceId);
                break;

            case AST_TMR_TYPE_RESTART:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                if (NULL == pAstPortEntry)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "pAstPortEntry NULL \n");
                    return RST_FAILURE;
                }

                pAstContextInfo = AST_CURR_CONTEXT_INFO ();

                if (pAstContextInfo != NULL)
                {
                    pAstContextInfo->pRestartTimer = NULL;
                }
                else
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Invalid Context !!!\n");

                    return RST_FAILURE;
                }
                if (pAstPortEntry->u1RecScenario == LOOP_INC_RECOVERY)
                {
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                 u2InstanceId);
                    if (NULL == pPerStPortInfo)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "pPerStPortInfo NULL \n");
                        return RST_FAILURE;
                    }

                    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
                        && (pAstPortEntry->bLoopInconsistent == RST_TRUE)
                        && (pAstPortEntry->bLoopGuard == RST_TRUE))
                    {
                        NotifyProtoToApp.STPNotify.u4IfIndex =
                            pAstPortEntry->u4IfIndex;
                        AstCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                                      "SYS: Enabling Port %s ...\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                    }
                    pAstPortEntry->u1RecScenario = AST_INIT_VAL;
                }
                else
                {
                    if (AstRestartStateMachines () != RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: AstRestartStateMachines invocation failed!!!\n");
                        i4RetVal = RST_FAILURE;
                    }
                }
                AST_DBG (AST_TMR_DBG, "TMR:  Restart Timer EXPIRED\n");

                break;

            case AST_TMR_TYPE_ROOT_INC_RECOVERY:
                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRootIncRecTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: Root Inconistency Recovery Timer EXPIRED"
                              " for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                if (RstPortInfoSmMakeAged (pPerStPortInfo, NULL) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

                break;

            default:
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Invalid Timer Type\n");
                i4RetVal = RST_FAILURE;

        }                        /* End of switch */
        if (AST_IS_RST_ENABLED () && (u1SyncFlag == RST_TRUE)
            && (AST_NODE_STATUS () == RED_AST_ACTIVE))
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_EXPIRED);
            u2InstanceId = RST_DEFAULT_INSTANCE;
            AstRedSendSyncMessages (u2InstanceId, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, u2PortNum) = RST_FALSE;
            u1SyncFlag = RST_FALSE;
        }
    }
    else
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Corrupt entry pointer in timer node !!!\n");
        i4RetVal = RST_FAILURE;
    }

    pAstTimer->u1IsTmrStarted = RST_FALSE;
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Expired Timer Memory Block Release FAILED!\n");
            i4RetVal = RST_FAILURE;
        }
    }
    AST_DBG (AST_TMR_DBG, "TMR: Expired Timer processed \n");

    if (i4RetVal == RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC, "TMR: Expired Timer(s) processed \n");
        AST_DBG (AST_TMR_DBG,
                 "TMR: Timer Expiry event successfully processed ...\n");
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstStopAllRunningTimers                              */
/*                                                                           */
/* Description        : This function is called whenever any port is deleted */
/*                      or when the Module is disabled. This function will   */
/*                      stop all the running timers for this port.           */
/*                                                                           */
/* Input(s)           : pPerStInfo - Per Instance Information                */
/*                      pAstPortEntry - Per Port Information                 */
/*                      pPerStPortInfo - Per Instance Port information       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstStopAllRunningTimers (tAstPerStInfo * pPerStInfo,
                         tAstPortEntry * pAstPortEntry,
                         tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pCommPortInfo = &(pAstPortEntry->CommPortInfo);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (pCommPortInfo->pHelloWhenTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry, AST_TMR_TYPE_HELLOWHEN)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for HelloWhenTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pCommPortInfo->pRapidAgeDurtnTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pAstPortEntry,
             AST_TMR_TYPE_RAPIDAGE_DURATION) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RapidAgeDuration timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_FDWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for FdWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pCommPortInfo->pMdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry, AST_TMR_TYPE_MDELAYWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for MdWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRbWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RBWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RbWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RCVDINFOWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RcvdInfoTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RRWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RrWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pTcWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_TCWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for TcWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pCommPortInfo->pHoldTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry, AST_TMR_TYPE_HOLD)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for HoldTmr FAILED!\n");
            return RST_FAILURE;
        }
    }
    if (pCommPortInfo->pEdgeDelayWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry, AST_TMR_TYPE_EDGEDELAYWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for EdgeDelayWhile Timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pCommPortInfo->pPseudoInfoHelloWhenTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry,
                          AST_TMR_TYPE_PSEUDOINFOHELLOWHEN) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for PseudoInfoHelloWhen Timer FAILED!\n");
            return MST_FAILURE;
        }

    }

    if (pCommPortInfo->pDisableRecoveryTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry,
                          AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for disable recovery  Timer FAILED!\n");
            return RST_FAILURE;
        }

    }

#ifdef MRP_WANTED
    if (pPerStRstPortInfo->pTcDetectedTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_TCDETECTED)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for TcDetectedTmr FAILED!\n");
            return RST_FAILURE;
        }
    }
#endif

    if (pPerStRstPortInfo->pRootIncRecTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPerStPortInfo,
             AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for Root Inconsistency Recovery FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pPerStInfo != NULL)
    {
        if (pPerStInfo->pFlushTgrTmr != NULL)
        {
            if (AstStopTimer ((VOID *) pPerStInfo, AST_TMR_TYPE_FLUSHTGR)
                != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for FlushTimer FAILED!\n");
                return RST_FAILURE;
            }

        }
    }

    if (AST_CURR_CONTEXT_INFO ()->pRestartTimer != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry, AST_TMR_TYPE_RESTART)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for Restart Timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_TMR_DBG | AST_INIT_SHUT_DBG,
                  "TMR: Port %s: Stopped All Running Timers... \n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetRemainingTime                                  */
/*                                                                           */
/* Description        : This function is called whenever we need  to find    */
/*                      how much time is remaining for time out              */
/*                      by any module/function. The function returns the     */
/*                      remaining time to expire, for the specified timer.   */
/*                                                                           */
/* Input(s)           : *pAstEntry - Pointer to the port entry               */
/*                      u1TimerType - The type of timer in one particular    */
/*                                    session                                */
/*                      pu4RemainingTime - The remaining time, to expire in  */
/*                                         secs                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.TmrListId                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
UINT4
AstGetRemainingTime (VOID *pAstEntry,
                     UINT1 u1TimerType, UINT4 *pu4RemainingTime)
{
    tAstTmrListId       TmrListId;
    tAstTimer          *pAstTimer = NULL;
    if (pAstEntry == NULL)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 " TMR: AstGetRemainingTime called with EMPTY Port Entry\n");
        return RST_FAILURE;
    }

    TmrListId = gAstGlobalInfo.TmrListId;
    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:

            pAstTimer = ((tAstPerStRstPortInfo *) pAstEntry)->pFdWhileTmr;
            break;

        case AST_TMR_TYPE_HELLOWHEN:

            pAstTimer =
                ((tAstPortEntry *) pAstEntry)->CommPortInfo.pHelloWhenTmr;
            break;

        case AST_TMR_TYPE_MDELAYWHILE:

            pAstTimer = ((tAstPortEntry *) pAstEntry)->CommPortInfo.pMdWhileTmr;
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:

            pAstTimer = ((tAstPerStRstPortInfo *) pAstEntry)->pRcvdInfoTmr;
            break;

        case AST_TMR_TYPE_TCWHILE:

            pAstTimer = ((tAstPerStRstPortInfo *) pAstEntry)->pTcWhileTmr;
            break;

        case AST_TMR_TYPE_HOLD:

            pAstTimer = ((tAstPortEntry *) pAstEntry)->CommPortInfo.pHoldTmr;
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:

            pAstTimer =
                ((tAstPortEntry *) pAstEntry)->CommPortInfo.pEdgeDelayWhileTmr;
            break;

        case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:

            pAstTimer =
                ((tAstPortEntry *) pAstEntry)->CommPortInfo.
                pPseudoInfoHelloWhenTmr;
            break;

        case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:

            pAstTimer =
                ((tAstPortEntry *) pAstEntry)->CommPortInfo.pDisableRecoveryTmr;
            break;

#ifdef MRP_WANTED
        case AST_TMR_TYPE_TCDETECTED:

            pAstTimer = ((tAstPerStRstPortInfo *) pAstEntry)->pTcDetectedTmr;
            break;
#endif
        case AST_TMR_TYPE_FLUSHTGR:
            pAstTimer = ((tAstPerStInfo *) pAstEntry)->pFlushTgrTmr;
            break;
        default:
            AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                     " TMR: Improper Timer type to get remaining time\n");
            return RST_FAILURE;

    }                            /* switch on TimerType ends */

    if (pAstTimer == NULL)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 " TMR: Empty Timer Node, AstGetRemainingTimer failed \n");
        return RST_FAILURE;
    }

    if (AST_GET_REMAINING_TIME (TmrListId, &(pAstTimer->AstAppTimer),
                                pu4RemainingTime) == AST_TMR_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 " TMR: GetRemainingTime failed\n");
        return RST_FAILURE;
    }

    if (u1TimerType != AST_TMR_TYPE_FLUSHTGR)
    {
        *pu4RemainingTime =
            *pu4RemainingTime / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    }

    return RST_SUCCESS;

}                                /* AstGetRemainingTime ends */

/*****************************************************************************/
/* Function Name      : AstFlushFdbOnInstance                                */
/*                                                                           */
/* Description        : This routine invokes the flushes the entries         */
/*                      learnt on a particular instance.                     */
/*                                                                           */
/* Input(s)           : u2InstId - Instance ID                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstFlushFdbOnInstance (UINT2 u2InstId)
{
    UINT4               u4IfIndex = AST_INVALID_PORT_NUM;
    if (AST_IS_PVRST_STARTED ())
    {
        AstVlanDeleteVlanEntries (u2InstId, VLAN_OPTIMIZE);
    }
    else if (AST_IS_MST_STARTED () || AST_IS_RST_STARTED ())
    {
        AstVlanDeleteFdbEntries (u4IfIndex, VLAN_OPTIMIZE);
    }
    return;
}

/* End of file */
