/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: rstpbproc.c,v 1.37 2016/01/13 13:08:09 siva Exp $
 *
 * Description: This file contains Protocol routines for the 
 *              STP Provider Bridging Module.
 *
 *******************************************************************/
#include "asthdrs.h"

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : MstConfigureRestrictedRoleAndTCN                     */
/*                                                                           */
/* Description        : This Function configures values for restricted role  */
/*                      and restricted TCN for the Given port                */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                      u1NewRole  - MST_TRUE/MST_FALSE                      */
/*                      u1NewTCN   - MST_TRUE/MST_FALSE                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
MstConfigureRestrictedRoleAndTCN (tAstPortEntry * pPortEntry, UINT1 u1NewRole,
                                  UINT1 u1NewTCN)
{

    if (MstSetPortRestrictedRole (pPortEntry, u1NewRole) != MST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                      AST_ALL_FAILURE_DBG,
                      "MSG: MstSetRestrictedRole function returned FAILURE for "
                      "port : %s\n",
                      AST_GET_IFINDEX_STR (pPortEntry->u2PortNo));
        return RST_FAILURE;
    }

    /* Set restrictedTcn status now. */
    AST_PORT_RESTRICTED_TCN (pPortEntry) = u1NewTCN;

    return RST_SUCCESS;
}
#endif /*MSTP_WANTED */

/*****************************************************************************/
/* Function Name      : RstConfigureRestrictedRoleAndTCN                     */
/*                                                                           */
/* Description        : This Function configures values for restricted role  */
/*                      and restricted TCN for the Given port                */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                      u1NewRole  - RST_TRUE/RST_FALSE                      */
/*                      u1NewTCN   - RST_TRUE/RST_FALSE                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstConfigureRestrictedRoleAndTCN (tAstPortEntry * pPortEntry, UINT1 u1NewRole,
                                  UINT1 u1NewTCN)
{

    if (RstSetPortRestrictedRole (pPortEntry, (tAstBoolean) u1NewRole) !=
        RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                      AST_ALL_FAILURE_DBG,
                      "MSG: RstSetRestrictedRole function returned FAILURE for "
                      "port : %s\n",
                      AST_GET_IFINDEX_STR (pPortEntry->u2PortNo));
        return RST_FAILURE;
    }

    /* Set restrictedTcn status now. */
    AST_PORT_RESTRICTED_TCN (pPortEntry) = (tAstBoolean) u1NewTCN;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCVlanComponentInit                              */
/*                                                                           */
/* Description        : This Function initializes the CVLAN component and its*/
/*                      Ports. It is also responsible for triggering SEMs    */
/*                                                                           */
/* Input(s)           : pPortEntry - CEP Port Entry (index of CVLAN comp)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
RstPbCVlanComponentInit (tAstPortEntry * pParentPortInfo)
{
    tAstContextInfo    *pAstCVlanContext = NULL;
    tAstPerStPortInfo  *pParentPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pParentRstPortInfo = NULL;
    UINT4               u4CepIfIndex = 0;    /*If Index of CEP */
    UINT1               u1ContextModuleStatus = RST_ENABLED;

    u4CepIfIndex = pParentPortInfo->u4IfIndex;

    if (RstPbAllocCVlanMemblockForPB (pParentPortInfo) != AST_MEM_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Allocate CVLAN context information for"
                      "Port %u \n", (AST_IFENTRY_IFINDEX (pParentPortInfo)));

        RstPbDeAllocCVlanMemblockForPB (pParentPortInfo);

        /* No need to restore SVLAN ctxt since we have not switched to CVLAN
         * context*/

        return RST_FAILURE;
    }

    /* Get the global module status. */
#ifdef MSTP_WANTED
    if ((AST_IS_MST_STARTED ()) &&
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] != MST_ENABLED))
    {
        u1ContextModuleStatus = RST_DISABLED;
    }
#endif
    if ((AST_IS_RST_STARTED ()) &&
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] != RST_ENABLED))
    {
        u1ContextModuleStatus = RST_DISABLED;
    }

    pAstCVlanContext = AST_PB_PORT_CVLAN_CTXT (pParentPortInfo);

    /*Storing the Current Context Pointer in the CVLAN Information */
    pAstCVlanContext->pPbCVlanInfo->pParentContext = AST_CURR_CONTEXT_INFO ();

    /* Store the parent context's ContextId in the C-VLAN context.
     * This will be used to print the debug/trace messages. */
    pAstCVlanContext->u4ContextId = AST_CURR_CONTEXT_ID ();

    /*Initialising PB Port Info of the CEP Port Entry */
    AST_PB_PORT_INFO (pParentPortInfo)->u4CepIfIndex =
        pParentPortInfo->u4IfIndex;
    AST_PB_PORT_INFO (pParentPortInfo)->Svid = AST_PB_CEP_PROT_PORT_NUM;

    /*Storing the  AstPortInfo for Parent Port Entry */
    pParentPerStPortInfo = AST_GET_PERST_PORT_INFO (pParentPortInfo->u2PortNo,
                                                    RST_DEFAULT_INSTANCE);
    pParentRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pParentPortInfo->u2PortNo,
                                                      RST_DEFAULT_INSTANCE);

    /* Swapping from SVLAN context to CVLAN context and storing SVLAN context 
     * Ptr. Don't switch Red Context to Red C-VLAN context. Red C-VLAN context
     * will be created at the end of this function. */
    AstPbSelOnlyCvlanContext (pParentPortInfo);

#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    /* Form the context information to be printed during debug and trace
     * messages. */
    MEMSET (AST_CONTEXT_STR (), 0, AST_CONTEXT_STR_LEN);
    FORM_CVLAN_COMP_STR (u4CepIfIndex);
#endif

    /* Initializing CVLAN Context Variables */
    AST_BRIDGE_MODE () = AST_PROVIDER_EDGE_BRIDGE_MODE;
    AST_COMP_TYPE () = AST_PB_C_VLAN;

    /*Initialize the cep if Index in the CVLAN context information */
    AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = u4CepIfIndex;

    /* Fill the CEP local port number in the S-VLAN context also. */
    AST_CURR_CONTEXT_INFO ()->u2CepPortNo = pParentPortInfo->u2PortNo;

    AST_MAX_NUM_PORTS = AST_MAX_SERVICES_PER_CUSTOMER;

    /* Creating CVLAN context Specific Mempools */
    if (AstPbCvlanMemPoolInit () != AST_MEM_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Creation of Mempool for CVLAN specific information "
                 "(Port,PerStPort) failed. \n");

        AstPbCvlanMemPoolDeInit ();

        /* Don't Select Red C-VLAN context here, as it is not yet
         * created. So select only normal C-VLAN context. */
        AstPbRestoreOnlyCvlanContext ();

        RstPbDeAllocCVlanMemblockForPB (pParentPortInfo);

        return RST_FAILURE;
    }

    /*Allocating Port, PerSt Array Blocks from CVLAN context Mempool */
    if (AstPbAllocCVlanPortTblAndPerStInfo () != AST_MEM_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Allocation of CVLAN Port Table and PerSt Port Table"
                 "failed \n");

        AstPbDeAllocCVlanPortTblAndPerStInfo ();
        AstPbCvlanMemPoolDeInit ();

        AstPbRestoreOnlyCvlanContext ();
        RstPbDeAllocCVlanMemblockForPB (pParentPortInfo);

        return RST_FAILURE;
    }

    /* Now Initialize the CVLAN component ie) Create All PEPs in the CVLAN
     * context if present in the L2IWF Module and Copy the CEP port Information
     * present in the SVLAN context to the CVLAN Context.*/

    /* CEP port will be already disabled in the SVLAN context since the port
     * type change is allowed only after admin shut of the port*/

    if (RstComponentInit () != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Starting of CVLAN component failed \n");

        RstPbDeInitCVlanComponent ();

        return RST_FAILURE;
    }

    /* Initialize the memory required for this C-VLAN component
     * high availability feature. */
    if (AstRedPbCVlanInit (pParentPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Starting Red for CVLAN component failed \n");

        RstPbDeInitCVlanComponent ();

        return RST_FAILURE;
    }

    /* Now Create CEP port in the Port and PerStPort of CVLAN context */
    /* HL Port Id is always 1 for CEP in any CVLAN context --- ASSUMPTION
     * This Assumption is just made for Performance enhancement 
     * Since PerStInfo for the CVLAN context is created in the
     * RstComponentInit, RstPbCreateCvlanPort is called after that.
     * */
    if (RstPbCreateCvlanPort (u4CepIfIndex, AST_PB_CEP_PROT_PORT_NUM,
                              pParentPortInfo,
                              pParentPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Creation of CEP in the CVLAN context failed \n");

        RstPbDeInitCVlanComponent ();

        return RST_FAILURE;
    }

    /* Create PEP (if present in L2Iwf) in CVLAN component */
    if (AstPbCreateCVlanPorts () != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Creation of PEP from l2iwf in CVLAN context failed \n");

        RstPbDeInitCVlanComponent ();

        return RST_FAILURE;
    }

    /*Now Enable the CVLAN component ie) Trigger SEM for CEP and PEP */
    /* The CVLAN component can be only enabled when the CEP port spanning
     * tree status is enabled and the global module status is also enabled.*/
    if ((u1ContextModuleStatus == RST_ENABLED) &&
        (pParentRstPortInfo->bPortEnabled == RST_TRUE))
    {
        if (RstComponentEnable () != RST_SUCCESS)
        {
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: RSTP Enable failed \n");

            RstPbDeInitCVlanComponent ();

            return RST_FAILURE;
        }
    }

    /* Reverting to the SVLAN context */
    AstPbRestoreContext ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbDeInitCVlanComponent                            */
/*                                                                           */
/* Description        : This function is responsible for shutdown of CVLAN   */
/*                      context and deleting all its mem blocks and mempools */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPbDeInitCVlanComponent (VOID)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    UINT4               u4CepIfIndex = 0;

    /* Store the CEP ifIndex for later processing */
    u4CepIfIndex = AST_CURR_CONTEXT_INFO ()->u4CepIfIndex;

    if (RstComponentShutdown () != RST_SUCCESS)
    {
        AST_TRC (ALL_FAILURE_TRC | AST_INIT_SHUT_DBG,
                 "CVLAN component Shutdown Failed \r\n");

        return RST_FAILURE;
    }

    /* Remove the high availabitly related information for this
     * C-VLAN component. */
    AstRedPbCVlanPortTblDeInit ();

    /* Remove Dynamic Arrays allocated for PerStPortInfo, PerStInfo and 
     * PortInfo */
    AstPbDeAllocCVlanPortTblAndPerStInfo ();

    /* Delete all mempools dynamically created for CVLAN Dynamic Arrays for
     * PerStInfo, PortInfo and also delete RBTree for Port2Index map table*/
    AstPbCvlanMemPoolDeInit ();

    AST_PB_CVLAN_INFO ()->pParentContext = NULL;

    /*Get CEP Port Entry */
    pAstPortInfo = AstGetIfIndexEntry (u4CepIfIndex);

    if (pAstPortInfo == NULL)
    {
        AST_TRC (ALL_FAILURE_TRC | AST_INIT_SHUT_DBG, " MAJOR PROBLEM CVLAN"
                 "CONTEXT DELETION FAILED !!!!!!!!!!!!!! \n");

        return RST_FAILURE;
    }

    /* Switch to the parent context of CVLAN context since you cant delete a
     * context inside the same context*/
    AstPbRestoreContext ();

    /* Delete CVLAN ctxt info, CVLAN context and PbportInfo of CEP */
    RstPbDeAllocCVlanMemblockForPB (pAstPortInfo);

    /*Delete Red CVLAN context from the Red SVLAN context */
    AstRedPbRelCvlanRedCompMem (pAstPortInfo->u2PortNo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbCreateCvlanPort                                 */
/*                                                                           */
/* Description        : This function creates the port in the CVLAN component*/
/*                      Specifically CEP and PEP in CVLAN comp. It generates */
/*                      HlportId for all ports in the CVLAN comp and add it  */
/*                      to the global port table and per st port info table  */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP Port If Index                     */
/*                      u2ProtocolPort - for CEP it will be 0xfff and For PEP*/
/*                      it can range from  0-4094.                           */
/*                      pParentPortEntry - for CEP, this denotes SVLAN comp  */
/*                      CEP port Entry from where we will copy the port prop.*/
/*                      to CVLAN comp CEP port Entry. For PEP, it will be NULL*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPbCreateCvlanPort (UINT4 u4CepIfIndex, UINT2 u2ProtocolPort,
                      tAstPortEntry * pParentPortEntry,
                      tAstPerStPortInfo * pParentPerStPortInfo)
{
    tAstPortEntry      *pAstPortInfo = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /*First Generate the Index for the PEP using Protocol port */
    /*Index generation is not needed for CEP, so this check is added */
    if (AstPbGenerateCvlanLocalPort (u2ProtocolPort, &u2PortNum) != RST_SUCCESS)
    {

        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Generation of Local Port Id used for indexing Port"
                      "failed for PEP (CEP Ifinex:%d, Svid : %d) \n",
                      u4CepIfIndex, u2ProtocolPort);
        return RST_FAILURE;
    }

    /*Allocating PortInfo, PBPortInfo and PerStPortInfo for CVLAN Port */
    if (RstPbAllocPortInfoBlocks (u2PortNum) != AST_MEM_SUCCESS)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Allocation for Memory Failed for Port %u "
                      "Protocol Port %u\n", u4CepIfIndex, u2PortNum);

        AstPbCVlanRemovePort2IndexMapBasedOnProtPort (u2ProtocolPort);

        RstPbDeAllocPortInfoBlocks (AST_GET_PORTENTRY (u2PortNum), u2PortNum);

        return RST_FAILURE;
    }
    pAstPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* The u2PortNum should be filled by the index before it is getting
     * AstPerStPortInfo*/
    pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                 RST_DEFAULT_INSTANCE);

    /*Updating PbPort Information */
    AST_PB_PORT_INFO (pAstPortInfo)->u4CepIfIndex = u4CepIfIndex;

    /*Updating Port Information information */
    pAstPortInfo->u2ProtocolPort = u2ProtocolPort;
    pAstPortInfo->u2PortNo = u2PortNum;

    /*Adding the Port to the Context Port array */
    AST_GET_PORTENTRY (u2PortNum) = pAstPortInfo;

    /* Initialize the general fields of the port. */
    RstInitGlobalPortInfo (u2PortNum, pAstPortInfo);
    AST_PB_PORT_INFO (pAstPortInfo)->pPortCVlanContext =
        AST_CURR_CONTEXT_INFO ();

    /*Initializing the Per St Port Information of the Port */
    RstInitPerStPortInfo (u2PortNum, RST_DEFAULT_INSTANCE, pAstPerStPortInfo);
    pAstPerStPortInfo->u1PortPriority = AST_PB_CVLAN_PORT_PRIORITY;
    AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, u2PortNum) = RST_FALSE;

#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    MEMSET (pAstPortInfo->au1StrIfIndex, 0, AST_IFINDEX_STR_LEN);
    SNPRINTF ((CHR1 *) pAstPortInfo->au1StrIfIndex, AST_IFINDEX_STR_LEN,
              "(%u, %d)", u4CepIfIndex, u2ProtocolPort);
#endif

    if (u2ProtocolPort == AST_PB_CEP_PROT_PORT_NUM)
    {
        /* It is a customer edge port. */
        AST_GET_BRG_PORT_TYPE (pAstPortInfo) = AST_CUSTOMER_EDGE_PORT;

        /* Get port configured information (not related to any MSTI) from the 
         * parent port and copy them to this port info. */
        /* Get the IfIndex from parent entry and store it in this port entry.
         * This will be used to get some information about this physical port
         * from CFA and other related modules. */

        AST_IFENTRY_IFINDEX (pAstPortInfo) =
            AST_IFENTRY_IFINDEX (pParentPortEntry);

        pAstPortInfo->u4PhyPortIndex = pParentPortEntry->u4PhyPortIndex;

        pAstPortInfo->u1AdminPointToPoint =
            pParentPortEntry->u1AdminPointToPoint;
        pAstPortInfo->bAutoEdge = pParentPortEntry->bAutoEdge;

        /* C-VLAN context id is same as that of parent context id. 
         * So find out which spanning tree (RSTP/MSTP) is running
         * in the parent context. */
#ifdef MSTP_WANTED
        if (gau1AstSystemControl[AST_CURR_CONTEXT_ID ()] == MST_START)
        {
            /* Parent context is running Mstp. So take the port path
             * cost from ParentPerStPortInfo. */
            pAstPortInfo->u4PathCost = pParentPerStPortInfo->u4PortPathCost;
        }
        else
#endif /*MSTP_WANTED */
        {
            /* Parent context is running Rstp. So take the port path
             * cost from ParentPortInfo. */
            pAstPortInfo->u4PathCost = pParentPortEntry->u4PathCost;
        }
        pAstPortInfo->u1EntryStatus = pParentPortEntry->u1EntryStatus;

        /* If port path cost is configured for parent port entry, the
         * same value will be used of CEP in C-VLAN comp. */
        pAstPerStPortInfo->u4PortAdminPathCost =
            pParentPerStPortInfo->u4PortAdminPathCost;

        /* Copy the Admin Edge status from the parent port. */
        pAstPortInfo->bAdminEdgePort = pParentPortEntry->bAdminEdgePort;

        /* The value of Oper Point-To-Point will be updated after triggering 
         * LA and getting the status */
        AST_UPDATE_OPER_P2P (u2PortNum, pAstPortInfo->u1AdminPointToPoint);
        AST_PB_PORT_INFO (pAstPortInfo)->Svid = 0;

        /* Module status depends on the spanning tree status of the
         * corresponding customer edge port. Since the CVLAN component
         * Enabled is in initialisation thread, it is taken care there.
         * By Now the CVLAN module status is disabled.*/
        AST_MODULE_STATUS_SET (RST_DISABLED);

    }
    else
    {
        AST_GET_BRG_PORT_TYPE (pAstPortInfo) = AST_PROVIDER_EDGE_PORT;
        AST_PB_PORT_INFO (pAstPortInfo)->Svid = u2ProtocolPort;

        pAstPortInfo->u1AdminPointToPoint = (UINT1) RST_P2P_AUTO;
        pAstPortInfo->bOperPointToPoint = RST_FALSE;
        pAstPortInfo->u1EntryStatus = CFA_IF_DOWN;

        /* Admin Edge and OperEdge are already set to false in 
         * RstInitGlobalPortInfo function. So no need to that here. */
        pAstPortInfo->bAutoEdge = RST_TRUE;
        pAstPortInfo->u4PathCost = AST_PB_PEP_PORT_PATH_COST;

        /* S PortAdminPathCost as PathCost, other wise in RstEnablePort, 
         * port cost will be calculated for this port too. */
        pAstPerStPortInfo->u4PortAdminPathCost = AST_PB_PEP_PORT_PATH_COST;

        /* Module can be already enabled, before this PEP is created. */
        if (AST_MODULE_STATUS == RST_ENABLED)
        {
            if (RstStartSemsForPort (u2PortNum) == RST_FAILURE)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "CreatePort: Port %s: StartSems returned failure !\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AstPbCVlanRemovePort2IndexMapBasedOnProtPort (u2ProtocolPort);

                RstPbDeAllocPortInfoBlocks (pAstPortInfo, u2PortNum);

                return RST_FAILURE;
            }
        }
    }

    if (AstRedPbCreateCvlanPort (u2PortNum) == RST_FAILURE)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "CreatePort: Red Create C-VLAN Port %s: "
                      "failure!\n", AST_GET_IFINDEX_STR (u2PortNum));
        AstPbCVlanRemovePort2IndexMapBasedOnProtPort (u2ProtocolPort);

        RstPbDeAllocPortInfoBlocks (pAstPortInfo, u2PortNum);

        return RST_FAILURE;
    }

    AstDeriveMacAddrFromPortType (u2PortNum);

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: Created Successfully...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s: Created Successfully...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPbDeleteCvlanPort                                 */
/*                                                                           */
/* Description        : This function deletes the port in the CVLAN component*/
/*                      Specifically CEP and PEP in CVLAN comp. It generates */
/*                      HlportId for all ports in the CVLAN comp and add it  */
/*                      to the global port table and per st port info table  */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP Port If Index                     */
/*                      u2ProtocolPort - for CEP it will be 0xfff and For PEP*/
/*                      it can range from  0-4094.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPbDeleteCvlanPort (UINT4 u4CepIfIndex, UINT2 u2PortNum, UINT2 u2ProtocolPort,
                      tAstPortEntry * pParentPortEntry)
{
    tAstPortEntry      *pAstPortInfo = NULL;

    UNUSED_PARAM (pParentPortEntry);

    pAstPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortInfo == NULL)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                      "SYS: Port (%u, %d) does not exist...\n",
                      u4CepIfIndex, u2ProtocolPort);
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                      "SYS: Port (%u, %d) does not exist...\n",
                      u4CepIfIndex, u2ProtocolPort);
        return RST_FAILURE;
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG, "SYS: Deleting Port (%u, %u)...\n",
                  u4CepIfIndex, u2ProtocolPort);
    AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Deleting Port (%u, %u)...\n",
                  u4CepIfIndex, u2ProtocolPort);

    AstPbCVlanRemovePort2IndexMapBasedOnProtPort (u2ProtocolPort);

    if (RstComponentDeletePort (u2PortNum) != RST_SUCCESS)
    {

        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                      "SYS: RstDeletePort Failed for Port (%u, %u) \n",
                      u4CepIfIndex, u2ProtocolPort);

        return RST_FAILURE;
    }

    /* Delete the Red info for this C-VLAN component port. */
    AstRedPbDeleteCvlanPort (u2PortNum);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbSelOnlyCvlanContext                             */
/*                                                                           */
/* Description        : This Function is responsible for switching the       */
/*                      context from the SVLAN to the CVLAN. It also stores  */
/*                      the SVLAN context in the gpAstParentCtxtInfo and     */
/*                      then changes the current context pointer to CVLAN    */
/*                      context.                                             */
/*                      Note: This does not switch Red C-VLAN context.       */
/*                                                                           */
/* Input(s)           : pAstPortInfo- CEP Port Entry (index of CVLAN comp)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbSelOnlyCvlanContext (tAstPortEntry * pPortEntry)
{
    if (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        if ((pPortEntry->pPbPortInfo != NULL)
            && (AST_PB_PORT_CVLAN_CTXT (pPortEntry) != NULL))
        {
            /* Storing SVLAN context in Parent Ctxt */
            AST_PREV_CONTEXT_INFO () = AST_CURR_CONTEXT_INFO ();
            /*Storing CVLAN context as current Ctxt */
            AST_CURR_CONTEXT_INFO () = AST_PB_PORT_CVLAN_CTXT (pPortEntry);
            return RST_SUCCESS;
        }
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbRestoreOnlyCvlanContext                         */
/*                                                                           */
/* Description        : This Function is responsible for restoring the context*/
/*                      pointer which is already stored in gpAstParentCtxtInfo*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
AstPbRestoreOnlyCvlanContext (VOID)
{
    /* WARNING : This function should be called in CVLAN context only.
       It should be used in CVLAN component of Provider Edge Bridge */
    AST_CURR_CONTEXT_INFO () = AST_PREV_CONTEXT_INFO ();

    AST_PREV_CONTEXT_INFO () = NULL;

    return;
}

/*****************************************************************************/
/* Function Name      : AstPbCreateCVlanPorts                                */
/*                                                                           */
/* Description        : This Function is called when CVLAN component is      */
/*                      initialized. This function creates all the PEP in the*/
/*                      CVLAN component if and only if it is present in l2iwf*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbCreateCVlanPorts (VOID)
{

    UINT4               u4CepIfIndex;
    UINT4               u4NextCepIfIndex;
    UINT2               u2PrevProtoPort = 0;
    UINT2               u2ProtocolPort = 0;

    u4CepIfIndex = (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex;

    /* Already CEP is created in the C-VLAN component. Hence just get the PEPs
     * and fill the table. */
    while (AstL2IwfGetNextCVlanPort (u4CepIfIndex, u2PrevProtoPort,
                                     &u2ProtocolPort, &u4NextCepIfIndex) ==
           L2IWF_SUCCESS)
    {
        /* L2IWF maintains single RBTree For all PEPs. So There is a
         * Possibility that the L2IWF function can return the Next CEP's
         * PEP. To avoid this check is needed*/
        if (u4CepIfIndex != u4NextCepIfIndex)
        {
            break;
        }

        if (RstPbCreateCvlanPort (u4CepIfIndex, u2ProtocolPort, NULL, NULL)
            != RST_SUCCESS)
        {
            return RST_FAILURE;
        }

        u2PrevProtoPort = u2ProtocolPort;
        u4CepIfIndex = u4NextCepIfIndex;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbGenerateCvlanLocalPort                          */
/*                                                                           */
/* Description        : This function is an algorithm for generating the  HL */
/*                      Port Number for each protocol port created. It also  */
/*                      adds an entry in port2index map table                */
/*                                                                           */
/* Input(s)           : u2ProtocolPort - Protocol Port Number                */
/*                                                                           */
/* Output             : *pu2HLPortId - Generated HL Port Id                  */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbGenerateCvlanLocalPort (UINT2 u2ProtocolPort, UINT2 *pu2HLPortId)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2PortNum = 0;

    *pu2HLPortId = 0;

    /* Now generate u2PortNum from u2ProtocolPort. */
    /* Check whether there are free entries after the highest port 
     * index sofar allocated. */
    if ((AST_PB_CVLAN_INFO ())->u2HighPortIndex < AST_MAX_NUM_PORTS)
    {
        (AST_PB_CVLAN_INFO ())->u2HighPortIndex++;
        (AST_PB_CVLAN_INFO ())->u2NumPorts++;
        u2PortNum = (AST_PB_CVLAN_INFO ())->u2HighPortIndex;
    }
    else
    {
        if ((AST_PB_CVLAN_INFO ())->u2NumPorts < AST_MAX_NUM_PORTS)
        {
            /* There are some free entries in the port table. So
             * use one of those free entries. */
            u2PortNum = 1;
            AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
            {
                if (pPortEntry == NULL)
                {
                    (AST_PB_CVLAN_INFO ())->u2NumPorts++;
                    break;
                }
            }

            if (u2PortNum == (AST_MAX_NUM_PORTS + 1))
            {
                /* This situation should not araise. */
                return RST_FAILURE;
            }

        }
        else
        {
            /* There is no space in the existing table. So allocate a
             * new table bigger than the present one and copy the info
             * from the existing port table to the newer one. Once the 
             * copying is done, release the old table.*/
            if (AstPbIncrCvlanTablesSize () != RST_SUCCESS)
            {
                return RST_FAILURE;

            }
            (AST_PB_CVLAN_INFO ())->u2HighPortIndex++;
            (AST_PB_CVLAN_INFO ())->u2NumPorts++;
            u2PortNum = (AST_PB_CVLAN_INFO ())->u2HighPortIndex;
        }
    }

    if (AstPbCVlanAddPort2IndexMap (u2PortNum, u2ProtocolPort) != RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG |
                      AST_ALL_FAILURE_DBG, " Entry addition failed in the "
                      "Port to Index Map Table for Port (CEP IfIndex :%d, "
                      "Svid : %d \n",
                      (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex, u2ProtocolPort);

        AstPbCVlanRemovePort2IndexMapBasedOnProtPort (u2ProtocolPort);

        return RST_FAILURE;
    }

    /*Returning HL port Id for PEP */
    *pu2HLPortId = u2PortNum;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanAddPort2IndexMap                           */
/*                                                                           */
/* Description        : This function is used to add an port 2index map entry*/
/*                      in port 2 index map table.                           */
/*                                                                           */
/* Input(s)           : u2ProtocolPort - Protocol Port Number                */
/*                      u2PortNum      - HL Port Number                      */
/*                                                                           */
/* Output             : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstPbCVlanAddPort2IndexMap (UINT2 u2PortNum, UINT2 u2ProtocolPort)
{
    tAstPort2IndexMap  *pPort2IndexEntry = NULL;

    if (AST_PB_ALLOC_PORT2INDEX_MAP_BLOCK (pPort2IndexEntry) == NULL)
    {
        AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG |
                      AST_ALL_FAILURE_DBG, "Allocation of Port2Index"
                      "Entry Block failed for (Logical Port %u, Protocol "
                      "port %u) \n", u2PortNum, u2ProtocolPort);

        return RST_FAILURE;
    }

    pPort2IndexEntry->u2ProtPort = u2ProtocolPort;
    pPort2IndexEntry->u2PortIndex = u2PortNum;

    if (RBTreeAdd (AST_PB_CVLAN_PORT2INDEX_TREE (),
                   pPort2IndexEntry) != RB_SUCCESS)
    {
        AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC, "RB Tree Addition in Port"
                      "to Index mapping tree failed for (Local Port %u, "
                      "Protocol Port %u) failed \n", u2PortNum, u2ProtocolPort);
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCVlanGetPort2IndexMapBasedOnProtoPort           */
/*                                                                           */
/* Description        : This Function gets the port to index mapping from    */
/*                      the RBTree based on the Protocol Port Id             */
/*                                                                           */
/* Input(s)           : u2ProtocolPort - Protocol Port Number                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port2IndexMap Entry                                  */
/*****************************************************************************/

tAstPort2IndexMap  *
AstPbCVlanGetPort2IndexMapBasedOnProtoPort (UINT2 u2ProtocolPort)
{

    tAstPort2IndexMap   TempPort2IndexEntry;
    tAstPort2IndexMap  *pPort2IndexEntry = NULL;

    AST_MEMSET (&TempPort2IndexEntry, AST_INIT_VAL, sizeof (tAstPort2IndexMap));

    TempPort2IndexEntry.u2ProtPort = u2ProtocolPort;

    pPort2IndexEntry = RBTreeGet (AST_PB_CVLAN_PORT2INDEX_TREE (),
                                  (tRBElem *) & TempPort2IndexEntry);

    return pPort2IndexEntry;
}

/*****************************************************************************/
/* Function Name      : RstPbAllocPortInfoBlocks                             */
/*                                                                           */
/* Description        : This Function Allocates the Port Entry from the global*/
/*                      mempool, PB Port Entry from Global PB Mempool and    */
/*                      allocates perst port info from Global Mempool        */
/*                                                                           */
/* Input(s)           : u2LocalPort - HLPortId                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS/AST_MEM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
RstPbAllocPortInfoBlocks (UINT2 u2LocalPort)
{
    tAstPortEntry      *pAstPortInfo = NULL;

    /*Allocation Port Entry for CVLAN Port */
    if (AST_ALLOC_PORT_INFO_MEM_BLOCK (pAstPortInfo) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Allocation of Port Information Failed\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET (pAstPortInfo, AST_INIT_VAL, sizeof (tAstPortEntry));

    AST_GET_PORTENTRY (u2LocalPort) = pAstPortInfo;

    /*Allocating PB port Entry for CVLAN Port */
    if (AST_PB_ALLOC_PB_PORT_INFO_MEM_BLOCK (AST_PB_PORT_INFO (pAstPortInfo))
        == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Allocation of PB Port Information Failed\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET ((AST_PB_PORT_INFO (pAstPortInfo)), AST_INIT_VAL,
                sizeof (tAstPbPortInfo));

    /* Now allocate PerStPortInfo for this port. */
    if (AST_ALLOC_PERST_PORT_INFO_MEM_BLOCK
        ((AST_GET_PERST_PORT_INFO (u2LocalPort, RST_DEFAULT_INSTANCE))) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Allocation of Per Spanning Tree Port Information "
                 "Failed\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET (AST_GET_PERST_PORT_INFO (u2LocalPort, RST_DEFAULT_INSTANCE),
                AST_INIT_VAL, sizeof (tAstPerStPortInfo));

    /*Allocation BPDU Entry for Port */

    return ((INT4) AST_MEM_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : RstPbDeAllocPortInfoBlocks                           */
/*                                                                           */
/* Description        : This Function releases the Port Entry from the global*/
/*                      mempool, PB Port Entry from Global PB Mempool and    */
/*                      releases perst port info from Global Mempool        */
/*                                                                           */
/* Input(s)           : u2LocalPort - HLPortId                                */
/*                      pAstPortInfo - Port Pointer to be released           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS/AST_MEM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/

INT4
RstPbDeAllocPortInfoBlocks (tAstPortEntry * pAstPortInfo, UINT2 u2LocalPort)
{
    INT4                i4RetVal = (INT4) AST_MEM_SUCCESS;

    if (pAstPortInfo == NULL)
    {
        return i4RetVal;
    }

    /* Now Release PerStPortInfo for this port. */
    if (AST_GET_PERST_PORT_INFO (u2LocalPort, RST_DEFAULT_INSTANCE) != NULL)
    {
        if (AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK
            (AST_GET_PERST_PORT_INFO (u2LocalPort, RST_DEFAULT_INSTANCE)) !=
            AST_MEM_SUCCESS)
        {
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Releasing of Per Spanning Tree PortInformation "
                     "Failed\n");

            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
    }

    /*Releasing PB port Entry for CVLAN Port */
    if (AST_PB_PORT_INFO (pAstPortInfo) != NULL)
    {
        if (AST_PB_RELEASE_PB_PORT_INFO_MEM_BLOCK
            (AST_PB_PORT_INFO (pAstPortInfo)) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Releasing of PB Port Information Failed\n");
            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
    }

    /*Releasing Port Entry for CVLAN Port */
    if (pAstPortInfo != NULL)
    {
        if (AST_RELEASE_PORT_INFO_MEM_BLOCK (pAstPortInfo) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Releasing of Port Information Failed\n");

            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstPbAllocCVlanPortTblAndPerStInfo                   */
/*                                                                           */
/* Description        : This Function is responsible for allocating memory   */
/*                      blocks for Port Array and PerSt Info Array           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbAllocCVlanPortTblAndPerStInfo (VOID)
{
    /* Allocate memory for port table in this context. */
    if (AST_PB_ALLOC_CVLAN_PORT_TBL_MEM_BLOCK
        (AST_CURR_CONTEXT_INFO ()->ppPortEntry) == NULL)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for CVLAN Port Table failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for CVLAN Port Table failed !!!\n");
        return ((INT4) AST_MEM_FAILURE);
    }

    /* This Per St Info block is allocated from the Global mempool */
    if (AST_PB_ALLOC_CVLAN_PERST_TBL_MEM_BLOCK
        (AST_CURR_CONTEXT_INFO ()->ppPerStInfo) == NULL)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for CVLAN Per Spanning Tree Info "
                 "failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for CVLAN per Spanning Tree Info "
                 "failed !!!\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET (AST_PERST_INFO_TBL (), AST_INIT_VAL,
                (sizeof (tAstPerStInfo *)));

    AST_MEMSET (AST_PORT_TBL (), AST_INIT_VAL,
                (AST_MAX_NUM_PORTS * sizeof (tAstPortEntry *)));

    return ((INT4) AST_MEM_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : AstPbDeAllocCVlanPortTblAndPerStInfo                 */
/*                                                                           */
/* Description        : This Function is responsible for releasing memory    */
/*                      blocks for Port Array and PerSt Info Array           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbDeAllocCVlanPortTblAndPerStInfo (VOID)
{
    INT4                i4RetVal = (INT4) AST_MEM_SUCCESS;

    /* This Block has to be released from Global Memory */
    if (AST_PERST_INFO_TBL () != NULL)
    {
        if (AST_PB_RELEASE_CVLAN_PERST_TBL_MEM_BLOCK
            (AST_PERST_INFO_TBL ()) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory Releasing for CVLAN Per Spanning Tree Info "
                     "failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory Releasing for CVLAN per Spanning Tree Info "
                     "failed !!!\n");

            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
        AST_PERST_INFO_TBL () = NULL;
    }

    /* Releasing memory for port table in this context. */
    if (AST_PORT_TBL () != NULL)
    {
        if (AST_PB_RELEASE_CVLAN_PORT_TBL_MEM_BLOCK
            (AST_PORT_TBL ()) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory Allocation for CVLAN Port Table failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory Allocation for CVLAN Port Table failed !!!\n");

            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
        AST_PORT_TBL () = NULL;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstPbAllocCVlanMemblockForPB                         */
/*                                                                           */
/* Description        : This Function is responsible for creating PbPortInfo */
/*                      of CEP, CVLAN context Information and allocating mem */
/*                      for pbCVlanInfo of CVLAN context                     */
/*                                                                           */
/* Input(s)           : pAstPortInfo- CEP Port Entry (index of CVLAN comp)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
RstPbAllocCVlanMemblockForPB (tAstPortEntry * pAstPortInfo)
{

    /* PB Port Info Block Allocated */
    if (AST_PB_ALLOC_PB_PORT_INFO_MEM_BLOCK (pAstPortInfo->pPbPortInfo) == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Allocation for PbPortInfo Failed for Port %u \n",
                      (AST_IFENTRY_IFINDEX (pAstPortInfo)));

        return ((INT4) AST_MEM_FAILURE);
    }

    /*NOTE: Whenever we allocate the Memory, It is essential to do Memset. */

    AST_MEMSET (pAstPortInfo->pPbPortInfo, AST_INIT_VAL,
                sizeof (tAstPbPortInfo));

    /* CVLAN context Block Allocated */
    if (AST_PB_ALLOC_CVLAN_CONTEXT_MEM_BLOCK
        (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)) == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Allocation of CVLAN context failed for Port %u \n",
                      (AST_IFENTRY_IFINDEX (pAstPortInfo)));

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET ((AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)), AST_INIT_VAL,
                sizeof (tAstContextInfo));

    /* Allocate CVLAN Info in CVLAN context created */
    if (AST_PB_ALLOC_CVLAN_INFO_MEM_BLOCK
        (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)->pPbCVlanInfo) == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Allocation for pCVlanInfo in CVLAN context Failed"
                      "for Port %u \n", (AST_IFENTRY_IFINDEX (pAstPortInfo)));

        return ((INT4) AST_MEM_FAILURE);
    }

    AST_MEMSET ((AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)->pPbCVlanInfo),
                AST_INIT_VAL, sizeof (tAstPbCVlanInfo));

    return ((INT4) AST_MEM_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : RstPbDeAllocCVlanMemblockForPB                       */
/*                                                                           */
/* Description        : This Function is responsible for deleting PbPortInfo */
/*                      of CEP, CVLAN context Information and releasing mem  */
/*                      for pbCVlanInfo of CVLAN context                     */
/*                                                                           */
/* Input(s)           : pAstPortInfo- CEP Port Entry (index of CVLAN comp)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
RstPbDeAllocCVlanMemblockForPB (tAstPortEntry * pAstPortInfo)
{
    INT4                i4RetVal = AST_MEM_SUCCESS;
    UINT4               u4IfIndex;

    u4IfIndex = AST_IFENTRY_IFINDEX (pAstPortInfo);

    /* Check if PB Port Info Block Deleted */
    if (AST_PB_PORT_INFO (pAstPortInfo) != NULL)
    {
        /* Check if CVLAN context Block Deleted */
        if (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo) != NULL)
        {
            /* Deleting of CVLAN Info in CVLAN context created */
            if (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)->pPbCVlanInfo != NULL)
            {
                if (AST_PB_RELEASE_CVLAN_INFO_MEM_BLOCK
                    (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)->pPbCVlanInfo) !=
                    AST_MEM_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                  "SYS: Release of pCVlanInfo in CVLAN context Failed"
                                  "for Port %u \n", u4IfIndex);

                    i4RetVal = (INT4) AST_MEM_FAILURE;
                }

                AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)->pPbCVlanInfo = NULL;
            }

            if (AST_PB_RELEASE_CVLAN_CONTEXT_MEM_BLOCK
                (AST_PB_PORT_CVLAN_CTXT (pAstPortInfo)) != AST_MEM_SUCCESS)
            {
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Release of CVLAN context failed for Port %u \n",
                              u4IfIndex);

                i4RetVal = (INT4) AST_MEM_FAILURE;
            }
            AST_PB_PORT_CVLAN_CTXT (pAstPortInfo) = NULL;

        }

        if (AST_PB_RELEASE_PB_PORT_INFO_MEM_BLOCK
            (AST_PB_PORT_INFO (pAstPortInfo)) != AST_MEM_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: Release for PbPortInfo Failed for Port %u \n",
                          u4IfIndex);

            i4RetVal = (INT4) AST_MEM_FAILURE;
        }

        AST_PB_PORT_INFO (pAstPortInfo) = NULL;

    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstPbCvlanMemPoolInit                                */
/*                                                                           */
/* Description        : This Function is responsible for creating mempools   */
/*                      for Port Array, PerstPort Array, Port2Index Mapping  */
/*                      Table.It also creates a RBTree for Port2Index Mapping*/
/*                      table                                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbCvlanMemPoolInit (VOID)
{

    /* Create Port and PerSt Port Array blocks */
    if (AstPbCreateCVlanPortArrayPools (AST_MAX_NUM_PORTS,
                                        &(AST_CVLAN_PORT_TBL_MEMPOOL_ID),
                                        &(AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID))
        != AST_MEM_SUCCESS)
    {
        return ((INT4) AST_MEM_FAILURE);
    }

    /*Creation of RBTree for Port2IndexMapTbl in CVLAN Info */
    AST_PB_CVLAN_PORT2INDEX_TREE () =
        AST_CREATE_RBTREE (AstPbCmpPort2IndexMapTable);

    if (AST_PB_CVLAN_PORT2INDEX_TREE () == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Embedded RBTree Creation for Port2Index Map Table"
                 "Failed\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    return AST_MEM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCvlanMemPoolDeInit                              */
/*                                                                           */
/* Description        : This Function is responsible for deleting mempools   */
/*                      for Port Array, PerstPort Array, Port2Index Mapping  */
/*                      Table.It also deletes RBTree for Port2Index Mapping  */
/*                      table                                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbCvlanMemPoolDeInit (VOID)
{
    INT4                i4RetVal = AST_MEM_SUCCESS;

    /*Deleting Port2Index Mapping Table from the CVLAN Information */
    if (AST_PB_CVLAN_PORT2INDEX_TREE () != NULL)
    {
        AST_DESTROY_RBTREE ();
        AST_PB_CVLAN_PORT2INDEX_TREE () = NULL;
    }

    /* Delete Perst Port Array and Port Array blocks */
    i4RetVal = AstPbDeleteCVlanPortArrayPools (&(AST_CVLAN_PORT_TBL_MEMPOOL_ID),
                                               &
                                               (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID));

    AST_CVLAN_PORT_TBL_MEMPOOL_ID = AST_INIT_VAL;
    AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID = AST_INIT_VAL;

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstPbCreateCVlanPortArrayPools                       */
/*                                                                           */
/* Description        : This Function is responsible for creating the  CVLAN */
/*                      port array and perst port array mempools for the     */
/*                      given block size                                     */
/*                                                                           */
/* Input(s)           : u4BlockSize - Block size should be allocated for pools*/
/*                                                                           */
/* Output(s)          : *pPortArrayPool - Pointer to CVLAN port array mempool*/
/*                      *pPerStPortArrayPool - Pointer to CVLAN perst port   */
/*                       array mempool                                       */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbCreateCVlanPortArrayPools (UINT4 u4BlockSize,
                                tAstMemPoolId * pPortArrayPool,
                                tAstMemPoolId * pPerStPortArrayPool)
{
    /* CVLAN Port Array Mempool */
    if (AST_CREATE_MEM_POOL
        ((u4BlockSize * sizeof (tAstPortEntry *)),
         gFsRstpSizingParams[AST_CVLAN_PB_PORTINFO_SIZING_ID].
         u4PreAllocatedUnits, MEM_HEAP_MEMORY_TYPE,
         pPortArrayPool) == AST_MEM_FAILURE)
    {
        AST_TRC (AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: Memory Pool Creation for CVLAN Port Table Failure for AST module\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Memory Pool Creation for CVLAN Port Table Failure for AST module\n");
        return ((INT4) AST_MEM_FAILURE);
    }

    /* CVLAN Perst Port Array Mempool */
    if (AST_CREATE_MEM_POOL
        ((u4BlockSize * sizeof (tAstPerStPortInfo *)),
         gFsRstpSizingParams[AST_CVLAN_PB_PERST_PORTINFO_SIZING_ID].
         u4PreAllocatedUnits, MEM_HEAP_MEMORY_TYPE,
         pPerStPortArrayPool) == AST_MEM_FAILURE)

    {
        AST_TRC (AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: Memory Pool Creation for CVLAN PerStPort Table Failure for AST module\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Memory Pool Creation for CVLAN PerStPort Table Failure for AST module\n");

        return ((INT4) AST_MEM_FAILURE);
    }

    return ((INT4) AST_MEM_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : AstPbDeleteCVlanPortArrayPools                       */
/*                                                                           */
/* Description        : This Function is responsible for deleting the  CVLAN */
/*                      port array and perst port array mempools.            */
/*                                                                           */
/* Input (s)          : *pPortArrayPool - Pointer to CVLAN port array mempool*/
/*                      *pPerStPortArrayPool - Pointer to CVLAN perst port   */
/*                       array mempool                                       */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : AST_MEM_SUCCESS / AST_MEM_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbDeleteCVlanPortArrayPools (tAstMemPoolId * pPortArrayPool,
                                tAstMemPoolId * pPerStPortArrayPool)
{
    INT4                i4RetVal = (INT4) AST_MEM_SUCCESS;

    /* Per Spanning Tree Port Table Mempool deletion */
    if (*pPerStPortArrayPool != AST_INIT_VAL)
    {
        if (AST_DELETE_MEM_POOL (*pPerStPortArrayPool) == AST_MEM_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory pool Deletion for Per Spanning Tree Port Table failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory pool Deletion for Per Spanning Tree Port Table failed !!!\n");
            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
        *pPerStPortArrayPool = AST_INIT_VAL;
    }

    /* Per Port Table Mempool deletion */
    if (*pPortArrayPool != AST_INIT_VAL)
    {
        if (AST_DELETE_MEM_POOL (*pPortArrayPool) == AST_MEM_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_OS_RESOURCE_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory pool Deletion for Port Table Array failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory pool Deletion for Port Table Array failed !!!\n");
            i4RetVal = (INT4) AST_MEM_FAILURE;
        }
        *pPortArrayPool = AST_INIT_VAL;
    }

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : AstPbIncrCvlanTablesSize                             */
/*                                                                           */
/* Description        : This Function is responsible for incrementing the    */
/*                      port array and perst port array mempools when the    */
/*                      number of PEPs exceeded than AST_MAX_NUM_PORTS in a  */
/*                      CVLAN component.                                     */
/*                                                                           */
/*                                                                           */
/* Input (s)          :None                                                  */
/*                                                                           */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstPbIncrCvlanTablesSize (VOID)
{
    tAstMemPoolId       PortTblPoolId = 0;
    tAstMemPoolId       PerStPortTblPoolId = 0;
    tAstPortEntry     **ppPortEntry;
    tAstPerStPortInfo **ppPerStPortInfo;
    UINT4               u4BlockSize = 0;

/* This function assumes that the current context pointer points to
 * C-VLAN context. */

    /*Allocate Blocksize of Num Port + Pep Block Size */
    u4BlockSize = AST_MAX_NUM_PORTS + AST_PEP_BLOCK_SIZE;

    if (AstPbCreateCVlanPortArrayPools (u4BlockSize, &PortTblPoolId,
                                        &PerStPortTblPoolId) != AST_MEM_SUCCESS)
    {
        return RST_FAILURE;
    }

    /*Allocate Memory from the MemPool */
    AST_ALLOC_PORT_TBL_BLOCK (PortTblPoolId, ppPortEntry);
    if (ppPortEntry == NULL)
    {
        return RST_FAILURE;
    }

    AST_ALLOC_PERST_PORT_TBL_BLOCK (PerStPortTblPoolId, ppPerStPortInfo);
    if (ppPerStPortInfo == NULL)
    {
        AST_RELEASE_PORT_TBL_BLOCK (PortTblPoolId, ppPortEntry);
        return RST_FAILURE;
    }

    AST_MEMSET (ppPortEntry, AST_INIT_VAL,
                (u4BlockSize * sizeof (tAstPortEntry *)));

    AST_MEMSET (ppPerStPortInfo, AST_INIT_VAL,
                (u4BlockSize * sizeof (tAstPerStPortInfo *)));

    /*Copying the Existing Port Array and PerSt Port Array Into Locally
     * Allocated Pointers*/
    AST_MEMCPY (ppPortEntry, AST_PORT_TBL (),
                (AST_MAX_NUM_PORTS) * sizeof (tAstPortEntry *));

    AST_MEMCPY (ppPerStPortInfo,
                AST_PERST_PORT_INFO_TBL (RST_DEFAULT_INSTANCE),
                ((AST_MAX_NUM_PORTS) * sizeof (tAstPerStPortInfo *)));

    /* Releasing Existing Port Array and Perst port Array blocks after copying
     * the data*/

    AST_RELEASE_PORT_TBL_BLOCK (AST_CVLAN_PORT_TBL_MEMPOOL_ID, AST_PORT_TBL ());

    AST_RELEASE_PERST_PORT_TBL_BLOCK
        (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID,
         AST_PERST_PORT_INFO_TBL (RST_DEFAULT_INSTANCE));

    /* Destroying the Mempools allocated for the CVLAN component for Port Array
     * and Perst Port Array*/
    AstPbDeleteCVlanPortArrayPools (&(AST_CVLAN_PORT_TBL_MEMPOOL_ID),
                                    &(AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID));

    /*Assigning Newly Allocated Mempool Ids to the CVLAN component Mempool */
    AST_CVLAN_PORT_TBL_MEMPOOL_ID = PortTblPoolId;
    AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID = PerStPortTblPoolId;

    /* Restoring the Port Array and Perst Port Array */
    AST_PORT_TBL () = ppPortEntry;
    AST_PERST_PORT_INFO_TBL (RST_DEFAULT_INSTANCE) = ppPerStPortInfo;

    /* Increase the port table size in Red C-VLAN component. */
    AstRedPbIncrCvlanPortTableSize ();

    /*Change the Max Num Ports in the CVLAN context */
    AST_MAX_NUM_PORTS = (UINT2) (AST_MAX_NUM_PORTS + AST_PEP_BLOCK_SIZE);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbCmpPort2IndexMapTable                           */
/*                                                                           */
/* Description        : This function is called by RB Tree FSAP routine when */
/*                      addding/deleting/get of a node in port 2 index map   */
/*                      table. This function is comparision fn used by RBTree*/
/*                      FSAP utility                                         */
/*                                                                           */
/*                                                                           */
/* Input (s)          :pNodeA, PNodeB - Nodes to be compared                 */
/*                                                                           */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : 1/0/-1                                               */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbCmpPort2IndexMapTable (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tAstPort2IndexMap  *pPort2IndexA = (tAstPort2IndexMap *) pNodeA;
    tAstPort2IndexMap  *pPort2IndexB = (tAstPort2IndexMap *) pNodeB;

    if (pPort2IndexA->u2ProtPort != pPort2IndexB->u2ProtPort)
    {
        if (pPort2IndexA->u2ProtPort > pPort2IndexB->u2ProtPort)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    return 0;
}

/* Protocol routines */
