/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: aspbmbsm.c,v 1.5 2012/08/08 10:36:28 siva Exp $                      */
/*                                                                           */
/*  FILE NAME             : aspbmbsm.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : RSTP Module                                      */
/*  MODULE NAME           : RSTP                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains RSTP MBSM related routines.   */
/*                                                                           */
/*****************************************************************************/

#include "asthdrs.h"
/*****************************************************************************/
/* Function Name      : AstPbMbsmSetCvlanPortStatesInHw                      */
/*                                                                           */
/* Description        : This function programs the HW with the STP software  */
/*                      configuration.                                       */
/*                                                                           */
/*                      This function will be called from the STP module     */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  pInPortEntry -  PortEntry of the CEP belonging to   */
/*                                       the card that is getting inserted.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gpAstContextInfo                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstPbMbsmSetCvlanPortStatesInHw (tAstPortEntry * pInPortEntry,
                                 tMbsmSlotInfo * pSlotInfo)
{

    tAstPortEntry      *pPortInfo = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;
    UINT4               u4CepIfIndex = 0;
    UINT2               u2PortNum = 0;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;

    if (AstPbSelectCvlanContext (pInPortEntry) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: CVlan Select context failed for"
                      "CEP Port %d \r\n", pInPortEntry->u4IfIndex);
        return;
    }

    u4ContextId = (AST_PB_CVLAN_INFO ()->pParentContext->u4ContextId);
    u4CepIfIndex = (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        u1PortState =
            AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE, u2PortNum);

        if (u2PortNum == AST_PB_CEP_CVLAN_LOCAL_PORT_NUM)
        {

            RstpFsMiRstpMbsmNpSetPortState (u4ContextId, u4CepIfIndex,
                                        u1PortState, pSlotInfo);
        }
        else
        {

            RstpFsMiPbRstpMbsmNpSetPortState (u4ContextId, u4CepIfIndex, u2PortNum,
                                          u1PortState, pSlotInfo);
        }
    }

    AstPbRestoreContext ();
    return;
}
