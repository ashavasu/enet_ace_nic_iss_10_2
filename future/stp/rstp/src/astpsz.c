/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: astpsz.c,v 1.6 2017/12/08 10:56:18 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ASTSZ_C
#include "asthdrs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
AstSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < AST_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsASTSizingParams[i4SizingId].u4StructSize,
                                     FsASTSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(ASTMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            AstSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
        /*changes related to CSR 127990 */
        FsASTSizingParams[i4SizingId].u4MemPreAllocated =
            FsASTSizingParams[i4SizingId].u4StructSize *
            FsASTSizingParams[i4SizingId].u4PreAllocatedUnits;
        FsASTSizingParams[i4SizingId].u4MemPreAllocated =
            FsASTSizingParams[i4SizingId].u4StructSize *
            FsASTSizingParams[i4SizingId].u4PreAllocatedUnits;

    }
    return OSIX_SUCCESS;
}

INT4
AstSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsASTSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ASTMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
AstSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < AST_MAX_SIZING_ID; i4SizingId++)
    {
        if (ASTMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ASTMemPoolIds[i4SizingId]);
            ASTMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
