/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbrslw.c,v 1.21 2012/06/20 12:48:16 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "asthdrs.h"
# include "stpcli.h"
# include "fsmpbrcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbProviderStpStatus
 Input       :  The Indices

                The Object 
                retValFsPbProviderStpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbProviderStpStatus (INT4 *pi4RetValFsPbProviderStpStatus)
{
    *pi4RetValFsPbProviderStpStatus = AST_SVLAN_ADMIN_STATUS ();

    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbProviderStpStatus
 Input       :  The Indices

                The Object 
                setValFsPbProviderStpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbProviderStpStatus (INT4 i4SetValFsPbProviderStpStatus)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsPbProviderStpStatus == (INT4) AST_SVLAN_ADMIN_STATUS ())
    {
        AST_TRC (AST_MGMT_TRC, "MGMT: S-VLAN Spanning Tree already has the "
                 "same Status\n");
        return (INT1) SNMP_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsPbProviderStpStatus == (INT4) RST_ENABLED)
    {
        pNode->MsgType = AST_SVLAN_ENABLE_MSG;
    }
    else
    {
        pNode->MsgType = AST_SVLAN_DISABLE_MSG;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbProviderStpStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPbProviderStpStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbProviderStpStatus
 Input       :  The Indices

                The Object 
                testValFsPbProviderStpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbProviderStpStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsPbProviderStpStatus)
{
    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: STP System Shutdown; CANNOT Enable/Disable RSTP\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((AST_IS_1AD_BRIDGE ()) != RST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbProviderStpStatus != RST_ENABLED) &&
        (i4TestValFsPbProviderStpStatus != RST_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbProviderStpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbProviderStpStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbRstCVlanBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbRstCVlanBridgeTable
 Input       :  The Indices
                FsPbRstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbRstCVlanBridgeTable (INT4 i4FsPbRstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (AST_PB_IS_PORT_SVLAN_CEP (pAstPortEntry) != RST_TRUE)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT : This Port %d is not of type CEP. It is not a CVLAN"
                      "component\n", i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbRstCVlanBridgeTable
 Input       :  The Indices
                FsPbRstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbRstCVlanBridgeTable (INT4 *pi4FsPbRstPort)
{
    return nmhGetNextIndexFsPbRstCVlanBridgeTable (0, pi4FsPbRstPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbRstCVlanBridgeTable
 Input       :  The Indices
                FsPbRstPort
                nextFsPbRstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbRstCVlanBridgeTable (INT4 i4FsPbRstPort,
                                        INT4 *pi4NextFsPbRstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP System Control is Shutdown!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstSnmpLowGetNextValidIndex (i4FsPbRstPort, pi4NextFsPbRstPort) !=
        RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " No Next Port exists !!! \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) *pi4NextFsPbRstPort);

    while (pAstPortEntry != NULL)
    {
        if (AST_PB_IS_PORT_SVLAN_CEP (pAstPortEntry) == RST_TRUE)
        {
            *pi4NextFsPbRstPort = pAstPortEntry->u2PortNo;

            return (INT1) SNMP_SUCCESS;
        }

        i4FsPbRstPort = *pi4NextFsPbRstPort;

        if (AstSnmpLowGetNextValidIndex (i4FsPbRstPort, pi4NextFsPbRstPort) !=
            RST_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     " No Next Port exists !!! \n");

            break;
        }

        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) *pi4NextFsPbRstPort);

    }

    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
             "No More Ports are configured as CVLAN component\n");

    return (INT1) SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeId
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeId (INT4 i4FsPbRstPort,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsPbRstCVlanBridgeId)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstCtxtBrgEntry = NULL;
    tAstPerStBridgeInfo *pAstPerStBrgInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;
    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    pAstCtxtBrgEntry = AST_GET_BRGENTRY ();
    pAstPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    pu1List = pRetValFsPbRstCVlanBridgeId->pu1_OctetList;

    u2Val = pAstPerStBrgInfo->u2BrgPriority;

    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pAstCtxtBrgEntry->BridgeAddr);

    pRetValFsPbRstCVlanBridgeId->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeDesignatedRoot
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeDesignatedRoot (INT4 i4FsPbRstPort,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsPbRstCVlanBridgeDesignatedRoot)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStBridgeInfo *pAstPerStBrgInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;
    tAstMacAddr         TempMacAddr;

    AST_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed \n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    pRetValFsPbRstCVlanBridgeDesignatedRoot->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    if (!(AST_IS_RST_ENABLED ()))
    {
        pu1List = pRetValFsPbRstCVlanBridgeDesignatedRoot->pu1_OctetList;

        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    pu1List = pRetValFsPbRstCVlanBridgeDesignatedRoot->pu1_OctetList;
    u2Val = pAstPerStBrgInfo->RootId.u2BrgPriority;

    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pAstPerStBrgInfo->RootId.BridgeAddr);

    /*Restoring the context pointer */
    AstPbRestoreContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeRootCost
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeRootCost (INT4 i4FsPbRstPort,
                                  INT4 *pi4RetValFsPbRstCVlanBridgeRootCost)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStBridgeInfo *pAstPerStBrgInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanBridgeRootCost = AST_INIT_VAL;

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    *pi4RetValFsPbRstCVlanBridgeRootCost = pAstPerStBrgInfo->u4RootCost;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeMaxAge
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeMaxAge (INT4 i4FsPbRstPort,
                                INT4 *pi4RetValFsPbRstCVlanBridgeMaxAge)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanBridgeMaxAge = AST_INIT_VAL;

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /*Root Bridge's Max Age value */

    *pi4RetValFsPbRstCVlanBridgeMaxAge =
        (INT4) AST_SYS_TO_MGMT (pAstBrgEntry->RootTimes.u2MaxAge);

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeHelloTime
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeHelloTime (INT4 i4FsPbRstPort,
                                   INT4 *pi4RetValFsPbRstCVlanBridgeHelloTime)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanBridgeHelloTime =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_HELLO_TIME);

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /* Root bridge's Hello Time */
    *pi4RetValFsPbRstCVlanBridgeHelloTime =
        (INT4) AST_SYS_TO_MGMT (pAstBrgEntry->RootTimes.u2HelloTime);

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeHoldTime
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeHoldTime (INT4 i4FsPbRstPort,
                                  INT4 *pi4RetValFsPbRstCVlanBridgeHoldTime)
{
    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    /*No need of context switch as this object is not a variable , it is a
     * constant*/
    *pi4RetValFsPbRstCVlanBridgeHoldTime =
        (INT4) AST_SYS_TO_MGMT (AST_HOLD_TIME);

    UNUSED_PARAM (i4FsPbRstPort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeForwardDelay
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeForwardDelay (INT4 i4FsPbRstPort,
                                      INT4
                                      *pi4RetValFsPbRstCVlanBridgeForwardDelay)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanBridgeForwardDelay =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_FWD_DELAY);

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /* Root Bridge's Forward Delay */
    *pi4RetValFsPbRstCVlanBridgeForwardDelay =
        (INT4) AST_SYS_TO_MGMT (pAstBrgEntry->RootTimes.u2ForwardDelay);

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanBridgeTxHoldCount
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanBridgeTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanBridgeTxHoldCount (INT4 i4FsPbRstPort,
                                     INT4
                                     *pi4RetValFsPbRstCVlanBridgeTxHoldCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /* Transmit Hold Count for this Bridge */
    *pi4RetValFsPbRstCVlanBridgeTxHoldCount =
        (UINT1) pAstBrgEntry->u1TxHoldCount;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanStpHelloTime
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanStpHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanStpHelloTime (INT4 i4FsPbRstPort,
                                INT4 *pi4RetValFsPbRstCVlanStpHelloTime)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanStpHelloTime =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_HELLO_TIME);
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /* This bridge's Hello Time */
    *pi4RetValFsPbRstCVlanStpHelloTime =
        (INT4) AST_SYS_TO_MGMT (pAstBrgEntry->BridgeTimes.u2HelloTime);

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanStpMaxAge
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanStpMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanStpMaxAge (INT4 i4FsPbRstPort,
                             INT4 *pi4RetValFsPbRstCVlanStpMaxAge)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed \n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanStpMaxAge =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_MAX_AGE);

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /*This Bridge's Max Age value */

    *pi4RetValFsPbRstCVlanStpMaxAge = pAstBrgEntry->BridgeTimes.u2MaxAge;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanStpForwardDelay
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanStpForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanStpForwardDelay (INT4 i4FsPbRstPort,
                                   INT4 *pi4RetValFsPbRstCVlanStpForwardDelay)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pAstBrgEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanStpForwardDelay =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_FWD_DELAY);

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    pAstBrgEntry = AST_GET_BRGENTRY ();

    /* This Bridge's Forward Delay */
    *pi4RetValFsPbRstCVlanStpForwardDelay =
        pAstBrgEntry->BridgeTimes.u2ForwardDelay;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanStpTopChanges
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanStpTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanStpTopChanges (INT4 i4FsPbRstPort,
                                 UINT4 *pu4RetValFsPbRstCVlanStpTopChanges)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanStpTopChanges = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    /* Number of times the topology changes occured in this bridge */
    *pu4RetValFsPbRstCVlanStpTopChanges = pPerStBrgInfo->u4NumTopoCh;

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanStpTimeSinceTopologyChange
 Input       :  The Indices
                FsPbRstPort

                The Object 
                retValFsPbRstCVlanStpTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanStpTimeSinceTopologyChange (INT4 i4FsPbRstPort,
                                              UINT4
                                              *pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstSysTime         CurrSysTime;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange = AST_INIT_VAL;

        AstPbRestoreContext ();
        return SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    /* Number of times the topology changes occured in this bridge */

    if (pPerStBrgInfo->u4NumTopoCh != 0)
    {
        AST_GET_SYS_TIME (&CurrSysTime);

        *pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange =
            (UINT4) (CurrSysTime -
                     (tAstSysTime) pPerStBrgInfo->u4TimeSinceTopoCh);
        *pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange =
            AST_CONVERT_STUPS_2_CENTI_SEC
            (*pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange);
    }
    else
    {
        *pu4RetValFsPbRstCVlanStpTimeSinceTopologyChange = 0;
    }

    /*Restoring the context pointer */
    AstPbRestoreContext ();

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbRstCVlanPortInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable (INT4 i4FsPbRstPort,
                                                   INT4 i4FsPbRstCepSvid)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (AST_PB_IS_PORT_SVLAN_CEP (pAstPortEntry) != RST_TRUE)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT : This Port %d is not of type CEP. It is not a CVLAN"
                      "component\n", i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    if (AstGetPepPortEntryWithCtxtSwitch (pAstPortEntry, i4FsPbRstCepSvid) ==
        NULL)
    {
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Port %d SVID %d is not present in this CVLAN"
                      "component\n", i4FsPbRstPort, i4FsPbRstCepSvid);

        return (INT1) SNMP_FAILURE;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbRstCVlanPortInfoTable (INT4 *pi4FsPbRstPort,
                                           INT4 *pi4FsPbRstCepSvid)
{
    return (nmhGetNextIndexFsPbRstCVlanPortInfoTable
            (0, pi4FsPbRstPort, 0, pi4FsPbRstCepSvid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsPbRstPort
                nextFsPbRstPort
                FsPbRstCepSvid
                nextFsPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbRstCVlanPortInfoTable (INT4 i4FsPbRstPort,
                                          INT4 *pi4NextFsPbRstPort,
                                          INT4 i4FsPbRstCepSvid,
                                          INT4 *pi4NextFsPbRstCepSvid)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tVlanId             Svid = AST_PB_CEP_PROT_PORT_NUM;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsPbRstPort > AST_MAX_NUM_PORTS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPbRstPort == AST_MAX_NUM_PORTS) &&
        (i4FsPbRstCepSvid > AST_PB_CEP_PROT_PORT_NUM))
    {
        /* Beyond last port in the C-VLAN component. */
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsPbRstPort <= 0)
    {
        i4FsPbRstPort = 0;
    }
    else
    {
        /* Valid port number. */
        pAstPortEntry = AST_GET_PORTENTRY (i4FsPbRstPort);
    }

    if (i4FsPbRstCepSvid < 0)
    {
        i4FsPbRstCepSvid = 0;
    }

    /* 
     * If (i4FsPbRstPort == 0), then get the next CEP port.
     * If port entry does not exist for given port num, then get the next
     * cep port.
     * If (Given port is not a CEP), the get the next CEP port.
     * If (Given port is CEP, but the given svid >= 0xfff) then get
     * next cep port.
     */
    if ((i4FsPbRstPort == 0) || (pAstPortEntry == NULL) ||
        (i4FsPbRstCepSvid >= AST_PB_CEP_PROT_PORT_NUM) ||
        (AST_GET_BRG_PORT_TYPE (pAstPortEntry) != AST_CUSTOMER_EDGE_PORT))
    {
        pAstPortEntry = AstGetNextCepPortEntry ((UINT2) i4FsPbRstPort);
        if (pAstPortEntry == NULL)
        {
            /* There are no other CEPs present. */
            return SNMP_FAILURE;
        }

        /* We have selected next cep port, so initialize i4FsPbRstCepSvid as
         * zero. */
        i4FsPbRstCepSvid = 0;
    }

    /* Now the CEP is selected.
     * So get the Pep whose svid is greater than i4FsPbRstCepSvid.
     * For this we have go to that C-VLAN component. */

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT : selection of CVLAN context from this port %d"
                      "failed\n", i4FsPbRstPort);

        return (INT1) SNMP_FAILURE;
    }

    Svid = (tVlanId) AstGetNextPepPortEntry (i4FsPbRstCepSvid);

    /* Nothing to be done in this C-VLAN  componnent, so restore 
     * the context.*/
    AstPbRestoreContext ();

    if (Svid == 0)
    {
        /* Something wrong. */
        return SNMP_FAILURE;
    }

    *pi4NextFsPbRstPort = pAstPortEntry->u2PortNo;
    *pi4NextFsPbRstCepSvid = Svid;

    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortPriority
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortPriority (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                INT4 *pi4RetValFsPbRstCVlanPortPriority)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    *pi4RetValFsPbRstCVlanPortPriority = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPbRstCVlanPortPriority =
        (INT4) pPepPerStPortInfo->u1PortPriority;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortPathCost
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortPathCost (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                INT4 *pi4RetValFsPbRstCVlanPortPathCost)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortPathCost = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPbRstCVlanPortPathCost = (INT4) pPepPortEntry->u4PathCost;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortRole
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortRole (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                            INT4 *pi4RetValFsPbRstCVlanPortRole)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    *pi4RetValFsPbRstCVlanPortRole = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortRole = AST_PORT_ROLE_DISABLED;

        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortRole = (INT4) pPepPerStPortInfo->u1PortRole;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortState (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                             INT4 *pi4RetValFsPbRstCVlanPortState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    *pi4RetValFsPbRstCVlanPortState = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortState = AST_PORT_STATE_DISABLED;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortState =
        (INT4) AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE,
                                             pPepPortEntry->u2PortNo);

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortAdminEdgePort
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortAdminEdgePort (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                     INT4
                                     *pi4RetValFsPbRstCVlanPortAdminEdgePort)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortAdminEdgePort = AST_SNMP_FALSE;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (pPepPortEntry->bAdminEdgePort == RST_TRUE)
    {
        *pi4RetValFsPbRstCVlanPortAdminEdgePort = AST_SNMP_TRUE;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortOperEdgePort
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortOperEdgePort (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                    INT4 *pi4RetValFsPbRstCVlanPortOperEdgePort)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortOperEdgePort = AST_SNMP_FALSE;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    if (pPepPortEntry->bOperEdgePort == RST_TRUE)
    {
        *pi4RetValFsPbRstCVlanPortOperEdgePort = AST_SNMP_TRUE;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortAdminPointToPoint
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortAdminPointToPoint (INT4 i4FsPbRstPort,
                                         INT4 i4FsPbRstCepSvid,
                                         INT4
                                         *pi4RetValFsPbRstCVlanPortAdminPointToPoint)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortAdminPointToPoint = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPbRstCVlanPortAdminPointToPoint
        = (INT4) pPepPortEntry->u1AdminPointToPoint;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortOperPointToPoint
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortOperPointToPoint (INT4 i4FsPbRstPort,
                                        INT4 i4FsPbRstCepSvid,
                                        INT4
                                        *pi4RetValFsPbRstCVlanPortOperPointToPoint)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortOperPointToPoint = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortOperPointToPoint = AST_SNMP_FALSE;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    if (pPepPortEntry->bOperPointToPoint == RST_TRUE)
    {
        *pi4RetValFsPbRstCVlanPortOperPointToPoint = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPbRstCVlanPortOperPointToPoint = AST_SNMP_FALSE;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortAutoEdge
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortAutoEdge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortAutoEdge (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                INT4 *pi4RetValFsPbRstCVlanPortAutoEdge)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    *pi4RetValFsPbRstCVlanPortAutoEdge = AST_SNMP_FALSE;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (pPepPortEntry->bAutoEdge == RST_TRUE)
    {
        *pi4RetValFsPbRstCVlanPortAutoEdge = AST_SNMP_TRUE;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortDesignatedRoot
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortDesignatedRoot (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsPbRstCVlanPortDesignatedRoot)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Value = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    pu1List = pRetValFsPbRstCVlanPortDesignatedRoot->pu1_OctetList;

    pRetValFsPbRstCVlanPortDesignatedRoot->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_PUT_2BYTE (pu1List, u2Value);
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    u2Value = pPepPerStPortInfo->RootId.u2BrgPriority;

    AST_PUT_2BYTE (pu1List, u2Value);
    AST_PUT_MAC_ADDR (pu1List, pPepPerStPortInfo->RootId.BridgeAddr);

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortDesignatedCost
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortDesignatedCost (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      INT4
                                      *pi4RetValFsPbRstCVlanPortDesignatedCost)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortDesignatedCost = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortDesignatedCost = pPepPortEntry->u4PathCost;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortDesignatedBridge
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortDesignatedBridge (INT4 i4FsPbRstPort,
                                        INT4 i4FsPbRstCepSvid,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsPbRstCVlanPortDesignatedBridge)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Value = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    pu1List = pRetValFsPbRstCVlanPortDesignatedBridge->pu1_OctetList;

    pRetValFsPbRstCVlanPortDesignatedBridge->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    u2Value = pPepPerStPortInfo->DesgBrgId.u2BrgPriority;

    AST_PUT_2BYTE (pu1List, u2Value);
    AST_PUT_MAC_ADDR (pu1List, pPepPerStPortInfo->DesgBrgId.BridgeAddr);

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortDesignatedPort
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortDesignatedPort (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsPbRstCVlanPortDesignatedPort)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Value = 0;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    pu1List = pRetValFsPbRstCVlanPortDesignatedPort->pu1_OctetList;
    pRetValFsPbRstCVlanPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    if (pPepPortEntry->u1EntryStatus != AST_PORT_OPER_DOWN)
    {
        u2Value = pPepPerStPortInfo->u2DesgPortId;

        AST_PUT_2BYTE (pu1List, u2Value);
    }
    else
    {
        AST_MEMSET (pu1List, AST_MEMSET_VAL, (AST_PORT_ID_SIZE));
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortForwardTransitions
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortForwardTransitions (INT4 i4FsPbRstPort,
                                          INT4 i4FsPbRstCepSvid,
                                          UINT4
                                          *pu4RetValFsPbRstCVlanPortForwardTransitions)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortForwardTransitions = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortForwardTransitions =
        pPepPerStPortInfo->u4NumFwdTransitions;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbRstCVlanPortSmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbRstCVlanPortSmTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbRstCVlanPortSmTable (INT4 i4FsPbRstPort,
                                                 INT4 i4FsPbRstCepSvid)
{
    return (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable (i4FsPbRstPort,
                                                               i4FsPbRstCepSvid));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbRstCVlanPortSmTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbRstCVlanPortSmTable (INT4 *pi4FsPbRstPort,
                                         INT4 *pi4FsPbRstCepSvid)
{
    return (nmhGetNextIndexFsPbRstCVlanPortInfoTable
            (0, pi4FsPbRstPort, 0, pi4FsPbRstCepSvid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbRstCVlanPortSmTable
 Input       :  The Indices
                FsPbRstPort
                nextFsPbRstPort
                FsPbRstCepSvid
                nextFsPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbRstCVlanPortSmTable (INT4 i4FsPbRstPort,
                                        INT4 *pi4NextFsPbRstPort,
                                        INT4 i4FsPbRstCepSvid,
                                        INT4 *pi4NextFsPbRstCepSvid)
{
    return (nmhGetNextIndexFsPbRstCVlanPortInfoTable
            (i4FsPbRstPort, pi4NextFsPbRstPort, i4FsPbRstCepSvid,
             pi4NextFsPbRstCepSvid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortInfoSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortInfoSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortInfoSmState (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                   INT4 *pi4RetValFsPbRstCVlanPortInfoSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortInfoSmState = RST_PINFOSM_STATE_DISABLED;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortInfoSmState =
        (INT4) pPepPerStPortInfo->u1PinfoSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortMigSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortMigSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortMigSmState (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                  INT4 *pi4RetValFsPbRstCVlanPortMigSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstCommPortInfo   *pPepCommPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepCommPortInfo = AST_GET_COMM_PORT_INFO (pPepPortEntry->u2PortNo);

    if (pPepCommPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortMigSmState = RST_PMIGSM_STATE_CHECKING_RSTP;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortMigSmState =
        (INT4) pPepCommPortInfo->u1PmigSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortRoleTransSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortRoleTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortRoleTransSmState (INT4 i4FsPbRstPort,
                                        INT4 i4FsPbRstCepSvid,
                                        INT4
                                        *pi4RetValFsPbRstCVlanPortRoleTransSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortRoleTransSmState =
            (INT1) RST_PMIGSM_STATE_CHECKING_RSTP;
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortRoleTransSmState =
        (INT4) pPepPerStPortInfo->u1ProleTrSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortStateTransSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortStateTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortStateTransSmState (INT4 i4FsPbRstPort,
                                         INT4 i4FsPbRstCepSvid,
                                         INT4
                                         *pi4RetValFsPbRstCVlanPortStateTransSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortStateTransSmState =
            (INT1) RST_PSTATETRSM_STATE_DISCARDING;
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortStateTransSmState =
        (INT4) pPepPerStPortInfo->u1PstateTrSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortTopoChSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortTopoChSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortTopoChSmState (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                     INT4
                                     *pi4RetValFsPbRstCVlanPortTopoChSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPepPerStPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepPerStPortInfo = AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                                 RST_DEFAULT_INSTANCE);

    if (pPepPerStPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortTopoChSmState =
            (INT1) RST_TOPOCHSM_STATE_INACTIVE;
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortTopoChSmState =
        (INT4) pPepPerStPortInfo->u1TopoChSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortTxSmState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortTxSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortTxSmState (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                 INT4 *pi4RetValFsPbRstCVlanPortTxSmState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstCommPortInfo   *pPepCommPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pPepCommPortInfo = AST_GET_COMM_PORT_INFO (pPepPortEntry->u2PortNo);

    if (pPepCommPortInfo == NULL)
    {
        AstPbRestoreContext ();

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Given PEP port Information is not Present!!!!\n");

        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortTxSmState =
            (INT1) RST_PTXSM_STATE_TRANSMIT_INIT;
        AstPbRestoreContext ();
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPbRstCVlanPortTxSmState =
        (INT4) pPepCommPortInfo->u1PortTxSmState;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbRstCVlanPortStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable (INT4 i4FsPbRstPort,
                                                    INT4 i4FsPbRstCepSvid)
{
    return (nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable (i4FsPbRstPort,
                                                               i4FsPbRstCepSvid));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbRstCVlanPortStatsTable (INT4 *pi4FsPbRstPort,
                                            INT4 *pi4FsPbRstCepSvid)
{
    return nmhGetNextIndexFsPbRstCVlanPortInfoTable
        (0, pi4FsPbRstPort, 0, pi4FsPbRstCepSvid);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsPbRstPort
                nextFsPbRstPort
                FsPbRstCepSvid
                nextFsPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbRstCVlanPortStatsTable (INT4 i4FsPbRstPort,
                                           INT4 *pi4NextFsPbRstPort,
                                           INT4 i4FsPbRstCepSvid,
                                           INT4 *pi4NextFsPbRstCepSvid)
{
    return nmhGetNextIndexFsPbRstCVlanPortInfoTable
        (i4FsPbRstPort, pi4NextFsPbRstPort, i4FsPbRstCepSvid,
         pi4NextFsPbRstCepSvid);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortRxRstBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortRxRstBpduCount (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      UINT4
                                      *pu4RetValFsPbRstCVlanPortRxRstBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortRxRstBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortRxRstBpduCount = pPepPortEntry->u4NumRstBpdusRxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortRxConfigBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortRxConfigBpduCount (INT4 i4FsPbRstPort,
                                         INT4 i4FsPbRstCepSvid,
                                         UINT4
                                         *pu4RetValFsPbRstCVlanPortRxConfigBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortRxConfigBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortRxConfigBpduCount =
        pPepPortEntry->u4NumConfigBpdusRxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortRxTcnBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortRxTcnBpduCount (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      UINT4
                                      *pu4RetValFsPbRstCVlanPortRxTcnBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortRxTcnBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortRxTcnBpduCount = pPepPortEntry->u4NumTcnBpdusRxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortTxRstBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortTxRstBpduCount (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      UINT4
                                      *pu4RetValFsPbRstCVlanPortTxRstBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortTxRstBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortTxRstBpduCount = pPepPortEntry->u4NumRstBpdusTxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortTxConfigBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortTxConfigBpduCount (INT4 i4FsPbRstPort,
                                         INT4 i4FsPbRstCepSvid,
                                         UINT4
                                         *pu4RetValFsPbRstCVlanPortTxConfigBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortTxConfigBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortTxConfigBpduCount =
        pPepPortEntry->u4NumConfigBpdusTxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortTxTcnBpduCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortTxTcnBpduCount (INT4 i4FsPbRstPort, INT4 i4FsPbRstCepSvid,
                                      UINT4
                                      *pu4RetValFsPbRstCVlanPortTxTcnBpduCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortTxTcnBpduCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortTxTcnBpduCount = pPepPortEntry->u4NumTcnBpdusTxd;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortInvalidRstBpduRxCount (INT4 i4FsPbRstPort,
                                             INT4 i4FsPbRstCepSvid,
                                             UINT4
                                             *pu4RetValFsPbRstCVlanPortInvalidRstBpduRxCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortInvalidRstBpduRxCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortInvalidRstBpduRxCount =
        pPepPortEntry->u4InvalidRstBpdusRxdCount;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortInvalidConfigBpduRxCount (INT4 i4FsPbRstPort,
                                                INT4 i4FsPbRstCepSvid,
                                                UINT4
                                                *pu4RetValFsPbRstCVlanPortInvalidConfigBpduRxCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortInvalidConfigBpduRxCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortInvalidConfigBpduRxCount =
        pPepPortEntry->u4InvalidConfigBpdusRxdCount;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortInvalidTcnBpduRxCount (INT4 i4FsPbRstPort,
                                             INT4 i4FsPbRstCepSvid,
                                             UINT4
                                             *pu4RetValFsPbRstCVlanPortInvalidTcnBpduRxCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortInvalidTcnBpduRxCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortInvalidTcnBpduRxCount =
        pPepPortEntry->u4InvalidTcnBpdusRxdCount;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortProtocolMigrationCount
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortProtocolMigrationCount (INT4 i4FsPbRstPort,
                                              INT4 i4FsPbRstCepSvid,
                                              UINT4
                                              *pu4RetValFsPbRstCVlanPortProtocolMigrationCount)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pu4RetValFsPbRstCVlanPortProtocolMigrationCount = AST_INIT_VAL;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsPbRstCVlanPortProtocolMigrationCount =
        pPepPortEntry->u4ProtocolMigrationCount;

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbRstCVlanPortEffectivePortState
 Input       :  The Indices
                FsPbRstPort
                FsPbRstCepSvid

                The Object 
                retValFsPbRstCVlanPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbRstCVlanPortEffectivePortState (INT4 i4FsPbRstPort,
                                          INT4 i4FsPbRstCepSvid,
                                          INT4
                                          *pi4RetValFsPbRstCVlanPortEffectivePortState)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "STP System Module status is Disabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPbRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPbRstPort);

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO ((UINT2) i4FsPbRstPort,
                                                RST_DEFAULT_INSTANCE);

    if (pRstPortInfo->bPortEnabled != RST_TRUE)
    {
        *pi4RetValFsPbRstCVlanPortEffectivePortState = RST_PORT_OPER_DISABLED;

        return (INT1) SNMP_SUCCESS;
    }

    if (pCepPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
         (pCepPortEntry, i4FsPbRstCepSvid)) == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        *pi4RetValFsPbRstCVlanPortEffectivePortState = RST_PORT_OPER_DISABLED;

        AstPbRestoreContext ();

        return (INT1) SNMP_SUCCESS;
    }

    if (pPepPortEntry->u1EntryStatus == AST_PORT_OPER_UP)
    {
        *pi4RetValFsPbRstCVlanPortEffectivePortState = RST_PORT_OPER_ENABLED;
    }
    else
    {
        *pi4RetValFsPbRstCVlanPortEffectivePortState = RST_PORT_OPER_DISABLED;
    }

    AstPbRestoreContext ();

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  AstGetPepPortEntryWithCtxtSwitch 
 
 Description :  This function selects the given CEP's C-Vlan context as the
                current context and then returns the PEP entry for the given
                SVLAN. If the PEP entry is NULL it restores back the original
                context.

 Input       :  The Indices
                pCepPortEntry - CEP Port Entry  
                i4VlanId - SVlanId indicates the PEP port Number

 Output      : None 
               
 Returns     : tAstPortEntry -- Return Pep PortEntry 
****************************************************************************/

tAstPortEntry      *
AstGetPepPortEntryWithCtxtSwitch (tAstPortEntry * pCepPortEntry, INT4 i4VlanId)
{
    tAstPort2IndexMap   DummyEntry;
    tAstPort2IndexMap  *pTempPtr = NULL;
    tAstPortEntry      *pPepPortEntry = NULL;

    if (AstPbSelectCvlanContext (pCepPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT : selection of CVLAN context from this port %d "
                      "failed\n", pCepPortEntry->u2PortNo);

        return NULL;
    }

    DummyEntry.u2ProtPort = (UINT2) i4VlanId;

    pTempPtr = (tAstPort2IndexMap *) RBTreeGet (AST_PB_CVLAN_PORT2INDEX_TREE (),
                                                (tRBElem *) & DummyEntry);

    if (pTempPtr == NULL)
    {
        AstPbRestoreContext ();
        return NULL;
    }

    pPepPortEntry = AST_GET_PORTENTRY (pTempPtr->u2PortIndex);

    return pPepPortEntry;
}

/****************************************************************************
 Function    :  AstGetNextPepPortEntry
 Input       :  The Indices
                i4VlanId - SVlanId indicates the PEP port Number

 Output      : None 
               
 Returns     : PortIndex -- Return Pep PortIndex
****************************************************************************/

INT4
AstGetNextPepPortEntry (INT4 i4VlanId)
{
    tAstPort2IndexMap   DummyEntry;
    tAstPort2IndexMap  *pTempPtr = NULL;
    UINT2               u2PepPortIndex = 0;
    tAstPortEntry       *pAstPortEntry = NULL;

    DummyEntry.u2ProtPort = (UINT2) i4VlanId;

    pTempPtr =
        (tAstPort2IndexMap *) RBTreeGetNext (AST_PB_CVLAN_PORT2INDEX_TREE (),
                                             (tRBElem *) & DummyEntry, NULL);

    if (pTempPtr == NULL)
    {
        return 0;
    }

    pAstPortEntry = AST_GET_PORTENTRY (pTempPtr->u2PortIndex);
    if (pAstPortEntry == NULL)
    {
        return 0;
    }

    u2PepPortIndex = pAstPortEntry->u2ProtocolPort;

    return u2PepPortIndex;
}

/****************************************************************************
 Function    :  AstGetNextCepPortEntry
 Input       :  The Indices
                u2InPort - CEP Port Index 

 Output      : None 
               
 Returns     : tAstPortEntry * -- Pointer to next CEP Port Index
****************************************************************************/

tAstPortEntry      *
AstGetNextCepPortEntry (UINT2 u2InPort)
{
    UINT2               u2Port = 0;
    tAstPortEntry      *pPortEntry = NULL;

    u2Port = (UINT2) (u2InPort + 1);
    AST_GET_NEXT_PORT_ENTRY (u2Port, pPortEntry)
    {

        if ((pPortEntry != NULL) &&
            (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT))
        {
            return pPortEntry;
        }
    }

    return NULL;
}
