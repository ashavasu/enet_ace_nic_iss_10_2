/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsrswr.c,v 1.4 2011/03/21 12:38:13 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "rstp.h"
# include  "fsmsrslw.h"
# include  "fsmsrswr.h"
# include  "fsmsrsdb.h"

INT4
GetNextIndexFsDot1dStpExtTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStpExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStpExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot1dStpVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpTxHoldCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpTxHoldCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpTxHoldCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpTxHoldCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpVersion (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpTxHoldCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpTxHoldCount (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot1dStpExtTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsDot1dStpExtPortTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1dStpExtPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1dStpExtPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot1dStpPortProtocolMigrationGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortProtocolMigration
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortAdminEdgePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortAdminEdgePort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortOperEdgePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortOperEdgePort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortAdminPointToPointGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortAdminPointToPoint
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortOperPointToPointGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortOperPointToPoint
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortAdminPathCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot1dStpExtPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot1dStpPortAdminPathCost
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot1dStpPortProtocolMigrationSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpPortProtocolMigration
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpPortAdminEdgePortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpPortAdminEdgePort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpPortAdminPointToPointSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpPortAdminPointToPoint
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpPortAdminPathCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot1dStpPortAdminPathCost
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpPortProtocolMigrationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpPortProtocolMigration (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsDot1dStpPortAdminEdgePortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpPortAdminEdgePort (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpPortAdminPointToPointTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpPortAdminPointToPoint (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsDot1dStpPortAdminPathCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot1dStpPortAdminPathCost (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsDot1dStpExtPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot1dStpExtPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

VOID
RegisterFSMSRS ()
{
    SNMPRegisterMibWithLock (&FsDot1dStpExtTableOID, &FsDot1dStpExtTableEntry,
                             AstLock, AstUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&FsDot1dStpExtPortTableOID,
                             &FsDot1dStpExtPortTableEntry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmsrsOID, (const UINT1 *) "fsmsrst");
}

VOID
UnRegisterFSMSRS ()
{
    SNMPUnRegisterMib (&FsDot1dStpExtTableOID, &FsDot1dStpExtTableEntry);
    SNMPUnRegisterMib (&FsDot1dStpExtPortTableOID,
                       &FsDot1dStpExtPortTableEntry);
    SNMPDelSysorEntry (&fsmsrsOID, (const UINT1 *) "fsmsrst");
}
