/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbrlw.c,v 1.16 2014/03/05 11:46:32 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "asthdrs.h"
# include  "fssnmp.h"
# include "fsmpbrcli.h"
/* LOW LEVEL Routines for Table : FsMIPbRstContextInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbRstContextInfoTable
 Input       :  The Indices
                FsMIPbRstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbRstContextInfoTable (INT4 i4FsMIPbRstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIPbRstContextId) == RST_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((UINT4) i4FsMIPbRstContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        return SNMP_FAILURE;
    }
    if (AST_IS_1AD_BRIDGE () != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbRstContextInfoTable
 Input       :  The Indices
                FsMIPbRstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbRstContextInfoTable (INT4 *pi4FsMIPbRstContextId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetNextIndexFsMIPbRstContextInfoTable (-1, pi4FsMIPbRstContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbRstContextInfoTable
 Input       :  The Indices
                FsMIPbRstContextId
                nextFsMIPbRstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbRstContextInfoTable (INT4 i4FsMIPbContextId,
                                          INT4 *pi4NextFsMIPbContextId)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;

    if (AST_IS_VC_VALID (i4FsMIPbContextId) == RST_FALSE)
    {
        i4FsMIPbContextId = -1;    /* We will give the first entry */
    }

    for (u4ContextId = (UINT4) (i4FsMIPbContextId + 1); u4ContextId <=
         AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) == RST_SUCCESS)
        {
            if (((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ())) &&
                (AST_IS_1AD_BRIDGE () == RST_TRUE))
            {
                *pi4NextFsMIPbContextId = (INT4) u4ContextId;
                AstReleaseContext ();
                return SNMP_SUCCESS;
            }

            AstReleaseContext ();
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbProviderStpStatus
 Input       :  The Indices
                FsMIPbRstContextId

                The Object 
                retValFsMIPbProviderStpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbProviderStpStatus (INT4 i4FsMIPbRstContextId,
                               INT4 *pi4RetValFsMIPbProviderStpStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstSelectContext ((UINT4) i4FsMIPbRstContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbProviderStpStatus (pi4RetValFsMIPbProviderStpStatus);

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbProviderStpStatus
 Input       :  The Indices
                FsMIPbRstContextId

                The Object 
                setValFsMIPbProviderStpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbProviderStpStatus (INT4 i4FsMIPbRstContextId,
                               INT4 i4SetValFsMIPbProviderStpStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstSelectContext ((UINT4) i4FsMIPbRstContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPbProviderStpStatus (i4SetValFsMIPbProviderStpStatus);

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbProviderStpStatus
 Input       :  The Indices
                FsMIPbRstContextId

                The Object 
                testValFsMIPbProviderStpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbProviderStpStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIPbRstContextId,
                                  INT4 i4TestValFsMIPbProviderStpStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (AST_IS_VC_VALID (i4FsMIPbRstContextId) == RST_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((UINT4) i4FsMIPbRstContextId) == RST_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPbProviderStpStatus (pu4ErrorCode,
                                        i4TestValFsMIPbProviderStpStatus);

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbRstContextInfoTable
 Input       :  The Indices
                FsMIPbRstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbRstContextInfoTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbRstCVlanBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbRstCVlanBridgeTable
 Input       :  The Indices
                FsMIPbRstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbRstCVlanBridgeTable (INT4 i4FsMIPbRstPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbRstCVlanBridgeTable (u2LocalPort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbRstCVlanBridgeTable
 Input       :  The Indices
                FsMIPbRstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbRstCVlanBridgeTable (INT4 *pi4FsMIPbRstPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIPbRstCVlanBridgeTable (0, pi4FsMIPbRstPort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbRstCVlanBridgeTable
 Input       :  The Indices
                FsMIPbRstPort
                nextFsMIPbRstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbRstCVlanBridgeTable (INT4 i4FsMIPbRstPort,
                                          INT4 *pi4NextFsMIPbRstPort)
{
    tAstPortEntry       AstPortEntry;
    tAstPortEntry      *pAstNextPortEntry = NULL;

    if (i4FsMIPbRstPort < 0)
    {
        i4FsMIPbRstPort = 0;
    }

    MEMSET (&AstPortEntry, 0, sizeof (tAstPortEntry));
    AstPortEntry.u4IfIndex = i4FsMIPbRstPort;

    while ((pAstNextPortEntry = AstGetNextIfIndexEntry (&AstPortEntry)) != NULL)
    {
        if (AST_GET_BRG_PORT_TYPE (pAstNextPortEntry) == AST_CUSTOMER_EDGE_PORT)
        {
            *pi4NextFsMIPbRstPort = pAstNextPortEntry->u4IfIndex;
            return SNMP_SUCCESS;
        }

        AstPortEntry.u4IfIndex = pAstNextPortEntry->u4IfIndex;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeId
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeId (INT4 i4FsMIPbRstPort,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMIPbRstCVlanBridgeId)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeId (u2LocalPort,
                                           pRetValFsMIPbRstCVlanBridgeId);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeDesignatedRoot
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeDesignatedRoot (INT4 i4FsMIPbRstPort,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMIPbRstCVlanBridgeDesignatedRoot)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeDesignatedRoot
        (u2LocalPort, pRetValFsMIPbRstCVlanBridgeDesignatedRoot);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeRootCost
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeRootCost (INT4 i4FsMIPbRstPort,
                                    INT4 *pi4RetValFsMIPbRstCVlanBridgeRootCost)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeRootCost
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeRootCost);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeMaxAge
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeMaxAge (INT4 i4FsMIPbRstPort,
                                  INT4 *pi4RetValFsMIPbRstCVlanBridgeMaxAge)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeMaxAge
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeMaxAge);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeHelloTime
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeHelloTime (INT4 i4FsMIPbRstPort,
                                     INT4
                                     *pi4RetValFsMIPbRstCVlanBridgeHelloTime)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeHelloTime
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeHelloTime);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeHoldTime
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeHoldTime (INT4 i4FsMIPbRstPort,
                                    INT4 *pi4RetValFsMIPbRstCVlanBridgeHoldTime)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeHoldTime
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeHoldTime);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeForwardDelay
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeForwardDelay (INT4 i4FsMIPbRstPort,
                                        INT4
                                        *pi4RetValFsMIPbRstCVlanBridgeForwardDelay)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeForwardDelay
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeForwardDelay);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanBridgeTxHoldCount
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanBridgeTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanBridgeTxHoldCount (INT4 i4FsMIPbRstPort,
                                       INT4
                                       *pi4RetValFsMIPbRstCVlanBridgeTxHoldCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanBridgeTxHoldCount
        (u2LocalPort, pi4RetValFsMIPbRstCVlanBridgeTxHoldCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpHelloTime
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpHelloTime (INT4 i4FsMIPbRstPort,
                                  INT4 *pi4RetValFsMIPbRstCVlanStpHelloTime)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanStpHelloTime
        (u2LocalPort, pi4RetValFsMIPbRstCVlanStpHelloTime);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpMaxAge
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpMaxAge (INT4 i4FsMIPbRstPort,
                               INT4 *pi4RetValFsMIPbRstCVlanStpMaxAge)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanStpMaxAge
        (u2LocalPort, pi4RetValFsMIPbRstCVlanStpMaxAge);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpForwardDelay
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpForwardDelay (INT4 i4FsMIPbRstPort,
                                     INT4
                                     *pi4RetValFsMIPbRstCVlanStpForwardDelay)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanStpForwardDelay
        (u2LocalPort, pi4RetValFsMIPbRstCVlanStpForwardDelay);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpTopChanges
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpTopChanges (INT4 i4FsMIPbRstPort,
                                   UINT4 *pu4RetValFsMIPbRstCVlanStpTopChanges)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanStpTopChanges
        (u2LocalPort, pu4RetValFsMIPbRstCVlanStpTopChanges);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpTimeSinceTopologyChange
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpTimeSinceTopologyChange (INT4 i4FsMIPbRstPort,
                                                UINT4
                                                *pu4RetValFsMIPbRstCVlanStpTimeSinceTopologyChange)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanStpTimeSinceTopologyChange
        (u2LocalPort, pu4RetValFsMIPbRstCVlanStpTimeSinceTopologyChange);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanStpDebugOption
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                retValFsMIPbRstCVlanStpDebugOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanStpDebugOption (INT4 i4FsMIPbRstPort,
                                    INT4 *pi4RetValFsMIPbRstCVlanStpDebugOption)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (u2LocalPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      u2LocalPort);
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsMIPbRstCVlanStpDebugOption = AST_INIT_VAL;

        AstPbRestoreContext ();
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMIPbRstCVlanStpDebugOption = (INT4) AST_DEBUG_OPTION;

    AstPbRestoreContext ();
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPbRstCVlanStpDebugOption
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                setValFsMIPbRstCVlanStpDebugOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPbRstCVlanStpDebugOption (INT4 i4FsMIPbRstPort,
                                    INT4 i4SetValFsMIPbRstCVlanStpDebugOption)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT4               u4SeqNum = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_SUCCESS;
    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP OR MSTP Module status is Disabled!\n");
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (u2LocalPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);

    /*Since CVLAN component is spawned as a separate context.We do
     * context switching at each time when we need CVLAN context
     * information*/

    if (AstPbSelectCvlanContext (pAstPortEntry) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT: Selecting context for this CEP Port %d failed\n",
                      u2LocalPort);
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        AstPbRestoreContext ();
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    AST_DEBUG_OPTION = (UINT4) i4SetValFsMIPbRstCVlanStpDebugOption;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPbRstCVlanStpDebugOption,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIPbRstCVlanStpDebugOption));

    AstPbRestoreContext ();
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPbRstCVlanStpDebugOption
 Input       :  The Indices
                FsMIPbRstPort

                The Object 
                testValFsMIPbRstCVlanStpDebugOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPbRstCVlanStpDebugOption (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIPbRstPort,
                                       INT4
                                       i4TestValFsMIPbRstCVlanStpDebugOption)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((!(AST_IS_RST_STARTED ())) && (!(AST_IS_MST_STARTED ())))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (u2LocalPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);

    if (AST_PB_IS_PORT_SVLAN_CEP (pAstPortEntry) != RST_TRUE)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "MGMT : This Port %d is not of type CEP. It is not a CVLAN"
                      "component\n", u2LocalPort);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_CEP_ERR);
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMIPbRstCVlanStpDebugOption < AST_MIN_DEBUG_VAL) ||
        (i4TestValFsMIPbRstCVlanStpDebugOption > RST_MAX_DEBUG_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPbRstCVlanBridgeTable
 Input       :  The Indices
                FsMIPbRstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPbRstCVlanBridgeTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPbRstCVlanPortInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortInfoTable (INT4 i4FsMIPbRstPort,
                                                     INT4 i4FsMIPbRstCepSvid)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable
        (u2LocalPort, i4FsMIPbRstCepSvid);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortInfoTable (INT4 *pi4FsMIPbRstPort,
                                             INT4 *pi4FsMIPbRstCepSvid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (0, pi4FsMIPbRstPort,
                                                    0, pi4FsMIPbRstCepSvid);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbRstCVlanPortInfoTable
 Input       :  The Indices
                FsMIPbRstPort
                nextFsMIPbRstPort
                FsMIPbRstCepSvid
                nextFsMIPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (INT4 i4FsMIPbRstPort,
                                            INT4 *pi4NextFsMIPbRstPort,
                                            INT4 i4FsMIPbRstCepSvid,
                                            INT4 *pi4NextFsMIPbRstCepSvid)
{
    tAstPortEntry       AstPortEntry;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    if (i4FsMIPbRstCepSvid < 0)
    {
        i4FsMIPbRstCepSvid = 0;
    }

    if (i4FsMIPbRstPort < 0)
    {
        i4FsMIPbRstPort = 0;
    }
    else
    {
        pAstPortEntry = AstGetIfIndexEntry (i4FsMIPbRstPort);
        if (pAstPortEntry != NULL)
        {
            i4RetVal = AstGetNextCVlanPort (pAstPortEntry, i4FsMIPbRstCepSvid,
                                            pi4NextFsMIPbRstCepSvid);
            if (i4RetVal == RST_SUCCESS)
            {
                *pi4NextFsMIPbRstPort = pAstPortEntry->u4IfIndex;
                return SNMP_SUCCESS;
            }
        }
    }

    MEMSET (&AstPortEntry, 0, sizeof (tAstPortEntry));
    AstPortEntry.u4IfIndex = i4FsMIPbRstPort;

    while ((pAstPortEntry = AstGetNextIfIndexEntry (&AstPortEntry)) != NULL)
    {
        i4FsMIPbRstCepSvid = 0;

        i4RetVal = AstGetNextCVlanPort (pAstPortEntry, i4FsMIPbRstCepSvid,
                                        pi4NextFsMIPbRstCepSvid);
        if (i4RetVal == RST_SUCCESS)
        {
            *pi4NextFsMIPbRstPort = pAstPortEntry->u4IfIndex;
            return SNMP_SUCCESS;
        }
        AstPortEntry.u4IfIndex = pAstPortEntry->u4IfIndex;
    }

    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstGetNextCVlanPort                                  */
/*                                                                           */
/* Description        : This function gets the next port in the c-vlan comp. */
/*                      If the given port is not a CEP, then it returns      */
/*                      failure.                                             */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the port entry.              */
/*                      i4Svid - Svlan id of the c-vlan port.                */
/*                                                                           */
/* Output(s)          : pi4NextSvid - Next svlan id of the c-vlan port.      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetNextCVlanPort (tAstPortEntry * pPortEntry, INT4 i4Svid, INT4 *pi4NextSvid)
{
    INT4                i4NextLocalPort = 0;

    if (AST_GET_BRG_PORT_TYPE (pPortEntry) != AST_CUSTOMER_EDGE_PORT)
    {
        return RST_FAILURE;
    }

    if (AstSelectContext (pPortEntry->u4ContextId) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    if (nmhGetNextIndexFsPbRstCVlanPortInfoTable
        (AST_IFENTRY_LOCAL_PORT (pPortEntry), &i4NextLocalPort,
         i4Svid, pi4NextSvid) != SNMP_FAILURE)
    {
        if (i4NextLocalPort == AST_IFENTRY_LOCAL_PORT (pPortEntry))
        {
            AstReleaseContext ();
            return RST_SUCCESS;
        }
    }

    AstReleaseContext ();
    return RST_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortPriority
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortPriority (INT4 i4FsMIPbRstPort, INT4 i4FsMIPbRstCepSvid,
                                  INT4 *pi4RetValFsMIPbRstCVlanPortPriority)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortPriority
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValFsMIPbRstCVlanPortPriority);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortPathCost
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortPathCost (INT4 i4FsMIPbRstPort, INT4 i4FsMIPbRstCepSvid,
                                  INT4 *pi4RetValFsMIPbRstCVlanPortPathCost)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortPathCost
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValFsMIPbRstCVlanPortPathCost);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortRole
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortRole (INT4 i4FsMIPbRstPort, INT4 i4FsMIPbRstCepSvid,
                              INT4 *pi4RetValFsMIPbRstCVlanPortRole)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortRole
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValFsMIPbRstCVlanPortRole);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortState (INT4 i4FsMIPbRstPort, INT4 i4FsMIPbRstCepSvid,
                               INT4 *pi4RetValFsMIPbRstCVlanPortState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValFsMIPbRstCVlanPortState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortAdminEdgePort
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortAdminEdgePort (INT4 i4FsMIPbRstPort,
                                       INT4 i4FsMIPbRstCepSvid,
                                       INT4 *pi4RetValRstCVlanPortAdminEdgePort)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortAdminEdgePort
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortAdminEdgePort);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortOperEdgePort
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortOperEdgePort (INT4 i4FsMIPbRstPort,
                                      INT4 i4FsMIPbRstCepSvid,
                                      INT4 *pi4RetValRstCVlanPortOperEdgePort)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortOperEdgePort
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortOperEdgePort);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortAdminPointToPoint
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortAdminPointToPoint (INT4 i4FsMIPbRstPort,
                                           INT4 i4FsMIPbRstCepSvid,
                                           INT4
                                           *pi4RetValRstCVlanPortAdminPointToPoint)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortAdminPointToPoint
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pi4RetValRstCVlanPortAdminPointToPoint);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortOperPointToPoint
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortOperPointToPoint (INT4 i4FsMIPbRstPort,
                                          INT4 i4FsMIPbRstCepSvid,
                                          INT4
                                          *pi4RetValRstCVlanPortOperPointToPoint)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortOperPointToPoint
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pi4RetValRstCVlanPortOperPointToPoint);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortAutoEdge
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortAutoEdge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortAutoEdge (INT4 i4FsMIPbRstPort, INT4 i4FsMIPbRstCepSvid,
                                  INT4 *pi4RetValFsMIPbRstCVlanPortAutoEdge)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortAutoEdge
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValFsMIPbRstCVlanPortAutoEdge);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortDesignatedRoot
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortDesignatedRoot (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValRstCVlanPortDesignatedRoot)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortDesignatedRoot
        (u2LocalPort, i4FsMIPbRstCepSvid, pRetValRstCVlanPortDesignatedRoot);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortDesignatedCost
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortDesignatedCost (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        INT4
                                        *pi4RetValRstCVlanPortDesignatedCost)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortDesignatedCost
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortDesignatedCost);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortDesignatedBridge
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortDesignatedBridge (INT4 i4FsMIPbRstPort,
                                          INT4 i4FsMIPbRstCepSvid,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValRstCVlanPortDesignatedBridge)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortDesignatedBridge
        (u2LocalPort, i4FsMIPbRstCepSvid, pRetValRstCVlanPortDesignatedBridge);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortDesignatedPort
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortDesignatedPort (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIPbRstCVlanPortDesignatedPort)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortDesignatedPort
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pRetValFsMIPbRstCVlanPortDesignatedPort);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortForwardTransitions
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortForwardTransitions (INT4 i4FsMIPbRstPort,
                                            INT4 i4FsMIPbRstCepSvid,
                                            UINT4
                                            *pu4RetValRstCVlanPortForwardTransitions)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortForwardTransitions
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RetValRstCVlanPortForwardTransitions);

    AstReleaseContext ();
    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FsMIPbRstCVlanPortSmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbRstCVlanPortSmTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortSmTable (INT4 i4FsMIPbRstPort,
                                                   INT4 i4FsMIPbRstCepSvid)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbRstCVlanPortSmTable
        (u2LocalPort, i4FsMIPbRstCepSvid);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbRstCVlanPortSmTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortSmTable (INT4 *pi4FsMIPbRstPort,
                                           INT4 *pi4FsMIPbRstCepSvid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetNextIndexFsMIPbRstCVlanPortSmTable (0, pi4FsMIPbRstPort,
                                                  0, pi4FsMIPbRstCepSvid);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbRstCVlanPortSmTable
 Input       :  The Indices
                FsMIPbRstPort
                nextFsMIPbRstPort
                FsMIPbRstCepSvid
                nextFsMIPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbRstCVlanPortSmTable (INT4 i4FsMIPbRstPort,
                                          INT4 *pi4NextFsMIPbRstPort,
                                          INT4 i4FsMIPbRstCepSvid,
                                          INT4 *pi4NextFsMIPbRstCepSvid)
{
    return
        (nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4FsMIPbRstPort,
                                                     pi4NextFsMIPbRstPort,
                                                     i4FsMIPbRstCepSvid,
                                                     pi4NextFsMIPbRstCepSvid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortInfoSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortInfoSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortInfoSmState (INT4 i4FsMIPbRstPort,
                                     INT4 i4FsMIPbRstCepSvid,
                                     INT4 *pi4RetValRstCVlanPortInfoSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortInfoSmState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortInfoSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortMigSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortMigSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortMigSmState (INT4 i4FsMIPbRstPort,
                                    INT4 i4FsMIPbRstCepSvid,
                                    INT4 *pi4RetValRstCVlanPortMigSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortMigSmState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortMigSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortRoleTransSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortRoleTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortRoleTransSmState (INT4 i4FsMIPbRstPort,
                                          INT4 i4FsMIPbRstCepSvid,
                                          INT4
                                          *pi4RetValRstCVlanPortRoleTransSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortRoleTransSmState
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pi4RetValRstCVlanPortRoleTransSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortStateTransSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortStateTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortStateTransSmState (INT4 i4FsMIPbRstPort,
                                           INT4 i4FsMIPbRstCepSvid,
                                           INT4
                                           *pi4RetValRstCVlanPortStateTransSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortStateTransSmState
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pi4RetValRstCVlanPortStateTransSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortTopoChSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortTopoChSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortTopoChSmState (INT4 i4FsMIPbRstPort,
                                       INT4 i4FsMIPbRstCepSvid,
                                       INT4 *pi4RetValRstCVlanPortTopoChSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortTopoChSmState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortTopoChSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortTxSmState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortTxSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortTxSmState (INT4 i4FsMIPbRstPort,
                                   INT4 i4FsMIPbRstCepSvid,
                                   INT4 *pi4RetValRstCVlanPortTxSmState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortTxSmState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RetValRstCVlanPortTxSmState);

    AstReleaseContext ();
    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FsMIPbRstCVlanPortStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortStatsTable (INT4 i4FsMIPbRstPort,
                                                      INT4 i4FsMIPbRstCepSvid)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable
        (u2LocalPort, i4FsMIPbRstCepSvid);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortStatsTable (INT4 *pi4FsMIPbRstPort,
                                              INT4 *pi4FsMIPbRstCepSvid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetNextIndexFsMIPbRstCVlanPortStatsTable (0, pi4FsMIPbRstPort,
                                                     0, pi4FsMIPbRstCepSvid);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPbRstCVlanPortStatsTable
 Input       :  The Indices
                FsMIPbRstPort
                nextFsMIPbRstPort
                FsMIPbRstCepSvid
                nextFsMIPbRstCepSvid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPbRstCVlanPortStatsTable (INT4 i4FsMIPbRstPort,
                                             INT4 *pi4NextFsMIPbRstPort,
                                             INT4 i4FsMIPbRstCepSvid,
                                             INT4 *pi4NextFsMIPbRstCepSvid)
{
    return
        (nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4FsMIPbRstPort,
                                                     pi4NextFsMIPbRstPort,
                                                     i4FsMIPbRstCepSvid,
                                                     pi4NextFsMIPbRstCepSvid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortRxRstBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortRxRstBpduCount (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        UINT4
                                        *pu4RetValFsMIPbRstCVlanPortRxRstBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortRxRstBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RetValFsMIPbRstCVlanPortRxRstBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortRxConfigBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortRxConfigBpduCount (INT4 i4FsMIPbRstPort,
                                           INT4 i4FsMIPbRstCepSvid,
                                           UINT4
                                           *pu4RetValRstCVlanPortRxConfigBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortRxConfigBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RetValRstCVlanPortRxConfigBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortRxTcnBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortRxTcnBpduCount (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        UINT4
                                        *pu4RetValRstCVlanPortRxTcnBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortRxTcnBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid, pu4RetValRstCVlanPortRxTcnBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortTxRstBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortTxRstBpduCount (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        UINT4
                                        *pu4RetValRstCVlanPortTxRstBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortTxRstBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid, pu4RetValRstCVlanPortTxRstBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortTxConfigBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortTxConfigBpduCount (INT4 i4FsMIPbRstPort,
                                           INT4 i4FsMIPbRstCepSvid,
                                           UINT4
                                           *pu4RetValRstCVlanPortTxConfigBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortTxConfigBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RetValRstCVlanPortTxConfigBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortTxTcnBpduCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortTxTcnBpduCount (INT4 i4FsMIPbRstPort,
                                        INT4 i4FsMIPbRstCepSvid,
                                        UINT4
                                        *pu4RetValRstCVlanPortTxTcnBpduCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortTxTcnBpduCount
        (u2LocalPort, i4FsMIPbRstCepSvid, pu4RetValRstCVlanPortTxTcnBpduCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortInvalidRstBpduRxCount (INT4 i4FsMIPbRstPort,
                                               INT4 i4FsMIPbRstCepSvid,
                                               UINT4
                                               *pu4RstCVlanPortInvalidRstBpduRxCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortInvalidRstBpduRxCount
        (u2LocalPort, i4FsMIPbRstCepSvid, pu4RstCVlanPortInvalidRstBpduRxCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortInvalidConfigBpduRxCount (INT4 i4FsMIPbRstPort,
                                                  INT4 i4FsMIPbRstCepSvid,
                                                  UINT4
                                                  *pu4RstCVlanPortInvalidConfigBpduRxCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortInvalidConfigBpduRxCount
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RstCVlanPortInvalidConfigBpduRxCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortInvalidTcnBpduRxCount (INT4 i4FsMIPbRstPort,
                                               INT4 i4FsMIPbRstCepSvid,
                                               UINT4
                                               *pu4RstCVlanPortInvalidTcnBpduRxCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortInvalidTcnBpduRxCount
        (u2LocalPort, i4FsMIPbRstCepSvid, pu4RstCVlanPortInvalidTcnBpduRxCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortProtocolMigrationCount
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortProtocolMigrationCount (INT4 i4FsMIPbRstPort,
                                                INT4 i4FsMIPbRstCepSvid,
                                                UINT4
                                                *pu4RstCVlanPortProtocolMigrationCount)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortProtocolMigrationCount
        (u2LocalPort, i4FsMIPbRstCepSvid,
         pu4RstCVlanPortProtocolMigrationCount);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPbRstCVlanPortEffectivePortState
 Input       :  The Indices
                FsMIPbRstPort
                FsMIPbRstCepSvid

                The Object 
                retValFsMIPbRstCVlanPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPbRstCVlanPortEffectivePortState (INT4 i4FsMIPbRstPort,
                                            INT4 i4FsMIPbRstCepSvid,
                                            INT4
                                            *pi4RstCVlanPortEffectivePortState)
{
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPbRstPort, &u4ContextId,
                                      &u2LocalPort) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPbRstCVlanPortEffectivePortState
        (u2LocalPort, i4FsMIPbRstCepSvid, pi4RstCVlanPortEffectivePortState);

    AstReleaseContext ();
    return i1RetVal;

}
