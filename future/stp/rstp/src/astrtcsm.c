/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrtcsm.c,v 1.32 2017/11/20 13:12:26 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Topology Change State Event Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

#ifdef NPAPI_WANTED
extern INT4         gi4AstInitProcess;
#endif

/*****************************************************************************/
/* Function Name      : RstTopoChMachine                                     */
/*                                                                           */
/* Description        : This is the main routine for the Topology Change     */
/*                      State Machine. This function is responsible for      */
/*                      topology change detection, notification & propagation*/
/*                      and for filtering database flushing.                 */
/*                                                                           */
/* Input(s)           : u1Event - The event that has caused this Topology    */
/*                                Change State Machine to be called          */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2State = (UINT1) AST_INIT_VAL;

    u2State = (UINT2) pPerStPortInfo->u1TopoChSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: Topo Ch Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_TCSM][u2Event],
                  gaaau1AstSemState[AST_TCSM][u2State]);

    AST_DBG_ARG3 (AST_TCSM_DBG,
                  "TCSM: Port %s: Topo Ch Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_TCSM][u2Event],
                  gaaau1AstSemState[AST_TCSM][u2State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_TOPOCHSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_TCSM_DBG,
                 "TCSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_TOPO_CH_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_TCSM_DBG, "TCSM: No Operations to perform\n");
        return RST_SUCCESS;
    }

    if ((*(RST_TOPO_CH_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: Topology Change Event Function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeInactive                              */
/*                                                                           */
/* Description        : This function is called during the initialisation of */
/*                      the Topology Change State Machine and also if the    */
/*                      Port is not in the RootPort or DesignatedPort roles. */
/*                      This changes the state of the Topology Change state  */
/*                      machine to the INACTIVE state.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeInactive (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_TCWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: AstStopTimer for TcWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

#ifdef MRP_WANTED
    if (pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPerStPortInfo,
             (UINT1) AST_TMR_TYPE_TCDETECTED) != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: AstStopTimer for TcDetectedTmr FAILED!\n");
            return RST_FAILURE;
        }
    }
#endif
    pAstCommPortInfo->bTcAck = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_Inactive: Port %s: FdbFlush = TRUE tcWhile = 0 TcAck = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_INACTIVE;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state INACTIVE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /*Flush port when it is operationally disabled or when it becomes an alternate port.
       Flushing is not done when a port is disabled in RSTP by management */
    if (pRstPortInfo->bPortEnabled != RST_FALSE)
    {
        RstTopoChSmFlushFdb (pPerStPortInfo->u2PortNo, VLAN_NO_OPTIMIZE);
    }

    if ((pRstPortInfo->bLearn == RST_TRUE) &&
        ((AST_CURR_CONTEXT_INFO ())->bBegin == RST_FALSE))
    {
        return RstTopoChSmMakeLearning (pPerStPortInfo);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeLearning                              */
/*                                                                           */
/* Description        : This function is called whenever the Port is a Backup*/
/*                      or Alternate Port (i.e. until the Port becomes a Root*/
/*                      or Designated Port. This is called when the Topology */
/*                      Change State machine is in the INIT state and this   */
/*                      function changes the state to the LEARNING state.    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeLearning (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    pPerStRstPortInfo->bRcvdTc = RST_FALSE;
    pAstCommPortInfo->bRcvdTcn = RST_FALSE;
    pAstCommPortInfo->bRcvdTcAck = RST_FALSE;
    pPerStRstPortInfo->bTc = RST_FALSE;
    pPerStRstPortInfo->bTcProp = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_Learning: Port %s: RcvdTc = RcvdTcn = RcvdTcAck = Tc = TcProp = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_LEARNING;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state LEARNING\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstTopoChSmChkDetected (pPerStPortInfo) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    if (RstTopoChSmChkInactive (pPerStPortInfo) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmChkInactive                               */
/*                                                                           */
/* Description        : This function is called to check whether the port    */
/*                      can be moved to the INACTIVE state from the          */
/*                      LEARNING state.                                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmChkInactive (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if ((pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_ROOT)
        &&
        (pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_DESIGNATED)
        &&
        ((pRstPortInfo->bLearn == RST_FALSE) &&
         (pRstPortInfo->bLearning == RST_FALSE))
        &&
        ((pRstPortInfo->bRcvdTc == RST_FALSE) &&
         (pAstCommPortInfo->bRcvdTcn == RST_FALSE) &&
         (pAstCommPortInfo->bRcvdTcAck == RST_FALSE) &&
         (pRstPortInfo->bTcProp == RST_FALSE)))
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s Role is NOT ROOT/DESIGNATED; Changing to INACTIVE state\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstTopoChSmMakeInactive (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: RstTopoChSmMakeInactive function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmChkDetected                               */
/*                                                                           */
/* Description        : This function is called to check whether the port    */
/*                      can be moved to the DETECTED state from the          */
/*                      LEARNING state.                                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmChkDetected (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT) ||
         (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED))
        &&
        (pRstPortInfo->bForward == RST_TRUE)
        && (pPortInfo->bOperEdgePort == RST_FALSE))
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s Role is ROOT/DESIGNATED; Changing to DETECTED state\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstTopoChSmMakeDetected (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: RstTopoChSmMakeDetected function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeDetected                              */
/*                                                                           */
/* Description        : This function is called whenever the Port has        */
/*                      detected a topology change. This is called when the  */
/*                      Topology Change state machine is in the ACTIVE state */
/*                      and this function changes the state to the DETECTED  */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeDetected (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT4               u4Ticks = 0;
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: TC has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %s: TC has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    pAstCommPortInfo->bNewInfo = RST_TRUE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_Detected: Port %s: NewInfo = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /*Incrementing the TC detected count */
    AST_INCR_TC_DETECTED_COUNT (pPortInfo->u2PortNo);
    OsixGetSysTime (&u4Ticks);
    pPortInfo->u4TCDetectedTimeStamp = u4Ticks;

    if (RstTopoChSmNewTcWhile (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmNewTcWhile function returned FAILURE!\n");
        return RST_FAILURE;
    }

    if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                pPortInfo, RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstTopoChSmSetTcPropTree (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmSetTcPropTree function returned FAILURE!\n");
        return RST_FAILURE;
    }

#ifdef MRP_WANTED
    if (RstTopoChSmNewTcDetected (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmNewTcDetected function returned "
                 "FAILURE!\n");
        return RST_FAILURE;
    }
#endif
    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_DETECTED;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state DETECTED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    RstTopoChSmMakeActive (pPerStPortInfo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeActive                                */
/*                                                                           */
/* Description        : This function is called from all the other states of */
/*                      this state machine excepting the INACTIVE and        */
/*                      LEARNING states, and control transitions             */
/*                      unconditionally to the ACTIVE state.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeActive (tAstPerStPortInfo * pPerStPortInfo)
{
    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_ACTIVE;
    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state ACTIVE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeNotifiedTcn                           */
/*                                                                           */
/* Description        : This function is called whenever a TCN Bpdu has been */
/*                      received on this Port. This is called when the       */
/*                      Topology Change state machine is in the ACTIVE state */
/*                      and this function changes the state to the NOTIFIED_ */
/*                      TCN state.                                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeNotifiedTcn (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: RCVDTCN has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %s: RCVDTCN has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstTopoChSmNewTcWhile (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmNewTcWhile function returned FAILURE!\n");
        return RST_FAILURE;
    }

    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_NOTIFIED_TCN;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state NOTIFIED_TCN\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pAstCommPortInfo->bNewInfo == RST_TRUE)
    {
        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "TCSM_RcvdTcn: Port %s: NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, RST_DEFAULT_INSTANCE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if (RstTopoChSmMakeNotifiedTc (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmMakeNotifiedTc function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeNotifiedTc                            */
/*                                                                           */
/* Description        : This function is called whenever a Bpdu with the TC  */
/*                      flag set has been received on this Port. This is     */
/*                      called unconditionally when the Topology Change State*/
/*                      is in the NOTIFIED_TCN state or from the ACTIVE state*/
/*                      on receipt of the Bpdu and this function changes the */
/*                      state to the NOTIFIED_TC state.                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeNotifiedTc (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: RCVDTC has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %s: RCVDTC has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->PerStRstPortInfo.bRcvdTc = RST_FALSE;
    pAstCommPortInfo->bRcvdTcn = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_NotifiedTc: Port %s: RcvdTc = RcvdTcn = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
    {
        pAstCommPortInfo->bTcAck = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "TCSM_NotifiedTc: Port %s: TcAck = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_NotifiedTc: Port %s: TcProp = TRUE for all ports EXCEPT this port\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_NOTIFIED_TC;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state NOTIFIED_TC\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstTopoChSmSetTcPropTree (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: RstTopoChSmSetTcPropTree function returned FAILURE!\n");
        return RST_FAILURE;
    }

    RstTopoChSmMakeActive (pPerStPortInfo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakePropagating                           */
/*                                                                           */
/* Description        : This function is called if this Port is not an Edge  */
/*                      Port and its TcProp variable has been set by some    */
/*                      other port of this bridge indicating the need to     */
/*                      propagate a topology change. This is called when the */
/*                      Topology Change State Machine is in the ACTIVE state */
/*                      and this function changes the state to the           */
/*                      PROPAGATING state.                                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakePropagating (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: TCPROP has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %s: TCPROP has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pAstPortEntry->bOperEdgePort != RST_TRUE)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s is NOT an EDGE port, hence PROPAGATING\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstTopoChSmNewTcWhile (pPerStPortInfo) != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: RstTopoChSmNewTcWhile function returned FAILURE!\n");
            return RST_FAILURE;
        }

        /* Flushing on this port because TcProp is set. Can optimize this (since
         * TcProp would have been set on all ports on the bridge except one) by
         * combining all the flush calls into a single flush for the whole
         * bridge. The disadvantage here is that Edge ports (for which TcProp
         * will not be set) will also be flushed. */
        RstTopoChSmFlushFdb (pPerStPortInfo->u2PortNo, VLAN_OPTIMIZE);

        pPerStPortInfo->PerStRstPortInfo.bTcProp = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "TCSM_Propogating: Port %s: FdbFlush = TRUE TcProp = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        pPerStPortInfo->u1TopoChSmState =
            (UINT1) RST_TOPOCHSM_STATE_PROPAGATING;

        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s: Moved to state PROPAGATING\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (pAstCommPortInfo->bNewInfo == RST_TRUE)
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TCSM_Propagating: Port %s: NewInfo = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                        pAstPortEntry, RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }

        RstTopoChSmMakeActive (pPerStPortInfo);

    }                            /* End of Oper Edge check */
    else
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "TCSM: Port %s is an EDGE port, hence NO PROPAGATION\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s is an EDGE port, hence NO PROPAGATION\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmMakeAcknowledged                          */
/*                                                                           */
/* Description        : This function is called if a Bpdu is received on this*/
/*                      Port with the TC Acknowledge Flag set. This is called*/
/*                      when the Topology Change State Machine is in the     */
/*                      ACTIVE state and this function changes the state to  */
/*                      the ACKNOWLEDGED state.                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmMakeAcknowledged (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: RCVDTCACK has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %s: RCVDTCACK has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_TCWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: AstStopTimer for TcWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->bRcvdTcAck = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "TCSM_Acknowledged: Port %s: tcWhile = 0 RcvdTcAck = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_ACKNOWLEDGED;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %s: Moved to state ACKNOWLEDGED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    RstTopoChSmMakeActive (pPerStPortInfo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmFlushFdb                                  */
/*                                                                           */
/* Description        : This function performs the functionality of flushing */
/*                      the Filtering Database to remove the information that*/
/*                      was learnt on this Port.                             */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the Port whose learnt */
/*                                  information needs to be flushed          */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                                       grouped with the flush call for     */
/*                                       other ports as a single bridge flush*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstTopoChSmFlushFdb (UINT2 u2PortNum, INT4 i4OptimizeFlag)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        return;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo == NULL)
    {
        return;
    }

    /* Shift Learnt information from Retiring Ports - To Do */

    if (pPortInfo->bOperEdgePort == RST_TRUE)
    {
        return;
    }

    /* In Asynchronous NPAPI mode to avoid the stale FDB entires, flushing can be
     * done after successful port state change indication from hardware */
    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        if ((pPerStPortInfo->i4NpPortStateStatus == AST_WAITING_FOR_CALLBACK) &&
            (AST_GET_SYSTEMACTION != RST_DISABLED) &&
            (pRstPortInfo->bPortEnabled == RST_TRUE))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "TCSM: Flush on Port %s is not done\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return;
        }
    }
    if (AstIsVlanEnabledInContext () == RST_TRUE)
    {
        if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
        {
            /* Need not set Short Ageout time again for this port 
             * when ShortAgeout Duration timer is running */
            if (pCommPortInfo->pRapidAgeDurtnTmr == NULL)
            {
                AstVlanSetShortAgeoutTime (pPortInfo);
            }

            /* FdbFlush has been set. Short Ageout time should be applied
             * for a duration of FwdDelay more seconds. Hence restart the 
             * duration timer (even if it is already running) */
            /* Start the duration for FwdDelay+(100 centi-seconds) so that the
             * forwarding module gets to ageout entries atleast once at
             * the end of FwdDelay seconds before reverting back to 
             * long ageout */
            if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                               (UINT1) AST_TMR_TYPE_RAPIDAGE_DURATION,
                               (UINT2) (pPortInfo->DesgTimes.u2ForwardDelay +
                                        (1 * AST_CENTI_SECONDS))) !=
                RST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: AstStartTimer for Rapid Age duration FAILED!\n");
            }
        }
        else
        {
            AstPbVlanDeleteFdbEntries (pPortInfo, (UINT1) i4OptimizeFlag);

        }
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Learnt Entries on Port %s have been flushed!\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Learnt Entries on Port %s have been flushed!\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmSetTcPropTree                             */
/*                                                                           */
/* Description        : This function sets the variable TcProp to TRUE for   */
/*                      all the Ports except the Port that called this       */
/*                      procedure.                                           */
/*                      This procedure also increments the topology change   */
/*                      count and generates topology change trap if tcWhile  */
/*                      timer is not running for any of the ports.           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP)  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmSetTcPropTree (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = 1;
    INT4                i4RetVal = RST_FAILURE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "SetTcPropTree: Port %s: TcProp = TRUE for all ports except this port\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    /* restrictedTCN causes the Port not to propagate received topology change 
     * notifications and topology changes to other Ports.*/

    /* Hence if restrictedTCN is set for the port that is invoking this 
     * procedure, then do nothing. */
    if (AST_PORT_RESTRICTED_TCN (pAstPortEntry) == (tAstBoolean) RST_TRUE)
    {
        return RST_SUCCESS;
    }

    if ((AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE) ||
        (AST_IS_VIRTUAL_INST_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE))
    {
        i4RetVal = RstProviderSetTcPropTree (pPerStPortInfo->u2PortNo);
        return i4RetVal;
    }

    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pAstPortEntry)
    {
        if (pAstPortEntry == NULL)
        {
            continue;
        }
        pAstPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

        pRstPortInfo = AST_GET_RST_PORT_INFO (pAstPerStPortInfo);

        if (pAstPerStPortInfo == pPerStPortInfo)
        {
            continue;
        }

        if (!((AST_IS_PORT_UP (u2PortNum)) &&
              (pRstPortInfo->bPortEnabled == RST_TRUE)))
        {
            continue;
        }
        pAstPerStPortInfo->PerStRstPortInfo.bTcProp = RST_TRUE;

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "TCSM: Propagating Topology Change Info (TcProp) on Port %s\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Propagating Topology Change Info (TcProp) on Port %s\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        if (pAstPerStPortInfo->u1TopoChSmState != RST_TOPOCHSM_STATE_LEARNING)
        {
            if (RstTopoChMachine
                ((UINT1) RST_TOPOCHSM_EV_TCPROP,
                 pAstPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "TCSM: RstTopoChMachine Entry function "
                         "returned FAILURE!\n");
                AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: RstTopoChMachine Entry function "
                         "returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
        else
        {

            pAstCommPortInfo =
                AST_GET_COMM_PORT_INFO (pAstPerStPortInfo->u2PortNo);

            if (pAstCommPortInfo == NULL)
            {
                continue;
            }

            pRstPortInfo->bRcvdTc = RST_FALSE;
            pAstCommPortInfo->bRcvdTcn = RST_FALSE;
            pAstCommPortInfo->bRcvdTcAck = RST_FALSE;
            pRstPortInfo->bTc = RST_FALSE;
            pRstPortInfo->bTcProp = RST_FALSE;
            pAstPerStPortInfo->u1TopoChSmState =
                (UINT1) RST_TOPOCHSM_STATE_LEARNING;

            if (((pAstPerStPortInfo->u1PortRole ==
                  (UINT1) AST_PORT_ROLE_ROOT) ||
                 (pAstPerStPortInfo->u1PortRole ==
                  (UINT1) AST_PORT_ROLE_DESIGNATED))
                &&
                (pRstPortInfo->bForward == RST_TRUE)
                && (pAstPortEntry->bOperEdgePort == RST_FALSE))
            {

                pAstCommPortInfo->bNewInfo = RST_TRUE;

                if (RstTopoChSmNewTcWhile (pAstPerStPortInfo) != RST_SUCCESS)
                {
                    return RST_FAILURE;
                }
                if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                            pAstPortEntry,
                                            RST_DEFAULT_INSTANCE) !=
                    RST_SUCCESS)
                {
                    return RST_FAILURE;
                }

                pRstPortInfo->bTcProp = RST_TRUE;
                pAstPerStPortInfo->u1TopoChSmState =
                    (UINT1) RST_TOPOCHSM_STATE_DETECTED;
                RstTopoChSmMakeActive (pAstPerStPortInfo);
            }
            if (RstTopoChSmChkInactive (pPerStPortInfo) != RST_SUCCESS)
            {
                return RST_FAILURE;
            }

        }
    }
    return RST_SUCCESS;
}

#ifdef MRP_WANTED
/*****************************************************************************/
/* Function Name      : RstTopoChSmNewTcDetected                             */
/*                                                                           */
/* Description       : This function starts the tcDetected timer with        */
/*                     duration equal to Hello Time + 1 seconds when partner */
/*                     bridge port is RSTP capable or equal to Root Bridge   */
/*                     MaxAge and Forward Delay value otherwise.             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmNewTcDetected (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2TcDetectedDuration = AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    if (pAstCommPortInfo->bSendRstp == RST_TRUE)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s: Partner Bridge Port is RSTP capable\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        u2TcDetectedDuration =
            (UINT2) ((pAstPortEntry->DesgTimes.u2HelloTime) +
                     (1 * AST_CENTI_SECONDS));
    }
    else
    {
        u2TcDetectedDuration = (UINT2) ((pBrgInfo->RootTimes.u2MaxAge) +
                                        (pBrgInfo->RootTimes.u2ForwardDelay));
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: Set TcDetected Timer Duration as %u\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  u2TcDetectedDuration);

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_TCSM_DBG,
                  "NewTcDtetcted: Port %s: tcDetected = %u\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  u2TcDetectedDuration);

    /* Start tcDetected only if it is not already running */
    if (pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr == NULL)
    {
        if (AstStartTimer ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                           (UINT1) AST_TMR_TYPE_TCDETECTED,
                           u2TcDetectedDuration) != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: AstStartTimer for TcDetected FAILED!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : RstTopoChSmNewTcWhile                                */
/*                                                                           */
/* Description        : This function sets the value of the TcWhile Timer    */
/*                      duration depending on whether it is a point-to-point */
/*                      link with the Partner Bridge Port being RSTP capable */
/*                      and starts the TcWhile Timer for this duration.      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstTopoChSmNewTcWhile (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo;
    tAstBoolean         bBridgeTcPresent = RST_FALSE;
    UINT2               u2TcWhileDuration = AST_INIT_VAL;
    UINT2               u2PortNum = 1;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if (pPortEntry != NULL)
        {
            pTmpPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
            if (pTmpPerStPortInfo == NULL)
            {
                continue;
            }
            if (pTmpPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
            {
                bBridgeTcPresent = RST_TRUE;
                break;
            }
        }
    }

    if (bBridgeTcPresent == RST_FALSE)
    {
        /* TC is not present at the bridge level
         * so increment the topology change count and send trap.
         */
        AST_INCR_NUM_OF_TOPO_CHANGES (RST_DEFAULT_INSTANCE);
        AST_GET_SYS_TIME ((tAstSysTime
                           *) (&(pPerStBrgInfo->u4TimeSinceTopoCh)));
        gu4RecentTopoChPort = pAstPortEntry->u4IfIndex;
        AstTopologyChangeTrap (RST_DEFAULT_INSTANCE, (INT1 *) AST_BRG_TRAPS_OID,
                               AST_BRG_TRAPS_OID_LEN);
    }

    if (pAstCommPortInfo->bSendRstp == RST_TRUE)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %s: Partner Bridge Port is RSTP capable\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        u2TcWhileDuration =
            (UINT2) ((pAstPortEntry->DesgTimes.u2HelloTime) +
                     (1 * AST_CENTI_SECONDS));

        /* Set newInfo only if TcWhile timer is not running */
        if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr == NULL)
        {
            pAstCommPortInfo->bNewInfo = RST_TRUE;
        }
    }
    else
    {
        u2TcWhileDuration = (UINT2) ((pBrgInfo->RootTimes.u2MaxAge) +
                                     (pBrgInfo->RootTimes.u2ForwardDelay));
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %s: Set TcWhile Timer Duration as %u\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  u2TcWhileDuration);

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_TCSM_DBG,
                  "NewTcWhile: Port %s: tcWhile = %u\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  u2TcWhileDuration);

    /* Start tcWhile only if it is not already running */
    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr == NULL)
    {
        if (AstStartTimer ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                           (UINT1) AST_TMR_TYPE_TCWHILE, u2TcWhileDuration)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: AstStartTimer for TcWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstTopoChSmEventImpossible                           */
/*                                                                           */
/* Description        : This function is called on the occurence of an event */
/*                      that is not possible in the present state of the     */
/*                      Topology Change State Machine.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstTopoChSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo)
{
    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "TCSM: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
             "TCSM: This is an IMPOSSIBLE EVENT in this State!\n");

    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return RST_FAILURE;
    }
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstInitTopoChStateMachine                            */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaTopoChMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaTopoChMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RstInitTopoChStateMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_TOPOCHSM_MAX_EVENTS][20] = {
        "BEGIN", "LEARN_SET", "NOT_DESG_ROOT", "FORWARD", "OPEREDGE_RESET",
        "OPEREDGE_SET", "RCVDTC", "RCVDTCN", "RCVDTCACK",
        "TCPROP"
    };
    UINT1               aau1SemState[RST_TOPOCHSM_MAX_STATES][20] = {
        "INIT", "INACTIVE", "DETECTED", "ACTIVE",
        "NOTIFIED_TCN", "NOTIFIED_TC", "PROPAGATING", "ACKNOWLEDGED"
    };

    for (i4Index = 0; i4Index < RST_TOPOCHSM_MAX_EVENTS; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemEvent[AST_TCSM][i4Index],
                    aau1SemEvent[i4Index]);
    }
    for (i4Index = 0; i4Index < RST_TOPOCHSM_MAX_STATES; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemState[AST_TCSM][i4Index],
                    aau1SemState[i4Index]);
    }
    /* Port Topology change SEM */
    /* BEGIN Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = RstTopoChSmMakeInactive;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmMakeInactive;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeInactive;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_BEGIN]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* LEARN_SET Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_LEARN_SET]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* NOT_DESG_ROOT Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmChkInactive;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* FORWARD Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmChkDetected;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_FORWARD]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* OPEREDGE_RESET Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmChkDetected;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_RESET]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* OPEREDGE_SET Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_OPEREDGE_SET]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* RCVDTC Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeNotifiedTc;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTC]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* RCVDTCN Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeNotifiedTcn;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCN]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* RCVDTCACK Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakeAcknowledged;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_RCVDTCACK]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    /* TCPROP Event */
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_LEARNING].pAction = RstTopoChSmMakeLearning;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_DETECTED].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_ACTIVE].pAction = RstTopoChSmMakePropagating;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_PROPAGATING].pAction = RstTopoChSmEventImpossible;
    RST_TOPO_CH_MACHINE[RST_TOPOCHSM_EV_TCPROP]
        [RST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = RstTopoChSmEventImpossible;

    AST_DBG (AST_TCSM_DBG, "TCSM: Loaded TCSM SEM successfully\n");

    return;
}

/* End of file */
