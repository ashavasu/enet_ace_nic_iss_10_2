/* $Id: astppssm.c,v 1.10 2015/02/11 10:38:37 siva Exp $ */
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPseudoInfoMachine                                 */
/*                                                                           */
/* Description        : This is the routine to trigger PseudoInfo State      */
/*                      State Machine for an event.                          */
/*                      This routine calls the appropriate action routines   */
/*                      for the event-state combination.                     */
/*                                                                           */
/* Input(s)           : u1Event   - The event that has caused this           */
/*                                  State Machine to be called.              */
/*                      u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoMachine (UINT1 u1Event, UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT1               u1State = 0;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG3 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, RstPseudoInfoMachine triggered for Event:%s and State:%s\n",
                  u2PortNum, gaaau1AstSemEvent[AST_PSSM][u1Event],
                  gaaau1AstSemState[AST_PSSM][u1State]);

    if (RST_PORT_PSEUDO_INFO_MACHINE[u1Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_PSSM_DBG, "PSEUDO_SM: No Operation to perform\n");

        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_PSEUDO_INFO_MACHINE[u1Event][u1State].pAction))
        (u2PortNum, pRcvdBpdu);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "PSEUDO_SEM: Event Handler returned failure \n");
        return RST_FAILURE;

    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmCheckL2gp                             */
/*                                                                           */
/* Description        : This is the routine to validate whether state machine*/
/*                      is ready to move to PseudoInfoRx State.              */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmCheckL2gp (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    UNUSED_PARAM (pRcvdBpdu);

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pAstPortEntry->u1EntryStatus == AST_PORT_OPER_UP) &&
        (AST_PORT_IS_L2GP (pAstPortEntry) == RST_TRUE) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE))
    {
        RstPseudoInfoSmMakePseudoInfoRx (u2PortNum, NULL);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmMakeInit                              */
/*                                                                           */
/* Description        : This is the routine to move state machine to INIT    */
/*                      state and stop the PseudoInfoHelloWhen timer if      */
/*                      already running.                                     */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmMakeInit (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT1               u1State = 0;
    UINT2               u2Duration = AST_INIT_VAL;

    UNUSED_PARAM (pRcvdBpdu);

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if (pAstCommPortInfo->pPseudoInfoHelloWhenTmr != NULL)
    {
        /*
         * If PseudoInfoHelloWhen timer is running, stop the timer
         * */

        if (AstStopTimer ((VOID *) pAstPortEntry,
                          AST_TMR_TYPE_PSEUDOINFOHELLOWHEN) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for PSEUDOINFOHELLOWHEN FAILED!\n");
            return RST_FAILURE;

        }

    }

    /* If IsL2Gp is False but port is enabled, start the EdegeDelayWhile
     * timer.
     * Stop EdgeDelayWhile Timer in RST_PSEUDO_INFO_STATE_RX, Bpdu will
     * be generated in RST_PSEUDO_INFO_STATE_RX state and given to PISM.
     * therefore there is no need to run EdgeDelayWhileTimer for this
     * port.
     */
    if ((pAstPortEntry->u1EntryStatus == AST_PORT_OPER_UP) &&
        (AST_PORT_IS_L2GP (pAstPortEntry) != RST_TRUE))
    {
        u2Duration =
            (UINT2) ((AST_GET_BRGENTRY ())->u1MigrateTime * AST_CENTI_SECONDS);

        if (AstStartTimer ((VOID *) pAstPortEntry, RST_DEFAULT_INSTANCE,
                           (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                           (UINT2) u2Duration) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
            AST_DBG (AST_PSSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->u1PortPseudoSmState = RST_PSEUDO_INFO_STATE_INIT;

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG2 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, PseudoInfo Moved to State %s\r\n",
                  u2PortNum, gaaau1AstSemState[AST_PSSM][u1State]);

    i4RetVal = RstPseudoInfoSmCheckL2gp (u2PortNum, pRcvdBpdu);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmMakePseudoInfoRx                      */
/*                                                                           */
/* Description        : This routine generates PseudoInfo Bpdu, sends it to  */
/*                      port information state machine for further processing*/
/*                      and moves state machine to PSEUDO_INFO_RX State.     */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occured.  */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmMakePseudoInfoRx (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBpdu           *pPseudoRstBpdu = NULL;
#ifdef MSTP_WANTED
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tMstBpdu           *pPseudoMstBpdu = NULL;
#endif
    UINT1               u1State = 0;

    UNUSED_PARAM (pRcvdBpdu);

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pAstCommPortInfo->bRcvdStp = RST_FALSE;

    if (pAstCommPortInfo->bRcvdRstp == RST_FALSE)
    {
        pAstCommPortInfo->bRcvdRstp = RST_TRUE;

        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_RSTP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PSEUDO_SM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                          "PSEUDO_SM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }

    /*Call the BDSM with RCVD bpdu event */
    if (pAstPortEntry->bOperEdgePort == RST_TRUE)
    {
        pAstPortEntry->bOperEdgePort = RST_FALSE;
        RstBrgDetectionMachine (RST_BRGDETSM_EV_BPDU_RCVD, u2PortNum);
    }

    if (AST_IS_RST_ENABLED ())
    {
        pPseudoRstBpdu = gAstGlobalInfo.gpPseudoRstBpdu;

        AST_MEMSET (pPseudoRstBpdu, AST_INIT_VAL, sizeof (tAstBpdu));

        RstPreparePseudoInfo (u2PortNum, &pPseudoRstBpdu);

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        pRstPortInfo->bRcvdMsg = RST_TRUE;

        if (RstPortInfoMachine (RST_PINFOSM_EV_RCVD_BPDU, pPerStPortInfo,
                                pPseudoRstBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PSEUDO_SM: RstPortInfoMachine function returned FAILURE for Index %s\r\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        pPseudoMstBpdu = gAstGlobalInfo.gpPseudoMstBpdu;

        AST_MEMSET (pPseudoMstBpdu, AST_INIT_VAL, sizeof (tMstBpdu));

        MstPreparePseudoInfo (u2PortNum, &pPseudoMstBpdu);

        pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

        pCistMstiPortInfo->bRcvdInternal = RST_TRUE;

        if (MstPortRcvSetRcvdMsgs (pPseudoMstBpdu, u2PortNum) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PSEUDO_SM: MstPortRcvSetRcvdMsgs function returned FAILURE for Index %s\r\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }
    }
#endif

    /* Stop EdgeDelayWhile Timer in RST_PSEUDO_INFO_STATE_RX, Bpdu is  
     * generated in RST_PSEUDO_INFO_STATE_RX state and given to PISM.
     * therefore there is no need to run EdgeDelayWhileTimer for this
     * port.
     */
    if (pAstCommPortInfo->pEdgeDelayWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pAstPortEntry,
                          (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstStopTimer for EdgeDelayWhileTmr FAILED!\n");
            AST_DBG (AST_PSSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "MSG: AstStopTimer for EdgeDelayWhileTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->u1PortPseudoSmState = RST_PSEUDO_INFO_STATE_RX;

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG2 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, PseudoInfo Moved to State %s\r\n",
                  u2PortNum, gaaau1AstSemState[AST_PSSM][u1State]);

    if (pAstCommPortInfo->pPseudoInfoHelloWhenTmr == NULL)
    {
        return (RstPseudoInfoSmMakePseudoInfoHelloExpiry (u2PortNum, NULL));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmMakeBpduDiscard                       */
/*                                                                           */
/* Description        : This routine reset rcvdBpdu and Move State Machine to*/
/*                      PSEUDO_INFO_DISCARD State.                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmMakeBpduDiscard (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT1               u1State = 0;

    UNUSED_PARAM (pRcvdBpdu);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pAstCommPortInfo->bRcvdBpdu = RST_FALSE;

    pAstCommPortInfo->u1PortPseudoSmState = RST_PSEUDO_INFO_STATE_BPDU_DISCARD;

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG2 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, PseudoInfo Moved to State %s\r\n",
                  u2PortNum, gaaau1AstSemState[AST_PSSM][u1State]);

    RstPseudoInfoSmMakePseudoInfoRx (u2PortNum, NULL);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmMakeBpduCheck                         */
/*                                                                           */
/* Description        : This routine reset rcvdBpdu and Move State Machine to*/
/*                      PSEUDO_INFO_CHECK State.                             */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmMakeBpduCheck (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT1               u1State = 0;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if (AST_IS_RST_ENABLED ())
    {
        RstCheckBPDUConsistency (u2PortNum, &pRcvdBpdu->uBpdu.RstBpdu);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        MstCheckBPDUConsistency (u2PortNum, &pRcvdBpdu->uBpdu.MstBpdu);
    }
#endif
    pAstCommPortInfo->bRcvdBpdu = RST_FALSE;

    pAstCommPortInfo->u1PortPseudoSmState = RST_PSEUDO_INFO_STATE_BPDU_CHECK;

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG2 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, PseudoInfo Moved to State %s\r\n",
                  u2PortNum, gaaau1AstSemState[AST_PSSM][u1State]);

    RstPseudoInfoSmMakePseudoInfoRx (u2PortNum, NULL);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmBpduCheckAndDiscard                   */
/*                                                                           */
/* Description        : In this routine, if enableBPDUrx is set trigger state*/
/*                      machine to move to PSEUDO_INFO_CHECK state, otherwise*/
/*                      PSEUDO_INFO_DISCARD State.                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmBpduCheckAndDiscard (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (AST_PORT_ENABLE_BPDU_RX (pAstPortEntry) == RST_TRUE)
    {
        RstPseudoInfoSmMakeBpduCheck (u2PortNum, pRcvdBpdu);
    }
    else
    {
        RstPseudoInfoSmMakeBpduDiscard (u2PortNum, pRcvdBpdu);
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmMakePseudoInfoHelloExpiry             */
                                                                                                                                                                                                                                                                                                                                                                                                          /*                                                                           *//* Description        : This routine handles PseudoInfoHelloWhen timer expiry */
/*                      event, restart timer and move to PSEUDO_INFO_HELLO_E- */
/*                      XPIRY State.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmMakePseudoInfoHelloExpiry (UINT2 u2PortNum,
                                          tAstBpduType * pRcvdBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT1               u1State = 0;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    /*Start the PseudoInfoHelloWhen Timer */
    if (AstStartTimer ((VOID *) pAstPortEntry, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_PSEUDOINFOHELLOWHEN,
                       (UINT2) pAstPortEntry->DesgTimes.u2HelloTime) !=
        RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstStartTimer for PseudoInfoHelloWhenTmr FAILED!\n");
        AST_DBG (AST_PSSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: AstStartTimer for PseudoInfoHelloWhenTmr FAILED!\n");
        return RST_FAILURE;
    }

    pAstCommPortInfo->u1PortPseudoSmState = RST_PSEUDO_INFO_STATE_HELLO_EXPIRY;

    u1State = pAstCommPortInfo->u1PortPseudoSmState;

    AST_DBG_ARG2 (AST_PSSM_DBG,
                  "PSEUDO_SM: PORT: %d, PseudoInfo Moved to State %s\r\n",
                  u2PortNum, gaaau1AstSemState[AST_PSSM][u1State]);

    RstPseudoInfoSmMakePseudoInfoRx (u2PortNum, pRcvdBpdu);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPseudoInfoSmEventImpossible                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPseudoInfoSmEventImpossible (UINT2 u2PortNum, tAstBpduType * pRcvdBpdu)
{

#if defined(RSTP_TRACE_WANTED) || defined(RSTP_DEBUG)
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                  "PSEUDO_SEM: Port %s:  IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                  "PSEUDO_SEM: Port %s:  IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

#else
    UNUSED_PARAM (u2PortNum);
#endif
    UNUSED_PARAM (pRcvdBpdu);

    if (AstHandleImpossibleState (u2PortNum) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPreparePseudoInfo                                 */
/*                                                                           */
/* Description        : This function creates a Pseudo Info RST BPDU with    */
/*                      RSTP information filled but it does not contain      */
/*                      Ethernet header. It will be presented to spanning    */
/*                      tree as if it received from the Physical Port on     */
/*                      which it was invoked.                                */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPreparePseudoInfo (UINT2 u2PortNum, tAstBpdu ** ppPseudoRstBpdu)
{
    tAstBpdu           *pRstBpdu = NULL;
    tAstBridgeEntry    *pBrgEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pRstBpdu = (*ppPseudoRstBpdu);

    pBrgEntry = AST_GET_BRGENTRY ();

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    pRstBpdu->u2ProtocolId = AST_INIT_VAL;
    pRstBpdu->u1Version = AST_VERSION_3;
    pRstBpdu->u1BpduType = AST_BPDU_TYPE_RST;

    pRstBpdu->u1Flags =
        RST_SET_DESG_PROLE_FLAG | RST_SET_LEARNING_FLAG |
        RST_SET_FORWARDING_FLAG;

    pRstBpdu->RootId.u2BrgPriority = pPerStPortInfo->PseudoRootId.u2BrgPriority;
    AST_MEMCPY (pRstBpdu->RootId.BridgeAddr,
                pPerStPortInfo->PseudoRootId.BridgeAddr, AST_MAC_ADDR_LEN);

    pRstBpdu->u4RootPathCost = AST_INIT_VAL;

    pRstBpdu->u2PortId = AST_INIT_VAL;

    pRstBpdu->u2MessageAge = pBrgEntry->BridgeTimes.u2MsgAgeOrHopCount;
    pRstBpdu->u2MaxAge = pBrgEntry->BridgeTimes.u2MaxAge;
    pRstBpdu->u2HelloTime = pBrgEntry->BridgeTimes.u2HelloTime;
    pRstBpdu->u2FwdDelay = pBrgEntry->BridgeTimes.u2ForwardDelay;

    pRstBpdu->u1Version1Len = AST_INIT_VAL;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstCheckBPDUConsistency                              */
/*                                                                           */
/* Description        : This function compares the message priority vector of*/
/*                      the information received on the port with the port   */
/*                      priority vector for the port, and                    */
/*                         a)If the received message priority vector is      */
/*                           superior to the port priority vector, and       */
/*                         b)The BPDU is an ST BPDU (version 0 or version 1) */
/*                           or                                              */
/*                         c)The BPDU is an RST or MST BPDU (type 2,         */
/*                           version 2 or above) and its Port Role values    */
/*                           indicates Designated and its Learning flag is   */
/*                           set.                                            */
/*                     Then on this port the disputed flag is set and agreed */
/*                     is cleared and trigger Role Transition State machine  */
/*                     to re-evaluate port state.                            */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstCheckBPDUConsistency (UINT2 u2PortNum, tAstBpdu * pRcvdBpdu)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               u1RcvdInfo;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    u1RcvdInfo = (UINT1) RstPortInfoSmRcvInfo (pPerStPortInfo, pRcvdBpdu);

    if (u1RcvdInfo == RST_SUPERIOR_DESG_MSG)
    {
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        pRstPortInfo->bDisputed = RST_TRUE;
        pRstPortInfo->bAgreed = RST_FALSE;

        /* 
         * Trigger Role Transition Machine to Re-Evaluate Port State
         * */
        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_DISPUTED_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PSEUDO_SEM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PSEUDO_SEM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitPseudoInfoMachine                             */
/*                                                                           */
/* Description        : Initialises the Port PseudoInfo State Machine.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
RstInitPseudoInfoMachine (VOID)
{
#if defined(RSTP_TRACE_WANTED) || defined(RSTP_DEBUG)
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PSEUDO_INFO_MAX_EVENTS][20] = {
        "BEGIN", "PORT_ENABLED", "PORT_DISABLED", "L2GP_ENABLED",
        "L2GP_DISABLED", "PSEUDO_INFO_EXPIRY", "RCVD_BPDU"
    };

    UINT1               aau1SemState[RST_PSEUDO_INFO_MAX_STATE][20] = {
        "PSEUDO_INFO_INIT", "PSEUDO_INFO_RX", "BPDU_CHECK", "HELLO_EXPIRY"
    };

    for (i4Index = 0; i4Index < RST_PSEUDO_INFO_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PSSM][i4Index],
                    aau1SemEvent[i4Index], STRLEN(aau1SemEvent[i4Index]));
	gaaau1AstSemEvent[AST_PSSM][i4Index][STRLEN(aau1SemEvent[i4Index])] = '\0';
    }
    for (i4Index = 0; i4Index < RST_PSEUDO_INFO_MAX_STATE; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PSSM][i4Index],
                    aau1SemState[i4Index], STRLEN(aau1SemState[i4Index]));
	gaaau1AstSemState[AST_PSSM][i4Index][STRLEN(aau1SemState[i4Index])] = '\0';
    }
#endif

    /* Event 0 - RST_PSEUDO_INFO_EV_BEGIN */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_BEGIN]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = RstPseudoInfoSmMakeInit;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_BEGIN]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmMakeInit;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_BEGIN]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_BEGIN]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 1 - RST_PSEUDO_INFO_EV_PORT_ENABLED */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_ENABLED]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = RstPseudoInfoSmCheckL2gp;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_ENABLED]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_ENABLED]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_ENABLED]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 2 - RST_PSEUDO_INFO_EV_PORT_DISABLED */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_DISABLED]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = NULL;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_DISABLED]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmMakeInit;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_DISABLED]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_PORT_DISABLED]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 3 - RST_PSEUDO_INFO_EV_L2GP_ENABLED */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_ENABLED]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = RstPseudoInfoSmCheckL2gp;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_ENABLED]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_ENABLED]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_ENABLED]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 4 - RST_PSEUDO_INFO_EV_L2GP_DISABLED */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_DISABLED]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = NULL;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_DISABLED]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmMakeInit;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_DISABLED]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_L2GP_DISABLED]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 5 - RST_PSEUDO_INFO_EV_HELLO_EXPIRY */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_HELLO_EXPIRY]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_HELLO_EXPIRY]
        [RST_PSEUDO_INFO_STATE_RX].pAction =
        RstPseudoInfoSmMakePseudoInfoHelloExpiry;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_HELLO_EXPIRY]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_HELLO_EXPIRY]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    /* Event 6 - RST_PSEUDO_INFO_EV_RCVD_BPDU */
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_RCVD_BPDU]
        [RST_PSEUDO_INFO_STATE_INIT].pAction = NULL;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_RCVD_BPDU]
        [RST_PSEUDO_INFO_STATE_RX].pAction = RstPseudoInfoSmBpduCheckAndDiscard;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_RCVD_BPDU]
        [RST_PSEUDO_INFO_STATE_BPDU_CHECK].pAction =
        RstPseudoInfoSmEventImpossible;
    RST_PORT_PSEUDO_INFO_MACHINE[RST_PSEUDO_INFO_EV_RCVD_BPDU]
        [RST_PSEUDO_INFO_STATE_HELLO_EXPIRY].pAction =
        RstPseudoInfoSmEventImpossible;

    return;
}
