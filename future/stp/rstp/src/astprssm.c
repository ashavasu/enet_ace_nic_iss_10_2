/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astprssm.c,v 1.31 2017/10/30 14:29:19 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Role Selection State Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortRoleSelectionMachine                          */
/*                                                                           */
/* Description        : This is the main routine for the Port Role Selection */
/*                      State Machine.                                       */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortRoleSelectionMachine (UINT2 u2Event, UINT2 u2InstanceId)
{
    UINT2               u2State = (UINT2) AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4RetVal = (INT4) RST_SUCCESS;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    u2State = (UINT2) pPerStBrgInfo->u1ProleSelSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "RSSM: Role Sel Machine Called with Event: %s,State: %s and Instance: %u\n",
                  gaaau1AstSemEvent[AST_RSSM][u2Event],
                  gaaau1AstSemState[AST_RSSM][u2State], u2InstanceId);

    AST_DBG_ARG3 (AST_RSSM_DBG,
                  "RSSM: Role Sel Machine Called with Event: %s,State: %s and Instance: %u\n",
                  gaaau1AstSemEvent[AST_RSSM][u2Event],
                  gaaau1AstSemState[AST_RSSM][u2State], u2InstanceId);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PROLESELSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_RSSM_DBG,
                 "RSSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_PORT_ROLE_SEL_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_ROLE_SEL_MACHINE[u2Event][u2State].pAction))
        (u2InstanceId);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: Event routine returned FAILURE !!!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleSelSmMakeInit                                */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'INIT_BRIDGE'.                                       */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleSelSmMakeInit (UINT2 u2InstanceId)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED () == MST_TRUE)
    {
        MstPRoleSelSmUpdtRolesDisabledTree (u2InstanceId);
    }
    else
    {
#endif /* MSTP_WANTED */
        RstProleSelSmUpdtRoleDisabledTree (u2InstanceId);
#ifdef MSTP_WANTED
    }
#endif /* MSTP_WANTED */

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);

    pPerStBrgInfo->u1ProleSelSmState = (UINT1) RST_PROLESELSM_STATE_INIT_BRIDGE;

    AST_DBG (AST_RSSM_DBG, "RSSM: Moved to state INIT_BRIDGE \n");

    if ((AST_CURR_CONTEXT_INFO ())->bBegin == RST_FALSE)
    {
        return (RstProleSelSmMakeRoleSelection (u2InstanceId));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleSelSmMakeRoleSelection                       */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'ROLE_SELECTION'.                                    */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleSelSmMakeRoleSelection (UINT2 u2InstanceId)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pPerStBrgInfo->u1ProleSelSmState =
        (UINT1) RST_PROLESELSM_STATE_ROLE_SELECTION;

    AST_DBG (AST_RSSM_DBG, "RSSM: Moved to state ROLE_SELECTION \n");

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED () == MST_TRUE)
    {
        /* MstPRoleSelSmClearReselectTree  functionality is handled 
           inside RoleSelection state machine */
        if (MstPRoleSelSmUpdtRoles (u2InstanceId) != MST_SUCCESS)
        {
            AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     "RSSM: UpdtRolesBridge  returned FAILURE !!!\n");
            return RST_FAILURE;
        }
        MstPRoleSelSmSetSelectedTree (u2InstanceId);
    }
    else
    {
#endif /* MSTP_WANTED */
        RstProleSelSmClearReselectTree (u2InstanceId);
        if (RstProleSelSmUpdtRolesTree (u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     "RSSM: UpdtRolesBridge  returned FAILURE !!!\n");
            return RST_FAILURE;
        }
        if (RstProleSelSmSetSelectedTree (u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     "RSSM: SetSelectedBridge  returned FAILURE !!!\n");
            return RST_FAILURE;
        }
#ifdef MSTP_WANTED
    }
#endif /* MSTP_WANTED */

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstProleSelSmUpdtRoleDisabledTree                    */
/*                                                                           */
/* Description        : This routine performs the 'updtRoleDisabledBridge'   */
/*                      procedure of the state machine.                      */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
VOID
RstProleSelSmUpdtRoleDisabledTree (UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if (pPortEntry != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                   MST_SYNC_ROLE_UPDATE);
            AST_DBG_ARG1 (AST_RSSM_DBG,
                          "RSSM: Port %s: Port Role Selected as -> DISABLED\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }
    AST_DBG (AST_SM_VAR_DBG,
             "RSSM_InitBridge: SelectedRole = DISABLED for all ports\n");

}

/*****************************************************************************/
/* Function Name      : RstProleSelSmClearReselectTree                       */
/*                                                                           */
/* Description        : This routine performs the 'clearReleselectTree'      */
/*                      procedure of the state machine.                      */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
VOID
RstProleSelSmClearReselectTree (UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if (pPortEntry != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_TRUE)
            {
                pRstPortInfo->bReSelect = RST_FALSE;

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: RESELECT variable CLEARED\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
            }
        }
    }

    AST_DBG (AST_SM_VAR_DBG,
             "RSSM_RoleSelection: Reselect = FALSE for all ports\n");

    return;
}

/*****************************************************************************/
/* Function Name      : RstProleSelSmUpdtRolesTree                           */
/*                                                                           */
/* Description        : This routine performs the 'updtRolesTree'            */
/*                      procedure of the state machine.                      */
/*                      This routine computes and selects the Port Role for  */
/*                      all the bridge ports that are existing.              */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleSelSmUpdtRolesTree (UINT2 u2InstanceId)
{
    tAstBridgeId        MyBrgId;
    tAstBridgeId        TmpRootId;
    tAstBridgeId        TmpDesgBrgId;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstTimes          *pPortTimes = NULL;
    tAstTimes          *pRootTimes = NULL;
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRole[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1NewRole[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT1               au1OldRootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT4               u4TmpRootCost = (UINT4) AST_INIT_VAL;
    UINT2               u2TmpDesgPort = (UINT2) AST_INIT_VAL;
    UINT4               u4Val = (UINT4) AST_INIT_VAL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPort = (UINT2) AST_INIT_VAL;
    UINT2               u2Val = (UINT4) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT2               u2PortId = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPortId = (UINT2) AST_INIT_VAL;
    UINT1               u1PrevPortRole = AST_PORT_ROLE_DISABLED;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));
    AST_MEMSET (&TmpRootId, AST_INIT_VAL, sizeof (TmpRootId));
    AST_MEMSET (&TmpDesgBrgId, AST_INIT_VAL, sizeof (TmpDesgBrgId));
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);
    AST_MEMSET (au1RootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldRootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    /* Assume the root as this bridge itself initially */
    TmpRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(TmpRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    u4TmpRootCost = 0;
    TmpDesgBrgId = TmpRootId;
    u2TmpDesgPort = 0;
    u2TmpRootPort = 0;

    MyBrgId = TmpRootId;

    /* Calculate 
     * 1. The Root Bridge Id among the Root Bridge Id held in 
     *    all functional ports.
     * 2. The Root Port as the port that is having better information
     * 3. Calculate the Root Path Cost as the best cost to the Root Bridge
     */
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_FALSE)
        {
            continue;
        }

        /* If restrictedRole is set, then don't consider this port
         * for bridge root priority vector calculations. */
        if (AST_PORT_RESTRICTED_ROLE (pPortInfo) == (tAstBoolean) RST_TRUE)
        {
            continue;
        }
        /* Port is not Disabled */

        if (pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED)
        {
            /* Port Message is a Received Message */

            if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                            &(pPerStPortInfo->DesgBrgId.BridgeAddr),
                            AST_MAC_ADDR_LEN) == RST_BRGID1_SAME)
            {
                continue;
            }

            i4RetVal = RstCompareBrgId (&(pPerStPortInfo->RootId),
                                        &(TmpRootId));
            switch (i4RetVal)
            {
                case RST_BRGID1_SUPERIOR:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Root BrgId received by Port is SUPERIOR\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    TmpRootId = pPerStPortInfo->RootId;
                    u4TmpRootCost = pPerStPortInfo->u4RootCost +
                        pPortInfo->u4PathCost;
                    TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                    u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                    u2TmpRootPort = u2PortNum;
                    break;

                case RST_BRGID1_SAME:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Root Bridge ID received by Port is the SAME\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    u4Val = pPerStPortInfo->u4RootCost + pPortInfo->u4PathCost;
                    if (u4Val < u4TmpRootCost)
                    {

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Path Cost received by Port is BETTER\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));

                        u4TmpRootCost = u4Val;
                        TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                        u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                        u2TmpRootPort = u2PortNum;
                    }
                    else if (u4Val == u4TmpRootCost)
                    {

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Path Cost received by Port is the SAME\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));

                        i4RetVal =
                            RstCompareBrgId (&(pPerStPortInfo->DesgBrgId),
                                             &(TmpDesgBrgId));
                        switch (i4RetVal)
                        {
                            case RST_BRGID1_SUPERIOR:

                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %s: DesgBrgId received by Port is SUPERIOR\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));

                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = u2PortNum;
                                break;

                            case RST_BRGID1_SAME:

                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %s: DesgBrgId received by Port is the SAME\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));

                                if (pPerStPortInfo->u2DesgPortId <
                                    u2TmpDesgPort)
                                {

                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Desg PortId received by Port is BETTER\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum));

                                    u2TmpDesgPort =
                                        pPerStPortInfo->u2DesgPortId;
                                    u2TmpRootPort = u2PortNum;
                                }
                                else if (pPerStPortInfo->u2DesgPortId
                                         == u2TmpDesgPort)
                                {
                                    u2TmpRootPortId = (UINT2)
                                        (AST_GET_PERST_PORT_INFO
                                         (u2TmpRootPort, u2InstanceId)->
                                         u1PortPriority);
                                    u2TmpRootPortId =
                                        (UINT2) (u2TmpRootPortId << 8);

                                    u2TmpRootPortId =
                                        u2TmpRootPortId |
                                        AST_GET_LOCAL_PORT (u2TmpRootPort);

                                    u2PortId =
                                        (UINT2) (pPerStPortInfo->
                                                 u1PortPriority);
                                    u2PortId = (UINT2) (u2PortId << 8);

                                    u2PortId =
                                        u2PortId |
                                        AST_GET_LOCAL_PORT (u2PortNum);

                                    if (u2PortId < u2TmpRootPortId)
                                    {
                                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                                      "RSSM: Port %s: Received Port Id is SUPERIOR\n",
                                                      AST_GET_IFINDEX_STR
                                                      (u2PortNum));
                                        u2TmpRootPort = u2PortNum;
                                    }
                                }
                                break;

                            default:
                                break;
                        }        /* End switch(i4RetVal) */
                    }
                    break;

                default:
                    break;
            }                    /* End of switch(i4RetVal)... */
        }                        /* End if(pRstPortInfo->u1InfoIs ..)... */

    }                            /* End for (u2PortNum = 1; ...) */

    /* Now the Chosen Root Priority Vector is copied into global 
     * Root Priority Vector
     */
    if (u2TmpRootPort != AST_INIT_VAL)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));
        AST_DBG_ARG1 (AST_RSSM_DBG,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2TmpRootPort, u2InstanceId);
        pPortInfo = AST_GET_PORTENTRY (u2TmpRootPort);

        /* Calculate if there is any change in Root Bridge */
        i4RetVal = AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                               &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (RST_DEFAULT_INSTANCE);
            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId,
                            RST_DEFAULT_INSTANCE, (INT1 *) AST_BRG_TRAPS_OID,
                            AST_BRG_TRAPS_OID_LEN);
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "ROOT BRIDGE CHANGED...Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              RST_DEFAULT_INSTANCE, au1OldRootBrgAddr,
                              au1RootBrgAddr, au1TimeStr);
        }

        pPerStBrgInfo->RootId = TmpRootId;
        pPerStBrgInfo->u2RootPort = u2TmpRootPort;
        pPerStBrgInfo->u4RootCost = u4TmpRootCost;
        pBrgInfo->RootTimes = pPortInfo->PortTimes;
        pBrgInfo->RootTimes.u2MsgAgeOrHopCount =
            (pBrgInfo->RootTimes.u2MsgAgeOrHopCount + AST_CENTI_SECONDS);
    }
    else
    {
        AST_TRC (AST_CONTROL_PATH_TRC,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");
        AST_DBG (AST_RSSM_DBG,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");

        /* Calculate if there is any change in Root Bridge */
        i4RetVal = AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                               &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (RST_DEFAULT_INSTANCE);
            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId,
                            RST_DEFAULT_INSTANCE, (INT1 *) AST_BRG_TRAPS_OID,
                            AST_BRG_TRAPS_OID_LEN);
            AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
            AST_MEMSET (au1OldRootBrgAddr, 0, sizeof (au1OldRootBrgAddr));
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);

            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              RST_DEFAULT_INSTANCE, au1OldRootBrgAddr,
                              au1RootBrgAddr, au1TimeStr);
        }

        pPerStBrgInfo->RootId = TmpRootId;
        pPerStBrgInfo->u2RootPort = u2TmpRootPort;
        pPerStBrgInfo->u4RootCost = u4TmpRootCost;
        pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;
    }

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            /* Port is not yet created */
            continue;
        }
        if (AST_IS_RST_ENABLED ())
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

            /* Set DesignatedTimes for each port to RootTimes. 
             * Copy the Hello time alone from the BridgeTimes */
            pPortInfo->DesgTimes = pBrgInfo->RootTimes;
            pPortInfo->DesgTimes.u2HelloTime =
                pBrgInfo->BridgeTimes.u2HelloTime;

            switch (pRstPortInfo->u1InfoIs)
            {
                case RST_INFOIS_DISABLED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is DISABLED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    u1PrevPortRole =
                        AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);

                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DISABLED;
                    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);
                    if (u1PrevPortRole != AST_PORT_ROLE_DISABLED)
                    {
                        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                            RST_TRUE;
                    }

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %s: Port Role Selected as -> DISABLED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Port Role Selected as -> DISABLED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    break;

                case RST_INFOIS_AGED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is AGED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    pRstPortInfo->bUpdtInfo = RST_TRUE;
                    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);
                    u1PrevPortRole =
                        AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);

                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);

                    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                    {
                        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                            RST_TRUE;
                    }
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    break;

                case RST_INFOIS_MINE:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is MINE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    u1PrevPortRole =
                        AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);

                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);

                    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                    {
                        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                            RST_TRUE;
                    }

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    i4RetVal =
                        RstIsDesgPrBetterThanPortPr (u2PortNum, u2InstanceId);
                    if (i4RetVal == RST_SAME_MSG)
                    {
                        /* Root Times is compared instead of
                         * Port Times of Root Port.
                         */
                        pPortTimes = &(pPortInfo->PortTimes);
                        pRootTimes = &(pBrgInfo->RootTimes);

                        if ((pPortTimes->u2MaxAge != pRootTimes->u2MaxAge) ||
                            (pPortTimes->u2HelloTime != pRootTimes->u2HelloTime)
                            || (pPortTimes->u2ForwardDelay !=
                                pRootTimes->u2ForwardDelay) ||
                            (pPortTimes->u2MsgAgeOrHopCount !=
                             pRootTimes->u2MsgAgeOrHopCount))
                        {
                            pRstPortInfo->bUpdtInfo = RST_TRUE;
                            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                        }
                    }
                    else
                    {
                        pRstPortInfo->bUpdtInfo = RST_TRUE;
                        MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                    }
                    break;

                case RST_INFOIS_RECEIVED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is RECEIVED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    if (u2TmpRootPort == u2PortNum)
                    {
                        u1PrevPortRole =
                            AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);

                        pPerStPortInfo->u1SelectedPortRole =
                            (UINT1) AST_PORT_ROLE_ROOT;
                        MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                               MST_SYNC_ROLE_UPDATE);

                        if (u1PrevPortRole != AST_PORT_ROLE_ROOT)
                        {
                            AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                                RST_TRUE;
                        }
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                      "RSSM: Port %s: Port Role Selected as -> ROOT PORT\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Port Role Selected as -> ROOT PORT\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        pRstPortInfo->bUpdtInfo = RST_FALSE;
                        MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                    }
                    else
                    {
                        i4RetVal =
                            RstIsDesgPrBetterThanPortPr (u2PortNum,
                                                         u2InstanceId);
                        if (i4RetVal != RST_BETTER_MSG)
                        {
                            if (AST_MEMCMP
                                (&(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                 &(MyBrgId.BridgeAddr[0]),
                                 AST_MAC_ADDR_SIZE) != RST_BRGID1_SAME)
                            {
                                u1PrevPortRole =
                                    AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);
                                pPerStPortInfo->u1SelectedPortRole
                                    = (UINT1) AST_PORT_ROLE_ALTERNATE;

                                /* If operating in C-VLAN component/I-Comp, 
                                 * change the role if the root port and the 
                                 * current port are both PEPs/VIPs.
                                 * */
                                if (u2TmpRootPort != AST_INIT_VAL)
                                {
                                    RstProvCheckAndAlterLogicalIfPortRole
                                        (u2PortNum, u2TmpRootPort);
                                }

                                if (u1PrevPortRole !=
                                    pPerStPortInfo->u1SelectedPortRole)
                                {
                                    AST_SET_CHANGED_FLAG (u2InstanceId,
                                                          u2PortNum) = RST_TRUE;
                                }
                                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                              "RSSM: Port %s: Port Role Selected as -> ALTERNATE\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));
                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %s: Port Role Selected as -> ALTERNATE\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));
                                pRstPortInfo->bUpdtInfo = RST_FALSE;
                                MstPUpdtAllSyncedFlag (u2InstanceId,
                                                       pPerStPortInfo,
                                                       MST_SYNC_BSELECT_UPDATE);
                            }
                            else
                            {
                                /* This bridge is the Designated Bridge for this
                                 * port.
                                 */
                                u2Val = (UINT2) (pPerStPortInfo->u2DesgPortId &
                                                 (UINT2) AST_PORTNUM_MASK);
                                if (u2Val != AST_GET_LOCAL_PORT (u2PortNum))
                                {
                                    u1PrevPortRole =
                                        AST_GET_PORT_ROLE (u2InstanceId,
                                                           u2PortNum);
                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_BACKUP;
                                    MstPUpdtAllSyncedFlag (u2InstanceId,
                                                           pPerStPortInfo,
                                                           MST_SYNC_ROLE_UPDATE);

                                    if (u1PrevPortRole != AST_PORT_ROLE_BACKUP)
                                    {
                                        AST_SET_CHANGED_FLAG (u2InstanceId,
                                                              u2PortNum) =
                                            RST_TRUE;
                                    }

                                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                                  "RSSM: Port %s: Port Role Selected as -> BACKUP\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum));
                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Port Role Selected as -> BACKUP\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum));
                                    pRstPortInfo->bUpdtInfo = RST_FALSE;
                                    MstPUpdtAllSyncedFlag (u2InstanceId,
                                                           pPerStPortInfo,
                                                           MST_SYNC_BSELECT_UPDATE);
                                }
                                else
                                {
                                    /* Such a condition may occur when a BDPU is 
                                     * received that contains all the values 
                                     * that are the same as that would be 
                                     * transmitted from this port, except the 
                                     * designated port priority. For example, if 
                                     * this port is a designated port, and 
                                     * before a BPDU is received from the Root 
                                     * Port attached to this LAN, the Port 
                                     * Priority of this port is made worse. In 
                                     * this case, the values received from the 
                                     * Root Port will be the same as sent out by 
                                     * this Designated port, but with the old 
                                     * port priority value (which will be 
                                     * superior). In this case, the Port, which 
                                     * will still be a Designated Port, should 
                                     * update its designated and port priority
                                     * vector and transmit a BPDU with the new 
                                     * information. */
                                    u1PrevPortRole =
                                        AST_GET_PORT_ROLE (u2InstanceId,
                                                           u2PortNum);
                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                                    MstPUpdtAllSyncedFlag (u2InstanceId,
                                                           pPerStPortInfo,
                                                           MST_SYNC_ROLE_UPDATE);
                                    if (u1PrevPortRole !=
                                        AST_PORT_ROLE_DESIGNATED)
                                    {
                                        AST_SET_CHANGED_FLAG (u2InstanceId,
                                                              u2PortNum) =
                                            RST_TRUE;
                                    }
                                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum));
                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum));
                                    pRstPortInfo->bUpdtInfo = RST_TRUE;
                                    MstPUpdtAllSyncedFlag (u2InstanceId,
                                                           pPerStPortInfo,
                                                           MST_SYNC_BSELECT_UPDATE);
                                }
                            }
                        }
                        else
                        {
                            u1PrevPortRole =
                                AST_GET_PORT_ROLE (u2InstanceId, u2PortNum);

                            pPerStPortInfo->u1SelectedPortRole =
                                (UINT1) AST_PORT_ROLE_DESIGNATED;
                            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                                   MST_SYNC_ROLE_UPDATE);

                            if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                            {
                                AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) =
                                    RST_TRUE;
                            }
                            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                          "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                          AST_GET_IFINDEX_STR (u2PortNum));
                            AST_DBG_ARG1 (AST_RSSM_DBG,
                                          "RSSM: Port %s: Port Role Selected as -> DESIGNATED\n",
                                          AST_GET_IFINDEX_STR (u2PortNum));
                            pRstPortInfo->bUpdtInfo = RST_TRUE;
                            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                        }
                    }
                    break;
                default:
                    return RST_FAILURE;
            }                    /* End Switch */

            if ((AST_GET_CHANGED_FLAG (u2InstanceId, u2PortNum) == RST_TRUE))
            {
                /*Port Role Changed -> Generate Trap */
                AstNewPortRoleTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    pPerStPortInfo->u1SelectedPortRole,
                                    u1PrevPortRole, RST_DEFAULT_INSTANCE,
                                    (INT1 *) AST_BRG_TRAPS_OID,
                                    AST_BRG_TRAPS_OID_LEN);
                AST_MEMSET (au1OldRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
                AST_MEMSET (au1NewRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
                AstGetRoleStr (pPerStPortInfo->u1PortRole, au1OldRole);
                AstGetRoleStr (pPerStPortInfo->u1SelectedPortRole, au1NewRole);
                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG5 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old Role:%s New Role:%s Time Stamp: %s\n",
                                  AST_GET_IFINDEX_STR (u2PortNum),
                                  RST_DEFAULT_INSTANCE, au1OldRole, au1NewRole,
                                  au1TimeStr);

                if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                {
                    AstRedSendUpdate ();
                }
            }
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstIsDesgPrBetterThanPortPr                          */
/*                                                                           */
/* Description        : This routine compares the Designated Priority vector */
/*                      and the Port Priority vector and returns if the      */
/*                      Designated Priority vector is BETTER THAN or INFERIOR*/
/*                      TO or SAME AS the Port Priority vector.              */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                      u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstIsDesgPrBetterThanPortPr (UINT2 u2PortNum, UINT2 u2InstanceId)
{
    tAstBridgeId        MyBrgId;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

    MyBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(MyBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    i4RetVal = RstCompareBrgId (&(pPerStBrgInfo->RootId),
                                &(pPerStPortInfo->RootId));
    switch (i4RetVal)
    {
        case RST_BRGID1_SUPERIOR:
            return RST_BETTER_MSG;
        case RST_BRGID1_INFERIOR:
            return RST_INFERIOR_MSG;
        default:
            break;
    }
    /* Same Root Id */
    if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
    {
        return RST_BETTER_MSG;
    }
    else if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
    {
        return RST_INFERIOR_MSG;
    }
    /* Same Root Path Cost */
    i4RetVal = RstCompareBrgId (&(MyBrgId), &(pPerStPortInfo->DesgBrgId));
    switch (i4RetVal)
    {
        case RST_BRGID1_SUPERIOR:
            return RST_BETTER_MSG;
        case RST_BRGID1_INFERIOR:
            return RST_INFERIOR_MSG;
        default:
            /* Same DesgBrgId */
            break;
    }
    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    u2Val = u2Val | AST_GET_LOCAL_PORT (u2PortNum);

    if (u2Val < pPerStPortInfo->u2DesgPortId)
    {
        return RST_BETTER_MSG;
    }
    else if (u2Val > pPerStPortInfo->u2DesgPortId)
    {
        return RST_INFERIOR_MSG;
    }
    return RST_SAME_MSG;
}

/*****************************************************************************/
/* Function Name      : RstProleSelSmSetSelectedTree                         */
/*                                                                           */
/* Description        : This routine performs the 'setSelectedTree'          */
/*                      procedure of the state machine.                      */
/*                      This routine sets the selected variable for all the  */
/*                      ports.                                               */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstProleSelSmSetSelectedTree (UINT2 u2InstanceId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPort = (UINT2) AST_INIT_VAL;
    UINT2               u2MaxPort = (UINT2) AST_INIT_VAL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    u2TmpRootPort = pPerStBrgInfo->u2RootPort;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        /* Call the Role Transition State Machine or
         * Port Information State Machine for all the ports 
         * other than Root, Alternate and Backup Ports. 
         */
        if (pPortInfo != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ROOT) ||
                (pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_BACKUP))
            {
                continue;
            }

            pRstPortInfo->bSelected = (tAstBoolean) RST_TRUE;
            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);

            AST_DBG_ARG1 (AST_RSSM_DBG,
                          "RSSM: Port %s: SELECTED variable SET\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (pRstPortInfo->bUpdtInfo == (tAstBoolean) RST_FALSE)
            {
                if (pPerStPortInfo->u1SelectedPortRole
                    != AST_PORT_ROLE_DISABLED)
                {
                    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                                  "RSSM_RoleSelection: Port %s: Selected = TRUE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                }

                if (RstPortRoleTrMachine
                    ((UINT2) RST_PROLETRSM_EV_SELECTED_SET,
                     pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return RST_FAILURE;
                }
            }
            else
            {
                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RSSM_RoleSelection: Port %s: Update Info = Selected = TRUE\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_UPDATEINFO,
                                        pPerStPortInfo,
                                        (tAstBpdu *) AST_NO_VAL) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return RST_FAILURE;
                }
            }

            if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_SELECTED_SET,
                                        pPortInfo, RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }

    /* Now Call the Role Transition State Machine
     * for Alternate and Backup ports. 
     */
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_BACKUP))
            {
                pRstPortInfo->bSelected = (tAstBoolean) RST_TRUE;
                MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: SELECTED variable SET\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                if (pRstPortInfo->bUpdtInfo == (tAstBoolean) RST_FALSE)
                {
                    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                                  "RSSM_RoleSelection: Port %s: Selected = TRUE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    if (RstPortRoleTrMachine
                        ((UINT2) RST_PROLETRSM_EV_SELECTED_SET,
                         pPerStPortInfo) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        return RST_FAILURE;
                    }
                }
                else
                {
                    /* No updations on ports that have received superior info
                       - This should never happen !!! */
                    if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_UPDATEINFO,
                                            pPerStPortInfo,
                                            (tAstBpdu *) AST_NO_VAL) !=
                        RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        return RST_FAILURE;
                    }
                }
            }

            if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_SELECTED_SET,
                                        pPortInfo, RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }

    /* Finally call the Role Transition State Machine
     * for the Root port.
     * In case of C-VLAN component there can be multiple root ports
     */
    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) ||
        (AST_IS_I_COMPONENT () == RST_TRUE))
    {
        u2PortNum = 1;
        u2MaxPort = AST_MAX_NUM_PORTS;
    }
    else
    {
        u2PortNum = u2TmpRootPort;
        u2MaxPort = u2TmpRootPort;
    }

    for (; (u2PortNum != 0) && (u2PortNum <= u2MaxPort); u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if (pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ROOT)
            {
                pRstPortInfo->bSelected = (tAstBoolean) RST_TRUE;
                MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: SELECTED variable SET\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                if (pRstPortInfo->bUpdtInfo == (tAstBoolean) RST_FALSE)
                {
                    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                                  "RSSM_RoleSelection: Port %s: Selected = TRUE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    if (RstPortRoleTrMachine
                        ((UINT2) RST_PROLETRSM_EV_SELECTED_SET,
                         pPerStPortInfo) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: RstPortRoleTrMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        return RST_FAILURE;
                    }
                }
                else
                {
                    /* No updations on ports that have received superior info
                       - This should never happen !!! */
                    if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_UPDATEINFO,
                                            pPerStPortInfo,
                                            (tAstBpdu *) AST_NO_VAL) !=
                        RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: RstPortInfoMachine function returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        return RST_FAILURE;
                    }
                }
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProleSelSmEventImpossible                         */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE                                          */
/*****************************************************************************/
INT4
RstProleSelSmEventImpossible (UINT2 u2InstanceId)
{
    AST_TRC (AST_CONTROL_PATH_TRC,
             "RSSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_DBG (AST_RSSM_DBG,
             "RSSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_UNUSED (u2InstanceId);
    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : RstInitPortRoleSelectionMachine                      */
/*                                                                           */
/* Description        : Initialises the Port Role Selection State Machine.   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstInitPortRoleSelectionMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PROLESELSM_MAX_EVENTS][20] = {
        "BEGIN", "BEGIN_CLEARED", "RESELECT"
    };
    UINT1               aau1SemState[RST_PROLESELSM_MAX_STATES][20] = {
        "INIT_BRIDGE", "ROLE_SELECTION"
    };

    for (i4Index = 0; i4Index < RST_PROLESELSM_MAX_EVENTS; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemEvent[AST_RSSM][i4Index],
                    aau1SemEvent[i4Index]);
    }
    for (i4Index = 0; i4Index < RST_PROLESELSM_MAX_STATES; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemState[AST_RSSM][i4Index],
                    aau1SemState[i4Index]);
    }

    /* Event 0 - RST_PROLESELSM_EV_BEGIN */
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_BEGIN]
        [RST_PROLESELSM_STATE_INIT_BRIDGE].pAction = RstProleSelSmMakeInit;
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_BEGIN]
        [RST_PROLESELSM_STATE_ROLE_SELECTION].pAction = RstProleSelSmMakeInit;

    /* Event 1 - RST_PROLESELSM_EV_BEGIN_CLEARED */
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_BEGIN_CLEARED]
        [RST_PROLESELSM_STATE_INIT_BRIDGE].pAction =
        RstProleSelSmMakeRoleSelection;
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_BEGIN_CLEARED]
        [RST_PROLESELSM_STATE_ROLE_SELECTION].pAction = NULL;

    /* Event 2 - RST_PROLESELSM_EV_RESELECT */
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_RESELECT]
        [RST_PROLESELSM_STATE_INIT_BRIDGE].pAction = NULL;
    RST_PORT_ROLE_SEL_MACHINE[RST_PROLESELSM_EV_RESELECT]
        [RST_PROLESELSM_STATE_ROLE_SELECTION].pAction =
        RstProleSelSmMakeRoleSelection;

    AST_DBG (AST_TCSM_DBG, "RSSM: Loaded RSSM SEM successfully\n");

    return;
}
