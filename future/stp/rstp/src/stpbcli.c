/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stpbcli.c,v 1.19 
*
*********************************************************************/
/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : stpbcli.c                                        |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : RSTP/MSTP                                        |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI RSTP/MSTP Commands       |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 26th Jun 2006   |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#ifndef __STPBCLI_C__
#define __STPBCLI_C__

#include "asthdrs.h"
#include "stpcli.h"
#include "stpclipt.h"
#ifdef MSTP_WANTED
#include "astmlow.h"
#endif
#ifdef L2RED_WANTED
#include "astpred.h"
#else
#include "astprdstb.h"
#endif
#include "vcm.h"
#include "astpbcli.h"
#include "fsmpmslw.h"
#include "fsmsbrlw.h"
#include "fsmprslw.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_stp_pb_cmd                             */
/*                                                                           */
/*     DESCRIPTION      : This function processes all the Provider Bridge STP*/
/*                        commands                                           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to  the CLI Context               */
/*                        
                          u4Command- Command Identifier to be passed         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_stp_pb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT1              *args[STP_CLI_MAX_ARGS];
    UINT4               u4Value = 0;
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                argno = 0;
    INT4                i4Args = 0;

    CliRegisterLock (CliHandle, AstLock, AstUnLock);

    AST_LOCK ();

    if (AstCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == STP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_STP_NO_PROVIDER_MOD_STATUS:
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                CliPrintf (CliHandle, "\r%% Bridge is not provider core bridge "
                           "or provider edge bridge since PVRST is "
                           "enabled \r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
#endif
            if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
            {
                u4Value = RST_DISABLED;
                i4RetVal = AstPbCliSetProviderModStatus (CliHandle, u4Value);
            }
            break;

        case CLI_STP_PROVIDER_MOD_STATUS:
#ifdef PVRST_WANTED
            if (AST_IS_PVRST_ENABLED ())
            {
                CliPrintf (CliHandle, "\r%% Bridge is not provider core bridge "
                           "or provider edge bridge since PVRST is "
                           "enabled \r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
#endif
            if ((AST_IS_RST_STARTED ()) || (AST_IS_MST_STARTED ()))
            {
                u4Value = RST_ENABLED;
                i4RetVal = AstPbCliSetProviderModStatus (CliHandle, u4Value);
            }
            break;

        case CLI_CVLAN_STP_DEBUG_ENABLE:
            if (args[2] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[2]);
                StpCliSetDebugLevel (CliHandle, i4Args);
            }

            /* If IfIndex is given then get the Context-Id from it */
            if (u4Index != 0)
            {
                if (AstVcmGetContextInfoFromIfIndex (u4Index, &u4ContextId,
                                                     &u2LocalPortId) !=
                    VCM_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return RST_FAILURE;
                }
                /* args[0] - Debug Value */

                if ((AstIsRstStartedInContext (u4ContextId)) ||
                    (AstIsMstStartedInContext (u4ContextId)))
                {
                    /* args[0] - Spanning Tree debugging feature  
                       to be enabled */
                    u4Value = CLI_PTR_TO_U4 (args[0]);

                    i4RetVal = RstSetCVlanDebug (CliHandle, u4Index, u4Value,
                                                 RST_SET_CMD);
                }
            }
            break;

        case CLI_CVLAN_STP_DEBUG_DISABLE:

            /* If IfIndex is given then get the Context-Id from it */
            if (u4Index != 0)
            {
                if (AstVcmGetContextInfoFromIfIndex (u4Index, &u4ContextId,
                                                     &u2LocalPortId) !=
                    RST_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return RST_FAILURE;
                }
                /* args[0] - Debug Value */

                if ((AstIsRstStartedInContext (u4ContextId)) ||
                    (AstIsMstStartedInContext (u4ContextId)))
                {
                    /* args[0] - Spanning Tree debugging feature  
                       to be enabled */
                    u4Value = CLI_PTR_TO_U4 (args[0]);

                    i4RetVal = RstSetCVlanDebug (CliHandle, u4Index, u4Value,
                                                 RST_NO_CMD);
                }
            }
            break;

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            AstReleaseContext ();
            AST_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    AstReleaseContext ();

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_STP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", StpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    AST_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_stp_pb_show_cmd                        */
/*                                                                           */
/*     DESCRIPTION      : This function processes all the PB STP commands    */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to  the CLI Context               */
/*                        u4Command- Command Identifier to be passed         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_stp_pb_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[STP_CLI_MAX_ARGS];
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = AST_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId;
    UINT1              *pu1ContextName = NULL;
    INT1                argno = 0;

    CliRegisterLock (CliHandle, AstLock, AstUnLock);

    AST_LOCK ();

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing
     * IfIndex as the first argument in variable argument list. Like that
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == STP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    while (AstCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4Index, u4TempContextId,
            &u4ContextId, &u2LocalPortId) == RST_SUCCESS)
    {

        switch (u4Command)
        {

                /*Show CVLAN Spanning Tree */

            case CLI_STP_SHOW_CVLAN_DETAIL:
                if ((AstIsRstStartedInContext (u4ContextId)) ||
                    (AstIsMstStartedInContext (u4ContextId)))
                {
                    switch (CLI_PTR_TO_U4 (args[0]))
                    {
                        case STP_SHOW_DETAIL:
                            i4RetVal =
                                RstCliPbShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Index, 0,
                                                          AST_SNMP_TRUE);
                            break;
                        case STP_ACTIVE_DETAIL:
                            i4RetVal =
                                RstCliPbShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Index,
                                                          STP_ACTIVE_PORTS,
                                                          AST_SNMP_TRUE);
                            break;
                        case STP_ACTIVE_PORTS:
                            i4RetVal =
                                RstCliPbShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Index,
                                                          STP_ACTIVE_PORTS,
                                                          AST_SNMP_FALSE);
                            break;
                        case STP_SUMMARY_PORT_STATES:
                            i4RetVal =
                                RstCliPbShowSpanningTree
                                (CliHandle, u4ContextId, u4Index,
                                 STP_SUMMARY_PORT_STATES, AST_SNMP_FALSE);
                            break;
                        default:
                            i4RetVal = CLI_FAILURE;
                            break;
                    }
                }

                break;

            default:
                /* Given command does not match with any of SHOW commands */
                CliPrintf (CliHandle, "\r%% Invalid Command !\r\n ");
                AST_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;

        }

        /* If SwitchName or Interface is given as input for show command
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4Index != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_STP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", StpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    AST_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AstPbCliSetProviderModStatus                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the status of the provider      */
/*                        spanning tree as enabled or disabled.              */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4RstModStatus- Provider RSTP Module status        */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbCliSetProviderModStatus (tCliHandle CliHandle, UINT4 u4RstModStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPbProviderStpStatus (&u4ErrCode,
                                        (INT4) u4RstModStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhSetFsPbProviderStpStatus ((INT4) u4RstModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbDisplayPortDetails                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Port Details           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4NextPort- Port Index                             */
/*                        i4SVlanId - SVid                                   */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliPbDisplayPortDetails (tCliHandle CliHandle, INT4 i4NextPort,
                            INT4 i4SVlanId)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;
    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;
    tAstPortEntry      *pParentPortEntry = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortEntry = NULL;
    UINT4               u4TxBpduCount = 0;
    UINT4               u4RxBpduCount = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4PortState;
    INT4                i4PortStateStatus = AST_BLOCK_SUCCESS;
    INT4                i4PortRole;
    INT4                i4PathCost;
    INT4                i4Priority;
    INT4                i4DesigCost;
    INT4                i4PortFast;
    INT4                i4FwdTransition;
    UINT4               u4HelloTmr;
    UINT4               u4FwdWhileTmr = 0;
    UINT4               u4TCWhileTmr = 0;
    INT4                i4LinkType;
    UINT2               u2Val;
    UINT2               u2RootPrio;
    UINT2               u2BrgPrio;
    UINT2               u2PortPrio;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_RSTP_MAX_PORTID_BUFFER];

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RootBrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetRootBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    RetPortId.pu1_OctetList = &au1PortIdBuf[0];
    RetPortId.i4_Length = CLI_RSTP_MAX_PORTID_BUFFER;

    /* Displaying Port info */

    /* Port Roles */
    nmhGetFsMIPbRstCVlanPortRole (i4NextPort, i4SVlanId, &i4PortRole);

    /* PORT STATES */
    nmhGetFsMIPbRstCVlanPortState (i4NextPort, i4SVlanId, &i4PortState);

    /*PortPathCost */
    nmhGetFsMIPbRstCVlanPortPathCost (i4NextPort, i4SVlanId, &i4PathCost);

    /* Port priority */
    nmhGetFsMIPbRstCVlanPortPriority (i4NextPort, i4SVlanId, &i4Priority);

    /*Designated Root */
    AST_MEMSET (RetRootBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPbRstCVlanPortDesignatedRoot (i4NextPort, i4SVlanId,
                                            &RetRootBridgeId);

    /*Designated Bridge  */
    AST_MEMSET (RetBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPbRstCVlanPortDesignatedBridge (i4NextPort, i4SVlanId,
                                              &RetBridgeId);

    /*Designated Port */
    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    nmhGetFsMIPbRstCVlanPortDesignatedPort (i4NextPort, i4SVlanId, &RetPortId);

    /*Designated Port PathCost */
    nmhGetFsMIPbRstCVlanPortDesignatedCost (i4NextPort, i4SVlanId,
                                            &i4DesigCost);

    /* Forward Transitions */
    nmhGetFsMIPbRstCVlanPortForwardTransitions (i4NextPort, i4SVlanId,
                                                (UINT4 *) &i4FwdTransition);

    /* PortFast Feature */
    nmhGetFsMIPbRstCVlanPortOperEdgePort (i4NextPort, i4SVlanId, &i4PortFast);

    /* Link Type */
    nmhGetFsMIPbRstCVlanPortOperPointToPoint (i4NextPort, i4SVlanId,
                                              &i4LinkType);

    /*BPDUS TRANSMITTED */
    u4TxBpduCount = 0;
    u4TxBpduCount = RstCliPbPortTxBpduCount (i4NextPort, i4SVlanId);

    /* BPDUs RECIEVED */
    u4RxBpduCount = 0;
    u4RxBpduCount = RstCliPbPortRxBpduCount (i4NextPort, i4SVlanId);

    if ((pParentPortEntry = AstGetIfIndexEntry (i4NextPort)) == NULL)
    {
        return;
    }

    /* Get the port entry from the C-VLAN context by doing a context 
     * switch. */
    /* Before selecting C-VLAN context, select the parent context. 
     * Otherwise Red C-VLAN context selection will crash. */

    if (AstSelectContext (pParentPortEntry->u4ContextId) == RST_FAILURE)
    {
        return;
    }

    pAstPortEntry = AstGetPepPortEntryWithCtxtSwitch (pParentPortEntry,
                                                      i4SVlanId);
    if (pAstPortEntry == NULL)
    {
        return;
    }

    if (AstGetRemainingTime ((VOID *) pAstPortEntry, AST_TMR_TYPE_HELLOWHEN,
                             &u4HelloTmr) == RST_FAILURE)
    {
        u4HelloTmr = 0;
    }

    pAstPerStRstPortEntry =
        AST_GET_PERST_RST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                     RST_DEFAULT_INSTANCE);

    if (pAstPerStRstPortEntry != NULL)
    {

        if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                 AST_TMR_TYPE_FDWHILE,
                                 &u4FwdWhileTmr) == RST_FAILURE)
        {
            u4FwdWhileTmr = 0;
        }
        if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                 AST_TMR_TYPE_TCWHILE,
                                 &u4TCWhileTmr) == RST_FAILURE)
        {
            u4TCWhileTmr = 0;
        }

    }

    AstPbRestoreContext ();
    AstReleaseContext ();

    if (i4SVlanId == AST_PB_CEP_PROT_PORT_NUM)
    {
        i4RetVal =
            AstCfaCliGetIfName ((UINT4) (i4NextPort), (INT1 *) au1IntfName);

        if (i4RetVal == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Port Name for Index %d is not  found \r\n",
                       i4NextPort);
            return;
        }

        CliPrintf (CliHandle, "\r\nCustomer Edge Port (%s) [Physical] is ",
                   au1IntfName);
    }
    else
    {
        CliPrintf (CliHandle, "\r\nProvider Edge Port (Service: %d) [Logical] "
                   "is ", i4SVlanId);
    }
    StpCliPrintRstPortRole (CliHandle, i4PortRole);

    CliPrintf (CliHandle, ", ");

    StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
    CliPrintf (CliHandle, "\n");

    CliPrintf (CliHandle, "Port PathCost %d, ", i4PathCost);
    CliPrintf (CliHandle, "Port Priority %d, ", i4Priority);
    CliPrintf (CliHandle, "Port Identifier  %3d.%d \r\n", i4Priority,
               i4NextPort);

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);
    AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
    PrintMacAddress (RetRootBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1RootBrgAddr);
    CliPrintf (CliHandle, "Designated Root has priority %d,", u2RootPrio);
    CliPrintf (CliHandle, " address %s \r\n", au1RootBrgAddr);

    u2BrgPrio = 0;
    u2BrgPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1BrgAddr);
    CliPrintf (CliHandle, "Designated Bridge has priority %d,", u2BrgPrio);
    CliPrintf (CliHandle, " address %s \r\n", au1BrgAddr);

    u2PortPrio = 0;
    u2Val = 0;
    u2PortPrio = (UINT2) (RetPortId.pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
    u2Val = AstCliGetPortIdFromOctetList (RetPortId);
    u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);
    CliPrintf (CliHandle, "Designated Port Id is %d.%d,", u2PortPrio, u2Val);

    CliPrintf (CliHandle, " Designated PathCost %d \r\n", i4DesigCost);
    CliPrintf (CliHandle, "No of Transitions to forwarding State :%d\r\n",
               i4FwdTransition);

    if (i4PortFast == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "PortFast is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "PortFast is disabled\r\n");
    }

    if (i4LinkType == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "LinkType is point to Point\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Link Type is Shared\r\n");
    }

    CliPrintf (CliHandle, "BPDUs : sent %d ,", u4TxBpduCount);
    CliPrintf (CliHandle, " received %d\r\n", u4RxBpduCount);
    CliPrintf (CliHandle,
               "Timers: Hello - %d, Forward Delay - %d, Topology Change - %d\r\n",
               u4HelloTmr, u4FwdWhileTmr, u4TCWhileTmr);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbPortTxBpduCount                            */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of transmitted RSTP  */
/*                        config  and TCN BPDU s count for CVLAN component   */
/*                                                                           */
/*     INPUT            : i4NextPort - PortIndex                             */
/*                        i4SVlanId  - Service VlanId                        */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4TxBpduCount                                      */
/*****************************************************************************/

UINT4
RstCliPbPortTxBpduCount (INT4 i4NextPort, INT4 i4SVlanId)
{
    UINT4               u4Count;
    UINT4               u4TxBpduCount;

    /* Rst BPDU */
    nmhGetFsMIPbRstCVlanPortTxRstBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4TxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIPbRstCVlanPortTxConfigBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4TxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIPbRstCVlanPortTxTcnBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4TxBpduCount += u4Count;

    return u4TxBpduCount;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbPortRxBpduCount                            */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of received  RSTP    */
/*                        config  and TCN BPDU s count for CVLAN component   */
/*                                                                           */
/*                                                                           */
/*     INPUT            : i4NextPort - PortIndex                             */
/*                        i4SVlanId  - Service Vlan Id                       */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4RxBpduCount                                      */
/*****************************************************************************/

UINT4
RstCliPbPortRxBpduCount (INT4 i4NextPort, INT4 i4SVlanId)
{
    UINT4               u4Count;
    UINT4               u4RxBpduCount;

    /* Rst BPDU */
    nmhGetFsMIPbRstCVlanPortRxRstBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4RxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIPbRstCVlanPortRxConfigBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4RxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIPbRstCVlanPortRxTcnBpduCount (i4NextPort, i4SVlanId, &u4Count);
    u4RxBpduCount += u4Count;

    return u4RxBpduCount;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbShowSpanningTree                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the customer spanning tree  */
/*                        information for the given port or for all ceps in  */
/*                        the system.                                        */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - IfIndex of the CEP                     */
/*                        u4Active - whether to display for only active ports*/
/*                        u1IsDetail - Is detailed informtion to be shown or */
/*                        not.                                               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
RstCliPbShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4IfIndex, UINT4 u4Active, UINT1 u1IsDetail)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4NextCepPort = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    if (i4IfIndex != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (i4IfIndex);

        if (pAstPortEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }

        if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) != AST_CUSTOMER_EDGE_PORT)
        {
            CliPrintf (CliHandle, "\r%% This Interface is not Customer Edge Port."
                       "No CVLAN Component exists in this Interface \r\n");
            return CLI_SUCCESS;
        }

        i4RetVal = AstCfaCliGetIfName ((UINT4) (i4IfIndex),
                                       (INT1 *) au1IntfName);
        if (i4RetVal == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Port Name for Index %d is not  found \r\n",
                       i4NextCepPort);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nPort [%s] ", au1IntfName);

        if (u1IsDetail == AST_SNMP_TRUE)
        {
            RstCliPbShowCepSpanningTreeDetail (CliHandle, i4IfIndex, u4Active);
        }
        else
        {
            RstCliPbShowCepSpanningTree (CliHandle, i4IfIndex, u4Active);
        }

        return CLI_SUCCESS;
    }

    /* IfIndex is not given in the command. So display all C-VLAN components 
     * spanning tree information. */
    while (AstPbGetNextCepPortInCurrContext
           (u4ContextId, (UINT4) i4IfIndex,
            (UINT4 *) &i4NextCepPort) == RST_SUCCESS)
    {
        i4RetVal = AstCfaCliGetIfName ((UINT4) (i4NextCepPort),
                                       (INT1 *) au1IntfName);
        if (i4RetVal == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Port Name for Index %d is not  found \r\n",
                       i4NextCepPort);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nPort [%s] ", au1IntfName);

        if (u1IsDetail == AST_SNMP_TRUE)
        {
            RstCliPbShowCepSpanningTreeDetail (CliHandle, i4NextCepPort,
                                               u4Active);
        }
        else
        {
            RstCliPbShowCepSpanningTree (CliHandle, i4NextCepPort, u4Active);
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    " -------------------------------------------------------\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }
        i4IfIndex = i4NextCepPort;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbShowCepSpanningTreeDetail                  */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the detail info of the     */
/*                        customer spanning tree for the given customer edge */
/*                        port.                                              */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - IfIndex of the customer edge port      */
/*                        u4Active - whether to display for only active      */
/*                                   ports.                                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
RstCliPbShowCepSpanningTreeDetail (tCliHandle CliHandle, INT4 i4IfIndex,
                                   UINT4 u4Active)
{
    INT4                i4CurrentPort;
    INT4                i4NextPort;
    INT4                i4PortState;
    INT1                i1OutCome;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1Flag = 0;
    UINT1               u1OperStatus;
    INT4                i4NextVlanId;
    INT4                i4CurrentVlanId;

    /* Display the Bridge Info */
    RstCliPbDisplayBridgeDetails (CliHandle, i4IfIndex);

    /* Get the Current Port frm the Table */
    i1OutCome = nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4IfIndex,
                                                            &i4NextPort, 0,
                                                            &i4NextVlanId);

    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4NextPort != i4IfIndex)
        {
            break;
        }
        /* For CEP, the vlanId will be 0xfff. 
         * So this check is used to identify whether port is CEP or PEP 
         */
        if (i4NextVlanId == AST_PB_CEP_PROT_PORT_NUM)
        {
            AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                       &u1OperStatus);
        }
        else
        {
            AstL2IwfGetPbPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                         (tVlanId) i4NextVlanId, &u1OperStatus);
        }

        if (u1OperStatus != AST_PORT_OPER_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                nmhGetFsMIPbRstCVlanPortState (i4NextPort, i4NextVlanId,
                                               &i4PortState);

                if ((i4PortState == AST_PORT_STATE_LEARNING) ||
                    (i4PortState == AST_PORT_STATE_FORWARDING))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                /* Display Port Details */
                RstCliPbDisplayPortDetails (CliHandle, i4NextPort,
                                            i4NextVlanId);

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }
            }
        }
        u1Flag = 0;
        /*Get the next Port from the Table */
        i4CurrentPort = i4NextPort;
        i4CurrentVlanId = i4NextVlanId;
        i1OutCome = nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4CurrentPort,
                                                                &i4NextPort,
                                                                i4CurrentVlanId,
                                                                &i4NextVlanId);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbDisplayBridgeDetails                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Customer Bridge Details */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - IfIndex of the CEP.                    */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliPbDisplayBridgeDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE BridgeId;
    UINT1              *pu1List = NULL;
    INT4                i4HelloTime;
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4RootFwdDelay;
    INT4                i4RootHelloTime;
    INT4                i4RootMaxAge;
    INT4                i4ModStatus = RST_DISABLED;
    INT4                i4HoldTime;
#ifdef MSTP_WANTED
    INT4                i4RetVal;
#endif /*MSTP_WANTED */
    UINT4               u4TopChange;
    UINT4               u4TopTime;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2BrgPrio = 0;
    UINT1               au1RootBrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    tAstMacAddr         MacAddress;

    MEMSET (au1RootBrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    BridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    BridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    if (AstGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return;
    }

    /* Get the module status for the virtual context. If that
     * is disabled, then C-VLAN module status is also disabled. */
    if (AstIsRstStartedInContext (u4ContextId))
    {
        nmhGetFsDot1dStpPortEnable (i4IfIndex, &i4ModStatus);
    }
#ifdef MSTP_WANTED
    if (AstIsMstStartedInContext (u4ContextId))
    {
        i4RetVal = MST_FORCE_STATE_DISABLED;
        nmhGetFsMIMstCistForcePortState (i4IfIndex, &i4RetVal);
        if (i4RetVal == MST_FORCE_STATE_ENABLED)
        {
            i4ModStatus = RST_ENABLED;
        }
        else
        {
            i4ModStatus = RST_DISABLED;
        }
    }
#endif

    /* Bridge Priority -- can be derived from bridge id */
    /*Bridge Address -- can be get from the bridge id */
    nmhGetFsMIPbRstCVlanBridgeId (i4IfIndex, &BridgeId);

    pu1List = &(BridgeId.pu1_OctetList[AST_BRG_PRIORITY_SIZE]);

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    u2BrgPrio = AstGetBrgPrioFromBrgId (BridgeId);

    AST_GET_MAC_ADDR (MacAddress, pu1List);

    /*Hello Time */

    nmhGetFsMIPbRstCVlanStpHelloTime (i4IfIndex, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */

    nmhGetFsMIPbRstCVlanStpMaxAge (i4IfIndex, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsMIPbRstCVlanStpForwardDelay (i4IfIndex, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* To Check  if the  Bridge is a ROOT Bridge */

    /* Get the RootBridge ID- Priority and MacAddress */

    AST_MEMSET (au1RootBrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPbRstCVlanBridgeDesignatedRoot (i4IfIndex, &RetBridgeId);

    /* Topology Changes */

    nmhGetFsMIPbRstCVlanStpTopChanges (i4IfIndex, &u4TopChange);

    /* Time Since Topology Changes */

    nmhGetFsMIPbRstCVlanStpTimeSinceTopologyChange (i4IfIndex, &u4TopTime);
    u4TopTime = u4TopTime / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    u4TopTime = AST_MGMT_TO_SYS (u4TopTime);

    /*Hold Time */

    nmhGetFsMIPbRstCVlanBridgeTxHoldCount (i4IfIndex, &i4HoldTime);
    /*Root Bridge Hello time */

    nmhGetFsMIPbRstCVlanBridgeHelloTime (i4IfIndex, &i4RootHelloTime);
    i4RootHelloTime = AST_MGMT_TO_SYS (i4RootHelloTime);

    /*root  Bridge MaxAge */

    nmhGetFsMIPbRstCVlanBridgeMaxAge (i4IfIndex, &i4RootMaxAge);
    i4RootMaxAge = AST_MGMT_TO_SYS (i4RootMaxAge);

    /* Root Bridge Forward Delay */

    nmhGetFsMIPbRstCVlanBridgeForwardDelay (i4IfIndex, &i4RootFwdDelay);
    i4RootFwdDelay = AST_MGMT_TO_SYS (i4RootFwdDelay);

    if (i4ModStatus == RST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\n CVLAN Component RSTP for Interface 0/%d is disabled\r\n");
    }

    else
    {
        CliPrintf (CliHandle, "\n CVLAN Bridge for Interface 0/%d is "
                   "enabled \n"
                   "Executing RSTP to participate in Customer Spanning"
                   " Tree Protocol\r\n", i4IfIndex);
    }

    CliPrintf (CliHandle, "Bridge Identifier has priority %d, ", u2BrgPrio);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "Address %s\r\n", au1BrgAddr);

    CliPrintf (CliHandle, "Configured Hello time %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, " Max Age %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs \r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    /* Root Bridge Info */
    if (AST_MEMCMP ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                    MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle,
                   "We are the root of the customer spanning tree\r\n");
    }

    CliPrintf (CliHandle, "Number of Topology Changes %d \r\n", u4TopChange);

    CliPrintf (CliHandle, "Time since topology Change %d seconds ago\r\n",
               u4TopTime);
    CliPrintf (CliHandle, "Transmit Hold-Count %d \r\n", i4HoldTime);
    CliPrintf (CliHandle, "Max Age %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4RootMaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootMaxAge));
    CliPrintf (CliHandle, " Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4RootFwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootFwdDelay));
    CliPrintf (CliHandle, "Hello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4RootHelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootHelloTime));

    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbShowCepSpanningTree                        */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the summary of the         */
/*                        customer spanning tree for the given customer edge */
/*                        port.                                              */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - IfIndex of the customer edge port      */
/*                        u4Active - whether to display for only active      */
/*                                   ports.                                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
RstCliPbShowCepSpanningTree (tCliHandle CliHandle, INT4 i4IfIndex,
                             UINT4 u4Active)
{
    tSNMP_OCTET_STRING_TYPE BridgeId;
    INT4                i4CurrentVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4ModStatus = 0;
    INT4                i4HelloTime = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4PortRole = 0;
    INT4                i4PortState = 0;
    INT4                i4PortStateStatus = AST_BLOCK_SUCCESS;
    INT4                i4PortCost = 0;
    INT4                i4PortPriority = 0;
    INT4                i4LinkType = 0;
    INT4                i4RetVal = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort = 0;
    UINT4               u4ContextId = 0;
    UINT1              *pu1List = NULL;
    UINT2               u2LocalPortId = 0;
    UINT2               u2BrgPrio = 0;
    tAstMacAddr         MacAddress;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               i1OutCome = 0;
    UINT1               u1Flag = 0;
    UINT4               u4Quit = CLI_SUCCESS;
    UINT1               u1OperStatus;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    BridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    BridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    if (AstGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Get the module status for the virtual context. If that
     * is disabled, then C-VLAN module status is also disabled. */
    if (AstIsRstStartedInContext (u4ContextId))
    {
        nmhGetFsDot1dStpPortEnable (i4IfIndex, &i4ModStatus);
    }
#ifdef MSTP_WANTED
    if (AstIsMstStartedInContext (u4ContextId))
    {
        i4RetVal = MST_FORCE_STATE_DISABLED;
        nmhGetFsMIMstCistForcePortState (i4IfIndex, &i4RetVal);
        if (i4RetVal == MST_FORCE_STATE_ENABLED)
        {
            i4ModStatus = RST_ENABLED;
        }
        else
        {
            i4ModStatus = RST_DISABLED;
        }
    }
#endif

    /*Display Root Bridge Details */
    RstCliPbShowRootBridgeInfo (CliHandle, i4IfIndex);

    /*Display Bridge Details */

    /* Bridge Priority -- can be derived from bridge id */
    /*Bridge Address -- can be get from the bridge id */
    nmhGetFsMIPbRstCVlanBridgeId (i4IfIndex, &BridgeId);

    pu1List = &(BridgeId.pu1_OctetList[AST_BRG_PRIORITY_SIZE]);

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    u2BrgPrio = AstGetBrgPrioFromBrgId (BridgeId);

    AST_GET_MAC_ADDR (MacAddress, pu1List);

    /*Hello Time */

    nmhGetFsMIPbRstCVlanStpHelloTime (i4IfIndex, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */
    nmhGetFsMIPbRstCVlanStpMaxAge (i4IfIndex, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */
    nmhGetFsMIPbRstCVlanStpForwardDelay (i4IfIndex, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (i4ModStatus == RST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nCustomer Spanning tree Protocol has been disabled\r\n");
    }
    else
    {

        CliPrintf (CliHandle,
                   "\r\nCustomer Spanning Tree Enabled Protocol RSTP \r\n");

    }

    CliPrintf (CliHandle, "Bridge Id       Priority %d\r\n ", u2BrgPrio);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "               Address %s\r\n", au1BrgAddr);

    CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "Max Age %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "                Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    i4RetVal = AstCfaCliGetIfName ((UINT4) (i4IfIndex), (INT1 *) au1IntfName);
    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Port Name for Index %d is not  found \r\n",
                   i4IfIndex);
    }

    /* Display the Port Role,PortState,PortCost,PortPriority,PortLinkType */

    CliPrintf (CliHandle,
               "%-20s%-13s%-13s%-9s%-7s%-8s\r\n",
               "Name", "Role", "State", "Cost", "Prio", "Type");
    CliPrintf (CliHandle,
               "%-20s%-13s%-13s%-9s%-7s%-8s\r\n",
               "----", "----", "-----", "----", "----", "------");

    /* Get The Current Port */

    i1OutCome = nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4IfIndex,
                                                            &i4NextPort, 0,
                                                            &i4NextVlanId);

    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4NextPort != i4IfIndex)
        {
            break;
        }
        /* Get the port oper status for RSTP/MSTP */
        /* For CEP, the vlanId will be 0xfff. 
         * So this check is used to identify whether port is CEP or PEP 
         */
        if (i4NextVlanId == AST_PB_CEP_PROT_PORT_NUM)
        {
            AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                       &u1OperStatus);
        }
        else
        {
            AstL2IwfGetPbPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                         (tVlanId) i4NextVlanId, &u1OperStatus);
        }

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                nmhGetFsMIPbRstCVlanPortState (i4NextPort, i4NextVlanId,
                                               &i4PortState);

                if ((i4PortState == AST_PORT_STATE_LEARNING) ||
                    (i4PortState == AST_PORT_STATE_FORWARDING))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                /*PortRole */
                nmhGetFsMIPbRstCVlanPortRole (i4NextPort, i4NextVlanId,
                                              &i4PortRole);

                /*Port State */
                nmhGetFsMIPbRstCVlanPortState (i4NextPort, i4NextVlanId,
                                               &i4PortState);
                /*Port Cost */
                nmhGetFsMIPbRstCVlanPortPathCost (i4NextPort, i4NextVlanId,
                                                  &i4PortCost);
                /*PortPriority */
                nmhGetFsMIPbRstCVlanPortPriority (i4NextPort, i4NextVlanId,
                                                  &i4PortPriority);
                /* Link Type */
                nmhGetFsMIPbRstCVlanPortOperPointToPoint (i4NextPort,
                                                          i4NextVlanId,
                                                          &i4LinkType);

                if (i4NextVlanId == AST_PB_CEP_PROT_PORT_NUM)
                {
                    CliPrintf (CliHandle, "CEP-%-9s       ", au1IntfName);
                }
                else
                {
                    CliPrintf (CliHandle, "PEP-Service: %-4d   ", i4NextVlanId);
                }

                StpCliPrintRstPortRole (CliHandle, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");

                StpCliDisplayPortState (CliHandle, i4PortState,
                                        i4PortStateStatus);
                CliPrintf (CliHandle, "%-3s", " ");

                CliPrintf (CliHandle, "%-9d", i4PortCost);

                CliPrintf (CliHandle, "%-7d", i4PortPriority);

                if (i4LinkType == AST_SNMP_TRUE)
                {
                    /* u4Quit contains the paging status */
                    u4Quit = CliPrintf (CliHandle, "P2P\r\n");
                }
                else
                {
                    u4Quit = CliPrintf (CliHandle, "SharedLan\r\n");
                }
                /* Get the Next Port from the Table */

            }
            if (u4Quit == CLI_FAILURE)
            {
                break;
            }
        }
        u1Flag = 0;
        i4CurrentPort = i4NextPort;
        i4CurrentVlanId = i4NextVlanId;
        i1OutCome =
            nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (i4CurrentPort,
                                                        &i4NextPort,
                                                        i4CurrentVlanId,
                                                        &i4NextVlanId);

    }                            /*while */

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPbShowRootBridgeInfo                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the customer rootbridge*/
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - CEP Port                               */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliPbShowRootBridgeInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tAstMacAddr         MacAddress;
    INT4                i4Count = 1;
    INT4                i4NextSvid = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4CurrSvid = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4PortRole;
    INT4                i4RootCost = 0;
    INT4                i4HelloTime = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4MaxAge = 0;
    tSNMP_OCTET_STRING_TYPE BridgeId;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1              *pu1List = NULL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT2               u2RootPrio = 0;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT1                i1FirstPort = RST_TRUE;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    BridgeId.pu1_OctetList = &au1BrgBuf[0];
    BridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;
    /* To Check  if the  Bridge is a ROOT Bridge */

    nmhGetFsMIPbRstCVlanBridgeId (i4IfIndex, &BridgeId);

    pu1List = &(BridgeId.pu1_OctetList[AST_BRG_PRIORITY_SIZE]);

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    AST_GET_MAC_ADDR (MacAddress, pu1List);

    /*Displays the Root Id,Root Timers */

    /* Get The  Root ID-Priority and MacAddress */

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPbRstCVlanBridgeDesignatedRoot (i4IfIndex, &RetBridgeId);

    /*Root Cost */

    nmhGetFsMIPbRstCVlanBridgeRootCost (i4IfIndex, &i4RootCost);

    /* Root HelloTime */
    nmhGetFsMIPbRstCVlanBridgeHelloTime (i4IfIndex, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Root MaxAge */
    nmhGetFsMIPbRstCVlanBridgeMaxAge (i4IfIndex, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */
    nmhGetFsMIPbRstCVlanBridgeForwardDelay (i4IfIndex, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (AST_MEMCMP
        ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
         MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "\r\nWe are the root of the Spanning Tree\r\n");

    }

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    CliPrintf (CliHandle, "Root Id         Priority   %d\r\n", u2RootPrio);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                     au1BrgAddr);
    CliPrintf (CliHandle, "                Address    %s\r\n ", au1BrgAddr);

    CliPrintf (CliHandle, "               Cost       %d\r\n", i4RootCost);

    i4CurrIfIndex = i4IfIndex;
    i4CurrSvid = 0;

    CliPrintf (CliHandle, "                Root Ports  ");
    while ((nmhGetNextIndexFsMIPbRstCVlanPortInfoTable
            (i4CurrIfIndex, &i4NextIfIndex, i4CurrSvid, &i4NextSvid)
            == SNMP_SUCCESS))
    {
        if (i4NextIfIndex != i4IfIndex)
        {
            break;
        }
        nmhGetFsMIPbRstCVlanPortRole (i4IfIndex, i4NextSvid, &i4PortRole);

        if (i4PortRole == AST_PORT_ROLE_ROOT)
        {
            if (i1FirstPort == RST_TRUE)
            {
                i1FirstPort = RST_FALSE;
            }
            else
            {
                CliPrintf (CliHandle, ", ");
                if ((i4Count % 4) == 0)
                {
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "                            ");
                }
                i4Count++;
            }

            if (i4NextSvid == AST_PB_CEP_PROT_PORT_NUM)
            {
                i4RetVal = AstCfaCliGetIfName ((UINT4) (i4IfIndex),
                                               (INT1 *) au1IntfName);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Port Name for Index %d is not  found \r\n",
                               i4IfIndex);
                    return;
                }
                CliPrintf (CliHandle, "%-9s", au1IntfName);
            }
            else
            {
                CliPrintf (CliHandle, "Service: %d", i4NextSvid);
            }
        }

        i4CurrIfIndex = i4NextIfIndex;
        i4CurrSvid = i4NextSvid;
    }
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "Max Age %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "                Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    return;

}

/*****************************************************************************/
/* Function Name      : RstpPbPrintModuleStatus                              */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Spanning tree                                    */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
RstpPbPrintModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4ModStatus = RST_DISABLED;
    INT4                i4ContextId = (INT4) u4ContextId;

    /*Module Status */

    nmhGetFsMIPbProviderStpStatus (i4ContextId, &i4ModStatus);

    if (i4ModStatus == MST_DISABLED)
    {
        CliPrintf (CliHandle, "no spanning-tree provider\r\n");
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstCliPrintStpModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Global Spanning tree and SVLAN Spanning Tree     */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
RstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4GlobalModStatus, INT4 i4Version)
{
    INT4                i4SVlanModuleStatus = RST_DISABLED;
    UINT4               u4BridgeMode = AST_CUSTOMER_BRIDGE_MODE;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode) == L2IWF_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4GlobalModStatus == RST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nSpanning tree Protocol has been disabled\r\n");

        if ((u4BridgeMode == AST_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_EDGE_BRIDGE_MODE))
        {
            /* Show the S-VLAN spanning tree status. */
            nmhGetFsMIPbProviderStpStatus (i4ContextId, &i4SVlanModuleStatus);

            if (i4SVlanModuleStatus == RST_DISABLED)
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol has "
                           "been disabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol enabled\r\n");
            }
        }
    }
    else
    {

        if ((u4BridgeMode == AST_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_EDGE_BRIDGE_MODE))
        {
            CliPrintf (CliHandle, "\r\nSpanning tree Protocol Enabled. \r\n");

            /* Show the S-VLAN spanning tree status. */
            nmhGetFsMIPbProviderStpStatus (i4ContextId, &i4SVlanModuleStatus);

            if (i4SVlanModuleStatus == RST_DISABLED)
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol has "
                           "been disabled\r\n");
            }
            else
            {
                if (i4Version == AST_VERSION_0)
                {
                    CliPrintf (CliHandle,
                               "\r\nS-VLAN component is executing the stp "
                               "compatible Rapid Spanning Tree Protocol\r\n");
                }
                else if (i4Version == AST_VERSION_2)
                {
                    CliPrintf (CliHandle,
                               "\r\nS-VLAN component is executing the rstp "
                               "compatible Rapid Spanning Tree Protocol\r\n ");
                }
            }
        }
        else
        {
            if (i4Version == AST_VERSION_0)
            {
                CliPrintf (CliHandle,
                           "\r\nBridge is executing the stp compatible "
                           "Rapid Spanning Tree Protocol\r\n");
            }
            else if (i4Version == AST_VERSION_2)
            {
                CliPrintf (CliHandle,
                           "\r\nBridge is executing the rstp compatible "
                           "Rapid Spanning Tree Protocol\r\n");
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstSetCVlanDebug                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Debugging support           */
/*                        for the RSTP                                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  - Handle to the CLI Context            */
/*                        u4RstDebug  - Debug Value                          */
/*                        u1Action    - Set/No Command                       */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstSetCVlanDebug (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4RstDbg,
                  UINT1 u1Action)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4DbgVal = 0;

    UNUSED_PARAM (CliHandle);

    if (u1Action == RST_NO_CMD)
    {
        /* For Debug Disable, Get the existing Debug Value 
         * and negate that value
         */
        nmhGetFsMIPbRstCVlanStpDebugOption ((INT4) u4IfIndex,
                                            (INT4 *) &u4DbgVal);
        u4DbgVal = u4DbgVal & (~u4RstDbg);
    }
    else if (u1Action == RST_SET_CMD)
    {
        u4DbgVal = u4RstDbg;
    }

    if (nmhTestv2FsMIPbRstCVlanStpDebugOption (&u4ErrCode, (INT4) u4IfIndex,
                                               (INT4) u4DbgVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsMIPbRstCVlanStpDebugOption ((INT4) u4IfIndex, (INT4) u4DbgVal);

    return CLI_SUCCESS;

}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : MstCliPrintStpModuleStatus                           */
/*                                                                           */
/* Description        : This function is used to show the module status of   */
/*                      the Global Spanning tree and SVLAN Spanning Tree     */
/*                                                                           */
/* Input(s)           : CliHandle - CLI context Id                           */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
MstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4GlobalModStatus, INT4 i4Version)
{
    INT4                i4SVlanModuleStatus = MST_DISABLED;
    UINT4               u4BridgeMode = AST_CUSTOMER_BRIDGE_MODE;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode) == L2IWF_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4GlobalModStatus == MST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nSpanning tree Protocol has been disabled\r\n");

        /* In case of 1ad bridge, show the status of the S-VLAN component. */
        if ((u4BridgeMode == AST_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_EDGE_BRIDGE_MODE))
        {
            /* Show the S-VLAN spanning tree status. */
            nmhGetFsMIPbProviderStpStatus (i4ContextId, &i4SVlanModuleStatus);

            if (i4SVlanModuleStatus == RST_DISABLED)
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol has "
                           "been disabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol enabled\r\n");
            }
        }
    }
    else
    {

        if ((u4BridgeMode == AST_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_EDGE_BRIDGE_MODE))
        {
            CliPrintf (CliHandle, "\r\nSpanning tree Protocol Enabled. \r\n");

            /* Show the S-VLAN spanning tree status. */
            nmhGetFsMIPbProviderStpStatus (i4ContextId, &i4SVlanModuleStatus);

            if (i4SVlanModuleStatus == RST_DISABLED)
            {
                CliPrintf (CliHandle,
                           "\r\nProvider Spanning tree Protocol has "
                           "been disabled\r\n");
            }
            else
            {
                if (i4Version == AST_VERSION_0)
                {
                    CliPrintf (CliHandle,
                               "\r\nS-VLAN Component: MST00 is executing the "
                               "stp compatible Multiple Spanning Tree "
                               "Protocol\r\n");
                }
                else if (i4Version == AST_VERSION_2)
                {
                    CliPrintf (CliHandle,
                               "\r\nS-VLAN Component: MST00 is executing the "
                               "rstp compatible Multiple Spanning Tree "
                               "Protocol\r\n");
                }
                else if (i4Version == AST_VERSION_3)
                {
                    CliPrintf (CliHandle,
                               "\r\nS-VLAN Component: MST00 is executing the "
                               "mstp compatible Multiple Spanning Tree "
                               "Protocol\r\n");
                }
            }
        }
        else
        {
            if (i4Version == AST_VERSION_0)
            {
                CliPrintf (CliHandle,
                           "\r\nMST00 is executing the stp compatible "
                           "Multiple Spanning Tree Protocol\r\n");
            }
            else if (i4Version == AST_VERSION_2)
            {
                CliPrintf (CliHandle,
                           "\r\nMST00 is executing the rstp compatible "
                           "Multiple Spanning Tree Protocol\r\n");
            }
            else if (i4Version == AST_VERSION_3)
            {
                CliPrintf (CliHandle,
                           "\r\nMST00 is executing the mstp compatible "
                           "Multiple Spanning Tree Protocol\r\n");
            }
        }

    }

    return CLI_SUCCESS;
}
#endif /*MSTP_WANTED */
#endif /*__STPBCLI_C*/
