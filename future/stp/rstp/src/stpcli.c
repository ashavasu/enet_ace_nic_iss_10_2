
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stpcli.c,v 1.117 2017/12/22 10:06:02 siva Exp $
*
* Description: CLI routines invocation
*********************************************************************/
/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : stpcli.c                                         |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : RSTP/MSTP                                        |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI RSTP/MSTP Commands       |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 3rd Nov 2004    |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#ifndef __STPCLI_C__
#define __STPCLI_C__

#include "asthdrs.h"
#include "stpcli.h"
#include "stpclipt.h"
#ifdef MSTP_WANTED
#include "astmlow.h"
#endif
#ifdef PVRST_WANTED
#include "fspvrslw.h"
#endif
#ifdef L2RED_WANTED
#include "astpred.h"
#else
#include "astprdstb.h"
#endif
#include "vcm.h"

UINT4               gu4StpTrcLvl;
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_stp_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function processes all the STP commands       */
/*                               
                                                                             */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to  the CLI Context               */
/*                        
                          u4Command- Command Identifier to be passed         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_stp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT4              *args[STP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4Value = 0;
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4CmdType = 0;
    UINT1               u1Action = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ContextId;
    UINT4               u4ContextOfIfIndex;
    UINT1               u1BpduGuardAction = AST_INIT_VAL;
    UINT2               u2LocalPortId = 0;
#if defined MSTP_WANTED || PVRST_WANTED
    UINT4               u4Instance = 0;
#endif
    tMacAddr            au1InMacAddr;
    INT4                i4Status;
#ifdef MSTP_WANTED
    UINT1               au1RegionName[MST_CONFIG_NAME_LEN];
    UINT1               au1MstVlanList[MST_VLAN_LIST_SIZE];
    UINT1               au1Cmd[MAX_PROMPT_LEN];
#endif
    INT4                i4Args = 0;
#ifdef PVRST_WANTED
    INT4                i4IfIndex = 0;
#endif

    CliRegisterLock (CliHandle, AstLock, AstUnLock);

    AST_LOCK ();

    if (AstCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == STP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_STP_SHUT:
            /* Shut down spanning tree */

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetSystemShut (CliHandle);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetSystemShut (CliHandle);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetSystemShut (CliHandle);
            }
#endif
            /* Returning CLI_SUCCESS in case of spanning tree is already 
             * shutdown 
             */
            else
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

            /* System Control Status */
        case CLI_STP_SYS_CTRL:

            /* args[0] - Command for RSTP Start or MSTP Start or PVRST Start */

            u4Value = CLI_PTR_TO_U4 (args[0]);

            if (u4Value == RST_START)
            {

                i4RetVal = RstCliSetSystemCtrl (CliHandle);
            }
#ifdef MSTP_WANTED
            else if (u4Value == MST_START)
            {
                i4RetVal = MstCliSetSystemCtrl (CliHandle);
            }
#endif
#ifdef PVRST_WANTED
            else if ((u4Value == PVRST_START))
            {
                if (u4ContextId != AST_DEFAULT_CONTEXT)
                {
                    CliPrintf (CliHandle,
                               "\r%% PVRST Module Cannot"
                               " be Started in Multiple Context\r\n");
                    break;
                }
                else
                {
                    i4RetVal = PvrstCliSetSystemCtrl (CliHandle);
                }
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Multiple Spanning Tree Module is not"
                           " present in this solution\r\n");
            }
            break;

            /*Module Status */
        case CLI_STP_MOD_STATUS:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = RST_ENABLED;
                i4RetVal = RstCliSetModStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_ENABLED;
                i4RetVal = MstCliSetModStatus (CliHandle, u4Value);

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = PVRST_ENABLED;
                i4RetVal = PvrstCliSetModStatus (CliHandle, u4Value);
            }
#endif
#ifdef MSTP_WANTED
            else
            {
                i4RetVal = MstCliSetSystemCtrl (CliHandle);
            }
#else
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Multiple Spanning Tree Module is not"
                           " present in this solution\r\n");
            }
#endif

            break;

        case CLI_STP_NO_MOD_STATUS:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = RST_DISABLED;
                i4RetVal = RstCliSetModStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DISABLED;
                i4RetVal = MstCliSetModStatus (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = PVRST_DISABLED;
                i4RetVal = PvrstCliSetModStatus (CliHandle, u4Value);
            }
#endif

            break;
        case CLI_STP_GBL_BPDUGUARD_ENABLE:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal = RstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal = MstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal = PvrstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_GBL_BPDUGUARD_DISABLE:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal = RstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal = MstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal = PvrstCliSetBpduGuardStatus (CliHandle, u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_AST_PERF_DATA_ENABLED:

            u4Value = AST_PERFORMANCE_STATUS_ENABLE;

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetStpPerformanceStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetStpPerformanceStatus (CliHandle, u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_AST_PERF_DATA_DISABLED:

            u4Value = AST_PERFORMANCE_STATUS_DISABLE;

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetStpPerformanceStatus (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetStpPerformanceStatus (CliHandle, u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_COMP_MODE:

            /* args[0] - STP version 
             * i.e STP compatible or RSTP or MSTP */

            u4Value = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {

                i4RetVal = RstCliSetBrgVersion (CliHandle, u4Value);

            }
#ifdef MSTP_WANTED

            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetBrgVersion (CliHandle, u4Value);

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetBrgVersion (CliHandle, u4Value);
            }
#endif
            break;

        case CLI_STP_NO_COMP_MODE:

            if (AST_IS_RST_STARTED ())
            {

                u4Value = AST_VERSION_2;
                i4RetVal = RstCliSetBrgVersion (CliHandle, u4Value);

            }
#ifdef MSTP_WANTED

            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_VERSION_3;
                i4RetVal = MstCliSetBrgVersion (CliHandle, u4Value);

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_VERSION_2;
                i4RetVal = PvrstCliSetBrgVersion (CliHandle, u4Value);
            }
#endif

            break;
        case CLI_STP_BRG_TIMES:

            if (AST_IS_RST_STARTED ())
            {

                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */
                /*args[1]-> u4Value i.e Values of the timers. */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);
                u4Value = u4Value * AST_CENTI_SECONDS;

                if (u4CmdType == STP_FORWARD_TIME)
                {

                    i4RetVal = RstCliSetFwdTime (CliHandle, u4Value);

                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    i4RetVal = RstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {
                    i4RetVal = RstCliSetMaxAge (CliHandle, u4Value);
                }

            }

#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */
                /*args[1]-> u4Value i.e Values of the timers. */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);
                u4Value = u4Value * AST_CENTI_SECONDS;

                if (u4CmdType == STP_FORWARD_TIME)
                {
                    i4RetVal = MstCliSetFwdTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    i4RetVal = MstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {
                    i4RetVal = MstCliSetMaxAge (CliHandle, u4Value);
                }

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% PVRST is Started Enter Vlan Id For Configuration \r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

        case CLI_STP_ERROR_RECOVERY:

            if (AST_IS_RST_STARTED ())
            {
                /* args[0] - RST Interface Error Recovery Time */

                u4Value = CLI_PTR_TO_U4 (args[0]);
                u4Value = u4Value * AST_CENTI_SECONDS;
                i4RetVal = RstCliSetPortErrorRecovery (CliHandle,
                                                       (UINT4) u2LocalPortId,
                                                       u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                /* args[0] - MST Interface Error Recovery Time */

                u4Value = CLI_PTR_TO_U4 (args[0]);
                u4Value = u4Value * AST_CENTI_SECONDS;
                i4RetVal = MstCliSetPortErrorRecovery (CliHandle,
                                                       (UINT4) u2LocalPortId,
                                                       u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% PVRST does not support configuration of Error Disable Timer\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;
#ifdef MSTP_WANTED
        case CLI_MSTP_BRG_TIMES:

            if (AST_IS_MST_STARTED ())
            {

                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */
                /*args[1]-> u4Value i.e Values of the timers. */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);
                u4Value = u4Value * AST_CENTI_SECONDS;

                if (u4CmdType == STP_FORWARD_TIME)
                {
                    i4RetVal = MstCliSetFwdTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    i4RetVal = MstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {
                    i4RetVal = MstCliSetMaxAge (CliHandle, u4Value);
                }

            }
            else
            {

                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;
#endif

        case CLI_STP_NO_BRG_TIMES:

            if (AST_IS_RST_STARTED ())
            {
                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == STP_FORWARD_TIME)
                {
                    u4Value = RST_DEFAULT_BRG_FWD_DELAY;
                    i4RetVal = RstCliSetFwdTime (CliHandle, u4Value);

                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    u4Value = RST_DEFAULT_BRG_HELLO_TIME;
                    i4RetVal = RstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {
                    u4Value = RST_DEFAULT_BRG_MAX_AGE;
                    i4RetVal = RstCliSetMaxAge (CliHandle, u4Value);
                }

            }

#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */
                /*args[1]-> u4Value i.e Values of the timers. */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == STP_FORWARD_TIME)
                {

                    u4Value = MST_DEFAULT_BRG_FWD_DELAY;
                    i4RetVal = MstCliSetFwdTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    u4Value = MST_DEFAULT_BRG_HELLO_TIME;
                    i4RetVal = MstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {

                    u4Value = MST_DEFAULT_BRG_MAX_AGE;
                    i4RetVal = MstCliSetMaxAge (CliHandle, u4Value);
                }

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% PVRST is Started Enter Vlan Id For Configuration \r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

#ifdef MSTP_WANTED
        case CLI_MSTP_NO_BRG_TIMES:

            if (AST_IS_MST_STARTED ())
            {

                /*args[0]-> u4Time i.e Forward Time,HelloTime ,MaxAge */
                /*args[1]-> u4Value i.e Values of the timers. */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == STP_FORWARD_TIME)
                {

                    u4Value = MST_DEFAULT_BRG_FWD_DELAY;
                    i4RetVal = MstCliSetFwdTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_HELLO_TIME)
                {
                    u4Value = MST_DEFAULT_BRG_HELLO_TIME;
                    i4RetVal = MstCliSetHelloTime (CliHandle, u4Value);
                }
                else if (u4CmdType == STP_MAX_AGE)
                {

                    u4Value = MST_DEFAULT_BRG_MAX_AGE;
                    i4RetVal = MstCliSetMaxAge (CliHandle, u4Value);
                }

            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

#endif

        case CLI_STP_TX_HOLDCOUNT:

            /* args[0] - Transmit Hold Count in seconds */

            u4Value = *args[0];

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetBridgeTxHoldCount (CliHandle, u4Value);
            }

#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetBridgeTxHoldCount (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% PVRST is Started Enter Vlan Id For Configuration \r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

        case CLI_STP_NO_TX_HOLDCOUNT:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = RST_DEFAULT_BRG_TX_LIMIT;
                i4RetVal = RstCliSetBridgeTxHoldCount (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED

            else if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_BRG_TX_LIMIT;
                i4RetVal = MstCliSetBridgeTxHoldCount (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% PVRST is Started Enter Vlan Id For Configuration \r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif

            break;

        case CLI_STP_FLUSH_INTERVAL:

            /* args[0] - Flush interval value in centi-seconds */

            u4Value = *args[0];

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetFlushInterval (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetFlushInterval (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetFlushInterval (CliHandle, u4Value);
            }
#endif
            break;

        case CLI_STP_NO_FLUSH_INTERVAL:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = RST_DEFAULT_FLUSH_INTERVAL;
                i4RetVal = RstCliSetFlushInterval (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_FLUSH_INTERVAL;
                i4RetVal = MstCliSetFlushInterval (CliHandle, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = PVRST_DEFAULT_FLUSH_INTERVAL;
                i4RetVal = PvrstCliSetFlushInterval (CliHandle, u4Value);
            }
#endif
            break;

        case CLI_STP_FLUSH_IND_THRESHOLD:

            /* args[0] - Flush indication threshold value */

            u4Value = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetFlushIndThreshold (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstCliSetFlushIndThreshold (CliHandle, MST_CIST_CONTEXT,
                                                u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported in PVRST mode\r\n");
                i4RetVal = CLI_FAILURE;

            }
#endif

            break;

        case CLI_STP_NO_FLUSH_IND_THRESHOLD:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = RST_DEFAULT_FLUSH_IND_THRESHOLD;
                i4RetVal = RstCliSetFlushIndThreshold (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_FLUSH_IND_THRESHOLD;
                i4RetVal =
                    MstCliSetFlushIndThreshold (CliHandle, MST_CIST_CONTEXT,
                                                u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported in PVRST mode\r\n");
                i4RetVal = CLI_FAILURE;

            }
#endif

            break;

#ifdef MSTP_WANTED
        case CLI_MSTP_FLUSH_IND_THRESHOLD:

            /* args[0] - Instance ID */
            /* args[1] - Flush Indication threshold */

            u4Instance = CLI_PTR_TO_U4 (args[0]);
            u4Value = CLI_PTR_TO_U4 (args[1]);

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstCliSetFlushIndThreshold (CliHandle, u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_MSTP_NO_FLUSH_IND_THRESHOLD:

            /* args[0] - Instance ID */

            u4Instance = CLI_PTR_TO_U4 (args[0]);
            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_FLUSH_IND_THRESHOLD;
                i4RetVal =
                    MstCliSetFlushIndThreshold (CliHandle, u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_MSTP_MAX_HOPS:

            /* args[0]- MST Maximum no of Hops */

            u4Value = CLI_PTR_TO_U4 (args[0]);
            u4Value = u4Value * AST_CENTI_SECONDS;

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetMaxHops (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_MSTP_NO_MAX_HOPS:

            u4Value = MST_DEFAULT_MAX_HOPCOUNT;
            u4Value = u4Value * AST_CENTI_SECONDS;

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetMaxHops (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_MSTP_BRG_PRIORITY:

            /* args[0] - Instance ID */
            /* args[1] - BridgePriority */

            u4Instance = CLI_PTR_TO_U4 (args[0]);
            u4Value = CLI_PTR_TO_U4 (args[1]);

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstCliSetBrgPriority (CliHandle, u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;
        case CLI_MSTP_ROOT_PRIORITY:

            /* args[0] - Instance ID */
            /* args[1] - BridgePriority */

            u4Instance = CLI_PTR_TO_U4 (args[0]);
            u4Value = CLI_PTR_TO_U4 (args[1]);

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstCliSetRootPriority (CliHandle, u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;
#endif
        case CLI_STP_BRG_PRIORITY:

            /* args[1] - BridgePriority */

            u4Value = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetBrgPriority (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else
            {
                i4RetVal =
                    MstCliSetBrgPriority (CliHandle, MST_CIST_CONTEXT, u4Value);
            }
#endif
            break;
#ifdef MSTP_WANTED
        case CLI_MSTP_NO_BRG_PRIORITY:

            /* args[0] - Instance ID */
            u4Instance = CLI_PTR_TO_U4 (args[0]);
            u4Value = MST_DEFAULT_BRG_PRIORITY;

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstCliSetBrgPriority (CliHandle, u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

#endif
        case CLI_STP_NO_BRG_PRIORITY:

            if (AST_IS_I_COMPONENT () == RST_TRUE)
            {
                u4Value = AST_PB_CVLAN_BRG_PRIORITY;
            }
            else
            {
                u4Value = RST_DEFAULT_BRG_PRIORITY;
            }

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetBrgPriority (CliHandle, u4Value);
            }
#ifdef MSTP_WANTED
            else
            {
                i4RetVal =
                    MstCliSetBrgPriority (CliHandle, MST_CIST_CONTEXT, u4Value);
            }
#endif
            break;
        case CLI_STP_DYNAMIC_PATHCOST_CALC:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPathCostCalculation
                    (CliHandle, AST_PATHCOST_CALC_DYNAMIC);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstCliSetPathCostCalculation
                    (CliHandle, AST_PATHCOST_CALC_DYNAMIC);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetPathCostCalculation
                    (CliHandle, AST_PATHCOST_CALC_DYNAMIC);
            }
#endif
            break;

        case CLI_STP_DYNAMIC_LAGG_PATHCOST_CALC:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetLaggPathCostCalculation
                    (CliHandle, AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstCliSetLaggPathCostCalculation
                    (CliHandle, AST_SNMP_TRUE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetLaggPathCostCalculation
                    (CliHandle, AST_SNMP_TRUE);
            }
#endif
            break;

        case CLI_STP_NO_DYNAMIC_PATHCOST_CALC:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPathCostCalculation (CliHandle,
                                                         AST_PATHCOST_CALC_MANUAL);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstCliSetPathCostCalculation
                    (CliHandle, AST_PATHCOST_CALC_MANUAL);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetPathCostCalculation
                    (CliHandle, AST_PATHCOST_CALC_MANUAL);
            }
#endif
            break;

        case CLI_STP_NO_DYNAMIC_LAGG_PATHCOST_CALC:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetLaggPathCostCalculation (CliHandle,
                                                             AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstCliSetLaggPathCostCalculation
                    (CliHandle, AST_SNMP_FALSE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstCliSetLaggPathCostCalculation
                    (CliHandle, AST_SNMP_FALSE);
            }
#endif
            break;
        case CLI_STP_OPTIMIZATION_ALT_ROLE:

            if (!(AST_IS_RST_STARTED ()))
            {
                CliPrintf (CliHandle, "%% RSTP is shutdown!!!\r\n");
                i4RetVal = CLI_FAILURE;
            }
            else
            {
                /* args[0]- Optimization enabled/disabled status */
                u4Value = CLI_PTR_TO_U4 (args[0]);

                i4RetVal = RstCliSetForwardDelayAltPort (CliHandle, u4Value);
            }
            break;

#ifdef MSTP_WANTED
        case CLI_MSTP_CONFIG_MODE:

            if (!(AST_IS_MST_STARTED ()))
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            else if (AST_FORCE_VERSION != AST_VERSION_3)
            {
                CliPrintf (CliHandle,
                           "%% Bridge is not in MST Compatible mode\r\n");
                i4RetVal = CLI_FAILURE;
            }
            else
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s", CLI_MODE_MST);
                i4RetVal = CliChangePath ((CHR1 *) au1Cmd);
            }

            break;
        case CLI_MST_NAME:

            /* args[0]- Region Name */

            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstSetRegnName (CliHandle, (UINT1 *) args[0]);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_MST_NO_NAME:

            if (AST_IS_MST_STARTED ())
            {
                MEMSET (au1RegionName, 0, MST_CONFIG_NAME_LEN);
                i4RetVal = MstSetRegnName (CliHandle, au1RegionName);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_MST_REVISION:

            if (AST_IS_MST_STARTED ())
            {
                /* args[0] - Mst version */

                u4Value = *args[0];
                i4RetVal = MstCliSetRegionVersion (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_MST_NO_REVISION:

            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_CONFIG_LEVEL;
                i4RetVal = MstCliSetRegionVersion (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_MSTP_MAX_INST:
            if (AST_IS_MST_STARTED ())
            {
                u4Value = *(args[0]);
                i4RetVal = MstCliSetMaxInstanceNumber (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is Shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;
        case CLI_MSTP_NO_MAX_INST:

            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_MAX_INST;
                i4RetVal = MstCliSetMaxInstanceNumber (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is Shutdown\r\n");

                i4RetVal = CLI_FAILURE;
            }
            break;
        case CLI_MSTP_INST_MAP_ENABLED:

            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_INST_MAP_ENABLED;
                i4RetVal = MstCliSetInstanceMapping (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is Shutdown\r\n");

                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_MSTP_INST_MAP_DISABLED:
            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_INST_MAP_DISABLED;
                i4RetVal = MstCliSetInstanceMapping (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is Shutdown\r\n");

                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_MSTP_VLAN_INST:

            u1Action = MST_SET_CMD;

            if (AST_IS_MST_STARTED ())
            {
                /* args[0] - Instance Id
                   args[1] - VlanRange */
                u4Instance = CLI_PTR_TO_U4 (args[0]);
                if ((AST_GET_NO_OF_ACTIVE_INSTANCES >=
                     AST_GET_MAX_MST_INSTANCES)
                    && (u4Instance > AST_GET_MAX_MST_INSTANCES))

                {
                    CliPrintf (CliHandle,
                               "\r%% No Of Active Instances reaches MaxMstInst Value\r\n");
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
                if (CLI_STRLEN (args[1]) > MST_VLAN_LIST_SIZE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Vlan list exceeds the Max Length limit\r\n");
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                CLI_MEMSET (au1MstVlanList, 0, MST_VLAN_LIST_SIZE);
                if (CliStrToPortList ((UINT1 *) args[1], au1MstVlanList,
                                      MST_VLAN_LIST_SIZE,
                                      CFA_L2VLAN) == CLI_FAILURE)
                {

                    CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }

                i4RetVal =
                    MstCliSetVlanInstanceMapping (CliHandle, u4Instance,
                                                  au1MstVlanList, u1Action);

            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_MSTP_NO_VLAN_INST:

            u1Action = MST_NO_CMD;

            if (AST_IS_MST_STARTED ())
            {

                /* args[0] - Instance Id
                   args[1] - VlanRange(Optional) */

                u4Instance = CLI_PTR_TO_U4 (args[0]);

                /* If VlanRange is passed .. */
                if (args[1] != NULL)
                {

                    CLI_MEMSET (au1MstVlanList, 0, MST_VLAN_LIST_SIZE);
                    if (CliStrToPortList ((UINT1 *) args[1], au1MstVlanList,
                                          MST_VLAN_LIST_SIZE,
                                          CFA_L2VLAN) == MST_FAILURE)
                    {

                        CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                        i4RetVal = CLI_FAILURE;
                    }
                    else
                    {
                        i4RetVal =
                            MstCliSetVlanInstanceMapping (CliHandle, u4Instance,
                                                          au1MstVlanList,
                                                          u1Action);
                    }
                }
                /* No VlanRange  is passed... */
                else
                {
                    i4RetVal =
                        MstCliSetVlanInstanceMapping (CliHandle, u4Instance,
                                                      NULL, u1Action);
                }

            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
#endif
#ifdef PBB_WANTED
        case CLI_STP_PBB_LINK_TYPE:
            if (VcmGetContextInfoFromIfIndex
                ((UINT4) u4Index, &u4ContextId, &u2LocalPortId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Port not mapped to any of the context \r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            /*fall through */
#endif

        case CLI_STP_PORT_PROPERTIES:

            if (AST_IS_RST_STARTED ())
            {
                /* args[0] - Port Property  
                   args[1] - Value of the Port Property
                 */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);

                if (u4CmdType == AST_PORT_STATE_DISABLED)
                    u4Value = RST_PORT_DISABLED;
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    if (u4Value == P2P_TRUE)
                        u4Value = RST_P2P_FORCETRUE;
                    else if (u4Value == P2P_FALSE)
                        u4Value = RST_P2P_FORCEFALSE;
                }
                else if (u4CmdType == CLI_AST_ENABLED)
                {
                    u4Value = CREATE_AND_GO;
                }

                else
                    u4Value = CLI_PTR_TO_U4 (args[1]);

                i4RetVal =
                    RstSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                    u4CmdType, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                /* args[0] - Port Property to be set 
                   args[1] - Value of the Port Property
                 */
                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == AST_PORT_STATE_DISABLED)
                    u4Value = MST_FORCE_STATE_DISABLED;
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    if (u4Value == P2P_TRUE)
                        u4Value = AST_FORCE_TRUE;
                    else if (u4Value == P2P_FALSE)
                        u4Value = AST_FORCE_FALSE;
                }
                else if (u4CmdType == CLI_AST_ENABLED)
                {
                    u4Value = CREATE_AND_GO;
                }

                else
                    u4Value = CLI_PTR_TO_U4 (args[1]);

                i4RetVal =
                    MstCistSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                        u4CmdType, u4Value);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                /* args[0] - Port Property to be set 
                   args[1] - Value of the Port Property
                 */
                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == AST_PORT_STATE_DISABLED)
                    u4Value = AST_SNMP_FALSE;
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    if (u4Value == P2P_TRUE)
                        u4Value = AST_FORCE_TRUE;
                    else if (u4Value == P2P_FALSE)
                        u4Value = AST_FORCE_FALSE;
                }
                else if (u4CmdType == STP_PORTFAST)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);

                }
                else if (u4CmdType == CLI_AST_ENABLED)
                {
                    u4Value = CREATE_AND_GO;
                }

                u4Index = CLI_GET_IFINDEX ();
                i4RetVal =
                    PvrstSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                      u4CmdType, u4Value);
            }
#endif

            break;
#ifdef PBB_WANTED
        case CLI_STP_PBB_NO_LINK_TYPE:
            if (VcmGetContextInfoFromIfIndex
                ((UINT4) u4Index, &u4ContextId, &u2LocalPortId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Port not mapped to any of the context \r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            /*fall through */
#endif

        case CLI_STP_NO_PORT_PROPERTIES:

            if (AST_IS_RST_STARTED ())
            {
                /* args[0]- Port property to be reset */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == STP_NO_PORT_COST)
                {
                    u4Value = AST_NO_VAL;
                }
                else if (u4CmdType == AST_PORT_STATE_DISABLED)
                {
                    u4Value = RST_PORT_ENABLED;
                }
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = RST_P2P_AUTO;
                }
                else if (u4CmdType == STP_PORTFAST)
                {

                    u4Value = AST_SNMP_FALSE;
                }
                else if (u4CmdType == STP_PORT_PRIORITY)
                {
                    u4Value = RST_DEFAULT_PORT_PRIORITY;

                }
                else if (u4CmdType == CLI_AST_DISABLED)
                {
                    u4Value = DESTROY;
                }

                i4RetVal =
                    RstSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                    u4CmdType, u4Value);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                /* args[0] - Port Property to be reset 
                 */
                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == STP_NO_PORT_COST)
                {
                    u4Value = AST_NO_VAL;
                }
                else if (u4CmdType == AST_PORT_STATE_DISABLED)
                {
                    u4Value = MST_FORCE_STATE_ENABLED;
                }
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = AST_AUTO;
                }
                else if (u4CmdType == STP_PORTFAST)
                {

                    u4Value = AST_SNMP_FALSE;
                }
                else if (u4CmdType == STP_PORT_PRIORITY)
                {
                    u4Value = MST_DEFAULT_PORT_PRIORITY;

                }
                else if (u4CmdType == CLI_AST_DISABLED)
                {
                    u4Value = DESTROY;
                }

                i4RetVal =
                    MstCistSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                        u4CmdType, u4Value);

            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {

                /* args[0] - Port Property to be reset 
                 */
                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == AST_PORT_STATE_DISABLED)
                {
                    u4Value = AST_SNMP_TRUE;
                }
                else if (u4CmdType == STP_LINK_TYPE)
                {
                    u4Value = AST_FORCE_FALSE;
                }
                else if (u4CmdType == STP_PORTFAST)
                {
                    u4Value = AST_SNMP_FALSE;
                }
                else if (u4CmdType == CLI_AST_DISABLED)
                {
                    u4Value = DESTROY;
                }

                u4Index = CLI_GET_IFINDEX ();
                i4RetVal =
                    PvrstSetPortProp (CliHandle, (UINT4) u2LocalPortId,
                                      u4CmdType, u4Value);

            }
#endif
            break;

#ifdef MSTP_WANTED
        case CLI_MST_PORT_PROPERTIES:

            if (AST_IS_MST_STARTED ())
            {
                /* args[0]-->Mst InstanceId                       */
                /* args[1]-->Command-Mst PortCost or PortPriority */
                /* args[2]-->value of Port Priority or PortCost   */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4CmdType = CLI_PTR_TO_U4 (args[1]);
                u4Value = CLI_PTR_TO_U4 (args[2]);

                i4RetVal =
                    MstSetPortProperties (CliHandle, (UINT4) u2LocalPortId,
                                          u4Instance, u4CmdType, u4Value);

            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_MST_NO_PORT_PROPERTIES:

            if (AST_IS_MST_STARTED ())
            {
                /* args[0]-> Mst Instance id */
                /* args[1]-> Command- Mst PortPathCost or PortPriority */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4CmdType = CLI_PTR_TO_U4 (args[1]);

                if (u4CmdType == STP_NO_PORT_COST)
                {
                    u4Value = AST_NO_VAL;
                }
                else if (u4CmdType == STP_PORT_PRIORITY)
                {
                    u4Value = MST_DEFAULT_PORT_PRIORITY;
                }
                else if (u4CmdType == AST_PORT_STATE_DISABLED)
                {
                    u4Value = MST_FORCE_STATE_ENABLED;
                }

                i4RetVal =
                    MstSetPortProperties (CliHandle, (UINT4) u2LocalPortId,
                                          u4Instance, u4CmdType, u4Value);

            }
            else
            {

                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_INT_HELLOTIME:

            if (AST_IS_MST_STARTED ())
            {

                /* args[0] - MST Interface HelloTime */

                u4Value = CLI_PTR_TO_U4 (args[0]);
                u4Value = u4Value * AST_CENTI_SECONDS;
                i4RetVal = MstCliSetPortHelloTime (CliHandle,
                                                   (UINT4) u2LocalPortId,
                                                   u4Value);
            }
            else
            {

                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_NO_INT_HELLOTIME:

            if (AST_IS_MST_STARTED ())
            {
                u4Value = MST_DEFAULT_BRG_HELLO_TIME;
                i4RetVal = MstCliSetPortHelloTime (CliHandle,
                                                   (UINT4) u2LocalPortId,
                                                   u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
#endif

        case CLI_STP_INT_RESTRIC_ROLE:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRestrictedRole (CliHandle,
                                                        u2LocalPortId,
                                                        AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRestrictedRole (CliHandle,
                                                        u2LocalPortId,
                                                        AST_SNMP_TRUE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% Restricted-role can not be configured in PVRST mode\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif

            break;

        case CLI_STP_NO_INT_RESTRIC_ROLE:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRestrictedRole (CliHandle,
                                                        u2LocalPortId,
                                                        AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRestrictedRole (CliHandle,
                                                        u2LocalPortId,
                                                        AST_SNMP_FALSE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% Restricted-role can not be configured in PVRST mode\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

        case CLI_STP_INT_RESTRIC_TCN:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRestrictedTcn (CliHandle,
                                                       u2LocalPortId,
                                                       AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRestrictedTcn (CliHandle,
                                                       u2LocalPortId,
                                                       AST_SNMP_TRUE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% Restricted-tcn can not be configured in PVRST mode\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;
        case CLI_STP_NO_INT_RESTRIC_TCN:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRestrictedTcn (CliHandle,
                                                       u2LocalPortId,
                                                       AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRestrictedTcn (CliHandle,
                                                       u2LocalPortId,
                                                       AST_SNMP_FALSE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "%% Restricted-role can not be configured in PVRST mode \r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

        case CLI_STP_PORT_AUTOEDGE:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal =
                    RstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                        AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal =
                    MstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                        AST_SNMP_TRUE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                          AST_SNMP_TRUE);
            }
#endif
            break;

        case CLI_STP_PORT_NO_AUTOEDGE:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal =
                    RstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                        AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                        AST_SNMP_FALSE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstSetPortAutoEdge (CliHandle, (UINT4) u2LocalPortId,
                                          AST_SNMP_FALSE);
            }
#endif

            break;

            /*RSTP clear statistics feature */
        case CLI_STP_PORT_COUNTER_RESET:
            if (RST_SUCCESS != AstGetContextInfoFromIfIndex (u4Index,
                                                             &u4ContextOfIfIndex,
                                                             &u2LocalPortId))
            {
                CliPrintf (CliHandle,
                           "\r\n AstGetContextInfoFromIfIndex failed\r\n");
                break;
            }

            if (u4ContextId != u4ContextOfIfIndex)
            {
                CliPrintf (CliHandle,
                           "\r\n Interface not mapped to this context\r\n");
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliResetPortCounters (CliHandle, u4Index);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliResetPortCounters (CliHandle, u4Index);
            }
#endif
            else
            {
                CliPrintf (CliHandle, "\n STP module is shutdown\n");
            }
            break;

        case CLI_STP_COUNTER_RESET:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliResetBridgeCounters (CliHandle);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliResetBridgeCounters (CliHandle);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                PvrstResetCounters ();
                i4RetVal = CLI_SUCCESS;
            }
#endif
            else
            {
                CliPrintf (CliHandle, "\n STP module is shutdown\n");
            }
            break;

#ifdef MSTP_WANTED
        case CLI_MSTP_COUNTER_RESET:
            u4Instance = CLI_PTR_TO_U4 (args[0]);
            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliResetPerStBridgeCounters (CliHandle,
                                                           (UINT2) u4Instance);
            }
            else
            {
                CliPrintf (CliHandle, "\n MST module is shutdown\n");
            }
            break;

        case CLI_MSTP_PORT_COUNTER_RESET:
            u4Instance = CLI_PTR_TO_U4 (args[0]);
            if (MST_SUCCESS != AstGetContextInfoFromIfIndex (u4Index,
                                                             &u4ContextOfIfIndex,
                                                             &u2LocalPortId))
            {
                CliPrintf (CliHandle,
                           "\r\n AstGetContextInfoFromIfIndex failed\r\n");
                break;
            }

            if (u4ContextId != u4ContextOfIfIndex)
            {
                CliPrintf (CliHandle,
                           "\r\n Interface not mapped to this context\r\n");
                break;
            }
            if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliResetPerStPortCounters (CliHandle,
                                                         (UINT2) u4Instance,
                                                         u4Index);
            }
            else
            {
                CliPrintf (CliHandle, "\n MST module is shutdown\n");
            }
            break;

#endif

        case CLI_STP_BPDUGUARD_DISABLE:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal =
                    RstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                         u4Value);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal =
                    PvrstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                           u4Value);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_DISABLE;
                i4RetVal =
                    MstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                         u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_BPDUGUARD_ENABLE:

            u1BpduGuardAction = (UINT1) (CLI_PTR_TO_U4 (args[0]));

            if (AST_IS_RST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal =
                    RstBpduguardEnable (CliHandle, (INT4) u2LocalPortId,
                                        u4Value, u1BpduGuardAction);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal =
                    PvrstBpduguardEnable (CliHandle, (INT4) u2LocalPortId,
                                          u4Value, u1BpduGuardAction);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_ENABLE;
                i4RetVal =
                    MstBpduguardEnable (CliHandle, (INT4) u2LocalPortId,
                                        u4Value, u1BpduGuardAction);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_NO_BPDUGUARD:

            if (AST_IS_RST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_NONE;
                i4RetVal =
                    RstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                         u4Value);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_NONE;
                i4RetVal =
                    PvrstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                           u4Value);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u4Value = AST_BPDUGUARD_NONE;
                i4RetVal =
                    MstBpduguardDisable (CliHandle, (INT4) u2LocalPortId,
                                         u4Value);
            }
#endif
            else
            {
                CliPrintf (CliHandle,
                           "%% This command is not supported when spanning-tree is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_STP_GUARD_ROOT:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRootGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRootGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_TRUE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstGuardRoot (CliHandle, (INT4) u2LocalPortId);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

        case CLI_STP_NO_GUARD:

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortRootGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
                i4RetVal = RstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortRootGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
                i4RetVal = MstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstNoGuard (CliHandle, (INT4) u2LocalPortId);
                i4IfIndex = CLI_GET_IFINDEX ();
                i4RetVal = PvrstCliSetPortLoopGuard (CliHandle,
                                                     (UINT2) i4IfIndex,
                                                     AST_SNMP_FALSE);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
#endif
            break;

#ifdef PVRST_WANTED

        case CLI_STP_ENCAP_DOT1Q:

            if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstEncapDot1q (CliHandle, (INT4) u2LocalPortId);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_ENCAP_ISL:

            if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal = PvrstEncapISL (CliHandle, (INT4) u2LocalPortId);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_PVRST_INST_STATUS_DOWN_PORT:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId        */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }
                i4RetVal =
                    PvrstInstStatusOnPort (CliHandle, (INT4) u2LocalPortId,
                                           u4Instance, AST_SNMP_FALSE);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;
        case CLI_STP_PVRST_INST_STATUS_UP_PORT:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId        */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }

                i4RetVal =
                    PvrstInstStatusOnPort (CliHandle, (INT4) u2LocalPortId,
                                           u4Instance, AST_SNMP_TRUE);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_INST_PROP:
            if (AST_IS_PVRST_STARTED ())
            {
                /*args[0]-> Vlan Instance Id */
                /*args[1]-> u4Type i.e Forward Time,HelloTime ,MaxAge,HoldCount */
                /*args[2]-> u4Value i.e Values of the timers or HoldCount */
                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4CmdType = CLI_PTR_TO_U4 (args[1]);
                u4Value = CLI_PTR_TO_U4 (args[2]);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }
                if (u4CmdType == CLI_STP_FORWARD_TIME)
                {
                    u4Value = u4Value * AST_CENTI_SECONDS;
                    i4RetVal =
                        PvrstCliSetForwardDelay (CliHandle, u4Instance,
                                                 u4Value);
                }
                else if (u4CmdType == CLI_STP_HELLO_TIME)
                {
                    u4Value = u4Value * AST_CENTI_SECONDS;
                    i4RetVal =
                        PvrstCliSetHelloTime (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_MAX_AGE)
                {
                    u4Value = u4Value * AST_CENTI_SECONDS;
                    i4RetVal =
                        PvrstCliSetMaxAge (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_HOLD_COUNT)
                {
                    i4RetVal =
                        PvrstCliSetHoldCount (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_BRG_PRIORITY)
                {
                    i4RetVal =
                        PvrstCliSetBrgPriority (CliHandle, u4Instance, u4Value);
                }
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_NO_INST_PROP:
            if (AST_IS_PVRST_STARTED ())
            {
                /*args[0]-> Vlan Instance Id */
                /*args[1]-> u4Type i.e Forward Time,HelloTime ,MaxAge,HoldCount */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4CmdType = CLI_PTR_TO_U4 (args[1]);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }
                if (u4CmdType == CLI_STP_FORWARD_TIME)
                {
                    u4Value = PVRST_DEFAULT_BRG_FWD_DELAY;
                    i4RetVal =
                        PvrstCliSetForwardDelay (CliHandle, u4Instance,
                                                 u4Value);
                }
                else if (u4CmdType == CLI_STP_HELLO_TIME)
                {
                    u4Value = PVRST_DEFAULT_BRG_HELLO_TIME;
                    i4RetVal =
                        PvrstCliSetHelloTime (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_MAX_AGE)
                {
                    u4Value = PVRST_DEFAULT_BRG_MAX_AGE;
                    i4RetVal =
                        PvrstCliSetMaxAge (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_HOLD_COUNT)
                {
                    u4Value = PVRST_DEFAULT_BRG_TX_LIMIT;
                    i4RetVal =
                        PvrstCliSetHoldCount (CliHandle, u4Instance, u4Value);
                }
                else if (u4CmdType == CLI_STP_BRG_PRIORITY)
                {
                    u4Value = PVRST_DEFAULT_BRG_PRIORITY;
                    i4RetVal =
                        PvrstCliSetBrgPriority (CliHandle, u4Instance, u4Value);
                }
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_STP_VLAN_PORTPRIORITY:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId        */
                /* args[1]-->value of Port Priority */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }
                i4RetVal =
                    PvrstVlanPortPriority (CliHandle, (UINT4) u2LocalPortId,
                                           u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_NO_VLAN_PORTPRIORITY:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId   */
                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4Value = (UINT4) PVRST_DEFAULT_PORT_PRIORITY;
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }
                i4RetVal =
                    PvrstVlanPortPriority (CliHandle, (UINT4) u2LocalPortId,
                                           u4Instance, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_VLAN_COST:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId    */
                /* args[1]-->value of Port Cost */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4Value = CLI_PTR_TO_U4 (args[1]);
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }

                i4RetVal =
                    PvrstVlanCost (CliHandle, (UINT4) u2LocalPortId, u4Instance,
                                   u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_NO_VLAN_COST:

            if (AST_IS_PVRST_STARTED ())
            {
                /* args[0]-->Vlan InstanceId    */

                u4Instance = CLI_PTR_TO_U4 (args[0]);
                u4Value = 0;
                AstSelectContext (AST_DEFAULT_CONTEXT);
                if (((AST_FORCE_VERSION == AST_VERSION_0)) && (u4Instance != 1))
                {
                    AST_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    CliPrintf (CliHandle, "\r%% compatibility is STP\r\n");
                    return CLI_FAILURE;
                }

                i4RetVal =
                    PvrstVlanCost (CliHandle, (UINT4) u2LocalPortId, u4Instance,
                                   u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST Module is shutdown.\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_STP_PVRST_FLUSH_IND_THRESHOLD:
            u4Instance = CLI_PTR_TO_U4 (args[0]);
            u4Value = CLI_PTR_TO_U4 (args[1]);

            if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstCliSetFlushIndThreshold (CliHandle, u4Instance,
                                                  u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% PVRST is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_STP_PVRST_NO_FLUSH_IND_THRESHOLD:
            u4Instance = CLI_PTR_TO_U4 (args[0]);
            if (AST_IS_PVRST_STARTED ())
            {
                u4Value = PVRST_DEFAULT_FLUSH_IND_THRESHOLD;
                i4RetVal =
                    PvrstCliSetFlushIndThreshold (CliHandle, u4Instance,
                                                  u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
#endif
        case CLI_STP_INIT_PROTOCOL_MIGRATION:

            /* args[0] - Context name */
            if (args[0] != NULL)
            {
                if (AstVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle, "Switch %s Does not exist.\r\n",
                               args[0]);
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            else if (args[0] == NULL)
            {
                /* SI MODE and the switch name is not provided as input
                 * Go ahead for Default context
                 * */
                u4ContextId = L2IWF_DEFAULT_CONTEXT;
            }
            else
            {
                /* MI MODE and the switch name is not given
                 * Also Interface is also not provided.
                 * Return Failure
                 * */
                CliPrintf (CliHandle, "%%  Enter a Valid Context Name.\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                if (AST_MODULE_STATUS != RST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% RSTP Module not Enabled\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                else
                {
                    i4RetVal = RstCliSetProtocolMigration (CliHandle);
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (AST_MODULE_STATUS != MST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% MSTP Module not Enabled\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                else
                {
                    i4RetVal = MstCliSetProtocolMigration (CliHandle);
                }
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_ENABLED ())
            {
                if (AST_MODULE_STATUS != PVRST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% PVRST Module not Enabled\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                else
                {
                    i4RetVal = PvrstCliSetProtocolMigration (CliHandle);
                }
            }
#endif
            break;

        case CLI_STP_INIT_PORT_PROTOCOL_MIGRATION:

            if (AstGetContextInfoFromIfIndex (u4Index, &u4ContextId,
                                              &u2LocalPortId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Interface not mapped to any "
                           "of the context.\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (AstSispIsSispPortPresentInCtxt (u4ContextId) == VCM_TRUE)
            {
                CLI_SET_ERR (CLI_STP_SISP_PROTO_MIG_ERR);
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                if (AST_MODULE_STATUS != RST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% RSTP Module not Enabled \r\n");
                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    i4RetVal = RstCliSetPortProtocolMigration
                        (CliHandle, (UINT4) u2LocalPortId);
                }
            }

#ifdef MSTP_WANTED

            else if (AST_IS_MST_STARTED ())
            {
                if (AST_MODULE_STATUS != MST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% MSTP Module not Enabled \r\n");
                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    i4RetVal = MstCliSetPortProtocolMigration
                        (CliHandle, (UINT4) u2LocalPortId);
                }
            }
#endif
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                if (AST_MODULE_STATUS != PVRST_ENABLED)
                {
                    CliPrintf (CliHandle, "%% PVRST Module not Enabled \r\n");
                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    i4RetVal = PvrstCliSetPortProtocolMigration
                        ((UINT4) u2LocalPortId);
                }
            }
#endif
            break;

        case CLI_STP_DEBUG_ENABLE:
            if (args[2] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[2]);
                StpCliSetDebugLevel (CliHandle, i4Args);
            }
            /* args[0] - Context Name */
            /* args[1] - Debug Value */

            if (args[0] != NULL)
            {
                if (AstVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = AST_DEFAULT_CONTEXT;
            }

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                u1Action = RST_SET_CMD;

                /* args[1] - Spanning Tree debugging feature  
                   to be enabled */
                u4Value = CLI_PTR_TO_U4 (args[1]);

                i4RetVal = RstSetDebug (CliHandle, u4Value, u1Action);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u1Action = PVRST_SET_CMD;

                /* args[1] -Spanning Tree debugging feature  to be enabled */

                u4Value = CLI_PTR_TO_U4 (args[1]);

                i4RetVal = PvrstSetDebug (CliHandle, u4Value, u1Action);
            }
#endif

#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                u1Action = MST_SET_CMD;

                /* args[1] - Spanning Tree debugging feature  
                   to be enabled */
                u4Value = CLI_PTR_TO_U4 (args[1]);

                i4RetVal = MstSetDebug (CliHandle, u4Value, u1Action);
            }
#endif
            break;

        case CLI_STP_DEBUG_DISABLE:

            /* args[0] - Context Name */
            /* args[1] - Debug Value */

            if (args[0] != NULL)
            {
                if (AstVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = AST_DEFAULT_CONTEXT;
            }

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (AST_IS_RST_STARTED ())
            {
                u1Action = RST_NO_CMD;

                /* args[1] - Spanning Tree debugging feature  
                   to be disabled */

                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal = RstSetDebug (CliHandle, u4Value, u1Action);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                u1Action = PVRST_NO_CMD;

                /* args[1] -Spanning Tree debugging feature  to be disabled */
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal = PvrstSetDebug (CliHandle, u4Value, u1Action);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                u1Action = MST_NO_CMD;

                /* args[1] - Spanning Tree debugging feature  
                   to be disabled */
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal = MstSetDebug (CliHandle, u4Value, u1Action);
            }
#endif
            break;

        case CLI_STP_GBL_DEBUG_ENABLE:

            i4RetVal = RstSetGlobalDebug (CliHandle, AST_SNMP_TRUE);
            break;

        case CLI_STP_GBL_DEBUG_DISABLE:

            i4RetVal = RstSetGlobalDebug (CliHandle, AST_SNMP_FALSE);
            break;

        case CLI_STP_PSEUDOROOT_ID:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstConfigPseudoRootId
                    (CliHandle, (UINT4) u2LocalPortId, au1InMacAddr,
                     (UINT2) (*args[1]));
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstConfigPseudoRootId
                    (CliHandle, (UINT4) u2LocalPortId, MST_CIST_CONTEXT,
                     au1InMacAddr, (UINT2) (*args[1]));
            }
#endif
            break;
#ifdef MSTP_WANTED
        case CLI_MSTP_PSEUDOROOT_ID:

            u4Instance = CLI_PTR_TO_U4 (args[0]);

            StrToMac ((UINT1 *) args[1], au1InMacAddr);

            if (AST_IS_MST_STARTED ())
            {

                i4RetVal = MstConfigPseudoRootId
                    (CliHandle, (UINT4) u2LocalPortId, u4Instance,
                     au1InMacAddr, (UINT2) (*args[2]));
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
#endif
        case CLI_STP_L2GP:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstSetPortL2gp (CliHandle, (UINT4) u2LocalPortId,
                                           (tAstBoolean) AST_SNMP_TRUE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstSetPortL2gp (CliHandle, (UINT4) u2LocalPortId,
                                           (tAstBoolean) AST_SNMP_TRUE);
            }
#endif
            break;
        case CLI_STP_NO_L2GP:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstSetPortL2gp (CliHandle, (UINT4) u2LocalPortId,
                                           (tAstBoolean) AST_SNMP_FALSE);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstSetPortL2gp (CliHandle, (UINT4) u2LocalPortId,
                                           (tAstBoolean) AST_SNMP_FALSE);
            }
#endif
            break;
        case CLI_STP_BPDU_RX:

            i4Status = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal =
                    RstSetBpduRx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstSetBpduRx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstSetBpduRx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#endif
            break;
        case CLI_STP_BPDU_TX:

            i4Status = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal =
                    RstSetBpduTx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstSetBpduTx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstSetBpduTx (CliHandle, (UINT4) u2LocalPortId, i4Status);
            }
#endif
            break;
        case CLI_STP_NO_PSEUDOROOT_ID:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstResetPseudoRootId (CliHandle,
                                                 (UINT4) u2LocalPortId);
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstResetPseudoRootId (CliHandle, MST_CIST_CONTEXT,
                                                 (UINT4) u2LocalPortId);
            }
#endif

            break;
#ifdef MSTP_WANTED
        case CLI_MSTP_NO_PSEUDOROOT_ID:
            if (AST_IS_MST_STARTED ())
            {
                u4Instance = CLI_PTR_TO_U4 (args[0]);

                i4RetVal = MstResetPseudoRootId (CliHandle, u4Instance,
                                                 (UINT4) u2LocalPortId);
            }
            else
            {
                CliPrintf (CliHandle, "%% MSTP is shutdown\r\n");
                i4RetVal = CLI_FAILURE;

            }

            break;
#endif
        case CLI_STP_INTERFACE_LOOP_GUARD:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_TRUE);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4IfIndex = CLI_GET_IFINDEX ();
                i4RetVal = PvrstCliSetPortLoopGuard (CliHandle,
                                                     (UINT2) i4IfIndex,
                                                     AST_SNMP_TRUE);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_TRUE);
            }
#endif
            break;

        case CLI_STP_NO_INTERFACE_LOOP_GUARD:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4IfIndex = CLI_GET_IFINDEX ();
                i4RetVal = PvrstCliSetPortLoopGuard (CliHandle,
                                                     (UINT2) i4IfIndex,
                                                     AST_SNMP_FALSE);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal = MstCliSetPortLoopGuard (CliHandle,
                                                   u2LocalPortId,
                                                   AST_SNMP_FALSE);
            }
#endif
            break;

        case CLI_STP_DOT1W_ENABLED:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortDot1WEnable (CliHandle,
                                                     u2LocalPortId,
                                                     AST_SNMP_TRUE);
            }
            break;

        case CLI_STP_DOT1W_DISABLED:
            if (AST_IS_RST_STARTED ())
            {
                i4RetVal = RstCliSetPortDot1WEnable (CliHandle,
                                                     u2LocalPortId,
                                                     AST_SNMP_FALSE);
            }
            break;
        case CLI_STP_BPDU_FILTER:
            i4Status = CLI_PTR_TO_U4 (args[0]);

            if (AST_IS_RST_STARTED ())
            {
                i4RetVal =
                    RstSetBpduFilter (CliHandle, (UINT4) u2LocalPortId,
                                      i4Status);
            }
#ifdef PVRST_WANTED
            else if (AST_IS_PVRST_STARTED ())
            {
                i4RetVal =
                    PvrstSetBpduFilter (CliHandle, (UINT4) u2LocalPortId,
                                        i4Status);
            }
#endif
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                i4RetVal =
                    MstSetBpduFilter (CliHandle, (UINT4) u2LocalPortId,
                                      i4Status);
            }
#endif
            break;

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            AstReleaseContext ();
            AST_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    AstReleaseContext ();

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_STP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", StpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

INT4
cli_process_stp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4Value = 0;
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4CmdType = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = AST_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId;
#if defined MSTP_WANTED || PVRST_WANTED
    UINT4               u4Instance = 0;
#endif
#ifdef MSTP_WANTED
    UINT1               u1Detail = 0;
    BOOL1               bPerfDataStatus;
#endif
    UINT1              *pu1ContextName = NULL;
    UINT4              *args[STP_CLI_MAX_ARGS];
    INT1                argno = 0;

    CLI_SET_ERR (0);
    CliRegisterLock (CliHandle, AstLock, AstUnLock);

    AST_LOCK ();

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing
     * IfIndex as the first argument in variable argument list. Like that
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == STP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    while (AstCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4Index, u4TempContextId,
            &u4ContextId, &u2LocalPortId) == RST_SUCCESS)
    {

        switch (u4Command)
        {
                /* Show Spanning Tree */

            case CLI_STP_SHOW_INFO:

                if (AstIsRstStartedInContext (u4ContextId))
                {
                    /* args[0] - Spanning tree feature to be displayed
                       args[1] - Optional Spanning tree feature for a 
                       particular feature

                     */

                    if (args[0] != NULL)
                    {
                        u4CmdType = CLI_PTR_TO_U4 (args[0]);
                    }
                    switch (u4CmdType)
                    {
                        case STP_PATHCOST_METHOD:

                            i4RetVal = RstCliShowSpanningTreeMethod (CliHandle);
                            break;

                        case STP_BLOCKED_PORTS:

                            i4RetVal =
                                RstCliDisplayBlockedPorts (CliHandle,
                                                           u4ContextId);
                            break;

                        case STP_SUMMARY_PORT_STATES:
                            i4RetVal =
                                RstCliShowSummary (CliHandle, u4ContextId);
                            break;

                        case STP_RED_SUMMARY:
                            if (AST_NODE_STATUS () == RED_AST_STANDBY)
                            {
                                AstRedDumpSyncedUpOutputs (CliHandle);
                                RedDumpSyncedUpPdus (CliHandle);
                                RedDumpSyncedUpData (CliHandle);
                            }
                            break;

                        default:

                            i4RetVal =
                                RstCliShowSpanningTree (CliHandle,
                                                        u4ContextId, 0);
                            break;
                    }            /*Inner Switch */

                }                /* RSTP Mode Over */

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {

                    /* args[0] - Spanning tree feature to be displayed
                       args[1] - Optional Spanning tree feature for a 
                       particular feature
                       args[2] - Instance ID
                     */

                    if (args[0] != NULL)
                    {
                        u4CmdType = CLI_PTR_TO_U4 (args[0]);
                    }

                    switch (u4CmdType)
                    {
                        case STP_PATHCOST_METHOD:

                            i4RetVal = MstCliShowSpanningTreeMethod (CliHandle);
                            break;
                        case STP_BLOCKED_PORTS:

                            i4RetVal = MstCliDisplayBlockedPorts
                                (CliHandle, u4ContextId);
                            break;

                        case STP_SUMMARY_PORT_STATES:
                            i4RetVal = MstCliShowSummary (CliHandle,
                                                          u4ContextId);
                            break;

                        case STP_RED_SUMMARY:
                            if (AST_NODE_STATUS () == RED_AST_STANDBY)
                            {
                                AstRedDumpSyncedUpOutputs (CliHandle);
                                RedDumpSyncedUpPdus (CliHandle);
                                RedDumpSyncedUpData (CliHandle);
                            }
                            break;

                        default:

                            i4RetVal = MstCliShowSpanningTree
                                (CliHandle, u4ContextId, 0);
                            break;
                    }            /*Inner Switch */

                }                /* MSTP Mode Over */
#endif
#ifdef PVRST_WANTED
                else if ((u4ContextId == AST_DEFAULT_CONTEXT)
                         && (AST_IS_PVRST_STARTED ()))
                {
                    /* args[0] - Spanning tree feature to be displayed
                       args[1] - Optional Spanning tree feature for a 
                       particular feature
                       args[2] - Instance ID
                     */

                    if (args[0] != NULL)
                    {
                        u4CmdType = CLI_PTR_TO_U4 (args[0]);
                    }

                    switch (u4CmdType)
                    {
                        case STP_PATHCOST_METHOD:

                            i4RetVal = PvrstCliShowSpanningTreeMethod
                                (CliHandle, u4ContextId);
                            break;
                        case STP_BLOCKED_PORTS:

                            CliPrintf (CliHandle,
                                       "\r%% Blocked Ports can be retrieved per VLAN basis in PVRST \r\n");
                            i4RetVal = (INT4) CLI_FAILURE;
                            break;

                        case STP_SUMMARY_PORT_STATES:
                            CliPrintf (CliHandle,
                                       "\r%% Summary can be retrieved per VLAN basis in PVRST \r\n");
                            i4RetVal = (INT4) CLI_FAILURE;

                            break;

                        case STP_RED_SUMMARY:
                            if (AST_NODE_STATUS () == RED_AST_STANDBY)
                            {
                                AstRedDumpSyncedUpOutputs (CliHandle);
                                RedDumpSyncedUpPdus (CliHandle);
                                RedDumpSyncedUpData (CliHandle);
                            }
                            break;

                        default:

                            for (u4Instance = 1; u4Instance <= VLAN_MAX_VLAN_ID;
                                 u4Instance++)
                            {
                                if ((AstL2IwfMiIsVlanActive
                                     (u4ContextId,
                                      (tVlanId) u4Instance) == OSIX_FALSE)
                                    || (INVALID_INDEX ==
                                        AstL2IwfMiGetVlanInstMapping
                                        (u4ContextId, (tVlanId) u4Instance)))
                                {
                                    continue;
                                }
                                i4RetVal =
                                    PvrstCliShowSpanningTree (CliHandle,
                                                              u4ContextId,
                                                              u4Instance, 0);
                            }
                            break;
                    }
                }
#endif
                break;

            case CLI_STP_SHOW_DETAIL:

                if (AstIsRstStartedInContext (u4ContextId))
                {
                    if (CLI_PTR_TO_U4 (args[0]) == STP_SHOW_DETAIL)
                    {
                        i4RetVal =
                            RstCliShowSpanningTreeDetail (CliHandle,
                                                          u4ContextId, 0);
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_DETAIL)
                    {
                        i4RetVal =
                            RstCliShowSpanningTreeDetail (CliHandle,
                                                          u4ContextId,
                                                          STP_ACTIVE_PORTS);
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_PORTS)
                    {
                        i4RetVal =
                            RstCliShowSpanningTree (CliHandle, u4ContextId,
                                                    STP_ACTIVE_PORTS);
                    }
                }

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    if (CLI_PTR_TO_U4 (args[0]) == STP_SHOW_DETAIL)
                    {
                        i4RetVal =
                            MstCliShowSpanningTreeDetail (CliHandle,
                                                          u4ContextId, 0);
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_DETAIL)
                    {
                        i4RetVal =
                            MstCliShowSpanningTreeDetail (CliHandle,
                                                          u4ContextId,
                                                          STP_ACTIVE_PORTS);
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_PORTS)
                    {
                        i4RetVal =
                            MstCliShowSpanningTree (CliHandle, u4ContextId,
                                                    STP_ACTIVE_PORTS);
                    }

                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    if (CLI_PTR_TO_U4 (args[0]) == STP_SHOW_DETAIL)
                    {
                        for (u4Instance = 1; u4Instance <= VLAN_MAX_VLAN_ID;
                             u4Instance++)
                        {
                            if ((AstL2IwfMiIsVlanActive
                                 (u4ContextId, (tVlanId) u4Instance)
                                 == OSIX_FALSE) ||
                                (INVALID_INDEX ==
                                 AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                               (tVlanId)
                                                               u4Instance)))

                            {
                                continue;
                            }

                            i4RetVal =
                                PvrstCliShowSpanningTreeDetail (CliHandle,
                                                                u4ContextId,
                                                                u4Instance, 0);
                        }
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_DETAIL)
                    {
                        for (u4Instance = 1; u4Instance <= VLAN_MAX_VLAN_ID;
                             u4Instance++)
                        {
                            if ((AstL2IwfMiIsVlanActive
                                 (u4ContextId, (tVlanId) u4Instance)
                                 == OSIX_FALSE) ||
                                (INVALID_INDEX ==
                                 AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                               (tVlanId)
                                                               u4Instance)))

                            {
                                continue;
                            }

                            i4RetVal =
                                PvrstCliShowSpanningTreeDetail (CliHandle,
                                                                u4ContextId,
                                                                u4Instance,
                                                                CLI_STP_VLAN_ACTIVE);
                        }
                    }
                    else if (CLI_PTR_TO_U4 (args[0]) == STP_ACTIVE_PORTS)
                    {
                        for (u4Instance = 1; u4Instance <= VLAN_MAX_VLAN_ID;
                             u4Instance++)
                        {
                            if ((AstL2IwfMiIsVlanActive
                                 (u4ContextId, (tVlanId) u4Instance)
                                 == OSIX_FALSE) ||
                                (INVALID_INDEX ==
                                 AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                               (tVlanId)
                                                               u4Instance)))

                            {
                                continue;
                            }
                            i4RetVal =
                                PvrstCliShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Instance,
                                                          CLI_STP_VLAN_ACTIVE);
                        }
                    }
                }
#endif
                break;

            case CLI_STP_SHOW_PORT_BPDUGUARD:

                if (AstIsRstStartedInContext (u4ContextId))
                {

                    RstDisplayInterfaceBpduGuard (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    MstDisplayInterfaceBpduGuard (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    PvrstDisplayInterfaceBpduGuard (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#endif
                break;

            case CLI_STP_SHOW_PORT_INCONSISTENCY:

                if (AstIsRstStartedInContext (u4ContextId))
                {
                    RstDisplayPortInconsistency (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    MstDisplayPortInconsistency (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    PvrstDisplayPortInconsistency (CliHandle, (INT4) u4Index);
                    i4RetVal = CLI_SUCCESS;
                }
#endif
                break;

            case CLI_STP_SHOW_PORT_INFO:

                if (AstIsRstStartedInContext (u4ContextId))
                {
                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }
                    if ((u4Value == PVRST_ENCAP_TYPE) ||
                        (u4Value == PVRST_ROOT_GUARD))
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                        i4RetVal = CLI_FAILURE;
                    }
                    else
                    {
                        i4RetVal = RstCliDisplayInterfaceDetails
                            (CliHandle, u4ContextId, u4Index, u4Value);
                    }

                }
#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }
                    if ((u4Value == PVRST_ENCAP_TYPE) ||
                        (u4Value == PVRST_ROOT_GUARD))
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                        i4RetVal = CLI_FAILURE;
                    }
                    else
                    {

                        i4RetVal = MstCliDisplayInterfaceDetails
                            (CliHandle, u4ContextId, u4Index, u4Value);
                    }
                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }
                    switch (u4Value)
                    {

                        case STP_PORTFAST:

                            PvrstDisplayInterfacePortFast (CliHandle,
                                                           u4ContextId,
                                                           (INT4) u4Index);
                            i4RetVal = CLI_SUCCESS;

                            break;

                        case PVRST_ENCAP_TYPE:

                            PvrstDisplayInterfaceEncapType (CliHandle,
                                                            u4ContextId,
                                                            (INT4) u4Index);
                            i4RetVal = CLI_SUCCESS;

                            break;

                        case PVRST_ROOT_GUARD:

                            PvrstDisplayInterfaceRootGuard (CliHandle,
                                                            u4ContextId,
                                                            (INT4) u4Index);
                            i4RetVal = CLI_SUCCESS;

                            break;

                        case STP_RESTRICTED_ROLE:
                            CliPrintf (CliHandle,
                                       "\r%% Restricted-role can not be retrieved in PVRST \r\n");
                            break;

                        case STP_RESTRICTED_TCN:
                            CliPrintf (CliHandle,
                                       "\r%% Restricted-tcn can not be retrieved in PVRST \r\n");
                            break;

                        case STP_PORT_PRIORITY:
                        case STP_PORT_COST:
                        case STP_SHOW_DETAIL:
                        case STP_PORT_STATE:
                        case STP_ROOT_COST:
                        case STP_PORT_STATS:
                        default:
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Interface related info can be retrieved"
                                       " per VLAN basis in PVRST  \r\n");
                            i4RetVal = (INT4) CLI_FAILURE;
                            break;
                        }
                    }
                }
#endif
                break;

            case CLI_STP_SHOW_ROOT_INFO:

                if (AstIsRstStartedInContext (u4ContextId))
                {

                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }

                    i4RetVal =
                        RstCliShowSpanningTreeRoot (CliHandle, u4ContextId,
                                                    u4Value);
                }

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }

                    i4RetVal =
                        MstCliShowSpanningTreeRoot (CliHandle, u4ContextId,
                                                    u4Value);
                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    CliPrintf (CliHandle,
                               "\r%% Root Info can be retrieved per VLAN basis in PVRST\r\n");
                    i4RetVal = (INT4) CLI_FAILURE;

                }
#endif
                break;

            case CLI_STP_SHOW_BRIDGE_INFO:

                if (AstIsRstStartedInContext (u4ContextId))
                {

                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }

                    i4RetVal =
                        RstCliShowSpanningTreeBridge (CliHandle,
                                                      u4ContextId, u4Value);
                }

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    if (args[0] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[0]);
                    }

                    i4RetVal =
                        MstCliShowSpanningTreeBridge (CliHandle,
                                                      u4ContextId, u4Value);
                }
#endif
#ifdef PVRST_WANTED
                else if (AstIsPvrstStartedInContext (u4ContextId))
                {
                    CliPrintf (CliHandle,
                               "\r%% Bridge Info can be retrieved per VLAN basis in PVRST \r\n");
                    i4RetVal = (INT4) CLI_FAILURE;
                }
#endif

                break;
            case CLI_STP_SHOW_L2GP_INTF:
                if (AstIsRstStartedInContext (u4ContextId))
                {
                    i4RetVal =
                        RstCliShowSpanningTreePortL2gp (CliHandle, u4ContextId,
                                                        u4Index);
                }

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    i4RetVal = MstCliShowSpanningTreePortL2gp
                        (CliHandle, u4ContextId, u4Index);

                }
#endif

                break;
            case CLI_STP_SHOW_L2GP_ALL:
                if (AstIsRstStartedInContext (u4ContextId))
                {
                    i4RetVal = RstCliShowSpanningTreePortL2gp (CliHandle,
                                                               u4ContextId, 0);
                }

#ifdef MSTP_WANTED
                else if (AstIsMstStartedInContext (u4ContextId))
                {
                    i4RetVal = MstCliShowSpanningTreePortL2gp
                        (CliHandle, u4ContextId, 0);

                }
#endif

                break;

#ifdef MSTP_WANTED
            case CLI_STP_SHOW_MST_INFO:

                if (!(AstIsMstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Multiple Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    if (args[0] != NULL)
                    {
                        u4Instance = *(args[0]);
                    }
                    if (args[1] != NULL)
                    {
                        u1Detail = 1;
                    }
                    i4RetVal = MstShowSpanningTreeMst
                        (CliHandle, u4ContextId, u4Instance, u1Detail);
                }
                break;

            case CLI_STP_SHOW_MST_INST_MAP:

                if (!(AstIsMstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Multiple Spanning Tree is disabled\r\n");
                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    i4RetVal = MstShowSpanningTreeMstConfig (CliHandle,
                                                             u4ContextId);
                }

                break;

            case CLI_STP_SHOW_MST_INTF:

                if (!(AstIsMstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Multiple Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    if (args[0] != NULL)
                    {
                        u4Instance = *(args[0]);
                    }

                    if (CLI_PTR_TO_U4 (args[1]) == STP_PORT_STATS)
                    {
                        i4RetVal = MstShowSpanningTreeMstInterfaceStats
                            (CliHandle, u4ContextId, u4Instance, u4Index);
                    }

                    else if (CLI_PTR_TO_U4 (args[1]) == STP_HELLO_TIME)
                    {
                        i4RetVal =
                            MstShowSpanningTreeMstInterfaceHelloTime
                            (CliHandle, u4ContextId, u4Instance, u4Index);
                    }
                    else if (CLI_PTR_TO_U4 (args[1]) == STP_SHOW_DETAIL)
                    {

                        MstShowSpanningTreeMstInstIntfDetail
                            (CliHandle, u4ContextId, u4Instance, u4Index);
                    }
                    else
                    {
                        i4RetVal = MstShowSpanningTreeMstInterfaceDetails
                            (CliHandle, u4ContextId, u4Index, u4Instance);
                    }
                }
                break;
#endif

#ifdef PVRST_WANTED

            case CLI_STP_SHOW_PVRST_INST_DET:

                if (!(AstIsPvrstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    /* args[0] - Instance
                       args[1] - Spanning tree feature to be displayed
                     */

                    /* Validate Instance Id */
                    u4Instance = CLI_PTR_TO_U4 (args[0]);
                    if (INVALID_INDEX ==
                        AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                      (tVlanId) u4Instance))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    if (PvrstValidateInstanceEntry (u4Instance) !=
                        PVRST_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }

                    if (args[1] != NULL)
                    {
                        u4CmdType = CLI_PTR_TO_U4 (args[1]);
                    }
                    switch (u4CmdType)
                    {
                        case CLI_STP_VLAN_DETAIL:

                            i4RetVal =
                                PvrstCliShowSpanningTreeDetail (CliHandle,
                                                                u4ContextId,
                                                                u4Instance, 0);
                            break;

                        case CLI_STP_VLAN_ACTIVE_DETAIL:

                            i4RetVal =
                                PvrstCliShowSpanningTreeDetail (CliHandle,
                                                                u4ContextId,
                                                                u4Instance,
                                                                CLI_STP_VLAN_ACTIVE);
                            break;

                        case CLI_STP_VLAN_ACTIVE:

                            i4RetVal =
                                PvrstCliShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Instance,
                                                          CLI_STP_VLAN_ACTIVE);
                            break;

                        case CLI_STP_VLAN_BLOCKEDPORTS:

                            i4RetVal =
                                PvrstCliDisplayBlockedPorts (CliHandle,
                                                             u4ContextId,
                                                             u4Instance);

                            break;

                        case CLI_STP_VLAN_PATHCOST:

                            i4RetVal =
                                PvrstCliShowSpanningTreeMethod (CliHandle,
                                                                u4ContextId);
                            break;

                        case CLI_STP_VLAN_SUMMARY:
                            i4RetVal =
                                PvrstCliShowSummary (CliHandle, u4ContextId,
                                                     u4Instance);
                            break;

                        default:

                            i4RetVal =
                                PvrstCliShowSpanningTree (CliHandle,
                                                          u4ContextId,
                                                          u4Instance, 0);

                            break;
                    }            /*Inner Switch */
                }                /* PVRST Mode Over */
                break;

            case CLI_STP_SHOW_PVRST_INST_BRG_DET:

                if (!(AstIsPvrstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    u4Instance = CLI_PTR_TO_U4 (args[0]);
                    /* args[0] - Instance
                       args[1] - Spanning tree feature to be displayed
                     */

                    /* Validate Instance Id */
                    if (INVALID_INDEX ==
                        AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                      (tVlanId) u4Instance))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);

                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    if (PvrstValidateInstanceEntry (u4Instance) !=
                        PVRST_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }

                    if (args[1] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[1]);
                    }
                    i4RetVal =
                        PvrstCliShowSpanningTreeBrgDet (CliHandle, u4ContextId,
                                                        u4Instance, u4Value);
                }
                break;
            case CLI_STP_SHOW_PVRST_INST_ROOT_DET:

                if (!(AstIsPvrstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    u4Instance = CLI_PTR_TO_U4 (args[0]);
                    /* args[0] - Instance
                       args[1] - Spanning tree feature to be displayed
                     */

                    /* Validate Instance Id */
                    if (INVALID_INDEX ==
                        AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                      (tVlanId) u4Instance))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);

                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    if (PvrstValidateInstanceEntry (u4Instance) !=
                        PVRST_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }

                    if (args[1] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[1]);
                    }
                    i4RetVal =
                        PvrstCliShowSpanningTreeRootDet (CliHandle, u4ContextId,
                                                         u4Instance, u4Value);
                }
                break;
            case CLI_STP_SHOW_PVRST_INTERFACE_DET:

                if (!(AstIsPvrstStartedInContext (u4ContextId)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Per VLAN Rapid Spanning Tree is disabled\r\n");

                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    u4Instance = CLI_PTR_TO_U4 (args[0]);
                    /* args[0] - Instance
                       args[1] - Spanning tree feature to be displayed
                     */

                    /* Validate Instance Id */
                    if (INVALID_INDEX ==
                        AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                      (tVlanId) u4Instance))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);

                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    if (PvrstValidateInstanceEntry (u4Instance) !=
                        PVRST_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n VLAN %d does not exist\r\n",
                                   u4Instance);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }

                    if (args[1] != NULL)
                    {
                        u4Value = CLI_PTR_TO_U4 (args[1]);
                    }
                    i4RetVal =
                        PvrstCliDisplayInterfaceDetails (CliHandle, u4ContextId,
                                                         u4Instance, u4Index,
                                                         u4Value);
                }
                break;
#endif
            case CLI_STP_SHOW_PERF_DATA:

                if (AstSelectContext (u4ContextId) == RST_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                i4RetVal = RstCliShowPerformanceData (CliHandle);
                AstReleaseContext ();
                break;

            case CLI_STP_SHOW_PERF_DATA_IFACE:

                if ((!(AstIsRstStartedInContext (u4ContextId)) &&
                     (!(AstIsMstStartedInContext (u4ContextId)))))
                {
                    CliPrintf (CliHandle,
                               "\r\n RSTP/MSTP Module is not started "
                               "in this bridge\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                if (AstSelectContext (u4ContextId) == RST_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                i4RetVal =
                    RstCliShowPerformanceDataIface (CliHandle, u4ContextId,
                                                    u4Index);
                AstReleaseContext ();
                break;
#ifdef MSTP_WANTED
            case CLI_STP_SHOW_PERF_DATA_INST_IFACE:

                if (AstSelectContext (u4ContextId) == RST_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                AstL2IwfGetPerformanceDataStatus (u4ContextId,
                                                  &bPerfDataStatus);
                AstReleaseContext ();
                if (bPerfDataStatus == AST_TRUE)
                {
                    u4Instance = CLI_PTR_TO_U4 (args[0]);
                    if (!(AstIsMstStartedInContext (u4ContextId)))
                    {
                        CliPrintf (CliHandle, "\r\n MSTP Module is not started"
                                   " in this bridge\r\n");
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    if (AstSelectContext (u4ContextId) == RST_FAILURE)
                    {
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                    i4RetVal = MstCliShowPerformanceDataInstIface (CliHandle,
                                                                   u4Index,
                                                                   u4Instance);
                    AstReleaseContext ();
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r%% Enable the PerformanceData status !\r\n ");
                }
                break;
#endif
            default:
                /* Given command does not match with any of the SHOW commands */
                CliPrintf (CliHandle, "\r%% Invalid Command !\r\n ");
                AST_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
        }
        /* If SwitchName or Interface is given as input for show command
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4Index != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_STP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", StpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    AST_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    CliPrintf (CliHandle, "\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssSpanningTreeShowDebugging                       */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Spanning tree debug level */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssSpanningTreeShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    if (AstSelectContext (AST_DEFAULT_CONTEXT) != RST_SUCCESS)
    {
        UNUSED_PARAM (CliHandle);
        return;
    }
    if ((!AST_IS_MST_STARTED ()) && (!AST_IS_RST_STARTED ())
        && (!AST_IS_PVRST_STARTED ()))
    {
        AstReleaseContext ();
        UNUSED_PARAM (CliHandle);
        return;
    }
    if (AST_IS_RST_STARTED ())
    {
        nmhGetFsRstDebugOption (&i4DbgLevel);
    }
    else if (AST_IS_MST_STARTED ())
    {
#ifdef MSTP_WANTED
        nmhGetFsMstDebug (&i4DbgLevel);
#endif
    }
    else
    {
#ifdef PVRST_WANTED
        nmhGetFsPvrstDebug (&i4DbgLevel);
#endif
    }

    AstReleaseContext ();
    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rSpanning Tree :");

    if ((i4DbgLevel & AST_INIT_SHUT_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree init and shutdown debugging is on");
    }

    if ((i4DbgLevel & AST_MGMT_DBG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Spanning tree management debugging is on");
    }

    if ((i4DbgLevel & AST_MEM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree memory related debugging is on");
    }

    if ((i4DbgLevel & AST_BPDU_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree bpdu related debugging is on");
    }

    if ((i4DbgLevel & AST_EVENT_HANDLING_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree event handling related debugging is on");
    }

    if ((i4DbgLevel & AST_TMR_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree timers related debugging is on");
    }
    if ((i4DbgLevel & AST_PISM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port Information State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_PRSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port receive State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_RSSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port role selection State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_RTSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port role transition State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_STSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port state transition State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_PMSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree Port protocol migration State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_TCSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree topology change State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_TXSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree transmit State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_BDSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree bridge detection State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_PSSM_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree pseudoInfo State Machine debugging is on");
    }
    if ((i4DbgLevel & AST_RED_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree redundancy related debugging is on");
    }
    if ((i4DbgLevel & AST_SM_VAR_DBG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spanning tree state-machine variables related debugging is on");
    }
    if ((i4DbgLevel & AST_ALL_FAILURE_DBG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Spanning tree error debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstCliSelectContextOnMode                        */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
AstCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                           UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    UINT1               u1IntfCmdFlag = AST_FALSE;    /* This flag is used in MI 
                                                       case, to know whether 
                                                       the command is an 
                                                       interface mode command or
                                                       MST mode command. */

    /* For debug commands the context-name will be present in args[0], so
     * the select context will be done seperately within the
     * switch statement*/
    if ((u4Cmd == CLI_STP_GBL_DEBUG_ENABLE) ||
        (u4Cmd == CLI_STP_GBL_DEBUG_DISABLE) ||
        (u4Cmd == CLI_STP_DEBUG_ENABLE) || (u4Cmd == CLI_STP_DEBUG_DISABLE) ||
        (u4Cmd == CLI_STP_INIT_PROTOCOL_MIGRATION) ||
        (u4Cmd == CLI_STP_INIT_PORT_PROTOCOL_MIGRATION) ||
        (u4Cmd == CLI_CVLAN_STP_DEBUG_ENABLE) ||
        (u4Cmd == CLI_CVLAN_STP_DEBUG_DISABLE))
    {
        return RST_SUCCESS;
    }

    *pu4Context = AST_DEFAULT_CONTEXT;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = (UINT4) CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != AST_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI.
         * If the command is a MSTP mode command then the context-id won't be
         * AST_CLI_INVALID_CONTEXT, So this is an interface mode command.
         * Now by refering this flag we have to get the context-id and
         * local port number from the IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = AST_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * AstVcmGetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (AstVcmGetSystemMode (AST_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == AST_TRUE)
        {
            /* This is an Interface Mode command 
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (AstVcmGetContextInfoFromIfIndex
                    (i4PortId, pu4Context, pu2LocalPort) != VCM_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return RST_FAILURE;
                }
            }
        }
    }

    /* Since we are calling SI nmh routine for Configuration commands
     * we have to do SelectContext for Config commands*/
    if (AstSelectContext (*pu4Context) != RST_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return RST_FAILURE;
    }

    if ((u4Cmd != CLI_STP_SYS_CTRL) && (u4Cmd != CLI_STP_SHUT)
        && (u4Cmd != CLI_STP_NO_MOD_STATUS)
        && !(AST_IS_RST_STARTED () || AST_IS_MST_STARTED () ||
             AST_IS_PVRST_STARTED ()))
    {
        if (u4Cmd == CLI_STP_MOD_STATUS)
        {
            return RST_SUCCESS;
        }

        CliPrintf (CliHandle, "\r%% Spanning Tree is shutdown\r\n ");
        AstReleaseContext ();
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstCliGetContextForShowCmd                       */
/*                                                                           */
/*    Description         : This function is called only when the show       */
/*                          command is given. It handles 3 different         */
/*                          sceneario of show commands.                      */
/*                            1. If switch name is given, we have to get     */
/*                               the context Id from it.                     */
/*                            2. If Interface Index is present, from that    */
/*                               we have to get the context Id.              */
/*                            3. Else we have to go for each and every       */
/*                               context.                                    */
/*                          For the above all thing we have to check for     */
/*                          the Module status of the context.                */
/*                                                                           */
/*    Input(s)            : u4Cmd            - Command.                      */
/*                          pu1Name          - Switch-Name.                  */
/*                          u4IfIndex        - Interface Index.              */
/*                          u4CurrContext    - Context-Id.                   */
/*                                                                           */
/*    Output(s)           : CliHandle        - Contains error messages.      */
/*                          pu4NextContextId - Context Id.                   */
/*                          pu2LocalPort     - Local Port Number.            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name,
                            UINT4 u4IfIndex, UINT4 u4CurrContext,
                            UINT4 *pu4NextContext, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];

    *pu2LocalPort = 0;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1Name != NULL)
    {
        if (AstVcmIsSwitchExist (pu1Name, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "Switch %s Does not exist.\r\n", pu1Name);
            return RST_FAILURE;
        }
    }
    /* If IfIndex is given then get the Context-Id from it */
    else if (u4IfIndex != 0)
    {
        if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                             pu2LocalPort) != RST_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Interface not mapped to "
                       "any of the context.\r\n ");
            return RST_FAILURE;
        }
    }
    else
    {
        /* Case 1: At first entry for this funtion, If Switch-name is not given, 
           then get the first active Context. For this if we give 
           0xffffffff as current context for AstGetNextActiveContext 
           function it will return the First Active context. 
           Case 2: At the Next frequent entries it will as per we want. */
        if (AstGetNextActiveContext (u4CurrContext,
                                     &u4ContextId) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    do
    {
        u4CurrContext = u4ContextId;

        /* To avoid "Switch <switch-name>" print in SI */
        if (AstVcmGetSystemMode (AST_PROTOCOL_ID) == VCM_MI_MODE)
        {
            /* if switch name is not given we have to get the switch-name 
             * from context-id for Display purpose */
            AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

            if (pu1Name == NULL)
            {
                AstVcmGetAliasName (u4ContextId, au1ContextName);
            }
            else
            {
                AST_MEMCPY (au1ContextName, pu1Name, STRLEN (pu1Name));
            }
            CliPrintf (CliHandle, "\r\n\rSwitch %s\r\n", au1ContextName);
        }

        if (!(AstIsRstStartedInContext (u4ContextId)
              || AstIsMstStartedInContext (u4ContextId)
              || AstIsPvrstStartedInContext (u4ContextId)))
        {
            CliPrintf (CliHandle, "\r%% Spanning Tree is shutdown\r\n ");
            if ((pu1Name != NULL) || (u4IfIndex != 0))
            {
                /* We have to come out of the loop when the switch is 
                 * shutdown, if the command is for specific switch else
                 * continue the Loop. */
                break;
            }
            continue;
        }
        *pu4NextContext = u4ContextId;
        return RST_SUCCESS;
    }
    while (AstGetNextActiveContext (u4CurrContext,
                                    &u4ContextId) == RST_SUCCESS);

    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstBpduguardEnable                                 */
/*                                                                           */
/*     DESCRIPTION      : This function enables bpdu-guard on the given      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstBpduguardEnable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,
                    UINT1 u1BpduGuardAction)
{
    UINT4               u4ErrCode = 0;

    if (u1BpduGuardAction == AST_INIT_VAL)
    {
        /*Default BPDU Guard Action is Disabling Spanning-tree */
        u1BpduGuardAction = AST_PORT_STATE_DISABLED;
    }

    if (nmhTestv2FsRstPortBpduGuard (&u4ErrCode, i4Index,
                                     u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRstPortBpduGuardAction (&u4ErrCode, i4Index,
                                           (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRstPortBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsRstPortBpduGuardAction (i4Index, (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  StpCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
StpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    gu4StpTrcLvl = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        gu4StpTrcLvl = AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_MEM_DBG |
            AST_BPDU_DBG | AST_EVENT_HANDLING_DBG | AST_TMR_DBG |
            AST_PISM_DBG | AST_PRSM_DBG | AST_RSSM_DBG |
            AST_RTSM_DBG | AST_STSM_DBG | AST_PMSM_DBG | AST_TCSM_DBG |
            AST_TXSM_DBG | AST_BDSM_DBG | AST_ALL_FAILURE_DBG | AST_RED_DBG |
            AST_SM_VAR_DBG | AST_PSSM_DBG;
    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        gu4StpTrcLvl = AST_INIT_SHUT_DBG | AST_MGMT_DBG | AST_MEM_DBG |
            AST_BPDU_DBG | AST_EVENT_HANDLING_DBG | AST_TMR_DBG |
            AST_PISM_DBG | AST_PRSM_DBG | AST_RSSM_DBG |
            AST_RTSM_DBG | AST_STSM_DBG | AST_PMSM_DBG | AST_TCSM_DBG |
            AST_TXSM_DBG | AST_BDSM_DBG;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        gu4StpTrcLvl =
            AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG | AST_EVENT_HANDLING_DBG;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        gu4StpTrcLvl =
            AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG | AST_EVENT_HANDLING_DBG;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        gu4StpTrcLvl = AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4StpTrcLvl = AST_INIT_SHUT_DBG;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstBpduguardDisable                                */
/*                                                                           */
/*     DESCRIPTION      : This function disables bpdu-guard on the given     */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstBpduguardDisable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsRstPortBpduGuard (&u4ErrCode,
                                     i4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2FsRstPortBpduGuardAction (&u4ErrCode, i4Index,
                                           AST_PORT_STATE_DISABLED) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsRstPortBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetFsRstPortBpduGuardAction (i4Index, AST_PORT_STATE_DISABLED) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

#endif
