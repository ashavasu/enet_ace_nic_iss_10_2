/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmibrlw.c,v 1.12 2014/09/16 10:37:04 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmsbrlw.h"
# include   "vcm.h"
# include  "asthdrs.h"
# include  "stpcli.h"

/* LOW LEVEL Routines for Table : FsDot1dStpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStpTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStpTable (INT4 i4FsDot1dStpContextId)
{

    if (AST_IS_VC_VALID (i4FsDot1dStpContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStpTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStpTable (INT4 *pi4FsDot1dStpContextId)
{

    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1dStpContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStpTable
 Input       :  The Indices
                FsDot1dStpContextId
                nextFsDot1dStpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dStpTable (INT4 i4FsDot1dStpContextId,
                                INT4 *pi4NextFsDot1dStpContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsDot1dStpContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1dStpContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStpProtocolSpecification
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpProtocolSpecification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpProtocolSpecification (INT4 i4FsDot1dStpContextId,
                                       INT4
                                       *pi4RetValFsDot1dStpProtocolSpecification)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1dStpProtocolSpecification
        (pi4RetValFsDot1dStpProtocolSpecification);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPriority
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPriority (INT4 i4FsDot1dStpContextId,
                          INT4 *pi4RetValFsDot1dStpPriority)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPriority (pi4RetValFsDot1dStpPriority);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpTimeSinceTopologyChange
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpTimeSinceTopologyChange (INT4 i4FsDot1dStpContextId,
                                         UINT4
                                         *pu4RetValFsDot1dStpTimeSinceTopologyChange)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1dStpTimeSinceTopologyChange
        (pu4RetValFsDot1dStpTimeSinceTopologyChange);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpTopChanges
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpTopChanges (INT4 i4FsDot1dStpContextId,
                            UINT4 *pu4RetValFsDot1dStpTopChanges)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpTopChanges (pu4RetValFsDot1dStpTopChanges);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpDesignatedRoot
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpDesignatedRoot (INT4 i4FsDot1dStpContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsDot1dStpDesignatedRoot)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpDesignatedRoot (pRetValFsDot1dStpDesignatedRoot);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpRootCost
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpRootCost (INT4 i4FsDot1dStpContextId,
                          INT4 *pi4RetValFsDot1dStpRootCost)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpRootCost (pi4RetValFsDot1dStpRootCost);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpRootPort
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpRootPort (INT4 i4FsDot1dStpContextId,
                          INT4 *pi4RetValFsDot1dStpRootPort)
{
    INT1                i1RetVal;
    INT4                i4LocalPortNum;

    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpRootPort (&i4LocalPortNum);

    if (i4LocalPortNum != 0)
    {
        *pi4RetValFsDot1dStpRootPort = (INT4) AST_GET_IFINDEX (i4LocalPortNum);
    }
    else
    {
        *pi4RetValFsDot1dStpRootPort = 0;
    }

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpMaxAge
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpMaxAge (INT4 i4FsDot1dStpContextId,
                        INT4 *pi4RetValFsDot1dStpMaxAge)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpMaxAge (pi4RetValFsDot1dStpMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpHelloTime
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpHelloTime (INT4 i4FsDot1dStpContextId,
                           INT4 *pi4RetValFsDot1dStpHelloTime)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpHelloTime (pi4RetValFsDot1dStpHelloTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpHoldTime
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpHoldTime (INT4 i4FsDot1dStpContextId,
                          INT4 *pi4RetValFsDot1dStpHoldTime)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpHoldTime (pi4RetValFsDot1dStpHoldTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpForwardDelay
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpForwardDelay (INT4 i4FsDot1dStpContextId,
                              INT4 *pi4RetValFsDot1dStpForwardDelay)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpForwardDelay (pi4RetValFsDot1dStpForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpBridgeMaxAge
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpBridgeMaxAge (INT4 i4FsDot1dStpContextId,
                              INT4 *pi4RetValFsDot1dStpBridgeMaxAge)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpBridgeMaxAge (pi4RetValFsDot1dStpBridgeMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpBridgeHelloTime
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpBridgeHelloTime (INT4 i4FsDot1dStpContextId,
                                 INT4 *pi4RetValFsDot1dStpBridgeHelloTime)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1dStpBridgeHelloTime (pi4RetValFsDot1dStpBridgeHelloTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpBridgeForwardDelay
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                retValFsDot1dStpBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpBridgeForwardDelay (INT4 i4FsDot1dStpContextId,
                                    INT4 *pi4RetValFsDot1dStpBridgeForwardDelay)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1dStpBridgeForwardDelay
        (pi4RetValFsDot1dStpBridgeForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPriority
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPriority (INT4 i4FsDot1dStpContextId,
                          INT4 i4SetValFsDot1dStpPriority)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPriority (i4SetValFsDot1dStpPriority);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpBridgeMaxAge
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpBridgeMaxAge (INT4 i4FsDot1dStpContextId,
                              INT4 i4SetValFsDot1dStpBridgeMaxAge)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpBridgeMaxAge (i4SetValFsDot1dStpBridgeMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpBridgeHelloTime
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpBridgeHelloTime (INT4 i4FsDot1dStpContextId,
                                 INT4 i4SetValFsDot1dStpBridgeHelloTime)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1dStpBridgeHelloTime (i4SetValFsDot1dStpBridgeHelloTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpBridgeForwardDelay
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                setValFsDot1dStpBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpBridgeForwardDelay (INT4 i4FsDot1dStpContextId,
                                    INT4 i4SetValFsDot1dStpBridgeForwardDelay)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetDot1dStpBridgeForwardDelay (i4SetValFsDot1dStpBridgeForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPriority
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPriority (UINT4 *pu4ErrorCode, INT4 i4FsDot1dStpContextId,
                             INT4 i4TestValFsDot1dStpPriority)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPriority (pu4ErrorCode, i4TestValFsDot1dStpPriority);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpBridgeMaxAge
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpBridgeMaxAge (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot1dStpContextId,
                                 INT4 i4TestValFsDot1dStpBridgeMaxAge)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpBridgeMaxAge (pu4ErrorCode,
                                       i4TestValFsDot1dStpBridgeMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpBridgeHelloTime
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpBridgeHelloTime (UINT4 *pu4ErrorCode,
                                    INT4 i4FsDot1dStpContextId,
                                    INT4 i4TestValFsDot1dStpBridgeHelloTime)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpBridgeHelloTime (pu4ErrorCode,
                                          i4TestValFsDot1dStpBridgeHelloTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpBridgeForwardDelay
 Input       :  The Indices
                FsDot1dStpContextId

                The Object 
                testValFsDot1dStpBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDot1dStpContextId,
                                       INT4
                                       i4TestValFsDot1dStpBridgeForwardDelay)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsDot1dStpContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpBridgeForwardDelay (pu4ErrorCode,
                                             i4TestValFsDot1dStpBridgeForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStpTable
 Input       :  The Indices
                FsDot1dStpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDot1dStpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dStpPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dStpPortTable (INT4 i4FsDot1dStpPort)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dStpPortTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dStpPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dStpPortTable (INT4 *pi4FsDot1dStpPort)
{
    return nmhGetNextIndexFsDot1dStpPortTable (0, pi4FsDot1dStpPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dStpPortTable
 Input       :  The Indices
                FsDot1dStpPort
                nextFsDot1dStpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dStpPortTable (INT4 i4FsDot1dStpPort,
                                    INT4 *pi4NextFsDot1dStpPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsDot1dStpPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsDot1dStpPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsDot1dStpPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                && (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                              (pAstPortEntry))))
            {
                *pi4NextFsDot1dStpPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortPriority
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortPriority (INT4 i4FsDot1dStpPort,
                              INT4 *pi4RetValFsDot1dStpPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortPriority ((INT4) u2LocalPortId,
                                           pi4RetValFsDot1dStpPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortState
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortState (INT4 i4FsDot1dStpPort,
                           INT4 *pi4RetValFsDot1dStpPortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortState ((INT4) u2LocalPortId,
                                        pi4RetValFsDot1dStpPortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortEnable
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortEnable (INT4 i4FsDot1dStpPort,
                            INT4 *pi4RetValFsDot1dStpPortEnable)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortEnable ((INT4) u2LocalPortId,
                                         pi4RetValFsDot1dStpPortEnable);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortPathCost
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortPathCost (INT4 i4FsDot1dStpPort,
                              INT4 *pi4RetValFsDot1dStpPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortPathCost ((INT4) u2LocalPortId,
                                           pi4RetValFsDot1dStpPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortDesignatedRoot
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortDesignatedRoot (INT4 i4FsDot1dStpPort,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsDot1dStpPortDesignatedRoot)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortDesignatedRoot ((INT4) u2LocalPortId,
                                                 pRetValFsDot1dStpPortDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortDesignatedCost
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortDesignatedCost (INT4 i4FsDot1dStpPort,
                                    INT4 *pi4RetValFsDot1dStpPortDesignatedCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortDesignatedCost ((INT4) u2LocalPortId,
                                                 pi4RetValFsDot1dStpPortDesignatedCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortDesignatedBridge
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortDesignatedBridge (INT4 i4FsDot1dStpPort,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsDot1dStpPortDesignatedBridge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortDesignatedBridge ((INT4) u2LocalPortId,
                                                   pRetValFsDot1dStpPortDesignatedBridge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortDesignatedPort
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortDesignatedPort (INT4 i4FsDot1dStpPort,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsDot1dStpPortDesignatedPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortDesignatedPort ((INT4) u2LocalPortId,
                                                 pRetValFsDot1dStpPortDesignatedPort);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortForwardTransitions
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                retValFsDot1dStpPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dStpPortForwardTransitions (INT4 i4FsDot1dStpPort,
                                        UINT4
                                        *pu4RetValFsDot1dStpPortForwardTransitions)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dStpPortForwardTransitions ((INT4) u2LocalPortId,
                                                     pu4RetValFsDot1dStpPortForwardTransitions);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsDot1dStpPortPathCost32
 Input       :  The Indices
                FsDot1dStpPort
                The Object 
                retValFsDot1dStpPortPathCost32
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot1dStpPortPathCost32 (INT4 i4FsDot1dStpPort,
                                INT4 *pi4RetValFsDot1dStpPortPathCost32)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1dStpPortPathCost32 ((INT4) u2LocalPortId,
                                             pi4RetValFsDot1dStpPortPathCost32);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortPriority
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortPriority (INT4 i4FsDot1dStpPort,
                              INT4 i4SetValFsDot1dStpPortPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortPriority ((INT4) u2LocalPortId,
                                           i4SetValFsDot1dStpPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortEnable
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortEnable (INT4 i4FsDot1dStpPort,
                            INT4 i4SetValFsDot1dStpPortEnable)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstVcmGetContextInfoFromIfIndex ((UINT4) i4FsDot1dStpPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortEnable ((INT4) u2LocalPortId,
                                         i4SetValFsDot1dStpPortEnable);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortPathCost
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortPathCost (INT4 i4FsDot1dStpPort,
                              INT4 i4SetValFsDot1dStpPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortPathCost ((INT4) u2LocalPortId,
                                           i4SetValFsDot1dStpPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDot1dStpPortPathCost32
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                setValFsDot1dStpPortPathCost32
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dStpPortPathCost32 (INT4 i4FsDot1dStpPort,
                                INT4 i4SetValFsDot1dStpPortPathCost32)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dStpPortPathCost32 ((INT4) u2LocalPortId,
                                             i4SetValFsDot1dStpPortPathCost32);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortPriority
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsDot1dStpPort,
                                 INT4 i4TestValFsDot1dStpPortPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortPriority (pu4ErrorCode, (INT4) u2LocalPortId,
                                       i4TestValFsDot1dStpPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortEnable
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortEnable (UINT4 *pu4ErrorCode, INT4 i4FsDot1dStpPort,
                               INT4 i4TestValFsDot1dStpPortEnable)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dStpPortEnable (pu4ErrorCode, (INT4) u2LocalPortId,
                                            i4TestValFsDot1dStpPortEnable);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortPathCost
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortPathCost (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot1dStpPort,
                                 INT4 i4TestValFsDot1dStpPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortPathCost (pu4ErrorCode, (INT4) u2LocalPortId,
                                       i4TestValFsDot1dStpPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dStpPortPathCost32
 Input       :  The Indices
                FsDot1dStpPort

                The Object 
                testValFsDot1dStpPortPathCost32
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dStpPortPathCost32 (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDot1dStpPort,
                                   INT4 i4TestValFsDot1dStpPortPathCost32)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsDot1dStpPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1dStpPortPathCost32 (pu4ErrorCode, (INT4) u2LocalPortId,
                                         i4TestValFsDot1dStpPortPathCost32);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dStpPortTable
 Input       :  The Indices
                FsDot1dStpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dStpPortTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
