/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astptxsm.c,v 1.57 2017/11/30 06:29:48 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Transmit State Event Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortTransmitMachine                               */
/*                                                                           */
/* Description        : This is the main routine for the Port Transmit State */
/*                      Machine. This function is called whenever a BPDU     */
/*                      (TCN Bpdu, CONFIG Bpdu, RST Bpdu or MST Bpdu) needs  */
/*                      to be transmitted.                                   */
/*                                                                           */
/* Input(s)           : u1Event - The Event that has caused the Port Transmit*/
/*                                Machine to be called                       */
/*                      pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTransmitMachine (UINT2 u2Event, tAstPortEntry * pAstPortEntry,
                        UINT2 u2InstanceId)
{
    UINT2               u2State = (UINT1) AST_INIT_VAL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (NULL == pAstPortEntry)
    {
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Event %s: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_TXSM][u2Event]);
        return RST_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                              u2InstanceId);
    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG3 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Event %s: Port %s does not exist for instance %d!!!\n",
                      gaaau1AstSemEvent[AST_TXSM][u2Event],
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                      u2InstanceId);
        return RST_FAILURE;
    }
    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    u2State = (UINT2) pAstCommPortInfo->u1PortTxSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "TXSM: Port %s: Tx Machine Called with Event: %s,State: %s of Instance %u\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                  gaaau1AstSemEvent[AST_TXSM][u2Event],
                  gaaau1AstSemState[AST_TXSM][u2State], u2InstanceId);

    AST_DBG_ARG4 (AST_TXSM_DBG,
                  "TXSM: Port %s: Tx Machine Called with Event: %s,State: %s of Instance %u\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                  gaaau1AstSemEvent[AST_TXSM][u2Event],
                  gaaau1AstSemState[AST_TXSM][u2State], u2InstanceId);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PTXSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_TXSM_DBG,
                 "TXSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if ((RST_PORT_TX_MACHINE[u2Event][u2State].pAction) == NULL)
    {
        AST_DBG (AST_TXSM_DBG, "TXSM: No Operations to perform\n");
        return RST_SUCCESS;
    }

    if ((*(RST_PORT_TX_MACHINE[u2Event][u2State].pAction))
        (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: State %d Event %d: Port Transmit Event Function returned FAILURE!\n",
                      u2State, u2Event);
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeTransmitInit                          */
/*                                                                           */
/* Description        : This function is called during initialisation of the */
/*                      Port Transmit State Machine. This changes the state  */
/*                      of the Port Transmit state machine to TRANSMIT_INIT  */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmMakeTransmitInit (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    u2PortNum = pAstPortEntry->u2PortNo;

    pAstCommPortInfo->u1TxCount = (UINT1) 0;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum,
                                                RST_DEFAULT_INSTANCE);

    if (pAstCommPortInfo->pHoldTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pAstPortEntry, (UINT1) AST_TMR_TYPE_HOLD) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: AstStopTimer for HoldTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (AST_IS_RST_ENABLED ())
    {
        pAstCommPortInfo->bNewInfo = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "TXSM_TransmitInit: Port %s: NewInfo = TRUE TxCount = 0\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_FALSE;
            AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti = MST_FALSE;
        }
        else
        {
            AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti = MST_FALSE;
        }
        /* Starting of HelloWhen timer removed from here
         * to IDLE state as per 802.1Q 2005
         * */
    }
#endif

    pAstCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_INIT;

    AST_DBG_ARG1 (AST_TXSM_DBG, "TXSM: Port %s: Moved to state TRANSMIT_INIT\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* If the port is disabled the port should remain in the init state.
     * This change has been specified in 802.1Q-REV/D2 onwards */

    /* 802.1ah/D4-2: If enableBPDUTx is false then port should
     * remain in the init state.
     */
    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) ||
        (pAstPortEntry->u1EntryStatus == CFA_IF_DOWN) ||
        (pRstPortInfo->bPortEnabled == RST_FALSE) ||
        (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_FALSE))
    {
        return RST_SUCCESS;
    }

    if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmCheckIdle                                 */
/*                                                                           */
/* Description        : This function is called when BEGIN is cleared        */
/*                      in the case of RSTP to check if the machine can      */
/*                      transition from the INIT state to the IDLE state.    */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmCheckIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                u2InstanceId);
#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        /* This check has been introduced as per 802.1Q 2005 sec 13.15
         * */
        if (MstPortTxSmAllTransmitReady (pAstPortEntry->u2PortNo) == MST_FALSE)
        {
            AST_DBG (AST_TXSM_DBG,
                     "TXSM: - Selected, Update Info is not Valid Exiting.\n");
            return RST_SUCCESS;
        }
    }
#endif

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_FALSE) &&
        (pAstPortEntry->u1EntryStatus == CFA_IF_UP) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE) &&
        (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_TRUE))
    {
        if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeTransmitConfig                        */
/*                                                                           */
/* Description        : This function is called whenever a Config BPDU needs */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TRANSMIT_CONFIG state.      */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmMakeTransmitConfig (tAstPortEntry * pAstPortEntry,
                               UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
    u2PortNum = pAstPortEntry->u2PortNo;

    if (AST_IS_RST_ENABLED ())
    {
        pAstCommPortInfo->bNewInfo = RST_FALSE;
    }
#ifdef MSTP_WANTED
    else
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_FALSE;
        }
    }
#endif /* MSTP_WANTED */

    if (RstPortTxSmTxConfig (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmConfig function returned FAILURE!\n");
        return RST_FAILURE;
    }

    /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
    (pAstCommPortInfo->u1TxCount)++;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Config BPDU sent, incremented TxCount is: %u\n",
                  pAstCommPortInfo->u1TxCount);
    pAstCommPortInfo->bTcAck = RST_FALSE;

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "TXSM_TransmitConfig: Port %s: NewInfo = TcAck = FALSE TxCount = %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), pAstCommPortInfo->u1TxCount);

    if (pAstCommPortInfo->pHoldTmr == NULL)
    {
        if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                           (UINT1) AST_TMR_TYPE_HOLD,
                           AST_HOLD_TIME) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: AstStartTimer  for HoldTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_CONFIG;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %s: Moved to state TRANSMIT_CONFIG\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeTransmitTcn                           */
/*                                                                           */
/* Description        : This function is called whenever a TCN BPDU needs    */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TANSMIT_TCN state.          */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmMakeTransmitTcn (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
    u2PortNum = pAstPortEntry->u2PortNo;

    if (AST_IS_RST_ENABLED ())
    {
        pAstCommPortInfo->bNewInfo = RST_FALSE;
    }
#ifdef MSTP_WANTED
    else
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_FALSE;
        }
    }
#endif /* MSTP_WANTED */

    if (RstPortTxSmTxTcn (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmTcn function returned FAILURE!\n");
        return RST_FAILURE;
    }

    /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
    (pAstCommPortInfo->u1TxCount)++;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: TCN BPDU sent, incremented TxCount is: %u\n",
                  pAstCommPortInfo->u1TxCount);
    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "TXSM_TransmitConfig: Port %s: NewInfo = TcAck = FALSE TxCount = %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), pAstCommPortInfo->u1TxCount);

    if (pAstCommPortInfo->pHoldTmr == NULL)
    {
        if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                           (UINT1) AST_TMR_TYPE_HOLD,
                           AST_HOLD_TIME) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: AstStartTimer for HoldTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_TCN;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %s: Moved to state TRANSMIT_TCN\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeTransmitRstp                          */
/*                                                                           */
/* Description        : This function is called whenever a RST BPDU needs    */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TANSMIT_RSTP state.         */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortTxSmMakeTransmitRstp (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
    u2PortNum = pAstPortEntry->u2PortNo;

    if (AST_IS_RST_ENABLED ())
    {
        pAstCommPortInfo->bNewInfo = RST_FALSE;

        if (RstPortTxSmTxRstp (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmRstp function returned FAILURE!\n");
            return RST_FAILURE;
        }
        /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
        (pAstCommPortInfo->u1TxCount)++;
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_FALSE;
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti = MST_FALSE;

        if (AST_FORCE_VERSION == AST_VERSION_2)

        {
            if (RstPortTxSmTxRstp (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
            {
                AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                         "TXSM: RstPortTxSmRstp function returned FAILURE!\n");
                return RST_FAILURE;
            }
            /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
            (pAstCommPortInfo->u1TxCount)++;
        }
        else
        {
            if (MstPortTxSmTxMstp (pAstPortEntry, u2InstanceId) != MST_SUCCESS)
            {
                AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                         "TXSM: MstPortTxSmMstp function returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
    }
#endif /* MSTP_WANTED */

    pAstCommPortInfo->bTcAck = RST_FALSE;

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "TXSM_TransmitRstp: Port %s: NewInfo = TcAck = FALSE TxCount = %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), pAstCommPortInfo->u1TxCount);

    if (pAstCommPortInfo->pHoldTmr == NULL)
    {
        if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                           (UINT1) AST_TMR_TYPE_HOLD,
                           AST_HOLD_TIME) != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: AstStartTimer for HoldTmr FAILED!\n");
            return RST_FAILURE;
        }
    }

    pAstCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_RSTP;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %s: Moved to state TRANSMIT_RSTP\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeTransmitPeriodic                      */
/*                                                                           */
/* Description        : This function is called whenever the Hellowhen timer */
/*                      for this port expires and this might result in the   */
/*                      periodic transmission of Hello messages on this port */
/*                      This is called when the Port Tx state machine is in  */
/*                      the IDLE state and this function changes the state to*/
/*                      the TRANSMIT_PERIODIC state.                         */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmMakeTransmitPeriodic (tAstPortEntry * pAstPortEntry,
                                 UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
#ifdef MSTP_WANTED
    tAstBoolean         bMstiFlag = MST_FALSE;
#endif

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                              u2InstanceId);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG (AST_TXSM_DBG, "TXSM: Selected,UpdtInfo is not Valid.\n");
        return RST_SUCCESS;
    }

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        /* This check has been introduced as per 802.1Q 2005 sec 13.15
         * */
        if (MstPortTxSmAllTransmitReady (pAstPortEntry->u2PortNo) == MST_FALSE)
        {
            AST_DBG (AST_TXSM_DBG,
                     "TXSM: - Selected, Update Info is not Valid Exiting.\n");
            return RST_SUCCESS;
        }
    }
#endif

#ifdef NPAPI_WANTED
    /* Any failed NPAPI calls are retried every hello time. */
    if ((AST_IS_RST_ENABLED ()) &&
        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        if (pPerStPortInfo->i4NpFailRetryCount < AST_MAX_RETRY_COUNT)
        {
            RstRetryNpapi (pAstPortEntry);
        }
    }
#endif

    if (AST_IS_RST_ENABLED ())
    {
        if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED) ||
            ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT) &&
             (pPerStRstPortInfo->pTcWhileTmr != NULL)))
        {
            pAstCommPortInfo->bNewInfo = RST_TRUE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = TRUE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }

#ifdef NPAPI_WANTED
        else if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
        {
            /* If setting port state to blocking in hardware has failed, 
             * then NewInfo should be set even on Alternate or Backup ports
             * in order to send inferior info to the peer */
            pAstCommPortInfo->bNewInfo = RST_TRUE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = TRUE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }
#endif
        else
        {
            pAstCommPortInfo->bNewInfo = RST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = FALSE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }
    }
#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        /* Check is concerned about CIST
         * Getting the information about CIST
         * */
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                  RST_DEFAULT_INSTANCE);

        if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
            || ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
                && (pPerStRstPortInfo->pTcWhileTmr != NULL)))
        {
            AST_GET_CIST_MSTI_PORT_INFO (pAstPortEntry->u2PortNo)->bNewInfo
                = MST_TRUE;
        }

#ifdef NPAPI_WANTED
        else if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
        {
            /* If setting port state to blocking in hardware has failed,
             * then NewInfo should be set even on Alternate or Backup ports
             * in order to send inferior info to the peer */
            AST_GET_CIST_MSTI_PORT_INFO (pAstPortEntry->u2PortNo)->bNewInfo
                = MST_TRUE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = TRUE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }
#endif
        else
        {
            AST_GET_CIST_MSTI_PORT_INFO (pAstPortEntry->u2PortNo)->bNewInfo
                = MST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = FALSE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }
        /* Restoring the value this structure had already
         * */
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                  u2InstanceId);

        bMstiFlag = MstMstiDesignatedOrTcPropagatingRoot
            (pAstPortEntry->u2PortNo);
        if (bMstiFlag == MST_TRUE)
        {
            AST_GET_CIST_MSTI_PORT_INFO (pAstPortEntry->u2PortNo)->bNewInfoMsti
                = MST_TRUE;
        }
        else
        {
            AST_GET_CIST_MSTI_PORT_INFO (pAstPortEntry->u2PortNo)->bNewInfoMsti
                = MST_FALSE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "TXSM_TransmitPeriodic: Port %s: NewInfo = FALSE\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        }

#ifdef NPAPI_WANTED
        if (AST_IS_MST_ENABLED () &&
            (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
        {
            MstpRetryNpapiForAllInstances (pAstPortEntry);
        }
#endif /* NPAPI_WANTED */
    }
#endif
    pAstCommPortInfo->u1PortTxSmState =
        (UINT1) RST_PTXSM_STATE_TRANSMIT_PERIODIC;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %s: Moved to state TRANSMIT_PERIODIC\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

    if (RstPortTxSmMakeIdle (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstPortTxSmMakeIdle function returned FAILURE!\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmMakeIdle                                  */
/*                                                                           */
/* Description        : This function is called from all the other states of */
/*                      the Port Transmit state machine. This is called when */
/*                      the Port Tx State machine is in any of the other     */
/*                      states (TRANSMIT_INIT, TRANSMIT_PERIODIC, TRANSMIT_  */
/*                      CONFIG, TRANSMIT_TCN or TRANSMIT_RSTP) and this      */
/*                      function changes the state to the IDLE state.        */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmMakeIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBoolean         bTmpNewInfo = RST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;

    u2PortNum = pAstPortEntry->u2PortNo;
    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    pAstCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_IDLE;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %s: Moved to state IDLE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    u2Duration = pAstPortEntry->DesgTimes.u2HelloTime;

    if (AstStartTimer ((VOID *) pAstPortEntry, MST_CIST_CONTEXT,
                       (UINT1) AST_TMR_TYPE_HELLOWHEN,
                       u2Duration) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: AstStartTimer for HelloWhenTmr FAILED!\n");
        return RST_FAILURE;
    }

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "TXSM_Idle: Port %s: helloWhen = %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2Duration);

    /* Since the timer has just been started there is no need to check the
       transition to the TransmitPeriodic state */

    bTmpNewInfo = pAstCommPortInfo->bNewInfo;
#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            bTmpNewInfo = (tAstBoolean)
                ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo)
                 || (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti));
        }
    }
#endif /* MSTP_WANTED */

    if (bTmpNewInfo == RST_TRUE)
    {
        if (RstPortTxSmNewInfoSetIdle (pAstPortEntry, u2InstanceId)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmNewInfoSetIdle function "
                     "returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmSelectedSetIdle                           */
/*                                                                           */
/* Description        : This function is called when a Selected set event is */
/*                      received from the Role Selection state machine or    */
/*                      when the Port Transmit State Machine is in the IDLE  */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmSelectedSetIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        /* This check has been introduced as per 802.1Q 2005 sec 13.15
         * */
        if (MstPortTxSmAllTransmitReady (pAstPortEntry->u2PortNo) == MST_FALSE)
        {
            AST_DBG (AST_TXSM_DBG,
                     "TXSM: - Selected, Update Info is not Valid Exiting.\n");
            return RST_SUCCESS;
        }
    }
#endif

    if (pAstCommPortInfo->bNewInfo == RST_TRUE)
    {
        if (RstPortTxSmNewInfoSetIdle (pAstPortEntry, u2InstanceId)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmNewInfoSetIdle function "
                     "returned FAILURE!\n");
            return RST_FAILURE;
        }
    }
    else if (pAstCommPortInfo->pHelloWhenTmr == NULL)
    {
        /* Hello timer could have expired when Selected was false. So
         * handle the expiry now */
        if (RstPortTxSmMakeTransmitPeriodic (pAstPortEntry, u2InstanceId)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmMakeTransmitPeriodic function "
                     "returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmNewInfoSetIdle                            */
/*                                                                           */
/* Description        : This function is called when a NewInfo set event is  */
/*                      received from the Port Information state machine or  */
/*                      the Port Role Transtion state machine, when the Port */
/*                      Transmit State Machine is in the IDLE state.         */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmNewInfoSetIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pAstPortEntry->u2PortNo;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG (AST_TXSM_DBG,
                 "TXSM: Selected,Update Info is not Valid - Exiting.\n");
        return RST_SUCCESS;
    }

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        /* This check has been introduced as per 802.1Q 2005 sec 13.15
         * */
        if (MstPortTxSmAllTransmitReady (u2PortNum) == MST_FALSE)
        {
            AST_DBG (AST_TXSM_DBG,
                     "TXSM: - Selected, Update Info is not Valid Exiting.\n");
            return RST_SUCCESS;
        }
    }
#endif
    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstCommPortInfo->u1TxCount < pAstBridgeEntry->u1TxHoldCount)
    {
        if (pAstCommPortInfo->bSendRstp == RST_TRUE)
        {
            if (AST_IS_RST_ENABLED ())
            {
                if (pAstCommPortInfo->pHelloWhenTmr != NULL)
                {
                    if (RstPortTxSmMakeTransmitRstp
                        (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: RstPortTxSmMakeTransmitRstp function "
                                 "returned FAILURE!\n");
                        return RST_FAILURE;
                    }
                }
            }
#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {
                /* Checking for the conditions to move to TRANSMIT_RSTP state
                 * sendRSTP && (newInfo || (newInfoMsti && !mstMasterPort))
                 *   && (txCount < TxHoldCount) && (helloWhen != 0)
                 *
                 * sendRSTP && Hold Count check has been performed already
                 * */

                if ((pAstCommPortInfo->pHelloWhenTmr != NULL) &&
                    ((pAstPortEntry->CistMstiPortInfo.bNewInfo == MST_TRUE) ||
                     ((pAstPortEntry->CistMstiPortInfo.bNewInfoMsti == MST_TRUE)
                      && (pAstCommPortInfo->bMstiMasterPort == MST_FALSE))))
                {

                    if (RstPortTxSmMakeTransmitRstp
                        (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: RstPortTxSmMakeTransmitRstp function "
                                 "returned FAILURE!\n");
                        return RST_FAILURE;
                    }
                }
            }
#endif
        }                        /* End of bSendRstp is RST_TRUE */
        else
        {                        /* bSendRstp is RST_FALSE */
            if (pAstCommPortInfo->pHelloWhenTmr != NULL)
            {
#ifdef MSTP_WANTED
                /* In case of MSTP, if port is in STP mode, NewInfoMsti should
                 * NOT be considered for transmission */
                if ((AST_IS_MST_ENABLED ()) &&
                    (pAstPortEntry->CistMstiPortInfo.bNewInfo != MST_TRUE))
                {
                    return RST_SUCCESS;
                }
#endif
                if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
                {
                    if (RstPortTxSmMakeTransmitTcn (pAstPortEntry, u2InstanceId)
                        != RST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: RstPortTxSmMakeTransmitTcn function "
                                 "returned FAILURE!\n");
                        return RST_FAILURE;
                    }            /* Root Port Role */
                }
                else if (pPerStPortInfo->u1PortRole
                         == (UINT1) AST_PORT_ROLE_DESIGNATED)
                {
                    if (RstPortTxSmMakeTransmitConfig
                        (pAstPortEntry, u2InstanceId) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: RstPortTxSmMakeTransmitConfig function "
                                 "returned FAILURE\n");
                        return RST_FAILURE;
                    }
                }                /* Designated Port Role */
            }                    /* HelloWhenTmr is running */
        }                        /* End of bSendRstp is RST_FALSE */
    }                            /* Tx Count within Hold Count limits */
    else
    {
        AST_DBG_ARG1 (AST_TXSM_DBG,
                      "TXSM: Port %s: Hold Rate EXCEEDED, Not Sending BPDU...\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmHoldTmrExpIdle                            */
/*                                                                           */
/* Description        : This function is called when Hold timer expiry event */
/*                      is received from the Timer Submodule, when the Port  */
/*                      Transmit State Machine is in the IDLE state.         */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmHoldTmrExpIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBoolean         bTmpNewInfo = RST_FALSE;

    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

    if (AST_IS_RST_ENABLED ())
    {
        bTmpNewInfo = pAstCommPortInfo->bNewInfo;
    }
#ifdef MSTP_WANTED
    else
    {
        if (AST_IS_MST_ENABLED ())
        {
            /* This check has been introduced as per 802.1Q 2005 sec 13.15
             * */
            if (MstPortTxSmAllTransmitReady (pAstPortEntry->u2PortNo) ==
                MST_FALSE)
            {
                AST_DBG (AST_TXSM_DBG,
                         "TXSM: - Selected, Update Info is not Valid Exiting.\n");
                return RST_SUCCESS;
            }
        }
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            bTmpNewInfo = (tAstBoolean)
                ((AST_GET_CIST_MSTI_PORT_INFO
                  (pAstPortEntry->u2PortNo)->bNewInfo)
                 || (AST_GET_CIST_MSTI_PORT_INFO
                     (pAstPortEntry->u2PortNo)->bNewInfoMsti));
        }
    }
#endif /* MSTP_WANTED */

    if (bTmpNewInfo == RST_TRUE)
    {
        if (RstPortTxSmNewInfoSetIdle (pAstPortEntry, u2InstanceId)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: RstPortTxSmNewInfoSetIdle function "
                     "returned FAILURE!\n");
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmTxConfig                                  */
/*                                                                           */
/* Description        : This function is called by RstPortTxSmMakeTransmit   */
/*                      Config function in order to transmit a Config BPDU.  */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmTxConfig (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstBufChainHeader *pConfigBpdu = NULL;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;

    if ((pConfigBpdu = AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_CONFIG +
                                          AST_ENET_LLC_HEADER_SIZE, AST_OFFSET))
        == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        AST_INCR_BUFFER_FAILURE_COUNT ();
        AstBufferFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: Config Bpdu Allocation of CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: Config BPDU Allocation of CRU Buffer FAILED!\n");

        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "SYS: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pAstPortEntry->u2PortNo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return RST_FAILURE;
        }

        return RST_SUCCESS;
    }

    if (RstFormBpdu (pAstPortEntry, u2InstanceId, (UINT1) AST_BPDU_TYPE_CONFIG,
                     pConfigBpdu, &u4DataLength, &u1Flag) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pConfigBpdu, RST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: Config BPDU Release CRU Buffer FAILED!\n");
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: Config BPDU Release CRU Buffer FAILED!\n");
        }
        return RST_FAILURE;
    }

    if (AstFillEnetHeader (pConfigBpdu, &u4DataLength,
                           pAstPortEntry) == RST_FAILURE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                      AST_BUFFER_TRC,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

        AST_RELEASE_CRU_BUF (pConfigBpdu, RST_FALSE);
        return RST_FAILURE;
    }
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
#ifdef NPAPI_WANTED
        if (gAstAllowDirectNpTx == RST_TRUE)
        {
            if (AstHandlePacketToNP (pConfigBpdu,
                                     pAstPortEntry->u4IfIndex,
                                     u4DataLength) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                              "Unable to transmit packet\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
                return RST_FAILURE;
            }
        }
        else
        {
            AstHandleOutgoingPktOnPort (pConfigBpdu, pAstPortEntry,
                                        u4DataLength, AST_PROT_BPDU,
                                        (UINT1) AST_ENCAP_NONE);

        }
#else
        AstHandleOutgoingPktOnPort (pConfigBpdu, pAstPortEntry, u4DataLength,
                                    AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);
#endif

    }
    else
    {
        AST_RELEASE_CRU_BUF (pConfigBpdu, RST_FALSE);
    }
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TXSM: <<<<< Port %s: Config BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: <<<<< Port %s: Config BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

    /* Incrementing the Config BPDU received count */
    AST_INCR_TX_CONFIG_BPDU_COUNT (pAstPortEntry->u2PortNo);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmTxTcn                                     */
/*                                                                           */
/* Description        : This function is called by RstPortTxSmMakeTransmitTcn*/
/*                      function in order to transmit a TCN BPDU.            */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmTxTcn (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstBufChainHeader *pTcnBpdu = NULL;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;

    if ((pTcnBpdu = AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_TCN +
                                       AST_ENET_LLC_HEADER_SIZE, AST_OFFSET))
        == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        AST_INCR_BUFFER_FAILURE_COUNT ();
        AstBufferFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: TCN BPDU Allocation of CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: TCN BPDU Allocation of CRU Buffer FAILED!\n");
        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "SYS: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pAstPortEntry->u2PortNo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return RST_FAILURE;
        }

        return RST_SUCCESS;
    }

    if (RstFormBpdu (pAstPortEntry, u2InstanceId, (UINT1) AST_BPDU_TYPE_TCN,
                     pTcnBpdu, &u4DataLength, &u1Flag) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pTcnBpdu, RST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: TCN BPDU Release CRU Buffer FAILED!\n");
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: TCN BPDU Release CRU Buffer FAILED!\n");
        }
        return RST_FAILURE;
    }

    if (AstFillEnetHeader (pTcnBpdu, &u4DataLength, pAstPortEntry)
        == RST_FAILURE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                      AST_BUFFER_TRC,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

        AST_RELEASE_CRU_BUF (pTcnBpdu, RST_FALSE);
        return RST_FAILURE;
    }
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
#ifdef NPAPI_WANTED
        if (gAstAllowDirectNpTx == RST_TRUE)
        {
            if (AstHandlePacketToNP (pTcnBpdu,
                                     pAstPortEntry->u4IfIndex,
                                     u4DataLength) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                              "Unable to transmit packet\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
                return RST_FAILURE;
            }
        }
        else
        {
            AstHandleOutgoingPktOnPort (pTcnBpdu, pAstPortEntry, u4DataLength,
                                        AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);

        }
#else
        AstHandleOutgoingPktOnPort (pTcnBpdu, pAstPortEntry,
                                    u4DataLength, AST_PROT_BPDU,
                                    (UINT1) AST_ENCAP_NONE);
#endif
    }
    else
    {
        AST_RELEASE_CRU_BUF (pTcnBpdu, RST_FALSE);
    }
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TXSM: <<<<< Port %s: TCN BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: <<<<< Port %s: TCN BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

    /* Incrementing the TCN BPDU received count */
    AST_INCR_TX_TCN_BPDU_COUNT (pAstPortEntry->u2PortNo);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmTxRstp                                    */
/*                                                                           */
/* Description        : This function is called by RstPortTxSmMakeTransmit   */
/*                      Rstp function in order to transmit a RST BPDU.       */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmTxRstp (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    tAstBufChainHeader *pRstBpdu = NULL;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT4               u4Ticks = 0;

    if ((pRstBpdu =
         AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_RST + AST_ENET_LLC_HEADER_SIZE,
                            AST_OFFSET)) == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        AST_INCR_BUFFER_FAILURE_COUNT ();
        AstBufferFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: RST BPDU Allocation of CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: RST BPDU Allocation of CRU Buffer FAILED!\n");

        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "SYS: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pAstPortEntry->u2PortNo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return RST_FAILURE;
        }

        return RST_SUCCESS;
    }

    if (RstFormBpdu (pAstPortEntry, u2InstanceId, (UINT1) AST_BPDU_TYPE_RST,
                     pRstBpdu, &u4DataLength, &u1Flag) != RST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: RstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pRstBpdu, RST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: RST BPDU Release CRU Buffer FAILED!\n");
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: RST BPDU Release CRU Buffer FAILED!\n");
        }
        return RST_FAILURE;
    }

    if (AstFillEnetHeader (pRstBpdu, &u4DataLength, pAstPortEntry)
        == RST_FAILURE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                      AST_BUFFER_TRC,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

        AST_RELEASE_CRU_BUF (pRstBpdu, RST_FALSE);
        return RST_FAILURE;
    }
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
#ifdef NPAPI_WANTED
        if (gAstAllowDirectNpTx == RST_TRUE)
        {
            if (AstHandlePacketToNP (pRstBpdu,
                                     pAstPortEntry->u4IfIndex,
                                     u4DataLength) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                              "Unable to transmit packet\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
                return RST_FAILURE;
            }
        }
        else
        {
            AstHandleOutgoingPktOnPort (pRstBpdu, pAstPortEntry, u4DataLength,
                                        AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);

        }
#else

        AstHandleOutgoingPktOnPort (pRstBpdu, pAstPortEntry,
                                    u4DataLength, AST_PROT_BPDU,
                                    (UINT1) AST_ENCAP_NONE);
#endif
    }
    else
    {
        AST_RELEASE_CRU_BUF (pRstBpdu, RST_FALSE);
    }
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TXSM: <<<<< Port %s: RST BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: <<<<< Port %s: RST BPDU handed over to Bridge >>>>>\n",
                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

    OsixGetSysTime (&u4Ticks);

    /* Incrementing the RST BPDU received count */
    AST_INCR_TX_RST_BPDU_COUNT (pAstPortEntry->u2PortNo);

    /* Incrementing the proposal and agreement packet transmitted count */
    if (u1Flag == ((UINT1) RST_SET_AGREEMENT_FLAG))
    {
        AST_INCR_TX_AGREEMENT_COUNT (pAstPortEntry->u2PortNo);
        pAstPortEntry->u4AgreementPktSentTimeStamp = u4Ticks;
    }
    if (u1Flag == ((UINT1) RST_SET_PROPOSAL_FLAG))
    {
        AST_INCR_TX_PROPOSAL_COUNT (pAstPortEntry->u2PortNo);
        pAstPortEntry->u4ProposalPktSentTimeStamp = u4Ticks;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstFormBpdu                                          */
/*                                                                           */
/* Description        : This function is called by RstPortTxSmTxConfig,      */
/*                      RstPortTxSmTxTcn or RstPortTxSmTxRstp functions in   */
/*                      order to form a Config BPDU, TCN BPDU or RST BPDU.   */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                      u1MsgType - Indicates the type of BPDU to be formed  */
/*                                  AST_BPDU_TYPE_TCN or                     */
/*                                  AST_BPDU_TYPE_CONFIG or                  */
/*                                  AST_BPDU_TYPE_RST                        */
/*                      pBuf - Pointer to the CRU Buffer in which the BPDU   */
/*                             is to be formed                               */
/*                      pu4DataLength - The number of bytes that have been   */
/*                                      filled in the BPDU                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstFormBpdu (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId,
             UINT1 u1MsgType, tAstBufChainHeader * pBuf, UINT4 *pu4DataLength,
             UINT1 *pu1Flag)
{
    UINT4               u4Length = 0;
    UINT4               u4DataPaddLen = 0;
    UINT2               u2ProtocolId = (UINT2) AST_INIT_VAL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1Val = (UINT1) AST_INIT_VAL;
    UINT1               u1Version = (UINT1) AST_INIT_VAL;
    UINT1               u1Version1Length = (UINT1) AST_INIT_VAL;
    UINT1               u1BpduType = (UINT1) AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    AST_UNUSED (u2InstanceId);

    pu1BpduStart = pu1BpduEnd = gAstGlobalInfo.gpu1AstBpdu;
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                              RST_DEFAULT_INSTANCE);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    u2ProtocolId = (UINT2) AST_PROTOCOL_ID;

    switch (u1MsgType)
    {
        case AST_BPDU_TYPE_TCN:

            u1Version = (UINT1) AST_VERSION_0;
            u1BpduType = (UINT1) AST_BPDU_TYPE_TCN;

            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        case AST_BPDU_TYPE_CONFIG:

            u1Version = (UINT1) AST_VERSION_0;
            u1BpduType = (UINT1) AST_BPDU_TYPE_CONFIG;

            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            if (pPerStRstPortInfo->pTcWhileTmr != NULL)
            {
                u1Val = (UINT1) (u1Val | RST_SET_TOPOCH_FLAG);
            }

            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

            if (pAstCommPortInfo->bTcAck == RST_TRUE)
            {
                u1Val = (UINT1) (u1Val | RST_SET_CONFIG_TOPOCH_ACK_FLAG);
            }

            AST_PUT_1BYTE (pu1BpduEnd, u1Val);

            RstFillBpdu (pPerStPortInfo, &pu1BpduEnd);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        case AST_BPDU_TYPE_RST:

            u1Version = (UINT1) AST_VERSION_2;
            u1BpduType = (UINT1) AST_BPDU_TYPE_RST;

            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            u1Val = (UINT1) (u1Val | RST_SET_RST_TOPOCH_ACK_FLAG);

#ifdef NPAPI_WANTED
            if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
            {
                /* Inferior info should have Dssig, Learning flag
                 * set to make peer port move to discarding state */
                u1Val = (UINT1) (u1Val | RST_SET_FORWARDING_FLAG);
                u1Val = (UINT1) (u1Val | RST_SET_LEARNING_FLAG);
                u1Val = (UINT1) (u1Val | RST_SET_DESG_PROLE_FLAG);
            }
            else
#endif
            {
                if (pPerStRstPortInfo->bAgree == RST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_AGREEMENT_FLAG);
                    *pu1Flag = (UINT1) RST_SET_AGREEMENT_FLAG;
                }

                if (pPerStRstPortInfo->bForwarding == RST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_FORWARDING_FLAG);
                }
                if (pPerStRstPortInfo->bLearning == RST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_LEARNING_FLAG);
                }

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DESIGNATED)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_DESG_PROLE_FLAG);
                }
                else if (pPerStPortInfo->u1PortRole ==
                         (UINT1) AST_PORT_ROLE_ROOT)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_ROOT_PROLE_FLAG);
                }
                else if ((pPerStPortInfo->u1PortRole ==
                          (UINT1) AST_PORT_ROLE_ALTERNATE) ||
                         (pPerStPortInfo->u1PortRole ==
                          (UINT1) AST_PORT_ROLE_BACKUP))
                {
                    u1Val = (UINT1) (u1Val | RST_SET_ALTBACK_PROLE_FLAG);
                }
                else
                {
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                             "TXSM: Impossible Port Role while Transmitting BPDU\n");
                    return RST_FAILURE;
                }

                if (pPerStRstPortInfo->bProposing == RST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_PROPOSAL_FLAG);
                    *pu1Flag = (UINT1) RST_SET_PROPOSAL_FLAG;
                }

                if (pPerStRstPortInfo->pTcWhileTmr != NULL)
                {
                    u1Val = (UINT1) (u1Val | RST_SET_TOPOCH_FLAG);
                }
            }

            AST_PUT_1BYTE (pu1BpduEnd, u1Val);

            RstFillBpdu (pPerStPortInfo, &pu1BpduEnd);

            u1Version1Length = (UINT1) AST_VERSION_1_LENGTH;
            AST_PUT_1BYTE (pu1BpduEnd, u1Version1Length);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        default:

            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Invalid Type of BPDU to be sent\n");
            return RST_FAILURE;
    }

    u4Length = *pu4DataLength;
    if (u4Length > RST_MAX_BPDU_SIZE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: Invalid DataLength!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: Invalid DataLength!\n");
        return RST_FAILURE;
    }
    /* Fill the next u4DataPaddLength as all zeros 0 to make PDU total
       of  RST_MAX_BPDU_SIZE */
    u4DataPaddLen = RST_MAX_BPDU_SIZE - u4Length;
    AST_MEMSET (pu1BpduEnd, AST_MEMSET_VAL, u4DataPaddLen);
    pu1BpduEnd = pu1BpduEnd + u4DataPaddLen;

    if (AST_COPY_OVER_CRU_BUF (pBuf, gAstGlobalInfo.gpu1AstBpdu,
                               AST_ENET_LLC_HEADER_SIZE, u4Length)
        == AST_CRU_FAILURE)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstFillBpdu                                          */
/*                                                                           */
/* Description        : This function is called by RstFormBpdu function in   */
/*                      order to fill in the contents of the BPDU in the CRU */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                      *pu1Bpdu - Pointer to the linear attay in which the  */
/*                                 contents of the BPDU are to be filled     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
VOID
RstFillBpdu (tAstPerStPortInfo * pPerStPortInfo, UINT1 **ppu1Bpdu)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT1              *pu1TmpBpdu = NULL;
    UINT4               u4Val = AST_INIT_VAL;
    UINT2               u2PortId = AST_INIT_VAL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT1               u1Val = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
#ifdef MSTP_WANTED
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
#endif /* MSTP_WANTED */

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pBrgInfo = AST_GET_BRGENTRY ();

    pPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (pPortEntry == NULL)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return;
    }

    pu1TmpBpdu = *ppu1Bpdu;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo,
                                              RST_DEFAULT_INSTANCE);
    if (pPerStPortInfo == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 "FAILURE:AST_GET_PERST_PORT_INFO is NULL !!!\n");
        return;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pRstPortInfo == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 "Failure:AST_GET_RST_PORT_INFO is NULL !!!\n");
        return;
    }
#ifdef NPAPI_WANTED
    if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
    {
        /* Transmitting self information which will automatically
         * be inferior */

        u2Val = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (pu1TmpBpdu, u2Val);
        AST_PUT_MAC_ADDR (pu1TmpBpdu, pBrgInfo->BridgeAddr);

        /* Root cost set to 0 */
        u4Val = 0;
        AST_PUT_4BYTE (pu1TmpBpdu, u4Val);

        u2Val = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (pu1TmpBpdu, u2Val);
        AST_PUT_MAC_ADDR (pu1TmpBpdu, pBrgInfo->BridgeAddr);

        u2PortId = u2PortId | pPerStPortInfo->u1PortPriority;
        u2PortId = (UINT2) (u2PortId << 8);

        if ((AST_IS_CUSTOMER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE)
            || (AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) ==
                RST_TRUE))
        {
            u2PortId =
                u2PortId | AST_GET_PROTOCOL_PORT (pPerStPortInfo->u2PortNo);
        }
        else
        {
            u2PortId = u2PortId | AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);
        }

        AST_PUT_2BYTE (pu1TmpBpdu, u2PortId);

        /* Total 2 Bytes of the timer were splitted into Seconds 
         * and Centi-Seconds and filled in the packet as follows.
         */
        /*****************************************
         |Seconds (8 bits)|Centi-Seconds (8 bits)|
         *****************************************/
        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val =
            (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.
                                        u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        *ppu1Bpdu = pu1TmpBpdu;
        return;
    }
#endif

    /* Transmitting the Designated Priority vector on all ports */

    u2Val = pPerStPortInfo->RootId.u2BrgPriority;
    AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

    AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->RootId.BridgeAddr);

    if ((pRstPortInfo->bAgree == RST_TRUE)
        && (pPortEntry->bDot1wEnabled == RST_TRUE)
        && (pRstPortInfo->u1SendConfRoot == RST_TRUE))
    {
        u4Val = pPerStPortInfo->u4RootCost;
    }
    else
    {
        u4Val = pPerStBrgInfo->u4RootCost;
    }
    AST_PUT_4BYTE (pu1TmpBpdu, u4Val);

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        if (pCommPortInfo->bSendRstp == RST_FALSE)
        {
            /* If a port is connected to a legacy STP bridge, the port's 
             * DesignatedPriorityVector will have it's own BridgeId in the 
             * RegionalRootId field - Sec 13.10 of 802.1s */
            u2Val = (AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT))->u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

            AST_PUT_MAC_ADDR (pu1TmpBpdu, pBrgInfo->BridgeAddr);
        }
        else
        {
            u2Val = pBrgInfo->MstBridgeEntry.RegionalRootId.u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

            AST_PUT_MAC_ADDR (pu1TmpBpdu,
                              pBrgInfo->MstBridgeEntry.RegionalRootId.
                              BridgeAddr);
        }
    }
    else
    {
#endif /* MSTP_WANTED */

        /*If the mode is 802.1W and the proposal has been agreed, port priority vector is transmitted on all ports. */
        if ((pRstPortInfo->bAgree == RST_TRUE)
            && (pPortEntry->bDot1wEnabled == RST_TRUE)
            && (pRstPortInfo->u1SendConfRoot == RST_TRUE))
        {
            u2Val = pPerStPortInfo->DesgBrgId.u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);
            AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->DesgBrgId.BridgeAddr);

        }
        else
        {
            /* The Bridge Id is the transmitting bridge id as in the Designated Priority
             * vector, even if the port is a Root port */
            u2Val = pPerStBrgInfo->u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);
            AST_PUT_MAC_ADDR (pu1TmpBpdu, pBrgInfo->BridgeAddr);
        }
#ifdef MSTP_WANTED
    }
#endif /* MSTP_WANTED */
    if ((pRstPortInfo->bAgree == RST_TRUE)
        && (pPortEntry->bDot1wEnabled == RST_TRUE)
        && (pRstPortInfo->u1SendConfRoot == RST_TRUE))
    {
        u2PortId = pPerStPortInfo->u2DesgPortId;

        AST_PUT_2BYTE (pu1TmpBpdu, u2PortId);
    }
    else
    {
        /* The Port Id is the transmitting port id as in the Designated Priority
         * vector, even if the port is a Root port */
        u2PortId = u2PortId | pPerStPortInfo->u1PortPriority;
        u2PortId = (UINT2) (u2PortId << 8);

        if ((AST_IS_CUSTOMER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE)
            || (AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) ==
                RST_TRUE))
        {
            u2PortId =
                u2PortId | AST_GET_PROTOCOL_PORT (pPerStPortInfo->u2PortNo);
        }
        else
        {
            u2PortId = u2PortId | AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);
        }

        AST_PUT_2BYTE (pu1TmpBpdu, u2PortId);
    }

    /* Transmitting the Designated Times on all ports */
    /* Total 2 Bytes of the timer were splitted into Seconds 
     * and Centi-Seconds and filled in the packet as follows.
     */
    /*****************************************
      |Seconds (8 bits)|Centi-Seconds (8 bits)|
     *****************************************/
    u1Val = (UINT1)
        AST_PROT_TO_BPDU_SEC (pPortEntry->DesgTimes.u2MsgAgeOrHopCount);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1)
        AST_PROT_TO_BPDU_CENTI_SEC (pPortEntry->DesgTimes.u2MsgAgeOrHopCount);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPortEntry->DesgTimes.u2MaxAge);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC (pPortEntry->DesgTimes.u2MaxAge);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPortEntry->DesgTimes.u2HelloTime);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1)
        AST_PROT_TO_BPDU_CENTI_SEC (pPortEntry->DesgTimes.u2HelloTime);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPortEntry->DesgTimes.u2ForwardDelay);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    u1Val = (UINT1)
        AST_PROT_TO_BPDU_CENTI_SEC (pPortEntry->DesgTimes.u2ForwardDelay);
    AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

    *ppu1Bpdu = pu1TmpBpdu;

    if (pPortEntry->bDot1wEnabled == RST_TRUE)
    {
        pRstPortInfo->u1SendConfRoot = RST_FALSE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : RstPortTxSmEventImpossible                           */
/*                                                                           */
/* Description        : This function is called on the occurence of an event */
/*                      that is not possible in the present state of the Port*/
/*                      Transmit State Machine.                              */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortTxSmEventImpossible (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId)
{
    AST_UNUSED (u2InstanceId);
    AST_UNUSED (pAstPortEntry);

    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "TXSM: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
             "TXSM: This is an IMPOSSIBLE EVENT in this State!\n");

    if (AstHandleImpossibleState (pAstPortEntry->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitPortTxStateMachine                            */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RstInitPortTxStateMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PTXSM_MAX_EVENTS][20] = {
        "BEGIN", "BEGIN_CLEARED", "NEWINFO_SET", "HELLOWHEN_EXP",
        "HOLDTMR_EXP", "SELECTED_SET", "PORT_ENABLED", "PORT_DISABLED"
    };

    UINT1               aau1SemState[RST_PTXSM_MAX_STATES][20] = {
        "TRANSMIT_INIT", "TRANSMIT_PERIODIC",
        "TRANSMIT_CONFIG", "TRANSMIT_TCN",
        "TRANSMIT_RSTP", "IDLE"
    };

    for (i4Index = 0; i4Index < RST_PTXSM_MAX_EVENTS; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemEvent[AST_TXSM][i4Index],
                    aau1SemEvent[i4Index]);
    }
    for (i4Index = 0; i4Index < RST_PTXSM_MAX_STATES; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemState[AST_TXSM][i4Index],
                    aau1SemState[i4Index]);
    }
    /* Port Transmit SEM */
    /* BEGIN Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = RstPortTxSmMakeTransmitInit;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN][RST_PTXSM_STATE_TRANSMIT_CONFIG].
        pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN][RST_PTXSM_STATE_TRANSMIT_TCN].
        pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN][RST_PTXSM_STATE_TRANSMIT_RSTP].
        pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN][RST_PTXSM_STATE_IDLE].pAction =
        RstPortTxSmMakeTransmitInit;

    /* BEGIN_CLEARED Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = RstPortTxSmCheckIdle;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_TRANSMIT_TCN].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_BEGIN_CLEARED]
        [RST_PTXSM_STATE_IDLE].pAction = NULL;

    /* NEWINFO_SET Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET][RST_PTXSM_STATE_TRANSMIT_TCN].
        pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_NEWINFO_SET][RST_PTXSM_STATE_IDLE].
        pAction = RstPortTxSmNewInfoSetIdle;

    /* HELLOWHEN_EXP Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP]
        [RST_PTXSM_STATE_TRANSMIT_TCN].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HELLOWHEN_EXP][RST_PTXSM_STATE_IDLE].
        pAction = RstPortTxSmMakeTransmitPeriodic;

    /* HOLDTMR_EXP Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP][RST_PTXSM_STATE_TRANSMIT_TCN].
        pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_HOLDTMR_EXP][RST_PTXSM_STATE_IDLE].
        pAction = RstPortTxSmHoldTmrExpIdle;

    /* SELECTED_SET Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET]
        [RST_PTXSM_STATE_TRANSMIT_TCN].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_SELECTED_SET][RST_PTXSM_STATE_IDLE].
        pAction = RstPortTxSmSelectedSetIdle;

    /* PORT_ENABLED Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = RstPortTxSmCheckIdle;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_TRANSMIT_TCN].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_ENABLED]
        [RST_PTXSM_STATE_IDLE].pAction = RstPortTxSmEventImpossible;

    /* PORT_DISABLED Event */
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_TRANSMIT_CONFIG].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_TRANSMIT_TCN].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_TRANSMIT_RSTP].pAction = RstPortTxSmEventImpossible;
    RST_PORT_TX_MACHINE[RST_PTXSM_EV_TX_DISABLED]
        [RST_PTXSM_STATE_IDLE].pAction = RstPortTxSmMakeTransmitInit;

    AST_DBG (AST_TXSM_DBG, "TXSM: Loaded TXSM SEM successfully\n");

    return;
}

/*****************************************************************************/
/* Function Name      : AstFillEnetHeader                                    */
/*                                                                           */
/* Description        : Forms the ethernet header and fills it in the packet */
/*                      CRU buffer passed in.                                */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the packet CRU buffer .            */
/*                      pAstPortEntry - Pointer to the port information.     */
/*                      pu4DataLength - Length of the packet before adding   */
/*                                      the ethernet header                  */
/*                                                                           */
/* Output(s)          : pu4DataLength - Length of the packet after  adding   */
/*                                      the ethernet header                  */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS (or) RST_FAILURE                         */
/*****************************************************************************/
INT4
AstFillEnetHeader (tAstBufChainHeader * pBpdu, UINT4 *pu4DataLength,
                   tAstPortEntry * pPortEntry)
{
    tAstCfaIfInfo       CfaIfInfo;
    UINT1               au1Llc[AST_LLC_HEADER_SIZE] = { 0x42, 0x42, 0x03 };
    UINT1               au1EnetLLCHeader[AST_ENET_LLC_HEADER_SIZE];
    INT4                i4Index = AST_INIT_VAL;
    UINT2               u2TypeLen;
    UINT4               u4DataLength = *pu4DataLength;
    tAstMacAddr        *pSrcMacAddr = NULL;

    AST_MEMSET (au1EnetLLCHeader, AST_MEMSET_VAL, sizeof (au1EnetLLCHeader));

    /* The destination address will be stored in the AstPortEntry depending
     * upon the Port Type. Hence copying the same here
     * */
    AST_MEMCPY (&au1EnetLLCHeader[0], &(pPortEntry->au1DestMACAddr),
                AST_MAC_ADDR_SIZE);

    i4Index = AST_MAC_ADDR_SIZE;

    /* For SVLAN context, get port mac address from CFA and fill in BPDU and
     * send*/
    if ((AST_COMP_TYPE () != AST_PB_C_VLAN) &&
        (AST_IS_I_COMPONENT () != RST_TRUE))
    {
        if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pPortEntry), &CfaIfInfo) !=
            CFA_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: AstCfaGetIfInfo call FAILED!\n");
            return RST_FAILURE;
        }

        /*Incase of SVLAN context, the SrcMac address of a BPDU will be the
         * Port Mac Address*/
        pSrcMacAddr = &CfaIfInfo.au1MacAddr;
    }
    else
    {
        /*In case of CVLAN context, the SrcMac Address of a BPDU will be always
         * the CEP mac address, which is nothing but bridge mac address of
         * CVLAN component*/

        /* In case of I-Component also, the address should be the I-Component
         * MAC Address 
         * */
        pSrcMacAddr = &(AST_GET_BRGENTRY ())->BridgeAddr;
    }

    MEMCPY (&au1EnetLLCHeader[i4Index], pSrcMacAddr, AST_MAC_ADDR_SIZE);

    i4Index += AST_MAC_ADDR_SIZE;

    /* The length in the type/length field of the mac frame, is
     * size of the mac client data following the type/length field
     * (ie) BPDU length + LLC header size */

    u2TypeLen = (UINT2) (u4DataLength + AST_LLC_HEADER_SIZE);
    u2TypeLen = (UINT2) (OSIX_HTONS (u2TypeLen));
    MEMCPY (&au1EnetLLCHeader[i4Index], &u2TypeLen, AST_TYPE_LEN_SIZE);
    i4Index += AST_TYPE_LEN_SIZE;

    MEMCPY (&au1EnetLLCHeader[i4Index], au1Llc, AST_LLC_HEADER_SIZE);
    i4Index += AST_LLC_HEADER_SIZE;

    /* Incrementing the total length of the data in pBuf */
    u4DataLength = u4DataLength + AST_ENET_LLC_HEADER_SIZE;
    *pu4DataLength = u4DataLength;

    if (AST_COPY_OVER_CRU_BUF (pBpdu, au1EnetLLCHeader, 0,
                               AST_ENET_LLC_HEADER_SIZE) == AST_CRU_FAILURE)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/* End of File */
