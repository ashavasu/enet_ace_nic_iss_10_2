
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbrdsb.c,v 1.5 2010/08/25 13:59:03 prabuc Exp $
 *
 * Description: This file contains the stub functions for AST Provider 
 *              Bridge High availability feature. This file will be
 *              used if Ast Pb red is not requried.
 *   
 *
 *******************************************************************/
#include "asthdrs.h"
#include "rmgr.h"
#include "cli.h"

/*****************************************************************************/
/* Function Name      : AstRedPbCVlanInit                                    */
/*                                                                           */
/* Description        : Allocate Memory pool for Per Port Inst strucutre for */
/*                      C-VLAN components.                                   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCVlanInit (UINT2 u2CepPortNo)
{
    UNUSED_PARAM (u2CepPortNo);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCVlanPortTblDeInit                           */
/*                                                                           */
/* Description        : Releases the C-VLAN red port table mempool.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gpAstRedContextInfo                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*****************************************************************************/
INT4
AstRedPbCVlanPortTblDeInit (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbRelCvlanRedCompMem                           */
/*                                                                           */
/* Description        : Free the Red C-VLAN component memory.                */
/*                      CAUTION: This funcion should be called only in       */
/*                               S-VLAN component.                           */
/* Input(s)           : u2Port - port no in S-VLAN component.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo,                                   */
/*                      gpAstRedContextInfo                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbRelCvlanRedCompMem (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbSelectCVlanContext                           */
/* Description        : This function selects the Red C-VLAN component.      */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbSelectCVlanContext (UINT2 u2ParentPort)
{
    UNUSED_PARAM (u2ParentPort);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbRestoreContext                               */
/* Description        : This function restores the parent Red Context.       */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbRestoreContext (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbIncrCvlanPortTableSize                       */
/*                                                                           */
/* Description        : This Function the port array in the Red-CVlan comp.  */
/*                      In the process, it creates a new mempool with        */
/*                      increased Block size and copies the info from        */
/*                      old table to new Red C-VLAN port table.              */
/*                                                                           */
/*                                                                           */
/* Input (s)          :None                                                  */
/*                                                                           */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedPbIncrCvlanPortTableSize (VOID)
{
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCreateCvlanPort                              */
/*                                                                           */
/* Description        : Create PB-RED port info for the C-VLAN component.    */
/*                      Warning: This function has to be called only after   */
/*                               the selection Red C-VLAN component.         */
/*                                                                           */
/* Input(s)           : u2LocalPort - Protocol port number for the given     */
/*                                    port.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCreateCvlanPort (UINT2 u2LocalPort)
{
    UNUSED_PARAM (u2LocalPort);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbDeleteCvlanPort                              */
/*                                                                           */
/* Description        : Delete PB-RED port info from the C-VLAN component.   */
/*                      Warning: This function has to be called only after   */
/*                               the selection Red C-VLAN component.         */
/*                                                                           */
/* Input(s)           : u2Port - Protocol port number for the given port.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbDeleteCvlanPort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbSelCvlanAndGetLocalPort                      */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - Selects the C-VLAN component based on given   */
/*                             CEP port number.                              */
/*                           - Get the local port for the given C-VLAN port  */
/*                             whose protocol port number is given.          */
/*                                                                           */
/* Input(s)           : u2CepPortNo    - CEP port number of the C-VLAN       */
/*                                       component.                          */
/*                      u2ProtocolPort - Protocol port number of the C-VLAN  */
/*                                       port.                               */
/*                                                                           */
/* Output(s)          : pu2LocalPort - Local port number of the C-VLAN port  */
/*                                     identified by the given protocol port.*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbSelCvlanAndGetLocalPort (UINT2 u2CepPortNo,
                                 UINT2 u2ProtocolPort, UINT2 *pu2LocalPort)
{
    UNUSED_PARAM (u2CepPortNo);
    UNUSED_PARAM (u2ProtocolPort);
    UNUSED_PARAM (pu2LocalPort);

    /* Based on the output of this function, some other instructions will
     * get executed. So it is safe to return failure here. */
    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstRedPbHandleBulkUptOnCep                           */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - Send the bulk update for the given C-VLAN     */
/*                             component.                                    */
/*                           - Get the local port for the given C-VLAN port  */
/*                             whose protocol port number is given.          */
/*                                                                           */
/* Input(s)           : u2CepPortNo    - CEP port number of the C-VLAN       */
/*                                       component.                          */
/*                                                                           */
/* In and Out Params  : ppBuf - Pointer to buffer where the information      */
/*                              needs to be put. If this is null, then a     */
/*                              new buffer will be allocated.                */
/*                      pu4Offset - Offset the above buffer from where the   */
/*                              next write operation needs to be started.    */
/*                      pu4BulkUpdPortCnt - Number of port sync up messages  */
/*                                          that can be sent.                */
/*                                                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo.u4BulkUpdNextPort                  */
/*                      gAstRedGlobalInfo.u4BulkUpdNextCvlanPort             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo.u4BulkUpdNextPort                  */
/*                      gAstRedGlobalInfo.u4BulkUpdNextCvlanPort             */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbHandleBulkUptOnCep (UINT2 u2CepPortNo, VOID **ppBuf, UINT4 *pu4Offset,
                            UINT4 *pu4BulkUpdPortCnt)
{
    UNUSED_PARAM (u2CepPortNo);
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (pu4Offset);
    UNUSED_PARAM (pu4BulkUpdPortCnt);

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbApplyCvlanTimers                             */
/*                                                                           */
/*                      remaining time.                                      */
/*                       Remaining time                                      */
/*                        = [Expected Expiry time - Current time]            */
/*                                                                           */
/* Input(s)           : pParentCepPortInfo - CEP port entry in the S-VLAN    */
/*                                           context.                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbApplyCvlanTimers (tAstPortEntry * pParentPortEntry)
{
    UNUSED_PARAM (pParentPortEntry);

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCvlanHwAudit                                 */
/*                                                                           */
/* Description        : This function does the hw audit for the given        */
/*                      C-VLAN component.                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context id.                    */
/*                      u2PortIndex - Cep local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCvlanHwAudit (UINT4 u4ContextId, UINT2 u2PortIndex)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2PortIndex);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedClearAllCvlanSyncUpData                        */
/* Description        : Clears stale Data On all Ports of all C-VLAN         */
/*                      components.                                          */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearAllCvlanSyncUpData (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbUpdtCvlanPortStates                          */
/*                                                                           */
/* Description        : Update all the synced Up Port state for all ports    */
/*                      in the given C-VLAN component.                       */
/*                                                                           */
/* Input(s)           : u2CepPortNo - CEP local port number                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbUpdtCvlanPortStates (UINT2 u2CepPortNo)
{
    UNUSED_PARAM (u2CepPortNo);
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncUpAllCvlanTmrData                      */
/*                                                                           */
/* Description        : Dumped Synced Up C-VLAN Tmr datas on all Standby     */
/*                      C-VLAN components for the given context.             */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handle                               */
/*                      u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : pu4PagingStatus - Cli Paging Status                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpAllCvlanTmrData (VOID)
{

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpAllCvlanSyncUpOutputs                      */
/*                                                                           */
/* Description        : Dumped Synced Up outputs on all Standby C-VLAN       */
/*                      components for the given context.                    */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handle                               */
/*                                                                           */
/* Output(s)          : pu4PagingStatus - Cli Paging Status                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpAllCvlanSyncUpOutputs (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpAllCvlanSyncUpPdus                         */
/*                                                                           */
/* Description        : Dumped Synced Up Pdus on all Standby C-VLAN          */
/*                      components for the given context.                    */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handle                               */
/*                                                                           */
/* Output(s)          : pu4PagingStatus - Cli Paging Status                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpAllCvlanSyncUpPdus (VOID)
{

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedHwPepCreateNotify                              */
/*                                                                           */
/* Description        : This function is used to post message, if pep is     */
/*                      created in hw.                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      SVlanId   - Service Vlan Id        .                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstRedHwPepCreateNotify (UINT4 u4IfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleHwPepCreate                              */
/*                                                                           */
/* Description        : This function is called from the Message processing  */
/*                      thread to Handle PEP Port Creation msg in Hw         */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstRedHandleHwPepCreate (tAstMsgNode * pMsgNode)
{
    UNUSED_PARAM (pMsgNode);

    return RST_SUCCESS;
}
