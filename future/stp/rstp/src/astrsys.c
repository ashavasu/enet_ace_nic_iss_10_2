/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrsys.c,v 1.151 2017/12/29 09:31:23 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              RSTP Module.
 *
 *******************************************************************/

#ifdef RSTP_WANTED
#define _ASTRSYS_C

#include "asthdrs.h"

#ifdef NPAPI_WANTED
INT4                gi4AstInitProcess = 0;
#endif /* NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : RstModuleInit                                        */
/*                                                                           */
/* Description        : This function allocates memory pools in case of SI,  */
/*                      allocates port table and perst-info table and        */
/*                      initializes the module. In case of customer/provider */
/*                      bridges customer RSTP will be started. In case of    */
/*                      provider core/edge bridges S-VLAN spanning tree will */
/*                      started.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstModuleInit (VOID)
{
    INT4                i4Mode = AST_INIT_VAL;

    if (AST_IS_RST_STARTED ())
    {
        return RST_SUCCESS;
    }

    if (AstDeriveOperatingMode (&i4Mode) == RST_FAILURE)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Bridge Mode retrieving from l2iwf Failed !!!!\n  quitting...\n");
        (VOID) RstModuleShutdown ();
        return RST_FAILURE;
    }
    /* The value of i4Mode will be used during ASTP module initialisation only.
     * Hence ignoring the same 
     * */

    /*For SVLAN component, the Max Number of Ports Per Context is the Port
       Table size of the context */
    AST_MAX_NUM_PORTS = AST_MAX_PORTS_PER_CONTEXT;

    if (AstAllocPortAndPerStInfoBlock (AST_RSTP_MODULE) != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                 AST_ALL_FAILURE_TRC,
                 "SYS: Mem alloc failed for PortTable/PerStInfo Table "
                 "during Rstp Initialization. \r\n");
        (VOID) RstModuleShutdown ();

        return RST_FAILURE;
    }

    if (RstComponentInit () != RST_SUCCESS)
    {

        AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                 AST_ALL_FAILURE_TRC,
                 "SYS: Rstp Initialization in SVLAN component" "Failed \r\n");
        (VOID) RstModuleShutdown ();
        return RST_FAILURE;
    }

    /* Make S-VLAN component status as enabled. This makes the
     * RstModuleEnable to enable the S-VLAN spanning tree. */
    AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

    /* When system comes up as a 1ad bridge, Ast will come up before
     * vlan and hence the port types in l2iwf will be customer bridge
     * port. So if AstPbCheckAllPortTypes is called, it will return 
     * failure. So don't call that if vlan is in shutdown state. */
    if (AstVlanGetStartedStatus (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        /* In case of 1ad bridge, check for port types. */
        if (AST_IS_1AD_BRIDGE () == RST_TRUE
            || AST_IS_1AH_BRIDGE () == RST_TRUE)
        {
            if (AstPbCheckAllPortTypes () == RST_FAILURE)
            {
                AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                         AST_ALL_FAILURE_TRC,
                         "SYS: Rstp Initialization in PbCheckAll Port Types"
                         "Failed \r\n");
                (VOID) RstModuleShutdown ();
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return RST_FAILURE;
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstComponentInit                                     */
/*                                                                           */
/* Description        : This function initialises data structures used by    */
/*                      RSTP module                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstComponentInit (VOID)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PortsCreated = AST_INIT_VAL;
    UINT2               u2PrevPort = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    UINT1               u1PortType = AST_INVALID_PROVIDER_PORT;

    /* Assumption:
     * 1) The appropriate context is selected before calling this function */

    AST_FORCE_VERSION = (UINT1) AST_VERSION_2;

    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Initializing RSTP Module ... \n");
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Initializing RSTP Module ... \n");

    /* Initialise the State Machines */
    RstInitStateMachines ();

    /* Creating PerStInfo for Default instance */
    if (AstCreateSpanningTreeInst (RST_DEFAULT_INSTANCE, &pPerStInfo)
        == RST_FAILURE)
    {
        AST_MODULE_STATUS_SET (RST_DISABLED);
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        return RST_FAILURE;
    }

    if (AST_COMP_TYPE () != AST_PB_C_VLAN)
    {
        AstL2IwfVlanToInstMappingInit (AST_CURR_CONTEXT_ID ());
    }

    AstCreateSpanningTreeInstanceInL2Iwf (RST_DEFAULT_INSTANCE);

    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);
    pBrgInfo = AST_GET_BRGENTRY ();

    RstInitGlobalBrgInfo ();

    /* Since CVLAN component Mac address is the CEP port Mac Address, the
     * Original Switch Mac address should not be used for CVLAN component*/
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        AstIssGetContextMacAddress (AST_CURR_CONTEXT_ID (),
                                    pBrgInfo->BridgeAddr);
    }
    else
    {
        /*For CVLAN component, the Max Number of Services that can be given to
           a customer will be the port table size of the CVLAN context */
        AstPbCVlanFillBrgMacAddr (pBrgInfo->BridgeAddr);
    }

    RstInitPerStBrgInfo (pPerStBrgInfo);

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = AST_INIT_VAL;

        /* RstCVlanComponentInit will take of creating PEP and CEP in CVLAN
         * component*/

        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {
            while (AstL2IwfGetNextValidPortForContext (AST_CURR_CONTEXT_ID (),
                                                       u2PrevPort, &u2PortNum,
                                                       &u4IfIndex) ==
                   L2IWF_SUCCESS)
            {
                if (AstL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
                {
                    u2PrevPort = u2PortNum;
                    continue;
                }

                if (AstIsExtInterface (u2PortNum) == AST_TRUE)
                {
                    u2PrevPort = u2PortNum;
                    continue;
                }

                AstGetPbPortType (u4IfIndex, &u1PortType);

                if (u1PortType == AST_PROVIDER_INSTANCE_PORT)
                {
                    u2PrevPort = u2PortNum;
                    continue;
                }

                /* Port creation should be blocked if the number of ports created has reached
                 * max ports in system. */

                AstGetNumOfPortsCreated (&u4PortsCreated);

                if (u4PortsCreated >= AST_SIZING_PORT_COUNT)
                {
                    break;
                }

                if (RstCreatePort (u4IfIndex, u2PortNum) != MST_SUCCESS)
                {
                    (VOID) RstModuleShutdown ();
                    return RST_FAILURE;
                }

#ifdef L2RED_WANTED
                if (AstRedCreatePortAndPerPortInst (u2PortNum) == RST_FAILURE)
                {
                    (VOID) RstModuleShutdown ();
                    return RST_FAILURE;
                }
#endif

                pPortInfo = AST_GET_PORTENTRY (u2PortNum);

                /* Oper status returned here is sum of
                 * all lower layer oper status */
                AstGetPortOperStatusFromL2Iwf (STP_MODULE,
                                               u2PortNum, &u1OperStatus);

                /* The Port path cost is updated if the link is already UP 
                 * when the Spanning Tree module is STARTED
                 */
                if ((AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
                     == RST_FALSE) &&
                    (pPortInfo->bPathCostInitialized == RST_FALSE) &&
                    (u1OperStatus == AST_UP))
                {
                    pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
                    pPortInfo->bPathCostInitialized = RST_TRUE;
                }

                /* Derive the MAC Address type for the specific port */
                AstDeriveMacAddrFromPortType (u2PortNum);
                u2PrevPort = u2PortNum;
            }
        }
    }

    AST_TRAP_TYPE = AST_TRAP_ABNORMAL | AST_TRAP_NORMAL;

    /* Set the flush interval count to default */
    AST_FLUSH_INTERVAL = AST_INIT_VAL;

    AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

    AST_ADMIN_STATUS = (UINT1) RST_DISABLED;

    AST_SYSTEM_CONTROL_SET ((UINT1) RST_START);

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "Value of Force Version is %u\n", AST_FORCE_VERSION);
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Initialised RSTP Module SUCCESSFULLY... \n");
    AST_DBG (AST_INIT_SHUT_DBG,
             "SYS: Initialized RSTP Module SUCCESSFULLY... \n");
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstModuleShutDown                                    */
/*                                                                           */
/* Description        : This function gets the Timer submodule               */
/*                      deinitialization, deletes the memory pools.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1AstSystemControl                                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
RstModuleShutdown (VOID)
{
    tRstPortInfo       *pRstPortInfo = NULL;
    UINT2               u2Port = 0;
    if (!AST_IS_RST_STARTED ())
    {
        return RST_SUCCESS;
    }

    AstPbAllCVlanCompShutdown ();

    RstComponentShutdown ();

    /* Set module status as disabled. */
    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = RST_DISABLED;

    AstReleasePortAndPerStInfoBlock ();

    AST_BRIDGE_MODE () = AST_INVALID_BRIDGE_MODE;

    for (u2Port = 1; u2Port <= AST_MAX_NUM_PORTS; u2Port++)
    {
        AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_STP,
                                                OSIX_DISABLED);
    }

    if (AST_COUNT_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList) != 0)
    {
        while ((pRstPortInfo = AST_GET_FIRST (&(AST_CURR_CONTEXT_INFO ())->
                                              PortList)) != NULL)
        {
            AST_DELETE_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                                 pRstPortInfo);
            AST_RELEASE_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);
        }

    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstComponentShutdown                                 */
/*                                                                           */
/* Description        : This function gets the Timer submodule               */
/*                      deinitialization of CVLAN and SVLAN component.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1AstSystemControl                                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
RstComponentShutdown (VOID)
{
    INT4                i4RetVal = RST_SUCCESS;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    AST_DBG (AST_INIT_SHUT_DBG,
             "SYS: Performing Shutdown of RSTP Module ... \n");
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Performing Shutdown of RSTP Module ... \n");

    if (AST_MODULE_STATUS == (UINT1) RST_ENABLED)
    {
        (VOID) RstComponentDisable ();
    }

    u2PortNum = 1;

    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }
        if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
        {
            AstRedPbDeleteCvlanPort (u2PortNum);
        }
        else
        {
#ifdef L2RED_WANTED
            AstRedDeletePortAndPerPortInst (u2PortNum);
#endif
        }

        AstReleasePortMemBlocks (pPortInfo);

    }

    if ((pPerStInfo = AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE)) != NULL)
    {
        AstDeleteMstInstanceFromL2Iwf (RST_DEFAULT_INSTANCE);

        AstDeleteMstInstance (RST_DEFAULT_INSTANCE, pPerStInfo);

    }

    pBrgInfo = AST_GET_BRGENTRY ();
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->bBridgeClearStats = RST_DISABLED;
    pBrgInfo->u4RstOptStatus = RST_ENABLED;

    AST_SYSTEM_CONTROL_SET ((UINT1) RST_SHUTDOWN);

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: RSTP Module Shutdown SUCCESSFULLY\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: RSTP Module Shutdown SUCCESSFULLY\n");

    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : RstModuleEnable                                      */
/*                                                                           */
/* Description        : Called by management when RSTP is enabled. Enables   */
/*                      CVLAN and SVLAN modules in PEB. In other Bridge Modes*/
/*                      it enables the SVLAN module only                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.apPerStInfo[]                         */
/*                      gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstModuleEnable (VOID)
{
    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED)
    {
        return RST_SUCCESS;
    }

    AST_ADMIN_STATUS = RST_ENABLED;

    if (AST_IS_1AD_BRIDGE () == RST_TRUE)
    {
        /* The S-VLAN component is configured as enabled, then enable the S-VLAN 
         * component spanning tree. */
        if (AST_SVLAN_ADMIN_STATUS () == RST_ENABLED)
        {
            if (RstComponentEnable () != RST_SUCCESS)
            {
                AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                         AST_ALL_FAILURE_DBG,
                         "SYS: SVLAN RSTP Module Enable is not proper..."
                         "Failures Occurred !!!\n"
                         "          Still Continuing ...\n");
                AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                         "SYS: SVLAN RSTP Module Enable is not proper..."
                         "Failures Occurred !!!\n"
                         "          Still Continuing ...\n");

                return RST_FAILURE;
            }
        }
    }
    else
    {
        /* In case of customer / provider bridge just enable the module. */
        if (RstComponentEnable () != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_DBG, "SYS: RSTP Module Enable is not "
                     "proper...Failures Occurred !!!\n"
                     "          Still Continuing ...\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: RSTP Module Enable is not proper...Failures "
                     "Occurred !!!\n" "          Still Continuing ...\n");

            return RST_FAILURE;
        }
    }

    /*Enable all the CVLAN component in the Bridge */
    if (AstPbAllCVlanCompEnable () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        return RST_FAILURE;
    }

    AstRstInstTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                    RST_ENABLED);

    if (AST_NODE_STATUS () != RED_AST_IDLE)
    {
        gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = RST_ENABLED;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstComponentEnable                                   */
/*                                                                           */
/* Description        : Called by management when RSTP is enabled.           */
/*                      This function initialises all the data structures    */
/*                      used by RSTP module                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.apPerStInfo[]                         */
/*                      gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstComponentEnable (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Enabling RSTP Module.\n");
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Enabling RSTP Module.\n");

    if (AST_MODULE_STATUS == (UINT1) RST_ENABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Enabled...Quitting without processing.\n");
        return RST_SUCCESS;
    }

    AST_GET_SYSTEMACTION = RST_ENABLED;

    /* In Active & Standby node RSTP can be directly enabled.
     * When NodeStatus is in IDLE State,RSTP should not be enabled.
     * RSTP can be enabled only when NodeStatus is in ACTIVE or STANDBY State.
     */

    if (AST_NODE_STATUS () <= RED_AST_IDLE)
    {
        return RST_SUCCESS;
    }
    /* Setting the Module Status as ENABLED */
    AST_MODULE_STATUS_SET ((UINT1) RST_ENABLED);

    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        AstIssGetContextMacAddress (AST_CURR_CONTEXT_ID (),
                                    pBrgInfo->BridgeAddr);
    }
    else
    {
        AstPbCVlanFillBrgMacAddr (pBrgInfo->BridgeAddr);
    }

    RstReInitRstpInfo ();

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        AstMiRstpNpInitHw ();
    }
    gi4AstInitProcess = 1;
#endif

    if (AstAssertBegin () != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");

#ifdef NPAPI_WANTED
        gi4AstInitProcess = 0;
#endif
        return RST_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        AstMiNpDeleteAllFdbEntries ();
    }
    gi4AstInitProcess = 0;
#endif

    /* Incrementing the RSTP Up Count */
    AST_INCR_ASTP_UP_COUNT ();

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "Value of Force Version is %u\n", AST_FORCE_VERSION);
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: RSTP Module Enabled.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: RSTP Module Enabled.\n");

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstModuleDisable                                     */
/*                                                                           */
/* Description        : This function disables the RSTP functionality in both*/
/*                      CVLAN and SVLAN components of the Provider Bridge    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstModuleDisable (VOID)
{
    INT4                i4RetVal = RST_SUCCESS;
#ifdef L2RED_WANTED
    if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] ==
         (UINT1) RST_DISABLED) && (AST_ADMIN_STATUS == (UINT1) RST_DISABLED))
#else
    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == (UINT1) RST_DISABLED)
#endif
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without "
                 "processing.\n");
        return RST_SUCCESS;
    }

    AstRstInstTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                    RST_DISABLED);

    if (AstPbAllCVlanCompDisable () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Disable is not proper...Failures"
                 "Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Disable is not proper...Failures"
                 "Occurred !!!\n");

        i4RetVal = RST_FAILURE;
    }

    /*This function is called for the SVLAN component Disable */
    if (RstComponentDisable () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN RSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN RSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");

        i4RetVal = RST_FAILURE;
    }

    AST_ADMIN_STATUS = (UINT1) RST_DISABLED;

    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = RST_DISABLED;

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstComponentDisable                                  */
/*                                                                           */
/* Description        : This function disables the RSTP functionality. It    */
/*                      passess 'port-disabled' as events to the state       */
/*                      machines.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstComponentDisable (VOID)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1PrevStatus;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Disabling RSTP Module...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Disabling RSTP Module...\n");

    if (AST_MODULE_STATUS == (UINT1) RST_DISABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without "
                 "processing.\n");
        return RST_SUCCESS;
    }

    pPerStInfo = AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE);
    if (pPerStInfo == NULL)
    {
        i4RetVal = RST_SUCCESS;
    }

    AST_GET_SYSTEMACTION = RST_DISABLED;
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if ((pPortInfo) == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                  RST_DEFAULT_INSTANCE);

        pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

        /* Revert to the long ageout time for this port */
        if (pCommPortInfo->pRapidAgeDurtnTmr != NULL)
        {
            AstVlanResetShortAgeoutTime (pPortInfo);
        }

        if (RstStopAllRunningTimers (pPerStInfo, pPortInfo, pPerStPortInfo) !=
            RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Unable to stop the running timers !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Unable to stop the running timers !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            i4RetVal = RST_FAILURE;
        }

        if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
        {
            pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

            if (pPortInfo->bLoopInconsistent == AST_TRUE)
            {
                pPortInfo->bLoopInconsistent = AST_FALSE;
            }
            pPortInfo->u1RecScenario = AST_INIT_VAL;
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if (pRstPortInfo->bPortEnabled == RST_TRUE)
            {
                u1PrevStatus =
                    AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE,
                                                  u2PortNum);

                /* Indicate to L2Iwf, Hardware and GARP only if there is change in 
                 * port state */
                if (u1PrevStatus == AST_PORT_STATE_FORWARDING)
                {
                    continue;
                }

                /* Set port state as FORWARDING in the 
                 * common database and indicate to Hw and Garp.
                 */

                AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE,
                                            u2PortNum,
                                            AST_PORT_STATE_FORWARDING);

                AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNum) =
                    AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_RST_ENABLED () &&
                    (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                {
                    AstMiRstpNpSetPortState (u2PortNum,
                                             AST_PORT_STATE_FORWARDING);
                }
#endif /* NPAPI_WANTED */
            }
        }

        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            AstRedClearPduOnActive (u2PortNum);
        }

    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pPerStBrgInfo->u1ProleSelSmState = 0;

    /* Incrementing the RSTP Down Count */
    AST_INCR_ASTP_DOWN_COUNT ();

    /* Setting the Module Status as DISABLED */
    AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: RSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: RSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");
    }
    else
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: RSTP Module Disabled.\n");
        AST_DBG (AST_INIT_SHUT_DBG, "SYS: RSTP Module Disabled.\n");
    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : RstCreatePort                                        */
/*                                                                           */
/* Description        : Creates Port by allocating memory for the port entry */
/*                      and initialises the port state machines. Makes the   */
/*                      Port operational in RSTP if the operational status   */
/*                      passed indicates up status.                          */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port that is to be    */
/*                                  created.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstCfaIfInfo       CfaIfInfo;
    UINT1               u1IfType = CFA_ENET;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    AST_MEMSET (au1IfName, AST_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

#ifdef NPAPI_WANTED
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1L2IwfPortState = AST_PORT_STATE_DISCARDING;
#endif

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Creating Port: %u ... \n", u4IfIndex);
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Creating Port: %u ... \n", u4IfIndex);

    if (AstSispIsSispEnabledOnPort (u4IfIndex) == VCM_TRUE)
    {
        /* SISP is enabled in a port mapped to this context. Hence,
         * RSTP cannot be started */
        AST_DBG_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                      "SYS: RSTP cannot be enabled when SISP is enabled in a port %d mapped to this context",
                      u4IfIndex);

        return RST_FAILURE;
    }

    /* This function should be called only in SVLAN component Context.
     * For Creating CVLAN Port, a Separate function is  used.*/
    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: CreatePort: Port Already Created\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    /* Check whether the interface type is valid. Currently 
     * ethernet or PPP or ATMVC are valid interfaces.
     */
    if (AstCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get CFA Information for Port %u\n",
                      u4IfIndex);
        return RST_SUCCESS;
    }

    u1IfType = CfaIfInfo.u1IfType;

    if ((AST_INTF_TYPE_VALID (u1IfType)) == RST_FALSE)
    {
        return RST_SUCCESS;
    }

    if (CfaIfInfo.u1BrgPortType == AST_PROVIDER_INSTANCE_PORT)
    {
        return RST_SUCCESS;
    }

    if (AST_ALLOC_PORT_INFO_MEM_BLOCK (pPortInfo) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Info Memory Block Allocation failed !!!\n",
                      u4IfIndex);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Info Memory Block Allocation failed !!!\n",
                      u4IfIndex);
        return RST_FAILURE;
    }

    AST_MEMSET (pPortInfo, AST_INIT_VAL, sizeof (tAstPortEntry));
    AST_GET_PORTENTRY (u2PortNum) = pPortInfo;

    pPortInfo->u4ContextId = AST_CURR_CONTEXT_ID ();
    pPortInfo->u4IfIndex = u4IfIndex;
    pPortInfo->u4PhyPortIndex = u4IfIndex;
    CfaCliConfGetIfName (u4IfIndex, piIfName);
#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    MEMSET (pPortInfo->au1StrIfIndex, 0, CFA_MAX_PORT_NAME_LENGTH);
    SNPRINTF ((CHR1 *) pPortInfo->au1StrIfIndex, CFA_MAX_PORT_NAME_LENGTH, "%s",
              piIfName);
#endif
    MEMCPY (pPortInfo->au1PortMACAddr, CfaIfInfo.au1MacAddr, AST_MAC_ADDR_SIZE);
    /* Add this node to the IfIndex RBTree and to the PerContext DLL */
    AstAddToIfIndexTable (pPortInfo);

    RstInitGlobalPortInfo (u2PortNum, pPortInfo);
    /* Fix For SilverCrek test: Get on Port that is down will
     * return value as zero that is out of range */
    pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;
    pPortInfo->u2ProtocolPort = u2PortNum;
    /* Path cost will be calculated from speed when the link is up.
     * Until then mark the path cost as uninitialised */
    pPortInfo->bPathCostInitialized = RST_FALSE;
    pPortInfo->bPortClearStats = RST_FALSE;
    AstUpdatePortType (pPortInfo, CfaIfInfo.u1BrgPortType);
    AST_PB_PORT_INFO (pPortInfo) = NULL;

    if (AST_ALLOC_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo) == NULL)
    {

        (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);
        AST_GET_PORTENTRY (u2PortNum) = NULL;

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Per SpTree Port Info Memory Block Allocation failed !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Per SpTree Port Info Memory Block Allocation failed !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    AST_MEMSET (pPerStPortInfo, AST_INIT_VAL, sizeof (tAstPerStPortInfo));
    AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE) = pPerStPortInfo;

#ifdef NPAPI_WANTED
    if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        if ((RstpFsMiRstNpGetPortState
             (AST_CURR_CONTEXT_ID (), u4IfIndex, &u1PortState)) == FNP_SUCCESS)
        {
            u1L2IwfPortState = AstGetInstPortStateFromL2Iwf
                (RST_DEFAULT_INSTANCE, u2PortNum);
            if (u1L2IwfPortState != u1PortState)
            {
                /*update hardware with the control plane port state */
                RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                            u4IfIndex, u1L2IwfPortState);
            }
        }
    }
#endif

    RstInitPerStPortInfo (u2PortNum, RST_DEFAULT_INSTANCE, pPerStPortInfo);

    AST_SET_CHANGED_FLAG (RST_DEFAULT_INSTANCE, u2PortNum) = FALSE;

    if (AST_MODULE_STATUS == RST_ENABLED)
    {
        if (RstStartSemsForPort (u2PortNum) == RST_FAILURE)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "CreatePort: Port %s: StartSems returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);
            AST_GET_PORTENTRY (u2PortNum) = NULL;

            (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE) = NULL;

            return MST_FAILURE;
        }
    }

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_ENABLED);
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: Created Successfully...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Port %s: Created Successfully...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_ALLOC_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);

    if (pRstPortInfo == NULL)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "CreatePort: Port %s: StartSems returned failure !\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);
        AST_GET_PORTENTRY (u2PortNum) = NULL;

        (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
        AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE) = NULL;

        return RST_FAILURE;
    }
    AST_MEMSET (pRstPortInfo, 0, sizeof (tRstPortInfo));
    pRstPortInfo->pPortInfo = pPortInfo;
    pRstPortInfo->u2PortNum = u2PortNum;
    AST_ADD_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                      (tTMO_SLL_NODE *) & (pRstPortInfo->NextNode));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstDeletePort                                        */
/*                                                                           */
/* Description        : Deleting a CEP results in CVLAN shutdown / Deletion  */
/*                      of CVLAN component. Normal Port deletion will happen */
/*                      in SVLAN component.                                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port that is to be    */
/*                                  deleted.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstDeletePort (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tRstPortInfo       *pRstPortInfo = NULL;

    if (AST_IS_PORT_VALID (u2PortNum) == AST_FALSE)
    {
        /* Port number exceeds the allowed range */
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Delete Port for invalid port number !!!\n");
        return RST_FAILURE;
    }

    if ((pAstPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: DeletePort: Port is not existing to delete !!!\n");
        return RST_FAILURE;
    }

    /*If the Delete Port indication comes for a CEP, delete the CVLAN component
     * and its all PEPs and also delete the port from the SVLAN context*/
    if (AstPbCVlanCompShutdown (pAstPortEntry) != RST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Deletion of CVLAN component Failed FATAL" "ERROR!!!\n");

        return RST_FAILURE;
    }

    /*Deleting the Port from the GIVEN context */
    RstComponentDeletePort (u2PortNum);

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_DISABLED);

    if (AST_COUNT_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList) != 0)
    {
        AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                           pRstPortInfo, tRstPortInfo *)
        {
            if (pRstPortInfo->pPortInfo == pAstPortEntry)
            {
                AST_DELETE_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                                     pRstPortInfo);
                AST_RELEASE_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);
                break;
            }
        }

    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstComponentDeletePort                               */
/*                                                                           */
/* Description        : Deletes the port information and releases the memory */
/*                      occupied by it.                                      */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port that is to be    */
/*                                  deleted.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstComponentDeletePort (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               au1PortStr[AST_IFINDEX_STR_LEN];

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Deleting Port %u...\n", u2PortNum);
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Deleting Port %u...\n", u2PortNum);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pRstPortInfo->bPortEnabled = (tAstBoolean) RST_FALSE;

    if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
    {
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_PORT_DISABLED,
                                     u2PortNum) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }

        if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_PORT_DISABLED,
                                pPerStPortInfo, NULL) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }

        RstBrgDetectionMachine (RST_BRGDETSM_EV_PORTDISABLED, u2PortNum);
    }

    if (RstStopAllRunningTimers (NULL, pPortInfo, pPerStPortInfo) !=
        RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Unable to stop the running timers !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Unable to stop the running timers !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    MEMCPY (au1PortStr, AST_GET_IFINDEX_STR (pPortInfo->u2PortNo),
            AST_IFINDEX_STR_LEN);

    AstReleasePortMemBlocks (pPortInfo);

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: Deleted Successfully\n", au1PortStr);
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Port %u: Deleted Successfully\n", au1PortStr);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstEnablePort                                        */
/*                                                                           */
/* Description        : This routine calls the state machines with the event */
/*                      for enabling the port and making the port operational*/
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port                  */
/*                      u1TrigType - Indicates if the Port is Enabled from   */
/*                      Bridge/CFA or at STP level .                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstEnablePort (UINT2 u2PortNum, UINT1 u1TrigType)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstBoolean         portUpdated = RST_FALSE;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    UINT4               u4Isid = AST_INIT_VAL;
#endif

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Enabling Port %s ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Enabling Port %s ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_TRUE)
        {
            return RST_SUCCESS;
        }
        pRstPortInfo->bPortEnabled = (tAstBoolean) RST_TRUE;

        if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
        {
            /* Update spanning tree status for this port in L2IWF. */
            RstUpdtProtoPortStateInL2Iwf (u2PortNum, u1TrigType);
        }

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return RST_SUCCESS;
        }
    }

    /* The value of Oper P2P is updated while enabling the port since the P2P
     * status depends on the duplexity of the port as detected by the hardware
     * when the link is up.*/
    portUpdated = pPortInfo->bIEEE8021apAdminP2P;
    AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);
    pPortInfo->bIEEE8021apAdminP2P = portUpdated;
    /* The pathcost is updated while enabling the port since it depends on the
     * speed of the port as detected by the hardware when the link is up.*/
    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
        {
            return RST_SUCCESS;
        }
        pAstBridgeEntry = AST_GET_BRGENTRY ();

        /* The Port path cost is updated if there is any change in link speed
         * when a link up trigger is received - 
         * if the pathcost has NOT already been configured for this port
         * and if either dynamic pathcost calculation is enabled
         *     or if the port is coming up for the first time after creation */

        /* Except for CEP, All CVLAN component Ports will always have the
         * Path Cost Configured Flag as TRUE by default */
        if ((AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
             == RST_FALSE) &&
            ((pAstBridgeEntry->u1DynamicPathcostCalculation == RST_TRUE) ||
             (pPortInfo->bPathCostInitialized == RST_FALSE)))
        {
            pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
            pPortInfo->bPathCostInitialized = RST_TRUE;
        }
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port Up event should be processed.
         * If Cfa Port Up event comes, directly set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_UP)
        {

            AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE, u2PortNum,
                                        AST_PORT_STATE_FORWARDING);

            AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNum) =
                AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_FORWARDING);
            }
#endif /*NPAPI_WANTED */
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module disabled. Returning...\n");
        return RST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        if (AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE,
                                          u2PortNum)
            != AST_PORT_STATE_DISCARDING)
        {
            /* 
             * Spanning tree is now being enabled on this port. 
             * Put the port in DISCARDING state (previously it could
             * have been in FORWARDING since spanning tree was disabled 
             * on this port) 
             */
            AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE, u2PortNum,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNum) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_DISCARDING);
            }
#endif /* NPAPI_WANTED */
        }
        /*Flushing is done when a port is enabled in RSTP */
        RstTopoChSmFlushFdb (pPerStPortInfo->u2PortNo, VLAN_NO_OPTIMIZE);
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;
        if (pCommPortInfo != NULL)
        {
            pCommPortInfo->bBpduInconsistent = AST_FALSE;
        }
        if (pRstPortInfo->bPortEnabled != (tAstBoolean) RST_TRUE)
        {
            if (AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE,
                                              u2PortNum) !=
                AST_PORT_STATE_FORWARDING)
            {
                /* 
                 * The port is now being enabled at the bridge level. 
                 * But the port is disabled in spanning tree.
                 * Put the port in FORWARDING state (previously it could have 
                 * been in DISCARDING since the port was disabled at the bridge
                 * level) 
                 */

                AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE,
                                            u2PortNum,
                                            AST_PORT_STATE_FORWARDING);

                AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE,
                                            u2PortNum) =
                    AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    AstMiRstpNpSetPortState (u2PortNum,
                                             AST_PORT_STATE_FORWARDING);
                }
#endif

                return RST_SUCCESS;
            }
        }
    }
    if (AST_IS_VIRTUAL_INST_PORT (u2PortNum) == RST_TRUE)
    {
        /* Update the ISID associated with the port
         * */
#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
        MEMSET (pPortInfo->au1StrIfIndex, 0, AST_IFINDEX_STR_LEN);
        /* ISID should be obtained from L2IWF
         * */
        if (AstL2IwfGetIsid (pPortInfo->u4IfIndex, &u4Isid) != L2IWF_SUCCESS)
        {
            return RST_FAILURE;
        }
        SNPRINTF ((CHR1 *) pPortInfo->au1StrIfIndex, AST_IFINDEX_STR_LEN,
                  "%s-%s:%d", "VIP", "ISID", u4Isid);
#endif
    }

    if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_PORT_ENABLED,
                                 u2PortNum) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Protocol Migration Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Protocol Migration Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_PORT_ENABLED,
                            pPerStPortInfo, NULL) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Info Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Info Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    /* Should trigger the TXSM only at the end since the priority vectors should
     * be initialised before transmission */
    if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_TX_ENABLED, pPortInfo,
                                RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }
    if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_PORT_ENABLED, u2PortNum,
                              NULL) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for Port Enabled event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for Port Enabled event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;

    }

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: Enabled Successfully\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Port %s: Enabled Successfully\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstDisablePort                                       */
/*                                                                           */
/* Description        : This routine calls the state machines with the event */
/*                      for disabling the port and makes the Port State      */
/*                      Machines to disable                                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port                  */
/*                      u1TrigType - Indicates if the Port is disabled at    */
/*                                     Bridge/CFA level or STP level.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstDisablePort (UINT2 u2PortNum, UINT1 u1TrigType)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
#ifdef ICCH_WANTED
    UINT1               u1IsMclagEnabled = 0;
#endif

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Disabling Port %s ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Disabling Port %s ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    pPortInfo->bLoopInconsistent = AST_FALSE;
    pPortInfo->u1RecScenario = AST_INIT_VAL;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_FALSE)
        {
            return RST_SUCCESS;
        }
        pRstPortInfo->bPortEnabled = (tAstBoolean) RST_FALSE;

        /* Update spanning tree status for this port in L2IWF. */
        RstUpdtProtoPortStateInL2Iwf (u2PortNum, u1TrigType);

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return RST_SUCCESS;
        }
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port Down event should be processed.
         * If Cfa Port Down event comes, directly set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_DOWN)
        {
            AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE, u2PortNum,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNum) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_DISCARDING);
            }
#endif /*NPAPI_WANTED */

            if (AstIsVlanEnabledInContext () == RST_TRUE)
            {
                AstPbVlanDeleteFdbEntries (pPortInfo, VLAN_NO_OPTIMIZE);
            }
            else
            {
                /* Inform the bridge to delete in its MAC table
                 * all the entries learned for this port.
                 */
#ifdef BRIDGE_WANTED
                AstBrgDelFwdEntryForPort (pPortInfo);
#endif
            }
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module disabled. Returning...\n");
        return RST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_DOWN)
    {
        /*When STP Receives port oper down indication 
           set bpdu inconsistent state to false. 
           When BPDU is received on the port in which bpdu guard is enable, 
           the caller of this function will set the flag */
        if ((pCommPortInfo->bBpduInconsistent != RST_TRUE) ||
            (pCommPortInfo->u1BpduGuardAction != AST_PORT_ADMIN_DOWN))
        {
            pCommPortInfo->bBpduInconsistent = AST_FALSE;
        }
        if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_DOWN)
        {
            return RST_SUCCESS;
        }
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

        if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_FALSE)
        {
            if (AstGetInstPortStateFromL2Iwf (RST_DEFAULT_INSTANCE, u2PortNum)
                != AST_PORT_STATE_DISCARDING)

            {
                /* 
                 * The port is now being disabled at the bridge level. 
                 * But the port is disabled in spanning tree.
                 * Put the port in DISCARDING state (previously it could have 
                 * been in FORWARDING since the port was disabled in spanning  
                 * tree) 
                 */
                AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE,
                                            u2PortNum,
                                            AST_PORT_STATE_DISCARDING);

                AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNum) =
                    AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    AstMiRstpNpSetPortState (u2PortNum,
                                             AST_PORT_STATE_DISCARDING);
                }
#endif /* NPAPI_WANTED */
#ifdef ICCH_WANTED
                /* Dont delete FDB entries learnt on MC-LAG enabled
                 * interface */
                LaApiIsMclagInterface (u2PortNum, &u1IsMclagEnabled);
                if ((u1IsMclagEnabled == OSIX_TRUE)
                    && (IcchGetPeerNodeState () == ICCH_PEER_UP))
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_DATA_PATH_TRC,
                             "Deletion of FDB entries skipped for MC-LAG enabled interface\n");
                    return RST_SUCCESS;
                }
#endif
                /* Delete the Fdb Entries Learnt on the Port */
                AstVlanDeleteFdbEntries (u2PortNum, VLAN_OPTIMIZE);

            }

            return RST_SUCCESS;
        }
    }

    RstPortReceiveMachine ((UINT2) RST_PRCVSM_EV_PORT_DISABLED, NULL,
                           u2PortNum);

    RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_TX_DISABLED, pPortInfo,
                            RST_DEFAULT_INSTANCE);

    RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_PORT_DISABLED, u2PortNum);

    /* BrgDetectionMachine is triggered before the Port Info state machine 
       because Flushing is not done for the Non-Edge port since BrgDetectionMachine 
       moves the edgeport to nonedge port(i.e AutoEdge is set) when the port is disabled */
    RstBrgDetectionMachine (RST_BRGDETSM_EV_PORTDISABLED, u2PortNum);

    if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_PORT_DISABLED,
                            pPerStPortInfo, NULL) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Info Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Info Machine Returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_PORT_DISABLED, u2PortNum,
                              NULL) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for Port Disabled event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for Port Disabled event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    /* The Rapid age duration timer should not be stopped. On it's expiry the 
     * short aging should be reset on this port */

    if (pCommPortInfo->pMdWhileTmr != NULL)
    {
        if (AstStopTimer (pPortInfo, AST_TMR_TYPE_MDELAYWHILE) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for MDelayWhile timer Failed !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }

    if (pCommPortInfo->pHelloWhenTmr != NULL)
    {
        if (AstStopTimer (pPortInfo, AST_TMR_TYPE_HELLOWHEN) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Stop Timer for HelloWhen timer Failed !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }
    if (pRstPortInfo->pRootIncRecTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPerStPortInfo,
             AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for Root Inconsistency recovery timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    /* Here Flushing is triggered  when the port is made ADMIN_DOWN */
    if (u1TrigType == (UINT1) AST_EXT_PORT_DOWN)
    {
        if (pPortInfo->bAdminEdgePort == RST_TRUE)
        {
            if (AstIsVlanEnabledInContext () == RST_TRUE)
            {
                if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
                {
                    /* Need not set Short Ageout time again for this port
                     * when ShortAgeout Duration timer is running */
                    if (pCommPortInfo->pRapidAgeDurtnTmr == NULL)
                    {
                        AstVlanSetShortAgeoutTime (pPortInfo);

                    }

                    /* FdbFlush has been set. Short Ageout time should be applied
                     * for a duration of FwdDelay more seconds. Hence restart the
                     * duration timer (even if it is already running) */
                    /* Start the duration for FwdDelay+1 seconds so that the
                     * forwarding module gets to ageout entries atleast once at
                     * the end of FwdDelay seconds before reverting back to
                     * long ageout */
                    if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                                       (UINT1) AST_TMR_TYPE_RAPIDAGE_DURATION,
                                       (UINT2) (pPortInfo->DesgTimes.
                                                u2ForwardDelay +
                                                (1 * AST_CENTI_SECONDS))) !=
                        RST_SUCCESS)
                    {
                        AST_DBG (AST_TCSM_DBG | AST_TMR_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TCSM: AstStartTimer for Rapid Age duration FAILED!\n");
                    }
                }
                else
                {
                    AstFlushFdbEntries (pPortInfo, (UINT1) VLAN_NO_OPTIMIZE);

                }
            }

        }
    }

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: Disabled Successfully\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Port %s: Disabled Successfully\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstReInitRstpInfo                                    */
/*                                                                           */
/* Description        : Initialises the Rstp Info on Module Disable          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/

VOID
RstReInitRstpInfo ()
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    /* Initialization of instance specific spanning tree information */
    AST_MEMCPY (&(pPerStBrgInfo->RootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;
    pBrgInfo->bBridgeClearStats = RST_DISABLED;
    pBrgInfo->u4RstOptStatus = RST_ENABLED;

    pPerStBrgInfo->u1ProleSelSmState = (UINT1) RST_PROLESELSM_STATE_INIT_BRIDGE;
    pPerStBrgInfo->u2RootPort = AST_NO_VAL;
    pPerStBrgInfo->u4RootCost = AST_INIT_VAL;
    pPerStBrgInfo->u4TimeSinceTopoCh = AST_INIT_VAL;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        /* Initialization of global port information */
        if (pPortInfo == NULL)
        {
            continue;
        }

        pPortInfo->DesgTimes = pBrgInfo->RootTimes;
        pPortInfo->PortTimes = pPortInfo->DesgTimes;
        pPortInfo->bPortClearStats = RST_FALSE;
        pPortInfo->bIEEE8021apAdminP2P = RST_FALSE;
        pPortInfo->bLoopInconsistent = RST_FALSE;
        pPortInfo->bDot1wEnabled = RST_FALSE;
        pPortInfo->u1RecScenario = AST_INIT_VAL;
        pPortInfo->bPVIDInconsistent = RST_FALSE;
        pPortInfo->bPTypeInconsistent = RST_FALSE;
        pPortInfo->bPTypeIncExists = RST_FALSE;
        pPortInfo->bPVIDIncExists = RST_FALSE;
        pCommPortInfo = &(pPortInfo->CommPortInfo);
        pCommPortInfo->bInitPm = RST_FALSE;
        pCommPortInfo->bMCheck = RST_FALSE;
        pCommPortInfo->bNewInfo = RST_FALSE;
        pCommPortInfo->bRcvdBpdu = RST_FALSE;
        pCommPortInfo->bRcvdRstp = RST_FALSE;
        pCommPortInfo->bRcvdStp = RST_FALSE;
        pCommPortInfo->bRcvdTcAck = RST_FALSE;
        pCommPortInfo->bRcvdTcn = RST_FALSE;
        pCommPortInfo->bTcAck = RST_FALSE;
        pCommPortInfo->u1BrgDetSmState = (UINT1) RST_BRGDETSM_STATE_NOT_EDGE;
        pCommPortInfo->u1PmigSmState = (UINT1) RST_PMIGSM_STATE_CHECKING_RSTP;
        pCommPortInfo->u1PortRcvSmState = (UINT1) RST_PRCVSM_STATE_DISCARD;
        pCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_INIT;
        pCommPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;
        pCommPortInfo->bBpduInconsistent = RST_FALSE;

        /* Initialization of instance specific port information */
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
        pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_DISABLED;
        pPerStPortInfo->u1ProleTrSmState =
            (UINT1) RST_PROLETRSM_STATE_INIT_PORT;
        pPerStPortInfo->u1PstateTrSmState =
            (UINT1) RST_PSTATETRSM_STATE_DISCARDING;
        pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_INACTIVE;
        pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_ROLE_UPDATE);
        pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
        pPerStPortInfo->u1OldPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_ROLE_UPDATE);
        pPerStPortInfo->u4RootCost = AST_INIT_VAL;

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        pRstPortInfo->bAgreed = RST_FALSE;
        pRstPortInfo->bForward = RST_FALSE;
        pRstPortInfo->bForwarding = RST_FALSE;
        pRstPortInfo->bLearn = RST_FALSE;
        pRstPortInfo->bLearning = RST_FALSE;
        pRstPortInfo->bProposed = RST_FALSE;
        pRstPortInfo->bProposing = RST_FALSE;
        pRstPortInfo->bRcvdMsg = RST_FALSE;
        pRstPortInfo->bRcvdTc = RST_FALSE;
        pRstPortInfo->bReRoot = RST_FALSE;
        pRstPortInfo->bReSelect = RST_FALSE;
        pRstPortInfo->bSync = RST_FALSE;
        pRstPortInfo->bSynced = RST_FALSE;
        MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        pRstPortInfo->bTc = RST_FALSE;
        pRstPortInfo->bTcProp = RST_FALSE;
        pRstPortInfo->bUpdtInfo = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
        pRstPortInfo->bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
        pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_DISABLED;
        pRstPortInfo->u1RcvdInfo = (UINT1) RST_OTHER_MSG;
        pRstPortInfo->bDisputed = RST_FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : RstInitGlobalBrgInfo                                 */
/*                                                                           */
/* Description        : Initialises the Bridge Global Information.           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number of the port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
RstInitGlobalBrgInfo (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    AST_MEMSET (&(pBrgInfo->BridgeAddr), AST_INIT_VAL, AST_MAC_ADDR_SIZE);
    pBrgInfo->u1MigrateTime = (UINT1) RST_DEFAULT_MIGRATE_TIME;
    pBrgInfo->u1TxHoldCount = (UINT1) RST_DEFAULT_BRG_TX_LIMIT;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->BridgeTimes.u2MaxAge = RST_DEFAULT_BRG_MAX_AGE;
    pBrgInfo->BridgeTimes.u2ForwardDelay = RST_DEFAULT_BRG_FWD_DELAY;
    pBrgInfo->BridgeTimes.u2HelloTime = RST_DEFAULT_BRG_HELLO_TIME;
    pBrgInfo->BridgeTimes.u2MsgAgeOrHopCount = AST_INIT_VAL;
    pBrgInfo->u1DynamicPathcostCalculation = AST_FALSE;
    pBrgInfo->u1DynamicPathcostCalcLagg = AST_SNMP_FALSE;
    pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;
    pBrgInfo->bBridgeClearStats = RST_DISABLED;
    pBrgInfo->u4RstOptStatus = RST_ENABLED;
    return;
}

/*****************************************************************************/
/* Function Name      : RstInitPerStBrgInfo                                  */
/*                                                                           */
/* Description        : Initialises the Instance Specific Spanning Tree      */
/*                      Information.                                         */
/*                                                                           */
/* Input(s)           : pPerStBrgInfo - Per Spanning Tree Bridge Information */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
RstInitPerStBrgInfo (tAstPerStBridgeInfo * pPerStBrgInfo)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    /* The Bridge Priority for CVLAN component is different from
       the SVLAN component. This macro will take care of the Giving the
       Bridge Priority depending of the context's component type */

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE) ||
        (AST_IS_I_COMPONENT () == RST_TRUE))
    {
        pPerStBrgInfo->u2BrgPriority = AST_PB_CVLAN_BRG_PRIORITY;
    }
    else
    {
        pPerStBrgInfo->u2BrgPriority = RST_DEFAULT_BRG_PRIORITY;
    }

    AST_MEMCPY (&(pPerStBrgInfo->RootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

    pPerStBrgInfo->u1ProleSelSmState = (UINT1) RST_PROLESELSM_STATE_INIT_BRIDGE;
    pPerStBrgInfo->u2RootPort = AST_NO_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    pPerStBrgInfo->u4RootCost = AST_INIT_VAL;
    pPerStBrgInfo->u4FlushIndThreshold = RST_DEFAULT_FLUSH_IND_THRESHOLD;
    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStBrgInfo->u4TimeSinceTopoCh)));
    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    /* Leave the OldRootId as all zeros */

    return;
}

/*****************************************************************************/
/* Function Name      : RstInitGlobalPortInfo                                */
/*                                                                           */
/* Description        : Initialises the Global Port Information              */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the information is */
/*                                  to be initialised.                       */
/*                      pPortInfo - Port Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
RstInitGlobalPortInfo (UINT2 u2PortNum, tAstPortEntry * pPortInfo)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstBoolean         portUpdated = RST_FALSE;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo->u2PortNo = u2PortNum;
    pPortInfo->bAdminEdgePort = RST_FALSE;

    AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_FALSE);

    if ((AST_BRIDGE_MODE () != AST_PROVIDER_EDGE_BRIDGE_MODE) ||
        (AST_COMP_TYPE () != AST_PB_C_VLAN) ||
        (AST_IS_I_COMPONENT () == RST_TRUE))
    {
        pPortInfo->u1AdminPointToPoint = (UINT1) RST_P2P_AUTO;
        /* The value of Oper Point-To-Point will be updated after triggering 
         * LA and getting the status */
        portUpdated = pPortInfo->bIEEE8021apAdminP2P;
        AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);
        pPortInfo->bIEEE8021apAdminP2P = portUpdated;
    }

    pPortInfo->bOperEdgePort = RST_FALSE;

    pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
    pPortInfo->bIEEE8021apAdminP2P = RST_FALSE;
    pPortInfo->bPortClearStats = AST_FALSE;
    pPortInfo->bLoopInconsistent = RST_FALSE;
    pPortInfo->bDot1wEnabled = RST_FALSE;
    pPortInfo->u1RecScenario = AST_INIT_VAL;
    pPortInfo->DesgTimes = pBrgInfo->RootTimes;
    pPortInfo->PortTimes = pPortInfo->DesgTimes;
    pPortInfo->bRootGuard = RST_FALSE;
    pPortInfo->bRootInconsistent = RST_FALSE;
    pPortInfo->bPVIDInconsistent = RST_FALSE;
    pPortInfo->bPTypeInconsistent = RST_FALSE;
    pPortInfo->bPTypeIncExists = RST_FALSE;
    pPortInfo->bPVIDIncExists = RST_FALSE;
    AST_PORT_RESTRICTED_ROLE (pPortInfo) = RST_FALSE;
    AST_PORT_RESTRICTED_TCN (pPortInfo) = RST_FALSE;
    pPortInfo->u4ErrorRecovery = AST_DEFAULT_ERROR_RECOVERY;
    pCommPortInfo = &(pPortInfo->CommPortInfo);
    pCommPortInfo->bInitPm = RST_FALSE;
    pCommPortInfo->bMCheck = RST_FALSE;
    pCommPortInfo->bNewInfo = RST_FALSE;
    pCommPortInfo->bRcvdBpdu = RST_FALSE;
    pCommPortInfo->bRcvdRstp = RST_FALSE;
    pCommPortInfo->bRcvdStp = RST_FALSE;
    pCommPortInfo->bRcvdTcAck = RST_FALSE;
    pCommPortInfo->bRcvdTcn = RST_FALSE;
    pCommPortInfo->bSendRstp = RST_TRUE;
    pCommPortInfo->bTcAck = RST_FALSE;
    pCommPortInfo->u1PmigSmState = (UINT1) RST_PMIGSM_STATE_CHECKING_RSTP;
    pCommPortInfo->u1PortTxSmState = (UINT1) RST_PTXSM_STATE_TRANSMIT_INIT;
    pCommPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;
    pCommPortInfo->u4BpduGuard = AST_BPDUGUARD_NONE;
    pCommPortInfo->bBpduInconsistent = RST_FALSE;
    pPortInfo->bAutoEdge = AST_DEFAULT_AUTOEDGE_VALUE;
    pPortInfo->u1EnableBPDUFilter = AST_BPDUFILTER_DISABLE;
    AST_PORT_ENABLE_BPDU_RX (pPortInfo) = RST_TRUE;
    AST_PORT_ENABLE_BPDU_TX (pPortInfo) = RST_TRUE;
    AST_PORT_IS_L2GP (pPortInfo) = RST_FALSE;

    return;
}

/*****************************************************************************/
/* Function Name      : RstInitPerStPortInfo                                 */
/*                                                                           */
/* Description        : Initialises the Spanning Tree Specific Port          */
/*                      Information                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the information is */
/*                                  to be initialised.                       */
/*                      u2InstanceId - Instance of Spanning Tree.            */
/*                      pPerStPortInfo - Per Spanning Tree Port Information  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
RstInitPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId,
                      tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT1               u1TunnelStatus;

    pBrgInfo = AST_GET_BRGENTRY ();
    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pPerStPortInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_DISABLED;
    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_INIT_PORT;
    pPerStPortInfo->u1PstateTrSmState = (UINT1) RST_PSTATETRSM_STATE_DISCARDING;
    pPerStPortInfo->u1TopoChSmState = (UINT1) RST_TOPOCHSM_STATE_INACTIVE;
    pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_ROLE_UPDATE);
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u1OldPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);
    pPerStPortInfo->u2PortNo = u2PortNum;
    pPerStPortInfo->u2Inst = u2InstanceId;
    u2Val = (UINT2) RST_DEFAULT_PORT_PRIORITY;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);

    pPerStPortInfo->u2DesgPortId = AST_GET_PROTOCOL_PORT (u2PortNum) | u2Val;
    pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
    pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
    pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;
    pPerStPortInfo->u4RootCost = AST_INIT_VAL;
    pPerStPortInfo->u1PortPriority = (UINT1) RST_DEFAULT_PORT_PRIORITY;
    pPerStPortInfo->u4PortAdminPathCost = 0;
    AST_MEMCPY (&(pPerStPortInfo->PseudoRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    pPerStPortInfo->PseudoRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStPortInfo->bIsPseudoRootIdConfigured = RST_FALSE;
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AstGetInstPortStateFromL2Iwf (u2InstanceId, u2PortNum);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pRstPortInfo->bAgreed = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;
    pRstPortInfo->bForwarding = RST_FALSE;
    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bLearning = RST_FALSE;
    pRstPortInfo->bProposed = RST_FALSE;
    pRstPortInfo->bProposing = RST_FALSE;
    pRstPortInfo->bRcvdMsg = RST_FALSE;
    pRstPortInfo->bRcvdTc = RST_FALSE;
    pRstPortInfo->bReRoot = RST_FALSE;
    pRstPortInfo->bReSelect = RST_FALSE;
    pRstPortInfo->bSync = RST_FALSE;
    pRstPortInfo->bSynced = RST_FALSE;
    MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bTc = RST_FALSE;
    pRstPortInfo->bTcProp = RST_FALSE;
    pRstPortInfo->bUpdtInfo = RST_FALSE;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSELECT_UPDATE);
    pRstPortInfo->bSelected = RST_FALSE;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSELECT_UPDATE);
    pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_DISABLED;
    pRstPortInfo->u1RcvdInfo = (UINT1) RST_OTHER_MSG;
    pRstPortInfo->bDisputed = RST_FALSE;
    pRstPortInfo->bPortEnabled = RST_TRUE;

    /* In case of PEB/PCB and Customer Bridge, STP tunnel status should   
     * not be enabled on the port for enabling STP on that port */
    if ((AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE) ||
        (AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE))
    {
        AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX (u2PortNum),
                                               L2_PROTO_STP, &u1TunnelStatus);

        if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            pRstPortInfo->bPortEnabled = RST_FALSE;
        }
    }

    /* In case of q-in-q bridge mode, Tunnel Status should not be enabled   
     * on the port for enabling STP on that port */
    if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
    {
        AstL2IwfGetPortVlanTunnelStatus (AST_GET_IFINDEX (u2PortNum),
                                         (BOOL1 *) & u1TunnelStatus);

        if (u1TunnelStatus == OSIX_TRUE)
        {
            pRstPortInfo->bPortEnabled = RST_FALSE;
        }
    }

    return;
}

/*RSTP clear statistics feature*/

/*****************************************************************************/
/* Function Name      : RstResetCounters                                     */
/*                                                                           */
/* Description        : This function resets all bridge and port statistics  */
/*                      counters                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstResetCounters ()
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();
    if (NULL == pBrgInfo)
    {
        return;
    }

    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;

    if (NULL == AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE))
    {
        return;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    if (NULL == pPerStBrgInfo)
    {
        return;
    }

    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    pPerStBrgInfo->u4TotalFlushCount = AST_INIT_VAL;

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if ((pPortInfo) == NULL)
        {
            continue;
        }
        /*Resets the CEP Port statistics counters */
        if (pPortInfo->u1PortType == AST_CUSTOMER_EDGE_PORT)
        {
            if (RST_FAILURE == RstResetCepPortCounters (pPortInfo))
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "Clearing CEP Port Counters Returned failure !!!\n");
                continue;
            }
        }
        else
        {
            pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
            pPortInfo->u4NumRstTCDetectedCount = AST_INIT_VAL;
            pPortInfo->u4NumRstTCRxdCount = AST_INIT_VAL;
            pPortInfo->u4NumRstRxdInfoWhileExpCount = AST_INIT_VAL;
            pPortInfo->u4NumRstProposalPktsSent = AST_INIT_VAL;
            pPortInfo->u4NumRstProposalPktsRcvd = AST_INIT_VAL;
            pPortInfo->u4NumRstAgreementPktSent = AST_INIT_VAL;
            pPortInfo->u4NumRstAgreementPktRcvd = AST_INIT_VAL;
            pPortInfo->u4NumRstImpossibleStateOcc = AST_INIT_VAL;

            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
            if (NULL == pPerStPortInfo)
            {
                continue;
            }
            pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
            pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
            pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
            pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : RstResetPortCounters                                 */
/*                                                                           */
/* Description        : This function resets all port statistics             */
/*                      counters                                             */
/*                                                                           */
/* Input(s)           : i4PortNum - Identifier of the por                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstResetPortCounters (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (NULL == pPortInfo)
    {
        return;
    }
    /*Resets the CEP Port statistics counters */
    if (pPortInfo->u1PortType == AST_CUSTOMER_EDGE_PORT)
    {
        if (RST_FAILURE == RstResetCepPortCounters (pPortInfo))
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "Clearing CEP Port Counters Returned failure !!!\n");
            return;
        }
    }
    else
    {
        pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
        pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
        pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
        pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
        pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
        pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
        pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
        pPortInfo->u4NumRstTCDetectedCount = AST_INIT_VAL;
        pPortInfo->u4NumRstTCRxdCount = AST_INIT_VAL;
        pPortInfo->u4NumRstRxdInfoWhileExpCount = AST_INIT_VAL;
        pPortInfo->u4NumRstProposalPktsSent = AST_INIT_VAL;
        pPortInfo->u4NumRstProposalPktsRcvd = AST_INIT_VAL;
        pPortInfo->u4NumRstAgreementPktSent = AST_INIT_VAL;
        pPortInfo->u4NumRstAgreementPktRcvd = AST_INIT_VAL;
        pPortInfo->u4NumRstImpossibleStateOcc = AST_INIT_VAL;

        if (NULL == AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE))
        {
            return;
        }

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
        if (NULL == pPerStPortInfo)
        {
            return;
        }
        pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
        pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
    }
    return;

}

/*RSTP clear statistics feature*/

/*****************************************************************************/
/* Function Name      : RstInitStateMachines                                 */
/*                                                                           */
/* Description        : Initialises the Global State Machine Entries         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
RstInitStateMachines (VOID)
{
    MEMSET (gaaau1AstSemState, 0, sizeof (gaaau1AstSemState));
    MEMSET (gaaau1AstSemEvent, 0, sizeof (gaaau1AstSemEvent));
    /* Bridge Detection Machine */
    RstInitBrgDetectionStateMachine ();

    /* Port Receive Machine */
    RstInitPortRxStateMachine ();

    /* Port Info Machine */
    RstInitPortInfoMachine ();

    /* Port Role Selection Machine */
    RstInitPortRoleSelectionMachine ();

    /* Port Role Transition Machine */
    RstInitPortRoleTrMachine ();

    /* Port State Transition Machine */
    RstInitPortStateTrMachine ();

    /* Port Migration Machine */
    RstInitProtocolMigrationMachine ();

    /* Topology Change Machine */
    RstInitTopoChStateMachine ();

    /* Port Transmit Machine */
    RstInitPortTxStateMachine ();

    /* Port Pseudo Info Machine */
    RstInitPseudoInfoMachine ();

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstStartSemsForPort                        */
/*                                                                           */
/*    Description               : This function initialises all the State    */
/*                                Machines relatd to the port.               */
/*                                                                           */
/*    Input(s)                 :  u2PortNum - Port Number of the port that   */
/*                                            is to be created.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS/RST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
RstStartSemsForPort (UINT2 u2PortNum)
#else
INT4
RstStartSemsForPort (u2PortNum)
     UINT2               u2PortNum;
#endif
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Starting SEMs for port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Starting SEMs for port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s not present for default instance\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        return RST_FAILURE;
    }

    RstBrgDetectionMachine (RST_BRGDETSM_EV_BEGIN, u2PortNum);

    if (RstPortReceiveMachine ((UINT2) RST_PRCVSM_EV_BEGIN, NULL,
                               u2PortNum) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Receive Migration Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Receive Migration Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_BEGIN,
                                 u2PortNum) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Protocol Migration Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Protocol Migration Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortStateTrMachine ((UINT2) RST_PSTATETRSM_EV_BEGIN, pPerStPortInfo,
                               RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port State Transition Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port State Transition Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortRoleTrMachine ((UINT2) RST_PROLETRSM_EV_BEGIN, pPerStPortInfo)
        != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Role Transition Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RTSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Role Transition Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_BEGIN, pPerStPortInfo,
                            NULL) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstTopoChMachine ((UINT2) RST_TOPOCHSM_EV_BEGIN, pPerStPortInfo)
        != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_BEGIN, pPortInfo,
                                RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;
    }

    if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_BEGIN, u2PortNum, NULL) !=
        RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for BEGIN event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Pseudo Info Machine Returned failure for BEGIN event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_FAILURE;

    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s: StartSemsForPort: Completed starting SEMs \n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %s: StartSemsForPort: Completed starting SEMs \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstAssertBegin                             */
/*                                                                           */
/*    Description               : This function initialises all the State    */
/*                                Machines. It is called when the global     */
/*                                condition BEGIN is asserted.               */
/*                                                                           */
/*    Input(s)                 :  None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS/RST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
AstAssertBegin (VOID)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;

    if (AST_IS_MST_ENABLED ())
    {
#ifdef MSTP_WANTED
        /* If MSTP is enabled, then assert MstAssertBegin */
        if (MstAssertBegin () == MST_FAILURE)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_INIT_SHUT_TRC, "SYS: Asserting BEGIN failed!\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "SYS: Asserting BEGIN failed!\n");
            return MST_FAILURE;
        }
#endif
        return MST_SUCCESS;
    }

    else if (AST_IS_RST_ENABLED ())
    {
        (AST_CURR_CONTEXT_INFO ())->bBegin = RST_TRUE;

        if (RstPortRoleSelectionMachine ((UINT2) RST_PROLESELSM_EV_BEGIN,
                                         RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     " SYS: Role Selection Machine Returned failure !!!\n");
            AST_DBG (AST_INIT_SHUT_TRC | AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     " SYS: Role Selection Machine Returned failure !!!\n");

            return RST_FAILURE;
        }

        u2PortNum = 1;

        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if ((pPortInfo) == NULL)
            {
                continue;
            }

            /* Oper status returned here is sum of all lower layer oper status */
            AstGetPortOperStatusFromL2Iwf (STP_MODULE, u2PortNum,
                                           &u1OperStatus);

            if (AST_IS_1AD_BRIDGE () == RST_TRUE)
            {
                if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
                {
                    if (AST_GET_BRG_PORT_TYPE (pPortInfo) ==
                        AST_PROVIDER_EDGE_PORT)
                    {
                        /* Determine Oper Point to Point for PEP from L2IWF */
                        if (RstUpdatePepOperPtoP
                            (pPortInfo->u2PortNo, u1OperStatus) != RST_SUCCESS)
                        {
                            /* FATAL ERROR !!!!!!!!!!!!!!!!! */
                            continue;
                        }
                    }
                }
                else
                {
                    /* SVLAN context */

                    /* SVLAN context CEP should be blocked */
                    if (AST_IS_CUSTOMER_EDGE_PORT (pPortInfo->u2PortNo) ==
                        RST_TRUE)
                    {
                        /* Set the port oper status as down in the S-VLAN component.
                         * This makes the sem to be in diabled state in S-VLAN comp
                         * for this port. */
                        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;
                        continue;
                    }

                }
                /* For CNP/PNP/PPNP/PCEP/PCNP in SVLAN context &
                 * For CEP/PEP in CVLAN context Proceed on triggering SEMs*/
            }

            if (u1OperStatus == AST_UP)
            {
                pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;
            }

            if (RstStartSemsForPort (u2PortNum) == RST_FAILURE)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "Module Enable: Port %s: StartSems returned failure !\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return RST_FAILURE;
            }
        }

        (AST_CURR_CONTEXT_INFO ())->bBegin = RST_FALSE;

        u2PortNum = 1;

        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if ((pPortInfo) == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      RST_DEFAULT_INSTANCE);

            /* Since the priority vectors are not yet initialized no bpdus
             * should be transmitted as yet. This is achieved because - 
             * Selected is false for all ports at this time, and hence 
             * all state transitions in TXSM are disabled. 
             * So the below trigger merely serves to transition the TXSM 
             * to the IDLE state - ready for transmission when Selected is set 
             * by RSSM */
            if (RstPortTransmitMachine
                ((UINT2) RST_PTXSM_EV_BEGIN_CLEARED, pPortInfo,
                 RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return RST_FAILURE;
            }

            if (RstPortInfoMachine ((UINT2) RST_PINFOSM_EV_BEGIN_CLEARED,
                                    pPerStPortInfo, NULL) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return RST_FAILURE;
            }
        }

        /* Triggering RSSM will inturn make RTSM, STSM, TCSM for all ports come 
         * out of their init states. 
         * At last, it will also trigger TXSM for each port with the SELECTED_SET
         * event, thus causing the first bpdu to be transmitted out from the port.
         */

        if (RstPortRoleSelectionMachine
            ((UINT2) RST_PROLESELSM_EV_BEGIN_CLEARED,
             RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     " SYS: Role Selection Machine Returned failure !!!\n");
            AST_DBG (AST_INIT_SHUT_TRC | AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     " SYS: Role Selection Machine Returned failure !!!\n");

            return RST_FAILURE;
        }

        /* The rest of the state machines (BDSM, PMSM, PRSM) can remain in their
         * init states until any external events occur */
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstUpdtProtoPortStateInL2Iwf               */
/*                                                                           */
/*    Description               : This function updates the spanning tree    */
/*                                status of this port in l2iwf.              */
/*                                                                           */
/*    Input(s)                 :  u2Port - Port where this status change has */
/*                                         occured.                          */
/*                                u1TrigType - AST_STP_PORT_DOWN or          */
/*                                             AST_STP_PORT_UP               */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS/RST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstUpdtProtoPortStateInL2Iwf (UINT2 u2Port, UINT1 u1TrigType)
{
    switch (u1TrigType)
    {
        case AST_STP_PORT_UP:
            AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_STP,
                                                    OSIX_ENABLED);
            break;
        case AST_STP_PORT_DOWN:
            AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_STP,
                                                    OSIX_DISABLED);
            break;
        default:
            break;
    }

    return RST_SUCCESS;
}

#ifdef NPAPI_WANTED
VOID
RstRetryNpapi (tAstPortEntry * pPortEntry)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = pPortEntry->u2PortNo;

    if ((pPerStPortInfo =
         AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: DeletePort: Port is not existing to delete !!!\n");
        return;
    }

    pPerStPortInfo->i4NpFailRetryCount++;

    if (pPerStPortInfo->i4NpPortStateStatus == AST_BLOCK_FAILURE)
    {
        if (AstMiRstpNpSetPortState (u2PortNum,
                                     AST_PORT_STATE_DISCARDING) == FNP_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, RST_DEFAULT_INSTANCE,
                                       AST_PORT_STATE_DISCARDING);
        }
        else if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_SYNCHRONOUS)
        {
            AstHwPortStateUpdtSuccess (pPortEntry, RST_DEFAULT_INSTANCE,
                                       AST_PORT_STATE_DISCARDING);
        }
    }
    else if (pPerStPortInfo->i4NpPortStateStatus == AST_FORWARD_FAILURE)
    {
        if (AstMiRstpNpSetPortState (u2PortNum,
                                     AST_PORT_STATE_FORWARDING) == FNP_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, RST_DEFAULT_INSTANCE,
                                       AST_PORT_STATE_FORWARDING);
        }
        else if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_SYNCHRONOUS)
        {
            AstHwPortStateUpdtSuccess (pPortEntry, RST_DEFAULT_INSTANCE,
                                       AST_PORT_STATE_FORWARDING);
        }
    }
}
#endif /*NPAPI_WANTED */
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstReCalculatePathcost                     */
/*                                                                           */
/*    Description               : This function calculates path cost for the */
/*                                given port number                          */
/*                                                                           */
/*    Input(s)                 :  u4IfIndex - Interface for which path cost  */
/*                                is to be calculated                        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS/RST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstReCalculatePathcost (UINT4 u4IfIndex)
{
    UINT4               u4PathCost = 0;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT1               u1ChangeFlag = RST_FALSE;

    u2PortNum = (UINT2) u4IfIndex;
    if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        return RST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    u4PathCost = pPortEntry->u4PathCost;

    if (AstPathcostConfiguredFlag (u2PortNum, RST_DEFAULT_INSTANCE)
        == RST_FALSE)
    {
        pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
    }

    if (u4PathCost != pPortEntry->u4PathCost)
    {
        u1ChangeFlag = RST_TRUE;
        pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
        pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;
    }

    if (AST_IS_RST_ENABLED ())
    {
        if (u1ChangeFlag == RST_TRUE)
        {

            if (RstPortRoleSelectionMachine
                ((UINT1) RST_PROLESELSM_EV_RESELECT,
                 RST_DEFAULT_INSTANCE) != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_MGMT_TRC,
                         "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RoleSelectionMachine function returned FAILURE!\n");
                return RST_FAILURE;
            }
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstSetPortBPDUGuard                        */
/*                                                                           */
/*    Description               : This function reselects topology when      */
/*                                bpdu guard is set by management and        */
/*                                configures the port rstp status            */
/*                                                                           */
/*    Input(s)                  : u4PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType -BPDU guard                      */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS On Success.                    */
/*                                RST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstSetPortBPDUGuard (UINT4 u4PortNo, UINT4 u4GuardType, UINT4 u4BpduGuardStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2InstIndex = RST_DEFAULT_INSTANCE;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    tAstCfaIfInfo       IfInfo;
    tNotifyProtoToApp   NotifyProtoToApp;
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    AST_MEMSET (&IfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);

    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return RST_FAILURE;
    }
    if (u4GuardType == AST_RST_CONFIG_BPDUGUARD_PORT_MSG)
    {
        if (pCommPortInfo->u4BpduGuard == u4BpduGuardStatus)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                          "SYS: RstpStatus: BPDU Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG,
                          "SYS: BPDU Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            return RST_SUCCESS;
        }
        pCommPortInfo->u4BpduGuard = u4BpduGuardStatus;

        if (u4BpduGuardStatus != RST_TRUE)
        {
            if ((pCommPortInfo->bBpduInconsistent == RST_TRUE) &&
                (pCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN))
            {
                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                  AST_GET_IFINDEX_STR ((UINT2) u4PortNo),
                                  au1TimeStr);
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_UP;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u4PortNo);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
            }
            else
            {
                AstL2IwfGetPortOperStatus (STP_MODULE, u4PortNo, &u1OperStatus);
                if (u1OperStatus == AST_PORT_OPER_UP)
                {

                    AstEnablePort ((UINT2) u4PortNo, u2InstIndex,
                                   AST_EXT_PORT_UP);
                }
            }
            /* BpduInconsistent reset when Bpdu guard is disabled */
            pCommPortInfo->bBpduInconsistent = RST_FALSE;
            /*Stop the Error Disable Recovery timer if it is running */
            if (pCommPortInfo->pDisableRecoveryTmr != NULL)
            {
                if (AstStopTimer (pPortEntry,
                                  AST_TMR_TYPE_ERROR_DISABLE_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ERROR"
                             " DISABLE RECOVERY Timer!\n");
                    return RST_FAILURE;
                }
            }

        }

    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstSetGlobalBPDUGuard                      */
/*                                                                           */
/*    Description               : This function reselects topology when      */
/*                                global bpdu guard is set by management and */
/*                                triggers reselection for the edge port     */
/*                                                                           */
/*    Input(s)                  : u4BpduGuardStatus -Global BPDU Guard status*/
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS On Success.                    */
/*                                RST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
AstSetGlobalBPDUGuard (UINT4 u4BpduGuardStatus)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstCfaIfInfo       IfInfo;
    UINT2               u2PortNum;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    AST_MEMSET (&IfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
        if ((pPortInfo == NULL) || (pCommPortInfo == NULL))
        {
            continue;
        }
        if (u4BpduGuardStatus == AST_BPDUGUARD_DISABLE)
        {
            if ((pCommPortInfo->u4BpduGuard) == AST_BPDUGUARD_NONE)
            {
                /* BpduInconsistent reset when Bpdu guard is disabled */
                if (pCommPortInfo->bBpduInconsistent == AST_TRUE)
                {
                    pCommPortInfo->bBpduInconsistent = AST_FALSE;
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      au1TimeStr);
                    if (AstCfaGetIfInfo (pPortInfo->u4IfIndex, &IfInfo) !=
                        CFA_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                                      "SYS: Cannot Get IfType for Port %u\n",
                                      pPortInfo->u4IfIndex);
                        return RST_SUCCESS;
                    }
                    if (IfInfo.u1IfOperStatus == AST_PORT_OPER_UP)
                    {
                        AstEnablePort ((UINT2) u2PortNum, RST_DEFAULT_INSTANCE,
                                       AST_EXT_PORT_UP);
                    }

                }
            }
        }
    }
    AST_GBL_BPDUGUARD_STATUS = u4BpduGuardStatus;
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : RstSetPortBPDUGuardAction                  */
/*                                                                           */
/*    Description               : This function reselects topology when      */
/*                                bpdu guard is set by management and        */
/*                                configures the port rstp status            */
/*                                                                           */
/*    Input(s)                  : u4PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS On Success.                    */
/*                                RST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
RstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);

    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return RST_FAILURE;
    }
    pCommPortInfo->u1BpduGuardAction = u1BpduGuardAction;

    return RST_SUCCESS;
}

#endif /* RSTP_WANTED */
