/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1w1lw.c,v 1.12 2015/10/12 11:11:12 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "std1w1lw.h"
# include  "fsmsrslw.h"
# include  "fsmsbrlw.h"
# include "rstp.h"
# include "asthdrs.h"

#ifdef MSTP_WANTED
# include  "astminc.h"
# include "stpcli.h"
# include "cli.h"
# include "fsmpmscli.h"
#endif
           /* LOW LEVEL Routines for Table : Ieee8021SpanningTreeTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021SpanningTreeTable
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021SpanningTreeTable (UINT4
                                                   u4Ieee8021SpanningTreeComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    i4RetVal = nmhValidateIndexInstanceFsDot1dStpExtTable
        ((INT4) u4Ieee8021SpanningTreeComponentId);

    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021SpanningTreeTable
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021SpanningTreeTable (UINT4
                                           *pu4Ieee8021SpanningTreeComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    i4RetVal = nmhGetFirstIndexFsDot1dStpExtTable
        ((INT4 *) pu4Ieee8021SpanningTreeComponentId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021SpanningTreeComponentId));

    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021SpanningTreeTable
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId
                nextIeee8021SpanningTreeComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021SpanningTreeTable (UINT4
                                          u4Ieee8021SpanningTreeComponentId,
                                          UINT4
                                          *pu4NextIeee8021SpanningTreeComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    i4RetVal = nmhGetNextIndexFsDot1dStpTable ((INT4)
                                               u4Ieee8021SpanningTreeComponentId,
                                               (INT4 *)
                                               pu4NextIeee8021SpanningTreeComponentId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021SpanningTreeComponentId));
    return (INT1) i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeProtocolSpecification
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeProtocolSpecification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeProtocolSpecification (UINT4
                                                 u4Ieee8021SpanningTreeComponentId,
                                                 INT4
                                                 *pi4RetValIeee8021SpanningTreeProtocolSpecification)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpProtocolSpecification ((INT4)
                                                          u4Ieee8021SpanningTreeComponentId,
                                                          pi4RetValIeee8021SpanningTreeProtocolSpecification);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        *pi4RetValIeee8021SpanningTreeProtocolSpecification = AST_IEEE8021q;
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePriority
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePriority (UINT4 u4Ieee8021SpanningTreeComponentId,
                                    INT4 *pi4RetValIeee8021SpanningTreePriority)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhGetFsDot1dStpPriority ((INT4)
                                             u4Ieee8021SpanningTreeComponentId,
                                             pi4RetValIeee8021SpanningTreePriority);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhGetFsMIMstCistBridgePriority ((INT4)
                                                    u4Ieee8021SpanningTreeComponentId,
                                                    pi4RetValIeee8021SpanningTreePriority);

    }
#endif
    else
    {
	*pi4RetValIeee8021SpanningTreePriority = (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_PRIORITY);
    }
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeTimeSinceTopologyChange
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeTimeSinceTopologyChange (UINT4
                                                   u4Ieee8021SpanningTreeComponentId,
                                                   UINT4
                                                   *pu4RetValIeee8021SpanningTreeTimeSinceTopologyChange)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpTimeSinceTopologyChange ((INT4)
                                                            u4Ieee8021SpanningTreeComponentId,
                                                            pu4RetValIeee8021SpanningTreeTimeSinceTopologyChange);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistTimeSinceTopologyChange ((INT4)
                                                             u4Ieee8021SpanningTreeComponentId,
                                                             pu4RetValIeee8021SpanningTreeTimeSinceTopologyChange);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeTopChanges
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeTopChanges (UINT4 u4Ieee8021SpanningTreeComponentId,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValIeee8021SpanningTreeTopChanges)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpTopChanges ((INT4)
                                               u4Ieee8021SpanningTreeComponentId,
                                               (UINT4 *)
                                               &
                                               (pu8RetValIeee8021SpanningTreeTopChanges->
                                                lsn));
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistTopChanges ((INT4)
                                                u4Ieee8021SpanningTreeComponentId,
                                                (UINT4 *)
                                                &
                                                (pu8RetValIeee8021SpanningTreeTopChanges->
                                                 lsn));
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeDesignatedRoot
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeDesignatedRoot (UINT4
                                          u4Ieee8021SpanningTreeComponentId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIeee8021SpanningTreeDesignatedRoot)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpDesignatedRoot
            ((UINT1) u4Ieee8021SpanningTreeComponentId,
             pRetValIeee8021SpanningTreeDesignatedRoot);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        /* Code change for accessing the current CIST Root Switch */
        tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
        UINT1              *pu1List = NULL;
        UINT2               u2Val = 0;

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);
        if (pPerStBrgInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        pu1List = pRetValIeee8021SpanningTreeDesignatedRoot->pu1_OctetList;

        u2Val = pPerStBrgInfo->RootId.u2BrgPriority;
        AST_PUT_2BYTE (pu1List, u2Val);
        AST_PUT_MAC_ADDR (pu1List, pPerStBrgInfo->RootId.BridgeAddr);

        pRetValIeee8021SpanningTreeDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRootCost
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRootCost (UINT4 u4Ieee8021SpanningTreeComponentId,
                                    INT4 *pi4RetValIeee8021SpanningTreeRootCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpRootCost ((INT4) u4Ieee8021SpanningTreeComponentId,
                                      pi4RetValIeee8021SpanningTreeRootCost);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistRootCost ((INT4) u4Ieee8021SpanningTreeComponentId,
                                       pi4RetValIeee8021SpanningTreeRootCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRootPort
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRootPort (UINT4 u4Ieee8021SpanningTreeComponentId,
                                    UINT4
                                    *pu4RetValIeee8021SpanningTreeRootPort)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpRootPort ((INT4) u4Ieee8021SpanningTreeComponentId,
                                      (INT4 *)
                                      pu4RetValIeee8021SpanningTreeRootPort);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistRootPort ((INT4) u4Ieee8021SpanningTreeComponentId,
                                       (INT4 *)
                                       pu4RetValIeee8021SpanningTreeRootPort);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeMaxAge
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeMaxAge (UINT4 u4Ieee8021SpanningTreeComponentId,
                                  INT4 *pi4RetValIeee8021SpanningTreeMaxAge)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpMaxAge ((INT4) u4Ieee8021SpanningTreeComponentId,
                                    pi4RetValIeee8021SpanningTreeMaxAge);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistMaxAge ((INT4) u4Ieee8021SpanningTreeComponentId,
                                     pi4RetValIeee8021SpanningTreeMaxAge);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeHelloTime
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeHelloTime (UINT4 u4Ieee8021SpanningTreeComponentId,
                                     INT4
                                     *pi4RetValIeee8021SpanningTreeHelloTime)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpHelloTime ((INT4) u4Ieee8021SpanningTreeComponentId,
                                       pi4RetValIeee8021SpanningTreeHelloTime);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistHelloTime ((INT4)
                                        u4Ieee8021SpanningTreeComponentId,
                                        pi4RetValIeee8021SpanningTreeHelloTime);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeHoldTime
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeHoldTime (UINT4 u4Ieee8021SpanningTreeComponentId,
                                    INT4 *pi4RetValIeee8021SpanningTreeHoldTime)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpHoldTime ((INT4) u4Ieee8021SpanningTreeComponentId,
                                      pi4RetValIeee8021SpanningTreeHoldTime);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistHoldTime ((INT4) u4Ieee8021SpanningTreeComponentId,
                                       pi4RetValIeee8021SpanningTreeHoldTime);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeForwardDelay
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeForwardDelay (UINT4 u4Ieee8021SpanningTreeComponentId,
                                        INT4
                                        *pi4RetValIeee8021SpanningTreeForwardDelay)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpForwardDelay ((INT4)
                                          u4Ieee8021SpanningTreeComponentId,
                                          pi4RetValIeee8021SpanningTreeForwardDelay);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistForwardDelay ((INT4)
                                           u4Ieee8021SpanningTreeComponentId,
                                           pi4RetValIeee8021SpanningTreeForwardDelay);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeBridgeMaxAge
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeBridgeMaxAge (UINT4 u4Ieee8021SpanningTreeComponentId,
                                        INT4
                                        *pi4RetValIeee8021SpanningTreeBridgeMaxAge)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal =
            nmhGetFsDot1dStpBridgeMaxAge ((INT4)
                                          u4Ieee8021SpanningTreeComponentId,
                                          pi4RetValIeee8021SpanningTreeBridgeMaxAge);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal =
            nmhGetFsMIMstCistBridgeMaxAge ((INT4)
                                           u4Ieee8021SpanningTreeComponentId,
                                           pi4RetValIeee8021SpanningTreeBridgeMaxAge);

    }
#endif
    else
    {
	*pi4RetValIeee8021SpanningTreeBridgeMaxAge = (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_MAX_AGE);
    }
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeBridgeHelloTime
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeBridgeHelloTime (UINT4
                                           u4Ieee8021SpanningTreeComponentId,
                                           INT4
                                           *pi4RetValIeee8021SpanningTreeBridgeHelloTime)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpBridgeHelloTime ((INT4)
                                                    u4Ieee8021SpanningTreeComponentId,
                                                    pi4RetValIeee8021SpanningTreeBridgeHelloTime);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhGetFsMIMstCistBridgeHelloTime ((INT4)
                                                     u4Ieee8021SpanningTreeComponentId,
                                                     pi4RetValIeee8021SpanningTreeBridgeHelloTime);

    }
#endif
    else
    {
	*pi4RetValIeee8021SpanningTreeBridgeHelloTime = (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_HELLO_TIME);	
    }
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeBridgeForwardDelay
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeBridgeForwardDelay (UINT4
                                              u4Ieee8021SpanningTreeComponentId,
                                              INT4
                                              *pi4RetValIeee8021SpanningTreeBridgeForwardDelay)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhGetFsDot1dStpBridgeForwardDelay ((INT4)
                                                       u4Ieee8021SpanningTreeComponentId,
                                                       pi4RetValIeee8021SpanningTreeBridgeForwardDelay);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistBridgeForwardDelay ((INT4)
                                                        u4Ieee8021SpanningTreeComponentId,
                                                        pi4RetValIeee8021SpanningTreeBridgeForwardDelay);

    }
#endif
    else 
    {
	*pi4RetValIeee8021SpanningTreeBridgeForwardDelay = (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_FWD_DELAY);	
    }

    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeVersion
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeVersion (UINT4 u4Ieee8021SpanningTreeComponentId,
                                   INT4 *pi4RetValIeee8021SpanningTreeVersion)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpVersion ((INT4)
                                            u4Ieee8021SpanningTreeComponentId,
                                            pi4RetValIeee8021SpanningTreeVersion);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstForceProtocolVersion ((INT4)
                                                      u4Ieee8021SpanningTreeComponentId,
                                                      pi4RetValIeee8021SpanningTreeVersion);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRstpTxHoldCount
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                retValIeee8021SpanningTreeRstpTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRstpTxHoldCount (UINT4
                                           u4Ieee8021SpanningTreeComponentId,
                                           INT4
                                           *pi4RetValIeee8021SpanningTreeRstpTxHoldCount)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    /* This is the equivalent object to ieee8021RstpStpExtTxHoldCount 
     * mentioned in the description portion of ieee8021MstpCistTable (std1s1ap.mib) */

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpTxHoldCount ((INT4)
                                                u4Ieee8021SpanningTreeComponentId,
                                                pi4RetValIeee8021SpanningTreeRstpTxHoldCount);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstTxHoldCount ((INT4)
                                             u4Ieee8021SpanningTreeComponentId,
                                             pi4RetValIeee8021SpanningTreeRstpTxHoldCount);

    }

#endif
    else
    {
	*pi4RetValIeee8021SpanningTreeRstpTxHoldCount = (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_TX_LIMIT);
    }
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreePriority
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreePriority (UINT4 u4Ieee8021SpanningTreeComponentId,
                                    INT4 i4SetValIeee8021SpanningTreePriority)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal =
            nmhSetFsDot1dStpPriority ((INT4) u4Ieee8021SpanningTreeComponentId,
                                      i4SetValIeee8021SpanningTreePriority);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhSetFsMIMstCistBridgePriority ((INT4)
                                             u4Ieee8021SpanningTreeComponentId,
                                             i4SetValIeee8021SpanningTreePriority);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeBridgeMaxAge
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreeBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeBridgeMaxAge (UINT4 u4Ieee8021SpanningTreeComponentId,
                                        INT4
                                        i4SetValIeee8021SpanningTreeBridgeMaxAge)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpBridgeMaxAge ((INT4)
                                                 u4Ieee8021SpanningTreeComponentId,
                                                 i4SetValIeee8021SpanningTreeBridgeMaxAge);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistBridgeMaxAge ((INT4)
                                                  u4Ieee8021SpanningTreeComponentId,
                                                  i4SetValIeee8021SpanningTreeBridgeMaxAge);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeBridgeHelloTime
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreeBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeBridgeHelloTime (UINT4
                                           u4Ieee8021SpanningTreeComponentId,
                                           INT4
                                           i4SetValIeee8021SpanningTreeBridgeHelloTime)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpBridgeHelloTime ((INT4)
                                                    u4Ieee8021SpanningTreeComponentId,
                                                    i4SetValIeee8021SpanningTreeBridgeHelloTime);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhSetFsMIMstCistBridgeHelloTime ((INT4)
                                                     u4Ieee8021SpanningTreeComponentId,
                                                     i4SetValIeee8021SpanningTreeBridgeHelloTime);

    }
#endif

    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeBridgeForwardDelay
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreeBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeBridgeForwardDelay (UINT4
                                              u4Ieee8021SpanningTreeComponentId,
                                              INT4
                                              i4SetValIeee8021SpanningTreeBridgeForwardDelay)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpBridgeForwardDelay ((INT4)
                                                       u4Ieee8021SpanningTreeComponentId,
                                                       i4SetValIeee8021SpanningTreeBridgeForwardDelay);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistBridgeForwardDelay ((INT4)
                                                        u4Ieee8021SpanningTreeComponentId,
                                                        i4SetValIeee8021SpanningTreeBridgeForwardDelay);

    }
#endif

    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeVersion
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreeVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeVersion (UINT4 u4Ieee8021SpanningTreeComponentId,
                                   INT4 i4SetValIeee8021SpanningTreeVersion)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpVersion ((INT4)
                                            u4Ieee8021SpanningTreeComponentId,
                                            i4SetValIeee8021SpanningTreeVersion);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhSetFsMIMstForceProtocolVersion ((INT4)
                                                      u4Ieee8021SpanningTreeComponentId,
                                                      i4SetValIeee8021SpanningTreeVersion);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeRstpTxHoldCount
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                setValIeee8021SpanningTreeRstpTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeRstpTxHoldCount (UINT4
                                           u4Ieee8021SpanningTreeComponentId,
                                           INT4
                                           i4SetValIeee8021SpanningTreeRstpTxHoldCount)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    /* This is the equivalent object to ieee8021RstpStpExtTxHoldCount 
     * mentioned in the description portion of ieee8021MstpCistTable (std1s1ap.mib) */

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhSetFsDot1dStpTxHoldCount ((INT4)
                                                u4Ieee8021SpanningTreeComponentId,
                                                i4SetValIeee8021SpanningTreeRstpTxHoldCount);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstTxHoldCount ((INT4)
                                             u4Ieee8021SpanningTreeComponentId,
                                             i4SetValIeee8021SpanningTreeRstpTxHoldCount);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreePriority
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreePriority (UINT4 *pu4ErrorCode,
                                       UINT4 u4Ieee8021SpanningTreeComponentId,
                                       INT4
                                       i4TestValIeee8021SpanningTreePriority)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpPriority (pu4ErrorCode,
                                                (INT4)
                                                u4Ieee8021SpanningTreeComponentId,
                                                i4TestValIeee8021SpanningTreePriority);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if ((i4TestValIeee8021SpanningTreePriority < AST_BRGPRIORITY_MIN_VAL)
            || (i4TestValIeee8021SpanningTreePriority >
                AST_BRGPRIORITY_MAX_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        if (AST_IS_I_COMPONENT () == RST_TRUE)
        {
            /* For I Components priority will always be 65535
             * */
            if (i4TestValIeee8021SpanningTreePriority !=
                AST_PB_CVLAN_BRG_PRIORITY)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                AstReleaseContext ();
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (i4TestValIeee8021SpanningTreePriority &
                AST_BRGPRIORITY_PERMISSIBLE_MASK)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                AstReleaseContext ();
                return SNMP_FAILURE;
            }
        }
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeBridgeMaxAge
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreeBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeBridgeMaxAge (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021SpanningTreeComponentId,
                                           INT4
                                           i4TestValIeee8021SpanningTreeBridgeMaxAge)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpBridgeMaxAge (pu4ErrorCode,
                                                    (INT4)
                                                    u4Ieee8021SpanningTreeComponentId,
                                                    i4TestValIeee8021SpanningTreeBridgeMaxAge);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistBridgeMaxAge (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021SpanningTreeComponentId,
                                                     i4TestValIeee8021SpanningTreeBridgeMaxAge);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeBridgeHelloTime
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreeBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeBridgeHelloTime (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021SpanningTreeComponentId,
                                              INT4
                                              i4TestValIeee8021SpanningTreeBridgeHelloTime)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Hello Time Range : .
     * Reference section: IEEE 802.1w 2001 Table 8-3 */

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValIeee8021SpanningTreeBridgeHelloTime)
         != AST_OK)
        || (i4TestValIeee8021SpanningTreeBridgeHelloTime <
            AST_MIN_HELLOTIME_VAL)
        || (i4TestValIeee8021SpanningTreeBridgeHelloTime >
            AST_MAX_IEEE_HELLOTIME_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_BRG_HELLOTIME_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValIeee8021SpanningTreeBridgeHelloTime),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeBridgeForwardDelay
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreeBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021SpanningTreeComponentId,
                                                 INT4
                                                 i4TestValIeee8021SpanningTreeBridgeForwardDelay)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpBridgeForwardDelay (pu4ErrorCode,
                                                          (INT4)
                                                          u4Ieee8021SpanningTreeComponentId,
                                                          i4TestValIeee8021SpanningTreeBridgeForwardDelay);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistBridgeForwardDelay (pu4ErrorCode,
                                                           (INT4)
                                                           u4Ieee8021SpanningTreeComponentId,
                                                           i4TestValIeee8021SpanningTreeBridgeForwardDelay);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeVersion
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreeVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeVersion (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021SpanningTreeComponentId,
                                      INT4 i4TestValIeee8021SpanningTreeVersion)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpVersion (pu4ErrorCode,
                                               (INT4)
                                               u4Ieee8021SpanningTreeComponentId,
                                               i4TestValIeee8021SpanningTreeVersion);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstForceProtocolVersion (pu4ErrorCode,
                                                         (INT4)
                                                         u4Ieee8021SpanningTreeComponentId,
                                                         i4TestValIeee8021SpanningTreeVersion);

    }
#endif

    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeRstpTxHoldCount
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId

                The Object 
                testValIeee8021SpanningTreeRstpTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeRstpTxHoldCount (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021SpanningTreeComponentId,
                                              INT4
                                              i4TestValIeee8021SpanningTreeRstpTxHoldCount)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreeComponentId);

    /* This is the equivalent object to ieee8021RstpStpExtTxHoldCount 
     * mentioned in the description portion of ieee8021MstpCistTable (std1s1ap.mib) */

    if (AstSelectContext (u4Ieee8021SpanningTreeComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpTxHoldCount (pu4ErrorCode,
                                                   (INT4)
                                                   u4Ieee8021SpanningTreeComponentId,
                                                   i4TestValIeee8021SpanningTreeRstpTxHoldCount);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstTxHoldCount (pu4ErrorCode,
                                                (INT4)
                                                u4Ieee8021SpanningTreeComponentId,
                                                i4TestValIeee8021SpanningTreeRstpTxHoldCount);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021SpanningTreeTable
 Input       :  The Indices
                Ieee8021SpanningTreeComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021SpanningTreeTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021SpanningTreePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021SpanningTreePortTable
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021SpanningTreePortTable (UINT4
                                                       u4Ieee8021SpanningTreePortComponentId,
                                                       UINT4
                                                       u4Ieee8021SpanningTreePort)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AST_IS_RST_STARTED ())
    {
        return (nmhValidateIndexInstanceFsDot1dStpPortTable
                ((INT4) u4Ieee8021SpanningTreePort));
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        return (nmhValidateIndexInstanceFsMIMstCistPortTable
                ((INT4) u4Ieee8021SpanningTreePort));
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021SpanningTreePortTable
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021SpanningTreePortTable (UINT4
                                               *pu4Ieee8021SpanningTreePortComponentId,
                                               UINT4
                                               *pu4Ieee8021SpanningTreePort)
{

    UINT2               u2LocalPortId = 0;
    INT4                i4Ret = RST_FAILURE;

    if (AST_IS_RST_STARTED ())
    {
        if (nmhGetFirstIndexFsDot1dStpPortTable ((INT4 *)
                                                 pu4Ieee8021SpanningTreePort) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (nmhGetFirstIndexFsMIMstCistPortTable ((INT4 *)
                                                  pu4Ieee8021SpanningTreePort)
            != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    else
    {
        return SNMP_FAILURE;
    }

    i4Ret = AstGetContextInfoFromIfIndex (*pu4Ieee8021SpanningTreePort,
                                          pu4Ieee8021SpanningTreePortComponentId,
                                          &u2LocalPortId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021SpanningTreePortComponentId));

    UNUSED_PARAM (i4Ret);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021SpanningTreePortTable
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                nextIeee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort
                nextIeee8021SpanningTreePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021SpanningTreePortTable (UINT4
                                              u4Ieee8021SpanningTreePortComponentId,
                                              UINT4
                                              *pu4NextIeee8021SpanningTreePortComponentId,
                                              UINT4 u4Ieee8021SpanningTreePort,
                                              UINT4
                                              *pu4NextIeee8021SpanningTreePort)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4Ret = RST_FAILURE;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AST_IS_RST_STARTED ())
    {
        if (nmhGetNextIndexFsDot1dStpPortTable ((INT4)
                                                u4Ieee8021SpanningTreePort,
                                                (INT4 *)
                                                pu4NextIeee8021SpanningTreePort)
            != SNMP_SUCCESS)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        if (nmhGetNextIndexFsMIMstCistPortTable ((INT4)
                                                 u4Ieee8021SpanningTreePort,
                                                 (INT4 *)
                                                 pu4NextIeee8021SpanningTreePort)
            != SNMP_SUCCESS)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
    }
#endif
    i4Ret = AstGetContextInfoFromIfIndex (*pu4NextIeee8021SpanningTreePort,
                                          pu4NextIeee8021SpanningTreePortComponentId,
                                          &u2LocalPortId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021SpanningTreePortComponentId));
    AstReleaseContext ();

    UNUSED_PARAM (i4Ret);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortPriority
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortPriority (UINT4
                                        u4Ieee8021SpanningTreePortComponentId,
                                        UINT4 u4Ieee8021SpanningTreePort,
                                        INT4
                                        *pi4RetValIeee8021SpanningTreePortPriority)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortPriority (u4Ieee8021SpanningTreePort,
                                                 pi4RetValIeee8021SpanningTreePortPriority);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortPriority (u4Ieee8021SpanningTreePort,
                                                  pi4RetValIeee8021SpanningTreePortPriority);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortState
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortState (UINT4
                                     u4Ieee8021SpanningTreePortComponentId,
                                     UINT4 u4Ieee8021SpanningTreePort,
                                     INT4
                                     *pi4RetValIeee8021SpanningTreePortState)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhGetFsDot1dStpPortState (u4Ieee8021SpanningTreePort,
                                              pi4RetValIeee8021SpanningTreePortState);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhGetFsMIMstCistPortState (u4Ieee8021SpanningTreePort,
                                               pi4RetValIeee8021SpanningTreePortState);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortEnabled
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortEnabled (UINT4
                                       u4Ieee8021SpanningTreePortComponentId,
                                       UINT4 u4Ieee8021SpanningTreePort,
                                       INT4
                                       *pi4RetValIeee8021SpanningTreePortEnabled)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortEnable (u4Ieee8021SpanningTreePort,
                                               pi4RetValIeee8021SpanningTreePortEnabled);

        if (*pi4RetValIeee8021SpanningTreePortEnabled == RST_PORT_ENABLED)
            *pi4RetValIeee8021SpanningTreePortEnabled = AST_SNMP_TRUE;
        else if (*pi4RetValIeee8021SpanningTreePortEnabled == RST_PORT_DISABLED)
            *pi4RetValIeee8021SpanningTreePortEnabled = AST_SNMP_FALSE;
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {

        i4RetVal = nmhGetFsMIMstCistForcePortState (u4Ieee8021SpanningTreePort,
                                                    pi4RetValIeee8021SpanningTreePortEnabled);

        if (*pi4RetValIeee8021SpanningTreePortEnabled == RST_PORT_ENABLED)
            *pi4RetValIeee8021SpanningTreePortEnabled = AST_SNMP_TRUE;
        else if (*pi4RetValIeee8021SpanningTreePortEnabled == RST_PORT_DISABLED)
            *pi4RetValIeee8021SpanningTreePortEnabled = AST_SNMP_FALSE;

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortPathCost (UINT4
                                        u4Ieee8021SpanningTreePortComponentId,
                                        UINT4 u4Ieee8021SpanningTreePort,
                                        INT4
                                        *pi4RetValIeee8021SpanningTreePortPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpPortPathCost32 ((INT4) u4Ieee8021SpanningTreePort,
                                            pi4RetValIeee8021SpanningTreePortPathCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistPortPathCost ((INT4) u4Ieee8021SpanningTreePort,
                                           pi4RetValIeee8021SpanningTreePortPathCost);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortDesignatedRoot
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortDesignatedRoot (UINT4
                                              u4Ieee8021SpanningTreePortComponentId,
                                              UINT4 u4Ieee8021SpanningTreePort,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021SpanningTreePortDesignatedRoot)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpPortDesignatedRoot (u4Ieee8021SpanningTreePort,
                                                pRetValIeee8021SpanningTreePortDesignatedRoot);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistPortDesignatedRoot (u4Ieee8021SpanningTreePort,
                                                 pRetValIeee8021SpanningTreePortDesignatedRoot);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortDesignatedCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortDesignatedCost (UINT4
                                              u4Ieee8021SpanningTreePortComponentId,
                                              UINT4 u4Ieee8021SpanningTreePort,
                                              INT4
                                              *pi4RetValIeee8021SpanningTreePortDesignatedCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortDesignatedCost
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreePortDesignatedCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortDesignatedCost
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreePortDesignatedCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortDesignatedBridge
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortDesignatedBridge (UINT4
                                                u4Ieee8021SpanningTreePortComponentId,
                                                UINT4
                                                u4Ieee8021SpanningTreePort,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pRetValIeee8021SpanningTreePortDesignatedBridge)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortDesignatedBridge
            (u4Ieee8021SpanningTreePort,
             pRetValIeee8021SpanningTreePortDesignatedBridge);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortDesignatedBridge
            (u4Ieee8021SpanningTreePort,
             pRetValIeee8021SpanningTreePortDesignatedBridge);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortDesignatedPort
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetIeee8021SpanningTreePortDesignatedPort (UINT4
                                              u4Ieee8021SpanningTreePortComponentId,
                                              UINT4 u4Ieee8021SpanningTreePort,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021SpanningTreePortDesignatedPort)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhGetFsDot1dStpPortDesignatedPort (u4Ieee8021SpanningTreePort,
                                                pRetValIeee8021SpanningTreePortDesignatedPort);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal =
            nmhGetFsMIMstCistPortDesignatedPort (u4Ieee8021SpanningTreePort,
                                                 pRetValIeee8021SpanningTreePortDesignatedPort);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreePortForwardTransitions
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreePortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreePortForwardTransitions (UINT4
                                                  u4Ieee8021SpanningTreePortComponentId,
                                                  UINT4
                                                  u4Ieee8021SpanningTreePort,
                                                  tSNMP_COUNTER64_TYPE *
                                                  pu8RetValIeee8021SpanningTreePortForwardTransitions)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortForwardTransitions ((INT4)
                                                           u4Ieee8021SpanningTreePort,
                                                           (UINT4 *)
                                                           &
                                                           (pu8RetValIeee8021SpanningTreePortForwardTransitions->
                                                            lsn));

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortForwardTransitions
            (u4Ieee8021SpanningTreePort,
             (UINT4 *)
             &(pu8RetValIeee8021SpanningTreePortForwardTransitions->lsn));

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRstpPortProtocolMigration
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreeRstpPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRstpPortProtocolMigration (UINT4
                                                     u4Ieee8021SpanningTreePortComponentId,
                                                     UINT4
                                                     u4Ieee8021SpanningTreePort,
                                                     INT4
                                                     *pi4RetValIeee8021SpanningTreeRstpPortProtocolMigration)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortProtocolMigration
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortProtocolMigration);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortProtocolMigration
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortProtocolMigration);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRstpPortAdminEdgePort
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreeRstpPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRstpPortAdminEdgePort (UINT4
                                                 u4Ieee8021SpanningTreePortComponentId,
                                                 UINT4
                                                 u4Ieee8021SpanningTreePort,
                                                 INT4
                                                 *pi4RetValIeee8021SpanningTreeRstpPortAdminEdgePort)
{

    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhGetFsDot1dStpPortAdminEdgePort
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortAdminEdgePort);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortAdminEdgeStatus
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortAdminEdgePort);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRstpPortOperEdgePort
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreeRstpPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRstpPortOperEdgePort (UINT4
                                                u4Ieee8021SpanningTreePortComponentId,
                                                UINT4
                                                u4Ieee8021SpanningTreePort,
                                                INT4
                                                *pi4RetValIeee8021SpanningTreeRstpPortOperEdgePort)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortOperEdgePort
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortOperEdgePort);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortOperEdgeStatus
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortOperEdgePort);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021SpanningTreeRstpPortAdminPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                retValIeee8021SpanningTreeRstpPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021SpanningTreeRstpPortAdminPathCost (UINT4
                                                 u4Ieee8021SpanningTreePortComponentId,
                                                 UINT4
                                                 u4Ieee8021SpanningTreePort,
                                                 INT4
                                                 *pi4RetValIeee8021SpanningTreeRstpPortAdminPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhGetFsDot1dStpPortAdminPathCost
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortAdminPathCost);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhGetFsMIMstCistPortAdminPathCost
            (u4Ieee8021SpanningTreePort,
             pi4RetValIeee8021SpanningTreeRstpPortAdminPathCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreePortPriority
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreePortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreePortPriority (UINT4
                                        u4Ieee8021SpanningTreePortComponentId,
                                        UINT4 u4Ieee8021SpanningTreePort,
                                        INT4
                                        i4SetValIeee8021SpanningTreePortPriority)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpPortPriority (u4Ieee8021SpanningTreePort,
                                                 i4SetValIeee8021SpanningTreePortPriority);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistPortPriority
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreePortPriority);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreePortEnabled
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreePortEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreePortEnabled (UINT4
                                       u4Ieee8021SpanningTreePortComponentId,
                                       UINT4 u4Ieee8021SpanningTreePort,
                                       INT4
                                       i4SetValIeee8021SpanningTreePortEnabled)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    if (i4SetValIeee8021SpanningTreePortEnabled == AST_SNMP_TRUE)
        i4SetValIeee8021SpanningTreePortEnabled = RST_PORT_ENABLED;
    else if (i4SetValIeee8021SpanningTreePortEnabled == AST_SNMP_FALSE)
        i4SetValIeee8021SpanningTreePortEnabled = RST_PORT_DISABLED;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpPortEnable (u4Ieee8021SpanningTreePort,
                                               i4SetValIeee8021SpanningTreePortEnabled);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistForcePortState
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreePortEnabled);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreePortPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreePortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreePortPathCost (UINT4
                                        u4Ieee8021SpanningTreePortComponentId,
                                        UINT4 u4Ieee8021SpanningTreePort,
                                        INT4
                                        i4SetValIeee8021SpanningTreePortPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal =
            nmhSetFsDot1dStpPortPathCost32 ((INT4) u4Ieee8021SpanningTreePort,
                                            i4SetValIeee8021SpanningTreePortPathCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistPortPathCost
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreePortPathCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeRstpPortProtocolMigration
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreeRstpPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeRstpPortProtocolMigration (UINT4
                                                     u4Ieee8021SpanningTreePortComponentId,
                                                     UINT4
                                                     u4Ieee8021SpanningTreePort,
                                                     INT4
                                                     i4SetValIeee8021SpanningTreeRstpPortProtocolMigration)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpPortProtocolMigration
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortProtocolMigration);

    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistPortProtocolMigration
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortProtocolMigration);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeRstpPortAdminEdgePort
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreeRstpPortAdminEdgePort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeRstpPortAdminEdgePort (UINT4
                                                 u4Ieee8021SpanningTreePortComponentId,
                                                 UINT4
                                                 u4Ieee8021SpanningTreePort,
                                                 INT4
                                                 i4SetValIeee8021SpanningTreeRstpPortAdminEdgePort)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpPortAdminEdgePort
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortAdminEdgePort);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistPortAdminEdgeStatus
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortAdminEdgePort);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetIeee8021SpanningTreeRstpPortAdminPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                setValIeee8021SpanningTreeRstpPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021SpanningTreeRstpPortAdminPathCost (UINT4
                                                 u4Ieee8021SpanningTreePortComponentId,
                                                 UINT4
                                                 u4Ieee8021SpanningTreePort,
                                                 INT4
                                                 i4SetValIeee8021SpanningTreeRstpPortAdminPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021SpanningTreePortComponentId);

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhSetFsDot1dStpPortAdminPathCost
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortAdminPathCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhSetFsMIMstCistPortAdminPathCost
            (u4Ieee8021SpanningTreePort,
             i4SetValIeee8021SpanningTreeRstpPortAdminPathCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreePortPriority
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreePortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreePortPriority (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021SpanningTreePortComponentId,
                                           UINT4 u4Ieee8021SpanningTreePort,
                                           INT4
                                           i4TestValIeee8021SpanningTreePortPriority)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhTestv2FsDot1dStpPortPriority (pu4ErrorCode,
                                                    u4Ieee8021SpanningTreePort,
                                                    i4TestValIeee8021SpanningTreePortPriority);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMstCistPortPriority (pu4ErrorCode,
                                                   u4Ieee8021SpanningTreePort,
                                                   i4TestValIeee8021SpanningTreePortPriority);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreePortEnabled
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreePortEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreePortEnabled (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Ieee8021SpanningTreePortComponentId,
                                          UINT4 u4Ieee8021SpanningTreePort,
                                          INT4
                                          i4TestValIeee8021SpanningTreePortEnabled)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021SpanningTreePortEnabled == AST_SNMP_TRUE)
        i4TestValIeee8021SpanningTreePortEnabled = RST_PORT_ENABLED;
    else if (i4TestValIeee8021SpanningTreePortEnabled == AST_SNMP_FALSE)
        i4TestValIeee8021SpanningTreePortEnabled = RST_PORT_DISABLED;
    else
        return (UINT1) SNMP_FAILURE;

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {

        i4RetVal = nmhTestv2FsDot1dStpPortEnable (pu4ErrorCode,
                                                  u4Ieee8021SpanningTreePort,
                                                  i4TestValIeee8021SpanningTreePortEnabled);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistForcePortState (pu4ErrorCode,
                                                       u4Ieee8021SpanningTreePort,
                                                       i4TestValIeee8021SpanningTreePortEnabled);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreePortPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreePortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreePortPathCost (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021SpanningTreePortComponentId,
                                           UINT4 u4Ieee8021SpanningTreePort,
                                           INT4
                                           i4TestValIeee8021SpanningTreePortPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpPortPathCost32 (pu4ErrorCode,
                                                      (INT4)
                                                      u4Ieee8021SpanningTreePort,
                                                      i4TestValIeee8021SpanningTreePortPathCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistPortPathCost (pu4ErrorCode,
                                                     u4Ieee8021SpanningTreePort,
                                                     i4TestValIeee8021SpanningTreePortPathCost);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeRstpPortProtocolMigration
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreeRstpPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeRstpPortProtocolMigration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021SpanningTreePortComponentId,
                                                        UINT4
                                                        u4Ieee8021SpanningTreePort,
                                                        INT4
                                                        i4TestValIeee8021SpanningTreeRstpPortProtocolMigration)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpPortProtocolMigration
            (pu4ErrorCode,
             u4Ieee8021SpanningTreePort,
             i4TestValIeee8021SpanningTreeRstpPortProtocolMigration);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistPortProtocolMigration (pu4ErrorCode,
                                                              u4Ieee8021SpanningTreePort,
                                                              i4TestValIeee8021SpanningTreeRstpPortProtocolMigration);

    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeRstpPortAdminEdgePort
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreeRstpPortAdminEdgePort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeRstpPortAdminEdgePort (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021SpanningTreePortComponentId,
                                                    UINT4
                                                    u4Ieee8021SpanningTreePort,
                                                    INT4
                                                    i4TestValIeee8021SpanningTreeRstpPortAdminEdgePort)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpPortAdminEdgePort (pu4ErrorCode,
                                                         u4Ieee8021SpanningTreePort,
                                                         i4TestValIeee8021SpanningTreeRstpPortAdminEdgePort);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistPortAdminEdgeStatus (pu4ErrorCode,
                                                            u4Ieee8021SpanningTreePort,
                                                            i4TestValIeee8021SpanningTreeRstpPortAdminEdgePort);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021SpanningTreeRstpPortAdminPathCost
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort

                The Object 
                testValIeee8021SpanningTreeRstpPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021SpanningTreeRstpPortAdminPathCost (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021SpanningTreePortComponentId,
                                                    UINT4
                                                    u4Ieee8021SpanningTreePort,
                                                    INT4
                                                    i4TestValIeee8021SpanningTreeRstpPortAdminPathCost)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID
        (u4Ieee8021SpanningTreePortComponentId);

    if (AstValidateComponentId (u4Ieee8021SpanningTreePortComponentId,
                                u4Ieee8021SpanningTreePort) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4Ieee8021SpanningTreePortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        i4RetVal = nmhTestv2FsDot1dStpPortAdminPathCost (pu4ErrorCode,
                                                         u4Ieee8021SpanningTreePort,
                                                         i4TestValIeee8021SpanningTreeRstpPortAdminPathCost);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        i4RetVal = nmhTestv2FsMIMstCistPortAdminPathCost (pu4ErrorCode,
                                                          u4Ieee8021SpanningTreePort,
                                                          i4TestValIeee8021SpanningTreeRstpPortAdminPathCost);
    }
#endif
    AstReleaseContext ();
    return (INT1) i4RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021SpanningTreePortTable
 Input       :  The Indices
                Ieee8021SpanningTreePortComponentId
                Ieee8021SpanningTreePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021SpanningTreePortTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
