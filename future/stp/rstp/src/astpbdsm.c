/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbdsm.c,v 1.14 2015/02/11 10:38:37 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Bridge Detection State Machine. 
 *
 *******************************************************************/

#define _ASTBDSM_C_
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstBrgDetectionMachine                               */
/*                                                                           */
/* Description        : This is the main routine for the Port Bridge         */
/*                      Detection State Machine.                             */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      u2PortNum - The Port Number.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBrgDetectionMachine (UINT2 u2Event, UINT2 u2PortNum)
{
    UINT2               u2State;
    INT4                i4RetVal;

    if (NULL == AST_GET_PORTENTRY (u2PortNum))
    {
        AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                      "BDSM: Event %s: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_BDSM][u2Event]);
        return RST_FAILURE;
    }
    u2State = (UINT2) (AST_GET_COMM_PORT_INFO (u2PortNum))->u1BrgDetSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "BDSM: Port %u: Bridge Detection Machine Called with Event: %s, State: %s\n",
                  u2PortNum,
                  gaaau1AstSemEvent[AST_BDSM][u2Event],
                  gaaau1AstSemState[AST_BDSM][u2State]);

    AST_DBG_ARG3 (AST_BDSM_DBG,
                  "BDSM: Port %u: Bridge Detection Machine Called with Event: %s, State: %s\n",
                  u2PortNum,
                  gaaau1AstSemEvent[AST_BDSM][u2Event],
                  gaaau1AstSemState[AST_BDSM][u2State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_BRGDETSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_BDSM_DBG,
                 "BDSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_BRG_DET_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_BDSM_DBG, "BDSM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_BRG_DET_MACHINE[u2Event][u2State].pAction)) (u2PortNum);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                 "BDSM: Event routine returned FAILURE !!!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstBdSmInit                                          */
/*                                                                           */
/* Description        : This routine initializes the operational value of    */
/*                      the Edge Port parameter to TRUE.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBdSmInit (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /*Check for AdminEdge and call the appropriate functions */
    if (pAstPortEntry->bAdminEdgePort == RST_TRUE)
    {
        if (RstBdSmMakeEdge (u2PortNum) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_BDSM_DBG,
                          "BDSM: Port %s: RstBdSmMakeEdge() call returned failure\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }
    else
    {
        if (RstBdSmMakeNotEdge (u2PortNum) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_BDSM_DBG,
                          "BDSM: Port %s: RstBdSmMakeNotEdge() call returned failure\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstBdSmAdminEdgeOrPortDisabled                      */
/*                                                                           */
/* Description        : This routine is called when Admin Edge status is     */
/*                      configured or when the port is disabled (oper down)  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBdSmAdminEdgeOrPortDisabled (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = &(pAstPortEntry->CommPortInfo);
    pAstPerStRstPortEntry =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if ((pAstPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN) ||
        (pAstPerStRstPortEntry->bPortEnabled == RST_FALSE))
    {
        pAstPerStRstPortEntry->bForward = AST_FALSE;
        pAstPerStRstPortEntry->bLearn = AST_FALSE;

        /* Port has become disabled. Stop EdgeDelayWhile if running */
        if (pCommPortInfo->pEdgeDelayWhileTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pAstPortEntry,
                 AST_TMR_TYPE_EDGEDELAYWHILE) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: AstStopTimer for EdgeDelayWhile Timer FAILED!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }

        /*Check for AdminEdge and call the appropriate functions */
        if (pAstPortEntry->bAdminEdgePort == RST_TRUE)
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "BDSM: Port %s: AdminEdge = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstBdSmMakeEdge (u2PortNum) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: MakeEdge returned FAILURE",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
        else
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "BDSM: Port %s: AdminEdge = FALSE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstBdSmMakeNotEdge (u2PortNum) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: MakeEdge returned FAILURE",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }
    else
    {
        AST_DBG_ARG1 (AST_BDSM_DBG,
                      "BDSM: Port %s: Port not disabled\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstBdSmChkMakeEdge                                   */
/*                                                                           */
/* Description        : This routine checks if the port can transition to    */
/*                      Edge state.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBdSmChkMakeEdge (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if ((pPortEntry->bAutoEdge == RST_TRUE) &&
        (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
        (pCommPortInfo->bSendRstp == RST_TRUE) &&
        (pRstPortInfo->bProposing == RST_TRUE))
    {
        if (RstBdSmMakeEdge (u2PortNum) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %s: MakeEdge returned FAILURE",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;

        }
        return RST_SUCCESS;
    }
    else if (((pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN) ||
              (pRstPortInfo->bPortEnabled == RST_FALSE))
             && (pPortEntry->bAdminEdgePort == RST_TRUE))
    {
        if (RstBdSmMakeEdge (u2PortNum) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %s: MakeEdge returned FAILURE",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
        return RST_SUCCESS;
    }

    AST_DBG_ARG1 (AST_BDSM_DBG,
                  "BDSM: Port %s: All the conditions to move to Edge port are NOT PRESENT\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstBdSmMakeEdge                                      */
/*                                                                           */
/* Description        : This routine initializes the operational value of    */
/*                      the Edge Port parameter to TRUE.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBdSmMakeEdge (UINT2 u2PortNum)
{
    BOOL1               bOperEdge = OSIX_FALSE;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#ifdef MSTP_WANTED
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst;
#endif

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    AST_DBG_ARG1 (AST_BDSM_DBG, "BDSM: Port %s: Moved to state EDGE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AstGetPortOperEdgeStatusFromL2Iwf (u2PortNum, &bOperEdge);

    if (bOperEdge == OSIX_FALSE)
    {
        /* Indicate the L2Iwf Common Database to update the info. 
         * L2Iwf takes care of indicating PNAC about Bridge
         * Detection */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_TRUE);
    }

    (AST_GET_COMM_PORT_INFO (u2PortNum))->u1BrgDetSmState =
        (UINT1) RST_BRGDETSM_STATE_EDGE;
    pPortEntry->bOperEdgePort = RST_TRUE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "BDSM_Edge: Port %s: OperEdge = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (AST_IS_RST_ENABLED ())
    {
        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_OPEREDGE_SET, pPerStPortInfo)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %s: Role Transition Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                          "BDSM: Port %s: Role Transition Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }

        if (RstTopoChMachine (RST_TOPOCHSM_EV_OPEREDGE_SET, pPerStPortInfo)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                          "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (MstPortRoleTransitMachine
                ((UINT1) MST_PROLETRSM_EV_OPEREDGE_SET, pPerStPortInfo,
                 u2MstInst) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                              "BDSM: Port %s: Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }

            if (MstTopologyChMachine
                (MST_TOPOCHSM_EV_OPEREDGE_SET, u2MstInst,
                 pPerStPortInfo) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                              "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }
#endif /* MSTP_WANTED */

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstBdSmMakeNotEdge                                   */
/*                                                                           */
/* Description        : This routine is called whenever a BPDU is received   */
/*                      on this Port. This sets the Operational value of the */
/*                      Edge Port parameter to be False.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstBdSmMakeNotEdge (UINT2 u2PortNum)
{
    BOOL1               bOperEdge = OSIX_FALSE;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#ifdef MSTP_WANTED
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst;
#endif

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    AST_DBG_ARG1 (AST_BDSM_DBG, "BDSM: Port %s: Moved to state NOT_EDGE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AstGetPortOperEdgeStatusFromL2Iwf (u2PortNum, &bOperEdge);

    if (bOperEdge == OSIX_TRUE)
    {
        /* Indicate the L2Iwf Common Database to update the info. 
         * L2Iwf takes care of indicating PNAC about Bridge
         * Detection */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_FALSE);
    }

    /* Because we do not have a port receive sem for rstp we cannot make 
     * operEdge false when bpdu is received. It will be done here. 
     */
    pPortEntry->bOperEdgePort = RST_FALSE;

    (AST_GET_COMM_PORT_INFO (u2PortNum))->u1BrgDetSmState =
        (UINT1) RST_BRGDETSM_STATE_NOT_EDGE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "BDSM_NotEdge: Port %s: OperEdge = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (AST_IS_RST_ENABLED ())
    {
        if (RstTopoChMachine (RST_TOPOCHSM_EV_OPEREDGE_RESET, pPerStPortInfo)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                          "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (MstTopologyChMachine
                (MST_TOPOCHSM_EV_OPEREDGE_RESET, u2MstInst, pPerStPortInfo)
                != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_BDSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                              "BDSM: Port %s: Topology Change Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
        }
    }
#endif /* MSTP_WANTED */

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitBrgDetectionStateMachine                      */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RstInitBrgDetectionStateMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_BRGDETSM_MAX_EVENTS][20] = {
        "BEGIN", "ADMIN_EDGE_SET", "BPDU_RCVD",
        "EDGEDELAYWHILE_EXP", "SENDRSTP_SET", "AUTOEDGE_SET",
        "PORTDISABLED"
    };
    UINT1               aau1SemState[RST_BRGDETSM_MAX_STATES][20] = {
        "EDGE", "NOT_EDGE"
    };
    /* Bridge Detection SEM */

    for (i4Index = 0; i4Index < RST_BRGDETSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_BDSM][i4Index],
                    aau1SemEvent[i4Index], STRLEN(aau1SemEvent[i4Index]));
	gaaau1AstSemEvent[AST_BDSM][i4Index][STRLEN(aau1SemEvent[i4Index])] = '\0';
    }
    for (i4Index = 0; i4Index < RST_BRGDETSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_BDSM][i4Index],
                    aau1SemState[i4Index], STRLEN(aau1SemState[i4Index]));
	gaaau1AstSemState[AST_BDSM][i4Index][STRLEN(aau1SemState[i4Index])] = '\0';
    }

    /* Event 1 BEGIN Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_BEGIN]
        [RST_BRGDETSM_STATE_EDGE].pAction = RstBdSmInit;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_BEGIN]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmInit;

    /* Event 2 ADMIN_EDGE Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_ADMIN_SET]
        [RST_BRGDETSM_STATE_EDGE].pAction = RstBdSmAdminEdgeOrPortDisabled;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_ADMIN_SET]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmAdminEdgeOrPortDisabled;

    /* Event 3 BPDU_RCVD Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_BPDU_RCVD]
        [RST_BRGDETSM_STATE_EDGE].pAction = RstBdSmMakeNotEdge;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_BPDU_RCVD]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = NULL;

    /* Event 4 EDGE_DELAY_WHILE_EXP Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmChkMakeEdge;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP]
        [RST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 5 SENDRSTP_SET Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_SENDRSTP_SET]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmChkMakeEdge;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_SENDRSTP_SET]
        [RST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 6 AUTOEDGE_SET Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_AUTOEDGE_SET]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmChkMakeEdge;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_AUTOEDGE_SET]
        [RST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 7 PORT_DISABLED Event */
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_PORTDISABLED]
        [RST_BRGDETSM_STATE_EDGE].pAction = RstBdSmAdminEdgeOrPortDisabled;
    RST_BRG_DET_MACHINE[RST_BRGDETSM_EV_PORTDISABLED]
        [RST_BRGDETSM_STATE_NOT_EDGE].pAction = RstBdSmAdminEdgeOrPortDisabled;

    AST_DBG (AST_TXSM_DBG, "BDSM: Loaded BDSM SEM successfully\n");

    return;
}

/* End of File */
