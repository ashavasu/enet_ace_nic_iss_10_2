/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rstpnpwr.c,v 1.3 2013/11/14 11:33:46 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              for Hardware API's w.r.t RSTP
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __RSTP_NP_WR_C
#define __RSTP_NP_WR_C

#include "astminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRstpNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
RstpNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRstpNpModInfo = &(pFsHwNp->RstpNpModInfo);

    if (NULL == pRstpNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_STP_NP_HW_INIT:
        {
            tRstpNpWrFsMiStpNpHwInit *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiStpNpHwInit;
            u1RetVal = FsMiStpNpHwInit (pEntry->u4ContextId);
            break;
        }
        case FS_MI_RSTP_NP_INIT_HW:
        {
            tRstpNpWrFsMiRstpNpInitHw *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpNpInitHw;
            FsMiRstpNpInitHw (pEntry->u4ContextId);
            break;
        }
        case FS_MI_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiRstpNpSetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpNpSetPortState;
            u1RetVal =
                FsMiRstpNpSetPortState (pEntry->u4ContextId, pEntry->u4IfIndex,
                                        pEntry->u1Status);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_RSTP_MBSM_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiRstpMbsmNpSetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpSetPortState;
            u1RetVal =
                FsMiRstpMbsmNpSetPortState (pEntry->u4ContextId,
                                            pEntry->u4IfIndex, pEntry->u1Status,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MI_RSTP_MBSM_NP_INIT_HW:
        {
            tRstpNpWrFsMiRstpMbsmNpInitHw *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpInitHw;
            u1RetVal =
                FsMiRstpMbsmNpInitHw (pEntry->u4ContextId, pEntry->pSlotInfo);
            break;
        }
#endif
        case FS_MI_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiRstNpGetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiRstNpGetPortState;
            u1RetVal =
                FsMiRstNpGetPortState (pEntry->u4ContextId, pEntry->u4IfIndex,
                                       pEntry->pu1Status);
            break;
        }
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstpNpSetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstpNpSetPortState;
            u1RetVal =
                FsMiPbRstpNpSetPortState (pEntry->u4ContextId,
                                          pEntry->u4IfIndex, pEntry->SVid,
                                          pEntry->u1Status);
            break;
        }
        case FS_MI_PB_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstNpGetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstNpGetPortState;
            u1RetVal =
                FsMiPbRstNpGetPortState (pEntry->u4ContextId, pEntry->u4IfIndex,
                                         pEntry->SVlanId, pEntry->pu1Status);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstpMbsmNpSetPortState *pEntry = NULL;
            pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstpMbsmNpSetPortState;
            u1RetVal =
                FsMiPbRstpMbsmNpSetPortState (pEntry->u4ContextId,
                                              pEntry->u4IfIndex, pEntry->Svid,
                                              pEntry->u1Status,
                                              pEntry->pSlotInfo);
            break;
        }
#endif /*PB_WANTED */
#endif
#ifdef PBB_WANTED
        case FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS:
        {
            tRstpNpWrFsMiPbbRstHwServiceInstancePtToPtStatus *pEntry = NULL;
            pEntry =
                &pRstpNpModInfo->RstpNpFsMiPbbRstHwServiceInstancePtToPtStatus;
            u1RetVal =
                FsMiPbbRstHwServiceInstancePtToPtStatus (pEntry->u4ContextId,
                                                         pEntry->u4VipIndex,
                                                         pEntry->u4Isid,
                                                         pEntry->
                                                         u1PointToPointStatus);
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __RSTP_NP_WR_C */
