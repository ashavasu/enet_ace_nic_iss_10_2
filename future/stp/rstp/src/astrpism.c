/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrpism.c,v 1.44 2017/11/20 13:12:25 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Information State Event Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortInfoMachine                                   */
/*                                                                           */
/* Description        : This is the main routine for the Port Information    */
/*                      State Machine. This function is responsible for      */
/*                      processing the newly received information in a bpdu  */
/*                      and for calling the Role Selection State Machine.    */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      pRcvdBpdu - Pointer to the Received BPDU structure.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
RstPortInfoMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo,
                    tAstBpdu * pRcvdBpdu)
{
    UINT2               u2State = (UINT2) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) RST_SUCCESS;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Event %s: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_PISM][u2Event]);
        return RST_FAILURE;
    }

    u2State = (UINT2) pPerStPortInfo->u1PinfoSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "PISM: Port %s: Port Info Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_PISM][u2Event],
                  gaaau1AstSemState[AST_PISM][u2State]);

    AST_DBG_ARG3 (AST_PISM_DBG,
                  "PISM: Port %s: Port Info Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                  gaaau1AstSemEvent[AST_PISM][u2Event],
                  gaaau1AstSemState[AST_PISM][u2State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PINFOSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_PISM_DBG,
                 "PISM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_PORT_INFO_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_PISM_DBG, "PISM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_INFO_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo, pRcvdBpdu);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: Event routine returned FAILURE\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmRiWhileExpCurrent                       */
/*                                                                           */
/* Description        : This routine is called when the received info while  */
/*                      timer expires in the 'CURRENT' state.                */
/*                      This routine moves the state machine state to 'AGED' */
/*                      when the port has received information.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4Ticks = 0;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: RCVDINFO expiry, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    OsixGetSysTime (&u4Ticks);
    /*Incrementing the rcvinfowhile count */
    AST_INCR_RCVDINFOWHILE_COUNT (pPerStPortInfo->u2PortNo);
    pAstPortEntry->u4RcvInfoWhileExpTimeStamp = u4Ticks;

    if ((pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED) &&
        (pRstPortInfo->bUpdtInfo == (tAstBoolean) RST_FALSE) &&
        (pRstPortInfo->bRcvdMsg == (tAstBoolean) RST_FALSE))
    {
        if (RstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: MakeAged returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmUpdtInfoCurrent                         */
/*                                                                           */
/* Description        : This routine is called when updt info variable is    */
/*                      set by 'Role Transition State Machine' when this     */
/*                      state machine is in 'CURRENT' state.                 */
/*                      This routine Updates the port information's Port     */
/*                      Priority Vector by changing the state to 'UPDATE'.   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo * pPerStPortInfo,
                              tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo->bSelected == RST_TRUE)
    {
        if (RstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: MakeUpdate returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeDisabled                            */
/*                                                                           */
/* Description        : This routine changes the state of this state machine */
/*                      to DISABLED state.                                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeDisabled (tAstPerStPortInfo * pPerStPortInfo,
                           tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moving to state DISABLED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bRcvdMsg = RST_FALSE;
    pRstPortInfo->bProposing = RST_FALSE;
    pRstPortInfo->bAgree = RST_FALSE;
    pRstPortInfo->bAgreed = RST_FALSE;
    pRstPortInfo->bProposed = RST_FALSE;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    /* On disabling STP LoopInc state to be cleared in case enabled */
    pPortInfo->bLoopInconsistent = RST_FALSE;
    pPortInfo->bRootInconsistent = RST_FALSE;
    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Disabled: Port %s: Proposing = Proposed = Agree = Agreed = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo,
                          (UINT1) AST_TMR_TYPE_RCVDINFOWHILE) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Stop RcvdInfoWhile Timer returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return RST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Disabled: Port %s: rcvdInfoWhile = 0\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_DISABLED;
    pRstPortInfo->bReSelect = (tAstBoolean) RST_TRUE;
    pRstPortInfo->bSelected = (tAstBoolean) RST_FALSE;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSELECT_UPDATE);

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Disabled: Port %s: InfoIs = DISABLED Reselect = TRUE Selected = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_DISABLED;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state DISABLED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortRoleSelectionMachine ((UINT2) RST_PROLESELSM_EV_RESELECT,
                                     (UINT2) RST_DEFAULT_INSTANCE)
        != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "PISM: RoleSelectionMachine returned FAILURE\n");
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: RoleSelectionMachine returned FAILURE\n");
        return RST_FAILURE;
    }

    /* 
     * When Module is disabled, port also considered as disabled and
     * state machine should not moved to aged for disabled port
     * */

    if ((AST_IS_PORT_UP (u2PortNum)) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE) &&
        ((AST_CURR_CONTEXT_INFO ())->bBegin == RST_FALSE) &&
        (AST_GET_SYSTEMACTION == RST_ENABLED))
    {
        if (RstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: MakeAged returned FAILURE",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmClearedBegin                            */
/*                                                                           */
/* Description        : This routine exits from the DISABLED state machine   */
/*                      state when the BEGIN global variable is cleared.     */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmClearedBegin (tAstPerStPortInfo * pPerStPortInfo,
                           tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    AST_UNUSED (pRcvdBpdu);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((AST_IS_PORT_UP (pPerStPortInfo->u2PortNo)) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE))
    {
        pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_AGED;
        pRstPortInfo->bReSelect = RST_TRUE;
        pRstPortInfo->bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "PISM_Aged: Port %s: InfoIs = AGED Reselect = TRUE Selected = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_AGED;

        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: Moved to state AGED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        /* Role selection is triggered once after all ports have handled
         * the BEGIN_CLEARED event so that roles need not be 
         * calculated seperately for each port */
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeAged                                */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'AGED'.                                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeAged (tAstPerStPortInfo * pPerStPortInfo, tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_AGED;

    pRstPortInfo->bReSelect = RST_TRUE;
    pRstPortInfo->bSelected = RST_FALSE;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSELECT_UPDATE);

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Aged: Port %s: InfoIs = AGED Reselect = TRUE Selected = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_AGED;

    /* As the earlier received superior information is aged out, clear the 
     * same from the redundancy database. Otherwise, the old information will 
     * get applied on the standby during bulk updated and the spanning 
     * tree may misbehave for some time.*/
    AST_CLEAR_PDU (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state AGED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_RESELECT,
                                     RST_DEFAULT_INSTANCE) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Role Selection Machine returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: Role Selection Machine returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (pRstPortInfo->bUpdtInfo == RST_TRUE)
    {
        if (RstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: PortInfo Make Update returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeUpdate                              */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'UPDATE'                                             */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeUpdate (tAstPerStPortInfo * pPerStPortInfo,
                         tAstBpdu * pRcvdBpdu)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: UPDATING port info \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bProposed = RST_FALSE;
    pRstPortInfo->bProposing = RST_FALSE;

    /* Disabling transmission of inferior info in case the blocking the port
       had failed earlier.
       Now that the port has become designated it should start sending proper
       spanning tree information so that the toploogy converges. */
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Update: Port %s: Proposing = Proposed = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /* Dont Clear Agreed if the information that is being updated on the port 
     * is going to be better */
    if ((pRstPortInfo->bAgreed == RST_TRUE) &&
        (RstPortInfoSmBetterOrSameInfo (pPerStPortInfo, NULL)) == RST_TRUE)
    {
        pRstPortInfo->bAgreed = RST_TRUE;
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Update: Port %s: Agreed = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
    else
    {
        pRstPortInfo->bAgreed = RST_FALSE;
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Update: Port %s: Agreed = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    if ((pRstPortInfo->bSynced == RST_TRUE) &&
        (pRstPortInfo->bAgreed == RST_TRUE))
    {
        pRstPortInfo->bSynced = RST_TRUE;
        MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Update: Port %s: Synced = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
    else
    {
        pRstPortInfo->bSynced = RST_FALSE;
        MstPUpdtAllSyncedFlag ((UINT2) RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Update: Port %s: Synced = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    /* Copy the Designated Priority Vector to the Port Priority Vector */

    if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
    {
        /* This Bridge is the Root Bridge */
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.BridgeAddr),
                    AST_MAC_ADDR_SIZE);
        pPerStPortInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStPortInfo->u4RootCost = AST_NO_VAL;

    }
    else
    {
        pPerStPortInfo->RootId = pPerStBrgInfo->RootId;
        pPerStPortInfo->u4RootCost = pPerStBrgInfo->u4RootCost;
    }

    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.BridgeAddr),
                AST_MAC_ADDR_SIZE);
    pPerStPortInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    pPerStPortInfo->u2DesgPortId =
        (UINT2) ((AST_GET_LOCAL_PORT (pPortInfo->u2PortNo) &
                  AST_PORTNUM_MASK) | u2Val);
    pPortInfo->PortTimes = pPortInfo->DesgTimes;

    pRstPortInfo->bUpdtInfo = RST_FALSE;
    MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                           MST_SYNC_BSELECT_UPDATE);
    pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_MINE;
    pCommPortInfo->bNewInfo = RST_TRUE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Update: Port %s: UpdtInfo = FALSE InfoIs = MINE NewInfo = TRUE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_UPDATE;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state UPDATE \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (RstPortRoleTrMachine (RST_PROLETRSM_EV_UPDTINFO_RESET,
                              pPerStPortInfo) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "PISM: Port %s: Role Transition Machine Returned Failure!!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: Role Transition Machine Returned Failure!!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (pCommPortInfo->bNewInfo == RST_TRUE)
    {
        if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    (UINT2) RST_DEFAULT_INSTANCE)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    if (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
    {

        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: MakeCurrent returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeCurrent                             */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'CURRENT'                                            */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeCurrent (tAstPerStPortInfo * pPerStPortInfo,
                          tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_CURRENT;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state CURRENT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if ((pRstPortInfo->bSelected == RST_TRUE) &&
        (pRstPortInfo->bUpdtInfo == RST_TRUE))
    {
        if (RstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: MakeCurrent returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeReceive                             */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'RECEIVE'                                            */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeReceive (tAstPerStPortInfo * pPerStPortInfo,
                          tAstBpdu * pRcvdBpdu)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT1               u1RcvdInfo = (UINT1) AST_INIT_VAL;
    UINT1               u1RootInConState = (UINT1) AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    if (pPortInfo == NULL)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u DOES NOT exist..."
                      "Returned Failure \n", u2PortNum);
        return RST_FAILURE;
    }

    if (pRstPortInfo->bUpdtInfo != RST_FALSE)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port%s: bUpdtInfo is TRUE, exiting SEM... \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return RST_SUCCESS;
    }

    u1RcvdInfo = (UINT1) RstPortInfoSmRcvInfo (pPerStPortInfo, pRcvdBpdu);
    pRstPortInfo->u1RcvdInfo = u1RcvdInfo;

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_RECEIVE;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state RECEIVE \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    switch (u1RcvdInfo)
    {
        case RST_SUPERIOR_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %s: SUPERIOR DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: SUPERIOR DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            /* If Root-Guard is enabled, on reception of Superior information 
             * the port's state will be maintained in Discarding  until 
             * the superior information ceases. */
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == RST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr != NULL))
            {
                /* RootInconsistent set, when the port transitions to 
                 * Discarding state.*/
                pPortInfo->bRootInconsistent = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                AstRootIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPortInfo->
                                                            u2PortNo),
                                           pPortInfo->bRootInconsistent,
                                           RST_DEFAULT_INSTANCE,
                                           (INT1 *) AST_BRG_TRAPS_OID,
                                           AST_BRG_TRAPS_OID_LEN);
                if (pRstPortInfo->pRootIncRecTmr != NULL)
                {
                    /*It means Root Inconsistency timer is running, it is 
                     *already in root statistency state*/
                    u1RootInConState = RST_TRUE;
                }

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY,
                     AST_ROOT_INC_RECOVERY_TIME) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Start Root Inconsistency Recovery "
                                  "Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;

                }
                /* Trigger the state machine, only if it was not in 
                 *root inconsistency state (previous state) */
                if (RST_TRUE != u1RootInConState)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Block : Root Guard feature blocking Port: %s at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      au1TimeStr);
                    if (RstPortStateTrMachine
                        (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                         RST_DEFAULT_INSTANCE) != RST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                }
                if (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu) !=
                    RST_SUCCESS)
                {

                    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                  "PISM: Port %u: MakeCurrent "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return RST_FAILURE;
                }
                return RST_SUCCESS;
            }

            if (RstPortInfoSmMakeSuperior (pPerStPortInfo,
                                           pRcvdBpdu) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: MakeSuperior returned FAILURE\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }

            AST_RED_SET_SYNC_FLAG (RST_DEFAULT_INSTANCE);

            break;

        case RST_REPEATED_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %s: REPEATED DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: REPEATED DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "PISM_Receive: Port %s: Received Info = REPEAT_DESG_INFO\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstPortInfoSmMakeRepeat (pPerStPortInfo,
                                         pRcvdBpdu) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: MakeRepeat returned FAILURE\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }
            break;

        case RST_INFERIOR_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %s: INFERIOR DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: INFERIOR DESIGNATED Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "PISM_Receive: Port %s: Received Info = INFERIOR_DESG_INFO\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == RST_TRUE)
                && (pPortInfo->bRootInconsistent == RST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (AstStopTimer ((VOID *) pPerStPortInfo,
                                  (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
                UtlGetTimeStr (au1TimeStr);
                pPortInfo->bRootInconsistent = RST_FALSE;
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);
            }

            if (RstPortInfoSmMakeInferiorDesg (pPerStPortInfo, pRcvdBpdu)
                != RST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeInferiorDesg returned FAILURE\n");
                return RST_FAILURE;
            }

            break;

        case RST_INFERIOR_ROOT_ALT_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %s: INFERIOR ROOT_ALT Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: INFERIOR ROOT_ALT Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "PISM_Receive: Port %s: Received Info = INFERIOR_ROOTALT_INFO\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == RST_TRUE)
                && (pPortInfo->bRootInconsistent == RST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (AstStopTimer ((VOID *) pPerStPortInfo,
                                  (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
                UtlGetTimeStr (au1TimeStr);
                pPortInfo->bRootInconsistent = RST_FALSE;
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), au1TimeStr);
            }

            if (RstPortInfoSmMakeNotDesg (pPerStPortInfo, pRcvdBpdu)
                != RST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeNotDesg returned FAILURE\n");
                return RST_FAILURE;
            }

            if (pRstPortInfo->u1InfoIs != (UINT1) RST_INFOIS_RECEIVED)
            {
                AST_RED_SET_SYNC_FLAG (RST_DEFAULT_INSTANCE);
            }

            break;

        case RST_OTHER_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %s: OTHER Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: OTHER Message Received\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "PISM_Receive: Port %s: Received Info = OTHERINFO\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (pCommPortInfo->bRcvdTcn == RST_TRUE)
            {
                AST_DBG (AST_PISM_DBG,
                         "PISM: Calling Topology Change Machine with RCVDTCN event\n");

                if (RstTopoChMachine (RST_TOPOCHSM_EV_RCVDTCN,
                                      pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                  "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }

            pRstPortInfo->bRcvdMsg = RST_FALSE;
            if (RstPortInfoSmMakeCurrent (pPerStPortInfo,
                                          pRcvdBpdu) != RST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeCurrent returned FAILURE\n");
                return RST_FAILURE;
            }
            break;
        default:
            return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeSuperior                            */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'SUPERIOR'.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeSuperior (tAstPerStPortInfo * pPerStPortInfo,
                           tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;
    UINT2               u2EffectiveAge = 0;
    UINT2               u2Val = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pRstPortInfo->bAgreed = RST_FALSE;
    pRstPortInfo->bProposing = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Superior: Port %s: Agreed = Proposing = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    RstPortInfoSmRecordProposal (pPerStPortInfo, pRcvdBpdu);

    if (RstPortInfoSmSetTcFlags (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: SetTcFlags Returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    /* Dont clear Agree if the information that is being updated on the port 
     * is going to be better */
    if ((pRstPortInfo->bAgree == RST_TRUE) &&
        (RstPortInfoSmBetterOrSameInfo (pPerStPortInfo, pRcvdBpdu) == RST_TRUE))
    {
        pRstPortInfo->bAgree = RST_TRUE;
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Superior: Port %s: Agree = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
    else
    {
        pRstPortInfo->bAgree = RST_FALSE;
        AST_DBG_ARG1 (AST_SM_VAR_DBG, "PISM_Superior: Port %s: Agree = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }

    /* Copy the Message Priority Vector to the Port Priority Vector 
     * only for the not aged out BPDU */

    u2EffectiveAge = pRcvdBpdu->u2MessageAge + (1 * AST_CENTI_SECONDS);
    AST_BPDU_CALC_ROUND_OFF (u2EffectiveAge, u2Val);

    if (u2EffectiveAge <= pRcvdBpdu->u2MaxAge)
    {
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &(pRcvdBpdu->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStPortInfo->RootId.u2BrgPriority = pRcvdBpdu->RootId.u2BrgPriority;

        AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                    &(pRcvdBpdu->DesgBrgId.BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStPortInfo->DesgBrgId.u2BrgPriority =
            pRcvdBpdu->DesgBrgId.u2BrgPriority;
        pPerStPortInfo->u4RootCost = pRcvdBpdu->u4RootPathCost;
        pPerStPortInfo->u2DesgPortId = pRcvdBpdu->u2PortId;
    }
    RstPortInfoSmRecordTimes (pPortInfo, pRcvdBpdu);

    if (RstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: UpdtRcvdInfoWhile returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (u2EffectiveAge <= pRcvdBpdu->u2MaxAge)
    {
        pRstPortInfo->u1InfoIs = (UINT1) RST_INFOIS_RECEIVED;
        pRstPortInfo->bReSelect = RST_TRUE;
        pRstPortInfo->bSelected = RST_FALSE;
        MstPUpdtAllSyncedFlag (RST_DEFAULT_INSTANCE, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
    }
    pRstPortInfo->bRcvdMsg = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "PISM_Superior: Port %s: InfoIs = RECEIVED Reselect = TRUE Selected = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_SUPERIOR;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state SUPERIOR \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    /* Change the State as current here itself */
    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_CURRENT;

    /* If RcvdInfoWhile timer is not running then move to the aged state 
     * untill forwarding state for the port is restricted(Loop-guard). 
     * The RcvdInfoWhile timer is restarted and the forwarding state is restricted,
     * if BPDU is not received till the expiry of edge-delay while timer*/

    if (pRstPortInfo->pRcvdInfoTmr == NULL)
    {
        if ((pPortInfo->bLoopGuard == RST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
            (pPortInfo->bOperPointToPoint == RST_TRUE)
            && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
#ifndef L2RED_WANTED
            u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
            u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
            if (AstStartTimer
                ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                 (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
                 u2RiWhileDuration) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Start RcvdInfoWhile Timer returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = %u\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2RiWhileDuration);
        }
        /* If RcvdInfoWhile timer is not running then move to the aged state */
        else
        {
            return RstPortInfoSmRiWhileExpCurrent (pPerStPortInfo, pRcvdBpdu);
        }
    }

    if (pRstPortInfo->bReSelect == RST_TRUE)
    {
        if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_RESELECT,
                                         RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Role Selection Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Role Selection Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

        if (pRstPortInfo->bProposed == RST_TRUE)
        {
            if (RstPortRoleTrMachine (RST_PROLETRSM_EV_PROPOSED_SET,
                                      pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "PISM: Role Transition Machine returned FAILURE\n");
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: Role Transition Machine returned FAILURE\n");
                return RST_FAILURE;
            }
        }
    }

    return (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeRepeat                              */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'REPEAT'.                                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeRepeat (tAstPerStPortInfo * pPerStPortInfo,
                         tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    RstPortInfoSmRecordProposal (pPerStPortInfo, pRcvdBpdu);

    if (RstPortInfoSmSetTcFlags (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: SetTcFlags Returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    if (RstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo) != RST_SUCCESS)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: UpdtRcvdInfoWhile returned FAILURE\n");
        return RST_FAILURE;
    }

    pRstPortInfo->bRcvdMsg = RST_FALSE;

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_REPEAT;

    /* If RcvdInfoWhile timer is not running then move to the aged state */
    if (pRstPortInfo->pRcvdInfoTmr == NULL)
    {
#ifndef L2RED_WANTED
        u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
        u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
        /* If RcvdInfoWhile timer is not running then move to the aged state
         * untill forwarding state for the port is restricted(Loop-guard). 
         * The RcvdInfoWhile timer is restarted and the forwarding state is restricted,
         * if BPDU is not received till the expiry of edge-delay while timer*/
        if ((pPortInfo->bLoopGuard == RST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
            (pPortInfo->bOperPointToPoint == RST_TRUE)
            && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
            if (AstStartTimer
                ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                 (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
                 u2RiWhileDuration) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Start RcvdInfoWhile Timer returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = %u\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2RiWhileDuration);

        }
        /* If RcvdInfoWhile timer is not running then move to the aged state */
        else
        {
            return RstPortInfoSmRiWhileExpCurrent (pPerStPortInfo, pRcvdBpdu);
        }
    }

    if (pRstPortInfo->bProposed == RST_TRUE)
    {
        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_PROPOSED_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state REPEAT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    return (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeNotDesg                             */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'NOT_DESIGNATED'.                                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeNotDesg (tAstPerStPortInfo * pPerStPortInfo,
                          tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    RstPortInfoSmRecordAgreement (pPerStPortInfo, pRcvdBpdu);

    if (RstPortInfoSmSetTcFlags (pPerStPortInfo, pRcvdBpdu) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: SetTcFlags Returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return RST_FAILURE;
    }

    pRstPortInfo->bRcvdMsg = RST_FALSE;

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_NOT_DESG;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state NOT_DESIGNATED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pRstPortInfo->bAgreed == RST_TRUE)
    {
        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_AGREED_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }
    return (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : RstPortRecordAgreement                               */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordAgreement'procedure as given in     */
/*                      P802.1D/D1 - 2003 Edition.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                     port information.                     */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                              tAstBpdu * pRcvBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = 0;
    UINT4               u4Ticks = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pRcvBpdu->u1Flags & RST_SET_AGREEMENT_FLAG)
    {
        AST_INCR_RX_AGREEMENT_COUNT (pAstPortEntry->u2PortNo);
        OsixGetSysTime (&u4Ticks);
        pAstPortEntry->u4AgreementPktRcvdTimeStamp = u4Ticks;

    }
    if ((AST_FORCE_VERSION == (UINT1) AST_VERSION_2) &&
        (pAstPortEntry->bOperPointToPoint == RST_TRUE) &&
        (pRcvBpdu->u1Flags & RST_SET_AGREEMENT_FLAG))
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: AGREEMENT Received for Port %s\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_PISM_DBG, "PISM: SETTING AGREED FLAG for Port %s \n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        pRstPortInfo->bAgreed = RST_TRUE;
        pRstPortInfo->bProposing = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RecordAgreement: Port %s: Agreed = TRUE Proposing = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
    else
    {
        pRstPortInfo->bAgreed = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "RecordAgreement: Port %s: Agreed = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmMakeInferiorDesg                        */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'INFERIOR_DESIGNATED'.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmMakeInferiorDesg (tAstPerStPortInfo * pPerStPortInfo,
                               tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    RstPortInfoSmRecordDispute (pPerStPortInfo, pRcvdBpdu);
    pRstPortInfo->bRcvdMsg = RST_FALSE;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pPerStPortInfo->u1PinfoSmState = (UINT1) RST_PINFOSM_STATE_INFERIOR_DESG;
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state INFERIOR_DESIGNATED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if (pRstPortInfo->bDisputed == RST_TRUE)
    {
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                          "Port:%s Disputed state occured at : %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          au1TimeStr);
        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "PISM_InferiorDesig: Port %s: Disputed = TRUE Agreed = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_DISPUTED_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

        AST_RED_SET_SYNC_FLAG (RST_DEFAULT_INSTANCE);

    }

    return (RstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmBetterOrSameInfo                        */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'betterorsameinfo' procedure.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU (will be    */
/*                                  NULL is called from PIM: UPDATE state)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUPERIOR_DESG_MSG (or) RST_REPEATED_DESG_MSG (or)*/
/*                      RST_OTHER_MSG (or) RST_FAILURE                       */
/*****************************************************************************/
INT4
RstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                               tAstBpdu * pRcvdBpdu)
{
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBridgeId        MyBridgeId;
    UINT2               u2MyPortId;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* BetterOrSameInfo does not need the NewInfoIs parameter
     * as mentioned in 1D 2004. The parameter pRcvdBpdu is sufficient to 
     * identify whether the value of NewInfoIs is received or mine.
     */

    if (pRcvdBpdu != NULL)
    {
        /* Called from PIM: SUPERIOR */
        if (pRstPortInfo->u1InfoIs != RST_INFOIS_RECEIVED)
        {
            return RST_FALSE;
        }

        i4RetVal = RstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);

        if ((i4RetVal == RST_SUPERIOR_DESG_MSG) || (i4RetVal == RST_SAME_MSG))
        {
            return RST_TRUE;
        }
        else
        {
            return RST_FALSE;
        }
    }
    else
    {
        /* Called from PIM: UPDATE */
        if (pRstPortInfo->u1InfoIs != RST_INFOIS_MINE)
        {
            return RST_FALSE;
        }

        MyBridgeId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        AST_MEMCPY (&(MyBridgeId.BridgeAddr),
                    &((AST_GET_BRGENTRY ())->BridgeAddr), AST_MAC_ADDR_SIZE);

        u2MyPortId = (UINT2) pPerStPortInfo->u1PortPriority;
        u2MyPortId = (UINT2) (u2MyPortId << AST_PORTPRIORITY_BIT_OFFSET);
        u2MyPortId = u2MyPortId | AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);

        i4RetVal = RstCompareBrgId (&(pPerStBrgInfo->RootId),
                                    &(pPerStPortInfo->RootId));
        if (i4RetVal == RST_BRGID1_SUPERIOR)
        {
            return RST_TRUE;
        }
        if (i4RetVal == RST_BRGID1_INFERIOR)
        {
            return RST_FALSE;
        }

        if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
        {
            return RST_TRUE;
        }
        if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
        {
            return RST_FALSE;
        }

        i4RetVal = RstCompareBrgId (&(MyBridgeId),
                                    &(pPerStPortInfo->DesgBrgId));
        if (i4RetVal == RST_BRGID1_SUPERIOR)
        {
            return RST_TRUE;
        }
        if (i4RetVal == RST_BRGID1_INFERIOR)
        {
            return RST_FALSE;
        }

        if (u2MyPortId < pPerStPortInfo->u2DesgPortId)
        {
            return RST_TRUE;
        }
        if (u2MyPortId > pPerStPortInfo->u2DesgPortId)
        {
            return RST_FALSE;
        }
        else
        {
            /* Info is the same */
            return RST_TRUE;
        }
    }
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmRcvInfo                                 */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'rcvdBpdu' procedure.                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUPERIOR_DESG_MSG (or) RST_REPEATED_DESG_MSG (or)*/
/*                      RST_OTHER_MSG (or) RST_FAILURE                       */
/*****************************************************************************/
INT4
RstPortInfoSmRcvInfo (tAstPerStPortInfo * pPerStPortInfo, tAstBpdu * pRcvdBpdu)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2RcvPortNum = 0;
    UINT2               u2DesgPortNum = 0;
    UINT1               u1RcvdPortRole = (UINT1) AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Bridge's own Old superior information received. Discard it. */
    if (AST_MEMCMP (&(pRcvdBpdu->RootId.BridgeAddr[0]),
                    &(pBrgInfo->BridgeAddr[0]), AST_MAC_ADDR_SIZE) == 0)
    {
        if (pRcvdBpdu->RootId.u2BrgPriority != pPerStBrgInfo->u2BrgPriority)
        {
            return RST_OTHER_MSG;
        }
    }

    if (pRcvdBpdu->u1BpduType == AST_BPDU_TYPE_TCN)
    {
        pCommPortInfo->bRcvdTcn = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "SetTcFlags: Port %s: RcvdTcn = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        return RST_OTHER_MSG;
    }

    u1RcvdPortRole = (UINT1) (pRcvdBpdu->u1Flags & RST_FLAG_MASK_PORT_ROLE);

    /* If the port role flag in the bpdu denotes designated port role
     * or if the bpdu type denotes a Config bpdu type consider it to be
     * a designated message */

    if ((u1RcvdPortRole == (UINT1) RST_SET_DESG_PROLE_FLAG) ||
        (pRcvdBpdu->u1BpduType == AST_BPDU_TYPE_CONFIG))
    {
        /* Check if the message is 'Superior Designated',
         * 'Inferior Designated' or 'Repeated Designated'.
         */
        i4RetVal = RstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);
        switch (i4RetVal)
        {
            case RST_BETTER_MSG:
                return RST_SUPERIOR_DESG_MSG;

            case RST_INFERIOR_MSG:
                if (AST_MEMCMP (&(pRcvdBpdu->DesgBrgId.BridgeAddr[0]),
                                &(pPerStPortInfo->DesgBrgId.
                                  BridgeAddr[0]), AST_MAC_ADDR_SIZE) == 0)
                {
                    u2RcvPortNum =
                        (UINT2) (pRcvdBpdu->u2PortId & AST_PORTNUM_MASK);
                    u2DesgPortNum =
                        (UINT2) (pPerStPortInfo->
                                 u2DesgPortId & AST_PORTNUM_MASK);
                    if (u2RcvPortNum == u2DesgPortNum)
                    {
                        /* Classify inferior info received from 
                         * previous Designated port as Superior */
                        return RST_SUPERIOR_DESG_MSG;
                    }
                }

                return RST_INFERIOR_DESG_MSG;

            case RST_SAME_MSG:

                if ((pRcvdBpdu->u2MessageAge !=
                     pPortInfo->PortTimes.u2MsgAgeOrHopCount) ||
                    (pRcvdBpdu->u2MaxAge !=
                     pPortInfo->PortTimes.u2MaxAge) ||
                    (pRcvdBpdu->u2FwdDelay !=
                     pPortInfo->PortTimes.u2ForwardDelay) ||
                    (pRcvdBpdu->u2HelloTime !=
                     pPortInfo->PortTimes.u2HelloTime))
                {
                    return RST_SUPERIOR_DESG_MSG;
                }

                /* This check is not present in 802.1D 2004 but is needed to 
                 * discard bpdus received as a result of a loopback condition. 
                 * This has been incorporated in 802.1Q-REV/Draft 3 */
                if (pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED)
                {
                    return RST_REPEATED_DESG_MSG;
                }
                else
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_BPDU_DBG | AST_PISM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "PISM: Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return RST_INFERIOR_DESG_MSG;
                }

            default:
                /* This condition is not possible */
                return RST_FAILURE;
        }
    }
    else if ((u1RcvdPortRole == (UINT1) RST_SET_ROOT_PROLE_FLAG) ||
             (u1RcvdPortRole == (UINT1) RST_SET_ALTBACK_PROLE_FLAG))
    {
        i4RetVal = RstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);

        if ((i4RetVal == RST_SAME_MSG) || (i4RetVal == RST_INFERIOR_MSG))
        {
            return RST_INFERIOR_ROOT_ALT_MSG;
        }
    }

    return RST_OTHER_MSG;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmGetMsgType                              */
/*                                                                           */
/* Description        : This routine calculates the message type and returns */
/*                      if the message is superior or inferior or same.      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_BETTER_MSG / RST_SAME_MSG / RST_INFERIOR_MSG     */
/*****************************************************************************/
INT4
RstPortInfoSmGetMsgType (tAstPerStPortInfo * pPerStPortInfo,
                         tAstBpdu * pRcvdBpdu)
{
    INT4                i4RetVal = 0;

    i4RetVal = RstCompareBrgId (&(pRcvdBpdu->RootId),
                                &(pPerStPortInfo->RootId));
    switch (i4RetVal)
    {
        case RST_BRGID1_SUPERIOR:
            return RST_BETTER_MSG;

        case RST_BRGID1_INFERIOR:
            return RST_INFERIOR_MSG;

        default:
            break;
    }
    /* Now RST_BRGID1_SAME */
    if (pRcvdBpdu->u4RootPathCost < pPerStPortInfo->u4RootCost)
    {
        return RST_BETTER_MSG;
    }
    else if (pRcvdBpdu->u4RootPathCost > pPerStPortInfo->u4RootCost)
    {
        return RST_INFERIOR_MSG;
    }
    i4RetVal = RstCompareBrgId (&(pRcvdBpdu->DesgBrgId),
                                &(pPerStPortInfo->DesgBrgId));
    switch (i4RetVal)
    {
        case RST_BRGID1_SUPERIOR:
            return RST_BETTER_MSG;
        case RST_BRGID1_INFERIOR:
            return RST_INFERIOR_MSG;
        default:
            break;
    }

    if (pRcvdBpdu->u2PortId < pPerStPortInfo->u2DesgPortId)
    {
        return RST_BETTER_MSG;
    }

    if (pRcvdBpdu->u2PortId > pPerStPortInfo->u2DesgPortId)
    {
        return RST_INFERIOR_MSG;
    }

    return RST_SAME_MSG;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmUpdtBpduVersion                         */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'updtBpduVersion' procedure.               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstPortInfoSmUpdtBpduVersion (tAstPerStPortInfo * pPerStPortInfo,
                              tAstBpdu * pRcvdBpdu)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Updating BPDU version\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    if ((pRcvdBpdu->u1Version == (UINT1) AST_VERSION_0) &&
        ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG) ||
         (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN)))
    {
        pCommPortInfo->bRcvdStp = RST_TRUE;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: Calling Port Migration Machine with RCVD_STP event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_STP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Migration Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Migration Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

    }
    else if ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST) &&
             ((AST_CURR_CONTEXT_INFO ())->u1ForceVersion >=
              (UINT1) AST_VERSION_2))
    {

        pCommPortInfo->bRcvdRstp = RST_TRUE;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: Calling Port Migration Machine with RCVD_RSTP event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_RSTP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Migration Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Migration Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Successfully updated BPDU version\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmSetTcFlags                              */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'setTcFlags' procedure.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RstPortInfoSmSetTcFlags (tAstPerStPortInfo * pPerStPortInfo,
                         tAstBpdu * pRcvdBpdu)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT4               u4Ticks = 0;
    tAstPortEntry      *pAstPortEntry = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Setting Tc flags\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG)) ||
        ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)))
    {
        if ((pRcvdBpdu->u1Flags & RST_FLAG_MASK_TC) != (UINT1) AST_NO_VAL)
        {
            pRstPortInfo->bRcvdTc = RST_TRUE;
            AST_INCR_TC_RX_COUNT (u2PortNum);
            OsixGetSysTime (&u4Ticks);
            pAstPortEntry->u4TCReceivedTimeStamp = u4Ticks;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "SetTcFlags: Port %s: RcvdTc = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: Calling Topology Change Machine with RCVDTC event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (RstTopoChMachine (RST_TOPOCHSM_EV_RCVDTC,
                                  pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }

        if ((pRcvdBpdu->u1Flags & RST_FLAG_MASK_TCACK) != (UINT1) AST_NO_VAL)
        {
            pCommPortInfo->bRcvdTcAck = RST_TRUE;

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "SetTcFlags: Port %s: RcvdTcAck = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_DBG (AST_PISM_DBG,
                     "PISM: Calling Topology Change Machine with RCVDTCACK event\n");

            if (RstTopoChMachine (RST_TOPOCHSM_EV_RCVDTCACK,
                                  pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
    }
    else if (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN)
    {
        pCommPortInfo->bRcvdTcn = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "SetTcFlags: Port %s: RcvdTcn = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG (AST_PISM_DBG,
                 "PISM: Calling Topology Change Machine with RCVDTCN event\n");

        if (RstTopoChMachine (RST_TOPOCHSM_EV_RCVDTCN,
                              pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Successfully set Tc flags\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmRecordProposal                          */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordProposed' procedure.                */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmRecordProposal (tAstPerStPortInfo * pPerStPortInfo,
                             tAstBpdu * pRcvdBpdu)
{
    UINT1               u1TmpFlags = (UINT1) AST_INIT_VAL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT4               u4Ticks = 0;

    /* Proposal should be accepted in StpCompatible mode. It will cause newInfo
     * to be set on this port which will inturn cause a TCN to be sent out thus
     * forcing the adjacent port to step down */
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)
    {
        u1TmpFlags = RST_SET_PROPOSAL_FLAG;
        if ((pRcvdBpdu->u1Flags & u1TmpFlags) == u1TmpFlags)
        {
            AST_INCR_RX_PROPOSAL_COUNT (pPerStPortInfo->u2PortNo);
            OsixGetSysTime (&u4Ticks);
            pPortInfo->u4ProposalPktRcvdTimeStamp = u4Ticks;

        }
        u1TmpFlags = (UINT1) (RST_SET_PROPOSAL_FLAG | RST_SET_DESG_PROLE_FLAG);
        if ((pRcvdBpdu->u1Flags & u1TmpFlags) == u1TmpFlags)
        {
            pRstPortInfo->bProposed = RST_TRUE;
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RecordProposal: Port %s: Proposed = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmUpdtRcvdInfoWhile                       */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'updtRcvdInfoWhile' procedure.             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2EffectiveAge = 0;
    UINT2               u2RiWhileDuration = 0;
    UINT2               u2Val = 0;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Updating RcvdInfoWhile\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    /* As per Standard, 802.1D 2004, "Message Age, incremented by 1 second
     * and rounded to the nearest whole second, does not exceed Max Age*/
    /* So, Round of part is done here */

    u2EffectiveAge = (UINT2) (pPortInfo->PortTimes.u2MsgAgeOrHopCount
                              + (1 * AST_CENTI_SECONDS));

    AST_BPDU_CALC_ROUND_OFF (u2EffectiveAge, u2Val);

    if (u2EffectiveAge <= pPortInfo->PortTimes.u2MaxAge)
    {
#ifndef L2RED_WANTED
        u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
        u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif

        if (AstStartTimer
            ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
             (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
             u2RiWhileDuration) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Start RcvdInfoWhile Timer returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = %u\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2RiWhileDuration);

    }
    else
    {
        if (pRstPortInfo->pRcvdInfoTmr != NULL)
        {
            if (AstStopTimer ((VOID *) pPerStPortInfo,
                              (UINT1) AST_TMR_TYPE_RCVDINFOWHILE)
                != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Stop RcvdInfoWhile Timer returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = 0\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: Effective Age Exceeds MaxAge, Message to be aged out !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        /* u2RiWhileDuration = 0; */
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmRecordTimes                             */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordTimes' procedure as given in        */
/*                      P802.1D/D1 - 2003 Edition.                           */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port information.         */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstPortInfoSmRecordTimes (tAstPortEntry * pPortInfo, tAstBpdu * pRcvdBpdu)
{
    pPortInfo->PortTimes.u2MaxAge = pRcvdBpdu->u2MaxAge;

    if (pRcvdBpdu->u2HelloTime <= AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL))
    {
        pPortInfo->PortTimes.u2HelloTime =
            AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL);
    }
    else
    {
        pPortInfo->PortTimes.u2HelloTime = pRcvdBpdu->u2HelloTime;
    }

    pPortInfo->PortTimes.u2ForwardDelay = pRcvdBpdu->u2FwdDelay;
    pPortInfo->PortTimes.u2MsgAgeOrHopCount = pRcvdBpdu->u2MessageAge;
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmRecordDispute                           */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordDispute' procedure.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                     port information.                     */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                            tAstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    if (pPortInfo == NULL)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u DOES NOT exist..."
                      "Returned Failure \n", pPerStPortInfo->u2PortNo);
        return;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pRcvdBpdu->u1Flags & RST_FLAG_MASK_LEARNING) ||
        ((pRcvdBpdu->u1Version == AST_VERSION_0)
         && (AST_PORT_ROOT_GUARD (pPortInfo) == RST_TRUE)))
    {
        /* This procedure is completely wrong in 802.1D 2004
         * The correct procedure is in 802.1Q-REV/D3 */
        pRstPortInfo->bDisputed = RST_TRUE;
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                          "Port:%s Disputed state occured at : %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          au1TimeStr);
        pPerStPortInfo->PerStRstPortInfo.bAgreed = RST_FALSE;

        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %s: DISPUTED Set...\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    }
}

/*****************************************************************************/
/* Function Name      : RstPortInfoSmEventImpossible                         */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE                                          */
/*****************************************************************************/
INT4
RstPortInfoSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo,
                              tAstBpdu * pRcvdBpdu)
{
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                  "PISM: Port %s: IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                  "PISM: Port %s: IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
    AST_UNUSED (pRcvdBpdu);

    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitPortInfoMachine                               */
/*                                                                           */
/* Description        : Initialises the Port Information State Machine       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RstInitPortInfoMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PINFOSM_MAX_EVENTS][20] = {
        "BEGIN", "BEGIN_CLEARED", "PORT_ENABLED", "PORT_DISABLED",
        "RCVD_BPDU", "RCVDINFOWHILE_EXP", "UPDATE_INFO"
    };
    UINT1               aau1SemState[RST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "NOT_DESG",
        "CURRENT", "RECEIVE", "INFERIOR_DESG"
    };

    for (i4Index = 0; i4Index < RST_PINFOSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PISM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PISM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < RST_PINFOSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PISM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PISM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* Event 0 - RST_PINFOSM_EV_BEGIN */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_DISABLED].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction = RstPortInfoSmMakeDisabled;

    /* Event 1 - RST_PINFOSM_EV_BEGIN_CLEARED */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_DISABLED].pAction = RstPortInfoSmClearedBegin;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_AGED].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_UPDATE].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_REPEAT].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_CURRENT].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_RECEIVE].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_BEGIN_CLEARED]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction = NULL;

    /* Event 2 - RST_PINFOSM_EV_PORT_ENABLED */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_DISABLED].pAction = RstPortInfoSmMakeAged;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_ENABLED]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        RstPortInfoSmEventImpossible;

    /* Event 3 - RST_PINFOSM_EV_PORT_DISABLED */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_DISABLED].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_PORT_DISABLED]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        RstPortInfoSmEventImpossible;

    /* Event 4 - RST_PINFOSM_EV_RCVD_BPDU */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_DISABLED].pAction = RstPortInfoSmMakeDisabled;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmMakeReceive;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVD_BPDU]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        RstPortInfoSmEventImpossible;

    /* Event 5 - RST_PINFOSM_EV_RCVDINFOWHILE_EXP */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_DISABLED].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmRiWhileExpCurrent;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        RstPortInfoSmEventImpossible;

    /* Event 6 - RST_PINFOSM_EV_UPDATEINFO */
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_DISABLED].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_AGED].pAction = RstPortInfoSmMakeUpdate;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_UPDATE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_SUPERIOR].pAction = NULL;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_REPEAT].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_NOT_DESG].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_CURRENT].pAction = RstPortInfoSmUpdtInfoCurrent;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_RECEIVE].pAction = RstPortInfoSmEventImpossible;
    RST_PORT_INFO_MACHINE[RST_PINFOSM_EV_UPDATEINFO]
        [RST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        RstPortInfoSmEventImpossible;

    AST_DBG (AST_TCSM_DBG, "PISM: Loaded PISM SEM successfully\n");
    return;
}
