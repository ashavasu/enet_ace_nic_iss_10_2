/* $Id: std1w1wr.c,v 1.2 2011/12/20 10:55:07 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "std1w1lw.h"
# include  "std1w1wr.h"
# include  "std1w1db.h"
# include "asthdrs.h"

INT4
GetNextIndexIeee8021SpanningTreeTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021SpanningTreeTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021SpanningTreeTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTD1W1 ()
{
    SNMPRegisterMibWithLock (&std1w1OID, &std1w1Entry, AstLock, AstUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&std1w1OID, (const UINT1 *) "std1w1ap");
}

VOID
UnRegisterSTD1W1 ()
{
    SNMPUnRegisterMib (&std1w1OID, &std1w1Entry);
    SNMPDelSysorEntry (&std1w1OID, (const UINT1 *) "std1w1ap");
}

INT4
Ieee8021SpanningTreeProtocolSpecificationGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeProtocolSpecification
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeTimeSinceTopologyChangeGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeTimeSinceTopologyChange
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021SpanningTreeTopChangesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeTopChanges
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021SpanningTreeDesignatedRootGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeDesignatedRoot
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021SpanningTreeRootCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRootCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeRootPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRootPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021SpanningTreeMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeMaxAge
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeHelloTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeHelloTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeHoldTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeForwardDelayGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeForwardDelay
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeBridgeMaxAgeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeBridgeMaxAge
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeBridgeHelloTimeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeBridgeHelloTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeBridgeForwardDelayGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeBridgeForwardDelay
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeVersion
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeRstpTxHoldCountGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRstpTxHoldCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreePriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeMaxAgeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeBridgeMaxAge
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeHelloTimeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeBridgeHelloTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeForwardDelaySet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeBridgeForwardDelay
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeVersion
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpTxHoldCountSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeRstpTxHoldCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreePriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreePriority (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeMaxAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeBridgeMaxAge (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeHelloTimeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeBridgeHelloTime (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021SpanningTreeBridgeForwardDelayTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeBridgeForwardDelay (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Ieee8021SpanningTreeVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeVersion (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpTxHoldCountTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeRstpTxHoldCount (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021SpanningTreeTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021SpanningTreeTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021SpanningTreePortTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021SpanningTreePortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021SpanningTreePortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021SpanningTreePortPriorityGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortStateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortState
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortEnabledGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortEnabled
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortPathCostGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortPathCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortDesignatedRootGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortDesignatedRoot
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021SpanningTreePortDesignatedCostGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortDesignatedCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortDesignatedBridgeGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortDesignatedBridge
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021SpanningTreePortDesignatedPortGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortDesignatedPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021SpanningTreePortForwardTransitionsGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreePortForwardTransitions
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021SpanningTreeRstpPortProtocolMigrationGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRstpPortProtocolMigration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeRstpPortAdminEdgePortGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRstpPortAdminEdgePort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeRstpPortOperEdgePortGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRstpPortOperEdgePort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreeRstpPortAdminPathCostGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021SpanningTreePortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021SpanningTreeRstpPortAdminPathCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021SpanningTreePortPrioritySet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreePortPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortEnabledSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreePortEnabled
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortPathCostSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreePortPathCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortProtocolMigrationSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeRstpPortProtocolMigration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortAdminEdgePortSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeRstpPortAdminEdgePort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortAdminPathCostSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021SpanningTreeRstpPortAdminPathCost
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreePortPriority (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreePortEnabled (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortPathCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreePortPathCost (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortProtocolMigrationTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeRstpPortProtocolMigration (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortAdminEdgePortTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeRstpPortAdminEdgePort (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021SpanningTreeRstpPortAdminPathCostTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021SpanningTreeRstpPortAdminPathCost (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021SpanningTreePortTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021SpanningTreePortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
