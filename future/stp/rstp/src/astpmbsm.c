/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: astpmbsm.c,v 1.21 2013/12/07 11:01:42 siva Exp $
 *
 * Description: This file contains hardware related routines used in VLAN module.
 *
 *******************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : astpmbsm.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : RSTP module                                    */
/*    MODULE NAME           : RSTP module Card Updation                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 21 Jan 2005                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the RSTP module.                               */
/*---------------------------------------------------------------------------*/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstMbsmUpdateCardInsertion                           */
/*                                                                           */
/* Description        : This function programs the HW with the STP software  */
/*                      configuration.                                       */
/*                                                                           */
/*                      This function will be called from the STP module     */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmPortInfo - Structure containing the PortList,  */
/*                                       Starting Index and the port count.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
AstMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4PortCount = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4CurrContext = 0;
    UINT4               u4PortNo = 0;
#ifdef MSTP_WANTED
    UINT2               u2MstInst = 0;
    tVlanId             VlanId;
#endif
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               u1IsSetInPortList = OSIX_FALSE;

    /* For the newly added ports check whether the ports are mapped with an
     * Instance. If so, set the MSTP Instance Port State in the HW. */

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    if (!AstIsRstEnabledInContext (u4ContextId))
    {
        /*Scan for each context created */

        if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
        {
            return MBSM_SUCCESS;
        }
        do
        {
            if (AstSelectContext (u4ContextId) == RST_FAILURE)
            {
                return MBSM_FAILURE;
            }

#ifdef MSTP_WANTED
            if (!AstIsMstEnabledInContext (u4ContextId))
            {
                AstReleaseContext ();
                u4CurrContext = u4ContextId;
                continue;
            }
            if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {

                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    if (MstpFsMiMstpMbsmNpInitHw (u4ContextId, pSlotInfo) ==
                        FNP_FAILURE)
                    {
                        AstReleaseContext ();
                        return MBSM_FAILURE;
                    }
                }

                /*Scan for Each Instance */

                for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES;
                     u2MstInst++)
                {
                    /* Check for the presence of Instances in the system. */
                    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
                    {
                        continue;
                    }

                    /* Create the Instances that are already available in other slots 
                     * of the system. */
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        if (MstpFsMiMstpMbsmNpCreateInstance
                            (u4ContextId, u2MstInst, pSlotInfo) == FNP_FAILURE)
                        {
                            AstReleaseContext ();
                            return MBSM_FAILURE;
                        }
                    }
                }                /*End of Instance FOR Loop */

                /*Scan for Each Vlan */

                for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
                {
                    u2MstInst =
                        AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

                    /* Program the HW to Map the Vlans for the created 
                     * Instances. */
                    if (AstL2IwfMiIsVlanActive (u4ContextId, VlanId) !=
                        OSIX_TRUE)
                    {
                        continue;
                    }

                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        if (MstpFsMiMstpMbsmNpAddVlanInstMapping
                            (u4ContextId, VlanId, u2MstInst,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            AstReleaseContext ();
                            return MBSM_FAILURE;
                        }
                    }
                }                /* End of Vlan For loop */
            }

#endif
            AstReleaseContext ();

            u4CurrContext = u4ContextId;

        }
        while (AstGetNextActiveContext (u4CurrContext, &u4ContextId)
               == RST_SUCCESS);
    }

    /* If not Preconfigured program the defaults. */
    if (MBSM_SLOT_INFO_ISPRECONF (pSlotInfo) == MBSM_FALSE)
    {
        return MBSM_SUCCESS;
    }

    while (u4PortCount != 0)
    {

        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4PortNo,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortList),
                                 u1IsSetInPortListStatus);
        /* PORT INSERT for the corresponding bit in portList */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            if (AstMbsmProgramPortProperties (u4PortNo, pSlotInfo)
                != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }
#ifdef MSTP_WANTED
            if (AstSispMbsmUpdateCardInsertion (u4PortNo, pSlotInfo)
                != MBSM_SUCCESS)
            {
                return MBSM_FAILURE;
            }
#endif
        }
        u4PortCount--;
        u4PortNo++;
    }                            /* End of While loop */

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMbsmProcessUpdateMessage                          */
/*                                                                           */
/* Description        : This function constructs the AST Q Mesg and calls    */
/*                      the AST CONTROL event to process this Mesg.          */
/*                                                                           */
/*                      This function will be called from the RSTP module    */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
AstMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Cmd)
{
    tAstQMsg           *pQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (!(AST_IS_INITIALISED ()))
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if (AST_ALLOC_CFGQ_MSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
#if defined (RSTP_TRAP_WANTED) || defined (MSTP_TRAP_WANTED) || defined (PVRST_TRAP_WANTED)
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
#endif
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "STAP: Message Memory Block Allocation FAILED!!!\n");
        return MBSM_FAILURE;
    }

    AST_MEMSET (pQMsg, AST_MEMSET_VAL, sizeof (tAstQMsg));

    AST_QMSG_TYPE (pQMsg) = (UINT4) i4Cmd;

    if (!(pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg);
        return MBSM_FAILURE;
    }

    MEMCPY (pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));
    if (AST_SEND_TO_QUEUE (AST_CFG_QID, (UINT1 *) &pQMsg, AST_DEF_MSG_LEN)
        == AST_OSIX_SUCCESS)
    {
        if (AST_SEND_EVENT (AST_TASK_ID, AST_MSG_EVENT) != AST_OSIX_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Send Event FAILED!\n");
            return MBSM_FAILURE;
        }
        return MBSM_SUCCESS;
    }
    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC, "MGMT: Send to Q FAILED!\n");

    MEM_FREE (pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg);

    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Message Memory Block FAILED!\n");
    }
    return MBSM_FAILURE;
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstSispMbsmUpdateCardInsertion                       */
/*                                                                           */
/* Description        : This function programs the port state for the SISP   */
/*                      logical interfaces running over this physical        */
/*                      interface. This should be invoked for SISP enabled   */
/*                      interfaces when the card in which this interface is  */
/*                      present is attached.                                 */
/*                                                                           */
/* Input(s)           : u4PortNo - Port Number.                              */
/*                      pSlotInfo - Slot Information used for H/W Pgmming.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
AstSispMbsmUpdateCardInsertion (UINT4 u4PortNo, tMbsmSlotInfo * pSlotInfo)
{
    INT4                i4RetVal = MBSM_SUCCESS;
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = AST_INIT_VAL;
    UINT4               u4PortNum = AST_INIT_VAL;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (MstVcmSispGetSispPortsInfoOfPhysicalPort (u4PortNo, VCM_FALSE,
                                                  (VOID *) au4SispPorts,
                                                  &u4PortCnt) != VCM_SUCCESS)
    {
        /* This port has no logical interfaces or secondary context
         * mapping
         * */
        return MBSM_SUCCESS;
    }

    for (; u4PortNum < u4PortCnt; u4PortNum++)
    {
        /* The following function will select the context and perform
         * based on the same internally. Hence after finishing the operations
         * the context should be restored */
        if (AstMbsmProgramPortProperties (au4SispPorts[u4PortNum], pSlotInfo)
            != MBSM_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }                            /*for all the SISP logical ports */

    return (i4RetVal);
}
#endif

/*****************************************************************************/
/* Function Name      : AstMbsmProgramPortProperties                         */
/*                                                                           */
/* Description        : This function programs the port state for the given  */
/*                      interface. It will also program the spanning tree    */
/*                      enabled status in the hardware for the corresponding */
/*                      context in which this interface is present.          */
/*                                                                           */
/* Input(s)           : u4PortNo - Port Number.                              */
/*                      pSlotInfo - Slot Information used for H/W Pgmming.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
AstMbsmProgramPortProperties (UINT4 u4PortNo, tMbsmSlotInfo * pSlotInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
#ifdef MSTP_WANTED
    UINT2               u2MstInst = AST_INIT_VAL;
#endif
    UINT1               u1PortState = AST_INIT_VAL;

    if (L2IWF_SUCCESS == AstL2IwfIsPortInPortChannel (u4PortNo))
    {
        return MBSM_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4PortNo, &u4ContextId,
                                      &u2LocalPort) == RST_SUCCESS)
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            return MBSM_FAILURE;
        }

        if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE) &&
            AstIsRstEnabledInContext (u4ContextId))
        {
            if (RstpFsMiRstpMbsmNpInitHw (u4ContextId, pSlotInfo) ==
                FNP_FAILURE)
            {
                AstReleaseContext ();
                return MBSM_FAILURE;
            }
        }
        pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);

        if (pAstPortEntry == NULL)
        {
            AstReleaseContext ();
            return MBSM_FAILURE;
        }

        if (AstIsRstEnabledInContext (u4ContextId))
        {
            /* In case of ports other than CEP, check for S-VLAN
             * status.
             * If Cep, then get state for all ports(CEP, PEPs) in the 
             * corresponding C-VLAN and program them in hw. */

            if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
            {
                /* Get the port state of all ports (CEP, PEPs) for the
                 * C-VLAN component having this CEP, and then program 
                 * them hardware.*/

                AstPbMbsmSetCvlanPortStatesInHw (pAstPortEntry, pSlotInfo);
            }
            else
            {
                u1PortState = AstL2IwfGetInstPortState
                    (RST_DEFAULT_INSTANCE, u4PortNo);

                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    if (RstpFsMiRstpMbsmNpSetPortState
                        (u4ContextId, u4PortNo, u1PortState,
                         pSlotInfo) == FNP_FAILURE)
                    {
                        AstReleaseContext ();
                        return MBSM_FAILURE;
                    }
                }
            }
        }
#ifdef MSTP_WANTED
        else if (AstIsMstEnabledInContext (u4ContextId))
        {
            /* In case of ports other than CEP, check for S-VLAN
             * status.
             * If Cep, then get state for all ports(CEP, PEPs) in the 
             * corresponding C-VLAN and program them in hw. */

            if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
            {
                /* Get the port state of all ports (CEP, PEPs) for the
                 * C-VLAN component having this CEP, and then program 
                 * them hardware.*/

                AstPbMbsmSetCvlanPortStatesInHw (pAstPortEntry, pSlotInfo);
            }
            else
            {
                for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES;
                     u2MstInst++)
                {
                    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
                    {
                        continue;
                    }

                    u1PortState = AstL2IwfGetInstPortState (u2MstInst,
                                                            u4PortNo);

                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        if (MstpFsMiMstpMbsmNpSetInstancePortState
                            (u4ContextId, AST_GET_PHY_PORT (u2LocalPort),
                             u2MstInst, u1PortState, pSlotInfo) == FNP_FAILURE)
                        {
                            AstReleaseContext ();
                            return MBSM_FAILURE;
                        }
                    }
                }                /* For all the valid instances */
            }
        }                        /*MSTP ENabled */
#endif
        AstReleaseContext ();
    }

    return MBSM_SUCCESS;
}
