
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbred.c,v 1.24 2016/03/19 12:58:28 siva Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for AST Provider Bridge feature.
 *
 *
 *******************************************************************/
#ifdef L2RED_WANTED
#include "asthdrs.h"
#include "rmgr.h"
#include "cli.h"

/*****************************************************************************/
/* Function Name      : AstRedCVlanCrtPortTblMemPool                         */
/*                                                                           */
/* Description        : Allocate Memory pool for RED C-VLAN port table.      */
/*                                                                           */
/* Input(s)           : u4BlockSize - Size of the Memblocks in the mempool.  */
/*                                                                           */
/* Output(s)          : pRedPortTblPool - Id of the mempool created.         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : AST_MEM_FAILURE / AST_MEM_SUCCESS                     */
/*****************************************************************************/
INT4
AstRedCVlanCrtPortTblMemPool (UINT4 u4BlockSize,
                              tAstMemPoolId * pRedPortTblPool)
{
    /* CVLAN Port Array Mempool */
    if (AST_CREATE_MEM_POOL
        ((u4BlockSize * sizeof (tAstRedPortInfo *)),
         gFsRstpSizingParams[AST_RED_CVLAN_PORT_TBL_SIZING_ID].
         u4PreAllocatedUnits, AST_MEMORY_TYPE,
         pRedPortTblPool) == AST_MEM_FAILURE)
    {
        AST_TRC (AST_OS_RESOURCE_TRC | AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: Memory Pool Creation for Red CVLAN Port Table Failure "
                 "for AST module\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: Memory Pool Creation for Red CVLAN Port Table Failure "
                 "for AST module\n");
        return ((INT4) AST_MEM_FAILURE);
    }

    return AST_MEM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCVlanInit                                    */
/*                                                                           */
/* Description        : Allocate Memory pool for Per Port Inst strucutre for */
/*                      C-VLAN components.                                   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCVlanInit (UINT2 u2CepPortNo)
{
    INT4                i4RetVal = (INT4) AST_MEM_FAILURE;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;

    AST_ALLOC_RED_MEM_BLOCK (AST_RED_CVLAN_COMP_POOL_ID (),
                             AST_RED_CVLAN_COMP (u2CepPortNo),
                             tAstRedContextInfo);

    if (AST_RED_CVLAN_COMP (u2CepPortNo) == NULL)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return RST_FAILURE;
    }

    u4ContextId = AST_CURR_CONTEXT_ID ();

    AstRedPbSelectCVlanContext (u2CepPortNo);

    AST_RED_PARENT_CTXT_ID () = u4ContextId;

    AST_RED_CTXT_CEP_LOCAL_NO () = u2CepPortNo;

    i4RetVal =
        AstRedCVlanCrtPortTblMemPool (AST_MAX_NUM_PORTS,
                                      &(AST_RED_CVLAN_PORT_TBL_POOL_ID ()));

    if (i4RetVal == (INT4) AST_MEM_FAILURE)
    {
        AstRedPbRestoreContext ();
        AST_FREE_RED_MEM_BLOCK (AST_RED_CVLAN_COMP_POOL_ID (),
                                AST_RED_CVLAN_COMP (u2CepPortNo));
        return RST_FAILURE;
    }

    AST_ALLOC_RED_MEM_BLOCK (AST_RED_CVLAN_PORT_TBL_POOL_ID (),
                             AST_RED_CONTEXT_PORT_TBL (), tAstRedPortInfo *);

    if (AST_RED_CONTEXT_PORT_TBL () == NULL)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");

        AST_DELETE_MEM_POOL (AST_RED_CVLAN_PORT_TBL_POOL_ID ());
        AstRedPbRestoreContext ();
        AST_FREE_RED_MEM_BLOCK (AST_RED_CVLAN_COMP_POOL_ID (),
                                AST_RED_CVLAN_COMP (u2CepPortNo));
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCVlanPortTblDeInit                           */
/*                                                                           */
/* Description        : Releases the C-VLAN red port table mempool.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gpAstRedContextInfo                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*****************************************************************************/
INT4
AstRedPbCVlanPortTblDeInit (VOID)
{

    if (AST_RED_CVLAN_PORT_TBL_POOL_ID () != AST_INIT_VAL)
    {
        AST_DELETE_MEM_POOL (AST_RED_CVLAN_PORT_TBL_POOL_ID ());
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbRelCvlanRedCompMem                           */
/*                                                                           */
/* Description        : Free the Red C-VLAN component memory.                */
/*                      CAUTION: This funcion should be called only in       */
/*                               S-VLAN component.                           */
/* Input(s)           : u2Port - port no in S-VLAN component.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo,                                   */
/*                      gpAstRedContextInfo                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbRelCvlanRedCompMem (UINT2 u2Port)
{
    if (AST_RED_CURR_CONTEXT_INFO () != NULL)
    {
        if (AST_RED_CVLAN_COMP (u2Port) != NULL)
        {
            AST_FREE_RED_MEM_BLOCK (AST_RED_CVLAN_COMP_POOL_ID (),
                                    AST_RED_CVLAN_COMP (u2Port));
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbSelectCVlanContext                           */
/* Description        : This function selects the Red C-VLAN component.      */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbSelectCVlanContext (UINT2 u2ParentPort)
{
    if (AST_RED_CURR_CONTEXT_INFO () == NULL)
    {
        return RST_FAILURE;
    }

    if (AST_RED_CVLAN_COMP (u2ParentPort) == NULL)
    {
        return RST_FAILURE;
    }

    AST_RED_PREV_CONTEXT_INFO () = AST_RED_CURR_CONTEXT_INFO ();
    AST_RED_CURR_CONTEXT_INFO () = AST_RED_CVLAN_COMP (u2ParentPort);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbRestoreContext                               */
/* Description        : This function restores the parent Red Context.       */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbRestoreContext (VOID)
{

    /* WARNING : This function should be called in Red CVLAN context only.
       It should be used in CVLAN component of Provider Edge Bridge */
    AST_RED_CURR_CONTEXT_INFO () = AST_RED_PREV_CONTEXT_INFO ();

    AST_RED_PREV_CONTEXT_INFO () = NULL;

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbIncrCvlanPortTableSize                       */
/*                                                                           */
/* Description        : This Function the port array in the Red-CVlan comp.  */
/*                      In the process, it creates a new mempool with        */
/*                      increased Block size and copies the info from        */
/*                      old table to new Red C-VLAN port table.              */
/*                                                                           */
/*                                                                           */
/* Input (s)          :None                                                  */
/*                                                                           */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstRedPbIncrCvlanPortTableSize (VOID)
{
    tAstMemPoolId       PortTblPoolId = 0;
    tAstRedPortInfo   **ppRedPortInfo = NULL;
    UINT4               u4BlockSize = 0;

    /* This function assumes that the current context pointer points to
     * C-VLAN context and the current Red Context ptr points to Red
     * C-VLAN component. */

    /*Allocate Blocksize of Num Port + Pep Block Size */
    u4BlockSize = AST_MAX_NUM_PORTS + AST_PEP_BLOCK_SIZE;

    /* Create Mempool for increased size. */
    if (AstRedCVlanCrtPortTblMemPool (u4BlockSize, &PortTblPoolId)
        == (INT4) AST_MEM_FAILURE)
    {
        return RST_FAILURE;
    }

    /*Allocate Memory from the MemPool */
    AST_ALLOC_RED_MEM_BLOCK (PortTblPoolId, ppRedPortInfo, tAstRedPortInfo *);

    if (ppRedPortInfo == NULL)
    {
        AST_DELETE_MEM_POOL (PortTblPoolId);
        return RST_FAILURE;
    }

    AST_MEMSET (ppRedPortInfo, AST_INIT_VAL,
                (u4BlockSize * sizeof (tAstRedPortInfo *)));

    /* Copying the Existing Port Array into the newly allocated 
     * dynamic array (contiguous mem). */
    AST_MEMCPY (ppRedPortInfo, AST_RED_CONTEXT_PORT_TBL (),
                (AST_MAX_NUM_PORTS) * sizeof (tAstRedPortInfo *));

    /* Releasing Existing Port Array blocks after copying the data. */
    AST_FREE_RED_MEM_BLOCK (AST_RED_CVLAN_PORT_TBL_POOL_ID (),
                            AST_RED_CONTEXT_PORT_TBL ());

    /* Delete the old mempool. */
    AST_DELETE_MEM_POOL (AST_RED_CVLAN_PORT_TBL_POOL_ID ());

    /* Assigning Newly Allocated Mempool Ids to the Red CVLAN component 
     * Mempool. */
    AST_RED_CVLAN_PORT_TBL_POOL_ID () = PortTblPoolId;

    /* Restoring the Port Array. */
    AST_RED_CONTEXT_PORT_TBL () = ppRedPortInfo;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbCreateCvlanPort                              */
/*                                                                           */
/* Description        : Create PB-RED port info for the C-VLAN component.    */
/*                      Warning: This function has to be called only after   */
/*                               the selection Red C-VLAN component.         */
/*                                                                           */
/* Input(s)           : u2LocalPort - Protocol port number for the given     */
/*                                    port.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCreateCvlanPort (UINT2 u2LocalPort)
{
    tAstRedPortInfo    *pAstRedPortEntry = NULL;
    tAstRedPerPortInstInfo *pAstRedPerPortInstInfo = NULL;

    pAstRedPortEntry = AST_RED_CONTEXT_PORT_INFO (u2LocalPort);

    if (pAstRedPortEntry == NULL)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_PORT_INFO_MEMPOOL_ID,
                                 pAstRedPortEntry, tAstRedPortInfo);

        if (pAstRedPortEntry == NULL)
        {
            AST_TRC (ALL_FAILURE_TRC, "MemBlock Allocation FAILED.\n");
            return RST_FAILURE;
        }

        MEMSET (pAstRedPortEntry, AST_INIT_VAL, sizeof (tAstRedPortInfo));

        AST_RED_CONTEXT_PORT_INFO (u2LocalPort) = pAstRedPortEntry;

        /* Allocate Per Port Inst entry. */
        pAstRedPerPortInstInfo = AST_RED_CONTEXT_PERPORT_INST_TBL (u2LocalPort);

        if (pAstRedPerPortInstInfo == NULL)
        {
            AST_ALLOC_RED_MEM_BLOCK (AST_RED_CVLAN_PERST_PORTINFO_POOL_ID (),
                                     pAstRedPerPortInstInfo,
                                     tAstRedPerPortInstInfo);

            if (pAstRedPerPortInstInfo == NULL)
            {
                AST_TRC (ALL_FAILURE_TRC, "MemBlock Allocation FAILED.\n");
                return RST_FAILURE;
            }

            AST_RED_CONTEXT_PERPORT_INST_TBL (u2LocalPort) =
                pAstRedPerPortInstInfo;

            pAstRedPerPortInstInfo = NULL;
        }

        MEMSET (AST_RED_CONTEXT_PERPORT_INST_TBL (u2LocalPort),
                AST_INIT_VAL, sizeof (tAstRedPerPortInstInfo));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbDeleteCvlanPort                              */
/*                                                                           */
/* Description        : Delete PB-RED port info from the C-VLAN component.   */
/*                      Warning: This function has to be called only after   */
/*                               the selection Red C-VLAN component.         */
/*                                                                           */
/* Input(s)           : u2Port - Protocol port number for the given port.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbDeleteCvlanPort (UINT2 u2Port)
{
    tAstBpdu           *pAstPdu = NULL;

    AstRedClearSyncUpDataOnPort (u2Port);

    /* AstRedClearSyncUpDataOnPort clear the Timer related sync up 
     * data for the given port. But clears the dynamic sync up PDU
     * for the port only when the node is active.
     * So if the node is not active we have to clear the PDU sync up
     * data */
    if (AST_NODE_STATUS () != RED_AST_STANDBY)
    {
        if (AST_IS_RST_ENABLED ())
        {
            pAstPdu = AST_RED_RST_PDU_PTR (u2Port);
            if (pAstPdu != NULL)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pAstPdu);
                AST_RED_RST_PDU_PTR (u2Port) = NULL;
            }
        }
    }

    if (AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port) != NULL)
    {
        if (AST_RED_CONTEXT_PERPORT_INST_ENTRY
            (RST_DEFAULT_INSTANCE, u2Port) != NULL)
        {
            AST_FREE_RED_MEM_BLOCK
                (AST_RED_CVLAN_PERST_PORTINFO_POOL_ID (),
                 AST_RED_CONTEXT_PERPORT_INST_ENTRY
                 (RST_DEFAULT_INSTANCE, u2Port));
        }

        AST_RED_CONTEXT_PERPORT_INST_TBL (u2Port) = NULL;
    }

    AST_FREE_RED_MEM_BLOCK (AST_RED_PORT_INFO_MEMPOOL_ID,
                            AST_RED_CONTEXT_PORT_INFO (u2Port));

    AST_RED_CONTEXT_PORT_INFO (u2Port) = NULL;

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbSelCvlanAndGetLocalPort                      */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - Selects the C-VLAN component based on given   */
/*                             CEP port number.                              */
/*                           - Get the local port for the given C-VLAN port  */
/*                             whose protocol port number is given.          */
/*                                                                           */
/* Input(s)           : u2CepPortNo    - CEP port number of the C-VLAN       */
/*                                       component.                          */
/*                      u2ProtocolPort - Protocol port number of the C-VLAN  */
/*                                       port.                               */
/*                                                                           */
/* Output(s)          : pu2LocalPort - Local port number of the C-VLAN port  */
/*                                     identified by the given protocol port.*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbSelCvlanAndGetLocalPort (UINT2 u2CepPortNo,
                                 UINT2 u2ProtocolPort, UINT2 *pu2LocalPort)
{
    tAstPortEntry      *pParentCepPortInfo = NULL;
    tAstPort2IndexMap  *pPort2IndexMap = NULL;

    pParentCepPortInfo = AST_GET_PORTENTRY (u2CepPortNo);

    if (pParentCepPortInfo == NULL)
    {
        AST_DBG (AST_RED_DBG, "AstRedStorePortInfo: "
                 "PB Parent CEP port info. is not present \n");
        return RST_FAILURE;
    }

    if (AstPbSelectCvlanContext (pParentCepPortInfo) != RST_SUCCESS)
    {
        /* FATAL ERROR !!!!! This is Impossible...Port type is CEP
         * and the Port has no cvlan context*/
        AST_DBG (AST_RED_DBG, "AstRedStorePortInfo: "
                 "PB C-VLAN context selection is failed \n");
        return RST_FAILURE;
    }

    /* Sub reference is not invalid and it is not CEP, So it must be
     * a PEP*/

    /* Convert Protocol Port to HLPort */
    pPort2IndexMap =
        AstPbCVlanGetPort2IndexMapBasedOnProtoPort (u2ProtocolPort);

    if (pPort2IndexMap == NULL)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC | AST_RED_DBG,
                 "MSG: Port Not Created in CVLAN context !!!\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                 AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MSG: Port Not Created in CVLAN context !!!\n");
        AstPbRestoreContext ();
        return RST_FAILURE;
    }

    *pu2LocalPort = pPort2IndexMap->u2PortIndex;

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRedPbHandleBulkUptOnCep                           */
/*                                                                           */
/* Description        : This function does the following:                    */
/*                           - Send the bulk update for the given C-VLAN     */
/*                             component.                                    */
/*                           - Get the local port for the given C-VLAN port  */
/*                             whose protocol port number is given.          */
/*                                                                           */
/* Input(s)           : u2CepPortNo    - CEP port number of the C-VLAN       */
/*                                       component.                          */
/*                                                                           */
/* In and Out Params  : ppBuf - Pointer to buffer where the information      */
/*                              needs to be put. If this is null, then a     */
/*                              new buffer will be allocated.                */
/*                      pu4Offset - Offset the above buffer from where the   */
/*                              next write operation needs to be started.    */
/*                      pu4BulkUpdPortCnt - Number of port sync up messages  */
/*                                          that can be sent.                */
/*                                                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstRedGlobalInfo.u4BulkUpdNextPort                  */
/*                      gAstRedGlobalInfo.u4BulkUpdNextCvlanPort             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstRedGlobalInfo.u4BulkUpdNextPort                  */
/*                      gAstRedGlobalInfo.u4BulkUpdNextCvlanPort             */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbHandleBulkUptOnCep (UINT2 u2CepPortNo, VOID **ppBuf, UINT4 *pu4Offset,
                            UINT4 *pu4BulkUpdPortCnt)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2Port = 0;

    pCepPortEntry = AST_GET_PORTENTRY (u2CepPortNo);
    if (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE)
    {
        return;
    }

    for (u2Port = (UINT2) gAstRedGlobalInfo.u4BulkUpdNextCvlanPort;
         u2Port <= (UINT2) AST_MAX_NUM_PORTS; u2Port++)
    {
        pPortEntry = AST_GET_PORTENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }
        /* Oper UP ? */
        if (!AST_IS_PORT_UP (u2Port))
            continue;

        AstRedHandleBulkUpdatePerPort (u2Port, ppBuf, pu4Offset);

        (*pu4BulkUpdPortCnt)--;
        if (*pu4BulkUpdPortCnt == 0)
        {
            break;
        }
    }

    gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = AST_MIN_NUM_PORTS;
    if (u2Port < AST_MAX_NUM_PORTS)
    {
        /* Bulk update needs to be send for some more ports in this
         * C-VLAN component. 
         * This means next time we have to start from this C-VLAN 
         * component - so decrement the already incremented
         * gAstRedGlobalInfo.u4BulkUpdNextPort. */

        gAstRedGlobalInfo.u4BulkUpdNextPort--;
        gAstRedGlobalInfo.u4BulkUpdNextCvlanPort = u2Port + 1;
    }

    AstPbRestoreContext ();
    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbApplyCvlanTimers                             */
/*                                                                           */
/*                      remaining time.                                      */
/*                       Remaining time                                      */
/*                        = [Expected Expiry time - Current time]            */
/*                                                                           */
/* Input(s)           : pParentCepPortInfo - CEP port entry in the S-VLAN    */
/*                                           context.                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbApplyCvlanTimers (tAstPortEntry * pParentPortEntry)
{
    UINT2               u2Port;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstPbSelectCvlanContext (pParentPortEntry) == RST_FAILURE)
    {
        return;
    }

    if (!AST_IS_RST_ENABLED ())
    {
        AstPbRestoreContext ();
        return;
    }

    for (u2Port = 1; u2Port <= AST_MAX_NUM_PORTS; u2Port++)
    {
        /* Is Port Created ? */
        pAstPortEntry = AST_GET_PORTENTRY (u2Port);

        if (pAstPortEntry == NULL)
            continue;

        AstRedApplyPerPortTimers (pAstPortEntry, RST_DEFAULT_INSTANCE);
    }

    AstPbRestoreContext ();

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbSelParentAndCvlanCntxt                       */
/*                                                                           */
/* Description        : This function selects the S-VLAN context based on    */
/*                      given context id and then selects context for the    */
/*                      C-VLAN component identified by cep local port id.    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context id.                    */
/*                      u2PortIndex - Cep local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbSelParentAndCvlanCntxt (UINT4 u4ContextId, UINT2 u2PortIndex)
{

    tAstPortEntry      *pCepPortEntry = NULL;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    pCepPortEntry = AST_GET_PORTENTRY (u2PortIndex);
    if (pCepPortEntry == NULL)
    {
        AstReleaseContext ();
        return RST_FAILURE;
    }

    if (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE)
    {
        AstReleaseContext ();
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstRedPbCvlanHwAudit                                 */
/*                                                                           */
/* Description        : This function does the hw audit for the given        */
/*                      C-VLAN component.                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context id.                    */
/*                      u2PortIndex - Cep local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstRedPbCvlanHwAudit (UINT4 u4ContextId, UINT2 u2PortIndex)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2Port;
    UINT2               u2MaxPorts = 0;

    AST_LOCK ();

    if (AstRedPbSelParentAndCvlanCntxt (u4ContextId, u2PortIndex)
        == RST_FAILURE)
    {
        AST_UNLOCK ();
        return RST_SUCCESS;
    }

    u2MaxPorts = AST_MAX_NUM_PORTS;

    AstPbRestoreContext ();
    AstReleaseContext ();
    AST_UNLOCK ();

    for (u2Port = 1; u2Port <= u2MaxPorts; u2Port++)
    {
        AST_LOCK ();

        if (AstRedPbSelParentAndCvlanCntxt (u4ContextId, u2PortIndex)
            == RST_FAILURE)
        {
            AST_UNLOCK ();
            return RST_SUCCESS;
        }

        if (!(AST_IS_RST_ENABLED ()))
        {
            AST_DBG (AST_RED_DBG,
                     "RED C-VLAN context: RSTP Module not enabled\n");
            AstPbRestoreContext ();
            AstReleaseContext ();
            AST_UNLOCK ();
            return RST_SUCCESS;
        }

        if ((pPerStInfo = AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE)) == NULL)
        {
            AstPbRestoreContext ();
            AstReleaseContext ();
            AST_UNLOCK ();
            return RST_SUCCESS;
        }

        /* As the total number of ports in C-VLAN ports can change
         * at runtime update u2MaxPorts here. */
        u2MaxPorts = AST_MAX_NUM_PORTS;

        /* Is Port Created ? */
        pPortEntry = AST_GET_PORTENTRY (u2Port);
        if (pPortEntry == NULL)
        {
            AstPbRestoreContext ();
            AstReleaseContext ();
            AST_UNLOCK ();
            continue;
        }

        /* Is Port Oper Up ? */
        if (!AST_IS_PORT_UP (u2Port))
        {
            AstPbRestoreContext ();
            AstReleaseContext ();
            AST_UNLOCK ();
            continue;
        }

        if (AstRedPortInstHwAudit (RST_DEFAULT_INSTANCE,
                                   pPortEntry) == RST_FAILURE)
        {
            AstPbRestoreContext ();
            AstReleaseContext ();
            AST_UNLOCK ();
            return RST_FAILURE;
        }

        AstPbRestoreContext ();
        AstReleaseContext ();
        AST_UNLOCK ();
    }

    return RST_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function Name      : AstRedClearAllCvlanSyncUpData                        */
/* Description        : Clears stale Data On all Ports of all C-VLAN         */
/*                      components.                                          */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedClearAllCvlanSyncUpData (VOID)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2LocalPort;

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        pPortEntry = AST_GET_PORTENTRY (u2LocalPort);

        if ((pPortEntry != NULL) &&
            (AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT))
        {

            if (AstPbSelectCvlanContext (pPortEntry) == RST_FAILURE)
            {
                continue;
            }

            if (AST_IS_RST_ENABLED ())
            {
                AstRedClearAllSyncUpData ();
            }

            AstPbRestoreContext ();
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedPbUpdtCvlanPortStates                          */
/*                                                                           */
/* Description        : Update all the synced Up Port state for all ports    */
/*                      in the given C-VLAN component.                       */
/*                                                                           */
/* Input(s)           : u2CepPortNo - CEP local port number                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbUpdtCvlanPortStates (UINT2 u2CepPortNo)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    UINT2               u2LocalPort;

    pCepPortEntry = AST_GET_PORTENTRY (u2CepPortNo);

    if (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE)
    {
        return;
    }

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        /* Is Port Created ? */
        if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
            continue;

        /* Oper UP ? */
        if (!AST_IS_PORT_UP (u2LocalPort))
            continue;

        AstRedUpdatePortStates (RST_DEFAULT_INSTANCE, u2LocalPort);

    }

    AstPbRestoreContext ();
}

/*****************************************************************************/
/* Function Name      : AstRedPbDumpSyncedUpCvlanPdus                        */
/*                                                                           */
/* Description        : Dumps synced up PDU for all C-VLAN component ports   */
/*                      for the given C-VLAN component.                      */
/*                                                                           */
/* Input(s)           : u2CepPortNo - CEP local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedPbDumpSyncedUpCvlanPdus (UINT2 u2CepPortNo)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    UINT2               u2Port;

    pCepPortEntry = AST_GET_PORTENTRY (u2CepPortNo);

    if (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE)
    {
        return;
    }

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d, C-VLAN with CEP  %d : PDU "
            "Synced Updata.\n", AST_CURR_CONTEXT_ID (),
            pCepPortEntry->u4IfIndex);

    for (u2Port = 1; u2Port <= AST_MAX_NUM_PORTS; u2Port++)
    {

        if (AST_GET_PORTENTRY (u2Port) == NULL)
            continue;

        RedDumpSyncedUpPduonPort (u2Port);

    }

    AstPbRestoreContext ();

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncUpCvlanTmrData                         */
/*                                                                           */
/* Description        : Dumped Synced Up times on Standby on all             */
/*                      Ports for the given C-VLAN component.                */
/*                                                                           */
/* Input(s)           : u2CepPortNo - CEP local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpCvlanTmrData (UINT2 u2CepPortNo)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    UINT2               u2LocalPort = 0;

    pCepPortEntry = AST_GET_PORTENTRY (u2CepPortNo);

    if ((pCepPortEntry == NULL) ||
        (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE))
    {
        return;
    }
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d, C-VLAN with CEP  %d : Timer "
            "Synced Updata.\n", AST_CURR_CONTEXT_ID (),
            pCepPortEntry->u4IfIndex);

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
        {
            continue;
        }

        AstRedDumpSyncUpTmrsPerPortInst (RST_DEFAULT_INSTANCE, u2LocalPort);

    }
    AstPbRestoreContext ();

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncUpAllCvlanTmrData                      */
/*                                                                           */
/* Description        : Dumped Synced Up C-VLAN Tmr datas on all Standby     */
/*                      C-VLAN components for the given context.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpAllCvlanTmrData (VOID)
{
    UINT2               u2LocalPort = 0;

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        if ((AST_GET_PORTENTRY (u2LocalPort) != NULL) &&
            (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE))
        {
            AstRedDumpSyncUpCvlanTmrData (u2LocalPort);
        }

    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpAllCvlanSyncUpOutputs                      */
/*                                                                           */
/* Description        : Dumped Synced Up outputs on all Standby C-VLAN       */
/*                      components for the given context.                    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpAllCvlanSyncUpOutputs (VOID)
{
    UINT2               u2LocalPort = 0;

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        if ((AST_GET_PORTENTRY (u2LocalPort) != NULL) &&
            (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE))
        {
            AstRedDumpSyncUpCvlanOutputs (u2LocalPort);

        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpAllCvlanSyncUpPdus                         */
/*                                                                           */
/* Description        : Dumped Synced Up Pdus on all Standby C-VLAN          */
/*                      components for the given context.                    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpAllCvlanSyncUpPdus (VOID)
{
    UINT2               u2LocalPort = 0;

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        if ((AST_GET_PORTENTRY (u2LocalPort) != NULL) &&
            (AST_IS_CUSTOMER_EDGE_PORT (u2LocalPort) == RST_TRUE))
        {
            AstRedPbDumpSyncedUpCvlanPdus (u2LocalPort);
        }

    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedDumpSyncedUpCvlanData                          */
/*                                                                           */
/* Description        : Dumped Synced Up outputs on Standby on all           */
/*                      Ports for the given C-VLAN component.                */
/*                                                                           */
/* Input(s)           : u2CepPortNo - CEP local port number.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AstRedDumpSyncUpCvlanOutputs (UINT2 u2CepPortNo)
{
    tAstPortEntry      *pCepPortEntry = NULL;
    UINT2               u2LocalPort = 0;

    pCepPortEntry = AST_GET_PORTENTRY (u2CepPortNo);

    if ((pCepPortEntry == NULL) ||
        (AstPbSelectCvlanContext (pCepPortEntry) == RST_FAILURE))
    {
        return;
    }

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"\nContext: %d, C-VLAN with CEP  %d : Output "
            "Synced Updata.\n", AST_CURR_CONTEXT_ID (),
            pCepPortEntry->u4IfIndex);

    for (u2LocalPort = 1; u2LocalPort <= AST_MAX_NUM_PORTS; u2LocalPort++)
    {
        if (AST_GET_PORTENTRY (u2LocalPort) != NULL)
        {
            AstRedDumpSyncUpOutputPerPort (RST_DEFAULT_INSTANCE, u2LocalPort);
        }

    }

    AstPbRestoreContext ();

    return;
}

/*****************************************************************************/
/* Function Name      : AstRedHandleHwPepCreate                              */
/*                                                                           */
/* Description        : This function is called from the Message processing  */
/*                      thread to Handle PEP Port Creation msg in Hw         */
/*                                                                           */
/* Input(s)           : pMsgNode - Contains information of creating a PEP Port*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Called By          : Task Loop                                            */
/*                                                                           */
/*****************************************************************************/

INT4
AstRedHandleHwPepCreate (tAstMsgNode * pMsgNode)
{
    UINT1               u1PortState;

    if (!(AST_IS_RST_STARTED ()) && !(AST_IS_MST_STARTED ()))
    {

        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                 AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                 "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;

    }

    /*Get Port state from Sw */
    u1PortState = AstL2IwfGetPbPortState (AST_GET_IFINDEX (pMsgNode->u4PortNo),
                                          pMsgNode->uMsg.VlanId);
#ifdef NPAPI_WANTED
    /* Set the port state for the given pe . */
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        RstpFsMiPbRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                      AST_GET_IFINDEX (pMsgNode->u4PortNo),
                                      pMsgNode->uMsg.VlanId, u1PortState);
    }
#else
    UNUSED_PARAM (u1PortState);
#endif

    return RST_SUCCESS;
}
#endif
