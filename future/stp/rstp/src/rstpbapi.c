/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: rstpbapi.c,v 1.9 2016/01/13 13:08:09 siva Exp $
 *
 * Description: This file contains the external APIs provided by      
 *              STP to work with provider bridging.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstCreateProviderEdgePort                            */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      in context of VLAN to indicate the creation of a PEP.*/
/*                                                                           */
/* Input(s)           : u4IfIndex- CEP Port's Interface Index                */
/*                      tMstVlanId  - Service VLAN id for this PEP           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfCreateProviderEdgePort                          */
/*****************************************************************************/
INT4
AstCreateProviderEdgePort (UINT4 u4IfIndex, tMstVlanId SVlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_CREATE_PEP_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.VlanId = SVlanId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteProviderEdgePort                            */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module in ctxt*/
/*                      of VLAN to indicate the deletion of a PEP.           */
/*                      This function must not be called when spanning-tree  */
/*                      is in shutdown state.                                */
/*                                                                           */
/* Input(s)           : u2PortNum - CEP Port's Interface Index               */
/*                      SVlanId   - Vlan Id indicating this PEP              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfDeleteProviderEdgePort                          */
/*****************************************************************************/
INT4
AstDeleteProviderEdgePort (UINT4 u4IfIndex, tMstVlanId SVlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_DELETE_PEP_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->uMsg.VlanId = SVlanId;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstPbHandleInFrame                                   */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module in ctxt*/
/*                      of VLAN to handover the BPDUs to ASTP Task by posting*/
/*                      the BDPUs to the AST Queue and sending an event      */
/*                      to ASTP Task. This function is called when the BPDU  */
/*                      received on a PEP port                               */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the BPDU CRU Buffer             */
/*                      u2IfIndex - CEP Port's Interface Index               */
/*                      SVlanId  - VlanId denoting the PEP Port Num          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingFrameOnPEP                        */
/*****************************************************************************/
INT4
AstPbHandleInFrame (tAstBufChainHeader * pCruBuf, UINT2 u2IfIndex,
                    tMstVlanId SVlanId)
{
    tAstInterface       IfaceId;
    INT4                i4RetVal = RST_SUCCESS;
    tAstCfaIfInfo       CfaIfInfo;
    tAstQMsg           *pQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AST_IS_INITIALISED () != RST_TRUE)
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: AST is not initialized!!!\n");
        }
        return RST_FAILURE;
    }
    AST_MEMSET (&IfaceId, AST_INIT_VAL, sizeof (IfaceId));
    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    if (AstVcmGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: CRU Buffer Release FAILED!!!\n");
            return RST_FAILURE;
        }
        return RST_FAILURE;
    }

    if (!(AstIsRstEnabledInContext (u4ContextId) ||
          AstIsMstEnabledInContext (u4ContextId) ||
          AstIsPvrstEnabledInContext (u4ContextId))
        || (AST_NODE_STATUS () != RED_AST_ACTIVE))
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: CRU Buffer Release FAILED!!!\n");
            return RST_FAILURE;
        }
        return RST_SUCCESS;
    }

    if (AstCfaGetIfInfo (u2IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        AST_GLOBAL_DBG (u4ContextId, AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                        "SYS: Cannot Get IfType for Port %u\n", u2IfIndex);

        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) == AST_CRU_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "MSG: Received BPDU CRU Buffer -\n");
            AST_GLOBAL_TRC (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "Release FAILED!\n");
        }

        return RST_FAILURE;
    }

    /* PEP interface is treated as the ENET Interface , so if type is configured
     * as ENET interface*/
    IfaceId.u1_InterfaceType = AST_ENET_PHYS_INTERFACE_TYPE;

    if (AST_ALLOC_QMSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId, AST_INIT_SHUT_DBG | AST_MEM_DBG |
                        AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");

        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: CRU Buffer Release FAILED!!!\n");
        }
        return RST_SUCCESS;
    }

    AST_MEMSET (pQMsg, AST_MEMSET_VAL, sizeof (tAstQMsg));

    IfaceId.u4IfIndex = (UINT4) u2IfIndex;

    /*Using u2_SubReferenceNum as SVID in case a BPDU received on the PEP */
    IfaceId.u2_SubReferenceNum = SVlanId;

    if (SVlanId == 0)
    {
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Invalid Input SVLAN id is zero !!!\n");

        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: CRU Buffer Release FAILED!!!\n");
        }
        AST_RELEASE_QMSG_MEM_BLOCK (pQMsg);

        return RST_FAILURE;
    }

    /* set the interface id in the buffer */
    AST_BUF_SET_INTERFACEID (pCruBuf, IfaceId);
    AST_QMSG_TYPE (pQMsg) = AST_BPDU_RCVD_QMSG;
    pQMsg->uQMsg.pBpduInQ = pCruBuf;

    if (AST_SEND_TO_QUEUE (AST_INPUT_QID,
                           (UINT1 *) &pQMsg,
                           AST_DEF_MSG_LEN) == AST_OSIX_SUCCESS)

    {
        if (AST_SEND_EVENT (AST_TASK_ID, AST_BPDU_EVENT) != AST_OSIX_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: Sending AST_MSG_EVENT to ASTP Task FAILED!!!\n");
            i4RetVal = RST_SUCCESS;
        }
    }
    else
    {
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                        AST_ALL_FAILURE_DBG,
                        "STAP: Posting Message to Ast Queue FAILED!!!\n");
        i4RetVal = RST_FAILURE;
    }

    if (i4RetVal == RST_FAILURE)
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) == AST_CRU_FAILURE)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "MSG: Received BPDU CRU Buffer -\n");
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "Release FAILED!\n");
        }

        if (AST_RELEASE_QMSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                            "MGMT: Release of Local Message Memory Block FAILED!\n");
            i4RetVal = RST_FAILURE;
        }

    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : AstPepOperStatusIndication                           */
/*                                                                           */
/* Description        : This function is called from the L2iwf Module to     */
/*                      indicate the operational status of the provider edge */
/*                      port status.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Global IfIndex of the CEP belonging  */
/*                                  to this C-VLAN component.                */
/*                      SVlanId   - Svid associated with the given PEP.      */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*                                                                           */
/*****************************************************************************/
INT4
AstPepOperStatusIndication (UINT4 u4IfIndex, tMstVlanId SVlanId, UINT1 u1Status)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN or DISABLED!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN or DISABLED!\n");

        /*Warning ::: Dont use wrapper functions here, Since we have
         * not selected context as of now*/

        if (u1Status == CFA_IF_UP)
        {
            AstL2IwfSetPbPortState (u4IfIndex, SVlanId,
                                    AST_PORT_STATE_FORWARDING);
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                RstpFsMiPbRstpNpSetPortState (u4ContextId, u4IfIndex, SVlanId,
                                              AST_PORT_STATE_FORWARDING);
            }
#endif /*NPAPI_WANTED */
        }
        else
        {
            AstL2IwfSetPbPortState (u4IfIndex, SVlanId,
                                    AST_PORT_STATE_DISCARDING);
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                RstpFsMiPbRstpNpSetPortState (u4ContextId, u4IfIndex, SVlanId,
                                              AST_PORT_STATE_DISCARDING);
            }
#endif /*NPAPI_WANTED */

        }

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = u4IfIndex;
    pNode->uMsg.VlanId = SVlanId;
    pNode->u4ContextId = u4ContextId;

    /* Even though instance id is not needed for updating port oper status,
     * the Port Operstatus and Port Stp Enabled Status is handled in
     * a single function, so a check has been in MstEnablePort which 
     * leads to fill up this variable*/
    pNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    /* There is no need to set the trigger type here. For PEP, STP 
     * up/down is not possible. So ENABLE_PEP_PORT_MSG means it is
     * PEP port oper up. Other PEP oper down. Also if trigger type 
     * is set, then it will overwrite the VlanId field in  uMsg, as
     * u1TrigType and VlanId are in the same union. */
    switch (u1Status)
    {
        case AST_UP:
            pNode->MsgType = (tAstLocalMsgType) AST_ENABLE_PEP_PORT_MSG;
            break;

        case AST_DOWN:
            pNode->MsgType = (tAstLocalMsgType) AST_DISABLE_PEP_PORT_MSG;
            break;
    }

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : AstRedHwPepCreateNotify                              */
/*                                                                           */
/* Description        : This function is used to post message, if pep is     */
/*                      created in hw.                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      SVlanId   - Service Vlan Id        .                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstRedHwPepCreateNotify (UINT4 u4IfIndex, tVlanId SVlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.VlanId = SVlanId;

    pNode->MsgType = (tAstLocalMsgType) AST_HW_PEP_CREATE_MSG;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}
#endif
