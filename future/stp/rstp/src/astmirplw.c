/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmirplw.c,v 1.44 2017/12/29 09:31:22 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdrslow.h"
# include  "fsmprslw.h"
# include   "vcm.h"
# include  "asthdrs.h"
# include  "fsmprscli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRstGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIRstGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstGlobalTrace (INT4 *pi4RetValFsMIRstGlobalTrace)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalTraceOption == AST_TRUE)
    {
        *pi4RetValFsMIRstGlobalTrace = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIRstGlobalTrace = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIRstGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsMIRstGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsMIRstGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstGlobalDebug (INT4 *pi4RetValFsMIRstGlobalDebug)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalDebugOption == AST_TRUE)
    {
        *pi4RetValFsMIRstGlobalDebug = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIRstGlobalDebug = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIRstGlobalDebug);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRstGlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIRstGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstGlobalTrace (INT4 i4SetValFsMIRstGlobalTrace)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsMIRstGlobalTrace == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstGlobalTrace,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, " %i", i4SetValFsMIRstGlobalTrace));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIRstGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsMIRstGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsMIRstGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstGlobalDebug (INT4 i4SetValFsMIRstGlobalDebug)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsMIRstGlobalDebug == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_FALSE;
    }
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstGlobalDebug,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, " %i", i4SetValFsMIRstGlobalDebug));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIRstGlobalDebug);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRstGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIRstGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstGlobalTrace (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMIRstGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIRstGlobalTrace != AST_SNMP_TRUE) &&
        (i4TestValFsMIRstGlobalTrace != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIRstGlobalTrace);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsMIRstGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstGlobalDebug (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMIRstGlobalDebug)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIRstGlobalDebug != AST_SNMP_TRUE) &&
        (i4TestValFsMIRstGlobalDebug != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIRstGlobalDebug);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRstGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRstGlobalTrace (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRstGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRstGlobalDebug (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1wFutureRstTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1wFutureRstTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1wFutureRstTable (INT4
                                                 i4FsMIDot1wFutureRstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIDot1wFutureRstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1wFutureRstTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1wFutureRstTable (INT4 *pi4FsMIDot1wFutureRstContextId)
{
    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIDot1wFutureRstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1wFutureRstTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
                nextFsMIDot1wFutureRstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1wFutureRstTable (INT4 i4FsMIDot1wFutureRstContextId,
                                        INT4
                                        *pi4NextFsMIDot1wFutureRstContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIDot1wFutureRstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIDot1wFutureRstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRstSystemControl
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstSystemControl (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 *pi4RetValFsMIRstSystemControl)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstSystemControl (pi4RetValFsMIRstSystemControl);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstModuleStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstModuleStatus (INT4 i4FsMIDot1wFutureRstContextId,
                           INT4 *pi4RetValFsMIRstModuleStatus)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstModuleStatus (pi4RetValFsMIRstModuleStatus);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstTraceOption
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstTraceOption (INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 *pi4RetValFsMIRstTraceOption)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstTraceOption (pi4RetValFsMIRstTraceOption);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstDebugOption
Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstDebugOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstDebugOption (INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 *pi4RetValFsMIRstDebugOption)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstDebugOption (pi4RetValFsMIRstDebugOption);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstRstpUpCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRstpUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRstpUpCount (INT4 i4FsMIDot1wFutureRstContextId,
                          UINT4 *pu4RetValFsMIRstRstpUpCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRstpUpCount (pu4RetValFsMIRstRstpUpCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstRstpDownCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRstpDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRstpDownCount (INT4 i4FsMIDot1wFutureRstContextId,
                            UINT4 *pu4RetValFsMIRstRstpDownCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRstpDownCount (pu4RetValFsMIRstRstpDownCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstBufferFailureCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstBufferFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstBufferFailureCount (INT4 i4FsMIDot1wFutureRstContextId,
                                 UINT4 *pu4RetValFsMIRstBufferFailureCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstBufferFailureCount
        (pu4RetValFsMIRstBufferFailureCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstMemAllocFailureCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstMemAllocFailureCount (INT4 i4FsMIDot1wFutureRstContextId,
                                   UINT4 *pu4RetValFsMIRstMemAllocFailureCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstMemAllocFailureCount
        (pu4RetValFsMIRstMemAllocFailureCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstNewRootIdCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstNewRootIdCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstNewRootIdCount (INT4 i4FsMIDot1wFutureRstContextId,
                             UINT4 *pu4RetValFsMIRstNewRootIdCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstNewRootIdCount (pu4RetValFsMIRstNewRootIdCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRoleSelSmState
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstPortRoleSelSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRoleSelSmState (INT4 i4FsMIDot1wFutureRstContextId,
                                 INT4 *pi4RetValFsMIRstPortRoleSelSmState)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRoleSelSmState
        (pi4RetValFsMIRstPortRoleSelSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstOldDesignatedRoot
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstOldDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstOldDesignatedRoot (INT4 i4FsMIDot1wFutureRstContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIRstOldDesignatedRoot)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstOldDesignatedRoot (pRetValFsMIRstOldDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstDynamicPathcostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstDynamicPathcostCalculation (INT4 i4FsMIDot1wFutureRstContextId,
                                         INT4
                                         *pi4RetValFsMIRstDynamicPathcostCalculation)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstDynamicPathcostCalculation
        (pi4RetValFsMIRstDynamicPathcostCalculation);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstContextName
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                retValFsMIRstContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstContextName (INT4 i4FsMIDot1wFutureRstContextId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIRstContextName)
{
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];

    AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

    if (AST_IS_VC_VALID (i4FsMIDot1wFutureRstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    AstVcmGetAliasName (i4FsMIDot1wFutureRstContextId, au1ContextName);

    AST_MEMCPY (pRetValFsMIRstContextName->pu1_OctetList,
                au1ContextName, AST_STRLEN (au1ContextName));
    pRetValFsMIRstContextName->i4_Length = AST_STRLEN (au1ContextName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstCalcPortPathCostOnSpeedChg (INT4 i4FsMIDot1wFutureRstContextId,
                                         INT4
                                         *pi4RetValFsMIRstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstCalcPortPathCostOnSpeedChg
        (pi4RetValFsMIRstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstRcvdEvent
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRcvdEvent (INT4 i4FsMIDot1wFutureRstContextId,
                        INT4 *pi4RetValFsMIRstRcvdEvent)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRcvdEvent (pi4RetValFsMIRstRcvdEvent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstRcvdEventSubType
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRcvdEventSubType (INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 *pi4RetValFsMIRstRcvdEventSubType)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRcvdEventSubType (pi4RetValFsMIRstRcvdEventSubType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstRcvdEventTimeStamp
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRcvdEventTimeStamp (INT4 i4FsMIDot1wFutureRstContextId,
                                 UINT4 *pu4RetValFsMIRstRcvdEventTimeStamp)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRcvdEventTimeStamp
        (pu4RetValFsMIRstRcvdEventTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstRcvdPortStateChangeTimeStamp
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstRcvdPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIRstRcvdPortStateChangeTimeStamp
    (INT4 i4FsMIDot1wFutureRstContextId,
     UINT4 *pu4RetValFsMIRstRcvdPortStateChangeTimeStamp)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstRcvdPortStateChangeTimeStamp
        (pu4RetValFsMIRstRcvdPortStateChangeTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRstSystemControl
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstSystemControl (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 i4SetValFsMIRstSystemControl)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstSystemControl (i4SetValFsMIRstSystemControl);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstFlushInterval
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstFlushInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstFlushInterval (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 *pi4RetValFsMIRstFlushInterval)
{
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Started!\n");
        *pi4RetValFsMIRstFlushInterval = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsMIRstFlushInterval = (INT4) AST_FLUSH_INTERVAL;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstFlushIndicationThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstFlushIndicationThreshold (INT4 i4FsMIDot1wFutureRstContextId,
                                       INT4
                                       *pi4RetValFsMIRstFlushIndicationThreshold)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pi4RetValFsMIRstFlushIndicationThreshold = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    if (NULL == pAstPerStBridgeInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIRstFlushIndicationThreshold =
        pAstPerStBridgeInfo->u4FlushIndThreshold;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstTotalFlushCount
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstTotalFlushCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstTotalFlushCount (INT4 i4FsMIDot1wFutureRstContextId,
                              UINT4 *pu4RetValFsMIRstTotalFlushCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Enabled!\n");
        *pu4RetValFsMIRstTotalFlushCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    if (NULL == pAstPerStBridgeInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIRstTotalFlushCount = pAstPerStBridgeInfo->u4TotalFlushCount;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstFwdDelayAltPortRoleTrOptimization
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                retValFsMIRstFwdDelayAltPortRoleTrOptimization
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstFwdDelayAltPortRoleTrOptimization (INT4
                                                i4FsMIDot1wFutureRstContextId,
                                                INT4
                                                *pi4RetValFsMIRstFwdDelayAltPortRoleTrOptimization)
{
    tAstBridgeEntry    *pBrgEntry = NULL;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pBrgEntry = AST_GET_BRGENTRY ();

    if (pBrgEntry == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsMIRstFwdDelayAltPortRoleTrOptimization = RST_ENABLED;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsMIRstFwdDelayAltPortRoleTrOptimization =
        pBrgEntry->u4RstOptStatus;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstBpduGuard
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                retValFsMIRstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstBpduGuard (INT4 i4FsMIDot1wFutureRstContextId,
                        INT4 *pi4RetValFsMIRstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstBpduGuard (pi4RetValFsMIRstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstModuleStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstModuleStatus (INT4 i4FsMIDot1wFutureRstContextId,
                           INT4 i4SetValFsMIRstModuleStatus)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstModuleStatus (i4SetValFsMIRstModuleStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstTraceOption
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstTraceOption (INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 i4SetValFsMIRstTraceOption)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstTraceOption (i4SetValFsMIRstTraceOption);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstDebugOption
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstDebugOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstDebugOption (INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 i4SetValFsMIRstDebugOption)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstDebugOption (i4SetValFsMIRstDebugOption);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstDynamicPathcostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstDynamicPathcostCalculation (INT4 i4FsMIDot1wFutureRstContextId,
                                         INT4
                                         i4SetValFsMIRstDynamicPathcostCalculation)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstDynamicPathcostCalculation
        (i4SetValFsMIRstDynamicPathcostCalculation);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstCalcPortPathCostOnSpeedChg (INT4 i4FsMIDot1wFutureRstContextId,
                                         INT4
                                         i4SetValFsMIRstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstCalcPortPathCostOnSpeedChg
        (i4SetValFsMIRstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstFlushInterval
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstFlushInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstFlushInterval (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 i4SetValFsMIRstFlushInterval)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIRstFlushInterval == (INT4) AST_FLUSH_INTERVAL)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    AST_FLUSH_INTERVAL = (UINT4) i4SetValFsMIRstFlushInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstFlushInterval,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIRstFlushInterval));

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstFlushIndicationThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstFlushIndicationThreshold (INT4 i4FsMIDot1wFutureRstContextId,
                                       INT4
                                       i4SetValFsMIRstFlushIndicationThreshold)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIRstFlushIndicationThreshold ==
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo->u4FlushIndThreshold =
        (UINT4) i4SetValFsMIRstFlushIndicationThreshold;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstFlushIndicationThreshold,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIRstFlushIndicationThreshold));

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstFwdDelayAltPortRoleTrOptimization
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                setValFsMIRstFwdDelayAltPortRoleTrOptimization
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstFwdDelayAltPortRoleTrOptimization (INT4
                                                i4FsMIDot1wFutureRstContextId,
                                                INT4
                                                i4SetValFsMIRstFwdDelayAltPortRoleTrOptimization)
{
    tAstBridgeEntry    *pBrgEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pBrgEntry = AST_GET_BRGENTRY ();

    if (pBrgEntry == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIRstFwdDelayAltPortRoleTrOptimization ==
        (INT4) pBrgEntry->u4RstOptStatus)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    pBrgEntry->u4RstOptStatus =
        (UINT4) i4SetValFsMIRstFwdDelayAltPortRoleTrOptimization;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIRstFwdDelayAltPortRoleTrOptimization, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIRstFwdDelayAltPortRoleTrOptimization));

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstBpduGuard
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                setValFsMIRstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstBpduGuard (INT4 i4FsMIDot1wFutureRstContextId,
                        INT4 i4SetValFsMIRstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstBpduGuard (i4SetValFsMIRstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRstSystemControl
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstSystemControl (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 i4TestValFsMIRstSystemControl)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstSystemControl (pu4ErrorCode,
                                            i4TestValFsMIRstSystemControl);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstModuleStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstModuleStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIDot1wFutureRstContextId,
                              INT4 i4TestValFsMIRstModuleStatus)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstModuleStatus (pu4ErrorCode,
                                           i4TestValFsMIRstModuleStatus);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstTraceOption
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstTraceOption (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIDot1wFutureRstContextId,
                             INT4 i4TestValFsMIRstTraceOption)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstTraceOption (pu4ErrorCode,
                                          i4TestValFsMIRstTraceOption);
    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstDebugOption
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstDebugOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstDebugOption (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIDot1wFutureRstContextId,
                             INT4 i4TestValFsMIRstDebugOption)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstDebugOption (pu4ErrorCode,
                                          i4TestValFsMIRstDebugOption);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstDynamicPathcostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstDynamicPathcostCalculation (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1wFutureRstContextId,
                                            INT4
                                            i4TestValFsMIRstDynamicPathcostCalculation)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstDynamicPathcostCalculation (pu4ErrorCode,
                                                         i4TestValFsMIRstDynamicPathcostCalculation);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1wFutureRstContextId,
                                            INT4
                                            i4TestValFsMIRstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstCalcPortPathCostOnSpeedChg (pu4ErrorCode,
                                                         i4TestValFsMIRstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstFlushInterval
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstFlushInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstFlushInterval (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 i4TestValFsMIRstFlushInterval)
{
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIRstFlushInterval < AST_FLUSH_INTERVAL_MIN_VAL) ||
        (i4TestValFsMIRstFlushInterval > AST_FLUSH_INTERVAL_MAX_VAL))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstFlushIndicationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstFlushIndicationThreshold (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIDot1wFutureRstContextId,
                                          INT4
                                          i4TestValFsMIRstFlushIndicationThreshold)
{
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIRstFlushIndicationThreshold < AST_MIN_FLUSH_IND) ||
        (i4TestValFsMIRstFlushIndicationThreshold > AST_MAX_FLUSH_IND))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstFwdDelayAltPortRoleTrOptimization
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                testValFsMIRstFwdDelayAltPortRoleTrOptimization
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstFwdDelayAltPortRoleTrOptimization (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsMIDot1wFutureRstContextId,
                                                   INT4
                                                   i4TestValFsMIRstFwdDelayAltPortRoleTrOptimization)
{

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIRstFwdDelayAltPortRoleTrOptimization != RST_ENABLED) &&
        (i4TestValFsMIRstFwdDelayAltPortRoleTrOptimization != RST_DISABLED))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstBpduGuard
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
                                                                                                                               The Object
                testValFsMIRstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstBpduGuard (UINT4 *pu4ErrorCode,
                           INT4 i4FsMIDot1wFutureRstContextId,
                           INT4 i4TestValFsMIRstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstBpduGuard (pu4ErrorCode, i4TestValFsMIRstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1wFutureRstTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1wFutureRstTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRstPortExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRstPortExtTable
 Input       :  The Indices
                FsMIRstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRstPortExtTable (INT4 i4FsMIRstPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsRstPortExtTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRstPortExtTable
 Input       :  The Indices
                FsMIRstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRstPortExtTable (INT4 *pi4FsMIRstPort)
{
    return nmhGetNextIndexFsMIRstPortExtTable (0, pi4FsMIRstPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRstPortExtTable
 Input       :  The Indices
                FsMIRstPort
                nextFsMIRstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRstPortExtTable (INT4 i4FsMIRstPort,
                                    INT4 *pi4NextFsMIRstPort)
{

    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIRstPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsMIRstPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIRstPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry)) &&
                (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                           (pAstPortEntry))))
            {
                *pi4NextFsMIRstPort = pAstPortEntry->u4IfIndex;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsMIRstPortRole
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRole (INT4 i4FsMIRstPort, INT4 *pi4RetValFsMIRstPortRole)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRole ((INT4) u2LocalPortId,
                                    pi4RetValFsMIRstPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortOperVersion
Input       :  The Indices
FsMIRstPort

The Object 
retValFsMIRstPortOperVersion
Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortOperVersion (INT4 i4FsMIRstPort,
                              INT4 *pi4RetValFsMIRstPortOperVersion)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortOperVersion ((INT4) u2LocalPortId,
                                           pi4RetValFsMIRstPortOperVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortInfoSmState
Input       :  The Indices
FsMIRstPort

                The Object 
                retValFsMIRstPortInfoSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortInfoSmState (INT4 i4FsMIRstPort,
                              INT4 *pi4RetValFsMIRstPortInfoSmState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortInfoSmState ((INT4) u2LocalPortId,
                                           pi4RetValFsMIRstPortInfoSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortMigSmState
Input       :  The Indices
FsMIRstPort

The Object 
retValFsMIRstPortMigSmState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIRstPortMigSmState (INT4 i4FsMIRstPort,
                             INT4 *pi4RetValFsMIRstPortMigSmState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortMigSmState ((INT4) u2LocalPortId,
                                          pi4RetValFsMIRstPortMigSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRoleTransSmState
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRoleTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRoleTransSmState (INT4 i4FsMIRstPort,
                                   INT4 *pi4RetValFsMIRstPortRoleTransSmState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRoleTransSmState ((INT4) u2LocalPortId,
                                                pi4RetValFsMIRstPortRoleTransSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortStateTransSmState
Input       :  The Indices
FsMIRstPort

The Object 
                retValFsMIRstPortStateTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortStateTransSmState (INT4 i4FsMIRstPort,
                                    INT4 *pi4RetValFsMIRstPortStateTransSmState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortStateTransSmState ((INT4) u2LocalPortId,
                                                 pi4RetValFsMIRstPortStateTransSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortTopoChSmState
Input       :  The Indices
FsMIRstPort

                The Object 
                retValFsMIRstPortTopoChSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortTopoChSmState (INT4 i4FsMIRstPort,
                                INT4 *pi4RetValFsMIRstPortTopoChSmState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTopoChSmState ((INT4) u2LocalPortId,
                                             pi4RetValFsMIRstPortTopoChSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortTxSmState
Input       :  The Indices
FsMIRstPort

The Object 
                retValFsMIRstPortTxSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortTxSmState (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortTxSmState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTxSmState ((INT4) u2LocalPortId,
                                         pi4RetValFsMIRstPortTxSmState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortRxRstBpduCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRxRstBpduCount (INT4 i4FsMIRstPort,
                                 UINT4 *pu4RetValFsMIRstPortRxRstBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRxRstBpduCount ((INT4) u2LocalPortId,
                                              pu4RetValFsMIRstPortRxRstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortRxConfigBpduCount
Input       :  The Indices
FsMIRstPort

                The Object 
                retValFsMIRstPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRxConfigBpduCount (INT4 i4FsMIRstPort,
                                    UINT4
                                    *pu4RetValFsMIRstPortRxConfigBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRxConfigBpduCount ((INT4) u2LocalPortId,
                                                 pu4RetValFsMIRstPortRxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortRxTcnBpduCount
Input       :  The Indices
FsMIRstPort

The Object 
retValFsMIRstPortRxTcnBpduCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIRstPortRxTcnBpduCount (INT4 i4FsMIRstPort,
                                 UINT4 *pu4RetValFsMIRstPortRxTcnBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRxTcnBpduCount ((INT4) u2LocalPortId,
                                              pu4RetValFsMIRstPortRxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortTxRstBpduCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortTxRstBpduCount (INT4 i4FsMIRstPort,
                                 UINT4 *pu4RetValFsMIRstPortTxRstBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTxRstBpduCount ((INT4) u2LocalPortId,
                                              pu4RetValFsMIRstPortTxRstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortTxConfigBpduCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortTxConfigBpduCount (INT4 i4FsMIRstPort,
                                    UINT4
                                    *pu4RetValFsMIRstPortTxConfigBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTxConfigBpduCount ((INT4) u2LocalPortId,
                                                 pu4RetValFsMIRstPortTxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortTxTcnBpduCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortTxTcnBpduCount (INT4 i4FsMIRstPort,
                                 UINT4 *pu4RetValFsMIRstPortTxTcnBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTxTcnBpduCount ((INT4) u2LocalPortId,
                                              pu4RetValFsMIRstPortTxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortInvalidRstBpduRxCount (INT4 i4FsMIRstPort,
                                        UINT4
                                        *pu4RetValFsMIRstPortInvalidRstBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortInvalidRstBpduRxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIRstPortInvalidRstBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortInvalidConfigBpduRxCount
Input       :  The Indices
FsMIRstPort

The Object 
                retValFsMIRstPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortInvalidConfigBpduRxCount (INT4 i4FsMIRstPort,
                                           UINT4
                                           *pu4RetValFsMIRstPortInvalidConfigBpduRxCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortInvalidConfigBpduRxCount ((INT4) u2LocalPortId,
                                                        pu4RetValFsMIRstPortInvalidConfigBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIRstPortInvalidTcnBpduRxCount
Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortInvalidTcnBpduRxCount (INT4 i4FsMIRstPort,
                                        UINT4
                                        *pu4RetValFsMIRstPortInvalidTcnBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortInvalidTcnBpduRxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIRstPortInvalidTcnBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortProtocolMigrationCount
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMIRstPortProtocolMigrationCount (INT4 i4FsMIRstPort, UINT4
                                         *pu4RetValFsMIRstPortProtocolMigrationCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortProtocolMigrationCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIRstPortProtocolMigrationCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortEffectivePortState
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortEffectivePortState (INT4 i4FsMIRstPort,
                                     INT4
                                     *pi4RetValFsMIRstPortEffectivePortState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortEffectivePortState ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIRstPortEffectivePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortAutoEdge
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortAutoEdge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortAutoEdge (INT4 i4FsMIRstPort,
                           INT4 *pi4RetValFsMIRstPortAutoEdge)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortAutoEdge ((INT4) u2LocalPortId,
                                        pi4RetValFsMIRstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRestrictedRole
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRestrictedRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRestrictedRole (INT4 i4FsMIRstPort,
                                 INT4 *pi4RetValFsMIRstPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRestrictedRole ((INT4) u2LocalPortId,
                                       pi4RetValFsMIRstPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRestrictedTCN
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRestrictedTCN
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRestrictedTCN (INT4 i4FsMIRstPort,
                                INT4 *pi4RetValFsMIRstPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRestrictedTCN ((INT4) u2LocalPortId,
                                      pi4RetValFsMIRstPortRestrictedTCN);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortEnableBPDURx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortEnableBPDURx (INT4 i4FsMIRstPort,
                               INT4 *pi4RetValFsMIRstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortEnableBPDURx ((INT4) u2LocalPortId,
                                            pi4RetValFsMIRstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortEnableBPDUTx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortEnableBPDUTx (INT4 i4FsMIRstPort,
                               INT4 *pi4RetValFsMIRstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortEnableBPDUTx ((INT4) u2LocalPortId,
                                            pi4RetValFsMIRstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortPseudoRootId
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortPseudoRootId (INT4 i4FsMIRstPort,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIRstPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortPseudoRootId ((INT4) u2LocalPortId,
                                            pRetValFsMIRstPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortIsL2Gp
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortIsL2Gp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortIsL2Gp (INT4 i4FsMIRstPort, INT4 *pi4RetValFsMIRstPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortIsL2Gp ((INT4) u2LocalPortId,
                                      pi4RetValFsMIRstPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortLoopGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortLoopGuard (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortLoopGuard)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortLoopGuard ((INT4) u2LocalPortId,
                                  pi4RetValFsMIRstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRcvdEvent
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRcvdEvent (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortRcvdEvent)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRcvdEvent ((INT4) u2LocalPortId,
                                  pi4RetValFsMIRstPortRcvdEvent);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRcvdEventSubType
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRcvdEventSubType (INT4 i4FsMIRstPort,
                                   INT4 *pi4RetValFsMIRstPortRcvdEventSubType)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRcvdEventSubType ((INT4) u2LocalPortId,
                                         pi4RetValFsMIRstPortRcvdEventSubType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRcvdEventTimeStamp
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRcvdEventTimeStamp (INT4 i4FsMIRstPort,
                                     UINT4
                                     *pu4RetValFsMIRstPortRcvdEventTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRcvdEventTimeStamp ((INT4) u2LocalPortId,
                                           pu4RetValFsMIRstPortRcvdEventTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortStateChangeTimeStamp
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortStateChangeTimeStamp (INT4 i4FsMIRstPort,
                                       UINT4
                                       *pu4RetValFsMIRstPortStateChangeTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortStateChangeTimeStamp ((INT4) u2LocalPortId,
                                             pu4RetValFsMIRstPortStateChangeTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRowStatus
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRowStatus (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortRowStatus)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRowStatus ((INT4) u2LocalPortId,
                                  pi4RetValFsMIRstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstLoopInconsistentState
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstLoopInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstLoopInconsistentState (INT4 i4FsMIRstPort,
                                    INT4 *pi4RetValFsMIRstLoopInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortLoopInconsistentState ((INT4) u2LocalPortId,
                                              pi4RetValFsMIRstLoopInconsistentState);
    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortBpduGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstPortBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortBpduGuard (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortBpduGuard ((INT4) u2LocalPortId,
                                         pi4RetValFsMIRstPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstStpPerfStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                retValFsMIRstStpPerfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstStpPerfStatus (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 *pi4RetValFsMIRstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1wFutureRstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstStpPerfStatus (pi4RetValFsMIRstStpPerfStatus);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhGetFsMIRstPortRootGuard
  Input       :  The Indices
                 FsMIRstPort
                 The Object
                 retValFsMIRstPortRootGuard
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRootGuard (INT4 i4FsMIRstPort,
                            INT4 *pi4RetValFsMIRstPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortRootGuard ((INT4) u2LocalPortId,
                                  pi4RetValFsMIRstPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstRootInconsistentState
 Input       :  The Indices
                FsMIRstPort
 
                The Object
                retValFsMIRstRootInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstRootInconsistentState (INT4 i4FsMIRstPort,
                                    INT4 *pi4RetValFsMIRstRootInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstRootInconsistentState ((INT4) u2LocalPortId,
                                          pi4RetValFsMIRstRootInconsistentState);

    AstReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
   Function    :  nmhGetFsMIRstPortErrorRecovery
   Input       :  The Indices
                  FsMIRstPort
 
                  The Object
                  retValFsMIRstPortErrorRecovery
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortErrorRecovery (INT4 i4FsMIRstPort,
                                INT4 *pi4RetValFsMIRstPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortErrorRecovery ((INT4) u2LocalPortId,
                                      pi4RetValFsMIRstPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstPortStpModeDot1wEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortStpModeDot1wEnabled (INT4 i4FsMIRstPort,
                                      INT4
                                      *pi4RetValFsMIRstPortStpModeDot1wEnabled)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortStpModeDot1wEnabled ((INT4) u2LocalPortId,
                                            pi4RetValFsMIRstPortStpModeDot1wEnabled);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortBpduInconsistentState
 Input       :  The Indices
                FsMIRstPort

                The Object
                retValFsMIRstPortBpduInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortBpduInconsistentState (INT4 i4FsMIRstPort,
                                        INT4
                                        *pi4RetValFsMIRstPortBpduInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortBpduInconsistentState ((INT4) u2LocalPortId,
                                              pi4RetValFsMIRstPortBpduInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortBpduGuardAction
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortBpduGuardAction (INT4 i4FsMIRstPort,
                                  INT4 *pi4RetValFsMIRstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortBpduGuardAction ((INT4) u2LocalPortId,
                                               pi4RetValFsMIRstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortBpduGuardAction
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortBpduGuardAction (INT4 i4FsMIRstPort,
                                  INT4 i4SetValFsMIRstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortBpduGuardAction ((INT4) u2LocalPortId,
                                               i4SetValFsMIRstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortBpduGuardAction
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortBpduGuardAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortBpduGuardAction (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                     INT4 i4TestValFsMIRstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortBpduGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                     i4TestValFsMIRstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRstPortAutoEdge
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortAutoEdge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortAutoEdge (INT4 i4FsMIRstPort, INT4 i4SetValFsMIRstPortAutoEdge)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortAutoEdge ((INT4) u2LocalPortId,
                                        i4SetValFsMIRstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortRestrictedRole
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortRestrictedRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortRestrictedRole (INT4 i4FsMIRstPort,
                                 INT4 i4SetValFsMIRstPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsRstPortRestrictedRole ((INT4) u2LocalPortId,
                                       i4SetValFsMIRstPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortRestrictedTCN
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortRestrictedTCN
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortRestrictedTCN (INT4 i4FsMIRstPort,
                                INT4 i4SetValFsMIRstPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsRstPortRestrictedTCN ((INT4) u2LocalPortId,
                                      i4SetValFsMIRstPortRestrictedTCN);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortEnableBPDURx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortEnableBPDURx (INT4 i4FsMIRstPort,
                               INT4 i4SetValFsMIRstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortEnableBPDURx ((INT4) u2LocalPortId,
                                            i4SetValFsMIRstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortEnableBPDUTx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortEnableBPDUTx (INT4 i4FsMIRstPort,
                               INT4 i4SetValFsMIRstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortEnableBPDUTx ((INT4) u2LocalPortId,
                                            i4SetValFsMIRstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortPseudoRootId
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortPseudoRootId (INT4 i4FsMIRstPort,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIRstPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortPseudoRootId ((INT4) u2LocalPortId,
                                            pSetValFsMIRstPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortIsL2Gp
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortIsL2Gp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortIsL2Gp (INT4 i4FsMIRstPort, INT4 i4SetValFsMIRstPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortIsL2Gp ((INT4) u2LocalPortId,
                                      i4SetValFsMIRstPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortLoopGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                setValFsMIRstPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortLoopGuard (INT4 i4FsMIRstPort,
                            INT4 i4SetValFsMIRstPortLoopGuard)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsRstPortLoopGuard ((INT4) u2LocalPortId,
                                  i4SetValFsMIRstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortAutoEdge
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortAutoEdge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortAutoEdge (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                              INT4 i4TestValFsMIRstPortAutoEdge)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortAutoEdge (pu4ErrorCode, (INT4) u2LocalPortId,
                                           i4TestValFsMIRstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortRestrictedRole
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortRestrictedRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortRestrictedRole (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRstPort,
                                    INT4 i4TestValFsMIRstPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortRestrictedRole (pu4ErrorCode, (INT4) u2LocalPortId,
                                          i4TestValFsMIRstPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortRestrictedTCN
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortRestrictedTCN
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortRestrictedTCN (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                   INT4 i4TestValFsMIRstPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortRestrictedTCN (pu4ErrorCode, (INT4) u2LocalPortId,
                                         i4TestValFsMIRstPortRestrictedTCN);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortEnableBPDURx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortEnableBPDURx (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                  INT4 i4TestValFsMIRstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortEnableBPDURx (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIRstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortEnableBPDUTx
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortEnableBPDUTx (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                  INT4 i4TestValFsMIRstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortEnableBPDUTx (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIRstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortPseudoRootId
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortPseudoRootId (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIRstPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortPseudoRootId (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               pTestValFsMIRstPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortIsL2Gp
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortIsL2Gp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortIsL2Gp (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                            INT4 i4TestValFsMIRstPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortIsL2Gp (pu4ErrorCode,
                                         (INT4) u2LocalPortId,
                                         i4TestValFsMIRstPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortLoopGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                testValFsMIRstPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                           SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortLoopGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                               INT4 i4TestValFsMIRstPortLoopGuard)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortLoopGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                     i4TestValFsMIRstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRstPortExtTable
 Input       :  The Indices
                FsMIRstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRstPortExtTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRstSetGlobalTraps
 Input       :  The Indices

                The Object 
                retValFsMIRstSetGlobalTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstSetGlobalTraps (INT4 *pi4RetValFsMIRstSetGlobalTraps)
{
    *pi4RetValFsMIRstSetGlobalTraps = AST_GLOBAL_TRAP;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstGlobalErrTrapType
 Input       :  The Indices

                The Object 
                retValFsMIRstGlobalErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstGlobalErrTrapType (INT4 *pi4RetValFsMIRstGlobalErrTrapType)
{
    *pi4RetValFsMIRstGlobalErrTrapType = AST_ERR_TRAP;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRstSetGlobalTraps
 Input       :  The Indices

                The Object 
                setValFsMIRstSetGlobalTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstSetGlobalTraps (INT4 i4SetValFsMIRstSetGlobalTraps)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    AST_GLOBAL_TRAP = (UINT4) i4SetValFsMIRstSetGlobalTraps;
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstSetGlobalTraps,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, " %i", i4SetValFsMIRstSetGlobalTraps));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRstSetGlobalTraps
 Input       :  The Indices

                The Object 
                testValFsMIRstSetGlobalTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstSetGlobalTraps (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMIRstSetGlobalTraps)
{

    if ((i4TestValFsMIRstSetGlobalTraps < 0)
        || (i4TestValFsMIRstSetGlobalTraps > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRstSetGlobalTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRstSetGlobalTraps (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1wFsRstTrapsControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1wFsRstTrapsControlTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1wFsRstTrapsControlTable (INT4
                                                         i4FsMIDot1wFutureRstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIDot1wFutureRstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1wFsRstTrapsControlTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1wFsRstTrapsControlTable (INT4
                                                 *pi4FsMIDot1wFutureRstContextId)
{

    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIDot1wFutureRstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsMIDot1wFsRstTrapsControlTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
                nextFsMIDot1wFutureRstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1wFsRstTrapsControlTable (INT4
                                                i4FsMIDot1wFutureRstContextId,
                                                INT4
                                                *pi4NextFsMIDot1wFutureRstContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIDot1wFutureRstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIDot1wFutureRstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRstSetTraps
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstSetTraps (INT4 i4FsMIDot1wFutureRstContextId,
                       INT4 *pi4RetValFsMIRstSetTraps)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstSetTraps (pi4RetValFsMIRstSetTraps);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstGenTrapType
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstGenTrapType (INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 *pi4RetValFsMIRstGenTrapType)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstGenTrapType (pi4RetValFsMIRstGenTrapType);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRstSetTraps
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstSetTraps (INT4 i4FsMIDot1wFutureRstContextId,
                       INT4 i4SetValFsMIRstSetTraps)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstSetTraps (i4SetValFsMIRstSetTraps);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRstSetTraps
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstSetTraps (UINT4 *pu4ErrorCode,
                          INT4 i4FsMIDot1wFutureRstContextId,
                          INT4 i4TestValFsMIRstSetTraps)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstSetTraps (pu4ErrorCode, i4TestValFsMIRstSetTraps);

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1wFsRstTrapsControlTable
 Input       :  The Indices
                FsMIDot1wFutureRstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1wFsRstTrapsControlTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRstPortTrapNotificationTable
 Input       :  The Indices
                FsMIRstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRstPortTrapNotificationTable (INT4
                                                          i4FsMIRstPortTrapIndex)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsRstPortTrapNotificationTable
        ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRstPortTrapNotificationTable
 Input       :  The Indices
                FsMIRstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRstPortTrapNotificationTable (INT4 *pi4FsMIRstPortTrapIndex)
{
    return nmhGetNextIndexFsMIRstPortTrapNotificationTable (0,
                                                            pi4FsMIRstPortTrapIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexFsMIRstPortTrapNotificationTable
 Input       :  The Indices
                FsMIRstPortTrapIndex
                nextFsMIRstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRstPortTrapNotificationTable (INT4 i4FsMIRstPortTrapIndex,
                                                 INT4
                                                 *pi4NextFsMIRstPortTrapIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIRstPortTrapIndex);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsRstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsMIRstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIRstPortTrapIndex <
                 (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                &&
                (AstIsRstStartedInContext
                 (AST_IFENTRY_CONTEXT_ID (pAstPortEntry))))
            {
                *pi4NextFsMIRstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsMIRstPortMigrationType
 Input       :  The Indices
                FsMIRstPortTrapIndex

                The Object 
                retValFsMIRstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortMigrationType (INT4 i4FsMIRstPortTrapIndex,
                                INT4 *pi4RetValFsMIRstPortMigrationType)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortMigrationType ((INT4) u2LocalPortId,
                                             pi4RetValFsMIRstPortMigrationType);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPktErrType
 Input       :  The Indices
                FsMIRstPortTrapIndex

                The Object 
                retValFsMIRstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPktErrType (INT4 i4FsMIRstPortTrapIndex,
                         INT4 *pi4RetValFsMIRstPktErrType)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPktErrType ((INT4) u2LocalPortId,
                                      pi4RetValFsMIRstPktErrType);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPktErrVal
 Input       :  The Indices
                FsMIRstPortTrapIndex

                The Object 
                retValFsMIRstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPktErrVal (INT4 i4FsMIRstPortTrapIndex,
                        INT4 *pi4RetValFsMIRstPktErrVal)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPktErrVal ((INT4) u2LocalPortId,
                                     pi4RetValFsMIRstPktErrVal);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortRoleType
 Input       :  The Indices
                FsMIRstPortTrapIndex

        The Object
        RetValFsMIRstPortRol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortRoleType (INT4 i4FsMIRstPortTrapIndex,
                           INT4 *pi4RetValFsMIRstPortRole)
{
    /* This Routine should be called before the PortRole Selection is done *
     * Then only it will give the Correct Value                *
     * In other words, only during the Port Role Selection,            *
     * the values of the Selected Role and                 *
     * Old Role (Routine below) Differ                     *
     * After Selection is finished, both will be similair         */

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRoleType ((INT4) u2LocalPortId,
                                        pi4RetValFsMIRstPortRole);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIRstOldRoleType
 Input       :  The Indices
                FsMIRstPortTrapIndex

                The Object 
                retValFsMIRstOldPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstOldRoleType (INT4 i4FsMIRstPortTrapIndex,
                          INT4 *pi4RetValFsMIRstOldPortRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstOldPortRoleType ((INT4) u2LocalPortId,
                                           pi4RetValFsMIRstOldPortRole);
    AstReleaseContext ();

    return i1RetVal;

}

/* RSTP clear statistics feature*/
/****************************************************************************
 Function    :  nmhGetFsMIRstClearBridgeStats
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                retValFsMIRstClearBridgeStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstClearBridgeStats (INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 *pi4RetValFsMIRstClearBridgeStats)
{
    tAstBridgeEntry    *pBrgEntry = NULL;

    /*Switch to associated context */
    if (RST_SUCCESS != AstSelectContext (i4FsMIDot1wFutureRstContextId))
    {
        return SNMP_FAILURE;
    }

    /* Obtain the existing value of bBridgeClearStats  */
    pBrgEntry = AST_GET_BRGENTRY ();
    if (NULL == pBrgEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIRstClearBridgeStats = pBrgEntry->bBridgeClearStats;

    /*Switch back context */
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstClearBridgeStats
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                setValFsMIRstClearBridgeStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstClearBridgeStats (INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 i4SetValFsMIRstClearBridgeStats)
{
    tAstBridgeEntry    *pBrgEntry = NULL;

    /*Switch to associated context */
    if (RST_SUCCESS != AstSelectContext (i4FsMIDot1wFutureRstContextId))
    {
        return SNMP_FAILURE;
    }

    pBrgEntry = AST_GET_BRGENTRY ();
    if (NULL == pBrgEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((RST_DISABLED == pBrgEntry->bBridgeClearStats) &&
        (RST_ENABLED == i4SetValFsMIRstClearBridgeStats))
    {
        pBrgEntry->bBridgeClearStats = RST_ENABLED;
        RstResetCounters ();
        pBrgEntry->bBridgeClearStats = RST_DISABLED;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstClearBridgeStats
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object 
                testValFsMIRstClearBridgeStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstClearBridgeStats (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIDot1wFutureRstContextId,
                                  INT4 i4TestValFsMIRstClearBridgeStats)
{
    /*Switch to associated context */
    if (RST_SUCCESS != AstSelectContext (i4FsMIDot1wFutureRstContextId))
    {
        return SNMP_FAILURE;
    }

    /*Check the range of values */
    if ((RST_DISABLED != i4TestValFsMIRstClearBridgeStats) &&
        (RST_ENABLED != i4TestValFsMIRstClearBridgeStats))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*Switch back context */
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRstPortClearStats
 Input       :  The Indices
                FsMIRstPort

                The Object 
                retValFsMIRstPortClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRstPortClearStats (INT4 i4FsMIRstPort,
                             INT4 *pi4RetValFsMIRstPortClearStats)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /*Switch to associated context */
    if (RST_SUCCESS != AstSelectContext ((INT4) u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (u2LocalPortId) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);

    if (NULL == pAstPortEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIRstPortClearStats = pAstPortEntry->bPortClearStats;

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortClearStats
 Input       :  The Indices
                FsMIRstPort

                The Object 
                setValFsMIRstPortClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortClearStats (INT4 i4FsMIRstPort,
                             INT4 i4SetValFsMIRstPortClearStats)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (RST_SUCCESS != AstSelectContext ((INT4) u4ContextId))
    {
        return SNMP_FAILURE;
    }

    /*Check for a valid port number */
    if (RST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
    if (NULL == pPortEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((RST_FALSE == pPortEntry->bPortClearStats) &&
        (RST_TRUE == i4SetValFsMIRstPortClearStats))
    {
        pPortEntry->bPortClearStats = RST_TRUE;
        RstResetPortCounters (u2LocalPortId);
        pPortEntry->bPortClearStats = RST_FALSE;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortRowStatus
 Input       :  The Indices
                FsMIRstPort

                The Object
                setValFsMIRstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortRowStatus (INT4 i4FsMIRstPort,
                            INT4 i4SetValFsMIRstPortRowStatus)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIRstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortRowStatus (u2LocalPortId,
                                         i4SetValFsMIRstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortBpduGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                setValFsMIRstPortBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortBpduGuard (INT4 i4FsMIRstPort,
                            INT4 i4SetValFsMIRstPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortBpduGuard ((INT4) u2LocalPortId,
                                         i4SetValFsMIRstPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIRstStpPerfStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                setValFsMIRstStpPerfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstStpPerfStatus (INT4 i4FsMIDot1wFutureRstContextId,
                            INT4 i4SetValFsMIRstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1wFutureRstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstStpPerfStatus (i4SetValFsMIRstStpPerfStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhSetFsMIRstPortRootGuard
  Input       :  The Indices
                 FsMIRstPort
 
                 The Object
                 setValFsMIRstPortRootGuard
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortRootGuard (INT4 i4FsMIRstPort,
                            INT4 i4SetValFsMIRstPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsRstPortRootGuard ((INT4) u2LocalPortId,
                                  i4SetValFsMIRstPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
   Function    :  nmhSetFsMIRstPortErrorRecovery
   Input       :  The Indices
                  FsMIRstPort

                  The Object
                  setValFsMIRstPortErrorRecovery

   Output      :  The Set Low Lev Routine Take the Indices &
                  Sets the Value accordingly.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortErrorRecovery (INT4 i4FsMIRstPort,
                                INT4 i4SetValFsMIRstPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsRstPortErrorRecovery ((INT4) u2LocalPortId,
                                             i4SetValFsMIRstPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsMIRstPort

                The Object
                setValFsMIRstPortStpModeDot1wEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRstPortStpModeDot1wEnabled (INT4 i4FsMIRstPort,
                                      INT4
                                      i4SetValFsMIRstPortStpModeDot1wEnabled)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsRstPortStpModeDot1wEnabled ((INT4) u2LocalPortId,
                                            i4SetValFsMIRstPortStpModeDot1wEnabled);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortClearStats
 Input       :  The Indices
                FsMIRstPort

                The Object 
                testValFsMIRstPortClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortClearStats (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIRstPort,
                                INT4 i4TestValFsMIRstPortClearStats)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (RST_SUCCESS != AstSelectContext ((INT4) u4ContextId))
    {
        return SNMP_FAILURE;
    }
    /*Check for a valid port number */
    if (RST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((RST_FALSE != i4TestValFsMIRstPortClearStats) &&
        (RST_TRUE != i4TestValFsMIRstPortClearStats))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortRowStatus
 Input       :  The Indices
                FsMIRstPort

                The Object
                testValFsMIRstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                               INT4 i4TestValFsMIRstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIRstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsRstPortRowStatus (pu4ErrorCode, u2LocalPortId,
                                            i4TestValFsMIRstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortBpduGuard
 Input       :  The Indices
                FsMIRstPort

                The Object
                testValFsMIRstPortBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortBpduGuard (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIRstPort,
                               INT4 i4TestValFsMIRstPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortBpduGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                     i4TestValFsMIRstPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstStpPerfStatus
 Input       :  The Indices
                FsMIDot1wFutureRstContextId

                The Object
                testValFsMIRstStpPerfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstStpPerfStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1wFutureRstContextId,
                               INT4 i4TestValFsMIRstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1wFutureRstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1wFutureRstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstStpPerfStatus (pu4ErrorCode,
                                     i4TestValFsMIRstStpPerfStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhTestv2FsMIRstPortRootGuard
  Input       :  The Indices
                 FsMIRstPort

                 The Object
                 testValFsMIRstPortRootGuard
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                               INT4 i4TestValFsMIRstPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortRootGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                     i4TestValFsMIRstPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/* RSTP clear statistics feature*/
/****************************************************************************
   Function    :  nmhTestv2FsMIRstPortErrorRecovery
   Input       :  The Indices
                  FsMIRstPort

                  The Object
                  testValFsMIRstPortErrorRecovery
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
   Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                                                      Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortErrorRecovery (UINT4 *pu4ErrorCode, INT4 i4FsMIRstPort,
                                   INT4 i4TestValFsMIRstPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortErrorRecovery (pu4ErrorCode, (INT4) u2LocalPortId,
                                         i4TestValFsMIRstPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsMIRstPort

                The Object
                testValFsMIRstPortStpModeDot1wEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRstPortStpModeDot1wEnabled (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIRstPort,
                                         INT4
                                         i4TestValFsMIRstPortStpModeDot1wEnabled)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsRstPortStpModeDot1wEnabled (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIRstPortStpModeDot1wEnabled);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 * Function    :  nmhGetFsMIRstPortTCDetectedCount
 * Input       :  The Indices
 *                i4ContextId
 *
 *                The Object
 *                pu4RetValFsMIRstPortTCReceivedCount
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *******************************************************************************/
INT1
nmhGetFsMIRstPortTCDetectedCount (INT4 i4FsMIRstPort,
                                  UINT4 *pu4RetValFsMIRstPortTCDetectedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortTCDetectedCount ((INT4) u2LocalPortId,
                                        pu4RetValFsMIRstPortTCDetectedCount);
    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortTCReceivedCount
 *  Input       :  The Indices
 *                 i4ContextId
 * 
 *                 The Object
 *                 pu4RetValFsMIRstPortTCReceivedCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortTCReceivedCount (INT4 i4FsMIRstPort,
                                  UINT4 *pu4RetValFsMIRstPortTCReceivedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsRstPortTCReceivedCount ((INT4) u2LocalPortId,
                                        pu4RetValFsMIRstPortTCReceivedCount);
    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 * Function    :  nmhGetFsMIRstPortRcvInfoWhileExpCount
 * Input       :  The Indices
 *                FsMIRstPort
 *
 *                The Object
 *                retValFsMIRstPortRcvInfoWhileExpCount
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortRcvInfoWhileExpCount (INT4 i4FsMIRstPort,
                                       UINT4
                                       *pu4RetValFsMIRstPortRcvInfoWhileExpCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsRstPortRcvInfoWhileExpCount ((INT4) u2LocalPortId,
                                             pu4RetValFsMIRstPortRcvInfoWhileExpCount);
    AstReleaseContext ();
    return i1RetVal;
}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortProposalPktsSent
 *  Input       :  The Indices
 *                 FsMIRstPort
 *   
 *                 The Object
 *                 retValFsMIRstPortProposalPktsSent
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortProposalPktsSent (INT4 i4FsMIRstPort,
                                   UINT4 *pu4RetValFsMIRstPortProposalPktsSent)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortProposalPktsSent ((INT4) u2LocalPortId,
                                                pu4RetValFsMIRstPortProposalPktsSent);

    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortAgreementPktSent
 *  Input       :  The Indices
 *                 FsMIRstPort
 *  
 *                 The Object
 *                 retValFsMIRstPortAgreementPktSent
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortAgreementPktSent (INT4 i4FsMIRstPort,
                                   UINT4 *pu4RetValFsMIRstPortAgreementPktSent)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortAgreementPktSent ((INT4) u2LocalPortId,
                                                pu4RetValFsMIRstPortAgreementPktSent);

    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortProposalPktsRcvd
 *  Input       :  The Indices
 *                 FsMIRstPort
 *
 *                 The Object
 *                 retValFsMIRstPortProposalPktsRcvd
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortProposalPktsRcvd (INT4 i4FsMIRstPort,
                                   UINT4 *pu4RetValFsMIRstPortProposalPktsRcvd)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortProposalPktsRcvd ((INT4) u2LocalPortId,
                                                pu4RetValFsMIRstPortProposalPktsRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortAgreementPktRcvd
 *  Input       :  The Indices
 *                 FsMIRstPort
 *
 *                 The Object
 *                 retValFsMIRstPortAgreementPktRcvd
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortAgreementPktRcvd (INT4 i4FsMIRstPort,
                                   UINT4 *pu4RetValFsMIRstPortAgreementPktRcvd)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortAgreementPktRcvd ((INT4) u2LocalPortId,
                                                pu4RetValFsMIRstPortAgreementPktRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
 *Function    :  nmhGetFsMIRstPortImpossibleStateOcc
 *Input       :  The Indices
 *               FsRstPort
 *
 *               The Object
 *               retValFsMIRstPortImpossibleStateOcc
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ********************************************************************************/
INT1
nmhGetFsMIRstPortImpStateOccurCount (INT4 i4FsMIRstPort,
                                     UINT4
                                     *pu4RetValFsMIRstPortImpossibleStateOcc)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsRstPortImpStateOccurCount ((INT4) u2LocalPortId,
                                           pu4RetValFsMIRstPortImpossibleStateOcc);
    AstReleaseContext ();
    return i1RetVal;
}

 /****************************************************************************
 *Function    :  nmhGetFsMIRstPortOldPortState
 *Input       :  The Indices
 *               FsMIRstPort
 * 
 *               The Object
 *               retValFsMIRstPortOldPortState
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortOldPortState (INT4 i4FsMIRstPort,
                               INT4 *pi4RetValFsMIRstPortOldPortState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsRstPortOldPortState ((INT4) u2LocalPortId,
                                     pi4RetValFsMIRstPortOldPortState);
    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
*Function    :  nmhGetFsMIRstPortTCDetectedTimeStamp
*Input       :  The Indices
*               FsMIDot1wFutureRstContextId
*
*               The Object
*               retValFsMIRstTCDetectedTimeStamp
*Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
*Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1
nmhGetFsMIRstPortTCDetectedTimeStamp (INT4 i4FsMIRstPort,
                                      UINT4
                                      *pu4RetValFsMIRstTCDetectedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTCDetectedTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstTCDetectedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * *Function    :  nmhGetFsMIRstPortTCReceivedTimeStamp
 * *Input       :  The Indices
 * *               FsMIDot1wFutureRstContextId
 * *
 * *               The Object
 * *               retValFsMIRstTCReceivedTimeStamp
 * *Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ******************************************************************************/
INT1
nmhGetFsMIRstPortTCReceivedTimeStamp (INT4 i4FsMIRstPort,
                                      UINT4
                                      *pu4RetValFsMIRstTCReceivedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortTCReceivedTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstTCReceivedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortRcvInfoWhileExpTimeStamp
 *  Input       :  The Indices
 *                 FsMIDot1wFutureRstContextId
 *
 *                 The Object
 *                 retValFsMIRstRcvInfoWhileExpTimeStamp
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortRcvInfoWhileExpTimeStamp (INT4 i4FsMIRstPort,
                                           UINT4
                                           *pu4RetValFsMIRstRcvInfoWhileExpTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortRcvInfoWhileExpTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstRcvInfoWhileExpTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
*  Function    :  nmhGetFsMIRstPortProposalPktSentTimeStamp
*  Input       :  The Indices
*                 FsMIDot1wFutureRstContextId
*
*                 The Object
*                 retValFsMIRstProposalPktSentTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIRstPortProposalPktSentTimeStamp (INT4 i4FsMIRstPort,
                                           UINT4
                                           *pu4RetValFsMIRstProposalPktSentTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortProposalPktSentTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstProposalPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
*  Function    :  nmhGetFsMIRstPortProposalPktRcvdTimeStamp
*  Input       :  The Indices
*                 FsMIDot1wFutureRstContextId
*
*                 The Object
*                 retValFsMIRstProposalPktRcvdTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIRstPortProposalPktRcvdTimeStamp (INT4 i4FsMIRstPort,
                                           UINT4
                                           *pu4RetValFsMIRstProposalPktRcvdTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortProposalPktRcvdTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstProposalPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
*  Function    :  nmhGetFsMIRstPortAgreementPktSentTimeStamp
*  Input       :  The Indices
*                 FsMIDot1wFutureRstContextId
*
*                 The Object
*                 retValFsMIRstAgreementPktSentTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIRstPortAgreementPktSentTimeStamp (INT4 i4FsMIRstPort,
                                            UINT4
                                            *pu4RetValFsMIRstAgreementPktSentTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortAgreementPktSentTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstAgreementPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 * Function    :  nmhGetFsMIRstPortAgreementPktRcvdTimeStamp
 * Input       :  The Indices
 *                FsMIDot1wFutureRstContextId
 * 
 *                The Object
 *                retValFsMIRstAgreementPktRcvdTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortAgreementPktRcvdTimeStamp (INT4 i4FsMIRstPort,
                                            UINT4
                                            *pu4RetValFsMIRstAgreementPktRcvdTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortAgreementPktRcvdTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstAgreementPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 *  Function    :  nmhGetFsMIRstPortImpStateOccurTimeStamp
 *  Input       :  The Indices
 *                 FsMIDot1wFutureRstContextId
 * 
 *                 The Object
 *                 retValFsMIRstImpStateOccurTimeStamp
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                          store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsMIRstPortImpStateOccurTimeStamp (INT4 i4FsMIRstPort,
                                         UINT4
                                         *pu4RetValFsMIRstImpStateOccurTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIRstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsRstPortImpStateOccurTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIRstImpStateOccurTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}
