/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpstsm.c,v 1.40 2017/12/01 11:56:12 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port State Transition State Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortStateTrMachine                                */
/*                                                                           */
/* Description        : This is the main routine for the Port State          */
/*                      Transition State Machine.                            */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortStateTrMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo,
                       UINT2 u2InstanceId)
{
    UINT2               u2State;
    INT4                i4RetVal = RST_SUCCESS;

    u2State = (UINT2) pPerStPortInfo->u1PstateTrSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "STSM: Port %s: Inst %d: State Tr Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId,
                  gaaau1AstSemEvent[AST_STSM][u2Event],
                  gaaau1AstSemState[AST_STSM][u2State]);

    AST_DBG_ARG4 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: State Tr Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId,
                  gaaau1AstSemEvent[AST_STSM][u2Event],
                  gaaau1AstSemState[AST_STSM][u2State]);

    if (RST_PORT_STATE_TR_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                 "STSM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    i4RetVal = (*(RST_PORT_STATE_TR_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo, u2InstanceId);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                 "STSM: Event routine returned failure !!!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmMakeDiscarding                          */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'DISCARDING'.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmMakeDiscarding (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2InstanceId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
#ifdef NPAPI_WANTED
#ifdef MSTP_WANTED
    UINT4               u4PhyPort = AST_INIT_VAL;
#endif
#endif
    UINT2               u2PortNum;
    UINT4               u4RestartTime = AST_INIT_VAL;
    UINT1               u1Status;
    UINT1               u1PrevStatus = 0;
    UINT1               u1OperStatus = AST_INIT_VAL;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (AST_GET_BRG_PORT_TYPE (pPortEntry) != AST_PROVIDER_EDGE_PORT)
    {
        if (AstCfaGetIfOperStatus (AST_GET_IFINDEX (u2PortNum),
                                   &u1OperStatus) != CFA_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "STSM: OperStatus for Port:%u returned failure!\n",
                          AST_GET_IFINDEX (u2PortNum));
            return RST_SUCCESS;
        }
    }
    else
    {
        if (L2IwfGetPbPortOperStatus
            (STP_MODULE, AST_PB_PORT_INFO (pPortEntry)->u4CepIfIndex,
             pPortEntry->u2ProtocolPort, &u1OperStatus) != L2IWF_SUCCESS)
        {
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "STSM: OperStatus for PEP Port:(%u,%d) returned failure!\n",
                          AST_PB_PORT_INFO (pPortEntry)->u4CepIfIndex,
                          pPortEntry->u2ProtocolPort);
            return RST_SUCCESS;
        }
    }

    /*  In esynchronnus NPAPI mode 
     *    - Delay updating L2IWF and the SEM variables Learning and Forwarding
     *      until callback is received from NPAPI.
     *  Exceptions -
     *   If this function is called as part of
     *      1) spanning tree per-port disable or
     *      2) spanning tree module disable
     *   then the Learning and Forwarding variables can be reset immediately
     *   here, since the actual port state programmed in HW will be Forwarding.
     *   Handling callback in these 2 cases causes the SEM variable Forwarding
     *   to be wrongly set to TRUE. Hence callback handling is disabled for
     *   these cases.    
     *   So L2Iwf should also be updated synchronously for these 2 cases.
     *
     *   NOTE: In RSTP, state machines are not triggered for the Module Disable
     *   case - L2Iwf is updated directly in a for loop.
     */

    /* Previous state is obtained to avoid repeated hardware programming.
     * But we should not get the previous state from L2IWF,
     * since in case of asynchronous hardwares,
     * L2Iwf port state may not reflect the last programmed port state
     * (i.e. if the port state reverts back before the NPAPI callback arrives)
     * Hence maintaining a separate variable to track the port state that was
     * last programmed in hardware.
     */
    if (((AST_CURR_CONTEXT_INFO ())->ppPerStInfo == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId] == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]->
            ppPerStPortInfo == NULL))
    {
        return RST_FAILURE;
    }
    if (pRstPortInfo->bPortEnabled == RST_TRUE)
    {
        u1PrevStatus = AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum);
    }
    pPerStPortInfo->u1PstateTrSmState = (UINT1) RST_PSTATETRSM_STATE_DISCARDING;
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Moved to state DISCARDING \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    /* 
     * If this function is called when
     *       1) RSTP/MSTP Module is disabled or
     *       2) Port is disabled in RSTP/MSTP alone 
     *          by configuration
     *    then set port state as FORWARDING in the 
     *    common database and indicate to Hw and Garp.
     * Else
     *    make the port state as DISCARDING in the 
     *    common database and indicate to Hw and Garp.
     */

    if (((AST_GET_SYSTEMACTION == RST_DISABLED) ||
         (pRstPortInfo->bPortEnabled == RST_FALSE)) &&
        (u1OperStatus == CFA_IF_UP))
    {
        u1Status = AST_PORT_STATE_FORWARDING;

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "STSM: Port %s: Inst %d: Port State is FORWARDING\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);
    }
    else
    {
        u1Status = AST_PORT_STATE_DISCARDING;

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "STSM: Port %s: Inst %d: Port State is DISCARDING\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);
    }

    /* if previous port state is same as the current port state, 
     * Do not invoke NP trigger to set the port state **/

    if ((u1OperStatus == CFA_IF_DOWN) && (u1Status == u1PrevStatus))
    {
        return RST_SUCCESS;
    }

    /*  In asynchronous NPAPI mode 
     *    - Delay updating L2IWF and the SEM variables Learning and Forwarding
     *      until callback is received from NPAPI.
     *  Exceptions -
     *   If this function is called as part of
     *      1) spanning tree per-port disable or
     *      2) spanning tree module disable
     *   then the Learning and Forwarding variables can be reset immediately
     *   here since the actual port state programmed will be Forwarding.
     *   Similarly L2Iwf can also be updated immediately.
     *
     *   NOTE: In RSTP, state machines are not triggered for the module disable
     *   case and L2Iwf is updated directly in a for loop.
     */
    if ((u1PrevStatus != RST_FALSE) && (u1Status == u1PrevStatus))
    {
        if ((AST_GET_SYSTEMACTION == RST_DISABLED) ||
            (pRstPortInfo->bPortEnabled == RST_FALSE))
        {
            /* Hardware has already been programmed as Forwarding.
             * Just update the SEM variables to be in sync with the state */
            pRstPortInfo->bLearning = RST_FALSE;
            pRstPortInfo->bForwarding = RST_FALSE;

#ifdef NPAPI_WANTED
            /* Indicate Forwarding state to L2Iwf explicitly since u1PrevStatus
             * only indicates the last programmed state in hardware but may
             * not reflect the actual L2Iwf state in case of
             * Asynchronous NPAPI mode. */
            if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
            {
                AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNum, u1Status);
            }
#endif
        }
        return RST_SUCCESS;
    }

    AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;
#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if (AST_IS_RST_ENABLED () && (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))

    {
        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;

        if (AstMiRstpNpSetPortState (u2PortNum, u1Status) == RST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       (UINT2) u1Status);
            return RST_SUCCESS;
        }
    }

#ifdef MSTP_WANTED
    /* Program port state for instance */
    else if (AST_IS_MST_ENABLED () &&
             (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = MST_TRUE;

        /* If it is sisp enabled logical port, then invoke the hardware
         * with the corresponding physical port
         * */
        if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
        {
            u4PhyPort = AST_GET_PHY_PORT (u2PortNum);
        }
        else
        {
            /* Get the corresponding global ifindex. It is a normal
             * port
             * */
            u4PhyPort = AST_GET_IFINDEX (u2PortNum);
        }
        if (FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                              u4PhyPort,
                                              u2InstanceId,
                                              u1Status) == FNP_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       (UINT2) u1Status);
            return RST_SUCCESS;
        }
    }
#endif /* MSTP_WANTED */

    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) = u1Status;

    if ((AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS) &&
        (AST_GET_SYSTEMACTION != RST_DISABLED) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE))
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return RST_SUCCESS;
    }
#else
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) = u1Status;
#endif /* NPAPI_WANTED */

    if (u1Status == AST_PORT_STATE_FORWARDING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_SUCCESS;
    }
    else
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;
    }
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "STSM_Discarding: Port %s: Learning = Forwarding = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    (VOID) RstPstateTrSmDisableLearning (u2PortNum, u2InstanceId);
    pRstPortInfo->bLearning = RST_FALSE;
    (VOID) RstPstateTrSmDisableForwarding (u2PortNum, u2InstanceId);
    pRstPortInfo->bForwarding = RST_FALSE;

    /* Indicate to L2Iwf, Hardware and GARP only if there is change in 
     * port state */
    AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNum, u1Status);
    if (u1PrevStatus != AST_PORT_STATE_DISCARDING)
    {
        AstGetStateStr (u1PrevStatus, au1OldState);
        UtlGetTimeStr (au1TimeStr);
        if (AST_IS_RST_ENABLED ())
        {
            AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    AST_PORT_STATE_DISCARDING, u1PrevStatus,
                                    u2InstanceId, (INT1 *) AST_BRG_TRAPS_OID,
                                    AST_BRG_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Port:%s  Old State:%s New State:Discarding"
                              " Time Stamp: %s\r\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1OldState, au1TimeStr);
        }
#ifdef MSTP_WANTED
        else if (AST_IS_MST_ENABLED ())
        {
            if (u2InstanceId == MST_CIST_CONTEXT)
            {
                AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                        RST_PSTATETRSM_STATE_DISCARDING,
                                        u1PrevStatus, u2InstanceId,
                                        (INT1 *) AST_MST_TRAPS_OID,
                                        AST_MST_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old State:%s New State:Discarding"
                                  " Time Stamp: %s\r\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstanceId,
                                  au1OldState, au1TimeStr);
            }
            else
            {
                if (MstIsPortMemberOfInst
                    ((INT4) (AST_GET_IFINDEX ((INT4) u2PortNum)),
                     (INT4) u2InstanceId) != MST_FALSE)
                {
                    AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                            AST_PORT_STATE_DISCARDING,
                                            u1PrevStatus, u2InstanceId,
                                            (INT1 *) AST_MST_TRAPS_OID,
                                            AST_MST_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                                      "Port:%s Instance:%u Old State:%s New State:Discarding"
                                      " Time Stamp: %s\r\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2InstanceId, au1OldState, au1TimeStr);
                }
            }
        }
#endif
    }
    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
        (pPortEntry->bLoopInconsistent == RST_TRUE) &&
        (pPortEntry->bLoopGuard == RST_TRUE))
    {
        pPortEntry->u1RecScenario = LOOP_INC_RECOVERY;
        AST_GET_RANDOM_TIME (RST_DEFAULT_RESTART_INTERVAL, u4RestartTime);
        u4RestartTime = u4RestartTime * AST_CENTI_SECONDS;
        AST_DBG_ARG2 (AST_ALL_FLAG,
                      "TMR: Port %s: Starting RESTART Timer with duration %u\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u4RestartTime);
        if (AstStartTimer (pPortEntry, u2InstanceId,
                           AST_TMR_TYPE_RESTART,
                           (UINT2) u4RestartTime) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;

        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmMakeLearning                            */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'LEARNING'.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmMakeLearning (tAstPerStPortInfo * pPerStPortInfo,
                           UINT2 u2InstanceId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
#ifdef NPAPI_WANTED
#ifdef MSTP_WANTED
    UINT4               u4PhyPort = AST_INIT_VAL;
#endif
#endif
    UINT2               u2PortNum;
    UINT1               u1PrevStatus = AST_INIT_VAL;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Previous state is obtained to avoid repeated hardware programming.
     * But we should not get the previous state from L2IWF,
     * since in case of asynchronous hardwares,
     * L2Iwf port state may not reflect the last programmed port state
     * (i.e. if the port state reverts back before the NPAPI callback arrives)
     */
    if (((AST_CURR_CONTEXT_INFO ())->ppPerStInfo == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId] == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]->
            ppPerStPortInfo == NULL))
    {
        return RST_FAILURE;
    }
    u1PrevStatus = AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum);

    pPerStPortInfo->u1PstateTrSmState = (UINT1) RST_PSTATETRSM_STATE_LEARNING;

    if (u1PrevStatus == AST_PORT_STATE_LEARNING)
    {
        return RST_SUCCESS;
    }

    AstGetStateStr (u1PrevStatus, au1OldState);
    UtlGetTimeStr (au1TimeStr);
    if (AST_IS_RST_ENABLED ())
    {
        AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                AST_PORT_STATE_LEARNING, u1PrevStatus,
                                u2InstanceId, (INT1 *) AST_BRG_TRAPS_OID,
                                AST_BRG_TRAPS_OID_LEN);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Port:%s  Old State:%s New State:Learning Time Stamp: %s\r\n",
                          AST_GET_IFINDEX_STR (u2PortNum), au1OldState,
                          au1TimeStr);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    RST_PSTATETRSM_STATE_LEARNING, u1PrevStatus,
                                    u2InstanceId, (INT1 *) AST_MST_TRAPS_OID,
                                    AST_MST_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Port:%s Instance:%u Old State:%s New State:Learning Time Stamp: %s\r\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId,
                              au1OldState, au1TimeStr);
        }
        else
        {

            if (MstIsPortMemberOfInst
                ((INT4) (AST_GET_IFINDEX ((INT4) u2PortNum)),
                 (INT4) u2InstanceId) != MST_FALSE)
            {
                AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                        AST_PORT_STATE_LEARNING, u1PrevStatus,
                                        u2InstanceId,
                                        (INT1 *) AST_MST_TRAPS_OID,
                                        AST_MST_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old State:%s New State:Learning Time Stamp: %s\r\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId,
                                  au1OldState, au1TimeStr);
            }
        }
    }
#endif
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Moved to state LEARNING \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if (AST_IS_RST_ENABLED () && (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;

        if (AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_LEARNING)
            == RST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       AST_PORT_STATE_LEARNING);
            return RST_SUCCESS;
        }
    }

#ifdef MSTP_WANTED
    /* Program port state for instance */
    else if (AST_IS_MST_ENABLED () &&
             (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = MST_TRUE;

        /* If it is sisp enabled logical port, then invoke the hardware
         * with the corresponding physical port
         * */
        if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
        {
            u4PhyPort = AST_GET_PHY_PORT (u2PortNum);
        }
        else
        {
            /* Get the corresponding global ifindex. It is a normal
             * port
             * */
            u4PhyPort = AST_GET_IFINDEX (u2PortNum);
        }

        if (FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (), u4PhyPort,
                                              u2InstanceId,
                                              AST_PORT_STATE_LEARNING)
            == FNP_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       AST_PORT_STATE_LEARNING);
            return RST_SUCCESS;
        }
    }
#endif /* MSTP_WANTED */

    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AST_PORT_STATE_LEARNING;
    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return RST_SUCCESS;
    }
#else
    UNUSED_PARAM (pPortEntry);
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AST_PORT_STATE_LEARNING;
#endif /* NPAPI_WANTED */

    pPerStPortInfo->i4NpPortStateStatus = AST_LEARN_SUCCESS;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "STSM_Learning: Port %s: Learning = TRUE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    (VOID) RstPstateTrSmEnableLearning (u2PortNum, u2InstanceId);
    pRstPortInfo->bLearning = RST_TRUE;

    /* Indicate to L2Iwf, Hardware only if there is change in port state */
    AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNum, (UINT1)
                                AST_PORT_STATE_LEARNING);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmMakeForwarding                          */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'FORWARDING'.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmMakeForwarding (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2InstanceId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT1               u1PrevStatus = AST_INIT_VAL;
    UINT2               u2PortNum;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    AST_MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);
#ifdef NPAPI_WANTED
#ifdef MSTP_WANTED
    UINT4               u4PhyPort = AST_INIT_VAL;

#endif
#endif
    u2PortNum = pPerStPortInfo->u2PortNo;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /* Previous state is obtained to avoid repeated hardware programming.
     * But we should not get the previous state from L2IWF,
     * since in case of asynchronous hardwares,
     * L2Iwf port state may not reflect the last programmed port state
     * (i.e. if the port state reverts back before the NPAPI callback arrives)
     */
    if (((AST_CURR_CONTEXT_INFO ())->ppPerStInfo == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId] == NULL)
        || ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]->
            ppPerStPortInfo == NULL))
    {
        return RST_FAILURE;
    }
    u1PrevStatus = AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum);

    pPerStPortInfo->u1PstateTrSmState = (UINT1) RST_PSTATETRSM_STATE_FORWARDING;
    if (u1PrevStatus == AST_PORT_STATE_FORWARDING)
    {
        return RST_SUCCESS;
    }

    AstGetStateStr (u1PrevStatus, au1OldState);
    UtlGetTimeStr (au1TimeStr);
    if (AST_IS_RST_ENABLED ())
    {
        AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                AST_PORT_STATE_FORWARDING, u1PrevStatus,
                                u2InstanceId, (INT1 *) AST_BRG_TRAPS_OID,
                                AST_BRG_TRAPS_OID_LEN);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Port:%s  Old State:%s New State:Forwarding Time Stamp: %s\r\n",
                          AST_GET_IFINDEX_STR (u2PortNum), au1OldState,
                          au1TimeStr);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_ENABLED ())
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    RST_PSTATETRSM_STATE_FORWARDING,
                                    u1PrevStatus, u2InstanceId,
                                    (INT1 *) AST_MST_TRAPS_OID,
                                    AST_MST_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Port:%s Instance:%u Old State:%s New State:Forwarding Time Stamp: %s\r\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId,
                              au1OldState, au1TimeStr);
        }
        else
        {
            if (MstIsPortMemberOfInst
                ((INT4) (AST_GET_IFINDEX ((INT4) u2PortNum)),
                 (INT4) u2InstanceId) != MST_FALSE)
            {
                AstPortStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                        AST_PORT_STATE_FORWARDING, u1PrevStatus,
                                        u2InstanceId,
                                        (INT1 *) AST_MST_TRAPS_OID,
                                        AST_MST_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old State:%s New State:Forwarding Time Stamp: %s\r\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId,
                                  au1OldState, au1TimeStr);
            }
        }
    }
#endif
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Moved to state FORWARDING \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if (AST_IS_RST_ENABLED () && (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_TRUE;
        if (AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_FORWARDING)
            == RST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       AST_PORT_STATE_FORWARDING);
            return RST_SUCCESS;
        }
    }
#ifdef MSTP_WANTED
    /* Program port state for instance */
    else if (AST_IS_MST_ENABLED () &&
             (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
#ifdef NPAPI_WANTED
        if (AST_GET_IFINDEX (u2PortNum) > BRG_MAX_PHY_PORTS &&
            AST_GET_IFINDEX (u2PortNum) < BRG_MAX_PHY_PLUS_LOG_PORTS)
        {
            gAstStateTransStatus = AST_TRANS_INTRANSITION;
        }
#endif

        AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = MST_TRUE;

        /* If it is sisp enabled logical port, then invoke the hardware
         * with the corresponding physical port
         * */
        if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
        {
            u4PhyPort = AST_GET_PHY_PORT (u2PortNum);
        }
        else
        {
            /* Get the corresponding global ifindex. It is a normal
             * port
             * */
            u4PhyPort = AST_GET_IFINDEX (u2PortNum);
        }

        if (FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (), u4PhyPort,
                                              u2InstanceId,
                                              AST_PORT_STATE_FORWARDING)
            == FNP_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                       AST_PORT_STATE_FORWARDING);
            return RST_SUCCESS;
        }

    }
#endif /* MSTP_WANTED */
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AST_PORT_STATE_FORWARDING;

    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return RST_SUCCESS;
    }
#else
    UNUSED_PARAM (pPortEntry);
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AST_PORT_STATE_FORWARDING;
#endif /* NPAPI_WANTED */

    pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_SUCCESS;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "STSM_Forwarding: Port %s: Forwarding = TRUE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    (VOID) RstPstateTrSmEnableForwarding (u2PortNum, u2InstanceId);
    pRstPortInfo->bForwarding = RST_TRUE;

    /* Indicate to L2Iwf, Hardware and GARP only if there is change in port 
     * state */
    AstSetInstPortStateToL2Iwf (u2InstanceId, u2PortNum,
                                (UINT1) AST_PORT_STATE_FORWARDING);

    AST_GET_NUM_FWD_TRANSITIONS (pPerStPortInfo)++;
#ifdef PBB_PERFORMANCE_WANTED
    /*Measure Time Till Port State changed to Forwarding,
     * In PBB Performance Test Case value has to maesured
     * for PortNum =2 only*/

    if (u2PortNum == 2)
    {
        STP_PBB_PERF_MARK_END_TIME (gu4TotalTimeTaken);
        STP_PBB_PERF_PRINT_TIME (gu4TimeTaken);
    }
#endif
    if (pPortEntry->bDot1wEnabled == RST_TRUE)
    {
        pRstPortInfo->u1SendConfRoot = RST_TRUE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmEnableLearning                          */
/*                                                                           */
/* Description        : This routine calls the bridge module and enables     */
/*                      the Learning functionality in it.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Learning is to be enabled.               */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmEnableLearning (UINT2 u2PortNum, UINT2 u2InstanceId)
{

    AST_UNUSED (u2PortNum);
    AST_UNUSED (u2InstanceId);
    AST_DBG_ARG2 (AST_STSM_DBG, "STSM: Port %s: Inst %d: Learning Enabled\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmDisableLearning                         */
/*                                                                           */
/* Description        : This routine calls the bridge module and disables    */
/*                      the Learning functionality in it.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Learning is to be disabled.              */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmDisableLearning (UINT2 u2PortNum, UINT2 u2InstanceId)
{

    AST_UNUSED (u2PortNum);
    AST_UNUSED (u2InstanceId);
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Learning Disabled\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmEnableForwarding                        */
/*                                                                           */
/* Description        : This routine calls the bridge module and enables     */
/*                      the forwarding functionality in it.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Forwarding is to be enabled.             */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmEnableForwarding (UINT2 u2PortNum, UINT2 u2InstanceId)
{

    AST_UNUSED (u2PortNum);
    AST_UNUSED (u2InstanceId);
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Forwarding Enabled\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmDisableForwarding                       */
/*                                                                           */
/* Description        : This routine calls the bridge module and disables    */
/*                      the forwarding functionality in it.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Forwarding is to be disabled.            */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPstateTrSmDisableForwarding (UINT2 u2PortNum, UINT2 u2InstanceId)
{

    AST_UNUSED (u2PortNum);
    AST_UNUSED (u2InstanceId);
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %s: Inst %d: Forwarding Disabled\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmEventImpossible                         */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       Port Information.                   */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE                                          */
/*****************************************************************************/
INT4
RstPstateTrSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo,
                              UINT2 u2InstanceId)
{
    AST_UNUSED (u2InstanceId);
    AST_TRC (AST_CONTROL_PATH_TRC,
             "STSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_DBG (AST_STSM_DBG,
             "STSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");

    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_STSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPstateTrSmEventNotHandle                          */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an Event/State combination where      */
/*                      no specific operations are performed.                */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       Port Information.                   */
/*                      u2InstanceId - Spanning tree Instance Identifier for */
/*                                     for which this state machine is       */
/*                                     called.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE                                          */
/*****************************************************************************/
INT4
RstPstateTrSmEventNotHandle (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2InstanceId)
{
    AST_TRC (AST_CONTROL_PATH_TRC,
             "STSM: EVENT/STATE Combination occurred where no specific operation needs to be performed.\n");
    AST_DBG (AST_STSM_DBG,
             "STSM: EVENT/STATE Combination occurred where no specific operation needs to be performed.\n");

    AST_UNUSED (u2InstanceId);
    AST_UNUSED (pPerStPortInfo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitPortStateTrMachine                            */
/*                                                                           */
/* Description        : Initialises the Port State Transition State Machine. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstInitPortStateTrMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PSTATETRSM_MAX_EVENTS][20] = {
        "BEGIN", "LEARN", "FORWARD",
        "LEARN_FWD_DISABLED"
    };
    UINT1               aau1SemState[RST_PSTATETRSM_MAX_STATES][20] = {
        "DISCARDING", "LEARNING", "FORWARDING"
    };

    for (i4Index = 0; i4Index < RST_PSTATETRSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_STSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_STSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < RST_PSTATETRSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_STSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_STSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /* Event 0 - RST_PSTATETRSM_EV_BEGIN */
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_BEGIN]
        [RST_PSTATETRSM_STATE_DISCARDING].pAction = RstPstateTrSmMakeDiscarding;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_BEGIN]
        [RST_PSTATETRSM_STATE_LEARNING].pAction = RstPstateTrSmMakeDiscarding;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_BEGIN]
        [RST_PSTATETRSM_STATE_FORWARDING].pAction = RstPstateTrSmMakeDiscarding;

    /* Event 1 - RST_PSTATETRSM_EV_LEARN */
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN]
        [RST_PSTATETRSM_STATE_DISCARDING].pAction = RstPstateTrSmMakeLearning;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN]
        [RST_PSTATETRSM_STATE_LEARNING].pAction = NULL;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN]
        [RST_PSTATETRSM_STATE_FORWARDING].pAction = NULL;

    /* Event 2 - RST_PSTATETRSM_EV_FORWARD */
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_FORWARD]
        [RST_PSTATETRSM_STATE_DISCARDING].pAction =
        RstPstateTrSmEventImpossible;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_FORWARD]
        [RST_PSTATETRSM_STATE_LEARNING].pAction = RstPstateTrSmMakeForwarding;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_FORWARD]
        [RST_PSTATETRSM_STATE_FORWARDING].pAction = RstPstateTrSmEventNotHandle;

    /* Event 3 - RST_PSTATETRSM_EV_LEARN_FWD_DISABLED */
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [RST_PSTATETRSM_STATE_DISCARDING].pAction = RstPstateTrSmMakeDiscarding;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [RST_PSTATETRSM_STATE_LEARNING].pAction = RstPstateTrSmMakeDiscarding;
    RST_PORT_STATE_TR_MACHINE[RST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [RST_PSTATETRSM_STATE_FORWARDING].pAction = RstPstateTrSmMakeDiscarding;

    AST_DBG (AST_TXSM_DBG, "STSM: Loaded STSM SEM successfully\n");
    return;
}
