/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: rstpapi.c,v 1.99 2017/09/11 12:44:28 siva Exp $
 *
 * Description: This file contains all API routines provided by the
 *              RSTP Module.
 *
 *******************************************************************/

#include "asthdrs.h"
#include "cfa.h"
#include "mux.h"

/*****************************************************************************/
/* Function Name      : AstAllPortDisable                                    */
/*                                                                           */
/* Description        : This function is called from the Bridge Module and   */
/*                      sends an event to the ASTP Task to disable all Ports */
/*                      in the default context.                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : InMakeAllInterfacesDown                              */
/*****************************************************************************/
INT4
AstAllPortDisable (VOID)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_ALL_PORT_DISABLE_MSG;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCreateContext                                     */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the creation of a context.               */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstCreateContext (UINT4 u4ContextId)
{
    tAstMsgNode        *pNode = NULL;

    if (AST_IS_INITIALISED () == RST_FALSE)
    {
        return RST_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_CREATE_CONTEXT_MSG;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteContext                                     */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the deletion of a context.               */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstDeleteContext (UINT4 u4ContextId)
{
    tAstMsgNode        *pNode = NULL;

    if (AST_IS_INITIALISED () == RST_FALSE)
    {
        return RST_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_DELETE_CONTEXT_MSG;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUpdateContextName                                 */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      to indicate the updation of the context name.        */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstUpdateContextName (UINT4 u4ContextId)
{
    tAstMsgNode        *pNode = NULL;

    if (AST_IS_INITIALISED () == RST_FALSE)
    {
        return RST_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_UPDATE_CONTEXT_NAME;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstMapPort                                           */
/*                                                                           */
/* Description        : This function is called from the VCM Module          */
/*                      to indicate the creation of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/
INT4
AstMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstMsgNode        *pNode = NULL;

    if (u2PortNum > AST_MAX_PORTS_PER_SWITCH)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: Total Number of ports in this swtich is greater than 4094.! Exiting...\n");

#ifdef SYSLOG_WANTED
        AST_LOCK ();
        if (AstSelectContext (u4ContextId) == RST_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, AST_SYSLOG_ID,
                          "Total number of ports in switch %s exceeds 4094."
                          "STP can run only on ports upto 4094. Hence this port"
                          "will not be participating in the spanning tree",
                          AST_CONTEXT_STR ()));

            AstReleaseContext ();
        }
        AST_UNLOCK ();
#endif

        return RST_SUCCESS;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        AstL2IwfSetProtocolEnabledStatusOnPort (u4ContextId,
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");

        AstL2IwfSetProtocolEnabledStatusOnPort (u4ContextId,
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);

        return RST_FAILURE;

    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_MAP_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.u2LocalPortId = u2PortNum;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {

        AstL2IwfSetProtocolEnabledStatusOnPort (u4ContextId,
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);

        return RST_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUnmapPort                                         */
/*                                                                           */
/* Description        : This function is called from the VCM Module          */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be deleted*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortDeleteIndication                            */
/*****************************************************************************/
INT4
AstUnmapPort (UINT4 u4IfIndex)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_UNMAP_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCreatePort                                        */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the creation of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/
INT4
AstCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstMsgNode        *pNode = NULL;

    if (u2PortNum > AST_MAX_PORTS_PER_SWITCH)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: Total Number of ports in this switch is greater than 4094.! Exiting...\n");

#ifdef SYSLOG_WANTED
        AST_LOCK ();
        if (AstSelectContext (u4ContextId) == RST_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, AST_SYSLOG_ID,
                          "Total number of ports in switch %s exceeds 4094."
                          "STP can run only on ports upto 4094. Hence this port"
                          "will not be participating in the spanning tree",
                          AST_CONTEXT_STR ()));

            AstReleaseContext ();
        }
        AST_UNLOCK ();
#endif

        return RST_SUCCESS;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.u2LocalPortId = u2PortNum;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    L2_SYNC_TAKE_SEM ();

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeletePort                                        */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be deleted*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortDeleteIndication                            */
/*****************************************************************************/
INT4
AstDeletePort (UINT4 u4IfIndex)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleInFrame                                     */
/*                                                                           */
/* Description        : This API handles the incoming BPDUs and posts the    */
/*                      BPDU to AST Queue and sends an event to ASTP Task    */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the BPDU CRU Buffer             */
/*                      u4IfIndex - Interface Index of the Port              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE / RST_DATA                 */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingLayer2Pkt                         */
/*****************************************************************************/
INT4
AstHandleInFrame (tAstBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    tCfaIfInfo          IfInfo;

    /* Get the Context ID and Interface Type before calling AstWrHandleInFrame 
     * for providing backward compatibility */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (AstCfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return RST_FAILURE;
    }

    return (AstWrHandleInFrame (pCruBuf, u4IfIndex, u4ContextId,
                                IfInfo.u1IfType));
}

/*****************************************************************************/
/* Function Name      : AstWrHandleInFrame                                   */
/*                                                                           */
/* Description        : This Wrapper function handles the incoming BPDUs and */
/*                      posts the BPDU to AST Queue and sends an event to    */
/*                      ASTP Task                                            */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the BPDU CRU Buffer             */
/*                      u4IfIndex - Interface Index of the Port              */
/*                      u4ContextId - Context Id of the Interface            */
/*                      u1IfType -  Interface type of the Interface          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE / RST_DATA                 */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingLayer2Pkt                         */
/*****************************************************************************/
INT4
AstWrHandleInFrame (tAstBufChainHeader * pCruBuf, UINT4 u4IfIndex,
                    UINT4 u4ContextId, UINT1 u1IfType)
{
    tAstInterface       IfaceId;
    INT4                i4RetVal = RST_SUCCESS;
    tAstCfaIfInfo       CfaIfInfo;
    tAstQMsg           *pQMsg = NULL;

    if (AST_IS_INITIALISED () != RST_TRUE)
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: AST is not initialized and CRU BUF failed!\n");
        }
        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG |
                        AST_ALL_FAILURE_DBG, "STAP: AST is not initialized!\n");

        return RST_FAILURE;
    }
    AST_MEMSET (&IfaceId, AST_INIT_VAL, sizeof (IfaceId));
    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    if ((AST_NODE_STATUS () != RED_AST_ACTIVE) ||
        !(AstIsRstEnabledInContext (u4ContextId) ||
          AstIsMstEnabledInContext (u4ContextId) ||
          AstIsPvrstEnabledInContext (u4ContextId)))

    {
        /* If none of the STP module is enabled then the packet has to be given 
         * to VLAN module*/
        return RST_DATA;
    }

    IfaceId.u1_InterfaceType = u1IfType;

    if (AST_ALLOC_QMSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");

        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) != (INT4) CRU_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: CRU BUffer Release FAILED!!!\n");
        }
        return RST_SUCCESS;
    }

    AST_MEMSET (pQMsg, AST_MEMSET_VAL, sizeof (tAstQMsg));

    IfaceId.u4IfIndex = u4IfIndex;
    /* Handled for ethernet and PPP */
    /*This subreference Number is used while the ISS is compiled for Provider
     * Bridges. So dont change this variable while bridge is operating in
     * Provider Edge Bridge. For More info Go to AstPbHandleInFrame.*/
    IfaceId.u2_SubReferenceNum = 0;

    /* set the interface id in the buffer */
    AST_BUF_SET_INTERFACEID (pCruBuf, IfaceId);
    AST_QMSG_TYPE (pQMsg) = AST_BPDU_RCVD_QMSG;
    pQMsg->uQMsg.pBpduInQ = pCruBuf;

    if (AST_SEND_TO_QUEUE (AST_INPUT_QID,
                           (UINT1 *) &pQMsg, AST_DEF_MSG_LEN)
        == AST_OSIX_SUCCESS)

    {
        if (AST_SEND_EVENT (AST_TASK_ID, AST_BPDU_EVENT) != AST_OSIX_SUCCESS)
        {
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                            AST_ALL_FAILURE_DBG,
                            "STAP: Sending AST_MSG_EVENT to ASTP Task FAILED!!!\n");
            i4RetVal = RST_SUCCESS;
        }
    }
    else
    {
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG |
                        AST_ALL_FAILURE_DBG,
                        "STAP: Posting Message to Ast Queue FAILED!!!\n");
        i4RetVal = RST_FAILURE;
    }

    if (i4RetVal == RST_FAILURE)
    {
        if (AST_RELEASE_CRU_BUF (pCruBuf, RST_FALSE) == AST_CRU_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "MSG: Received Bpdu CRU Buffer -\n");
            AST_GLOBAL_TRC (u4ContextId,
                            AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                            "Release FAILED!\n");
        }

        if (AST_RELEASE_QMSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
        {
            AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                            "MGMT: Release of Local Message Memory Block FAILED!\n");
            i4RetVal = RST_FAILURE;
        }

    }
    return (i4RetVal);
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : AstAsyncNpUpdateStatus                                */
/*                                                                           */
/* Description        : This function is called from the VCM module to       */
/*                      to indicate the creation of a Port.                  */
/*                                                                           */
/* Input(s)           :  CallId - Unique number that identifies the source   */
/*                                NPAPI call                                 */
/*                       Params - Union consisting of a structure for each   */
/*                       asynchronous NPAPI                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : InEnablePort                                         */
/*****************************************************************************/
VOID
AstAsyncNpUpdateStatus (UINT4 CallId, unAsyncNpapi * ast)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT4               u4IfIndex;
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              ast->FsMiRstpNpSetPortState.u4ContextId);
        AST_GLOBAL_TRC (ast->FsMiRstpNpSetPortState.u4ContextId,
                        AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                        AST_MGMT_TRC,
                        "MGMT: Message Memory Block Allocation FAILED!\n");

        return;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_HW_PORTSTATE_UPDATED_MSG;

    if (CallId == AS_FS_MI_RSTP_NP_SET_PORT_STATE)
    {
        u4IfIndex = ast->FsMiRstpNpSetPortState.u4IfIndex;
        pNode->uMsg.AstNpapiCbParams.AstNpapiParams.FsMiRstpNpSetPortState =
            ast->FsMiRstpNpSetPortState;
    }
    else if (CallId == AS_FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE)
    {
        u4IfIndex = ast->FsMiMstpNpSetInstancePortState.u4IfIndex;
        pNode->uMsg.AstNpapiCbParams.AstNpapiParams.
            FsMiMstpNpSetInstancePortState =
            ast->FsMiMstpNpSetInstancePortState;
    }
    else
    {
        u4IfIndex = ast->FsMiPvrstNpSetVlanPortState.u4IfIndex;
        pNode->uMsg.AstNpapiCbParams.AstNpapiParams.
            FsMiPvrstNpSetVlanPortState = ast->FsMiPvrstNpSetVlanPortState;
    }

    pNode->uMsg.AstNpapiCbParams.CallId = CallId;

    if (AstVcmGetContextInfoFromIfIndex ((UINT2) u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {

        AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode);
        return;
    }

    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return;
    }
    return;
}
#endif /*NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : AstModuleEnable                                      */
/*                                                                           */
/* Description        : This function is called from the root task during    */
/*                      System start-up to Enable the RSTP/MSTP module so    */
/*                      that it becomes operational.                         */
/*                      It sends an event to the ASTP Task.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : Root Task (during start up)                          */
/*****************************************************************************/
VOID
AstModuleEnable (INT1 *pu1Dummy)
{
    UNUSED_PARAM (pu1Dummy);
    if (AstModuleDefaultCxtStart () == RST_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    lrInitComplete (OSIX_SUCCESS);

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsRstpSizingInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : AstModuleDefaultCxtStart                             */
/*                                                                           */
/* Description        : This function is to start the RSTP/MSTP module so    */
/*                      that it becomes operational.                         */
/*                      It sends an event to the ASTP Task.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstModuleDefaultCxtStart (VOID)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_TRC (u4ContextId,
                        AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                        AST_MGMT_TRC,
                        "MGMT: Message Memory Block Allocation FAILED!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4ContextId = u4ContextId;

#ifdef MSTP_WANTED
    if (AstIsMstStartedInContext (u4ContextId))
    {
        pNode->MsgType = AST_ENABLE_MST_MSG;
    }
    else
    {
#endif
#ifdef PVRST_WANTED
        if (AstIsPvrstStartedInContext (u4ContextId))
        {
            pNode->MsgType = AST_ENABLE_PVRST_MSG;
        }
        else
        {
#endif
            if (AstIsRstStartedInContext (u4ContextId))
            {
                pNode->MsgType = AST_ENABLE_RST_MSG;
            }
            else
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
                {
                    AST_GLOBAL_TRC (u4ContextId,
                                    AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                    "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                "MGMT: Neither RSTP nor MSTP is started. Please start either one of these\n");

                return RST_FAILURE;
            }
#ifdef PVRST_WANTED
        }
#endif
#ifdef MSTP_WANTED
    }
#endif
    if (AstSendEventToAstTask (pNode, u4ContextId) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUpdatePortStatus                                  */
/*                                                                           */
/* Description        : This API is called from the Bridge Module to         */
/*                      indicate the operational status of the physical port */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Global IfIndex of the port to which  */
/*                                  this indication belongs                  */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : nmhSetDot1dBasePortAdminStatus                       */
/*****************************************************************************/
INT4
AstUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    /* Get the Context ID before calling AstWrUpdatePortStatus ()
     * for providing backward compatibility */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    return (AstWrUpdatePortStatus (u4IfIndex, u1Status, u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstWrUpdatePortStatus                                */
/*                                                                           */
/* Description        : This Wrapper function is called from the Bridge      */
/*                      Module to indicate the operational status of the     */
/*                      physical port status.                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Global IfIndex of the port to which  */
/*                                  this indication belongs                  */
/*                      u1Status - The status of the Port (up or down)       */
/*                      u4ContextId - Context Information of the port        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : nmhSetDot1dBasePortAdminStatus                       */
/*****************************************************************************/

INT4
AstWrUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1Status, UINT4 u4ContextId)
{
    tAstMsgNode        *pNode = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT4               u4Ticks = 0;
    UINT2               u2LocalPortId = 0;
#ifdef NPAPI_WANTED
#ifdef MSTP_WANTED
    UINT2               u2MstInst = 0;
#endif
    UINT1               u1OwnerProtId = 0;
    UINT4               u4PhyIfIndex = u4IfIndex;
#endif
#ifdef ICCH_WANTED
    UINT1               u1IsMclagEnabled = 0;
#endif
    BOOL1               bPerfDataStatus;

    if (!(AstIsRstEnabledInContext (u4ContextId) ||
          AstIsMstEnabledInContext (u4ContextId) ||
          AstIsPvrstEnabledInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN or DISABLED!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN or DISABLED!\n");

        if (u1Status == CFA_IF_UP)
        {
            AstL2IwfSetInstPortState ((UINT2) RST_DEFAULT_INSTANCE,
                                      (UINT2) u4IfIndex,
                                      (UINT1) AST_PORT_STATE_FORWARDING);
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                /* If the port is SISP logical port, then invoke using the 
                 * appropriate IfIndex
                 * */
                L2IwfGetPortStateCtrlOwner (u4PhyIfIndex, &u1OwnerProtId,
                                            OSIX_TRUE);
#ifdef MSTP_WANTED
                if ((u4IfIndex >= AST_MIN_SISP_IF_INDEX) &&
                    (u4IfIndex <= AST_MAX_SISP_IF_INDEX))
                {
                    MstVcmSispGetPhysicalPortOfSispPort (u4IfIndex,
                                                         &u4PhyIfIndex);
                }
                if (u1OwnerProtId != ERPS_MODULE)
                {
                    MstpFsMiMstpNpSetInstancePortState (u4ContextId,
                                                        u4PhyIfIndex,
                                                        MST_CIST_CONTEXT,
                                                        AST_PORT_STATE_FORWARDING);
                }
                for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES;
                     u2MstInst++)
                {
                    AstL2IwfSetInstPortState ((UINT2) u2MstInst,
                                              (UINT2) u4IfIndex,
                                              (UINT1)
                                              AST_PORT_STATE_FORWARDING);
                }
#endif
                if (u1OwnerProtId != ERPS_MODULE)
                {
                    RstpFsMiRstpNpSetPortState (u4ContextId, u4PhyIfIndex,
                                                AST_PORT_STATE_FORWARDING);
                }
            }
#endif /*NPAPI_WANTED */
            AST_LOCK ();
            if (AstGetContextInfoFromIfIndex
                (u4IfIndex, &u4ContextId, &u2LocalPortId) != RST_FAILURE)
            {
                if (AstSelectContext (u4ContextId) == RST_SUCCESS)
                {
                    if ((AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE) != NULL) &&
                        (AST_GET_PERST_PORT_INFO
                         ((UINT2) u4IfIndex, RST_DEFAULT_INSTANCE) != NULL))
                    {
                        AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE,
                                                    (UINT2) u4IfIndex) =
                            AST_PORT_STATE_FORWARDING;
                    }
                    AstReleaseContext ();
                }
            }
            AST_UNLOCK ();
        }
        else
        {
            AstL2IwfSetInstPortState ((UINT2) RST_DEFAULT_INSTANCE,
                                      (UINT2) u4IfIndex,
                                      (UINT1) AST_PORT_STATE_DISCARDING);
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                /* If the port is SISP logical port, then invoke using the 
                 * appropriate IfIndex
                 * */
                L2IwfGetPortStateCtrlOwner (u4PhyIfIndex, &u1OwnerProtId,
                                            OSIX_TRUE);
#ifdef MSTP_WANTED
                if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
                {
                    MstVcmSispGetPhysicalPortOfSispPort (u4IfIndex,
                                                         &u4PhyIfIndex);
                }
                if (u1OwnerProtId != ERPS_MODULE)
                {
                    MstpFsMiMstpNpSetInstancePortState (u4ContextId,
                                                        u4PhyIfIndex,
                                                        MST_CIST_CONTEXT,
                                                        AST_PORT_STATE_DISCARDING);
                }
#endif
                if (u1OwnerProtId != ERPS_MODULE)
                {
                    RstpFsMiRstpNpSetPortState (u4ContextId, u4PhyIfIndex,
                                                AST_PORT_STATE_DISCARDING);
                }
            }
#endif /*NPAPI_WANTED */
            AST_LOCK ();
            if (AstGetContextInfoFromIfIndex
                (u4IfIndex, &u4ContextId, &u2LocalPortId) != RST_FAILURE)
            {
                if (AstSelectContext (u4ContextId) == RST_SUCCESS)
                {
                    if ((AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE) != NULL) &&
                        (AST_GET_PERST_PORT_INFO
                         ((UINT2) u4IfIndex, RST_DEFAULT_INSTANCE) != NULL))
                    {
                        AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE,
                                                    (UINT2) u4IfIndex) =
                            AST_PORT_STATE_DISCARDING;
                    }
                    AstReleaseContext ();
                }
            }
            AST_UNLOCK ();
            if (AstVlanIsVlanEnabledInContext (u4ContextId) == VLAN_TRUE)
            {
#ifdef ICCH_WANTED
                /* Dont delete FDB entries learnt on MC-LAG enabled
                 * interface */
                LaApiIsMclagInterface (u4IfIndex, &u1IsMclagEnabled);
                if ((u1IsMclagEnabled == OSIX_TRUE)
                    && (IcchGetPeerNodeState () == ICCH_PEER_UP))
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_DATA_PATH_TRC,
                             "Deletion of FDB entries skipped for MC-LAG enabled "
                             " interface when STP is disabled globally\r\n");
                    return RST_SUCCESS;
                }
#endif
                AstVlanDeleteFdbEntries (u4IfIndex, VLAN_NO_OPTIMIZE);
            }
            else
            {
                /* Inform the bridge to delete in its MAC table 
                 * all the entries learned for this port.
                 */
#ifdef BRIDGE_WANTED
                BrgDeleteFwdEntryForPort (u4IfIndex);
#endif
            }
        }

        return RST_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    AstL2IwfGetPerformanceDataStatus (u4ContextId, &bPerfDataStatus);
    if (bPerfDataStatus == L2IWF_TRUE)
    {
        AST_LOCK ();
        if (AstGetContextInfoFromIfIndex
            (u4IfIndex, &u4ContextId, &u2LocalPortId) != RST_FAILURE)
        {
            AstGetSysTime (&u4Ticks);
            /*u2LocalPortId = (UINT4) u4IfIndex; */
            if (AstSelectContext (u4ContextId) == RST_SUCCESS)
            {
                /* Update RcvdEvent details in AstBridgeEntry - Global Info */
                pBrgInfo = AST_GET_BRGENTRY ();
                if (pBrgInfo != NULL)
                {
                    pBrgInfo->u4RcvdEventTimeStamp = u4Ticks;
                    pBrgInfo->i4RcvdEvent = AST_CONFIGURATION;
                    pBrgInfo->i4RcvdEventSubType = u1Status;
                }
                else
                {
                    AST_GLOBAL_TRC (u4ContextId,
                                    AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                    "MGMT: BrgInfo is NULL!\n");
                    AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                                    "MGMT: BrgInfo is NULL!\n");
                }

                /* Update RcvdEvent detials in AstPortEntry - Per Port Info */
                pPortInfo = AST_GET_PORTENTRY ((UINT4) u2LocalPortId);
                if (pPortInfo != NULL)
                {
                    pPortInfo->u4RcvdEventTimeStamp = u4Ticks;
                    pPortInfo->i4RcvdEvent = AST_CONFIGURATION;
                    pPortInfo->i4RcvdEventSubType = u1Status;
                }
                else
                {
                    AST_GLOBAL_TRC (u4ContextId,
                                    AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                    "MGMT: PortInfo is NULL!\n");
                    AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                                    "MGMT: PortInfo is NULL!\n");
                }
                AstReleaseContext ();
            }
        }
        AST_UNLOCK ();
    }

    /* Even though instance id is not needed for updating port oper status,
     * the Port Operstatus and Port Stp Enabled Status is handled in
     * a single function, so a check has been in MstEnablePort which 
     * leads to fill up this variable*/
    pNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    switch (u1Status)
    {
        case AST_UP:
            pNode->MsgType = (tAstLocalMsgType) AST_ENABLE_PORT_MSG;
            pNode->uMsg.u1TrigType = (UINT1) AST_EXT_PORT_UP;
            break;

        case AST_DOWN:
            pNode->MsgType = (tAstLocalMsgType) AST_DISABLE_PORT_MSG;
            pNode->uMsg.u1TrigType = (UINT1) AST_EXT_PORT_DOWN;
            break;
    }

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : AstIsContextStpCompatible                            */
/*                                                                           */
/* Description        : Called by other modules to know if STP compatibility */
/*                      is enabled in the given virtual context.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes, Context is STP compatible.                  */
/*                      0 - No, Context is NOT STP compatible.               */
/*****************************************************************************/
INT4
AstIsContextStpCompatible (UINT4 u4ContextId)
{
    AST_LOCK ();

    if (AstSelectContext (u4ContextId) == RST_SUCCESS)
    {
        if (AST_FORCE_VERSION == AST_VERSION_0)
        {
            AstReleaseContext ();
            AST_UNLOCK ();
            return RST_SUCCESS;
        }
    }

    AstReleaseContext ();
    AST_UNLOCK ();
    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstIsRstStartedInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if RSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Rstp is started in the given context         */
/*                      0 - No, Rstp is NOT started in the given context     */
/*****************************************************************************/
INT4
AstIsRstStartedInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return 0;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == RST_START))
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : AstIsRstEnabledInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if RSTP is enabled   */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Rstp is enabled in the given context         */
/*                      0 - No, Rstp is NOT enabled in the given context     */
/*****************************************************************************/
INT4
AstIsRstEnabledInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return 0;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == RST_START) &&
        (gau1AstModuleStatus[u4ContextId] == RST_ENABLED))
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : RstGetEnabledStatus                                  */
/*                                                                           */
/* Description        : Gets the RSTP module status. Called by other modules */
/*                      to know if RSTP is enabled or disabled.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_ENABLED (or) RST_DISABLED                        */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : InSetLogicalPortOperStatus                           */
/*                      BridgeAggOperStatusIndication                        */
/*                      BridgeUpdatePortStatus                               */
/*****************************************************************************/
INT4
RstGetEnabledStatus (VOID)
{
    INT4                i4RetVal = RST_DISABLED;
#ifdef L2RED_WANTED
    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        i4RetVal = AST_ADMIN_STATUS;
    }
#else
    if (AstIsRstEnabledInContext (AST_DEFAULT_CONTEXT))
    {
        i4RetVal = RST_ENABLED;
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstGetStartedStatus                                  */
/*                                                                           */
/* Description        : Gets the RSTP system status. Called by other modules */
/*                      to know if RSTP is started or shutdown               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_START (or) RST_SHUTDOWN                          */
/*****************************************************************************/
INT4
RstGetStartedStatus (VOID)
{
    if (AstIsRstStartedInContext (AST_DEFAULT_CONTEXT))
    {
        return RST_START;
    }
    return RST_SHUTDOWN;
}

/*****************************************************************************/
/* Function Name      : AstLock                                              */
/*                                                                           */
/* Description        : This function is used to take the AST mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, AstTaskMain                                   */
/*****************************************************************************/
INT4
AstLock (VOID)
{
    if (AST_TAKE_SEM (gAstGlobalInfo.SemId) != AST_OSIX_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                        AST_OS_RESOURCE_TRC | AST_ALL_FAILURE_TRC,
                        "MSG: Failed to Take AST Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    /* AstSelectContext and AstReleaseContext does not have stubs in case of
     * SI mode. Also context selection will not be done implicitly even in SI
     * mode. 
     * Therefore in SI mode also before calling SI nmh we have to do select 
     * context. 
     * Registering select and release context with SNMP is not possible as, 
     * Vlan and Garp shares same mibs. Putting select and release contexts in 
     * SI mib wrapper routines is painful. So it is better to do 
     * select context for default context in Lock function. Since we have 
     * opted the above way in vlan we have to follow the same here too. */

    AstSelectContext (AST_DEFAULT_CONTEXT);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the AST mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, AstTaskMain                                   */
/*****************************************************************************/
INT4
AstUnLock (VOID)
{
    AstReleaseContext ();
    AST_GIVE_SEM (gAstGlobalInfo.SemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstReleaseMemory                                     */
/*                                                                           */
/* Description        : This function is used to release all allocated       */
/*                      memory in the RSTP/MSTP module.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*                                                                           */
/* Called By          : ISS                                                  */
/*****************************************************************************/
INT4
AstReleaseMemory (VOID)
{
    AST_LOCK ();
    AstDeleteAllContextsAndMemPools ();
    AST_UNLOCK ();
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : AstIsPathcostConfigured                            */
/*                                                                           */
/* Description          : This is to determine whether pathcost has been     */
/*                        configured in this port. This should be called     */
/*                        from outside RSTP/MSTP only.                       */
/*                                                                           */
/* Input(s)             : u4IfIndex    - Global IfIndex                      */
/*                        u2InstanceId - Instance ID                         */
/*                                                                           */
/* Output(s)            : None                                               */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : RST_TRUE  - If pathcost has been configured        */
/*                        RST_FALSE - If pathcost has not been configured    */
/*                                                                           */
/* Called By            : MSR                                                */
/*****************************************************************************/
UINT1
AstIsPathcostConfigured (UINT4 u4IfIndex, UINT2 u2InstanceId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == RST_FAILURE)
    {
        return RST_FALSE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return RST_FALSE;
    }

    if (AstIsPathCostSet (u2LocalPortId, u2InstanceId) == RST_FALSE)
    {
        AstReleaseContext ();
        return RST_FALSE;
    }

    AstReleaseContext ();

    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstPortSpeedChgIndication                            */
/*                                                                           */
/* Description        : This function is called from the LA Module           */
/*                      to indicate change in active ports of channel group. */
/*                      This function needs to be called whenever port path  */
/*                      cost for a port needs to be re-calculated based on   */
/*                      the latest speed of the port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port whose port path cost */
/*                                  needs to be re-calculted based on latest */
/*                                  speed of that port                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfAddActivePortToPortChannel                      */
/*                      L2IwfRemoveActivePortToPortChannel                   */
/*****************************************************************************/
INT4
AstPortSpeedChgIndication (UINT4 u4IfIndex)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    if (u4IfIndex > AST_MAX_PORTS_PER_SWITCH)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: Total Number of ports in this swtich is greater than 4094.! Exiting...\n");

        return RST_SUCCESS;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_PORT_SPEED_CHANGE_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.u2LocalPortId = u2LocalPortId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstResetCounters                                     */
/*                                                                           */
/* Description        : This function clears the Spanning Tree Counters      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/* Called By          : Web Module                                           */
/*                                                                           */
/* Calling Function   : IssProcessAstClearCountersPage                       */
/*****************************************************************************/
INT4
AstResetCounters (UINT4 u4ContextId)
{
    AST_LOCK ();

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        return RST_FAILURE;
    }
#ifdef MSTP_WANTED
    MstResetCounters ();
#endif
    RstResetCounters ();

    AstReleaseContext ();
    AST_UNLOCK ();
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetBrgPrioFromBrgId                               */
/*                                                                           */
/* Description        : This function gets the Priority from the Bridge Id   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/* Called By          : Web Module                                      */
/*             Cli Module                                                    */
/*                                                                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT2
AstGetBrgPrioFromBrgId (tSNMP_OCTET_STRING_TYPE BridgeId)
{
    UINT2               u2Priority;

    AST_MEMCPY ((UINT1 *) &u2Priority, BridgeId.pu1_OctetList,
                AST_BRG_PRIORITY_SIZE);
    u2Priority = OSIX_NTOHS (u2Priority);

    return u2Priority;
}

/*****************************************************************************/
/* Function Name      : AstStartModule                                       */
/*                                                                           */
/* Description        : This is the wrapper function to start the STP module */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstStartModule (VOID)
{
    AST_LOCK ();
    if (AstModuleInit () == RST_FAILURE)
    {
        AST_UNLOCK ();
        return RST_FAILURE;
    }
    AST_UNLOCK ();
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstShutdownModule                                    */
/*                                                                           */
/* Description        : This function shuts RSTP/MSTP module.                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstShutdownModule (VOID)
{
    tAstBufChainHeader *pBpdu = NULL;
    tAstQMsg           *pQMsg = NULL;

    AST_LOCK ();
    AstDeleteAllContextsAndMemPools ();
#ifdef L2RED_WANTED

    /*Clear all the sync up data before Restarting Modules */
    AstRedClearAllContextSyncUpData ();

    /* Release all the MemPools of sync-up database */
    AstRedRelPduMemPools ();

    AstRedRelStoreMemPools ();

    if (AST_RED_HW_AUDIT_TASKID != AST_INIT_VAL)
    {
        AST_DELETE_TASK (AST_RED_HW_AUDIT_TASKID);
        AST_RED_HW_AUDIT_TASKID = AST_INIT_VAL;
    }
    AST_NODE_STATUS () = RED_AST_SHUT_START_INPROGRESS;
#endif

    AST_IS_INITIALISED () = RST_FALSE;
    SYS_LOG_DEREGISTER (gAstGlobalInfo.i4SysLogId);
    gAstGlobalInfo.i4SysLogId = 0;

    if (AST_INPUT_QID != (tAstQId) AST_INIT_VAL)
    {
        /* Before DeInitializing the task, remove and release 
         * all messages in the Queue */
        while (AST_GET_BPDU_FROM_AST_QUEUE (AST_INPUT_QID, (UINT1 *) &pQMsg,
                                            AST_DEF_MSG_LEN, AST_OSIX_NO_WAIT)
               == AST_OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }
            /* Processing of BPDUs */
            if (AST_QMSG_TYPE (pQMsg) == AST_BPDU_RCVD_QMSG)
            {
                pBpdu = pQMsg->uQMsg.pBpduInQ;
                if (AST_RELEASE_CRU_BUF (pBpdu, RST_FALSE) == AST_CRU_FAILURE)
                {
                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_EVENT_HANDLING_DBG | AST_MEM_DBG |
                                    AST_ALL_FAILURE_DBG,
                                    "MSG: Received CRU Buffer Release FAILED -");
                    AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                    AST_EVENT_HANDLING_DBG | AST_MEM_DBG |
                                    AST_ALL_FAILURE_DBG,
                                    "in AST Task DeInit!\n");
                }
            }

            if (AST_RELEASE_QMSG_MEM_BLOCK (pQMsg) != AST_MEM_SUCCESS)
            {
                AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                "MSG: Release of Local Message Memory Block FAILED!\n");
                AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                                AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                                "MSG: Release of Local Message Memory Block FAILED!\n");
            }
        }
    }

    if (AST_CFG_QID != (tAstQId) AST_INIT_VAL)
    {
        /* Before DeInitializing the task, remove and release 
         * all messages in the Configuration Queue */
        while (AST_RECV_FROM_QUEUE (AST_CFG_QID, (UINT1 *) &pQMsg,
                                    AST_DEF_MSG_LEN, AST_OSIX_NO_WAIT)
               == AST_OSIX_SUCCESS)

        {
            if (pQMsg == NULL)
            {
                continue;
            }

            switch (AST_QMSG_TYPE (pQMsg))
            {
                case AST_SNMP_CONFIG_QMSG:

                    if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                        (pQMsg->uQMsg.pMsgNode) != AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of Local Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Local Message Memory Block FAILED!\n");
                    }

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");
                    }
                    break;

#ifdef L2RED_WANTED
                case AST_RM_QMSG:

                    if ((pQMsg->uQMsg.pRmMsg->u1Event == RM_MESSAGE) &&
                        (pQMsg->uQMsg.pRmMsg->pFrame != NULL))
                    {
                        RM_FREE (pQMsg->uQMsg.pRmMsg->pFrame);
                    }
                    else if (((pQMsg->uQMsg.pRmMsg->u1Event == RM_STANDBY_UP) ||
                              (pQMsg->uQMsg.pRmMsg->u1Event == RM_STANDBY_DOWN))
                             && (pQMsg->uQMsg.pRmMsg->pFrame != NULL))
                    {
                        AstRmReleaseMemoryForMsg ((UINT1 *) pQMsg->uQMsg.
                                                  pRmMsg->pFrame);
                    }

                    if (AST_RELEASE_LOCALMSG_MEM_BLOCK
                        (pQMsg->uQMsg.pRmMsg) != AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of Local Message Memory \
                                        Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Local Message Memory \
                                        Block FAILED!\n");
                    }

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFGQ Message Memory Block "
                                        "FAILED!\n");
                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of Q Message Memory Block "
                                        "FAILED!\n");
                    }
                    break;
#endif

#ifdef MBSM_WANTED
                case MBSM_MSG_CARD_INSERT:
                case MBSM_MSG_CARD_REMOVE:

                    MEM_FREE (pQMsg->uQMsg.MbsmCardUpdate.pMbsmProtoMsg);

                    if (AST_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_GLOBAL_TRC (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");

                        AST_GLOBAL_DBG (AST_INVALID_CONTEXT,
                                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                        AST_EVENT_HANDLING_DBG,
                                        "MSG: Release of CFG Q Message Memory Block FAILED!\n");
                    }
                    break;
#endif

            }
        }
    }

    AstRedRmInit ();
    AstRedDeRegisterWithRM ();

    /* Unregister with packet handler when STP is shut. Registration will
     * happen in AstInit whieh is called at module Start*/
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_RSTP_BRG);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_PROVIDER_STP);
#ifdef PVRST_WANTED
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_PVRST_CISCO_ISL1);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_PVRST_CISCO_ISL2);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_PVRST_DOT1Q);
#endif

    AST_UNLOCK ();
    return RST_SUCCESS;
}

#ifdef PBB_WANTED
/*****************************************************************************/
/* Function Name      : AstUpdateIntfType                                    */
/*                                                                           */
/* Description        : This function changes the Interface type to the      */
/*                      specified one. This should be only used to update    */
/*                      the port type from CNP S-Tagged to CNP C-Tagged in   */
/*                      I Components.                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                      u1IntfType  - New interface type.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
AstUpdateIntfType (UINT4 u4ContextId, UINT1 u1IntfType)
{
    tAstMsgNode        *pNode = NULL;

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_UPDT_PORT_TYPE_MSG;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.u1BrgPortType = u1IntfType;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}
#endif

/****************************************************************************
 *
 *    FUNCTION NAME    : AstApiStpConfigInfo 
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to do the following.
 *                       STP_OPER_POINT_POINT_STATUS_GET
 *                       STP_ADMIN_POINT_POINT_STATUS_GET
 *                       STP_ADMIN_POINT_POINT_STATUS_TEST
 *                       STP_ADMIN_POINT_POINT_STATUS_SET
 *
 *    INPUT            : pStpConfigInfo - Pointer to the structure which
 *                                  contains the stp info from the above
 *                                  modules.
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
AstApiStpConfigInfo (tStpConfigInfo * pStpConfigInfo)
{

#ifdef MSTP_WANTED
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
#endif
    INT1                i1RetVal = OSIX_SUCCESS;

    AST_LOCK ();

    if ((AST_IS_RST_STARTED () == AST_FALSE)
        && (AST_IS_MST_STARTED () == AST_FALSE))
    {
        AST_UNLOCK ();
        return i1RetVal;
    }

    switch (pStpConfigInfo->u1InfoType)
    {
        case STP_OPER_POINT_POINT_STATUS_GET:
            i1RetVal = AstUtilGetPortOperPointToPoint (pStpConfigInfo);
            break;

        case STP_ADMIN_POINT_POINT_STATUS_GET:
            i1RetVal = AstUtilGetPortAdminPointToPoint (pStpConfigInfo);
            break;

        case STP_ADMIN_POINT_POINT_STATUS_TEST:
            i1RetVal = AstUtilTestPortAdminPointToPoint (pStpConfigInfo);
            break;

        case STP_ADMIN_POINT_POINT_STATUS_SET:
            i1RetVal = AstUtilSetPortAdminPointToPoint (pStpConfigInfo);
#ifdef MSTP_WANTED
            if (AstGetContextInfoFromIfIndex (pStpConfigInfo->u4IfIndex,
                                              &u4ContextId,
                                              &u2LocalPort) != RST_FAILURE)
            {
                if (AstSelectContext (u4ContextId) != RST_FAILURE)
                {
                    if (AstSetCistPortMacEnabled (u2LocalPort,
                                                  pStpConfigInfo->
                                                  i4PortAdminPointToPoint) ==
                        SNMP_FAILURE)
                    {
                        AstReleaseContext ();
                        i1RetVal = (INT1) OSIX_FAILURE;
                    }
                    else
                    {
                        AstReleaseContext ();
                        i1RetVal = (INT1) OSIX_SUCCESS;
                    }
                }
            }
#endif
            break;
        default:
            break;
    }
    AST_UNLOCK ();
    return i1RetVal;
}

/*****************************************************************************/
/* Function Name      : AstCreatePortFromLa                                  */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      to indicate the creation of a Port from LA.          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/

INT4
AstCreatePortFromLa (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstMsgNode        *pNode = NULL;

    if (u2PortNum > AST_MAX_PORTS_PER_SWITCH)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: Total Number of ports in this switch is greater than 4094.! Exiting...\n");

#ifdef SYSLOG_WANTED
        AST_LOCK ();
        if (AstSelectContext (u4ContextId) == RST_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, AST_SYSLOG_ID,
                          "Total number of ports in switch %s exceeds 4094."
                          "STP can run only on ports upto 4094. Hence this port"
                          "will not be participating in the spanning tree",
                          AST_CONTEXT_STR ()));

            AstReleaseContext ();
        }
        AST_UNLOCK ();
#endif

        return RST_SUCCESS;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    /* When Automatic port create feature is disabled the port 
     * create indication from LA should not create port in STP instead 
     * should update the port row status as Active.To do this 
     * AST_CREATE_PORT_FROM_LA_MSG message is posted as event*/

    pNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_FROM_LA_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.u2LocalPortId = u2PortNum;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    L2_SYNC_TAKE_SEM ();
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeletePortFromLa                                  */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      to indicate the deletion of a Port from LA.          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be deleted*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortDeleteIndication                            */
/*****************************************************************************/
INT4
AstDeletePortFromLa (UINT4 u4IfIndex)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPortId) == VCM_FAILURE)
    {
        return RST_FAILURE;
    }

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return RST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_FROM_LA_MSG;
    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIndicateSTPStatus                                 */
/*                                                                           */
/* Description        : This function is called to indicate                  */
/*                      STP ENABLE/DISABLE status to VLAN module.            */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port              */
/*                      STPStatus - STP enabled/Disabled                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/* Calling Function   : MstComponentDisable/RstComponentDisable              */
/*                      MstAssertBegin/AstAssertBegin                        */
/*****************************************************************************/
VOID
AstIndicateSTPStatus (UINT4 u4PortNum, BOOL1 bSTPStatus)
{
    tAstSTPTunnelInfo   STPTunnelInfo;

    AST_MEMSET (&STPTunnelInfo, 0, sizeof (STPTunnelInfo));

    STPTunnelInfo.u4PortNum = AST_GET_IFINDEX (u4PortNum);
    STPTunnelInfo.u4ContextId = AST_CURR_CONTEXT_ID ();

    if (bSTPStatus == AST_TRUE)
    {
        STPTunnelInfo.bSTPStatus = AST_TRUE;
    }
    else
    {
        STPTunnelInfo.bSTPStatus = AST_FALSE;
    }

    VlanHandleSTPStatusChange (STPTunnelInfo);

    return;
}

/*****************************************************************************/
/* Function Name      : AstDisableStpOnPort                                  */
/*                                                                           */
/* Description        : This function is called to disable spanning tree     */
/*                      on specific interface                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstDisableStpOnPort (UINT4 u4IfIndex)
{

    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    AST_LOCK ();
    if ((!AST_IS_PVRST_STARTED ()) && (!AST_IS_MST_STARTED ()) &&
        (!AST_IS_RST_STARTED ()))
    {
        AST_UNLOCK ();
        return OSIX_SUCCESS;
    }
    if (AstGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return OSIX_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        AstReleaseContext ();
        AST_UNLOCK ();
        return OSIX_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = u2LocalPortId;
    if (AST_IS_PVRST_STARTED ())
    {
        pNode->MsgType = AST_PVRST_CONFIG_STATUS_PORT_MSG;
#ifdef PVRST_WANTED
        pNode->uMsg.bStatus = PVRST_FALSE;
#endif
    }
    else if ((AST_IS_MST_STARTED ()) || (AST_IS_RST_STARTED ()))
    {
        pNode->MsgType = AST_DISABLE_PORT_MSG;
        pNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_DOWN;
    }

    if (AstProcessSnmpRequest (pNode) != RST_SUCCESS)
    {
        AstReleaseContext ();
        AST_UNLOCK ();
        return OSIX_FAILURE;
    }

    AstReleaseContext ();
    AST_UNLOCK ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsStpEnabled                                      */
/*                                                                           */
/* Description        : This function is called to check whether STP is      */
/*                      enabled                                              */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstIsStpEnabled (INT4 i4IfIndex, UINT4 u4ContextId)
{
    UINT2               u2LocalPort = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (AstGetContextInfoFromIfIndex
        ((UINT4) i4IfIndex, &u4ContextId, &u2LocalPort) != RST_FAILURE)
    {
        if ((AstIsRstEnabledInContext (u4ContextId))
            || (AstIsMstEnabledInContext (u4ContextId)))

        {
            AstSelectContext (u4ContextId);
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2LocalPort, RST_DEFAULT_INSTANCE);
            if (pPerStPortInfo != NULL)
            {
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                if (pRstPortInfo != NULL)
                {
                    if (pRstPortInfo->bPortEnabled == RST_FALSE)
                    {
                        AstReleaseContext ();
                        return OSIX_FAILURE;
                    }
                    AstReleaseContext ();
                    return OSIX_SUCCESS;
                }
            }

        }
    }
    AstReleaseContext ();
    return OSIX_FAILURE;

}
