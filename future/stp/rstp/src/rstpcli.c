
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rstpcli.c,v 1.132 2018/01/22 09:40:44 siva Exp $
*
* Description:  Function Definition for CLI RSTP Commands
*********************************************************************/
/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : rstpcli.c                                        |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : RSTP                                             |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Function Definition for CLI RSTP Commands        |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 3rd Nov 2004    |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#include "asthdrs.h"
#include "stpcli.h"
#include "stpclipt.h"
#include "fsmsrslw.h"
#include "fsmprslw.h"
#include "fsmsbrlw.h"
#include "astpbcli.h"

#ifdef MSTP_WANTED
#include "astmlow.h"
#include  "fsmpmslw.h"
#endif
#ifdef PVRST_WANTED
#include "fspvrslw.h"
#include "fsmppvlw.h"
#endif

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliSetSystemShut                               */
/*                                                                          */
/*     DESCRIPTION      : This function sets the system control to shut     */
/*                                                                          */
/*     INPUT            :  CliHandle- Handle to the Cli Context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
RstCliSetSystemShut (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstSystemControl (&u4ErrCode, RST_SNMP_SHUTDOWN) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRstSystemControl (RST_SNMP_SHUTDOWN) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliSetSystemControl                            */
/*                                                                          */
/*     DESCRIPTION      : This function sets the system control (start)     */
/*                        of the RSTP Module after Shutting Down MSTP.      */
/*                        and Enables the  RSTP Module                      */
/*                                                                          */
/*     INPUT            :  CliHandle- Handle to the Cli Context             */
/*                         u4RstSysCtrl-  System Control Value              */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
RstCliSetSystemCtrl (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;

#ifdef MSTP_WANTED
    /* If the context has SISP enabled ports as member ports, than RSTP cannot
     * be the operating mode for the context
     * */

    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_BRG_MODE_ERR);
        return CLI_FAILURE;
    }

    if (AST_IS_MST_STARTED ())
    {
        CliPrintf (CliHandle, "\rSpanning Tree protocol enabled is MST. "
                   "Now MST is being shutdown and RST is being enabled\r\n");
        /*Shutting Down MSTP  */
        if (nmhTestv2FsMstSystemControl (&u4ErrCode, MST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsMstSystemControl (MST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
#endif
#ifdef PVRST_WANTED
    if (AST_IS_PVRST_STARTED ())
    {
        CliPrintf (CliHandle, "\rSpanning Tree protocol enabled is PVRST. "
                   "Now PVRST is being shutdown and RST is being enabled\r\n");
        /*Shutting Down MSTP  */
        if (nmhTestv2FsPvrstSystemControl (&u4ErrCode, PVRST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsPvrstSystemControl (PVRST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
#endif
    if (nmhTestv2FsRstSystemControl (&u4ErrCode, RST_SNMP_START) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRstSystemControl (RST_SNMP_START) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /*RSTP module is started and enabled */
    RstCliSetModStatus (CliHandle, RST_ENABLED);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetModuleStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the module status               */
/*                        Enabled/Disabled) of the RSTPModule.               */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4RstModStatus- RSTP Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetModStatus (tCliHandle CliHandle, UINT4 u4RstModStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstModuleStatus (&u4ErrCode, (UINT4) u4RstModStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsRstModuleStatus ((UINT4) u4RstModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetFwdTime                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Forward Time for            */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CliContext                */
/*                        u4FwdTime- Forward Delay                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliSetFwdTime (tCliHandle CliHandle, UINT4 u4FwdTime)
{
    UINT4               u4ErrCode = 0;

    u4FwdTime = AST_SYS_TO_MGMT (u4FwdTime);

    if (nmhTestv2Dot1dStpBridgeForwardDelay (&u4ErrCode, (INT4) u4FwdTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1dStpBridgeForwardDelay ((INT4) u4FwdTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetHelloTime                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--handle to the CLI Context              */
/*                        u4HelloTime-HelloTime                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliSetHelloTime (tCliHandle CliHandle, UINT4 u4HelloTime)
{
    UINT4               u4ErrCode = 0;

    u4HelloTime = AST_SYS_TO_MGMT (u4HelloTime);

    if (nmhTestv2Dot1dStpBridgeHelloTime (&u4ErrCode, u4HelloTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1dStpBridgeHelloTime (u4HelloTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetMaxAge                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MaxAge for                  */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--Handle to the CLI Context              */
/*                          u4MaxAge--MaxAge                                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliSetMaxAge (tCliHandle CliHandle, UINT4 u4MaxAge)
{
    UINT4               u4ErrCode = 0;

    u4MaxAge = AST_SYS_TO_MGMT (u4MaxAge);

    if (nmhTestv2Dot1dStpBridgeMaxAge (&u4ErrCode, u4MaxAge) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot1dStpBridgeMaxAge (u4MaxAge) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetTxHoldCount                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets and resets the Tx Hold Count    */
/*                        for the RSTP                                       */
/*                        .                                                  */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the CLI Context               */
/*                        u4TxHoldCount- TransmitHoldCount                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetBridgeTxHoldCount (tCliHandle CliHandle, UINT4 u4TxHoldCount)
{

    UINT4               u4ErrCode = 0;

    /* Transmit Hold Count */
    if (nmhTestv2Dot1dStpTxHoldCount (&u4ErrCode,
                                      u4TxHoldCount) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetDot1dStpTxHoldCount (u4TxHoldCount) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetBrgPriority                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Bridge Priority             */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to the Context                    */
/*                        u4BrgPriority-BridgePriority                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetBrgPriority (tCliHandle CliHandle, UINT4 u4BrgPriority)
{
    UINT4               u4ErrCode = 0;

    if (AST_IS_RST_STARTED ())
    {

        if (nmhTestv2Dot1dStpPriority (&u4ErrCode, u4BrgPriority) ==
            SNMP_FAILURE)
        {
            if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "\r%% Bridge Priority for PVRST cannot be set here\r\n");
                return CLI_FAILURE;
            }
            if (AST_IS_I_COMPONENT () == RST_TRUE)
            {
                CliPrintf (CliHandle,
                           "\r%% Bridge Priority must be 65535 for I-Components"
                           "\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Bridge Priority must be in increments of 4096"
                           " and can be upto 61440\r\n");
                CliPrintf (CliHandle, "\r%% Allowed values are:\r\n"
                           "0     4096  8192  12288 16384 20480 24576 28672\r\n"
                           "32768 36864 40960 45056 49152 53248 57344 61440\r\n");
            }
            return CLI_FAILURE;

        }
        if (nmhSetDot1dStpPriority (u4BrgPriority) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetBrgVersion                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the RSTP version to STP         */
/*                          Compatible or RSTP Compatible.                   */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4Version - STP Version                            */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetBrgVersion (tCliHandle CliHandle, UINT4 u4Version)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1dStpVersion (&u4ErrCode, u4Version) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot set the RST bridge to be MST compatible\r\n");
        return (CLI_FAILURE);

    }
    if (nmhSetDot1dStpVersion (u4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  RstCliSetPathCostCalculation                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathCostCalc-Pathcost calculation method         */
/*                        (Dynamic/Manual)                                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathCostCalc)
{
    UINT4               u4ErrCode = 0;

    if (u4PathCostCalc == AST_PATHCOST_CALC_DYNAMIC)
    {
        u4PathCostCalc = AST_SNMP_TRUE;
    }
    else if (u4PathCostCalc == AST_PATHCOST_CALC_MANUAL)
    {
        u4PathCostCalc = AST_SNMP_FALSE;
    }

    if (nmhTestv2FsRstDynamicPathcostCalculation (&u4ErrCode,
                                                  u4PathCostCalc) ==
        SNMP_FAILURE)
    {

        return (CLI_FAILURE);

    }
    if (nmhSetFsRstDynamicPathcostCalculation (u4PathCostCalc) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  RstCliSetLaggPathCostCalculation                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                        for LAGG ports.                                    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathCostCalc-Enabled/Disabled                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetLaggPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathCostCalc)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsRstCalcPortPathCostOnSpeedChg (&u4ErrCode,
                                                  u4PathCostCalc) ==
        SNMP_FAILURE)
    {

        return (CLI_FAILURE);

    }
    nmhSetFsRstCalcPortPathCostOnSpeedChg (u4PathCostCalc);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstSetPortProp                                     */
/*                                                                           */
/*     DESCRIPTION      : This function set the properties for an Interface  */
/*                        for both RSTp and MSTP                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        u4PortNum - PortNumber                             */
/*                        u1Flag    - Flag to Check which property to be set */
/*                        u4Val     - RST Property                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstSetPortProp (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4CmdType,
                UINT4 u4Val)
{
    UINT4               u4ErrCode = 0;
    UINT1               u1OperStatus;

    /* Validating Parameters and setting PortPathCost */
    if (u4CmdType == STP_PORT_COST)
    {
        if (nmhTestv2Dot1dStpPortAdminPathCost (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }

        /*Setting PathCost for a Port */
        if (nmhSetDot1dStpPortAdminPathCost (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* When the path cost is cleared, reset the pathcost configured variable
       for the port.And also calculate the pathcost dynamically */
    else if (u4CmdType == STP_NO_PORT_COST)
    {
        if (AstPathcostConfiguredFlag ((UINT2) u4PortNum, RST_DEFAULT_INSTANCE)
            == RST_TRUE)
        {

            if (nmhTestv2Dot1dStpPortAdminPathCost (&u4ErrCode, u4PortNum, 0)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /*Setting PathCost for a Port */

            if (nmhSetDot1dStpPortAdminPathCost (u4PortNum, 0) == SNMP_FAILURE)
            {

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (AstValidatePortEntry ((INT4) u4PortNum) != RST_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "Such a Port DOES NOT exist!\n");
            CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
            return CLI_FAILURE;
        }
    }

    else if ((u4CmdType == CLI_AST_ENABLED) || (u4CmdType == CLI_AST_DISABLED))
    {

        if (nmhTestv2FsRstPortRowStatus (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rUnable to create/delete RSTP port.\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsRstPortRowStatus (u4PortNum, u4Val) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    else if (u4CmdType == AST_PORT_STATE_DISABLED)
    {
        if (nmhTestv2Dot1dStpPortEnable (&u4ErrCode, u4PortNum, u4Val) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1dStpPortEnable (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4CmdType == STP_LINK_TYPE)
    {
        if (nmhTestv2Dot1dStpPortAdminPointToPoint
            (&u4ErrCode, u4PortNum, u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1dStpPortAdminPointToPoint (u4PortNum, u4Val) ==
            SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else if (u4CmdType == STP_PORTFAST)
    {

        if (nmhTestv2Dot1dStpPortAdminEdgePort (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1dStpPortAdminEdgePort (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        AstL2IwfGetPortOperStatus
            (STP_MODULE, AST_GET_IFINDEX (u4PortNum), &u1OperStatus);

        if (u4Val != AST_SNMP_FALSE)
        {

            CliPrintf (CliHandle,
                       "\rWarning: portfast should only be enabled on ports "
                       "connected to a single host.\r\nConnecting hubs, concentrators, "
                       "switches, bridges, etc... to this interface\r\nwhen portfast "
                       "is enabled, can cause temporary bridging loops.\r\n"
                       "Use with CAUTION\r\n\r\n");

            if (u1OperStatus != CFA_IF_DOWN)
            {

                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will  "
                           "have effect \r\nonly when the interface is shutdown\r\n");
            }

        }
        else if (u4Val == AST_SNMP_FALSE)
        {
            if (u1OperStatus != CFA_IF_DOWN)
            {
                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will  "
                           "have effect  \r\nonly when the interface is shutdown\r\n");
            }
        }
    }
    else if (u4CmdType == STP_PORT_PRIORITY)
    {

        if (nmhTestv2Dot1dStpPortPriority (&u4ErrCode, u4PortNum,
                                           u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /*Setting PortPriority */
        if (nmhSetDot1dStpPortPriority (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetProtocolMigration                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port Protocol Migration i.e.*/
/*                        mcheck variable on all ports.                      */
/*                                                                           */
/*     INPUT            : tCliHandle   -  Handle to the CLiContext           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetProtocolMigration (tCliHandle CliHandle)
{

    INT4                i4CurPort = 0;
    INT4                i4NextPort = 0;
    UINT4               u4TempContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT4                i4OutCome = 0;
    UINT1               u1OperStatus = 0;

    i4OutCome = AstGetFirstPortInCurrContext (&i4NextPort);
    if (i4OutCome == RST_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No PortEntry from the table \r\n");
        return CLI_FAILURE;
    }
    do
    {
        if (AstGetContextInfoFromIfIndex (i4NextPort, &u4TempContextId,
                                          &u2LocalPortId) != RST_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%Retrieving Context ID fails\r\n");
            return CLI_FAILURE;
        }

        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);
        if (CFA_IF_DOWN == u1OperStatus)
        {
            i4CurPort = i4NextPort;
            continue;
        }

        if (RstCliSetPortProtocolMigration (CliHandle,
                                            (INT4) u2LocalPortId) !=
            CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%Setting Protocol Migration on Port fails\r\n");
            return CLI_FAILURE;
        }

        i4CurPort = i4NextPort;
    }
    while (AstGetNextPortInCurrContext (i4CurPort, &i4NextPort) != RST_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortProtocolMigration                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the  Protocol Migration on      */
/*                        a single port   i.e. mcheck variable on one port   */
/*                                                                           */
/*     INPUT            : tCliHandle   -  Handle to the CLiContext           */
/*                        u4IfIndex    -  Port Number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetPortProtocolMigration (tCliHandle CliHandle, UINT4 u4IfIndex)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1dStpPortProtocolMigration
        (&u4ErrCode, u4IfIndex, AST_SNMP_TRUE) != SNMP_FAILURE)
    {

        if (nmhSetDot1dStpPortProtocolMigration
            (u4IfIndex, AST_SNMP_TRUE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortRestrictedRole                        */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Restricted Role on port*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local PortNum of the port.           */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetPortRestrictedRole (tCliHandle CliHandle, UINT2 u2LocalPort,
                             INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortRestrictedRole (&u4ErrCode, (INT4) u2LocalPort,
                                          i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsRstPortRestrictedRole ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortRootGuard                             */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Root Guard on port    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local PortNum of the port.           */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetPortRootGuard (tCliHandle CliHandle, UINT2 u2LocalPort, INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortRootGuard (&u4ErrCode, (INT4) u2LocalPort,
                                     i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsRstPortRootGuard ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortRestrictedTcn                         */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Restricted Tcn on port*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local Port Number of the port.       */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetPortRestrictedTcn (tCliHandle CliHandle, UINT2 u2LocalPort,
                            INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortRestrictedTCN (&u4ErrCode, (INT4) u2LocalPort,
                                         i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsRstPortRestrictedTCN ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortLoopGuard                             */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Loop Guard on port    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetPortLoopGuard (tCliHandle CliHandle, UINT2 u2LocalPort, INT4 i4Value)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstPortLoopGuard (&u4ErrCode, u2LocalPort, i4Value)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRstPortLoopGuard (u2LocalPort, i4Value) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstSetDebug                                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Debugging support           */
/*                        for the RSTP                                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  - Handle to the CLI Context            */
/*                        u4RstDebug  - Debug Value                          */
/*                        u1Action    - Set/No Command                       */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstSetDebug (tCliHandle CliHandle, UINT4 u4RstDbg, UINT1 u1Action)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4DbgVal = 0;

    UNUSED_PARAM (CliHandle);

    if (u1Action == RST_NO_CMD)
    {
        /* For Debug Disable, Get the existing Debug Value 
         * and negate that value
         */
        nmhGetFsRstDebugOption ((INT4 *) &u4DbgVal);
        u4DbgVal = u4DbgVal & (~u4RstDbg);
    }
    else if (u1Action == RST_SET_CMD)
    {
        u4DbgVal = u4RstDbg;
    }

    if (nmhTestv2FsRstDebugOption (&u4ErrCode, (INT4) u4DbgVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsRstDebugOption ((INT4) u4DbgVal);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstSetGlobalDebug                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global Debugging support    */
/*                        for the RSTP/MSTP                                  */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  - Handle to the CLI Context            */
/*                        u1Action    - Set/No Command                       */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstSetGlobalDebug (tCliHandle CliHandle, UINT1 u1Action)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsMIRstGlobalDebug (&u4ErrCode, u1Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsMIRstGlobalDebug (u1Action);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstSetPortAutoEdge                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the auto edge status for a port.*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        u4Status - enable/disable                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/
INT4
RstSetPortAutoEdge (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4Status)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsRstPortAutoEdge (&u4ErrCode, u4PortNum, u4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRstPortAutoEdge (u4PortNum, u4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstConfigPseudoRootId                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pseudo RootId for a port.   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        MacAddress - Mac Address of Pseudo RootId          */
/*                        u2Priority - Priority of Pseudo RootId             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstConfigPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum,
                       tMacAddr MacAddress, UINT2 u2Priority)
{
    UINT1               au1RootIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    UINT4               u4ErrCode;

    PseudoRootId.pu1_OctetList = &au1RootIdBuf[0];
    PseudoRootId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    AST_MEMSET (au1RootIdBuf, AST_INIT_VAL, CLI_RSTP_MAX_BRIDGEID_BUFFER);

    PseudoRootId.pu1_OctetList[0] = (UINT1) ((u2Priority & 0xff00) >> 8);
    PseudoRootId.pu1_OctetList[1] = (UINT1) (u2Priority & 0x00ff);

    AST_MEMCPY (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                MacAddress, AST_MAC_ADDR_SIZE);

    if (nmhTestv2FsRstPortPseudoRootId (&u4ErrCode, (INT4) u4PortNum,
                                        &PseudoRootId) != SNMP_SUCCESS)
    {
        return MST_FAILURE;
    }

    if (nmhSetFsRstPortPseudoRootId ((INT4) u4PortNum, &PseudoRootId)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstSetPortL2gp                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable l2gp status for  */
/*                        a port.                                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableL2gp - L2Gp Status of port                  */
/*                                      TRUE - to configure port as L2gp port*/
/*                                      FALSE- to remove L2gp status of port */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstSetPortL2gp (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableL2gp)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortIsL2Gp (&u4ErrCode, (INT4) u4PortNum,
                                  i4EnableL2gp) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRstPortIsL2Gp ((INT4) u4PortNum, i4EnableL2gp) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortDot1WEnable                           */
/*                                                                           */
/*     DESCRIPTION      : This function is to enable the bridge to           */
/*                        send agreement pdu in accordance with 802.1W       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetPortDot1WEnable (tCliHandle CliHandle, UINT2 u2LocalPort, INT4 i4Value)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstPortStpModeDot1wEnabled (&u4ErrCode, u2LocalPort, i4Value)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRstPortStpModeDot1wEnabled (u2LocalPort, i4Value) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstSetBpduRx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu receive     */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduRx - bpdu receive status of give port   */
/*                                      TRUE - bpdu can be received on port  */
/*                                      FALSE- received bpdus will be ignored*/
/*                                             on this port.                 */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduRx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortEnableBPDURx (&u4ErrCode, (INT4) u4PortNum,
                                        i4EnableBpduRx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRstPortEnableBPDURx ((INT4) u4PortNum, i4EnableBpduRx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstSetBpduTx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu transmit    */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduTx - bpdu transmit status of give port  */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduTx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsRstPortEnableBPDUTx (&u4ErrCode, (INT4) u4PortNum,
                                        i4EnableBpduTx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRstPortEnableBPDUTx ((INT4) u4PortNum, i4EnableBpduTx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstSetBpduFilter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu transmit    */
/*                        or receive status for a port.                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduFilter - bpdu transmit status of give   */
/*                                            port                           */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                  INT4 i4EnableBpduFilter)
{

    if (RstSetBpduTx (CliHandle, u4PortNum, i4EnableBpduFilter) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (RstSetBpduRx (CliHandle, u4PortNum, i4EnableBpduFilter) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowSpanningTreeMethod                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Spanning Tree method    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliShowSpanningTreeMethod (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\r\nSpanning Tree port pathcost method is Long\r\n");
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowSpanningTreeDetail                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Information for         */
/*                        "Show Spanning Tree Detail" Command                */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliShowSpanningTreeDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Active)
{
    INT4                i4CurrentPort;
    INT4                i4NextPort;
    INT4                i4PortState;
    INT4                i4PortRole;
    INT4                i4OutCome;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1Flag = 0;
    UINT1               u1OperStatus;

    /* Display the Bridge Info */
    RstCliDisplayBridgeDetails (CliHandle, u4ContextId);

    /* Get the Current Port from the Table */
    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);
    while (i4OutCome != RST_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != AST_PORT_OPER_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                nmhGetFsMIRstPortRole (i4NextPort, &i4PortRole);
                nmhGetFsDot1dStpPortState (i4NextPort, &i4PortState);
                if (((i4PortState == AST_PORT_STATE_LEARNING) ||
                     (i4PortState == AST_PORT_STATE_FORWARDING))
                    && (i4PortRole != AST_PORT_ROLE_DISABLED))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                /* Display Port Details */
                RstCliDisplayPortDetails (CliHandle, u4ContextId, i4NextPort);

                u4PagingStatus = CliPrintf (CliHandle, "\r");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }
            }
        }
        u1Flag = 0;
        /*Get the next Port from the Table */
        i4CurrentPort = i4NextPort;
        i4OutCome =
            AstGetNextPortInContext (u4ContextId, i4CurrentPort, &i4NextPort);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliDisplayBridgeDetails                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Bridge Details         */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliDisplayBridgeDetails (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    INT4                i4HelloTime = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4RootFwdDelay = 0;
    INT4                i4RootHelloTime = 0;
    INT4                i4RootMaxAge = 0;
    INT4                i4ModStatus = 0;
    INT4                i4Priority = 0;
    INT4                i4HoldTime = 0;
    UINT4               u4TopChange = 0;
    UINT4               u4TopTime = 0;
    INT4                i4AltRoleOpt = 0;

    INT4                i4Version = 0;
    tAstMacAddr         MacAddress;
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4DynamicPathCost;
    INT4                i4FlushInterval = 0;
    INT4                i4Threshold = 0;
    UINT4               u4FlushCount = 0;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /*Spanning Tree Bridge Version */

    nmhGetFsMIRstModuleStatus (i4ContextId, &i4ModStatus);
    if (i4ModStatus != RST_DISABLED)
    {
        nmhGetFsDot1dStpVersion (i4ContextId, &i4Version);
    }
    nmhGetFsMIRstFwdDelayAltPortRoleTrOptimization (i4ContextId, &i4AltRoleOpt);

    nmhGetFsMIRstBpduGuard (i4ContextId, &i4BpduGuard);
    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard disable\r\n");
    }

    /* Bridge Priority */

    nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);

    /*Bridge Address */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    RstGetBridgeAddr (&MacAddress);
    AstReleaseContext ();

    /*Hello Time */

    nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */

    nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Dynamic Path Cost */

    nmhGetFsMIRstDynamicPathcostCalculation (i4ContextId, &i4DynamicPathCost);

    /* Flush Interval */
    nmhGetFsMIRstFlushInterval (i4ContextId, &i4FlushInterval);
    nmhGetFsMIRstTotalFlushCount (i4ContextId, &u4FlushCount);
    nmhGetFsMIRstFlushIndicationThreshold (i4ContextId, &i4Threshold);

    /* To Check  if the  Bridge is a ROOT Bridge */

    /* Get the RootBridge ID- Priority and MacAddress */

    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &RetBridgeId);

    /* Topology Changes */

    nmhGetFsDot1dStpTopChanges (i4ContextId, &u4TopChange);

    /* Time Since Topology Changes */

    nmhGetFsDot1dStpTimeSinceTopologyChange (i4ContextId, &u4TopTime);
    u4TopTime = u4TopTime / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    /*Hold Time */

    nmhGetFsDot1dStpTxHoldCount (i4ContextId, &i4HoldTime);
    if (i4AltRoleOpt != AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "Forwarddelay optimization alternate-role Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "Forwarddelay optimization alternate-role Enabled\r\n");
    }

    /*Root Bridge Hello time */

    nmhGetFsDot1dStpHelloTime (i4ContextId, &i4RootHelloTime);
    i4RootHelloTime = AST_MGMT_TO_SYS (i4RootHelloTime);

    /*root  Bridge MaxAge */

    nmhGetFsDot1dStpMaxAge (i4ContextId, &i4RootMaxAge);
    i4RootMaxAge = AST_MGMT_TO_SYS (i4RootMaxAge);

    /* Root Bridge Forward Delay */

    nmhGetFsDot1dStpForwardDelay (i4ContextId, &i4RootFwdDelay);
    i4RootFwdDelay = AST_MGMT_TO_SYS (i4RootFwdDelay);

    RstCliPrintStpModuleStatus (CliHandle, u4ContextId, i4ModStatus, i4Version);

    CliPrintf (CliHandle, "Bridge Identifier has priority %d, ", i4Priority);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "Address %s\r\n", au1BrgAddr);
    CliPrintf (CliHandle, "Configured Hello time %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, " Max Age %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs \r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    if (i4DynamicPathCost == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Dynamic Path Cost Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Dynamic Path Cost Disabled\r\n");
    }
    CliPrintf (CliHandle, "Flush Interval %d centi-sec,", i4FlushInterval);
    CliPrintf (CliHandle, " Flush Invocations %d \r\n", u4FlushCount);
    CliPrintf (CliHandle, "Flush Indication threshold %d \r\n", i4Threshold);

    /* Root Bridge Info */

    if (AST_MEMCMP ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                    MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "We are the root of the spanning tree\r\n");
    }

    CliPrintf (CliHandle, "Number of Topology Changes %d \r\n", u4TopChange);

    CliPrintf (CliHandle, "Time since topology Change %d seconds ago\r\n",
               u4TopTime);
    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4RetVal = AstCfaCliGetIfName (gu4RecentTopoChPort, (INT1 *) au1IntfName);
    CliPrintf (CliHandle, "Port which caused last topology change : %s\r\n",
               au1IntfName);

    CliPrintf (CliHandle, "Transmit Hold-Count %d \r\n", i4HoldTime);
    CliPrintf (CliHandle,
               "Root Times:Max Age %d sec %d cs\tForward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4RootMaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootMaxAge),
               AST_PROT_TO_BPDU_SEC (i4RootFwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootFwdDelay));
    CliPrintf (CliHandle, "Hello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4RootHelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootHelloTime));
    UNUSED_PARAM (i4RetVal);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliDisplayPortDetails                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Port Details           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        i4NextPort- Port Index                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliDisplayPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4NextPort)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;

    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;

    UINT4               u4TxBpduCount = 0;
    UINT4               u4RxBpduCount = 0;
    INT4                i4RestricRole = AST_SNMP_FALSE;
    INT4                i4RestricTCN = AST_SNMP_FALSE;
    INT4                i4RootGuard = AST_SNMP_FALSE;
    UINT2               u2Val = 0;
    UINT2               u2RootPrio = 0;
    UINT2               u2BrgPrio = 0;
    UINT2               u2PortPrio = 0;

    INT4                i4Intf = 0;
    INT4                i4PortState = 0;
    INT4                i4PortStateStatus = 0;
    INT4                i4PortRole = 0;
    INT4                i4PathCost = 0;
    INT4                i4Priority = 0;
    INT4                i4DesigCost = 0;
    INT4                i4PortFast = 0;
    INT4                i4FwdTransition = 0;
    INT4                i4HelloTime = AST_INIT_VAL;
    INT4                i4BPDUTxState;
    INT4                i4BPDURxState;
    INT4                i4IsL2Gp;
    UINT4               u4HelloTmr = 0;
    UINT4               u4FwdWhileTmr = 0;
    UINT4               u4TCWhileTmr = 0;
    INT4                i4LinkType = 0;
    UINT2               u2Prio;

    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];

    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_RSTP_MAX_PORTID_BUFFER];
    UINT1               au1PseudoRootIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortEntry = NULL;
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    INT4                i4LoopGuard = 0;
    INT4                i4AutoEdge = 0;
    INT4                i4OperEdge = 0;
    INT4                i4ErrorRecovery = AST_DEFAULT_ERROR_RECOVERY;
    INT4                i4Dot1WEnable = 0;
    UINT4               u4TxProposCount = 0;
    UINT4               u4RxProposCount = 0;
    UINT4               u4TxAgreementCount = 0;
    UINT4               u4RxAgreementCount = 0;
    UINT4               u4TxProposTimeStamp = 0;
    UINT4               u4RxProposTimeStamp = 0;
    UINT4               u4TxAgreementTimeStamp = 0;
    UINT4               u4RxAgreementTimeStamp = 0;
    UINT4               u4TopChRxd = 0;
    UINT4               u4DetecTopCh = 0;
    UINT4               u4DetecTopChTimeStamp = 0;
    UINT4               u4ImpStateOccTimeStamp = 0;
    UINT4               u4TopChRxdTimeStamp = 0;
    UINT4               u4ImpossibleStateOcc = 0;
    UINT4               u4RcvInfoCount = 0;
    UINT4               u4RcvInfoTimeStamp = 0;
    UINT1               au1Date[AST_ARRAY_TIMESTAMP] = { 0 };

    MEMSET (au1PseudoRootIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RootBrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetRootBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    RetPortId.pu1_OctetList = &au1PortIdBuf[0];
    RetPortId.i4_Length = CLI_RSTP_MAX_PORTID_BUFFER;

    /* Displaying Port info */

    /* Get the interface name for this Port Number */

    i4Intf = AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

    if (i4Intf == CLI_FAILURE)
    {

        CliPrintf (CliHandle,
                   "\r\n%% Port Name for Index %d is not  found \r\n",
                   i4NextPort);
        return;

    }

    /* Port Roles */

    nmhGetFsMIRstPortRole (i4NextPort, &i4PortRole);
    /* PORT STATES */

    nmhGetFsDot1dStpPortState (i4NextPort, &i4PortState);

    AstGetHwPortStateStatus (i4NextPort, RST_DEFAULT_INSTANCE,
                             &i4PortStateStatus);

    /*PortPathCost */

    nmhGetFsDot1dStpPortPathCost32 (i4NextPort, &i4PathCost);

    /* Port priority */
    nmhGetFsDot1dStpPortPriority (i4NextPort, &i4Priority);

    /* PortID- priority.number */
    nmhGetFsDot1dStpPortPriority (i4NextPort, &i4Priority);

    /* Immediate Superior's Hello Time
     * This impliese hello time should be displayed only for Root,
     * Alternate and Backup ports, so that the user can know about
     * the received info while timer
     * */
    if ((i4PortRole == AST_PORT_ROLE_ROOT) ||
        (i4PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (i4PortRole == AST_PORT_ROLE_BACKUP))
    {
        AstCliGetPortInfoHelloTime (i4NextPort, &i4HelloTime);
    }

    /*RestrictedRole */
    nmhGetFsMIRstPortRestrictedRole (i4NextPort, &i4RestricRole);

    /*RestrictedTCN */
    nmhGetFsMIRstPortRestrictedTCN (i4NextPort, &i4RestricTCN);

    /*Designated Root */
    AST_MEMSET (RetRootBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsDot1dStpPortDesignatedRoot (i4NextPort, &RetRootBridgeId);

    /*Designated Bridge  */
    AST_MEMSET (RetBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsDot1dStpPortDesignatedBridge (i4NextPort, &RetBridgeId);

    /*Designated Port */
    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    nmhGetFsDot1dStpPortDesignatedPort (i4NextPort, &RetPortId);

    /*Designated Port PathCost */

    nmhGetFsDot1dStpPortDesignatedCost (i4NextPort, &i4DesigCost);

    /* Forward Transitions */

    nmhGetFsDot1dStpPortForwardTransitions (i4NextPort,
                                            (UINT4 *) &i4FwdTransition);

    /* Auto-Edge Feature */

    nmhGetFsMIRstPortAutoEdge (i4NextPort, &i4AutoEdge);

    /* PortFast Feature */

    nmhGetFsDot1dStpPortAdminEdgePort (i4NextPort, &i4PortFast);

    /* OperEdge Feature */

    nmhGetFsDot1dStpPortOperEdgePort (i4NextPort, &i4OperEdge);

    /* Link Type */

    nmhGetFsDot1dStpPortOperPointToPoint (i4NextPort, &i4LinkType);

    /*BPDUS TRANSMITTED */

    u4TxBpduCount = 0;
    u4TxBpduCount = RstCliPortTxBpduCount (i4NextPort);

    /*Bpdu transmit status */
    nmhGetFsMIRstPortEnableBPDURx (i4NextPort, &i4BPDURxState);

    /*Bpdu receive status */
    nmhGetFsMIRstPortEnableBPDUTx (i4NextPort, &i4BPDUTxState);
    /*Proposal packets sent */
    nmhGetFsMIRstPortProposalPktsSent (i4NextPort, &u4TxProposCount);

    /*Proposal packets sent time stamp */
    nmhGetFsMIRstPortProposalPktSentTimeStamp (i4NextPort,
                                               &u4TxProposTimeStamp);

    /*Agreement packtes sent */
    nmhGetFsMIRstPortAgreementPktSent (i4NextPort, &u4TxAgreementCount);

    /*Agreement packtes sent time stamp */
    nmhGetFsMIRstPortAgreementPktSentTimeStamp (i4NextPort,
                                                &u4TxAgreementTimeStamp);

    /*Proposal packets received */
    nmhGetFsMIRstPortProposalPktsRcvd (i4NextPort, &u4RxProposCount);

    /*Proposal packets received time stamp */
    nmhGetFsMIRstPortProposalPktRcvdTimeStamp (i4NextPort,
                                               &u4RxProposTimeStamp);

    /*Agreement packets received */
    nmhGetFsMIRstPortAgreementPktRcvd (i4NextPort, &u4RxAgreementCount);

    /*Agreement packets received time stamp */
    nmhGetFsMIRstPortAgreementPktRcvdTimeStamp (i4NextPort,
                                                &u4RxAgreementTimeStamp);

    /*Topology changes received */
    nmhGetFsMIRstPortTCReceivedCount (i4NextPort, &u4TopChRxd);

    /*Topology changes received time stamp */
    nmhGetFsMIRstPortTCReceivedTimeStamp (i4NextPort, &u4TopChRxdTimeStamp);

    /*Topology change detected */
    nmhGetFsMIRstPortTCDetectedCount (i4NextPort, &u4DetecTopCh);

    /*Topology change detected time stamp */
    nmhGetFsMIRstPortTCDetectedTimeStamp (i4NextPort, &u4DetecTopChTimeStamp);

    /*Impossible State count */
    nmhGetFsMIRstPortImpStateOccurCount (i4NextPort, &u4ImpossibleStateOcc);

    /*Impossible State Timestamp */
    nmhGetFsMIRstPortImpStateOccurTimeStamp (i4NextPort,
                                             &u4ImpStateOccTimeStamp);

    /*RcvInfoWhile Count */
    nmhGetFsMIRstPortRcvInfoWhileExpCount (i4NextPort, &u4RcvInfoCount);

    /* Rcvinfowhile Timestamp */
    nmhGetFsMIRstPortRcvInfoWhileExpTimeStamp (i4NextPort, &u4RcvInfoTimeStamp);

    /*Layer two gateway port */
    nmhGetFsMIRstPortIsL2Gp (i4NextPort, &i4IsL2Gp);

    /*PseudoRootId */
    nmhGetFsMIRstPortPseudoRootId (i4NextPort, &PseudoRootId);

    /*Root Guard */
    nmhGetFsMIRstPortRootGuard (i4NextPort, &i4RootGuard);

    /* Loop Guard  */
    nmhGetFsMIRstPortLoopGuard (i4NextPort, &i4LoopGuard);

    PrintMacAddress (PseudoRootId.pu1_OctetList
                     + AST_BRG_PRIORITY_SIZE, au1PseudoRootAddr);

    u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

    /*Error Disable Recovery Timer */
    nmhGetFsMIRstPortErrorRecovery (i4NextPort, &i4ErrorRecovery);

    /*DOT1W Enabled Status */

    nmhGetFsMIRstPortStpModeDot1wEnabled (i4NextPort, &i4Dot1WEnable);

    /* BPDUs RECIEVED */

    u4RxBpduCount = 0;
    u4RxBpduCount = RstCliPortRxBpduCount (i4NextPort);

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }

    if ((pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4NextPort)) == NULL)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, "\rPort %ld deleted \r\n", i4NextPort);
        return;
    }

    if (AstGetRemainingTime ((VOID *) pAstPortEntry, AST_TMR_TYPE_HELLOWHEN,
                             &u4HelloTmr) == RST_FAILURE)
    {
        u4HelloTmr = 0;
    }

    pAstPerStRstPortEntry =
        AST_GET_PERST_RST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                     MST_CIST_CONTEXT);
    if (pAstPerStRstPortEntry != NULL)
    {

        if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                 AST_TMR_TYPE_FDWHILE,
                                 &u4FwdWhileTmr) == RST_FAILURE)
        {
            u4FwdWhileTmr = 0;
        }
        if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                 AST_TMR_TYPE_TCWHILE,
                                 &u4TCWhileTmr) == RST_FAILURE)
        {
            u4TCWhileTmr = 0;
        }

    }
    AstReleaseContext ();
    CliPrintf (CliHandle, "\r\nPort %d [%s] is ", i4NextPort, au1IntfName);
    StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
    CliPrintf (CliHandle, ", ");

    StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
    CliPrintf (CliHandle, "\n");

    CliPrintf (CliHandle, "Port PathCost %d, ", i4PathCost);
    CliPrintf (CliHandle, "Port Priority %d, ", i4Priority);
    CliPrintf (CliHandle, "Port Identifier  %3d.%d \r\n", i4Priority,
               i4NextPort);

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);
    AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
    PrintMacAddress (RetRootBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1RootBrgAddr);
    CliPrintf (CliHandle, "Designated Root has priority %d,", u2RootPrio);
    CliPrintf (CliHandle, " address %s \r\n", au1RootBrgAddr);

    u2BrgPrio = 0;
    u2BrgPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1BrgAddr);
    CliPrintf (CliHandle, "Designated Bridge has priority %d,", u2BrgPrio);
    CliPrintf (CliHandle, " address %s \r\n", au1BrgAddr);

    u2PortPrio = 0;
    u2Val = 0;
    u2PortPrio = (UINT2) (RetPortId.pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
    u2Val = AstCliGetPortIdFromOctetList (RetPortId);
    u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);
    CliPrintf (CliHandle, "Designated Port Id is %d.%d,", u2PortPrio, u2Val);

    CliPrintf (CliHandle, " Designated PathCost %d \r\n", i4DesigCost);

    if (i4HelloTime != AST_INIT_VAL)
    {
        /* HelloTime info have been filled up
         * */
        CliPrintf (CliHandle, "Received Hello Time %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4HelloTime),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    }

    CliPrintf (CliHandle, "No of Transitions to forwarding State :%d\r\n",
               i4FwdTransition);

    if (i4AutoEdge == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Auto-Edge is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Auto-Edge is disabled\r\n");
    }

    if (i4PortFast == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "PortFast is enabled, ");
    }
    else
    {
        CliPrintf (CliHandle, "PortFast is disabled, ");
    }

    if (i4OperEdge == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Oper-Edge is enabled\r\n");
    }

    else
    {
        CliPrintf (CliHandle, "Oper-Edge is disabled\r\n");
    }
    if (i4LinkType == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "LinkType is point to Point\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Link Type is Shared\r\n");
    }

    CliPrintf (CliHandle, "BPDUs : sent %d ,", u4TxBpduCount);
    CliPrintf (CliHandle, " received %d\r\n", u4RxBpduCount);
    CliPrintf (CliHandle,
               "Timers: Hello - %d, Forward Delay - %d, Topology Change - %d, \r\n",
               u4HelloTmr, u4FwdWhileTmr, u4TCWhileTmr);
    CliPrintf (CliHandle, "Error Disable Recovery Interval %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4ErrorRecovery),
               AST_PROT_TO_BPDU_CENTI_SEC (i4ErrorRecovery));

    if (i4RestricRole == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Restricted Role is enabled.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Restricted Role is disabled.\r\n");
    }

    if (i4RestricTCN == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Restricted TCN is enabled.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Restricted TCN is disabled.\r\n");
    }
    if (i4IsL2Gp == RST_TRUE)
    {
        CliPrintf (CliHandle,
                   "%s is operating as Layer Two Gateway Port.\r\n",
                   au1IntfName);

        CliPrintf (CliHandle,
                   "PseudoRootId Priority %d, MacAddress %s\r\n", u2Prio,
                   au1PseudoRootAddr);

    }

    CliPrintf (CliHandle, "%s", "bpdu-transmit ");

    if (i4BPDUTxState == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "disabled");
    }

    CliPrintf (CliHandle, "%s", "bpdu-receive ");

    if (i4BPDURxState == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "disabled");
    }
    if (i4RootGuard == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Root Guard is enabled.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Root Guard is disabled.\r\n");
    }
    if (i4LoopGuard == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "Loop Guard is enabled.");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "Loop Guard is disabled.");
    }
    if (i4Dot1WEnable == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "Dot1W mode enabled.");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "Dot1W mode disabled.");
    }

    CliPrintf (CliHandle, "TC detected count                : %d \r\n",
               u4DetecTopCh);
    if (u4DetecTopChTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC detected Timestamp            : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4DetecTopChTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC detected Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "TC received count                : %d \r\n",
               u4TopChRxd);
    if (u4TopChRxdTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC received Timestamp            : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4TopChRxdTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC received Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "RcvInfoWhile expiry count        : %d  \r\n",
               u4RcvInfoCount);
    if (u4RcvInfoTimeStamp == 0)
    {
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4RcvInfoTimeStamp, au1Date);
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Impossible state occurence count : %d \r\n",
               u4ImpossibleStateOcc);
    if (u4ImpStateOccTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Impossible state Timestamp       : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4ImpStateOccTimeStamp, au1Date);
        CliPrintf (CliHandle, "Impossible State Timestamp       : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Proposal BPDUs\r\n");
    CliPrintf (CliHandle, "---------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4TxProposCount);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4RxProposCount);
    if (u4TxProposTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4TxProposTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4RxProposTimeStamp == 0)
    {

        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4RxProposTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Agreement BPDUs\r\n");
    CliPrintf (CliHandle, "----------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4TxAgreementCount);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4RxAgreementCount);
    if (u4TxAgreementTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4TxAgreementTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4RxAgreementTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        RstUtilTicksToDate (u4RxAgreementTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPortTxBpduCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of transmitted RSTP  */
/*                        config  and TCN BPDU s count                       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4NextPort - PortIndex                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4TxBpduCount                                      */
/*****************************************************************************/

UINT4
RstCliPortTxBpduCount (INT4 i4NextPort)
{
    UINT4               u4Count;
    UINT4               u4TxBpduCount;

    /* Rst BPDU */
    nmhGetFsMIRstPortTxRstBpduCount (i4NextPort, &u4Count);
    u4TxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIRstPortTxConfigBpduCount (i4NextPort, &u4Count);
    u4TxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIRstPortTxTcnBpduCount (i4NextPort, &u4Count);
    u4TxBpduCount += u4Count;

    return u4TxBpduCount;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliPortRxBpduCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of received  RSTP    */
/*                        config  and TCN BPDU s count                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4NextPort - PortIndex                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4RxBpduCount                                      */
/*****************************************************************************/

UINT4
RstCliPortRxBpduCount (INT4 i4NextPort)
{
    UINT4               u4Count;
    UINT4               u4RxBpduCount;

    /* Rst BPDU */
    nmhGetFsMIRstPortRxRstBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIRstPortRxConfigBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIRstPortRxTcnBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount += u4Count;

    return u4RxBpduCount;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StpCliPrintRstPortRole                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the   RSTP Port Roles      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4Val- Integer Value for the Role                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
StpCliPrintRstPortRole (tCliHandle CliHandle, INT4 i4Val)
{
    switch (i4Val)
    {
        case AST_PORT_ROLE_DISABLED:
            CliPrintf (CliHandle, "Disabled  ");
            break;
        case AST_PORT_ROLE_ALTERNATE:
            CliPrintf (CliHandle, "Alternate ");
            break;
        case AST_PORT_ROLE_BACKUP:
            CliPrintf (CliHandle, "Back Up   ");
            break;
        case AST_PORT_ROLE_ROOT:
            CliPrintf (CliHandle, "Root      ");
            break;
        case AST_PORT_ROLE_DESIGNATED:
            CliPrintf (CliHandle, "Designated");
            break;
        default:
            CliPrintf (CliHandle, "Unknown   ");
            break;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StpCliDisplayPortRole                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the   RSTP/MSTP Port Roles */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        i4Val- Integer Value for the Role                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
StpCliDisplayPortRole (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val)
{
    if (AstIsRstStartedInContext (u4ContextId))
    {
        StpCliPrintRstPortRole (CliHandle, i4Val);
    }
#ifdef MSTP_WANTED
    else if (AstIsMstStartedInContext (u4ContextId))
    {

        switch (i4Val)
        {
            case MST_PORT_ROLE_DISABLED:
                CliPrintf (CliHandle, "Disabled  ");
                break;
            case MST_PORT_ROLE_ALTERNATE:
                CliPrintf (CliHandle, "Alternate ");
                break;
            case MST_PORT_ROLE_BACKUP:
                CliPrintf (CliHandle, "Back Up   ");
                break;
            case MST_PORT_ROLE_ROOT:
                CliPrintf (CliHandle, "Root      ");
                break;
            case MST_PORT_ROLE_DESIGNATED:
                CliPrintf (CliHandle, "Designated");
                break;
            case MST_PORT_ROLE_MASTER:
                CliPrintf (CliHandle, "Master    ");
                break;
            default:
                CliPrintf (CliHandle, "Unknown   ");
                break;
        }

    }
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StpCliDisplayPortState                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the   RSTP                 */
/*                         port States                                       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4Val- Integer Value for the State                 */
/*                        i4NpPortStatus- Hardware programming status        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
StpCliDisplayPortState (tCliHandle CliHandle, INT4 i4Val, INT4 i4NpPortStatus)
{
#ifdef NPAPI_WANTED
    if ((i4NpPortStatus == AST_BLOCK_FAILURE) ||
        (i4NpPortStatus == AST_LEARN_FAILURE) ||
        (i4NpPortStatus == AST_FORWARD_FAILURE))
    {
        switch (i4NpPortStatus)
        {
            case AST_BLOCK_FAILURE:
                CliPrintf (CliHandle, "-DiscardFailed-");
                break;
            case AST_LEARN_FAILURE:
                CliPrintf (CliHandle, "-LearnFailed-");
                break;
            case AST_FORWARD_FAILURE:
                CliPrintf (CliHandle, "-ForwardFailed-");
                break;
        }
    }
    else
#endif
    {
        UNUSED_PARAM (i4NpPortStatus);
        switch (i4Val)
        {
            case AST_PORT_STATE_DISCARDING:
                CliPrintf (CliHandle, "Discarding");
                break;
            case AST_PORT_STATE_LEARNING:
                CliPrintf (CliHandle, "Learning  ");
                break;
            case AST_PORT_STATE_FORWARDING:
                CliPrintf (CliHandle, "Forwarding");
                break;
            default:
                CliPrintf (CliHandle, "Unknown   ");
                break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowSpanningTreeRoot                         */
/*                                                                           */
/*     DESCRIPTION      : This function  dispalys the root bridge parameters */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Val-Info to be displayed                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliShowSpanningTreeRoot (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Val)
{
    tSNMP_OCTET_STRING_TYPE DesigRoot;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4RootCost = 0;
    INT4                i4RootPort = 0;
    UINT2               u2RootPrio = 0;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4ContextId = (INT4) u4ContextId;

    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    DesigRoot.pu1_OctetList = &au1BrgIdBuf[0];
    DesigRoot.i4_Length = AST_BRG_ADDR_DIS_LEN;

    switch (u4Val)
    {
        case STP_ADDRESS:

            AST_MEMSET (au1BrgAddr, 0, AST_BRG_ADDR_DIS_LEN);
            nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &DesigRoot);
            break;

        case STP_ROOT_COST:

            nmhGetFsDot1dStpRootCost (i4ContextId, &i4RootCost);
            break;

        case STP_SHOW_DETAIL:
            /*Root Bridge Priority */
            RstCliShowRootBridgeInfo (CliHandle, u4ContextId);
            break;

        case STP_FORWARD_TIME:

            nmhGetFsDot1dStpForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            break;

        case STP_ID:

            nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &DesigRoot);
            break;

        case STP_MAX_AGE:

            nmhGetFsDot1dStpMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
            break;

        case STP_ROOT_PORT:
            MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            nmhGetFsDot1dStpRootPort (i4ContextId, &i4RootPort);
            AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);
            break;

        case STP_PRIORITY:

            nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &DesigRoot);
            break;
        default:
            /* Display Root Bridge Info in Table Format */
            RstCliShowRootBridgeTable (CliHandle, u4ContextId);
            break;
    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been gruped under the below
     *  switch-case */

    switch (u4Val)
    {
        case STP_ADDRESS:

            /* First 2 bytes contain the bridge priority and the
             * next 6 bytes contain the Bridge MAC*/
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (DesigRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                             au1BrgAddr);
            CliPrintf (CliHandle, "\r\nRoot Bridge Address is %s\r\n",
                       au1BrgAddr);
            break;

        case STP_ROOT_COST:

            CliPrintf (CliHandle, "\r\nRoot Cost is %d \r\n", i4RootCost);
            break;

        case STP_FORWARD_TIME:

            CliPrintf (CliHandle, "\r\nForward delay is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            break;

        case STP_ID:

            AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
            CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length,
                           au1BrgId, AST_BRG_ID_DIS_LEN);

            CliPrintf (CliHandle, "\r\nRoot Bridge Id is %s \r\n", au1BrgId);
            break;

        case STP_MAX_AGE:

            CliPrintf (CliHandle, "\r\nRoot MaxAge is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            break;

        case STP_ROOT_PORT:
            if (AST_INIT_VAL != i4RootPort)
            {
                CliPrintf (CliHandle, "\r\nRoot Port is %d[%s]\r\n", i4RootPort,
                           au1IntfName);
            }
            break;

        case STP_PRIORITY:

            u2RootPrio = AstGetBrgPrioFromBrgId (DesigRoot);
            CliPrintf (CliHandle, "\r\nRoot Priority is %5d \r\n", u2RootPrio);
            break;

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowSpanningTreeBridge                       */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  Bridge information    */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Val-Root Bridge Info to be displayed             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliShowSpanningTreeBridge (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Val)
{

    tAstMacAddr         MacAddress;
    INT4                i4HelloTime = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4Priority = 0;
    INT4                i4ContextId = (INT4) u4ContextId;

    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];

    switch (u4Val)
    {
        case STP_ADDRESS:

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            RstGetBridgeAddr (&MacAddress);
            AstReleaseContext ();
            PrintMacAddress (MacAddress, au1BrgAddr);
            break;

        case STP_SHOW_DETAIL:
            /* Bridge Priority */

            nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);

            /*Bridge Address */

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            RstGetBridgeAddr (&MacAddress);
            AstReleaseContext ();
            PrintMacAddress (MacAddress, au1BrgAddr);

            /*Hello Time */

            nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

            /*Max Age */

            nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            /*Forward Delay */

            nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            break;

        case STP_FORWARD_TIME:

            nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            break;
        case STP_HELLO_TIME:

            nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);
            break;

        case STP_ID:

            /* Bridge ID */

            nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);

            /* Valid values of priorities in hex are 0x1000(4096), 0x2000 (8192)
             * ,..0xF000(61440).
             * This needs to be displayed as 10:00, 20:00..F0:00 respectively.
             * The second byte is always 00
             * The first byte can be computed as shown- 
             * 4096/256 = 16 = 0x10 , 8192/256 = 0x20.., 61440/256 =0xFF
             */

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            RstGetBridgeAddr (&MacAddress);
            AstReleaseContext ();
            PrintMacAddress (MacAddress, au1BrgAddr);

            break;
        case STP_MAX_AGE:

            nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
            break;

        case STP_PRIORITY:

            nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);
            break;

        default:

            /* Bridge ID */

            nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);

            /* Valid values of priorities in hex are 0x1000(4096), 0x2000 (8192)
             * ,..0xF000(61440).
             * This needs to be displayed as 10:00, 20:00..F0:00 respectively.
             * The second byte is always 00
             * The first byte can be computed as shown- 
             * 4096/256 = 16 = 0x10 , 8192/256 = 0x20.., 61440/256 =0xFF
             */

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            RstGetBridgeAddr (&MacAddress);
            AstReleaseContext ();
            PrintMacAddress (MacAddress, au1BrgAddr);

            /*HelloTime */
            nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

            /*MaxAge */
            nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            /* Forward Delay */
            nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            break;
    }

    /* Since Lock needs to be released before printing the above inforamtion, all the
     * Cliprintfs are grouped in the below switch-case */

    switch (u4Val)
    {

        case STP_ADDRESS:

            CliPrintf (CliHandle, "\r\nBridge Address is %s\r\n", au1BrgAddr);
            break;

        case STP_SHOW_DETAIL:

            CliPrintf (CliHandle, "\r\nBridge Id       Priority %d,\r\n ",
                       i4Priority);

            CliPrintf (CliHandle, "               Address %s\r\n", au1BrgAddr);

            CliPrintf (CliHandle, "                Hello Time %dsec %dcs, ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            CliPrintf (CliHandle, "Max Age %dsec %dcs, ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

            CliPrintf (CliHandle,
                       "Forward Delay %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

            break;

        case STP_FORWARD_TIME:

            CliPrintf (CliHandle,
                       "\r\nBridge Forward delay is %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            break;

        case STP_HELLO_TIME:

            CliPrintf (CliHandle, "\r\nBridge Hello Time is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
            break;

        case STP_ID:

            CliPrintf (CliHandle, "\r\nBridge ID is %x:00:",
                       i4Priority / (0x100));

            CliPrintf (CliHandle, "%s\r\n", au1BrgAddr);
            break;

        case STP_MAX_AGE:

            CliPrintf (CliHandle, "\r\nBridge Max Age is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            break;

        case STP_PROTOCOL:

            CliPrintf (CliHandle, "\r\nBridge Protocol Running is RSTP\r\n");

            break;

        case STP_PRIORITY:

            CliPrintf (CliHandle, "\r\nBridge Priority is %5d\r\n", i4Priority);
            break;

        default:

            CliPrintf (CliHandle,
                       "\r\n%-25s%-13s%-13s%-15s%-8s\r\n", "Bridge ID",
                       "  HelloTime", "MaxAge", "FwdDly", "Protocol");

            CliPrintf (CliHandle,
                       "%-25s%-13s%-13s%-15s%-8s\r\n",
                       "---------", "  ---------", "------", "------",
                       "--------");

            /*Bridge ID */

            CliPrintf (CliHandle, "%2x:00:", i4Priority / (0x100));

            CliPrintf (CliHandle, "%s ", au1BrgAddr);

            /*Hello Time */
            CliPrintf (CliHandle, "%-2.2ds %-2.2dcs   ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            /*MaxAge */
            CliPrintf (CliHandle, "%-2.2ds %-2.2dcs     ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

            /*FwdDelay */
            CliPrintf (CliHandle, "%-2d s %-2d cs ",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

            CliPrintf (CliHandle, "    %s\r\n", "rstp");
            break;

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  RSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
RstpShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Module)
{
    UINT4               u4SysMode;

    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    u4SysMode = AstVcmGetSystemMode (AST_PROTOCOL_ID);

    if (RstpShowRunningConfigScalars (CliHandle, u4ContextId) == CLI_SUCCESS)
    {
        if (u4SysMode == VCM_MI_MODE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }

        if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG)
        {
            RstpShowRunningConfigInterface (CliHandle, u4ContextId);
        }
    }
    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstpShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in rstp for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
RstpShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4SysMode = (UINT4) AST_INIT_VAL;
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];
    INT4                i4ModStatus;
    INT4                i4SystemControl;
    INT4                i4MstSysControl = MST_SNMP_SHUTDOWN;
    INT4                i4PvrstSysControl = PVRST_SNMP_SHUTDOWN;
    INT4                i4Version = 0;
    INT4                i4HelloTime = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4Priority = 0;
    INT4                i4HoldCount = 0;
    INT4                i4DynamicPathcostCalc = AST_SNMP_FALSE;
    INT4                i4DynPathCostCalcSpeedChng = AST_SNMP_FALSE;
    INT4                i4FlushInterval = AST_INIT_VAL;
    INT4                i4FlushThreshold = AST_INIT_VAL;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4AltRoleOpt = 0;

    /*SystemControl */
    nmhGetFsMIRstSystemControl (i4ContextId, &i4SystemControl);
    nmhGetFsMIRstFwdDelayAltPortRoleTrOptimization (i4ContextId, &i4AltRoleOpt);
    u4SysMode = AstVcmGetSystemMode (AST_PROTOCOL_ID);

    if (i4SystemControl == RST_SNMP_SHUTDOWN)
    {
        if (u4ContextId == AST_DEFAULT_CONTEXT)
        {
#ifdef MSTP_WANTED
            nmhGetFsMIMstSystemControl (i4ContextId, &i4MstSysControl);
#endif
#ifdef PVRST_WANTED
            nmhGetFsMIPvrstSystemControl (i4ContextId, &i4PvrstSysControl);
#endif

            if ((i4MstSysControl == MST_SNMP_SHUTDOWN) &&
                (i4PvrstSysControl == PVRST_SNMP_SHUTDOWN))
            {
                if (u4SysMode == VCM_MI_MODE)
                {
                    AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

                    AstVcmGetAliasName (u4ContextId, au1ContextName);
                    if (STRLEN (au1ContextName) != 0)
                    {
                        CliPrintf (CliHandle, "\rswitch  %s \r\n",
                                   au1ContextName);
                    }
                }
                CliPrintf (CliHandle, "shutdown spanning-tree \r\n");
                if (u4SysMode == VCM_MI_MODE)
                {
                    CliPrintf (CliHandle, "! \r\n");
                }
            }
        }
        return CLI_FAILURE;
    }

    nmhGetFsMIRstModuleStatus (i4ContextId, &i4ModStatus);

    /*StpVersion */
    nmhGetFsDot1dStpVersion (i4ContextId, &i4Version);

    /*ForwardDelay */
    nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);

    /*HelloTime */
    nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);

    /*Max Age */
    nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);

    /* Flush Interval */
    nmhGetFsMIRstFlushInterval (i4ContextId, &i4FlushInterval);

    /* Flush Indication Threshold */
    nmhGetFsMIRstFlushIndicationThreshold (i4ContextId, &i4FlushThreshold);
    /*TransmitHoldCount */
    nmhGetFsDot1dStpTxHoldCount (i4ContextId, &i4HoldCount);
    nmhGetFsMIRstBpduGuard (i4ContextId, &i4BpduGuard);

    /*Priority */
    nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority);

    /*Dynamic Pathcost calculation */
    nmhGetFsMIRstDynamicPathcostCalculation (i4ContextId,
                                             &i4DynamicPathcostCalc);
    /*Dynamic path cost calculation on interface speed change */
    nmhGetFsMIRstCalcPortPathCostOnSpeedChg (i4ContextId,
                                             &i4DynPathCostCalcSpeedChng);

#ifndef MSTP_WANTED
    if (i4ContextId != AST_DEFAULT_CONTEXT)
#endif
    {
        if (i4SystemControl == RST_SNMP_START)
        {
            if (u4SysMode == VCM_MI_MODE)
            {
                AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

                AstVcmGetAliasName (u4ContextId, au1ContextName);
                if (STRLEN (au1ContextName) != 0)
                {
                    CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
                }
            }

            CliPrintf (CliHandle, "spanning-tree mode rst\r\n");
        }
    }

    /*Module Status */

    if (i4ModStatus != RST_ENABLED)
    {
        CliPrintf (CliHandle, "no spanning-tree\r\n");
    }

    if ((AstCliGetBridgeMode (u4ContextId) == AST_PROVIDER_EDGE_BRIDGE_MODE) ||
        (AstCliGetBridgeMode (u4ContextId) == AST_PROVIDER_CORE_BRIDGE_MODE))
    {
        RstpPbPrintModuleStatus (CliHandle, u4ContextId);
    }
    if (i4Version == AST_VERSION_0)
    {
        CliPrintf (CliHandle, "spanning-tree compatibility stp\r\n");
    }

    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (i4FwdDelay != RST_DEFAULT_BRG_FWD_DELAY)
    {
        CliPrintf (CliHandle, "spanning-tree forward-time %d \r\n",
                   AST_PROT_TO_BPDU_SEC (i4FwdDelay));
    }

    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    if (i4HelloTime != RST_DEFAULT_BRG_HELLO_TIME)
    {
        CliPrintf (CliHandle, "spanning-tree hello-time %d \r\n",
                   AST_PROT_TO_BPDU_SEC (i4HelloTime));
    }

    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
    if (i4MaxAge != RST_DEFAULT_BRG_MAX_AGE)
    {
        CliPrintf (CliHandle, "spanning-tree max-age %d\r\n",
                   AST_PROT_TO_BPDU_SEC (i4MaxAge));
    }

    if (i4HoldCount != RST_DEFAULT_BRG_TX_LIMIT)
    {
        CliPrintf (CliHandle, "spanning-tree transmit hold-count %d \r\n",
                   i4HoldCount);
    }

    if (i4Priority != RST_DEFAULT_BRG_PRIORITY)
    {
        if (AstCliGetBridgeMode (u4ContextId) != AST_ICOMPONENT_BRIDGE_MODE)
        {
            CliPrintf (CliHandle, "spanning-tree priority %d\r\n", i4Priority);
        }
    }

    if (i4DynamicPathcostCalc == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic\r\n");
    }

    if (i4DynPathCostCalcSpeedChng == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic lag-speed\r\n");
    }

    if (i4FlushInterval != RST_DEFAULT_FLUSH_INTERVAL)
    {
        CliPrintf (CliHandle, "spanning-tree flush-interval %d\r\n",
                   i4FlushInterval);
    }

    if (i4FlushThreshold != RST_DEFAULT_FLUSH_IND_THRESHOLD)
    {
        CliPrintf (CliHandle, "spanning-tree flush-indication-threshold %d\r\n",
                   i4FlushThreshold);
    }
    i4FlushThreshold = AST_INIT_VAL;

    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard default\r\n");
    }
    if (i4AltRoleOpt != AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "spanning-tree forwarddelay optimization alternate-role disabled\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstpShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface table for  Rstp   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
RstpShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4OutCome;
    INT4                i4NextPort;
    INT4                i4CurrentPort;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1HeadFlag;
    AST_MEMSET (au1NameStr, 0, sizeof (au1NameStr));

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while ((i4OutCome != RST_FAILURE)
           && (i4NextPort <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        u1HeadFlag = AST_FALSE;
        RstpShowRunningConfigInterfaceDetails (CliHandle, i4NextPort,
                                               &u1HeadFlag);

        CliRegisterLock (CliHandle, AstLock, AstUnLock);
        AST_LOCK ();
        if (u1HeadFlag == AST_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
            u1HeadFlag = AST_FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);

    }

    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : RstpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in Rstp   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
RstpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex,
                                       UINT1 *pu1Flag)
{
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstMacAddr         BrgMacAddr;
    UINT4               u4ContextId;
    INT4                i4EdgePort;
    INT4                i4AutoEdge;
    INT4                i4PortPriority;
    INT4                i4PortStatus;
    INT4                i4LinkType;
    INT4                i4PortPathCost;
    INT4                i4RestricRole = (INT4) AST_INIT_VAL;
    INT4                i4RootGuard;
    INT4                i4RestricTCN = (INT4) AST_INIT_VAL;
    INT4                i4EnableBpduRx;
    INT4                i4EnableBpduTx;
    INT4                i4IsL2Gp;
    INT4                i4BrgPriority;
    INT4                i4LocalPort;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4BpduGuardAction = AST_INIT_VAL;
    INT4                i4Dot1wEnabled;
    UINT2               u2LocalPortId;
    UINT2               u2Prio;
    UINT1               u1PbPortType = VLAN_INVALID_PROVIDER_PORT;
    UINT1               au1PseudoRootIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               u1IsPathCostSet = AST_SNMP_FALSE;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1RestrictedRole = AST_FALSE;
    UINT1               u1RestrictedTCN = AST_FALSE;
    UINT1               u1PrintPseudoRootId = AST_SNMP_FALSE;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];
    /* Accessing MI nmhGet and nmhGetNext routines slows down the 
     * SRC output. Hence it is better to access the SI nmhGet and nmhGetNext
     * routines. So from the given IfIndex do the following:
     *            - get the Context id and Local port number.
     *            - Select the context
     *            - call SI nmh routines with local port number.
     * 
     * If we register AstLock and AstUnLock only with CliPrintf, then we
     * may face the following issue:
     *         - if the SRC output crosses a page, 
     *                 - the CLI will call AstUnLock.
     *                 - if the user presses any key other than 'q', the CLI
     *                   will call AstLock function.
     *                   As AstLock selects the default context, and this
     *                   may be same as the context selected in this 
     *                   show running config function. This will result in
     *                   wrong SRC out put.
     * To over come the above problem,  the following are added:
     *                - A global variable (gu4StpCliContext) - to store the 
     *                  Context required by SRC function.
     *                - and register the following 2 functions:
     *                - AstLockAndSelCliContext and AstUnLock.
     * For SRC functions that uses SI nmh routines the following to be done:
     *        - register with CLI the above mentioned function and AstUnLock. 
     *        - After selecting the context, set the gu4StpCliContext to
     *          the required context.
     * By this way, once the paging happens the CLI will call 
     * AstLockAndSelCliContext, when the user want to see more output.
     * And this AstLockAndSelCliContext will take the RSTP sem and will
     * also select the proper context.
     *
     */

    MEMSET (au1PseudoRootIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (BrgMacAddr, AST_INIT_VAL, AST_MAC_ADDR_SIZE);

    /* Needed this check when called from Cfa */

    CliRegisterLock (CliHandle, AstLockAndSelCliContext, AstUnLock);
    AST_LOCK ();

    if (AstGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    i4LocalPort = (INT4) u2LocalPortId;

    if (!AstIsRstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Select the context here as we call SI nmh routines below */
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing AST-LOCK in CliPrintf */
    gu4StpCliContext = u4ContextId;

    /* Get the Bridge Port-type */
    AstGetPbPortType (i4IfIndex, &u1PbPortType);

    if (nmhValidateIndexInstanceDot1dStpPortTable (i4LocalPort) == SNMP_SUCCESS)
    {
        pAstPortEntry = AST_GET_PORTENTRY (i4LocalPort);

        if (pAstPortEntry == NULL)
        {
            gu4StpCliContext = 0;
            AST_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        /*AutoEdge */
        nmhGetFsRstPortAutoEdge (i4LocalPort, &i4AutoEdge);
        /*PortPriority */

        nmhGetDot1dStpPortPriority (i4LocalPort, &i4PortPriority);

        /*PortStatus */
        nmhGetDot1dStpPortEnable (i4LocalPort, &i4PortStatus);

        /*EdgePort */
        nmhGetDot1dStpPortAdminEdgePort (i4LocalPort, &i4EdgePort);

        /*LinkType */
        nmhGetDot1dStpPortAdminPointToPoint (i4LocalPort, &i4LinkType);

        nmhGetFsRstPortStpModeDot1wEnabled (i4LocalPort, &i4Dot1wEnabled);

        nmhGetFsRstPortEnableBPDURx (i4LocalPort, &i4EnableBpduRx);
        nmhGetFsRstPortEnableBPDUTx (i4LocalPort, &i4EnableBpduTx);
        nmhGetFsRstPortIsL2Gp (i4LocalPort, &i4IsL2Gp);
        /*RestrictedRole */
        nmhGetFsRstPortRestrictedRole (i4LocalPort, &i4RestricRole);

        /*RestrictedTCN */
        nmhGetFsRstPortRestrictedTCN (i4LocalPort, &i4RestricTCN);

        if (u1PbPortType == VLAN_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_CUSTOMER_BRIDGE_PORT)
        {
            /*RestrictedRole */
            if (i4RestricRole == AST_SNMP_TRUE)
            {
                u1RestrictedRole = AST_SNMP_TRUE;
            }
            /*RestrictedTCN */
            if (i4RestricTCN == AST_SNMP_TRUE)
            {
                u1RestrictedTCN = AST_SNMP_TRUE;
            }
        }
        else
        {
            /*RestrictedRole */
            if (i4RestricRole == AST_SNMP_FALSE)
            {
                u1RestrictedRole = AST_SNMP_FALSE;
            }
            /*RestrictedTCN */
            if (i4RestricTCN == AST_SNMP_FALSE)
            {
                u1RestrictedTCN = AST_SNMP_FALSE;
            }
        }
        nmhGetFsRstPortRootGuard (i4LocalPort, &i4RootGuard);
        nmhGetFsRstPortBpduGuard (i4LocalPort, &i4BpduGuard);
        nmhGetFsRstPortPseudoRootId (i4LocalPort, &PseudoRootId);
        RstGetBridgeAddr (&BrgMacAddr);

        nmhGetDot1dStpPriority (&i4BrgPriority);

        PrintMacAddress (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1PseudoRootAddr);

        u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);
        /*Do not display, if Pseudo RootId is same as Birdge Id */
        if ((AST_MEMCMP (BrgMacAddr, PseudoRootId.pu1_OctetList +
                         AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE) != 0) ||
            (u2Prio != i4BrgPriority))
        {
            u1PrintPseudoRootId = AST_SNMP_TRUE;
        }
        if (AstIsPathCostSet ((UINT2) i4LocalPort,
                              RST_DEFAULT_INSTANCE) == RST_TRUE)
        {
            u1IsPathCostSet = AST_SNMP_TRUE;
        }

        if (((i4AutoEdge != AST_DEFAULT_AUTOEDGE_VALUE) ||
             (i4PortPriority != RST_DEFAULT_PORT_PRIORITY) ||
             (i4PortStatus != RST_PORT_ENABLED) ||
             (i4EdgePort != AST_SNMP_FALSE) ||
             (i4LinkType != RST_P2P_AUTO) ||
             (i4EnableBpduRx != AST_SNMP_TRUE) ||
             (i4EnableBpduTx != AST_SNMP_TRUE) || (i4IsL2Gp != AST_SNMP_FALSE)
             || (u1IsPathCostSet != AST_SNMP_FALSE)
             || (pAstPortEntry->bDot1wEnabled != RST_FALSE)
             || (u1RestrictedRole != AST_FALSE)
             || (u1RestrictedTCN != AST_FALSE)
             || (pAstPortEntry->bLoopGuard != RST_FALSE)
             || (i4BpduGuard != AST_BPDUGUARD_NONE)
             || (i4RootGuard != AST_SNMP_FALSE)
             || (u1PrintPseudoRootId != AST_SNMP_FALSE))
            && (*pu1Flag != AST_TRUE))
        {

            CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            *pu1Flag = AST_TRUE;
        }

        if (i4AutoEdge != AST_DEFAULT_AUTOEDGE_VALUE)
        {
            CliPrintf (CliHandle, "no spanning-tree auto-edge\r\n");
        }

        /*PortPathCost */
        if (u1IsPathCostSet != AST_SNMP_FALSE)
        {

            nmhGetDot1dStpPortAdminPathCost (i4LocalPort, &i4PortPathCost);
            CliPrintf (CliHandle, "spanning-tree cost %d\r\n", i4PortPathCost);
        }

        if (i4PortPriority != RST_DEFAULT_PORT_PRIORITY)
        {
            CliPrintf (CliHandle, "spanning-tree port-priority %d\r\n",
                       i4PortPriority);
        }

        if (i4PortStatus != RST_PORT_ENABLED)
        {
            CliPrintf (CliHandle, "spanning-tree disable\r\n");
        }

        if (i4EdgePort != AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree portfast\r\n");
        }

        if (i4LinkType != RST_P2P_AUTO)
        {
            if (i4LinkType == RST_P2P_FORCETRUE)
            {
                CliPrintf (CliHandle,
                           "spanning-tree link-type point-to-point\r\n");
            }

        }
        if (u1RestrictedRole == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree restricted-role\r\n");
        }
        else if (u1RestrictedRole == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "no spanning-tree restricted-role\r\n");
        }

        if (u1RestrictedTCN == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree restricted-tcn\r\n");
        }
        else if (u1RestrictedTCN == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "no spanning-tree restricted-tcn\r\n");
        }

        if (pAstPortEntry->bLoopGuard != RST_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree loop-guard\r\n");
        }

        if (pAstPortEntry->bDot1wEnabled == RST_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree mode dot1W enable\r\n");
        }

        if (i4EnableBpduRx != AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-receive disabled\r\n");
        }

        if (i4EnableBpduTx != AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-transmit disabled\r\n");
        }

        if (i4IsL2Gp != AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree layer2-gateway-port\r\n");
        }

        /*Do not display, if Pseudo RootId is same as Birdge Id */
        if (u1PrintPseudoRootId != AST_SNMP_FALSE)
        {
            CliPrintf
                (CliHandle,
                 "spanning-tree pseudoRootId priority %d mac-address %s\r\n",
                 u2Prio, au1PseudoRootAddr);

        }
        if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
        {
            nmhGetFsMIRstPortBpduGuardAction (i4IfIndex, &i4BpduGuardAction);
            if (i4BpduGuardAction == AST_PORT_ADMIN_DOWN)
            {
                CliPrintf (CliHandle,
                           "spanning-tree bpduguard enable admin-down\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "spanning-tree bpduguard enable\r\n");
            }
        }
        /*Root Guard */
        if (i4RootGuard == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree guard root\r\n");
        }

    }

    gu4StpCliContext = 0;
    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliDisplayInterfaceDetails                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface Details      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Value-Interface Info to be displayed             */
/*                        u4Index-Interface Index                            */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliDisplayInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Index, UINT4 u4Val)
{

    INT4                i4RetVal = 0;
    INT4                i4FwdTransition = 0;
    INT4                i4PortRole = 0;
    INT4                i4PortState = 0;
    INT4                i4PortStateStatus = 0;
    INT4                i4PortFast = 0;
    INT4                i4RootCost = 0;
    INT4                i4LinkType = 0;
    INT4                i4MigrnCnt = 0;
    INT4                i4PortPriority = 0;
    INT4                i4RstBpduCnt = 0;
    INT4                i4ConfigBpduCnt = 0;
    INT4                i4TcnBpduCnt = 0;
    INT4                i4RstBpdu = 0;
    INT4                i4ConfigBpdu = 0;
    INT4                i4TcnBpdu = 0;
    INT4                i4PortCost = 0;
    INT4                i4PathCost = 0;
    INT4                i4InvalidBpduCnt = 0;
    INT4                i4LoopIncState = AST_FALSE;
    INT4                i4RootInconsistentState = AST_FALSE;
    INT4                i4BpduInconsistentState = AST_FALSE;
    INT4                i4RestricRole = 0;
    INT4                i4RestricTCN = 0;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tAstPortEntry      *pAstPortEntry = NULL;

    if (u4Index != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4Index);

        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }

    switch (u4Val)
    {
        case STP_PORT_COST:

            nmhGetFsDot1dStpPortPathCost32 (u4Index, &i4PathCost);
            break;

        case STP_PORT_PRIORITY:

            nmhGetFsDot1dStpPortPriority (u4Index, &i4PortPriority);
            break;

        case STP_PORTFAST:
#ifdef PVRST_WANTED
            if (AstIsPvrstStartedInContext (u4ContextId))
            {
                nmhGetFsPvrstPortAdminEdgeStatus (u4Index, &i4PortFast);
            }
            else
#endif
            {
                nmhGetFsDot1dStpPortAdminEdgePort (u4Index, &i4PortFast);
            }

            break;
        case STP_SHOW_DETAIL:

            RstCliDisplayPortDetails (CliHandle, u4ContextId, u4Index);
            break;
        case STP_PORT_STATE:

            nmhGetFsDot1dStpPortState (u4Index, &i4PortState);

            AstGetHwPortStateStatus (u4Index, RST_DEFAULT_INSTANCE,
                                     &i4PortStateStatus);
            break;
        case STP_ROOT_COST:

            nmhGetFsDot1dStpRootCost (u4Index, &i4RootCost);
            break;

        case STP_RESTRICTED_ROLE:

            nmhGetFsMIRstPortRestrictedRole (u4Index, &i4RestricRole);
            break;

        case STP_RESTRICTED_TCN:

            nmhGetFsMIRstPortRestrictedTCN (u4Index, &i4RestricTCN);
            break;

        case STP_PORT_STATS:

            i4RetVal = AstCfaCliGetIfName (u4Index, (INT1 *) au1IntfName);

            /* Forward Transitions */

            nmhGetFsDot1dStpPortForwardTransitions ((INT4) u4Index,
                                                    (UINT4 *) &i4FwdTransition);

            /* PortRecieved RSTP BPDU Count */

            nmhGetFsMIRstPortRxRstBpduCount ((INT4) u4Index,
                                             (UINT4 *) &i4RstBpduCnt);

            /* Port Received Config BPDU Count */

            nmhGetFsMIRstPortRxConfigBpduCount ((INT4) u4Index,
                                                (UINT4 *) &i4ConfigBpduCnt);

            /* Port Recieved TCN BPDU Count */
            nmhGetFsMIRstPortRxTcnBpduCount ((INT4) u4Index,
                                             (UINT4 *) &i4TcnBpduCnt);

            /* Port Transmitted RSTP BPDU Count */
            nmhGetFsMIRstPortTxRstBpduCount ((INT4) u4Index,
                                             (UINT4 *) &i4RstBpdu);

            /* Port Transmitted Config BPDU Count */
            nmhGetFsMIRstPortTxConfigBpduCount ((INT4) u4Index,
                                                (UINT4 *) &i4ConfigBpdu);

            /* Port Transmitted TCN BPDU Count */
            nmhGetFsMIRstPortTxTcnBpduCount ((INT4) u4Index,
                                             (UINT4 *) &i4TcnBpdu);

            /* Port Invalid BPDU Received Count */
            nmhGetFsMIRstPortInvalidRstBpduRxCount ((INT4) u4Index,
                                                    (UINT4 *)
                                                    &i4InvalidBpduCnt);

            /* Port Protocol Migration Count */
            nmhGetFsMIRstPortProtocolMigrationCount ((INT4) u4Index,
                                                     (UINT4 *) &i4MigrnCnt);

            break;

        default:

            /*PortRole */
            nmhGetFsMIRstPortRole (u4Index, &i4PortRole);

            /*Port State */
            nmhGetFsDot1dStpPortState (u4Index, &i4PortState);

            AstGetHwPortStateStatus (u4Index, RST_DEFAULT_INSTANCE,
                                     &i4PortStateStatus);
            /*Port Cost */
            nmhGetFsDot1dStpPortPathCost32 (u4Index, &i4PortCost);
            /*PortPriority */
            nmhGetFsDot1dStpPortPriority (u4Index, &i4PortPriority);
            /* Link Type */
#ifdef PVRST_WANTED
            if (AstIsPvrstStartedInContext (u4ContextId))
            {
                nmhGetFsMIPvrstPortOperPointToPoint (u4Index, &i4LinkType);
            }
            else
#endif
            {
                nmhGetFsDot1dStpPortOperPointToPoint (u4Index, &i4LinkType);
            }
            break;

    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been gruped under the below
     *  switch-case */

    switch (u4Val)

    {

        case STP_PORT_COST:

            CliPrintf (CliHandle, "\r\nPort cost is %-9d \r\n", i4PathCost);
            break;

        case STP_PORT_PRIORITY:

            CliPrintf (CliHandle, "\r\nPort Priority is %d \r\n",
                       i4PortPriority);
            break;

        case STP_PORTFAST:

            if (i4PortFast == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nPortFast is enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nPortFast is disabled\r\n");
            }

            break;

        case STP_SHOW_DETAIL:

            /* Already displayed */
            break;
        case STP_PORT_STATE:

            CliPrintf (CliHandle, "\r\n");
            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "\r\n");
            break;

        case STP_ROOT_COST:

            CliPrintf (CliHandle, "\r\nRoot Cost is %d\r\n", i4RootCost);
            break;

        case STP_RESTRICTED_ROLE:
            if (i4RestricRole == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nRestricted Role is Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRestricted Role is Disabled\r\n");
            }
            break;

        case STP_RESTRICTED_TCN:
            if (i4RestricTCN == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nRestricted TCN is Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRestricted TCN is Disabled\r\n");
            }
            break;

        case STP_PORT_STATS:

            /* Get the Interface Name for the Port */
            if (i4RetVal == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%% PortName for index %d  is not found \r\n",
                           u4Index);
            }
            CliPrintf (CliHandle, "\r\nStatistics for Port %-4s\r\n",
                       au1IntfName);

            CliPrintf (CliHandle,
                       "Number of Transitions to forwarding State : %d\r\n",
                       i4FwdTransition);

            CliPrintf (CliHandle,
                       "Number of RSTP BPDU Count received        : %d\r\n",
                       i4RstBpduCnt);

            CliPrintf (CliHandle,
                       "Number of Config BPDU Count received      : %d\r\n",
                       i4ConfigBpduCnt);

            CliPrintf (CliHandle,
                       "Number of TCN BPDU Count received         : %d\r\n",
                       i4TcnBpduCnt);

            CliPrintf (CliHandle,
                       "Number of Invalid BPDU Count received     : %d\r\n",
                       i4InvalidBpduCnt);

            CliPrintf (CliHandle,
                       "Number of RSTP BPDU Count Transmitted     : %d\r\n",
                       i4RstBpdu);

            CliPrintf (CliHandle,
                       "Number of Config BPDU Count Transmitted   : %d\r\n",
                       i4ConfigBpdu);

            CliPrintf (CliHandle,
                       "Number of TCN BPDU Count Transmitted      : %d\r\n",
                       i4TcnBpdu);

            CliPrintf (CliHandle,
                       "Port Protocol Migration Count             : %d\r\n",
                       i4MigrnCnt);
            break;

        default:

            CliPrintf (CliHandle,
                       "\r\n%-13s%-13s%-9s%-5s%-8s\r\n", "Role", "State",
                       "Cost", "Prio", "Type");

            CliPrintf (CliHandle, "%-13s%-13s%-9s%-5s%-8s\r\n", "----", "-----",
                       "----", "----", "----");

            StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");

            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "%-3s", " ");

            CliPrintf (CliHandle, "%-9d", i4PortCost);

            CliPrintf (CliHandle, "%-5d", i4PortPriority);

            if (i4LinkType == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "P2P");
            }
            else
            {
                CliPrintf (CliHandle, "SharedLan");
            }
            nmhGetFsMIRstLoopInconsistentState (u4Index, &i4LoopIncState);
            nmhGetFsMIRstRootInconsistentState (u4Index,
                                                &i4RootInconsistentState);
            nmhGetFsMIRstPortBpduInconsistentState (u4Index,
                                                    &i4BpduInconsistentState);

            if (i4LoopIncState == RST_TRUE)
            {
                CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
            }
            else if (i4RootInconsistentState == RST_TRUE)
            {
                CliPrintf (CliHandle, "(*Root-Inc)\r\n");
            }
            else if (i4BpduInconsistentState == RST_TRUE)
            {
                CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
            }

            break;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowSpanningTree                             */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the RootBridge information */
/*                          the bridge details and also the  information of  */
/*                          all the active ports                             */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Val-Info to be displayed                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Active)
{
    INT4                i4Priority;

    INT4                i4ModStatus;
    INT4                i4Version = 0;
    INT4                i4HelloTime;
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4PortCost;
    INT4                i4PortPriority;
    INT4                i4LinkType;
    tAstMacAddr         MacAddress;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4CurrentPort, i4NextPort;
    INT4                i4OutCome;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = 0;
    UINT4               u4Quit = CLI_SUCCESS;
    UINT1               u1OperStatus;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4DynamicPathCost;
    INT4                i4DynamicPathCostLag;
    INT4                i4LoopIncState = AST_FALSE;
    INT4                i4RootInconsistentState = AST_FALSE;
    INT4                i4BpduInconsistentState = AST_FALSE;

    nmhGetFsMIRstModuleStatus (i4ContextId, &i4ModStatus);

    if (i4ModStatus != RST_DISABLED)
    {
        nmhGetFsDot1dStpVersion (i4ContextId, &i4Version);
    }

    /*Display Root Bridge Details */
    RstCliShowRootBridgeInfo (CliHandle, u4ContextId);

    /*Display Bridge Details */

    /* Bridge Priority */

    if (nmhGetFsDot1dStpPriority (i4ContextId, &i4Priority) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    /*Bridge Address */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    RstGetBridgeAddr (&MacAddress);
    AstReleaseContext ();

    /*Hello Time */

    nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */

    nmhGetFsDot1dStpBridgeMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsDot1dStpBridgeForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Dynamic Path Cost */

    nmhGetFsMIRstDynamicPathcostCalculation (i4ContextId, &i4DynamicPathCost);
    nmhGetFsMIRstCalcPortPathCostOnSpeedChg
        (i4ContextId, &i4DynamicPathCostLag);

    RstCliPrintStpModuleStatus (CliHandle, u4ContextId, i4ModStatus, i4Version);

    CliPrintf (CliHandle, "Bridge Id       Priority %d\r\n ", i4Priority);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "               Address %s\r\n", au1BrgAddr);

    CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "Max Age %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "                Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    if (i4DynamicPathCost == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Disabled\r\n");
    }

    if (i4DynamicPathCostLag == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Disabled\r\n");
    }

    /* Display the Port Role,PortState,PortCost,PortPriority,PortLinkType */

    CliPrintf (CliHandle,
               "%-18s%-13s%-13s%-9s%-7s%-8s\r\n",
               "Name", "Role", "State", "Cost", "Prio", "Type");
    CliPrintf (CliHandle,
               "%-18s%-13s%-13s%-9s%-7s%-8s\r\n",
               "----", "----", "-----", "----", "----", "------");

    /* Get The Current Port */

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);
    while (i4OutCome != RST_FAILURE)
    {
        /* Get the port oper status for RSTP/MSTP */
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                nmhGetFsMIRstPortRole (i4NextPort, &i4PortRole);
                nmhGetFsDot1dStpPortState (i4NextPort, &i4PortState);
                if (((i4PortState == AST_PORT_STATE_LEARNING) ||
                     (i4PortState == AST_PORT_STATE_FORWARDING))
                    && (i4PortRole != AST_PORT_ROLE_DISABLED))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

                /*PortRole */
                nmhGetFsMIRstPortRole (i4NextPort, &i4PortRole);

                /*Port State */
                nmhGetFsDot1dStpPortState (i4NextPort, &i4PortState);

                AstGetHwPortStateStatus (i4NextPort, RST_DEFAULT_INSTANCE,
                                         &i4PortStateStatus);
                /*Port Cost */
                nmhGetFsDot1dStpPortPathCost32 (i4NextPort, &i4PortCost);
                /*PortPriority */
                nmhGetFsDot1dStpPortPriority (i4NextPort, &i4PortPriority);
                /* Link Type */
                nmhGetFsDot1dStpPortOperPointToPoint (i4NextPort, &i4LinkType);

                CliPrintf (CliHandle, "%-18s", au1IntfName);

                StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");

                StpCliDisplayPortState (CliHandle, i4PortState,
                                        i4PortStateStatus);
                CliPrintf (CliHandle, "%-3s", " ");

                CliPrintf (CliHandle, "%-9d", i4PortCost);

                CliPrintf (CliHandle, "%-7d", i4PortPriority);

                if (i4LinkType == AST_SNMP_TRUE)
                {
                    /* u4Quit contains the paging status */
                    u4Quit = CliPrintf (CliHandle, "P2P");
                }
                else
                {
                    u4Quit = CliPrintf (CliHandle, "SharedLan");
                }
                nmhGetFsMIRstLoopInconsistentState (i4NextPort,
                                                    &i4LoopIncState);
                nmhGetFsMIRstRootInconsistentState (i4NextPort,
                                                    &i4RootInconsistentState);
                nmhGetFsMIRstPortBpduInconsistentState (i4NextPort,
                                                        &i4BpduInconsistentState);

                if (i4LoopIncState == RST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                }
                else if (i4RootInconsistentState == RST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                }
                else if (i4BpduInconsistentState == RST_TRUE)
                {
                    CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                /* Get the Next Port from the Table */

            }
            if (u4Quit == CLI_FAILURE)
            {
                break;
            }
        }
        u1Flag = 0;
        i4CurrentPort = i4NextPort;
        i4OutCome =
            AstGetNextPortInContext (u4ContextId, i4CurrentPort, &i4NextPort);

    }                            /*while */

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliMIShowRootBridgeInfo                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliShowRootBridgeInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tAstMacAddr         MacAddress;

    INT4                i4RetVal = 0;
    INT4                i4HelloTime = 0;
    INT4                i4RootCost = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4MaxAge = 0;

    UINT2               u2RootPrio = 0;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4RootPort = 0;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4ModStatus;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /* To Check  if the  Bridge is a ROOT Bridge */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    RstGetBridgeAddr (&MacAddress);
    AstReleaseContext ();

    /*Displays the Root Id,Root Timers */
    nmhGetFsMIRstModuleStatus (i4ContextId, &i4ModStatus);

    /* Get The  Root ID-Priority and MacAddress */

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &RetBridgeId);

    /*Root Cost */

    nmhGetFsDot1dStpRootCost (i4ContextId, &i4RootCost);

    /*Root Port */
    nmhGetFsDot1dStpRootPort (i4ContextId, &i4RootPort);

    i4RetVal = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);

    /*Root Hello Time */

    nmhGetFsDot1dStpBridgeHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Root MaxAge */

    nmhGetFsDot1dStpMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsDot1dStpForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (AST_MEMCMP
        ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
         MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "\r\nWe are the root of the Spanning Tree\r\n");

    }

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    CliPrintf (CliHandle, "Root Id         Priority   %d\r\n", u2RootPrio);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                     au1BrgAddr);
    CliPrintf (CliHandle, "                Address    %s\r\n ", au1BrgAddr);

    CliPrintf (CliHandle, "               Cost       %d\r\n", i4RootCost);

    /* Get the Interface Name for the Port */
    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   " %%PortName for index %d  is not found \r\n", i4RootPort);
    }
    CliPrintf (CliHandle, "                Port       %-9s\r\n", au1IntfName);

    CliPrintf (CliHandle, "                Max Age %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "                Hello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliShowRootBridgeTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information                                        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
RstCliShowRootBridgeTable (tCliHandle CliHandle, UINT4 u4ContextId)
{

    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4Intf = CLI_FAILURE;
    INT4                i4RootPort;
    INT4                i4RootCost;

    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE DesigRoot;
    UINT1              *pu1Temp = NULL;
    INT4                i4ContextId = (INT4) u4ContextId;

    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    DesigRoot.pu1_OctetList = &au1BrgIdBuf[0];
    DesigRoot.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /*Root Bridge Id */
    nmhGetFsDot1dStpDesignatedRoot (i4ContextId, &DesigRoot);

    /* Root Cost */

    nmhGetFsDot1dStpRootCost (i4ContextId, &i4RootCost);

    /*Root MaxAge */

    nmhGetFsDot1dStpMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsDot1dStpForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* Root Port */
    nmhGetFsDot1dStpRootPort (i4ContextId, &i4RootPort);

    pu1Temp = &au1IntfName[0];
    AST_MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4Intf = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) pu1Temp);

    /*Displays the Root Id,Root Timers,RootCost,RootPort  in the table format */
    CliPrintf (CliHandle,
               "\r\n%-25s%-9s%-15s%-15s%-9s\r\n",
               "Root ID", "RootCost", "MaxAge", "FwdDly", "RootPort");
    CliPrintf (CliHandle,
               "%-25s%-9s%-15s%-15s%-9s\r\n",
               "-------", "--------", "------", "------", "--------");

    AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
    CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length, au1BrgId,
                   AST_BRG_ID_DIS_LEN);
    CliPrintf (CliHandle, "%-25s", au1BrgId);

    CliPrintf (CliHandle, "%-9d", i4RootCost);
    CliPrintf (CliHandle, "%-2.2dsec %-2.2dcs     ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "%-2.2dsec %-2.2dcs     ",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    /* Get the Interface Name for the Port */
    if (i4Intf == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%PortName for index %d  is not found \r\n",
                   i4Intf);
    }
    CliPrintf (CliHandle, "%-9s\r\n", pu1Temp);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliDisplayBlockedPorts                          */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the BlockedPorts       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
RstCliDisplayBlockedPorts (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = 0;
    INT4                i4CurrentPort, i4NextPort;
    INT4                i4OutCome;
    UINT4               u4BlockedPort = 0;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&CfaIfInfo, 0, sizeof (CfaIfInfo));
    CliPrintf (CliHandle, "\r\nBlocked Interfaces List:\r\n");

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while (i4OutCome != RST_FAILURE)
    {
        nmhGetFsDot1dStpPortState (i4NextPort, &i4RetVal);
        if (AstCfaGetIfInfo ((UINT4) i4NextPort, &CfaIfInfo) != CFA_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Cannot Get CFA Information for Port %s\n",
                          AST_GET_IFINDEX_STR (i4NextPort));
        }
        if ((i4RetVal == AST_PORT_STATE_DISCARDING) &&
            (CfaIfInfo.u1IfAdminStatus != AST_PORT_ADMIN_DOWN))
        {
            i4RetVal =
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

            CliPrintf (CliHandle, "%s,", au1IntfName);

            u4BlockedPort++;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome =
            AstGetNextPortInContext (u4ContextId, i4CurrentPort, &i4NextPort);
    }

    CliPrintf (CliHandle, "\b");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "\rThe Number of Blocked Ports in the system is :%d\r\n\r\n",
               u4BlockedPort);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstCliShowSUmmary                                    */
/*                                                                           */
/* Description        : Displays the spanningtree port states and  port roles*/
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context                */
/*                      u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/

INT4
RstCliShowSummary (tCliHandle CliHandle, UINT4 u4ContextId)
{

    INT4                i4PrevPort;
    INT4                i4NextPort;
    INT4                i4OutCome;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4RstPortStatus;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IntfName, 0, sizeof (au1IntfName));

    /* Displays the Spanning Tree PathCost Method ,PortStates 
       and Roles */

    CliPrintf (CliHandle, "\r\nSpanning tree enabled protocol" " is RSTP");
    RstCliShowSpanningTreeMethod (CliHandle);
    CliPrintf (CliHandle, "\r\nRSTP Port Roles and States\r\n");
    CliPrintf (CliHandle,
               "%-18s%-13s%-13s%-13s\r\n",
               "Port-Index", "Port-Role", "Port-State", "Port-Status");
    CliPrintf (CliHandle,
               "%-18s%-13s%-13s%-13s\r\n",
               "----------", "---------", "----------", "-----------");

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while (i4OutCome != RST_FAILURE)
    {
        nmhGetFsMIRstPortRole (i4NextPort, &i4PortRole);
        nmhGetFsDot1dStpPortState (i4NextPort, &i4PortState);
        AstGetHwPortStateStatus (i4NextPort, RST_DEFAULT_INSTANCE,
                                 &i4PortStateStatus);
        nmhGetFsDot1dStpPortEnable (i4NextPort, &i4RstPortStatus);

        AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);
        CliPrintf (CliHandle, "%-18s", au1IntfName);
        /* Port Role */
        StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
        CliPrintf (CliHandle, "%-3s", " ");

        /* Port State */
        StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
        CliPrintf (CliHandle, "%-3s", " ");

        /* Port Status */
        if (i4RstPortStatus == RST_PORT_ENABLED)
        {
            CliPrintf (CliHandle, "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Disabled\r\n");
        }

        i4PrevPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId,
                                             i4PrevPort, &i4NextPort);
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

UINT2
AstCliGetPortIdFromOctetList (tSNMP_OCTET_STRING_TYPE PortId)
{
    UINT2               u2PortId;

    AST_MEMCPY ((UINT1 *) &u2PortId, PortId.pu1_OctetList, AST_PORT_ID_SIZE);
    u2PortId = OSIX_NTOHS (u2PortId);

    return u2PortId;
}

/*****************************************************************************/
/* Function Name      : RstCliShowSpanningTreePortL2gp                       */
/*                                                                           */
/* Description        : Displays l2gp specific information of port.          */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context                */
/*                      u4ContextId - Context Identifier                     */
/*                      u4Index - Port Index.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/

INT4
RstCliShowSpanningTreePortL2gp (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4IsL2Gp;
    INT4                i4OutCome;
    INT4                i4NextPort;
    INT4                i4CurrentPort;
    UINT2               u2Prio;
    UINT1               u1OperStatus;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PseudoRootIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];

    MEMSET (au1PseudoRootIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    if (u4Index != 0)
    {
        i4NextPort = u4Index;
        pAstPortEntry = AstGetIfIndexEntry (u4Index);

        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }

    }

    while (i4OutCome != RST_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            nmhGetFsMIRstPortIsL2Gp (i4NextPort, &i4IsL2Gp);

            if (i4IsL2Gp == RST_TRUE)
            {
                /* Get the interface name for this Port Number */
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

                nmhGetFsMIRstPortPseudoRootId (i4NextPort, &PseudoRootId);

                PrintMacAddress (PseudoRootId.pu1_OctetList
                                 + AST_BRG_PRIORITY_SIZE, au1PseudoRootAddr);

                u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

                CliPrintf (CliHandle, "\r\n Port %s \r\n", au1IntfName);

                CliPrintf (CliHandle, "%18s", "PseudoRootId");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, "%s  %15s", "Priority", "MacAddress");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, "%s   %s", "---------",
                           "------------------");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, " %d", u2Prio);
                CliPrintf (CliHandle, "%27s", au1PseudoRootAddr);

                CliPrintf (CliHandle, "\r\n");
            }
        }

        if (u4Index != 0)
        {
            break;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstResetPseudoRootId                               */
/*                                                                           */
/*     DESCRIPTION      : This function Resets the Pseudo RootId for a port. */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
RstResetPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum)
{
    tAstMacAddr         MacAddress;
    INT4                i4Priority;

    AST_MEMSET (MacAddress, AST_INIT_VAL, AST_MAC_ADDR_SIZE);

    RstGetBridgeAddr (&MacAddress);

    if (nmhGetDot1dStpPriority (&i4Priority) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return (RstConfigPseudoRootId (CliHandle, u4PortNum, MacAddress,
                                   (UINT2) i4Priority));
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : AstCliGetPortInfoHelloTime                        */
/*                                                                          */
/*     DESCRIPTION      : This function gets the hello time value received  */
/*                        in this port.                                     */
/*                                                                          */
/*     INPUT            :  i4NextPort   - port number                       */
/*                         pi4HelloTime - pointer for the hello time value  */
/*                                        to be updated                     */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  SNMP_FAILURE/SNMP_SUCCESS                        */
/*                                                                          */
/*****************************************************************************/

INT4
AstCliGetPortInfoHelloTime (INT4 i4NextPort, INT4 *pi4HelloTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstGetContextInfoFromIfIndex (i4NextPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);

    if (pAstPortEntry == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4HelloTime = (INT4) (pAstPortEntry->PortTimes.u2HelloTime);

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : AstCliGetBridgeMode                               */
/*                                                                          */
/*     DESCRIPTION      : This function gets the bridge mode.               */
/*                                                                          */
/*     INPUT            :  u4ContextId - Context Identifier                 */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  Bridge Mode                                      */
/*                                                                          */
/*****************************************************************************/
UINT4
AstCliGetBridgeMode (UINT4 u4ContextId)
{
    UINT4               u4BridgeMode = AST_INVALID_BRIDGE_MODE;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return AST_INVALID_BRIDGE_MODE;
    }

    u4BridgeMode = AST_BRIDGE_MODE ();

    AstReleaseContext ();

    return u4BridgeMode;
}

/* RSTP clear statistics feature */
/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliResetBridgeCounters                         */
/*                                                                          */
/*     DESCRIPTION      : This function resets all the bridge and           */
/*                        interface associated counters.                    */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
RstCliResetBridgeCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    i1Return =
        nmhTestv2FsMIRstClearBridgeStats (&u4ErrCode, u4CurrContextId,
                                          RST_ENABLED);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }

    i1Return = nmhSetFsMIRstClearBridgeStats (u4CurrContextId, RST_ENABLED);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliResetPortCounters                           */
/*                                                                          */
/*     DESCRIPTION      : This function resets all interface                */
/*                        associated counters.                              */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
RstCliResetPortCounters (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsMIRstPortClearStats (&u4ErrCode, u4IfIndex, RST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }
    i1Return = nmhSetFsMIRstPortClearStats (u4IfIndex, RST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* RSTP clear statistics feature */
/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliShowPerformanceData                         */
/*                                                                          */
/*     DESCRIPTION      : This function displays performance data           */
/*                        related to STP                                    */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/
INT4
RstCliShowPerformanceData (tCliHandle CliHandle)
{
    UINT4               u4EventTimeStamp = 0;
    UINT4               u4StateChangeTimeStamp = 0;
    UINT4               u4Vlan = 0;
    INT4                i4Event = 0;
    INT4                i4EventSubType = 0;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();
    INT2                i2MstInst = 0;
    INT1               *pIfName = NULL;
    CHR1                ac1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tAstPerStInfo      *pPerStInfo = NULL;
#ifdef PVRST_WANTED
    UINT4               u4TempContextId = 0;
    INT2                i2Count = 0;
    INT2                i2Counter = 0;
    UINT2               u2LocalPortId = 0;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#endif
    AST_MEMSET (ac1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pIfName = (INT1 *) &au1IfName[0];

    if (AstIsPvrstEnabledInContext (u4CurrContextId))
    {
        CliPrintf (CliHandle, "\r\n STP Topology change statistics    \r\n");
        CliPrintf (CliHandle, " ===============================    \r\n");
        AstSelectContext (u4CurrContextId);
        i2MstInst = 0;
        for (u4Vlan = 1; u4Vlan <= VLAN_MAX_VLAN_ID; u4Vlan++)

        {
            if ((AstL2IwfMiIsVlanActive
                 (u4CurrContextId, (tVlanId) u4Vlan)
                 == OSIX_FALSE) ||
                (INVALID_INDEX ==
                 AstL2IwfMiGetVlanInstMapping (u4CurrContextId,
                                               (tVlanId) u4Vlan)))
            {
                continue;
            }
            i2MstInst =
                (INT2) AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                     (tVlanId) u4Vlan);
            if (i2MstInst == INVALID_INDEX)
            {
                return PVRST_FAILURE;
            }
#ifdef PVRST_WANTED
            pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (i2MstInst);

            CliPrintf (CliHandle, "\r\n Vlan %d :   \r\n", u4Vlan);
            CliPrintf (CliHandle, "===============================    \r\n");
            CliPrintf (CliHandle, "Topology change detected ports :   \r\n");
            i2Count = pPerStBrgInfo->u1DetHead;
            if (i2Count == 0)
            {
                i2Count = 9;
            }
            i2Counter = 2;
            while (i2Counter > 0)
            {
                if (pPerStBrgInfo->au2TcDetPorts[i2Count] != 0)
                {
                    if (AstGetContextInfoFromIfIndex
                        (pPerStBrgInfo->au2TcDetPorts[i2Count],
                         &u4TempContextId, &u2LocalPortId) != RST_SUCCESS)
                    {
                        --i2Count;
                        if (i2Count == 0)
                        {
                            i2Count = 9;
                        }
                        continue;
                    }

                    AST_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (pPerStBrgInfo->au2TcDetPorts[i2Count],
                                         pIfName);
                    CliPrintf (CliHandle, "\r\nPort  : %s  \r\n", pIfName);
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2LocalPortId, i2MstInst);
                    CliPrintf (CliHandle,
                               "Topology change detected count : %d  \r\n",
                               pPerStPortInfo->u4TcDetectedCount);
                    UtlGetTimeStrForTicks (pPerStPortInfo->
                                           u4TcDetectedTimestamp, ac1TimeStr);
                    CliPrintf (CliHandle,
                               "Topology change detected time  : %s \r\n",
                               ac1TimeStr);

                }
                --i2Count;
                --i2Counter;
            }
            CliPrintf (CliHandle, "\r\n    \r\n");
            CliPrintf (CliHandle, "Topology change received ports :   \r\n");
            i2Count = pPerStBrgInfo->u1RcvdHead;
            if (i2Count == 0)
            {
                i2Count = 9;
            }
            i2Counter = 2;
            while (i2Counter > 0)
            {
                if (pPerStBrgInfo->au2TcRecvPorts[i2Count] != 0)
                {
                    if (AstGetContextInfoFromIfIndex
                        (pPerStBrgInfo->au2TcRecvPorts[i2Count],
                         &u4TempContextId, &u2LocalPortId) != RST_SUCCESS)
                    {
                        --i2Count;
                        if (i2Count == 0)
                        {
                            i2Count = 9;
                        }
                        continue;

                    }

                    AST_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (pPerStBrgInfo->au2TcRecvPorts[i2Count],
                                         pIfName);
                    CliPrintf (CliHandle, "\r\nPort  : %s  \r\n", pIfName);
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2LocalPortId, i2MstInst);
                    CliPrintf (CliHandle,
                               "Topology change received count : %d  \r\n",
                               pPerStPortInfo->u4TcRcvdCount);
                    AST_MEMSET (ac1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                    UtlGetTimeStrForTicks (pPerStPortInfo->u4TcRcvdTimestamp,
                                           ac1TimeStr);
                    CliPrintf (CliHandle,
                               "Topology change received time  : %s \r\n",
                               ac1TimeStr);
                }
                --i2Count;
                --i2Counter;
            }
#endif
        }
        AstReleaseContext ();

        return CLI_SUCCESS;
    }
    else
    {
        nmhGetFsMIRstRcvdEventTimeStamp (u4CurrContextId, &u4EventTimeStamp);
        nmhGetFsMIRstRcvdPortStateChangeTimeStamp (u4CurrContextId,
                                                   &u4StateChangeTimeStamp);
        nmhGetFsMIRstRcvdEvent (u4CurrContextId, &i4Event);
        nmhGetFsMIRstRcvdEventSubType (u4CurrContextId, &i4EventSubType);

        CliPrintf (CliHandle, "\r\n STP Performance data    \r\n");
        CliPrintf (CliHandle, " =====================    \r\n");
        switch (i4EventSubType)
        {
            case AST_EXT_PORT_UP:
                CliPrintf (CliHandle, "\r Received Event           "
                           "                      : PORT_UP\r\n");
                break;
            case AST_EXT_PORT_DOWN:
                CliPrintf (CliHandle, "\r Received Event           "
                           "                      : PORT_DOWN\r\n");
                break;

        }

        CliPrintf (CliHandle, "\r Received Event Time Stamp"
                   "(In millisecs)        : %u\r\n", u4EventTimeStamp);
        CliPrintf (CliHandle, "\r Port State Change Time Stamp"
                   "(In millisecs)     : %u\r\n", u4StateChangeTimeStamp);
        if (AstIsMstEnabledInContext (u4CurrContextId))
        {
            CliPrintf (CliHandle,
                       "\r\n STP Topology change statistics    \r\n");
            CliPrintf (CliHandle, " ===============================    \r\n");
            AstSelectContext (u4CurrContextId);
            i2MstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
#ifdef PVRST_WANTED
                pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);
                CliPrintf (CliHandle, "\r\n Instance MST0%d :   \r\n",
                           i2MstInst);
                CliPrintf (CliHandle,
                           "===============================    \r\n");
                CliPrintf (CliHandle,
                           "\r\n Topology change detected ports :   \r\n");
                i2Count = pPerStBrgInfo->u1DetHead;
                if (i2Count == 0)
                {
                    i2Count = 9;
                }
                i2Counter = 2;
                while (i2Counter > 0)
                {
                    if (pPerStBrgInfo->au2TcDetPorts[i2Count] != 0)
                    {
                        if (AstGetContextInfoFromIfIndex
                            (pPerStBrgInfo->au2TcDetPorts[i2Count],
                             &u4TempContextId, &u2LocalPortId) != RST_SUCCESS)
                        {
                            --i2Count;
                            if (i2Count == 0)
                            {
                                i2Count = 9;
                            }

                            continue;
                        }

                        AST_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName (pPerStBrgInfo->
                                             au2TcDetPorts[i2Count], pIfName);
                        CliPrintf (CliHandle, "\r\nPort  : %s  \r\n", pIfName);
                        pPerStPortInfo =
                            AST_GET_PERST_PORT_INFO (u2LocalPortId, i2MstInst);
                        CliPrintf (CliHandle,
                                   "Topology change detected count : %d  \r\n",
                                   pPerStPortInfo->u4TcDetectedCount);
                        AST_MEMSET (ac1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                        UtlGetTimeStrForTicks (pPerStPortInfo->
                                               u4TcDetectedTimestamp,
                                               ac1TimeStr);
                        CliPrintf (CliHandle,
                                   "Topology change detected time  : %s \r\n",
                                   ac1TimeStr);
                    }
                    --i2Count;
                    --i2Counter;
                }
                CliPrintf (CliHandle, "\r\n    \r\n");
                CliPrintf (CliHandle,
                           "\r\n Topology change received ports :   \r\n");
                i2Count = pPerStBrgInfo->u1RcvdHead;
                if (i2Count == 0)
                {
                    i2Count = 9;
                }
                i2Counter = 2;
                while (i2Counter > 0)
                {
                    if (pPerStBrgInfo->au2TcRecvPorts[i2Count] != 0)
                    {
                        if (AstGetContextInfoFromIfIndex
                            (pPerStBrgInfo->au2TcRecvPorts[i2Count],
                             &u4TempContextId, &u2LocalPortId) != RST_SUCCESS)
                        {
                            --i2Count;
                            if (i2Count == 0)
                            {
                                i2Count = 9;
                            }

                            continue;
                        }

                        AST_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName (pPerStBrgInfo->
                                             au2TcRecvPorts[i2Count], pIfName);
                        CliPrintf (CliHandle, "\r\nPort  : %s  \r\n", pIfName);
                        pPerStPortInfo =
                            AST_GET_PERST_PORT_INFO (u2LocalPortId, i2MstInst);
                        CliPrintf (CliHandle,
                                   "Topology change received count : %d  \r\n",
                                   pPerStPortInfo->u4TcRcvdCount);
                        AST_MEMSET (ac1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                        UtlGetTimeStrForTicks (pPerStPortInfo->
                                               u4TcDetectedTimestamp,
                                               ac1TimeStr);
                        CliPrintf (CliHandle,
                                   "Topology change received time  : %s \r\n",
                                   ac1TimeStr);
                    }
                    --i2Count;
                    --i2Counter;

                }
#endif
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliShowPerformanceDataIface                    */
/*                                                                          */
/*     DESCRIPTION      : This function displays performance data           */
/*                        on a particular port                              */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4IfIndex - Interface Index                       */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/
INT4
RstCliShowPerformanceDataIface (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4IfIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4EventTimeStamp = 0;
    UINT4               u4StateChangeTimeStamp = 0;
    INT4                i4Event = 0;
    INT4                i4EventSubType = 0;
#ifdef MSTP_WANTED
    INT4                i4IfIndex = (INT4) u4IfIndex;
    INT4                i4NextIfIndex = 0;
    INT4                i4Inst = 0;
    INT4                i4NextInst = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                i1OutCome = SNMP_SUCCESS;
#endif

    if (u4IfIndex == 0)
    {
        return CLI_FAILURE;
    }
    else
    {
        pAstPortEntry = AstGetIfIndexEntry (u4IfIndex);

        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }
    if (AstIsRstStartedInContext (u4ContextId))
    {
        nmhGetFsMIRstPortRcvdEventTimeStamp (u4IfIndex, &u4EventTimeStamp);
        nmhGetFsMIRstPortRcvdEvent (u4IfIndex, &i4Event);
        nmhGetFsMIRstPortRcvdEventSubType (u4IfIndex, &i4EventSubType);
        nmhGetFsMIRstPortStateChangeTimeStamp (u4IfIndex,
                                               &u4StateChangeTimeStamp);
    }
#ifdef MSTP_WANTED
    else if (AstIsMstStartedInContext (u4ContextId))
    {
        nmhGetFsMIMstCistPortRcvdEventTimeStamp (u4IfIndex, &u4EventTimeStamp);
        nmhGetFsMIMstCistPortRcvdEvent (u4IfIndex, &i4Event);
        nmhGetFsMIMstCistPortRcvdEventSubType (u4IfIndex, &i4EventSubType);
    }
#endif
    CliPrintf (CliHandle, "\r\n STP Performance data at Port %d \r\n",
               u4IfIndex);
    CliPrintf (CliHandle, "\r\n ================================    \r\n");

    CliPrintf (CliHandle, "\rRcvd Event Time Stamp"
               "(In millisecs)                 : %d\r\n", u4EventTimeStamp);
    if (i4EventSubType == AST_EXT_PORT_UP)
    {
        CliPrintf (CliHandle, "\rRcvd Event           "
                   "                               : PORT_UP\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rRcvd Event           "
                   "                               : PORT_DOWN\r\n");
    }
    if (AstIsRstStartedInContext (u4ContextId))
    {
        CliPrintf (CliHandle, "\rRcvd State Change Time Stamp"
                   "(In millisecs)          : %d\r\n", u4StateChangeTimeStamp);
    }
    else
    {
#ifdef MSTP_WANTED
        if (AstIsMstStartedInContext (u4ContextId))
        {
            CliPrintf (CliHandle,
                       "\r\n Inst Number\t PortStateChangeTimeStamp\r\n");
            CliPrintf (CliHandle,
                       "\r -----------\t ------------------------\r\n");
            i4NextInst = MST_CIST_CONTEXT;
            i4NextIfIndex = i4IfIndex;
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiPortTable (i4IfIndex, &i4NextIfIndex,
                                                     i4Inst, &i4NextInst);
            while ((i1OutCome == SNMP_SUCCESS) && (i4NextIfIndex == i4IfIndex))
            {
                nmhGetFsMIMstMstiPortStateChangeTimeStamp (u4IfIndex,
                                                           i4NextInst,
                                                           &u4StateChangeTimeStamp);
                CliPrintf (CliHandle, "\r %d\t\t %d\r\n", i4NextInst,
                           u4StateChangeTimeStamp);
                i4Inst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiPortTable (i4IfIndex,
                                                         &i4NextIfIndex, i4Inst,
                                                         &i4NextInst);
                u4PagingStatus = CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }
            }
        }
#endif
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliSetFlushInterval                            */
/*                                                                          */
/*     DESCRIPTION      : This function configures the flush trigger        */
/*                        interval value for the context                    */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4FlushInterval - Flush interval in centi-secs    */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/
INT4
RstCliSetFlushInterval (tCliHandle CliHandle, UINT4 u4FlushInterval)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIRstFlushInterval (&u4ErrCode, u4CurrContextId,
                                       (INT4) u4FlushInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIRstFlushInterval (u4CurrContextId, (INT4) u4FlushInterval)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : RstCliSetFlushIndThreshold                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the flush indication     */
/*                        threshold for the instance                        */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4Threshold - Flush indication threshold          */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/****************************************************************************/
INT4
RstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Threshold)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIRstFlushIndicationThreshold (&u4ErrCode,
                                                  u4CurrContextId,
                                                  (INT4) u4Threshold)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIRstFlushIndicationThreshold (u4CurrContextId,
                                               (INT4) u4Threshold)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  RstCliSetForwardDelayAltPort                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the optimization status         */
/*                        (enabled/disabled) for the RSTP Module             */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                        u4RstOptStatus - RSTP Optimization status          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RstCliSetForwardDelayAltPort (tCliHandle CliHandle, UINT4 u4RstOptStatus)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIRstFwdDelayAltPortRoleTrOptimization
        (&u4ErrCode, u4CurrContextId, (INT4) u4RstOptStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Setting the fdWhile timer during ALTERNATE ROLE TRANSITION */
    if (nmhSetFsMIRstFwdDelayAltPortRoleTrOptimization
        (u4CurrContextId, (INT4) u4RstOptStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetBpduGuardStatus                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global status               */
/*                        Enabled/Disabled) of the BPDU Guard Feature.       */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4GblBpduGuardStatus - enable/disable status       */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetBpduGuardStatus (tCliHandle CliHandle, UINT4 u4GblBpduGuardStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstBpduGuard (&u4ErrCode, u4GblBpduGuardStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsRstBpduGuard (u4GblBpduGuardStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetStpPerformanceStatus                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global status               */
/*                        Enabled/Disabled) of the STP Performance Feature.  */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4GblStpPerformanceStatus - enable/disable status  */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetStpPerformanceStatus (tCliHandle CliHandle,
                               UINT4 u4GblStpPerformanceStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRstStpPerfStatus
        (&u4ErrCode, (INT4) u4GblStpPerformanceStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsRstStpPerfStatus ((INT4) u4GblStpPerformanceStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% nmhSetFsRstStpPerfStatus failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : RstDisplayInterfaceBpduGuard                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays Bpdu Guard Status on the    */
/*                        Interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
RstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (nmhValidateIndexInstanceFsMIRstPortExtTable (i4Index) == SNMP_SUCCESS)
    {
        pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
        if (pAstPortEntry == NULL)
        {
            return;
        }

        nmhGetFsMIRstPortBpduGuard (i4Index, &i4BpduGuard);
        /*Bpdu Guard  */
        if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nInterface BPDU Guard  is Enabled \r\n");
        }
        else if (i4BpduGuard == AST_BPDUGUARD_DISABLE)
        {
            CliPrintf (CliHandle, "\r\nInterface BPDU Guard  is Disabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nInterface BPDU Guard  status is none \r\n");
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RstCliSetPortErrorRecovery                         */
/*                                                                           */
/*     DESCRIPTION      : This function set the Error Recovery Time for an   */
/*                        Interface                                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle   -Handle to the CLI context             */
/*                        u4PortNum   - Port Number                          */
/*                        u4ErrorRecovery - ErrorRecover                     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
RstCliSetPortErrorRecovery (tCliHandle CliHandle, UINT4 u4PortNum,
                            UINT4 u4ErrorRecovery)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4TmpErrorRecovery = 0;
    u4TmpErrorRecovery = AST_SYS_TO_MGMT (u4ErrorRecovery);

    if (nmhTestv2FsRstPortErrorRecovery
        (&u4ErrCode, (INT4) u4PortNum,
         (INT4) u4TmpErrorRecovery) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsRstPortErrorRecovery
        ((INT4) u4PortNum, (INT4) u4TmpErrorRecovery) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RstDisplayPortInconsistency                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays port-level inconsistency    */
/*                        due to root guard or loop guard.                   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
RstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Inconsistency = AST_INIT_VAL;

    if (nmhGetFsMIRstRootInconsistentState (i4Index,
                                            &i4Inconsistency) != SNMP_FAILURE)
    {
        if (i4Inconsistency == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "Root Inconsistent\r\n");
        }
    }

    if (nmhGetFsMIRstLoopInconsistentState (i4Index,
                                            &i4Inconsistency) != SNMP_FAILURE)
    {
        if (i4Inconsistency == RST_TRUE)
        {
            CliPrintf (CliHandle, "Loop Inconsistent\r\n");
        }
    }
    if (nmhGetFsMIRstPortBpduInconsistentState (i4Index,
                                                &i4Inconsistency) !=
        SNMP_FAILURE)
    {
        if (i4Inconsistency == RST_TRUE)
        {
            CliPrintf (CliHandle, "Bpdu Inconsistent\r\n");
        }
    }

    return;
}
