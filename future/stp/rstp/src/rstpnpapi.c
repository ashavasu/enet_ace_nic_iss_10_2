/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rstpnpapi.c,v 1.3 2013/11/14 11:33:46 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              for Hardware API's w.r.t RSTP
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __RSTP_NP_API_C
#define __RSTP_NP_API_C

#include "astminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiStpNpHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiStpNpHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiStpNpHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiStpNpHwInit (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiStpNpHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_STP_NP_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiStpNpHwInit;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiRstpNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstpNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstpNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiRstpNpInitHw (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstpNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RSTP_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstpNpInitHw;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiRstpNpSetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstpNpSetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstpNpSetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstpNpSetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RSTP_NP_SET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstpNpSetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiRstpMbsmNpSetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstpMbsmNpSetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstpMbsmNpSetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstpMbsmNpSetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RSTP_MBSM_NP_SET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpSetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiRstpMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstpMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstpMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiRstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstpMbsmNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RSTP_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpInitHw;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiRstNpGetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstNpGetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstNpGetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1Status)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstNpGetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RST_NP_GET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstNpGetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1Status = pu1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef PB_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiPbRstpNpSetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPbRstpNpSetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPbRstpNpSetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiPbRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVid,
                              UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiPbRstpNpSetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_PB_RSTP_NP_SET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstpNpSetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVid = SVid;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiPbRstNpGetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPbRstNpGetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPbRstNpGetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiPbRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                             tVlanId SVlanId, UINT1 *pu1Status)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiPbRstNpGetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_PB_RST_NP_GET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstNpGetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SVlanId = SVlanId;
    pEntry->pu1Status = pu1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiPbRstpMbsmNpSetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPbRstpMbsmNpSetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPbRstpMbsmNpSetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiPbRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId Svid, UINT1 u1Status,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiPbRstpMbsmNpSetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstpMbsmNpSetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->Svid = Svid;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */
#endif /* PB_WANTED */
#ifdef PBB_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : RstpFsMiPbbRstHwServiceInstancePtToPtStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPbbRstHwServiceInstancePtToPtStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiPbbRstHwServiceInstancePtToPtStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RstpFsMiPbbRstHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                             UINT4 u4VipIndex, UINT4 u4Isid,
                                             UINT1 u1PointToPointStatus)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiPbbRstHwServiceInstancePtToPtStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS,    /* Function/OpCode */
                         u4VipIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiPbbRstHwServiceInstancePtToPtStatus;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4VipIndex = u4VipIndex;
    pEntry->u4Isid = u4Isid;
    pEntry->u1PointToPointStatus = u1PointToPointStatus;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* PBB_WANTED */
#endif /* __RSTP_NP_API_C */
