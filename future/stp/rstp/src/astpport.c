/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpport.c,v 1.38 2017/12/11 09:58:31 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              AST Module.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstCfaCliGetIfName                                   */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    UINT4               u4Isid = AST_INIT_VAL;

    /* This is used by CLI module to print the interface name. Now Get the 
     * interface name for this Port Number, if this is a physical Port.
     * Otherwise for VIPs, display the ISID value. The context identifier is
     * already displayed. Hence, it is enough if ISID alone is displayed. For
     * PEPs, it is taken care seperately.
     * */

    if (AstIsPortVip (u4IfIndex) == RST_TRUE)
    {
        /* Port is a VIP */
        if (AstL2IwfGetIsid (u4IfIndex, &u4Isid) != L2IWF_SUCCESS)
        {
            return CLI_FAILURE;
        }
        SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH,
                  "%s-%s:%d", "VIP", "ISID", u4Isid);
        return CLI_SUCCESS;
    }
    else
    {
        return (CfaCliGetIfName (u4IfIndex, pi1IfName));
    }
}

/*****************************************************************************/
/* Function Name      : AstCfaGetIfInfo                                      */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface information.                   */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : AstCfaGetIfOperStatus                                */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface oper status from the given interface index.*/
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1OperStatus   - Interface Oper Status.             */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstCfaGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    return (CfaGetIfOperStatus (u4IfIndex, pu1OperStatus));
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstAttrRPUpdateInstVlanMap                           */
/*                                                                           */
/* Description        : This function calls the GARP/MRP Module to update the*/
/*                      VLAN and the instance mapping.                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1Action    - Add / Update/ Delete                   */
/*                      u2NewMapId  - New Map Identifier                     */
/*                      u2OldMapId  - Old Map Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                      u2MapEvent  - Indicates whether it is a single VLAN  */
/*                                    to MSTI or list of VLAN map/unmap.     */
/*                      pu1Result   - output to indicate message post or     */
/*                                    updation.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
AstAttrRPUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId,
                            UINT2 u2OldMapId, tVlanId VlanId,
                            UINT2 u2MapEvent, UINT1 *pu1Result)
{
    if (AstGarpIsGarpEnabledInContext (AST_CURR_CONTEXT_ID ()) == GARP_TRUE)
    {
        GarpMiUpdateInstVlanMap (u4ContextId, u1Action, u2NewMapId, u2OldMapId,
                                 VlanId, u2MapEvent, pu1Result);
    }
    else if (AstMrpIsMrpStartedInContext (AST_CURR_CONTEXT_ID ()) == OSIX_TRUE)
    {
        MrpApiUpdateInstVlanMap (u4ContextId, u1Action, u2NewMapId, u2OldMapId,
                                 VlanId, u2MapEvent, pu1Result);
    }

}

/*****************************************************************************/
/* Function Name      : AstVlanMiFlushFdbId                                  */
/*                                                                           */
/* Description        : This function calls the VLAN module to flush the     */
/*                      FDB entries in the context.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u4FdbId- FDB Identifier                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanMiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
    return (VlanApiFlushFdbId (u4ContextId, u4FdbId));
}

/*****************************************************************************/
/* Function Name      : AstVlanMiGetVlanLearningMode                         */
/*                                                                           */
/* Description        : This function calls the VLAN module to flush the     */
/*                      FDB entries in the context.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
AstVlanMiGetVlanLearningMode (UINT4 u4ContextId)
{
    return (L2IwfGetVlanLearningType (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetMemberVlanList                            */
/*                                                                           */
/* Description        : This API is used to get the list of VLANs in which   */
/*                      the particular port is member of.                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                      u4IfIndex - ifIndex of the driver port.              */
/*                                                                           */
/* Output(s)          : VlanListInPort - Bit map for the vlans to which this */
/*                                       this port is member of.             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfGetMemberVlanList (UINT4 u4ContextId, UINT4 u4IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pVlanIdList)
{
    return (L2IwfGetPortVlanListFromIfIndex (u4ContextId, pVlanIdList,
                                             u4IfIndex, AST_L2_MEMBER_PORT));
}

/*****************************************************************************/
/* Function Name      : MstVcmSispGetPhysicalPortOfSispPort                  */
/*                                                                           */
/* Description        : This function will provide the physical interface    */
/*                      index over which the given logical port runs. This   */
/*                      will be invoked by MSTP to obtain the corresponding  */
/*                      physical interface index over which a logical port   */
/*                      runs.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Logical interface index.                 */
/*                                                                           */
/* Output(s)          : pu4PhyIndex - Physical interface index over which    */
/*                                    this logical runs.                     */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
MstVcmSispGetPhysicalPortOfSispPort (UINT4 u4IfIndex, UINT4 *pu4PhyIndex)
{
    return (VcmSispGetPhysicalPortOfSispPort (u4IfIndex, pu4PhyIndex));
}

/*****************************************************************************/
/* Function Name      : MstVcmSispGetSispPortsInfoOfPhysicalPort             */
/*                                                                           */
/* Description        : This function will provide the physical interface    */
/*                      index over which the given logical port runs. This   */
/*                      will be invoked by MSTP to obtain the corresponding  */
/*                      physical interface index over which a logical port   */
/*                      runs.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Logical interface index.                 */
/*                                                                           */
/* Output(s)          : pau4SispPorts-Physical interface index over which    */
/*                                    this logical runs.                     */
/*                      pu4PortCnt - No. of ports                            */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
MstVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                          UINT1 u1RetLocalPorts,
                                          VOID *paSispPorts,
                                          UINT4 *pu4PortCount)
{
    return (VcmSispGetSispPortsInfoOfPhysicalPort
            (u4PhyIfIndex, u1RetLocalPorts, paSispPorts, pu4PortCount));
}
#endif

/*****************************************************************************/
/* Function Name      : AstVlanFlushFdbEntries                               */
/*                                                                           */
/* Description        : This function calls the VLAN module to flush the     */
/*                      FDB entries.                                         */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      u4FdbId- FDB Identifier                              */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                      grouped with the flush call for other ports as a     */
/*                      as a single flush call.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanFlushFdbEntries (UINT4 u4Port, UINT4 u4FdbId, INT4 i4OptimizeFlag)
{
    return (VlanFlushFdbEntriesOnPort (u4Port, u4FdbId, STP_MODULE,
                                       i4OptimizeFlag));
}

/*****************************************************************************/
/* Function Name      : AstGarpIsGarpEnabledInContext                        */
/*                                                                           */
/* Description        : This function calls the GARP module to get the       */
/*                      the GARP Module Status.                              */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_TRUE / GARP_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
AstGarpIsGarpEnabledInContext (UINT4 u4ContextId)
{
    return (GarpIsGarpEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstMrpIsMrpEnabledInContext                          */
/*                                                                           */
/* Description        : This function calls the MRP module to get the        */
/*                      the MRP Module Status.                               */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
AstMrpIsMrpStartedInContext (UINT4 u4ContextId)
{
    return (MrpApiIsMrpStarted (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstIssGetContextMacAddress                           */
/*                                                                           */
/* Description        : This function calls the CFA module to get the        */
/*                      system MAC address.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                                                                           */
/* Output(s)          : pSwitchMac - pointer to the switch MAC address       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
AstIssGetContextMacAddress (UINT4 u4ContextId, tMacAddr pSwitchMac)
{
    IssGetContextMacAddress (u4ContextId, pSwitchMac);
}

/*****************************************************************************/
/* Function Name      : AstVcmGetAliasName                                   */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************/
/* Function Name      : AstVcmGetContextInfoFromIfIndex                      */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                 UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

/*****************************************************************************/
/* Function Name      : AstVcmGetSystemMode                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : AstVcmGetSystemModeExt                               */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : AstVcmIsSwitchExist                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************/
/* Function Name      : AstVlanDeleteFdbEntries                              */
/*                                                                           */
/* Description        : This function calls the VLAN module to delete the    */
/*                      FDB entries.                                         */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                      grouped with the flush call for other ports as a     */
/*                      as a single flush call.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanDeleteFdbEntries (UINT4 u4Port, INT4 i4OptimizeFlag)
{
    return (VlanDeleteFdbEntries (u4Port, i4OptimizeFlag));
}

/*****************************************************************************/
/* Function Name      : AstVlanGetStartedStatus                              */
/*                                                                           */
/* Description        : This function calls the VLAN module to get the       */
/*                      system status.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanGetStartedStatus (UINT4 u4ContextId)
{
    return (VlanGetStartedStatus (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstVlanIsVlanEnabledInContext                        */
/*                                                                           */
/* Description        : This function calls the VLAN module to get the       */
/*                      module status.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanIsVlanEnabledInContext (UINT4 u4ContextId)
{
    return (VlanIsVlanEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetBridgeMode                                */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPortOperStatus                            */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u4IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
AstL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                           UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u4IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPortVlanTunnelStatus                      */
/*                                                                           */
/* Description        : This routine returns the port's tunnel status        */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Index of the port whose tunnel    */
/*                                    status is to be obtained.              */
/*                                                                           */
/* Output(s)          : bTunnelPort - Boolean value indicating whether       */
/*                                    Tunnelling is enabled on the port      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfGetPortVlanTunnelStatus (UINT4 u4IfIndex, BOOL1 * pbTunnelPort)
{
    return (L2IwfGetPortVlanTunnelStatus (u4IfIndex, pbTunnelPort));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstL2IwfGetProtocolTunnelStatusOnPort            */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u2Protocol     - L2 Protocol                     */
/*                                                                           */
/*    Output(s)           : u1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
AstL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                       UINT1 *pu1TunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, u2Protocol, pu1TunnelStatus);
}

/*****************************************************************************/
/* Function Name      : AstL2IwfHandleOutgoingPktOnPort                      */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      PNAC packet.                                         */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      u4IfIndex        - Port                              */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4IfIndex, UINT4 u4PktSize,
                                 UINT2 u2Protocol, UINT1 u1Encap)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex, u4PktSize,
                                          u2Protocol, u1Encap));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfIsPortInPortChannel                          */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfMiGetVlanFdbId                               */
/*                                                                           */
/* Description        : This routine returns the FdbId corresponding to the  */
/*                      given VlanId. It accesses the L2Iwf common database  */
/*                      for the given context                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
AstL2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanFdbId (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfMiIsVlanActive                               */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
AstL2IwfMiIsVlanActive (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiIsVlanActive (u4ContextId, VlanId));
}

/*****************************************************************************/
/*    Function Name             : AstL2IwfSendCustBpduOnPep                  */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfSendCustBpduOnPep (UINT4 u4CepIfIndex, tVlanId SVlanId,
                           tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4PktSize)
{
    return (L2IwfSendCustBpduOnPep (u4CepIfIndex, SVlanId, pBuf, u4PktSize));
}

/*****************************************************************************/
/*    Function Name             : AstL2IwfSetPortOperPointToPointStatus      */
/*                                                                           */
/*    Description               : This API is used by L2 modules to set port */
/*                                oper status in L2Iwf and trigger Vlan and  */
/*                                hardware for change of oper point to point */
/*                                status                                     */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Context Id                   */
/*                                u2LocalPortId - Local port index of port   */
/*                                bOperPointToPoint - point to point status  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfSetPortOperPointToPointStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                       BOOL1 bOperPointToPoint)
{
    INT4                i4RetVal = L2IWF_FAILURE;

    i4RetVal = L2IwfSetPortOperPointToPointStatus (u4ContextId, u2LocalPortId,
                                                   bOperPointToPoint);

    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name             : AstL2IwfSetPerformanceDataStatus           */
/*                                                                           */
/*    Description               : This API is used by L2 modules to set port */
/*                                oper status in L2Iwf and trigger Vlan and  */
/*                                hardware for change of oper point to point */
/*                                status                                     */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Context Id                   */
/*                                bPerfDataStatus - point to point status  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfSetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 bPerfDataStatus)
{
    INT4                i4RetVal = L2IWF_FAILURE;

    i4RetVal = L2IwfSetPerformanceDataStatus (u4ContextId, bPerfDataStatus);

    return i4RetVal;
}

INT4
AstL2IwfGetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 * pbPerfDataStatus)
{
    INT4                i4RetVal = L2IWF_FAILURE;

    i4RetVal = L2IwfGetPerformanceDataStatus (u4ContextId, pbPerfDataStatus);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstIssGetAsyncMode                                   */
/*                                                                           */
/* Description        : This function calls the ISS module to check          */
/*                      NPAPI operating in Synchronous or Asynchronous mode  */
/*                                                                           */
/* Input(s)           : i4ProtoId - Protocol Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstIssGetAsyncMode (INT4 i4ProtoId)
{
    return (IssGetAsyncMode (i4ProtoId));
}

#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstL2IwfGetVlanPortPvid                              */
/*                                                                           */
/* Description        : This routine returns the port VLAN Id for the given  */
/*                      given Port Index. It accesses the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortPvid   - Port VLAN Id.                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfGetVlanPortPvid (UINT2 u2IfIndex, tVlanId * pVlanId)
{
    return L2IwfGetVlanPortPvid (u2IfIndex, pVlanId);
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetVlanPortType                              */
/*                                                                           */
/* Description        : This routine returns the port tunnel type            */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortType   - Port Type VLAN_ACCESS_PORT/            */
/*                                              VLAN_HYBRID_PORT/            */
/*                                              VLAN_TRUNK_PORT              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType)
{
    return L2IwfGetVlanPortType (u4IfIndex, pPortType);
}

#endif

#ifdef L2RED_WANTED
#ifdef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : AstVlanRedHandleUpdateEvents                         */
/*                                                                           */
/* Description        : This function calls the VLAN module to update the    */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstVlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (VlanRedHandleUpdateEvents (u1Event, pData, u2DataLen));
}
#else
#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : AstLldpRedRcvPktFromRm                               */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task.                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event to be sent to LLDP                 */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
UINT4
AstLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
#endif
/*****************************************************************************/
/* Function Name      : AstRmEnqMsgToRmFromAppl                              */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : AstRmGetNodeState                                    */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : AstRmGetStandbyNodeCount                            */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
AstRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : AstRmGetStaticConfigStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
AstRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : AstRmHandleProtocolEvent                          */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_INITIATE_BULK_UPDATE /            */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
AstRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : AstRmRegisterProtocols                               */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : AstRmDeRegisterProtocols                             */
/*                                                                           */
/* Description        : This function calls the RM module to deregister.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_RSTPMSTP_APP_ID));
}

/*****************************************************************************/
/* Function Name      : AstRmReleaseMemoryForMsg                             */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
AstRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : AstRmSetBulkUpdatesStatus                            */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}
#endif
/*****************************************************************************/
/* Function Name      : AstL2IwfGetIsid                                      */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the ISID */
/*                      associated with the given VIP.                       */
/*                                                                           */
/* Input(s)           : u4Vip - VIP Port Identifier.                         */
/*                                                                           */
/* Output(s)          : pu4Isid - ISID value that will be updated by L2IEF   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfGetIsid (UINT4 u4Vip, UINT4 *pu4Isid)
{
    return (L2IwfGetIsid (u4Vip, pu4Isid));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetVip                                       */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the  VIP */
/*                      associated with the given ISID And context           */
/*                                                                           */
/* Input(s)           : u4Vip - VIP Port Identifier.                         */
/*                                                                           */
/* Output(s)          : pu4Isid - ISID value that will be updated by L2IEF   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfGetVip (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip)
{
    return (L2IwfGetVip (u4Context, u4Isid, pu2Vip));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfTransmitBpduOnVip                            */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to transmit the */
/*                      BPDU out of a VIP.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                      u4IfIndex - ifIndex of the driver port.              */
/*                      pBuf      - Buf containing BPDU                      */
/*                      u4PktSize - Packet Size                              */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfTransmitBpduOnVip (UINT4 u4ContextId, UINT4 u4IfIndex,
                           tCRU_BUF_CHAIN_DESC * pBuf)
{
    tPbbTag             PbbTag;

    AST_MEMSET (&PbbTag, 0, sizeof (tPbbTag));

    PbbTag.InnerIsidTag.u1Priority = VLAN_DEF_TUNNEL_BPDU_PRIORITY;

    return (L2IwfTransmitFrameOnVip (u4ContextId, u4IfIndex, pBuf, NULL,
                                     &PbbTag, L2_STAP_PKT));
}

/*****************************************************************************/
/* Function Name      : AstSispIsSispPortPresentInCtxt                       */
/*                                                                           */
/* Description        : This function will check whether the context has any */
/*                      SISP enabled ports mapped to the same. Hence, if     */
/*                      spanning tree mode is to be configured as RSTP in a  */
/*                      context, that has SISP enabled ports mapped to the   */
/*                      same, this will block the same                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT1
AstSispIsSispPortPresentInCtxt (UINT4 u4ContextId)
{
    return (VcmSispIsSispPortPresentInCtxt (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstSispIsSispEnabledOnPort                           */
/*                                                                           */
/* Description        : MSTP will invoke this API to determine whether SISP  */
/*                      has been enabled in the interface. If enabled, the   */
/*                      result will be updated as VCM_TRUE and VCM_FALSE,    */
/*                      otherwise.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE/VCM_FALSE                                   */
/*                                                                           */
/*****************************************************************************/
UINT1
AstSispIsSispEnabledOnPort (UINT4 u4IfIndex)
{
    UINT1               u1Status = RST_FALSE;

    L2IwfGetSispPortCtrlStatus (u4IfIndex, &u1Status);

    u1Status = (u1Status == SISP_ENABLE) ? RST_TRUE : RST_FALSE;

    return (u1Status);
}

/*****************************************************************************
 *
 *    Function Name       : AstCfaCliConfGetIfName
 *
 *    Description         : This function returns Interface name for specified
 *                          interface
 *
 *    Input(s)            : u4IfIndex - Interface Index 
 *
 *    Output(s)           : pi1IfName - Pointer to buffer
 *
 *
 *    Returns            : CLI_SUCCESS if name assigned for pi1IfName
 *                         CLI_FAILURE if name is not assign pi1IfName
 *                         CFA_FAILURE if u4IfIndex is not valid interface
 *
 *****************************************************************************/
INT4
AstCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : AstPortMrpApiNotifyStpInfo                           */
/*                                                                           */
/*                                                                           */
/* DESCRIPTION      : This function is called from STP/L2IWF modules         */
/*                    to indicate the following.                             */
/*                    - MRP_PORT_ROLE_CHG_MSG                                */
/*                    - MRP_PORT_OPER_P2P_MSG                                */
/*                    - MRP_TCDETECTED_TMR_STATUS                            */
/*                                                                           */
/* INPUT            : pMrpInfo - Pointer to the structure which              */
/*                                  contains the MRP info from the above     */
/*                                  modules.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              */
/*****************************************************************************/
INT4
AstPortMrpApiNotifyStpInfo (tMrpInfo * pMrpInfo)
{
    return (L2IwfMrpApiNotifyStpInfo (pMrpInfo));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AstVcmGetIfIndexFromLocalPort                      */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Interface Index      */
/*                        from the given Localport and Context-Id.           */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u2LocalPort    - LocalPort.                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstVcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                               UINT4 *pu4IfIndex)
{
    return (VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPort, pu4IfIndex));
}

/*****************************************************************************/
/* Function Name      : AstCfaGetIfaceType                                   */
/*                                                                           */
/* Description        : This function gets the interface type                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfType:Pointer to the Interface type.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
AstCfaGetIfaceType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    return (CfaGetIfaceType (u4IfIndex, pu1IfType));
}

/*****************************************************************************/
/* Function Name        : AstL2IwfGetPortVlanMemberList                      */
/*                                                                           */
/* Description          : Returns the List of VLANs for which the given      */
/*                        port is a member.                                  */
/*                                                                           */
/* Input (s)            : u4IfIndex - Interface Index                        */
/*                                                                           */
/*                                                                           */
/* Output               : pu1VlanList - VLAN List                            */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
AstL2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList)
{
    return (L2IwfGetPortVlanMemberList (u4IfIndex, pu1VlanList));
}

/*****************************************************************************/
/* Function Name      : AstCfaNotifyProtoToApp                                */
/*                                                                           */
/* Description        : This function calls the CFA Module to notify the     */
/*                      applications.                                        */
/*                                                                           */
/* Input(s)           : u1ModeId - Specifies from which module the           */
/*                      notication is given.                                 */
/*                      NotifyProtoToApp - contains the indications specific */
/*                      to the notification.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
AstCfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{
    CfaNotifyProtoToApp (u1ModeId, NotifyProtoToApp);
}

#ifdef NPAPI_WANTED

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPotentialTxPortForAg                      */
/*                                                                           */
/* Description        : This function calls the L2Iwf function to get the    */
/*                      port on which pdu is transmitted for LAG             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of port-channel          */
/*                                                                           */
/* Output(s)          : pu2EgressPort - Port number of tx port               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
AstL2IwfGetPotentialTxPortForAgg (UINT4 u4IfIndex, UINT2 *pu2EgressPort)
{
    return (L2IwfGetPotentialTxPortForAgg ((UINT2) u4IfIndex, pu2EgressPort));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPortPnacAuthStatus                        */
/*                                                                           */
/* Description        : This function calls the L2Iwf function to get the    */
/*                      port's authstatus                       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of port                 */
/*                                                                           */
/* Output(s)          : u2PortAuthStatus - Auth status of port               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
#ifdef PNAC_WANTED
INT4
AstL2IwfGetPortPnacAuthStatus (UINT4 u4IfIndex, UINT2 *u2PortAuthStatus)
{
    return (L2IwfGetPortPnacAuthStatus (u4IfIndex, u2PortAuthStatus));
}
#endif
/*****************************************************************************/
/* Function Name      : AstCfaPostPacketToNP                                   */
/*                                                                           */
/* Description        : This function sends the STP BPDU to CFA packet  NP layer
 *                         transmitting task                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of port                 */
/*         pu1DataBuf - Buffer containing pkt                   */
/*         u4PktSize  - Size of the pkt to be transmitted       */
/*                                                                           */
/* Output(s)          : Non                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
AstCfaPostPacketToNP (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2EgressPort,
                      UINT4 u4PktSize)
{
    return (CfaPostPacketToNP (pBuf, u2EgressPort, u4PktSize));
}

#endif

/*****************************************************************************/
/* Function Name        : AstPortLaDisableOnMcLagIf                          */
/*                                                                           */
/* Description          : This function disables STP on all MC-LAG interfaces*/
/*                                                                           */
/* Input (s)            : None                                               */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : OSIX_SUCCESS/OSIX_FAILURE.                         */
/*****************************************************************************/
INT4
AstPortLaDisableOnMcLagIf ()
{
#ifdef LA_WANTED
    if (LaApiDisableStpOnMclagInt () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIcchGetIcclIfIndex                                */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
AstIcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclIfIndex (pu4IfIndex);
#else
    UNUSED_PARAM (pu4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : AstLaIsMclagInterface                                */
/*                                                                           */
/* Description        : This function is used to check whether the interface */
/*                      is MC-LAG interface or not.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE/RST_FALSE                                   */
/*                                                                           */
/*****************************************************************************/
UINT1
AstLaIsMclagInterface (UINT4 u4IfIndex)
{
#ifdef LA_WANTED
    UINT1               u1IsMclag = 0;
    LaApiIsMclagInterface (u4IfIndex, &u1IsMclag);
    if (u1IsMclag == OSIX_TRUE)
    {
        return RST_TRUE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return RST_FALSE;
}

/*****************************************************************************/
/* Function Name      : AstLaGetMCLAGSystemStatus                            */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_MCLAG_ENABLED or AST_MCLAG_DISABLED              */
/*                                                                           */
/*****************************************************************************/
UINT1
AstLaGetMCLAGSystemStatus (VOID)
{
#ifdef LA_WANTED
    return LaGetMCLAGSystemStatus ();
#else
    return AST_MCLAG_DISABLED;
#endif
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetConfiguredPortsForPortChannel             */
/*                                                                           */
/* Description        : This function is used to get the information about   */
/*                      configured ports in a port channel                   */
/* Input(s)           : u4IfIndex - IfIndex of Port channel                  */
/*                                                                           */
/* Output(s)          : au2ConfPorts - List of Configured ports              */
/*                      u2NumConfiguredPorts - Number of configured ports    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfGetConfiguredPortsForPortChannel (UINT2 u2AggId, UINT2 au2ConfPorts[],
                                          UINT2 *pu2NumPorts)
{
    return (L2IwfGetConfiguredPortsForPortChannel
            (u2AggId, au2ConfPorts, pu2NumPorts));
}

/*****************************************************************************/
/* Function Name      : STPCfaNotifyProtoToApp                     */
/*                                                                           */
/* Description        : This function calls the CFA Module to notify the     */
/*                      applications.                                        */
/*                                                                           */
/* Input(s)           : u1ModeId - Specifies from which module the           */
/*                      notication is given.                                 */
/*                      NotifyProtoToApp - contains the indications specific */
/*                      to the notification.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
STPCfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{
    CfaNotifyProtoToApp (u1ModeId, NotifyProtoToApp);
}

/*****************************************************************************/
/* Function Name      : AstL2IwfIsPvidVlanOfAnyPorts                         */
/*                                                                           */
/* Description        : This Api is used to check whether the VLAN is having */
/*                      is having only untagged ports                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u2VlanId - VLAN Index                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_TRUE / AST_FALSE                                 */
/*****************************************************************************/
INT4
AstL2IwfIsPvidVlanOfAnyPorts (UINT4 u4ContextId, tVlanId VlanId)
{
    if (L2IwfIsPvidVlanOfAnyPorts (u4ContextId, VlanId) == L2IWF_TRUE)
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}

/*****************************************************************************
*
*    Function Name         : AstCfaGetGlobalModTrc
*
*    Description           : This function is used to Get the Trace events from
*                                CFA module
*
*    Input(s)              : None
*
*    Output(s)             : gau4ModTrcEvents
*
*    Global Variables Referred : gau4ModTrcEvents
*
*    Returns               : gau4ModTrcEvents[0]
*******************************************************************************/
UINT4
AstCfaGetGlobalModTrc ()
{
    return AST_CONTROL_PATH_TRC;
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetUntaggedPortList                          */
/*                                                                           */
/* Description        : This function is used to get the untagged port list  */
/*                      of a VLAN from L2IWF                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      u1FCoEVlanType - L2_VLAN / FCOE_VLAN                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
AstL2IwfMiGetVlanUnTagPorts (UINT4 u4ContextId, tVlanId VlanId,
                             tPortList UnTagPorts)
{
    return L2IwfMiGetVlanUnTagPorts (u4ContextId, VlanId, UnTagPorts);
}

/*****************************************************************************/
/* Function Name      : AstL2IwfMiGetVlanEgressPorts                         */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the      */
/*                      VLAN egress porlist for the given vlan, instance     */
/*                      L2Iwf returns port list for the given VLAN           */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                    : VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : EgressPorts - egress port list (physical port)       */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfMiGetVlanEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                              tPortList EgressPorts)
{
    return (L2IwfMiGetVlanEgressPorts (u4ContextId, VlanId, EgressPorts));
}

/*****************************************************************************/
/* Function Name      : AstVlanDeleteVlanEntries                              */
/*                                                                           */
/* Description        : This function calls the VLAN module to delete the    */
/*                      FDB entries.                                         */
/*                                                                           */
/* Input(s)           : u4Vlan - Vlan number                                 */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                      grouped with the flush call for other ports as a     */
/*                      as a single flush call.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstVlanDeleteVlanEntries (UINT4 u4Vlan, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (i4OptimizeFlag);
    return (VlanMiFlushFdbId (AST_DEFAULT_CONTEXT, u4Vlan));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfIsPortInInst                                 */
/*                                                                           */
/* Description        : This function calls the L2IWF Module                 */
/*                                                                           */
/* Input(s)           : Context Id, Interface index, MstInstanec             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfIsPortInInst (UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4MstInst)
{
    return (L2IwfIsPortInInst (u4ContextId, u4IfIndex, i4MstInst));
}

/*****************************************************************************/
/* Function Name      : AstVlanApiIsPvidVlanOfAnyPorts                       */
/*                                                                           */
/* Description        : This Api is used to check whether the VLAN is having */
/*                      is having only untagged ports                        */
/*                                                                           */
/* Input(s)           : u2VlanId - VLAN Index                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_TRUE / AST_FALSE                               */
/*****************************************************************************/
INT4
AstVlanApiIsPvidVlanOfAnyPorts (tVlanId VlanId)
{
    if (VlanApiIsPvidVlanOfAnyPorts (VlanId) == VLAN_TRUE)
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}

/* End of File */
