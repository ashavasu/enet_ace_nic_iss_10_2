/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: astrtrap.c,v 1.38 2017/10/30 14:29:20 siva Exp $
 *
 * Description:This file contains routines for handling       
 *             different traps                            
 *
 *******************************************************************/

#include "asthdrs.h"
#include "snmctdfs.h"
#include "snmcport.h"
#ifdef SNMP_3_WANTED
#include "fsrst.h"
#endif
#include "fssnmp.h"

#ifdef SNMP_3_WANTED
static INT1         ai1TempBuffer[257];
#endif

/*****************************************************************************/
/* Function Name      : AstGlobalMemFailTrap                                 */
/*                                                                           */
/* Description        : This routine generates trap message when memory      */
/*                      allocation occurs                                    */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstGlobalMemFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen, UINT4 u4ContextId)
{
    tAstBrgErrTraps     AstBrgErrTraps;
    tMacAddr            BridgeAddr;

    gAstGlobalInfo.u1AstErrTrapType = AST_TRAP_MEM_FAIL;

    AST_LOCK ();
    AstSelectContext (u4ContextId);

    if (IS_AST_GLOBAL_BRG_ERR_EVNT ())
    {
        AstIssGetContextMacAddress (u4ContextId, BridgeAddr);

        AST_MEMCPY (&(AstBrgErrTraps.BrgAddr), BridgeAddr, AST_MAC_ADDR_SIZE);

        AstBrgErrTraps.u1ErrTrapType = AST_TRAP_MEM_FAIL;

#ifdef MSTP_WANTED
        if (AstIsMstEnabledInContext (u4ContextId))
        {
            MstSnmpIfSendTrap (AST_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                               u1TrapOidLen, (VOID *) &AstBrgErrTraps);

        }
#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
        if (AstIsPvrstEnabledInContext (u4ContextId))
        {
            PvrstSnmpIfSendTrap (AST_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                                 u1TrapOidLen, (VOID *) &AstBrgErrTraps);

        }
#endif /* PVRST_WANTED */

        if (AstIsRstEnabledInContext (u4ContextId))
        {
            RstSnmpIfSendTrap (AST_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                               u1TrapOidLen, (VOID *) &AstBrgErrTraps);

        }
    }

    AstReleaseContext ();
    AST_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : AstMemFailTrap                                       */
/*                                                                           */
/* Description        : This routine generates trap message when memory      */
/*                      allocation occurs                                    */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
AstMemFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    /* Assumptions: 
     * Protocol Lock is already taken.
     * Virtual Context already selected (AST_CURR_CONTEXT_INFO is valid).
     */
    tAstBrgErrTraps     AstBrgErrTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (AST_CURR_CONTEXT_INFO () == NULL)
    {
        /* Called during Task initialisation. Return here */
        return;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    gAstGlobalInfo.u1AstErrTrapType = AST_TRAP_MEM_FAIL;

    if (IS_AST_GLOBAL_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgErrTraps.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgErrTraps.u1ErrTrapType = AST_TRAP_MEM_FAIL;
        AstSnmpIfSendTrap (AST_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgErrTraps);
    }
}

/*****************************************************************************/
/* Function Name      : AstBufferFailTrap                                    */
/*                                                                           */
/* Description        : This routine generates trap message when buffer      */
/*                      allocation occurs                                    */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstBufferFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgErrTraps     AstBrgErrTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    gAstGlobalInfo.u1AstErrTrapType = AST_TRAP_BUF_FAIL;

    if (IS_AST_GLOBAL_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgErrTraps.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgErrTraps.u1ErrTrapType = AST_TRAP_BUF_FAIL;
        AstSnmpIfSendTrap (AST_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgErrTraps);
    }
}

/*****************************************************************************/
/* Function Name      : AstNewPortRoleTrap                                   */
/*                                                                           */
/* Description        : This routine generates trap message when new port    */
/*                      role is selected                                     */
/*                                                                           */
/* Input(s)           : u2PortNum      - Port Number                         */
/*                      u1SelectedRole - SelectedRole for the Port           */
/*                      u1PreviousRole - Previous Role of the Port           */
/*                      pi1TrapsOid    - OID of the associated Trap Object   */
/*                      u1TrapOidLen   - Length of the Trap OID              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstNewPortRoleTrap (UINT2 u2PortNum, UINT1 u1SelectedRole, UINT1 u1PreviousRole,
                    UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstNewPortRoleTrap AstPortRoleTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstPortRoleTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        AstPortRoleTrap.u2PortNum = u2PortNum;
        AstPortRoleTrap.u1SelectedRole = u1SelectedRole;
        AstPortRoleTrap.u1PreviousRole = u1PreviousRole;
        AstPortRoleTrap.u2MstInst = u2InstId;

        AstSnmpIfSendTrap (AST_NEW_PORTROLE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstPortRoleTrap);
    }
}

/*****************************************************************************/
/* Function Name      : AstNewRootTrap                                       */
/*                                                                           */
/* Description        : This routine generates trap message when new root    */
/*                      occurs                                               */
/*                                                                           */
/* Input(s)           : OldRootId    - Old designated Root                   */
/*                      RootId       - designated Root                       */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstNewRootTrap (tAstBridgeId OldRootId, tAstBridgeId RootId, UINT2 u2InstId,
                INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{

    tAstBrgNewRootTrap  AstBrgNewRootTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgNewRootTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        AstBrgNewRootTrap.AstRoot.u2BrgPriority =
            AST_HTONS (RootId.u2BrgPriority);
        AST_MEMCPY (&(AstBrgNewRootTrap.AstRoot.BridgeAddr),
                    &(RootId.BridgeAddr), AST_MAC_ADDR_SIZE);

        AstBrgNewRootTrap.AstOldRoot.u2BrgPriority =
            AST_HTONS (OldRootId.u2BrgPriority);
        AST_MEMCPY (&(AstBrgNewRootTrap.AstOldRoot.BridgeAddr),
                    &(OldRootId.BridgeAddr), AST_MAC_ADDR_SIZE);

        AstBrgNewRootTrap.u2MstInst = u2InstId;
        AstSnmpIfSendTrap (AST_NEW_ROOT_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgNewRootTrap);
    }
}

/*****************************************************************************/
/* Function Name      : AstTopologyChangeTrap                                */
/*                                                                           */
/* Description        : This routine generates trap message when topology    */
/*                      change  occurs                                       */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstTopologyChangeTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgTopologyChgTrap AstBrgTopologyChgTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgTopologyChgTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        AstBrgTopologyChgTrap.u2MstInst = u2InstId;
        AstSnmpIfSendTrap (AST_TOPOLOGY_CHG_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgTopologyChgTrap);
    }

}

/*****************************************************************************/
/* Function Name      : AstProtocolMigrationTrap                             */
/*                                                                           */
/* Description        : This routine generates trap message when protocol    */
/*                      migration occurs                                     */
/*                                                                           */
/* Input(s)           : u2PortNum -port number                               */
/*                      u1Version - curremt STP Version                      */
/*                      u1SendVersion - sending STP version                  */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstProtocolMigrationTrap (UINT2 u2PortNum, UINT1 u1Version, UINT1 u1SendVersion,
                          INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgProtocolMigrationTrap AstBrgProtocolMigrationTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();
    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgProtocolMigrationTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgProtocolMigrationTrap.u2Port = u2PortNum;
        AstBrgProtocolMigrationTrap.u1Version = u1Version;
        AstBrgProtocolMigrationTrap.u1GenTrapType = u1SendVersion;
        AstSnmpIfSendTrap (AST_PROTOCOL_MIGRATION_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgProtocolMigrationTrap);
    }
    (AST_GET_PORTENTRY (u2PortNum))->u1MigrationType = u1SendVersion;
}

/*****************************************************************************/
/* Function Name      : AstInvalidBpduRxdTrap                                */
/*                                                                           */
/* Description        : This routine generates trap message when invalid     */
/*                      Packet received occurs                               */
/*                                                                           */
/* Input(s)           : u2PortNum -port number                               */
/*                      u1ErrType - type of packet error                     */
/*                      u2ErrVal -  Error value corresponding to the type    */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstInvalidBpduRxdTrap (UINT2 u2PortNum, UINT1 u1ErrType, UINT2 u2ErrVal,
                       INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgInvalidBpduRxdTrap AstBrgInvalidBpduRxdTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgInvalidBpduRxdTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgInvalidBpduRxdTrap.u2Port = u2PortNum;
        AstBrgInvalidBpduRxdTrap.u1ErrTrapType = u1ErrType;
        AstBrgInvalidBpduRxdTrap.u2ErrVal = u2ErrVal;
        AstSnmpIfSendTrap (AST_INVALID_BPDU_RXD_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgInvalidBpduRxdTrap);
    }
    (AST_GET_PORTENTRY (u2PortNum))->u1PktErrType = u1ErrType;
    (AST_GET_PORTENTRY (u2PortNum))->u2PktErrValue = u2ErrVal;
}

/*****************************************************************************/
/* Function Name      : AstHwFailTrap                                        */
/*                                                                           */
/* Description        : This routine generates trap message when setting     */
/*                      port state in hardware failure occurs                */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstHwFailTrap (UINT2 u2PortNum, UINT2 u2InstId, UINT2 u2PortState,
               INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{

    tAstBrgHwFailTrap   AstBrgHwFailTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgHwFailTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgHwFailTrap.u2Port = u2PortNum;
        AstBrgHwFailTrap.u2MstInst = u2InstId;
        AstBrgHwFailTrap.u2PortState = u2PortState;
        AstSnmpIfSendTrap (AST_HW_FAIL_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgHwFailTrap);
    }
}

 /*****************************************************************************/
 /* Function Name      : AstLoopIncStateChangeTrap                            */
 /*                                                                           */
 /* Description        : This routine generates trap message when when the    */
 /*                     port has been moved to the loop inconsistent state.  */
 /*                                                                           */
 /* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
 /*                      u1TrapOidLen - Length of the Trap OID                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : None                                                 */
 /*****************************************************************************/
VOID
AstLoopIncStateChangeTrap (UINT2 u2PortNum, UINT4 u4LoopInconsistentState,
                           UINT2 u2InstId, INT1 *pi1TrapsOid,
                           UINT1 u1TrapOidLen)
{

    tAstBrgLoopIncStateChange AstBrgLoopIncStateChange;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgLoopIncStateChange.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgLoopIncStateChange.u2Port = u2PortNum;
        AstBrgLoopIncStateChange.u4LoopInconsistentState =
            u4LoopInconsistentState;
        AstBrgLoopIncStateChange.u2InstId = u2InstId;
        AstSnmpIfSendTrap (AST_LOOP_INCSTATE_CHANGE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgLoopIncStateChange);
    }
}

 /*****************************************************************************/
 /* Function Name      : AstBPDUIncStateChangeTrap                            */
 /*                                                                           */
 /* Description        : This routine generates trap message when when the    */
 /*                      port has been moved to the BPDU inconsistent state.  */
 /*                                                                           */
 /* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
 /*                      u1TrapOidLen - Length of the Trap OID                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : None                                                 */
 /*****************************************************************************/
VOID
AstBPDUIncStateChangeTrap (UINT2 u2PortNum, UINT4 BPDUInconsistentState,
                           UINT2 u2InstId, INT1 *pi1TrapsOid,
                           UINT1 u1TrapOidLen)
{

    tAstBrgBPDUIncStateChange AstBrgBPDUIncStateChange;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgBPDUIncStateChange.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgBPDUIncStateChange.u2Port = u2PortNum;
        AstBrgBPDUIncStateChange.u4BPDUInconsistentState =
            BPDUInconsistentState;
        AstBrgBPDUIncStateChange.u2InstId = u2InstId;
        AstSnmpIfSendTrap (AST_BPDU_INCSTATE_CHANGE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgBPDUIncStateChange);
    }
}

 /*****************************************************************************/
 /* Function Name      : AstRootIncStateChangeTrap                            */
 /*                                                                           */
 /* Description        : This routine generates trap message when when the    */
 /*                      port has been moved to the root inconsistent state.  */
 /*                                                                           */
 /* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
 /*                      u1TrapOidLen - Length of the Trap OID                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : None                                                 */
 /*****************************************************************************/
VOID
AstRootIncStateChangeTrap (UINT2 u2PortNum, UINT4 RootInconsistentState,
                           UINT2 u2InstId, INT1 *pi1TrapsOid,
                           UINT1 u1TrapOidLen)
{

    tAstBrgRootIncStateChange AstBrgRootIncStateChange;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgRootIncStateChange.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgRootIncStateChange.u2Port = u2PortNum;
        AstBrgRootIncStateChange.u4RootInconsistentState =
            RootInconsistentState;
        AstBrgRootIncStateChange.u2InstId = u2InstId;
        AstSnmpIfSendTrap (AST_ROOT_INCSTATE_CHANGE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgRootIncStateChange);
    }
}

 /*****************************************************************************/
 /* Function Name      : AstPortStateChangeTrap                               */
 /*                                                                           */
 /* Description        : This routine generates trap message when the         */
 /*                       current state of the port has been changed.         */
 /*                                                                           */
 /* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
 /*                      u1TrapOidLen - Length of the Trap OID                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : None                                                 */
 /*****************************************************************************/
VOID
AstPortStateChangeTrap (UINT2 u2PortNum, UINT2 PortState, UINT4 OldPortState,
                        UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{

    tAstBrgPortStateChange AstBrgPortStateChange;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(AstBrgPortStateChange.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AstBrgPortStateChange.u2Port = u2PortNum;
        AstBrgPortStateChange.u2PortState = PortState;
        AstBrgPortStateChange.u4OldPortState = OldPortState;
        AstBrgPortStateChange.u2InstId = u2InstId;
        AstSnmpIfSendTrap (AST_PORTSTATE_CHANGE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &AstBrgPortStateChange);
    }
}

/*****************************************************************************/

/******************************************************************************
* Function : AstSnmpIfSendTrap 
* Input    : u1TrapId - Trap Identifier
*           pi1TrapOid -pointer to Trap OID
*           u1OidLen - OID Length 
*           pTrapInfo - void pointer to the trap information 
* Output   : None
* Returns  : None
*******************************************************************************/

VOID
AstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo)
{

#ifdef MSTP_WANTED
    if ((AST_IS_MST_ENABLED ()) && (AST_MST_TRAP_TYPE != AST_TRAP_DISABLE))
    {
        MstSnmpIfSendTrap (u1TrapId, pi1TrapOid, u1OidLen, pTrapInfo);

    }
#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
    if ((AST_IS_PVRST_ENABLED ()) && (AST_TRAP_TYPE != AST_TRAP_DISABLE))
    {
        PvrstSnmpIfSendTrap (u1TrapId, pi1TrapOid, u1OidLen, pTrapInfo);

    }
#endif /* PVRST_WANTED */

    if ((AST_IS_RST_ENABLED ()) && (AST_TRAP_TYPE != AST_TRAP_DISABLE))
    {
        RstSnmpIfSendTrap (u1TrapId, pi1TrapOid, u1OidLen, pTrapInfo);

    }
}

/******************************************************************************
* Function : RstSnmpIfSendTrap 
* Input    : u1TrapId - Trap Identifier
*           pi1TrapOid -pointer to Trap OID
*           u1OidLen - OID Length 
*           pTrapInfo - void pointer to the trap information 
* Output   : None
* Returns  : None
*******************************************************************************/

VOID
RstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tAstBrgGenTraps    *pAstBrgGenTraps;
    tAstBrgErrTraps    *pAstBrgErrTraps;
    tAstBrgNewRootTrap *pAstBrgNewRootTrap;
    tAstBrgTopologyChgTrap *pAstBrgTopologyChgTrap;
    tAstBrgProtocolMigrationTrap *pAstBrgProtocolMigrationTrap;
    tAstBrgInvalidBpduRxdTrap *pAstBrgInvalidBpduRxdTrap;
    tAstNewPortRoleTrap *pAstNewPortRoleTrap = NULL;
    tAstBrgHwFailTrap  *pAstBrgHwFailTrap = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring, *pContextName = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[256];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4SysMode;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tAstBrgLoopIncStateChange *pAstBrgLoopIncStateChange = NULL;
    tAstBrgBPDUIncStateChange *pAstBrgBPDUIncStateChange = NULL;
    tAstBrgRootIncStateChange *pAstBrgRootIncStateChange = NULL;
    tAstBrgPortStateChange *pAstBrgPortStateChange = NULL;

    UNUSED_PARAM (u1OidLen);
    pEnterpriseOid = SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /* In MI except trap AST_BRG_ERR_TRAP_VAL the context-name is getting 
     * filled, since it is global for the entire module */

    switch (u1TrapId)
    {

        case AST_BRG_GEN_TRAP_VAL:

            pAstBrgGenTraps = (tAstBrgGenTraps *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_BRG_GEN_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgGenTraps->BrgAddr,
                                          (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_GEN_TRAP_TYPE);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u1GenTrapType,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_BRG_ERR_TRAP_VAL:

            pAstBrgErrTraps = (tAstBrgErrTraps *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_BRG_ERR_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgErrTraps->BrgAddr,
                                          AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_ERR_TRAP_TYPE);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgErrTraps->
                                      u1ErrTrapType, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_ROOT_TRAP_VAL:

            pAstBrgNewRootTrap = (tAstBrgNewRootTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_NEW_ROOT_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgNewRootTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_OLD_DESIG_ROOT);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstOldRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_DESIG_ROOT);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_PORTROLE_TRAP_VAL:

            pAstNewPortRoleTrap = (tAstNewPortRoleTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_NEW_PORTROLE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstNewPortRoleTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_PREVIOUS_ROLE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1PreviousRole, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_SELECTED_ROLE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1SelectedRole, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_TOPOLOGY_CHG_TRAP_VAL:

            pAstBrgTopologyChgTrap = (tAstBrgTopologyChgTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_TOPOLOGY_CHG_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgTopologyChgTrap->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_TRUE);

            break;

        case AST_PROTOCOL_MIGRATION_TRAP_VAL:

            pAstBrgProtocolMigrationTrap =
                (tAstBrgProtocolMigrationTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_PROTOCOL_MIGRATION_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgProtocolMigrationTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_STP_VERSION);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgProtocolMigrationTrap->
                                      u1Version, NULL, NULL, SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_MIGRATION_TYPE);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgProtocolMigrationTrap->
                                             u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgProtocolMigrationTrap->
                                      u1GenTrapType, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_INVALID_BPDU_RXD_TRAP_VAL:

            pAstBrgInvalidBpduRxdTrap = (tAstBrgInvalidBpduRxdTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_INVALID_BPDU_RXD_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgInvalidBpduRxdTrap->BrgAddr,
                                          AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_PKT_ERR_TYPE);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u1ErrTrapType, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_PKT_ERR_VAL);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u2ErrVal, NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;
        case AST_HW_FAIL_TRAP_VAL:

            pAstBrgHwFailTrap = (tAstBrgHwFailTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_HW_FAIL_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgHwFailTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_PORT_STATE);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgHwFailTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgHwFailTrap->
                                      u2PortState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;
        case AST_LOOP_INCSTATE_CHANGE_TRAP_VAL:

            pAstBrgLoopIncStateChange = (tAstBrgLoopIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_LOOP_INCSTATE_CHANGE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgLoopIncStateChange->
                                          BrgAddr, (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_LOOP_INCSTATE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgLoopIncStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgLoopIncStateChange->
                                      u4LoopInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_BPDU_INCSTATE_CHANGE_TRAP_VAL:

            pAstBrgBPDUIncStateChange = (tAstBrgBPDUIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_BPDU_INCSTATE_CHANGE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgBPDUIncStateChange->
                                          BrgAddr, (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BPDU_INCSTATE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgBPDUIncStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgBPDUIncStateChange->
                                      u4BPDUInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_ROOT_INCSTATE_CHANGE_TRAP_VAL:

            pAstBrgRootIncStateChange = (tAstBrgRootIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_ROOT_INCSTATE_CHANGE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgRootIncStateChange->
                                          BrgAddr, (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_ROOT_INCSTATE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgRootIncStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgRootIncStateChange->
                                      u4RootInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;
        case AST_PORTSTATE_CHANGE_TRAP_VAL:

            pAstBrgPortStateChange = (tAstBrgPortStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_PORTSTATE_CHANGE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgPortStateChange->
                                          BrgAddr, (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_PORT_STATE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u2PortState, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_OLD_PORTSTATE);

            pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u4OldPortState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;

        default:
            SNMP_FreeOid (pEnterpriseOid);
            return;
    }

    u4SysMode = AstVcmGetSystemModeExt (AST_PROTOCOL_ID);
    if (u4SysMode == VCM_MI_MODE)
    {
        /* In case of SI, the context name pContextName should be NULL. */
        AstVcmGetAliasName (AST_CURR_CONTEXT_ID (), au1ContextName);
        pContextName =
            SNMP_AGT_FormOctetString (au1ContextName, (VCM_ALIAS_MAX_LEN - 1));
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, pContextName);
    if (u4SysMode == VCM_MI_MODE)
    {
        SNMP_AGT_FreeOctetString (pContextName);
    }
#else /* SNMP_2_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_2_WANTED */

}

/******************************************************************************
* Function : RstMakeObjIdFromDotNew                                         *
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
RstMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
#ifdef SNMP_3_WANTED
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    UINT4               u4OidLen = 0;
    UINT4               u4Len = 0;

    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE)
    {
        porig_mib_oid_table = orig_mib_oid_table_SI;
        u4OidLen = sizeof (orig_mib_oid_table_SI);
    }
    else
    {
        porig_mib_oid_table = orig_mib_oid_table_MI;
        u4OidLen = sizeof (orig_mib_oid_table_MI);
    }

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
             ((u2Index <
               (u4OidLen / sizeof (struct MIB_OID)))
              && ((porig_mib_oid_table + u2Index)->pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 ((porig_mib_oid_table + u2Index)->pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN ((porig_mib_oid_table + u2Index)->pName)))
            {
                u4Len =
                    MEM_MAX_BYTES (STRLEN
                                   ((porig_mib_oid_table + u2Index)->pNumber),
                                   (sizeof (ai1TempBuffer) - 1));
                STRNCPY ((INT1 *) ai1TempBuffer,
                         (porig_mib_oid_table + u2Index)->pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }
        if (u2Index < (u4OidLen / sizeof (struct MIB_OID)))
        {
            if ((porig_mib_oid_table + u2Index)->pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 STRLEN ((INT1 *) pi1TextStr));
        ai1TempBuffer[STRLEN ((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         (u2Index < 257 && ai1TempBuffer[u2Index] != '\0'); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }

    MEMSET (pOidPtr->pu4_OidList, 0, SNMP_MAX_OID_LENGTH);
    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; ((u2Index < SNMP_MAX_OID_LENGTH) &&
                       (u2Index < u2DotCount + 1)); u2Index++)
    {
        if ((pi1TempPtr = RstParseSubIdNew
             (pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
#else
    UNUSED_PARAM (pi1TextStr);
#endif
    return (pOidPtr);
}

/******************************************************************************
* Function : RstParseSubIdNew
* Input    : ppu1TempPtr
* Output   : value of ppu1TempPtr
* Returns  : RST_SUCCESS or RST_FAILURE
*******************************************************************************/

INT1               *
RstParseSubIdNew (INT1 *pi1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    INT1               *pi1Tmp;

    for (pi1Tmp = pi1TempPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                               ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                               ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pi1Tmp & 0xf);
    }

    pi1TempPtr = pi1Tmp;
    *pu4Value = u4Value;
    return pi1TempPtr;
}

/*****************************************************************************/
/* Function Name      : AstRstInstTrap                                       */
/*                                                                           */
/* Description        : This routine generates trap message when an RSTP     */
/*                      instance is enabled / disabled                       */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of associated Trap Object.         */
/*                      u1TrapOidLen - Length of the Trap OID.               */
/*                      u1Status - Enable/Disable                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstRstInstTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen, UINT1 u1Status)
{
    tAstBrgGenTraps     astBrgGenTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType = u1Status;
    if (IS_AST_BRG_GEN_EVNT ())
    {
        AST_MEMCPY (&(astBrgGenTraps.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        astBrgGenTraps.u1GenTrapType = u1Status;

        AstSnmpIfSendTrap (AST_BRG_GEN_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &astBrgGenTraps);
    }
}

/*************************** END OF FILE(trap.c) ***********************/
