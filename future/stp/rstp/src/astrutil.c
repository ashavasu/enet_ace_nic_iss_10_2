/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrutil.c,v 1.99 2017/12/29 09:31:23 siva Exp $
 *
 * Description: This file contains all utility routines used by the
 *              RSTP Module.
 *
 *******************************************************************/

#include "asthdrs.h"
#include "fsvlan.h"
#ifdef MSTP_WANTED
#include "astmtrap.h"
#endif
#define AST_TRC_BUF_SIZE    500
/*****************************************************************************/
/* Function Name      : RstCompareBrgId                                      */
/*                                                                           */
/* Description        : Compares the Bridge Identifiers 1 and 2 and returns  */
/*                      the which identifier is better.                      */
/*                                                                           */
/* Input(s)           : pBrgId1 - Pointer to Bridge Identifier 1.            */
/*                      pBrgId2 - Pointer to Bridge Identifier 2.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_BRGID1_SUPERIOR (or)                             */
/*                      RST_BRGID1_INFERIOR (or)                             */
/*                      RST_BRGID1_SAME.                                     */
/*****************************************************************************/
INT4
RstCompareBrgId (tAstBridgeId * pBrgId1, tAstBridgeId * pBrgId2)
{
    INT4                i4RetVal = 0;

    if (pBrgId1->u2BrgPriority < pBrgId2->u2BrgPriority)
    {
        return RST_BRGID1_SUPERIOR;
    }
    else if (pBrgId1->u2BrgPriority > pBrgId2->u2BrgPriority)
    {
        return RST_BRGID1_INFERIOR;
    }

    /* Now Compare the Bridge Address to find Superior or Inferior */
    i4RetVal = AST_MEMCMP (&(pBrgId1->BridgeAddr), &(pBrgId2->BridgeAddr),
                           AST_MAC_ADDR_LEN);

    if (i4RetVal < 0)
    {
        return RST_BRGID1_SUPERIOR;
    }
    else if (i4RetVal > 0)
    {
        return RST_BRGID1_INFERIOR;
    }
    else
    {
        return RST_BRGID1_SAME;
    }
}

/*****************************************************************************/
/* Function Name      : RstGetBridgeAddr                                     */
/*                                                                           */
/* Description        : This routine is invoked from CLI to get the          */
/*                      MAC address of this Switch.                          */
/*                                                                           */
/* Input(s)           : pBrgAddr - Pointer to the Bridge MAC Address         */
/*                                 variable through which the address is to  */
/*                                 be returned.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS (or) RST_FAILURE                         */
/*****************************************************************************/
INT4
RstGetBridgeAddr (tAstMacAddr * pBrgAddr)
{
    AST_MEMCPY (pBrgAddr, (AST_GET_BRGENTRY ())->BridgeAddr, AST_MAC_ADDR_LEN);
    return RST_SUCCESS;
}

/******************************************************************************/
/* Function Name    : AstMapSpeedToPathcost                                   */
/*                                                                            */
/* Description      : This function returns the path cost that should be      */
/*                    assigned to the Port depending on the Port speed.       */
/*                                                                            */
/* Input(s)         : u4Speed - Contains Port speed if it is less than 10Gbps */
/*                  : u4HighSpeed - Contains Port Speed if it is greater than */
/*                                  or equal to 10 Gbps set in 1000s of Gbps  */
/* Output(s)        : None                                                    */
/*                                                                            */
/* Return Value(s)  : UINT4 pathcost                                          */
/*                                                                            */
/*****************************************************************************/
UINT4
AstMapSpeedToPathcost (UINT4 u4Speed, UINT4 u4HighSpeed)
{
    FLT4                f4Val = 0;

    /* The relationship between port pathcost and port speed is
     *
     *   PathCost = 20000000000 / PortSpeed 
     *
     */

    /* Pathcost for speeds <= 100 kbps is the same */
    if (u4Speed < AST_PORT_SPEED_100KBPS)
    {
        u4Speed = AST_PORT_SPEED_100KBPS;
    }
    /* For Speed 100K - 4G */
    if ((u4Speed / AST_PORT_SPEED_1KBPS) <= AST_PORT_SPEED_4MBPS)
    {
        u4Speed = u4Speed / 1000;    /* Dividing speed by 1000 so that 
                                     * it does not exceed 4000000 */
        f4Val = ((FLT4) 4000000 / u4Speed) * 5000;
    }

    /* For Speed 4G - 32-bit-max */
    else if (((u4Speed / AST_PORT_SPEED_1KBPS) > AST_PORT_SPEED_4MBPS) &&
             (u4Speed < AST_32BIT_MAX))
    {
        u4Speed = u4Speed / 1000000;    /* Dividing speed by 1000000 so that 
                                         * it does not exceed 20000000 */
        f4Val = (FLT4) 20000000 / u4Speed;
    }
    /* For Speed 32-bit-max and above */
    else if (u4Speed >= AST_32BIT_MAX)
    {
        /* u4HighSpeed stores the speed in units of Mbps.
         * For e.g. for 10GBPS, u4HighSpeed has a value of 10000 (Mbps)
         */
        if (u4HighSpeed != 0)
        {
            f4Val = (FLT4) 20000000 / u4HighSpeed;
        }
    }

    return ((UINT4) f4Val);
}

/******************************************************************************/
/* Function Name    : AstCalculatePathcost                                    */
/*                                                                            */
/* Description      : This function returns the path cost that should be      */
/*                    assigned to the Port                                    */
/*                                                                            */
/* Input(s)         : u2PortNum - Port number                                 */
/*                                                                            */
/* Output(s)        : None                                                    */
/*                                                                            */
/* Return Value(s)  : u4PathCost - pathcost calculated dynamically from speed */
/*                                                                            */
/*****************************************************************************/
UINT4
AstCalculatePathcost (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT4               u4PathCost = AST_PORT_PATHCOST_100MBPS;
    UINT2               u2NumConfiguredPorts = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
    tAstCfaIfInfo       CfaIfInfo;

    AST_MEMSET (au2ConfPorts, AST_MEMSET_VAL, L2IWF_MAX_PORTS_PER_CONTEXT);

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pPortInfo), &CfaIfInfo)
        != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SNMP: Cannot Get CFA Information for Port %s\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return u4PathCost;
    }
    u4PathCost = AstMapSpeedToPathcost (CfaIfInfo.u4IfSpeed,
                                        CfaIfInfo.u4IfHighSpeed);

    /* When dynamic pathcost calculation is disabled, for a lag interface 
     * pathcost is calculated based on the number of configured ports
     * in port-channel.To get the total speed, speed of one port in 
     * port-channel is fetched from CFA and it is multiplied by total 
     * number of ports in port-channel. This speed is used to calculate 
     * the pathcost */

    if ((CfaIfInfo.u1IfType == CFA_LAGG) &&
        ((AST_CURR_CONTEXT_INFO ())->BridgeEntry.u1DynamicPathcostCalcLagg
         == AST_SNMP_FALSE))
    {
        if (AstL2IwfGetConfiguredPortsForPortChannel
            ((UINT2) AST_GET_IFINDEX (u2PortNum), au2ConfPorts,
             &u2NumConfiguredPorts) == L2IWF_FAILURE)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: Cannot Get Configured port list for port %s\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return u4PathCost;
        }
        if (AstCfaGetIfInfo (au2ConfPorts[0], &CfaIfInfo) != CFA_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Cannot Get CFA Information for Port %s\n",
                          u2PortNum);
            return u4PathCost;
        }
        if (CfaIfInfo.u4IfSpeed != AST_32BIT_MAX)
        {
            u4IfSpeed = CfaIfInfo.u4IfSpeed * u2NumConfiguredPorts;
        }
        else
        {
            u4IfSpeed = CfaIfInfo.u4IfSpeed;
        }
        u4IfHighSpeed = CfaIfInfo.u4IfHighSpeed * u2NumConfiguredPorts;
        u4PathCost = AstMapSpeedToPathcost (u4IfSpeed, u4IfHighSpeed);
    }
    else
    {
        u4PathCost = AstMapSpeedToPathcost (CfaIfInfo.u4IfSpeed,
                                            CfaIfInfo.u4IfHighSpeed);
    }
    return (u4PathCost);
}

#ifdef RSTP_DEBUG
/*****************************************************************************/
/* Function Name      : RstDumpGlobalBrgInfo                                 */
/*                                                                           */
/* Description        : Prints the Global Bridge Information Structure.      */
/*                      This routine is used for Debuging.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstDumpGlobalBrgInfo (VOID)
{
    tAstBridgeEntry    *pGlobBrgInfo = NULL;
    tAstTimes          *pTimes = NULL;

    pGlobBrgInfo = AST_GET_BRGENTRY ();
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping Global Bridge Information...\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pGlobBrgInfo->BridgeAddr[0], pGlobBrgInfo->BridgeAddr[1],
               pGlobBrgInfo->BridgeAddr[2], pGlobBrgInfo->BridgeAddr[3],
               pGlobBrgInfo->BridgeAddr[4], pGlobBrgInfo->BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping GlobalBrgInfo's RootTimes ...\n");
    pTimes = &(pGlobBrgInfo->RootTimes);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2MaxAge: 0x%x, u2HelloTime: 0x%x,\n"
               "u2ForwardDelay: 0x%x, u2MsgAgeOrHopCount: 0x%x\n",
               pTimes->u2MaxAge, pTimes->u2HelloTime,
               pTimes->u2ForwardDelay, pTimes->u2MsgAgeOrHopCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping GlobalBrgInfo's BridgeTimes ...\n");
    pTimes = &(pGlobBrgInfo->BridgeTimes);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2MaxAge: 0x%x, u2HelloTime: 0x%x, \n"
               "u2ForwardDelay: 0x%x, u2MsgAgeOrHopCount: 0x%x\n",
               pTimes->u2MaxAge, pTimes->u2HelloTime,
               pTimes->u2ForwardDelay, pTimes->u2MsgAgeOrHopCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4AstpUpCount: 0x%x, u4AstpDownCount: 0x%x, u1MigrateTime: 0x%x\n",
               pGlobBrgInfo->u4AstpUpCount,
               pGlobBrgInfo->u4AstpDownCount, pGlobBrgInfo->u1MigrateTime);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1TxHoldCount: 0x%x\n", pGlobBrgInfo->u1TxHoldCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "End of Dump Global Port Information\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    return;
}

/*****************************************************************************/
/* Function Name      : RstDumpPerStBrgInfo                                  */
/*                                                                           */
/* Description        : Prints the Instance specific Bridge Information      */
/*                      Structure contents.                                  */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance id of bridge information.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstDumpPerStBrgInfo (UINT2 u2InstanceId)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        return;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping PerStBrgInfo for instance: %u ...\n", u2InstanceId);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "RootId.u2Priority: 0x%x \n",
               pPerStBrgInfo->RootId.u2BrgPriority);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "RootId.BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pPerStBrgInfo->RootId.BridgeAddr[0],
               pPerStBrgInfo->RootId.BridgeAddr[1],
               pPerStBrgInfo->RootId.BridgeAddr[2],
               pPerStBrgInfo->RootId.BridgeAddr[3],
               pPerStBrgInfo->RootId.BridgeAddr[4],
               pPerStBrgInfo->RootId.BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4RootCost: 0x%x, u4CistInternalRootCost: 0x%x, u4NewRootIdCount: 0x%x\n",
               pPerStBrgInfo->u4RootCost,
               pPerStBrgInfo->u4CistInternalRootCost,
               pPerStBrgInfo->u4NewRootIdCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4TimeSinceTopoCh: 0x%x, u4NumTopoCh: 0x%x\n",
               pPerStBrgInfo->u4TimeSinceTopoCh, pPerStBrgInfo->u4NumTopoCh);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2BrgPriority: 0x%x, u2RootPort: 0x%x, u1ProleSelSmState: %u\n",
               pPerStBrgInfo->u2BrgPriority,
               pPerStBrgInfo->u2RootPort, pPerStBrgInfo->u1ProleSelSmState);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "End of PerStBrgInfo Dump\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    return;
}

/*****************************************************************************/
/* Function Name      : RstDumpGlobalPortInfo                                */
/*                                                                           */
/* Description        : Prints the Global Port Information Structure         */
/*                      contents.                                            */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstDumpGlobalPortInfo (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstTimes          *pTimes = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pPortInfo == NULL)
    {
        return;
    }
    pCommPortInfo = &(pPortInfo->CommPortInfo);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping Global Port Information for IfIndex: %s LocalPortNum: %u...\n",
               AST_GET_IFINDEX_STR (u2PortNum), u2PortNum);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping Global CommPortInfo...\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "pMdWhileTmr: 0x%x, pHelloWhenTmr: 0x%x, pHoldTmr: 0x%x\n",
               pCommPortInfo->pMdWhileTmr, pCommPortInfo->pHelloWhenTmr,
               pCommPortInfo->pHoldTmr);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bNewInfo: %u, bRcvdBpdu: %u, bInitPm: %u, bMCheck: %u, bSendRstp: %u\n",
               pCommPortInfo->bNewInfo, pCommPortInfo->bRcvdBpdu,
               pCommPortInfo->bInitPm, pCommPortInfo->bMCheck,
               pCommPortInfo->bSendRstp);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bRcvdRstp: %u, bRcvdStp: %u, bRcvdTcn: %u, bRcvdTcAck: %u, bTcAck: %u\n",
               pCommPortInfo->bRcvdRstp, pCommPortInfo->bRcvdStp,
               pCommPortInfo->bRcvdTcn, pCommPortInfo->bRcvdTcAck,
               pCommPortInfo->bTcAck);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping RstPortInfo's PortTimes ...\n");
    pTimes = &(pPortInfo->PortTimes);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2MaxAge: 0x%x, u2HelloTime: 0x%x, u2ForwardDelay: 0x%x, u2MsgAgeOrHopCount: 0x%x\n",
               pTimes->u2MaxAge, pTimes->u2HelloTime,
               pTimes->u2ForwardDelay, pTimes->u2MsgAgeOrHopCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping RstPortInfo's DesgTimes ...\n");
    pTimes = &(pPortInfo->DesgTimes);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2MaxAge: 0x%x, u2HelloTime: 0x%x, u2ForwardDelay: 0x%x, u2MsgAgeOrHopCount: 0x%x\n",
               pTimes->u2MaxAge, pTimes->u2HelloTime,
               pTimes->u2ForwardDelay, pTimes->u2MsgAgeOrHopCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PortTxSmState: %u, u1PmigSmState: %u, u1TxCount: %u\n",
               pCommPortInfo->u1PortTxSmState, pCommPortInfo->u1PmigSmState,
               pCommPortInfo->u1TxCount);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping Global PortInfo...\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bOperEdgePort: %u, bAdminEdgePort: %u, bOperPointToPoint: %u, u4PathCost: 0x%x\n",
               pPortInfo->bOperEdgePort, pPortInfo->bAdminEdgePort,
               pPortInfo->bOperPointToPoint, pPortInfo->u4PathCost);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               " u4NumRstBpdusRxd: 0x%x, u4NumConfigBpdusRxd: 0x%x, u4NumTcnBpdusRxd: 0x%x\n",
               pPortInfo->u4NumRstBpdusRxd, pPortInfo->u4NumConfigBpdusRxd,
               pPortInfo->u4NumTcnBpdusRxd);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               " u4NumRstBpdusTxd: 0x%x, u4NumConfigBpdusTxd: 0x%x, u4NumTcnBpdusTxd: 0x%x\n",
               pPortInfo->u4NumRstBpdusTxd, pPortInfo->u4NumConfigBpdusTxd,
               pPortInfo->u4NumTcnBpdusTxd);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               " u2PortNo: 0x%x, u1AdminPointToPoint: %u, u1EntryStatus: %u\n",
               pPortInfo->u2PortNo, pPortInfo->u1AdminPointToPoint,
               pPortInfo->u1EntryStatus);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "End of Dump Global Port Information\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    return;
}

/*****************************************************************************/
/* Function Name      : RstDumpRstPortInfo                                   */
/*                                                                           */
/* Description        : Prints the instance specific RSTP Port Information   */
/*                      Structure contents.                                  */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      u2InstanceId - Instance id of the spanning tree.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstDumpRstPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        return;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pPerStPortInfo == NULL)
    {
        return;
    }

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping RstPortInfo for Port: %u, Instance: %u...\n",
               u2PortNum, u2InstanceId);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "pFdWhileTmr: 0x%x, pTcWhileTmr: 0x%x, pRbWhileTmr: 0x%x, pRrWhileTmr: 0x%x\n",
               pRstPortInfo->pFdWhileTmr, pRstPortInfo->pTcWhileTmr,
               pRstPortInfo->pRbWhileTmr, pRstPortInfo->pRrWhileTmr);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping RstPortInfo's SEM variables...\n");
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bUpdtInfo: %u, bProposing: %u, bProposed: %u, bAgreed: %u\n",
               pRstPortInfo->bUpdtInfo, pRstPortInfo->bProposing,
               pRstPortInfo->bProposed, pRstPortInfo->bAgreed);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bSync: %u, bSynced: %u, bReRoot: %u, bReSelect: %u, bSelected: %u\n",
               pRstPortInfo->bSync, pRstPortInfo->bSynced,
               pRstPortInfo->bReRoot, pRstPortInfo->bReSelect,
               pRstPortInfo->bSelected);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bRcvdTc: %u, bTc: %u, bTcProp: %u, bLearn: %u, bForward: %u\n",
               pRstPortInfo->bRcvdTc, pRstPortInfo->bTc,
               pRstPortInfo->bTcProp, pRstPortInfo->bLearn,
               pRstPortInfo->bForward);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "bLearning: %u, bForwarding: %u, u1RcvdInfo: %u, u1InfoIs: %u, bPortEnabled: %u\n",
               pRstPortInfo->bLearning, pRstPortInfo->bForwarding,
               pRstPortInfo->u1RcvdInfo, pRstPortInfo->u1InfoIs,
               pRstPortInfo->bPortEnabled);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    return;

}

/*****************************************************************************/
/* Function Name      : RstDumpPerStPortInfo                                 */
/*                                                                           */
/* Description        : Prints the instance specific Port Information        */
/*                      Structure contents.                                  */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      u2InstanceId - Instance id of the spanning tree.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstDumpPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId)
{
    UINT2               u2Val1 = 0;
    UINT2               u2Val2 = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeId       *pBrgId = NULL;
    UINT1               aau1PortRole[5][20] = {
        "DISABLED", "ALTERNATE", "BACKUP",
        "ROOT_PORT", "DESIGNATED_PORT"
    };
    UINT1               aau1PinfoSmState[RST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "AGREEMENT",
        "CURRENT", "RECEIVE"
    };
    UINT1               aau1ProleTrSmState[RST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "BLOCK_PORT",
        "BLOCKED_PORT", "ROOT_PORT", "DESG_PORT"
    };
    UINT1               aau1PstateTrSmState[RST_PSTATETRSM_MAX_STATES][20] = {
        "DISCARDING", "LEARNING", "FORWARDING"
    };
    UINT1               aau1TopoChSmState[RST_TOPOCHSM_MAX_STATES][20] = {
        "INIT", "INACTIVE", "ACTIVE", "DETECTED",
        "NOTIFIED_TCN", "NOTIFIED_TC", "PROPAGATING", "ACKNOWLEDGED"
    };
    tAstPerStInfo      *pPerStInfo = NULL;

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        return;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pPerStPortInfo == NULL)
    {
        return;
    }

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping PerStPortInfo for Port: %u, Instance: %u...\n",
               u2PortNum, u2InstanceId);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME, "DesgBrgId...\n");
    pBrgId = &(pPerStPortInfo->DesgBrgId);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2BrgPriority: 0x%x ", pBrgId->u2BrgPriority);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pBrgId->BridgeAddr[0],
               pBrgId->BridgeAddr[1], pBrgId->BridgeAddr[2],
               pBrgId->BridgeAddr[3], pBrgId->BridgeAddr[4],
               pBrgId->BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME, "RootId...\n");
    pBrgId = &(pPerStPortInfo->RootId);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2BrgPriority: 0x%x ", pBrgId->u2BrgPriority);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pBrgId->BridgeAddr[0],
               pBrgId->BridgeAddr[1], pBrgId->BridgeAddr[2],
               pBrgId->BridgeAddr[3], pBrgId->BridgeAddr[4],
               pBrgId->BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4NumFwdTransitions: 0x%x, u4NumBpdusRx: 0x%x, u4NumBpdusTx: 0x%x\n",
               pPerStPortInfo->u4NumFwdTransitions,
               pPerStPortInfo->u4NumBpdusRx, pPerStPortInfo->u4NumBpdusTx);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4RootCost: 0x%x, u2DesgPortId: 0x%x, u2PortNo: 0x%x, u1PortPriority: 0x%2x\n",
               pPerStPortInfo->u4RootCost, pPerStPortInfo->u2DesgPortId,
               pPerStPortInfo->u2PortNo, pPerStPortInfo->u1PortPriority);

    u2Val1 = pPerStPortInfo->u1PortRole;
    u2Val2 = pPerStPortInfo->u1SelectedPortRole;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PortRole: %s, u1SelectedPortRole: %s\n",
               aau1PortRole[u2Val1], aau1PortRole[u2Val2]);

    u2Val1 = pPerStPortInfo->u1PinfoSmState;
    u2Val2 = pPerStPortInfo->u1ProleTrSmState;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PinfoSmState: %s, u1ProleTrSmState: %s \n",
               aau1PinfoSmState[u2Val1], aau1ProleTrSmState[u2Val2]);

    u2Val1 = pPerStPortInfo->u1PstateTrSmState;
    u2Val2 = pPerStPortInfo->u1TopoChSmState;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PstateTrSmState: %s, u1TopoChSmState: %s \n",
               aau1PstateTrSmState[u2Val1], aau1TopoChSmState[u2Val2]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");
    return;
}

/*****************************************************************************/
/* Function Name      : RstBufDump                                           */
/*                                                                           */
/* Description        : Prints Buffer Header Information.                    */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : pBuf - CRU Buffer pointer.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstBufDump (tAstBufChainHeader * pBuf)
{
    tCRU_BUF_DATA_DESC *pFirstDataDesc = NULL;

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "pNextChain: %p, pFirstDataDesc: %p, pLastDataDesc: %p\n",
               pBuf->pNextChain, pBuf->pFirstDataDesc, pBuf->pLastDataDesc);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "pFirstValidDataDesc: %p \n", pBuf->pFirstValidDataDesc);

    pFirstDataDesc = pBuf->pFirstDataDesc;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "pNext: %p, pPrev: %p, pu1_FirstByte: %p, pu1_FirstValidByte: %p\n",
               pFirstDataDesc->pNext, pFirstDataDesc->pPrev,
               pFirstDataDesc->pu1_FirstByte,
               pFirstDataDesc->pu1_FirstValidByte);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               " u4_ValidByteCount: %u, u4_FreeByteCount: %u, u2_UsageCount: %u\n",
               pFirstDataDesc->u4_ValidByteCount,
               pFirstDataDesc->u4_FreeByteCount, pFirstDataDesc->u2_UsageCount);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               " u2_QueId: %u\n", pFirstDataDesc->u2_QueId);

    return;
}
#endif /* RSTP_DEBUG */

/*****************************************************************************/
/* Function Name      : RstPktDump                                           */
/*                                                                           */
/* Description        : Prints Packet contents.                              */
/*                      This routine is used for Trace.                      */
/*                                                                           */
/* Input(s)           : pBuf - CRU Buffer pointer.                           */
/*                      u2Length - Length of valid data.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RstPktDump (tAstBufChainHeader * pMsg, UINT2 u2Length)
{
    tAstFrameSize      *pBuf = NULL;
    UINT2               u2Cnt = 0;

    if (u2Length > AST_MAX_ETH_FRAME_SIZE)
    {
        return;
    }
    if (AST_ALLOC_ETHFRAME_SIZE_MEM_BLOCK (pBuf) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return;
    }
    MEMSET (pBuf->au1Buf, 0, AST_MAX_ETH_FRAME_SIZE);

    AST_COPY_FROM_CRU_BUF (pMsg, &(pBuf->au1Buf[0]), 0, u2Length);

    for (u2Cnt = 1; u2Cnt <= u2Length; u2Cnt++)
    {
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, " ", "%2x",
                   pBuf->au1Buf[u2Cnt - 1]);
        if ((u2Cnt % 8) == 0)
        {
            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, " ", "\n");
        }
    }
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, " ", "\n");
    AST_RELEASE_ETHFRAME_SIZE_MEM_BLOCK (pBuf);
    return;
}

/*****************************************************************************/
/* Function Name      : AstIncrInvalidBpduCounter                            */
/*                                                                           */
/* Description        : This Function Increments the Invalid Bpdus counter   */
/*                      based on the type of Bpdus received.                 */
/*                                                                           */
/* Input(s)           : u2ortNum   - Port Number for which the counters      */
/*                                   needs to be incremented.                */
/*                      u1BpduType - Bpdu Type                               */
/*                      u1Version  - Bpdu Version                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
AstIncrInvalidBpduCounter (UINT2 u2PortNum, UINT1 u1BpduType, UINT1 u1Version)
{

    switch (u1BpduType)
    {
        case AST_BPDU_TYPE_CONFIG:
            /* Incrementing the Invalid Config BPDU Received Count */
            AST_INCR_INVALID_CONFIG_BPDU_RXD_COUNT (u2PortNum);
            break;

        case AST_BPDU_TYPE_TCN:
            /* Incrementing the Invalid TCN BPDU Received Count */
            AST_INCR_INVALID_TCN_BPDU_RXD_COUNT (u2PortNum);
            break;

        case AST_BPDU_TYPE_RST:

            /* Fall through */

        default:

#ifdef MSTP_WANTED
            if (AST_IS_MST_ENABLED ())
            {
                if (AST_FORCE_VERSION <= AST_VERSION_2)
                {
                    /* All Bpdus, irrespective of version, to be considered 
                     * as RST bpdus */
                    AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                }
                else
                {
                    if (u1Version == AST_VERSION_2)
                    {
                        /* Incrementing the Invalid RST BPDU Received 
                         * Count */
                        AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
                    }
                    else
                    {
                        /* Incrementing the Invalid MST BPDU Received 
                         * Count*/
                        AST_INCR_INVALID_MST_BPDU_COUNT
                            (RST_DEFAULT_INSTANCE, u2PortNum);
                    }
                }
            }
#else

            AST_UNUSED (u1Version);

#endif /*MSTP_WANTED */
            if (AST_IS_RST_ENABLED () || AST_IS_PVRST_ENABLED ())
            {
                /* All Bpdus, irrespective of version, to be considered 
                 * as RST bpdus */
                AST_INCR_INVALID_RST_BPDU_RXD_COUNT (u2PortNum);
            }

            break;

    }
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name        : RstDisableStpInHw                                  */
/*                                                                           */
/* Description          : This function programs the port state in hardware  */
/*                        when rstp/mstp is disabled.                        */
/*                        The Port that are operationally up is programmed   */
/*                        as forwarding and the ports that are operationally */
/*                        is programmed as discarding.                       */
/*                                                                           */
/* Input(s)             : None                                               */
/*                                                                           */
/* Output(s)            : None                                               */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : RST_SUCCESS - On success                           */
/*                        RST_FAILURE - On failure. */
/*****************************************************************************/

INT4
RstDisableStpInHw (VOID)
{
    UINT2               u2IfIndex = 1;
    UINT1               u1OperStatus;
    tAstPortEntry      *pPortInfo = NULL;

    AST_GET_NEXT_PORT_ENTRY (u2IfIndex, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }

        AstGetPortOperStatusFromL2Iwf (STP_MODULE, u2IfIndex, &u1OperStatus);

        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            if (u1OperStatus == CFA_IF_UP)
            {

                AstMiRstpNpSetPortState (u2IfIndex, AST_PORT_STATE_FORWARDING);
            }
            else
            {
                AstMiRstpNpSetPortState (u2IfIndex, AST_PORT_STATE_DISCARDING);

            }
        }
    }
    return RST_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : AstPathcostConfiguredFlag                            */
/*                                                                           */
/* Description        :  indicates whether the pathcost has been configured  */
/*                       on this port for this instance using AdminPathCost  */
/*                                                                           */
/* Input(s)           : u2LocalPortId - Local Context based port number      */
/*                      u2InstanceId  - MST Instance ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstPathcostConfiguredFlag (UINT2 u2LocalPortId, UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);

    if ((pAstPortEntry != NULL)
        && (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT))
    {
        if (AstPbSelectCvlanContext (pAstPortEntry) == RST_SUCCESS)
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         u2InstanceId);

            if (pPerStPortInfo == NULL)
            {
                AstPbRestoreContext ();
                return RST_FALSE;
            }

            if (pPerStPortInfo->u4PortAdminPathCost == 0)
            {
                AstPbRestoreContext ();
                return RST_FALSE;
            }
        }
        else
        {
            return RST_FALSE;
        }
        AstPbRestoreContext ();
    }
    else
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstanceId);

        if (pPerStPortInfo == NULL)
        {
            return RST_FALSE;
        }

        if (pPerStPortInfo->u4PortAdminPathCost == 0)
        {
            return RST_FALSE;
        }
    }

    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstCreateSpanningTreeInst                            */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      PerStInfo and array for PerStPortInfo                */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be memory allocated       */
/*                                                                           */
/* Output(s)          : ppPerStInfo - Allocated Memory Block pointer for stInfo*/
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
AstCreateSpanningTreeInst (UINT2 u2MstInst, tAstPerStInfo ** ppPerStInfo)
{

    /* Creating PerStInfo for Default instance */
    if ((AST_ALLOC_PERST_INFO_MEM_BLOCK (*ppPerStInfo)) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for Spanning Tree Information "
                 "failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for Spanning Tree Information "
                 "failed !!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (*ppPerStInfo, AST_INIT_VAL, sizeof (tAstPerStInfo));
    KW_FALSEPOSITIVE_FIX (*ppPerStInfo);
    AST_GET_PERST_INFO (u2MstInst) = *ppPerStInfo;

    /* For CVLAN component, there is a separate mempool for the Perst Port table
     * and PerSt Info blocks. For SVLAN component, there is a separate mempool
     * for in the Global Mempools. To differentiate the allocation, the
     * component type maintained in the context is used.*/
    if (AST_COMP_TYPE () == AST_PB_C_VLAN)
    {
        if (AST_ALLOC_PERST_PORT_TBL_BLOCK
            (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID,
             (AST_GET_PERST_INFO (u2MstInst)->ppPerStPortInfo)) == NULL)
        {
            AST_RELEASE_PERST_INFO_MEM_BLOCK (*ppPerStInfo);
            AST_GET_PERST_INFO (u2MstInst) = NULL;

            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: C-VLAN component with CEP: %d : Memory Allocation "
                          "for Spanning Tree Information failed !!!\n",
                          (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);

            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: C-VLAN component with CEP: %d : Memory Allocation "
                          "for Spanning Tree Information failed !!!\n",
                          (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);

            return RST_FAILURE;
        }

        AST_MEMSET (AST_PERST_PORT_INFO_TBL (u2MstInst), AST_INIT_VAL,
                    (AST_MAX_NUM_PORTS * sizeof (tAstPerStPortInfo *)));
    }
    else
    {
        if (AST_ALLOC_PERST_PORT_TBL_BLOCK (AST_PERST_PORT_TBL_MEMPOOL_ID,
                                            (AST_GET_PERST_INFO (u2MstInst)->
                                             ppPerStPortInfo)) == NULL)
        {
            AST_RELEASE_PERST_INFO_MEM_BLOCK (*ppPerStInfo);
            AST_GET_PERST_INFO (u2MstInst) = NULL;

            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory Allocation for Spanning Tree Information "
                     "failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory Allocation for Spanning Tree Information "
                     "failed !!!\n");

            return RST_FAILURE;
        }

        AST_MEMSET (AST_PERST_PORT_INFO_TBL (u2MstInst), AST_INIT_VAL,
                    (AST_MAX_NUM_PORTS * sizeof (tAstPerStPortInfo *)));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeleteMstInstance                                 */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                      PerStInfo and array of PerStPortInfo                 */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance Id to be memory allocated       */
/*                                                                           */
/* Output(s)          : ppPerStInfo - Memory Block pointer to be released    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
AstDeleteMstInstance (UINT2 u2MstInst, tAstPerStInfo * pPerStInfo)
{
    tMstInstInfo       *pMstInstInfo = NULL;
    if (pPerStInfo != NULL)
    {
        if (pPerStInfo->pFlushTgrTmr != NULL)
        {
            if (AstStopTimer ((VOID *) pPerStInfo, AST_TMR_TYPE_FLUSHTGR)
                != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for FlushTimer FAILED!\n");
            }

        }

        if (AST_COUNT_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList) != 0)
        {
            AST_SCAN_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList,
                               pMstInstInfo, tMstInstInfo *)
            {
                if (pMstInstInfo->pPerStInfo == pPerStInfo)
                {
                    AST_DELETE_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList,
                                         pMstInstInfo);
                    AST_RELEASE_MST_INSTINFO_MEM_BLOCK (pMstInstInfo);
                    break;
                }
            }

        }

        if (pPerStInfo->ppPerStPortInfo != NULL)
        {
            /* Release the PerSt Port Table from PerStInfo. */
            if (AST_COMP_TYPE () == AST_PB_C_VLAN)
            {
                AST_RELEASE_PERST_PORT_TBL_BLOCK
                    (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID,
                     pPerStInfo->ppPerStPortInfo);
            }
            else
            {
                AST_RELEASE_PERST_PORT_TBL_BLOCK
                    (AST_PERST_PORT_TBL_MEMPOOL_ID,
                     pPerStInfo->ppPerStPortInfo);
            }

        }

        /* Release PerStInfo. */
        AST_RELEASE_PERST_INFO_MEM_BLOCK (pPerStInfo);
    }
    AST_GET_PERST_INFO (u2MstInst) = NULL;

    return;

}

/*****************************************************************************/
/* Function Name      : AstTriggerRoleSeletionMachine                        */
/*                                                                           */
/* Description        : This functions triggers role selection machine by    */
/*                      setting the selected as false and reselect as true   */
/*                      for the given instance.                              */
/*                                                                           */
/* Input(s)           : u2PortNum - Pord Id.                                 */
/*                      u2InstId  - Mst Instance id.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
AstTriggerRoleSeletionMachine (UINT2 u2PortNum, UINT2 u2InstId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstId);

    pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
    MstPUpdtAllSyncedFlag (u2InstId, pPerStPortInfo, MST_SYNC_BSELECT_UPDATE);
    pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;

    if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                     u2InstId) != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RoleSelectionMachine function returned FAILURE!\n");
        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: RoleSelectionMachine function returned FAILURE!\n");
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstReleasePortMemBlocks                              */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                       PortEntry , PB Port Information and PerSt Port Info */
/*                                                                           */
/* Input(s)           : pPortInfo - Port Entry whose memory block should be  */
/*                      released                                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/

INT4
AstReleasePortMemBlocks (tAstPortEntry * pPortInfo)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPortInfo->u2PortNo;

    pPerStInfo = AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE);

    if (pPerStInfo != NULL)
    {

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

        if (pPerStPortInfo != NULL)
        {
            /*if Perst Port Information is present, then release it */
            (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
        }

        AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE) = NULL;

    }

    /* Remove this node from the IfIndex RBTree and the PerContext DLL */
    /* For CVLAN component Ports, RBTree Remove will give errors */
    AstRemoveFromIfIndexTable (pPortInfo);

    if (AST_PB_PORT_INFO (pPortInfo) != NULL)
    {

        AST_PB_RELEASE_PB_PORT_INFO_MEM_BLOCK (AST_PB_PORT_INFO (pPortInfo));

        /*If this port is Part of CVLAN component, then remove the 
         *        * CVLAN Port 2 Index Mapping from the RBTree*/
        if (AST_COMP_TYPE () == AST_PB_C_VLAN)
        {
            /* If this port is Part of CVLAN component, then remove the 
             * CVLAN Port 2 Index Mapping from the RBTree*/
            AstPbCVlanRemovePort2IndexMapBasedOnProtPort (pPortInfo->
                                                          u2ProtocolPort);
        }

    }

    AST_PB_PORT_INFO (pPortInfo) = NULL;

    /* Releasing tAstPortEntry Memory Block */
    (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);

    AST_GET_PORTENTRY (u2PortNum) = NULL;

    return RST_SUCCESS;
}

#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
/*****************************************************************************/
/* Function Name      : AstAllocUpCountBlock                                 */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      Context Instance UpCount Array                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u1Mode - Spanning tree mode                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstAllocUpCountBlock (UINT1 u1Mode)
{
    UINT4               u4Size = 0;
    /*Allocate memory for UpCount and Downcount Array */
    if (u1Mode == AST_PVRST_MODULE)
    {
        AST_INSTANCE_UPCOUNT () =
            (INT4 *) (VOID *) MemBuddyAlloc ((UINT1)
                                             AST_INSTANCE_UPCOUNT_MEMPOOL_ID,
                                             (AST_MAX_PVRST_INSTANCES + 1) * 4);
        u4Size = (AST_MAX_PVRST_INSTANCES + 1) * 4;
    }
    else
    {
        AST_INSTANCE_UPCOUNT () =
            (INT4 *) (VOID *) MemBuddyAlloc ((UINT1)
                                             AST_INSTANCE_UPCOUNT_MEMPOOL_ID,
                                             AST_MAX_MST_INSTANCES * 4);
        u4Size = AST_MAX_MST_INSTANCES * 4;
    }
    if (AST_INSTANCE_UPCOUNT () == NULL)
    {
        /*Incrementing memory failure count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for InstanceUpCount array"
                 "Failed!!!\r\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for InstanceUpCount array"
                 "Failed!!!\r\n");
        return RST_FAILURE;
    }

    AST_MEMSET (AST_INSTANCE_UPCOUNT (), AST_INIT_VAL, u4Size);
    return RST_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : AstAllocPortAndPerStInfoBlock                        */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      Context PortEntry Array and Context perst Array.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
AstAllocPortAndPerStInfoBlock (UINT1 u1Mode)
{
    UINT4               u4Size = 0;
    /* Allocate memory for port table in this context. */
    if (AST_ALLOC_PORT_TBL_BLOCK
        (AST_PORT_TBL_MEMPOOL_ID, (AST_CURR_CONTEXT_INFO ()->ppPortEntry))
        == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for Global Port Table failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for Global Port Table failed !!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (AST_PORT_TBL (), 0,
                (AST_MAX_NUM_PORTS * sizeof (tAstPortEntry *)));

    /* Allocate memory for PerStInfo table. */
    if (u1Mode == AST_PVRST_MODULE)
    {
        AST_PERST_INFO_TBL () =
            (tAstPerStInfo **) (VOID *) MemBuddyAlloc
            ((UINT1) AST_PERST_TBL_MEMPOOL_ID,
             (AST_MAX_PVRST_INSTANCES + 1) * sizeof (tAstPerStInfo *));

        u4Size = (AST_MAX_PVRST_INSTANCES + 1) * sizeof (tAstPerStInfo *);
    }
    else
    {
        AST_PERST_INFO_TBL () =
            (tAstPerStInfo **) (VOID *) MemBuddyAlloc
            ((UINT1) AST_PERST_TBL_MEMPOOL_ID, AST_MAX_MST_INSTANCES * 4);

        u4Size = AST_MAX_MST_INSTANCES * 4;
    }

    if (AST_PERST_INFO_TBL () == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for Global Spanning Tree Table failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for Global Spanning Tree Table failed !!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (AST_PERST_INFO_TBL (), AST_INIT_VAL, u4Size);

#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    /* Allocate Memory for Instance UpCount and DownCount Array */
    if (AstAllocUpCountBlock (u1Mode) != RST_SUCCESS)
    {
        return MST_FAILURE;
    }
#endif
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstReleasePortAndPerStInfoBlock                      */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                      Context PortEntry Array and Context perst Array.     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/

INT4
AstReleasePortAndPerStInfoBlock ()
{
    if (AST_PORT_TBL () != NULL)
    {
        AST_RELEASE_PORT_TBL_BLOCK (AST_PORT_TBL_MEMPOOL_ID, AST_PORT_TBL ());
        AST_PORT_TBL () = NULL;
    }

    if (AST_PERST_INFO_TBL () != NULL)
    {
        MemBuddyFree ((UINT1) AST_PERST_TBL_MEMPOOL_ID,
                      (UINT1 *) AST_PERST_INFO_TBL ());
        AST_PERST_INFO_TBL () = NULL;
    }

#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
    if (AST_INSTANCE_UPCOUNT () != NULL)
    {
        MemBuddyFree ((UINT1) AST_INSTANCE_UPCOUNT_MEMPOOL_ID,
                      (UINT1 *) AST_INSTANCE_UPCOUNT ());
        AST_INSTANCE_UPCOUNT () = NULL;
    }
#endif
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetIfIndexEntry                                   */
/*                                                                           */
/* Description        : Returns a pointer to a port Entry for given IfIndex. */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/
tAstPortEntry      *
AstGetIfIndexEntry (UINT4 u4IfIndex)
{
    tAstPortEntry       DummyEntry;

    DummyEntry.u4IfIndex = u4IfIndex;

    return (tAstPortEntry *) RBTreeGet (AST_GLOBAL_IFINDEX_TREE (),
                                        (tRBElem *) & DummyEntry);
}

/*****************************************************************************/
/* Function Name      : AstGetNextIfIndexEntry                               */
/*                                                                           */
/* Description        : Returns a pointer to the port entry of the next      */
/*                      interface in the system known to RSTP/MSTP           */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The next interface to this needs to  */
/*                                      returned.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/
tAstPortEntry      *
AstGetNextIfIndexEntry (tAstPortEntry * pAstPortEntry)
{
    return (tAstPortEntry *) RBTreeGetNext (AST_GLOBAL_IFINDEX_TREE (),
                                            (tRBElem *) pAstPortEntry, NULL);
}

/*****************************************************************************/
/* Function Name      : AstAddToIfIndexTable                                 */
/*                                                                           */
/* Description        : This routine adds the given port entry to both the   */
/*                      global interface database and the per context        */
/*                      interface database both of which are indexed by the  */
/*                      global ifindex                                       */
/*                                                                           */
/* Input(s)           : pAstPortEntry - Port entry to be added               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/
INT4
AstAddToIfIndexTable (tAstPortEntry * pAstPortEntry)
{
    if (RBTreeAdd (AST_PER_CONTEXT_IFINDEX_TREE (), (tRBElem *) pAstPortEntry)
        == RB_FAILURE)
    {
        return RST_FAILURE;
    }

    if (RBTreeAdd (AST_GLOBAL_IFINDEX_TREE (), (tRBElem *) pAstPortEntry)
        == RB_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstGetNumOfPortsCreated                              */
/*                                                                           */
/* Description        : This routine gives the total number of ports created */
/*                      in spanning-tree across all contexts.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* OutPut(s)          : pu4NumPortsCreated - Pointer gives the total number  */
/*                      of ports created across all the contexts.            */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/
INT4
AstGetNumOfPortsCreated (UINT4 *pu4NumPortsCreated)
{
    *pu4NumPortsCreated = 0;

    if (RBTreeCount (AST_GLOBAL_IFINDEX_TREE (), pu4NumPortsCreated)
        == RB_FAILURE)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRemoveFromIfIndexTable                            */
/*                                                                           */
/* Description        : This routine removes the given port entry from both  */
/*                      the global interface database and the per context    */
/*                      interface database both of which are indexed by the  */
/*                      global ifindex                                       */
/*                                                                           */
/* Input(s)           : pAstPortEntry - Port entry to be removed             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/
INT4
AstRemoveFromIfIndexTable (tAstPortEntry * pAstPortEntry)
{
    INT4                i4RetVal = RST_SUCCESS;

    if (AST_COMP_TYPE () != AST_PB_C_VLAN)
    {
        if (RBTreeRemove (AST_PER_CONTEXT_IFINDEX_TREE (),
                          (tRBElem *) pAstPortEntry) == RB_FAILURE)
        {
            i4RetVal = RST_FAILURE;
        }

        if (RBTreeRemove (AST_GLOBAL_IFINDEX_TREE (), (tRBElem *) pAstPortEntry)
            == RB_FAILURE)
        {
            i4RetVal = RST_FAILURE;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AstIfIndexTblCmpFn                                   */
/*                                                                           */
/* Description        : This routine will be invoked by the RB Tree library  */
/*                      to traverse through the global ifIndex based RB      */
/*                      tree.                                                */
/*                                                                           */
/* Input(s)           : pKey1 - Pointer to the first port entry.             */
/*                      pKey2 - Pointer to the second port entry.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1, if pKey1 is greater than pKey2                    */
/*                     -1, if pKey2 is greater than pKey1                    */
/*                      0, if pKey2 and pKey1 are equal.                     */
/*****************************************************************************/
INT4
AstIfIndexTblCmpFn (tRBElem * pKey1, tRBElem * pKey2)
{
    tAstPortEntry      *pAstPortEntry1 = (tAstPortEntry *) pKey1;
    tAstPortEntry      *pAstPortEntry2 = (tAstPortEntry *) pKey2;

    return (pAstPortEntry1->u4IfIndex - pAstPortEntry2->u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : AstFreePortEntry                                     */
/*                                                                           */
/* Description        : This routine is called when the Global IfIndex table */
/*                      is destroyed. It frees the memory occupied by the    */
/*                      given port entry pointer.                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index                             */
/*                      u1IfType - Type of the port                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*****************************************************************************/
INT4
AstFreePortEntry (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    AST_RELEASE_PORT_INFO_MEM_BLOCK ((tAstPortEntry *) pElem);

    return 1;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetContextInfoFromIfIndex                     */
/*                                                                           */
/*    Description         : This function retives the context and Local port */
/*                          Id from the given IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : pu4ContextId                                     */
/*                          pu2LocalPort                                     */
/*                                                                           */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                      .                      */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetContextInfoFromIfIndex (UINT4 u4Port, UINT4 *pu4ContextId,
                              UINT2 *pu2LocalPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    /* Get Global ifindex based RBTree and get the context ID and
       local port from the port entry else return failure */
    pAstPortEntry = AstGetIfIndexEntry (u4Port);

    if (pAstPortEntry == NULL)
    {
        return RST_FAILURE;
    }

    *pu4ContextId = pAstPortEntry->u4ContextId;
    *pu2LocalPort = pAstPortEntry->u2PortNo;

    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetFirstActiveContext                         */
/*                                                                           */
/*    Description         : This function gets the first active virtual      */
/*                          context ID in RSTP/MSTP.                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : pu4ContextId                                     */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                      .                      */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetFirstActiveContext (UINT4 *pu4ContextId)
{
    UINT4               u4ContextId;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            *pu4ContextId = u4ContextId;
            return RST_SUCCESS;
        }
    }
    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetNextActiveContext                          */
/*                                                                           */
/*    Description         : This function gets the next active virtual       */
/*                          context ID in RSTP/MSTP.                         */
/*                                                                           */
/*    Input(s)            : u4StpContextId - Next active context to this     */
/*                                           should be returned              */
/*                                                                           */
/*    Output(s)           : pu4ContextId                                     */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                      .                      */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetNextActiveContext (UINT4 u4StpContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId;

    for (u4ContextId = u4StpContextId + 1;
         u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            *pu4NextContextId = u4ContextId;
            return RST_SUCCESS;
        }
    }

    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetFirstPortInCurrContext                     */
/*                                                                           */
/*    Description         : This function is used to get the first port      */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pi4IfIndex - Interface Index.                    */
/*                                                                           */
/*    Global Variables Referred : gpAstContextInfo->PerContextIfIndexTable.  */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetFirstPortInCurrContext (INT4 *pi4IfIndex)
{
    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = (tAstPortEntry *) RBTreeGetFirst
        (AST_PER_CONTEXT_IFINDEX_TREE ());

    if (pPortEntry != NULL)
    {
        *pi4IfIndex = (INT4) pPortEntry->u4IfIndex;
        return RST_SUCCESS;
    }
    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetNextPortInCurrContext                      */
/*                                                                           */
/*    Description         : This function is used to get the next port       */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                                                                           */
/*    Global Variables Referred : gpAstContextInfo->PerContextIfIndexTable   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetNextPortInCurrContext (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPortEntry       Dummy;

    Dummy.u4IfIndex = (UINT4) i4IfIndex;

    pPortEntry = RBTreeGetNext (AST_PER_CONTEXT_IFINDEX_TREE (), &Dummy, NULL);

    if (pPortEntry != NULL)
    {
        *pi4NextIfIndex = (INT4) pPortEntry->u4IfIndex;
        return RST_SUCCESS;
    }
    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetFirstPortInContext                         */
/*                                                                           */
/*    Description         : This function is used to get the first port      */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pi4IfIndex - Interface Index.                    */
/*                                                                           */
/*    Global Variables Referred : gpAstContextInfo->PerContextIfIndexTable.  */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetFirstPortInContext (UINT4 u4ContextId, INT4 *pi4IfIndex)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    pPortEntry = (tAstPortEntry *) RBTreeGetFirst
        (AST_PER_CONTEXT_IFINDEX_TREE ());

    if (pPortEntry != NULL)
    {
        *pi4IfIndex = (INT4) pPortEntry->u4IfIndex;

        AstReleaseContext ();
        return RST_SUCCESS;
    }

    AstReleaseContext ();
    return RST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetNextPortInContext                          */
/*                                                                           */
/*    Description         : This function is used to get the next port       */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                                                                           */
/*    Global Variables Referred : gpAstContextInfo->PerContextIfIndexTable   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetNextPortInContext (UINT4 u4ContextId, INT4 i4IfIndex,
                         INT4 *pi4NextIfIndex)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPortEntry       Dummy;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    Dummy.u4IfIndex = (UINT4) i4IfIndex;

    pPortEntry = RBTreeGetNext (AST_PER_CONTEXT_IFINDEX_TREE (), &Dummy, NULL);

    if (pPortEntry != NULL)
    {
        *pi4NextIfIndex = (INT4) pPortEntry->u4IfIndex;

        AstReleaseContext ();

        return RST_SUCCESS;
    }

    AstReleaseContext ();

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstPbGetNextCepPortInCurrContext                     */
/*                                                                           */
/* Description        : This function is called by CLI routine. This function*/
/*                     will return Next CEP Port ENtry in the Current Ctxt.  */
/*                                                                           */
/* Input (s)          : u4ContextId - Curr Context Id                        */
/*                      u4IfIndex - Input Port Index                         */
/*                      *pu4NextCepIfIndex - Next CEP Port IfIndex           */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstPbGetNextCepPortInCurrContext (UINT4 u4Context, UINT4 u4IfIndex,
                                  UINT4 *pu4NextCepIfIndex)
{
    tAstPortEntry       TempAstPortInfo;
    tAstPortEntry      *pAstPortInfo = NULL;

    AST_MEMSET (&TempAstPortInfo, AST_INIT_VAL, sizeof (tAstPortEntry));

    TempAstPortInfo.u4IfIndex = u4IfIndex;

    *pu4NextCepIfIndex = 0;

    if (AstSelectContext (u4Context) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    pAstPortInfo = RBTreeGetNext (AST_PER_CONTEXT_IFINDEX_TREE (),
                                  &TempAstPortInfo, NULL);

    while (pAstPortInfo != NULL)
    {
        if (AST_GET_BRG_PORT_TYPE (pAstPortInfo) == AST_CUSTOMER_EDGE_PORT)
        {
            *pu4NextCepIfIndex = pAstPortInfo->u4IfIndex;
            AstReleaseContext ();
            return RST_SUCCESS;
        }
        pAstPortInfo =
            RBTreeGetNext (AST_PER_CONTEXT_IFINDEX_TREE (), pAstPortInfo, NULL);
    }

    AstReleaseContext ();

    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstTrapFillContextInfo                               */
/*                                                                           */
/* Description        : This function will fill the context-name in VbList   */
/*                      depends upon the mode.                               */
/*                                                                           */
/* Input (s)          : ppVbList          - Varable Bind List                */
/*                      SnmpCounter64Type - Counter                          */
/*                      u1IsLastObject    - Flag to indicate the position.   */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
VOID
AstTrapFillContextInfo (tSNMP_VAR_BIND ** ppVbList,
                        tSNMP_COUNTER64_TYPE SnmpCounter64Type,
                        UINT1 u1IsLastObject)
{
#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE *pOstring;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[256];

    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return;
    }

    if (AST_IS_RST_STARTED ())
    {
        SPRINTF ((CHR1 *) au1Buf, "%s", RST_MIB_OBJ_CONTEXT_NAME);
        pOid = RstMakeObjIdFromDotNew ((INT1 *) au1Buf);
    }
#ifdef MSTP_WANTED
    else if (AST_IS_MST_STARTED ())
    {
        SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CONTEXT_NAME);
        pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
    }
#endif
#ifdef PVRST_WANTED
    else if (AST_IS_PVRST_STARTED ())
    {
        SPRINTF ((CHR1 *) au1Buf, "%s", PVRST_MIB_OBJ_CONTEXT_NAME);
        pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
    }
#endif
    pOstring = SNMP_AGT_FormOctetString (AST_CONTEXT_STR (),
                                         (AST_SWITCH_ALIAS_LEN - 1));

    (*ppVbList)->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCounter64Type);

    /* If Context-name is the last object in the trap no need to do the
     * below operation */
    if (u1IsLastObject == AST_FALSE)
    {
        *ppVbList = (*ppVbList)->pNextVarBind;
    }
#else
    UNUSED_PARAM (ppVbList);
    UNUSED_PARAM (SnmpCounter64Type);
    UNUSED_PARAM (u1IsLastObject);
#endif
}

VOID
AstGlobalDebug (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         Str[AST_TRC_BUF_SIZE];
    int                 i;

    UNUSED_PARAM (u4Flags);
    if (!(AST_GLB_DBG_FLAG & AST_TRUE))
    {
        return;
    }

    if (AST_IS_INITIALISED () == RST_FALSE)
    {
        return;
    }

    if (u4ContextId == AST_INVALID_CONTEXT)
    {
        va_start (ap, fmt);
        vsprintf (&Str[0], fmt, ap);
        va_end (ap);
        UtlTrcLog (AST_GLB_DBG_FLAG, AST_TRUE, AST_MOD_NAME, Str);
    }
    else
    {
        va_start (ap, fmt);
        i = sprintf (&Str[0], "Context %u", u4ContextId);
        vsprintf (&Str[i], fmt, ap);
        va_end (ap);
        UtlTrcLog (AST_GLB_DBG_FLAG, AST_TRUE, AST_MOD_NAME, Str);
    }
}

VOID
AstGlobalTrace (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...)
{
#ifdef TRACE_WANTED
    va_list             ap;
    static CHR1         Str[AST_TRC_BUF_SIZE];
    int                 i;

    UNUSED_PARAM (u4Flags);
    if (!(AST_GLB_TRC_FLAG & AST_TRUE))
    {
        return;
    }

    if (AST_IS_INITIALISED () == RST_FALSE)
    {
        return;
    }

    if (u4ContextId == AST_INVALID_CONTEXT)
    {
        va_start (ap, fmt);
        vsprintf (&Str[0], fmt, ap);
        va_end (ap);
        UtlTrcLog (AST_GLB_TRC_FLAG, AST_TRUE, AST_MOD_NAME, Str);
    }
    else
    {

        va_start (ap, fmt);

        i = sprintf (&Str[0], "Context %u", u4ContextId);
        vsprintf (&Str[i], fmt, ap);
        va_end (ap);
        UtlTrcLog (AST_GLB_TRC_FLAG, AST_TRUE, AST_MOD_NAME, Str);
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Flags);
    UNUSED_PARAM (fmt);
    return;
#endif
}

/*****************************************************************************/
/* Function Name      : AstGetHwPortState                                    */
/*                                                                           */
/* Description        : This function will give the status of the hardware   */
/*                      port state set NPAPI call for the given port and     */
/*                      instance.                                            */
/*                                                                           */
/* Input (s)          : u4IfIndex         - Interface index                  */
/*                      i4Inst            - Instance of given port           */
/*                      *pi4PortStatus    - Port status for given Port       */
/*                                                                           */
/* Output (s)         : Hardware Port status.                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/RST_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetHwPortStateStatus (UINT4 u4IfIndex, INT4 i4Inst, INT4 *pi4PortStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (i4Inst == AST_TE_MSTID)
    {
        *pi4PortStatus = AST_FORWARD_SUCCESS;
        return RST_SUCCESS;

    }
    if (AstGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId, i4Inst);

    if (pPerStPortInfo != NULL)
    {
        *pi4PortStatus = pPerStPortInfo->i4NpPortStateStatus;
        AstReleaseContext ();

        return RST_SUCCESS;
    }

    AstReleaseContext ();
    return RST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : AstDeleteAllContextsAndMemPools                      */
/*                                                                           */
/* Description        : This is a utility function used to release           */
/*                      all allocated memory in the RSTP/MSTP module.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS or RST_FAILURE                           */
/*                                                                           */
/* Called By          : ISS                                                  */
/*****************************************************************************/
INT4
AstDeleteAllContextsAndMemPools (VOID)
{
    UINT4               u4ContextId;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            continue;
        }

        /* Context released once it is deleted */
        AstHandleDeleteContext (u4ContextId);
    }

    /* Delete the mempools */
    AstGlobalMemoryDeinit ();
    return RST_SUCCESS;
}

/* This is a temporary function used in the wrapper file present in Vlan module
 * for the Bridge and Spanning Tree MIB.
 * It should be removed when the wrapper code for Spanning tree MIB objects is
 * moved within the spanning tree folder. */

/*****************************************************************************/
/* Function Name      : AstGetIfIndex                                        */
/*                                                                           */
/* Description        : This is a utility function used to get global IfIndex*/
/*                      from the given Local port number in the given        */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Global IfIndex                                       */
/*                                                                           */
/* Called By          : ISS                                                  */
/*****************************************************************************/
INT4
AstGetIfIndex (INT4 i4Dot1dStpPort)
{
    return (AST_GET_IFINDEX (i4Dot1dStpPort));
}

/* This is a temporary function used in the wrapper file present in Vlan module
 * for the Bridge and Spanning Tree MIB.
 * It should be removed when the wrapper code for Spanning tree MIB objects is
 * moved within the spanning tree folder. */

/*****************************************************************************/
/* Function Name      : AstCurrContextId                                     */
/*                                                                           */
/* Description        : This is a utility function used to get current       */
/*                      context Id.                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Currently selected Virtual Context Identifier        */
/*                                                                           */
/* Called By          : ISS                                                  */
/*****************************************************************************/
INT4
AstCurrContextId (VOID)
{
    return (AST_CURR_CONTEXT_ID ());
}

/*****************************************************************************/
/* Function Name      : AstGetPseudoRootIdConfigStatus                       */
/*                                                                           */
/* Description        :  Indicates whether the PseudoRootId has been         */
/*                       manually configured for the given port or has been  */
/*                       dynamically calculated.                             */
/*                                                                           */
/* Input(s)           : u2LocalPortId - Local Port Index                     */
/*                      u2InstanceId  - MST Instance ID / zero for RST       */
/*                      u4ContextId   - The context Id                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetPseudoRootIdConfigStatus (UINT2 u2LocalPortId, UINT2 u2InstanceId,
                                UINT4 u4ContextId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return RST_FALSE;
    }

    pPerStInfo = (AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId];
    if (pPerStInfo == NULL)
    {
        AstReleaseContext ();
        return RST_FALSE;
    }
    pPerStPortInfo = pPerStInfo->ppPerStPortInfo[u2LocalPortId - 1];

    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return RST_FALSE;
    }

    if (pPerStPortInfo->bIsPseudoRootIdConfigured == RST_FALSE)
    {
        AstReleaseContext ();
        return RST_FALSE;
    }

    AstReleaseContext ();
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstIsPortVip                                         */
/*                                                                           */
/* Description        : Indicates whether the given port is a VIP or not.    */
/*                                                                           */
/* Input(s)           : u4PortId - Port Id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstIsPortVip (UINT4 u4PortId)
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;
    UINT2               u2LocalPortId = AST_INVALID_PORT_NUM;

    if (AstVcmGetContextInfoFromIfIndex (u4PortId, &u4ContextId,
                                         &u2LocalPortId) != VCM_SUCCESS)
    {
        return RST_FALSE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return RST_FALSE;
    }

    pPortInfo = AST_GET_PORTENTRY (u2LocalPortId);
    if (pPortInfo == NULL)
    {
        AstReleaseContext ();
        return RST_FALSE;
    }

    if (AST_IS_VIRTUAL_INST_PORT (u2LocalPortId) == RST_TRUE)
    {
        AstReleaseContext ();
        return RST_TRUE;
    }

    AstReleaseContext ();
    return RST_FALSE;
}

/*****************************************************************************/
/* Function Name      : AstDeriveMacAddrFromPortType                         */
/*                                                                           */
/* Description        : Calculates and stores the destination MAC Address    */
/*                      depending upon the port type                         */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Number                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
AstDeriveMacAddrFromPortType (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               u1PortType = AST_INIT_VAL;

    /* Before calling this function, Context should be selected
     * and Lock should be taken
     * */
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo == NULL)
    {
        return;
    }

    u1PortType = AST_GET_BRG_PORT_TYPE (pPortInfo);

    /* Following are the destination address mapped to the port types
     * 01.80.c2.00.00.00               01.80.c2.00.00.08
     * -----------------               -----------------
     * All other ports                   I-Component Ports
     * confirming to                     B-Component Ports
     * C-VLAN                            PNP
     *                                   CNP S-Tagged, Port-based
     *                                   PCEP
     *                                   PCNP
     *                                   CBP
     * */

    switch (u1PortType)
    {
        case AST_PROVIDER_NETWORK_PORT:
        case AST_VIRTUAL_INSTANCE_PORT:
        case AST_CNP_STAGGED_PORT:
        case AST_CNP_PORTBASED_PORT:
        case AST_PROP_CUSTOMER_EDGE_PORT:
        case AST_PROP_CUSTOMER_NETWORK_PORT:
            /* Port Confirms to S-VLAN. Hence the destination 
             * address will be 01-80-C2-00-00-08
             * */
            pPortInfo->au1DestMACAddr[0] = (UINT1) 0x01;
            pPortInfo->au1DestMACAddr[1] = (UINT1) 0x80;
            pPortInfo->au1DestMACAddr[2] = (UINT1) 0xC2;
            pPortInfo->au1DestMACAddr[3] = (UINT1) 0x00;
            pPortInfo->au1DestMACAddr[4] = (UINT1) 0x00;
            pPortInfo->au1DestMACAddr[5] = (UINT1) 0x08;
            break;

        default:
            /* This case covers all the normal ports confirming
             * to C-VLAN
             * */
            pPortInfo->au1DestMACAddr[0] = (UINT1) 0x01;
            pPortInfo->au1DestMACAddr[1] = (UINT1) 0x80;
            pPortInfo->au1DestMACAddr[2] = (UINT1) 0xC2;
            pPortInfo->au1DestMACAddr[3] = (UINT1) 0x00;
            pPortInfo->au1DestMACAddr[4] = (UINT1) 0x00;
            pPortInfo->au1DestMACAddr[5] = (UINT1) 0x00;
            break;
    }
}

/*****************************************************************************/
/* Function Name      : AstGetPbPortType                                */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
AstGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    UINT1               u1InterfaceType = AST_INIT_VAL;

    if (AstL2IwfGetPbPortType (u4IfIndex, pu1PortType) != L2IWF_SUCCESS)
    {
        return RST_FAILURE;
    }

    if (*pu1PortType == AST_CNP_STAGGED_PORT &&
        AST_IS_1AH_BRIDGE () == RST_TRUE)
    {
        /* Derive based on the interface type.
         * If interface type == C_VLAN, then porttype = CNP_C_TAGGED
         * */
        if ((AstL2IwfGetInterfaceType (AST_CURR_CONTEXT_ID (),
                                       &u1InterfaceType)) != L2IWF_SUCCESS)
        {
            return RST_FAILURE;
        }

        if (u1InterfaceType == AST_C_INTERFACE_TYPE)
        {
            *pu1PortType = AST_CNP_CTAGGED_PORT;
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : AstIsPathCostSet                                   */
/*                                                                           */
/* Description          : This is to determine whether pathcost has been     */
/*                        configured in this port.                           */
/*                                                                           */
/* Input(s)             : u2LocalPortId - Port Id                            */
/*                        u2InstanceId - Instance ID                         */
/*                                                                           */
/* Output(s)            : None                                               */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : RST_TRUE  - If pathcost has been configured        */
/*                        RST_FALSE - If pathcost has not been configured    */
/*                                                                           */
/* Called By            : CLI / MSR                                          */
/*****************************************************************************/
UINT1
AstIsPathCostSet (UINT2 u2LocalPortId, UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4PortAdminPathCost;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return RST_FALSE;
    }

    u4PortAdminPathCost = pPerStPortInfo->u4PortAdminPathCost;

    if (u4PortAdminPathCost == 0)
    {
        return RST_FALSE;
    }
    return RST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstLockAndSelCliContext                              */
/*                                                                           */
/* Description        : This function is used to take the RSTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread. This function also selects*/
/*                      the context stored in gu4StpCliContext variable.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
AstLockAndSelCliContext (VOID)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    i4RetVal = AstLock ();
    AstSelectContext (gu4StpCliContext);
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : AstGetSysTime                                     */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
AstGetSysTime (tOsixSysTime * pSysTime)
{

    tUtlSysPreciseTime  SysPreciseTime;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    /* Function used to return the system time in seconds and nanoseconds */
    UtlGetPreciseSysTime (&SysPreciseTime);
    /* Time returned in milliseconds */
    *pSysTime =
        ((SysPreciseTime.u4Sec * 1000) + (SysPreciseTime.u4NanoSec / 1000000));

    return RST_SUCCESS;

}

/************************************************************************/
/*  Function Name   : AstValidateComponentId                            */
/*  Description     : Verify whether the component Id is valid or not   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SNMP_FAILURE/SNMP_SUCCESS                         */
/************************************************************************/
INT1
AstValidateComponentId (UINT4 u4Ieee8021SpanningTreePortComponentId,
                        UINT4 u4Ieee8021SpanningTreePort)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (u4Ieee8021SpanningTreePort,
                                      &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        if (u4ContextId == u4Ieee8021SpanningTreePortComponentId)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : AstUtilGetPortAdminPointToPoint
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the BridgeBasePortAdminPointToPointi status.
 *
 *    INPUT            : pStpConfigInfo -STP Config Info
 *    OUTPUT           : NONE 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
AstUtilGetPortAdminPointToPoint (tStpConfigInfo * pStpConfigInfo)
{

    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (pStpConfigInfo->u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        if (AstSelectContext ((INT4) u4ContextId) == RST_SUCCESS)
        {
            if (AstValidatePortEntry (u2LocalPortId) == RST_SUCCESS)
            {
                pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
                *(pStpConfigInfo->pi4PortAdminPointToPoint) =
                    (INT4) pAstPortEntry->u1AdminPointToPoint;
                pStpConfigInfo->bAdminP2PUpdated =
                    pAstPortEntry->bIEEE8021apAdminP2P;
                AstReleaseContext ();
                return OSIX_SUCCESS;
            }
        }
    }
    AstReleaseContext ();
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : AstUtilGetPortOperPointToPoint
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the BridgeBasePortOperPointToPoint status.
 *
 *    INPUT            : pStpConfigInfo -STP Config Info
 *    OUTPUT           : NONE 
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
AstUtilGetPortOperPointToPoint (tStpConfigInfo * pStpConfigInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (pStpConfigInfo->u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        if (AstSelectContext ((INT4) u4ContextId) == RST_SUCCESS)
        {
            if (AstValidatePortEntry (u2LocalPortId) == RST_SUCCESS)
            {
                pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
                /* As per the IEEE mib, the ieee8021BridgeBasePortOperPointToPoint
                 * object should be assigned a value of FORCETRUE
                 * if Oper PointToPoint is True(1) else FORCEFALSE.*/

                if (pAstPortEntry->bOperPointToPoint == RST_TRUE)
                {
                    *(pStpConfigInfo->pi4PortOperPointToPoint) =
                        RST_P2P_FORCETRUE;
                }
                else
                {
                    *(pStpConfigInfo->pi4PortOperPointToPoint) =
                        RST_P2P_FORCEFALSE;
                }
                AstReleaseContext ();
                return OSIX_SUCCESS;
            }
        }
    }
    AstReleaseContext ();
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : AstUtilSetPortAdminPointToPoint
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to set the BridgeBasePortAdminPointToPoint.
 *
 *    INPUT            : pStpConfigInfo -STP Config Info
 *    OUTPUT           : NONE 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
AstUtilSetPortAdminPointToPoint (tStpConfigInfo * pStpConfigInfo)
{

    tAstMsgNode        *pNode = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    if (AstGetContextInfoFromIfIndex (pStpConfigInfo->u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        u4CurrContextId = AstCurrContextId ();

        if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Message Memory Block Allocation FAILED!\n");
            AstReleaseContext ();
            return OSIX_FAILURE;
        }
        AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));
        pNode->MsgType = AST_ADMIN_PTOP_MSG;
        pNode->u4PortNo = u2LocalPortId;
        pNode->uMsg.u1AdminPToP =
            (UINT1) (pStpConfigInfo->i4PortAdminPointToPoint);
        RM_GET_SEQ_NUM (&u4SeqNum);
        if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
        {
            i1RetVal = OSIX_SUCCESS;
        }
        AstSelectContext (u4CurrContextId);
    }
    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : AstUtilTestPortAdminPointToPoint
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to test the BridgeBasePortAdminPointToPoint.
 *
 *    INPUT            : pStpConfigInfo -STP Config Info
 *    OUTPUT           : NONE 
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
AstUtilTestPortAdminPointToPoint (tStpConfigInfo * pStpConfigInfo)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = OSIX_SUCCESS;

    if (AstGetContextInfoFromIfIndex (pStpConfigInfo->u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) == RST_SUCCESS)
    {
        if (AstSelectContext ((INT4) u4ContextId) == RST_SUCCESS)
        {
            if (!AST_IS_RST_STARTED () && !AST_IS_MST_STARTED ())
            {
                *(pStpConfigInfo->pu4ErrorCode) = SNMP_ERR_INCONSISTENT_VALUE;
                AstReleaseContext ();
                return OSIX_FAILURE;
            }
            if (AstValidatePortEntry (u2LocalPortId) != RST_SUCCESS)
            {
                *(pStpConfigInfo->pu4ErrorCode) = SNMP_ERR_NO_CREATION;
                AstReleaseContext ();
                return OSIX_FAILURE;
            }
            if ((pStpConfigInfo->i4PortAdminPointToPoint != RST_P2P_FORCETRUE)
                && (pStpConfigInfo->i4PortAdminPointToPoint !=
                    RST_P2P_FORCEFALSE)
                && (pStpConfigInfo->i4PortAdminPointToPoint != RST_P2P_AUTO))
            {
                *(pStpConfigInfo->pu4ErrorCode) = SNMP_ERR_WRONG_VALUE;
                AstReleaseContext ();
                return OSIX_FAILURE;
            }
            i1RetVal = OSIX_SUCCESS;
        }
    }
    AstReleaseContext ();
    return i1RetVal;
}

/* Modified for Attachment Circuit interface */

/*****************************************************************************/
/* Function Name      : AstIsExtInterface                                    */
/*                                                                           */
/* Description        : Indicates whether the given port is a Pseudo wire    */
/*                      Interface/Attachment Circuit interface or not.       */
/*                                                                           */
/* Input(s)           : i4PortId - Local Port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_TRUE / RST_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstIsExtInterface (INT4 i4PortId)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;
    UINT1               u1RetVal = AST_FALSE;
    INT4                i4Retval = CFA_FAILURE;

    AstVcmGetIfIndexFromLocalPort (AST_CURR_CONTEXT_ID (), i4PortId,
                                   &u4IfIndex);

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    i4Retval = AstCfaGetIfInfo (u4IfIndex, &IfInfo);

    if ((IfInfo.u1IfType == AST_PSEUDO_WIRE) ||
        ((IfInfo.u1IfType == AST_PROP_VIRTUAL_INTERFACE) &&
         (IfInfo.u1IfSubType == AST_ATTACHMENT_CIRCUIT_INTERFACE)) ||
        (IfInfo.u1IfType == AST_VXLAN_NVE))

    {
        u1RetVal = AST_TRUE;
    }
    UNUSED_PARAM (i4Retval);
    return u1RetVal;

}

/*****************************************************************************/
/* Function Name      : AstCreatePswInterface                                */
/*                                                                           */
/* Description        : This function creates port in RSTP/MSTP/PVRST. Based */
/*                      on the module started, it call RstCreatePort or      */
/*                      MstCreatePort or PvrstCreatePort. Once the creation  */
/*                      of port is done, this function call                  */
/*                      AstPbCheckPortTypeChange to update the port type for */
/*                      the given port.                                      */
/*                                                                           */
/* Input(s)           : i4PortId - Local port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstCreateExtInterface (INT4 i4PortId)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    UINT4               u4IfIndex;
    UINT1               u1OperStatus = AST_INIT_VAL;

    AstVcmGetIfIndexFromLocalPort (u4ContextId, i4PortId, &u4IfIndex);

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
#ifdef RSTP_WANTED
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
#endif
#ifdef MSTP_WANTED
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
#endif
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
    pAstMsgNode->u4PortNo = u4IfIndex;
    pAstMsgNode->u4ContextId = u4ContextId;
    pAstMsgNode->uMsg.u2LocalPortId = i4PortId;
    if (AstHandleCreatePort (pAstMsgNode) != RST_SUCCESS)
    {
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }
        return RST_FAILURE;
    }
    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Msg Memory Block FAILED!\n");
        return RST_FAILURE;
    }
    if (AstCfaGetIfOperStatus (u4IfIndex, &u1OperStatus) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfOperStatus for Port %u\n", u4IfIndex);
        return RST_FAILURE;
    }
    AstUpdateOperStatus (u4IfIndex, u1OperStatus);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeletePswInterface                                */
/*                                                                           */
/* Description        : This function deletes port in RSTP/MSTP/PVRST. Based */
/*                      on the module started, it call RstDeletePort or      */
/*                      MstDeletePort or PvrstDeletePort.                    */
/*                                                                           */
/* Input(s)           : i4PortId - Local port Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstDeleteExtInterface (INT4 i4PortId)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    INT1                i1RetVal = RST_SUCCESS;

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
#ifdef RSTP_WANTED
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
#endif
#ifdef MSTP_WANTED
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
#endif
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_MSG;
    pAstMsgNode->u4PortNo = i4PortId;
    pAstMsgNode->u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstHandleDeletePort (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = RST_FAILURE;
    }
    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Msg Memory Block FAILED!\n");
        return RST_FAILURE;
    }
    return i1RetVal;
}

/*****************************************************************************/
/* Function Name      : AstPortOperStatus                                    */
/*                                                                           */
/* Description        : This function is called to update the operational    */
/*                      status of the port status.                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Global IfIndex of the port to which  */
/*                                  this indication belongs                  */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
AstUpdateOperStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return RST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    /* Even though instance id is not needed for updating port oper status,
     * the Port Operstatus and Port Stp Enabled Status is handled in
     * a single function, so a check has been in MstEnablePort which 
     * leads to fill up this variable*/
    pNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    switch (u1Status)
    {
        case AST_UP:
            pNode->MsgType = (tAstLocalMsgType) AST_ENABLE_PORT_MSG;
            pNode->uMsg.u1TrigType = (UINT1) AST_EXT_PORT_UP;
            break;

        case AST_DOWN:
            pNode->MsgType = (tAstLocalMsgType) AST_DISABLE_PORT_MSG;
            pNode->uMsg.u1TrigType = (UINT1) AST_EXT_PORT_DOWN;
            break;
    }

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstFlushFdbEntries                         */
/*                                                                           */
/*    Description               : This function performs the functionality   */
/*                                of flushing the Filtering Database to      */
/*                                remove the information that was learnt on  */
/*                                this Port for all Fids.                    */
/*                                                                           */
/*    Input(s)                  : pPortInfo - Pointer to the port            */
/*                                            Information structure          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
AstFlushFdbEntries (tAstPortEntry * pPortInfo, UINT1 u1Optimize)
{
    tAstPbPortInfo     *pPbPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2PortNum = 0;

    u2PortNum = pPortInfo->u2PortNo;

    pPerStInfo = AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    if ((pPerStInfo == NULL) || (pPerStBrgInfo == NULL))
    {
        return;
    }

    u4IfIndex = AST_GET_IFINDEX (u2PortNum);

    /*
     * When flush interval is default value ( == 0), then normal
     * flow will continue. (i.e) Port based flushing
     * will be triggered.
     *
     * When flush interval is non-default value (!= 0), then
     * the following procedures will be taken place:
     *
     * If (Flush trigger timer == Running)
     * ==> Pending flushes will be updated as TRUE
     *
     * If (Flush Trigger timer == Not_Running)
     *
     * ==> If (Flush Interval Threshold == Default_Value(0))
     *        ==> Call Global flush. The same flush function
     *            will be invoked with invalid port num
     *        ==> Increment the global flush count after the
     *            last timer expiry
     *        ==> Increment the totalflush count for the switch
     *
     * ==> If (Actual_Flush_Indication_Count_After_last_timer_expiry
     *         < Flush_Interval_Threshold)
     *        ==> Call port based flushing with the actual
     *            port number value.
     *        ==> Increment the global flush count after the
     *            last timer expiry
     *        ==> Increment the total flush count for the switch
     *
     * ==> Else If (Flush INterval Threshold == Default value (0))
     *          || (Actual_Flush_Indication_Count_After_last_timer_expiry
     *          == Flush_Interval_Threshold)
     *        ==> Start flush trigger timer
     *        ==> Update pending flushes as false
     *
     *
     * During timer expiry, global flushing will be invoked by
     * filling the port number as invalid port identifier.
     *
     */

    if ((AST_FLUSH_INTERVAL == AST_INIT_VAL)
        || (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL))
    {
        /* For ports other than PEP, flush all the FDB entries on the port */
        if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
        {
            AstVlanDeleteFdbEntries (u4IfIndex, u1Optimize);
        }
        else
        {
            /* For PEP, delete the FDB entries for the corresponding CEP port
             * and SVLAN */
            pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);
            if (pPbPortInfo != NULL)
            {
                VlanFlushFdbEntriesOnPort (pPbPortInfo->u4CepIfIndex,
                                           pPbPortInfo->Svid, STP_MODULE,
                                           u1Optimize);
            }
        }
        (pPerStBrgInfo->u4TotalFlushCount)++;
    }
    else
    {
        if (pPerStInfo->pFlushTgrTmr == NULL)
        {
            if (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL)
            {
                u4IfIndex = AST_INVALID_PORT_NUM;
            }

            if ((pPerStBrgInfo->u4FlushIndCount <
                 pPerStBrgInfo->u4FlushIndThreshold) ||
                (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL))
            {
                /* For ports other than PEP, flush all the FDB entries on the port */
                if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
                {
                    AstVlanDeleteFdbEntries (u4IfIndex, u1Optimize);
                }
                else
                {
                    /* For PEP, delete the FDB entries for the corresponding CEP port
                     * and SVLAN */
                    pPbPortInfo = AST_PB_PORT_INFO (pPortInfo);
                    if (pPbPortInfo != NULL)
                    {
                        VlanFlushFdbEntriesOnPort (pPbPortInfo->u4CepIfIndex,
                                                   pPbPortInfo->Svid,
                                                   STP_MODULE, u1Optimize);
                    }
                }
                (pPerStBrgInfo->u4FlushIndCount)++;
                (pPerStBrgInfo->u4TotalFlushCount)++;
            }

            if (pPerStBrgInfo->u4FlushIndCount >=
                pPerStBrgInfo->u4FlushIndThreshold)
            {
                if (AstStartTimer ((VOID *) pPerStInfo, RST_DEFAULT_INSTANCE,
                                   (UINT1) AST_TMR_TYPE_FLUSHTGR,
                                   (UINT2) AST_FLUSH_INTERVAL) != RST_SUCCESS)
                {
                    AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: AstStartTimer for flush trigger FAILED!\n");
                }

                pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
            }
        }
        else
        {
            pPerStInfo->FlushFlag.u1PendingFlushes = AST_TRUE;
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AstPbVlanDeleteFdbEntries                            */
/*                                                                           */
/* Description        : This Function takes care of Deleting the Fdb Entries */
/*                      learned in the VLAN module                           */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to port entry.                   */
/*                      u1Optimize - OPTIMIZATION FLAG FOR DELETION          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/

INT4
AstPbVlanDeleteFdbEntries (tAstPortEntry * pPortInfo, UINT1 u1Optimize)
{

    AstFlushFdbEntries (pPortInfo, u1Optimize);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstHandleImpossibleState                             */
/*                                                                           */
/* Description        : This Function starts the RESTART timer based         */
/*                      on the port information received.                    */
/*                                                                           */
/* Input(s)           : u2PortNo - Port for which RestartTimer to be started */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE                             */
/*****************************************************************************/

INT4
AstHandleImpossibleState (UINT2 u2PortNo)
{
    tAstPortEntry      *pPortEntry = NULL;
#ifdef MSTP_WANTED
    tAstPerStInfo      *pPerStInfo = NULL;
#endif
    UINT4               u4RestartTime = AST_INIT_VAL;
    UINT2               u2MstInst = AST_INIT_VAL;
    UINT4               u4Ticks = 0;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pPortEntry = AST_GET_PORTENTRY (u2PortNo);
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    if (pPortEntry == NULL)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Port Entry does not exist\n",
                      AST_GET_IFINDEX_STR (u2PortNo));

        return RST_FAILURE;
    }
    UtlGetTimeStr (au1TimeStr);
    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "Port:%s Impossible state occured at : %s\n",
                      AST_GET_IFINDEX_STR (pPortEntry->u2PortNo), au1TimeStr);
    AST_INCR_IMPSTATE_OCCUR_COUNT (pPortEntry->u2PortNo);
    OsixGetSysTime (&u4Ticks);
    pPortEntry->u4ImpStateOccurTimeStamp = u4Ticks;
    AST_DBG_ARG1 (AST_ALL_FLAG,
                  "SYS: Restarting State Machines for context: %s due to Impossible event/state \n",
                  AST_CONTEXT_STR ());

    if (AST_IS_RST_ENABLED ())
    {
        if (AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNo) !=
            AST_PORT_STATE_DISCARDING)
        {
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                          "SYS: Port %s: Inst %d: Moving to state DISCARDING \n",
                          AST_GET_IFINDEX_STR (u2PortNo), RST_DEFAULT_INSTANCE);

            AstSetInstPortStateToL2Iwf (RST_DEFAULT_INSTANCE,
                                        u2PortNo, AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (RST_DEFAULT_INSTANCE, u2PortNo) =
                AST_PORT_STATE_DISCARDING;

#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                AstMiRstpNpSetPortState (u2PortNo, AST_PORT_STATE_DISCARDING);
            }
#endif /* NPAPI_WANTED */
        }
    }

#ifdef MSTP_WANTED
    if (AST_IS_MST_ENABLED ())
    {
        AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
        {
            if (pPerStInfo != NULL)
            {
                if (AST_GET_LAST_PROGRMD_STATE (u2MstInst, u2PortNo) !=
                    AST_PORT_STATE_DISCARDING)
                {
                    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                                  "SYS: Port %s: Inst %d: Moving to state DISCARDING \n",
                                  AST_GET_IFINDEX_STR (u2PortNo), u2MstInst);

                    AstSetInstPortStateToL2Iwf (u2MstInst,
                                                u2PortNo,
                                                AST_PORT_STATE_DISCARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2MstInst, u2PortNo) =
                        AST_PORT_STATE_DISCARDING;

#ifdef NPAPI_WANTED
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        AstMiMstpNpSetInstancePortState (u2PortNo,
                                                         u2MstInst,
                                                         AST_PORT_STATE_DISCARDING);
                    }
#endif /* NPAPI_WANTED */
                }
            }
        }
    }
#endif

    AST_DBG_ARG1 (AST_TMR_DBG,
                  "TMR: Port %s: Starting RESTART Timer\n",
                  AST_GET_IFINDEX_STR (u2PortNo));

    AST_GET_RANDOM_TIME (RST_DEFAULT_RESTART_INTERVAL, u4RestartTime);
    AST_DBG_ARG2 (AST_ALL_FLAG,
                  "TMR: Port %s: Starting RESTART Timer with duration %u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u4RestartTime);

    if (AstStartTimer (pPortEntry, u2MstInst,
                       AST_TMR_TYPE_RESTART, u4RestartTime) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (u2PortNo));

        return RST_FAILURE;

    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstRestartStateMachines                            */
/*                                                                           */
/* Description        : This function is called to restart state-machines    */
/*                      in case of impossible event/state combination.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS /RST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
AstRestartStateMachines (VOID)
{
    AST_DBG_ARG1 (AST_ALL_FLAG,
                  "SYS: Restarting State Machines for context: %s \n",
                  AST_CONTEXT_STR ());

    if (AstAssertBegin () != RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");

        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstSetCistPortMacEnabled                             */
/*                                                                           */
/* Description        : This function sets the MacEnabled parameter for the  */
/*                      port for CIST                                        */
/*                                                                           */
/* Input(s)           : pStpConfigInfo - IfIndex & AdminP2P details          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
AstSetCistPortMacEnabled (UINT2 u2LocalPortId, INT4 i4PortAdminPointToPoint)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2LocalPortId);
    if (pCommPortInfo != NULL)
    {
        /* Configuring the value of MacEnabled parameter for CIST
         * due to change in AdminPointToPoint parameter
         * as per the MIB definition of ieee8021MstpCistPortMacEnabled*/

        pCommPortInfo->bCistPortMacEnabled = AST_SNMP_FALSE;
        if (i4PortAdminPointToPoint == RST_P2P_FORCETRUE)
        {
            pCommPortInfo->bCistPortMacEnabled = AST_SNMP_TRUE;
        }

        /* Configuring the value of MacOperational parameter for CIST
         * due to change in OperPointToPoint parameter
         * as per the definition of MAC_Operational parameter in
         * Clause 6.4.2 MAC status parameters of IEEE 802.1 D 2004*/

        pCommPortInfo->bCistPortMacOperational = AST_SNMP_FALSE;
        if ((pCommPortInfo->bCistPortMacEnabled == AST_SNMP_TRUE) &&
            (pAstPortEntry->bOperPointToPoint == RST_TRUE))
        {
            pCommPortInfo->bCistPortMacOperational = AST_SNMP_TRUE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function Name      : MstPUpdtAllSyncedFlag                                */
/*                                                                           */
/* Description        : Sets the All synced variable dynamically             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
MstPUpdtAllSyncedFlag (UINT2 u2MstInst, tAstPerStPortInfo * pPerStPortInfo,
                       UINT2 u2mode)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
    switch (u2mode)
    {
        case MST_SYNC_BSYNCED_UPDATE:
            if (pRstPortInfo->bSynced == AST_TRUE)
            {
                if (pPerStPortInfo->bSyncedFlag != AST_INIT_VAL)
                {
                    pPerStPortInfo->bSyncedFlag = AST_FALSE;
                    pPerStBrgInfo->u2SyncedFlag--;
                }
            }
            else if (pRstPortInfo->bSynced == AST_FALSE)
            {
                if (pPerStPortInfo->bSyncedFlag != AST_TRUE)
                {
                    pPerStPortInfo->bSyncedFlag = AST_TRUE;
                    pPerStBrgInfo->u2SyncedFlag++;
                }

            }

            break;

        case MST_SYNC_ROLE_UPDATE:
            if (pPerStPortInfo->u1PortRole !=
                pPerStPortInfo->u1SelectedPortRole)
            {
                if (pPerStPortInfo->bRoleSyncedFlag != AST_TRUE)
                {
                    pPerStPortInfo->bRoleSyncedFlag = AST_TRUE;
                    pPerStBrgInfo->u2RoleSyncedFlag++;
                }
            }
            else
            {
                if (pPerStPortInfo->bRoleSyncedFlag != AST_INIT_VAL)
                {
                    pPerStPortInfo->bRoleSyncedFlag = AST_INIT_VAL;
                    pPerStBrgInfo->u2RoleSyncedFlag--;
                }

            }
            break;

        case MST_SYNC_BSELECT_UPDATE:
            if (RST_IS_NOT_SELECTED (pRstPortInfo))
            {
                if (pPerStPortInfo->bMstSelectedSyncedFlag != AST_TRUE)
                {
                    pPerStPortInfo->bMstSelectedSyncedFlag = AST_TRUE;
                    pPerStBrgInfo->u2MstSelectedSyncedFlag++;
                }
            }
            else
            {
                if (pPerStPortInfo->bMstSelectedSyncedFlag != AST_INIT_VAL)
                {
                    pPerStPortInfo->bMstSelectedSyncedFlag = AST_INIT_VAL;
                    pPerStBrgInfo->u2MstSelectedSyncedFlag--;
                }
            }
            break;
    }

}
VOID
AstConvertPortListToArray (tLocalPortList PortList,
                           UINT1 *pu4IfPortArray, UINT1 *u1NumPorts)
{
    UINT2               u2Port = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1Counter = 0;
    for (u2BytIndex = 0; u2BytIndex < CONTEXT_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortList[u2BytIndex];
        for (u2BitIndex = 0; ((u2BitIndex < 8)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * 8) + u2BitIndex + 1);
                pu4IfPortArray[u1Counter] = u2Port;
                u1Counter++;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    /* update number of ports */
    *u1NumPorts = u1Counter;
}

VOID
AstGetRoleStr (UINT1 u1Role, UINT1 *u1RoleString)
{
/*    AST_MEMSET (u1RoleString, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN); */
    switch (u1Role)
    {
        case AST_PORT_ROLE_DISABLED:
            AST_STRNCPY (u1RoleString, "Disabled", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_ROLE_ALTERNATE:
            AST_STRNCPY (u1RoleString, "Alternate", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_ROLE_BACKUP:
            AST_STRNCPY (u1RoleString, "Backup", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_ROLE_ROOT:
            AST_STRNCPY (u1RoleString, "Root", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_ROLE_DESIGNATED:
            AST_STRNCPY (u1RoleString, "Designated", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_ROLE_MASTER:
            AST_STRNCPY (u1RoleString, "Master", AST_BRG_ADDR_DIS_LEN);
            break;
        default:
            AST_STRNCPY (u1RoleString, "Unknown", AST_BRG_ADDR_DIS_LEN);
            break;
    }

}

VOID
AstGetStateStr (UINT1 u1State, UINT1 *u1StateString)
{
/*    AST_MEMSET (u1StateString, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN); */
    switch (u1State)
    {
        case AST_PORT_STATE_DISCARDING:
            AST_STRNCPY (u1StateString, "Discarding", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_STATE_LEARNING:
            AST_STRNCPY (u1StateString, "Learning", AST_BRG_ADDR_DIS_LEN);
            break;
        case AST_PORT_STATE_FORWARDING:
            AST_STRNCPY (u1StateString, "Forwarding", AST_BRG_ADDR_DIS_LEN);
            break;
        default:
            AST_STRNCPY (u1StateString, "Unknown", AST_BRG_ADDR_DIS_LEN);
            break;
    }
}

#ifdef NPAPI_WANTED

/*****************************************************************************/
/* Function Name      : AstHandlePacketToNP                                 */
/*                                                                           */
/* Description        : This function is used to handle the STP BPDU to NP   */
/*                                                                           */
/* Input(s)           : pBuf - Buffer containing the pkt to be transmitted   */
/*                      u4IfIndex - Interface index                 */
/*         u4PktSize - Packet size                  */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value       : RST_SUCCESS/RST_FAILURE                              */
/*****************************************************************************/

INT4
AstHandlePacketToNP (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                     UINT4 u4PktSize)
{
    UINT2               u2EgressPort = u4IfIndex;
#ifdef PNAC_WANTED
    UINT2               u2PortAuthStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
#endif
    tAstCfaIfInfo       CfaIfInfo;
    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tAstCfaIfInfo));

    if (AstCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RST_FAILURE;
    }
    /* For port-channel get the physical port id on which packet is to be transmitted */
    if (CfaIfInfo.u1IfType == CFA_LAGG)
    {
#ifdef LA_WANTED
#ifdef NPAPI_WANTED
        if (AstL2IwfGetPotentialTxPortForAgg (u4IfIndex, &u2EgressPort) ==
            L2IWF_FAILURE)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "HandlePktToNP:  Error in getting physical port id for LAG Port : %u\n",
                          u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            return RST_FAILURE;
        }
#endif
#endif
    }
    else if (CfaIfInfo.u1IfType == CFA_ENET)
    {
#ifdef PNAC_WANTED
        if (AstL2IwfGetPortPnacAuthStatus (u4IfIndex, &u2PortAuthStatus) ==
            L2IWF_FAILURE)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "HandlePktToNP:  Error in getting port AuthStatus for port %u\n",
                          u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return RST_FAILURE;
        }
        if (u2PortAuthStatus != PNAC_PORTSTATUS_AUTHORIZED)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "HandlePktToNP: Port : %u Port Status Unauthorised-Discarding packet",
                          u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            return RST_FAILURE;
        }
#endif
        u2EgressPort = u4IfIndex;
    }
#ifdef NPAPI_WANTED
    if (CfaIfInfo.u1IfOperStatus != CFA_IF_DOWN)
    {
        AstCfaPostPacketToNP (pBuf, u2EgressPort, u4PktSize);
    }
#endif
    return RST_SUCCESS;
}

#endif

extern UINT4        gu4Stups;
/****************************************************************************
 * *
 * *    FUNCTION NAME    : RstUtilTicksToDate
 * *
 * *    DESCRIPTION      : This function converts the milliseconds to Date Format
 * *
 * *    INPUT            : u4Ticks
 * *
 * *    OUTPUT           : u1DateFormat- Formatted Date
 * *
 * *    RETURNS          : None
 * ****************************************************************************/
PUBLIC VOID
RstUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks (u4Ticks, &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

/*****************************************************************************/
/*                                                                           */
/* Function         :   AstGetSyslogLevel                                    */
/*                                                                           */
/* Description      :   Gets the Syslog level based on Input Trace type      */
/*                                                                           */
/* Input(s)         :   u2TraceType                                          */
/*                                                                           */
/* Output(s)        :   None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred         :   None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified         :   None                                                 */
/*                                                                           */
/* Return Value(s)  :   Syslog Level based on Trace type                     */
/*****************************************************************************/
UINT2
AstGetSyslogLevel (UINT2 u2TraceType)
{
    UINT2               u2SyslogLevel = 0;
    switch (u2TraceType)
    {
        case AST_CONTROL_PATH_TRC:
            u2SyslogLevel = SYSLOG_INFO_LEVEL;
            break;

        case AST_ALL_FAILURE_TRC:
            u2SyslogLevel = SYSLOG_CRITICAL_LEVEL;
            break;

        default:
            break;
    }
    return u2SyslogLevel;
}
