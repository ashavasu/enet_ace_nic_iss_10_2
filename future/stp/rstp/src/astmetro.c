/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmetro.c,v 1.4 2015/02/09 12:47:19 siva Exp $
 *
 * Description: This file contains Protocol routines that is common to both PB 
 *              and PBB and will be used by ASTP module if either of them is
 *              defined.
 *******************************************************************/
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstHandlePortTypeChange                              */
/*                                                                           */
/* Description        : This Function handles the Provider Bridge Port type  */
/*                      Changes for a Port.                                  */
/*                                                                           */
/* Input(s)           : pPortEntry - Port for which port type changed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
AstHandlePortTypeChange (tAstPortEntry * pPortEntry, UINT1 u1NewPortType)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    INT4                i4RetVal = RST_SUCCESS;

    /* There is a Change in Port types */
    switch (u1NewPortType)
    {

        case AST_PROVIDER_NETWORK_PORT:

            /* No configurations needed do nothing */
            break;

        case AST_CNP_STAGGED_PORT:
        case AST_CNP_CTAGGED_PORT:
        case AST_CNP_PORTBASED_PORT:

            /* Will have configurations based on the component type */
            if ((AST_IS_I_COMPONENT ()) == RST_TRUE)
            {
                /* Configure the port priority here */
                pAstPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo,
                                             RST_DEFAULT_INSTANCE);
                pAstPerStPortInfo->u1PortPriority = AST_PB_CVLAN_PORT_PRIORITY;

            }
            else
            {
                /* As per section 16.3 of IEEE 802.1ad/D6, set 
                 * restrictedRole and restrictedTCN for customer
                 * network ports. */
                if (AST_IS_RST_STARTED ())
                {
                    if (RstConfigureRestrictedRoleAndTCN (pPortEntry, RST_TRUE,
                                                          RST_TRUE) !=
                        RST_SUCCESS)
                    {
                        AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "MSG: RstConfigureRestrictedRoleAndTCN function "
                                 "returned FAILURE\n");
                        return RST_FAILURE;
                    }
                }
#ifdef MSTP_WANTED
                else if (AST_IS_MST_STARTED ())
                {
                    /* From PNP/PCEP/PCNP/PPNP to CNP
                     * The CNP Ports should not be influenced by the Customer 
                     * Sent Provider STP BPDUs. So Restricted Role and 
                     * Restricted TCN will be always TRUE*/

                    if (MstConfigureRestrictedRoleAndTCN (pPortEntry, MST_TRUE,
                                                          MST_TRUE) !=
                        RST_SUCCESS)
                    {
                        AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "MSG: MstConfigureRestrictedRoleAndTCN function "
                                 "returned FAILURE\n");
                        return RST_FAILURE;
                    }
                }
#endif /*MSTP_WANTED */
            }
            break;

        case AST_PROP_CUSTOMER_NETWORK_PORT:
        case AST_PROP_CUSTOMER_EDGE_PORT:

            /* As per section 16.3 of IEEE 802.1ad/D6, set 
             * restrictedRole and restrictedTCN for customer
             * network ports. */
            if (AST_IS_RST_STARTED ())
            {
                if (RstConfigureRestrictedRoleAndTCN (pPortEntry, RST_TRUE,
                                                      RST_TRUE) != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstConfigureRestrictedRoleAndTCN function "
                             "returned FAILURE\n");
                    return RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                /* From PNP/PCEP/PCNP/PPNP to CNP
                 * The CNP Ports should not be influenced by the Customer 
                 * Sent Provider STP BPDUs. So Restricted Role and 
                 * Restricted TCN will be always TRUE*/

                if (MstConfigureRestrictedRoleAndTCN (pPortEntry, MST_TRUE,
                                                      MST_TRUE) != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstConfigureRestrictedRoleAndTCN function "
                             "returned FAILURE\n");
                    return RST_FAILURE;
                }
            }
#endif /*MSTP_WANTED */

            break;

        case AST_CUSTOMER_EDGE_PORT:

            /* Select C-VLAN context depends on the port type. 
             * So first set the port type. */
            AST_GET_BRG_PORT_TYPE (pPortEntry) = u1NewPortType;

            /* Create C-VLAN component and run RSTP on it. */
            if (RstPbCVlanComponentInit (pPortEntry) != RST_SUCCESS)
            {
                /* Revert the port type, as the C-VLAN comp init
                 * has failed. */
                AST_GET_BRG_PORT_TYPE (pPortEntry) = AST_PROVIDER_NETWORK_PORT;
                return RST_FAILURE;
            }

            break;

        case AST_PROP_PROVIDER_NETWORK_PORT:

            /* No specific configurations needed */
            break;

        case AST_VIRTUAL_INSTANCE_PORT:

            i4RetVal = AstRestoreVipDefaults (pPortEntry->u2PortNo);
            break;

        case AST_CUSTOMER_BACKBONE_PORT:

            /* In case of CBP Ports, standard specifies that it should not
             * accidentally assign to another n/w if connected directly 
             * without an I-Component.
             * */
            if (AST_IS_RST_STARTED ())
            {
                if (RstConfigureRestrictedRoleAndTCN (pPortEntry, RST_TRUE,
                                                      RST_FALSE) != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: RstConfigureRestrictedRoleAndTCN function "
                             "returned FAILURE\n");
                    return RST_FAILURE;
                }
            }
#ifdef MSTP_WANTED
            else if (AST_IS_MST_STARTED ())
            {
                if (MstConfigureRestrictedRoleAndTCN (pPortEntry, MST_TRUE,
                                                      RST_FALSE) != RST_SUCCESS)
                {
                    AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                             AST_ALL_FAILURE_DBG,
                             "MSG: MstConfigureRestrictedRoleAndTCN function "
                             "returned FAILURE\n");
                    return RST_FAILURE;
                }
            }
#endif /*MSTP_WANTED */

            if (AstSetPortBpduTxStatus (pPortEntry->u2PortNo, RST_FALSE) !=
                RST_SUCCESS)
            {
                AST_DBG (AST_EVENT_HANDLING_DBG | AST_MGMT_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: RstConfigureRestrictedRoleAndTCN function "
                         "returned FAILURE\n");
                return RST_FAILURE;
            }

            break;

        default:
            i4RetVal = RST_FAILURE;

    }

    AST_GET_BRG_PORT_TYPE (pPortEntry) = u1NewPortType;

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : RstProvCheckAndAlterLogicalIfPortRole                */
/*                                                                           */
/* Description        : This function implements the enhancement to RSTP to  */
/*                      PB & PBB functionalities. This function will be      */
/*                      called if the a port (u2PortNum) is selected as an   */
/*                      alternate port. This function does the following:    */
/*                          - If the root port is PEP, and the current port  */
/*                            is also a PEP, then set the port role as root  */
/*                            for the current port.                          */
/*                                                                           */
/*                          - If the root port is VIP, and the current port  */
/*                            is also a VIP, then set the port role as root  */
/*                            for the current port.                          */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port whose port role is       */
/*                                   selected as Alternate port in the Update*/
/*                                   roles tree function.                    */
/*                      u2RootPort - Id of the port whose role is selected   */
/*                                   as root port in UpdateRoles tree        */
/*                                   function.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstProvCheckAndAlterLogicalIfPortRole (UINT2 u2PortNum, UINT2 u2RootPort)
{
    tAstPortEntry      *pRootPortEntry = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1               u1LogicalIfType = AST_INIT_VAL;

    pRootPortEntry = AST_GET_PORTENTRY (u2RootPort);

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if ((AST_IS_CVLAN_COMPONENT () != RST_TRUE) &&
        (AST_IS_I_COMPONENT () != RST_TRUE))
    {
        /* Improper bridge modes. Do nothing and return safely. */
        return;
    }

    if ((AST_IS_CVLAN_COMPONENT () == RST_TRUE))
    {
        u1LogicalIfType = AST_PROVIDER_EDGE_PORT;
    }
    else if ((AST_IS_I_COMPONENT () == RST_TRUE))
    {
        u1LogicalIfType = AST_VIRTUAL_INSTANCE_PORT;
    }

    if ((AST_GET_BRG_PORT_TYPE (pRootPortEntry) == u1LogicalIfType) &&
        (AST_GET_BRG_PORT_TYPE (pPortInfo) == u1LogicalIfType))
    {
        /* This implies two possibilities.
         * 1. This is a provider edge port and another Provider Edge Port is 
         * selected as root port.
         * 2. This is a virtual instance port and another virtual instance Port 
         * is selected as root port.
         * Hence as per section 13.38.2 make this port as root port.*/
        pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_ROOT;
    }
    else
    {
        pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_ALTERNATE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : RstProviderSetSyncTree                               */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmSetSyncTree function, when a VIP/PEP     */
/*                      invokes setSyncTree Procedure. This routine will set */
/*                      the value of sync as true for CEP if the calling port*/
/*                      is PEP & sync as true for all the CNPs if the calling*/
/*                      port is VIP.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstProleTrSmSetSyncTree function  and   */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetSyncTree (UINT2 u2Port)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /* Set sync true for this port. */
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Port, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo != NULL)
    {
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pRstPortInfo->bSync = RST_TRUE;
    }
    else
    {
        /* This scenario is not Valid. Invoking port itself is not valid
         * Safely exit.
         * */
        return RST_FAILURE;
    }

    /* Reinitialising the values */
    pRstPortInfo = NULL;

    if ((AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE))
    {
        /* This is Provider edge port (PEP). */
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                     RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         RST_DEFAULT_INSTANCE);

        pRstPortInfo->bSync = RST_TRUE;

        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_SYNC_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    if (AST_IS_VIRTUAL_INST_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE)
    {

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

            if (pPerStPortInfo == NULL)
            {
                /* Invalid Port */
                continue;
            }
            if (u2PortNum == u2Port)
            {
                /* Is calling port, continue */
                continue;
            }

            /* If the calling port is PEP, then we have to set for CEP alone.
             * If the calling port is VIP, then we have to set the same for 
             * CNPs. */
            if ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2Port))) ==
                (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2PortNum))))
            {
                continue;
            }

            /* Port types are different */
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bSync = RST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: Sync Set...\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstPortRoleTrMachine (RST_PROLETRSM_EV_SYNC_SET,
                                      pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Role Transition Machine "
                              "returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Role Transition Machine "
                              "returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

            if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
            {
                /* If this is a C-VLAN component, then it is enough to set sync
                 * variable for CEP alone. In C-VLAN component, the first port 
                 * will be always CEP. Hence if we set sync for first port, it 
                 * is enough and hence, we can safely return
                 * */
                return RST_SUCCESS;
            }

            /* This is an I-Component, scan through all the ports in the 
             * component and set sync for all the physical CNPs present
             * */
        }
    }
    return RST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RstProviderSetReRootTree                             */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstProleTrSmSetReRootTree function, when a VIP/PEP   */
/*                      invokes setReRootTreeProcedure. This routine will set*/
/*                      the value of reRoot as true for CEP if calling port  */
/*                      is PEP & sync as true for all the CNPs if the calling*/
/*                      port is VIP.                                         */
/*                                                                           */
/* Input(s)           : u2PortNum -  Id of the port that invoked             */
/*                                   RstProleTrSmSetReRootTree function and  */
/*                                   hence this function.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetReRootTree (UINT2 u2Port)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /* Set sync true for this port. */
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Port, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo != NULL)
    {
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pRstPortInfo->bReRoot = RST_TRUE;
    }
    else
    {
        /* This scenario is not Valid. Invoking port itself is not valid
         * Safely exit.
         * */
        return RST_FAILURE;
    }

    /* Reinitialising the values */
    pRstPortInfo = NULL;

    if ((AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE))
    {
        /* This is Provider edge port (PEP). */
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                     RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         RST_DEFAULT_INSTANCE);

        if (pRstPortInfo->bPortEnabled != RST_TRUE)
        {
            /* This condition is not possible. */
            return RST_FALSE;
        }

        pRstPortInfo->bReRoot = RST_TRUE;

        if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOT_SET,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            return RST_FAILURE;
        }
    }

    if (AST_IS_VIRTUAL_INST_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE)
    {

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

            if (pPerStPortInfo == NULL)
            {
                /* Invalid Port */
                continue;
            }
            if (u2PortNum == u2Port)
            {
                /* Is calling port, continue */
                continue;
            }

            /* If the calling port is PEP, then we have to set for CEP alone.
             * If the calling port is VIP, then we have to set the same for 
             * CNPs. */
            if ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2Port))) ==
                (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2PortNum))))
            {
                continue;
            }

            /* Port types are different */
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bReRoot = RST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %s: ReRoot Set...\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            if (RstPortRoleTrMachine (RST_PROLETRSM_EV_REROOT_SET,
                                      pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Role Transition Machine "
                              "returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Role Transition Machine "
                              "returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return RST_FAILURE;
            }

            if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
            {
                /* If this is a C-VLAN component, then it is enough to set 
                 * reRoot variable for CEP alone. In C-VLAN component, the 
                 * first port will be always CEP. Hence if we set reRoot for 
                 * first port, it is enough and hence, we can safely return
                 * */
                return RST_SUCCESS;
            }

            /* This is an I-Component, scan through all the ports in the 
             * component and set reRoot for all the physical CNPs present
             * */
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstProviderSetTcPropTree                             */
/*                                                                           */
/* Description        : This function will be called by                      */
/*                      RstTopoChSmSetTcPropTree function, when a VIP/PEP    */
/*                      invokes this function. This function sets tcProp for */
/*                      the all the CNPs in this I-Component if the calling  */
/*                      port is VIP and sets tcProp for CEP, if the calling  */
/*                      port is PEP.                                         */
/*                                                                           */
/* Input(s)           : u2Port -  Id of the port that invoked                */
/*                                RstTopoChSmSetTcPropTree function and hence*/
/*                                this function.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS/ RST_FAILURE.                            */
/*****************************************************************************/
INT4
RstProviderSetTcPropTree (UINT2 u2Port)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = 0;

    /* Set sync true for this port. */
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Port, RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        return RST_FAILURE;
    }

    if ((AST_IS_PROVIDER_EDGE_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE))
    {
        /* This is Provider edge port (PEP). */
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                     RST_DEFAULT_INSTANCE);

        pRstPortInfo =
            AST_GET_PERST_RST_PORT_INFO (AST_PB_CEP_CVLAN_LOCAL_PORT_NUM,
                                         RST_DEFAULT_INSTANCE);

        if (pRstPortInfo->bPortEnabled != RST_TRUE)
        {
            /* This condition is not possible. */
            return RST_FAILURE;
        }

        if ((AST_IS_PORT_UP (pPerStPortInfo->u2PortNo)) &&
            (pRstPortInfo->bPortEnabled == RST_TRUE))
        {
            pRstPortInfo->bTcProp = RST_TRUE;
            pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

            if (pPerStPortInfo->u1TopoChSmState != RST_TOPOCHSM_STATE_LEARNING)
            {
                if (RstTopoChMachine
                    ((UINT1) RST_TOPOCHSM_EV_TCPROP, pPerStPortInfo)
                    != RST_SUCCESS)

                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TCSM: RstTopoChMachine Entry function "
                             "returned FAILURE!\n");
                    AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: RstTopoChMachine Entry function "
                             "returned FAILURE!\n");
                    return RST_FAILURE;
                }
            }
            else
            {
                pAstCommPortInfo = AST_GET_COMM_PORT_INFO
                    (pPerStPortInfo->u2PortNo);
                if (pAstCommPortInfo == NULL)
                    return RST_FAILURE;

                pRstPortInfo->bRcvdTc = RST_FALSE;
                pAstCommPortInfo->bRcvdTcn = RST_FALSE;
                pAstCommPortInfo->bRcvdTcAck = RST_FALSE;
                pRstPortInfo->bTc = RST_FALSE;
                pRstPortInfo->bTcProp = RST_FALSE;

                if (((pPerStPortInfo->u1PortRole ==
                      (UINT1) AST_PORT_ROLE_ROOT) ||
                     (pPerStPortInfo->u1PortRole ==
                      (UINT1) AST_PORT_ROLE_DESIGNATED))
                    &&
                    (pRstPortInfo->bForward == RST_TRUE)
                    && (pPortInfo->bOperEdgePort == RST_FALSE))
                {

                    pAstCommPortInfo->bNewInfo = RST_TRUE;
                    pPerStPortInfo->u1TopoChSmState =
                        (UINT1) RST_TOPOCHSM_STATE_LEARNING;

                    if (RstTopoChSmNewTcWhile (pPerStPortInfo) != RST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }

                    if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                                pPortInfo,
                                                RST_DEFAULT_INSTANCE)
                        != RST_SUCCESS)
                    {
                        return RST_FAILURE;
                    }

                    pRstPortInfo->bTcProp = RST_TRUE;
                    pPerStPortInfo->u1TopoChSmState =
                        (UINT1) RST_TOPOCHSM_STATE_DETECTED;
                    RstTopoChSmMakeActive (pPerStPortInfo);
                }
                if (RstTopoChSmChkInactive (pPerStPortInfo) != RST_SUCCESS)
                {
                    return RST_FAILURE;
                }
            }
        }
    }

    if (AST_IS_VIRTUAL_INST_PORT (pPerStPortInfo->u2PortNo) == RST_TRUE)
    {

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

            if (pPerStPortInfo == NULL)
            {
                /* Invalid Port */
                continue;
            }
            if (u2PortNum == u2Port)
            {
                /* Is calling port, continue */
                continue;
            }

            /* If the calling port is PEP, then we have to set for CEP alone.
             * If the calling port is VIP, then we have to set the same for i
             * CNPs. */
            if ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2Port))) ==
                (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (u2PortNum))))
            {
                continue;
            }

            /* Port types are different */
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if ((AST_IS_PORT_UP (u2PortNum)) &&
                (pRstPortInfo->bPortEnabled == RST_TRUE))
            {
                pRstPortInfo->bTcProp = RST_TRUE;

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                              "TCSM: Propagating Topology Change Info "
                              "(TcProp) on Port %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_TCSM_DBG,
                              "TCSM: Propagating Topology Change Info "
                              "(TcProp) on Port %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (RstTopoChMachine
                    ((UINT1) RST_TOPOCHSM_EV_TCPROP, pPerStPortInfo)
                    != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TCSM: RstTopoChMachine Entry function "
                             "returned FAILURE!\n");
                    AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: RstTopoChMachine Entry function "
                             "returned FAILURE!\n");
                    return RST_FAILURE;
                }
            }                    /* Port operationally up */

            if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
            {
                /* If this is a C-VLAN component, then it is enough to set i
                 * tcProp variable for CEP alone. In C-VLAN component, the 
                 * first port will be always CEP. Hence if we set tcProp for 
                 * first port, it is  enough and hence, we can safely return
                 * */
                return RST_SUCCESS;
            }

            /* This is an I-Component, scan through all the ports in the 
             * component and set reRoot for all the physical CNPs present
             * */
        }
    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstResetCepPortCounters                              */
/*                                                                           */
/* Description        : This function resets all CEP port statistics         */
/*                      counters                                             */
/*                                                                           */
/* Input(s)           : pCepPortInfo - CEP Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE/RST_SUCCESS                              */
/*****************************************************************************/

INT4
RstResetCepPortCounters (tAstPortEntry * pCepPortEntry)
{
    tAstPortEntry      *pPepPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4Svid = AST_INIT_VAL;
    INT4                i4NextSvid = AST_INIT_VAL;

    if (RST_SUCCESS != AstPbSelectCvlanContext (pCepPortEntry))
    {
        return RST_FAILURE;
    }
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    if (NULL == pPerStBrgInfo)
    {
        return RST_FAILURE;
    }
    /*Clearing CVLAN bridge counters for each CEP port */
    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    i4NextSvid = (tVlanId) AstGetNextPepPortEntry (i4Svid);
    AstPbRestoreContext ();
    do
    {
        /* Retrieving PEP portEntry */
        if (NULL == (pPepPortEntry = AstGetPepPortEntryWithCtxtSwitch
                     (pCepPortEntry, i4NextSvid)))
        {
            AstPbRestoreContext ();
            return RST_FAILURE;
        }
        pPepPortEntry->u4NumRstBpdusRxd = AST_INIT_VAL;
        pPepPortEntry->u4NumConfigBpdusRxd = AST_INIT_VAL;
        pPepPortEntry->u4NumTcnBpdusRxd = AST_INIT_VAL;
        pPepPortEntry->u4NumRstBpdusTxd = AST_INIT_VAL;
        pPepPortEntry->u4NumConfigBpdusTxd = AST_INIT_VAL;
        pPepPortEntry->u4NumTcnBpdusTxd = AST_INIT_VAL;
        pPepPortEntry->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
        pPepPortEntry->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
        pPepPortEntry->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
        pPepPortEntry->u4ProtocolMigrationCount = AST_INIT_VAL;
        if (NULL == AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE))
        {
            return RST_FAILURE;
        }
        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO (pPepPortEntry->u2PortNo,
                                     RST_DEFAULT_INSTANCE);
        if (NULL == pPerStPortInfo)
        {
            return RST_FAILURE;
        }
        pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
        pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
        /*Retrieving the next Svid to get the next PEP portEntry */
        i4Svid = i4NextSvid;
        i4NextSvid = (tVlanId) AstGetNextPepPortEntry (i4Svid);
        AstPbRestoreContext ();
    }
    while (AST_INIT_VAL != i4NextSvid);
    return RST_SUCCESS;
}
