/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpl2wr.c,v 1.8 2012/07/16 10:06:22 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              AST Module.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : AstL2IwfCreateSpanningTreeInstance                   */
/*                                                                           */
/* Description        : This function calls the L2IWF module to create the   */
/*                      spanning tree instance.                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u2InstanceId- Instance Identifier                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    return (L2IwfCreateSpanningTreeInstance (u4ContextId, u2InstanceId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfDeleteSpanningTreeInstance                   */
/*                                                                           */
/* Description        : This function calls the L2IWF module to delete the   */
/*                      spanning tree instance.                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u2InstanceId- Instance Identifier                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    return (L2IwfDeleteSpanningTreeInstance (u4ContextId, u2InstanceId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstL2IwfGetNextCVlanPort                   */
/*                                                                           */
/*    Description               : This function is used to get the next PEP  */
/*                                Port in a given CVLAN component            */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                u2PrevPort - Previous PEP HL Port Id       */
/*                                                                           */
/*    Output(s)                 :*pu2NextPort - Next PEP's HL Port Id        */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/

INT4
AstL2IwfGetNextCVlanPort (UINT4 u4CepIfIndex, UINT2 u2PrevPort,
                          UINT2 *pu2NextPort, UINT4 *pu4NextCepIfIndex)
{
    return (L2IwfGetNextCVlanPort (u4CepIfIndex, u2PrevPort,
                                   pu2NextPort, pu4NextCepIfIndex));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPbPortOperEdgeStatus                      */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : *pbOperEdge - OSIX_TRUE/OSIX_FALSE                   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
AstL2IwfGetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                 BOOL1 * pbOperEdge)
{
    return (L2IwfGetPbPortOperEdgeStatus (u4CepIfIndex, SVlanId, pbOperEdge));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstL2IwfGetPbPortOperStatus                */
/*                                                                           */
/*    Description               : This function is used to get the PEP       */
/*                                Port Oper Status which is updated by VLAN  */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                SVlanId - Service VLAN Identifier          */
/*                                u1Module - STP_MODULE                      */
/*                                                                           */
/*    Output(s)                 :*pu1OperStatus - PEP port oper status       */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

INT4
AstL2IwfGetPbPortOperStatus (UINT1 u1Module, UINT4 u4CepIfIndex,
                             tVlanId SVlanId, UINT1 *pu1OperStatus)
{
    return (L2IwfGetPbPortOperStatus (u1Module, u4CepIfIndex, SVlanId,
                                      pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPbPortState                               */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

UINT1
AstL2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId)
{
    return (L2IwfGetPbPortState (u4CepIfIndex, SVlanId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPbPortType                                */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
AstL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetPbPortType (u4IfIndex, pu1PortType));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetInterfaceType                             */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
AstL2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType)
{
    return (L2IwfGetInterfaceType (u4ContextId, pu1InterfaceType));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfPbGetVlanServiceType                         */
/*                                                                           */
/* Description        : This function is used to get the type of service the */
/*                      VLAN provides. The service type is updated by VLAN   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : *pu1ServiceType - VLAN_E_LINE /VLAN_E_LAN            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
AstL2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                              UINT1 *pu1ServiceType)
{
    return (L2IwfPbGetVlanServiceType (u4ContextId, VlanId, pu1ServiceType));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfPbSetInstCepPortState                        */
/*                                                                           */
/* Description        : This function is called to configure CEP Port State  */
/*                      for all Instances                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - CEP Port IfIndex                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/

INT4
AstL2IwfPbSetInstCepPortState (UINT4 u4CepIfIndex, UINT1 u1PortState)
{
    return (L2IwfPbSetInstCepPortState (u4CepIfIndex, u1PortState));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetPbPortOperEdgeStatus                      */
/*                                                                           */
/* Description        : This function is used to set Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                      bOperEdge - OSIX_TRUE/OSIX_FALSE                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
AstL2IwfSetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                 BOOL1 bOperEdge)
{
    return (L2IwfSetPbPortOperEdgeStatus (u4CepIfIndex, SVlanId, bOperEdge));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetPbPortState                               */
/*                                                                           */
/* Description        : This function is used to set Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                      u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
AstL2IwfSetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId, UINT1 u1PortState)
{
    return (L2IwfSetPbPortState (u4CepIfIndex, SVlanId, u1PortState));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetProtocolEnabledStatusOnPort               */
/*                                                                           */
/* Description        : This routine will be called by protocol routines to  */
/*                      update the corresponding protocol's enabled status   */
/*                      on a port.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      protocols enabled status is to be    */
/*                                      updated.                             */
/*                      u2Protocol    - Protocol whose port status needs to  */
/*                                      updated.                             */
/*                      u1Status      - Protocol status (enabled/disable)    */
/*                                      status for the port.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                        UINT2 u2Protocol, UINT1 u1Status)
{
    return (L2IwfSetProtocolEnabledStatusOnPort (u4ContextId, u2LocalPortId,
                                                 u2Protocol, u1Status));
}

#ifdef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstL2IwfFillConfigDigest                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to fill the     */
/*                      config digest information with the MSTI.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      au1DigPtr - Array in which the Config Digest info is */
/*                      to be filled.                                        */
/*                      i4NumEntries - Number of entries to be filled.       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
AstL2IwfFillConfigDigest (UINT4 u4ContextId, UINT1 au1DigPtr[],
                          INT4 i4NumEntries)
{
    return (L2IwfFillConfigDigest (u4ContextId, au1DigPtr, i4NumEntries));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfMiGetVlanListInInstance                      */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to the */
/*                      given Instance.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of vlans */
/*                                    mapped needs to be obtained.           */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans Mapped                               */
/*****************************************************************************/
UINT2
AstL2IwfMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                                 UINT1 *pu1VlanList)
{
    return (L2IwfMiGetVlanListInInstance (u4ContextId, u2MstInst, pu1VlanList));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfMiGetVlanMapInInstance                      */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to the */
/*                      given Instance.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of vlans */
/*                                    mapped needs to be obtained.           */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans Mapped                               */
/*****************************************************************************/
UINT2
AstL2IwfMiGetVlanMapInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                                 UINT1 *pu1VlanList)
{
    return (L2IwfMiGetVlanMapInInstance (u4ContextId, u2MstInst, pu1VlanList));
}
#endif

/*****************************************************************************/
/* Function Name      : AstL2IwfMiGetVlanInstMapping                         */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
AstL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanInstMapping (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetInstPortState                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      instance port state on a port.                       */
/*                                                                           */
/* Input(s)           : u2MstInst  - InstanceId for which the port state is  */
/*                      to be obtained.                                      */
/*                      u4IfIndex  - Global IfIndex of the port whose port   */
/*                      state is to be obtained.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
AstL2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex)
{
    return (L2IwfGetInstPortState (u2MstInst, u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetPortOperEdgeStatus                        */
/*                                                                           */
/* Description        : This routine returns the port's oper edge status     */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2IfIndex - Global IfIndex of the port whose oper    */
/*                                    edge status is to be obtained.         */
/*                                                                           */
/* Output(s)          : bOperEdge - Boolean value indicating whether the     */
/*                                  port is an Edge Port                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
AstL2IwfGetPortOperEdgeStatus (UINT2 u2IfIndex, BOOL1 * pbOperEdge)
{
    return (L2IwfGetPortOperEdgeStatus (u2IfIndex, pbOperEdge));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetInstPortState                             */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's state for that instance in the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2MstInst - InstanceId for which the port state is   */
/*                                  to be updated.                           */
/*                    : u2IfIndex - Global IfIndex of the port whose state   */
/*                                  is to be updated.                        */
/*                      u1PortState - Port state to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfSetInstPortState (UINT2 u2MstInst, UINT2 u2IfIndex, UINT1 u1PortState)
{
    return (L2IwfSetInstPortState (u2MstInst, u2IfIndex, u1PortState));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetPortOperEdgeStatus                        */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's oper edge status in the L2Iwf common database.*/
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Index of the port whose oper edge    */
/*                                    status is to be updated.               */
/*                      bOperEdge - Boolean value indicating whether the     */
/*                                  port is an Edge Port                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
AstL2IwfSetPortOperEdgeStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                               BOOL1 bOperEdge)
{
    return (L2IwfSetPortOperEdgeStatus (u4ContextId, u2LocalPortId, bOperEdge));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfSetVlanInstMapping                           */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Vlan to Instance mapping for the given VLAN in the   */
/*                      L2Iwf common database or the given context.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                      u2MstInst - InstanceId to be set for this Vlan       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstInst)
{
    return (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2MstInst));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfVlanToInstMappingInit                        */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Vlan to Instance mapping for the given VLAN in the   */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                      u2MstInst - InstanceId to be set.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
AstL2IwfVlanToInstMappingInit (UINT4 u4ContextId)
{
    return (L2IwfVlanToInstMappingInit (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : AstL2IwfGetDefaultVlanId                             */
/*                                                                           */
/* Description        : This function returns the Default Vlan Id            */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Default Vlan Id                                      */
/*****************************************************************************/
VOID
AstL2IwfGetDefaultVlanId (UINT2 *pDefaultVlanId)
{
    L2IwfGetDefaultVlanId (pDefaultVlanId);
}

#ifdef PVRST_WANTED
/*****************************************************************************/
/* Function Name      : AstL2IwfAllocInstActiveStateMem                      */
/*                                                                           */
/* Description        : This routine is called from PVRST to allocate the    */
/*                      memory for Active state info of instance in the      */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u1Mode - Specifies Mode (PVRST START/SHUTDOWN)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfAllocInstActiveStateMem (UINT4 u4ContextId, UINT1 u1Mode)
{
    return L2IwfAllocInstActiveStateMem (u4ContextId, u1Mode);
}

/*****************************************************************************/
/* Function Name      : AstL2IwfAllocInstPortStateMem                        */
/*                                                                           */
/* Description        : This routine is called from PVRST to allocate the    */
/*                      memory for Port's state info of instance in the      */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Local HLIndex of the port            */
/*                      u1Mode - Specifies mode PVRST START/SHUTDOWN         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
AstL2IwfAllocInstPortStateMem (UINT4 u4ContextId, UINT2 u2LocalPortId,
                               UINT1 u1Mode)
{
    return L2IwfAllocInstPortStateMem (u4ContextId, u2LocalPortId, u1Mode);
}
#endif
/*****************************************************************************/
/* Function Name      : AstL2IwfGetNextValidPortForContext                   */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the L2 modules to know the active ports in the    */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
AstL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    UINT2 *pu2NextLocalPort,
                                    UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_XSTP;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}
