/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvextn.h,v 1.5 2016/09/14 10:50:25 siva Exp $
 *
 * Description: This file contains External Declarations    
 *              for the PVRST Module 
 *
 *****************************************************************************/

#ifndef _ASTVEXTN_H
#define _ASTVEXTN_H


/* TRAP Related */

VOID RstCliPrintTestFnErrorMsg (UINT4 u4ErrCode,
                                UINT1 *pu1Msg, UINT1 **ppu1OutMsg);
extern VOID
arHmac_MD5 (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Key, INT4 i4KeyLen,
         UINT1 *pu1Digest);
extern tVlanId             gVlanDefVlanId;
#endif /* _ASTVEXTN_H */

