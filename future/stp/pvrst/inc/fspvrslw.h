/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspvrslw.h,v 1.16 2017/09/19 13:50:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstNoOfActiveInstances ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstBrgAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPvrstUpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsPvrstDownCount ARG_LIST((UINT4 *));

INT1
nmhGetFsPvrstPathCostDefaultType ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstDynamicPathCostCalculation ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstTrace ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstDebug ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstBufferOverFlowCount ARG_LIST((UINT4 *));

INT1
nmhGetFsPvrstMemAllocFailureCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstPathCostDefaultType ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstDynamicPathCostCalculation ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstTrace ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstPathCostDefaultType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstDynamicPathCostCalculation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPvrstSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstPathCostDefaultType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstDynamicPathCostCalculation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsFuturePvrstPortTable. */
INT1
nmhValidateIndexInstanceFsFuturePvrstPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFuturePvrstPortTable  */

INT1
nmhGetFirstIndexFsFuturePvrstPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFuturePvrstPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstPortAdminEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortOperEdgePortStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstBridgeDetectionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortEnabledStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstEncapType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortAdminPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortOperPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortInvalidBpdusRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsPvrstPortRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortLoopGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortLoopInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstBpduFilter ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortAutoEdge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortTypeInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortPVIDInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortBpduGuardAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstPortAdminEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortEnabledStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstRootGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstEncapType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortAdminPointToPoint ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstBpduFilter ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortAutoEdge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstPortBpduGuardAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstPortAdminEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortEnabledStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstEncapType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortAdminPointToPoint ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsPvrstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstBpduFilter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortAutoEdge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstPortBpduGuardAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFuturePvrstPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPvrstInstBridgeTable. */
INT1
nmhValidateIndexInstanceFsPvrstInstBridgeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPvrstInstBridgeTable  */

INT1
nmhGetFirstIndexFsPvrstInstBridgeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPvrstInstBridgeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstInstBridgePriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstRootPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstBridgeMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstBridgeHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstBridgeForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstTxHoldCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstNewRootCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstInstanceUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstInstanceDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortRoleSelSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPvrstInstRootMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstRootHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstRootForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstOldDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPvrstInstFlushIndicationThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstTotalFlushCount ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstInstBridgePriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstBridgeMaxAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstBridgeHelloTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstBridgeForwardDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstTxHoldCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstFlushIndicationThreshold ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstInstBridgePriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstBridgeMaxAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstBridgeHelloTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstTxHoldCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPvrstInstBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPvrstInstPortTable. */
INT1
nmhValidateIndexInstanceFsPvrstInstPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPvrstInstPortTable  */

INT1
nmhGetFirstIndexFsPvrstInstPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPvrstInstPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstInstPortEnableStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPvrstInstPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPvrstInstPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPvrstInstPortOperVersion ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortProtocolMigration ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortReceivedBpdus ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortRxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortRxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTransmittedBpdus ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTxSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortProtMigrationSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstProtocolMigrationCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstCurrentPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortInfoSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortRoleTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortStateTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortTopologyChangeSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortHelloTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortMaxAge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortForwardDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortHoldTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortAdminPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortOldPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortLoopInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortRootInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstInstPortTCDetectedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTCReceivedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTCDetectedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortTCReceivedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortProposalPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortProposalPktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortProposalPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortProposalPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortAgreementPktSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortAgreementPktRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortAgreementPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPvrstInstPortAgreementPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));



/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstInstPortEnableStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstPortPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstPortPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstPortProtocolMigration ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPvrstInstPortAdminPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstInstPortEnableStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstPortPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstPortPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPvrstInstPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPvrstInstPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstGlobalBpduGuard ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstForceProtocolVersion ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstFlushInterval ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstGlobalBpduGuard ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstForceProtocolVersion ARG_LIST((INT4 ));

INT1
nmhSetFsPvrstFlushInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstGlobalBpduGuard ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstForceProtocolVersion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPvrstFlushInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPvrstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstGlobalBpduGuard ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstForceProtocolVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPvrstFlushInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstSetTraps ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstGenTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsPvrstErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPvrstSetTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPvrstSetTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPvrstSetTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPvrstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsPvrstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPvrstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsPvrstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPvrstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPvrstPktErrVal ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPvrstPortRoleTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsPvrstPortRoleTrapNotificationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPvrstPortRoleTrapNotificationTable  */

INT1
nmhGetFirstIndexFsPvrstPortRoleTrapNotificationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPvrstPortRoleTrapNotificationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPvrstPortRoleType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPvrstOldRoleType ARG_LIST((INT4  , INT4 ,INT4 *));
