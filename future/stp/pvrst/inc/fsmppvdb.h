/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppvdb.h,v 1.17 2017/09/19 13:50:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPPVDB_H
#define _FSMPPVDB_H

UINT1 FsMIFuturePvrstTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFuturePvrstPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPvrstInstBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPvrstInstPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsPvrstTrapsControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPvrstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPvrstPortRoleTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmppv [] ={1,3,6,1,4,1,2076,154};
tSNMP_OID_TYPE fsmppvOID = {8, fsmppv};


UINT4 FsMIPvrstGlobalTrace [ ] ={1,3,6,1,4,1,2076,154,1,1};
UINT4 FsMIPvrstGlobalDebug [ ] ={1,3,6,1,4,1,2076,154,1,2};
UINT4 FsMIFuturePvrstContextId [ ] ={1,3,6,1,4,1,2076,154,1,3,1,1};
UINT4 FsMIPvrstSystemControl [ ] ={1,3,6,1,4,1,2076,154,1,3,1,2};
UINT4 FsMIPvrstModuleStatus [ ] ={1,3,6,1,4,1,2076,154,1,3,1,3};
UINT4 FsMIPvrstNoOfActiveInstances [ ] ={1,3,6,1,4,1,2076,154,1,3,1,4};
UINT4 FsMIPvrstBrgAddress [ ] ={1,3,6,1,4,1,2076,154,1,3,1,5};
UINT4 FsMIPvrstUpCount [ ] ={1,3,6,1,4,1,2076,154,1,3,1,6};
UINT4 FsMIPvrstDownCount [ ] ={1,3,6,1,4,1,2076,154,1,3,1,7};
UINT4 FsMIPvrstDynamicPathCostCalculation [ ] ={1,3,6,1,4,1,2076,154,1,3,1,9};
UINT4 FsMIPvrstTrace [ ] ={1,3,6,1,4,1,2076,154,1,3,1,10};
UINT4 FsMIPvrstDebug [ ] ={1,3,6,1,4,1,2076,154,1,3,1,11};
UINT4 FsMIPvrstBufferOverFlowCount [ ] ={1,3,6,1,4,1,2076,154,1,3,1,12};
UINT4 FsMIPvrstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,154,1,3,1,13};
UINT4 FsMIPvrstContextName [ ] ={1,3,6,1,4,1,2076,154,1,3,1,14};
UINT4 FsMIPvrstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,154,1,3,1,15};
UINT4 FsMIPvrstGlobalBpduGuard [ ] ={1,3,6,1,4,1,2076,154,1,3,1,16};
UINT4 FsMIPvrstForceProtocolVersion [ ] ={1,3,6,1,4,1,2076,154,1,3,1,17};
UINT4 FsMIPvrstFlushInterval [ ] ={1,3,6,1,4,1,2076,154,1,3,1,18};
UINT4 FsMIPvrstPort [ ] ={1,3,6,1,4,1,2076,154,1,4,1,1};
UINT4 FsMIPvrstPortAdminEdgeStatus [ ] ={1,3,6,1,4,1,2076,154,1,4,1,2};
UINT4 FsMIPvrstPortOperEdgePortStatus [ ] ={1,3,6,1,4,1,2076,154,1,4,1,3};
UINT4 FsMIPvrstBridgeDetectionSemState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,4};
UINT4 FsMIPvrstPortEnabledStatus [ ] ={1,3,6,1,4,1,2076,154,1,4,1,5};
UINT4 FsMIPvrstRootGuard [ ] ={1,3,6,1,4,1,2076,154,1,4,1,6};
UINT4 FsMIPvrstBpduGuard [ ] ={1,3,6,1,4,1,2076,154,1,4,1,7};
UINT4 FsMIPvrstEncapType [ ] ={1,3,6,1,4,1,2076,154,1,4,1,8};
UINT4 FsMIPvrstPortAdminPointToPoint [ ] ={1,3,6,1,4,1,2076,154,1,4,1,9};
UINT4 FsMIPvrstPortOperPointToPoint [ ] ={1,3,6,1,4,1,2076,154,1,4,1,10};
UINT4 FsMIPvrstPortInvalidBpdusRcvd [ ] ={1,3,6,1,4,1,2076,154,1,4,1,11};
UINT4 FsMIPvrstPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,154,1,4,1,12};
UINT4 FsMIPvrstPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,154,1,4,1,13};
UINT4 FsMIPvrstPortRowStatus [ ] ={1,3,6,1,4,1,2076,154,1,4,1,14};
UINT4 FsMIPvrstRootInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,15};
UINT4 FsMIPvrstPortLoopGuard [ ] ={1,3,6,1,4,1,2076,154,1,4,1,16};
UINT4 FsMIPvrstPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,17};
UINT4 FsMIPvrstPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,154,1,4,1,18};
UINT4 FsMIPvrstPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,154,1,4,1,19};
UINT4 FsMIPvrstBpduFilter [ ] ={1,3,6,1,4,1,2076,154,1,4,1,20};
UINT4 FsMIPvrstPortAutoEdge [ ] ={1,3,6,1,4,1,2076,154,1,4,1,21};
UINT4 FsMIPvrstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,22};
UINT4 FsMIPvrstPortTypeInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,23};
UINT4 FsMIPvrstPortPVIDInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,4,1,24};
UINT4 FsMIPvrstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,154,1,4,1,25};
UINT4 FsMIPvrstPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,154,1,4,1,26};
UINT4 FsMIPvrstPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,4,1,27};
UINT4 FsMIPvrstPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,154,1,4,1,28};
UINT4 FsMIPvrstPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,4,1,29};
UINT4 FsMIPvrstInstVlanId [ ] ={1,3,6,1,4,1,2076,154,1,5,1,1};
UINT4 FsMIPvrstInstBridgePriority [ ] ={1,3,6,1,4,1,2076,154,1,5,1,2};
UINT4 FsMIPvrstInstRootCost [ ] ={1,3,6,1,4,1,2076,154,1,5,1,3};
UINT4 FsMIPvrstInstRootPort [ ] ={1,3,6,1,4,1,2076,154,1,5,1,4};
UINT4 FsMIPvrstInstBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,154,1,5,1,5};
UINT4 FsMIPvrstInstBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,154,1,5,1,6};
UINT4 FsMIPvrstInstBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,154,1,5,1,7};
UINT4 FsMIPvrstInstHoldTime [ ] ={1,3,6,1,4,1,2076,154,1,5,1,8};
UINT4 FsMIPvrstInstTxHoldCount [ ] ={1,3,6,1,4,1,2076,154,1,5,1,9};
UINT4 FsMIPvrstInstTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,154,1,5,1,10};
UINT4 FsMIPvrstInstTopChanges [ ] ={1,3,6,1,4,1,2076,154,1,5,1,11};
UINT4 FsMIPvrstInstNewRootCount [ ] ={1,3,6,1,4,1,2076,154,1,5,1,12};
UINT4 FsMIPvrstInstInstanceUpCount [ ] ={1,3,6,1,4,1,2076,154,1,5,1,13};
UINT4 FsMIPvrstInstInstanceDownCount [ ] ={1,3,6,1,4,1,2076,154,1,5,1,14};
UINT4 FsMIPvrstInstPortRoleSelSemState [ ] ={1,3,6,1,4,1,2076,154,1,5,1,15};
UINT4 FsMIPvrstInstDesignatedRoot [ ] ={1,3,6,1,4,1,2076,154,1,5,1,16};
UINT4 FsMIPvrstInstRootMaxAge [ ] ={1,3,6,1,4,1,2076,154,1,5,1,17};
UINT4 FsMIPvrstInstRootHelloTime [ ] ={1,3,6,1,4,1,2076,154,1,5,1,18};
UINT4 FsMIPvrstInstRootForwardDelay [ ] ={1,3,6,1,4,1,2076,154,1,5,1,19};
UINT4 FsMIPvrstInstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,154,1,5,1,20};
UINT4 FsMIPvrstInstFlushIndicationThreshold [ ] ={1,3,6,1,4,1,2076,154,1,5,1,21};
UINT4 FsMIPvrstInstTotalFlushCount [ ] ={1,3,6,1,4,1,2076,154,1,5,1,22};
UINT4 FsMIPvrstInstPortIndex [ ] ={1,3,6,1,4,1,2076,154,1,6,1,1};
UINT4 FsMIPvrstInstPortEnableStatus [ ] ={1,3,6,1,4,1,2076,154,1,6,1,2};
UINT4 FsMIPvrstInstPortPathCost [ ] ={1,3,6,1,4,1,2076,154,1,6,1,3};
UINT4 FsMIPvrstInstPortPriority [ ] ={1,3,6,1,4,1,2076,154,1,6,1,4};
UINT4 FsMIPvrstInstPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,154,1,6,1,5};
UINT4 FsMIPvrstInstPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,154,1,6,1,6};
UINT4 FsMIPvrstInstPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,154,1,6,1,7};
UINT4 FsMIPvrstInstPortOperVersion [ ] ={1,3,6,1,4,1,2076,154,1,6,1,8};
UINT4 FsMIPvrstInstPortProtocolMigration [ ] ={1,3,6,1,4,1,2076,154,1,6,1,9};
UINT4 FsMIPvrstInstPortState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,10};
UINT4 FsMIPvrstInstPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,154,1,6,1,11};
UINT4 FsMIPvrstInstPortReceivedBpdus [ ] ={1,3,6,1,4,1,2076,154,1,6,1,12};
UINT4 FsMIPvrstInstPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,13};
UINT4 FsMIPvrstInstPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,14};
UINT4 FsMIPvrstInstPortTransmittedBpdus [ ] ={1,3,6,1,4,1,2076,154,1,6,1,15};
UINT4 FsMIPvrstInstPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,16};
UINT4 FsMIPvrstInstPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,17};
UINT4 FsMIPvrstInstPortTxSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,18};
UINT4 FsMIPvrstInstPortProtMigrationSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,19};
UINT4 FsMIPvrstInstProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,20};
UINT4 FsMIPvrstInstPortRole [ ] ={1,3,6,1,4,1,2076,154,1,6,1,21};
UINT4 FsMIPvrstInstCurrentPortRole [ ] ={1,3,6,1,4,1,2076,154,1,6,1,22};
UINT4 FsMIPvrstInstPortInfoSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,23};
UINT4 FsMIPvrstInstPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,24};
UINT4 FsMIPvrstInstPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,25};
UINT4 FsMIPvrstInstPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,26};
UINT4 FsMIPvrstInstPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,27};
UINT4 FsMIPvrstInstPortHelloTime [ ] ={1,3,6,1,4,1,2076,154,1,6,1,28};
UINT4 FsMIPvrstInstPortMaxAge [ ] ={1,3,6,1,4,1,2076,154,1,6,1,29};
UINT4 FsMIPvrstInstPortForwardDelay [ ] ={1,3,6,1,4,1,2076,154,1,6,1,30};
UINT4 FsMIPvrstInstPortHoldTime [ ] ={1,3,6,1,4,1,2076,154,1,6,1,31};
UINT4 FsMIPvrstInstPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,154,1,6,1,32};
UINT4 FsMIPvrstInstPortOldPortState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,33};
UINT4 FsMIPvrstInstPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,34};
UINT4 FsMIPvrstInstPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,154,1,6,1,35};
UINT4 FsMIPvrstInstPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,36};
UINT4 FsMIPvrstInstPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,154,1,6,1,37};
UINT4 FsMIPvrstInstPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,38};
UINT4 FsMIPvrstInstPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,39};
UINT4 FsMIPvrstInstPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,154,1,6,1,40};
UINT4 FsMIPvrstInstPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,154,1,6,1,41};
UINT4 FsMIPvrstInstPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,42};
UINT4 FsMIPvrstInstPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,43};
UINT4 FsMIPvrstInstPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,154,1,6,1,44};
UINT4 FsMIPvrstInstPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,154,1,6,1,45};
UINT4 FsMIPvrstInstPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,46};
UINT4 FsMIPvrstInstPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,154,1,6,1,47};
UINT4 FsMIFsPvrstSetGlobalTrapOption [ ] ={1,3,6,1,4,1,2076,154,2,1};
UINT4 FsMIPvrstGlobalErrTrapType [ ] ={1,3,6,1,4,1,2076,154,2,2};
UINT4 FsMIPvrstSetTraps [ ] ={1,3,6,1,4,1,2076,154,2,3,1,1};
UINT4 FsMIPvrstGenTrapType [ ] ={1,3,6,1,4,1,2076,154,2,3,1,2};
UINT4 FsMIPvrstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,154,2,4,1,1};
UINT4 FsMIPvrstPortMigrationType [ ] ={1,3,6,1,4,1,2076,154,2,4,1,2};
UINT4 FsMIPvrstPktErrType [ ] ={1,3,6,1,4,1,2076,154,2,4,1,3};
UINT4 FsMIPvrstPktErrVal [ ] ={1,3,6,1,4,1,2076,154,2,4,1,4};
UINT4 FsMIPvrstPortRoleType [ ] ={1,3,6,1,4,1,2076,154,2,5,1,1};
UINT4 FsMIPvrstOldRoleType [ ] ={1,3,6,1,4,1,2076,154,2,5,1,2};




tMbDbEntry fsmppvMibEntry[]= {

{{10,FsMIPvrstGlobalTrace}, NULL, FsMIPvrstGlobalTraceGet, FsMIPvrstGlobalTraceSet, FsMIPvrstGlobalTraceTest, FsMIPvrstGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIPvrstGlobalDebug}, NULL, FsMIPvrstGlobalDebugGet, FsMIPvrstGlobalDebugSet, FsMIPvrstGlobalDebugTest, FsMIPvrstGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIFuturePvrstContextId}, GetNextIndexFsMIFuturePvrstTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstSystemControl}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstSystemControlGet, FsMIPvrstSystemControlSet, FsMIPvrstSystemControlTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstModuleStatus}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstModuleStatusGet, FsMIPvrstModuleStatusSet, FsMIPvrstModuleStatusTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstNoOfActiveInstances}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstNoOfActiveInstancesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstBrgAddress}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstBrgAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstUpCount}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstDownCount}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstDynamicPathCostCalculation}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstDynamicPathCostCalculationGet, FsMIPvrstDynamicPathCostCalculationSet, FsMIPvrstDynamicPathCostCalculationTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstTrace}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstTraceGet, FsMIPvrstTraceSet, FsMIPvrstTraceTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPvrstDebug}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstDebugGet, FsMIPvrstDebugSet, FsMIPvrstDebugTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPvrstBufferOverFlowCount}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstBufferOverFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstMemAllocFailureCount}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstContextName}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFuturePvrstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstCalcPortPathCostOnSpeedChg}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstCalcPortPathCostOnSpeedChgGet, FsMIPvrstCalcPortPathCostOnSpeedChgSet, FsMIPvrstCalcPortPathCostOnSpeedChgTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstGlobalBpduGuard}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstGlobalBpduGuardGet, FsMIPvrstGlobalBpduGuardSet, FsMIPvrstGlobalBpduGuardTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstForceProtocolVersion}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstForceProtocolVersionGet, FsMIPvrstForceProtocolVersionSet, FsMIPvrstForceProtocolVersionTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstFlushInterval}, GetNextIndexFsMIFuturePvrstTable, FsMIPvrstFlushIntervalGet, FsMIPvrstFlushIntervalSet, FsMIPvrstFlushIntervalTest, FsMIFuturePvrstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPvrstPort}, GetNextIndexFsMIFuturePvrstPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortAdminEdgeStatus}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortAdminEdgeStatusGet, FsMIPvrstPortAdminEdgeStatusSet, FsMIPvrstPortAdminEdgeStatusTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortOperEdgePortStatus}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortOperEdgePortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstBridgeDetectionSemState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstBridgeDetectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortEnabledStatus}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortEnabledStatusGet, FsMIPvrstPortEnabledStatusSet, FsMIPvrstPortEnabledStatusTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstRootGuard}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstRootGuardGet, FsMIPvrstRootGuardSet, FsMIPvrstRootGuardTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstBpduGuard}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstBpduGuardGet, FsMIPvrstBpduGuardSet, FsMIPvrstBpduGuardTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPvrstEncapType}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstEncapTypeGet, FsMIPvrstEncapTypeSet, FsMIPvrstEncapTypeTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPvrstPortAdminPointToPoint}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortAdminPointToPointGet, FsMIPvrstPortAdminPointToPointSet, FsMIPvrstPortAdminPointToPointTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortOperPointToPoint}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortInvalidBpdusRcvd}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortInvalidBpdusRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortInvalidConfigBpduRxCount}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortInvalidTcnBpduRxCount}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortRowStatus}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortRowStatusGet, FsMIPvrstPortRowStatusSet, FsMIPvrstPortRowStatusTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 1, NULL},

{{12,FsMIPvrstRootInconsistentState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortLoopGuard}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortLoopGuardGet, FsMIPvrstPortLoopGuardSet, FsMIPvrstPortLoopGuardTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortLoopInconsistentState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortEnableBPDURx}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortEnableBPDURxGet, FsMIPvrstPortEnableBPDURxSet, FsMIPvrstPortEnableBPDURxTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIPvrstPortEnableBPDUTx}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortEnableBPDUTxGet, FsMIPvrstPortEnableBPDUTxSet, FsMIPvrstPortEnableBPDUTxTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIPvrstBpduFilter}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstBpduFilterGet, FsMIPvrstBpduFilterSet, FsMIPvrstBpduFilterTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIPvrstPortAutoEdge}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortAutoEdgeGet, FsMIPvrstPortAutoEdgeSet, FsMIPvrstPortAutoEdgeTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIPvrstPortBpduInconsistentState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortTypeInconsistentState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortTypeInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortPVIDInconsistentState}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortPVIDInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPvrstPortBpduGuardAction}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortBpduGuardActionGet, FsMIPvrstPortBpduGuardActionSet, FsMIPvrstPortBpduGuardActionTest, FsMIFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIPvrstPortRcvInfoWhileExpCount}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortImpStateOccurCount}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortImpStateOccurTimeStamp}, GetNextIndexFsMIFuturePvrstPortTable, FsMIPvrstPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstInstVlanId}, GetNextIndexFsMIPvrstInstBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstBridgePriority}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstBridgePriorityGet, FsMIPvrstInstBridgePrioritySet, FsMIPvrstInstBridgePriorityTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, "32768"},

{{12,FsMIPvrstInstRootCost}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstRootPort}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstBridgeMaxAge}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstBridgeMaxAgeGet, FsMIPvrstInstBridgeMaxAgeSet, FsMIPvrstInstBridgeMaxAgeTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstBridgeHelloTime}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstBridgeHelloTimeGet, FsMIPvrstInstBridgeHelloTimeSet, FsMIPvrstInstBridgeHelloTimeTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstBridgeForwardDelay}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstBridgeForwardDelayGet, FsMIPvrstInstBridgeForwardDelaySet, FsMIPvrstInstBridgeForwardDelayTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstHoldTime}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstTxHoldCount}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstTxHoldCountGet, FsMIPvrstInstTxHoldCountSet, FsMIPvrstInstTxHoldCountTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, "6"},

{{12,FsMIPvrstInstTimeSinceTopologyChange}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstTopChanges}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstNewRootCount}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstNewRootCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstInstanceUpCount}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstInstanceUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstInstanceDownCount}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstInstanceDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortRoleSelSemState}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstPortRoleSelSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstDesignatedRoot}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstRootMaxAge}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstRootMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstRootHelloTime}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstRootHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstRootForwardDelay}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstRootForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstOldDesignatedRoot}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstFlushIndicationThreshold}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstFlushIndicationThresholdGet, FsMIPvrstInstFlushIndicationThresholdSet, FsMIPvrstInstFlushIndicationThresholdTest, FsMIPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, "0"},

{{12,FsMIPvrstInstTotalFlushCount}, GetNextIndexFsMIPvrstInstBridgeTable, FsMIPvrstInstTotalFlushCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortIndex}, GetNextIndexFsMIPvrstInstPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortEnableStatus}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortEnableStatusGet, FsMIPvrstInstPortEnableStatusSet, FsMIPvrstInstPortEnableStatusTest, FsMIPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortPathCost}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortPathCostGet, FsMIPvrstInstPortPathCostSet, FsMIPvrstInstPortPathCostTest, FsMIPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortPriority}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortPriorityGet, FsMIPvrstInstPortPrioritySet, FsMIPvrstInstPortPriorityTest, FsMIPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstPortTableINDEX, 2, 0, 0, "128"},

{{12,FsMIPvrstInstPortDesignatedRoot}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortDesignatedBridge}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortDesignatedPort}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortOperVersion}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProtocolMigration}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProtocolMigrationGet, FsMIPvrstInstPortProtocolMigrationSet, FsMIPvrstInstPortProtocolMigrationTest, FsMIPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortForwardTransitions}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortReceivedBpdus}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortReceivedBpdusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortRxConfigBpduCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortRxTcnBpduCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTransmittedBpdus}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTransmittedBpdusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTxConfigBpduCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTxTcnBpduCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTxSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTxSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProtMigrationSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProtMigrationSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstProtocolMigrationCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortRole}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstCurrentPortRole}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortInfoSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortRoleTransitionSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortStateTransitionSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTopologyChangeSemState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortEffectivePortState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortHelloTime}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortMaxAge}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortForwardDelay}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortHoldTime}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortAdminPathCost}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortAdminPathCostGet, FsMIPvrstInstPortAdminPathCostSet, FsMIPvrstInstPortAdminPathCostTest, FsMIPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortOldPortState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortLoopInconsistentState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMIPvrstInstPortRootInconsistentState}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMIPvrstInstPortTCDetectedCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTCReceivedCount}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTCDetectedTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortTCReceivedTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProposalPktsSent}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProposalPktsRcvd}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProposalPktSentTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortProposalPktRcvdTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortAgreementPktSent}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortAgreementPktRcvd}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortAgreementPktSentTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstInstPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMIPvrstInstPortTable, FsMIPvrstInstPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{10,FsMIFsPvrstSetGlobalTrapOption}, NULL, FsMIFsPvrstSetGlobalTrapOptionGet, FsMIFsPvrstSetGlobalTrapOptionSet, FsMIFsPvrstSetGlobalTrapOptionTest, FsMIFsPvrstSetGlobalTrapOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIPvrstGlobalErrTrapType}, NULL, FsMIPvrstGlobalErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMIPvrstSetTraps}, GetNextIndexFsMIFsPvrstTrapsControlTable, FsMIPvrstSetTrapsGet, FsMIPvrstSetTrapsSet, FsMIPvrstSetTrapsTest, FsMIFsPvrstTrapsControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsPvrstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstGenTrapType}, GetNextIndexFsMIFsPvrstTrapsControlTable, FsMIPvrstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsPvrstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortTrapIndex}, GetNextIndexFsMIPvrstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortMigrationType}, GetNextIndexFsMIPvrstPortTrapNotificationTable, FsMIPvrstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPktErrType}, GetNextIndexFsMIPvrstPortTrapNotificationTable, FsMIPvrstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPktErrVal}, GetNextIndexFsMIPvrstPortTrapNotificationTable, FsMIPvrstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPvrstPortRoleType}, GetNextIndexFsMIPvrstPortRoleTrapNotificationTable, FsMIPvrstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPvrstOldRoleType}, GetNextIndexFsMIPvrstPortRoleTrapNotificationTable, FsMIPvrstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPvrstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmppvEntry = { 127, fsmppvMibEntry };

#endif /* _FSMPPVDB_H */

