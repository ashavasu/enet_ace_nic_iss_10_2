/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvtrap.h,v 1.10 2017/09/19 13:50:24 siva Exp $
 *
 * Description: All macros and structure used for traps for the PVRST module 
 *
 *****************************************************************************/

#ifndef _PVRST_TRAP_H_
#define _PVRST_TRAP_H_

#define PVRST_BRG_TRAPS_OID_LEN  10

#define PVRST_INST_UP_TRAP   3
#define PVRST_INST_DOWN_TRAP 4

#define PVRST_MIB_OBJ_CONTEXT_NAME      "fsMIPvrstContextName"

#define PVRST_MIB_OBJ_BASE_BRIDGE_ADDR_MI  "fsMIPvrstBrgAddress"
#define PVRST_MIB_OBJ_GEN_TRAP_TYPE_MI     "fsMIPvrstGenTrapType"
#define PVRST_MIB_OBJ_ERR_TRAP_TYPE_MI     "fsMIPvrstErrTrapType"
#define PVRST_MIB_OBJ_DESIG_ROOT_MI        "fsMIPvrstInstDesignatedRoot"
#define PVRST_MIB_OBJ_MIGRATION_TYPE_MI    "fsMIPvrstPortMigrationType"
#define PVRST_MIB_OBJ_PKT_ERR_TYPE_MI      "fsMIPvrstPktErrType"
#define PVRST_MIB_OBJ_PKT_ERR_VAL_MI       "fsMIPvrstPktErrVal"
#define PVRST_MIB_OBJ_PREVIOUS_ROLE_MI     "fsMIPvrstOldRoleType" 
#define PVRST_MIB_OBJ_SELECTED_ROLE_MI     "fsMIPvrstPortRoleType"
#define PVRST_MIB_OBJ_TOPOLOGY_CHGS_MI     "fsMIPvrstInstTopChanges"
#define PVRST_MIB_OBJ_INSTUP_COUNT_MI      "fsMIPvrstInstInstanceUpCount"
#define PVRST_MIB_OBJ_INSTDOWN_COUNT_MI    "fsMIPvrstInstInstanceDownCount"
#define PVRST_MIB_OBJ_PORT_STATE_MI        "fsMIPvrstInstPortState"
#define PVRST_MIB_OBJ_LOOP_INCSTATE_MI    "fsMIPvrstInstPortLoopInconsistentState"
#define PVRST_MIB_OBJ_BPDU_INCSTATE_MI     "fsMIPvrstPortBpduInconsistentState"
#define PVRST_MIB_OBJ_ROOT_INCSTATE_MI     "fsMIPvrstInstPortRootInconsistentState"
#define PVRST_MIB_OBJ_OLD_PORTSTATE_MI     "fsMIPvrstInstPortOldPortState"
#define PVRST_MIB_OBJ_OLD_DESIG_ROOT_MI    "fsMIPvrstInstOldDesignatedRoot"
#define PVRST_MIB_OBJ_BASE_BRIDGE_ADDR_SI  "fsPvrstBrgAddress"
#define PVRST_MIB_OBJ_GEN_TRAP_TYPE_SI     "fsPvrstGenTrapType"
#define PVRST_MIB_OBJ_ERR_TRAP_TYPE_SI     "fsPvrstErrTrapType"
#define PVRST_MIB_OBJ_DESIG_ROOT_SI        "fsPvrstInstDesignatedRoot"
#define PVRST_MIB_OBJ_MIGRATION_TYPE_SI    "fsPvrstPortMigrationType"
#define PVRST_MIB_OBJ_PKT_ERR_TYPE_SI      "fsPvrstPktErrType"
#define PVRST_MIB_OBJ_PKT_ERR_VAL_SI       "fsPvrstPktErrVal"
#define PVRST_MIB_OBJ_PREVIOUS_ROLE_SI     "fsPvrstOldRoleType" 
#define PVRST_MIB_OBJ_SELECTED_ROLE_SI     "fsPvrstPortRoleType"
#define PVRST_MIB_OBJ_TOPOLOGY_CHGS_SI     "fsPvrstInstTopChanges"
#define PVRST_MIB_OBJ_INSTUP_COUNT_SI      "fsPvrstInstInstanceUpCount"
#define PVRST_MIB_OBJ_INSTDOWN_COUNT_SI    "fsPvrstInstInstanceDownCount"
#define PVRST_MIB_OBJ_PORT_STATE_SI        "fsPvrstInstPortState"
#define PVRST_MIB_OBJ_LOOP_INCSTATE_SI    "fsPvrstInstPortLoopInconsistentState"
#define PVRST_MIB_OBJ_BPDU_INCSTATE_SI     "fsPvrstPortBpduInconsistentState"
#define PVRST_MIB_OBJ_ROOT_INCSTATE_SI     "fsPvrstInstPortRootInconsistentState"
#define PVRST_MIB_OBJ_OLD_PORTSTATE_SI     "fsPvrstInstPortOldPortState"
#define PVRST_MIB_OBJ_OLD_DESIG_ROOT_SI    "fsPvrstInstOldDesignatedRoot"

#define PVRST_MIB_OBJ_BASE_BRIDGE_ADDR \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_BASE_BRIDGE_ADDR_SI:  PVRST_MIB_OBJ_BASE_BRIDGE_ADDR_MI)

#define PVRST_MIB_OBJ_GEN_TRAP_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_GEN_TRAP_TYPE_SI:  PVRST_MIB_OBJ_GEN_TRAP_TYPE_MI)

#define PVRST_MIB_OBJ_ERR_TRAP_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_ERR_TRAP_TYPE_SI:  PVRST_MIB_OBJ_ERR_TRAP_TYPE_MI) 

#define PVRST_MIB_OBJ_DESIG_ROOT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_DESIG_ROOT_SI:  PVRST_MIB_OBJ_DESIG_ROOT_MI)

#define PVRST_MIB_OBJ_MIGRATION_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_MIGRATION_TYPE_SI:  PVRST_MIB_OBJ_MIGRATION_TYPE_MI)

#define PVRST_MIB_OBJ_PKT_ERR_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_PKT_ERR_TYPE_SI:  PVRST_MIB_OBJ_PKT_ERR_TYPE_MI) 

#define PVRST_MIB_OBJ_PKT_ERR_VAL \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_PKT_ERR_VAL_SI:  PVRST_MIB_OBJ_PKT_ERR_VAL_MI)

#define PVRST_MIB_OBJ_PREVIOUS_ROLE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_PREVIOUS_ROLE_SI :  PVRST_MIB_OBJ_PREVIOUS_ROLE_MI)

#define PVRST_MIB_OBJ_SELECTED_ROLE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_SELECTED_ROLE_SI:  PVRST_MIB_OBJ_SELECTED_ROLE_MI)

#define PVRST_MIB_OBJ_TOPOLOGY_CHGS \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_TOPOLOGY_CHGS_SI:  PVRST_MIB_OBJ_TOPOLOGY_CHGS_MI)

#define PVRST_MIB_OBJ_INSTUP_COUNT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_INSTUP_COUNT_SI:  PVRST_MIB_OBJ_INSTUP_COUNT_MI)

#define PVRST_MIB_OBJ_INSTDOWN_COUNT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_INSTDOWN_COUNT_SI:  PVRST_MIB_OBJ_INSTDOWN_COUNT_MI)

#define PVRST_MIB_OBJ_PORT_STATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_PORT_STATE_SI:  PVRST_MIB_OBJ_PORT_STATE_MI)

#define PVRST_MIB_OBJ_LOOP_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  PVRST_MIB_OBJ_LOOP_INCSTATE_SI: PVRST_MIB_OBJ_LOOP_INCSTATE_MI)

#define PVRST_MIB_OBJ_BPDU_INCSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? PVRST_MIB_OBJ_BPDU_INCSTATE_SI : PVRST_MIB_OBJ_BPDU_INCSTATE_MI)

#define PVRST_MIB_OBJ_ROOT_INCSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? PVRST_MIB_OBJ_ROOT_INCSTATE_SI : PVRST_MIB_OBJ_ROOT_INCSTATE_MI)

#define PVRST_MIB_OBJ_OLD_PORTSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? PVRST_MIB_OBJ_OLD_PORTSTATE_SI : PVRST_MIB_OBJ_OLD_PORTSTATE_MI)

#define PVRST_MIB_OBJ_OLD_DESIG_ROOT \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? PVRST_MIB_OBJ_OLD_DESIG_ROOT_SI : PVRST_MIB_OBJ_OLD_DESIG_ROOT_MI)

typedef struct _PvrstBrgProtocolMigrationTrap{
  tAstMacAddr   BrgAddr;
  UINT2         u2InstanceId;
  UINT2         u2Port;
  UINT1         u1Version;
  UINT1    u1GenTrapType;
} tPvrstBrgProtocolMigrationTrap;    

VOID AstPvrstInstUpTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID AstPvrstInstDownTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID AstPvrstProtocolMigrationTrap (UINT2 u2InstanceId, UINT2 u2PortNum, UINT1 u1Version, 
        UINT1 u1SendVersion,INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID PvrstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,VOID *pTrapInfo);
tSNMP_OID_TYPE     *PvrstMakeObjIdFromDotNew (INT1 *pi1TextStr);
INT1 * PvrstParseSubIdNew (INT1 *pi1TmpPtr, UINT4 *pu4Value);

#endif


