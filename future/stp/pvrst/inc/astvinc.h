/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvinc.h,v 1.5 2015/10/12 11:08:59 siva Exp $
 *
 * Description: This file contains header files to be included   
 *              by all source files of PVRST Module.
 *
 *****************************************************************************/

#ifndef _ASTVINC_H
#define _ASTVINC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "fsvlan.h"
#include "asttrc.h"
#include "srmbuf.h"
#include "rstp.h" 
#include "pvrst.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#include "la.h"
#include "iss.h"
#include "l2iwf.h"
#include "vcm.h"
#include "issu.h"

/* PVRST Related Files */
#include "astvcons.h"
#include "astconst.h"
#include "astvmacr.h"
#include "astmacr.h"
#include "astctdfs.h"
#include "pvsminpwr.h"
#include "astvextn.h"
#include "astprot.h"
#include "astvprot.h"
#include "astvtrap.h"
#include "fsmppvlw.h"
#include "fsmppvwr.h"
#include "fspvrslw.h"
#include "fspvrswr.h"

#ifdef L2RED_WANTED 
#include "astvred.h"
#else
#include "astvrdstb.h"
#endif

#include "astglob.h"
#include "astrtrap.h"
#include "astdbg.h"
#include "astpbprot.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

#endif /* _ASTVINC_H */

