/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspvrsdb.h,v 1.23 2017/09/19 13:50:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPVRSDB_H
#define _FSPVRSDB_H

UINT1 FsFuturePvrstPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPvrstInstBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPvrstInstPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPvrstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPvrstPortRoleTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fspvrs [] ={1,3,6,1,4,1,2076,161};
tSNMP_OID_TYPE fspvrsOID = {8, fspvrs};


UINT4 FsPvrstSystemControl [ ] ={1,3,6,1,4,1,2076,161,1,1};
UINT4 FsPvrstModuleStatus [ ] ={1,3,6,1,4,1,2076,161,1,2};
UINT4 FsPvrstNoOfActiveInstances [ ] ={1,3,6,1,4,1,2076,161,1,3};
UINT4 FsPvrstBrgAddress [ ] ={1,3,6,1,4,1,2076,161,1,4};
UINT4 FsPvrstUpCount [ ] ={1,3,6,1,4,1,2076,161,1,5};
UINT4 FsPvrstDownCount [ ] ={1,3,6,1,4,1,2076,161,1,6};
UINT4 FsPvrstPathCostDefaultType [ ] ={1,3,6,1,4,1,2076,161,1,7};
UINT4 FsPvrstDynamicPathCostCalculation [ ] ={1,3,6,1,4,1,2076,161,1,8};
UINT4 FsPvrstTrace [ ] ={1,3,6,1,4,1,2076,161,1,9};
UINT4 FsPvrstDebug [ ] ={1,3,6,1,4,1,2076,161,1,10};
UINT4 FsPvrstBufferOverFlowCount [ ] ={1,3,6,1,4,1,2076,161,1,11};
UINT4 FsPvrstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,161,1,12};
UINT4 FsPvrstPort [ ] ={1,3,6,1,4,1,2076,161,1,13,1,1};
UINT4 FsPvrstPortAdminEdgeStatus [ ] ={1,3,6,1,4,1,2076,161,1,13,1,2};
UINT4 FsPvrstPortOperEdgePortStatus [ ] ={1,3,6,1,4,1,2076,161,1,13,1,3};
UINT4 FsPvrstBridgeDetectionSemState [ ] ={1,3,6,1,4,1,2076,161,1,13,1,4};
UINT4 FsPvrstPortEnabledStatus [ ] ={1,3,6,1,4,1,2076,161,1,13,1,5};
UINT4 FsPvrstRootGuard [ ] ={1,3,6,1,4,1,2076,161,1,13,1,6};
UINT4 FsPvrstBpduGuard [ ] ={1,3,6,1,4,1,2076,161,1,13,1,7};
UINT4 FsPvrstEncapType [ ] ={1,3,6,1,4,1,2076,161,1,13,1,8};
UINT4 FsPvrstPortAdminPointToPoint [ ] ={1,3,6,1,4,1,2076,161,1,13,1,9};
UINT4 FsPvrstPortOperPointToPoint [ ] ={1,3,6,1,4,1,2076,161,1,13,1,10};
UINT4 FsPvrstPortInvalidBpdusRcvd [ ] ={1,3,6,1,4,1,2076,161,1,13,1,11};
UINT4 FsPvrstPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,161,1,13,1,12};
UINT4 FsPvrstPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,161,1,13,1,13};
UINT4 FsPvrstPortRowStatus [ ] ={1,3,6,1,4,1,2076,161,1,13,1,14};
UINT4 FsPvrstPortLoopGuard [ ] ={1,3,6,1,4,1,2076,161,1,13,1,15};
UINT4 FsPvrstPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,13,1,16};
UINT4 FsPvrstPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,161,1,13,1,17};
UINT4 FsPvrstPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,161,1,13,1,18};
UINT4 FsPvrstBpduFilter [ ] ={1,3,6,1,4,1,2076,161,1,13,1,19};
UINT4 FsPvrstPortAutoEdge [ ] ={1,3,6,1,4,1,2076,161,1,13,1,20};
UINT4 FsPvrstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,13,1,21};
UINT4 FsPvrstPortTypeInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,13,1,22};
UINT4 FsPvrstPortPVIDInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,13,1,23};
UINT4 FsPvrstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,161,1,13,1,24};
UINT4 FsPvrstPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,161,1,13,1,25};
UINT4 FsPvrstPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,13,1,26};
UINT4 FsPvrstPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,161,1,13,1,27};
UINT4 FsPvrstPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,13,1,28};
UINT4 FsPvrstInstVlanId [ ] ={1,3,6,1,4,1,2076,161,1,14,1,1};
UINT4 FsPvrstInstBridgePriority [ ] ={1,3,6,1,4,1,2076,161,1,14,1,2};
UINT4 FsPvrstInstRootCost [ ] ={1,3,6,1,4,1,2076,161,1,14,1,3};
UINT4 FsPvrstInstRootPort [ ] ={1,3,6,1,4,1,2076,161,1,14,1,4};
UINT4 FsPvrstInstBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,161,1,14,1,5};
UINT4 FsPvrstInstBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,161,1,14,1,6};
UINT4 FsPvrstInstBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,161,1,14,1,7};
UINT4 FsPvrstInstHoldTime [ ] ={1,3,6,1,4,1,2076,161,1,14,1,8};
UINT4 FsPvrstInstTxHoldCount [ ] ={1,3,6,1,4,1,2076,161,1,14,1,9};
UINT4 FsPvrstInstTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,161,1,14,1,10};
UINT4 FsPvrstInstTopChanges [ ] ={1,3,6,1,4,1,2076,161,1,14,1,11};
UINT4 FsPvrstInstNewRootCount [ ] ={1,3,6,1,4,1,2076,161,1,14,1,12};
UINT4 FsPvrstInstInstanceUpCount [ ] ={1,3,6,1,4,1,2076,161,1,14,1,13};
UINT4 FsPvrstInstInstanceDownCount [ ] ={1,3,6,1,4,1,2076,161,1,14,1,14};
UINT4 FsPvrstInstPortRoleSelSemState [ ] ={1,3,6,1,4,1,2076,161,1,14,1,15};
UINT4 FsPvrstInstDesignatedRoot [ ] ={1,3,6,1,4,1,2076,161,1,14,1,16};
UINT4 FsPvrstInstRootMaxAge [ ] ={1,3,6,1,4,1,2076,161,1,14,1,17};
UINT4 FsPvrstInstRootHelloTime [ ] ={1,3,6,1,4,1,2076,161,1,14,1,18};
UINT4 FsPvrstInstRootForwardDelay [ ] ={1,3,6,1,4,1,2076,161,1,14,1,19};
UINT4 FsPvrstInstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,161,1,14,1,20};
UINT4 FsPvrstInstFlushIndicationThreshold [ ] ={1,3,6,1,4,1,2076,161,1,14,1,21};
UINT4 FsPvrstInstTotalFlushCount [ ] ={1,3,6,1,4,1,2076,161,1,14,1,22};
UINT4 FsPvrstInstPortIndex [ ] ={1,3,6,1,4,1,2076,161,1,15,1,1};
UINT4 FsPvrstInstPortEnableStatus [ ] ={1,3,6,1,4,1,2076,161,1,15,1,2};
UINT4 FsPvrstInstPortPathCost [ ] ={1,3,6,1,4,1,2076,161,1,15,1,3};
UINT4 FsPvrstInstPortPriority [ ] ={1,3,6,1,4,1,2076,161,1,15,1,4};
UINT4 FsPvrstInstPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,161,1,15,1,5};
UINT4 FsPvrstInstPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,161,1,15,1,6};
UINT4 FsPvrstInstPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,161,1,15,1,7};
UINT4 FsPvrstInstPortOperVersion [ ] ={1,3,6,1,4,1,2076,161,1,15,1,8};
UINT4 FsPvrstInstPortProtocolMigration [ ] ={1,3,6,1,4,1,2076,161,1,15,1,9};
UINT4 FsPvrstInstPortState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,10};
UINT4 FsPvrstInstPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,161,1,15,1,11};
UINT4 FsPvrstInstPortReceivedBpdus [ ] ={1,3,6,1,4,1,2076,161,1,15,1,12};
UINT4 FsPvrstInstPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,13};
UINT4 FsPvrstInstPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,14};
UINT4 FsPvrstInstPortTransmittedBpdus [ ] ={1,3,6,1,4,1,2076,161,1,15,1,15};
UINT4 FsPvrstInstPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,16};
UINT4 FsPvrstInstPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,17};
UINT4 FsPvrstInstPortTxSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,18};
UINT4 FsPvrstInstPortProtMigrationSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,19};
UINT4 FsPvrstInstProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,20};
UINT4 FsPvrstInstPortRole [ ] ={1,3,6,1,4,1,2076,161,1,15,1,21};
UINT4 FsPvrstInstCurrentPortRole [ ] ={1,3,6,1,4,1,2076,161,1,15,1,22};
UINT4 FsPvrstInstPortInfoSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,23};
UINT4 FsPvrstInstPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,24};
UINT4 FsPvrstInstPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,25};
UINT4 FsPvrstInstPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,26};
UINT4 FsPvrstInstPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,27};
UINT4 FsPvrstInstPortHelloTime [ ] ={1,3,6,1,4,1,2076,161,1,15,1,28};
UINT4 FsPvrstInstPortMaxAge [ ] ={1,3,6,1,4,1,2076,161,1,15,1,29};
UINT4 FsPvrstInstPortForwardDelay [ ] ={1,3,6,1,4,1,2076,161,1,15,1,30};
UINT4 FsPvrstInstPortHoldTime [ ] ={1,3,6,1,4,1,2076,161,1,15,1,31};
UINT4 FsPvrstInstPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,161,1,15,1,32};
UINT4 FsPvrstInstPortOldPortState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,33};
UINT4 FsPvrstInstPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,34};
UINT4 FsPvrstInstPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,161,1,15,1,35};
UINT4 FsPvrstInstPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,36};
UINT4 FsPvrstInstPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,161,1,15,1,37};
UINT4 FsPvrstInstPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,38};
UINT4 FsPvrstInstPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,39};
UINT4 FsPvrstInstPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,161,1,15,1,40};
UINT4 FsPvrstInstPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,161,1,15,1,41};
UINT4 FsPvrstInstPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,42};
UINT4 FsPvrstInstPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,43};
UINT4 FsPvrstInstPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,161,1,15,1,44};
UINT4 FsPvrstInstPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,161,1,15,1,45};
UINT4 FsPvrstInstPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,46};
UINT4 FsPvrstInstPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,161,1,15,1,47};
UINT4 FsPvrstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,161,1,16};
UINT4 FsPvrstGlobalBpduGuard [ ] ={1,3,6,1,4,1,2076,161,1,17};
UINT4 FsPvrstForceProtocolVersion [ ] ={1,3,6,1,4,1,2076,161,1,18};
UINT4 FsPvrstFlushInterval [ ] ={1,3,6,1,4,1,2076,161,1,19};
UINT4 FsPvrstSetTraps [ ] ={1,3,6,1,4,1,2076,161,2,1};
UINT4 FsPvrstGenTrapType [ ] ={1,3,6,1,4,1,2076,161,2,2};
UINT4 FsPvrstErrTrapType [ ] ={1,3,6,1,4,1,2076,161,2,3};
UINT4 FsPvrstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,161,2,4,1,1};
UINT4 FsPvrstPortMigrationType [ ] ={1,3,6,1,4,1,2076,161,2,4,1,2};
UINT4 FsPvrstPktErrType [ ] ={1,3,6,1,4,1,2076,161,2,4,1,3};
UINT4 FsPvrstPktErrVal [ ] ={1,3,6,1,4,1,2076,161,2,4,1,4};
UINT4 FsPvrstPortRoleType [ ] ={1,3,6,1,4,1,2076,161,2,5,1,1};
UINT4 FsPvrstOldRoleType [ ] ={1,3,6,1,4,1,2076,161,2,5,1,2};


tMbDbEntry fspvrsMibEntry[]= {

{{10,FsPvrstSystemControl}, NULL, FsPvrstSystemControlGet, FsPvrstSystemControlSet, FsPvrstSystemControlTest, FsPvrstSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPvrstModuleStatus}, NULL, FsPvrstModuleStatusGet, FsPvrstModuleStatusSet, FsPvrstModuleStatusTest, FsPvrstModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPvrstNoOfActiveInstances}, NULL, FsPvrstNoOfActiveInstancesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstBrgAddress}, NULL, FsPvrstBrgAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstUpCount}, NULL, FsPvrstUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstDownCount}, NULL, FsPvrstDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstPathCostDefaultType}, NULL, FsPvrstPathCostDefaultTypeGet, FsPvrstPathCostDefaultTypeSet, FsPvrstPathCostDefaultTypeTest, FsPvrstPathCostDefaultTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPvrstDynamicPathCostCalculation}, NULL, FsPvrstDynamicPathCostCalculationGet, FsPvrstDynamicPathCostCalculationSet, FsPvrstDynamicPathCostCalculationTest, FsPvrstDynamicPathCostCalculationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsPvrstTrace}, NULL, FsPvrstTraceGet, FsPvrstTraceSet, FsPvrstTraceTest, FsPvrstTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsPvrstDebug}, NULL, FsPvrstDebugGet, FsPvrstDebugSet, FsPvrstDebugTest, FsPvrstDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsPvrstBufferOverFlowCount}, NULL, FsPvrstBufferOverFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstMemAllocFailureCount}, NULL, FsPvrstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsPvrstPort}, GetNextIndexFsFuturePvrstPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortAdminEdgeStatus}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortAdminEdgeStatusGet, FsPvrstPortAdminEdgeStatusSet, FsPvrstPortAdminEdgeStatusTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortOperEdgePortStatus}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortOperEdgePortStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstBridgeDetectionSemState}, GetNextIndexFsFuturePvrstPortTable, FsPvrstBridgeDetectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortEnabledStatus}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortEnabledStatusGet, FsPvrstPortEnabledStatusSet, FsPvrstPortEnabledStatusTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstRootGuard}, GetNextIndexFsFuturePvrstPortTable, FsPvrstRootGuardGet, FsPvrstRootGuardSet, FsPvrstRootGuardTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstBpduGuard}, GetNextIndexFsFuturePvrstPortTable, FsPvrstBpduGuardGet, FsPvrstBpduGuardSet, FsPvrstBpduGuardTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstEncapType}, GetNextIndexFsFuturePvrstPortTable, FsPvrstEncapTypeGet, FsPvrstEncapTypeSet, FsPvrstEncapTypeTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "0"},

{{12,FsPvrstPortAdminPointToPoint}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortAdminPointToPointGet, FsPvrstPortAdminPointToPointSet, FsPvrstPortAdminPointToPointTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortOperPointToPoint}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortInvalidBpdusRcvd}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortInvalidBpdusRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortInvalidConfigBpduRxCount}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortInvalidTcnBpduRxCount}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortRowStatus}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortRowStatusGet, FsPvrstPortRowStatusSet, FsPvrstPortRowStatusTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 1, NULL},

{{12,FsPvrstPortLoopGuard}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortLoopGuardGet, FsPvrstPortLoopGuardSet, FsPvrstPortLoopGuardTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsPvrstPortLoopInconsistentState}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsPvrstPortEnableBPDURx}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortEnableBPDURxGet, FsPvrstPortEnableBPDURxSet, FsPvrstPortEnableBPDURxTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsPvrstPortEnableBPDUTx}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortEnableBPDUTxGet, FsPvrstPortEnableBPDUTxSet, FsPvrstPortEnableBPDUTxTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsPvrstBpduFilter}, GetNextIndexFsFuturePvrstPortTable, FsPvrstBpduFilterGet, FsPvrstBpduFilterSet, FsPvrstBpduFilterTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsPvrstPortAutoEdge}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortAutoEdgeGet, FsPvrstPortAutoEdgeSet, FsPvrstPortAutoEdgeTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsPvrstPortBpduInconsistentState}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsPvrstPortTypeInconsistentState}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortTypeInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsPvrstPortPVIDInconsistentState}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortPVIDInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, "2"},

{{12,FsPvrstPortBpduGuardAction}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortBpduGuardActionGet, FsPvrstPortBpduGuardActionSet, FsPvrstPortBpduGuardActionTest, FsFuturePvrstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFuturePvrstPortTableINDEX, 1, 0, 0, "1"},

{{12,FsPvrstPortRcvInfoWhileExpCount}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortImpStateOccurCount}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortImpStateOccurTimeStamp}, GetNextIndexFsFuturePvrstPortTable, FsPvrstPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsFuturePvrstPortTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstVlanId}, GetNextIndexFsPvrstInstBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstBridgePriority}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstBridgePriorityGet, FsPvrstInstBridgePrioritySet, FsPvrstInstBridgePriorityTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, "32768"},

{{12,FsPvrstInstRootCost}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstRootPort}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstBridgeMaxAge}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstBridgeMaxAgeGet, FsPvrstInstBridgeMaxAgeSet, FsPvrstInstBridgeMaxAgeTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstBridgeHelloTime}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstBridgeHelloTimeGet, FsPvrstInstBridgeHelloTimeSet, FsPvrstInstBridgeHelloTimeTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstBridgeForwardDelay}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstBridgeForwardDelayGet, FsPvrstInstBridgeForwardDelaySet, FsPvrstInstBridgeForwardDelayTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstHoldTime}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstTxHoldCount}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstTxHoldCountGet, FsPvrstInstTxHoldCountSet, FsPvrstInstTxHoldCountTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, "6"},

{{12,FsPvrstInstTimeSinceTopologyChange}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstTopChanges}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstNewRootCount}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstNewRootCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstInstanceUpCount}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstInstanceUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstInstanceDownCount}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstInstanceDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstPortRoleSelSemState}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstPortRoleSelSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstDesignatedRoot}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstRootMaxAge}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstRootMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstRootHelloTime}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstRootHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstRootForwardDelay}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstRootForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstOldDesignatedRoot}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstFlushIndicationThreshold}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstFlushIndicationThresholdGet, FsPvrstInstFlushIndicationThresholdSet, FsPvrstInstFlushIndicationThresholdTest, FsPvrstInstBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstBridgeTableINDEX, 1, 0, 0, "0"},

{{12,FsPvrstInstTotalFlushCount}, GetNextIndexFsPvrstInstBridgeTable, FsPvrstInstTotalFlushCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstInstPortIndex}, GetNextIndexFsPvrstInstPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortEnableStatus}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortEnableStatusGet, FsPvrstInstPortEnableStatusSet, FsPvrstInstPortEnableStatusTest, FsPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortPathCost}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortPathCostGet, FsPvrstInstPortPathCostSet, FsPvrstInstPortPathCostTest, FsPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortPriority}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortPriorityGet, FsPvrstInstPortPrioritySet, FsPvrstInstPortPriorityTest, FsPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstPortTableINDEX, 2, 0, 0, "128"},

{{12,FsPvrstInstPortDesignatedRoot}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortDesignatedBridge}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortDesignatedPort}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortOperVersion}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProtocolMigration}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProtocolMigrationGet, FsPvrstInstPortProtocolMigrationSet, FsPvrstInstPortProtocolMigrationTest, FsPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortForwardTransitions}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortReceivedBpdus}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortReceivedBpdusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortRxConfigBpduCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortRxTcnBpduCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTransmittedBpdus}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTransmittedBpdusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTxConfigBpduCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTxTcnBpduCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTxSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTxSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProtMigrationSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProtMigrationSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstProtocolMigrationCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortRole}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstCurrentPortRole}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortInfoSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortRoleTransitionSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortStateTransitionSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTopologyChangeSemState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortEffectivePortState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortHelloTime}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortMaxAge}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortForwardDelay}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortHoldTime}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortAdminPathCost}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortAdminPathCostGet, FsPvrstInstPortAdminPathCostSet, FsPvrstInstPortAdminPathCostTest, FsPvrstInstPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},
{{12,FsPvrstInstPortOldPortState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortLoopInconsistentState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, "2"},

{{12,FsPvrstInstPortRootInconsistentState}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, "2"},

{{12,FsPvrstInstPortTCDetectedCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTCReceivedCount}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTCDetectedTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortTCReceivedTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProposalPktsSent}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProposalPktsRcvd}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProposalPktSentTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortProposalPktRcvdTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortAgreementPktSent}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortAgreementPktRcvd}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortAgreementPktSentTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstInstPortAgreementPktRcvdTimeStamp}, GetNextIndexFsPvrstInstPortTable, FsPvrstInstPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPvrstInstPortTableINDEX, 2, 0, 0, NULL},

{{10,FsPvrstCalcPortPathCostOnSpeedChg}, NULL, FsPvrstCalcPortPathCostOnSpeedChgGet, FsPvrstCalcPortPathCostOnSpeedChgSet, FsPvrstCalcPortPathCostOnSpeedChgTest, FsPvrstCalcPortPathCostOnSpeedChgDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsPvrstGlobalBpduGuard}, NULL, FsPvrstGlobalBpduGuardGet, FsPvrstGlobalBpduGuardSet, FsPvrstGlobalBpduGuardTest, FsPvrstGlobalBpduGuardDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPvrstForceProtocolVersion}, NULL, FsPvrstForceProtocolVersionGet, FsPvrstForceProtocolVersionSet, FsPvrstForceProtocolVersionTest, FsPvrstForceProtocolVersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsPvrstFlushInterval}, NULL, FsPvrstFlushIntervalGet, FsPvrstFlushIntervalSet, FsPvrstFlushIntervalTest, FsPvrstFlushIntervalDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsPvrstSetTraps}, NULL, FsPvrstSetTrapsGet, FsPvrstSetTrapsSet, FsPvrstSetTrapsTest, FsPvrstSetTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPvrstGenTrapType}, NULL, FsPvrstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsPvrstErrTrapType}, NULL, FsPvrstErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsPvrstPortTrapIndex}, GetNextIndexFsPvrstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortMigrationType}, GetNextIndexFsPvrstPortTrapNotificationTable, FsPvrstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPktErrType}, GetNextIndexFsPvrstPortTrapNotificationTable, FsPvrstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPktErrVal}, GetNextIndexFsPvrstPortTrapNotificationTable, FsPvrstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPvrstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsPvrstPortRoleType}, GetNextIndexFsPvrstPortRoleTrapNotificationTable, FsPvrstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},

{{12,FsPvrstOldRoleType}, GetNextIndexFsPvrstPortRoleTrapNotificationTable, FsPvrstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPvrstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},
};
tMibData fspvrsEntry = { 122, fspvrsMibEntry };

#endif /* _FSPVRSDB_H */

