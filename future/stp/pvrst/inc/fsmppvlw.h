/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppvlw.h,v 1.15 2017/09/19 13:50:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsMIPvrstGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsMIPvrstGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIPvrstGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPvrstGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIPvrstGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFuturePvrstTable. */
INT1
nmhValidateIndexInstanceFsMIFuturePvrstTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFuturePvrstTable  */

INT1
nmhGetFirstIndexFsMIFuturePvrstTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFuturePvrstTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstModuleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstNoOfActiveInstances ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstBrgAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIPvrstUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPathCostDefaultType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstDynamicPathCostCalculation ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstTrace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstDebug ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstBufferOverFlowCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstMemAllocFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstContextName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
INT1
nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstGlobalBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstForceProtocolVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstFlushInterval ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstModuleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPathCostDefaultType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstDynamicPathCostCalculation ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstTrace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstDebug ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstCalcPortPathCostOnSpeedChg ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstGlobalBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstForceProtocolVersion ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstFlushInterval ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPathCostDefaultType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstDynamicPathCostCalculation ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstTrace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstGlobalBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstForceProtocolVersion ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstFlushInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFuturePvrstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFuturePvrstPortTable. */
INT1
nmhValidateIndexInstanceFsMIFuturePvrstPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFuturePvrstPortTable  */

INT1
nmhGetFirstIndexFsMIFuturePvrstPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFuturePvrstPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstPortAdminEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortOperEdgePortStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstBridgeDetectionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortEnabledStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstEncapType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortAdminPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortOperPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortInvalidBpdusRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstRootInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortLoopGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortLoopInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstBpduFilter ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortAutoEdge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortTypeInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortPVIDInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortBpduGuardAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstPortAdminEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortEnabledStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstRootGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstEncapType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortAdminPointToPoint ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstBpduFilter ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortAutoEdge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstPortBpduGuardAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstPortAdminEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortEnabledStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstEncapType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortAdminPointToPoint ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstBpduFilter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortAutoEdge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstPortBpduGuardAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFuturePvrstPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPvrstInstBridgeTable. */
INT1
nmhValidateIndexInstanceFsMIPvrstInstBridgeTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPvrstInstBridgeTable  */

INT1
nmhGetFirstIndexFsMIPvrstInstBridgeTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPvrstInstBridgeTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstInstBridgePriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstRootCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstRootPort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstBridgeMaxAge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstBridgeHelloTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstBridgeForwardDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstHoldTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstTxHoldCount ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstTimeSinceTopologyChange ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstTopChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstNewRootCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstInstanceUpCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstInstanceDownCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortRoleSelSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPvrstInstRootMaxAge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstRootHelloTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstRootForwardDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstOldDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPvrstInstFlushIndicationThreshold ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstTotalFlushCount ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstInstBridgePriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstBridgeMaxAge ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstBridgeHelloTime ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstBridgeForwardDelay ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstTxHoldCount ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstFlushIndicationThreshold ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstInstBridgePriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstBridgeMaxAge ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstBridgeHelloTime ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstTxHoldCount ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPvrstInstBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPvrstInstPortTable. */
INT1
nmhValidateIndexInstanceFsMIPvrstInstPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPvrstInstPortTable  */

INT1
nmhGetFirstIndexFsMIPvrstInstPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPvrstInstPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstInstPortEnableStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPvrstInstPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPvrstInstPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPvrstInstPortOperVersion ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortProtocolMigration ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortReceivedBpdus ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortRxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortRxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTransmittedBpdus ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTxSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortProtMigrationSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstProtocolMigrationCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstCurrentPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortInfoSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortRoleTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortStateTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortTopologyChangeSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortHelloTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortMaxAge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortForwardDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortHoldTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortAdminPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortOldPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortLoopInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortRootInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstInstPortTCDetectedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTCReceivedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTCDetectedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortTCReceivedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortProposalPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortProposalPktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortProposalPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortProposalPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortAgreementPktSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortAgreementPktRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortAgreementPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPvrstInstPortAgreementPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstInstPortEnableStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstPortPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstPortPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstPortProtocolMigration ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPvrstInstPortAdminPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstInstPortEnableStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstPortPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstPortPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPvrstInstPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPvrstInstPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsPvrstSetGlobalTrapOption ARG_LIST((INT4 *));

INT1
nmhGetFsMIPvrstGlobalErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsPvrstSetGlobalTrapOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsPvrstSetGlobalTrapOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsPvrstSetGlobalTrapOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsPvrstTrapsControlTable. */
INT1
nmhValidateIndexInstanceFsMIFsPvrstTrapsControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsPvrstTrapsControlTable  */

INT1
nmhGetFirstIndexFsMIFsPvrstTrapsControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsPvrstTrapsControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstSetTraps ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstGenTrapType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPvrstSetTraps ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPvrstSetTraps ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsPvrstTrapsControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPvrstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMIPvrstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPvrstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMIPvrstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPvrstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstPktErrVal ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIPvrstPortRoleTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMIPvrstPortRoleTrapNotificationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPvrstPortRoleTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMIPvrstPortRoleTrapNotificationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPvrstPortRoleTrapNotificationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPvrstPortRoleType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPvrstOldRoleType ARG_LIST((INT4  , INT4 ,INT4 *));
