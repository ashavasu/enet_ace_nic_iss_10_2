/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvprot.h,v 1.23 2017/12/29 09:31:21 siva Exp $
 *
 * Description: This file contains PVRST Module Prototype         
 *              and definitions. 
 *
 *****************************************************************************/

#ifndef _ASTVPROT_H
#define _ASTVPROT_H

/******************************************************************************/
/*                                   astvsys.c                                */
/******************************************************************************/

/* SYSTEM MODULE */
INT4 PvrstModuleEnable (VOID);
INT4 PvrstComponentEnable (VOID);
INT4 PvrstModuleDisable (VOID);
INT4 PvrstComponentDisable (VOID);
INT4 PvrstCreateInstance (UINT2 u2InstanceId);
INT4 PvrstWrCreateInstance (UINT2 u2InstanceId);
INT4 PvrstDeleteInstance (UINT2 u2InstanceId);
INT4 PvrstEnableInstance (tVlanId VlanId);
INT4 PvrstInitInstance (UINT2 u2InstanceId);
INT4 
PvrstInitPerStBrgInfo (tAstPerStBridgeInfo * pPerStBrgInfo, UINT2 u2InstanceId);
VOID PvrstInitStateMachines(VOID);
INT4 PvrstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum);
INT4 PvrstPerStPortCreate (UINT2, UINT2);
INT4 PvrstDeletePort (UINT2);
VOID PvrstInitGlobalPortInfo (UINT2, tAstPortEntry *);
VOID PvrstInitPerStPortinfo (tAstPerStPortInfo *pPerStPortInfo);
VOID PvrstReInitPvrstInfo (VOID);
VOID PvrstReInitPvrstInstInfo (UINT2 u2PortNum, tVlanId VlanId);
INT4 PvrstStartSemsForPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPerStEnablePort (UINT2 u2PortNum, UINT2 u2InstanceId, UINT1 u1TrigType);
INT4 PvrstPerStDisablePort (UINT2 u2PortNum, tVlanId VlanId, UINT1 u1TrigType);
INT4 PvrstPerStPortDelete( UINT2 u2PortNum, tVlanId VlanId);
INT4 PvrstHandleInBpdu (tPvrstBpdu * pRcvdBpdu, UINT2 u2PortNum);
VOID PvrstPortRcvdFlagsStatsUpdt (tPvrstBpdu *pRcvBpdu, UINT2 u2PortNum,
                           UINT2 u2InstanceId);
INT4 PvrstHwInit (VOID);
INT4 PvrstHwDeInit (VOID);
INT4 PvrstAddTrunkPort (UINT4 u4PortNo);
INT4 PvrstAddTaggedPort (UINT4 u4PortNo , UINT2 u2InstanceId);
INT4 PvrstAddAccessPort(UINT4 u4PortNo, UINT2 u2InstanceId);
INT4 PvrstAddHybridPort (UINT4 u4PortNo);
INT4 PvrstConfigPvrstStatusPerPort (UINT4 u4PortNo, tAstBoolean bStatus);
INT4 PvrstSetPortGuards (UINT4 u4PortNo,UINT4 u4GuardType, tAstBoolean bStatus);
INT4 PvrstConfigPvrstInstStatusPerPort (tVlanId VlanId, UINT4 u4PortNo, 
                                        tAstBoolean bStatus);
INT4 PvrstReCalculatePathcost (UINT4);
INT4 PvrstSetPortBPDUGuard (UINT4 u4PortNo,UINT4 u4GuardType, UINT4 u4BpduGuardStatus);
INT4 PvrstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction);
INT4 PvrstSetProtocolMigration (UINT4 u4IfIndex, UINT2 u2VlanId);

/******************************************************************************/
/*                                   astvutil.c                               */
/******************************************************************************/
INT4 PvrstGetFreeInstIndex (UINT2 *u2FreeIndex);
UINT2 PvrstGetInstIndexForInstance ( UINT2 u2InstanceId);
INT4 PvrstInitInstIndexMap (VOID);
VOID PvrstDeInitInstIndexMap (VOID);
INT4 PvrstPathcostConfiguredFlag (UINT2 , UINT2 );
INT1 PvrstGetNextIndexPvrstInstBridgeTable (INT4 ,INT4 *);
INT1 PvrstGetNextIndexFsPvrstInstPort (INT4 i4FsPvrstInstVlanId,
      UINT2 u2PortIndex, INT4 * pi4NextPortIndex);
INT1
PvrstGetNextIndexFsMIPvrstInstPort (INT4 i4FsPvrstInstVlanId,
                                  UINT2 u2PortIndex, INT4 *pi4NextPortIndex);
INT4 PvrstValidateInstanceEntry (INT4 );
INT4 PvrstValidatePortEntry (INT4 , INT4 );
INT4 PvrstReleasePortMemBlocks (tAstPortEntry * pPortInfo);
INT4
PvrstCreateSpanningTreeInst (UINT2 u2InstIndex, tAstPerStInfo ** ppPerStInfo);
VOID
PvrstDeleteMstInstance (UINT2 u2InstIndex, tAstPerStInfo * pPerStInfo);

INT4
PvrstGetInstPortRootInconsistentState (UINT2 u2Port, UINT2 VlanId,
                                       INT4 *pi4InstPortRootIncStatus);
PUBLIC VOID
PvrstUtilTicksToDate (UINT4 u4msecs, UINT1 *pu1DateFormat);
/******************************************************************************/
/*                                   astptmr.c                                */
/******************************************************************************/

/* TIMER MODULE */
INT4 PvrstStartTimer (VOID *, UINT2, UINT1, UINT2);
INT4 PvrstStopTimer (VOID *, UINT2, UINT1);
INT4 PvrstTmrExpiryHandler (tAstTimer *);
INT4 PvrstStopAllRunningTimers (tAstPortEntry * ,
                         tAstPerStPortInfo * );
INT4 PvrstForceVersionStopAllRunningTimers (UINT2 u2PortNum);

/******************************************************************************/
/*                                  astpvrpism.c                                */
/******************************************************************************/

/* PORT INFORMATION STATE MACHINE */
INT4 PvrstPortInfoMachine(UINT2 u2Event, tAstPerStPortInfo *pPerStPortInfo,
                        tPvrstBpdu *pRcvdBpdu);
INT4 PvrstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeDisabled (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeAged (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeUpdate (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeCurrent (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeReceive (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeSuperior (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeRepeat (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmMakeAgreement (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmRcvBpdu (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmGetMsgType (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmUpdtBpduVersion (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmSetTcFlags (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmRecordProposed (tAstPerStPortInfo *, tPvrstBpdu *);
INT4 PvrstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo *);
VOID PvrstPortInfoSmRecordTimes (tAstPerStPortInfo * pPerStPortInfo,
                               tPvrstBpdu * pRcvdBpdu);
#ifdef IEEE8021Y_Z11_WANTED
INT4
PvrstPortInfoSmMakeNotDesg (tAstPerStPortInfo * pPerStPortInfo,
                            tPvrstBpdu * pRcvdBpdu);

VOID
PvrstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                              tPvrstBpdu * pRcvBpdu);

INT4
PvrstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                                 tPvrstBpdu * pRcvdBpdu);

#endif
#ifdef IEEE8021Y_Z12_WANTED
INT4 PvrstPortInfoSmMakeInferiorDesg (tAstPerStPortInfo * pPerStPortInfo,
                                     tPvrstBpdu * pRcvdBpdu);
VOID PvrstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                                 tPvrstBpdu * pRcvdBpdu);
#endif /* IEEE8021Y_Z12_WANTED */
INT4 PvrstPortInfoSmEventImpossible (tAstPerStPortInfo *, tPvrstBpdu *);
VOID PvrstInitPortInfoMachine(VOID);


/******************************************************************************/
/*                                  astpvrpmsm.c                                */
/******************************************************************************/

/* PORT MIGRATION STATE MACHINE */
INT4 PvrstPortMigrationMachine (UINT2 u2Event, 
                                UINT2 u2PortNum, UINT2 u2InstanceId);
INT4 PvrstPmigSmPortEnabledInit (UINT2, UINT2 );
INT4 PvrstPmigSmRcvdStpInSendingRstp (UINT2, UINT2 );
INT4 PvrstPmigSmRcvdRstpInSendingRstp (UINT2, UINT2 );
INT4 PvrstPmigSmRcvdRstpInSendingStp (UINT2, UINT2 );
INT4 PvrstPmigSmRcvdStpInSendingStp (UINT2, UINT2 );
INT4 PvrstPmigSmMakeInit (UINT2, UINT2 );
INT4 PvrstPmigSmMakeSendRstp (UINT2, UINT2 );
INT4 PvrstPmigSmMakeSendingRstp (UINT2, UINT2 );
INT4 PvrstPmigSmMakeSendStp (UINT2, UINT2);
INT4 PvrstPmigSmMakeSendingStp (UINT2, UINT2);
INT4 PvrstPmigSmEventImpossible (UINT2, UINT2 );
VOID PvrstInitProtocolMigrationMachine (VOID);


/******************************************************************************/
/*                                  astpvrrssm.c                                */
/******************************************************************************/

/* PORT ROLE SELECTION STATE MACHINE */
INT4 PvrstPortRoleSelectionMachine (UINT2, UINT2);
INT4 PvrstProleSelSmMakeInit (UINT2);
INT4 PvrstProleSelSmMakeRoleSelection (UINT2);
VOID PvrstProleSelSmUpdtRoleDisabledBridge(UINT2);
VOID PvrstProleSelSmClearReselectBridge (UINT2);
INT4 PvrstProleSelSmUpdtRolesBridge (UINT2);
INT4 PvrstIsDesgPrBetterThanPortPr(UINT2, UINT2);
INT4 PvrstProleSelSmSetSelectedBridge (UINT2);
INT4 PvrstProleSelSmEventImpossible (UINT2);
VOID PvrstInitPortRoleSelectionMachine(VOID);


/******************************************************************************/
/*                                  astpvrrtsm.c                                */
/******************************************************************************/

/* PORT ROLE TRANSITION STATE MACHINE */
VOID PvrstCopyStpParametersToNonDefInst(UINT2 u2PortNo);
VOID PvrstCopyStpParametersToAllInst(INT4 , tAstPerStPortInfo * );
INT4 PvrstHandleStpCompatabilityCh(INT4 , tAstPerStPortInfo * );
INT4 PvrstPortRoleTrMachine (UINT2 , tAstPerStPortInfo * );
INT4 PvrstProleTrSmUpdtInfoReset (tAstPerStPortInfo *); 
INT4 PvrstProleTrSmRerootSetRootPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmRerootSetDesgPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmSyncSetDesgPort (tAstPerStPortInfo *);
INT4 PvrstProleTrSmFwdExpRootPort (tAstPerStPortInfo *);
INT4 PvrstProleTrSmFwdExpDesgPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmRrWhileExpDesgPort (tAstPerStPortInfo *);
INT4 PvrstProleTrSmRbWhileExpRootPort (tAstPerStPortInfo *);
INT4 PvrstProleTrSmIsReRooted (UINT2, UINT2);
INT4 PvrstProleTrSmRoleChanged (tAstPerStPortInfo *);
INT4 PvrstProleTrSmMakeInitPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmMakeBlockPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmMakeRootPort (tAstPerStPortInfo * );
tAstBoolean PvrstIsAllOtherPortsSynced (UINT2, UINT2);
INT4 PvrstProleTrSmAllSyncedSetRootPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstProleTrSmRerootedSetRootPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstProleTrSmMakeDesgPort (tAstPerStPortInfo * );
INT4 PvrstProleTrSmMakeForward (tAstPerStPortInfo * );
INT4 PvrstProleTrSmMakeLearn (tAstPerStPortInfo * );
INT4 PvrstProleTrSmMakeDesignatedListen (tAstPerStPortInfo * );
INT4 PvrstProleTrSmSetSyncBridge (UINT2 ,UINT2);
INT4 PvrstProleTrSmSetReRootBridge (UINT2);
VOID PvrstInitPortRoleTrMachine(VOID);
INT4 PvrstProleTrSmProposedSetRootPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstProleTrSmAgreedSetDesgPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstProleTrSmOperEdgeSetDesgPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstProleTrSmMakeBlockedPort (tAstPerStPortInfo * pPerStPortInfo);
INT4 PvrstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo);
#ifdef IEEE8021Y_Z12_WANTED
INT4 PvrstProleTrSmDisputedSetDesgPort (tAstPerStPortInfo * pPerStPortInfo);
#endif /* IEEE8021Y_Z12_WANTED */


/******************************************************************************/
/*                                  astpvrstsm.c                                */
/******************************************************************************/

/* PORT STATE TRANSITION STATE MACHINE */
INT4 PvrstPortStateTrMachine (UINT2 , tAstPerStPortInfo *); 
INT4 PvrstPstateTrSmMakeDiscarding (tAstPerStPortInfo *);
INT4 PvrstPstateTrSmMakeLearning (tAstPerStPortInfo *);
INT4 PvrstPstateTrSmMakeForwarding (tAstPerStPortInfo *);
INT4 PvrstPstateTrSmEnableLearning (UINT2 );
INT4 PvrstPstateTrSmDisableLearning (UINT2 );
INT4 PvrstPstateTrSmEnableForwarding (UINT2 );
INT4 PvrstPstateTrSmDisableForwarding (UINT2 );
INT4 PvrstPstateTrSmEventImpossible (tAstPerStPortInfo *);
VOID PvrstInitPortStateTrMachine (VOID);


/******************************************************************************/
/*                                  astpvrtcsm.c                                */
/******************************************************************************/

/* TOPOLOGY CHANGE STATE MACHINE */
INT4 PvrstTopoChMachine (UINT2 u2Event, tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeInit (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeInactive (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeDetected (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeNotifiedTcn (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeNotifiedTc (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakePropagating (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeAcknowledged (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmMakeActive (tAstPerStPortInfo *pPerStPortInfo);
VOID PvrstTopoChSmFlushFdb (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmSetTcPropBridge (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmNewTcWhile (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstTopoChSmEventImpossible (tAstPerStPortInfo *pPerStPortInfo);
VOID PvrstInitTopoChStateMachine (VOID);
VOID PvrstTopologyChSmFlushEntries (tAstPerStPortInfo * pPerStPortInfo);
                                 

/******************************************************************************/
/*                                  astpvrtxsm.c                                */
/******************************************************************************/

/* PORT TRANSMIT STATE MACHINE */
INT4 PvrstPortTransmitMachine (UINT2 u2Event, 
                               tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeTransmitInit (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeTransmitConfig (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeTransmitTcn (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeTransmitRstp (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeTransmitPeriodic (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmMakeIdle (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmNewInfoSetIdle (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmHoldTmrExpIdle (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmTxConfig (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmTxTcn (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstPortTxSmTxRstp (tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstFormBpdu (tAstPerStPortInfo *pPerStPortInfo,
                  UINT1 u1MsgType, tAstBufChainHeader *pBuf,
                  UINT4 *pu4DataLength,UINT1 *pu1Flag);
INT4 PvrstPortTxSmEventImpossible (tAstPerStPortInfo *pPerStPortInfo);
VOID PvrstInitPortTxStateMachine (VOID);
INT4 PvrstFillEnetHeader (tAstBufChainHeader *pBpdu, UINT4 *pu4DataLength, 
                        tAstPerStPortInfo *pPerStPortInfo);
INT4 PvrstEncapsulateBpdu ( UINT2 u2InstanceId, tAstBufChainHeader  *pBuf,
        UINT4 *pu4DataLength, UINT2 u2PortNum );
VOID PvrstFillBpdu (tAstPerStPortInfo * pPerStPortInfo, 
   UINT1 **ppu1Bpdu);
VOID PvrstVlanTagFrame (tCRU_BUF_CHAIN_DESC * , tVlanId , UINT1 );
VOID PvrstTagISLHeader (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT2 u2PortNum);
INT4 PvrstPortTxSmCheckIdle (tAstPerStPortInfo * pPerStPortInfo);


/******************************************************************************/
/*                                  astpvrbdsm.c                                */
/******************************************************************************/

/* BRIDGE DETECTION STATE MACHINE */
INT4 PvrstBrgDetectionMachine (UINT2 u2Event, UINT2 u2PortNum);
INT4 PvrstBdSmMakeInit (UINT2 u2PortNum);
VOID PvrstInitBrgDetectionStateMachine (VOID);
INT4 PvrstBdSmMakeEdge (UINT2 u2PortNum);
INT4 PvrstBdSmChkMakeEdge (UINT2 u2PortNum);
INT4 PvrstBdSmMakeNotEdge (UINT2 u2PortNum);
INT4 PvrstBdSmAdminEdgeSetPortDisabled (UINT2 u2Portum);
INT4 PvrstBdSmStartEdgeDelayWhile (UINT2 u2PortNum,UINT2 u2InstIndex);

/**********************************
 * Prototypes for MI routines 
 **********************************/ 
INT4 PvrstCreateProtoMemPools (VOID);
INT4 PvrstDeleteProtoMemPools (VOID);

/**********************************
 * Prototypes for astvsnmp.c 
 **********************************/ 
INT4 PvrstSetInstPortPathCost (UINT2, UINT2, UINT4);
INT4 PvrstSetInstZeroPortPathCost (tVlanId VlanId, UINT2 u2PortNum);
INT4 PvrstSetPortPathcostConfigured (UINT2 u2PortNum,UINT2 u2InstIndex, UINT4 u4Val);
INT4 PvrstSetAdminPointToPoint (UINT2 u2PortNo, UINT1 u1AdminPToP);
INT4 PvrstSetBridgePriority (UINT2 u2InstanceId, UINT2 u2Priority);
VOID PvrstSetHelloTime (UINT2 u2InstanceId, UINT2 u2HelloTime);
VOID PvrstSetTxHoldCount (UINT1 u1TxHoldCount, UINT2 u2InstanceId);
VOID PvrstSetForwardDelay (UINT2 u2InstanceId, UINT2 u2ForwardDelay);
INT4 PvrstSetPortPriority (UINT2 u2InstanceId,UINT2 u2PortNo,UINT1 u1Priority);
INT4 PvrstSetBrgMaxAge (UINT2 , UINT2 );
INT4 PvrstSetDynamicPathcostCalc(UINT1);
INT4 PvrstSetDynaPathcostCalcLagg(UINT1);
INT4 PvrstChangePVID (UINT4 , tVlanId , tVlanId );
INT4 PvrstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean bStatus);
INT4 PvrstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus);

/******************************************************************************/
/*                                  pvrstislcrc.c                                */
/******************************************************************************/
UINT4 reflect_bits (UINT4 crc, INT4 bitnum);
VOID crc32_isl_calc (UINT1* p, UINT4 len,UINT1 *out);

/******************************************************************************/
/*                                  astpmbsm.c                                */
/******************************************************************************/
#ifdef MBSM_WANTED
INT1 FsMiPvrstMbsmNpSetVlanPortState (UINT4 ,UINT4 ,tVlanId ,UINT1 ,
                                         tMbsmSlotInfo * );
#endif

/******************************************************************************/
/*                                  astvhwwr.c                                */
/******************************************************************************/
INT4
PvrstHwSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId VlanId, UINT1 u1PortState);

#endif /* _ASTVPROT_H */

