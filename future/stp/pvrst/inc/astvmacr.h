/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvmacr.h,v 1.15 2017/12/11 09:58:31 siva Exp $
 *
 * Description: This file contains definitions of macros used  
 *              in the PVRST Module. 
 *
 *****************************************************************************/

#ifndef _ASTVMACR_H
#define _ASTVMACR_H

#define AST_PVRST_VLAN_MAP_MEMBLK_COUNT  (MAX_AST_CONTEXTS)
#define AST_PVRST_PERST_INFO_MEMBLK_COUNT  (AST_MAX_PVRST_INSTANCES)
#define AST_PVRST_PERST_PORT_INFO_MEMBLK_COUNT (AST_MAX_PORTS_PER_CONTEXT * AST_MAX_PVRST_INSTANCES)
#define AST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT (AST_MAX_PVRST_INSTANCES * 2)

#define VLAN_DEF_VLAN_ID        gVlanDefVlanId
#define AST_PERST_PVRST_BRGINFO_MEMPOOL_ID  \
        gAstGlobalInfo.PvrstBrgInfoMemPoolId
#define AST_PERST_PVRST_RST_PORTINFO_MEMPOOL_ID \
        gAstGlobalInfo.PvrstRstPortInfoMemPoolId

#define AST_PVRST_VLAN_TO_INDEX_MAPPING_MEMPOOL_ID \
        gAstGlobalInfo.PvrstVlanToIndexMapMemPoolId

/******************************************************************************/
/* State Machine Variables */
/******************************************************************************/
#define PVRST_PORT_INFO_MACHINE     gAstGlobalInfo.aaPvrstPortInfoMachine
#define PVRST_PORT_ROLE_SEL_MACHINE gAstGlobalInfo.aaPvrstPortRoleSelMachine
#define PVRST_PORT_ROLE_TR_MACHINE  gAstGlobalInfo.aaPvrstPortRoleTrMachine
#define PVRST_PORT_STATE_TR_MACHINE gAstGlobalInfo.aaPvrstPortStateTrMachine
#define PVRST_PORT_MIG_MACHINE      gAstGlobalInfo.aaPvrstPortMigMachine
#define PVRST_TOPO_CH_MACHINE       gAstGlobalInfo.aaPvrstTopoChMachine
#define PVRST_PORT_TX_MACHINE       gAstGlobalInfo.aaPvrstPortTxMachine
#define PVRST_BRG_DET_MACHINE       gAstGlobalInfo.aaPvrstBrgDetStateMachine   

#define PVRST_GET_PERST_INFO(u2InstanceId) \
  ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId])

#define PVRST_GET_PERST_BRG_INFO(u2InstanceId) \
   &((PVRST_GET_PERST_INFO(u2InstanceId))->PerStBridgeInfo)
   
#define PVRST_GET_PERST_PVRST_BRG_INFO(u2InstanceId) \
   (PVRST_GET_PERST_BRG_INFO(u2InstanceId))->pPerStPvrstBridgeInfo

#define AST_INSTANCE_UP_COUNT(u2MstInst) \
            AST_CURR_CONTEXT_INFO()->pInstanceUpCount[u2MstInst]
#define PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId) \
   (PVRST_GET_PERST_INFO(u2InstanceId))->ppPerStPortInfo[u2PortNum - 1]

#define PVRST_GET_PERST_RST_PORT_INFO(u2PortNum, u2InstanceId) \
    &(PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))\
             ->PerStRstPortInfo
           
#define PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum,u2InstanceId)    \
   (PVRST_GET_PERST_RST_PORT_INFO(u2PortNum, u2InstanceId))\
            ->pPerStPvrstRstPortInfo

#define PVRST_SET_CHANGED_FLAG(u2InstanceId, u2PortNum) \
  ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u1ChangedFlag)

#define   PVRST_GET_PORT_ROLE(u2InstanceId ,u2PortNum) \
              PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId)->u1PortRole
          
#define PVRST_GET_BRIDGE_PRIORITY(u2BridgePriority, u2Priority) \
{\
      u2Priority = (UINT2)(u2BridgePriority & 0xFFFF); \
}

#define PVRST_GET_CHANGED_FLAG(u2InstanceId, u2PortNum) \
            PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId)->u1ChangedFlag


#define PVRST_IS_INSTANCE_VALID(i4FsPvrstInstanceIndex) \
                (((i4FsPvrstInstanceIndex < PVRST_MIN_INSTANCES) || \
                (i4FsPvrstInstanceIndex > AST_MAX_PVRST_INSTANCES)) ? \
                PVRST_FALSE:PVRST_TRUE) 

#define PVRST_IS_VALID_VLANID(Vid) \
                (((Vid < PVRST_MIN_INSTANCES) || (Vid > VLAN_MAX_VLAN_ID))?\
                PVRST_FALSE:PVRST_TRUE)

#define PVRST_SET_BRIDGE_PRIORITY(u2Priority, u2BridgePriority) \
{\
      u2Priority = (UINT2) (u2Priority & 0xF000);\
      u2BridgePriority = (UINT2) (u2BridgePriority & 0x0FFF);\
      u2BridgePriority = u2BridgePriority | u2Priority;\
}

#define PVRST_GET_PORT_PRIORITY(u1PortPriority, u1Priority)\
{\
      u1Priority = (UINT1)(u1PortPriority & 0x0F); \
      u1PortPriority = (UINT1)(u1PortPriority & 0xF0); \
      u1Priority = u1Priority | u1PortPriority;\
}
#define PVRST_GET_NO_OF_ACTIVE_INSTANCES \
  (AST_CURR_CONTEXT_INFO ())->u2NoOfActiveInstances

#define PVRST_VLAN_TO_INDEX_MAP_PTR \
  (AST_CURR_CONTEXT_INFO ())->pInstIndexMap

#define PVRST_VLAN_TO_INDEX_MAP(u2InstIndex) \
  (AST_CURR_CONTEXT_INFO ())->pInstIndexMap [u2InstIndex - 1]

#define PVRST_SET_PORT_PRIORITY(u1Priority, u1PortPriority) \
{\
      u1PortPriority = (UINT1)(u1Priority & 0x0F); \
      u1Priority = (UINT1)(u1Priority & 0xF0); \
      u1PortPriority = u1Priority | u1PortPriority;\
}

#define PVRST_INCR_INSTANCE_UP_COUNT(u2InstanceId) \
  ((AST_CURR_CONTEXT_INFO())->pInstanceUpCount[u2InstanceId])++
 /* ((PVRST_GET_PERST_BRG_INFO(u2InstanceId))->u4InstanceUpCount)++*/

#define  PVRST_VLAN_IS_NULL_PORTLIST(au1List) \
         ((MEMCMP (au1List, gPvrstNullPortList, sizeof (gPvrstNullPortList)) == 0) \
          ? PVRST_TRUE : PVRST_FALSE)


/* au1ResultList = au1List1 ^ au1List2 */
#define PVRST_VLAN_XOR_PORT_LIST(au1ResultList, au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < BRG_PORT_LIST_SIZE/*VLAN_PORT_LIST_SIZE*/;\
                      u2ByteIndex++) {\
                    au1ResultList[u2ByteIndex] = au1List1[u2ByteIndex] ^ au1List2[u2ByteIndex];\
                 }\
              }
#ifndef STATISTICS_WANTED
#define PVRST_INCR_NEW_ROOTBRIDGE_COUNT(u2InstanceId) \
 ((PVRST_GET_PERST_BRG_INFO(u2InstanceId))->u4NewRootIdCount)++

#define PVRST_INCR_NUM_OF_TOPO_CHANGES(u2InstanceId) \
        (PVRST_GET_PERST_BRG_INFO(u2InstanceId))->u4NumTopoCh++

#define PVRST_INCR_RX_CONFIG_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumConfigBpdusRxd)++

#define PVRST_INCR_RX_TCN_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumTcnBpdusRxd)++

#define PVRST_INCR_RX_INST_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumRstBpdusRxd)++

#define PVRST_INCR_TX_CONFIG_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumConfigBpdusTxd)++

#define PVRST_INCR_TX_TCN_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumTcnBpdusTxd)++

#define PVRST_INCR_TX_INST_BPDU_COUNT(u2PortNum, u2InstanceId) \
        ((PVRST_GET_PERST_PVRST_RST_PORT_INFO(u2PortNum, u2InstanceId))->u4NumRstBpdusTxd)++

#define PVRST_INCR_TC_DETECTED_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4TcDetectedCount)++

#define PVRST_INCR_TC_RX_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4TcRcvdCount)++

#define PVRST_INCR_RX_PROPOSAL_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4ProposalRcvdCount)++

#define PVRST_INCR_RX_AGREEMENT_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4AgreementRcvdCount)++

#define PVRST_INCR_TX_PROPOSAL_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4ProposalTxCount)++

#define PVRST_INCR_TX_AGREEMENT_COUNT(u2PortNum,u2InstanceId) \
      ((PVRST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4AgreementTxCount)++

#else
#define PVRST_INCR_NEW_ROOTBRIDGE_COUNT(u2InstanceId)

#define PVRST_INCR_NUM_OF_TOPO_CHANGES(u2InstanceId)

#define PVRST_INCR_RX_CONFIG_BPDU_COUNT(u2PortNum, u2InstanceId)

#define PVRST_INCR_RX_TCN_BPDU_COUNT(u2PortNum, u2InstanceId)

#define PVRST_INCR_RX_INST_BPDU_COUNT(u2PortNum, u2InstanceId)

#define PVRST_INCR_TX_CONFIG_BPDU_COUNT(u2PortNum, u2InstanceId)

#define PVRST_INCR_TX_TCN_BPDU_COUNT(u2PortNum, u2InstanceId)

#define PVRST_INCR_TX_INST_BPDU_COUNT(u2PortNum, u2InstanceId)
#endif

#endif

 
/* _ASTVMACR_H */
