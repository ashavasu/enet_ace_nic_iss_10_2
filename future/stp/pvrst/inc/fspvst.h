/*$Id: fspvst.h,v 1.9 2017/09/19 13:50:24 siva Exp $ */
/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */
static struct MIB_OID orig_mib_oid_table_MI[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"transmission",        "1.3.6.1.2.1.10"},
{"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dStp",        "1.3.6.1.2.1.17.2"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"issExt",        "1.3.6.1.4.1.2076.81.8"},
{"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
{"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
{"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
{"futureMIPvrstMIB",        "1.3.6.1.4.1.2076.154"},
{"fsMIFuturePvrst",        "1.3.6.1.4.1.2076.154.1"},
{"fsMIPvrstGlobalTrace",        "1.3.6.1.4.1.2076.154.1.1"},
{"fsMIPvrstGlobalDebug",        "1.3.6.1.4.1.2076.154.1.2"},
{"fsMIFuturePvrstTable",        "1.3.6.1.4.1.2076.154.1.3"},
{"fsMIFuturePvrstEntry",        "1.3.6.1.4.1.2076.154.1.3.1"},
{"fsMIFuturePvrstContextId",        "1.3.6.1.4.1.2076.154.1.3.1.1"},
{"fsMIPvrstSystemControl",        "1.3.6.1.4.1.2076.154.1.3.1.2"},
{"fsMIPvrstModuleStatus",        "1.3.6.1.4.1.2076.154.1.3.1.3"},
{"fsMIPvrstNoOfActiveInstances",        "1.3.6.1.4.1.2076.154.1.3.1.4"},
{"fsMIPvrstBrgAddress",        "1.3.6.1.4.1.2076.154.1.3.1.5"},
{"fsMIPvrstUpCount",        "1.3.6.1.4.1.2076.154.1.3.1.6"},
{"fsMIPvrstDownCount",        "1.3.6.1.4.1.2076.154.1.3.1.7"},
{"fsMIPvrstPathCostDefaultType",        "1.3.6.1.4.1.2076.154.1.3.1.8"},
{"fsMIPvrstDynamicPathCostCalculation",        "1.3.6.1.4.1.2076.154.1.3.1.9"},
{"fsMIPvrstTrace",        "1.3.6.1.4.1.2076.154.1.3.1.10"},
{"fsMIPvrstDebug",        "1.3.6.1.4.1.2076.154.1.3.1.11"},
{"fsMIPvrstBufferOverFlowCount",        "1.3.6.1.4.1.2076.154.1.3.1.12"},
{"fsMIPvrstMemAllocFailureCount",        "1.3.6.1.4.1.2076.154.1.3.1.13"},
{"fsMIPvrstContextName",        "1.3.6.1.4.1.2076.154.1.3.1.14"},
{"fsMIPvrstFlushInterval",        "1.3.6.1.4.1.2076.154.1.3.1.18"},
{"fsMIFuturePvrstPortTable",        "1.3.6.1.4.1.2076.154.1.4"},
{"fsMIFuturePvrstPortEntry",        "1.3.6.1.4.1.2076.154.1.4.1"},
{"fsMIPvrstPort",        "1.3.6.1.4.1.2076.154.1.4.1.1"},
{"fsMIPvrstPortAdminEdgeStatus",        "1.3.6.1.4.1.2076.154.1.4.1.2"},
{"fsMIPvrstPortOperEdgePortStatus",        "1.3.6.1.4.1.2076.154.1.4.1.3"},
{"fsMIPvrstBridgeDetectionSemState",        "1.3.6.1.4.1.2076.154.1.4.1.4"},
{"fsMIPvrstPortEnabledStatus",        "1.3.6.1.4.1.2076.154.1.4.1.5"},
{"fsMIPvrstRootGuard",        "1.3.6.1.4.1.2076.154.1.4.1.6"},
{"fsMIPvrstBpduGuard",        "1.3.6.1.4.1.2076.154.1.4.1.7"},
{"fsMIPvrstEncapType",        "1.3.6.1.4.1.2076.154.1.4.1.8"},
{"fsMIPvrstPortAdminPointToPoint",        "1.3.6.1.4.1.2076.154.1.4.1.9"},
{"fsMIPvrstPortOperPointToPoint",        "1.3.6.1.4.1.2076.154.1.4.1.10"},
{"fsMIPvrstPortInvalidBpdusRcvd",        "1.3.6.1.4.1.2076.154.1.4.1.11"},
{"fsMIPvrstPortInvalidConfigBpduRxCount",        "1.3.6.1.4.1.2076.154.1.4.1.12"},
{"fsMIPvrstPortInvalidTcnBpduRxCount",        "1.3.6.1.4.1.2076.154.1.4.1.13"},
{"fsMIPvrstPortBpduInconsistentState",        "1.3.6.1.4.1.2076.154.1.4.1.22"},
{"fsMIPvrstPortRcvInfoWhileExpCount",        "1.3.6.1.4.1.2076.154.1.4.1.26"},
{"fsMIPvrstPortRcvInfoWhileExpTimeStamp",        "1.3.6.1.4.1.2076.154.1.4.1.27"},
{"fsMIPvrstPortImpStateOccurCount",        "1.3.6.1.4.1.2076.154.1.4.1.28"},
{"fsMIPvrstPortImpStateOccurTimeStamp",        "1.3.6.1.4.1.2076.154.1.4.1.29"},
{"fsMIPvrstPortRowStatus",        "1.3.6.1.4.1.2076.154.1.4.1.14"},
{"fsMIPvrstInstBridgeTable",        "1.3.6.1.4.1.2076.154.1.5"},
{"fsMIPvrstInstBridgeEntry",        "1.3.6.1.4.1.2076.154.1.5.1"},
{"fsMIPvrstInstVlanId",        "1.3.6.1.4.1.2076.154.1.5.1.1"},
{"fsMIPvrstInstBridgePriority",        "1.3.6.1.4.1.2076.154.1.5.1.2"},
{"fsMIPvrstInstRootCost",        "1.3.6.1.4.1.2076.154.1.5.1.3"},
{"fsMIPvrstInstRootPort",        "1.3.6.1.4.1.2076.154.1.5.1.4"},
{"fsMIPvrstInstBridgeMaxAge",        "1.3.6.1.4.1.2076.154.1.5.1.5"},
{"fsMIPvrstInstBridgeHelloTime",        "1.3.6.1.4.1.2076.154.1.5.1.6"},
{"fsMIPvrstInstBridgeForwardDelay",        "1.3.6.1.4.1.2076.154.1.5.1.7"},
{"fsMIPvrstInstHoldTime",        "1.3.6.1.4.1.2076.154.1.5.1.8"},
{"fsMIPvrstInstTxHoldCount",        "1.3.6.1.4.1.2076.154.1.5.1.9"},
{"fsMIPvrstInstTimeSinceTopologyChange",        "1.3.6.1.4.1.2076.154.1.5.1.10"},
{"fsMIPvrstInstTopChanges",        "1.3.6.1.4.1.2076.154.1.5.1.11"},
{"fsMIPvrstInstNewRootCount",        "1.3.6.1.4.1.2076.154.1.5.1.12"},
{"fsMIPvrstInstInstanceUpCount",        "1.3.6.1.4.1.2076.154.1.5.1.13"},
{"fsMIPvrstInstInstanceDownCount",        "1.3.6.1.4.1.2076.154.1.5.1.14"},
{"fsMIPvrstInstPortRoleSelSemState",        "1.3.6.1.4.1.2076.154.1.5.1.15"},
{"fsMIPvrstInstDesignatedRoot",        "1.3.6.1.4.1.2076.154.1.5.1.16"},
{"fsMIPvrstInstRootMaxAge",        "1.3.6.1.4.1.2076.154.1.5.1.17"},
{"fsMIPvrstInstRootHelloTime",        "1.3.6.1.4.1.2076.154.1.5.1.18"},
{"fsMIPvrstInstRootForwardDelay",        "1.3.6.1.4.1.2076.154.1.5.1.19"},
{"fsMIPvrstInstOldDesignatedRoot","1.3.6.1.4.1.2076.154.1.5.1.20"},
{"fsMIPvrstInstFlushIndicationThreshold",        "1.3.6.1.4.1.2076.154.1.5.1.21"},
{"fsMIPvrstInstTotalFlushCount",        "1.3.6.1.4.1.2076.154.1.5.1.22"},
{"fsMIPvrstInstPortTable",        "1.3.6.1.4.1.2076.154.1.6"},
{"fsMIPvrstInstPortEntry",        "1.3.6.1.4.1.2076.154.1.6.1"},
{"fsMIPvrstInstPortIndex",        "1.3.6.1.4.1.2076.154.1.6.1.1"},
{"fsMIPvrstInstPortEnableStatus",        "1.3.6.1.4.1.2076.154.1.6.1.2"},
{"fsMIPvrstInstPortPathCost",        "1.3.6.1.4.1.2076.154.1.6.1.3"},
{"fsMIPvrstInstPortPriority",        "1.3.6.1.4.1.2076.154.1.6.1.4"},
{"fsMIPvrstInstPortDesignatedRoot",        "1.3.6.1.4.1.2076.154.1.6.1.5"},
{"fsMIPvrstInstPortDesignatedBridge",        "1.3.6.1.4.1.2076.154.1.6.1.6"},
{"fsMIPvrstInstPortDesignatedPort",        "1.3.6.1.4.1.2076.154.1.6.1.7"},
{"fsMIPvrstInstPortOperVersion",        "1.3.6.1.4.1.2076.154.1.6.1.8"},
{"fsMIPvrstInstPortProtocolMigration",        "1.3.6.1.4.1.2076.154.1.6.1.9"},
{"fsMIPvrstInstPortState",        "1.3.6.1.4.1.2076.154.1.6.1.10"},
{"fsMIPvrstInstPortForwardTransitions",        "1.3.6.1.4.1.2076.154.1.6.1.11"},
{"fsMIPvrstInstPortReceivedBpdus",        "1.3.6.1.4.1.2076.154.1.6.1.12"},
{"fsMIPvrstInstPortRxConfigBpduCount",        "1.3.6.1.4.1.2076.154.1.6.1.13"},
{"fsMIPvrstInstPortRxTcnBpduCount",        "1.3.6.1.4.1.2076.154.1.6.1.14"},
{"fsMIPvrstInstPortTransmittedBpdus",        "1.3.6.1.4.1.2076.154.1.6.1.15"},
{"fsMIPvrstInstPortTxConfigBpduCount",        "1.3.6.1.4.1.2076.154.1.6.1.16"},
{"fsMIPvrstInstPortTxTcnBpduCount",        "1.3.6.1.4.1.2076.154.1.6.1.17"},
{"fsMIPvrstInstPortTxSemState",        "1.3.6.1.4.1.2076.154.1.6.1.18"},
{"fsMIPvrstInstPortProtMigrationSemState",        "1.3.6.1.4.1.2076.154.1.6.1.19"},
{"fsMIPvrstInstProtocolMigrationCount",        "1.3.6.1.4.1.2076.154.1.6.1.20"},
{"fsMIPvrstInstPortRole",        "1.3.6.1.4.1.2076.154.1.6.1.21"},
{"fsMIPvrstInstCurrentPortRole",        "1.3.6.1.4.1.2076.154.1.6.1.22"},
{"fsMIPvrstInstPortInfoSemState",        "1.3.6.1.4.1.2076.154.1.6.1.23"},
{"fsMIPvrstInstPortRoleTransitionSemState",        "1.3.6.1.4.1.2076.154.1.6.1.24"},
{"fsMIPvrstInstPortStateTransitionSemState",        "1.3.6.1.4.1.2076.154.1.6.1.25"},
{"fsMIPvrstInstPortTopologyChangeSemState",        "1.3.6.1.4.1.2076.154.1.6.1.26"},
{"fsMIPvrstInstPortEffectivePortState",        "1.3.6.1.4.1.2076.154.1.6.1.27"},
{"fsMIPvrstInstPortHelloTime",        "1.3.6.1.4.1.2076.154.1.6.1.28"},
{"fsMIPvrstInstPortMaxAge",        "1.3.6.1.4.1.2076.154.1.6.1.29"},
{"fsMIPvrstInstPortForwardDelay",        "1.3.6.1.4.1.2076.154.1.6.1.30"},
{"fsMIPvrstInstPortHoldTime",        "1.3.6.1.4.1.2076.154.1.6.1.31"},
{"fsMIPvrstInstPortAdminPathCost",        "1.3.6.1.4.1.2076.154.1.6.1.32"},
{"fsMIPvrstInstPortOldPortState","1.3.6.1.4.1.2076.154.1.6.1.33"},
{"fsMIPvrstInstPortLoopInconsistentState","1.3.6.1.4.1.2076.154.1.6.1.34"},
{"fsMIPvrstInstPortRootInconsistentState","1.3.6.1.4.1.2076.154.1.6.1.35"},
{"fsMIPvrstInstPortTCDetectedCount","1.3.6.1.4.1.2076.154.1.6.1.36"},
{"fsMIPvrstInstPortTCReceivedCount","1.3.6.1.4.1.2076.154.1.6.1.37"},
{"fsMIPvrstInstPortTCDetectedTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.38"},
{"fsMIPvrstInstPortTCReceivedTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.39"},
{"fsMIPvrstInstPortProposalPktsSent",        "1.3.6.1.4.1.2076.154.1.6.1.40"},
{"fsMIPvrstInstPortProposalPktsRcvd",        "1.3.6.1.4.1.2076.154.1.6.1.41"},
{"fsMIPvrstInstPortProposalPktSentTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.42"},
{"fsMIPvrstInstPortProposalPktRcvdTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.43"},
{"fsMIPvrstInstPortAgreementPktSent",        "1.3.6.1.4.1.2076.154.1.6.1.44"},
{"fsMIPvrstInstPortAgreementPktRcvd",        "1.3.6.1.4.1.2076.154.1.6.1.45"},
{"fsMIPvrstInstPortAgreementPktSentTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.46"},
{"fsMIPvrstInstPortAgreementPktRcvdTimeStamp",        "1.3.6.1.4.1.2076.154.1.6.1.47"},
{"fsMIFsPvrstTrapsControl",        "1.3.6.1.4.1.2076.154.2"},
{"fsMIFsPvrstSetGlobalTrapOption",        "1.3.6.1.4.1.2076.154.2.1"},
{"fsMIPvrstGlobalErrTrapType",        "1.3.6.1.4.1.2076.154.2.2"},
{"fsMIFsPvrstTrapsControlTable",        "1.3.6.1.4.1.2076.154.2.3"},
{"fsMIFsPvrstTrapsControlEntry",        "1.3.6.1.4.1.2076.154.2.3.1"},
{"fsMIPvrstSetTraps",        "1.3.6.1.4.1.2076.154.2.3.1.1"},
{"fsMIPvrstGenTrapType",        "1.3.6.1.4.1.2076.154.2.3.1.2"},
{"fsMIPvrstPortTrapNotificationTable",        "1.3.6.1.4.1.2076.154.2.4"},
{"fsMIPvrstPortTrapNotificationEntry",        "1.3.6.1.4.1.2076.154.2.4.1"},
{"fsMIPvrstPortTrapIndex",        "1.3.6.1.4.1.2076.154.2.4.1.1"},
{"fsMIPvrstPortMigrationType",        "1.3.6.1.4.1.2076.154.2.4.1.2"},
{"fsMIPvrstPktErrType",        "1.3.6.1.4.1.2076.154.2.4.1.3"},
{"fsMIPvrstPktErrVal",        "1.3.6.1.4.1.2076.154.2.4.1.4"},
{"fsMIPvrstPortRoleTrapNotificationTable",        "1.3.6.1.4.1.2076.154.2.5"},
{"fsMIPvrstPortRoleTrapNotificationEntry",        "1.3.6.1.4.1.2076.154.2.5.1"},
{"fsMIPvrstPortRoleType",        "1.3.6.1.4.1.2076.154.2.5.1.1"},
{"fsMIPvrstOldRoleType",        "1.3.6.1.4.1.2076.154.2.5.1.2"},
{"fsMIFuturePvrstTraps",        "1.3.6.1.4.1.2076.154.3"},
{"fsMIPvrstTraps",        "1.3.6.1.4.1.2076.154.3.0"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
{"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
{"ieee802dot1mibs",        "1.111.2.802.1"},
{"joint-iso-ccitt",        "2"},
{0,0},
};

static struct MIB_OID orig_mib_oid_table_SI[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"transmission",        "1.3.6.1.2.1.10"},
{"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dStp",        "1.3.6.1.2.1.17.2"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"issExt",        "1.3.6.1.4.1.2076.81.8"},
{"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
{"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
{"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
{"futurePvrstMIB",        "1.3.6.1.4.1.2076.161"},
{"fsFuturePvrst",        "1.3.6.1.4.1.2076.161.1"},
{"fsPvrstSystemControl",        "1.3.6.1.4.1.2076.161.1.1"},
{"fsPvrstModuleStatus",        "1.3.6.1.4.1.2076.161.1.2"},
{"fsPvrstNoOfActiveInstances",        "1.3.6.1.4.1.2076.161.1.3"},
{"fsPvrstBrgAddress",        "1.3.6.1.4.1.2076.161.1.4"},
{"fsPvrstUpCount",        "1.3.6.1.4.1.2076.161.1.5"},
{"fsPvrstDownCount",        "1.3.6.1.4.1.2076.161.1.6"},
{"fsPvrstPathCostDefaultType",        "1.3.6.1.4.1.2076.161.1.7"},
{"fsPvrstDynamicPathCostCalculation",        "1.3.6.1.4.1.2076.161.1.8"},
{"fsPvrstTrace",        "1.3.6.1.4.1.2076.161.1.9"},
{"fsPvrstDebug",        "1.3.6.1.4.1.2076.161.1.10"},
{"fsPvrstBufferOverFlowCount",        "1.3.6.1.4.1.2076.161.1.11"},
{"fsPvrstMemAllocFailureCount",        "1.3.6.1.4.1.2076.161.1.12"},
{"fsFuturePvrstPortTable",        "1.3.6.1.4.1.2076.161.1.13"},
{"fsFuturePvrstPortEntry",        "1.3.6.1.4.1.2076.161.1.13.1"},
{"fsPvrstPort",        "1.3.6.1.4.1.2076.161.1.13.1.1"},
{"fsPvrstPortAdminEdgeStatus",        "1.3.6.1.4.1.2076.161.1.13.1.2"},
{"fsPvrstPortOperEdgePortStatus",        "1.3.6.1.4.1.2076.161.1.13.1.3"},
{"fsPvrstBridgeDetectionSemState",        "1.3.6.1.4.1.2076.161.1.13.1.4"},
{"fsPvrstPortEnabledStatus",        "1.3.6.1.4.1.2076.161.1.13.1.5"},
{"fsPvrstRootGuard",        "1.3.6.1.4.1.2076.161.1.13.1.6"},
{"fsPvrstBpduGuard",        "1.3.6.1.4.1.2076.161.1.13.1.7"},
{"fsPvrstEncapType",        "1.3.6.1.4.1.2076.161.1.13.1.8"},
{"fsPvrstPortAdminPointToPoint",        "1.3.6.1.4.1.2076.161.1.13.1.9"},
{"fsPvrstPortOperPointToPoint",        "1.3.6.1.4.1.2076.161.1.13.1.10"},
{"fsPvrstPortInvalidBpdusRcvd",        "1.3.6.1.4.1.2076.161.1.13.1.11"},
{"fsPvrstPortInvalidConfigBpduRxCount",        "1.3.6.1.4.1.2076.161.1.13.1.12"},
{"fsPvrstPortInvalidTcnBpduRxCount",        "1.3.6.1.4.1.2076.161.1.13.1.13"},
{"fsPvrstPortRowStatus",        "1.3.6.1.4.1.2076.161.1.13.1.14"},
{"fsPvrstPortBpduInconsistentState",        "1.3.6.1.4.1.2076.161.1.13.1.21"},
{"fsPvrstPortRcvInfoWhileExpCount",        "1.3.6.1.4.1.2076.161.1.13.1.25"},
{"fsPvrstPortRcvInfoWhileExpTimeStamp",        "1.3.6.1.4.1.2076.161.1.13.1.26"},
{"fsPvrstPortImpStateOccurCount",        "1.3.6.1.4.1.2076.161.1.13.1.27"},
{"fsPvrstPortImpStateOccurTimeStamp",        "1.3.6.1.4.1.2076.161.1.13.1.28"},
{"fsPvrstInstBridgeTable",        "1.3.6.1.4.1.2076.161.1.14"},
{"fsPvrstInstBridgeEntry",        "1.3.6.1.4.1.2076.161.1.14.1"},
{"fsPvrstInstVlanId",        "1.3.6.1.4.1.2076.161.1.14.1.1"},
{"fsPvrstInstBridgePriority",        "1.3.6.1.4.1.2076.161.1.14.1.2"},
{"fsPvrstInstRootCost",        "1.3.6.1.4.1.2076.161.1.14.1.3"},
{"fsPvrstInstRootPort",        "1.3.6.1.4.1.2076.161.1.14.1.4"},
{"fsPvrstInstBridgeMaxAge",        "1.3.6.1.4.1.2076.161.1.14.1.5"},
{"fsPvrstInstBridgeHelloTime",        "1.3.6.1.4.1.2076.161.1.14.1.6"},
{"fsPvrstInstBridgeForwardDelay",        "1.3.6.1.4.1.2076.161.1.14.1.7"},
{"fsPvrstInstHoldTime",        "1.3.6.1.4.1.2076.161.1.14.1.8"},
{"fsPvrstInstTxHoldCount",        "1.3.6.1.4.1.2076.161.1.14.1.9"},
{"fsPvrstInstTimeSinceTopologyChange",        "1.3.6.1.4.1.2076.161.1.14.1.10"},
{"fsPvrstInstTopChanges",        "1.3.6.1.4.1.2076.161.1.14.1.11"},
{"fsPvrstInstNewRootCount",        "1.3.6.1.4.1.2076.161.1.14.1.12"},
{"fsPvrstInstInstanceUpCount",        "1.3.6.1.4.1.2076.161.1.14.1.13"},
{"fsPvrstInstInstanceDownCount",        "1.3.6.1.4.1.2076.161.1.14.1.14"},
{"fsPvrstInstPortRoleSelSemState",        "1.3.6.1.4.1.2076.161.1.14.1.15"},
{"fsPvrstInstDesignatedRoot",        "1.3.6.1.4.1.2076.161.1.14.1.16"},
{"fsPvrstInstRootMaxAge",        "1.3.6.1.4.1.2076.161.1.14.1.17"},
{"fsPvrstInstRootHelloTime",        "1.3.6.1.4.1.2076.161.1.14.1.18"},
{"fsPvrstInstRootForwardDelay",        "1.3.6.1.4.1.2076.161.1.14.1.19"},
{"fsPvrstInstOldDesignatedRoot",        "1.3.6.1.4.1.2076.161.1.14.1.20"},
{"fsPvrstInstPortTable",        "1.3.6.1.4.1.2076.161.1.15"},
{"fsPvrstInstPortEntry",        "1.3.6.1.4.1.2076.161.1.15.1"},
{"fsPvrstInstPortIndex",        "1.3.6.1.4.1.2076.161.1.15.1.1"},
{"fsPvrstInstPortEnableStatus",        "1.3.6.1.4.1.2076.161.1.15.1.2"},
{"fsPvrstInstPortPathCost",        "1.3.6.1.4.1.2076.161.1.15.1.3"},
{"fsPvrstInstPortPriority",        "1.3.6.1.4.1.2076.161.1.15.1.4"},
{"fsPvrstInstPortDesignatedRoot",        "1.3.6.1.4.1.2076.161.1.15.1.5"},
{"fsPvrstInstPortDesignatedBridge",        "1.3.6.1.4.1.2076.161.1.15.1.6"},
{"fsPvrstInstPortDesignatedPort",        "1.3.6.1.4.1.2076.161.1.15.1.7"},
{"fsPvrstInstPortOperVersion",        "1.3.6.1.4.1.2076.161.1.15.1.8"},
{"fsPvrstInstPortProtocolMigration",        "1.3.6.1.4.1.2076.161.1.15.1.9"},
{"fsPvrstInstPortState",        "1.3.6.1.4.1.2076.161.1.15.1.10"},
{"fsPvrstInstPortForwardTransitions",        "1.3.6.1.4.1.2076.161.1.15.1.11"},
{"fsPvrstInstPortReceivedBpdus",        "1.3.6.1.4.1.2076.161.1.15.1.12"},
{"fsPvrstInstPortRxConfigBpduCount",        "1.3.6.1.4.1.2076.161.1.15.1.13"},
{"fsPvrstInstPortRxTcnBpduCount",        "1.3.6.1.4.1.2076.161.1.15.1.14"},
{"fsPvrstInstPortTransmittedBpdus",        "1.3.6.1.4.1.2076.161.1.15.1.15"},
{"fsPvrstInstPortTxConfigBpduCount",        "1.3.6.1.4.1.2076.161.1.15.1.16"},
{"fsPvrstInstPortTxTcnBpduCount",        "1.3.6.1.4.1.2076.161.1.15.1.17"},
{"fsPvrstInstPortTxSemState",        "1.3.6.1.4.1.2076.161.1.15.1.18"},
{"fsPvrstInstPortProtMigrationSemState",        "1.3.6.1.4.1.2076.161.1.15.1.19"},
{"fsPvrstInstProtocolMigrationCount",        "1.3.6.1.4.1.2076.161.1.15.1.20"},
{"fsPvrstInstPortRole",        "1.3.6.1.4.1.2076.161.1.15.1.21"},
{"fsPvrstInstCurrentPortRole",        "1.3.6.1.4.1.2076.161.1.15.1.22"},
{"fsPvrstInstPortInfoSemState",        "1.3.6.1.4.1.2076.161.1.15.1.23"},
{"fsPvrstInstPortRoleTransitionSemState",        "1.3.6.1.4.1.2076.161.1.15.1.24"},
{"fsPvrstInstPortStateTransitionSemState",        "1.3.6.1.4.1.2076.161.1.15.1.25"},
{"fsPvrstInstPortTopologyChangeSemState",        "1.3.6.1.4.1.2076.161.1.15.1.26"},
{"fsPvrstInstPortEffectivePortState",        "1.3.6.1.4.1.2076.161.1.15.1.27"},
{"fsPvrstInstPortHelloTime",        "1.3.6.1.4.1.2076.161.1.15.1.28"},
{"fsPvrstInstPortMaxAge",        "1.3.6.1.4.1.2076.161.1.15.1.29"},
{"fsPvrstInstPortForwardDelay",        "1.3.6.1.4.1.2076.161.1.15.1.30"},
{"fsPvrstInstPortHoldTime",        "1.3.6.1.4.1.2076.161.1.15.1.31"},
{"fsPvrstInstPortAdminPathCost",        "1.3.6.1.4.1.2076.161.1.15.1.32"},
{"fsPvrstInstPortOldPortState","1.3.6.1.4.1.2076.161.1.15.1.33"},
{"fsPvrstInstPortLoopInconsistentState","1.3.6.1.4.1.2076.161.1.15.1.34"},
{"fsPvrstInstPortRootInconsistentState","1.3.6.1.4.1.2076.161.1.15.1.35"},
{"fsPvrstInstPortTCDetectedCount",        "1.3.6.1.4.1.2076.161.1.15.1.36"},
{"fsPvrstInstPortTCReceivedCount",        "1.3.6.1.4.1.2076.161.1.15.1.37"},
{"fsPvrstInstPortTCDetectedTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.38"},
{"fsPvrstInstPortTCReceivedTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.39"},
{"fsPvrstInstPortProposalPktsSent",        "1.3.6.1.4.1.2076.161.1.15.1.40"},
{"fsPvrstInstPortProposalPktsRcvd",        "1.3.6.1.4.1.2076.161.1.15.1.41"},
{"fsPvrstInstPortProposalPktSentTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.42"},
{"fsPvrstInstPortProposalPktRcvdTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.43"},
{"fsPvrstInstPortAgreementPktSent",        "1.3.6.1.4.1.2076.161.1.15.1.44"},
{"fsPvrstInstPortAgreementPktRcvd",        "1.3.6.1.4.1.2076.161.1.15.1.45"},
{"fsPvrstInstPortAgreementPktSentTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.46"},
{"fsPvrstInstPortAgreementPktRcvdTimeStamp",        "1.3.6.1.4.1.2076.161.1.15.1.47"},
{"fsPvrstGlobalBpduGuard",        "1.3.6.1.4.1.2076.161.1.17"},
{"fsPvrstTrapsControl",        "1.3.6.1.4.1.2076.161.2"},
{"fsPvrstSetTraps",        "1.3.6.1.4.1.2076.161.2.1"},
{"fsPvrstGenTrapType",        "1.3.6.1.4.1.2076.161.2.2"},
{"fsPvrstErrTrapType",        "1.3.6.1.4.1.2076.161.2.3"},
{"fsPvrstPortTrapNotificationTable",        "1.3.6.1.4.1.2076.161.2.4"},
{"fsPvrstPortTrapNotificationEntry",        "1.3.6.1.4.1.2076.161.2.4.1"},
{"fsPvrstPortTrapIndex",        "1.3.6.1.4.1.2076.161.2.4.1.1"},
{"fsPvrstPortMigrationType",        "1.3.6.1.4.1.2076.161.2.4.1.2"},
{"fsPvrstPktErrType",        "1.3.6.1.4.1.2076.161.2.4.1.3"},
{"fsPvrstPktErrVal",        "1.3.6.1.4.1.2076.161.2.4.1.4"},
{"fsPvrstPortRoleTrapNotificationTable",        "1.3.6.1.4.1.2076.161.2.5"},
{"fsPvrstPortRoleTrapNotificationEntry",        "1.3.6.1.4.1.2076.161.2.5.1"},
{"fsPvrstPortRoleType",        "1.3.6.1.4.1.2076.161.2.5.1.1"},
{"fsPvrstOldRoleType",        "1.3.6.1.4.1.2076.161.2.5.1.2"},
{"fsFuturePvrstTraps",        "1.3.6.1.4.1.2076.161.3"},
{"fsPvrstTraps",        "1.3.6.1.4.1.2076.161.3.0"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
{"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
{"ieee802dot1mibs",        "1.111.2.802.1"},
{"joint-iso-ccitt",        "2"},
{0,0},
};

static struct MIB_OID * porig_mib_oid_table = NULL;
#endif /* _SNMP_MIB_H */
