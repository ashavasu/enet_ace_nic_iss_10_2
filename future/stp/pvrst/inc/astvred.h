/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvred.h,v 1.1 2007/11/19 07:21:52 iss Exp $
 *
 * Description: This file contains Prototype definitions of functions
 *              used in redundancy support for PVRST Module.       
 *
 *******************************************************************/

#ifndef _ASTVRED_H
#define _ASTVRED_H

INT4 PvrstRedStorePduInActive (UINT2 , tPvrstBpdu *, UINT2, UINT2);

INT4 PvrstRedStorePduInStandby (UINT2, VOID *, UINT2);

INT4 PvrstRedSyncUpPdu (UINT2 , tPvrstBpdu* , UINT2);

INT4 PvrstRedHandlePvrstPdus (VOID *, UINT4 *, UINT2);

INT4 PvrstRedClearAllBpdusOnActive(VOID);

INT4 PvrstRedProtocolRestart (UINT2, UINT2);

INT4 PvrstRedClearAllSyncUpDataInInst(UINT2);

INT4 PvrstRedClearPduOnActive(UINT2);

VOID RedDumpPvrstPdu (VOID);

VOID RedDumpPvrstPduOnPort (UINT2 u2PortIfIndex);

/*INT4 PvrstRedClearInstanceOnActive (UINT2, UINT2);*/

INT4
PvrstRedGetNextTimer (UINT2, UINT2, UINT1, UINT1 *, UINT2 *, tAstTimer **);

#endif /* _ASTVRED_H */
