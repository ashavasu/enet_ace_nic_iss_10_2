/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvcons.h,v 1.14 2017/12/22 10:06:01 siva Exp $
 *
 * Description: This file contains constant definitions used in 
 *              PVRST Module. 
 *
 *****************************************************************************/

#ifndef _ASTVCONS_H
#define _ASTVCONS_H

/******************************************************************************/
/*                             Pvrst Protocol Constants                       */
/******************************************************************************/

#define PVRST_FORCE_TRUE    0
#define PVRST_FORCE_FALSE    1
#define PVRST_AUTO    2


#define PVRST_SNMP_START   1
#define PVRST_SNMP_SHUTDOWN   2

#define PVRST_PORT_OPER_ENABLED   1
#define PVRST_PORT_OPER_DISABLED  2

#define PVRST_PATHCOST_TYPE_16   1
#define PVRST_PATHCOST_TYPE_32   2

#define PVRST_TRANS_INTRANSITION 1
#define PVRST_TRANS_FINISHED 2


#define PVRST_DEFAULT_FLUSH_INTERVAL 0
#define PVRST_DEFAULT_FLUSH_IND_THRESHOLD 0


/* HELLO, FWD and AGE are in Centi-Seconds */
#define PVRST_DEFAULT_BRG_HELLO_TIME            (2 * AST_CENTI_SECONDS)
#define PVRST_DEFAULT_BRG_FWD_DELAY  (15 * AST_CENTI_SECONDS)
#define PVRST_DEFAULT_BRG_MAX_AGE  (20 * AST_CENTI_SECONDS)
#define PVRST_DEFAULT_BRG_TX_LIMIT  6 
#define PVRST_DEFAULT_BRG_PRIORITY  0x8000
#define PVRST_DEFAULT_PORT_PRIORITY  128
#define PVRST_DEFAULT_MIGRATE_TIME  3


#define PVRST_ROOT_INC_RECOVERY_TIME  (25 * AST_CENTI_SECONDS)

#define PVRST_P2P_FORCETRUE   0
#define PVRST_P2P_FORCEFALSE   1
#define PVRST_P2P_AUTO    2

#define PVRST_FLAG_MASK_TC   0x01         
#define PVRST_FLAG_MASK_PROPOSAL  0x02
#define PVRST_FLAG_MASK_PORT_ROLE  0x0C
#define PVRST_FLAG_MASK_LEARNING  0x10
#define PVRST_FLAG_MASK_FORWARDING  0x20
#define PVRST_FLAG_MASK_AGREEMENT  0x40 
#define PVRST_FLAG_MASK_TCACK   0x80
#define PVRST_PRIORITY_OFFSET   4
#define PVRST_SET_CONFIG_TOPOCH_ACK_FLAG 0x80
#define PVRST_SET_RST_TOPOCH_ACK_FLAG           0x00
#define PVRST_SET_AGREEMENT_FLAG         0x40
#define PVRST_SET_FORWARDING_FLAG         0x20
#define PVRST_SET_LEARNING_FLAG          0x10
#define PVRST_SET_PROPOSAL_FLAG          0x02
#define PVRST_SET_TOPOCH_FLAG          0x01
#define PVRST_SET_UNKNOWN_PROLE_FLAG            0x00
#define PVRST_SET_ALTBACK_PROLE_FLAG            0x04
#define PVRST_SET_ROOT_PROLE_FLAG         0x08
#define PVRST_SET_DESG_PROLE_FLAG         0x0C

#define PVRST_SUPERIOR_DESG_MSG  1
#define PVRST_REPEATED_DESG_MSG  2
#define PVRST_CONFIRMED_ROOT_MSG 3
#define PVRST_OTHER_MSG                 4
#define PVRST_INFERIOR_DESG_MSG  5

#define PVRST_INFERIOR_ROOT_ALT_MSG 6
#define PVRST_INFOIS_DISABLED  1
#define PVRST_INFOIS_AGED  2
#define PVRST_INFOIS_MINE  3
#define PVRST_INFOIS_RECEIVED  4

#define PVRST_BRGID1_INFERIOR  -1
#define PVRST_BRGID1_SUPERIOR  1
#define PVRST_BRGID1_SAME  0

#define PVRST_INFERIOR_MSG  -1
#define PVRST_BETTER_MSG  1
#define PVRST_SAME_MSG   0

#define PVRST_MIN_INSTANCES  1
#define SHIFT_BITS_BRG_PRIORITY  12

#define PVRST_MAX_BPDU_SIZE         40
#define PVRST_ENCAP_OVERHEADS     30
#define NON_ENCAP_PKT_LEN           60
#define DOT1Q_DATA_HDR_SIZE         26
#define DOT1Q_DATA_LEN              42
#define DOT1Q_CISCO_DATA_OFFSET     62
#define DOT1Q_TAG_LEN               4
#define ORG_CODE_SIZE               3
#define PROTOCOL_ID_LEN             2
#define TRAILER_LEN                 7
#define CRC_SIZE                    4
#define PVRST_ISL_ADDRESS_LENGTH    5
#define PVRST_INCR_LENGTH_SEVENTEEN  17
#define PVRST_INCR_LENGTH_SIX        6
#define PVRST_LENGTH_LLC_HEADER      3
#define PVRST_INCR_LENGTH_BY_TWO     2

#define PVRST_EDGE_TRUE    0
#define PVRST_EDGE_FALSE   1
#define PVRST_MIN_BRIDGE_PRIORITY   0x1000
#define PVRST_MAX_BRIDGE_PRIORITY   0xf000
#define PVRST_MIN_PORT_PATH_COST      1
#define PVRST_MAX_PORT_PATH_COST      200000000
#define PVRST_MIN_PORT_PRIORITY       0
#define PVRST_MAX_PORT_PRIORITY       0xff
#define PVRST_MIN_HELLOTIME_VAL        100
#define PVRST_MAX_HELLOTIME_VAL        1000
#define INVALID_INDEX    0
#define PVRST_DEFAULT_AUTOEDGE_VALUE     RST_TRUE
/******************************************************************************/
/*                         Port Information SEM Constants                     */
/******************************************************************************/


#ifdef IEEE8021Y_Z12_WANTED
#define PVRST_PINFOSM_MAX_STATES  9
#else /* IEEE8021Y_Z12_WANTED */
#define PVRST_PINFOSM_MAX_STATES  8
#endif /* IEEE8021Y_Z12_WANTED */
#define PVRST_PINFOSM_STATE_DISABLED  0
#define PVRST_PINFOSM_STATE_AGED  1
#define PVRST_PINFOSM_STATE_UPDATE  2
#define PVRST_PINFOSM_STATE_SUPERIOR  3
#define PVRST_PINFOSM_STATE_REPEAT  4
#ifdef IEEE8021Y_Z11_WANTED
#define PVRST_PINFOSM_STATE_NOT_DESG  5
#else
#define PVRST_PINFOSM_STATE_AGREEMENT  5
#endif/*End of IEEE8021Y_Z11_WANTED */
#define PVRST_PINFOSM_STATE_CURRENT  6
#define PVRST_PINFOSM_STATE_RECEIVE  7
#ifdef IEEE8021Y_Z12_WANTED                   
#define PVRST_PINFOSM_STATE_INFERIOR_DESG 8
#endif /* IEEE8021Y_Z12_WANTED */

#define PVRST_PINFOSM_MAX_EVENTS  6
#define PVRST_PINFOSM_EV_BEGIN   0
#define PVRST_PINFOSM_EV_PORT_ENABLED  1
#define PVRST_PINFOSM_EV_PORT_DISABLED  2
#define PVRST_PINFOSM_EV_RCVD_BPDU  3
#define PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP 4
#define PVRST_PINFOSM_EV_UPDATEINFO  5 


/******************************************************************************/
/*                         Port Role Selection SEM Constants                  */
/******************************************************************************/


#define PVRST_PROLESELSM_MAX_STATES  2
#define PVRST_PROLESELSM_STATE_INIT_BRIDGE 0
#define PVRST_PROLESELSM_STATE_ROLE_SELECTION 1

#define PVRST_PROLESELSM_MAX_EVENTS  2
#define PVRST_PROLESELSM_EV_BEGIN  0
#define PVRST_PROLESELSM_EV_RESELECT  1


/******************************************************************************/
/*                         Port Role Transition SEM Constants                 */
/******************************************************************************/


#define PVRST_PROLETRSM_MAX_STATES  5
#define PVRST_PROLETRSM_STATE_INIT_PORT  0
#define PVRST_PROLETRSM_STATE_BLOCK_PORT 1
#define PVRST_PROLETRSM_STATE_BLOCKED_PORT 2
#define PVRST_PROLETRSM_STATE_ROOT_PORT  3
#define PVRST_PROLETRSM_STATE_DESG_PORT  4
/* Added state Constants for clarity - These states are not included in 
* calculation of MAX_STATES. The order and value can be changed */
#define PVRST_PROLETRSM_STATE_BACKUP_PORT 5
#define PVRST_PROLETRSM_STATE_REROOTED  6
#define PVRST_PROLETRSM_STATE_ROOT_PROPOSED 7
#define PVRST_PROLETRSM_STATE_ROOT_AGREED 8
#define PVRST_PROLETRSM_STATE_REROOT  9
#define PVRST_PROLETRSM_STATE_DESG_PROPOSE 10
#define PVRST_PROLETRSM_STATE_DESG_SYNCED 11
#define PVRST_PROLETRSM_STATE_DESG_RETIRED 12
#define PVRST_PROLETRSM_STATE_DESG_LISTEN 13
   /* Common for Designated and root ports */
#define PVRST_PROLETRSM_STATE_LEARN  14
#define PVRST_PROLETRSM_STATE_FORWARD  15
#define PVRST_PROLETRSM_STATE_ROOT_LEARN PVRST_PROLETRSM_STATE_LEARN
#define PVRST_PROLETRSM_STATE_ROOT_FORWARD PVRST_PROLETRSM_STATE_FORWARD
#define PVRST_PROLETRSM_STATE_DESG_LEARN PVRST_PROLETRSM_STATE_LEARN
#define PVRST_PROLETRSM_STATE_DESG_FORWARD PVRST_PROLETRSM_STATE_FORWARD

#ifdef IEEE8021Y_Z12_WANTED
#define PVRST_PROLETRSM_MAX_EVENTS  14
#else /* IEEE8021Y_Z12_WANTED */
#define PVRST_PROLETRSM_MAX_EVENTS  13
#endif /* IEEE8021Y_Z12_WANTED */
#define PVRST_PROLETRSM_EV_BEGIN  0
#define PVRST_PROLETRSM_EV_FDWHILE_EXP  1 
#define PVRST_PROLETRSM_EV_RRWHILE_EXP  2
#define PVRST_PROLETRSM_EV_RBWHILE_EXP  3
#define PVRST_PROLETRSM_EV_SYNC_SET  4
#define PVRST_PROLETRSM_EV_REROOT_SET  5
#define PVRST_PROLETRSM_EV_SELECTED_SET  6
#define PVRST_PROLETRSM_EV_PROPOSED_SET  7
#define PVRST_PROLETRSM_EV_AGREED_SET  8
#define PVRST_PROLETRSM_EV_ALLSYNCED_SET 9
#define PVRST_PROLETRSM_EV_REROOTED_SET  10
#define PVRST_PROLETRSM_EV_UPDTINFO_RESET PVRST_PROLETRSM_EV_SELECTED_SET
#define PVRST_PROLETRSM_EV_OPEREDGE_SET  11
#define PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET  12
#ifdef IEEE8021Y_Z12_WANTED
#define PVRST_PROLETRSM_EV_DISPUTED_SET  13
#endif /* IEEE8021Y_Z12_WANTED */


/******************************************************************************/
/*                     Port State Transition SEM Constants                  */
/******************************************************************************/

#define PVRST_PSTATETRSM_MAX_STATES  3
#define PVRST_PSTATETRSM_STATE_DISCARDING 0
#define PVRST_PSTATETRSM_STATE_LEARNING  1
#define PVRST_PSTATETRSM_STATE_FORWARDING 2

#define PVRST_PSTATETRSM_MAX_EVENTS  4
#define PVRST_PSTATETRSM_EV_BEGIN  0
#define PVRST_PSTATETRSM_EV_LEARN  1
#define PVRST_PSTATETRSM_EV_FORWARD  2
#define PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED 3


/******************************************************************************/
/*                     Port Protocol Migration SEM Constants                  */
/******************************************************************************/

#define PVRST_PMIGSM_MAX_STATES   5

#define PVRST_PMIGSM_STATE_INIT   0
#define PVRST_PMIGSM_STATE_SEND_RSTP  1
#define PVRST_PMIGSM_STATE_SENDING_RSTP  2
#define PVRST_PMIGSM_STATE_SEND_STP  3
#define PVRST_PMIGSM_STATE_SENDING_STP  4


#define PVRST_PMIGSM_MAX_EVENTS   7

#define PVRST_PMIGSM_EV_BEGIN   0
#define PVRST_PMIGSM_EV_PORT_ENABLED  1
#define PVRST_PMIGSM_EV_PORT_DISABLED  2
#define PVRST_PMIGSM_EV_MCHECK   3
#define PVRST_PMIGSM_EV_FORCE_VER_LT_2  4
#define PVRST_PMIGSM_EV_RCVD_STP  5
#define PVRST_PMIGSM_EV_RCVD_RSTP  6


/******************************************************************************/
/*                         Port Transmit SEM Constants                        */
/******************************************************************************/


#define PVRST_PTXSM_MAX_STATES   6
#define PVRST_PTXSM_STATE_TRANSMIT_INIT  0
#define PVRST_PTXSM_STATE_TRANSMIT_PERIODIC 1
#define PVRST_PTXSM_STATE_TRANSMIT_CONFIG 2
#define PVRST_PTXSM_STATE_TRANSMIT_TCN  3
#define PVRST_PTXSM_STATE_TRANSMIT_RSTP  4
#define PVRST_PTXSM_STATE_IDLE   5

#define PVRST_PTXSM_MAX_EVENTS   6
#define PVRST_PTXSM_EV_BEGIN   0
#define PVRST_PTXSM_EV_NEWINFO_SET  1
#define PVRST_PTXSM_EV_HELLOWHEN_EXP  2
#define PVRST_PTXSM_EV_HOLDTMR_EXP  3
#define PVRST_PTXSM_EV_TX_ENABLED  4
#define PVRST_PTXSM_EV_TX_DISABLED  5

/******************************************************************************/
/*                         Topology Change SEM Constants                      */
/******************************************************************************/


#define PVRST_TOPOCHSM_MAX_STATES  8
#define PVRST_TOPOCHSM_STATE_INIT  0
#define PVRST_TOPOCHSM_STATE_INACTIVE  1
#define PVRST_TOPOCHSM_STATE_ACTIVE  2
#define PVRST_TOPOCHSM_STATE_DETECTED  3
#define PVRST_TOPOCHSM_STATE_NOTIFIED_TCN 4
#define PVRST_TOPOCHSM_STATE_NOTIFIED_TC 5
#define PVRST_TOPOCHSM_STATE_PROPAGATING 6
#define PVRST_TOPOCHSM_STATE_ACKNOWLEDGED 7

#define PVRST_TOPOCHSM_MAX_EVENTS  9
#define PVRST_TOPOCHSM_EV_BEGIN   0
#define PVRST_TOPOCHSM_EV_ROOTPORT  1
#define PVRST_TOPOCHSM_EV_BLOCKEDPORT  2
#define PVRST_TOPOCHSM_EV_DESGPORT  3
#define PVRST_TOPOCHSM_EV_RCVDTC  4
#define PVRST_TOPOCHSM_EV_RCVDTCN  5
#define PVRST_TOPOCHSM_EV_RCVDTCACK  6
#define PVRST_TOPOCHSM_EV_TC   7
#define PVRST_TOPOCHSM_EV_TCPROP  8

/******************************************************************************/
/*                   Bridge Detection SEM Constants                      */
/******************************************************************************/

#define PVRST_BRGDETSM_MAX_EVENTS  8
#define PVRST_BRGDETSM_EV_BEGIN   0
#define PVRST_BRGDETSM_EV_ADMIN_SET  1
#define PVRST_BRGDETSM_EV_BPDU_RCVD  2
#define PVRST_BRGDETSM_EV_EDGEDELAYWHILE_EXP 3
#define PVRST_BRGDETSM_EV_SENDRSTP_SET  4
#define PVRST_BRGDETSM_EV_PROPOSING_SET  5
#define PVRST_BRGDETSM_EV_PORTDISABLED  6
#define PVRST_BRGDETSM_EV_AUTOEDGE_SET 7

#define PVRST_BRGDETSM_MAX_STATES  2
#define PVRST_BRGDETSM_STATE_EDGE  0
#define PVRST_BRGDETSM_STATE_NOT_EDGE  1

/******************************************************************************/
/*         Respective Function numbers to the State machine functions         */
/******************************************************************************/

#define PVRST_UPDATE_INFO_RESET                      1
#define PVRST_REROOT_SET_ROOTPORT                    2
#define PVRST_RE_ROOT_SET_DESG_PORT                  3
#define PVRST_SYNC_SET_DESG_PORT                     4
#define PVRST_FWD_EXP_ROOT_PORT                      5
#define PVRST_FWD_EXP_DESG_PORT                      6
#define PVRST_WHILE_EXP_DESG_PORT                    7
#define PVRST_PROPOSED_SET_ROOT_PORT                 8
#define PVRST_AGREED_SET_DESG_PORT                   9
#define PVRST_WHILE_EXP_ROOT_PORT                    10
#define PVRST_DISPUTED_SET_DESG_PORT                 11
#define PVRST_IS_RE_ROOTED                           12
#define PVRST_ROLE_CHANGED                           13
#define PVRST_MAKE_INIT_PORT                         14
#define PVRST_MAKE_BLOCK_PORT                        15
#define PVRST_MAKE_BLOCKED_PORT                      16
#define PVRST_MAKE_ROOT_PORT                         17
#define PVRST_IS_ALL_OTHER_PORTS_SYNCED              18
#define PVRST_ALL_SYNCED_SET_ROOT_PORT               19
#define PVRST_REROOTED_SET_ROOT_PORT                 20
#define PVRST_MAKE_DESG_PORT                         21
#define PVRST_MAKE_FORWARD                           22
#define PVRST_MAKE_LEARN                             23
#define PVRST_MAKE_DESIGNATED_LISTEN                 24
#define PVRST_SET_SYNC_BRIDGE                        25
#define PVRST_SET_REROOT_BRIDGE                      26
#define PVRST_OPER_EDGE_SET_DESG_PORT                27
#define PVRST_LEARNING_FORWARDING_RESET              28


#endif /* _ASTVCONS_H */

