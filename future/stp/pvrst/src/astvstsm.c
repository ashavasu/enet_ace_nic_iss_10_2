/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvstsm.c,v 1.18 2017/11/30 06:29:20 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port State Transition State Machine.  
 *
 *****************************************************************************/

#include "asthdrs.h"
#ifdef NPAPI_WANTED
#include "rstnp.h"
#include "pvrstnp.h"
#endif

/*****************************************************************************/
/* Function Name      : PvrstPortStateTrMachine                              */
/*                                                                           */
/* Description        : This is the main routine for the Port State          */
/*                      Transition State Machine.                            */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortStateTrMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2State;
    INT4                i4RetVal = PVRST_SUCCESS;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Event %s: Invalid parameter"
                      " passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_STSM][u2Event]);
        return PVRST_FAILURE;
    }
    VlanId = pPerStPortInfo->u2Inst;
    u2State = (UINT2) pPerStPortInfo->u1PstateTrSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "STSM: Port %u: Inst %d: State Tr Machine Called"
                  " with Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo, VlanId,
                  gaaau1AstSemEvent[AST_STSM][u2Event],
                  gaaau1AstSemState[AST_STSM][u2State]);

    AST_DBG_ARG4 (AST_STSM_DBG,
                  "STSM: Port %u: Inst %d: State Tr Machine Called "
                  "with Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo, VlanId,
                  gaaau1AstSemEvent[AST_STSM][u2Event],
                  gaaau1AstSemState[AST_STSM][u2State]);

    if (PVRST_PORT_STATE_TR_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                 "STSM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    i4RetVal = (*(PVRST_PORT_STATE_TR_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                 "STSM: Event routine returned failure !!!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmMakeDiscarding                        */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'DISCARDING'.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmMakeDiscarding (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT4               u4RestartTime = AST_INIT_VAL;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT2               u2PortNum;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1Status;
    UINT1               u1PrevStatus = 0;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    tAstPortEntry      *pPortEntry = NULL;
    MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO ((UINT4) u2PortNum);

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /*  In asynchronous NPAPI mode 
     *    - Delay updating L2IWF and the SEM variables Learning and Forwarding
     *      until callback is received from NPAPI.
     *  Exceptions -
     *   If this function is called as part of
     *      1) spanning tree per-port disable or
     *      2) spanning tree module disable
     *   then the Learning and Forwarding variables can be reset immediately
     *   here, since the actual port state programmed in HW will be Forwarding.
     *   Handling callback in these 2 cases causes the SEM variable Forwarding
     *   to be wrongly set to TRUE. Hence callback handling is disabled for
     *   these cases.    
     *   So L2Iwf should also be updated synchronously for these 2 cases.
     */

    u1PrevStatus = AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum);

    pPerStPortInfo->u1PstateTrSmState = (UINT1)
        PVRST_PSTATETRSM_STATE_DISCARDING;
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %u: Inst %d: Moved to "
                  "state DISCARDING \n", pPerStPortInfo->u2PortNo, VlanId);

    /* 
     * If this function called when
     *       1) PVRST Module is disabled or
     *       2) Port is disabled in PVRST alone 
     *          by configuration
     *    then set port state as FORWARDING in the 
     *    common database and indicate to Hw and Garp.
     * Else
     *    make the port state as DISCARDING in the 
     *    common database and indicate to Hw and Garp.
     */

    if ((AST_GET_SYSTEMACTION == PVRST_DISABLED) ||
        (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE) ||
        (pPerStRstPortInfo->bPortEnabled == PVRST_FALSE))
    {
        u1Status = AST_PORT_STATE_FORWARDING;

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "STSM: Port %u: Inst %d: Port "
                      "State is FORWARDING\n",
                      pPerStPortInfo->u2PortNo, VlanId);
    }
    else
    {
        u1Status = AST_PORT_STATE_DISCARDING;

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "STSM: Port %u: Inst %d: Port State "
                      "is DISCARDING\n", pPerStPortInfo->u2PortNo, VlanId);
    }

    if ((u1PrevStatus != PVRST_FALSE) && (u1Status == u1PrevStatus))
    {
        if ((AST_GET_SYSTEMACTION == PVRST_DISABLED) ||
            (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE) ||
            (pPerStRstPortInfo->bPortEnabled == PVRST_FALSE))
        {
            /* Hardware has already been programmed as Forwarding.
             * Just update the SEM variables to be in sync with the state */
            pPerStRstPortInfo->bLearning = PVRST_FALSE;
            pPerStRstPortInfo->bForwarding = PVRST_FALSE;

#ifdef NPAPI_WANTED
            /* Indicate Forwarding state to L2Iwf explicitly since u1PrevStatus
             * only indicates the last programmed state in hardware but may
             * not reflect the actual L2Iwf state in case of
             * Asynchronous NPAPI mode. */
            if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
            {
                AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum, u1Status);
            }
#endif
        }
        return PVRST_SUCCESS;
    }

    PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_TRUE;

    AST_DBG_ARG1 (AST_SM_VAR_DBG,
                  "STSM_Discarding: Port %s: Learning = Forwarding = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if ((AST_IS_PVRST_ENABLED ()) &&
        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))

    {
        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_TRUE;

        if (PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                     AST_GET_IFINDEX (u2PortNum),
                                     VlanId, u1Status) == PVRST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstIndex, u1Status);
            return PVRST_SUCCESS;
        }
    }

    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) = u1Status;

    if ((AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS) &&
        (AST_GET_SYSTEMACTION != PVRST_DISABLED) &&
        (pPerStRstPortInfo->bPortEnabled == RST_TRUE))
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return PVRST_SUCCESS;
    }
#else
    UNUSED_PARAM (pPortEntry);
    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) = u1Status;
#endif /* NPAPI_WANTED */

    if (u1Status == AST_PORT_STATE_FORWARDING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_SUCCESS;
    }
    else
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;
    }
    pPerStPortInfo->i4TransmitSelfInfo = PVRST_FALSE;

    (VOID) PvrstPstateTrSmDisableLearning (u2PortNum);
    pPerStRstPortInfo->bLearning = PVRST_FALSE;

    (VOID) PvrstPstateTrSmDisableForwarding (u2PortNum);
    pPerStRstPortInfo->bForwarding = PVRST_FALSE;

    /* Indicate to L2Iwf, Hardware and GARP only if there is change in 
     * port state */

    AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum, u1Status);
    if (u1PrevStatus != AST_PORT_STATE_DISCARDING)
    {
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     VlanId,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            UtlGetTimeStr (au1TimeStr);
            AstGetStateStr (u1PrevStatus, au1OldState);
            AstPortStateChangeTrap ((UINT2)
                                    AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                                    AST_PORT_STATE_DISCARDING, u1PrevStatus,
                                    u2InstIndex, (INT1 *) AST_PVRST_TRAPS_OID,
                                    PVRST_BRG_TRAPS_OID_LEN);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Port:%s Vlan:%u Old State:%s New State:Discarding Time Stamp: %s\r\n ",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              VlanId, au1OldState, au1TimeStr);
        }
    }

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
        (pPerStPortInfo->bLoopIncStatus == RST_TRUE) &&
        (pPortEntry->bLoopGuard == AST_TRUE))
    {
        pPortEntry->u1RecScenario = LOOP_INC_RECOVERY;
        AST_GET_RANDOM_TIME (RST_DEFAULT_RESTART_INTERVAL, u4RestartTime);
        u4RestartTime = u4RestartTime * AST_CENTI_SECONDS;
        AST_DBG_ARG2 (AST_ALL_FLAG,
                      "TMR: Port %s: Starting RESTART Timer with duration %u\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u4RestartTime);
        if (AstStartTimer (pPortEntry, u2InstIndex,
                           AST_TMR_TYPE_RESTART,
                           (UINT2) u4RestartTime) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;

        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmMakeLearning                          */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'LEARNING'.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmMakeLearning (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PrevStatus = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    u1PrevStatus = AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum);

    pPerStPortInfo->u1PstateTrSmState = (UINT1) PVRST_PSTATETRSM_STATE_LEARNING;
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %u: Inst %d: Moved to state LEARNING \n",
                  pPerStPortInfo->u2PortNo, VlanId);

    if (u1PrevStatus == AST_PORT_STATE_LEARNING)
    {
        return PVRST_SUCCESS;
    }
    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                 VlanId,
                                 AST_GET_IFINDEX (pPerStPortInfo->u2PortNo)) ==
        OSIX_TRUE)
    {
        UtlGetTimeStr (au1TimeStr);
        AstGetStateStr (u1PrevStatus, au1OldState);
        AstPortStateChangeTrap ((UINT2)
                                AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                                AST_PORT_STATE_LEARNING, u1PrevStatus,
                                u2InstIndex, (INT1 *) AST_PVRST_TRAPS_OID,
                                PVRST_BRG_TRAPS_OID_LEN);
        AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                          "Port:%s Vlan:%u Old State:%s New State:Learning Time Stamp: %s\r\n ",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          VlanId, au1OldState, au1TimeStr);
    }
#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if (AST_IS_PVRST_ENABLED ()
        && (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_TRUE;

        if (PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                     AST_GET_IFINDEX (u2PortNum),
                                     VlanId, AST_PORT_STATE_LEARNING)
            == PVRST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstIndex,
                                       AST_PORT_STATE_LEARNING);
            return PVRST_SUCCESS;
        }
    }

    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
        AST_PORT_STATE_LEARNING;

    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return PVRST_SUCCESS;
    }
#else
    UNUSED_PARAM (pPortEntry);
    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
        AST_PORT_STATE_LEARNING;
#endif /* NPAPI_WANTED */

    pPerStPortInfo->i4NpPortStateStatus = AST_LEARN_SUCCESS;
    pPerStPortInfo->i4TransmitSelfInfo = PVRST_FALSE;

    (VOID) PvrstPstateTrSmEnableLearning (u2PortNum);
    pPerStRstPortInfo->bLearning = PVRST_TRUE;

    /* Indicate to L2Iwf, Hardware only if there is change in port state */

    AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum, (UINT1)
                                AST_PORT_STATE_LEARNING);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmMakeForwarding                        */
/*                                                                           */
/* Description        : This routine changes the state of the port to        */
/*                      'FORWARDING'.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmMakeForwarding (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PrevStatus = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
#ifdef NPAPI_WANTED
    if (AST_GET_IFINDEX (u2PortNum) > BRG_MAX_PHY_PORTS &&
        AST_GET_IFINDEX (u2PortNum) < BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        gAstStateTransStatus = PVRST_TRANS_INTRANSITION;
    }
#endif
    if (AST_IS_PVRST_ENABLED ())
    {
        if (pPortInfo->bOperEdgePort == PVRST_FALSE)
        {
            pPerStRstPortInfo->bTc = PVRST_TRUE;
            if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_TC,
                                    pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Inst %d: Topology Chang"
                              "e Machine returned FAILURE !!! \n",
                              pPerStPortInfo->u2PortNo, VlanId);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port%u: Inst %d: Topology Change"
                              " Machine returned FAILURE !!! \n",
                              pPerStPortInfo->u2PortNo, VlanId);
                return PVRST_FAILURE;
            }
        }
    }

    u1PrevStatus = AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum);

    pPerStPortInfo->u1PstateTrSmState = (UINT1)
        PVRST_PSTATETRSM_STATE_FORWARDING;
    AST_DBG_ARG2 (AST_STSM_DBG,
                  "STSM: Port %u: Inst %d: Moved to state FORWARDING \n",
                  pPerStPortInfo->u2PortNo, VlanId);

    if (u1PrevStatus == AST_PORT_STATE_FORWARDING)
    {
        pPerStPortInfo->i4NpPortStateStatus = AST_WAITING_FOR_CALLBACK;
        return PVRST_SUCCESS;
    }

    AST_GET_NUM_FWD_TRANSITIONS (pPerStPortInfo)++;

#ifdef NPAPI_WANTED
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;

    if (AST_IS_PVRST_ENABLED ()
        && (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_TRUE;

        if (PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                     AST_GET_IFINDEX (u2PortNum), VlanId,
                                     AST_PORT_STATE_FORWARDING) ==
            PVRST_FAILURE)
        {
            AstHwPortStateUpdtFailure (pPortEntry, u2InstIndex,
                                       AST_PORT_STATE_FORWARDING);
            return PVRST_SUCCESS;
        }
    }

    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
        AST_PORT_STATE_FORWARDING;
    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        return PVRST_SUCCESS;
    }
#else
    UNUSED_PARAM (pPortEntry);
    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
        AST_PORT_STATE_FORWARDING;
#endif /* NPAPI_WANTED */

    pPerStPortInfo->i4NpPortStateStatus = AST_FORWARD_SUCCESS;
    pPerStPortInfo->i4TransmitSelfInfo = PVRST_FALSE;

    (VOID) PvrstPstateTrSmEnableForwarding (u2PortNum);
    pPerStRstPortInfo->bForwarding = PVRST_TRUE;

    /* Indicate to L2Iwf and Hardware only if there is change in port 
     * state */

    AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                (UINT1) AST_PORT_STATE_FORWARDING);
    AstGetStateStr (u1PrevStatus, au1OldState);
    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                 VlanId,
                                 AST_GET_IFINDEX (pPerStPortInfo->u2PortNo)) ==
        OSIX_TRUE)
    {
        UtlGetTimeStr (au1TimeStr);
        AstPortStateChangeTrap ((UINT2)
                                AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                                AST_PORT_STATE_FORWARDING, u1PrevStatus,
                                u2InstIndex, (INT1 *) AST_PVRST_TRAPS_OID,
                                PVRST_BRG_TRAPS_OID_LEN);
        AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                          "Port:%s Vlan:%u Old State:%s New State:Forwarding Time Stamp: %s\r\n ",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          VlanId, au1OldState, au1TimeStr);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmEnableLearning                        */
/*                                                                           */
/* Description        : This routine calls the bridge module and enables     */
/*                      the Learning functionality in it.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Learning is to be enabled.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmEnableLearning (UINT2 u2PortNum)
{
    AST_UNUSED (u2PortNum);
    AST_DBG_ARG1 (AST_STSM_DBG,
                  "STSM: Port %u: Forwarding Disabled\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmDisableLearning                       */
/*                                                                           */
/* Description        : This routine calls the bridge module and disables    */
/*                      the Learning functionality in it.                    */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Learning is to be disabled.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmDisableLearning (UINT2 u2PortNum)
{
    AST_UNUSED (u2PortNum);
    AST_DBG_ARG1 (AST_STSM_DBG,
                  "STSM: Port %u: Forwarding Disabled\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmEnableForwarding                      */
/*                                                                           */
/* Description        : This routine calls the bridge module and enables     */
/*                      the forwarding functionality in it.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Forwarding is to be enabled.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmEnableForwarding (UINT2 u2PortNum)
{
    AST_UNUSED (u2PortNum);
    AST_DBG_ARG1 (AST_STSM_DBG,
                  "STSM: Port %u: Forwarding Disabled\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmDisableForwarding                     */
/*                                                                           */
/* Description        : This routine calls the bridge module and disables    */
/*                      the forwarding functionality in it.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The number of the port for which         */
/*                                  Forwarding is to be disabled.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmDisableForwarding (UINT2 u2PortNum)
{

    AST_UNUSED (u2PortNum);
    AST_DBG_ARG1 (AST_STSM_DBG,
                  "STSM: Port %u: Forwarding Disabled\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPstateTrSmEventImpossible                       */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       Port Information.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_FAILURE                                        */
/*****************************************************************************/
INT4
PvrstPstateTrSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo)
{

    AST_TRC (AST_CONTROL_PATH_TRC,
             "STSM-PVRST: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_DBG (AST_STSM_DBG,
             "STSM-PVRST: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_STSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitPortStateTrMachine                          */
/*                                                                           */
/* Description        : Initialises the Port State Transition State Machine. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PvrstInitPortStateTrMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_PSTATETRSM_MAX_EVENTS][20] = {
        "BEGIN", "LEARN", "FORWARD",
        "LEARN_FWD_DISABLED"
    };
    UINT1               aau1SemState[PVRST_PSTATETRSM_MAX_STATES][20] = {
        "DISCARDING", "LEARNING", "FORWARDING"
    };

    for (i4Index = 0; i4Index < PVRST_PSTATETRSM_MAX_EVENTS; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemEvent[AST_STSM][i4Index],
                    aau1SemEvent[i4Index]);
    }
    for (i4Index = 0; i4Index < PVRST_PSTATETRSM_MAX_STATES; i4Index++)
    {
        AST_STRCPY (gaaau1AstSemState[AST_STSM][i4Index],
                    aau1SemState[i4Index]);
    }
    /* Event 0 - PVRST_PSTATETRSM_EV_BEGIN */
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_BEGIN]
        [PVRST_PSTATETRSM_STATE_DISCARDING].pAction =
        PvrstPstateTrSmMakeDiscarding;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_BEGIN]
        [PVRST_PSTATETRSM_STATE_LEARNING].pAction =
        PvrstPstateTrSmMakeDiscarding;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_BEGIN]
        [PVRST_PSTATETRSM_STATE_FORWARDING].pAction =
        PvrstPstateTrSmMakeDiscarding;

    /* Event 1 - PVRST_PSTATETRSM_EV_LEARN */
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN]
        [PVRST_PSTATETRSM_STATE_DISCARDING].pAction =
        PvrstPstateTrSmMakeLearning;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN]
        [PVRST_PSTATETRSM_STATE_LEARNING].pAction = NULL;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN]
        [PVRST_PSTATETRSM_STATE_FORWARDING].pAction = NULL;

    /* Event 2 - PVRST_PSTATETRSM_EV_FORWARD */
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_FORWARD]
        [PVRST_PSTATETRSM_STATE_DISCARDING].pAction =
        PvrstPstateTrSmEventImpossible;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_FORWARD]
        [PVRST_PSTATETRSM_STATE_LEARNING].pAction =
        PvrstPstateTrSmMakeForwarding;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_FORWARD]
        [PVRST_PSTATETRSM_STATE_FORWARDING].pAction =
        PvrstPstateTrSmEventImpossible;

    /* Event 3 - PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED */
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [PVRST_PSTATETRSM_STATE_DISCARDING].pAction =
        PvrstPstateTrSmMakeDiscarding;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [PVRST_PSTATETRSM_STATE_LEARNING].pAction =
        PvrstPstateTrSmMakeDiscarding;
    PVRST_PORT_STATE_TR_MACHINE[PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED]
        [PVRST_PSTATETRSM_STATE_FORWARDING].pAction =
        PvrstPstateTrSmMakeDiscarding;

    AST_DBG (AST_TXSM_DBG, "STSM: Loaded STSM SEM successfully\n");
    return;
}
