/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspvrslw.c,v 1.79 2018/01/31 10:57:43 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "asthdrs.h"
# include "astvinc.h"
# include  "fssnmp.h"
# include "stpcli.h"
# include "fsmppvcli.h"

extern UINT4        fsmppv[8];
PRIVATE VOID        PvrstNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstSystemControl
 Input       :  The Indices

                The Object 
                retValFsPvrstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstSystemControl (INT4 *pi4RetValFsPvrstSystemControl)
{
    if (AST_IS_PVRST_STARTED ())
    {
        *pi4RetValFsPvrstSystemControl = (INT4) PVRST_SNMP_START;
    }
    else
    {
        *pi4RetValFsPvrstSystemControl = (INT4) PVRST_SNMP_SHUTDOWN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstModuleStatus
 Input       :  The Indices

                The Object 
                retValFsPvrstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstModuleStatus (INT4 *pi4RetValFsPvrstModuleStatus)
{
    if (AST_IS_PVRST_STARTED ())
    {
        if ((AST_NODE_STATUS () == RED_AST_ACTIVE) ||
            (AST_NODE_STATUS () == RED_AST_STANDBY))
        {
            *pi4RetValFsPvrstModuleStatus =
                gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()];
            return (INT1) SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsPvrstModuleStatus = (INT4) AST_ADMIN_STATUS;
            return (INT1) SNMP_SUCCESS;
        }
    }
    *pi4RetValFsPvrstModuleStatus = (INT4) PVRST_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstNoOfActiveInstances
 Input       :  The Indices

                The Object 
                retValFsPvrstNoOfActiveInstances
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstNoOfActiveInstances (INT4 *pi4RetValFsPvrstNoOfActiveInstances)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstNoOfActiveInstances = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsPvrstNoOfActiveInstances = PVRST_GET_NO_OF_ACTIVE_INSTANCES;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstBrgAddress
 Input       :  The Indices

                The Object 
                retValFsPvrstBrgAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstBrgAddress (tMacAddr * pRetValFsPvrstBrgAddress)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");

        AST_MEMSET ((UINT1 *) *pRetValFsPvrstBrgAddress, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    AST_MEMCPY ((UINT1 *) pRetValFsPvrstBrgAddress,
                &(pAstBridgeEntry->BridgeAddr), AST_MAC_ADDR_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstUpCount
 Input       :  The Indices

                The Object 
                retValFsPvrstUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstUpCount (UINT4 *pu4RetValFsPvrstUpCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstUpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstBridgeEntry = AST_GET_BRGENTRY ();
    *pu4RetValFsPvrstUpCount = pAstBridgeEntry->u4AstpUpCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstDownCount
 Input       :  The Indices

                The Object 
                retValFsPvrstDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstDownCount (UINT4 *pu4RetValFsPvrstDownCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstDownCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstBridgeEntry = AST_GET_BRGENTRY ();

    *pu4RetValFsPvrstDownCount = pAstBridgeEntry->u4AstpDownCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPathCostDefaultType
 Input       :  The Indices

                The Object 
                retValFsPvrstPathCostDefaultType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPathCostDefaultType (INT4 *pi4RetValFsPvrstPathCostDefaultType)
{
    UNUSED_PARAM (pi4RetValFsPvrstPathCostDefaultType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstDynamicPathCostCalculation
 Input       :  The Indices

                The Object 
                retValFsPvrstDynamicPathCostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstDynamicPathCostCalculation (INT4
                                         *pi4RetValFsPvrstDynamicPathCostCalculation)
{
    tAstBridgeEntry    *pAstBridgeEntry = AST_GET_BRGENTRY ();

    *pi4RetValFsPvrstDynamicPathCostCalculation = AST_SNMP_FALSE;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        return SNMP_SUCCESS;
    }

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == PVRST_TRUE)
    {
        *pi4RetValFsPvrstDynamicPathCostCalculation = AST_SNMP_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstTrace
 Input       :  The Indices

                The Object 
                retValFsPvrstTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstTrace (INT4 *pi4RetValFsPvrstTrace)
{
    *pi4RetValFsPvrstTrace = AST_TRACE_OPTION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstDebug
 Input       :  The Indices

                The Object 
                retValFsPvrstDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstDebug (INT4 *pi4RetValFsPvrstDebug)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstDebug = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsPvrstDebug = (INT4) AST_DEBUG_OPTION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstBufferOverFlowCount
 Input       :  The Indices

                The Object 
                retValFsPvrstBufferOverFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstBufferOverFlowCount (UINT4 *pu4RetValFsPvrstBufferOverFlowCount)
{
    *pu4RetValFsPvrstBufferOverFlowCount = gAstGlobalInfo.u4BufferFailureCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstMemAllocFailureCount
 Input       :  The Indices

                The Object 
                retValFsPvrstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstMemAllocFailureCount (UINT4 *pu4RetValFsPvrstMemAllocFailureCount)
{
    *pu4RetValFsPvrstMemAllocFailureCount = gAstGlobalInfo.u4MemoryFailureCount;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstSystemControl
 Input       :  The Indices

                The Object 
                setValFsPvrstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstSystemControl (INT4 i4SetValFsPvrstSystemControl)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4MsgType = AST_INIT_VAL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4IcclIfIndex = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (i4SetValFsPvrstSystemControl == (INT4) PVRST_SNMP_START)
    {
        if (AST_IS_PVRST_STARTED ())
        {
            AST_TRC (AST_MGMT_TRC, "MGMT: PVRST is already Started.\n");

            return SNMP_SUCCESS;
        }
        u4MsgType = AST_START_PVRST_MSG;
    }
    else
    {
        if (!AST_IS_PVRST_STARTED ())
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "LW: PVRST Module is not started to shut down !!!\n");
            return SNMP_SUCCESS;
        }
        u4MsgType = AST_STOP_PVRST_MSG;

    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN); */
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                 AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pNode->MsgType = u4MsgType;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstSystemControl,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstSystemControl));

    /* When MCLAG is enabled and ICCL interface is present,
     * PVRST is disabled on the ICCL interface and on the MC-LAG
     * interface*/
    if ((AST_IS_PVRST_STARTED ()) &&
        (AstLaGetMCLAGSystemStatus () == AST_MCLAG_ENABLED))
    {
        AstIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex != 0)
        {
            AST_UNLOCK ();
            /*Disable PVRST on ICCL */
            if (AstDisableStpOnPort (u4IcclIfIndex) != OSIX_SUCCESS)
            {
                i1RetVal = SNMP_FAILURE;
            }

            /* Disable PVRST on MC-LAG interfaces */
            AstPortLaDisableOnMcLagIf ();
            AST_LOCK ();
        }
    }

    AstSelectContext (u4CurrContextId);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4SetValFsPvrstSystemControl == (INT4) PVRST_SNMP_SHUTDOWN)
        {
            /* Notify MSR with the pvrst oids */
            PvrstNotifyProtocolShutdownStatus ();
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstModuleStatus
 Input       :  The Indices

                The Object 
                setValFsPvrstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstModuleStatus (INT4 i4SetValFsPvrstModuleStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4MsgType = AST_INIT_VAL;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if ((i4SetValFsPvrstModuleStatus ==
         (INT4) gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()])
        && (i4SetValFsPvrstModuleStatus == (INT4) AST_ADMIN_STATUS))
    {
        AST_TRC (AST_MGMT_TRC, "MGMT: PVRST already has the same Status\n");

        return (INT1) SNMP_SUCCESS;
    }
    if (i4SetValFsPvrstModuleStatus == (INT4) PVRST_ENABLED)
    {
        u4MsgType = AST_ENABLE_PVRST_MSG;
    }
    else
    {
        u4MsgType = AST_DISABLE_PVRST_MSG;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->MsgType = u4MsgType;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstModuleStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstModuleStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPathCostDefaultType
 Input       :  The Indices

                The Object 
                setValFsPvrstPathCostDefaultType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPathCostDefaultType (INT4 i4SetValFsPvrstPathCostDefaultType)
{
    UNUSED_PARAM (i4SetValFsPvrstPathCostDefaultType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstDynamicPathCostCalculation
 Input       :  The Indices

                The Object 
                setValFsPvrstDynamicPathCostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstDynamicPathCostCalculation (INT4
                                         i4SetValFsPvrstDynamicPathCostCalculation)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_DYNAMIC_PATHCOST_MSG;
    if (i4SetValFsPvrstDynamicPathCostCalculation == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = PVRST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = PVRST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstDynamicPathCostCalculation,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstDynamicPathCostCalculation));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstTrace
 Input       :  The Indices

                The Object 
                setValFsPvrstTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstTrace (INT4 i4SetValFsPvrstTrace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_TRACE_OPTION = (UINT4) i4SetValFsPvrstTrace;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "LW: Management SET of Trace Option as %d Success\n",
                  i4SetValFsPvrstTrace);

    AST_DEBUG_OPTION = (UINT4) AST_INIT_VAL;
    AST_DBG (AST_MGMT_DBG, "LW: Resetting the Debug Option...\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstTrace, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstTrace));
    AstSelectContext (u4CurrContextId);
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsPvrstDebug
 Input       :  The Indices

                The Object 
                setValFsPvrstDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstDebug (INT4 i4SetValFsPvrstDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_DEBUG_OPTION = (UINT4) i4SetValFsPvrstDebug;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "LW: Management SET of Debug Option as %d Success\n",
                  i4SetValFsPvrstDebug);

    AST_TRACE_OPTION = AST_INIT_VAL;
    AST_DBG (AST_MGMT_DBG, "LW: Resetting the Trace Option...\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstDebug, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstDebug));
    AstSelectContext (u4CurrContextId);
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstSystemControl
 Input       :  The Indices

                The Object 
                testValFsPvrstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstSystemControl (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsPvrstSystemControl)
{
    UINT4               u4BridgeMode = AST_INVALID_BRIDGE_MODE;

    if ((i4TestValFsPvrstSystemControl != (INT4) PVRST_SNMP_START) &&
        (i4TestValFsPvrstSystemControl != (INT4) PVRST_SNMP_SHUTDOWN))
    {
        *pu4ErrorCode = (INT4) SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module already Enabled.. Shutdown RSTP and Start PVRST!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_IS_MST_STARTED ())
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MST Module already Enabled.. Shutdown MST and Start PVRST!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        CLI_SET_ERR (CLI_STP_ASYNC_MODE_ERR);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }
#endif

    if (i4TestValFsPvrstSystemControl == (INT4) PVRST_SNMP_SHUTDOWN)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if (i4TestValFsPvrstSystemControl == (INT4) PVRST_SNMP_START)
    {
        /* Check whether the bridge mode is set. If not set
         * then return failure. */
        if (L2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &u4BridgeMode)
            != L2IWF_SUCCESS)
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        if (u4BridgeMode == AST_INVALID_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_STP_BRG_MODE_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
        if ((u4BridgeMode == AST_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == AST_PROVIDER_BRIDGE_MODE) ||
            (u4BridgeMode == AST_ICOMPONENT_BRIDGE_MODE) ||
            (u4BridgeMode == AST_BCOMPONENT_BRIDGE_MODE))

        {
            CLI_SET_ERR (CLI_STP_PVRST_PB_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_STP_BASE_BRIDGE_PVRST_ENABLED);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    if (VlanConfigCheckForPvrst (AST_DEFAULT_CONTEXT, pu4ErrorCode) ==
        VLAN_FAILURE)
    {
        switch (*pu4ErrorCode)
        {
            case VLAN_PVRST_VLAN_NOTSTARTED_ERR:
                CLI_SET_ERR (CLI_STP_VLAN_NOTSTARTED_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            case VLAN_PVRST_CXT_NOTPRESENT_ERR:
                CLI_SET_ERR (CLI_STP_CXT_NOTPRESENT_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            case VLAN_PVRST_HYBRID_PVID_ERR:
                CLI_SET_ERR (CLI_STP_HYBRID_PVID_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            case VLAN_PVRST_HYBRID_UNTAG_ERR:
                CLI_SET_ERR (CLI_STP_HYBRID_UNTAG_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            case VLAN_PVRST_FORBIDDEN_PORT_ERR:
                CLI_SET_ERR (CLI_STP_FORBIDDEN_PORT_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }
    }

    /* If the context has SISP enabled ports as member ports, than RSTP 
     * cannot be the operating mode for the context
     * */
    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_BRG_MODE_ERR);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;

        return (INT1) SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstModuleStatus
 Input       :  The Indices

                The Object 
                testValFsPvrstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstModuleStatus (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPvrstModuleStatus)
{
    if ((i4TestValFsPvrstModuleStatus != PVRST_ENABLED) &&
        (i4TestValFsPvrstModuleStatus != PVRST_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "LW: PVRST Module not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPathCostDefaultType
 Input       :  The Indices

                The Object 
                testValFsPvrstPathCostDefaultType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPathCostDefaultType (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPvrstPathCostDefaultType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsPvrstPathCostDefaultType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstDynamicPathCostCalculation
 Input       :  The Indices

                The Object 
                testValFsPvrstDynamicPathCostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstDynamicPathCostCalculation (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsPvrstDynamicPathCostCalculation)
{
    if ((i4TestValFsPvrstDynamicPathCostCalculation != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstDynamicPathCostCalculation != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstTrace
 Input       :  The Indices

                The Object 
                testValFsPvrstTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstTrace (UINT4 *pu4ErrorCode, INT4 i4TestValFsPvrstTrace)
{
    if ((i4TestValFsPvrstTrace < AST_MIN_TRACE_VAL) ||
        (i4TestValFsPvrstTrace > AST_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstDebug
 Input       :  The Indices

                The Object 
                testValFsPvrstDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsPvrstDebug)
{
    if ((i4TestValFsPvrstDebug < AST_MIN_DEBUG_VAL) ||
        (i4TestValFsPvrstDebug > PVRST_MAX_DEBUG_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPvrstSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstSystemControl (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstModuleStatus (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstPathCostDefaultType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstPathCostDefaultType (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstDynamicPathCostCalculation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstDynamicPathCostCalculation (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFuturePvrstPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFuturePvrstPortTable
 Input       :  The Indices
                FsPvrstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFuturePvrstPortTable (INT4 i4FsPvrstPort)
{
    if ((i4FsPvrstPort < AST_MIN_NUM_PORTS) ||
        (i4FsPvrstPort > AST_MAX_NUM_PORTS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFuturePvrstPortTable
 Input       :  The Indices
                FsPvrstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFuturePvrstPortTable (INT4 *pi4FsPvrstPort)
{
    return nmhGetNextIndexFsFuturePvrstPortTable (0, pi4FsPvrstPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFuturePvrstPortTable
 Input       :  The Indices
                FsPvrstPort
                nextFsPvrstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFuturePvrstPortTable (INT4 i4FsPvrstPort,
                                       INT4 *pi4NextFsPvrstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortIndex;
    UINT1               u1Flag = PVRST_FALSE;

    if (i4FsPvrstPort < 0)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }

    for (u2PortIndex = 1; u2PortIndex <= AST_MAX_NUM_PORTS; u2PortIndex++)
    {
        pAstPortEntry = AST_GET_PORTENTRY (u2PortIndex);

        if (pAstPortEntry == NULL)
        {
            continue;
        }
        if (pAstPortEntry->u2PortNo > i4FsPvrstPort)
        {
            if (u1Flag == PVRST_FALSE)
            {
                *pi4NextFsPvrstPort = pAstPortEntry->u2PortNo;
                u1Flag = PVRST_TRUE;
            }
            else
            {
                if (*pi4NextFsPvrstPort > pAstPortEntry->u2PortNo)
                {
                    *pi4NextFsPvrstPort = pAstPortEntry->u2PortNo;
                }
            }
        }
    }

    if (u1Flag == PVRST_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortAdminEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortAdminEdgeStatus (INT4 i4FsPvrstPort,
                                  INT4 *pi4RetValFsPvrstPortAdminEdgeStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstPortAdminEdgeStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->bAdminEdgePort == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortAdminEdgeStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortAdminEdgeStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortOperEdgePortStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortOperEdgePortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortOperEdgePortStatus (INT4 i4FsPvrstPort,
                                     INT4
                                     *pi4RetValFsPvrstPortOperEdgePortStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstPortOperEdgePortStatus = AST_INIT_VAL;
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->bOperEdgePort == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortOperEdgePortStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortOperEdgePortStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstBridgeDetectionSemState
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstBridgeDetectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstBridgeDetectionSemState (INT4 i4FsPvrstPort,
                                      INT4
                                      *pi4RetValFsPvrstBridgeDetectionSemState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->CommPortInfo.u1BrgDetSmState == PVRST_TRUE)
    {
        *pi4RetValFsPvrstBridgeDetectionSemState = PVRST_EDGE_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstBridgeDetectionSemState = PVRST_EDGE_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortEnabledStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortEnabledStatus (INT4 i4FsPvrstPort,
                                INT4 *pi4RetValFsPvrstPortEnabledStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstPortEnabledStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->CommPortInfo.bPortPvrstStatus == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortEnabledStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortEnabledStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstRootGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstRootGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstRootGuard (INT4 i4FsPvrstPort, INT4 *pi4RetValFsPvrstRootGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstRootGuard = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->CommPortInfo.bRootGuard == PVRST_TRUE)
    {
        *pi4RetValFsPvrstRootGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstRootGuard = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstBpduGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstBpduGuard (INT4 i4FsPvrstPort, INT4 *pi4RetValFsPvrstBpduGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstBpduGuard = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstBpduGuard = pAstPortEntry->CommPortInfo.u4BpduGuard;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstEncapType
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstEncapType (INT4 i4FsPvrstPort, INT4 *pi4RetValFsPvrstEncapType)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT1               u1PortType = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not started!\n");
        *pi4RetValFsPvrstEncapType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    AstL2IwfGetVlanPortType ((UINT2) pAstPortEntry->u4IfIndex, &u1PortType);
    if (u1PortType != VLAN_TRUNK_PORT)
    {
        *pi4RetValFsPvrstEncapType = PVRST_DOT1Q;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsPvrstEncapType = pAstPortEntry->CommPortInfo.eEncapType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortAdminPointToPoint (INT4 i4FsPvrstPort,
                                    INT4 *pi4RetValFsPvrstPortAdminPointToPoint)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not started!\n");
        *pi4RetValFsPvrstPortAdminPointToPoint = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstPortAdminPointToPoint = pAstPortEntry->u1AdminPointToPoint;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortOperPointToPoint
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortOperPointToPoint (INT4 i4FsPvrstPort,
                                   INT4 *pi4RetValFsPvrstPortOperPointToPoint)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not started!\n");
        *pi4RetValFsPvrstPortOperPointToPoint = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pAstPortEntry->bOperPointToPoint == RST_TRUE)
    {
        *pi4RetValFsPvrstPortOperPointToPoint = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortOperPointToPoint = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortInvalidBpdusRcvd
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortInvalidBpdusRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortInvalidBpdusRcvd (INT4 i4FsPvrstPort,
                                   UINT4 *pu4RetValFsPvrstPortInvalidBpdusRcvd)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstPortInvalidBpdusRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pu4RetValFsPvrstPortInvalidBpdusRcvd =
        pAstPortEntry->u4InvalidRstBpdusRxdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortInvalidConfigBpduRxCount (INT4 i4FsPvrstPort,
                                           UINT4
                                           *pu4RetValFsPvrstPortInvalidConfigBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstPortInvalidConfigBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pu4RetValFsPvrstPortInvalidConfigBpduRxCount =
        pAstPortEntry->u4InvalidConfigBpdusRxdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortInvalidTcnBpduRxCount (INT4 i4FsPvrstPort,
                                        UINT4
                                        *pu4RetValFsPvrstPortInvalidTcnBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstPortInvalidTcnBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pu4RetValFsPvrstPortInvalidTcnBpduRxCount =
        pAstPortEntry->u4InvalidTcnBpdusRxdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortRowStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortRowStatus (INT4 i4FsPvrstPort,
                            INT4 *pi4RetValFsPvrstPortRowStatus)
{
    if (!AST_IS_PVRST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: Pvrst Module not Enabled!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstPortRowStatus =
        (AST_GET_PORTENTRY (i4FsPvrstPort))->i4PortRowStatus;

    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortAdminEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortAdminEdgeStatus (INT4 i4FsPvrstPort,
                                  INT4 i4SetValFsPvrstPortAdminEdgeStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ADMIN_EDGEPORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    if (i4SetValFsPvrstPortAdminEdgeStatus == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bAdminEdgePort = PVRST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bAdminEdgePort = PVRST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortAdminEdgeStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortAdminEdgeStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortEnabledStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortEnabledStatus (INT4 i4FsPvrstPort,
                                INT4 i4SetValFsPvrstPortEnabledStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    /* Modified for Attachment Circuit interface */
    if ((AstIsExtInterface (i4FsPvrstPort) == AST_TRUE) &&
        (i4SetValFsPvrstPortEnabledStatus == AST_SNMP_TRUE))
    {
        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPvrstPort);
        if ((pAstPortEntry == NULL) &&
            (AstCreateExtInterface (i4FsPvrstPort) == RST_FAILURE))
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_CONFIG_STATUS_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    if (i4SetValFsPvrstPortEnabledStatus == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bStatus = PVRST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bStatus = PVRST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortEnabledStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortEnabledStatus));
    AstSelectContext (u4CurrContextId);

    /* Modified for Attachment Circuit interface */
    if ((AstIsExtInterface (i4FsPvrstPort) == AST_TRUE) &&
        (i4SetValFsPvrstPortEnabledStatus == AST_SNMP_FALSE))
    {
        if (AstDeleteExtInterface (i4FsPvrstPort) == RST_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstRootGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstRootGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstRootGuard (INT4 i4FsPvrstPort, INT4 i4SetValFsPvrstRootGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    if (i4SetValFsPvrstRootGuard == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bStatus = PVRST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bStatus = PVRST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstRootGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstRootGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstBpduGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstBpduGuard (INT4 i4FsPvrstPort, INT4 i4SetValFsPvrstBpduGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    pAstMsgNode->uMsg.u4BpduGuardStatus = (UINT4) i4SetValFsPvrstBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstBpduGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstEncapType
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstEncapType (INT4 i4FsPvrstPort, INT4 i4SetValFsPvrstEncapType)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT1               u1PortType = AST_INIT_VAL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPvrstPort);

    AstL2IwfGetVlanPortType ((UINT2) pPortEntry->u4IfIndex, &u1PortType);
    if (u1PortType != VLAN_TRUNK_PORT)
    {
        return SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_SET_ENCAP_TYPE_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;
    pAstMsgNode->uMsg.eEncapType = i4SetValFsPvrstEncapType;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstEncapType,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstEncapType));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortAdminPointToPoint (INT4 i4FsPvrstPort,
                                    INT4 i4SetValFsPvrstPortAdminPointToPoint)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ADMIN_PTOP_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;
    pAstMsgNode->uMsg.u1AdminPToP =
        (UINT1) i4SetValFsPvrstPortAdminPointToPoint;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortAdminPointToPoint,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortAdminPointToPoint));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortRowStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortRowStatus (INT4 i4FsPvrstPort,
                            INT4 i4SetValFsPvrstPortRowStatus)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1              *pu1VlanList = NULL;
    UINT2               u2VlanIndex = 0;
    BOOL1               b1Result = VLAN_FALSE;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1OperStatus = AST_INIT_VAL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pu1VlanList = (VOID *) UtilVlanAllocVlanIdSize (VLAN_LIST_SIZE);

    if (pu1VlanList == NULL)
    {
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, (UINT2) i4FsPvrstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fecthing the external port \n");
        UtilVlanReleaseVlanIdSize (pu1VlanList);
        return SNMP_FAILURE;

    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        UtilVlanReleaseVlanIdSize (pu1VlanList);
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->u4PortNo = u4IfIndex;
    pAstMsgNode->u4ContextId = u4ContextId;
    pAstMsgNode->uMsg.u2LocalPortId = (UINT2) i4FsPvrstPort;

    switch (i4SetValFsPvrstPortRowStatus)
    {
        case ACTIVE:
            pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
            }
            break;
        case NOT_IN_SERVICE:
            pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = NOT_IN_SERVICE;
            }
            break;
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
            if (AstHandleCreatePort (pAstMsgNode) != RST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Msg Memory Block FAILED!\n");
                }
                UtilVlanReleaseVlanIdSize (pu1VlanList);
                return SNMP_FAILURE;
            }
            AstL2IwfGetPortVlanMemberList (u4IfIndex, pu1VlanList);

            for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID;
                 u2VlanIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex,
                                         VLAN_LIST_SIZE, b1Result);

                if (b1Result == OSIX_TRUE)
                {
                    if (PvrstChangePVID ((UINT4) i4FsPvrstPort, 0, u2VlanIndex)
                        != PVRST_SUCCESS)
                    {
                        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                            AST_MEM_SUCCESS)
                        {
                            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
                        }
                        UtilVlanReleaseVlanIdSize (pu1VlanList);
                        return SNMP_FAILURE;
                    }

                }
            }

            pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
            if (pPortEntry != NULL)
            {
                if (i4SetValFsPvrstPortRowStatus == CREATE_AND_GO)
                {
                    AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
                }
                else if (i4SetValFsPvrstPortRowStatus == CREATE_AND_WAIT)
                {
                    AST_PORT_ROW_STATUS (pPortEntry) = NOT_READY;
                }
            }
            break;
        case DESTROY:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_MSG;
            pAstMsgNode->u4PortNo = i4FsPvrstPort;
            if (AstHandleDeletePort (pAstMsgNode) != RST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Msg Memory Block FAILED!\n");
                }
                UtilVlanReleaseVlanIdSize (pu1VlanList);
                return SNMP_FAILURE;
            }
            break;
    }

    if (AstCfaGetIfOperStatus (u4IfIndex, &u1OperStatus) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfOperStatus for Port %u\n", u4IfIndex);
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }
        UtilVlanReleaseVlanIdSize (pu1VlanList);
        return SNMP_FAILURE;
    }
    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pPortEntry != NULL)
    {
        if (u1OperStatus == AST_UP)
        {
            if (pPortEntry->i4PortRowStatus == ACTIVE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_UP);
            }
            else if (pPortEntry->i4PortRowStatus == NOT_IN_SERVICE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_DOWN);
            }
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    u4ContextId = AST_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortRowStatus,
                          u4SeqNum, TRUE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex,
                      i4SetValFsPvrstPortRowStatus));
    AstSelectContext (u4ContextId);

    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Msg Memory Block FAILED!\n");
        UtilVlanReleaseVlanIdSize (pu1VlanList);
        return SNMP_FAILURE;
    }
    UtilVlanReleaseVlanIdSize (pu1VlanList);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortAdminEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortAdminEdgeStatus (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                                     INT4 i4TestValFsPvrstPortAdminEdgeStatus)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortAdminEdgeStatus != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstPortAdminEdgeStatus != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortEnabledStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortEnabledStatus (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                                   INT4 i4TestValFsPvrstPortEnabledStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1TunnelStatus;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*Check for Pseudo/AC Interface, no validation for Pseudo/AC Interface */
    /* Modified for Attachment Circuit interface */
    if (AstIsExtInterface (i4FsPvrstPort) == AST_TRUE)
    {
        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPvrstPort);
        if ((i4TestValFsPvrstPortEnabledStatus == AST_SNMP_FALSE) &&
            (pAstPortEntry == NULL))
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "Such a Port DOES NOT exist!\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortEnabledStatus != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstPortEnabledStatus != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPvrstPort);
    if (i4TestValFsPvrstPortEnabledStatus == AST_SNMP_TRUE)
    {
        if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
        {
            AstL2IwfGetPortVlanTunnelStatus (pAstPortEntry->u4IfIndex,
                                             (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = (INT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_PORT_TYPE_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE) ||
            (AST_IS_CUSTOMER_EDGE_PORT ((UINT2) i4FsPvrstPort) == RST_TRUE))
        {
            /* Enabling the protocol status on a port is not allowed 
             * when the protocol tunnel status is set to Tunnel/Discard 
             * on a port.*/
            AstL2IwfGetProtocolTunnelStatusOnPort ((UINT2) pAstPortEntry->
                                                   u4IfIndex, L2_PROTO_STP,
                                                   &u1TunnelStatus);

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_TUNNEL_PROTO_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    if (i4TestValFsPvrstPortEnabledStatus == AST_SNMP_TRUE)
    {
        AstIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex == pAstPortEntry->u4IfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_STP_NO_PVRST_ON_ICCL_ERR);
            return SNMP_FAILURE;
        }

        u1RetVal = AstLaIsMclagInterface (pAstPortEntry->u4IfIndex);

        if (u1RetVal == RST_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_STP_NO_PVRST_ON_MCLAG_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstRootGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstRootGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                           INT4 i4TestValFsPvrstRootGuard)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstRootGuard != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstRootGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pPortEntry->bLoopGuard == AST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_LOOPGUARD_ENABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstBpduGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstBpduGuard (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                           INT4 i4TestValFsPvrstBpduGuard)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsPvrstBpduGuard != AST_BPDUGUARD_DISABLE) &&
        (i4TestValFsPvrstBpduGuard != AST_BPDUGUARD_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstEncapType
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstEncapType (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                           INT4 i4TestValFsPvrstEncapType)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT1               u1PortType = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstEncapType != PVRST_DOT1Q) &&
        (i4TestValFsPvrstEncapType != PVRST_ISL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    AstL2IwfGetVlanPortType ((UINT2) pAstPortEntry->u4IfIndex, &u1PortType);

    if ((u1PortType == VLAN_ACCESS_PORT) || (u1PortType == VLAN_HYBRID_PORT))
    {
        if (i4TestValFsPvrstEncapType != PVRST_DOT1Q)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_STP_TRUNK_ENCAP_ERR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (u1PortType != VLAN_TRUNK_PORT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortAdminPointToPoint (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                                       INT4
                                       i4TestValFsPvrstPortAdminPointToPoint)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortAdminPointToPoint != PVRST_FORCE_TRUE) &&
        (i4TestValFsPvrstPortAdminPointToPoint != PVRST_FORCE_FALSE) &&
        (i4TestValFsPvrstPortAdminPointToPoint != PVRST_AUTO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortRowStatus
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                               INT4 i4TestValFsPvrstPortRowStatus)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;

    if (AstIsExtInterface (i4FsPvrstPort) == AST_FALSE)
    {
        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortRowStatus < ACTIVE) ||
        (i4TestValFsPvrstPortRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsPvrstPort < AST_MIN_NUM_PORTS || i4FsPvrstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, (UINT2) i4FsPvrstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fecthing the external port \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    if (AstL2IwfIsPortInPortChannel (u4IfIndex) == RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFuturePvrstPortTable
 Input       :  The Indices
                FsPvrstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFuturePvrstPortTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPvrstInstBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPvrstInstBridgeTable
 Input       :  The Indices
                FsPvrstInstVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPvrstInstBridgeTable (INT4 i4FsPvrstInstVlanId)
{
    if ((i4FsPvrstInstVlanId < PVRST_MIN_INSTANCES) ||
        (i4FsPvrstInstVlanId > VLAN_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPvrstInstBridgeTable
 Input       :  The Indices
                FsPvrstInstVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPvrstInstBridgeTable (INT4 *pi4FsPvrstInstVlanId)
{
    INT1                i1RetVal;
    i1RetVal = nmhGetNextIndexFsPvrstInstBridgeTable (0, pi4FsPvrstInstVlanId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPvrstInstBridgeTable
 Input       :  The Indices
                FsPvrstInstVlanId
                nextFsPvrstInstVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPvrstInstBridgeTable (INT4 i4FsPvrstInstVlanId,
                                       INT4 *pi4NextFsPvrstInstVlanId)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (i4FsPvrstInstVlanId < 0 || i4FsPvrstInstVlanId > VLAN_MAX_VLAN_ID)
    {
        return SNMP_FAILURE;
    }

    if (PvrstGetNextIndexPvrstInstBridgeTable (i4FsPvrstInstVlanId,
                                               pi4NextFsPvrstInstVlanId) ==
        PVRST_SUCCESS)
    {
        /*AstReleaseContext (); */
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstInstBridgePriority
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstBridgePriority (INT4 i4FsPvrstInstVlanId,
                                 INT4 *pi4RetValFsPvrstInstBridgePriority)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2Priority = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstBridgePriority = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    PVRST_GET_BRIDGE_PRIORITY (pAstPerStBridgeInfo->u2BrgPriority, u2Priority);
    *pi4RetValFsPvrstInstBridgePriority = (INT4) u2Priority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstRootCost
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstRootCost (INT4 i4FsPvrstInstVlanId,
                           INT4 *pi4RetValFsPvrstInstRootCost)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:MST Module is Enabled!\n");
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    *pi4RetValFsPvrstInstRootCost = (INT4) (pAstPerStBridgeInfo->u4RootCost);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstRootPort
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstRootPort (INT4 i4FsPvrstInstVlanId,
                           INT4 *pi4RetValFsPvrstInstRootPort)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstRootPort = AST_NO_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:MST Module is Enabled!\n");
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    *pi4RetValFsPvrstInstRootPort = pAstPerStBridgeInfo->u2RootPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstBridgeMaxAge (INT4 i4FsPvrstInstVlanId,
                               INT4 *pi4RetValFsPvrstInstBridgeMaxAge)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstBridgeMaxAge =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_MAX_AGE);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstBridgeMaxAge
        = AST_SYS_TO_MGMT (pPerStPvrstBridgeInfo->BridgeTimes.u2MaxAge);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstBridgeHelloTime (INT4 i4FsPvrstInstVlanId,
                                  INT4 *pi4RetValFsPvrstInstBridgeHelloTime)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module is not Started!\n");
        *pi4RetValFsPvrstInstBridgeHelloTime =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_HELLO_TIME);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstBridgeHelloTime = (INT4) AST_SYS_TO_MGMT
        (pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstBridgeForwardDelay (INT4 i4FsPvrstInstVlanId,
                                     INT4
                                     *pi4RetValFsPvrstInstBridgeForwardDelay)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstBridgeForwardDelay =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_FWD_DELAY);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstBridgeForwardDelay = AST_SYS_TO_MGMT
        (pPerStPvrstBridgeInfo->BridgeTimes.u2ForwardDelay);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstHoldTime
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstHoldTime (INT4 i4FsPvrstInstVlanId,
                           INT4 *pi4RetValFsPvrstInstHoldTime)
{
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsPvrstInstHoldTime = (INT4) AST_SYS_TO_MGMT (AST_HOLD_TIME);
    AST_UNUSED (i4FsPvrstInstVlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstTxHoldCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstTxHoldCount (INT4 i4FsPvrstInstVlanId,
                              INT4 *pi4RetValFsPvrstInstTxHoldCount)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstTxHoldCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstTxHoldCount =
        (INT4) pPerStPvrstBridgeInfo->u1TxHoldCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstTimeSinceTopologyChange
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstTimeSinceTopologyChange (INT4 i4FsPvrstInstVlanId,
                                          UINT4
                                          *pu4RetValFsPvrstInstTimeSinceTopologyChange)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    tAstSysTime         CurrSysTime;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstTimeSinceTopologyChange = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    AST_MEMSET (&CurrSysTime, AST_MEMSET_VAL, sizeof (tAstSysTime));

    if (pAstPerStBridgeInfo->u4NumTopoCh != 0)
    {
        AST_GET_SYS_TIME (&CurrSysTime);

        *pu4RetValFsPvrstInstTimeSinceTopologyChange = (UINT4)
            (CurrSysTime - (tAstSysTime)
             pAstPerStBridgeInfo->u4TimeSinceTopoCh);
        *pu4RetValFsPvrstInstTimeSinceTopologyChange =
            AST_CONVERT_STUPS_2_CENTI_SEC
            (*pu4RetValFsPvrstInstTimeSinceTopologyChange);
    }
    else
    {
        *pu4RetValFsPvrstInstTimeSinceTopologyChange = 0;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstTopChanges
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstTopChanges (INT4 i4FsPvrstInstVlanId,
                             UINT4 *pu4RetValFsPvrstInstTopChanges)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstTopChanges = AST_INIT_VAL;
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    *pu4RetValFsPvrstInstTopChanges = pAstPerStBridgeInfo->u4NumTopoCh;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstNewRootCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstNewRootCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstNewRootCount (INT4 i4FsPvrstInstVlanId,
                               UINT4 *pu4RetValFsPvrstInstNewRootCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstNewRootCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    *pu4RetValFsPvrstInstNewRootCount = pAstPerStBridgeInfo->u4NewRootIdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstInstanceUpCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstInstanceUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstInstanceUpCount (INT4 i4FsPvrstInstVlanId,
                                  UINT4 *pu4RetValFsPvrstInstInstanceUpCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstInstanceUpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstInstanceUpCount = AST_INSTANCE_UP_COUNT (u2InstIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstInstanceDownCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstInstanceDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstInstanceDownCount (INT4 i4FsPvrstInstVlanId,
                                    UINT4
                                    *pu4RetValFsPvrstInstInstanceDownCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstInstanceDownCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4RetValFsPvrstInstInstanceDownCount =
            AST_INSTANCE_UP_COUNT (u2InstIndex);
    }
    else
    {
        *pu4RetValFsPvrstInstInstanceDownCount =
            (AST_INSTANCE_UP_COUNT (u2InstIndex) - 1);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortRoleSelSemState
 Input       :  The Indices
* IPLAN_SOURCE : ~~Code~~
* REVIEW_CLASS :-/~~Code~~ /~~Docn~~ /
* REVIEW_TYPE :
* SEVERITY :-/~~High~~ /~~Medium~~ /~~Low~~ /~~Suggestion~~ / 
* ROOT_CAUSE :-/~~Process~~ /~~Negligence~~ /~~Oversight~~ /~~Communication~~ /~~InadequateSkills~~ /~~CustomerProductProblem~~ / 
* STATUS : ~~OPEN~~ 
* LOCATION : ~~ code/future/stp/pvrst/src/fspvrslw.c  ,3624 ~~
* COMMENTS_
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstPortRoleSelSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortRoleSelSemState (INT4 i4FsPvrstInstVlanId,
                                      INT4
                                      *pi4RetValFsPvrstInstPortRoleSelSemState)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortRoleSelSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    *pi4RetValFsPvrstInstPortRoleSelSemState =
        pAstPerStBridgeInfo->u1ProleSelSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstDesignatedRoot
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstDesignatedRoot (INT4 i4FsPvrstInstVlanId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsPvrstInstDesignatedRoot)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        pu1List = pRetValFsPvrstInstDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsPvrstInstDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pu1List = pRetValFsPvrstInstDesignatedRoot->pu1_OctetList;

    u2Val = pAstPerStBridgeInfo->RootId.u2BrgPriority;
    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pAstPerStBridgeInfo->RootId.BridgeAddr);

    pRetValFsPvrstInstDesignatedRoot->i4_Length =
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstRootMaxAge
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstRootMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstRootMaxAge (INT4 i4FsPvrstInstVlanId,
                             INT4 *pi4RetValFsPvrstInstRootMaxAge)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstRootMaxAge =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_MAX_AGE);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (UINT2) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstRootMaxAge
        = AST_SYS_TO_MGMT (pPerStPvrstBridgeInfo->RootTimes.u2MaxAge);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstRootHelloTime
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstRootHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstRootHelloTime (INT4 i4FsPvrstInstVlanId,
                                INT4 *pi4RetValFsPvrstInstRootHelloTime)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstRootHelloTime =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_HELLO_TIME);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (UINT2) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstRootHelloTime
        = AST_SYS_TO_MGMT (pPerStPvrstBridgeInfo->RootTimes.u2HelloTime);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstRootForwardDelay
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstInstRootForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstRootForwardDelay (INT4 i4FsPvrstInstVlanId,
                                   INT4 *pi4RetValFsPvrstInstRootForwardDelay)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstRootForwardDelay =
            (INT4) AST_SYS_TO_MGMT (PVRST_DEFAULT_BRG_FWD_DELAY);
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (UINT2) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    *pi4RetValFsPvrstInstRootForwardDelay
        = AST_SYS_TO_MGMT (pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay);
    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstOldDesignatedRoot
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *
 * *                The Object
 * *                retValFsPvrstInstOldDesignatedRoot
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstOldDesignatedRoot (INT4 i4FsPvrstInstVlanId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsPvrstInstOldDesignatedRoot)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        pu1List = pRetValFsPvrstInstOldDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsPvrstInstOldDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pu1List = pRetValFsPvrstInstOldDesignatedRoot->pu1_OctetList;
    u2Val = pAstPerStBridgeInfo->OldRootId.u2BrgPriority;
    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pAstPerStBridgeInfo->OldRootId.BridgeAddr);

    pRetValFsPvrstInstOldDesignatedRoot->i4_Length =
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstFlushIndicationThreshold
 Input       :  The Indices
                FsMIPvrstInstVlanId

                The Object
                retValFsMIPvrstInstFlushIndicationThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstFlushIndicationThreshold (INT4 i4FsMIPvrstInstVlanId,
                                           INT4
                                           *pi4RetValFsMIPvrstInstFlushIndicationThreshold)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        *pi4RetValFsMIPvrstInstFlushIndicationThreshold = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId)
                                                i4FsMIPvrstInstVlanId);

    if (u2InstIndex == INVALID_INDEX)
    {
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        *pi4RetValFsMIPvrstInstFlushIndicationThreshold = AST_INIT_VAL;
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (u2InstIndex);
    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIPvrstInstFlushIndicationThreshold =
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhGetFsPvrstInstTotalFlushCount
  Input       :  The Indices
                 FsMIPvrstInstVlanId
 
                 The Object
                 retValFsMIPvrstInstTotalFlushCount
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPvrstInstTotalFlushCount (INT4 i4FsMIPvrstInstVlanId,
                                  UINT4 *pu4RetValFsMIPvrstInstTotalFlushCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (u2InstIndex);
    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId)
                                                i4FsMIPvrstInstVlanId);

    if (u2InstIndex == INVALID_INDEX)
    {
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);

        return SNMP_FAILURE;
    }

    *pu4RetValFsMIPvrstInstTotalFlushCount =
        pAstPerStBridgeInfo->u4TotalFlushCount;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Enabled!\n");
        *pu4RetValFsMIPvrstInstTotalFlushCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

}

 /* Low Level SET Routine for All Objects  */
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstInstBridgePriority
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                setValFsPvrstInstBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstBridgePriority (INT4 i4FsPvrstInstVlanId,
                                 INT4 i4SetValFsPvrstInstBridgePriority)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, 
           AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->MsgType = AST_BRIDGE_PRIORITY_MSG;
    pAstMsgNode->uMsg.u2BridgePriority =
        (UINT2) (i4SetValFsPvrstInstBridgePriority & AST_BRGPRIORITY_MASK);
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstBridgePriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsPvrstInstVlanId, i4SetValFsPvrstInstBridgePriority));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                setValFsPvrstInstBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstBridgeMaxAge (INT4 i4FsPvrstInstVlanId,
                               INT4 i4SetValFsPvrstInstBridgeMaxAge)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: Configure Max Age timers for `PVRST are"
                 " supported only on a per-port basis\n");
        return SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, 
           AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_BRG_MAXAGE_MSG;
    pAstMsgNode->uMsg.u2MaxAge =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsPvrstInstBridgeMaxAge);
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstBridgeMaxAge,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsPvrstInstVlanId, i4SetValFsPvrstInstBridgeMaxAge));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                setValFsPvrstInstBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstBridgeHelloTime (INT4 i4FsPvrstInstVlanId,
                                  INT4 i4SetValFsPvrstInstBridgeHelloTime)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: Configure Hello timers for `PVRST are"
                 " supported only on a per-port basis\n");
        return SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID,
           AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_HELLOTIME_MSG;
    pAstMsgNode->uMsg.u2HelloTime =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsPvrstInstBridgeHelloTime);
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstBridgeHelloTime,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsPvrstInstVlanId, i4SetValFsPvrstInstBridgeHelloTime));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                setValFsPvrstInstBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstBridgeForwardDelay (INT4 i4FsPvrstInstVlanId,
                                     INT4 i4SetValFsPvrstInstBridgeForwardDelay)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, 
           AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_FORWARDDELAY_MSG;
    pAstMsgNode->uMsg.u2ForwardDelay =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsPvrstInstBridgeForwardDelay);
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstBridgeForwardDelay,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsPvrstInstVlanId,
                      i4SetValFsPvrstInstBridgeForwardDelay));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstTxHoldCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                setValFsPvrstInstTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstTxHoldCount (INT4 i4FsPvrstInstVlanId,
                              INT4 i4SetValFsPvrstInstTxHoldCount)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, 
           AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_TX_HOLDCOUNT_MSG;
    pAstMsgNode->uMsg.u1TxHoldCount = (UINT1) (i4SetValFsPvrstInstTxHoldCount);
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstTxHoldCount,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsPvrstInstVlanId, i4SetValFsPvrstInstTxHoldCount));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhSetFsPvrstInstFlushIndicationThreshold
  Input       :  The Indices
                 i4FsMIPvrstInstVlanId
 
                 The Object
                 setValFsMIPvrstInstFlushIndicationThreshold
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPvrstInstFlushIndicationThreshold (INT4 i4FsMIPvrstInstVlanId,
                                           INT4
                                           i4SetValFsMIPvrstInstFlushIndicationThreshold)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId)
                                                i4FsMIPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (u2InstIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValFsMIPvrstInstFlushIndicationThreshold ==
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold)
    {
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo->u4FlushIndThreshold =
        (UINT4) i4SetValFsMIPvrstInstFlushIndicationThreshold;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstFlushIndicationThreshold,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIPvrstInstFlushIndicationThreshold));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstBridgePriority
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                testValFsPvrstInstBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstBridgePriority (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPvrstInstVlanId,
                                    INT4 i4TestValFsPvrstInstBridgePriority)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }

    if (i4FsPvrstInstVlanId == 0)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Instance Id is Zero !\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValFsPvrstInstBridgePriority < AST_BRGPRIORITY_MIN_VAL)
        || (i4TestValFsPvrstInstBridgePriority > AST_BRGPRIORITY_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPvrstInstBridgePriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                testValFsPvrstInstBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstBridgeMaxAge (UINT4 *pu4ErrorCode, INT4 i4FsPvrstInstVlanId,
                                  INT4 i4TestValFsPvrstInstBridgeMaxAge)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);

        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsPvrstInstBridgeMaxAge) != AST_OK)
        || (i4TestValFsPvrstInstBridgeMaxAge < AST_MIN_MAXAGE_VAL) ||
        (i4TestValFsPvrstInstBridgeMaxAge > AST_MAX_MAXAGE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((AST_BRG_FWDDELAY_MAXAGE_STD
         (pPerStPvrstBridgeInfo->BridgeTimes.u2ForwardDelay,
          AST_MGMT_TO_SYS (i4TestValFsPvrstInstBridgeMaxAge)))
        &&
        (AST_BRG_HELLOTIME_MAXAGE_STD
         (pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime,
          AST_MGMT_TO_SYS (i4TestValFsPvrstInstBridgeMaxAge))))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                testValFsPvrstInstBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstBridgeHelloTime (UINT4 *pu4ErrorCode,
                                     INT4 i4FsPvrstInstVlanId,
                                     INT4 i4TestValFsPvrstInstBridgeHelloTime)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);

        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsPvrstInstBridgeHelloTime) !=
         AST_OK)
        || (i4TestValFsPvrstInstBridgeHelloTime <
            PVRST_MIN_HELLOTIME_VAL)
        || (i4TestValFsPvrstInstBridgeHelloTime > PVRST_MAX_HELLOTIME_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_BRG_HELLOTIME_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValFsPvrstInstBridgeHelloTime),
         pPerStPvrstBridgeInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                testValFsPvrstInstBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                        INT4 i4FsPvrstInstVlanId,
                                        INT4
                                        i4TestValFsPvrstInstBridgeForwardDelay)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);

        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsPvrstInstBridgeForwardDelay) !=
         AST_OK)
        || (i4TestValFsPvrstInstBridgeForwardDelay <
            AST_MIN_FWDDELAY_VAL)
        || (i4TestValFsPvrstInstBridgeForwardDelay > AST_MAX_FWDDELAY_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_BRG_FWDDELAY_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValFsPvrstInstBridgeForwardDelay),
         pPerStPvrstBridgeInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstTxHoldCount
 Input       :  The Indices
                FsPvrstInstVlanId

                The Object 
                testValFsPvrstInstTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstTxHoldCount (UINT4 *pu4ErrorCode, INT4 i4FsPvrstInstVlanId,
                                 INT4 i4TestValFsPvrstInstTxHoldCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstTxHoldCount < AST_TXHOLDCOUNT_MIN_VAL) ||
        (i4TestValFsPvrstInstTxHoldCount > AST_TXHOLDCOUNT_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstFlushIndicationThreshold
 Input       :  The Indices
               i4FsMIPvrstInstVlanId

                The Object
                testValFsMIPvrstInstFlushIndicationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsPvrstInstFlushIndicationThreshold
    (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstInstVlanId,
     INT4 i4TestValFsMIPvrstInstFlushIndicationThreshold)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsMIPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMIPvrstInstFlushIndicationThreshold < AST_MIN_FLUSH_IND) ||
        (i4TestValFsMIPvrstInstFlushIndicationThreshold > AST_MAX_FLUSH_IND))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPvrstInstBridgeTable
 Input       :  The Indices
                FsPvrstInstVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstInstBridgeTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPvrstInstPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPvrstInstPortTable
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPvrstInstPortTable (INT4 i4FsPvrstInstVlanId,
                                              INT4 i4FsPvrstInstPortIndex)
{
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((i4FsPvrstInstPortIndex < AST_MIN_NUM_PORTS) ||
        (i4FsPvrstInstPortIndex > AST_MAX_NUM_PORTS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPvrstInstPortTable
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPvrstInstPortTable (INT4 *pi4FsPvrstInstVlanId,
                                      INT4 *pi4FsPvrstInstPortIndex)
{
    return nmhGetNextIndexFsPvrstInstPortTable (0, pi4FsPvrstInstVlanId, 0,
                                                pi4FsPvrstInstPortIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPvrstInstPortTable
 Input       :  The Indices
                FsPvrstInstVlanId
                nextFsPvrstInstVlanId
                FsPvrstInstPortIndex
                nextFsPvrstInstPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPvrstInstPortTable (INT4 i4FsPvrstInstVlanId,
                                     INT4 *pi4NextFsPvrstInstVlanId,
                                     INT4 i4FsPvrstInstPortIndex,
                                     INT4 *pi4NextFsPvrstInstPortIndex)
{
    UINT1               u1InstFlag = PVRST_FALSE;
    UINT1               u1PortFlag = PVRST_FALSE;

    if ((i4FsPvrstInstVlanId < 0) || (i4FsPvrstInstPortIndex < 0))
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (i4FsPvrstInstVlanId == 0)
    {
        i4FsPvrstInstVlanId++;
    }
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) == PVRST_SUCCESS)
    {
        u1InstFlag = PVRST_TRUE;

        if (PvrstGetNextIndexFsMIPvrstInstPort (i4FsPvrstInstVlanId,
                                                (UINT2)
                                                i4FsPvrstInstPortIndex,
                                                pi4NextFsPvrstInstPortIndex)
            == PVRST_SUCCESS)
        {
            u1PortFlag = PVRST_TRUE;
        }

        if ((u1InstFlag == PVRST_TRUE) && (u1PortFlag == PVRST_TRUE))
        {
            *pi4NextFsPvrstInstVlanId = i4FsPvrstInstVlanId;
            return SNMP_SUCCESS;
        }
        else
        {
            u1InstFlag = PVRST_FALSE;
            u1PortFlag = PVRST_FALSE;
            while (PvrstGetNextIndexPvrstInstBridgeTable (i4FsPvrstInstVlanId,
                                                          pi4NextFsPvrstInstVlanId)
                   == PVRST_SUCCESS)
            {
                u1InstFlag = PVRST_TRUE;
                i4FsPvrstInstPortIndex = AST_INIT_VAL;

                if (PvrstGetNextIndexFsPvrstInstPort (*pi4NextFsPvrstInstVlanId,
                                                      (UINT2)
                                                      i4FsPvrstInstPortIndex,
                                                      pi4NextFsPvrstInstPortIndex)
                    == PVRST_SUCCESS)
                {
                    u1PortFlag = PVRST_TRUE;
                }
                else
                {
                    i4FsPvrstInstVlanId = *pi4NextFsPvrstInstVlanId;
                }
                if ((u1PortFlag == PVRST_TRUE) && (u1InstFlag == PVRST_TRUE))
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        if (PvrstGetNextIndexPvrstInstBridgeTable (i4FsPvrstInstVlanId,
                                                   pi4NextFsPvrstInstVlanId) ==
            PVRST_SUCCESS)
        {
            u1InstFlag = PVRST_TRUE;

            if (i4FsPvrstInstPortIndex == AST_INIT_VAL)
            {
                i4FsPvrstInstPortIndex++;
            }
            if (PvrstValidatePortEntry
                (i4FsPvrstInstPortIndex,
                 *pi4NextFsPvrstInstVlanId) == PVRST_SUCCESS)
            {
                *pi4NextFsPvrstInstPortIndex = i4FsPvrstInstPortIndex;
                u1PortFlag = PVRST_TRUE;
            }
            else
            {
                if (PvrstGetNextIndexFsPvrstInstPort (*pi4NextFsPvrstInstVlanId,
                                                      (UINT2)
                                                      i4FsPvrstInstPortIndex,
                                                      pi4NextFsPvrstInstPortIndex)
                    == PVRST_SUCCESS)
                {
                    u1PortFlag = PVRST_TRUE;
                }
            }
            if ((u1InstFlag == PVRST_TRUE) && (u1PortFlag == PVRST_TRUE))
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortEnableStatus
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortEnableStatus (INT4 i4FsPvrstInstVlanId,
                                   INT4 i4FsPvrstInstPortIndex,
                                   INT4 *pi4RetValFsPvrstInstPortEnableStatus)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortEnableStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                (INT4) i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsPvrstInstPortIndex);
    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pAstPortEntry->CommPortInfo.bPortPvrstStatus == PVRST_FALSE)
    {
        *pi4RetValFsPvrstInstPortEnableStatus = AST_SNMP_FALSE;
    }
    else
    {
        if ((pPerStRstPortInfo != NULL)
            && (pPerStRstPortInfo->bPortEnabled == PVRST_TRUE))
        {
            *pi4RetValFsPvrstInstPortEnableStatus = AST_SNMP_TRUE;
        }
        else
        {
            *pi4RetValFsPvrstInstPortEnableStatus = AST_SNMP_FALSE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortPathCost (INT4 i4FsPvrstInstVlanId,
                               INT4 i4FsPvrstInstPortIndex,
                               INT4 *pi4RetValFsPvrstInstPortPathCost)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortPathCost = pPerStPvrstRstPortInfo->u4PathCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortPriority
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortPriority (INT4 i4FsPvrstInstVlanId,
                               INT4 i4FsPvrstInstPortIndex,
                               INT4 *pi4RetValFsPvrstInstPortPriority)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1               u1Priority = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortPriority = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        PVRST_GET_PORT_PRIORITY (pAstPerStPortInfo->u1PortPriority, u1Priority);
    }
    *pi4RetValFsPvrstInstPortPriority = u1Priority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortDesignatedRoot
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortDesignatedRoot (INT4 i4FsPvrstInstVlanId,
                                     INT4 i4FsPvrstInstPortIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsPvrstInstPortDesignatedRoot)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        pu1List = pRetValFsPvrstInstPortDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsPvrstInstPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;

    }
    /* No Get/Walk under Stp-Compatible mode 
     * for objects not applicable in this mode*/

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        pRetValFsPvrstInstPortDesignatedRoot->pu1_OctetList[0] = (UINT1)
            ((pAstPerStPortInfo->RootId.u2BrgPriority & 0xff00) >> 8);

        pRetValFsPvrstInstPortDesignatedRoot->pu1_OctetList[1] =
            (UINT1) (pAstPerStPortInfo->RootId.u2BrgPriority & 0x00ff);

        AST_MEMCPY (pRetValFsPvrstInstPortDesignatedRoot->pu1_OctetList +
                    PVRST_INCR_LENGTH_BY_TWO,
                    (&pAstPerStPortInfo->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);
    }
    pRetValFsPvrstInstPortDesignatedRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortDesignatedBridge
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortDesignatedBridge (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsPvrstInstPortDesignatedBridge)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        pu1List = pRetValFsPvrstInstPortDesignatedBridge->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsPvrstInstPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        pRetValFsPvrstInstPortDesignatedBridge->pu1_OctetList[0] = (UINT1)
            ((pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0xff00) >> 8);

        pRetValFsPvrstInstPortDesignatedBridge->pu1_OctetList[1] =
            (UINT1) (pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0x00ff);

        AST_MEMCPY (pRetValFsPvrstInstPortDesignatedBridge->pu1_OctetList +
                    PVRST_INCR_LENGTH_BY_TWO,
                    (&pAstPerStPortInfo->DesgBrgId.BridgeAddr),
                    AST_MAC_ADDR_SIZE);
    }
    pRetValFsPvrstInstPortDesignatedBridge->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortDesignatedPort
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortDesignatedPort (INT4 i4FsPvrstInstVlanId,
                                     INT4 i4FsPvrstInstPortIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsPvrstInstPortDesignatedPort)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        pu1List = pRetValFsPvrstInstPortDesignatedPort->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);

        pRetValFsPvrstInstPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    pu1List = pRetValFsPvrstInstPortDesignatedPort->pu1_OctetList;
    if (pAstPerStPortInfo != NULL)
    {
        u2Val = pAstPerStPortInfo->u2DesgPortId;
    }
    AST_PUT_2BYTE (pu1List, u2Val);

    pu1List -= 2;
    pRetValFsPvrstInstPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortOperVersion
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortOperVersion (INT4 i4FsPvrstInstVlanId,
                                  INT4 i4FsPvrstInstPortIndex,
                                  INT4 *pi4RetValFsPvrstInstPortOperVersion)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortOperVersion = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    /* Check to avoid showing port operational version as RSTP when the
     * bridge level configuration (StpVersion) is STPCompatible.
     * This happens when the port is operationally down and StpVersion 
     * is set to STPCompatible.
     */
    /* Return operational version as STP compatible when 
     * the global configuration is STPCompatible.
     */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        *pi4RetValFsPvrstInstPortOperVersion = AST_STP_COMPATIBLE_MODE;
        return (INT1) SNMP_SUCCESS;
    }
    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE)
    {
        *pi4RetValFsPvrstInstPortOperVersion = AST_STP_COMPATIBLE_MODE;
    }
    else
    {
        if (AST_FORCE_VERSION == AST_VERSION_2)
        {
            *pi4RetValFsPvrstInstPortOperVersion = AST_RST_MODE;
        }
        else
        {
            *pi4RetValFsPvrstInstPortOperVersion = AST_MST_MODE;
        }
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortProtocolMigration (INT4 i4FsPvrstInstVlanId,
                                        INT4 i4FsPvrstInstPortIndex,
                                        INT4
                                        *pi4RetValFsPvrstInstPortProtocolMigration)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortProtocolMigration = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo->bMCheck == PVRST_TRUE)
    {
        *pi4RetValFsPvrstInstPortProtocolMigration = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstInstPortProtocolMigration = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortState (INT4 i4FsPvrstInstVlanId,
                            INT4 i4FsPvrstInstPortIndex,
                            INT4 *pi4RetValFsPvrstInstPortState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST  Module not Started!\n");
        *pi4RetValFsPvrstInstPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (i4FsPvrstInstPortIndex))
        != AST_CUSTOMER_EDGE_PORT)
    {
        *pi4RetValFsPvrstInstPortState =
            AstGetInstPortStateFromL2Iwf (u2InstIndex,
                                          (UINT2) i4FsPvrstInstPortIndex);
    }
    else
    {
        *pi4RetValFsPvrstInstPortState = AST_PORT_STATE_DISCARDING;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortForwardTransitions
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortForwardTransitions (INT4 i4FsPvrstInstVlanId,
                                         INT4 i4FsPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsPvrstInstPortForwardTransitions)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortForwardTransitions = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pu4RetValFsPvrstInstPortForwardTransitions =
            pAstPerStPortInfo->u4NumFwdTransitions;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortReceivedBpdus
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortReceivedBpdus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortReceivedBpdus (INT4 i4FsPvrstInstVlanId,
                                    INT4 i4FsPvrstInstPortIndex,
                                    UINT4
                                    *pu4RetValFsPvrstInstPortReceivedBpdus)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortReceivedBpdus = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortReceivedBpdus = pPerStPvrstRstPortInfo->
        u4NumRstBpdusRxd;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortRxConfigBpduCount
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortRxConfigBpduCount (INT4 i4FsPvrstInstVlanId,
                                        INT4 i4FsPvrstInstPortIndex,
                                        UINT4
                                        *pu4RetValFsPvrstInstPortRxConfigBpduCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortRxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortRxConfigBpduCount =
        pPerStPvrstRstPortInfo->u4NumConfigBpdusRxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortRxTcnBpduCount
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortRxTcnBpduCount (INT4 i4FsPvrstInstVlanId,
                                     INT4 i4FsPvrstInstPortIndex,
                                     UINT4
                                     *pu4RetValFsPvrstInstPortRxTcnBpduCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortRxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortRxTcnBpduCount =
        pPerStPvrstRstPortInfo->u4NumTcnBpdusRxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortTransmittedBpdus
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortTransmittedBpdus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortTransmittedBpdus (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsPvrstInstPortTransmittedBpdus)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortTransmittedBpdus = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    /* No Get/Walk under Stp-Compatible mode */
    /* for objects not applicable in this mode */

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortTransmittedBpdus =
        pPerStPvrstRstPortInfo->u4NumRstBpdusTxd;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortTxConfigBpduCount
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortTxConfigBpduCount (INT4 i4FsPvrstInstVlanId,
                                        INT4 i4FsPvrstInstPortIndex,
                                        UINT4
                                        *pu4RetValFsPvrstInstPortTxConfigBpduCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortTxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortTxConfigBpduCount =
        pPerStPvrstRstPortInfo->u4NumConfigBpdusTxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortTxTcnBpduCount
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortTxTcnBpduCount (INT4 i4FsPvrstInstVlanId,
                                     INT4 i4FsPvrstInstPortIndex,
                                     UINT4
                                     *pu4RetValFsPvrstInstPortTxTcnBpduCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstPortTxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstPortTxTcnBpduCount =
        pPerStPvrstRstPortInfo->u4NumTcnBpdusTxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortTxSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortTxSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortTxSemState (INT4 i4FsPvrstInstVlanId,
                                 INT4 i4FsPvrstInstPortIndex,
                                 INT4 *pi4RetValFsPvrstInstPortTxSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortTxSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortTxSemState =
        pPerStPvrstRstPortInfo->u1PortTxSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortProtMigrationSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortProtMigrationSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortProtMigrationSemState (INT4 i4FsPvrstInstVlanId,
                                            INT4 i4FsPvrstInstPortIndex,
                                            INT4
                                            *pi4RetValFsPvrstInstPortProtMigrationSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortProtMigrationSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortProtMigrationSemState =
        pPerStPvrstRstPortInfo->u1PmigSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstProtocolMigrationCount
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstProtocolMigrationCount (INT4 i4FsPvrstInstVlanId,
                                         INT4 i4FsPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsPvrstInstProtocolMigrationCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstInstProtocolMigrationCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pu4RetValFsPvrstInstProtocolMigrationCount =
        pPerStPvrstRstPortInfo->u4ProtocolMigrationCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortRole
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortRole (INT4 i4FsPvrstInstVlanId,
                           INT4 i4FsPvrstInstPortIndex,
                           INT4 *pi4RetValFsPvrstInstPortRole)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST  Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        *pi4RetValFsPvrstInstPortRole = AST_PORT_ROLE_DISABLED;
    }
    else if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstPortRole = pAstPerStPortInfo->u1PortRole;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstCurrentPortRole
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstCurrentPortRole (INT4 i4FsPvrstInstVlanId,
                                  INT4 i4FsPvrstInstPortIndex,
                                  INT4 *pi4RetValFsPvrstInstCurrentPortRole)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstCurrentPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstCurrentPortRole = pAstPerStPortInfo->u1PortRole;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortInfoSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortInfoSemState (INT4 i4FsPvrstInstVlanId,
                                   INT4 i4FsPvrstInstPortIndex,
                                   INT4 *pi4RetValFsPvrstInstPortInfoSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortInfoSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstPortInfoSemState =
            pAstPerStPortInfo->u1PinfoSmState;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortRoleTransitionSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortRoleTransitionSemState (INT4 i4FsPvrstInstVlanId,
                                             INT4 i4FsPvrstInstPortIndex,
                                             INT4
                                             *pi4RetValFsPvrstInstPortRoleTransitionSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST  Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortRoleTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex, i4FsPvrstInstVlanId) !=
        PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstPortRoleTransitionSemState =
            pAstPerStPortInfo->u1ProleTrSmState;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortStateTransitionSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortStateTransitionSemState (INT4 i4FsPvrstInstVlanId,
                                              INT4 i4FsPvrstInstPortIndex,
                                              INT4
                                              *pi4RetValFsPvrstInstPortStateTransitionSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortStateTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstPortStateTransitionSemState =
            pAstPerStPortInfo->u1PstateTrSmState;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortTopologyChangeSemState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortTopologyChangeSemState (INT4 i4FsPvrstInstVlanId,
                                             INT4 i4FsPvrstInstPortIndex,
                                             INT4
                                             *pi4RetValFsPvrstInstPortTopologyChangeSemState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortTopologyChangeSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);
    if (pAstPerStPortInfo != NULL)
    {
        *pi4RetValFsPvrstInstPortTopologyChangeSemState =
            pAstPerStPortInfo->u1TopoChSmState;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortEffectivePortState
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortEffectivePortState (INT4 i4FsPvrstInstVlanId,
                                         INT4 i4FsPvrstInstPortIndex,
                                         INT4
                                         *pi4RetValFsPvrstInstPortEffectivePortState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortInfo = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstInstPortEffectivePortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstInstPortIndex);

    pAstPerStRstPortInfo =
        PVRST_GET_PERST_RST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (((pAstPortEntry->u1EntryStatus) == AST_PORT_OPER_UP)
        && (pAstPerStRstPortInfo->bPortEnabled) == PVRST_TRUE)

    {
        *pi4RetValFsPvrstInstPortEffectivePortState = PVRST_PORT_OPER_ENABLED;
    }
    else
    {
        *pi4RetValFsPvrstInstPortEffectivePortState = PVRST_PORT_OPER_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortHelloTime
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortHelloTime (INT4 i4FsPvrstInstVlanId,
                                INT4 i4FsPvrstInstPortIndex,
                                INT4 *pi4RetValFsPvrstInstPortHelloTime)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortHelloTime = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortHelloTime = AST_SYS_TO_MGMT
        (pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortMaxAge
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortMaxAge (INT4 i4FsPvrstInstVlanId,
                             INT4 i4FsPvrstInstPortIndex,
                             INT4 *pi4RetValFsPvrstInstPortMaxAge)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortMaxAge = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortMaxAge = AST_SYS_TO_MGMT
        (pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortForwardDelay
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortForwardDelay (INT4 i4FsPvrstInstVlanId,
                                   INT4 i4FsPvrstInstPortIndex,
                                   INT4 *pi4RetValFsPvrstInstPortForwardDelay)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortForwardDelay = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (i4FsPvrstInstPortIndex,
                                             u2InstIndex);

    *pi4RetValFsPvrstInstPortForwardDelay = AST_SYS_TO_MGMT
        (pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortHoldTime
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortHoldTime (INT4 i4FsPvrstInstVlanId,
                               INT4 i4FsPvrstInstPortIndex,
                               INT4 *pi4RetValFsPvrstInstPortHoldTime)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortHoldTime = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstInstPortHoldTime = AST_SYS_TO_MGMT (AST_HOLD_TIME);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                retValFsPvrstInstPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstInstPortAdminPathCost (INT4 i4FsPvrstInstVlanId,
                                    INT4 i4FsPvrstInstPortIndex,
                                    INT4 *pi4RetValFsPvrstInstPortAdminPathCost)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortAdminPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstInstPortAdminPathCost =
        pPerStPortInfo->u4PortAdminPathCost;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstInstPortEnableStatus
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                setValFsPvrstInstPortEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstPortEnableStatus (INT4 i4FsPvrstInstVlanId,
                                   INT4 i4FsPvrstInstPortIndex,
                                   INT4 i4SetValFsPvrstInstPortEnableStatus)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstInstPortIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN);
         */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PVRST_CONFIG_INST_STATUS_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;
    if (i4SetValFsPvrstInstPortEnableStatus == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bStatus = PVRST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bStatus = PVRST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstPortEnableStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      i4FsPvrstInstVlanId,
                      AST_GET_IFINDEX (i4FsPvrstInstPortIndex),
                      i4SetValFsPvrstInstPortEnableStatus));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstPortPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                setValFsPvrstInstPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstPortPathCost (INT4 i4FsPvrstInstVlanId,
                               INT4 i4FsPvrstInstPortIndex,
                               INT4 i4SetValFsPvrstInstPortPathCost)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN);
         */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PATH_COST_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
    pAstMsgNode->uMsg.u4PathCost = i4SetValFsPvrstInstPortPathCost;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstPortPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      i4FsPvrstInstVlanId,
                      AST_GET_IFINDEX (i4FsPvrstInstPortIndex),
                      i4SetValFsPvrstInstPortPathCost));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstPortPriority
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                setValFsPvrstInstPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstPortPriority (INT4 i4FsPvrstInstVlanId,
                               INT4 i4FsPvrstInstPortIndex,
                               INT4 i4SetValFsPvrstInstPortPriority)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); 
         */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PRIORITY_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
    pAstMsgNode->uMsg.u1PortPriority = (UINT1) i4SetValFsPvrstInstPortPriority;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstPortPriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      i4FsPvrstInstVlanId,
                      AST_GET_IFINDEX (i4FsPvrstInstPortIndex),
                      i4SetValFsPvrstInstPortPriority));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                setValFsPvrstInstPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstPortProtocolMigration (INT4 i4FsPvrstInstVlanId,
                                        INT4 i4FsPvrstInstPortIndex,
                                        INT4
                                        i4SetValFsPvrstInstPortProtocolMigration)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); 
         */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PROTOCOL_MIGRATION_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstPortProtocolMigration,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      i4FsPvrstInstVlanId,
                      AST_GET_IFINDEX (i4FsPvrstInstPortIndex),
                      i4SetValFsPvrstInstPortProtocolMigration));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                setValFsPvrstInstPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstInstPortAdminPathCost (INT4 i4FsPvrstInstVlanId,
                                    INT4 i4FsPvrstInstPortIndex,
                                    INT4 i4SetValFsPvrstInstPortAdminPathCost)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /*AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN);
         */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW:Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsPvrstInstPortAdminPathCost == 0)
    {
        pAstMsgNode->MsgType = AST_ZERO_PATHCOST_MSG;
        pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
        pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;
    }
    else
    {

        pAstMsgNode->MsgType = AST_PATH_COST_MSG;
        pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstInstPortIndex;
        pAstMsgNode->uMsg.u4PathCost = i4SetValFsPvrstInstPortAdminPathCost;
        pAstMsgNode->u2InstanceId = (UINT2) i4FsPvrstInstVlanId;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstInstPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      i4FsPvrstInstVlanId,
                      AST_GET_IFINDEX (i4FsPvrstInstPortIndex),
                      i4SetValFsPvrstInstPortAdminPathCost));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstPortEnableStatus
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                testValFsPvrstInstPortEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstPortEnableStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPvrstInstVlanId,
                                      INT4 i4FsPvrstInstPortIndex,
                                      INT4 i4TestValFsPvrstInstPortEnableStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_ACCESS_PORT_PVID_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (L2IwfMiIsVlanMemberPort (AST_DEFAULT_CONTEXT,
                                 (tVlanId) i4FsPvrstInstVlanId,
                                 AST_GET_IFINDEX (i4FsPvrstInstPortIndex))
        == OSIX_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Port is not member of the VLAN!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_PORT_NOT_VLAN_MEMBER_ERR);
        return (INT1) SNMP_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO ((UINT2) i4FsPvrstInstPortIndex);

    if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstPortEnableStatus != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstInstPortEnableStatus != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstPortPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                testValFsPvrstInstPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstPortPathCost (UINT4 *pu4ErrorCode, INT4 i4FsPvrstInstVlanId,
                                  INT4 i4FsPvrstInstPortIndex,
                                  INT4 i4TestValFsPvrstInstPortPathCost)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstPortPathCost < PVRST_MIN_PORT_PATH_COST) ||
        (i4TestValFsPvrstInstPortPathCost > PVRST_MAX_PORT_PATH_COST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstPortPriority
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                testValFsPvrstInstPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsPvrstInstVlanId,
                                  INT4 i4FsPvrstInstPortIndex,
                                  INT4 i4TestValFsPvrstInstPortPriority)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstPortPriority < PVRST_MIN_PORT_PRIORITY) ||
        (i4TestValFsPvrstInstPortPriority > PVRST_MAX_PORT_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsPvrstInstPortPriority & AST_PORTPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_PORT_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                testValFsPvrstInstPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstPortProtocolMigration (UINT4 *pu4ErrorCode,
                                           INT4 i4FsPvrstInstVlanId,
                                           INT4 i4FsPvrstInstPortIndex,
                                           INT4
                                           i4TestValFsPvrstInstPortProtocolMigration)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstPortProtocolMigration != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstInstPortProtocolMigration != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex

                The Object 
                testValFsPvrstInstPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstInstPortAdminPathCost (UINT4 *pu4ErrorCode,
                                       INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       INT4
                                       i4TestValFsPvrstInstPortAdminPathCost)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_MODE_ERR);
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
        return SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstInstPortAdminPathCost < 0) ||
        (i4TestValFsPvrstInstPortAdminPathCost > PVRST_MAX_PORT_PATH_COST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPvrstInstPortTable
 Input       :  The Indices
                FsPvrstInstVlanId
                FsPvrstInstPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstInstPortTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                retValFsPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstCalcPortPathCostOnSpeedChg (INT4
                                         *pi4RetValFsPvrstCalcPortPathCostOnSpeedChg)
{
    *pi4RetValFsPvrstCalcPortPathCostOnSpeedChg = AST_SNMP_FALSE;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        return SNMP_SUCCESS;
    }

    *pi4RetValFsPvrstCalcPortPathCostOnSpeedChg =
        (AST_GET_BRGENTRY ())->u1DynamicPathcostCalcLagg;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstGlobalBpduGuard
 Input       :  The Indices

                The Object
                retValFsPvrstGlobalBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstGlobalBpduGuard (INT4 *pi4RetValFsPvrstGlobalBpduGuard)
{
    if (AST_IS_PVRST_STARTED ())
    {
        *pi4RetValFsPvrstGlobalBpduGuard = (INT4) AST_GBL_BPDUGUARD_STATUS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstForceProtocolVersion
 Input       :  The Indices

                The Object 
                retValFsPvrstForceProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstForceProtocolVersion (INT4 *pi4RetValFsPvrstForceProtocolVersion)
{
    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "PVRST Module not Started!\n");
        *pi4RetValFsPvrstForceProtocolVersion = AST_VERSION_2;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPvrstForceProtocolVersion = (INT4) AST_FORCE_VERSION;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstFlushInterval
 Input       :  The Indices

                The Object
                retValFsPvrstFlushInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsPvrstFlushInterval (INT4 *pi4RetValFsPvrstFlushInterval)
{
    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "PVRST Module not Started!\n");
        *pi4RetValFsPvrstFlushInterval = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsPvrstFlushInterval = (INT4) AST_FLUSH_INTERVAL;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                setValFsPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstCalcPortPathCostOnSpeedChg (INT4
                                         i4SetValFsPvrstCalcPortPathCostOnSpeedChg)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ENABLE_PORT_SPEED_CHG_MSG;

    pAstMsgNode->uMsg.u1DynamicPathcostCalculation =
        (UINT1) i4SetValFsPvrstCalcPortPathCostOnSpeedChg;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstCalcPortPathCostOnSpeedChg,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstCalcPortPathCostOnSpeedChg));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstGlobalBpduGuard
 Input       :  The Indices

                The Object
                setValFsPvrstGlobalBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstGlobalBpduGuard (INT4 i4SetValFsPvrstGlobalBpduGuard)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsPvrstGlobalBpduGuard == AST_BPDUGUARD_ENABLE)
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_ENABLE_MSG;
    }
    else
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_DISABLE_MSG;
    }
    pNode->uMsg.u4BpduGuardStatus = i4SetValFsPvrstGlobalBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstGlobalBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstGlobalBpduGuard));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstForceProtocolVersion
 Input       :  The Indices

                The Object 
                setValFsPvrstForceProtocolVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstForceProtocolVersion (INT4 i4SetValFsPvrstForceProtocolVersion)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT4                i4CurrContextId = AST_INIT_VAL;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2PortNum = AST_INIT_VAL;
    tVlanId             VlanId = AST_DEF_VLAN_ID ();
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;

    AST_MEMSET (&SnmpNotifyInfo, AST_INIT_VAL, sizeof (tSnmpNotifyInfo));
    i4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_FORCE_VERSION_MSG;
    pNode->uMsg.u1ForceVersion = (UINT1) i4SetValFsPvrstForceProtocolVersion;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstForceProtocolVersion,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValFsPvrstForceProtocolVersion));
    AstSelectContext ((UINT4) i4CurrContextId);

    /* When force protocol version is set to STP, add all the ports to the default instance and
     * stop all the timers for ports in non default instances.*/
    /* if force version is not STP, Remove all the non member ports from default instance. */
    if (i4SetValFsPvrstForceProtocolVersion == AST_VERSION_0)
    {
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {

            AstL2IwfGetPortOperStatus (STP_MODULE, u2PortNum, &u1OperStatus);
            if (u1OperStatus == AST_UP)
            {

                if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
                {
                    continue;
                }
                /*stopping the timers for ports */
                PvrstForceVersionStopAllRunningTimers (u2PortNum);
                /* creating all the ports in default instance */
                if (PvrstPerStPortCreate (u2PortNum, VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "SYS: Unable to create port = %d"
                                  "for instance = %d !!!\n", u2PortNum, VlanId);
                    AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Unable to create port = "
                                  "%d for instance = %d !!!\n", u2PortNum,
                                  VlanId);
                    return PVRST_FAILURE;
                }
                PvrstPerStEnablePort (u2PortNum, VlanId, AST_STP_PORT_UP);
            }
        }
    }
    else if ((i4SetValFsPvrstForceProtocolVersion == AST_VERSION_2)
             && (AST_FORCE_VERSION == AST_VERSION_0))
    {
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {

            /*removing all the ports from the default instance */
            if (L2IwfMiIsVlanMemberPort
                (AST_CURR_CONTEXT_ID (), VlanId, u2PortNum) == OSIX_FALSE)
            {
                if (PvrstPerStPortDelete (u2PortNum, VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "DeletePort: Port %s Inst %d: PvrstPerStPortDelete"
                                  "returned failure !\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstFlushInterval
 Input       :  The Indices

                The Object
                setValFsMIPvrstFlushInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstFlushInterval (INT4 i4SetValFsMIPvrstFlushInterval)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4SetValFsMIPvrstFlushInterval == (INT4) AST_FLUSH_INTERVAL)
    {
        return SNMP_SUCCESS;
    }

    AST_FLUSH_INTERVAL = (UINT4) i4SetValFsMIPvrstFlushInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstFlushInterval,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIPvrstFlushInterval));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                testValFsPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsPvrstCalcPortPathCostOnSpeedChg)
{
    if ((i4TestValFsPvrstCalcPortPathCostOnSpeedChg != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstCalcPortPathCostOnSpeedChg != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstGlobalBpduGuard
 Input       :  The Indices

                The Object
                testValFsPvrstGlobalBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstGlobalBpduGuard (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsPvrstGlobalBpduGuard)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsPvrstGlobalBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsPvrstGlobalBpduGuard != AST_BPDUGUARD_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstForceProtocolVersion
 Input       :  The Indices

                The Object 
                testValFsPvrstForceProtocolVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsPvrstForceProtocolVersion)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: PVRST module not started. Cannot set Stp Version\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((AST_VERSION_0 == i4TestValFsPvrstForceProtocolVersion) ||
        (AST_VERSION_2 == i4TestValFsPvrstForceProtocolVersion))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
Function    :  nmhTestv2FsPvrstFlushInterval
Input       :  The Indices

The Object
testValFsPvrstFlushInterval
Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhTestv2FsPvrstFlushInterval (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsPvrstFlushInterval)
{

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstFlushInterval < AST_FLUSH_INTERVAL_MIN_VAL) ||
        (i4TestValFsPvrstFlushInterval > AST_FLUSH_INTERVAL_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstGlobalBpduGuard
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstGlobalBpduGuard (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPvrstForceProtocolVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstSetTraps
 Input       :  The Indices

                The Object 
                retValFsPvrstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstSetTraps (INT4 *pi4RetValFsPvrstSetTraps)
{
    *pi4RetValFsPvrstSetTraps = AST_TRAP_TYPE;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstGenTrapType
 Input       :  The Indices

                The Object 
                retValFsPvrstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstGenTrapType (INT4 *pi4RetValFsPvrstGenTrapType)
{
    *pi4RetValFsPvrstGenTrapType = AST_GEN_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstErrTrapType
 Input       :  The Indices

                The Object 
                retValFsPvrstErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstErrTrapType (INT4 *pi4RetValFsPvrstErrTrapType)
{
    *pi4RetValFsPvrstErrTrapType = AST_ERR_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPvrstSetTraps
 Input       :  The Indices

                The Object 
                setValFsPvrstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstSetTraps (INT4 i4SetValFsPvrstSetTraps)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_TRAP_TYPE = (UINT4) i4SetValFsPvrstSetTraps;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstSetTraps,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsPvrstSetTraps));
    AstSelectContext (u4CurrContextId);

    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPvrstSetTraps
 Input       :  The Indices

                The Object 
                testValFsPvrstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstSetTraps (UINT4 *pu4ErrorCode, INT4 i4TestValFsPvrstSetTraps)
{
    if ((i4TestValFsPvrstSetTraps < 0) ||
        (i4TestValFsPvrstSetTraps > AST_TRAP_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPvrstSetTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPvrstSetTraps (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

 /****************************************************************************
  Function    :  nmhDepv2FsPvrstFlushInterval
  Output      :  The Dependency Low Lev Routine Take the Indices &
                 check whether dependency is met or not.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPvrstFlushInterval (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPvrstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPvrstPortTrapNotificationTable (INT4
                                                          i4FsPvrstPortTrapIndex)
{
    if (AstSnmpLowValidatePortIndex (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPvrstPortTrapNotificationTable (INT4 *pi4FsPvrstPortTrapIndex)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
                nextFsPvrstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPvrstPortTrapNotificationTable (INT4 i4FsPvrstPortTrapIndex,
                                                 INT4
                                                 *pi4NextFsPvrstPortTrapIndex)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (i4FsPvrstPortTrapIndex < 0)
    {
        return (INT1) SNMP_FAILURE;
    }
    if (AstSnmpLowGetNextValidIndex
        (i4FsPvrstPortTrapIndex, pi4NextFsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstPortMigrationType
 Input       :  The Indices
                FsPvrstPortTrapIndex

                The Object 
                retValFsPvrstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortMigrationType (INT4 i4FsPvrstPortTrapIndex,
                                INT4 *pi4RetValFsPvrstPortMigrationType)
{
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: pvrst Module not Enabled!\n");
        *pi4RetValFsPvrstPortMigrationType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsPvrstPortMigrationType =
        (AST_GET_PORTENTRY (i4FsPvrstPortTrapIndex))->u1MigrationType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPktErrType
 Input       :  The Indices
                FsPvrstPortTrapIndex

                The Object 
                retValFsPvrstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPktErrType (INT4 i4FsPvrstPortTrapIndex,
                         INT4 *pi4RetValFsPvrstPktErrType)
{
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstPktErrType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsPvrstPktErrType = (INT4) (AST_GET_PORTENTRY
                                          (i4FsPvrstPortTrapIndex))->
        u1PktErrType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPktErrVal
 Input       :  The Indices
                FsPvrstPortTrapIndex

                The Object 
                retValFsPvrstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPktErrVal (INT4 i4FsPvrstPortTrapIndex,
                        INT4 *pi4RetValFsPvrstPktErrVal)
{
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstPktErrVal = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsPvrstPktErrVal = (INT4) (AST_GET_PORTENTRY
                                         (i4FsPvrstPortTrapIndex))->
        u2PktErrValue;
    return (INT1) SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPvrstPortRoleTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
                FsPvrstInstVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPvrstPortRoleTrapNotificationTable (INT4
                                                              i4FsPvrstPortTrapIndex,
                                                              INT4
                                                              i4FsPvrstInstVlanId)
{
    /* This table holds the Port role information necessary to *
     * generate trap during role changes. Here, we directly   *
     * call the corresponding Validate, getfirst and Getnext   *
     * routine for another table, having the same Indices.     */

    return (nmhValidateIndexInstanceFsPvrstInstPortTable
            (i4FsPvrstInstVlanId, i4FsPvrstPortTrapIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
                FsPvrstInstVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPvrstPortRoleTrapNotificationTable (INT4
                                                      *pi4FsPvrstPortTrapIndex,
                                                      INT4
                                                      *pi4FsPvrstInstVlanId)
{
    UINT1               i1RetVal;

    i1RetVal = nmhGetFirstIndexFsPvrstInstPortTable (pi4FsPvrstInstVlanId,
                                                     pi4FsPvrstPortTrapIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsPvrstPortTrapIndex
                nextFsPvrstPortTrapIndex
                FsPvrstInstVlanId
                nextFsPvrstInstVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPvrstPortRoleTrapNotificationTable (INT4
                                                     i4FsPvrstPortTrapIndex,
                                                     INT4
                                                     *pi4NextFsPvrstPortTrapIndex,
                                                     INT4 i4FsPvrstInstVlanId,
                                                     INT4
                                                     *pi4NextFsPvrstInstVlanId)
{
    UINT1               i1RetVal;

    i1RetVal = nmhGetNextIndexFsPvrstInstPortTable (i4FsPvrstInstVlanId,
                                                    pi4NextFsPvrstInstVlanId,
                                                    i4FsPvrstPortTrapIndex,
                                                    pi4NextFsPvrstPortTrapIndex);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPvrstPortRoleType
 Input       :  The Indices
                FsPvrstPortTrapIndex
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstPortRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortRoleType (INT4 i4FsPvrstPortTrapIndex,
                           INT4 i4FsPvrstInstVlanId,
                           INT4 *pi4RetValFsPvrstPortRoleType)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstPortRoleType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPvrstInstVlanId < 0) ||
        (i4FsPvrstInstVlanId >= AST_MAX_PVRST_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidatePortEntry (i4FsPvrstPortTrapIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPvrstPortRoleType =
        (INT4) (AST_GET_SELECTED_PORT_ROLE (u2InstIndex,
                                            (UINT2) i4FsPvrstPortTrapIndex));
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstOldRoleType
 Input       :  The Indices
                FsPvrstPortTrapIndex
                FsPvrstInstVlanId

                The Object 
                retValFsPvrstOldRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstOldRoleType (INT4 i4FsPvrstPortTrapIndex, INT4 i4FsPvrstInstVlanId,
                          INT4 *pi4RetValFsPvrstOldRoleType)
{
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "PVRST Module not Enabled!\n");
        *pi4RetValFsPvrstOldRoleType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsPvrstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPvrstInstVlanId < 0) ||
        (i4FsPvrstInstVlanId >= AST_MAX_PVRST_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidatePortEntry (i4FsPvrstPortTrapIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 " Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPvrstOldRoleType =
        (INT4) (AST_GET_PORT_ROLE (u2InstIndex,
                                   (UINT2) i4FsPvrstPortTrapIndex));
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  PvrstNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
PvrstNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsmppv, (sizeof (fsmppv) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsMIPvrstSystemControl,
                      (sizeof (FsMIPvrstSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the pvrst shutdown, with 
     * pvrst oids and its system control object */
    /* As the PVRST support is not provided for multiple contexts, the 
     * context id passed as -1 to the MSR task. */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortLoopGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortLoopGuard (INT4 i4FsPvrstPort,
                            INT4 *pi4RetValFsPvrstPortLoopGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_PORT_LOOP_GUARD (pAstPortEntry) == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortLoopGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortLoopGuard = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortLoopInconsistentState
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortLoopInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortLoopInconsistentState (INT4 i4FsPvrstPort,
                                        INT4
                                        *pi4RetValFsPvrstPortLoopInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tVlanId             PrevVlanId = (tVlanId) AST_INIT_VAL;
    tVlanId             VlanId = (tVlanId) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = AST_INIT_VAL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    AstL2IwfGetVlanPortType ((UINT2) pAstPortEntry->u4IfIndex, &u1PortType);

    if ((u1PortType == VLAN_TRUNK_PORT) || (u1PortType == VLAN_ACCESS_PORT))
    {
        while (L2IwfMiGetNextActiveVlan (AST_CURR_CONTEXT_ID (), PrevVlanId,
                                         &VlanId) == L2IWF_SUCCESS)
        {
            u2InstIndex =
                AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);

            PrevVlanId = VlanId;
            if (u2InstIndex == 0)
            {
                continue;
            }

            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (pPerStPortInfo->bLoopIncStatus == PVRST_FALSE)
            {
                *pi4RetValFsPvrstPortLoopInconsistentState = AST_SNMP_FALSE;
                continue;
            }
            else
            {
                *pi4RetValFsPvrstPortLoopInconsistentState = AST_SNMP_TRUE;
                break;
            }
        }
    }
    else
    {
        if (pAstPortEntry->bLoopInconsistent == PVRST_TRUE)
        {
            *pi4RetValFsPvrstPortLoopInconsistentState = AST_SNMP_TRUE;
        }
        else
        {
            *pi4RetValFsPvrstPortLoopInconsistentState = AST_SNMP_FALSE;
        }
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortEnableBPDURx
 Input       :  The Indices
                FsPvrstPort

                The Object
                retValFsPvrstPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortEnableBPDURx (INT4 i4FsPvrstPort,
                               INT4 *pi4RetValFsPvrstPortEnableBPDURx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (AST_PORT_ENABLE_BPDU_RX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsPvrstPortEnableBPDURx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortEnableBPDURx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsPvrstPort

                The Object
                retValFsPvrstPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortEnableBPDUTx (INT4 i4FsPvrstPort,
                               INT4 *pi4RetValFsPvrstPortEnableBPDUTx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsPvrstPortEnableBPDUTx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortEnableBPDUTx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstBpduFilter
 Input       :  The Indices
                FsPvrstPort

                The Object
                retValFsPvrstBpduFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstBpduFilter (INT4 i4FsPvrstPort, INT4 *pi4RetValFsPvrstBpduFilter)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAstPortEntry->u1EnableBPDUFilter == AST_BPDUFILTER_ENABLE)
    {
        *pi4RetValFsPvrstBpduFilter = AST_BPDUFILTER_ENABLE;
    }
    else
    {
        *pi4RetValFsPvrstBpduFilter = AST_BPDUFILTER_DISABLE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsPvrstPortAutoEdge
*  Input       :  The Indices
*                 FsPvrstPort
* 
*                 The Object
*                 retValFsPvrstPortAutoEdge
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsPvrstPortAutoEdge (INT4 i4FsPvrstPort,
                           INT4 *pi4RetValFsPvrstPortAutoEdge)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bAutoEdge == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortAutoEdge = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortAutoEdge = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortBpduInconsistentState
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortBpduInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortBpduInconsistentState (INT4 i4FsPvrstPort,
                                        INT4
                                        *pi4RetValFsPvrstPortBpduInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (pAstPortEntry->CommPortInfo.bBpduInconsistent == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortBpduInconsistentState = PVRST_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortBpduInconsistentState = PVRST_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortTypeInconsistentState
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortTypeInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortTypeInconsistentState (INT4 i4FsPvrstPort,
                                        INT4
                                        *pi4RetValFsPvrstPortTypeInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (pAstPortEntry->bPTypeInconsistent == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortTypeInconsistentState = PVRST_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortTypeInconsistentState = PVRST_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortPVIDInconsistentState
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortPVIDInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortPVIDInconsistentState (INT4 i4FsPvrstPort,
                                        INT4
                                        *pi4RetValFsPvrstPortPVIDInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (pAstPortEntry->bPVIDInconsistent == PVRST_TRUE)
    {
        *pi4RetValFsPvrstPortPVIDInconsistentState = PVRST_TRUE;
    }
    else
    {
        *pi4RetValFsPvrstPortPVIDInconsistentState = PVRST_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsPvrstPortBpduGuardAction
 Input       :  The Indices
                FsPvrstPort

                The Object 
                retValFsPvrstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortBpduGuardAction (INT4 i4FsPvrstPort,
                                  INT4 *pi4RetValFsPvrstPortBpduGuardAction)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstPortBpduGuardAction = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstPortBpduGuardAction =
        pAstPortEntry->CommPortInfo.u1BpduGuardAction;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortLoopGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortLoopGuard (INT4 i4FsPvrstPort,
                            INT4 i4SetValFsPvrstPortLoopGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_LOOP_GUARD_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;
    if (i4SetValFsPvrstPortLoopGuard == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bLoopGuard = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bLoopGuard = RST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortLoopGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortLoopGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortEnableBPDURx
 Input       :  The Indices
                FsPvrstPort

                The Object
                setValFsPvrstPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortEnableBPDURx (INT4 i4FsPvrstPort,
                               INT4 i4SetValFsPvrstPortEnableBPDURx)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsPvrstPortEnableBPDURx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsPvrstPortEnableBPDURx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsPvrstPortEnableBPDURx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_RX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortEnableBPDURx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortEnableBPDURx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsPvrstPort

                The Object
                setValFsPvrstPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortEnableBPDUTx (INT4 i4FsPvrstPort,
                               INT4 i4SetValFsPvrstPortEnableBPDUTx)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsPvrstPortEnableBPDUTx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsPvrstPortEnableBPDUTx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsPvrstPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_TX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortEnableBPDUTx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortEnableBPDUTx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstBpduFilter
 Input       :  The Indices
                FsPvrstPort

                The Object
                setValFsPvrstBpduFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstBpduFilter (INT4 i4FsPvrstPort, INT4 i4SetValFsPvrstBpduFilter)
{
    tAstPortEntry      *pPortEntry = NULL;
    INT1                i1RetValRx = SNMP_FAILURE;
    INT1                i1RetValTx = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    pPortEntry->u1EnableBPDUFilter = (UINT1) i4SetValFsPvrstBpduFilter;

    i1RetValTx =
        nmhSetFsPvrstPortEnableBPDUTx (i4FsPvrstPort,
                                       i4SetValFsPvrstBpduFilter);
    i1RetValRx =
        nmhSetFsPvrstPortEnableBPDURx (i4FsPvrstPort,
                                       i4SetValFsPvrstBpduFilter);

    if ((i1RetValTx == SNMP_SUCCESS) && (i1RetValRx == SNMP_SUCCESS))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        return (INT1) SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortAutoEdge
 Input       :  The Indices
                FsPvrstPort

                The Object
                setValFsPvrstPortAutoEdge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortAutoEdge (INT4 i4FsPvrstPort, INT4 i4SetValFsPvrstPortAutoEdge)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    if (pPortEntry->bAutoEdge == (tAstBoolean) i4SetValFsPvrstPortAutoEdge)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Port Auto Edge state is same as set value!\n");
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->MsgType = AST_AUTO_EDGEPORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;
    if (i4SetValFsPvrstPortAutoEdge == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bAutoEdgePort = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bAutoEdgePort = RST_FALSE;
    }
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortAutoEdge,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortAutoEdge));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsPvrstPortBpduGuardAction
 Input       :  The Indices
                FsPvrstPort

                The Object 
                setValFsPvrstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPvrstPortBpduGuardAction (INT4 i4FsPvrstPort,
                                  INT4 i4SetValFsPvrstPortBpduGuardAction)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        /* AstMemFailTrap (AST_PVRST_TRAPS_OID, AST_PVRST_TRAPS_OID_LEN); */
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->uMsg.u1BpduGuardAction =
        (UINT1) i4SetValFsPvrstPortBpduGuardAction;

    pAstMsgNode->MsgType = AST_PVRST_BPDUGUARD_ACTION_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsPvrstPort;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstPortBpduGuardAction,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsPvrstPort),
                      i4SetValFsPvrstPortBpduGuardAction));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortLoopGuard
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortLoopGuard (UINT4 *pu4ErrorCode,
                               INT4 i4FsPvrstPort,
                               INT4 i4TestValFsPvrstPortLoopGuard)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortLoopGuard != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstPortLoopGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsPvrstPort);

    if (pCommPortInfo != NULL)
    {
        if (pCommPortInfo->bRootGuard == AST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_ROOTGUARD_ENABLED_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortEnableBPDURx
 Input       :  The Indices
                FsPvrstPort

                The Object
                testValFsPvrstPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortEnableBPDURx (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPvrstPort,
                                  INT4 i4TestValFsPvrstPortEnableBPDURx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortEnableBPDURx != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstPortEnableBPDURx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPvrstPort < AST_MIN_NUM_PORTS)
        || (i4FsPvrstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsPvrstPort

                The Object
                testValFsPvrstPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortEnableBPDUTx (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPvrstPort,
                                  INT4 i4TestValFsPvrstPortEnableBPDUTx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortEnableBPDUTx != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstPortEnableBPDUTx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPvrstPort < AST_MIN_NUM_PORTS)
        || (i4FsPvrstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /*
     * enableBPDUTx can not be set to TRUE on L2GP Port and CBPs
     * */

    if (i4TestValFsPvrstPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }

        if (AST_IS_CUSTOMER_BACKBONE_PORT (i4FsPvrstPort) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstBpduFilter
 Input       :  The Indices
                FsPvrstPort

                The Object
                testValFsPvrstBpduFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstBpduFilter (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                            INT4 i4TestValFsPvrstBpduFilter)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstBpduFilter != AST_SNMP_TRUE) &&
        (i4TestValFsPvrstBpduFilter != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsPvrstPort < AST_MIN_NUM_PORTS)
        || (i4FsPvrstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortAutoEdge
 Input       :  The Indices
                FsPvrstPort

                The Object
                testValFsPvrstPortAutoEdge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortAutoEdge (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                              INT4 i4TestValFsPvrstPortAutoEdge)
{

    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set PortAutoEdgePort\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortAutoEdge == AST_SNMP_TRUE) ||
        (i4TestValFsPvrstPortAutoEdge == AST_SNMP_FALSE))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsPvrstPortBpduGuardAction
 Input       :  The Indices
                FsPvrstPort

                The Object 
                testValFsPvrstPortBpduGuardAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPvrstPortBpduGuardAction (UINT4 *pu4ErrorCode, INT4 i4FsPvrstPort,
                                     INT4 i4TestValFsPvrstPortBpduGuardAction)
{
    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPvrstPortBpduGuardAction != AST_PORT_ADMIN_DOWN) &&
        (i4TestValFsPvrstPortBpduGuardAction != AST_PORT_STATE_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhGetFsPvrstPortRcvInfoWhileExpCount
* Input       :  The Indices
*                FsPvrstPort
*
*                The Object
*                retValFsPvrstPortRcvInfoWhileExpCount
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortRcvInfoWhileExpCount (INT4 i4FsPvrstPort,
                                       UINT4
                                       *pu4RetValFsPvrstPortRcvInfoWhileExpCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstPortRcvInfoWhileExpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    *pu4RetValFsPvrstPortRcvInfoWhileExpCount =
        pAstPortEntry->u4NumRstRxdInfoWhileExpCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhGetFsPvrstPortRcvInfoWhileExpTimeStamp
* Input       :  The Indices
*                FsPvrstPort
*
*                The Object
*                retValFsPvrstPortRcvInfoWhileExpTimeStamp
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortRcvInfoWhileExpTimeStamp (INT4 i4FsPvrstPort,
                                           UINT4
                                           *pu4RetValFsPvrstPortRcvInfoWhileExpTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    *pu4RetValFsPvrstPortRcvInfoWhileExpTimeStamp =
        pAstPortEntry->u4RcvInfoWhileExpTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    :  nmhGetFsPvrstPortImpStateOccurCount
* Input       :  The Indices
*                FsPvrstPort
*
*                The Object
*                retValFsPvrstPortImpStateOccurCount
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortImpStateOccurCount (INT4 i4FsPvrstPort,
                                     UINT4
                                     *pu4RetValFsPvrstPortImpStateOccurCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "PVRST Module not Enabled!\n");
        *pu4RetValFsPvrstPortImpStateOccurCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);

    *pu4RetValFsPvrstPortImpStateOccurCount =
        pAstPortEntry->u4NumRstImpossibleStateOcc;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhGetFsPvrstPortImpStateOccurTimeStamp
* Input       :  The Indices
*                FsPvrstPort
*
*                The Object
*                retValFsPvrstPortImpStateOccurTimeStamp
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPvrstPortImpStateOccurTimeStamp (INT4 i4FsPvrstPort,
                                         UINT4
                                         *pu4RetValFsPvrstPortImpStateOccurTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsPvrstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsPvrstPort);
    *pu4RetValFsPvrstPortImpStateOccurTimeStamp =
        pAstPortEntry->u4ImpStateOccurTimeStamp;
    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortOldPortState
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortOldPortState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortOldPortState (INT4 i4FsPvrstInstVlanId,
                                   INT4 i4FsPvrstInstPortIndex,
                                   INT4 *pi4RetValFsPvrstInstPortOldPortState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortOldPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstInstPortOldPortState = pPerStPortInfo->i4OldPortState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortLoopInconsistentState
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortLoopInconsistentState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortLoopInconsistentState (INT4 i4FsPvrstInstVlanId,
                                            INT4 i4FsPvrstInstPortIndex,
                                            INT4
                                            *pi4RetValFsPvrstInstPortLoopInconsistentState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortLoopInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstInstPortLoopInconsistentState =
        pPerStPortInfo->bLoopIncStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortRootInconsistentState
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortRootInconsistentState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortRootInconsistentState (INT4 i4FsPvrstInstVlanId,
                                            INT4 i4FsPvrstInstPortIndex,
                                            INT4
                                            *pi4RetValFsPvrstInstPortRootInconsistentState)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pi4RetValFsPvrstInstPortRootInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsPvrstInstPortIndex);
    if (pAstCommPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsPvrstInstPortRootInconsistentState =
        pAstCommPortInfo->bRootInconsistent;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortTCDetectedCount
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortTCDetectedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortTCDetectedCount (INT4 i4FsPvrstInstVlanId,
                                      INT4 i4FsPvrstInstPortIndex,
                                      UINT4
                                      *pu4RetValFsPvrstInstPortTCDetectedCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortTCDetectedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortTCDetectedCount =
        pPerStPortInfo->u4TcDetectedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortTCReceivedCount
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortTCReceivedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortTCReceivedCount (INT4 i4FsPvrstInstVlanId,
                                      INT4 i4FsPvrstInstPortIndex,
                                      UINT4
                                      *pu4RetValFsPvrstInstPortTCReceivedCount)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortTCReceivedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortTCReceivedCount = pPerStPortInfo->u4TcRcvdCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortTCDetectedTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortTCDetectedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortTCDetectedTimeStamp (INT4 i4FsPvrstInstVlanId,
                                          INT4 i4FsPvrstInstPortIndex,
                                          UINT4
                                          *pu4RetValFsPvrstInstPortTCDetectedTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortTCDetectedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortTCDetectedTimeStamp =
        pPerStPortInfo->u4TcDetectedTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortTCReceivedTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortTCReceivedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortTCReceivedTimeStamp (INT4 i4FsPvrstInstVlanId,
                                          INT4 i4FsPvrstInstPortIndex,
                                          UINT4
                                          *pu4RetValFsPvrstInstPortTCReceivedTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortTCReceivedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortTCReceivedTimeStamp =
        pPerStPortInfo->u4TcRcvdTimeStamp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortProposalPktsSent
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortProposalPktsSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortProposalPktsSent (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsPvrstInstPortProposalPktsSent)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortProposalPktsSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortProposalPktsSent =
        pPerStPortInfo->u4ProposalTxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortProposalPktsRcvd
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortProposalPktsRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortProposalPktsRcvd (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsPvrstInstPortProposalPktsRcvd)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortProposalPktsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortProposalPktsRcvd =
        pPerStPortInfo->u4ProposalRcvdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortProposalPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortProposalPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortProposalPktSentTimeStamp (INT4 i4FsPvrstInstVlanId,
                                               INT4 i4FsPvrstInstPortIndex,
                                               UINT4
                                               *pu4RetValFsPvrstInstPortProposalPktSentTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortProposalPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortProposalPktSentTimeStamp =
        pPerStPortInfo->u4ProposalTxTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortProposalPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortProposalPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortProposalPktRcvdTimeStamp (INT4 i4FsPvrstInstVlanId,
                                               INT4 i4FsPvrstInstPortIndex,
                                               UINT4
                                               *pu4RetValFsPvrstInstPortProposalPktRcvdTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortProposalPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortProposalPktRcvdTimeStamp =
        pPerStPortInfo->u4ProposalRcvdTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortAgreementPktSent
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortAgreementPktSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortAgreementPktSent (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsPvrstInstPortAgreementPktSent)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortAgreementPktSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortAgreementPktSent =
        pPerStPortInfo->u4AgreementTxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortAgreementPktRcvd
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortAgreementPktRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortAgreementPktRcvd (INT4 i4FsPvrstInstVlanId,
                                       INT4 i4FsPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsPvrstInstPortAgreementPktRcvd)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortAgreementPktRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortAgreementPktRcvd =
        pPerStPortInfo->u4AgreementRcvdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortAgreementPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortAgreementPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortAgreementPktSentTimeStamp (INT4 i4FsPvrstInstVlanId,
                                                INT4 i4FsPvrstInstPortIndex,
                                                UINT4
                                                *pu4RetValFsPvrstInstPortAgreementPktSentTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortAgreementPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortAgreementPktSentTimeStamp =
        pPerStPortInfo->u4AgreementTxTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsPvrstInstPortAgreementPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsPvrstInstVlanId
 * *                FsPvrstInstPortIndex
 * *
 * *                The Object
 * *                retValFsPvrstInstPortAgreementPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsPvrstInstPortAgreementPktRcvdTimeStamp (INT4 i4FsPvrstInstVlanId,
                                                INT4 i4FsPvrstInstPortIndex,
                                                UINT4
                                                *pu4RetValFsPvrstInstPortAgreementPktRcvdTimeStamp)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_PVRST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Started!\n");
        *pu4RetValFsPvrstInstPortAgreementPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return SNMP_FAILURE;
    }

    if (PvrstValidateInstanceEntry (i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (PvrstValidatePortEntry (i4FsPvrstInstPortIndex,
                                i4FsPvrstInstVlanId) != PVRST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Port DOES NOT exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (i4FsPvrstInstPortIndex, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsPvrstInstPortAgreementPktRcvdTimeStamp =
        pPerStPortInfo->u4AgreementRcvdTimeStamp;

    return SNMP_SUCCESS;
}
