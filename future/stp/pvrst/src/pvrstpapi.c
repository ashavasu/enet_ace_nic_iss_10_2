/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: pvrstpapi.c,v 1.19 2017/10/26 12:21:00 siva Exp $
 *
 * Description: This file contains the interface modules used by other
 *              modules.
 *
 *****************************************************************************/

#include "asthdrs.h"
extern tLocalPortList gNullPortList;
/*****************************************************************************/
/* Function Name      : PvrstCreateVlanIndication                            */
/*                                                                           */
/* Description        : Called by Vlan module when a Vlan is Created         */
/*                                                                           */
/* Input(s)           : ContextId- Context Identifier                        */
/*                      VlanId- VlanId which is created                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstCreateVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tAstMsgNode        *pNode = NULL;

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Pvrst Module is not Started.. \n");
        return PVRST_SUCCESS;
    }

    if (PVRST_IS_VALID_VLANID (VlanId) == PVRST_FALSE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Invalid Vlan ID.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG, "API: Invalid Vlan ID.. \n");
        return PVRST_FAILURE;
    }

    AST_GLOBAL_DBG (u4ContextId, AST_EVENT_HANDLING_DBG,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                              PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "API: Message Memory Block Allocation FAILED!!!\n");
        return PVRST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_PVRST_VLAN_CREATED_MSG;
    pNode->u2InstanceId = VlanId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstDeleteVlanIndication                            */
/*                                                                           */
/* Description        : Called by Vlan module when a Vlan is Deleted         */
/*                                                                           */
/* Input(s)           : ContextId- Context Identifier                        */
/*                      VlanId- VlanId which is deleted                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstDeleteVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tAstMsgNode        *pNode = NULL;

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Pvrst Module is not Started.. \n");
        return PVRST_SUCCESS;
    }

    if (PVRST_IS_VALID_VLANID (VlanId) == PVRST_FALSE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Invalid Vlan ID.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG, "API: Invalid Vlan ID.. \n");
        return PVRST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                              PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "API: Message Memory Block Allocation FAILED!!!\n");
        return PVRST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_PVRST_VLAN_DELETE_MSG;
    pNode->u2InstanceId = VlanId;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstSetNativeVlan                                   */
/*                                                                           */
/* Description        : This will called by L2Iwf whenever the PVID of the   */
/*                      Port gets changed                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port no. whose Native Vlan is changed    */
/*                      OldVlanId - Previour PVID set for the port           */
/*                      NewVlanId - New PVID set on the port                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstSetNativeVlan (UINT4 u4IfIndex, tVlanId OldVlanId, tVlanId NewVlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                              PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "API: Message Memory Block Allocation FAILED!!!\n");
        return PVRST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_PVRST_PVID_MSG;
    pNode->u2InstanceId = OldVlanId;
    pNode->uMsg.VlanId = NewVlanId;
    pNode->u4PortNo = u4IfIndex;

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstSetPortType                                     */
/*                                                                           */
/* Description        : It is called in order to set the Port Type either as */
/*                      Access Port belonging to a particular Vlan or        */
/*                      Trunk Port belonging to all Vlans                    */
/*                                                                           */
/* Input(s)           : u4IfIndex- Interface no. whose Type is defined       */
/*                      PortType- It specify the Type(Access,Trunk)to be set */
/*                      VlanId- In case PortType is Access, it specifies to  */
/*                      which Vlan this port is to be associated             */
/*                      otherwise in case of Trunk it is not required        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstSetPortType (UINT2 u2IfIndex, UINT1 u1PortType, tVlanId VlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) u2IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                              PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "API: Message Memory Block Allocation FAILED!!!\n");
        return PVRST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    if (u1PortType == PVRST_TRUNK_PORT)
    {
        pNode->MsgType = (tAstLocalMsgType) AST_PVRST_ADD_TRUNK_PORT_MSG;
        pNode->u4PortNo = u2IfIndex;
        pNode->uMsg.u2LocalPortId = u2LocalPortId;
    }
    else if (u1PortType == PVRST_ACCESS_PORT)
    {
        pNode->MsgType = (tAstLocalMsgType) AST_PVRST_ADD_ACCESS_PORT_MSG;
        pNode->u4PortNo = u2IfIndex;
        pNode->uMsg.u2LocalPortId = u2LocalPortId;
        pNode->u2InstanceId = VlanId;
    }
    else if (u1PortType == PVRST_HYBRID_PORT)
    {
        pNode->MsgType = (tAstLocalMsgType) AST_PVRST_HYBRID_PORT_MSG;
        pNode->u4PortNo = u2IfIndex;
        pNode->uMsg.u2LocalPortId = u2LocalPortId;
    }
    else
    {
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "API: Release of Local Msg Memory Block FAILED!\n");
        }
        return PVRST_FAILURE;
    }

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : PvrstSetTaggedPort                                     */
/*                                                                             */
/* Description        : It is called in order to add/delete a tagged port to   */
/*                      spanning-tree instance                                 */
/*                                                                             */
/* Input(s)           : Addedports   - Tagged Ports to be added to instance    */
/*                      Deletedports - Tagged Ports to be deleted from instance*/
/*                      VlanId       - Vlan Id                                 */
/*                                                                             */
/*                                                                             */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Modified           : None                                                   */
/*                                                                             */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                            */
/*******************************************************************************/

INT4
PvrstSetTaggedPort (tLocalPortList Addedports, tLocalPortList Deletedports,
                    tVlanId VlanId)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4ContextId = AST_DEFAULT_CONTEXT;
    INT4                i4Index = 0;

    if (PVRST_VLAN_IS_NULL_PORTLIST (Addedports) == PVRST_FALSE)
    {
        if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                                  PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "API: Message Memory Block Allocation FAILED!!!\n");
            return PVRST_FAILURE;
        }

        AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));
        pNode->MsgType = (tAstLocalMsgType) AST_PVRST_ADD_TAGGED_PORT_MSG;
        for (i4Index = 0; i4Index < CONTEXT_PORT_LIST_SIZE; i4Index++)
        {
            pNode->TaggedPorts[i4Index] = Addedports[i4Index];
        }
        pNode->u2InstanceId = VlanId;
        if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
        {
            return PVRST_FAILURE;
        }

    }
    if (PVRST_VLAN_IS_NULL_PORTLIST (Deletedports) == PVRST_FALSE)
    {
        if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstGlobalMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                                  PVRST_BRG_TRAPS_OID_LEN, u4ContextId);
            AST_GLOBAL_DBG (u4ContextId,
                            AST_INIT_SHUT_DBG | AST_MEM_DBG |
                            AST_ALL_FAILURE_DBG,
                            "API: Message Memory Block Allocation FAILED!!!\n");
            return PVRST_FAILURE;
        }

        AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));
        pNode->MsgType = (tAstLocalMsgType) AST_PVRST_DELETE_TAGGED_PORT_MSG;
        for (i4Index = 0; i4Index < CONTEXT_PORT_LIST_SIZE; i4Index++)
        {
            pNode->TaggedPorts[i4Index] = Deletedports[i4Index];
        }
        pNode->u2InstanceId = VlanId;
        if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiVlanDeleteIndication                */
/*                                                                           */
/*    Description               : This routine is called whenever            */
/*                                a vlan is deleted. It unmaps this vlan     */
/*                                from the instance to which it is mapped in */
/*                                the hardware.                              */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is  deleted.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/
INT4
PvrstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    /* AST Lock need not be taken here since it does not access
     * any RSTP/ PVRST data structure */

    AST_GLOBAL_DBG (u4ContextId, AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
                    "API: Deleting Vlan %d .. \n", VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Deleting Vlan %d .. \n", VlanId);

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Pvrst Module is not Started.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG,
                        "API: Pvrst Module is not Started.. \n");
        return PVRST_SUCCESS;
    }

    if (PVRST_IS_VALID_VLANID (VlanId) == PVRST_FALSE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Invalid Vlan ID.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG, "API: Invalid Vlan ID.. \n");
        return PVRST_FAILURE;
    }
    if (PvrstDeleteVlanIndication (u4ContextId, VlanId) != PVRST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiVlanCreateIndication                */
/*                                                                           */
/*    Description               : This routine is called  whenever           */
/*                                a vlan is created. If this vlan is mapped  */
/*                                to any instance in the software, then it   */
/*                                maps this vlan to that instance in the     */
/*                                hardware.                                  */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is created          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/
INT4
PvrstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId)
{

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC |
                        AST_CONTROL_PATH_TRC,
                        "API: Pvrst Module is not Started.. \n");
        return PVRST_SUCCESS;
    }

    if (PVRST_IS_VALID_VLANID (VlanId) == PVRST_FALSE)
    {
        return PVRST_FAILURE;
    }

    AST_GLOBAL_DBG (u4ContextId, AST_EVENT_HANDLING_DBG,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);

    if (PvrstCreateVlanIndication (u4ContextId, VlanId) != PVRST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    AST_GLOBAL_DBG (u4ContextId, AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
                    "API: Successfully handled vlan %d creation indication \n",
                    VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Successfully handled vlan %d creation indication \n",
                    VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiDeleteAllVlanIndication             */
/*                                                                           */
/*    Description               : This routine is called whenever            */
/*                                vlan module is shutdown. It deletes all    */
/*                                the instances                              */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/
INT4
PvrstMiDeleteAllVlanIndication (UINT4 u4ContextId)
{
    /* AST Lock need not be taken here since it does not access
     * any RSTP/ PVRST data structure */
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Pvrst Module is not Started.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG,
                        "API: Pvrst Module is not Started.. \n");
        return PVRST_SUCCESS;
    }

    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (u4ContextId, VlanId) == OSIX_FALSE)
        {
            continue;
        }
        /*If instance is present then delete instance */
        if (PvrstDeleteVlanIndication (u4ContextId, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                     "API: Instance not Deleted for Vlan.\n");
            AST_DBG (AST_INIT_SHUT_DBG, "API: instance not"
                     "deleted for Vlan.\n");
            return PVRST_FAILURE;
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsPvrstStartedInContext                           */
/*                                                                           */
/* Description        : Called by other modules to know if PVRST is started  */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Pvrst is started in the given context        */
/*                      0 - No, Pvrst is NOT started in the given context    */
/*****************************************************************************/
INT4
AstIsPvrstStartedInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return AST_FALSE;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == PVRST_START))
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}

/*****************************************************************************/
/* Function Name      : AstIsPvrstEnabledInContext                           */
/*                                                                           */
/* Description        : Called by other modules to know if PVRST is enabled  */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Pvrst is enabled in the given context        */
/*                      0 - No, Pvrst is NOT enabled in the given context    */
/*****************************************************************************/
INT4
AstIsPvrstEnabledInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return AST_FALSE;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == PVRST_START) &&
        (gau1AstModuleStatus[u4ContextId] == PVRST_ENABLED))
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}

/*****************************************************************************/
/* Function Name        : PvrstIsPathcostConfigured                          */
/*                                                                           */
/* Description          : This is to determine whether pathcost has been     */
/*                        configured in this port. This should be called     */
/*                        from outside PVRST only.                           */
/*                                                                           */
/* Input(s)             : u2IfIndex    - Global IfIndex                      */
/*                        u2InstanceId - Instance ID                         */
/*                                                                           */
/* Output(s)            : None                                               */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : PVRST_TRUE  - If pathcost has been configured      */
/*                        PVRST_FALSE - If pathcost has not been configured  */
/*                                                                           */
/* Called By            : MSR                                                */
/*****************************************************************************/
UINT1
PvrstIsPathcostConfigured (UINT2 u2IfIndex, UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId, &u2LocalPortId)
        == RST_FAILURE)
    {
        return PVRST_FALSE;
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return PVRST_FALSE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId, u2InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        AstReleaseContext ();
        return PVRST_FALSE;
    }

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        AstReleaseContext ();
        return PVRST_FALSE;
    }

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return PVRST_FALSE;
    }

    if (pPerStPortInfo->u4PortAdminPathCost == 0)
    {
        AstReleaseContext ();
        return PVRST_FALSE;
    }

    AstReleaseContext ();
    return PVRST_TRUE;
}

/*****************************************************************************/
/* Function Name      : AstIsPvrstInstExist                                  */
/*                                                                           */
/* Description        : Called by other modules to know whether the instance */
/*                      should be present or not when there no member ports  */
/*                      in the VLAN                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                    : If AST_TRUE: Instance existance irrespective         */
/*                      of member ports in the VLAN                          */
/*                      If AST_FALSE: Instance exists only if 
                        there are one member port in the VLAN.               */
/*****************************************************************************/
INT4
AstIsPvrstInstExist (VOID)
{

    if (gu1PvrstInstExistance == RST_TRUE)
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}
