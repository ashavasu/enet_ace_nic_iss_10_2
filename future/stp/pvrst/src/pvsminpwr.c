/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: pvsminpwr.c,v 1.5 2009/12/29 12:20:48 prabuc Exp $
 *
 * Description: Stubs for network processor functions given here
 *
 *****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "pnac.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "rstp.h"
#include "pvrst.h"
#include "la.h"
#include "npapi.h"

#ifdef NP_KERNEL_WANTED
#include "chrdev.h"
#endif

/*******************************************************************************
 * FsMiPvrstNpCreateVlanSpanningTree
 *
 * DESCRIPTION:
 * Creates a Spanning Tree for the given VLAN. In case hardware supports automatic 
 * spanning tree creation for Vlan then this routine should not be ported
 *
 * INPUTS:
 * ContextId - Virtual Switch ID
 * VlanId - Vlan id of the Spanning tree to be created.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during creating
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiPvrstNpCreateVlanSpanningTree (UINT4 u4ContextId, tVlanId VlanId)
{
    INT4                i4RetVal;

    UNUSED_PARAM (u4ContextId);

    return FsPvrstNpCreateVlanSpanningTree (VlanId);
}

/*******************************************************************************
 * FsMiPvrstNpDeleteVlanSpanningTree
 *
 * DESCRIPTION:
 * Delete the Spanning Tree instance in the hardware for the given Vlan.
 *
 * INPUTS:
 * ContextId - Virtual Switch ID
 * VlanId - Vlan id of the Spanning tree to be deleted
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during deleting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiPvrstNpDeleteVlanSpanningTree (UINT4 u4ContextId, tVlanId VlanId)
{
    INT4                i4RetVal;

    UNUSED_PARAM (u4ContextId);

    return FsPvrstNpDeleteVlanSpanningTree (VlanId);
}

/*****************************************************************************
 * FsMiPvrstNpInitHw 
 *
 * DESCRIPTION:
 * This function performs any necessary PVRST related initialisation in 
 * the Hardware
 *
 * INPUTS:
 * ContextId - Virtual Switch ID
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

INT4
FsMiPvrstNpInitHw (UINT4 u4ContextId)
{
    INT4                i4RetVal;
    UNUSED_PARAM (u4ContextId);

    return FsPvrstNpInitHw ();
}

/*****************************************************************************
 * FsMiPvrstNpDeInitHw 
 *
 * DESCRIPTION:
 * Sets the PVRST Disable Option  in the Hardware.
 * This function removes PVRST related initialisation (done in FsMiPvrstNpInitHw) 
 * from the Hardware
 *
 * INPUTS:
 * ContextId - Virtual Switch ID
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

VOID
FsMiPvrstNpDeInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    FsPvrstNpDeInitHw ();
    return;
}

/*******************************************************************************
 * FsMiPvrstNpSetVlanPortState
 *
 * DESCRIPTION:
 * Sets the PVRST Port State in the Hardware for given Vlan. 
 *
 * INPUTS:
 * ContextId - Virtual Switch ID
 * u4IfIndex   - Interface index of whose Vlan state is to be updated
 * VlanId - Vlan ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT4
FsMiPvrstNpSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                             tVlanId VlanId, UINT1 u1PortState)
{
    UNUSED_PARAM (u4ContextId);
    return FsPvrstNpSetVlanPortState (u4IfIndex, VlanId, u1PortState);
}

/*****************************************************************************/
/* Function Name      : FsMiPvrstNpGetVlanPortState                          */
/*                                                                           */
/* Description        : Gets the Vlan Port State in the Hardware.            */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan ID.                               */
/*                      u4IfIndex - Port Number.                             */
/*                      pu1Status  - Status returned from Hardware.          */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiPvrstNpGetVlanPortState (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex,
                             UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsPvrstNpGetVlanPortState (VlanId, u4IfIndex, *pu1Status);
}
