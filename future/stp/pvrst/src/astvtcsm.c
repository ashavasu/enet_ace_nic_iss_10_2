/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvtcsm.c,v 1.20 2017/12/29 09:31:22 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Topology Change State Event Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"

#ifdef NPAPI_WANTED
extern INT4         gi4AstInitProcess;
#endif
/*****************************************************************************/
/* Function Name      : PvrstTopoChMachine                                   */
/*                                                                           */
/* Description        : This is the main routine for the Topology Change     */
/*                      State Machine. This function is responsible for      */
/*                      topology change detection, notification & propagation*/
/*                      and for filtering database flushing.                 */
/*                                                                           */
/* Input(s)           : u1Event - The event that has caused this Topology    */
/*                                Change State Machine to be called          */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2State = (UINT1) AST_INIT_VAL;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Event %s: Invalid parameter passed "
                      "to event handler routine\n",
                      gaaau1AstSemEvent[AST_TCSM][u2Event]);
        return PVRST_FAILURE;
    }
    u2State = (UINT2) pPerStPortInfo->u1TopoChSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: Topo Ch Machine Called with "
                  "Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_TCSM][u2Event],
                  gaaau1AstSemState[AST_TCSM][u2State]);

    AST_DBG_ARG3 (AST_TCSM_DBG,
                  "TCSM: Port %u: Topo Ch Machine Called with"
                  " Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_TCSM][u2Event],
                  gaaau1AstSemState[AST_TCSM][u2State]);

    if (PVRST_TOPO_CH_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_TCSM_DBG, "TCSM: No Operations to perform\n");
        return PVRST_SUCCESS;
    }

    if ((*(PVRST_TOPO_CH_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: Topology Change Event Function " "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeInit                                */
/*                                                                           */
/* Description        : This function is called during the initialisation of */
/*                      the Topology Change State Machine and also if the    */
/*                      Port is not in the RootPort or DesignatedPort roles. */
/*                      This changes the state of the Topology Change state  */
/*                      machine to the INIT state.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeInit (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    PvrstTopologyChSmFlushEntries (pPerStPortInfo);
    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                            u2InstIndex, (UINT1) AST_TMR_TYPE_TCWHILE)
            != PVRST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: PvrstStopTimer for TcWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_INIT;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved to state INIT\n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstTopoChSmMakeInactive (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmMakeInactive function"
                 " returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeInactive                            */
/*                                                                           */
/* Description        : This function is called whenever the Port is a Backup*/
/*                      or Alternate Port (i.e. until the Port becomes a Root*/
/*                      or Designated Port. This is called when the Topology */
/*                      Change State machine is in the INIT state and this   */
/*                      function changes the state to the INACTIVE state.    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeInactive (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    pPerStRstPortInfo->bRcvdTc = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_FALSE;
    pPerStRstPortInfo->bTc = PVRST_FALSE;
    pPerStRstPortInfo->bTcProp = PVRST_FALSE;

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_INACTIVE;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved "
                  "to state INACTIVE\n", pPerStPortInfo->u2PortNo);

    if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT) ||
        (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED))
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %u Role is ROOT/DESIGNATED;"
                      " Changing to ACTIVE state\n", pPerStPortInfo->u2PortNo);

        PvrstTopoChSmMakeActive (pPerStPortInfo);
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeDetected                            */
/*                                                                           */
/* Description        : This function is called whenever the Port has        */
/*                      detected a topology change. This is called when the  */
/*                      Topology Change state machine is in the ACTIVE state */
/*                      and this function changes the state to the DETECTED  */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeDetected (tAstPerStPortInfo * pPerStPortInfo)
{
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    UINT4               u4Ticks = AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    PVRST_INCR_TC_DETECTED_COUNT (pPerStPortInfo->u2PortNo, u2InstIndex);
    AST_GET_SYS_TIME (&u4Ticks);
    pPerStPortInfo->u4TcDetectedTimeStamp = u4Ticks;
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: TC has been set, taking action...\n",
                  pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %u: TC has been set, taking action...\n",
                  pPerStPortInfo->u2PortNo);
    AST_GET_SYS_TIME ((tAstSysTime
                       *) (&(pPerStPortInfo->u4TcDetectedTimestamp)));
    pPerStPortInfo->u4TcDetectedCount = pPerStPortInfo->u4TcDetectedCount + 1;
    pPerStBrgInfo->au2TcDetPorts[pPerStBrgInfo->u1DetHead] =
        (UINT2) AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);
    pPerStBrgInfo->u1DetHead = (UINT1) (pPerStBrgInfo->u1DetHead + 1);
    if (pPerStBrgInfo->u1DetHead == AST_MAX_TC_MEM_LEN)
    {
        pPerStBrgInfo->u1DetHead = AST_INIT_VAL;
    }

    if (PvrstTopoChSmNewTcWhile (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmNewTcWhile function "
                 "returned FAILURE!\n");
        return PVRST_FAILURE;
    }
    if (PvrstPortTransmitMachine ((UINT2)
                                  PVRST_PTXSM_EV_NEWINFO_SET,
                                  pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %u: Transmit "
                      "machine returned FAILURE !!! \n",
                      pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: Transmit "
                      "machine returned FAILURE !!! \n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (PvrstTopoChSmSetTcPropBridge (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmSetTcPropBridge function "
                 "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    pPerStPortInfo->PerStRstPortInfo.bTc = PVRST_FALSE;

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_DETECTED;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved to"
                  " state DETECTED\n", pPerStPortInfo->u2PortNo);

    PvrstTopoChSmMakeActive (pPerStPortInfo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeNotifiedTcn                         */
/*                                                                           */
/* Description        : This function is called whenever a TCN Bpdu has been */
/*                      received on this Port. This is called when the       */
/*                      Topology Change state machine is in the ACTIVE state */
/*                      and this function changes the state to the NOTIFIED_ */
/*                      TCN state.                                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeNotifiedTcn (tAstPerStPortInfo * pPerStPortInfo)
{
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: RCVDTCN has been set, taking action..\n",
                  pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %u: RCVDTCN has been set, taking action..\n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstTopoChSmNewTcWhile (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmNewTcWhile function "
                 "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_NOTIFIED_TCN;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved to"
                  " state NOTIFIED_TCN\n", pPerStPortInfo->u2PortNo);

    if (PvrstTopoChSmMakeNotifiedTc (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmMakeNotifiedTc "
                 "function returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeNotifiedTc                          */
/*                                                                           */
/* Description        : This function is called whenever a Bpdu with the TC  */
/*                      flag set has been received on this Port. This is     */
/*                      called unconditionally when the Topology Change State*/
/*                      is in the NOTIFIED_TCN state or from the ACTIVE state*/
/*                      on receipt of the Bpdu and this function changes the */
/*                      state to the NOTIFIED_TC state.                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstTopoChSmMakeNotifiedTc (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    u2InstIndex =
        AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                      pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStPortInfo->u4TcRcvdTimestamp)));
    ++pPerStPortInfo->u4TcRcvdCount;

    pPerStBrgInfo->au2TcRecvPorts[pPerStBrgInfo->u1RcvdHead] =
        (UINT2) AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);
    pPerStBrgInfo->u1RcvdHead = (UINT1) (pPerStBrgInfo->u1RcvdHead + 1);
    if (pPerStBrgInfo->u1RcvdHead == AST_MAX_TC_MEM_LEN)
    {
        pPerStBrgInfo->u1RcvdHead = AST_INIT_VAL;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: RCVDTC has been set, "
                  "taking action...\n", pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %u: RCVDTC has been set, "
                  "taking action...\n", pPerStPortInfo->u2PortNo);

    pPerStPortInfo->PerStRstPortInfo.bRcvdTc = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_FALSE;

    if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
    {
        pPerStPvrstRstPortInfo->bTcAck = PVRST_TRUE;
    }

    if (PvrstTopoChSmSetTcPropBridge (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstTopoChSmSetTcPropBridge function "
                 "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_NOTIFIED_TC;

    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved "
                  "to state NOTIFIED_TC\n", pPerStPortInfo->u2PortNo);

    PvrstTopoChSmMakeActive (pPerStPortInfo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakePropagating                         */
/*                                                                           */
/* Description        : This function is called if this Port is not an Edge  */
/*                      Port and its TcProp variable has been set by some    */
/*                      other port of this bridge indicating the need to     */
/*                      propagate a topology change. This is called when the */
/*                      Topology Change State Machine is in the ACTIVE state */
/*                      and this function changes the state to the           */
/*                      PROPAGATING state.                                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakePropagating (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: TCPROP has been set, taking action...\n",
                  pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %u: TCPROP has been set, taking action...\n",
                  pPerStPortInfo->u2PortNo);

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pAstPortEntry->bOperEdgePort != PVRST_TRUE)
    {

        if (PvrstTopoChSmNewTcWhile (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: PvrstTopoChSmNewTcWhile function "
                     "returned FAILURE!\n");
            return PVRST_FAILURE;
        }
        if ((AST_FORCE_VERSION == (UINT1) AST_VERSION_0) ||
            (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE))

        {
            /* Need not set Short Ageout time again for this port
             * when ShortAgeout Duration timer is running */
            if (pCommPortInfo->pRapidAgeDurtnTmr == NULL)
            {
                AstVlanSetShortAgeoutTime (pPortInfo);
            }

            /* FdbFlush has been set. Short Ageout time should be applied
             * for a duration of FwdDelay more seconds. Hence restart the
             * duration timer (even if it is already running) */
            /* Start the duration for FwdDelay+(100 centi-seconds) so that the
             * forwarding module gets to ageout entries atleast once at
             * the end of FwdDelay seconds before reverting back to
             * long ageout */
            if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                               (UINT1) AST_TMR_TYPE_RAPIDAGE_DURATION,
                               (UINT2) (pPortInfo->DesgTimes.u2ForwardDelay +
                                        (1 * AST_CENTI_SECONDS))) !=
                RST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: AstStartTimer for Rapid Age duration FAILED!\n");
            }
        }
        else
        {
            PvrstTopologyChSmFlushEntries (pPerStPortInfo);
        }
        pPerStPortInfo->PerStRstPortInfo.bTcProp = PVRST_FALSE;

        pPerStPortInfo->u1TopoChSmState =
            (UINT1) PVRST_TOPOCHSM_STATE_PROPAGATING;

        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %u: Moved to state PROPAGATING\n",
                      pPerStPortInfo->u2PortNo);

        PvrstTopoChSmMakeActive (pPerStPortInfo);

        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %u is NOT an EDGE port,"
                      " hence PROPAGATING\n", pPerStPortInfo->u2PortNo);

    }                            /* End of Oper Edge check */
    else
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "TCSM: Port %u is an EDGE port, "
                      "hence NO PROPAGATION\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %u is an EDGE port, "
                      "hence NO PROPAGATION\n", pPerStPortInfo->u2PortNo);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeAcknowledged                        */
/*                                                                           */
/* Description        : This function is called if a Bpdu is received on this*/
/*                      Port with the TC Acknowledge Flag set. This is called*/
/*                      when the Topology Change State Machine is in the     */
/*                      ACTIVE state and this function changes the state to  */
/*                      the ACKNOWLEDGED state.                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeAcknowledged (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: RCVDTCACK has been set,"
                  " taking action..\n", pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TCSM_DBG,
                  "TCSM: Port %u: RCVDTCACK has been set, "
                  "taking action..\n", pPerStPortInfo->u2PortNo);

    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
    {

        if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                            (UINT1) AST_TMR_TYPE_TCWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: PvrstStopTimer for TcWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_FALSE;

    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_ACKNOWLEDGED;
    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved to "
                  "state ACKNOWLEDGED\n", pPerStPortInfo->u2PortNo);

    PvrstTopoChSmMakeActive (pPerStPortInfo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmMakeActive                              */
/*                                                                           */
/* Description        : This function is called from all the other states of */
/*                      this state machine excepting the INIT state, and     */
/*                      control transitions unconditionally to the ACTIVE    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmMakeActive (tAstPerStPortInfo * pPerStPortInfo)
{
    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_ACTIVE;
    AST_DBG_ARG1 (AST_TCSM_DBG, "TCSM: Port %u: Moved to state ACTIVE\n",
                  pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmFlushFdb                                */
/*                                                                           */
/* Description        : This function performs the functionality of flushing */
/*                      the Filtering Database to remove the information that*/
/*                      was learnt on this Port.                             */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the Port whose learnt */
/*                                  information needs to be flushed          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

VOID
PvrstTopoChSmFlushFdb (tAstPerStPortInfo * pPerStPortInfo)
{

#ifdef NPAPI_WANTED
    if (gi4AstInitProcess == 1)
    {
        /* Flush called during initialisation. Ignore this because all entries
         * will fulushed at the end of initialisation */
        return;
    }
#endif

    /* Shift Learnt information from Retiring Ports */
#ifdef VLAN_WANTED
    if (VLAN_IS_VLAN_ENABLED () == VLAN_TRUE)
    {
        AstVlanFlushFdbEntries (AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                                pPerStPortInfo->u2Inst, VLAN_NO_OPTIMIZE);
    }
#endif
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "TCSM: Learnt Entries on Port %u for Vlan %u have been flushed!\n",
                  pPerStPortInfo->u2PortNo, pPerStPortInfo->u2Inst);
    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Learnt Entries on Port %u for Vlan %u have been flushed!\n",
                  pPerStPortInfo->u2PortNo, pPerStPortInfo->u2Inst);
    return;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmSetTcPropBridge                         */
/*                                                                           */
/* Description        : This function sets the variable TcProp to TRUE for   */
/*                      all the Ports except the Port that called this       */
/*                      procedure.                                           */
/*                      This procedure also increments the topology change   */
/*                      count and generates topology change trap if tcWhile  */
/*                      timer is not running for any of the ports.           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstTopoChSmSetTcPropBridge (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    /* restrictedTCN causes the Port not to propagate received topology change 
     * notifications and topology changes to other Ports.*/

    /* Hence if restrictedTCN is set for the port that is invoking this 
     * procedure, then do nothing. */
    if (AST_PORT_RESTRICTED_TCN (pAstPortEntry) == (tAstBoolean) RST_TRUE)
    {
        return PVRST_SUCCESS;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        return PVRST_SUCCESS;
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
        if (pAstPortEntry != NULL)
        {
            pAstPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pAstPerStPortInfo == NULL)
            {
                continue;
            }
            pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pAstPerStPortInfo);
            if ((AST_IS_PORT_UP (u2PortNum)) &&
                (pPerStRstPortInfo->bPortEnabled == PVRST_TRUE))
            {

                if (pAstPerStPortInfo != pPerStPortInfo)
                {

                    pAstPerStPortInfo->PerStRstPortInfo.bTcProp = PVRST_TRUE;

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "TCSM: Propagating Topology Change Info"
                                  " (TcProp) on Port %u\n", u2PortNum);
                    AST_DBG_ARG1 (AST_TCSM_DBG,
                                  "TCSM: Propagating Topology Change Info"
                                  " (TcProp) on Port %u\n", u2PortNum);

                    if (PvrstTopoChMachine
                        ((UINT1) PVRST_TOPOCHSM_EV_TCPROP,
                         pAstPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "TCSM: PvrstTopoChMachine Entry function "
                                 "returned FAILURE!\n");
                        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TCSM: PvrstTopoChMachine Entry function "
                                 "returned FAILURE!\n");
                        return PVRST_FAILURE;
                    }
                }                /* Not referring to this Port */
            }                    /* Port operationally up */
        }                        /* Valid Port pointer existing */
    }                            /* End of FOR Loop */

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmNewTcWhile                              */
/*                                                                           */
/* Description        : This function sets the value of the TcWhile Timer    */
/*                      duration depending on whether it is a point-to-point */
/*                      link with the Partner Bridge Port being RSTP capable */
/*                      and starts the TcWhile Timer for this duration.      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmNewTcWhile (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo;
    tAstBoolean         bBridgeTcPresent = PVRST_FALSE;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2TcWhileDuration = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    if ((NULL == pPerStPvrstBridgeInfo) || (NULL == pPerStBridgeInfo))
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: PVRST Bridge Information for "
                      "instance: %d is not valid\n", u2InstIndex);
        return PVRST_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (NULL == pPerStPvrstRstPortInfo)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: PVRST Port Information for "
                      "instance: %d is not valid\n", u2InstIndex);
        return PVRST_FAILURE;
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if (AST_GET_PORTENTRY (u2PortNum) != NULL)
        {
            pTmpPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pTmpPerStPortInfo == NULL)
            {
                continue;
            }
            if (pTmpPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
            {
                bBridgeTcPresent = PVRST_TRUE;
                break;
            }
        }
    }
    if (bBridgeTcPresent == PVRST_FALSE)
    {
        /* TC is not present at the bridge level
         * so increment the topology change count and send trap.
         */
        PVRST_INCR_NUM_OF_TOPO_CHANGES (u2InstIndex);
        AST_GET_SYS_TIME ((tAstSysTime
                           *) (&(pPerStBridgeInfo->u4TimeSinceTopoCh)));
        gu4RecentTopoChPort = pAstPortEntry->u4IfIndex;
        AstTopologyChangeTrap (VlanId, (INT1 *) AST_PVRST_TRAPS_OID,
                               PVRST_BRG_TRAPS_OID_LEN);
    }

    if ((pAstPortEntry->bOperPointToPoint == PVRST_TRUE) &&
        (pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE))
    {
        AST_DBG_ARG1 (AST_TCSM_DBG,
                      "TCSM: Port %u is a POINT-TO-POINT link & "
                      "Partner Bridge Port is RSTP capable\n",
                      pPerStPortInfo->u2PortNo);

        u2TcWhileDuration =
            (UINT2) (2 * (pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime));
    }
    else
    {
        u2TcWhileDuration =
            (UINT2) ((pPerStPvrstBridgeInfo->RootTimes.u2MaxAge) +
                     (pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay));
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "TCSM: Port %u: Set TcWhile Timer Duration as %u\n",
                  pPerStPortInfo->u2PortNo, u2TcWhileDuration);
    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %u: Set TcWhile Timer Duration as %u\n",
                  pPerStPortInfo->u2PortNo, u2TcWhileDuration);

    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_TCWHILE, u2TcWhileDuration)
        != PVRST_SUCCESS)
    {
        AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: PvrstStartTimer for TcWhileTmr FAILED!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstTopoChSmEventImpossible                         */
/*                                                                           */
/* Description        : This function is called on the occurence of an event */
/*                      that is not possible in the present state of the     */
/*                      Topology Change State Machine.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstTopoChSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo)
{
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "TCSM-PVRST: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
             "TCSM-PVRST: This is an IMPOSSIBLE EVENT in this State!\n");
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                 VlanId,
                                 AST_GET_IFINDEX (pPerStPortInfo->u2PortNo)) ==
        OSIX_TRUE)
    {
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Port:%s  Vlan:%u  Impossible state occured at : %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          VlanId, au1TimeStr);
        AST_INCR_IMPSTATE_OCCUR_COUNT (pPerStPortInfo->u2PortNo);
        AST_GET_SYS_TIME (&u4Ticks);
        pAstPortEntry->u4ImpStateOccurTimeStamp = u4Ticks;
    }
    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitTopoChStateMachine                          */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaTopoChMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaTopoChMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PvrstInitTopoChStateMachine (VOID)
{

    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_TOPOCHSM_MAX_EVENTS][20] = {
        "BEGIN", "ROOTPORT", "BLOCKEDPORT", "DESGPORT",
        "RCVDTC", "RCVDTCN", "RCVDTCACK", "TC", "TCPROP"
    };
    UINT1               aau1SemState[PVRST_TOPOCHSM_MAX_STATES][20] = {
        "INIT", "INACTIVE", "ACTIVE", "DETECTED",
        "NOTIFIED_TCN", "NOTIFIED_TC", "PROPAGATING", "ACKNOWLEDGED"
    };

    for (i4Index = 0; i4Index < PVRST_TOPOCHSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_TCSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_TCSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_TOPOCHSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_TCSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_TCSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /* Port Topology change SEM */
    /* BEGIN Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmMakeInit;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BEGIN]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* ROOTPORT Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeActive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_ROOTPORT]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* BLOCKEDPORT Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = NULL;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakeInit;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_BLOCKEDPORT]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* DESGPORT Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeActive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_DESGPORT]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* RCVDTC Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeInactive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakeNotifiedTc;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTC]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* RCVDTCN Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeInactive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakeNotifiedTcn;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCN]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* RCVDTCACK Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeInactive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakeAcknowledged;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_RCVDTCACK]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* TC Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeInactive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakeDetected;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TC]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    /* TCPROP Event */
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_INIT].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_INACTIVE].pAction = PvrstTopoChSmMakeInactive;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_ACTIVE].pAction = PvrstTopoChSmMakePropagating;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_DETECTED].pAction = PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_NOTIFIED_TC].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_PROPAGATING].pAction =
        PvrstTopoChSmEventImpossible;
    PVRST_TOPO_CH_MACHINE[PVRST_TOPOCHSM_EV_TCPROP]
        [PVRST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction =
        PvrstTopoChSmEventImpossible;

    AST_DBG (AST_TCSM_DBG, "TCSM: Loaded TCSM SEM successfully\n");

    return;
}

/*****************************************************************************/
/* Function Name      : PvrstTopologyChSmFlushEntries                        */
/*                                                                           */
/* Description        : This function performs the functionality of flushing */
/*                      the Filtering database to remove the information     */
/*              that was learnt on this vlan.                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance          */
/*                      specific  port information                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstTopologyChSmFlushEntries (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex =
        AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                      pPerStPortInfo->u2Inst);
    pPerStInfo = AST_GET_PERST_INFO (u2InstIndex);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstIndex);

    if ((pPerStInfo == NULL) || (pPerStBrgInfo == NULL))
    {
        return;
    }

    u4IfIndex = AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);

    /*
     * When flush interval is default value ( == 0), then normal
     * flow will continue. (i.e) Port,Instance based flushing
     * will be triggered.
     *
     * When flush interval is non-default value (!= 0), then
     * the following procedures will be taken place:
     *
     * If (Flush trigger timer == Running)
     * ==> Pending flushes will be updated as TRUE
     *
     * If (Flush Trigger timer == Not_Running)
     *
     * ==> If (Flush Interval Threshold == Default_Value(0))
     *        ==> Call Instance based flush. The same flush function
     *            will be invoked with invalid port num
     *        ==> Increment the instance based flush count after the
     *            last timer expiry
     *        ==> Increment the total flush count for the instance
     *
     * ==> If (Actual_Flush_Indication_Count_After_last_timer_expiry
     *         < Flush_Interval_Threshold)
     *        ==> Call port, instance based flushing with the actual
     *            port number value.
     *        ==> Increment the instance based flush count after the
     *            last timer expiry
     *        ==> Increment the total flush count for the instance
     *
     * ==> Else If (Flush INterval Threshold == Default value (0))
     *          || (Actual_Flush_Indication_Count_After_last_timer_expiry
     *          == Flush_Interval_Threshold)
     *        ==> Start flush trigger timer
     *        ==> Update pending flushes as false
     *
     *
     * During timer expiry, instance based flushing will be invoked by
     * filling the port number as invalid port identifier.
     *
     */

    if (AST_FLUSH_INTERVAL == AST_INIT_VAL)
    {
        if (AstVlanFlushFdbEntries
            (u4IfIndex, pPerStPortInfo->u2Inst, VLAN_OPTIMIZE) == L2IWF_FAILURE)
        {
            return;
        }
        (pPerStBrgInfo->u4TotalFlushCount)++;
    }
    else
    {
        if (pPerStInfo->pFlushTgrTmr == NULL)
        {
            if (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL)
            {
                u4IfIndex = AST_INVALID_PORT_NUM;
            }

            if ((pPerStBrgInfo->u4FlushIndCount <
                 pPerStBrgInfo->u4FlushIndThreshold) ||
                (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL))
            {
                if (AstVlanFlushFdbEntries
                    (u4IfIndex,
                     pPerStPortInfo->u2Inst, VLAN_OPTIMIZE) == L2IWF_FAILURE)
                {
                    return;
                }
                (pPerStBrgInfo->u4FlushIndCount)++;
                (pPerStBrgInfo->u4TotalFlushCount)++;
            }

            if (pPerStBrgInfo->u4FlushIndCount >=
                pPerStBrgInfo->u4FlushIndThreshold)
            {
                if (PvrstStartTimer ((VOID *) pPerStInfo, u2InstIndex,
                                     (UINT1) AST_TMR_TYPE_FLUSHTGR,
                                     (UINT2) AST_FLUSH_INTERVAL) != RST_SUCCESS)
                {
                    AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: AstStartTimer for flush trigger FAILED!\n");
                }

                pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
            }
        }
        else
        {
            pPerStInfo->FlushFlag.u1PendingFlushes = AST_TRUE;
        }
    }

}

/* End of file */
