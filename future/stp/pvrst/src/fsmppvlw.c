/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppvlw.c,v 1.28 2017/12/22 10:06:01 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "asthdrs.h"
# include  "astvinc.h"
# include  "vcm.h"
# include  "fssnmp.h"
# include "fsmppvcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIPvrstGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstGlobalTrace (INT4 *pi4RetValFsMIPvrstGlobalTrace)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalTraceOption == AST_TRUE)
    {
        *pi4RetValFsMIPvrstGlobalTrace = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIPvrstGlobalTrace = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIPvrstGlobalTrace);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsMIPvrstGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstGlobalDebug (INT4 *pi4RetValFsMIPvrstGlobalDebug)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalDebugOption == AST_TRUE)
    {
        *pi4RetValFsMIPvrstGlobalDebug = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIPvrstGlobalDebug = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIPvrstGlobalDebug);
    return SNMP_SUCCESS;
#endif

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPvrstGlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIPvrstGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstGlobalTrace (INT4 i4SetValFsMIPvrstGlobalTrace)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFsMIPvrstGlobalTrace == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_FALSE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstGlobalTrace,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ", i4SetValFsMIPvrstGlobalTrace));
    AstSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIPvrstGlobalTrace);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsMIPvrstGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstGlobalDebug (INT4 i4SetValFsMIPvrstGlobalDebug)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFsMIPvrstGlobalDebug == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_FALSE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIPvrstGlobalDebug,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ", i4SetValFsMIPvrstGlobalDebug));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIPvrstGlobalDebug);
    return SNMP_SUCCESS;
#endif

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIPvrstGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstGlobalTrace (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMIPvrstGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIPvrstGlobalTrace != AST_SNMP_TRUE) &&
        (i4TestValFsMIPvrstGlobalTrace != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIPvrstGlobalTrace);
    return SNMP_FAILURE;
#endif

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsMIPvrstGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstGlobalDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMIPvrstGlobalDebug)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIPvrstGlobalDebug != AST_SNMP_TRUE) &&
        (i4TestValFsMIPvrstGlobalDebug != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIPvrstGlobalDebug);
    return SNMP_FAILURE;
#endif

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPvrstGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPvrstGlobalTrace (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIPvrstGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPvrstGlobalDebug (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFuturePvrstTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFuturePvrstTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFuturePvrstTable (INT4 i4FsMIFuturePvrstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIFuturePvrstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFuturePvrstTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFuturePvrstTable (INT4 *pi4FsMIFuturePvrstContextId)
{
    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIFuturePvrstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFuturePvrstTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                nextFsMIFuturePvrstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFuturePvrstTable (INT4 i4FsMIFuturePvrstContextId,
                                     INT4 *pi4NextFsMIFuturePvrstContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIFuturePvrstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIFuturePvrstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstSystemControl
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstSystemControl (INT4 i4FsMIFuturePvrstContextId,
                              INT4 *pi4RetValFsMIPvrstSystemControl)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstSystemControl (pi4RetValFsMIPvrstSystemControl);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstModuleStatus
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstModuleStatus (INT4 i4FsMIFuturePvrstContextId,
                             INT4 *pi4RetValFsMIPvrstModuleStatus)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstModuleStatus (pi4RetValFsMIPvrstModuleStatus);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstNoOfActiveInstances
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstNoOfActiveInstances
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstNoOfActiveInstances (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 *pi4RetValFsMIPvrstNoOfActiveInstances)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstNoOfActiveInstances
        (pi4RetValFsMIPvrstNoOfActiveInstances);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstBrgAddress
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstBrgAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstBrgAddress (INT4 i4FsMIFuturePvrstContextId,
                           tMacAddr * pRetValFsMIPvrstBrgAddress)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstBrgAddress (pRetValFsMIPvrstBrgAddress);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstUpCount
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstUpCount (INT4 i4FsMIFuturePvrstContextId,
                        UINT4 *pu4RetValFsMIPvrstUpCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstUpCount (pu4RetValFsMIPvrstUpCount);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstDownCount
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstDownCount (INT4 i4FsMIFuturePvrstContextId,
                          UINT4 *pu4RetValFsMIPvrstDownCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstDownCount (pu4RetValFsMIPvrstDownCount);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPathCostDefaultType
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstPathCostDefaultType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPathCostDefaultType (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 *pi4RetValFsMIPvrstPathCostDefaultType)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstPathCostDefaultType
        (pi4RetValFsMIPvrstPathCostDefaultType);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstDynamicPathCostCalculation
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstDynamicPathCostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstDynamicPathCostCalculation (INT4 i4FsMIFuturePvrstContextId,
                                           INT4
                                           *pi4RetValFsMIPvrstDynamicPathCostCalculation)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstDynamicPathCostCalculation
        (pi4RetValFsMIPvrstDynamicPathCostCalculation);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstTrace
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstTrace (INT4 i4FsMIFuturePvrstContextId,
                      INT4 *pi4RetValFsMIPvrstTrace)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstTrace (pi4RetValFsMIPvrstTrace);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstDebug
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstDebug (INT4 i4FsMIFuturePvrstContextId,
                      INT4 *pi4RetValFsMIPvrstDebug)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstDebug (pi4RetValFsMIPvrstDebug);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstBufferOverFlowCount
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstBufferOverFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstBufferOverFlowCount (INT4 i4FsMIFuturePvrstContextId,
                                    UINT4
                                    *pu4RetValFsMIPvrstBufferOverFlowCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstBufferOverFlowCount
        (pu4RetValFsMIPvrstBufferOverFlowCount);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstMemAllocFailureCount
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstMemAllocFailureCount (INT4 i4FsMIFuturePvrstContextId,
                                     UINT4
                                     *pu4RetValFsMIPvrstMemAllocFailureCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstMemAllocFailureCount
        (pu4RetValFsMIPvrstMemAllocFailureCount);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstContextName
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstContextName (INT4 i4FsMIFuturePvrstContextId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIPvrstContextName)
{
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];

    AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

    if (AST_IS_VC_VALID (i4FsMIFuturePvrstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    VcmGetAliasName (i4FsMIFuturePvrstContextId, au1ContextName);

    AST_MEMCPY (pRetValFsMIPvrstContextName->pu1_OctetList,
                au1ContextName, AST_STRLEN (au1ContextName));
    pRetValFsMIPvrstContextName->i4_Length = AST_STRLEN (au1ContextName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg (INT4 i4FsMIFuturePvrstContextId,
                                           INT4
                                           *pi4RetValFsMIPvrstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstCalcPortPathCostOnSpeedChg
        (pi4RetValFsMIPvrstCalcPortPathCostOnSpeedChg);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstGlobalBpduGuard
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object
                retValFsMIPvrstGlobalBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstGlobalBpduGuard (INT4 i4FsMIFuturePvrstContextId,
                                INT4 *pi4RetValFsMIPvrstGlobalBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstGlobalBpduGuard (pi4RetValFsMIPvrstGlobalBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstForceProtocolVersion
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstForceProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstForceProtocolVersion (INT4 i4FsMIFuturePvrstContextId,
                                     INT4
                                     *pi4RetValFsMIPvrstForceProtocolVersion)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsPvrstForceProtocolVersion
        (pi4RetValFsMIPvrstForceProtocolVersion);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstFlushInterval
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object
                retValFsMIPvrstFlushInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstFlushInterval (INT4 i4FsMIFuturePvrstContextId,
                              INT4 *pi4RetValFsMIPvrstFlushInterval)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsPvrstFlushInterval (pi4RetValFsMIPvrstFlushInterval);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPvrstSystemControl
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstSystemControl (INT4 i4FsMIFuturePvrstContextId,
                              INT4 i4SetValFsMIPvrstSystemControl)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstSystemControl (i4SetValFsMIPvrstSystemControl);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstModuleStatus
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstModuleStatus (INT4 i4FsMIFuturePvrstContextId,
                             INT4 i4SetValFsMIPvrstModuleStatus)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstModuleStatus (i4SetValFsMIPvrstModuleStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPathCostDefaultType
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstPathCostDefaultType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPathCostDefaultType (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4SetValFsMIPvrstPathCostDefaultType)
{
    UNUSED_PARAM (i4FsMIFuturePvrstContextId);
    UNUSED_PARAM (i4SetValFsMIPvrstPathCostDefaultType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstDynamicPathCostCalculation
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstDynamicPathCostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstDynamicPathCostCalculation (INT4 i4FsMIFuturePvrstContextId,
                                           INT4
                                           i4SetValFsMIPvrstDynamicPathCostCalculation)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_PVRST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstDynamicPathCostCalculation
        (i4SetValFsMIPvrstDynamicPathCostCalculation);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstTrace
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstTrace (INT4 i4FsMIFuturePvrstContextId,
                      INT4 i4SetValFsMIPvrstTrace)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_PVRST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstTrace (i4SetValFsMIPvrstTrace);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstDebug
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstDebug (INT4 i4FsMIFuturePvrstContextId,
                      INT4 i4SetValFsMIPvrstDebug)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_PVRST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstDebug (i4SetValFsMIPvrstDebug);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstCalcPortPathCostOnSpeedChg (INT4 i4FsMIFuturePvrstContextId,
                                           INT4
                                           i4SetValFsMIPvrstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_PVRST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstCalcPortPathCostOnSpeedChg
        (i4SetValFsMIPvrstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstGlobalBpduGuard
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object
                setValFsMIPvrstGlobalBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstGlobalBpduGuard (INT4 i4FsMIFuturePvrstContextId,
                                INT4 i4SetValFsMIPvrstGlobalBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstGlobalBpduGuard (i4SetValFsMIPvrstGlobalBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstForceProtocolVersion
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstForceProtocolVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstForceProtocolVersion (INT4 i4FsMIFuturePvrstContextId,
                                     INT4 i4SetValFsMIPvrstForceProtocolVersion)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsPvrstForceProtocolVersion
        (i4SetValFsMIPvrstForceProtocolVersion);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstFlushInterval
 Input       :  The Indices
               FsMIFuturePvrstContextId

               The Object
               setValFsMIPvrstFlushInterval
 Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstFlushInterval (INT4 i4FsMIFuturePvrstContextId,
                              INT4 i4SetValFsMIPvrstFlushInterval)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsPvrstFlushInterval (i4SetValFsMIPvrstFlushInterval);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstSystemControl
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstSystemControl (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIFuturePvrstContextId,
                                 INT4 i4TestValFsMIPvrstSystemControl)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstSystemControl (pu4ErrorCode,
                                              i4TestValFsMIPvrstSystemControl);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstModuleStatus
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstModuleStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIFuturePvrstContextId,
                                INT4 i4TestValFsMIPvrstModuleStatus)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstModuleStatus (pu4ErrorCode,
                                             i4TestValFsMIPvrstModuleStatus);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPathCostDefaultType
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstPathCostDefaultType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPathCostDefaultType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIFuturePvrstContextId,
                                       INT4
                                       i4TestValFsMIPvrstPathCostDefaultType)
{
    UNUSED_PARAM (i4FsMIFuturePvrstContextId);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMIPvrstPathCostDefaultType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstDynamicPathCostCalculation
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstDynamicPathCostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstDynamicPathCostCalculation (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIFuturePvrstContextId,
                                              INT4
                                              i4TestValFsMIPvrstDynamicPathCostCalculation)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstDynamicPathCostCalculation (pu4ErrorCode,
                                                           i4TestValFsMIPvrstDynamicPathCostCalculation);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstTrace
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstTrace (UINT4 *pu4ErrorCode, INT4 i4FsMIFuturePvrstContextId,
                         INT4 i4TestValFsMIPvrstTrace)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstTrace (pu4ErrorCode, i4TestValFsMIPvrstTrace);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstDebug
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstDebug (UINT4 *pu4ErrorCode, INT4 i4FsMIFuturePvrstContextId,
                         INT4 i4TestValFsMIPvrstDebug)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstDebug (pu4ErrorCode, i4TestValFsMIPvrstDebug);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIFuturePvrstContextId,
                                              INT4
                                              i4TestValFsMIPvrstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (i4FsMIFuturePvrstContextId != AST_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstCalcPortPathCostOnSpeedChg (pu4ErrorCode,
                                                           i4TestValFsMIPvrstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstGlobalBpduGuard
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object
                testValFsMIPvrstGlobalBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstGlobalBpduGuard (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIFuturePvrstContextId,
                                   INT4 i4TestValFsMIPvrstGlobalBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstGlobalBpduGuard (pu4ErrorCode,
                                         i4TestValFsMIPvrstGlobalBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstForceProtocolVersion
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstForceProtocolVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIFuturePvrstContextId,
                                        INT4
                                        i4TestValFsMIPvrstForceProtocolVersion)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2FsPvrstForceProtocolVersion (pu4ErrorCode,
                                              i4TestValFsMIPvrstForceProtocolVersion);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhTestv2FsMIPvrstFlushInterval
  Input       :  The Indices
                 FsMIFuturePvrstContextId
 
                 The Object
                 testValFsMIPvrstFlushInterval
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPvrstFlushInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIFuturePvrstContextId,
                                 INT4 i4TestValFsMIPvrstFlushInterval)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RST_SUCCESS != AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId))
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2FsPvrstFlushInterval (pu4ErrorCode,
                                       i4TestValFsMIPvrstFlushInterval);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFuturePvrstTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFuturePvrstTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFuturePvrstPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFuturePvrstPortTable
 Input       :  The Indices
                FsMIPvrstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFuturePvrstPortTable (INT4 i4FsMIPvrstPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsFuturePvrstPortTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFuturePvrstPortTable
 Input       :  The Indices
                FsMIPvrstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFuturePvrstPortTable (INT4 *pi4FsMIPvrstPort)
{
    return nmhGetNextIndexFsMIFuturePvrstPortTable (0, pi4FsMIPvrstPort);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFuturePvrstPortTable
 Input       :  The Indices
                FsMIPvrstPort
                nextFsMIPvrstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFuturePvrstPortTable (INT4 i4FsMIPvrstPort,
                                         INT4 *pi4NextFsMIPvrstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIPvrstPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsPvrstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                            (pAstPortEntry)))
            {
                *pi4NextFsMIPvrstPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIPvrstPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                &&
                (AstIsPvrstStartedInContext
                 (AST_IFENTRY_CONTEXT_ID (pAstPortEntry))))
            {
                *pi4NextFsMIPvrstPort = pAstPortEntry->u4IfIndex;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortAdminEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortAdminEdgeStatus (INT4 i4FsMIPvrstPort,
                                    INT4 *pi4RetValFsMIPvrstPortAdminEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortAdminEdgeStatus ((INT4) u2LocalPortId,
                                                 pi4RetValFsMIPvrstPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortOperEdgePortStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortOperEdgePortStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortOperEdgePortStatus (INT4 i4FsMIPvrstPort,
                                       INT4
                                       *pi4RetValFsMIPvrstPortOperEdgePortStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortOperEdgePortStatus ((INT4) u2LocalPortId,
                                                    pi4RetValFsMIPvrstPortOperEdgePortStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstBridgeDetectionSemState
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstBridgeDetectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstBridgeDetectionSemState (INT4 i4FsMIPvrstPort,
                                        INT4
                                        *pi4RetValFsMIPvrstBridgeDetectionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstBridgeDetectionSemState ((INT4) u2LocalPortId,
                                                     pi4RetValFsMIPvrstBridgeDetectionSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortEnabledStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortEnabledStatus (INT4 i4FsMIPvrstPort,
                                  INT4 *pi4RetValFsMIPvrstPortEnabledStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortEnabledStatus ((INT4) u2LocalPortId,
                                               pi4RetValFsMIPvrstPortEnabledStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstRootGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstRootGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstRootGuard (INT4 i4FsMIPvrstPort,
                          INT4 *pi4RetValFsMIPvrstRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstRootGuard ((INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstBpduGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstBpduGuard (INT4 i4FsMIPvrstPort,
                          INT4 *pi4RetValFsMIPvrstBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstBpduGuard ((INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstEncapType
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstEncapType (INT4 i4FsMIPvrstPort,
                          INT4 *pi4RetValFsMIPvrstEncapType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstEncapType ((INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstEncapType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortAdminPointToPoint (INT4 i4FsMIPvrstPort,
                                      INT4
                                      *pi4RetValFsMIPvrstPortAdminPointToPoint)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortAdminPointToPoint ((INT4) u2LocalPortId,
                                                   pi4RetValFsMIPvrstPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortOperPointToPoint
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortOperPointToPoint (INT4 i4FsMIPvrstPort,
                                     INT4
                                     *pi4RetValFsMIPvrstPortOperPointToPoint)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortOperPointToPoint ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIPvrstPortOperPointToPoint);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortInvalidBpdusRcvd
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortInvalidBpdusRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortInvalidBpdusRcvd (INT4 i4FsMIPvrstPort,
                                     UINT4
                                     *pu4RetValFsMIPvrstPortInvalidBpdusRcvd)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortInvalidBpdusRcvd ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIPvrstPortInvalidBpdusRcvd);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortInvalidConfigBpduRxCount (INT4 i4FsMIPvrstPort,
                                             UINT4
                                             *pu4RetValFsMIPvrstPortInvalidConfigBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortInvalidConfigBpduRxCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIPvrstPortInvalidConfigBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortInvalidTcnBpduRxCount (INT4 i4FsMIPvrstPort,
                                          UINT4
                                          *pu4RetValFsMIPvrstPortInvalidTcnBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortInvalidTcnBpduRxCount ((INT4) u2LocalPortId,
                                                       pu4RetValFsMIPvrstPortInvalidTcnBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortRowStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortRowStatus (INT4 i4FsMIPvrstPort,
                              INT4 *pi4RetValFsMIPvrstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstPortRowStatus ((INT4) u2LocalPortId,
                                    pi4RetValFsMIPvrstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsMIPvrstRootInconsistentState
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                retValFsPvrstRootInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstRootInconsistentState (INT4 i4FsMIPvrstPort,
                                      INT4
                                      *pi4RetValFsMIPvrstRootInconsistentState)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_PVRST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: PVRST Module not Enabled!\n");
        *pi4RetValFsMIPvrstRootInconsistentState = AST_FALSE;
        return SNMP_SUCCESS;

    }
    if (AstValidatePortEntry (u2LocalPortId) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2LocalPortId);

    if (pCommPortInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIPvrstRootInconsistentState = pCommPortInfo->bRootInconsistent;

    AstReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortBpduGuardAction
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortBpduGuardAction (INT4 i4FsMIPvrstPort,
                                    INT4 *pi4RetValFsMIPvrstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortBpduGuardAction ((INT4) u2LocalPortId,
                                                 pi4RetValFsMIPvrstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortAdminEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortAdminEdgeStatus (INT4 i4FsMIPvrstPort,
                                    INT4 i4SetValFsMIPvrstPortAdminEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortAdminEdgeStatus ((INT4) u2LocalPortId,
                                                 i4SetValFsMIPvrstPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortEnabledStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortEnabledStatus (INT4 i4FsMIPvrstPort,
                                  INT4 i4SetValFsMIPvrstPortEnabledStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortEnabledStatus ((INT4) u2LocalPortId,
                                               i4SetValFsMIPvrstPortEnabledStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstRootGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstRootGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstRootGuard (INT4 i4FsMIPvrstPort, INT4 i4SetValFsMIPvrstRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstRootGuard ((INT4) u2LocalPortId,
                                       i4SetValFsMIPvrstRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstBpduGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstBpduGuard (INT4 i4FsMIPvrstPort, INT4 i4SetValFsMIPvrstBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstBpduGuard ((INT4) u2LocalPortId,
                                       i4SetValFsMIPvrstBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstEncapType
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstEncapType (INT4 i4FsMIPvrstPort, INT4 i4SetValFsMIPvrstEncapType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstEncapType ((INT4) u2LocalPortId,
                                       i4SetValFsMIPvrstEncapType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortAdminPointToPoint (INT4 i4FsMIPvrstPort,
                                      INT4
                                      i4SetValFsMIPvrstPortAdminPointToPoint)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortAdminPointToPoint ((INT4) u2LocalPortId,
                                                   i4SetValFsMIPvrstPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortRowStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortRowStatus (INT4 i4FsMIPvrstPort,
                              INT4 i4SetValFsMIPvrstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortRowStatus (u2LocalPortId,
                                           i4SetValFsMIPvrstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortBpduGuardAction
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortBpduGuardAction (INT4 i4FsMIPvrstPort,
                                    INT4 i4SetValFsMIPvrstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortBpduGuardAction ((INT4) u2LocalPortId,
                                                 i4SetValFsMIPvrstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortAdminEdgeStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortAdminEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortAdminEdgeStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIPvrstPort,
                                       INT4
                                       i4TestValFsMIPvrstPortAdminEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstPortAdminEdgeStatus (pu4ErrorCode, (INT4) u2LocalPortId,
                                             i4TestValFsMIPvrstPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortEnabledStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortEnabledStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                                     INT4 i4TestValFsMIPvrstPortEnabledStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstPortEnabledStatus (pu4ErrorCode, (INT4) u2LocalPortId,
                                           i4TestValFsMIPvrstPortEnabledStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstRootGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstRootGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                             INT4 i4TestValFsMIPvrstRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstRootGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                   i4TestValFsMIPvrstRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstBpduGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstBpduGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                             INT4 i4TestValFsMIPvrstBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstBpduGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                   i4TestValFsMIPvrstBpduGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstEncapType
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstEncapType (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                             INT4 i4TestValFsMIPvrstEncapType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstEncapType (pu4ErrorCode, (INT4) u2LocalPortId,
                                   i4TestValFsMIPvrstEncapType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortAdminPointToPoint
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortAdminPointToPoint (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIPvrstPort,
                                         INT4
                                         i4TestValFsMIPvrstPortAdminPointToPoint)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstPortAdminPointToPoint (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIPvrstPortAdminPointToPoint);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortRowStatus
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                                 INT4 i4TestValFsMIPvrstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIPvrstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstPortRowStatus (pu4ErrorCode, u2LocalPortId,
                                              i4TestValFsMIPvrstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortBpduGuardAction
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortBpduGuardAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortBpduGuardAction (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIPvrstPort,
                                       INT4
                                       i4TestValFsMIPvrstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstPortBpduGuardAction (pu4ErrorCode, (INT4) u2LocalPortId,
                                             i4TestValFsMIPvrstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFuturePvrstPortTable
 Input       :  The Indices
                FsMIPvrstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFuturePvrstPortTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPvrstInstBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPvrstInstBridgeTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPvrstInstBridgeTable (INT4
                                                  i4FsMIFuturePvrstContextId,
                                                  INT4 i4FsMIPvrstInstVlanId)
{
    if (AST_IS_VC_VALID (i4FsMIFuturePvrstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsPvrstInstBridgeTable
            (i4FsMIPvrstInstVlanId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPvrstInstBridgeTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPvrstInstBridgeTable (INT4 *pi4FsMIFuturePvrstContextId,
                                          INT4 *pi4FsMIPvrstInstVlanId)
{
    return
        nmhGetNextIndexFsMIPvrstInstBridgeTable (0,
                                                 pi4FsMIFuturePvrstContextId,
                                                 0, pi4FsMIPvrstInstVlanId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPvrstInstBridgeTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                nextFsMIFuturePvrstContextId
                FsMIPvrstInstVlanId
                nextFsMIPvrstInstVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPvrstInstBridgeTable (INT4 i4FsMIFuturePvrstContextId,
                                         INT4 *pi4NextFsMIFuturePvrstContextId,
                                         INT4 i4FsMIPvrstInstVlanId,
                                         INT4 *pi4NextFsMIPvrstInstVlanId)
{
    UINT4               u4PrevContextId;
    UINT4               u4ContextId = i4FsMIFuturePvrstContextId;

    if ((UINT4) i4FsMIFuturePvrstContextId >= AST_SIZING_CONTEXT_COUNT)
    {
        return SNMP_FAILURE;
    }

    if (AST_CONTEXT_INFO (i4FsMIFuturePvrstContextId) == NULL)
    {
        /* Given context is not valid - get the next valid context */

        if (AstGetNextActiveContext ((UINT4) i4FsMIFuturePvrstContextId,
                                     &u4ContextId) == RST_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIPvrstInstVlanId = 1;
    }

    do
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexFsPvrstInstBridgeTable (i4FsMIPvrstInstVlanId,
                                                   pi4NextFsMIPvrstInstVlanId)
            == SNMP_SUCCESS)
        {
            *pi4NextFsMIFuturePvrstContextId = (INT4) u4ContextId;

            AstReleaseContext ();

            return SNMP_SUCCESS;
        }

        u4PrevContextId = u4ContextId;
        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIPvrstInstVlanId = 1;

        AstReleaseContext ();

    }
    while (AstGetNextActiveContext (u4PrevContextId, &u4ContextId)
           != RST_FAILURE);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstBridgePriority
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstBridgePriority (INT4 i4FsMIFuturePvrstContextId,
                                   INT4 i4FsMIPvrstInstVlanId,
                                   INT4 *pi4RetValFsMIPvrstInstBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstBridgePriority
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstBridgePriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstRootCost
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstRootCost (INT4 i4FsMIFuturePvrstContextId,
                             INT4 i4FsMIPvrstInstVlanId,
                             INT4 *pi4RetValFsMIPvrstInstRootCost)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstRootCost
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstRootCost);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstRootPort
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstRootPort (INT4 i4FsMIFuturePvrstContextId,
                             INT4 i4FsMIPvrstInstVlanId,
                             INT4 *pi4RetValFsMIPvrstInstRootPort)
{
    INT4                i4LocalPortNum = AST_INIT_VAL;
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstRootPort
        (i4FsMIPvrstInstVlanId, &i4LocalPortNum);

    if ((i1RetVal != SNMP_FAILURE) && (i4LocalPortNum != 0))
    {
        *pi4RetValFsMIPvrstInstRootPort =
            (INT4) AST_GET_IFINDEX (i4LocalPortNum);
    }
    else
    {
        *pi4RetValFsMIPvrstInstRootPort = 0;
    }

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstBridgeMaxAge (INT4 i4FsMIFuturePvrstContextId,
                                 INT4 i4FsMIPvrstInstVlanId,
                                 INT4 *pi4RetValFsMIPvrstInstBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstBridgeMaxAge
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstBridgeMaxAge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstBridgeHelloTime (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    INT4 *pi4RetValFsMIPvrstInstBridgeHelloTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstBridgeHelloTime
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstBridgeForwardDelay (INT4 i4FsMIFuturePvrstContextId,
                                       INT4 i4FsMIPvrstInstVlanId,
                                       INT4
                                       *pi4RetValFsMIPvrstInstBridgeForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstBridgeForwardDelay
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstBridgeForwardDelay);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstHoldTime
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstHoldTime (INT4 i4FsMIFuturePvrstContextId,
                             INT4 i4FsMIPvrstInstVlanId,
                             INT4 *pi4RetValFsMIPvrstInstHoldTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstHoldTime
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstHoldTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstTxHoldCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstTxHoldCount (INT4 i4FsMIFuturePvrstContextId,
                                INT4 i4FsMIPvrstInstVlanId,
                                INT4 *pi4RetValFsMIPvrstInstTxHoldCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstTxHoldCount
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstTxHoldCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstTimeSinceTopologyChange
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstTimeSinceTopologyChange (INT4 i4FsMIFuturePvrstContextId,
                                            INT4 i4FsMIPvrstInstVlanId,
                                            UINT4
                                            *pu4RetValFsMIPvrstInstTimeSinceTopologyChange)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstTimeSinceTopologyChange
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstTimeSinceTopologyChange);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstTopChanges
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstTopChanges (INT4 i4FsMIFuturePvrstContextId,
                               INT4 i4FsMIPvrstInstVlanId,
                               UINT4 *pu4RetValFsMIPvrstInstTopChanges)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstTopChanges
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstTopChanges);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstNewRootCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstNewRootCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstNewRootCount (INT4 i4FsMIFuturePvrstContextId,
                                 INT4 i4FsMIPvrstInstVlanId,
                                 UINT4 *pu4RetValFsMIPvrstInstNewRootCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstNewRootCount
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstNewRootCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstInstanceUpCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstInstanceUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstInstanceUpCount (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    UINT4
                                    *pu4RetValFsMIPvrstInstInstanceUpCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstInstanceUpCount
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstInstanceUpCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstInstanceDownCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstInstanceDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstInstanceDownCount (INT4 i4FsMIFuturePvrstContextId,
                                      INT4 i4FsMIPvrstInstVlanId,
                                      UINT4
                                      *pu4RetValFsMIPvrstInstInstanceDownCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstInstanceDownCount
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstInstanceDownCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortRoleSelSemState
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstPortRoleSelSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRoleSelSemState (INT4 i4FsMIFuturePvrstContextId,
                                        INT4 i4FsMIPvrstInstVlanId,
                                        INT4
                                        *pi4RetValFsMIPvrstInstPortRoleSelSemState)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortRoleSelSemState
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstPortRoleSelSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstDesignatedRoot
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstDesignatedRoot (INT4 i4FsMIFuturePvrstContextId,
                                   INT4 i4FsMIPvrstInstVlanId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIPvrstInstDesignatedRoot)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstDesignatedRoot
        (i4FsMIPvrstInstVlanId, pRetValFsMIPvrstInstDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstRootMaxAge
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstRootMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstRootMaxAge (INT4 i4FsMIFuturePvrstContextId,
                               INT4 i4FsMIPvrstInstVlanId,
                               INT4 *pi4RetValFsMIPvrstInstRootMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstRootMaxAge
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstRootMaxAge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstRootHelloTime
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstRootHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstRootHelloTime (INT4 i4FsMIFuturePvrstContextId,
                                  INT4 i4FsMIPvrstInstVlanId,
                                  INT4 *pi4RetValFsMIPvrstInstRootHelloTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstRootHelloTime
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstRootHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstRootForwardDelay
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstInstRootForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstRootForwardDelay (INT4 i4FsMIFuturePvrstContextId,
                                     INT4 i4FsMIPvrstInstVlanId,
                                     INT4
                                     *pi4RetValFsMIPvrstInstRootForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstRootForwardDelay
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstRootForwardDelay);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhGetFsMIPvrstInstFlushIndicationThreshold
  Input       :  The Indices
                 FsMIFuturePvrstContextId
                 FsMIPvrstInstVlanId

                 The Object
                 retValFsMIPvrstInstFlushIndicationThreshold
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPvrstInstFlushIndicationThreshold (INT4 i4FsMIFuturePvrstContextId,
                                             INT4 i4FsMIPvrstInstVlanId,
                                             INT4
                                             *pi4RetValFsMIPvrstInstFlushIndicationThreshold)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstFlushIndicationThreshold
        (i4FsMIPvrstInstVlanId, pi4RetValFsMIPvrstInstFlushIndicationThreshold);

    AstReleaseContext ();

    return i1RetVal;

}

 /****************************************************************************
  Function    :  nmhGetFsMIPvrstInstTotalFlushCount
  Input       :  The Indices
                 FsMIFuturePvrstContextId
                 FsMIPvrstInstVlanId

                 The Object
                 retValFsMIPvrstInstTotalFlushCount
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstTotalFlushCount (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    UINT4
                                    *pu4RetValFsMIPvrstInstTotalFlushCount)
{

    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstTotalFlushCount
        (i4FsMIPvrstInstVlanId, pu4RetValFsMIPvrstInstTotalFlushCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIPvrstInstOldDesignatedRoot
 * * Input       :  The Indices
 * *                FsMIFuturePvrstContextId
 * *                FsMIPvrstInstVlanId
 * *
 * *                The Object
 * *                retValFsMIPvrstInstOldDesignatedRoot
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstOldDesignatedRoot (INT4 i4FsMIFuturePvrstContextId,
                                      INT4 i4FsMIPvrstInstVlanId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIPvrstInstOldDesignatedRoot)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstOldDesignatedRoot
        (i4FsMIPvrstInstVlanId, pRetValFsMIPvrstInstOldDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstBridgePriority
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                setValFsMIPvrstInstBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstBridgePriority (INT4 i4FsMIFuturePvrstContextId,
                                   INT4 i4FsMIPvrstInstVlanId,
                                   INT4 i4SetValFsMIPvrstInstBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstBridgePriority
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstBridgePriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                setValFsMIPvrstInstBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstBridgeMaxAge (INT4 i4FsMIFuturePvrstContextId,
                                 INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4SetValFsMIPvrstInstBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstBridgeMaxAge
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstBridgeMaxAge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                setValFsMIPvrstInstBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstBridgeHelloTime (INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4SetValFsMIPvrstInstBridgeHelloTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstBridgeHelloTime
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                setValFsMIPvrstInstBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstBridgeForwardDelay (INT4 i4FsMIFuturePvrstContextId,
                                       INT4 i4FsMIPvrstInstVlanId,
                                       INT4
                                       i4SetValFsMIPvrstInstBridgeForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstBridgeForwardDelay
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstBridgeForwardDelay);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstTxHoldCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                setValFsMIPvrstInstTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstTxHoldCount (INT4 i4FsMIFuturePvrstContextId,
                                INT4 i4FsMIPvrstInstVlanId,
                                INT4 i4SetValFsMIPvrstInstTxHoldCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstTxHoldCount
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstTxHoldCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
Function    :  nmhSetFsMIPvrstInstFlushIndicationThreshold
Input       :  The Indices
               FsMIFuturePvrstContextId
               FsMIPvrstInstVlanId

               The Object
               setValFsMIPvrstInstFlushIndicationThreshold
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstFlushIndicationThreshold (INT4 i4FsMIFuturePvrstContextId,
                                             INT4 i4FsMIPvrstInstVlanId,
                                             INT4
                                             i4SetValFsMIPvrstInstFlushIndicationThreshold)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstInstFlushIndicationThreshold
        (i4FsMIPvrstInstVlanId, i4SetValFsMIPvrstInstFlushIndicationThreshold);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstBridgePriority
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                testValFsMIPvrstInstBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstBridgePriority (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIFuturePvrstContextId,
                                      INT4 i4FsMIPvrstInstVlanId,
                                      INT4 i4TestValFsMIPvrstInstBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstBridgePriority
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstBridgePriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstBridgeMaxAge
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                testValFsMIPvrstInstBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstBridgeMaxAge (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIFuturePvrstContextId,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4TestValFsMIPvrstInstBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstBridgeMaxAge
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstBridgeMaxAge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstBridgeHelloTime
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                testValFsMIPvrstInstBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstBridgeHelloTime (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIFuturePvrstContextId,
                                       INT4 i4FsMIPvrstInstVlanId,
                                       INT4
                                       i4TestValFsMIPvrstInstBridgeHelloTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstBridgeHelloTime
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstBridgeForwardDelay
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                testValFsMIPvrstInstBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIFuturePvrstContextId,
                                          INT4 i4FsMIPvrstInstVlanId,
                                          INT4
                                          i4TestValFsMIPvrstInstBridgeForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstBridgeForwardDelay
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstBridgeForwardDelay);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstTxHoldCount
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId

                The Object 
                testValFsMIPvrstInstTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstTxHoldCount (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIFuturePvrstContextId,
                                   INT4 i4FsMIPvrstInstVlanId,
                                   INT4 i4TestValFsMIPvrstInstTxHoldCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstTxHoldCount
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstTxHoldCount);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
  Function    :  nmhTestv2FsMIPvrstInstFlushIndicationThreshold
  Input       :  The Indices
                 FsMIFuturePvrstContextId
                 FsMIPvrstInstVlanId

                 The Object
                 testValFsMIPvrstInstFlushIndicationThreshold
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstFlushIndicationThreshold (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIFuturePvrstContextId,
                                                INT4 i4FsMIPvrstInstVlanId,
                                                INT4
                                                i4TestValFsMIPvrstInstFlushIndicationThreshold)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstInstFlushIndicationThreshold
        (pu4ErrorCode, i4FsMIPvrstInstVlanId,
         i4TestValFsMIPvrstInstFlushIndicationThreshold);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPvrstInstBridgeTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                FsMIPvrstInstVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPvrstInstBridgeTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPvrstInstPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPvrstInstPortTable
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPvrstInstPortTable (INT4 i4FsMIPvrstInstVlanId,
                                                INT4 i4FsMIPvrstInstPortIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPvrstInstPortTable (i4FsMIPvrstInstVlanId,
                                                      (INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPvrstInstPortTable
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPvrstInstPortTable (INT4 *pi4FsMIPvrstInstVlanId,
                                        INT4 *pi4FsMIPvrstInstPortIndex)
{
    return (nmhGetNextIndexFsMIPvrstInstPortTable (0, pi4FsMIPvrstInstVlanId,
                                                   0,
                                                   pi4FsMIPvrstInstPortIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPvrstInstPortTable
 Input       :  The Indices
                FsMIPvrstInstVlanId
                nextFsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex
                nextFsMIPvrstInstPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPvrstInstPortTable (INT4 i4FsMIPvrstInstVlanId,
                                       INT4 *pi4NextFsMIPvrstInstVlanId,
                                       INT4 i4FsMIPvrstInstPortIndex,
                                       INT4 *pi4NextFsMIPvrstInstPortIndex)
{
    UINT1               u1InstFlag = PVRST_FALSE;
    UINT1               u1PortFlag = PVRST_FALSE;
    INT1                i1RetVal = PVRST_TRUE;

    if ((i4FsMIPvrstInstVlanId < 0) || (i4FsMIPvrstInstPortIndex < 0))
    {
        return SNMP_FAILURE;
    }
    if (i4FsMIPvrstInstVlanId == 0)
    {
        i4FsMIPvrstInstVlanId++;
    }
    if (!(AstIsPvrstStartedInContext (AST_DEFAULT_CONTEXT)))
    {
        return SNMP_FAILURE;
    }
    if (PvrstValidateInstanceEntry (i4FsMIPvrstInstVlanId) == PVRST_SUCCESS)
    {
        u1InstFlag = PVRST_TRUE;

        if (PvrstGetNextIndexFsMIPvrstInstPort (i4FsMIPvrstInstVlanId,
                                                (UINT2)
                                                i4FsMIPvrstInstPortIndex,
                                                pi4NextFsMIPvrstInstPortIndex)
            == PVRST_SUCCESS)
        {
            u1PortFlag = PVRST_TRUE;
        }

        if ((u1InstFlag == PVRST_TRUE) && (u1PortFlag == PVRST_TRUE))
        {
            *pi4NextFsMIPvrstInstVlanId = i4FsMIPvrstInstVlanId;
            if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                         (tVlanId) i4FsMIPvrstInstVlanId,
                                         (UINT4) *pi4NextFsMIPvrstInstPortIndex)
                == OSIX_FALSE)
            {
                i1RetVal =
                    nmhGetNextIndexFsMIPvrstInstPortTable
                    (*pi4NextFsMIPvrstInstVlanId, pi4NextFsMIPvrstInstVlanId,
                     *pi4NextFsMIPvrstInstPortIndex,
                     pi4NextFsMIPvrstInstPortIndex);
            }

            return SNMP_SUCCESS;
        }
        else
        {
            u1InstFlag = PVRST_FALSE;
            u1PortFlag = PVRST_FALSE;
            while (PvrstGetNextIndexPvrstInstBridgeTable (i4FsMIPvrstInstVlanId,
                                                          pi4NextFsMIPvrstInstVlanId)
                   == PVRST_SUCCESS)
            {
                u1InstFlag = PVRST_TRUE;
                i4FsMIPvrstInstPortIndex = AST_INIT_VAL;

                if (PvrstGetNextIndexFsMIPvrstInstPort
                    (*pi4NextFsMIPvrstInstVlanId,
                     (UINT2) i4FsMIPvrstInstPortIndex,
                     pi4NextFsMIPvrstInstPortIndex) == PVRST_SUCCESS)
                {
                    u1PortFlag = (UINT1) PVRST_TRUE;
                }
                else
                {
                    i4FsMIPvrstInstVlanId = *pi4NextFsMIPvrstInstVlanId;
                }
                if ((u1PortFlag == (UINT1) PVRST_TRUE)
                    && (u1InstFlag == (UINT1) PVRST_TRUE))
                {
                    if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                                 (tVlanId) *
                                                 pi4NextFsMIPvrstInstVlanId,
                                                 (UINT4)
                                                 *pi4NextFsMIPvrstInstPortIndex)
                        == OSIX_FALSE)
                    {
                        i1RetVal =
                            nmhGetNextIndexFsMIPvrstInstPortTable
                            (*pi4NextFsMIPvrstInstVlanId,
                             pi4NextFsMIPvrstInstVlanId,
                             *pi4NextFsMIPvrstInstPortIndex,
                             pi4NextFsMIPvrstInstPortIndex);
                    }

                    return SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        if (PvrstGetNextIndexPvrstInstBridgeTable (i4FsMIPvrstInstVlanId,
                                                   pi4NextFsMIPvrstInstVlanId)
            == PVRST_SUCCESS)
        {
            u1InstFlag = PVRST_TRUE;

            if (i4FsMIPvrstInstPortIndex == AST_INIT_VAL)
            {
                i4FsMIPvrstInstPortIndex++;
            }
            if (PvrstValidatePortEntry
                (i4FsMIPvrstInstPortIndex,
                 *pi4NextFsMIPvrstInstVlanId) == PVRST_SUCCESS)
            {
                *pi4NextFsMIPvrstInstPortIndex = i4FsMIPvrstInstPortIndex;
                u1PortFlag = PVRST_TRUE;
            }
            else
            {
                if (PvrstGetNextIndexFsMIPvrstInstPort
                    (*pi4NextFsMIPvrstInstVlanId,
                     (UINT2) i4FsMIPvrstInstPortIndex,
                     pi4NextFsMIPvrstInstPortIndex) == PVRST_SUCCESS)
                {
                    u1PortFlag = PVRST_TRUE;
                }
            }
            if ((u1InstFlag == PVRST_TRUE) && (u1PortFlag == PVRST_TRUE))
            {
                if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                             (tVlanId) *
                                             pi4NextFsMIPvrstInstVlanId,
                                             (UINT4)
                                             *pi4NextFsMIPvrstInstPortIndex) ==
                    OSIX_FALSE)
                {

                    i1RetVal =
                        nmhGetNextIndexFsMIPvrstInstPortTable
                        (*pi4NextFsMIPvrstInstVlanId,
                         pi4NextFsMIPvrstInstVlanId,
                         *pi4NextFsMIPvrstInstPortIndex,
                         pi4NextFsMIPvrstInstPortIndex);
                }

                return SNMP_SUCCESS;
            }
        }
    }
    UNUSED_PARAM (i1RetVal);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortEnableStatus
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortEnableStatus (INT4 i4FsMIPvrstInstVlanId,
                                     INT4 i4FsMIPvrstInstPortIndex,
                                     INT4
                                     *pi4RetValFsMIPvrstInstPortEnableStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortEnableStatus (i4FsMIPvrstInstVlanId,
                                           (INT4) u2LocalPortId,
                                           pi4RetValFsMIPvrstInstPortEnableStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortPathCost (INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4FsMIPvrstInstPortIndex,
                                 INT4 *pi4RetValFsMIPvrstInstPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortPathCost (i4FsMIPvrstInstVlanId,
                                       (INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstInstPortPathCost);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortPriority
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortPriority (INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4FsMIPvrstInstPortIndex,
                                 INT4 *pi4RetValFsMIPvrstInstPortPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortPriority (i4FsMIPvrstInstVlanId,
                                       (INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstInstPortPriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortDesignatedRoot
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortDesignatedRoot (INT4 i4FsMIPvrstInstVlanId,
                                       INT4 i4FsMIPvrstInstPortIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIPvrstInstPortDesignatedRoot)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortDesignatedRoot (i4FsMIPvrstInstVlanId,
                                             (INT4) u2LocalPortId,
                                             pRetValFsMIPvrstInstPortDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortDesignatedBridge
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortDesignatedBridge (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsMIPvrstInstPortDesignatedBridge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortDesignatedBridge (i4FsMIPvrstInstVlanId,
                                               (INT4) u2LocalPortId,
                                               pRetValFsMIPvrstInstPortDesignatedBridge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortDesignatedPort
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortDesignatedPort (INT4 i4FsMIPvrstInstVlanId,
                                       INT4 i4FsMIPvrstInstPortIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIPvrstInstPortDesignatedPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortDesignatedPort (i4FsMIPvrstInstVlanId,
                                             (INT4) u2LocalPortId,
                                             pRetValFsMIPvrstInstPortDesignatedPort);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortOperVersion
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortOperVersion (INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4FsMIPvrstInstPortIndex,
                                    INT4 *pi4RetValFsMIPvrstInstPortOperVersion)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortOperVersion (i4FsMIPvrstInstVlanId,
                                          (INT4) u2LocalPortId,
                                          pi4RetValFsMIPvrstInstPortOperVersion);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProtocolMigration (INT4 i4FsMIPvrstInstVlanId,
                                          INT4 i4FsMIPvrstInstPortIndex,
                                          INT4
                                          *pi4RetValFsMIPvrstInstPortProtocolMigration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortProtocolMigration (i4FsMIPvrstInstVlanId,
                                                (INT4) u2LocalPortId,
                                                pi4RetValFsMIPvrstInstPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortState (INT4 i4FsMIPvrstInstVlanId,
                              INT4 i4FsMIPvrstInstPortIndex,
                              INT4 *pi4RetValFsMIPvrstInstPortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortState (i4FsMIPvrstInstVlanId,
                                    (INT4) u2LocalPortId,
                                    pi4RetValFsMIPvrstInstPortState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortForwardTransitions
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortForwardTransitions (INT4 i4FsMIPvrstInstVlanId,
                                           INT4 i4FsMIPvrstInstPortIndex,
                                           UINT4
                                           *pu4RetValFsMIPvrstInstPortForwardTransitions)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortForwardTransitions (i4FsMIPvrstInstVlanId,
                                                 (INT4) u2LocalPortId,
                                                 pu4RetValFsMIPvrstInstPortForwardTransitions);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortReceivedBpdus
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortReceivedBpdus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortReceivedBpdus (INT4 i4FsMIPvrstInstVlanId,
                                      INT4 i4FsMIPvrstInstPortIndex,
                                      UINT4
                                      *pu4RetValFsMIPvrstInstPortReceivedBpdus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortReceivedBpdus (i4FsMIPvrstInstVlanId,
                                            (INT4) u2LocalPortId,
                                            pu4RetValFsMIPvrstInstPortReceivedBpdus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortRxConfigBpduCount
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRxConfigBpduCount (INT4 i4FsMIPvrstInstVlanId,
                                          INT4 i4FsMIPvrstInstPortIndex,
                                          UINT4
                                          *pu4RetValFsMIPvrstInstPortRxConfigBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortRxConfigBpduCount (i4FsMIPvrstInstVlanId,
                                                (INT4) u2LocalPortId,
                                                pu4RetValFsMIPvrstInstPortRxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortRxTcnBpduCount
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRxTcnBpduCount (INT4 i4FsMIPvrstInstVlanId,
                                       INT4 i4FsMIPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsMIPvrstInstPortRxTcnBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortRxTcnBpduCount (i4FsMIPvrstInstVlanId,
                                             (INT4) u2LocalPortId,
                                             pu4RetValFsMIPvrstInstPortRxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortTransmittedBpdus
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortTransmittedBpdus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTransmittedBpdus (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsMIPvrstInstPortTransmittedBpdus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortTransmittedBpdus (i4FsMIPvrstInstVlanId,
                                               (INT4) u2LocalPortId,
                                               pu4RetValFsMIPvrstInstPortTransmittedBpdus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortTxConfigBpduCount
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTxConfigBpduCount (INT4 i4FsMIPvrstInstVlanId,
                                          INT4 i4FsMIPvrstInstPortIndex,
                                          UINT4
                                          *pu4RetValFsMIPvrstInstPortTxConfigBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortTxConfigBpduCount (i4FsMIPvrstInstVlanId,
                                                (INT4) u2LocalPortId,
                                                pu4RetValFsMIPvrstInstPortTxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortTxTcnBpduCount
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTxTcnBpduCount (INT4 i4FsMIPvrstInstVlanId,
                                       INT4 i4FsMIPvrstInstPortIndex,
                                       UINT4
                                       *pu4RetValFsMIPvrstInstPortTxTcnBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortTxTcnBpduCount (i4FsMIPvrstInstVlanId,
                                             (INT4) u2LocalPortId,
                                             pu4RetValFsMIPvrstInstPortTxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortTxSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortTxSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTxSemState (INT4 i4FsMIPvrstInstVlanId,
                                   INT4 i4FsMIPvrstInstPortIndex,
                                   INT4 *pi4RetValFsMIPvrstInstPortTxSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortTxSemState (i4FsMIPvrstInstVlanId,
                                         (INT4) u2LocalPortId,
                                         pi4RetValFsMIPvrstInstPortTxSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortProtMigrationSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortProtMigrationSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProtMigrationSemState (INT4 i4FsMIPvrstInstVlanId,
                                              INT4 i4FsMIPvrstInstPortIndex,
                                              INT4
                                              *pi4RetValFsMIPvrstInstPortProtMigrationSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortProtMigrationSemState (i4FsMIPvrstInstVlanId,
                                                    (INT4) u2LocalPortId,
                                                    pi4RetValFsMIPvrstInstPortProtMigrationSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstProtocolMigrationCount
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstProtocolMigrationCount (INT4 i4FsMIPvrstInstVlanId,
                                           INT4 i4FsMIPvrstInstPortIndex,
                                           UINT4
                                           *pu4RetValFsMIPvrstInstProtocolMigrationCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstProtocolMigrationCount (i4FsMIPvrstInstVlanId,
                                                 (INT4) u2LocalPortId,
                                                 pu4RetValFsMIPvrstInstProtocolMigrationCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortRole
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRole (INT4 i4FsMIPvrstInstVlanId,
                             INT4 i4FsMIPvrstInstPortIndex,
                             INT4 *pi4RetValFsMIPvrstInstPortRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortRole (i4FsMIPvrstInstVlanId,
                                   (INT4) u2LocalPortId,
                                   pi4RetValFsMIPvrstInstPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstCurrentPortRole
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstCurrentPortRole (INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4FsMIPvrstInstPortIndex,
                                    INT4 *pi4RetValFsMIPvrstInstCurrentPortRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstCurrentPortRole (i4FsMIPvrstInstVlanId,
                                          (INT4) u2LocalPortId,
                                          pi4RetValFsMIPvrstInstCurrentPortRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortInfoSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortInfoSemState (INT4 i4FsMIPvrstInstVlanId,
                                     INT4 i4FsMIPvrstInstPortIndex,
                                     INT4
                                     *pi4RetValFsMIPvrstInstPortInfoSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortInfoSemState (i4FsMIPvrstInstVlanId,
                                           (INT4) u2LocalPortId,
                                           pi4RetValFsMIPvrstInstPortInfoSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortRoleTransitionSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRoleTransitionSemState (INT4 i4FsMIPvrstInstVlanId,
                                               INT4 i4FsMIPvrstInstPortIndex,
                                               INT4
                                               *pi4RetValFsMIPvrstInstPortRoleTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortRoleTransitionSemState (i4FsMIPvrstInstVlanId,
                                                     (INT4) u2LocalPortId,
                                                     pi4RetValFsMIPvrstInstPortRoleTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortStateTransitionSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortStateTransitionSemState (INT4 i4FsMIPvrstInstVlanId,
                                                INT4 i4FsMIPvrstInstPortIndex,
                                                INT4
                                                *pi4RetValFsMIPvrstInstPortStateTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortStateTransitionSemState (i4FsMIPvrstInstVlanId,
                                                      (INT4) u2LocalPortId,
                                                      pi4RetValFsMIPvrstInstPortStateTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortTopologyChangeSemState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTopologyChangeSemState (INT4 i4FsMIPvrstInstVlanId,
                                               INT4 i4FsMIPvrstInstPortIndex,
                                               INT4
                                               *pi4RetValFsMIPvrstInstPortTopologyChangeSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortTopologyChangeSemState (i4FsMIPvrstInstVlanId,
                                                     (INT4) u2LocalPortId,
                                                     pi4RetValFsMIPvrstInstPortTopologyChangeSemState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortEffectivePortState
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortEffectivePortState (INT4 i4FsMIPvrstInstVlanId,
                                           INT4 i4FsMIPvrstInstPortIndex,
                                           INT4
                                           *pi4RetValFsMIPvrstInstPortEffectivePortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortEffectivePortState (i4FsMIPvrstInstVlanId,
                                                 (INT4) u2LocalPortId,
                                                 pi4RetValFsMIPvrstInstPortEffectivePortState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortHelloTime
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortHelloTime (INT4 i4FsMIPvrstInstVlanId,
                                  INT4 i4FsMIPvrstInstPortIndex,
                                  INT4 *pi4RetValFsMIPvrstInstPortHelloTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortHelloTime (i4FsMIPvrstInstVlanId,
                                        (INT4) u2LocalPortId,
                                        pi4RetValFsMIPvrstInstPortHelloTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortMaxAge
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortMaxAge (INT4 i4FsMIPvrstInstVlanId,
                               INT4 i4FsMIPvrstInstPortIndex,
                               INT4 *pi4RetValFsMIPvrstInstPortMaxAge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortMaxAge (i4FsMIPvrstInstVlanId,
                                     (INT4) u2LocalPortId,
                                     pi4RetValFsMIPvrstInstPortMaxAge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortForwardDelay
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortForwardDelay (INT4 i4FsMIPvrstInstVlanId,
                                     INT4 i4FsMIPvrstInstPortIndex,
                                     INT4
                                     *pi4RetValFsMIPvrstInstPortForwardDelay)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortForwardDelay (i4FsMIPvrstInstVlanId,
                                           (INT4) u2LocalPortId,
                                           pi4RetValFsMIPvrstInstPortForwardDelay);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortHoldTime
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortHoldTime (INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4FsMIPvrstInstPortIndex,
                                 INT4 *pi4RetValFsMIPvrstInstPortHoldTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortHoldTime (i4FsMIPvrstInstVlanId,
                                       (INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstInstPortHoldTime);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                retValFsMIPvrstInstPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortAdminPathCost (INT4 i4FsMIPvrstInstVlanId,
                                      INT4 i4FsMIPvrstInstPortIndex,
                                      INT4
                                      *pi4RetValFsMIPvrstInstPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortAdminPathCost (i4FsMIPvrstInstVlanId,
                                            (INT4) u2LocalPortId,
                                            pi4RetValFsMIPvrstInstPortAdminPathCost);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstPortEnableStatus
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                setValFsMIPvrstInstPortEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstPortEnableStatus (INT4 i4FsMIPvrstInstVlanId,
                                     INT4 i4FsMIPvrstInstPortIndex,
                                     INT4 i4SetValFsMIPvrstInstPortEnableStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstInstPortEnableStatus (i4FsMIPvrstInstVlanId,
                                           (INT4) u2LocalPortId,
                                           i4SetValFsMIPvrstInstPortEnableStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstPortPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                setValFsMIPvrstInstPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstPortPathCost (INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4FsMIPvrstInstPortIndex,
                                 INT4 i4SetValFsMIPvrstInstPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstInstPortPathCost (i4FsMIPvrstInstVlanId,
                                       (INT4) u2LocalPortId,
                                       i4SetValFsMIPvrstInstPortPathCost);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstPortPriority
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                setValFsMIPvrstInstPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstPortPriority (INT4 i4FsMIPvrstInstVlanId,
                                 INT4 i4FsMIPvrstInstPortIndex,
                                 INT4 i4SetValFsMIPvrstInstPortPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstInstPortPriority (i4FsMIPvrstInstVlanId,
                                       (INT4) u2LocalPortId,
                                       i4SetValFsMIPvrstInstPortPriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                setValFsMIPvrstInstPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstPortProtocolMigration (INT4 i4FsMIPvrstInstVlanId,
                                          INT4 i4FsMIPvrstInstPortIndex,
                                          INT4
                                          i4SetValFsMIPvrstInstPortProtocolMigration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstInstPortProtocolMigration (i4FsMIPvrstInstVlanId,
                                                (INT4) u2LocalPortId,
                                                i4SetValFsMIPvrstInstPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                setValFsMIPvrstInstPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstInstPortAdminPathCost (INT4 i4FsMIPvrstInstVlanId,
                                      INT4 i4FsMIPvrstInstPortIndex,
                                      INT4
                                      i4SetValFsMIPvrstInstPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsPvrstInstPortAdminPathCost (i4FsMIPvrstInstVlanId,
                                            (INT4) u2LocalPortId,
                                            i4SetValFsMIPvrstInstPortAdminPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstPortEnableStatus
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                testValFsMIPvrstInstPortEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstPortEnableStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIPvrstInstVlanId,
                                        INT4 i4FsMIPvrstInstPortIndex,
                                        INT4
                                        i4TestValFsMIPvrstInstPortEnableStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstInstPortEnableStatus (pu4ErrorCode,
                                              i4FsMIPvrstInstVlanId,
                                              (INT4) u2LocalPortId,
                                              i4TestValFsMIPvrstInstPortEnableStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstPortPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                testValFsMIPvrstInstPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstPortPathCost (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4FsMIPvrstInstPortIndex,
                                    INT4 i4TestValFsMIPvrstInstPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstInstPortPathCost (pu4ErrorCode, i4FsMIPvrstInstVlanId,
                                          (INT4) u2LocalPortId,
                                          i4TestValFsMIPvrstInstPortPathCost);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstPortPriority
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                testValFsMIPvrstInstPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstPortPriority (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIPvrstInstVlanId,
                                    INT4 i4FsMIPvrstInstPortIndex,
                                    INT4 i4TestValFsMIPvrstInstPortPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstInstPortPriority (pu4ErrorCode, i4FsMIPvrstInstVlanId,
                                          (INT4) u2LocalPortId,
                                          i4TestValFsMIPvrstInstPortPriority);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstPortProtocolMigration
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                testValFsMIPvrstInstPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstPortProtocolMigration (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIPvrstInstVlanId,
                                             INT4 i4FsMIPvrstInstPortIndex,
                                             INT4
                                             i4TestValFsMIPvrstInstPortProtocolMigration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstInstPortProtocolMigration (pu4ErrorCode,
                                                   i4FsMIPvrstInstVlanId,
                                                   (INT4) u2LocalPortId,
                                                   i4TestValFsMIPvrstInstPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstInstPortAdminPathCost
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex

                The Object 
                testValFsMIPvrstInstPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstInstPortAdminPathCost (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         INT4
                                         i4TestValFsMIPvrstInstPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstInstPortAdminPathCost (pu4ErrorCode,
                                               i4FsMIPvrstInstVlanId,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIPvrstInstPortAdminPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIPvrstInstPortTable
 Input       :  The Indices
                FsMIPvrstInstVlanId
                FsMIPvrstInstPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIPvrstInstPortTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsPvrstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                retValFsMIFsPvrstSetGlobalTrapOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsPvrstSetGlobalTrapOption (INT4
                                      *pi4RetValFsMIFsPvrstSetGlobalTrapOption)
{
    *pi4RetValFsMIFsPvrstSetGlobalTrapOption = AST_GLOBAL_TRAP;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstGlobalErrTrapType
 Input       :  The Indices

                The Object 
                retValFsMIPvrstGlobalErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstGlobalErrTrapType (INT4 *pi4RetValFsMIPvrstGlobalErrTrapType)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFsPvrstErrTrapType (pi4RetValFsMIPvrstGlobalErrTrapType);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsPvrstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                setValFsMIFsPvrstSetGlobalTrapOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsPvrstSetGlobalTrapOption (INT4
                                      i4SetValFsMIFsPvrstSetGlobalTrapOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, AST_INIT_VAL, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);
    AST_GLOBAL_TRAP = (UINT4) i4SetValFsMIFsPvrstSetGlobalTrapOption;

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIFsPvrstSetGlobalTrapOption,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ",
                      i4SetValFsMIFsPvrstSetGlobalTrapOption));
    AstSelectContext (u4CurrContextId);

    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsPvrstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                testValFsMIFsPvrstSetGlobalTrapOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsPvrstSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMIFsPvrstSetGlobalTrapOption)
{
    if ((i4TestValFsMIFsPvrstSetGlobalTrapOption < 0) ||
        (i4TestValFsMIFsPvrstSetGlobalTrapOption > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsPvrstSetGlobalTrapOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsPvrstSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsPvrstTrapsControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsPvrstTrapsControlTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsPvrstTrapsControlTable (INT4
                                                      i4FsMIFuturePvrstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIFuturePvrstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsPvrstTrapsControlTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsPvrstTrapsControlTable (INT4 *pi4FsMIFuturePvrstContextId)
{
    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIFuturePvrstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsPvrstTrapsControlTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
                nextFsMIFuturePvrstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsPvrstTrapsControlTable (INT4 i4FsMIFuturePvrstContextId,
                                             INT4
                                             *pi4NextFsMIFuturePvrstContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIFuturePvrstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIFuturePvrstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstSetTraps
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstSetTraps (INT4 i4FsMIFuturePvrstContextId,
                         INT4 *pi4RetValFsMIPvrstSetTraps)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstSetTraps (pi4RetValFsMIPvrstSetTraps);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstGenTrapType
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                retValFsMIPvrstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstGenTrapType (INT4 i4FsMIFuturePvrstContextId,
                            INT4 *pi4RetValFsMIPvrstGenTrapType)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstGenTrapType (pi4RetValFsMIPvrstGenTrapType);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIPvrstSetTraps
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                setValFsMIPvrstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstSetTraps (INT4 i4FsMIFuturePvrstContextId,
                         INT4 i4SetValFsMIPvrstSetTraps)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstSetTraps (i4SetValFsMIPvrstSetTraps);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstSetTraps
 Input       :  The Indices
                FsMIFuturePvrstContextId

                The Object 
                testValFsMIPvrstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstSetTraps (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIFuturePvrstContextId,
                            INT4 i4TestValFsMIPvrstSetTraps)
{
    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIFuturePvrstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstSetTraps (pu4ErrorCode, i4TestValFsMIPvrstSetTraps);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsPvrstTrapsControlTable
 Input       :  The Indices
                FsMIFuturePvrstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsPvrstTrapsControlTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIPvrstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPvrstPortTrapNotificationTable (INT4
                                                            i4FsMIPvrstPortTrapIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsPvrstPortTrapNotificationTable ((INT4)
                                                                  u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPvrstPortTrapNotificationTable (INT4
                                                    *pi4FsMIPvrstPortTrapIndex)
{
    return nmhGetNextIndexFsMIPvrstPortTrapNotificationTable (0,
                                                              pi4FsMIPvrstPortTrapIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPvrstPortTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                nextFsMIPvrstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPvrstPortTrapNotificationTable (INT4
                                                   i4FsMIPvrstPortTrapIndex,
                                                   INT4
                                                   *pi4NextFsMIPvrstPortTrapIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIPvrstPortTrapIndex);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsPvrstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                            (pAstPortEntry)))
            {
                *pi4NextFsMIPvrstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIPvrstPortTrapIndex < (INT4) pAstPortEntry->u4IfIndex)
                && (AstIsPvrstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                                (pAstPortEntry))))
            {
                *pi4NextFsMIPvrstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortMigrationType
 Input       :  The Indices
                FsMIPvrstPortTrapIndex

                The Object 
                retValFsMIPvrstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortMigrationType (INT4 i4FsMIPvrstPortTrapIndex,
                                  INT4 *pi4RetValFsMIPvrstPortMigrationType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortMigrationType ((INT4) u2LocalPortId,
                                               pi4RetValFsMIPvrstPortMigrationType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPktErrType
 Input       :  The Indices
                FsMIPvrstPortTrapIndex

                The Object 
                retValFsMIPvrstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPktErrType (INT4 i4FsMIPvrstPortTrapIndex,
                           INT4 *pi4RetValFsMIPvrstPktErrType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPktErrType ((INT4) u2LocalPortId,
                                        pi4RetValFsMIPvrstPktErrType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPktErrVal
 Input       :  The Indices
                FsMIPvrstPortTrapIndex

                The Object 
                retValFsMIPvrstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPktErrVal (INT4 i4FsMIPvrstPortTrapIndex,
                          INT4 *pi4RetValFsMIPvrstPktErrVal)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPktErrVal ((INT4) u2LocalPortId,
                                       pi4RetValFsMIPvrstPktErrVal);

    AstReleaseContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIPvrstPortRoleTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                FsMIPvrstInstVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPvrstPortRoleTrapNotificationTable (INT4
                                                                i4FsMIPvrstPortTrapIndex,
                                                                INT4
                                                                i4FsMIPvrstInstVlanId)
{
    /* This table holds the Port role information necessary to *
     * generate trap during role changes. Here, we directly   *
     * call the corresponding Validate, getfirst and Getnext   *
     * routine for another table, having the same Indices.     */
    INT1                i1RetVal;

    i1RetVal = nmhValidateIndexInstanceFsMIPvrstInstPortTable
        (i4FsMIPvrstInstVlanId, i4FsMIPvrstPortTrapIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                FsMIPvrstInstVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPvrstPortRoleTrapNotificationTable (INT4
                                                        *pi4FsMIPvrstPortTrapIndex,
                                                        INT4
                                                        *pi4FsMIPvrstInstVlanId)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFirstIndexFsMIPvrstInstPortTable
        (pi4FsMIPvrstInstVlanId, pi4FsMIPvrstPortTrapIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIPvrstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                nextFsMIPvrstPortTrapIndex
                FsMIPvrstInstVlanId
                nextFsMIPvrstInstVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPvrstPortRoleTrapNotificationTable (INT4
                                                       i4FsMIPvrstPortTrapIndex,
                                                       INT4
                                                       *pi4NextFsMIPvrstPortTrapIndex,
                                                       INT4
                                                       i4FsMIPvrstInstVlanId,
                                                       INT4
                                                       *pi4NextFsMIPvrstInstVlanId)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetNextIndexFsMIPvrstInstPortTable (i4FsMIPvrstInstVlanId,
                                                      pi4NextFsMIPvrstInstVlanId,
                                                      i4FsMIPvrstPortTrapIndex,
                                                      pi4NextFsMIPvrstPortTrapIndex);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortRoleType
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstPortRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortRoleType (INT4 i4FsMIPvrstPortTrapIndex,
                             INT4 i4FsMIPvrstInstVlanId,
                             INT4 *pi4RetValFsMIPvrstPortRoleType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortRoleType ((INT4) u2LocalPortId,
                                          i4FsMIPvrstInstVlanId,
                                          pi4RetValFsMIPvrstPortRoleType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstOldRoleType
 Input       :  The Indices
                FsMIPvrstPortTrapIndex
                FsMIPvrstInstVlanId

                The Object 
                retValFsMIPvrstOldRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstOldRoleType (INT4 i4FsMIPvrstPortTrapIndex,
                            INT4 i4FsMIPvrstInstVlanId,
                            INT4 *pi4RetValFsMIPvrstOldRoleType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIPvrstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstOldRoleType ((INT4) u2LocalPortId,
                                         i4FsMIPvrstInstVlanId,
                                         pi4RetValFsMIPvrstOldRoleType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortLoopGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortLoopGuard (INT4 i4FsMIPvrstPort,
                              INT4 *pi4RetValFsMIPvrstPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortLoopGuard ((INT4) u2LocalPortId,
                                           pi4RetValFsMIPvrstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortLoopInconsistentState
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortLoopInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortLoopInconsistentState (INT4 i4FsMIPvrstPort,
                                          INT4
                                          *pi4RetValFsMIPvrstPortLoopInconsistentState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortLoopInconsistentState
        ((INT4) u2LocalPortId, pi4RetValFsMIPvrstPortLoopInconsistentState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortEnableBPDURx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                retValFsMIPvrstPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortEnableBPDURx (INT4 i4FsMIPvrstPort,
                                 INT4 *pi4RetValFsMIPvrstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortEnableBPDURx ((INT4) u2LocalPortId,
                                              pi4RetValFsMIPvrstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                retValFsMIPvrstPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortEnableBPDUTx (INT4 i4FsMIPvrstPort,
                                 INT4 *pi4RetValFsMIPvrstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortEnableBPDUTx ((INT4) u2LocalPortId,
                                              pi4RetValFsMIPvrstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstBpduFilter
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                retValFsMIPvrstBpduFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstBpduFilter (INT4 i4FsMIPvrstPort,
                           INT4 *pi4RetValFsMIPvrstBpduFilter)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstBpduFilter ((INT4) u2LocalPortId,
                                        pi4RetValFsMIPvrstBpduFilter);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 *  Function    :  nmhGetFsMIPvrstPortAutoEdge
 *  Input       :  The Indices
 *                 FsMIPvrstPort
 * 
 *                 The Object
 *                 retValFsMIPvrstPortAutoEdge
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstPortAutoEdge (INT4 i4FsMIPvrstPort,
                             INT4 *pi4RetValFsMIPvrstPortAutoEdge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortAutoEdge
        ((INT4) u2LocalPortId, pi4RetValFsMIPvrstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortBpduInconsistentState
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortBpduInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortBpduInconsistentState (INT4 i4FsMIPvrstPort,
                                          INT4
                                          *pi4RetValFsMIPvrstPortBpduInconsistentState)
{

    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortBpduInconsistentState
        ((INT4) u2LocalPortId, pi4RetValFsMIPvrstPortBpduInconsistentState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortTypeInconsistentState
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortTypeInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortTypeInconsistentState (INT4 i4FsMIPvrstPort,
                                          INT4
                                          *pi4RetValFsMIPvrstPortTypeInconsistentState)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortTypeInconsistentState
        ((INT4) u2LocalPortId, pi4RetValFsMIPvrstPortTypeInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIPvrstPortPVIDInconsistentState
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                retValFsMIPvrstPortPVIDInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIPvrstPortPVIDInconsistentState (INT4 i4FsMIPvrstPort,
                                          INT4
                                          *pi4RetValFsMIPvrstPortPVIDInconsistentState)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortPVIDInconsistentState
        ((INT4) u2LocalPortId, pi4RetValFsMIPvrstPortPVIDInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortLoopGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                testValFsMIPvrstPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortLoopGuard (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIPvrstPort,
                                 INT4 i4TestValFsMIPvrstPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsPvrstPortLoopGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                       i4TestValFsMIPvrstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortEnableBPDURx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                testValFsMIPvrstPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortEnableBPDURx (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIPvrstPort,
                                    INT4 i4TestValFsMIPvrstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstPortEnableBPDURx (pu4ErrorCode,
                                                 (INT4) u2LocalPortId,
                                                 i4TestValFsMIPvrstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                testValFsMIPvrstPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortEnableBPDUTx (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIPvrstPort,
                                    INT4 i4TestValFsMIPvrstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstPortEnableBPDUTx (pu4ErrorCode,
                                                 (INT4) u2LocalPortId,
                                                 i4TestValFsMIPvrstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIPvrstBpduFilter
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                testValFsMIPvrstBpduFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIPvrstBpduFilter (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                              INT4 i4TestValFsMIPvrstBpduFilter)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstBpduFilter (pu4ErrorCode,
                                           (INT4) u2LocalPortId,
                                           i4TestValFsMIPvrstBpduFilter);

    AstReleaseContext ();

    return i1RetVal;
}

 /****************************************************************************
 * Function    :  nmhTestv2FsMIPvrstPortAutoEdge
 * Input       :  The Indices
 *                FsMIPvrstPort
 *
 *                The Object
 *                testValFsMIPvrstPortAutoEdge
 * Output      :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsMIPvrstPortAutoEdge (UINT4 *pu4ErrorCode, INT4 i4FsMIPvrstPort,
                                INT4 i4TestValFsMIPvrstPortAutoEdge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsPvrstPortAutoEdge (pu4ErrorCode, (INT4) u2LocalPortId,
                                             i4TestValFsMIPvrstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortLoopGuard
 Input       :  The Indices
                FsMIPvrstPort

                The Object 
                setValFsMIPvrstPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortLoopGuard (INT4 i4FsMIPvrstPort,
                              INT4 i4SetValFsMIPvrstPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortLoopGuard ((INT4) u2LocalPortId,
                                           i4SetValFsMIPvrstPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortEnableBPDURx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                setValFsMIPvrstPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortEnableBPDURx (INT4 i4FsMIPvrstPort,
                                 INT4 i4SetValFsMIPvrstPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortEnableBPDURx ((INT4) u2LocalPortId,
                                              i4SetValFsMIPvrstPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortEnableBPDUTx
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                setValFsMIPvrstPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortEnableBPDUTx (INT4 i4FsMIPvrstPort,
                                 INT4 i4SetValFsMIPvrstPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortEnableBPDUTx ((INT4) u2LocalPortId,
                                              i4SetValFsMIPvrstPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstBpduFilter
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                setValFsMIPvrstBpduFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstBpduFilter (INT4 i4FsMIPvrstPort,
                           INT4 i4SetValFsMIPvrstBpduFilter)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstBpduFilter ((INT4) u2LocalPortId,
                                        i4SetValFsMIPvrstBpduFilter);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIPvrstPortAutoEdge
 Input       :  The Indices
                FsMIPvrstPort

                The Object
                setValFsMIPvrstPortAutoEdge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIPvrstPortAutoEdge (INT4 i4FsMIPvrstPort,
                             INT4 i4SetValFsMIPvrstPortAutoEdge)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsPvrstPortAutoEdge ((INT4) u2LocalPortId,
                                          i4SetValFsMIPvrstPortAutoEdge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIPvrstPortRcvInfoWhileExpCount
 * * Input       :  The Indices
 * *                FsMIPvrstPort
 * *
 * *                The Object
 * *                retValFsMIPvrstPortRcvInfoWhileExpCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstPortRcvInfoWhileExpCount (INT4 i4FsMIPvrstPort,
                                         UINT4
                                         *pu4RetValFsMIPvrstPortRcvInfoWhileExpCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortRcvInfoWhileExpCount
        ((INT4) u2LocalPortId, pu4RetValFsMIPvrstPortRcvInfoWhileExpCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIPvrstPortRcvInfoWhileExpTimeStamp
 * * Input       :  The Indices
 * *                FsMIPvrstPort
 * *
 * *                The Object
 * *                retValFsMIPvrstPortRcvInfoWhileExpTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstPortRcvInfoWhileExpTimeStamp (INT4 i4FsMIPvrstPort,
                                             UINT4
                                             *pu4RetValFsMIPvrstPortRcvInfoWhileExpTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortRcvInfoWhileExpTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIPvrstPortRcvInfoWhileExpTimeStamp);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIPvrstPortImpStateOccurCount
 * * Input       :  The Indices
 * *                FsMIPvrstPort
 * *
 * *                The Object
 * *                retValFsMIPvrstPortImpStateOccurCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstPortImpStateOccurCount (INT4 i4FsMIPvrstPort,
                                       UINT4
                                       *pu4RetValFsMIPvrstPortImpStateOccurCount)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortImpStateOccurCount
        ((INT4) u2LocalPortId, pu4RetValFsMIPvrstPortImpStateOccurCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIPvrstPortImpStateOccurTimeStamp
 * * Input       :  The Indices
 * *                FsMIPvrstPort
 * *
 * *                The Object
 * *                retValFsMIPvrstPortImpStateOccurTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstPortImpStateOccurTimeStamp (INT4 i4FsMIPvrstPort,
                                           UINT4
                                           *pu4RetValFsMIPvrstPortImpStateOccurTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstPortImpStateOccurTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIPvrstPortImpStateOccurTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortOldPortState
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortOldPortState
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortOldPortState (INT4 i4FsMIPvrstInstVlanId,
                                     INT4 i4FsMIPvrstInstPortIndex,
                                     INT4
                                     *pi4RetValFsMIPvrstInstPortOldPortState)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsPvrstInstPortOldPortState (i4FsMIPvrstInstVlanId,
                                           (INT4) u2LocalPortId,
                                           pi4RetValFsMIPvrstInstPortOldPortState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortLoopInconsistentState
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 * 
 *                The Object
 *                retValFsMIPvrstInstPortLoopInconsistentState
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortLoopInconsistentState (INT4 i4FsMIPvrstInstVlanId,
                                              INT4 i4FsMIPvrstInstPortIndex,
                                              INT4
                                              *pi4RetValFsMIPvrstInstPortLoopInconsistentState)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortLoopInconsistentState
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pi4RetValFsMIPvrstInstPortLoopInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsMIPvrstInstPortRootInconsistentState
 *  Input       :  The Indices
 *                 FsMIPvrstInstVlanId
 *                 FsMIPvrstInstPortIndex
 *  
 *                 The Object
 *                 retValFsMIPvrstInstPortRootInconsistentState
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortRootInconsistentState (INT4 i4FsMIPvrstInstVlanId,
                                              INT4 i4FsMIPvrstInstPortIndex,
                                              INT4
                                              *pi4RetValFsMIPvrstInstPortRootInconsistentState)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortRootInconsistentState
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pi4RetValFsMIPvrstInstPortRootInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortTCDetectedCount
 *Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *               The Object
 *               retValFsMIPvrstInstPortTCDetectedCount
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTCDetectedCount (INT4 i4FsMIPvrstInstVlanId,
                                        INT4 i4FsMIPvrstInstPortIndex,
                                        UINT4
                                        *pu4RetValFsMIPvrstInstPortTCDetectedCount)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortTCDetectedCount
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortTCDetectedCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortTCReceivedCount
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 * 
 *                The Object
 *                retValFsMIPvrstInstPortTCReceivedCount
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTCReceivedCount (INT4 i4FsMIPvrstInstVlanId,
                                        INT4 i4FsMIPvrstInstPortIndex,
                                        UINT4
                                        *pu4RetValFsMIPvrstInstPortTCReceivedCount)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortTCReceivedCount
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortTCReceivedCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortTCDetectedTimeStamp
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortTCDetectedTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTCDetectedTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                            INT4 i4FsMIPvrstInstPortIndex,
                                            UINT4
                                            *pu4RetValFsMIPvrstInstPortTCDetectedTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortTCDetectedTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortTCDetectedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortTCReceivedTimeStamp
 * Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 * 
 *                The Object
 *                retValFsMIPvrstInstPortTCReceivedTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortTCReceivedTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                            INT4 i4FsMIPvrstInstPortIndex,
                                            UINT4
                                            *pu4RetValFsMIPvrstInstPortTCReceivedTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortTCReceivedTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortTCReceivedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortProposalPktsSent
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 * 
 *                The Object
 *                retValFsMIPvrstInstPortProposalPktsSent
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProposalPktsSent (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsMIPvrstInstPortProposalPktsSent)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortProposalPktsSent
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortProposalPktsSent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortProposalPktsRcvd
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortProposalPktsRcvd
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProposalPktsRcvd (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsMIPvrstInstPortProposalPktsRcvd)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortProposalPktsRcvd
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortProposalPktsRcvd);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsMIPvrstInstPortProposalPktSentTimeStamp
 * Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 *
 *               The Object
 *               retValFsMIPvrstInstPortProposalPktSentTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProposalPktSentTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                                 INT4 i4FsMIPvrstInstPortIndex,
                                                 UINT4
                                                 *pu4RetValFsMIPvrstInstPortProposalPktSentTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortProposalPktSentTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortProposalPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortProposalPktRcvdTimeStamp
 *Input       :  The Indices
 *                FsMIPvrstInstVlanId
 *                FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortProposalPktRcvdTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortProposalPktRcvdTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                                 INT4 i4FsMIPvrstInstPortIndex,
                                                 UINT4
                                                 *pu4RetValFsMIPvrstInstPortProposalPktRcvdTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortProposalPktRcvdTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortProposalPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortAgreementPktSent
 *Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortAgreementPktSent
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortAgreementPktSent (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsMIPvrstInstPortAgreementPktSent)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortAgreementPktSent
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortAgreementPktSent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortAgreementPktRcvd
 *Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *               The Object
 *               retValFsMIPvrstInstPortAgreementPktRcvd
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortAgreementPktRcvd (INT4 i4FsMIPvrstInstVlanId,
                                         INT4 i4FsMIPvrstInstPortIndex,
                                         UINT4
                                         *pu4RetValFsMIPvrstInstPortAgreementPktRcvd)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortAgreementPktRcvd
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortAgreementPktRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortAgreementPktSentTimeStamp
 *Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *               The Object
 *               retValFsMIPvrstInstPortAgreementPktSentTimeStamp
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortAgreementPktSentTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                                  INT4 i4FsMIPvrstInstPortIndex,
                                                  UINT4
                                                  *pu4RetValFsMIPvrstInstPortAgreementPktSentTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortAgreementPktSentTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortAgreementPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 *Function    :  nmhGetFsMIPvrstInstPortAgreementPktRcvdTimeStamp
 *Input       :  The Indices
 *               FsMIPvrstInstVlanId
 *               FsMIPvrstInstPortIndex
 *
 *                The Object
 *                retValFsMIPvrstInstPortAgreementPktRcvdTimeStamp
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIPvrstInstPortAgreementPktRcvdTimeStamp (INT4 i4FsMIPvrstInstVlanId,
                                                  INT4 i4FsMIPvrstInstPortIndex,
                                                  UINT4
                                                  *pu4RetValFsMIPvrstInstPortAgreementPktRcvdTimeStamp)
{
    INT1                i1RetVal = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIPvrstInstPortIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsPvrstInstPortAgreementPktRcvdTimeStamp
        (i4FsMIPvrstInstVlanId, (INT4) u2LocalPortId,
         pu4RetValFsMIPvrstInstPortAgreementPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}
