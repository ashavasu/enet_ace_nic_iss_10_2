/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved.
 *
 * $Id: pvrstnpapi.c,v 1.3 2013/11/14 11:33:14 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              Hardware API's w.r.t PVRST
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __PVRST_NP_API_C
#define __PVRST_NP_API_C

#include "asthdrs.h"
#include "astvinc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpCreateVlanSpanningTree                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpCreateVlanSpanningTree
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpCreateVlanSpanningTree
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpCreateVlanSpanningTree (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpCreateVlanSpanningTree *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpCreateVlanSpanningTree;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpDeleteVlanSpanningTree                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpDeleteVlanSpanningTree
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpDeleteVlanSpanningTree
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpDeleteVlanSpanningTree (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpDeleteVlanSpanningTree *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeleteVlanSpanningTree;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpInitHw (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpInitHw;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpDeInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpDeInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpDeInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpDeInitHw (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpDeInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_DE_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeInitHw;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpSetVlanPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpSetVlanPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpSetVlanPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId VlanId, UINT1 u1PortState)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpSetVlanPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_SET_VLAN_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpSetVlanPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanId = VlanId;
    pEntry->u1PortState = u1PortState;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstNpGetVlanPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstNpGetVlanPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstNpGetVlanPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstNpGetVlanPortState (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT4 u4IfIndex, UINT1 *pu1Status)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstNpGetVlanPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_NP_GET_VLAN_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpGetVlanPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1Status = pu1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef  MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstMbsmNpSetVlanPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstMbsmNpSetVlanPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstMbsmNpSetVlanPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstMbsmNpSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tVlanId VlanId, UINT1 u1PortState,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstMbsmNpSetVlanPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_MBSM_NP_SET_VLAN_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstMbsmNpSetVlanPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanId = VlanId;
    pEntry->u1PortState = u1PortState;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstFsMiPvrstMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiPvrstMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiPvrstMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
PvrstFsMiPvrstMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstNpWrFsMiPvrstMbsmNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_PVRST_MOD,    /* Module ID */
                         FS_MI_PVRST_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pPvrstNpModInfo = &(FsHwNp.PvrstNpModInfo);
    pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstMbsmNpInitHw;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

#endif /* __PVRST_NP_API_C */
