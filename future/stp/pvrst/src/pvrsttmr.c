/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: pvrsttmr.c,v 1.23 2017/12/22 10:06:02 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Timer Module and other timer functionalities of
 *              PVRST Module.
 *
 *****************************************************************************/

#include "asthdrs.h"

 /*****************************************************************************/
 /* Function Name      : PvrstStartTimer                                      */
 /*                                                                           */
 /* Description        : This function is called whenever any timer needs to  */
 /*                      be started by the Module(s). This function allocates */
 /*                      a memory block for the timer node and then starts    */
 /*                      the timer of the specified type for the specified    */
 /*                      duration.                                            */
 /*                                                                           */
 /* Input(s)           : pPortPtr - A Void Pointer which may point to the     */
 /*                                Port Entry structure or to the Per         */
 /*                                Instance Port Info structure.              */
 /*                      u2InstIndex - Index of the spanning tree instance    */
 /*                      u1TimerType - The type of the timer that is to be    */
 /*                                    started                                */
 /*                      u2Duration - The duration for which the timer needs  */
 /*                                   to be started                           */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : gAstGlobalInfo.AstTmrMemPoolId                       */
 /*                      gAstGlobalInfo.AstTmrListId                          */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
 /*****************************************************************************/

INT4
PvrstStartTimer (VOID *pPortPtr, UINT2 u2InstIndex,
                 UINT1 u1TimerType, UINT2 u2Duration)
{
    tAstTimer          *pAstTimer = NULL;
    tAstTimer         **ppTimer = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstContextInfo    *pAstContextInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    INT4                i4RetVal = PVRST_SUCCESS;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstanceId = AST_INIT_VAL;
    UINT1               u1SyncFlag = PVRST_FALSE;
    UINT1               aau1TimerName[21][20] = { " ",
        "FDWHILE", "HELLOWHEN", "MDELAYWHILE", "RBWHILE", "RCVDINFOWHILE",
        "RRWHILE", "TCWHILE", "HOLD", "EDGEDELAYWHILE", "RESTART", " ",
        "RAPIDAGE DURATION", " ", " ", " ", " ", "ERROR_RECOVERY",
        "INC_RECOVERY",
        "ROOT_INC_RECOVERY", "FLUSHTGR"
    };
#ifdef L2RED_WANTED
    UINT1               u1TimerInfo;
#endif

    VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);
    if (PVRST_IS_VALID_VLANID (VlanId) == PVRST_FALSE)
    {
        return PVRST_FAILURE;
    }

    if (pPortPtr == NULL)
    {
        AST_DBG_ARG3 (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Duration %d: PvrstStartTimer for timer %s"
                      " called with Null Pointer\n", VlanId, u2Duration,
                      aau1TimerName[u1TimerType]);
        return PVRST_FAILURE;
    }
    if (u2Duration == 0)
    {
        AST_DBG_ARG2 (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Trying to Start Timer %s for Zero"
                      " Duration!!\n", VlanId, aau1TimerName[u1TimerType]);
        return PVRST_FAILURE;
    }

    /* Allocate the timer node from the Timer Memory Pool */
    if (AST_ALLOC_TMR_MEM_BLOCK (pAstTimer) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);
        AST_DBG_ARG3 (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                      "TMR: Inst %d: Duration %d: Timer Memory Block"
                      "Allocation for timer %s FAILED!\n",
                      VlanId, u2Duration, aau1TimerName[u1TimerType]);
        return PVRST_FAILURE;
    }

    AST_MEMSET (pAstTimer, AST_INIT_VAL, sizeof (tAstTimer));

    pAstTimer->pEntry = pPortPtr;
    pAstTimer->pAstContext = AST_CURR_CONTEXT_INFO ();
    pAstTimer->u2InstanceId = VlanId;
    pAstTimer->u1TimerType = u1TimerType;

    /* Store the pointer to the timer node in the corresponding timer pointer
     * and also validate the Timer Type */

    switch (u1TimerType)
    {

        case AST_TMR_TYPE_HELLOWHEN:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex,
                                    u1TimerType) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the"
                             "running HELLOWHEN Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s"
                          "and Instance %u for duration %u\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_FDWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running FDWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            u1SyncFlag = PVRST_TRUE;

            if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running"
                             "MDELAYWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPvrstRstPortInfo->pMdWhilePvrstTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_RBWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running RBWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port "
                          "%s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;

            if (pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running "
                             "RCVDINFOWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
#ifdef L2RED_WANTED
            else
            {
                u1SyncFlag = PVRST_TRUE;
            }
#endif
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s "
                          "and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_RRWHILE:

            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running RRWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }

            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_TCWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;

            if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running "
                             "TCWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_HOLD:

            /* As per IEEE Std 802.1s Hold Timer will be running per port.
             * In feature if the Hold timer needs to be run per Instance then
             * Instance should be passed as it is.
             * */
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPvrstRstPortInfo->pHoldTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running HOLD Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStPvrstRstPortInfo->pHoldTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u1SyncFlag = PVRST_TRUE;

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pEdgeDelayWhileTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running FDWHILE Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pEdgeDelayWhileTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;
        case AST_TMR_TYPE_RESTART:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;

            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            if (pAstContextInfo->pRestartTimer != NULL)
            {
                if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Memory Block Release FAILED!\n");
                    return PVRST_SUCCESS;
                }
                return PVRST_SUCCESS;

            }

            ppTimer = &(pAstContextInfo->pRestartTimer);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for duration %d centi-seconds\n",
                          aau1TimerName[AST_TMR_TYPE_EDGEDELAYWHILE + 1],
                          u2Duration);

            break;

        case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:

            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            if (pAstPortEntry != NULL)
            {
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                if (pAstCommPortInfo != NULL)
                {
                    if (IssuGetMaintModeOperation () != OSIX_TRUE)
                    {
                        u1SyncFlag = PVRST_TRUE;
                    }
                    u2PortNum = pAstPortEntry->u2PortNo;
                    if (pAstCommPortInfo->pDisableRecoveryTmr != NULL)
                    {
                        if (PvrstStopTimer
                            (pPortPtr, u2InstIndex,
                             AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) !=
                            RST_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                     "TMR: Unable to stop the running ERROR DISABLE RECOVERY Timer!\n");
                            i4RetVal = PVRST_FAILURE;
                            break;
                        }
                    }
                    ppTimer = &(pAstCommPortInfo->pDisableRecoveryTmr);
                    AST_DBG_ARG4 (AST_TMR_DBG,
                                  "TMR: Starting Timer %s for port %s and Instance %u"
                                  " for duration %d\n",
                                  aau1TimerName[u1TimerType],
                                  AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                                  u2InstIndex, u2Duration);
                }
            }
            break;
        case AST_TMR_TYPE_INC_RECOVERY:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pIncRecoveryTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)

                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running INC_RECOVERY Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pIncRecoveryTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_ROOT_INC_RECOVERY:

            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstIndex, u1TimerType)
                    != PVRST_SUCCESS)

                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ROOT_INC_RECOVERY Timer!\n");
                    i4RetVal = PVRST_FAILURE;
                    break;
                }
            }

            ppTimer = &(pPerStPvrstRstPortInfo->pRootIncRecoveryTmr);
            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            u1SyncFlag = RST_TRUE;

            u2PortNum = pAstPortEntry->u2PortNo;
            if (pAstCommPortInfo->pRapidAgeDurtnTmr != NULL)
            {
                if (AstStopTimer (pPortPtr, u1TimerType) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running RAPIDAGE Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pAstCommPortInfo->pRapidAgeDurtnTmr);

            AST_DBG_ARG4 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for port %s and Instance %u"
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          VlanId, u2Duration);
            break;

        case AST_TMR_TYPE_FLUSHTGR:
            pPerStInfo = (tAstPerStInfo *) pPortPtr;
            u2InstanceId = pPerStInfo->u2InstanceId;

            if (pPerStInfo->pFlushTgrTmr != NULL)
            {
                if (PvrstStopTimer (pPortPtr, u2InstanceId, u1TimerType) !=
                    RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running FLUSHTGR "
                             "Timer!\n");
                    i4RetVal = RST_FAILURE;
                    break;
                }
            }
            ppTimer = &(pPerStInfo->pFlushTgrTmr);
            AST_DBG_ARG3 (AST_TMR_DBG,
                          "TMR: Starting Timer %s for Instance %u"
                          " for duration %d\n",
                          aau1TimerName[u1TimerType], u2InstanceId, u2Duration);
            break;
        default:
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Invalid Timer Type\n");
            i4RetVal = PVRST_FAILURE;
            break;
    }

    if (i4RetVal == PVRST_FAILURE)
    {
        if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Memory Block Release FAILED!\n");
        }
        return i4RetVal;
    }

    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        if (AST_START_TIMER (AST_TMR_LIST_ID, &(pAstTimer->AstAppTimer),
                             u2Duration) != AST_TMR_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Starting Timer FAILED!\n");
            i4RetVal = PVRST_FAILURE;

            if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Memory Block Release FAILED!\n");
                i4RetVal = PVRST_FAILURE;
            }
            return i4RetVal;
        }
        pAstTimer->u1IsTmrStarted = PVRST_TRUE;
    }
    else
    {
        pAstTimer->u1IsTmrStarted = PVRST_FALSE;
    }
    *ppTimer = pAstTimer;

    if (u1TimerType == AST_TMR_TYPE_FLUSHTGR)
    {
        AST_DBG_ARG3 (AST_TMR_DBG,
                      "TMR: Inst %d: Started Timer %s for duration %u\n",
                      u2InstanceId,
                      aau1TimerName[u1TimerType],
                      (u2Duration / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    }

#ifdef L2RED_WANTED
    if ((AST_IS_PVRST_ENABLED ()) && (u1SyncFlag == PVRST_TRUE) &&
        (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STARTED);
        AstRedSendSyncMessages (VlanId, u2PortNum, RED_AST_TIMES, u1TimerInfo);
        /* If Stopping the Timer caused a change in the 
           port Role/State Sync the Port states */
        if (PVRST_GET_CHANGED_FLAG (u2InstIndex, u2PortNum) == PVRST_TRUE)
        {
            AstRedSendSyncMessages (VlanId, u2PortNum, RED_AST_PORT_INFO, 0);

            PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_FALSE;
        }
    }
#else
    UNUSED_PARAM (u1SyncFlag);
#endif

    AST_DBG_ARG4 (AST_TMR_DBG,
                  "TMR: Port %s: Inst %d: Started Timer %s for duration %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId,
                  aau1TimerName[u1TimerType],
                  (u2Duration / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    return (i4RetVal);
}

 /*****************************************************************************/
 /* Function Name      : PvrstStopTimer                                       */
 /*                                                                           */
 /* Description        : This function is called whenever any timer needs to  */
 /*                      be stopped by the Module(s). This function stops the */
 /*                      timer of the specified duration.                     */
 /*                                                                           */
 /* Input(s)           : pPortPtr - A Void Pointer which may point to the     */
 /*                                Port Entry structure or to the Per         */
 /*                                Instance Port Info structure.              */
 /*                      u2InstIndex - Index of the spanning tree instance    */
 /*                      u1TimerType - The type of the timer that is to be    */
 /*                                    started                                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : gAstGlobalInfo.AstTmrMemPoolId                       */
 /*                      gAstGlobalInfo.AstTmrListId                          */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
 /*****************************************************************************/

INT4
PvrstStopTimer (VOID *pPortPtr, UINT2 u2InstIndex, UINT1 u1TimerType)
{
    tAstTimer         **ppAstTimer = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT1               u1SyncFlag = PVRST_FALSE;
    UINT2               u2InstanceId = 0;
    tAstContextInfo    *pAstContextInfo = NULL;
#ifdef L2RED_WANTED
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT1               u1TimerInfo;
#endif
    UINT1               aau1TimerName[21][20] = { " ",
        "FDWHILE", "HELLOWHEN", "MDELAYWHILE", "RBWHILE", "RCVDINFOWHILE",
        "RRWHILE", "TCWHILE", "HOLD", "EDGEDELAYWHILE", "RESTART",
        "RAPIDAGE DURATION",
        " ", " ", " ", " ", " ", "ERROR_RECOVERY", "INC_RECOVERY",
        "ROOT_INC_RECOVERY", "FLUSHTGR"
            /*"RAPIDAGE_DURATION" */
    };

#ifdef L2RED_WANTED
    VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);
#endif

    if (pPortPtr == NULL)
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: PvrstStopTimer called with Null Pointer\n");
        return PVRST_FAILURE;
    }

    switch (u1TimerType)
    {

        case AST_TMR_TYPE_HELLOWHEN:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            ppAstTimer = &(pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_FDWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            ppAstTimer = &(pPerStPvrstRstPortInfo->pMdWhilePvrstTmr);
            u1SyncFlag = PVRST_TRUE;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RBWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount = 0;
            u1SyncFlag = PVRST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RRWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_TCWHILE:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            ppAstTimer = &(pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr);
            u2PortNum = pPerStPortInfo->u2PortNo;
            u1SyncFlag = PVRST_TRUE;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_HOLD:
            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            ppAstTimer = &(pPerStPvrstRstPortInfo->pHoldTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pEdgeDelayWhileTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            u1SyncFlag = PVRST_TRUE;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;
        case AST_TMR_TYPE_RESTART:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;

            pAstContextInfo = AST_CURR_CONTEXT_INFO ();

            ppAstTimer = &(pAstContextInfo->pRestartTimer);
            AST_DBG_ARG1 (AST_TMR_DBG,
                          "TMR: Stopping Timer %s \n",
                          aau1TimerName[AST_TMR_TYPE_EDGEDELAYWHILE + 1]);
            break;
        case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pDisableRecoveryTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);

            break;
        case AST_TMR_TYPE_INC_RECOVERY:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pIncRecoveryTmr);
            u2PortNum = pAstPortEntry->u2PortNo;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_ROOT_INC_RECOVERY:

            pPerStPortInfo = (tAstPerStPortInfo *) pPortPtr;
            u2PortNum = pPerStPortInfo->u2PortNo;
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            ppAstTimer = &(pPerStPvrstRstPortInfo->pRootIncRecoveryTmr);
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_RAPIDAGE_DURATION:
            pAstPortEntry = (tAstPortEntry *) pPortPtr;
            pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
            ppAstTimer = &(pAstCommPortInfo->pRapidAgeDurtnTmr);
            u2PortNum = pAstPortEntry->u2PortNo;
            u1SyncFlag = RST_TRUE;

            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Port %s: Stopping Timer %s \n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          aau1TimerName[u1TimerType]);
            break;

        case AST_TMR_TYPE_FLUSHTGR:
            pPerStInfo = (tAstPerStInfo *) pPortPtr;
            ppAstTimer = &(pPerStInfo->pFlushTgrTmr);
            u2InstanceId = pPerStInfo->u2InstanceId;
            AST_DBG_ARG2 (AST_TMR_DBG,
                          "TMR: Instance %u: Stopping Timer %s \n",
                          u2InstanceId, aau1TimerName[u1TimerType]);
            break;
        default:
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Invalid Timer Type\n");
            return PVRST_FAILURE;
    }

    if (*ppAstTimer == NULL)
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Timer not running, cannot stop\n");
        return PVRST_FAILURE;
    }

    if ((AST_NODE_STATUS () == RED_AST_ACTIVE) ||
        (AST_NODE_STATUS () == RED_AST_FORCE_SWITCHOVER_INPROGRESS) ||
        (AST_NODE_STATUS () == RED_AST_SHUT_START_INPROGRESS))
    {
        if ((*ppAstTimer)->u1IsTmrStarted == PVRST_TRUE)
        {
            /* Stop the Timer */
            if (AST_STOP_TIMER (AST_TMR_LIST_ID, &((*ppAstTimer)->AstAppTimer))
                == AST_TMR_FAILURE)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Stopping Timer FAILED!\n");
                return PVRST_FAILURE;
            }
            (*ppAstTimer)->u1IsTmrStarted = PVRST_FALSE;
        }
    }

    /* Free the Timer Memory Block to the Memory Pool */
    if (AST_RELEASE_TMR_MEM_BLOCK (*ppAstTimer) != AST_MEM_SUCCESS)
    {
        AST_DBG (AST_TMR_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Memory Block Release FAILED!\n");
        return PVRST_FAILURE;
    }

    *ppAstTimer = NULL;

#ifdef L2RED_WANTED
    if ((AST_IS_PVRST_ENABLED ()) && (u1SyncFlag == PVRST_TRUE) &&
        (AST_NODE_STATUS () == RED_AST_ACTIVE))
    {
        u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_STOPPED);
        AstRedSendSyncMessages (VlanId, u2PortNum, RED_AST_TIMES, u1TimerInfo);
        /* If Stopping the Timer caused a change in the port state 
         * Sync the Port states */
        if (PVRST_GET_CHANGED_FLAG (u2InstIndex, u2PortNum) == PVRST_TRUE)
        {
            AstRedSendSyncMessages (VlanId, u2PortNum, RED_AST_PORT_INFO, 0);

            PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_FALSE;
        }
    }
#else
    UNUSED_PARAM (u1SyncFlag);
#endif

    AST_DBG (AST_TMR_DBG, "TMR: Timer Stopped successfully\n");
    return PVRST_SUCCESS;
}

 /*****************************************************************************/
 /* Function Name      : PvrstTmrExpiryHandler                                */
 /*                                                                           */
 /* Description        : This function is called whenever any timer expires.  */
 /*                      This extracts all the expired timers at any instant  */
 /*                      of time and depending on the type of timer, it       */
 /*                      performs the necessary processing.                   */
 /*                                                                           */
 /* Input(s)           : pAstTimer - Timer node pointer                       */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : gAstGlobalInfo.AstTmrListId                          */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
 /*****************************************************************************/
INT4
PvrstTmrExpiryHandler (tAstTimer * pAstTimer)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT1               u1TimerType = (UINT1) AST_INIT_VAL;
    UINT2               u2InstanceId = (UINT1) AST_INIT_VAL;
    VOID               *pEntryPtr = NULL;
    INT4                i4RetVal = PVRST_SUCCESS;
    UINT2               u2PortNum = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;
    UINT1               u1SyncFlag = PVRST_FALSE;
    tAstContextInfo    *pAstContextInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

#ifdef L2RED_WANTED
    UINT1               u1TimerInfo;
#endif

    AST_DBG (AST_CONTROL_PATH_TRC,
             "TMR: PVRST Handling Timer Expiry event obtained ...\n");
    AST_DBG (AST_TMR_DBG, "TMR: PVRST Handling Timer "
             "Expiry event obtained ...\n");

    VlanId = pAstTimer->u2InstanceId;
    u1TimerType = pAstTimer->u1TimerType;
    pEntryPtr = pAstTimer->pEntry;

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if (pEntryPtr != NULL)
    {
        /* 
         * Calling the respective State machines to handle 
         * the Timer expiry 
         */

        switch (u1TimerType)
        {
            case AST_TMR_TYPE_HELLOWHEN:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                u2PortNum = pPerStPortInfo->u2PortNo;
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO
                    (u2PortNum, u2InstIndex);

                pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr = NULL;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: HELLOWHEN Timer EXPIRED"
                              " for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);

                if (PvrstPortTransmitMachine
                    ((UINT2) PVRST_PTXSM_EV_HELLOWHEN_EXP,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Tx SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Tx SEM returned FAILURE!\n");
                    i4RetVal = PVRST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_FDWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: FDWHILE Timer EXPIRED "
                              "for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              VlanId);

                if (PvrstPortRoleTrMachine
                    ((UINT2) PVRST_PROLETRSM_EV_FDWHILE_EXP,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM "
                             "returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM "
                             "returned FAILURE!\n");
                    i4RetVal = PVRST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_MDELAYWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO
                    (u2PortNum, u2InstIndex);

                pPerStPvrstRstPortInfo->pMdWhilePvrstTmr = NULL;
                if (pPerStPvrstRstPortInfo->bRcvdStp != PVRST_TRUE)
                {
                    PvrstPmigSmMakeSendingRstp (u2PortNum, u2InstIndex);
                }
                else
                {
                    PvrstPmigSmMakeSendingStp (u2PortNum, u2InstIndex);
                }

                AST_DBG_ARG2 (AST_TMR_DBG | AST_PMSM_DBG,
                              "TMR: Port %s: MDWHILE Timer EXPIRED "
                              "for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);
                break;

            case AST_TMR_TYPE_RBWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: RBWHILE Timer EXPIRED"
                              " for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);

                if (PvrstPortRoleTrMachine
                    ((UINT2) PVRST_PROLETRSM_EV_RBWHILE_EXP,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM"
                             " returned FAILURE!\n\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM "
                             "returned FAILURE!\n");
                    i4RetVal = PVRST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_RCVDINFOWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr = NULL;

#ifdef L2RED_WANTED
                pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount++;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;
                if (pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount >=
                    AST_NUM_TMR_INTERVAL_SPLITS)
                {

                    AST_DBG_ARG2 (AST_TMR_DBG | AST_PISM_DBG,
                                  "TMR: Port %s: RCVDINFOWHILE Timer EXPIRED for Instance: %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstIndex);

                    if (PvrstPortInfoMachine
                        ((UINT2) PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                         pPerStPortInfo, (tPvrstBpdu *) AST_INIT_VAL)
                        != PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        i4RetVal = PVRST_FAILURE;
                    }
                    pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount = 0;
                }
                else
                {
                    PvrstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo);
                }
#else
                if (PvrstPortInfoMachine
                    ((UINT2) PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                     pPerStPortInfo, (tPvrstBpdu *) AST_INIT_VAL)
                    != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Information SEM returned"
                             " FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Information SEM returned"
                             " FAILURE!\n");
                    i4RetVal = PVRST_FAILURE;
                }
#endif
                break;

            case AST_TMR_TYPE_RRWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: RRWHILE Timer EXPIRED"
                              " for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);

                if (PvrstPortRoleTrMachine
                    ((UINT2) PVRST_PROLETRSM_EV_RRWHILE_EXP,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM"
                             " returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM"
                             " returned FAILURE!\n");
                    i4RetVal = PVRST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_TCWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = PVRST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR: Port %s: TCWHILE Timer "
                              "EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);

                break;

            case AST_TMR_TYPE_HOLD:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                u2PortNum = pPerStPortInfo->u2PortNo;
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO
                    (u2PortNum, u2InstIndex);
                pPerStPvrstRstPortInfo->pHoldTmr = NULL;

                pPerStPvrstBridgeInfo =
                    PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

                if (pPerStPvrstRstPortInfo->u1TxCount > 0)
                {
                    (pPerStPvrstRstPortInfo->u1TxCount)--;
                }

                AST_DBG_ARG3 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: HOLD Timer EXPIRED "
                              "for Instance: %u, TxCount: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              VlanId, pPerStPvrstRstPortInfo->u1TxCount);

                if (pPerStPvrstRstPortInfo->u1TxCount ==
                    (pPerStPvrstBridgeInfo->u1TxHoldCount - (UINT1) 1))
                {
                    if (PvrstPortTransmitMachine
                        ((UINT2) PVRST_PTXSM_EV_HOLDTMR_EXP,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "TMR: Port Transmit SEM returned "
                                 "FAILURE!!!\n");
                        AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Transmit SEM returned "
                                 "FAILURE!!!\n");
                        i4RetVal = PVRST_FAILURE;
                    }
                }

                if (pPerStPvrstRstPortInfo->u1TxCount > (UINT1) AST_INIT_VAL)
                {
                    if (pPerStPvrstRstPortInfo->pHoldTmr == NULL)
                    {
                        if (PvrstStartTimer
                            ((VOID *) pPerStPortInfo, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_HOLD,
                             (UINT2) AST_HOLD_TIME) != PVRST_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                     "TMR: PvrstStartTimer routine"
                                     " returned FAILURE!!!\n");
                            i4RetVal = PVRST_FAILURE;
                        }
                    }
                }
                break;

            case AST_TMR_TYPE_EDGEDELAYWHILE:
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pEdgeDelayWhileTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = PVRST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %s: EDGEDELAYWHILE Timer EXPIRED "
                              "for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);
                PvrstBrgDetectionMachine ((UINT2)
                                          PVRST_BRGDETSM_EV_EDGEDELAYWHILE_EXP,
                                          u2PortNum);
                break;
            case AST_TMR_TYPE_RESTART:

                pAstContextInfo = AST_CURR_CONTEXT_INFO ();
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                if (pAstPortEntry == NULL)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "pAstPortEntry NULL \n");
                    return PVRST_FAILURE;

                }

                if (pAstContextInfo != NULL)
                {
                    pAstContextInfo->pRestartTimer = NULL;
                }
                else
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Invalid Context !!!\n");

                    return PVRST_FAILURE;
                }
                if (pAstPortEntry->u1RecScenario == LOOP_INC_RECOVERY)
                {
                    pPerStPortInfo =
                        PVRST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                   u2InstIndex);
                    if (pPerStPortInfo != NULL)
                    {

                        if ((pPerStPortInfo->u1PortRole ==
                             AST_PORT_ROLE_DESIGNATED)
                            && (pPerStPortInfo->bLoopIncStatus == RST_TRUE)
                            && (pAstPortEntry->bLoopGuard == AST_TRUE))
                        {
                            NotifyProtoToApp.STPNotify.u4IfIndex =
                                pAstPortEntry->u4IfIndex;
                            AstCfaNotifyProtoToApp (STP_NOTIFY,
                                                    NotifyProtoToApp);
                            AST_TRC_ARG1 (AST_INIT_SHUT_TRC |
                                          AST_CONTROL_PATH_TRC,
                                          "SYS: Enabling Port %s ...\n",
                                          AST_GET_IFINDEX_STR (u2PortNum));
                        }
                    }
                    pAstPortEntry->u1RecScenario = AST_INIT_VAL;
                }
                else
                {
                    if (AstRestartStateMachines () != PVRST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: AstRestartStateMachines invocation failed!!!\n");
                        i4RetVal = PVRST_FAILURE;
                    }
                }
                AST_DBG (AST_TMR_DBG, "TMR:  Restart Timer EXPIRED\n");

                break;
            case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;

                if (pAstPortEntry != NULL)
                {
                    pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                    if (pAstCommPortInfo != NULL)
                    {
                        pAstCommPortInfo->pDisableRecoveryTmr = NULL;
                        u2PortNum = pAstPortEntry->u2PortNo;
                        u1SyncFlag = PVRST_TRUE;
                        AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                                      "TMR: Port %u: ERROR DISABLE RECOVERY"
                                      " Timer EXPIRED for Instance: %u\n",
                                      pAstPortEntry->u2PortNo, u2InstIndex);

                        /* During ISSU Maintenance Mode 
                         * restart the Error Disable recovery Timer */
                        if (IssuGetMaintModeOperation () == OSIX_TRUE)
                        {
                            pAstTimer->u1IsTmrStarted = PVRST_FALSE;
                            u2Duration =
                                AST_DEFAULT_ERROR_RECOVERY + ISSU_TIMER_VALUE;

                            if (PvrstStartTimer ((VOID *) pPerStPortInfo,
                                                 u2InstIndex,
                                                 (UINT1)
                                                 AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                                                 u2Duration) != PVRST_SUCCESS)
                            {
                                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                         "PVRST_TMR:ISSU is in progress, Restarting"
                                         "PvrstStartTimer returned FAILURE!\n");
                                return PVRST_FAILURE;
                            }
                            AST_DBG (AST_TMR_DBG,
                                     "PVRST_TMR:ISSU is in progress,"
                                     "PvrstStartTimer Restarted!\n");
                            return PVRST_SUCCESS;
                        }

                        AstEnablePort (u2PortNum, u2InstIndex, AST_EXT_PORT_UP);

                        /* BpduInconsitent state is reset when the port transtions
                         * happens */
                        if ((pAstCommPortInfo->u4BpduGuard ==
                             AST_BPDUGUARD_ENABLE)
                            ||
                            ((pAstCommPortInfo->u4BpduGuard ==
                              AST_BPDUGUARD_NONE)
                             && (AST_GBL_BPDUGUARD_STATUS == PVRST_TRUE)))
                        {
                            if (pAstCommPortInfo->bBpduInconsistent == AST_TRUE)
                            {

                                pAstCommPortInfo->bBpduInconsistent =
                                    PVRST_FALSE;
                                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                                UtlGetTimeStr (au1TimeStr);
                                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                                  "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                                  AST_GET_IFINDEX_STR
                                                  (pAstPortEntry->u2PortNo),
                                                  au1TimeStr);
                            }
                        }
                    }
                }
                break;
            case AST_TMR_TYPE_INC_RECOVERY:
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pIncRecoveryTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = PVRST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %s: INC_TIMER Timer EXPIRED "
                              "for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);
               /** Recover from inconsistancy**/
                if ((pAstPortEntry->bPVIDInconsistent == AST_TRUE))
                {
                    pAstPortEntry->bPTypeInconsistent = AST_FALSE;
                  /**Recovering from Type inconsistancy : Port Un blocked**/
                    PvrstPerStEnablePort (u2PortNum, VlanId, AST_EXT_PORT_UP);

                }
                else
                {
                    pAstPortEntry->bPTypeInconsistent = AST_FALSE;
                  /**Recovering from Type inconsistancy : Port Un blocked**/
                    PvrstPerStEnablePort (u2PortNum, VlanId, AST_EXT_PORT_UP);
                }

                break;
            case AST_TMR_TYPE_ROOT_INC_RECOVERY:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                u2PortNum = pPerStPortInfo->u2PortNo;
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO
                    (u2PortNum, u2InstIndex);

                AST_DBG_ARG2 (AST_TMR_DBG | AST_PISM_DBG,
                              "TMR: Port %s: ROOT INC RECOVERY Timer EXPIRED "
                              "for Instance: %u\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);

                pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

                pPerStPvrstRstPortInfo->pRootIncRecoveryTmr = NULL;

                if (pAstCommPortInfo->bRootGuard == RST_TRUE)
                {
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 VlanId,
                                                 AST_GET_IFINDEX (u2PortNum)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "TMR:Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          VlanId, au1TimeStr);
                    }
                    if (PvrstPortInfoSmMakeAged (pPerStPortInfo, NULL) !=
                        PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "PISM: Role Transition Machine returned FAILURE\n");
                        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                 "PISM: Role Transition Machine returned FAILURE\n");
                        return PVRST_FAILURE;
                    }
                }

                break;

            case AST_TMR_TYPE_RAPIDAGE_DURATION:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pRapidAgeDurtnTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = RST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: RAPIDAGE DURATION Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              VlanId);

                if (AST_FORCE_VERSION == AST_VERSION_0)
                {
                    AstVlanResetShortAgeoutTime (pAstPortEntry);
                }
                break;

            case AST_TMR_TYPE_FLUSHTGR:

                pPerStInfo = (tAstPerStInfo *) pEntryPtr;
                pPerStInfo->pFlushTgrTmr = NULL;
                u2InstanceId = pPerStInfo->u2InstanceId;

                /* When the timer FlushInterval fires, then check the flag
                 * PENDING FLUSHES.. Based on that take decisions as follows
                 *
                 * -- If (PENDING FLUSHES == TRUE) .
                 * (a) Call the flushing call per instance . Flush (Instance)
                 *
                 * -- If (PENDING FLUSHES == FLASE) .
                 * (a) No need for any action
                 */

                if (pPerStInfo->FlushFlag.u1PendingFlushes == AST_TRUE)
                {
                    AstFlushFdbOnInstance (u2InstanceId);
                    pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
                    (pPerStInfo->PerStBridgeInfo.u4TotalFlushCount)++;
                }
                pPerStInfo->PerStBridgeInfo.u4FlushIndCount = AST_INIT_VAL;

                AST_DBG_ARG1 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR:  FLUSHTGR Timer EXPIRED for "
                              "Instance: %u\n", u2InstanceId);
                break;
            default:
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Invalid Timer Type\n");
                i4RetVal = PVRST_FAILURE;

        }                        /* End of switch */
#ifdef L2RED_WANTED
        if (AST_IS_PVRST_ENABLED () && (u1SyncFlag == PVRST_TRUE)
            && (AST_NODE_STATUS () == RED_AST_ACTIVE))
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_EXPIRED);
            AstRedSendSyncMessages (VlanId, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);

            if (NULL == PVRST_GET_PERST_INFO (u2InstIndex))
            {
                return PVRST_SUCCESS;
            }
            if (NULL == PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex))
            {
                return PVRST_SUCCESS;
            }

            /* If Stopping the Timer caused a change in the 
               port state Sync the Port states */
            if (PVRST_GET_CHANGED_FLAG (u2InstIndex, u2PortNum) == PVRST_TRUE)
            {
                AstRedSendSyncMessages (VlanId,
                                        u2PortNum, RED_AST_PORT_INFO, 0);

                PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_FALSE;
            }
            u1SyncFlag = PVRST_FALSE;
        }
#else
        UNUSED_PARAM (u1SyncFlag);
#endif
    }
    else
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Corrupt entry pointer in timer node !!!\n");
        i4RetVal = PVRST_FAILURE;
    }

    pAstTimer->u1IsTmrStarted = PVRST_FALSE;
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Expired Timer Memory Block Release FAILED!\n");
            i4RetVal = PVRST_FAILURE;
        }
    }

    AST_DBG (AST_TMR_DBG, "TMR: Expired Timer processed \n");
    if (i4RetVal == PVRST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC, "TMR: Expired Timer(s) processed \n");
        AST_DBG (AST_TMR_DBG,
                 "TMR: Timer Expiry event successfully processed ...\n");
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : PvrstStopAllRunningTimers                            */
/*                                                                           */
/* Description        : This function is called whenever any port is deleted */
/*                      or when the Module is disabled. This function will   */
/*                      stop all the running timers for this port.           */
/*                                                                           */
/* Input(s)           : None                                                 */
 /*                                                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstStopAllRunningTimers (tAstPortEntry * pAstPortEntry,
                           tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pCommPortInfo = &(pAstPortEntry->CommPortInfo);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_HELLOWHEN) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for HelloWhenTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_ROOT_INC_RECOVERY) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for ROOT INC RECOVERY FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pFdWhileTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_FDWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for FdWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_MDELAYWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for MdWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRbWhileTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_RBWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for RbWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_RCVDINFOWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for RcvdInfoTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pRrWhileTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for RrWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStRstPortInfo->pTcWhileTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_TCWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for TcWhileTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pPerStPvrstRstPortInfo->pHoldTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             AST_TMR_TYPE_HOLD) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for HoldTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }
    if (pCommPortInfo->pEdgeDelayWhileTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pAstPortEntry, u2InstIndex,
             AST_TMR_TYPE_EDGEDELAYWHILE) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for EdgeDelayWhile Timer FAILED!\n");
            return PVRST_FAILURE;
        }
    }
    if (AST_CURR_CONTEXT_INFO ()->pRestartTimer != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pAstPortEntry, u2InstIndex,
             AST_TMR_TYPE_RESTART) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for Restart Timer FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    if (pCommPortInfo->pDisableRecoveryTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pAstPortEntry, u2InstIndex,
                            AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Unable to stop the running ERROR DISABLE RECOVERY Timer!\n");
            return PVRST_FAILURE;
        }
    }
    if (pCommPortInfo->pIncRecoveryTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pAstPortEntry, u2InstIndex,
             AST_TMR_TYPE_INC_RECOVERY) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: PvrstStopTimer for INC_RECOVERY Timer FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_TMR_DBG | AST_INIT_SHUT_DBG,
                  "TMR: Port %u: Stopped All Running Timers... \n",
                  pAstPortEntry->u2PortNo);
    return PVRST_SUCCESS;
}

/*PVRST_FORCE_VERSION*/
/*****************************************************************************/
/* Function Name      : PvrstForceVersionStopAllRunningTimers                */
/*                                                                           */
/* Description        : This function is called whenever compatability       */
/*                      mode changes to STP. This function will              */
/*                      stop all the running timers for this port in all the */
/*                      non default instances                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstForceVersionStopAllRunningTimers (UINT2 u2PortNum)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        return PVRST_SUCCESS;
    }
    for (u2InstIndex = 2; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        if (PvrstStopAllRunningTimers (pPortInfo, pPerStPortInfo)
            != PVRST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                          AST_INIT_SHUT_DBG,
                          "SYS: Unable to stop the running timers for port"
                          "%s instance %d!!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum),
                          pPerStPortInfo->u2Inst);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*PVRST_FORCE_VERSION_END*/

 /* End of file */
