/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvrssm.c,v 1.17 2017/12/29 09:31:22 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Role Selection State Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"
#include "astvinc.h"

/*****************************************************************************/
/* Function Name      : PvrstPortRoleSelectionMachine                        */
/*                                                                           */
/* Description        : This is the main routine for the Port Role Selection */
/*                      State Machine.                                       */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      VlanId - Vlan Instance Identifier for                */
/*                               for which this state machine is  called.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortRoleSelectionMachine (UINT2 u2Event, tVlanId VlanId)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4RetVal = (INT4) PVRST_SUCCESS;
    UINT2               u2State = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    if (NULL == PVRST_GET_PERST_INFO (u2InstIndex))
    {
        return PVRST_FAILURE;
    }

    if ((u2InstIndex != AST_DEF_VLAN_ID ())
        && (AST_FORCE_VERSION == AST_VERSION_0))
    {
        return PVRST_SUCCESS;
    }
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    u2State = (UINT2) pPerStBrgInfo->u1ProleSelSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "RSSM: Role Sel Machine Called with Event: %s,State: %s"
                  "and Instance: %u\n", gaaau1AstSemEvent[AST_RSSM][u2Event],
                  gaaau1AstSemState[AST_RSSM][u2State], VlanId);

    AST_DBG_ARG3 (AST_RSSM_DBG,
                  "RSSM: Role Sel Machine Called with Event: %s,State: %s"
                  "and Instance: %u\n", gaaau1AstSemEvent[AST_RSSM][u2Event],
                  gaaau1AstSemState[AST_RSSM][u2State], VlanId);

    if (PVRST_PORT_ROLE_SEL_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    i4RetVal = (*(PVRST_PORT_ROLE_SEL_MACHINE[u2Event][u2State].pAction))
        (u2InstIndex);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: Event routine returned FAILURE !!!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmMakeInit                              */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'INIT_BRIDGE'.                                       */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleSelSmMakeInit (UINT2 u2InstIndex)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    PvrstProleSelSmUpdtRoleDisabledBridge (u2InstIndex);

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pPerStBrgInfo->u1ProleSelSmState =
        (UINT1) PVRST_PROLESELSM_STATE_INIT_BRIDGE;

    AST_DBG (AST_RSSM_DBG, "RSSM: Moved to state INIT_BRIDGE \n");

    return (PvrstProleSelSmMakeRoleSelection (u2InstIndex));
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmMakeRoleSelection                     */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'ROLE_SELECTION'.                                    */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleSelSmMakeRoleSelection (UINT2 u2InstIndex)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pPerStBrgInfo->u1ProleSelSmState =
        (UINT1) PVRST_PROLESELSM_STATE_ROLE_SELECTION;

    AST_DBG (AST_RSSM_DBG, "RSSM: Moved to state ROLE_SELECTION \n");

    PvrstProleSelSmClearReselectBridge (u2InstIndex);
    if (PvrstProleSelSmUpdtRolesBridge (u2InstIndex) != PVRST_SUCCESS)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: UpdtRolesBridge  returned FAILURE !!!\n");
        return PVRST_FAILURE;
    }
    if (PvrstProleSelSmSetSelectedBridge (u2InstIndex) != PVRST_SUCCESS)
    {
        AST_DBG (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                 "RSSM: SetSelectedBridge  returned FAILURE !!!\n");
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmUpdtRoleDisabledBridge                */
/*                                                                           */
/* Description        : This routine performs the 'updtRoleDisabledBridge'   */
/*                      procedure of the state machine.                      */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
VOID
PvrstProleSelSmUpdtRoleDisabledBridge (UINT2 u2InstIndex)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
        {

            if (AST_IS_PORT_UP (u2PortNum))
            {
                pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum,
                                                            u2InstIndex);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo->u1SelectedPortRole =
                    (UINT1) AST_PORT_ROLE_DISABLED;
                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %u: Port Role Selected"
                              "as -> DISABLED\n", u2PortNum);
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmClearReselectBridge                   */
/*                                                                           */
/* Description        : This routine performs the 'clearReleselectBridge'    */
/*                      procedure of the state machine.                      */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstProleSelSmClearReselectBridge (UINT2 u2InstIndex)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
        {
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            if (pPerStRstPortInfo->bPortEnabled == PVRST_TRUE)
            {
                pPerStRstPortInfo->bReSelect = PVRST_FALSE;
                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %u: RESELECT variable CLEARED\n",
                              u2PortNum);
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmUpdtRolesBridge                       */
/*                                                                           */
/* Description        : This routine performs the 'updtRolesBridge'          */
/*                      procedure of the state machine.                      */
/*                      This routine computes and selects the Port Role for  */
/*                      all the bridge ports that are existing.              */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleSelSmUpdtRolesBridge (UINT2 u2InstIndex)
{
    tAstBridgeId        MyBrgId;
    tAstBridgeId        TmpRootId;
    tAstBridgeId        TmpDesgBrgId;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstTimes          *pPortTimes = NULL;
    tAstTimes          *pRootTimes = NULL;
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRole[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1NewRole[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT4               u4TmpRootCost = (UINT4) AST_INIT_VAL;
    UINT4               u4Val = (UINT4) AST_INIT_VAL;
    UINT2               u2TmpDesgPort = (UINT2) AST_INIT_VAL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPort = (UINT2) AST_INIT_VAL;
    UINT2               u2ModVal = (UINT2) AST_INIT_VAL;
    UINT2               u2MsgAgeIncr = (UINT2) AST_INIT_VAL;
    UINT2               u2Val = (UINT4) AST_INIT_VAL;
    UINT2               u2PortId = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPortId = (UINT2) AST_INIT_VAL;
    UINT1               u1PrevPortRole = AST_PORT_ROLE_DISABLED;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);

    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));
    AST_MEMSET (&TmpRootId, AST_INIT_VAL, sizeof (TmpRootId));
    AST_MEMSET (&TmpDesgBrgId, AST_INIT_VAL, sizeof (TmpDesgBrgId));
    AST_MEMSET (au1RootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldRootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    /* Assume the root as this bridge itself initially */
    TmpRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(TmpRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    u4TmpRootCost = 0;
    TmpDesgBrgId = TmpRootId;
    u2TmpDesgPort = 0;
    u2TmpRootPort = 0;

    MyBrgId = TmpRootId;

    /* Calculate 
     * 1. The Root Bridge Id among the Root Bridge Id held in 
     *    all functional ports.
     * 2. The Root Port as the port that is having better information
     * 3. Calculate the Root Path Cost as the best cost to the Root Bridge
     */
    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_FALSE;

        pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStRstPortInfo->bPortEnabled == PVRST_FALSE)
        {
            continue;
        }

        /* Port is not Disabled */

        if (pPerStRstPortInfo->u1InfoIs == (UINT1) PVRST_INFOIS_RECEIVED)
        {
            /* Port Message is a Received Message */

            if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                            &(pPerStPortInfo->DesgBrgId.BridgeAddr),
                            AST_MAC_ADDR_LEN) == PVRST_BRGID1_SAME)
            {
                continue;
            }

            i4RetVal = RstCompareBrgId (&(pPerStPortInfo->RootId),
                                        &(TmpRootId));
            switch (i4RetVal)
            {
                case PVRST_BRGID1_SUPERIOR:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: Root BrgId received"
                                  "by Port is SUPERIOR\n", u2PortNum);

                    TmpRootId = pPerStPortInfo->RootId;
                    u4TmpRootCost = pPerStPortInfo->u4RootCost +
                        pPerStPvrstRstPortInfo->u4PathCost;
                    TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                    u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                    u2TmpRootPort = u2PortNum;
                    break;

                case PVRST_BRGID1_SAME:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: Root BrgId received"
                                  "by Port is the SAME\n", u2PortNum);

                    u4Val = pPerStPortInfo->u4RootCost +
                        pPerStPvrstRstPortInfo->u4PathCost;
                    if (u4Val < u4TmpRootCost)
                    {

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %u: Path Cost"
                                      "received by Port is BETTER\n",
                                      u2PortNum);

                        u4TmpRootCost = u4Val;
                        TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                        u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                        u2TmpRootPort = u2PortNum;
                    }
                    else if (u4Val == u4TmpRootCost)
                    {

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %u: Path Cost received"
                                      "by Port is the SAME\n", u2PortNum);

                        i4RetVal =
                            RstCompareBrgId (&(pPerStPortInfo->DesgBrgId),
                                             &(TmpDesgBrgId));
                        switch (i4RetVal)
                        {
                            case PVRST_BRGID1_SUPERIOR:

                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %u: DesgBrgId"
                                              "received by Port is SUPERIOR\n",
                                              u2PortNum);

                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = u2PortNum;
                                break;

                            case PVRST_BRGID1_SAME:

                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %u: DesgBrgId received"
                                              "by Port is the SAME\n",
                                              u2PortNum);

                                if (pPerStPortInfo->u2DesgPortId <
                                    u2TmpDesgPort)
                                {

                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %u: Desg PortId"
                                                  "received by Portis BETTER\n",
                                                  u2PortNum);

                                    u2TmpDesgPort =
                                        pPerStPortInfo->u2DesgPortId;
                                    u2TmpRootPort = u2PortNum;
                                }
                                else if (pPerStPortInfo->u2DesgPortId
                                         == u2TmpDesgPort)
                                {
                                    u2TmpRootPortId = (UINT2)
                                        ((PVRST_GET_PERST_PORT_INFO
                                          (u2TmpRootPort, u2InstIndex))->
                                         u1PortPriority);
                                    u2TmpRootPortId =
                                        (UINT2) (u2TmpRootPortId << 8);
                                    u2TmpRootPortId =
                                        u2TmpRootPortId |
                                        AST_GET_PROTOCOL_PORT (u2TmpRootPort);
                                    u2PortId =
                                        (UINT2) (pPerStPortInfo->
                                                 u1PortPriority);
                                    u2PortId = (UINT2) (u2PortId << 8);
                                    u2PortId = u2PortId |
                                        AST_GET_PROTOCOL_PORT (u2PortNum);
                                    if (u2PortId < u2TmpRootPortId)
                                    {
                                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                                      "RSSM: Port %u:: Recvd"
                                                      "PortId is SUPERIOR\n",
                                                      u2PortNum);
                                        u2TmpRootPort = u2PortNum;
                                    }
                                }
                                break;

                            default:
                                break;
                        }        /* End switch(i4RetVal) */
                    }
                    break;

                default:
                    break;
            }                    /* End of switch(i4RetVal)... */
        }                        /* End if(pPerStRstPortInfo->u1InfoIs ..)... */

    }                            /* End for (u2PortNum = 1; ...) */

    /* Now the Chosen Root Priority Vector is copied into global 
     * Root Priority Vector
     */
    if (u2TmpRootPort != AST_INIT_VAL)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RSSM: << ----Port %u selected as Root Port---- >>\n",
                      u2TmpRootPort);
        AST_DBG_ARG1 (AST_RSSM_DBG,
                      "RSSM: << ----Port %u selected as Root Port---- >>\n",
                      u2TmpRootPort);

        pPortInfo = AST_GET_PORTENTRY (u2TmpRootPort);
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2TmpRootPort, u2InstIndex);
        if (NULL == pPerStPortInfo)
        {
            return PVRST_FAILURE;
        }

        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2TmpRootPort, u2InstIndex);

        /* Calculate if there is any change in Root Bridge */
        i4RetVal = AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                               &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            PVRST_INCR_NEW_ROOTBRIDGE_COUNT (u2InstIndex);
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Vlan:%u Old Root:%s New Root:%s Time Stamp: %s\r\n ",
                              VlanId, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);
            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId, VlanId,
                            (INT1 *) AST_PVRST_TRAPS_OID,
                            PVRST_BRG_TRAPS_OID_LEN);
        }

        pPerStBrgInfo->RootId = TmpRootId;
        pPerStBrgInfo->u2RootPort = u2TmpRootPort;
        pPerStBrgInfo->u4RootCost = u4TmpRootCost;
        pPerStPvrstBridgeInfo->RootTimes = pPerStPvrstRstPortInfo->
            PortPvrstTimes;
        /* Below calcuation is done in terms of Centi-Seconds */
        u2ModVal = (UINT2) (pPerStPvrstRstPortInfo->
                            PortPvrstTimes.u2MaxAge % (16 * AST_CENTI_SECONDS));
        u2ModVal = (u2ModVal >= 800) ? (UINT2) 1 : (UINT2) 0;
        u2MsgAgeIncr = (UINT2) (pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge
                                / (16 * AST_CENTI_SECONDS));
        u2MsgAgeIncr = (UINT2) (u2MsgAgeIncr + u2ModVal);
        u2MsgAgeIncr = (u2MsgAgeIncr == 0) ? (UINT2) 1 : u2MsgAgeIncr;
        pPerStPvrstBridgeInfo->RootTimes.u2MsgAgeOrHopCount +=
            (u2MsgAgeIncr * AST_CENTI_SECONDS);

        pPerStPvrstRstPortInfo->DesgPvrstTimes =
            pPerStPvrstBridgeInfo->RootTimes;
    }
    else
    {
        AST_TRC (AST_CONTROL_PATH_TRC,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");
        AST_DBG (AST_RSSM_DBG,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");

        /* Calculate if there is any change in Root Bridge */
        i4RetVal = AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                               &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            PVRST_INCR_NEW_ROOTBRIDGE_COUNT (u2InstIndex);
            AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
            AST_MEMSET (au1OldRootBrgAddr, 0, sizeof (au1OldRootBrgAddr));

            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Vlan:%u Old Root:%s New Root:%s Time Stamp: %s\r\n ",
                              VlanId, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);

            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId,
                            VlanId, (INT1 *) AST_PVRST_TRAPS_OID,
                            PVRST_BRG_TRAPS_OID_LEN);
        }

        pPerStBrgInfo->RootId = TmpRootId;
        pPerStBrgInfo->u2RootPort = u2TmpRootPort;
        pPerStBrgInfo->u4RootCost = u4TmpRootCost;
        pPerStPvrstBridgeInfo->RootTimes = pPerStPvrstBridgeInfo->BridgeTimes;
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            /* Port is not yet created */
            continue;
        }
        if (AST_IS_PVRST_ENABLED ())
        {
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

            switch (pPerStRstPortInfo->u1InfoIs)
            {
                case PVRST_INFOIS_DISABLED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: InfoIs is DISABLED\n",
                                  u2PortNum);
                    u1PrevPortRole =
                        PVRST_GET_PORT_ROLE (u2InstIndex, u2PortNum);
                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DISABLED;

                    if (u1PrevPortRole != AST_PORT_ROLE_DISABLED)
                    {
                        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) =
                            PVRST_TRUE;
                    }

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %u: Port Role Selected"
                                  "as -> DISABLED\n", u2PortNum);
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: Port Role Selected"
                                  "as -> DISABLED\n", u2PortNum);
                    break;

                case PVRST_INFOIS_AGED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: InfoIs is AGED\n", u2PortNum);
                    pPerStRstPortInfo->bUpdtInfo = PVRST_TRUE;
                    u1PrevPortRole =
                        PVRST_GET_PORT_ROLE (u2InstIndex, u2PortNum);
                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;

                    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                    {
                        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) =
                            PVRST_TRUE;
                    }

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %u: Port Role Selected"
                                  "as -> DESIGNATED\n", u2PortNum);
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: Port Role"
                                  "Selected as -> DESIGNATED\n", u2PortNum);
                    break;

                case PVRST_INFOIS_MINE:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: InfoIs is MINE\n", u2PortNum);
                    u1PrevPortRole =
                        PVRST_GET_PORT_ROLE (u2InstIndex, u2PortNum);

                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                    {
                        PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) =
                            PVRST_TRUE;
                    }

                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "RSSM: Port %u: Port Role"
                                  "Selected as -> DESIGNATED\n", u2PortNum);
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: Port Role Selected"
                                  "as -> DESIGNATED\n", u2PortNum);
                    i4RetVal =
                        PvrstIsDesgPrBetterThanPortPr (u2PortNum, u2InstIndex);
                    if (i4RetVal == PVRST_SAME_MSG)
                    {
                        /* Root Times is compared instead of
                         * Port Times of Root Port.
                         */
                        pPortTimes = &(pPerStPvrstRstPortInfo->PortPvrstTimes);
                        pRootTimes = &(pPerStPvrstBridgeInfo->RootTimes);

                        if ((pPortTimes->u2MaxAge != pRootTimes->u2MaxAge) ||
                            (pPortTimes->u2HelloTime != pRootTimes->u2HelloTime)
                            || (pPortTimes->u2ForwardDelay !=
                                pRootTimes->u2ForwardDelay) ||
                            (pPortTimes->u2MsgAgeOrHopCount !=
                             pRootTimes->u2MsgAgeOrHopCount))
                        {
                            pPerStRstPortInfo->bUpdtInfo = PVRST_TRUE;
                        }
                    }
                    else
                    {
                        pPerStRstPortInfo->bUpdtInfo = PVRST_TRUE;
                    }
                    break;

                case PVRST_INFOIS_RECEIVED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %u: InfoIs is RECEIVED\n",
                                  u2PortNum);

                    if (u2TmpRootPort == u2PortNum)
                    {
                        u1PrevPortRole =
                            PVRST_GET_PORT_ROLE (u2InstIndex, u2PortNum);

                        pPerStPortInfo->u1SelectedPortRole =
                            (UINT1) AST_PORT_ROLE_ROOT;
                        if (u1PrevPortRole != AST_PORT_ROLE_ROOT)
                        {
                            PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) =
                                PVRST_TRUE;
                        }

                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                      "RSSM: Port %u: Port Role"
                                      "Selected as -> ROOT PORT\n", u2PortNum);
                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %u: Port Role"
                                      "Selected as -> ROOT PORT\n", u2PortNum);
                        pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;
                    }
                    else
                    {
                        i4RetVal =
                            PvrstIsDesgPrBetterThanPortPr (u2PortNum,
                                                           u2InstIndex);
                        if (i4RetVal != PVRST_BETTER_MSG)
                        {
                            if (AST_MEMCMP
                                (&(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                 &(MyBrgId.BridgeAddr[0]),
                                 AST_MAC_ADDR_SIZE) != PVRST_BRGID1_SAME)
                            {
                                u1PrevPortRole =
                                    PVRST_GET_PORT_ROLE (u2InstIndex,
                                                         u2PortNum);

                                pPerStPortInfo->u1SelectedPortRole
                                    = (UINT1) AST_PORT_ROLE_ALTERNATE;
                                /* If operating in C-VLAN component, change
                                 * the role if the root port and the current
                                 * port are both provider edge ports. */
                                /* RstPbCheckAndAlterCvlanPortRole (u2PortNum,
                                   u2TmpRootPort); */

                                if (u1PrevPortRole !=
                                    pPerStPortInfo->u1SelectedPortRole)
                                {
                                    PVRST_SET_CHANGED_FLAG (u2InstIndex,
                                                            u2PortNum) =
                                        PVRST_TRUE;
                                }

                                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                              "RSSM: Port %u: Port Role"
                                              "Selected as -> ALTERNATE\n",
                                              u2PortNum);
                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %u: Port Role"
                                              "Selected as -> ALTERNATE\n",
                                              u2PortNum);
                                pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;
                                if (VlanIsVlanEnabledInContext
                                    (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
                                {
                                    PvrstTopologyChSmFlushEntries
                                        (pPerStPortInfo);
                                }

                            }
                            else
                            {
                                u2Val = (UINT2) (pPerStPortInfo->u2DesgPortId &
                                                 (UINT2) AST_PORTNUM_MASK);
                                if (u2Val != AST_GET_PROTOCOL_PORT (u2PortNum))
                                {
                                    u1PrevPortRole =
                                        PVRST_GET_PORT_ROLE (u2InstIndex,
                                                             u2PortNum);

                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_BACKUP;

                                    if (u1PrevPortRole != AST_PORT_ROLE_BACKUP)
                                    {
                                        PVRST_SET_CHANGED_FLAG (u2InstIndex,
                                                                u2PortNum) =
                                            PVRST_TRUE;
                                    }

                                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                                  "RSSM: Port %u: Port Role"
                                                  "Selected as -> BACKUP\n",
                                                  u2PortNum);
                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %u: Port Role"
                                                  "Selected as -> BACKUP\n",
                                                  u2PortNum);
                                    pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;
                                }
                                else
                                {
                                    /* Such a condition may occur when a BDPU is 
                                     * received that contains all the values 
                                     * that are the same as that would be 
                                     * transmitted from this port, except the 
                                     * designated port priority. For example, if 
                                     * this port is a designated port, and 
                                     * before a BPDU is received from the Root 
                                     * Port attached to this LAN, the Port 
                                     * Priority of this port is made worse. In 
                                     * this case, the values received from the 
                                     * Root Port will be the same as sent out by 
                                     * this Designated port, but with the old 
                                     * port priority value (which will be 
                                     * superior). In this case, the Port, which 
                                     * will still be a Designated Port, should 
                                     * update its designated and port priority
                                     * vector and transmit a BPDU with the new 
                                     * information. */
                                    u1PrevPortRole =
                                        PVRST_GET_PORT_ROLE (u2InstIndex,
                                                             u2PortNum);

                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                                    if (u1PrevPortRole !=
                                        AST_PORT_ROLE_DESIGNATED)
                                    {
                                        PVRST_SET_CHANGED_FLAG (u2InstIndex,
                                                                u2PortNum) =
                                            PVRST_TRUE;
                                    }

                                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                                  "RSSM: Port %u: Port Role"
                                                  " Selected as -> DESIGNATED\n",
                                                  u2PortNum);
                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %u: Port Role"
                                                  "Selected as -> DESIGNATED\n",
                                                  u2PortNum);
                                    pPerStRstPortInfo->bUpdtInfo = PVRST_TRUE;
                                }
                            }
                        }
                        else
                        {
                            u1PrevPortRole =
                                PVRST_GET_PORT_ROLE (u2InstIndex, u2PortNum);

                            pPerStPortInfo->u1SelectedPortRole =
                                (UINT1) AST_PORT_ROLE_DESIGNATED;
                            if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
                            {
                                PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum)
                                    = PVRST_TRUE;
                            }

                            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                          "RSSM: Port %u: Port Role Selected"
                                          "as -> DESIGNATED\n", u2PortNum);
                            AST_DBG_ARG1 (AST_RSSM_DBG,
                                          "RSSM: Port %u: Port Role"
                                          "Selected as -> DESIGNATED\n",
                                          u2PortNum);
                            pPerStRstPortInfo->bUpdtInfo = PVRST_TRUE;
                        }
                    }
                    break;
                default:
                    return PVRST_FAILURE;
            }                    /* End Switch */
            if ((PVRST_GET_CHANGED_FLAG (u2InstIndex, u2PortNum) == PVRST_TRUE))
            {
                /*Port Role Changed -> Generate Trap */
                AstNewPortRoleTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    pPerStPortInfo->u1SelectedPortRole,
                                    u1PrevPortRole, u2InstIndex,
                                    (INT1 *) AST_PVRST_TRAPS_OID,
                                    PVRST_BRG_TRAPS_OID_LEN);
                if (pPerStPortInfo->u1PortRole !=
                    pPerStPortInfo->u1SelectedPortRole)
                {
                    AST_MEMSET (au1OldRole, 0, AST_BRG_ADDR_DIS_LEN);
                    AST_MEMSET (au1NewRole, 0, AST_BRG_ADDR_DIS_LEN);
                    AstGetRoleStr (pPerStPortInfo->u1PortRole, au1OldRole);
                    AstGetRoleStr (pPerStPortInfo->u1SelectedPortRole,
                                   au1NewRole);
                    if (L2IwfMiIsVlanMemberPort
                        (u4ContextId, VlanId,
                         AST_GET_IFINDEX (u2PortNum)) == OSIX_TRUE)
                    {

                        AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG5 (AST_CONTROL_PATH_TRC,
                                          "Port:%s Vlan:%u Old Role:%s New Role:%s Time Stamp: %s\r\n",
                                          AST_GET_IFINDEX_STR (u2PortNum),
                                          VlanId, au1OldRole, au1NewRole,
                                          au1TimeStr);
                    }
                }
                pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                /* Changes Related to CSR 125954 */
                if ((u1PrevPortRole == AST_PORT_ROLE_DESIGNATED) &&
                    ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ROOT)
                     || (pPerStPortInfo->u1SelectedPortRole ==
                         AST_PORT_ROLE_ALTERNATE))
                    && (pPortInfo->bLoopGuard == PVRST_TRUE))
                {
                    pPerStPortInfo->bLoopGuardStatus = PVRST_TRUE;
                }

                if (AST_NODE_STATUS () == RED_AST_ACTIVE)
                {
                    AstRedSendUpdate ();
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstIsDesgPrBetterThanPortPr                        */
/*                                                                           */
/* Description        : This routine compares the Designated Priority vector */
/*                      and the Port Priority vector and returns if the      */
/*                      Designated Priority vector is BETTER THAN or INFERIOR*/
/*                      TO or SAME AS the Port Priority vector.              */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*              : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_BETTER_MSG / PVRST_INFERIOR_MSG                */
/*                        PVRST_SAME_MSG                                         */
/*****************************************************************************/
INT4
PvrstIsDesgPrBetterThanPortPr (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstBridgeId        MyBrgId;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

    if (NULL == pPerStPortInfo)
    {
        return PVRST_SAME_MSG;
    }
    MyBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(MyBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    i4RetVal = RstCompareBrgId (&(pPerStBrgInfo->RootId),
                                &(pPerStPortInfo->RootId));
    switch (i4RetVal)
    {
        case PVRST_BRGID1_SUPERIOR:
            return PVRST_BETTER_MSG;
        case PVRST_BRGID1_INFERIOR:
            return PVRST_INFERIOR_MSG;
        default:
            break;
    }
    /* Same Root Id */
    if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
    {
        return PVRST_BETTER_MSG;
    }
    else if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
    {
        return PVRST_INFERIOR_MSG;
    }
    /* Same Root Path Cost */
    i4RetVal = RstCompareBrgId (&(MyBrgId), &(pPerStPortInfo->DesgBrgId));
    switch (i4RetVal)
    {
        case PVRST_BRGID1_SUPERIOR:
            return PVRST_BETTER_MSG;
        case PVRST_BRGID1_INFERIOR:
            return PVRST_INFERIOR_MSG;
        default:
            /* Same DesgBrgId */
            break;
    }
    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    u2Val = u2Val | AST_GET_PROTOCOL_PORT (u2PortNum);
    if (u2Val < pPerStPortInfo->u2DesgPortId)
    {
        return PVRST_BETTER_MSG;
    }
    else if (u2Val > pPerStPortInfo->u2DesgPortId)
    {
        return PVRST_INFERIOR_MSG;
    }
    return PVRST_SAME_MSG;
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmSetSelectedBridge                     */
/*                                                                           */
/* Description        : This routine performs the 'setSelectedBridge'        */
/*                      procedure of the state machine.                      */
/*                      This routine sets the selected variable for all the  */
/*                      ports.                                               */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleSelSmSetSelectedBridge (UINT2 u2InstIndex)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPort = (UINT2) AST_INIT_VAL;

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    u2TmpRootPort = pPerStBrgInfo->u2RootPort;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        /* Call the Role Transition State Machine or
         * Port Information State Machine for all the ports 
         * other than Root Port. 
         */
        if (u2PortNum != u2TmpRootPort)
        {
            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
            {
                pPerStPortInfo =
                    PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                pPerStRstPortInfo->bSelected = (tAstBoolean) PVRST_TRUE;

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %u: SELECTED variable SET\n",
                              u2PortNum);

                if (pPerStRstPortInfo->bUpdtInfo == (tAstBoolean) PVRST_FALSE)
                {
                    if (!
                        ((pPerStPortInfo->u2Inst != AST_DEF_VLAN_ID ())
                         && ((AST_FORCE_VERSION == AST_VERSION_0)
                             || (pPerStRstPortInfo->pPerStPvrstRstPortInfo->
                                 bSendRstp == PVRST_FALSE))))
                    {

                        if (PvrstPortRoleTrMachine
                            ((UINT2) PVRST_PROLETRSM_EV_SELECTED_SET,
                             pPerStPortInfo) != PVRST_SUCCESS)
                        {
                            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                          AST_ALL_FAILURE_TRC,
                                          "RSSM: Port %u: PvrstPortRoleTrMachine"
                                          "function returned FAILURE!\n",
                                          u2PortNum);
                            AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                          "RSSM: Port %u: PvrstPortRoleTrMachine"
                                          "function returned FAILURE!\n",
                                          u2PortNum);
                            return PVRST_FAILURE;
                        }
                    }
                }
                else
                {
                    if (PvrstPortInfoMachine
                        ((UINT2) PVRST_PINFOSM_EV_UPDATEINFO, pPerStPortInfo,
                         (tPvrstBpdu *) AST_NO_VAL) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %u: PvrstPortInfoMachine"
                                      "function returned FAILURE!\n",
                                      u2PortNum);
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %u: PvrstPortInfoMachine"
                                      "function returned FAILURE!\n",
                                      u2PortNum);
                        return PVRST_FAILURE;
                    }
                }
            }
        }

    }
    /* Now Call the Role Transition State Machine or
     * Port Information State Machine for Root Port.
     */
    if (u2TmpRootPort != AST_INIT_VAL)
    {
        u2PortNum = pPerStBrgInfo->u2RootPort;
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
        {
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (NULL == pPerStPortInfo)
            {
                return PVRST_FAILURE;
            }
            pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pPerStRstPortInfo->bSelected = (tAstBoolean) PVRST_TRUE;

            AST_DBG_ARG1 (AST_RSSM_DBG,
                          "RSSM: Port %u: SELECTED variable SET\n", u2PortNum);

            if (pPerStRstPortInfo->bUpdtInfo == (tAstBoolean) PVRST_FALSE)
            {
                if (!
                    ((pPerStPortInfo->u2Inst != AST_DEF_VLAN_ID ())
                     && ((AST_FORCE_VERSION == AST_VERSION_0)
                         || (pPerStRstPortInfo->pPerStPvrstRstPortInfo->
                             bSendRstp == PVRST_FALSE))))
                {
                    if (PvrstPortRoleTrMachine ((UINT2)
                                                PVRST_PROLETRSM_EV_SELECTED_SET,
                                                pPerStPortInfo) !=
                        PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %u: PvrstPortRoleTrMachine"
                                      "function returned FAILURE!\n",
                                      u2PortNum);
                        AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %u: PvrstPortRoleTrMachine"
                                      "function returned FAILURE!\n",
                                      u2PortNum);
                        return PVRST_FAILURE;
                    }
                }
            }
            else
            {
                if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_UPDATEINFO,
                                          pPerStPortInfo,
                                          (tPvrstBpdu *) AST_NO_VAL) !=
                    PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RSSM: Port %u: PvrstPortInfoMachine"
                                  "function returned FAILURE!\n", u2PortNum);
                    AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RSSM: Port %u: PvrstPortInfoMachine"
                                  "function returned FAILURE!\n", u2PortNum);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleSelSmEventImpossible                       */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : u2InstIndex - Index of Spanning Tree Identifier      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_FAILURE                                        */
/*****************************************************************************/
INT4
PvrstProleSelSmEventImpossible (UINT2 u2InstIndex)
{
    AST_TRC (AST_CONTROL_PATH_TRC,
             "RSSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_DBG (AST_RSSM_DBG,
             "RSSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_UNUSED (u2InstIndex);
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstInitPortRoleSelectionMachine                    */
/*                                                                           */
/* Description        : Initialises the Port Role Selection State Machine.   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PvrstInitPortRoleSelectionMachine (VOID)
{

/*#if defined(TRACE_WANTED) || defined(RST_DEBUG)*/
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_PROLESELSM_MAX_EVENTS][20] = {
        "BEGIN", "RESELECT"
    };
    UINT1               aau1SemState[PVRST_PROLESELSM_MAX_STATES][20] = {
        "INIT_BRIDGE", "ROLE_SELECTION"
    };

    for (i4Index = 0; i4Index < PVRST_PROLESELSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_RSSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_RSSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_PROLESELSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_RSSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_RSSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /*#endif *//* PVRST_TRACE_WANTED || PVRST_DEBUG */

    /* Event 0 - PVRST_PROLESELSM_EV_BEGIN */
    PVRST_PORT_ROLE_SEL_MACHINE[PVRST_PROLESELSM_EV_BEGIN]
        [PVRST_PROLESELSM_STATE_INIT_BRIDGE].pAction = PvrstProleSelSmMakeInit;
    PVRST_PORT_ROLE_SEL_MACHINE[PVRST_PROLESELSM_EV_BEGIN]
        [PVRST_PROLESELSM_STATE_ROLE_SELECTION].pAction =
        PvrstProleSelSmMakeInit;

    /* Event 1 - PVRST_PROLESELSM_EV_RESELECT */
    PVRST_PORT_ROLE_SEL_MACHINE[PVRST_PROLESELSM_EV_RESELECT]
        [PVRST_PROLESELSM_STATE_INIT_BRIDGE].pAction =
        PvrstProleSelSmEventImpossible;
    PVRST_PORT_ROLE_SEL_MACHINE[PVRST_PROLESELSM_EV_RESELECT]
        [PVRST_PROLESELSM_STATE_ROLE_SELECTION].pAction =
        PvrstProleSelSmMakeRoleSelection;

    AST_DBG (AST_TCSM_DBG, "RSSM: Loaded RSSM SEM successfully\n");

    return;
}
