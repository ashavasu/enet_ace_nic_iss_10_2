/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvsys.c,v 1.103 2018/01/22 09:40:40 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              PVRST Module. 
 *
 *****************************************************************************/

#ifndef _ASTVSYS_C
#define _ASTVSYS_C

#include "asthdrs.h"
#include "astvinc.h"

#ifdef NPAPI_WANTED
extern INT4         gi4AstInitProcess;
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstModuleInit                            */
/*                                                                           */
/*    Description               : Initialises the PVRST module global data   */
/*                                structures and creates memory pools. Also  */
/*                                creates timer list.                        */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstModuleInit (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PortsCreated = AST_INIT_VAL;
    tPortList           EgressIfPorts;
    tPortList           UntagIfPorts;
    tPortList           ResultIfPorts;
    tVlanId             VlanId;
    UINT2               u2PrevPort = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;

    /* Assumption:
     * The appropriate context is selected before calling this function*/

    if (AST_IS_PVRST_STARTED ())
    {
        return PVRST_SUCCESS;
    }

    if (MrpApiIsMvrpEnabled (AST_CURR_CONTEXT_ID ()) == OSIX_TRUE)
    {
        AST_TRC (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: MVRP Module is Enabled.It should be disabled"
                 "before enabling PVRST. !!!!\n");
        return PVRST_FAILURE;
    }
    else if (GvrpIsGvrpEnabledInContext (AST_CURR_CONTEXT_ID ()) == GVRP_TRUE)
    {
        AST_TRC (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: GVRP Module is Enabled.It should be disabled"
                 "before enabling PVRST. !!!!\n");
        return PVRST_FAILURE;

    }
    /* Setting Force version to AST_VERSION_2 i.e in RSTP Mode */
    (AST_CURR_CONTEXT_INFO ())->u1ForceVersion = AST_VERSION_2;
    AST_GEN_TRAP = AST_INIT_VAL;

    /*State-Machine variable changes Debug is not currently supported 
       in PVRST. This check is put because if RSTP is shut down and 
       Pvrst is started, then the debug option cannot be more than the
       maximum supported by PVRST debug option. */
    if ((AST_CURR_CONTEXT_INFO ())->u4DebugOption > PVRST_MAX_DEBUG_VAL)
    {
        (AST_CURR_CONTEXT_INFO ())->u4DebugOption = PVRST_MAX_DEBUG_VAL;
    }

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             " Initializing PVRST Module ... \n");
    AST_DBG (AST_INIT_SHUT_DBG, " Initializing PVRST Module ... \n");

    /* Getting the Bridge Mode from the L2IWF */
    if (L2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &(AST_BRIDGE_MODE ()))
        != L2IWF_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Bridge Mode from l2iwf Failed !!!!\n  quitting...\n");
        (VOID) PvrstModuleShutdown ();
        return PVRST_FAILURE;
    }

    /* In SI case, the Bridge Mode shall not be Invalid Bridge Mode. */
    if (AST_BRIDGE_MODE () == AST_INVALID_BRIDGE_MODE)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Bridge Mode is Invalid. Bridge Mode should be"
                 "configured properly before starting the AST Module \r\n");
        (VOID) PvrstModuleShutdown ();
        return PVRST_FAILURE;
    }

    /*Depending on the Bridge Mode, the Component Type is chosen. */
    AstUpdateCompType ();

    /*For SVLAN component, the Max Number of Ports Per Context is the Port
       Table size of the context */
    AST_MAX_NUM_PORTS = AST_MAX_PORTS_PER_CONTEXT;

    if (AstAllocPortAndPerStInfoBlock (AST_PVRST_MODULE) != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " SYS: Allocation of PortInfo and PerStInfo Table array"
                 "failed ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 " SYS: Allocation of PortInfo and PerStInfo Table array"
                 "failed ...\n");
        (VOID) PvrstModuleShutdown ();

        return PVRST_FAILURE;
    }

    /* Initialize Instance to Vlan Mapping to Init Value & Do same
     * in L2-Iwf also */
    if (PvrstInitInstIndexMap () != PVRST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " SYS: Allocation of Instance to Index Map " "failed ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 " SYS: Allocation of Instance to Index Map " "failed ...\n");
        (VOID) PvrstModuleShutdown ();

        return PVRST_FAILURE;
    }

    L2IwfVlanToInstMappingInit (AST_CURR_CONTEXT_ID ());

    /* Initialize the Pvrst State Machines */
    PvrstInitStateMachines ();

    pBrgInfo = AST_GET_BRGENTRY ();
    pBrgInfo->u1MigrateTime = (UINT1) RST_DEFAULT_MIGRATE_TIME;

    /* Since CVLAN component Mac address is the CEP port Mac Address, the
     * Original Switch Mac address should not be used for CVLAN component*/
    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        IssGetContextMacAddress (AST_CURR_CONTEXT_ID (), pBrgInfo->BridgeAddr);
    }
    else
    {
        /*For CVLAN component, the Max Number of Services that can be given to
           a customer will be the port table size of the CVLAN context */
        AstPbCVlanFillBrgMacAddr (pBrgInfo->BridgeAddr);
    }

    if (AST_IS_CVLAN_COMPONENT () == RST_FALSE)
    {
        AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = AST_INIT_VAL;

        /* It finds the next valid port for this context & checks if this is
         * already mapped to a channel if not create that port for PVRST */

        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {

            while (AstL2IwfGetNextValidPortForContext (AST_CURR_CONTEXT_ID (),
                                                       u2PrevPort, &u2PortNum,
                                                       &u4IfIndex) ==
                   L2IWF_SUCCESS)
            {
                /* If this port is a member of any channel move onto next port */
                if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
                {
                    u2PrevPort = u2PortNum;
                    continue;
                }

                if (AstIsExtInterface ((INT4) u2PortNum) == AST_TRUE)
                {
                    u2PrevPort = u2PortNum;
                    continue;
                }

                /* Port creation should be blocked if the number of ports created has reached
                 * max ports in system. */

                AstGetNumOfPortsCreated (&u4PortsCreated);

                if (u4PortsCreated >= AST_SIZING_PORT_COUNT)
                {
                    break;
                }

                /* PvstCreatePort is called which create Port for PVRST.
                 * If this return Failure then PvrstModuleShutdown is called
                 * which will uninitialise all the initialised parameters */
                if (PvrstCreatePort (u4IfIndex, u2PortNum) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "Port %u: CreatePort returned failure  !!!\n",
                                  u4IfIndex);
                    (VOID) PvrstModuleShutdown ();
                    return PVRST_FAILURE;
                }

#ifdef L2RED_WANTED
                if (AstRedCreatePortAndPerPortInst (u2PortNum) == RST_FAILURE)
                {
                    (VOID) PvrstModuleShutdown ();
                    return PVRST_FAILURE;
                }
#endif

                /* Oper status returned here is sum of
                 * all lower layer oper status */
                AstGetPortOperStatusFromL2Iwf (STP_MODULE, u2PortNum,
                                               &u1OperStatus);

                /* The Port path cost is updated if the link is already UP
                 * when the Spanning Tree module is STARTED
                 */
                pPortInfo = AST_GET_PORTENTRY (u2PortNum);

                if ((pPortInfo->bPathCostInitialized == RST_FALSE) &&
                    (u1OperStatus == AST_UP))
                {
                    pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
                    pPortInfo->bPathCostInitialized = RST_TRUE;
                }

                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "Port %u: CreatePort returned success !!!\n",
                              u4IfIndex);
                u2PrevPort = u2PortNum;
            }
        }

    }
    /* Initialize Ast Up/Down Count with 0 */
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    PVRST_GET_NO_OF_ACTIVE_INSTANCES = AST_INIT_VAL;
    AST_FLUSH_INTERVAL = PVRST_DEFAULT_FLUSH_INTERVAL;
    pBrgInfo->u1DynamicPathcostCalculation = PVRST_FALSE;
    pBrgInfo->u1DynamicPathcostCalcLagg = AST_SNMP_FALSE;
    pBrgInfo->bBridgeClearStats = PVRST_DISABLED;

    /* Allocatng memory for Instance state */
    AstL2IwfAllocInstActiveStateMem (AST_CURR_CONTEXT_ID (), PVRST_START);

    /* Create Instance for all the vlan's present */
    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        AST_MEMSET (EgressIfPorts, 0, sizeof (tPortList));
        AST_MEMSET (UntagIfPorts, 0, sizeof (tPortList));
        AST_MEMSET (ResultIfPorts, 0, sizeof (tPortList));

        AstL2IwfMiGetVlanEgressPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                      EgressIfPorts);

        L2IwfGetUntaggedPortList (AST_CURR_CONTEXT_ID (), VlanId, UntagIfPorts);

        PVRST_VLAN_XOR_PORT_LIST (ResultIfPorts, EgressIfPorts, UntagIfPorts);

        if (AstIsPvrstInstExist () == AST_FALSE)
        {

            if ((PVRST_VLAN_IS_NULL_PORTLIST (EgressIfPorts) == PVRST_TRUE) ||
                ((PVRST_VLAN_IS_NULL_PORTLIST (ResultIfPorts) == PVRST_TRUE) &&
                 (AstVlanApiIsPvidVlanOfAnyPorts (VlanId) == AST_FALSE)))
            {
                continue;
            }
        }
        /*If no instance present then create instance */
        if (PvrstCreateInstance (VlanId) == PVRST_FAILURE)
        {
            PvrstModuleShutdown ();

            AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                     "SYS: Instance not created for Vlan.\n");
            AST_DBG (AST_INIT_SHUT_DBG,
                     "SYS: instance not created for Vlan.\n");
            return PVRST_FAILURE;
        }
    }

    /* Module Status is set to Disabled & need to be Enabled when Vlan module
     * is enabled */
    AST_MODULE_STATUS_SET (PVRST_DISABLED);
    AST_ADMIN_STATUS = (UINT1) PVRST_DISABLED;

    /* System Control is set to PVRST_START */
    AST_SYSTEM_CONTROL_SET (PVRST_START);

    /* Make S-VLAN component status as enabled. This makes the
     * PvrstModuleEnable to enable the S-VLAN spanning tree during
     * system start up. */
    AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

    /* When system comes up as a 1ad bridge, Ast will come up before
     * vlan and hence the port types in l2iwf will be customer bridge
     * port. So if AstPbCheckAllPortTypes is called, it will return 
     * failure. So don't call that if vlan is in shutdown state. */
    if (VlanGetStartedStatus (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        /* In case of 1ad bridge, initialize the C-VLAN components. */
        if (AST_IS_1AD_BRIDGE () == RST_TRUE)
        {
            if (AstPbCheckAllPortTypes () == RST_FAILURE)
            {
                AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                         AST_ALL_FAILURE_TRC,
                         "SYS: Pvrst Initialization in PbCheckAll Port Types"
                         "Failed \r\n");
                (VOID) PvrstModuleShutdown ();
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return PVRST_FAILURE;
            }
        }
    }

    /*AST_PVRST_TRAP_TYPE = AST_TRAP_ABNORMAL; */
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Initialised PVRST Module SUCCESSFULLY... \n");
    AST_DBG (AST_INIT_SHUT_DBG,
             "SYS: Initialised PVRST Module SUCCESSFULLY... \n");

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
        if (pPortInfo == NULL)
        {
            continue;
        }
        AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                   &u1OperStatus);
        if ((u1OperStatus == (UINT1) AST_PORT_OPER_UP)
            && (pPortInfo->CommPortInfo.bPortPvrstStatus == PVRST_TRUE))
        {
            AstFlushFdbEntries (pPortInfo, VLAN_NO_OPTIMIZE);
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstModuleShutdown                        */
/*                                                                           */
/*    Description               : Deinitialises CVLAN & SVLAN PVRST module   */
/*                                structures and deletes memory pools. Also  */
/*                                deletes timer list.                        */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstModuleShutdown (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    INT4                i4RetVal = PVRST_SUCCESS;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Shutting down Pvrst Module ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Shutting down Pvrst Module ...\n");

    /* Check if PVRST is not started then return Success */
    if (!AST_IS_PVRST_STARTED ())
    {
        return PVRST_SUCCESS;
    }

    if (AstPbAllCVlanCompShutdown () != RST_SUCCESS)
    {
        i4RetVal = PVRST_FAILURE;
    }

    /* Check if PVRST Module is Enabled then it need to be Disabled before
     * Shutting Down */
    if (AST_MODULE_STATUS == PVRST_ENABLED)
    {
        (VOID) PvrstModuleDisable ();
    }

    /* Set module status as disabled. */
    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = PVRST_DISABLED;

    /* Set System Control value to PVRST_SHUTDOWN */
    AST_SYSTEM_CONTROL_SET (PVRST_SHUTDOWN);

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        /*If instance is present then delete instance */
        if (PvrstDeleteInstance (VlanId) == PVRST_FAILURE)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                     "SYS: Instance not Deleted for Vlan.\n");
            AST_DBG (AST_INIT_SHUT_DBG, "SYS: instance not"
                     "deleted for Vlan.\n");
            i4RetVal = PVRST_FAILURE;
        }
    }

    /* Allocatng memory for Instance state */
    AstL2IwfAllocInstActiveStateMem (AST_CURR_CONTEXT_ID (), PVRST_SHUTDOWN);

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;            /* If this port does not exist move onto next */
        }

        if (PvrstDeletePort (u2PortNum))
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "Port %u: DeletePort returned failure  !!!\n",
                          u2PortNum);
        }
        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            PvrstRedClearPduOnActive (u2PortNum);
        }

#ifdef L2RED_WANTED
        AstRedDeletePortAndPerPortInst (u2PortNum);
#endif
    }

    AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = AST_INIT_VAL;
    AST_PB_CVLAN_INFO () = NULL;

    /* Initialize Array of Instance to Vlan Mapping to Init Value */
    PvrstDeInitInstIndexMap ();

    AstReleasePortAndPerStInfoBlock ();

    pBrgInfo = AST_GET_BRGENTRY ();
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    PVRST_GET_NO_OF_ACTIVE_INSTANCES = AST_INIT_VAL;
    AST_BRIDGE_MODE () = AST_INVALID_BRIDGE_MODE;
    AST_GEN_TRAP = AST_INIT_VAL;

    if (VlanConfigUpdtForPvrstShutDown (AST_CURR_CONTEXT_ID ()) == VLAN_FAILURE)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Vlan Configuration for PVRST Shutdown Failed !!!!\n "
                 "quitting...\n");
        i4RetVal = PVRST_FAILURE;
    }

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Pvrst Module Shutdown SUCCESSFULLY\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Pvrst Module" "Shutdown SUCCESSFULLY\n");

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstModuleEnable                          */
/*                                                                           */
/*    Description               : Called by management when PVRST is enabled.*/
/*                                Enables CVLAN and SVLAN modules in PEB.    */
/*                                In other Bridge Modes it enables the SVLAN */
/*                                module only                                */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstModuleEnable (VOID)
{
    if (AST_CURR_CONTEXT_ID () != AST_DEFAULT_CONTEXT)
    {
        return PVRST_FAILURE;
    }
    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_ENABLED)
    {
        return PVRST_SUCCESS;
    }

    AST_ADMIN_STATUS = PVRST_ENABLED;

    if (AST_IS_1AD_BRIDGE () == RST_TRUE)
    {
        /* If S-VLAN component is configured as enabled, then enable the 
         * S-VLAN component spanning tree. */
        if (AST_SVLAN_ADMIN_STATUS () == PVRST_ENABLED)
        {
            if (PvrstComponentEnable () != PVRST_SUCCESS)
            {
                return PVRST_FAILURE;
            }
        }
    }
    else
    {
        /* In case of customer / provider bridge just enable the module. */
        if (PvrstComponentEnable () != PVRST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }

    if (AstPbAllCVlanCompEnable () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN PVRST Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN PVRST Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        return PVRST_FAILURE;
    }

    if (AST_NODE_STATUS () != RED_AST_IDLE)
    {
        gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = PVRST_ENABLED;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstComponentEnable                       */
/*                                                                           */
/*    Description               : Enables PVRST module makes it operational  */
/*                                Called by management action to enable the  */
/*                                PVRST Module.Then calls the CFA Module and */
/*                                allocates memory for all ports that are    */
/*                                created already. Also for all the ports    */
/*                                that are operationally up it initialises   */
/*                                the PVRST state machines.                  */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstComponentEnable (VOID)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tPortList           EgressIfPorts;
    tPortList           UntagIfPorts;
    tPortList           ResultIfPorts;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    tPortList          *pEgressIfPorts = NULL;
    tPortList          *pUntagIfPorts = NULL;
    tPortList          *pResultIfPorts = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Enabling Pvrst Module.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Enabling Pvrst Module.\n");

    if (AST_MODULE_STATUS == PVRST_ENABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Enabled...Quitting"
                 "without processing.\n");
        return PVRST_SUCCESS;
    }
    if (gu1PvrstInstExistance == RST_FALSE)
    {
        pEgressIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pEgressIfPorts == NULL)
        {
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return MST_FAILURE;
        }
        pUntagIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pUntagIfPorts == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return MST_FAILURE;
        }
        pResultIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pResultIfPorts == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return MST_FAILURE;
        }
    }

    AST_GET_SYSTEMACTION = PVRST_ENABLED;

    /* In Active & Standby node PVRST can be directly enabled.
     * When NodeStatus is in IDLE State,PVRST should not be enabled.
     * PVRST can be enabled only when NodeStatus is in ACTIVE or STANDBY State.
     */

    if (AST_NODE_STATUS () <= RED_AST_IDLE)
    {
        if (gu1PvrstInstExistance == RST_FALSE)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
        }

        return PVRST_SUCCESS;
    }
    /* Setting the Module Status as ENABLED */
    AST_MODULE_STATUS_SET (PVRST_ENABLED);

    /* Incrementing the Pvrst Up Count. It holds value of how many times Pvrst
     * Module is enabled */
    AST_INCR_ASTP_UP_COUNT ();

    PvrstReInitPvrstInfo ();

    if (PvrstHwInit () != PVRST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " Hardware Initialization failed !!!\n");
#ifdef NPAPI_WANTED
        gi4AstInitProcess = 0;
#endif
        if (gu1PvrstInstExistance == RST_FALSE)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
        }

        return PVRST_FAILURE;
    }

#ifdef NPAPI_WANTED
    gi4AstInitProcess = 1;
#endif

    /*Enable Instance for all the vlan's present */
    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        MEMSET (EgressIfPorts, 0, sizeof (tPortList));
        MEMSET (UntagIfPorts, 0, sizeof (tPortList));
        MEMSET (ResultIfPorts, 0, sizeof (tPortList));

        AstL2IwfMiGetVlanEgressPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                      EgressIfPorts);

        L2IwfGetUntaggedPortList (AST_CURR_CONTEXT_ID (), VlanId, UntagIfPorts);

        PVRST_VLAN_XOR_PORT_LIST (ResultIfPorts, EgressIfPorts, UntagIfPorts);
        if (AstIsPvrstInstExist () == AST_FALSE)
        {

            if ((PVRST_VLAN_IS_NULL_PORTLIST (EgressIfPorts) == PVRST_TRUE) ||
                ((PVRST_VLAN_IS_NULL_PORTLIST (ResultIfPorts) == PVRST_TRUE) &&
                 (AstVlanApiIsPvidVlanOfAnyPorts (VlanId) == AST_FALSE)))
            {
                continue;
            }
        }
        if (gu1PvrstInstExistance == RST_FALSE)
        {
            AST_MEMSET (pEgressIfPorts, AST_INIT_VAL, sizeof (tPortList));
            AST_MEMSET (pUntagIfPorts, AST_INIT_VAL, sizeof (tPortList));
            AST_MEMSET (pResultIfPorts, AST_INIT_VAL, sizeof (tPortList));

            AstL2IwfMiGetVlanEgressPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                          *pEgressIfPorts);

            AstL2IwfMiGetVlanUnTagPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                         *pUntagIfPorts);

            PVRST_VLAN_XOR_PORT_LIST (((UINT1 *) *pResultIfPorts),
                                      ((UINT1 *) *pEgressIfPorts),
                                      ((UINT1 *) *pUntagIfPorts));

            if ((PVRST_VLAN_IS_NULL_PORTLIST (pEgressIfPorts) == PVRST_TRUE) ||
                ((PVRST_VLAN_IS_NULL_PORTLIST (pResultIfPorts) == PVRST_TRUE) &&
                 (AstL2IwfIsPvidVlanOfAnyPorts (AST_CURR_CONTEXT_ID (), VlanId)
                  == AST_FALSE)))
            {
                continue;
            }
        }

        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
        if (u2InstIndex == INVALID_INDEX)
        {
#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif

            if ((PVRST_GET_NO_OF_ACTIVE_INSTANCES == AST_MAX_PVRST_INSTANCES)
                || (PVRST_GET_NO_OF_ACTIVE_INSTANCES == AST_INIT_VAL))
            {
                if (gu1PvrstInstExistance == RST_FALSE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                    FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                    FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
                }
                return PVRST_SUCCESS;
            }
            if (gu1PvrstInstExistance == RST_FALSE)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
            }

            return PVRST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }
        if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_BEGIN, VlanId) !=
            PVRST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Role Selection Machine returned error"
                          "for instance %d !!!\n", VlanId);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Role Selection Failure for Instance = %d"
                          "!!!\n", VlanId);

#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif
            if (gu1PvrstInstExistance == RST_FALSE)
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
            }

            return PVRST_FAILURE;
        }
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != VlanId)
                {
                    continue;
                }
            }

            AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                       &u1OperStatus);

            if (u1OperStatus == AST_UP)
            {
                pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;

                if (PvrstStartSemsForPort (pPerStPortInfo) == PVRST_FAILURE)
                {
                    AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "EnablePort: Port %u: StartSems"
                                  "returned failure !\n", u2PortNum);

#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    if (gu1PvrstInstExistance == RST_FALSE)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                        FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
                    }
                    return PVRST_FAILURE;
                }

                /*Restart Bridge Detection state machine on this port */
                PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BEGIN, u2PortNum);

                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if ((pPortInfo->CommPortInfo.bPortPvrstStatus == PVRST_FALSE) ||
                    (pRstPortInfo->bPortEnabled != PVRST_TRUE))
                {
                    continue;
                }

                AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                              "SYS: Port %u enabled. Triggering Enabled event"
                              "for Port Mig and Port Info SEM\n", u2PortNum);

                if (PvrstPortMigrationMachine
                    ((UINT2) PVRST_PMIGSM_EV_PORT_ENABLED, u2PortNum,
                     VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "SYS: Port %u: Port Protocol Migration Machine"
                                  "Returned failure  !!!\n", u2PortNum);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %u: Port Protocol Migration Machine"
                                  "Returned failure  !!!\n", u2PortNum);
#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    if (gu1PvrstInstExistance == RST_FALSE)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                        FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                        FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
                    }
                    return PVRST_FAILURE;
                }

                if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_PORT_ENABLED,
                                          pPerStPortInfo,
                                          NULL) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "SYS: Port %u: Port Info Machine Returned"
                                  "failure  !!!\n", u2PortNum);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %u: Port Info Machine Returned"
                                  "failure  !!!\n", u2PortNum);
#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    return PVRST_FAILURE;
                }
            }

        }
    }

#ifdef NPAPI_WANTED
    if (AST_IS_PVRST_ENABLED () &&
        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        VlanMiDeleteAllFdbEntries (AST_CURR_CONTEXT_ID ());
        gi4AstInitProcess = 0;
    }
#endif

    AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
             "SYS: PVRST Module Enabled.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: PVRST Module Enabled.\n");
    if (gu1PvrstInstExistance == RST_FALSE)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
        FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
        FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstModuleDisable                         */
/*                                                                           */
/*    Description               : It disables all the CVLAN component and    */
/*                                SVLAN component from the Provider Edge     */
/*                                Bridge. In Case of other Bridge modes, it  */
/*                                does normal disable processing.            */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstModuleDisable (VOID)
{
#ifdef L2RED_WANTED
    if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] ==
         (UINT1) PVRST_DISABLED)
        && (AST_ADMIN_STATUS == (UINT1) PVRST_DISABLED))
#else
    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == (UINT1) PVRST_DISABLED)
#endif
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without "
                 "processing.\n");
        return PVRST_SUCCESS;
    }

    if (AstPbAllCVlanCompDisable () != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    /*This function is called for the SVLAN component Disable */
    if (PvrstComponentDisable () != PVRST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN PVRST Module Disable is not proper...Failures Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN PVRST Module Disable is not proper...Failures Occurred !!!\n");

        return PVRST_FAILURE;
    }

    AST_ADMIN_STATUS = (UINT1) PVRST_DISABLED;
    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = PVRST_DISABLED;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstComponentDisable                      */
/*                                                                           */
/*    Description               : It disables the PVRST Module by disabling  */
/*                                all State Machines and releases all the    */
/*                                memory occupied by the PVRST module.       */
/*                                Stop all the timers that are running.      */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstComponentDisable (VOID)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4RetVal = PVRST_SUCCESS;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT1               u1PrevStatus;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Disabling Pvrst Module...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Disabling Pvrst Module...\n");

    if (AST_MODULE_STATUS == PVRST_DISABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without"
                 "processing.\n");
        return PVRST_SUCCESS;
    }
    AST_GET_SYSTEMACTION = PVRST_DISABLED;

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }
        pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            if (pPerStPortInfo->bLoopIncStatus == AST_TRUE)
            {
                pPerStPortInfo->bLoopIncStatus = AST_FALSE;
            }

            if (PvrstStopAllRunningTimers (pPortInfo, pPerStPortInfo)
                != PVRST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                              AST_INIT_SHUT_DBG,
                              "SYS: Unable to stop the running timers for port"
                              "%s instance %d!!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), VlanId);
                i4RetVal = PVRST_FAILURE;
            }

            if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
            {
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if (pRstPortInfo->bPortEnabled == PVRST_TRUE)
                {
                    u1PrevStatus =
                        AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum);

                    /* Indicate to L2Iwf and Hardware only if there is change in 
                     * port state */
                    if (u1PrevStatus == AST_PORT_STATE_FORWARDING)
                    {
                        continue;
                    }

                    /* Set port state as FORWARDING in the 
                     * common database and indicate to Hw.
                     */

                    AstSetInstPortStateToL2Iwf (u2InstIndex,
                                                u2PortNum,
                                                AST_PORT_STATE_FORWARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                        AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_PVRST_ENABLED () &&
                        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                    {
                        PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 AST_IFENTRY_IFINDEX
                                                 (pPortInfo), VlanId,
                                                 AST_PORT_STATE_FORWARDING);
                    }
#endif /* NPAPI_WANTED */

                }
            }
        }
        pPerStBrgInfo->u1ProleSelSmState = 0;
    }

    /* Incrementing the Pvrst Down Count. It holds value of how many times 
     * Module is disabled */
    AST_INCR_ASTP_DOWN_COUNT ();

    AST_MODULE_STATUS_SET (PVRST_DISABLED);
#ifdef NPAPI_WANTED
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if ((pPortInfo) == NULL)
        {
            continue;
        }
        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            continue;
        }
        for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
             u2InstIndex++)
        {
            if ((VlanId =
                 PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
            {
                continue;
            }
            if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                OSIX_FALSE)
            {
                continue;
            }
            if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
            {
                continue;
            }
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            if ((pRstPortInfo != NULL)
                && (pRstPortInfo->bPortEnabled == PVRST_TRUE))
            {
                printf ("Port Num is %d\n and Instance is %d\n", u2PortNum,
                        u2InstIndex);
                AstMiRstpNpSetPortState (u2PortNum, AST_PORT_STATE_FORWARDING);
                break;
            }
        }
    }
#endif

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Pvrst Module Disable is not proper..."
                 "Failures Occurred !!!\n" "          Still Continuing ...\n");
        AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Pvrst Module Disable is not proper..."
                 "Failures Occurred !!!\n" "          Still Continuing ...\n");
    }
    else
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Pvrst Module Disabled.\n");
        AST_DBG (AST_INIT_SHUT_DBG, "SYS: Pvrst Module Disabled.\n");
        PvrstHwDeInit ();
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstHwInit                                */
/*                                                                           */
/*    Description               : This function Reinitializes the Hardware   */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : gAstGlobalInfo                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstHwInit (VOID)
{
#ifdef NPAPI_WANTED
    UINT4               u4HwActiveInstance = AST_INIT_VAL;
    INT4                i4HwRetVal = AST_INIT_VAL;
    tVlanId             VlanId = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
#endif /* NPAPI_WANTED */

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Reinitializing Hardware ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Reinitializing Hardware ...\n");

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        PvrstFsMiPvrstNpInitHw (AST_CURR_CONTEXT_ID ());
    }

    /* Create Instance for all the vlan's present */
    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        u2InstIndex =
            AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }
        if (u4HwActiveInstance == AST_MAX_PVRST_INSTANCES)
        {
            break;
        }

        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            i4HwRetVal = PvrstFsMiPvrstNpCreateVlanSpanningTree
                (AST_CURR_CONTEXT_ID (), VlanId);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              " Creating instance %d in the hardware failed! \n",
                              VlanId);
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                              " Creating instance %d in the hardware failed! \n",
                              VlanId);
                return PVRST_FAILURE;
            }
            u4HwActiveInstance++;
        }

    }
#endif

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Hardware Reinitialized\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Hardware Reinitialized\n");

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstHwDeInit                              */
/*                                                                           */
/*    Description               : This function Resets the Pvrst information */
/*                                in the Hardware                            */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : gAstGlobalInfo                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstHwDeInit (VOID)
{

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Deinitializing Hardware ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Deinitializing Hardware ...\n");

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        PvrstFsMiPvrstNpDeInitHw (AST_CURR_CONTEXT_ID ());
    }
#endif /* NPAPI_WANTED */

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Hardware Deinitialized\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Hardware Deinitialized\n");

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstCreateInstance                        */
/*                                                                           */
/*    Description               : It creates instance for the given Vlan     */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCreateInstance (tVlanId VlanId)
{
    tPortList          *pEgressIfPorts = NULL;
    tPortList          *pUntagIfPorts = NULL;
    tPortList          *pResultIfPorts = NULL;

    if (gu1PvrstInstExistance == RST_FALSE)
    {
        pEgressIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pEgressIfPorts == NULL)
        {
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return PVRST_FAILURE;
        }
        pUntagIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pUntagIfPorts == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return PVRST_FAILURE;
        }
        pResultIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pResultIfPorts == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
            FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
            AST_DBG (AST_ALL_FAILURE_DBG,
                     "Error in Allocating memory for bitlist\n");
            AST_TRC (AST_ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist\n");
            return PVRST_FAILURE;
        }

        AST_MEMSET (pEgressIfPorts, AST_INIT_VAL, sizeof (tPortList));
        AST_MEMSET (pUntagIfPorts, AST_INIT_VAL, sizeof (tPortList));
        AST_MEMSET (pResultIfPorts, AST_INIT_VAL, sizeof (tPortList));

        AstL2IwfMiGetVlanEgressPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                      *pEgressIfPorts);
        AstL2IwfMiGetVlanUnTagPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                     *pUntagIfPorts);
        PVRST_VLAN_XOR_PORT_LIST (((UINT1 *) *pResultIfPorts),
                                  ((UINT1 *) *pEgressIfPorts),
                                  ((UINT1 *) *pUntagIfPorts));
        if (AstIsPvrstInstExist () == AST_FALSE)
        {
            if ((PVRST_VLAN_IS_NULL_PORTLIST (pEgressIfPorts) == PVRST_TRUE) ||
                ((PVRST_VLAN_IS_NULL_PORTLIST (pResultIfPorts) == PVRST_TRUE) &&
                 (AstL2IwfIsPvidVlanOfAnyPorts (AST_CURR_CONTEXT_ID (), VlanId)
                  == AST_FALSE)))
            {
                FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
                FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
                return PVRST_SUCCESS;
            }
        }
        FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
        FsUtilReleaseBitList ((UINT1 *) pUntagIfPorts);
        FsUtilReleaseBitList ((UINT1 *) pResultIfPorts);
    }

    if (PvrstWrCreateInstance (VlanId) != PVRST_SUCCESS)
    {
        AST_GLOBAL_TRC (AST_CURR_CONTEXT_ID (),
                        AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                        "MSG: Create Instance function returned FAILURE\n");
        AST_GLOBAL_DBG (AST_CURR_CONTEXT_ID (),
                        AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                        "MSG: Instance Created Failed...\n");
        return PVRST_FAILURE;
    }

    AST_GLOBAL_TRC (AST_CURR_CONTEXT_ID (), AST_INIT_SHUT_TRC,
                    "MSG: Instance Created successfully...\n");
    AST_GLOBAL_DBG (AST_CURR_CONTEXT_ID (),
                    AST_INIT_SHUT_DBG | AST_EVENT_HANDLING_DBG,
                    "MSG: Instance Created successfully...\n");

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstWrCreateInstance                      */
/*                                                                           */
/*    Description               : It creates instance for the given Vlan     */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstWrCreateInstance (tVlanId VlanId)
{

    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    UINT4               u4IfIndex = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    INT4                i4RetVal = 0;
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
#endif /* NPAPI_WANTED */
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2VlanCount = 0;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (gPvrstVlanIdList, 0, VLAN_MAX_COMMUNITY_VLANS + 1);

    /* Get the VLAN type and the associated VLAN */

    L2PvlanMappingInfo.u4ContextId = AST_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.pMappedVlans = gPvrstVlanIdList;
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* Secondary VLAN do not have PVRST instance 
     */

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN ||
        L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN)
    {
        /* Map the secondary VLAN to the primary VLAN PVRST instance
         */
        if (L2PvlanMappingInfo.pMappedVlans[0] != 0)
        {
            u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                        L2PvlanMappingInfo.
                                                        pMappedVlans[0]);

            if (AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                            VlanId,
                                            u2InstIndex) == L2IWF_FAILURE)
            {
                return PVRST_FAILURE;
            }

            /*Loop for all member ports for this secondary vlan
             * - Get the port state of this port in primary vlan and
             *   program the same in hardware.
             */
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
                if (pAstPortEntry == NULL)
                {
                    continue;
                }

                if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                             VlanId,
                                             AST_GET_IFINDEX (u2PortNum)) ==
                    OSIX_FALSE)
                {
                    continue;
                }
#ifdef NPAPI_WANTED
                if (AST_IS_PVRST_ENABLED () &&
                    (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                {
                    PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                             AST_IFENTRY_IFINDEX
                                             (pAstPortEntry), VlanId,
                                             AstGetInstPortStateFromL2Iwf
                                             (u2InstIndex, u2PortNum));
                }
#endif /*NPAPI_WANTED */

            }                    /* End of for */
        }

        return PVRST_FAILURE;
    }

    if (PVRST_GET_NO_OF_ACTIVE_INSTANCES == AST_MAX_PVRST_INSTANCES)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      " Maximum Spanning Tree Instances already active - %d \n",
                      AST_MAX_PVRST_INSTANCES);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      " Maximum Spanning Tree Instances already active - %d \n",
                      AST_MAX_PVRST_INSTANCES);
        return PVRST_SUCCESS;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        if (PvrstGetFreeInstIndex (&u2InstIndex) == PVRST_FAILURE)
        {
            return PVRST_FAILURE;
        }
        if (u2InstIndex == INVALID_INDEX)
        {
            return PVRST_FAILURE;
        }
        PVRST_VLAN_TO_INDEX_MAP (u2InstIndex) = VlanId;

        /* Updating Vlan to Instance Index mapping in L2Iwf common database.
         * Instance Index of a Vlan will be fetched from L2Iwf further so that
         * searching of InstIndex for a Vlan in pInstIndexMap can be reduced.*/

        AstL2IwfCreateSpanningTreeInstance (AST_CURR_CONTEXT_ID (),
                                            u2InstIndex);
        AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId,
                                    u2InstIndex);

        PVRST_GET_NO_OF_ACTIVE_INSTANCES++;

#ifdef NPAPI_WANTED
        if (AST_IS_PVRST_ENABLED () &&
            (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
        {
            i4HwRetVal =
                PvrstFsMiPvrstNpCreateVlanSpanningTree (AST_CURR_CONTEXT_ID (),
                                                        VlanId);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              " Creating instance %d in the hardware failed! \n",
                              VlanId);
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                              " Creating instance %d in the hardware failed! \n",
                              VlanId);
                AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId,
                                            AST_INIT_VAL);
                AstL2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (),
                                                    u2InstIndex);
                PVRST_VLAN_TO_INDEX_MAP (u2InstIndex) = AST_INIT_VAL;

                PVRST_GET_NO_OF_ACTIVE_INSTANCES--;
                return PVRST_FAILURE;
            }
        }
#endif /* NPAPI_WANTED */

        if (PvrstInitInstance (VlanId) != PVRST_SUCCESS)
        {
            PvrstDeleteInstance (VlanId);
            return PVRST_FAILURE;
        }

        PVRST_INCR_INSTANCE_UP_COUNT (u2InstIndex);
        AstPvrstInstUpTrap (VlanId, (INT1 *) AST_PVRST_TRAPS_OID,
                            PVRST_BRG_TRAPS_OID_LEN);
    }

    pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
    if (pPerStInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
        if (pAstPortEntry == NULL)
        {
            continue;
        }
        /* If Port Instance mapping already exists 
           continue to next port 
         */
        if (pPerStInfo->ppPerStPortInfo[u2PortNum - 1] != NULL)
        {
            continue;
        }

        u4IfIndex = pAstPortEntry->u4IfIndex;

        AstL2IwfGetVlanPortType ((UINT2) u4IfIndex, &u1PortType);
        if (u1PortType == VLAN_HYBRID_PORT)
        {
            if (AST_DEF_VLAN_ID () != VlanId)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &PVID);
                /* If port is a tagged port add it to spanning-tree instance */
                i4RetVal = VlanCheckPortType (VlanId, u2PortNum);
                if ((i4RetVal == PVRST_FALSE) && (PVID != VlanId))
                {
                    continue;
                }
                else if ((i4RetVal == PVRST_FALSE) &&
                         (!AST_IS_PVRST_STARTED ()))
                {
                    /*During PVRST initialisation, if PVID is already configured
                     *for the port, make it as untagged port for that VLAN and
                     *remove from the default vlan */
                    if (PVID != AST_DEF_VLAN_ID ())
                    {
                        VlanConfigDot1qPvidForPvrstStart (u2PortNum, PVID);
                    }

                }
            }
            else
            {
                AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &PVID);
                if (PVID != VlanId)
                {
                    continue;
                }

            }
        }
        if (u1PortType == VLAN_ACCESS_PORT)
        {
            AstL2IwfGetVlanPortPvid ((UINT2) u4IfIndex, &PVID);
            if (PVID != VlanId)
            {
                continue;
            }
        }
        /* Trunk port will be present in all vlans. Hence we can
         * blindly add that to any instance.
         * But vlan membership for host and promiscuous ports are configured
         * by the user and those ports will be members of only the configured
         * vlans.
         * Hence for host and promiscuous ports, before adding to the instance
         * check whether they belong to this vlan.
         */

        if ((u1PortType == VLAN_HOST_PORT) ||
            (u1PortType == VLAN_PROMISCOUS_PORT))
        {
            if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                         VlanId, AST_GET_IFINDEX (u2PortNum))
                == OSIX_FALSE)
            {
                continue;
            }
        }

        if (PvrstPerStPortCreate (u2PortNum, VlanId) != PVRST_SUCCESS)
        {
            PvrstDeleteInstance (VlanId);
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Unable to create port = %d"
                          "for instance = %d !!!\n", u2PortNum, VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Unable to create port = "
                          "%d for instance = %d !!!\n", u2PortNum, VlanId);
            return PVRST_FAILURE;
        }

        if ((PvrstPathcostConfiguredFlag (u2PortNum, u2InstIndex) ==
             PVRST_FALSE) &&
            (pAstPortEntry->bPathCostInitialized == PVRST_TRUE))
        {
            pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (u2PortNum, u2InstIndex);

            /* Instance is getting newly created on this port
             * and the port is already oper up.
             * Copy the saved path cost for the port */
            pPerStPvrstRstPortInfo->u4PathCost = pAstPortEntry->u4PathCost;
        }

        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port = %d Created for vlan = %d \n",
                      u2PortNum, VlanId);
    }
    if (AST_MODULE_STATUS == PVRST_ENABLED)
    {
        PvrstEnableInstance (VlanId);
    }

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_PRIMARY_VLAN)
    {
        /* map secondary VLAN with the primary VLAN PVRST instance
         */
        for (u2VlanCount = 0; u2VlanCount < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanCount++)
        {
            AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                        L2PvlanMappingInfo.
                                        pMappedVlans[u2VlanCount], u2InstIndex);
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstEnableInstance                        */
/*                                                                           */
/*    Description               : It enables instance for the given Vlan     */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstEnableInstance (tVlanId VlanId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1OperStatus;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_BEGIN, VlanId) !=
        PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Role Selection Machine returned error"
                      "for instance %d !!!\n", VlanId);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Role Selection Failure for Instance = %d"
                      "!!!\n", VlanId);

        return PVRST_FAILURE;
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                   &u1OperStatus);
        if (u1OperStatus == (UINT1) AST_PORT_OPER_UP)
        {

            if (PvrstPerStEnablePort (u2PortNum, VlanId,
                                      AST_STP_PORT_UP) != PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "Module Enable: Unable to enable port = "
                              "%d for instance = %d !!!\n", u2PortNum, VlanId);
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Unable to enable port = "
                              "%d for instance = %d !!!\n", u2PortNum, VlanId);

                return PVRST_FAILURE;
            }
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port = %d Created for vlan = %d \n",
                          u2PortNum, VlanId);
        }
        else
        {
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         AST_IFENTRY_IFINDEX
                                         (pPortInfo), VlanId,
                                         AST_PORT_STATE_DISCARDING);
            }
#endif
            AstSetInstPortStateToL2Iwf (u2InstIndex,
                                        u2PortNum, AST_PORT_STATE_DISCARDING);
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstDeleteInstance                        */
/*                                                                           */
/*    Description               : It deletes instance for the given for the  */
/*                                given Vlan Id                              */
/*                                                                           */
/*    Input(s)                  : VlanId- Vlan Instance Identifier           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstDeleteInstance (tVlanId VlanId)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    INT4                i4RetVal = PVRST_SUCCESS;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;

#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;

    UNUSED_PARAM (i4HwRetVal);
#endif /* NPAPI_WANTED */

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (gPvrstVlanIdList, 0, VLAN_MAX_COMMUNITY_VLANS + 1);

    /* Get the VLAN type and the associated VLAN */

    L2PvlanMappingInfo.u4ContextId = AST_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.pMappedVlans = gPvrstVlanIdList;
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* Secondary VLAN do not have PVRST instance
     */

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN ||
        L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN)
    {
        AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId,
                                    AST_INIT_VAL);
        return PVRST_SUCCESS;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        i4RetVal = PVRST_SUCCESS;
        return i4RetVal;
    }

    pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);

    if (pPerStInfo == NULL)
    {
        i4RetVal = PVRST_SUCCESS;
        return i4RetVal;
    }
    if (pPerStInfo->pFlushTgrTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStInfo, u2InstIndex,
             AST_TMR_TYPE_FLUSHTGR) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for FlushTimer FAILED!\n");
            i4RetVal = PVRST_FAILURE;
            return i4RetVal;
        }
    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        if (PvrstPerStPortDelete (u2PortNum, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "DeletePort: Port %s Inst %d: PvrstPerStPortDelete"
                          "returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
            i4RetVal = PVRST_FAILURE;
        }
    }

    AstPvrstInstDownTrap (VlanId, (INT1 *) AST_PVRST_TRAPS_OID,
                          PVRST_BRG_TRAPS_OID_LEN);

    AST_RELEASE_PVRST_PERST_BRG_INFO_MEM_BLOCK (pPerStInfo->PerStBridgeInfo.
                                                pPerStPvrstBridgeInfo);
    pPerStInfo->PerStBridgeInfo.pPerStPvrstBridgeInfo = NULL;

    PvrstDeleteMstInstance (u2InstIndex, pPerStInfo);

    AstL2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2InstIndex);
    /*Updating Instance to Vlan mapping in L2Iwf common database */
    AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId, AST_INIT_VAL);

    PVRST_VLAN_TO_INDEX_MAP (u2InstIndex) = AST_INIT_VAL;

    PVRST_GET_NO_OF_ACTIVE_INSTANCES--;

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        i4HwRetVal =
            PvrstFsMiPvrstNpDeleteVlanSpanningTree (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
    }
#endif /* NPAPI_WANTED */

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstDeleteInstance is not proper...Failures Occurred !!!\n"
                 " Still Continuing ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstDeleteInstance is not proper...Failures Occurred !!!\n"
                 " Still Continuing ...\n");
    }
    else
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Pvrst Instance is deleted.\n");
        AST_DBG (AST_INIT_SHUT_DBG, "SYS: Pvrst Instance is deleted.\n");
    }
    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstInitInstance                          */
/*                                                                           */
/*    Description               : Allocates the memory block for given       */
/*                                instance and initialise the instance       */
/*                                specific bridge information.               */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstInitInstance (tVlanId VlanId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstMacAddr         brgAddr;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }

    AST_MEMSET (&brgAddr, AST_INIT_VAL, sizeof (tAstMacAddr));

    if (PvrstCreateSpanningTreeInst (u2InstIndex, &pPerStInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC |
                      AST_OS_RESOURCE_TRC,
                      "SYS: Creation of Spanning tree PVRST Instance %d"
                      "failed !!!\n", VlanId);
        AST_DBG_ARG1 (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG |
                      AST_INIT_SHUT_DBG,
                      "SYS: Creation of Spanning Tree PVRST Instance %d"
                      "failed !!!\n", VlanId);
        return PVRST_FAILURE;
    }

    pPerStInfo->u2InstanceId = VlanId;
    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);

    if (PvrstInitPerStBrgInfo (pPerStBrgInfo, VlanId) != PVRST_SUCCESS)
    {
        PvrstDeleteMstInstance (u2InstIndex, pPerStInfo);
        return PVRST_FAILURE;
    }
    AST_DBG_ARG1 (AST_MGMT_DBG | AST_INIT_SHUT_DBG,
                  "SYS: Instance %d created\n", VlanId);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Instance %d created\n", VlanId);
    KW_FALSEPOSITIVE_FIX (pPerStInfo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstInitPerStBrgInfo                      */
/*                                                                           */
/*    Description               : This function initialises the Instance     */
/*                                specific Bridge Information.               */
/*                                                                           */
/*    Input(s)                  : pPerStBrgInfo - Pointer to information     */
/*                                               that needs to be initialised*/
/*                                VlanId - Vlan Instance Identifier          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstInitPerStBrgInfo (tAstPerStBridgeInfo * pPerStBrgInfo, tVlanId VlanId)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_ALLOC_PVRST_PERST_BRG_INFO_MEM_BLOCK
        (pPerStBrgInfo->pPerStPvrstBridgeInfo) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: InstanceId %u: PerStPvrstBridgeInfo Memory"
                      "Block Allocation failed !!!\n", VlanId);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: InstanceId %u: PerStPvrstBridgeInfo Memory"
                      "Block Allocation failed !!!\n", VlanId);
        return PVRST_FAILURE;
    }

    AST_MEMCPY (&(pPerStBrgInfo->RootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    /* The Bridge Priority for CVLAN component is different from
       the SVLAN component. This macro will take care of the Giving the
       Bridge Priority depending of the context's component type */

    if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
    {
        pPerStBrgInfo->u2BrgPriority = AST_PB_CVLAN_BRG_PRIORITY;
    }
    else
    {
        pPerStBrgInfo->u2BrgPriority = PVRST_DEFAULT_BRG_PRIORITY;
    }

    pPerStBrgInfo->u2BrgPriority = pPerStBrgInfo->u2BrgPriority | VlanId;

    pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

    pPerStBrgInfo->u1ProleSelSmState = PVRST_PROLESELSM_STATE_INIT_BRIDGE;
    pPerStBrgInfo->u2RootPort = AST_NO_VAL;
    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    pPerStBrgInfo->u4RootCost = AST_INIT_VAL;
    AST_MEMSET (pPerStBrgInfo->au2TcDetPorts, AST_INIT_VAL,
                sizeof (pPerStBrgInfo->au2TcDetPorts));
    AST_MEMSET (pPerStBrgInfo->au2TcRecvPorts, AST_INIT_VAL,
                sizeof (pPerStBrgInfo->au2TcDetPorts));
    pPerStBrgInfo->u1DetHead = AST_INIT_VAL;
    pPerStBrgInfo->u1RcvdHead = AST_INIT_VAL;
    pPerStBrgInfo->u4FlushIndThreshold = PVRST_DEFAULT_FLUSH_IND_THRESHOLD;

    MEMSET ((AST_CURR_CONTEXT_INFO ()->pInstanceUpCount), AST_INIT_VAL,
            (AST_MAX_PVRST_INSTANCES * sizeof (UINT4)));

    /* Initializing PerStPvrstBridgeInfo */
    pPerStBrgInfo->pPerStPvrstBridgeInfo->
        BridgeTimes.u2MaxAge = PVRST_DEFAULT_BRG_MAX_AGE;
    pPerStBrgInfo->pPerStPvrstBridgeInfo->
        BridgeTimes.u2ForwardDelay = PVRST_DEFAULT_BRG_FWD_DELAY;
    pPerStBrgInfo->pPerStPvrstBridgeInfo->
        BridgeTimes.u2HelloTime = PVRST_DEFAULT_BRG_HELLO_TIME;
    pPerStBrgInfo->pPerStPvrstBridgeInfo->
        BridgeTimes.u2MsgAgeOrHopCount = AST_INIT_VAL;

    RstGetBridgeAddr (&(pPerStBrgInfo->pPerStPvrstBridgeInfo->
                        bridgeId.BridgeAddr));

    pPerStBrgInfo->pPerStPvrstBridgeInfo->bridgeId.u2BrgPriority =
        PVRST_DEFAULT_BRG_PRIORITY;
    pPerStBrgInfo->pPerStPvrstBridgeInfo->bridgeId.u2BrgPriority |= VlanId;

    pPerStBrgInfo->pPerStPvrstBridgeInfo->RootTimes = pPerStBrgInfo->
        pPerStPvrstBridgeInfo->BridgeTimes;

    pPerStBrgInfo->pPerStPvrstBridgeInfo->u1TxHoldCount =
        (UINT1) PVRST_DEFAULT_BRG_TX_LIMIT;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstInitStateMachines                     */
/*                                                                           */
/*    Description               : This routine initializes the PVRST State   */
/*                                machines with the proper State Event and   */
/*                                action rooutine combinations               */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstInitStateMachines (VOID)
{

    MEMSET (gaaau1AstSemState, 0, sizeof (gaaau1AstSemState));
    MEMSET (gaaau1AstSemEvent, 0, sizeof (gaaau1AstSemEvent));
    /* Port Info Machine */
    PvrstInitPortInfoMachine ();

    /* Port Role Selection Machine */
    PvrstInitPortRoleSelectionMachine ();

    /* Port Role Transition Machine */
    PvrstInitPortRoleTrMachine ();

    /* Port State Transition Machine */
    PvrstInitPortStateTrMachine ();

    /* Port Migration Machine */
    PvrstInitProtocolMigrationMachine ();

    /* Topology Change Machine */
    PvrstInitTopoChStateMachine ();

    /* Port Transmit Machine */
    PvrstInitPortTxStateMachine ();

    /* Bridge Detection Machine */
    PvrstInitBrgDetectionStateMachine ();

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstCreatePort                            */
/*                                                                           */
/*    Description               : It Allocates memory for the port.          */
/*                                                                           */
/*                                                                           */
/*    Input(s)                  : u4IfIndex - Global IfIndex of the port     */
/*                                u2PortNum - Local per context Port Number  */
/*                                            of the port that is to be      */
/*                                            created.                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstCfaIfInfo       CfaIfInfo;
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               u1IfType = CFA_ENET;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    AST_MEMSET (au1IfName, AST_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Creating port %u ... \n", u4IfIndex);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Creating port %u ... \n", u4IfIndex);

    AstL2IwfAllocInstPortStateMem (AST_CURR_CONTEXT_ID (), u2PortNum,
                                   PVRST_START);

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Already created \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return PVRST_FAILURE;
    }

    /* Check whether the interface type is valid. Currently 
     * ethernet or PPP or ATMVC or pseudo wire are valid interfaces.
     */
    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfType for Port %u\n", u4IfIndex);
        return PVRST_FAILURE;
    }

    u1IfType = CfaIfInfo.u1IfType;

    if ((AST_INTF_TYPE_VALID (u1IfType)) == AST_FALSE)
    {
        return PVRST_FAILURE;
    }

    if (AST_ALLOC_PORT_INFO_MEM_BLOCK (pPortInfo) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 " Port Info Memory Block Allocation failed!!!\n");
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Info Memory Block"
                      "Allocation failed !!!\n", u4IfIndex);
        return PVRST_FAILURE;
    }

    AST_MEMSET (pPortInfo, AST_INIT_VAL, sizeof (tAstPortEntry));
    AST_GET_PORTENTRY (u2PortNum) = pPortInfo;

    pPortInfo->u4ContextId = AST_CURR_CONTEXT_ID ();
    pPortInfo->u4IfIndex = u4IfIndex;
    pPortInfo->u4PhyPortIndex = u4IfIndex;
    CfaCliConfGetIfName (u4IfIndex, piIfName);
    MEMCPY (pPortInfo->au1PortMACAddr, CfaIfInfo.au1MacAddr, AST_MAC_ADDR_SIZE);
#if (defined PVRST_DEBUG) || (defined TRACE_WANTED)
    MEMSET (pPortInfo->au1StrIfIndex, 0, CFA_MAX_PORT_NAME_LENGTH);
    SNPRINTF ((CHR1 *) pPortInfo->au1StrIfIndex, CFA_MAX_PORT_NAME_LENGTH, "%s",
              piIfName);
#endif

    AstAddToIfIndexTable (pPortInfo);

    PvrstInitGlobalPortInfo (u2PortNum, pPortInfo);
    /* Path cost will be calculated from speed when the link is up.
     * Until then mark the path cost as uninitialised */
    pPortInfo->bPathCostInitialized = PVRST_FALSE;

    /* For protocol operations (comparisons etc.,) too, the same
     * port number will be used. */
    pPortInfo->u2ProtocolPort = u2PortNum;

    /* Depending on the Bridge Mode, the Port type should be assigned. */
    AstUpdatePortType (pPortInfo, CfaIfInfo.u1BrgPortType);
    AST_PB_PORT_INFO (pPortInfo) = NULL;

    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_ENABLED);

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: CreatePort: Completed creating port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Completed creating port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstDeletePort                            */
/*                                                                           */
/*    Description               : Disables the port if port Operational      */
/*                                status is UP. Then it releases the memory  */
/*                                allocated for the port. Stops all the      */
/*                                timer which are running for this port.     */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the port that   */
/*                                            is to be deleted.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstDeletePort (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT4               u4IfIndex;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Deleting Port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleting Port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    if (AST_IS_PORT_VALID (u2PortNum) == AST_FALSE)
    {
        /* Port number exceeds the allowed range */
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Delete Port for invalid port number !!!\n");
        return PVRST_FAILURE;
    }

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: DeletePort: Port does not exist !!!\n");
        return PVRST_FAILURE;
    }

    /* If the Delete Port indication comes for a CEP, delete the CVLAN component
     * and its all PEPs and also delete the port from the SVLAN context*/
    /* The Deletion of Port in CVLAN component should be called from SVLAN
     * context*/
    if (AstPbCVlanCompShutdown (pPortInfo) != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    /*PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PORTDISABLED, u2PortNum); */

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
        if (pPerStInfo == NULL)
        {
            continue;
        }
        VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);
        if (PvrstPerStPortDelete (u2PortNum, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "DeletePort: Port %s Inst %d: PvrstPerStPortDelete"
                          "returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
            return PVRST_FAILURE;
        }
    }
    /* If deleted port is the last member port of Vlan, delete that instance */
    if (AstIsPvrstInstExist () == AST_FALSE)
    {
        if (pPerStInfo != NULL)
        {
            if (pPerStInfo->u2PortCount == AST_INIT_VAL)
            {
                if (PvrstDeleteInstance (VlanId) == PVRST_FAILURE)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                             "SYS: Instance not Deleted for Vlan.\n");
                    AST_DBG (AST_INIT_SHUT_DBG, "SYS: instance not"
                             "deleted for Vlan.\n");
                    return PVRST_FAILURE;
                }

            }
        }
    }
    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

    u4IfIndex = AST_IFENTRY_IFINDEX (pPortInfo);

    /* Remove this node from the IfIndex RBTree and the PerContext DLL */
    PvrstReleasePortMemBlocks (pPortInfo);

    AstL2IwfAllocInstPortStateMem (AST_CURR_CONTEXT_ID (), u2PortNum,
                                   PVRST_SHUTDOWN);

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_DISABLED);

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Deleted Port %u\n", u4IfIndex);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleted Port %u\n", u4IfIndex);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitGlobalPortInfo                              */
/*                                                                           */
/* Description        : Initialises the Global Port Information              */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the information is */
/*                                  to be initialised.                       */
/*                      pPortInfo - Port Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstInitGlobalPortInfo (UINT2 u2PortNum, tAstPortEntry * pPortInfo)
{
    pPortInfo->u2PortNo = u2PortNum;
    pPortInfo->bAdminEdgePort = PVRST_FALSE;

    AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_FALSE);

    if ((AST_BRIDGE_MODE () != AST_PROVIDER_EDGE_BRIDGE_MODE) ||
        (AST_COMP_TYPE () != AST_PB_C_VLAN))
    {
        pPortInfo->u1AdminPointToPoint = (UINT1) RST_P2P_AUTO;
        /* The value of Oper Point-To-Point will be updated after triggering 
         * LA and getting the status */
        AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);
    }

    pPortInfo->bOperEdgePort = PVRST_FALSE;
    pPortInfo->CommPortInfo.bPortPvrstStatus = PVRST_TRUE;
    pPortInfo->CommPortInfo.eEncapType = PVRST_DOT1Q;
    pPortInfo->CommPortInfo.u4BpduGuard = AST_BPDUGUARD_NONE;
    pPortInfo->CommPortInfo.bRootGuard = PVRST_FALSE;
    pPortInfo->CommPortInfo.bRootInconsistent = PVRST_FALSE;
    pPortInfo->CommPortInfo.bBpduInconsistent = PVRST_FALSE;
    pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
    AST_PORT_RESTRICTED_ROLE (pPortInfo) = (tAstBoolean) PVRST_FALSE;
    AST_PORT_RESTRICTED_TCN (pPortInfo) = (tAstBoolean) PVRST_FALSE;
    AST_PORT_LOOP_GUARD (pPortInfo) = (tAstBoolean) PVRST_FALSE;
    pPortInfo->bLoopInconsistent = (tAstBoolean) PVRST_FALSE;
    pPortInfo->bAutoEdge = PVRST_DEFAULT_AUTOEDGE_VALUE;
    pPortInfo->bPVIDInconsistent = (tAstBoolean) PVRST_FALSE;
    pPortInfo->bPTypeInconsistent = (tAstBoolean) PVRST_FALSE;
    pPortInfo->bPVIDIncExists = AST_FALSE;
    pPortInfo->bPTypeIncExists = AST_FALSE;
    pPortInfo->u1EnableBPDUFilter = AST_BPDUFILTER_DISABLE;
    AST_PORT_ENABLE_BPDU_RX (pPortInfo) = PVRST_TRUE;
    AST_PORT_ENABLE_BPDU_TX (pPortInfo) = PVRST_TRUE;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstInitPerStPortinfo                     */
/*                                                                           */
/*    Description               : This function initialises the per spanning */
/*                                tree port information.                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Port Structure that has   */
/*                                to be initialised                          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstInitPerStPortinfo (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1TunnelStatus = (UINT1) AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2PortNum = pPerStPortInfo->u2PortNo;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return;
    }

    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    /*Initializing PerStPortInfo variables */
    pPerStPortInfo->RootId.u2BrgPriority = PVRST_DEFAULT_BRG_PRIORITY;
    pPerStPortInfo->RootId.u2BrgPriority =
        pPerStPortInfo->RootId.u2BrgPriority | VlanId;
    pPerStPortInfo->DesgBrgId.u2BrgPriority = PVRST_DEFAULT_BRG_PRIORITY;
    pPerStPortInfo->DesgBrgId.u2BrgPriority =
        pPerStPortInfo->DesgBrgId.u2BrgPriority | VlanId;
    pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
    pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;
    pPerStPortInfo->i4NpFailRetryCount = AST_INIT_VAL;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;
    AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
        AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum);
    pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
    pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4RootCost = AST_INIT_VAL;

    u2Val = (UINT2) PVRST_DEFAULT_PORT_PRIORITY;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    pPerStPortInfo->u2DesgPortId = AST_GET_PROTOCOL_PORT (u2PortNum) | u2Val;

    pPerStPortInfo->u2PortNo = u2PortNum;
    pPerStPortInfo->u1PortPriority = PVRST_DEFAULT_PORT_PRIORITY;
    pPerStPortInfo->u1PortRole = AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u1SelectedPortRole = AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u1PinfoSmState = PVRST_PINFOSM_STATE_DISABLED;
    pPerStPortInfo->u1ProleTrSmState = PVRST_PROLETRSM_STATE_INIT_PORT;
    pPerStPortInfo->u1PstateTrSmState = PVRST_PSTATETRSM_STATE_DISCARDING;
    pPerStPortInfo->u1TopoChSmState = PVRST_TOPOCHSM_STATE_INIT;
    pPerStPortInfo->u4PortAdminPathCost = 0;
    pPerStPortInfo->bLoopGuardStatus = PVRST_FALSE;
    pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
    pPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
    pPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
    AST_GET_SYS_TIME ((tAstSysTime
                       *) (&(pPerStPortInfo->u4TcDetectedTimestamp)));
    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStPortInfo->u4TcRcvdTimestamp)));

    /*Initializing PerStRstPortInfo variables */
    pPerStRstPortInfo->bAgree = PVRST_FALSE;
    pPerStRstPortInfo->bAgreed = PVRST_FALSE;
    pPerStRstPortInfo->bForward = PVRST_FALSE;
    pPerStRstPortInfo->bForwarding = PVRST_FALSE;
    pPerStRstPortInfo->bLearn = PVRST_FALSE;
    pPerStRstPortInfo->bLearning = PVRST_FALSE;
    pPerStRstPortInfo->bProposed = PVRST_FALSE;
    pPerStRstPortInfo->bProposing = PVRST_FALSE;
    pPerStRstPortInfo->bRcvdTc = PVRST_FALSE;
    pPerStRstPortInfo->bReRoot = PVRST_FALSE;
    pPerStRstPortInfo->bReSelect = PVRST_FALSE;
    pPerStRstPortInfo->bSelected = PVRST_FALSE;
    pPerStRstPortInfo->bSync = PVRST_FALSE;
    pPerStRstPortInfo->bSynced = PVRST_FALSE;
    pPerStRstPortInfo->bTc = PVRST_FALSE;
    pPerStRstPortInfo->bTcProp = PVRST_FALSE;
    pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;
    pPerStRstPortInfo->u1InfoIs = PVRST_INFOIS_DISABLED;
    pPerStRstPortInfo->u1RcvdInfo = PVRST_OTHER_MSG;
    pPerStRstPortInfo->bRcvdMsg = PVRST_FALSE;
    pPerStRstPortInfo->bPortEnabled = PVRST_TRUE;
    pPerStRstPortInfo->u1RcvdExpTimerCount = AST_INIT_VAL;
#ifdef IEEE_8021Y_Z12
    pPerStRstPortInfo->bDisputed = PVRST_FALSE;
#endif

    /*Initializing PerStPvrstRstPortInfo variables */
    pPerStPvrstRstPortInfo->DesgPvrstTimes = pPerStPvrstBridgeInfo->RootTimes;
    pPerStPvrstRstPortInfo->PortPvrstTimes =
        pPerStPvrstRstPortInfo->DesgPvrstTimes;
    pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bInitPm = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bMCheck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bSendRstp = PVRST_TRUE;
    pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;
    /* Fix For SilverCrek test: Get on Port that is down will
     * return value as zero that is out of range */
    pPerStPvrstRstPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
    pPerStPvrstRstPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_INIT;
    pPerStPvrstRstPortInfo->u1PmigSmState = (UINT1) PVRST_PMIGSM_STATE_INIT;
    pPerStPvrstRstPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;
    pPerStPvrstRstPortInfo->u1MigrationType = AST_TRAP_SEND_RSTP;

    /* In case of PEB/PCB and Customer Bridge, STP tunnel status should
     * not be enabled on the port for enabling STP on that port */
    if ((AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE) ||
        (AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE))
    {
        AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX (u2PortNum),
                                               L2_PROTO_STP, &u1TunnelStatus);

        if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            pPerStRstPortInfo->bPortEnabled = PVRST_FALSE;
        }
    }

    /* In case of q-in-q bridge mode, Tunnel Status should not be enabled
     * on the port for enabling STP on that port */
    if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
    {
        AstL2IwfGetPortVlanTunnelStatus (AST_GET_IFINDEX (u2PortNum),
                                         (BOOL1 *) & u1TunnelStatus);

        if (u1TunnelStatus == OSIX_TRUE)
        {
            pPerStRstPortInfo->bPortEnabled = PVRST_FALSE;
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : PvrstReInitPvrstInfo                                 */
/*                                                                           */
/* Description        : Initialises the Pvrst Info on Module Disable         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstReInitPvrstInfo ()
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        /* Initialization of global port information */
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
        if (pPortInfo == NULL)
        {
            continue;
        }
    }

    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
        if (u2InstIndex == INVALID_INDEX)
        {
            return;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            return;
        }

        pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

        /* Initialization of instance specific spanning tree information */
        AST_MEMCPY (&(pPerStBrgInfo->RootId.BridgeAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStBrgInfo->u1ProleSelSmState =
            (UINT1) PVRST_PROLESELSM_STATE_INIT_BRIDGE;
        pPerStBrgInfo->u2RootPort = AST_NO_VAL;
        pPerStBrgInfo->u4RootCost = AST_INIT_VAL;
        pPerStBrgInfo->u4TimeSinceTopoCh = AST_INIT_VAL;

        pPerStPvrstBrgInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
        pPerStPvrstBrgInfo->RootTimes = pPerStPvrstBrgInfo->BridgeTimes;

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            /* Initialization Instance specific port information */
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
            pPerStPortInfo->u1PinfoSmState =
                (UINT1) PVRST_PINFOSM_STATE_DISABLED;
            pPerStPortInfo->u1ProleTrSmState =
                (UINT1) PVRST_PROLETRSM_STATE_INIT_PORT;
            pPerStPortInfo->u1PstateTrSmState =
                (UINT1) PVRST_PSTATETRSM_STATE_DISCARDING;
            pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_INIT;
            pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
            pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
            pPerStPortInfo->u4RootCost = AST_INIT_VAL;
#ifdef IEEE8021Y_Z12_WANTED
            pPerStPortInfo->bDisputed = PVRST_FALSE;
#endif /* IEEE8021Y_Z12_WANTED */

            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bAgreed = PVRST_FALSE;
            pRstPortInfo->bForward = PVRST_FALSE;
            pRstPortInfo->bForwarding = PVRST_FALSE;
            pRstPortInfo->bLearn = PVRST_FALSE;
            pRstPortInfo->bLearning = PVRST_FALSE;
            pRstPortInfo->bProposed = PVRST_FALSE;
            pRstPortInfo->bProposing = PVRST_FALSE;
            pRstPortInfo->bRcvdMsg = RST_FALSE;
            pRstPortInfo->bRcvdTc = PVRST_FALSE;
            pRstPortInfo->bReRoot = PVRST_FALSE;
            pRstPortInfo->bReSelect = PVRST_FALSE;
            pRstPortInfo->bSync = PVRST_FALSE;
            pRstPortInfo->bSynced = PVRST_FALSE;
            pRstPortInfo->bTc = PVRST_FALSE;
            pRstPortInfo->bTcProp = PVRST_FALSE;
            pRstPortInfo->bUpdtInfo = PVRST_FALSE;
            pRstPortInfo->bSelected = PVRST_FALSE;
            pRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_DISABLED;
            pRstPortInfo->u1RcvdInfo = (UINT1) PVRST_OTHER_MSG;
            pRstPortInfo->bDisputed = PVRST_FALSE;

            pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (u2PortNum, u2InstIndex);
            pPerStPvrstRstPortInfo->DesgPvrstTimes =
                pPerStPvrstBrgInfo->RootTimes;
            pPerStPvrstRstPortInfo->PortPvrstTimes =
                pPerStPvrstRstPortInfo->DesgPvrstTimes;
            pPerStPvrstRstPortInfo->bInitPm = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bMCheck = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_FALSE;
            pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;
            pPerStPvrstRstPortInfo->u1PortTxSmState =
                (UINT1) PVRST_PTXSM_STATE_TRANSMIT_INIT;
            pPerStPvrstRstPortInfo->u1PmigSmState =
                (UINT1) PVRST_PMIGSM_STATE_INIT;
            pPerStPvrstRstPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;

        }
    }
}

/*****************************************************************************/
/* Function Name      : PvrstReInitPvrstInstInfo                             */
/*                                                                           */
/* Description        : Initialises the Pvrst Instance Info on Disabling on  */
/*                      a port                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstReInitPvrstInstInfo (UINT2 u2PortNum, tVlanId VlanId)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBrgInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return;
    }
    pPerStPvrstBrgInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return;
    }

    /* Initialization Instance specific port information */
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        return;
    }
    pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_DISABLED;
    pPerStPortInfo->u1ProleTrSmState = (UINT1) PVRST_PROLETRSM_STATE_INIT_PORT;
    pPerStPortInfo->u1PstateTrSmState =
        (UINT1) PVRST_PSTATETRSM_STATE_DISCARDING;
    pPerStPortInfo->u1TopoChSmState = (UINT1) PVRST_TOPOCHSM_STATE_INIT;
    pPerStPortInfo->u1SelectedPortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u4RootCost = AST_INIT_VAL;
#ifdef IEEE8021Y_Z12_WANTED
    pPerStPortInfo->bDisputed = PVRST_FALSE;
#endif /* IEEE8021Y_Z12_WANTED */

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pRstPortInfo->bAgreed = PVRST_FALSE;
    pRstPortInfo->bForward = PVRST_FALSE;
    pRstPortInfo->bForwarding = PVRST_FALSE;
    pRstPortInfo->bLearn = PVRST_FALSE;
    pRstPortInfo->bLearning = PVRST_FALSE;
    pRstPortInfo->bProposed = PVRST_FALSE;
    pRstPortInfo->bProposing = PVRST_FALSE;
    pRstPortInfo->bRcvdMsg = RST_FALSE;
    pRstPortInfo->bRcvdTc = PVRST_FALSE;
    pRstPortInfo->bReRoot = PVRST_FALSE;
    pRstPortInfo->bReSelect = PVRST_FALSE;
    pRstPortInfo->bSync = PVRST_FALSE;
    pRstPortInfo->bSynced = PVRST_FALSE;
    pRstPortInfo->bTc = PVRST_FALSE;
    pRstPortInfo->bTcProp = PVRST_FALSE;
    pRstPortInfo->bUpdtInfo = PVRST_FALSE;
    pRstPortInfo->bSelected = PVRST_FALSE;
    pRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_DISABLED;
    pRstPortInfo->u1RcvdInfo = (UINT1) PVRST_OTHER_MSG;
    pRstPortInfo->bDisputed = PVRST_FALSE;

    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (u2PortNum, u2InstIndex);
    pPerStPvrstRstPortInfo->DesgPvrstTimes = pPerStPvrstBrgInfo->RootTimes;
    pPerStPvrstRstPortInfo->PortPvrstTimes =
        pPerStPvrstRstPortInfo->DesgPvrstTimes;
    pPerStPvrstRstPortInfo->bInitPm = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bMCheck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_INIT;
    pPerStPvrstRstPortInfo->u1PmigSmState = (UINT1) PVRST_PMIGSM_STATE_INIT;
    pPerStPvrstRstPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstStartSemsForPort                      */
/*                                                                           */
/*    Description               : This function initialises all the State    */
/*                                Machines relatd to the port.               */
/*                                                                           */
/*    Input(s)                 :  pPerStPortInfo - Port Structure that has to*/
/*                                be initialised                             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstStartSemsForPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPvrstRstPortInfo = NULL;
    UINT2               u2PortNum = pPerStPortInfo->u2PortNo;
    UINT2               VlanId = pPerStPortInfo->u2Inst;

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Starting SEMs for port %u for %d Vlan...\n",
                  u2PortNum, VlanId);
    AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Starting SEMs for port %u for %d vlan...\n",
                  u2PortNum, VlanId);

    pPvrstRstPortInfo = pPerStPortInfo->PerStRstPortInfo.pPerStPvrstRstPortInfo;
    if (pPvrstRstPortInfo == NULL)
    {
        AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                      "StartSemsForPort: Port %u: Pvrst RST port info"
                      "Entry Not Created for instance %d\n", u2PortNum, VlanId);
        return PVRST_FAILURE;
    }
    /*Restart Bridge Detection state machine on this port */
    PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BEGIN, u2PortNum);

    if (PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_BEGIN,
                                   u2PortNum, VlanId) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Protocol Migration"
                      "Machine Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Protocol Migration Machine"
                      "Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstPortStateTrMachine ((UINT2) PVRST_PSTATETRSM_EV_BEGIN,
                                 pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port State Transition"
                      "Machine Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port State Transition"
                      "Machine Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstPortRoleTrMachine ((UINT2)
                                PVRST_PROLETRSM_EV_BEGIN, pPerStPortInfo)
        != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Role Transition"
                      "Machine Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RTSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Role Transition"
                      "Machine Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_BEGIN,
                              pPerStPortInfo, (tPvrstBpdu *) AST_INIT_VAL)
        != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Info Machine"
                      "Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Info Machine"
                      "Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstTopoChMachine ((UINT2) PVRST_TOPOCHSM_EV_BEGIN, pPerStPortInfo)
        != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Topo change Machine "
                      "Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Topo Change Machine "
                      "Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstPortTransmitMachine ((UINT2) PVRST_PTXSM_EV_BEGIN, pPerStPortInfo)
        != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Transmit Machine"
                      " Returned failure  !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Transmit Machine"
                      "Returned failure  !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Port %u: StartSemsForPort: Completed"
                  " starting SEMs \n", u2PortNum);
    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %u: StartSemsForPort: Completed"
                  "starting SEMs \n", u2PortNum);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstPerStPortCreate                       */
/*                                                                           */
/*    Description               : This function maps the given port to the   */
/*                                given instance.                            */
/*                                                                           */
/*    Input(s)                 :  u2PortNum  - Port Number  to be created    */
/*                                VlanId- Vlan Instance ID to which          */
/*                                this Port belongs.                         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstPerStPortCreate (UINT2 u2PortNum, tVlanId VlanId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

#ifdef NPAPI_WANTED
    UINT1               u1OperStatus = AST_PORT_OPER_DOWN;
#endif

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }
    pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);

    if (pPerStInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPerStInfo->ppPerStPortInfo[u2PortNum - 1] != NULL)
    {
        return PVRST_SUCCESS;
    }

    if (AST_ALLOC_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Per SpTree Port Info Memory"
                      " Block Allocation failed !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Per SpTree Port Info Memory"
                      "Block Allocation failed !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    AST_MEMSET (pPerStPortInfo, AST_INIT_VAL, sizeof (tAstPerStPortInfo));

    pPerStInfo->ppPerStPortInfo[u2PortNum - 1] = pPerStPortInfo;

    pPerStPortInfo->u2Inst = VlanId;
    pPerStPortInfo->u2PortNo = u2PortNum;

    pPerStInfo->u2PortCount = (UINT2) ((pPerStInfo->u2PortCount) + 1);
    pPerStPortInfo->bDisableInProgress = AST_TRUE;
    if (AST_ALLOC_PVRST_PERST_RST_PORT_INFO_MEM_BLOCK (pPerStPortInfo->
                                                       PerStRstPortInfo.
                                                       pPerStPvrstRstPortInfo)
        == NULL)
    {
        AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
        AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) = NULL;
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: PerStPvrstRstPortInfo Memory"
                      "Block Allocation failed !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: PerStPvrstRstPortInfo Memory"
                      "Block Allocation failed !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }
    AST_MEMSET (pPerStPortInfo->PerStRstPortInfo.pPerStPvrstRstPortInfo,
                AST_INIT_VAL, sizeof (tPerStPvrstRstPortInfo));
#ifdef NPAPI_WANTED
    if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        if (AstL2IwfGetPortOperStatus (STP_MODULE, AST_GET_IFINDEX (u2PortNum),
                                       &u1OperStatus) == L2IWF_SUCCESS)
        {
            if (u1OperStatus == AST_PORT_OPER_DOWN)
            {
                AstSetInstPortStateToL2Iwf (u2InstIndex,
                                            u2PortNum,
                                            AST_PORT_STATE_DISCARDING);
                /*update hardware with the control plane port state */
                PvrstFsMiPvrstNpSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                  AST_GET_IFINDEX (u2PortNum),
                                                  VlanId,
                                                  AST_PORT_STATE_DISCARDING);
            }
        }
    }
#endif

    PvrstInitPerStPortinfo (pPerStPortInfo);

    PVRST_SET_CHANGED_FLAG (u2InstIndex, u2PortNum) = PVRST_FALSE;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstPerStEnablePort                       */
/*                                                                           */
/*    Description               : Enables the state machine for the port     */
/*                                with 'Port Enabled' event.                 */
/*                                                                           */
/*    Input(s)                  : u2PortNum    - Port Number of the port     */
/*                                               which is to be enabled.     */
/*                                VlanId - Vlan Instance Id.                 */
/*                                                                           */
/*                                u1TrigType - Indicates whether the Port    */
/*                                              is enabled at Bridge/CFA     */
/*                                              Level or at STP Level        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstPerStEnablePort (UINT2 u2PortNum, tVlanId VlanId, UINT1 u1TrigType)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    tVlanId             LocalVlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2TmpPvrstInst = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
        if (u2InstIndex == INVALID_INDEX)
        {
            return PVRST_FAILURE;
        }
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
        if (pPerStInfo == NULL)
        {
            AST_DBG (AST_INIT_SHUT_DBG, "SYS: Instance does not exist.\n");
            AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                     "SYS: Instance does not exist.\n");
            return PVRST_FAILURE;
        }
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (pPerStPortInfo == NULL)
        {
            AST_DBG (AST_INIT_SHUT_DBG,
                     "SYS: Port is not a member of Instance.\n");
            AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                     "SYS: Port is not a member of Instance.\n");
            return PVRST_SUCCESS;
        }

        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Enabling Port %u ...\n", u2PortNum);
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                      "SYS: Enabling Port %u ...\n", u2PortNum);
        pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

        AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);

        if (u1PortType == VLAN_ACCESS_PORT)
        {
            AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
            if (PVID != VlanId)
            {
                return PVRST_FAILURE;
            }
        }

        pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return PVRST_SUCCESS;
        }
        if (AST_GET_BRG_PORT_TYPE (pPortInfo) != AST_PROVIDER_EDGE_PORT)
        {
            /* Update spanning tree status for this port in L2IWF. */
            AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                    u2PortNum, L2_PROTO_STP,
                                                    OSIX_ENABLED);
        }

        PvrstReInitPvrstInstInfo (u2PortNum, VlanId);

        if (PvrstStartSemsForPort (pPerStPortInfo) == PVRST_FAILURE)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "EnablePort: Port %u: StartSems"
                          "returned failure !\n", u2PortNum);
            return PVRST_FAILURE;
        }
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        if (pPortInfo->u1EntryStatus == AST_PORT_OPER_UP)
        {
            return PVRST_SUCCESS;
        }

        AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);

        pPortInfo->bPVIDInconsistent = PVRST_FALSE;
        pPortInfo->bPTypeInconsistent = PVRST_FALSE;
        pPortInfo->bPTypeIncExists = PVRST_FALSE;
        pPortInfo->bPVIDIncExists = PVRST_FALSE;
        if (pCommPortInfo != NULL)
        {
            pCommPortInfo->bBpduInconsistent = AST_FALSE;
        }

        for (u2TmpPvrstInst = 1; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
             u2TmpPvrstInst++)
        {
            LocalVlanId = PVRST_VLAN_TO_INDEX_MAP (u2TmpPvrstInst);

            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != LocalVlanId)
                {
                    continue;
                }
            }
            pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2PortNum, u2TmpPvrstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            /* The Port path cost is updated from the link speed   
             * when a link up trigger is received 
             * if NO pathcost has been configured on this port
             * and if Dynamic pathcost calculation is enabled for the port
             *     or if the port is coming up for the first time after
             *     creation
             */
            if ((PvrstPathcostConfiguredFlag (u2PortNum, u2TmpPvrstInst) ==
                 PVRST_FALSE))
            {
                pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                    (u2PortNum, u2TmpPvrstInst);
                pPerStPvrstRstPortInfo->u4PathCost = pPortInfo->u4PathCost;
            }
        }
    }

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port Up event should be processed.
         * If Cfa Port Up event comes, directly set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_UP)
        {
            for (LocalVlanId = 1; LocalVlanId <= VLAN_MAX_VLAN_ID;
                 LocalVlanId++)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), LocalVlanId)
                    == OSIX_FALSE)
                {
                    continue;
                }
                u2InstIndex =
                    AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                  LocalVlanId);
                if (u2InstIndex == INVALID_INDEX)
                {
                    return PVRST_FAILURE;
                }

                pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                            AST_PORT_STATE_FORWARDING);

                if (AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
                {
                    continue;
                }
                AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                    AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_PVRST_ENABLED () &&
                    (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                {
                    PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                             AST_IFENTRY_IFINDEX
                                             (pPortInfo), LocalVlanId,
                                             AST_PORT_STATE_FORWARDING);
                }
#endif /*NPAPI_WANTED */
            }
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module disabled. Returning...\n");
        return PVRST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
        {
            AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                        AST_PORT_STATE_FORWARDING);

            AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         AST_IFENTRY_IFINDEX
                                         (pPortInfo), VlanId,
                                         AST_PORT_STATE_FORWARDING);
            }
#endif /*NPAPI_WANTED */

            return PVRST_SUCCESS;
        }

        if (AstGetInstPortStateFromL2Iwf (u2InstIndex, u2PortNum)
            != AST_PORT_STATE_DISCARDING)
        {
            /* 
             * Spanning tree is now being enabled on this port. 
             * Put the port in DISCARDING state (previously it could
             * have been in FORWARDING since spanning tree was disabled 
             * on this port) 
             */
            AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         AST_IFENTRY_IFINDEX (pPortInfo),
                                         VlanId, AST_PORT_STATE_DISCARDING);
            }
#endif /*NPAPI_WANTED */
        }
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;

        if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
        {
            for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
                 u2InstIndex++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
                if (pPerStInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo =
                    PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                if ((VlanId =
                     PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
                {
                    continue;
                }

                if ((VlanId =
                     PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
                {
                    continue;
                }
                AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                            AST_PORT_STATE_FORWARDING);
                AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                    AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_PVRST_ENABLED () &&
                    (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                {
                    PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                             AST_IFENTRY_IFINDEX
                                             (pPortInfo), VlanId,
                                             AST_PORT_STATE_FORWARDING);
                }
#endif /*NPAPI_WANTED */
            }
            return PVRST_SUCCESS;
        }

        for (u2TmpPvrstInst = 1; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
             u2TmpPvrstInst++)
        {
            LocalVlanId = PVRST_VLAN_TO_INDEX_MAP (u2TmpPvrstInst);

            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != LocalVlanId)
                {
                    continue;
                }
            }
            pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2PortNum, u2TmpPvrstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            PvrstReInitPvrstInstInfo (u2PortNum, LocalVlanId);

            if (PvrstStartSemsForPort (pPerStPortInfo) == PVRST_FAILURE)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "EnablePort: Port %u: StartSems"
                              "returned failure !\n", u2PortNum);
                return PVRST_FAILURE;
            }
            pPerStPortInfo->u2Inst = LocalVlanId;
            pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
            if (pRstPortInfo->bPortEnabled != PVRST_TRUE)
            {
                if (AstGetInstPortStateFromL2Iwf (u2TmpPvrstInst, u2PortNum)
                    != AST_PORT_STATE_FORWARDING)
                {
                    /* 
                     * The port is now being enabled at the bridge level. 
                     * But the port is disabled in spanning tree.
                     * Put the port in FORWARDING state (previously it could have 
                     * been in DISCARDING since the port was disabled at the bridge
                     * level) 
                     */
                    AstSetInstPortStateToL2Iwf (u2TmpPvrstInst, u2PortNum,
                                                AST_PORT_STATE_FORWARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2TmpPvrstInst, u2PortNum) =
                        AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_PVRST_ENABLED () &&
                        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                    {
                        PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                                 AST_IFENTRY_IFINDEX
                                                 (pPortInfo), LocalVlanId,
                                                 AST_PORT_STATE_FORWARDING);
                    }
#endif /*NPAPI_WANTED */
                }
                continue;
            }

            if (PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_PORT_ENABLED,
                                           u2PortNum, LocalVlanId)
                != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %u: Port Protocol Migration"
                              "Machine Returned failure !!!\n", u2PortNum);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %u: Port Protocol Migration"
                              "Machine Returned failure !!!\n", u2PortNum);
                return PVRST_FAILURE;
            }

            if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_PORT_ENABLED,
                                      pPerStPortInfo, NULL) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %u: Port Info Machine"
                              "Returned failure !!!\n", u2PortNum);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %u: Port Info Machine"
                              "Returned failure !!!\n", u2PortNum);
                return PVRST_FAILURE;
            }
        }
        /*Restart Bridge Detection state machine on this port */
        PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BEGIN, u2PortNum);
        return PVRST_SUCCESS;
    }

    /*Restart Bridge Detection state machine on this port */
    PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BEGIN, u2PortNum);

    AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);

    if (PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_PORT_ENABLED,
                                   u2PortNum, VlanId) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Protocol Migration"
                      "Machine Returned failure !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Protocol Migration"
                      "Machine Returned failure !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_PORT_ENABLED,
                              pPerStPortInfo, NULL) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port %u: Port Info Machine Returned"
                      "failure !!!\n", u2PortNum);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Info Machine Returned"
                      "failure !!!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Port %u: Enabled Successfully\n", u2PortNum);
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Port %u: Enabled Successfully\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstPerStDisablePort                      */
/*                                                                           */
/*    Description               : Disables the state machine for the port    */
/*                                with 'Port Disabled' event.                */
/*                                                                           */
/*    Input(s)                  : u2PortNum    - Port Number of the port     */
/*                                               which is to be disabled.    */
/*                                VlanId - Vlan Instance Id.                 */
/*                                u1TrigType - Indicates whether the Port    */
/*                                              is disabled at Bridge/CFA    */
/*                                              Level or at STP Level        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstPerStDisablePort (UINT2 u2PortNum, tVlanId VlanId, UINT1 u1TrigType)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStInfo      *pTmpPerStInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             LocalVlanId = RST_DEFAULT_INSTANCE;
    INT4                i4RetValue = PVRST_SUCCESS;
    UINT2               u2TmpPvrstInst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Disabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), VlanId);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Disabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    }
    else
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Port %s disabled in CFA. Disabling"
                      "Port in Pvrst...\n", AST_GET_IFINDEX_STR (u2PortNum));
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Port %s disabled in CFA. Disabling"
                      "Port in Pvrst...\n", AST_GET_IFINDEX_STR (u2PortNum));
    }

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
        if (u2InstIndex == INVALID_INDEX)
        {
            return PVRST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                          AST_INIT_SHUT_DBG,
                          "SYS: Instance %d does not exist" "!!!\n", VlanId);
            return PVRST_FAILURE;
        }
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
        if (pPortInfo == NULL)
        {
            return PVRST_SUCCESS;
        }
        pPortInfo->bLoopInconsistent = AST_FALSE;

        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (NULL == pPerStPortInfo)
        {
            return PVRST_SUCCESS;
        }
        pPerStPortInfo->bLoopIncStatus = AST_FALSE;
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        /* Update spanning tree status for this port in L2IWF. */
        AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);

        if ((AST_GBL_BPDUGUARD_STATUS != AST_TRUE) &&
            ((pAstCommPortInfo->u4BpduGuard != AST_BPDUGUARD_ENABLE) &&
             (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE)))

        {
            if (pPortInfo->u1EntryStatus != AST_PORT_OPER_UP)
            {
                return PVRST_SUCCESS;
            }
        }
    }

    if (!(AST_IS_PVRST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port 
         * Down event should be processed.
         * If Cfa Port Down event comes, directly 
         * set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_DOWN)
        {
            for (LocalVlanId = 1; LocalVlanId <= VLAN_MAX_VLAN_ID;
                 LocalVlanId++)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), LocalVlanId)
                    == OSIX_FALSE)
                {
                    continue;
                }
                u2InstIndex =
                    AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                  LocalVlanId);
                if (u2InstIndex == INVALID_INDEX)
                {
                    return PVRST_FAILURE;
                }
                pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                AstSetInstPortStateToL2Iwf (u2InstIndex, u2PortNum,
                                            AST_PORT_STATE_FORWARDING);
                if (AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
                {
                    continue;
                }
                AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u2PortNum) =
                    AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_PVRST_ENABLED () &&
                    (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
                {
                    PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                             AST_IFENTRY_IFINDEX
                                             (pPortInfo), LocalVlanId,
                                             AST_PORT_STATE_FORWARDING);
                }
#endif /*NPAPI_WANTED */
            }

            if (VlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ())
                == VLAN_TRUE)
            {
                if (NULL != pPortInfo)
                {
                    VlanDeleteFdbEntries
                        ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo),
                         VLAN_NO_OPTIMIZE);
                }
            }
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module Already disabled. Returning...\n");
        return PVRST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_DOWN)
    {
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
        /*When STP Receives port oper down indication
           set bpdu inconsistent state to false.
           When BPDU is received on the port in which bpdu guard is enable,
           the caller of this function will set the flag */

        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

        if (pAstCommPortInfo == NULL)
        {
            return PVRST_FAILURE;
        }
        if ((pAstCommPortInfo->bBpduInconsistent != RST_TRUE) ||
            (pAstCommPortInfo->u1BpduGuardAction != AST_PORT_ADMIN_DOWN))
        {

            pAstCommPortInfo->bBpduInconsistent = AST_FALSE;
        }

        if (pPortInfo->u1EntryStatus == AST_PORT_OPER_DOWN)
        {
            return PVRST_SUCCESS;
        }
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

        if (VlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
        {
            VlanDeleteFdbEntries ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo),
                                  VLAN_NO_OPTIMIZE);
        }

        /* STP Port Status is disabled from CFA hence
           the port should should be disabled for each instance it belongs */
        for (u2TmpPvrstInst = 1; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
             u2TmpPvrstInst++)
        {
            pTmpPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
            if (pTmpPerStInfo == NULL)
            {
                continue;
            }
            VlanId = PVRST_VLAN_TO_INDEX_MAP (u2TmpPvrstInst);

            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum,
                                                        u2TmpPvrstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo->bLoopIncStatus = AST_FALSE;
            pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

            AstSetInstPortStateToL2Iwf (u2TmpPvrstInst, u2PortNum,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (u2TmpPvrstInst, u2PortNum) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         pPortInfo->u4IfIndex, VlanId,
                                         AST_PORT_STATE_DISCARDING);
            }
#endif

            /* The value of Oper P2P is updated while disabling the port if 
             * Admin P2P is Auto, by triggering LA, because if the aggregatable
             * status of any port other than the lowest numbered port changes 
             * from Individual to Aggregatable, then only a Down indication is 
             * given and an Up indication is not given for that port. */
            AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);

            if (PvrstPortMigrationMachine
                ((UINT2) PVRST_PMIGSM_EV_PORT_DISABLED, u2PortNum,
                 VlanId) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %u: Port Protocol Migration"
                              "Machine Returned failure !!!\n", u2PortNum);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %u: Port Protocol Migration"
                              "Machine Returned failure !!!\n", u2PortNum);
                return PVRST_FAILURE;
            }

            if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_PORT_DISABLED,
                                      pPerStPortInfo,
                                      (tPvrstBpdu *) AST_INIT_VAL) !=
                PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %u: Port Info Machine"
                              "Returned failure !!!\n", u2PortNum);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %u: Port Info Machine"
                              "Returned failure !!!\n", u2PortNum);
                return PVRST_FAILURE;
            }
            if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_RESELECT,
                                               VlanId) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "SYS: Port %u: Role Selection Machine "
                              "returned FAILURE !!!\n", u2PortNum);
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Port %u: Role Selection Machine"
                              " returned FAILURE !!!\n", u2PortNum);
                return PVRST_FAILURE;
            }
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2TmpPvrstInst);

            if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
            {
                if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                    u2TmpPvrstInst, AST_TMR_TYPE_HELLOWHEN)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                             AST_INIT_SHUT_DBG,
                             "TMR: PvrstStopTimer for"
                             "HelloPvrstWhenTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_ROOT_INC_RECOVERY) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for ROOT INC RECOVERY FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr != NULL)
            {
                if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                    u2TmpPvrstInst, AST_TMR_TYPE_MDELAYWHILE)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                             AST_INIT_SHUT_DBG,
                             "TMR: PvrstStopTimer for"
                             "MdWhilePvrstTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pPerStPvrstRstPortInfo->pHoldTmr != NULL)
            {
                if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                    u2TmpPvrstInst,
                                    AST_TMR_TYPE_HOLD) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                             AST_INIT_SHUT_DBG,
                             "TMR: PvrstStopTimer for HoldPvrstTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pRstPortInfo->pFdWhileTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_FDWHILE) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for FdWhileTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pRstPortInfo->pRbWhileTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_RBWHILE) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for RbWhileTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pRstPortInfo->pRcvdInfoTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_RCVDINFOWHILE) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for RcvdInfoTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }
            if (pRstPortInfo->pRrWhileTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for RrWhileTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }
            if (pRstPortInfo->pTcWhileTmr != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_TCWHILE) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for TcWhileTmr FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pAstCommPortInfo->pEdgeDelayWhileTmr != NULL)
            {
                if (AstStopTimer ((VOID *) pPortInfo,
                                  AST_TMR_TYPE_EDGEDELAYWHILE) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: AstStopTimer for Edge Delay While Timer FAILED!\n");
                    return RST_FAILURE;
                }
            }
            if (pAstCommPortInfo->pDisableRecoveryTmr != NULL)
            {
                if (PvrstStopTimer ((VOID *) pPortInfo, u2TmpPvrstInst,
                                    AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) !=
                    RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ERROR DISABLE RECOVERY Timer!\n");
                    return PVRST_FAILURE;
                }
            }

            if (pAstCommPortInfo->pIncRecoveryTmr != NULL)
            {
                if (AstStopTimer ((VOID *) pPortInfo,
                                  AST_TMR_TYPE_INC_RECOVERY) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: AstStopTimer for Edge Delay While Timer FAILED!\n");
                    return RST_FAILURE;
                }
            }
            if (AST_CURR_CONTEXT_INFO ()->pRestartTimer != NULL)
            {
                if (PvrstStopTimer
                    ((VOID *) pPortInfo, u2TmpPvrstInst,
                     AST_TMR_TYPE_RESTART) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: PvrstStopTimer for Restart Timer FAILED!\n");
                    return PVRST_FAILURE;
                }
            }

        }
        /*Disable Bridge Detection Machine for this port */
        PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PORTDISABLED, u2PortNum);
        return PVRST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);

        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

        if (PvrstPortMigrationMachine
            (PVRST_PMIGSM_EV_PORT_DISABLED, u2PortNum, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Port Migration Machine"
                          "Returned failure for PORT_DISABLED"
                          "event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s Inst %d: Port Migration Machine"
                          "Returned failure for PORT_DISABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
        }

        if (PvrstPortInfoMachine (PVRST_PINFOSM_EV_PORT_DISABLED,
                                  pPerStPortInfo,
                                  (tPvrstBpdu *) AST_INIT_VAL) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Port Info Machine"
                          "Returned failure for PORT_DISABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s Inst %d: Port Info Machine"
                          "Returned failure for PORT_DISABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), VlanId);
        }

        if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_RESELECT,
                                           VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "SYS: Port %u: Role Selection Machine "
                          "returned FAILURE !!!\n", u2PortNum);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: Port %u: Role Selection Machine"
                          " returned FAILURE !!!\n", u2PortNum);
            return PVRST_FAILURE;
        }
        /* BpduInconsitent state is reset when spanning tree is
         * disabled on a port */
        if ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE) ||
            ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE) &&
             (AST_GBL_BPDUGUARD_STATUS == PVRST_TRUE)))

        {
            pAstCommPortInfo->bBpduInconsistent = PVRST_FALSE;
        }

        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
        {
            if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                u2InstIndex, AST_TMR_TYPE_HELLOWHEN)
                != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                         AST_INIT_SHUT_DBG,
                         "TMR: PvrstStopTimer for"
                         "HelloPvrstWhenTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr != NULL)
        {
            if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                u2InstIndex, AST_TMR_TYPE_MDELAYWHILE)
                != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                         "TMR: PvrstStopTimer for" "MdWhilePvrstTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pPerStPvrstRstPortInfo->pHoldTmr != NULL)
        {
            if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                                u2InstIndex,
                                AST_TMR_TYPE_HOLD) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                         "TMR: PvrstStopTimer for HoldPvrstTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
        {

            if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                                (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY) !=
                PVRST_SUCCESS)
            {
                AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: PvrstStopTimer for Root Inconsistent Tmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pRstPortInfo->pFdWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 AST_TMR_TYPE_FDWHILE) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: PvrstStopTimer for FdWhileTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pRstPortInfo->pRbWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 AST_TMR_TYPE_RBWHILE) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: PvrstStopTimer for RbWhileTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }

        if (pRstPortInfo->pRcvdInfoTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 AST_TMR_TYPE_RBWHILE) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: PvrstStopTimer for RcvdInfoTmr FAILED!\n");
                return PVRST_FAILURE;
            }
        }
        if (pAstCommPortInfo->pDisableRecoveryTmr != NULL)
        {
            if (AstStopTimer ((VOID *) pPortInfo,
                              AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) !=
                RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for disable recovery  Timer FAILED!\n");
                return RST_FAILURE;
            }
        }
        for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
             u2InstIndex++)
        {
            if ((LocalVlanId =
                 PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
            {
                continue;
            }
            if (LocalVlanId == VlanId)
            {
                continue;
            }
            if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
            {
                continue;
            }
            if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
            {
                continue;
            }
            pPerStRstPortInfo =
                PVRST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStRstPortInfo == NULL)
            {
                continue;
            }
            if (pPerStRstPortInfo->bPortEnabled == PVRST_TRUE)
            {
                /*EdgeDelayWhileTmr is not stopped when the port is a member of some other vlan */
                i4RetValue = PVRST_FAILURE;
                break;
            }
        }
        if (i4RetValue == PVRST_SUCCESS)
        {

            if (pAstCommPortInfo->pEdgeDelayWhileTmr != NULL)
            {
                if (AstStopTimer ((VOID *) pPortInfo,
                                  AST_TMR_TYPE_EDGEDELAYWHILE) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: AstStopTimer for Edge Delay While Timer FAILED!\n");
                    return RST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s successfully disabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s successfully disabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstPerStPortDelete                       */
/*                                                                           */
/*    Description               : This function maps the given port to the   */
/*                                given instance.                            */
/*                                                                           */
/*    Input(s)                 :  u2PortNum  - Port Number  to be created    */
/*                                VlanId- Vlan Instance ID to which          */
/*                                this Port belongs.                         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstPerStPortDelete (UINT2 u2PortNum, tVlanId VlanId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG, "SYS: Deleting Port %s for Instance %d.\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleting Port %s for Instance %d.\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    if (AST_IS_PORT_VALID (u2PortNum) == AST_FALSE)
    {
        /* Port number exceeds the allowed range */
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Delete Port for invalid port number !!!\n");
        return PVRST_FAILURE;
    }

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstPerStPortDelete: Port does not exist !!!\n");
        return PVRST_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
    if (pPerStInfo == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstPerStPortDelete: Port does not exist for "
                 "this instance!!!\n");
        return PVRST_SUCCESS;
    }
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstPerStPortDelete: Port does not exist for "
                 "this instance!!!\n");
        return PVRST_SUCCESS;
    }
    if (pPerStPortInfo->bDisableInProgress == AST_FALSE)
    {
        pPerStPortInfo->bDisableInProgress = AST_TRUE;
    }

    if (PvrstPerStDisablePort (u2PortNum, VlanId,
                               AST_STP_PORT_DOWN) != PVRST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "SYS: PvrstPerStPortDelete: Port %s Inst %d: "
                      "PvrstPerStDisablePort returned failure !\n",
                      AST_GET_IFINDEX_STR (u2PortNum), VlanId);
        return PVRST_FAILURE;
    }

    AST_RELEASE_PVRST_PERST_RST_PORT_INFO_MEM_BLOCK (pPerStPortInfo->
                                                     PerStRstPortInfo.
                                                     pPerStPvrstRstPortInfo);
    pPerStPortInfo->PerStRstPortInfo.pPerStPvrstRstPortInfo = NULL;
    pPerStPortInfo->bDisableInProgress = AST_FALSE;
    AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
    AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) = NULL;
    pPerStInfo->u2PortCount = (UINT2) (pPerStInfo->u2PortCount - 1);

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG, "SYS: Deleted Port %s for instance %d\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleted Port %s for instance %d\n",
                  AST_GET_IFINDEX_STR (u2PortNum), VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstHandleInBpdu                          */
/*                                                                           */
/*    Description               : This function Handles the received BPDU    */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu - Pointer to the received BPDU   */
/*                                            message structure.             */
/*                                u2PortNum - Port Number of the Port on     */
/*                                            which BPDU is received.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstHandleInBpdu (tPvrstBpdu * pRcvdBpdu, UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;
    UINT1               u1PortType = AST_INIT_VAL;
    tNotifyProtoToApp   NotifyProtoToApp;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pRcvdBpdu->VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (NULL == pPerStPvrstRstPortInfo)
    {
        return PVRST_FAILURE;
    }

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    if ((pRcvdBpdu->VlanId != AST_DEF_VLAN_ID ()) &&
        ((AST_FORCE_VERSION == AST_VERSION_0)
         || (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE)))
    {
        return PVRST_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();
    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE)
    {
        if ((AST_GBL_BPDUGUARD_STATUS == AST_TRUE)
            && (pPortEntry->bAdminEdgePort == AST_TRUE))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);

            if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_DOWN;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u2PortNum);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                pAstCommPortInfo->bBpduInconsistent = PVRST_TRUE;
            }

            else
            {
                /*Disable Spanning-tree on the port */
                if (PvrstPerStDisablePort
                    (u2PortNum, pRcvdBpdu->VlanId,
                     AST_EXT_PORT_DOWN) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "SYS: PvrstPerStDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    AST_DBG_ARG1 (AST_BPDU_DBG,
                                  "SYS: PvrstPerStDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    return PVRST_FAILURE;
                }
                /* BpduInconsitent state is set when port transtions
                 * Happens due to Bpdu Guard Feature enabled on that
                 *  port*/

                pAstCommPortInfo->bBpduInconsistent = PVRST_TRUE;

                u2Duration = AST_DEFAULT_ERROR_RECOVERY;

                if (PvrstStartTimer ((VOID *) pPortEntry, u2InstIndex,
                                     (UINT1)
                                     AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                                     u2Duration) != MST_SUCCESS)
                {
                    AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "SYS: PvrstStartTimer for disable recovery duration FAILED!\n");
                    return PVRST_FAILURE;
                }
                return PVRST_SUCCESS;
            }
        }
    }
    else if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        AST_DBG_ARG1 (AST_BPDU_DBG,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
        {
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_DOWN;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u2PortNum);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                pAstCommPortInfo->bBpduInconsistent = PVRST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent occur at : %s\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           u2InstIndex,
                                           (INT1 *) AST_PVRST_TRAPS_OID,
                                           PVRST_BRG_TRAPS_OID_LEN);
            }
        }

        else
        {
            if (PvrstPerStDisablePort
                (u2PortNum, pRcvdBpdu->VlanId,
                 AST_EXT_PORT_DOWN) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                              "SYS: PvrstPerStDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                AST_DBG_ARG1 (AST_BPDU_DBG,
                              "SYS: PvrstPerStDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                return PVRST_FAILURE;
            }
            /* BpduInconsitent state is set when port transtions
             * Happens due to Bpdu Guard Feature enabled on that
             *  port*/
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                pAstCommPortInfo->bBpduInconsistent = PVRST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent occur at : %s\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           u2InstIndex,
                                           (INT1 *) AST_PVRST_TRAPS_OID,
                                           PVRST_BRG_TRAPS_OID_LEN);
            }
            u2Duration = AST_DEFAULT_ERROR_RECOVERY;

            if (PvrstStartTimer ((VOID *) pPortEntry, u2InstIndex,
                                 (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                                 u2Duration) != MST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "SYS: PvrstStartTimer for disable recovery duration FAILED!\n");
                return PVRST_FAILURE;
            }

            return PVRST_SUCCESS;
        }
    }

    if (pAstCommPortInfo->bRootGuard == PVRST_TRUE)
    {
        if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
            (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE))
        {
            return PVRST_SUCCESS;
        }
    }
    PvrstPortRcvdFlagsStatsUpdt (pRcvdBpdu, u2PortNum, u2InstIndex);
    /*Resetting Loop-guard on reception of BPDU */
    if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
        && (pPortEntry->bOperPointToPoint == PVRST_TRUE)
        && (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE))
    {
        UtlGetTimeStr (au1TimeStr);
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     (tVlanId) pPerStPortInfo->u2Inst,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan  :%d at %s\n",
                              AST_GET_IFINDEX_STR (u2PortNum),
                              pPerStPortInfo->u2Inst, au1TimeStr);
        }
        pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;

        AstL2IwfGetVlanPortType ((UINT2) pPortEntry->u4IfIndex, &u1PortType);

        if (u1PortType == VLAN_ACCESS_PORT)
        {
            pPortEntry->bLoopInconsistent = PVRST_FALSE;
        }
    }
    /* ForceVersion check mentioned in 802.1w as part of definition for 
     * rcvdBPDU is removed here as per 802.1D Draft4 recommendation so 
     * that all BPDUs are accepted regardless of the bridge version */

    pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_TRUE;

    /*Call the BDSM with RCVD bpdu event */
    PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BPDU_RCVD, u2PortNum);

    u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    /*Start the EdgeDelayWhileTimer */
    if (PvrstStartTimer ((VOID *) pPortEntry, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                         (UINT2) u2Duration) != PVRST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: PvrstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: PvrstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        return PVRST_FAILURE;
    }

    if (PvrstPortInfoMachine (PVRST_PINFOSM_EV_RCVD_BPDU, pPerStPortInfo,
                              pRcvdBpdu) != PVRST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: PvrstPortInfoMachine function returned FAILURE!\n");
        AST_DBG (AST_BPDU_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: PvrstPortInfoMachine function returned FAILURE!\n");
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

VOID
PvrstPortRcvdFlagsStatsUpdt (tPvrstBpdu * pRcvBpdu, UINT2 u2PortNum,
                             UINT2 u2InstanceId)
{
    UINT1               u1TmpFlag = (UINT1) AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4Ticks = 0;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo != NULL)
    {
        if (AST_GET_PERST_INFO (u2InstanceId) != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

            if (pPerStPortInfo != NULL)
            {
                /* Reception of TC,Proposal and agreements packet  */
                u1TmpFlag = PVRST_SET_PROPOSAL_FLAG;

                if ((pRcvBpdu->u1Flags & u1TmpFlag) == PVRST_SET_PROPOSAL_FLAG)
                {
                    PVRST_INCR_RX_PROPOSAL_COUNT (u2PortNum, u2InstanceId);
                    OsixGetSysTime (&u4Ticks);
                    pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;
                }

                u1TmpFlag = PVRST_SET_AGREEMENT_FLAG;
                if ((pRcvBpdu->u1Flags & u1TmpFlag) == PVRST_SET_AGREEMENT_FLAG)
                {
                    PVRST_INCR_RX_AGREEMENT_COUNT (u2PortNum, u2InstanceId);
                    OsixGetSysTime (&u4Ticks);
                    pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;
                }
                u1TmpFlag = PVRST_SET_TOPOCH_FLAG;
                if ((pRcvBpdu->u1Flags & u1TmpFlag) == PVRST_SET_TOPOCH_FLAG)
                {
                    PVRST_INCR_TC_RX_COUNT (u2PortNum, u2InstanceId);
                    OsixGetSysTime (&u4Ticks);
                    pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
                }
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstAddTrunkPort                          */
/*                                                                           */
/*    Description               : This function makes the given Port Trunk   */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                            which is to be made Trunk      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstAddTrunkPort (UINT4 u4PortNo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT1               u1OperStatus;
    tAstPerStInfo      *pPerStInfo = NULL;
    BOOL1               b1RoleSelect = PVRST_TRUE;
    UINT2               u2InstId = AST_INIT_VAL;

    pPortInfo = AST_GET_PORTENTRY ((UINT2) u4PortNo);
    if (pPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPortInfo->bPTypeInconsistent == AST_TRUE)
    {
        pPortInfo->bPTypeInconsistent = AST_FALSE;
        if (PvrstStopTimer ((VOID *) pPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                            (UINT1) AST_TMR_TYPE_INC_RECOVERY) == PVRST_FAILURE)
        {
            return PVRST_FAILURE;
        }
       /**Recovering from Type inconsistancy : Port Un blocked**/
        PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId, AST_EXT_PORT_UP);
    }
    AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex, &u1OperStatus);

    if ((pPortInfo->bPathCostInitialized == PVRST_FALSE) &&
        (u1OperStatus == AST_PORT_OPER_UP))
    {
        pPortInfo->u4PathCost = AstCalculatePathcost ((UINT2) u4PortNo);
        pPortInfo->bPathCostInitialized = PVRST_TRUE;
    }

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        b1RoleSelect = PVRST_TRUE;
        if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        u2InstId =
            AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstId);

        if (pPerStInfo == NULL)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port not created: port = %d for"
                          "instance = %d !!!\n", u4PortNo, VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port not created: port = %d for"
                          "instance = %d !!!\n", u4PortNo, VlanId);

            return PVRST_FAILURE;
        }
        /* Static entry exists for port and vlan. Reconvergence should not happen */
        if (pPerStInfo->ppPerStPortInfo[u4PortNo - 1] != NULL)
        {
            b1RoleSelect = PVRST_FALSE;
        }
        if (PvrstPerStPortCreate ((UINT2) u4PortNo, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Unable to create port = %d for"
                          "instance = %d !!!\n", u4PortNo, VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS:  Unable to create port = %d for"
                          "instance = %d !!!\n", u4PortNo, VlanId);
            return PVRST_FAILURE;
        }

        if ((PvrstPathcostConfiguredFlag ((UINT2) u4PortNo, u2InstIndex) ==
             PVRST_FALSE) && (pPortInfo->bPathCostInitialized == PVRST_TRUE))
        {
            pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (u4PortNo, u2InstIndex);
            /* Instance is getting newly created on this port
             * and the port is already oper up.
             * Calculate the path cost for the port */
            pPerStPvrstRstPortInfo->u4PathCost = pPortInfo->u4PathCost;

        }

        if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
        {
            AstSetInstPortStateToL2Iwf (u2InstIndex, (UINT2) u4PortNo,
                                        AST_PORT_STATE_FORWARDING);

            AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u4PortNo) =
                AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         pPortInfo->u4IfIndex, VlanId,
                                         AST_PORT_STATE_FORWARDING);
            }
#endif /* NPAPI_WANTED */
            continue;
        }

        if ((u1OperStatus == (UINT1) AST_PORT_OPER_UP) &&
            (AST_MODULE_STATUS == PVRST_ENABLED))
        {
            if (PVRST_TRUE == b1RoleSelect)
            {
                PvrstReInitPvrstInstInfo ((UINT2) u4PortNo, VlanId);

                if (PvrstPortRoleSelectionMachine
                    (PVRST_PROLESELSM_EV_BEGIN, VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "SYS: Role Selection Machine returned error"
                                  "for instance %d !!!\n", VlanId);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Role Selection Failure for Instance = %d"
                                  "!!!\n", VlanId);

                    return PVRST_FAILURE;
                }

                if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                          AST_STP_PORT_UP) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "Module Enable: Unable to Enable port = "
                                  "%d for instance= %d in set pvrst status!!!\n",
                                  u4PortNo, VlanId);
                    AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS:  Unable to enable port = %d for"
                                  " instance = %d in set pvrst status!!!\n",
                                  u4PortNo, VlanId);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstAddTaggedPort                         */
/*                                                                           */
/*    Description               : This function adds the given tagged port to*/
/*                                spanning-tree instance of the Vlan         */
/*    Input(s)                  : u4PortNo - Port Number of the Port         */
/*                                VlanId   - Vlan ID                         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstAddTaggedPort (UINT4 u4PortNo, tVlanId VlanId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1OperStatus;

    pPortInfo = AST_GET_PORTENTRY ((UINT2) u4PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);

    if ((pPortInfo == NULL) || (pCommPortInfo == NULL))
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                      AST_ALL_FAILURE_TRC,
                      "SYS: Port information or common port"
                      "information are not present for port %d for"
                      " instance %d !!!\n", u4PortNo, VlanId);
        return PVRST_FAILURE;
    }
    AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex, &u1OperStatus);

    if ((pPortInfo->bPathCostInitialized == PVRST_FALSE) &&
        (u1OperStatus == AST_PORT_OPER_UP))
    {
        pPortInfo->u4PathCost = AstCalculatePathcost ((UINT2) u4PortNo);
        pPortInfo->bPathCostInitialized = PVRST_TRUE;
    }

    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) == OSIX_FALSE)
    {
        return PVRST_SUCCESS;
    }
    if (PvrstPerStPortCreate ((UINT2) u4PortNo, VlanId) != PVRST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Unable to create port = %d for"
                      "instance = %d !!!\n", u4PortNo, VlanId);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS:  Unable to create port = %d for"
                      "instance = %d !!!\n", u4PortNo, VlanId);
        return PVRST_FAILURE;
    }

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((VlanId == PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)))
        {
            break;
        }
    }

    if ((PvrstPathcostConfiguredFlag ((UINT2) u4PortNo, u2InstIndex) ==
         PVRST_FALSE) && (pPortInfo->bPathCostInitialized == PVRST_TRUE))
    {
        pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
            (u4PortNo, u2InstIndex);
        /* Instance is getting newly created on this port
         * and the port is already oper up.
         * Calculate the path cost for the port */
        pPerStPvrstRstPortInfo->u4PathCost = pPortInfo->u4PathCost;

    }

    if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
    {
        AstSetInstPortStateToL2Iwf (u2InstIndex, (UINT2) u4PortNo,
                                    AST_PORT_STATE_FORWARDING);

        AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u4PortNo) =
            AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
        if (AST_IS_PVRST_ENABLED () &&
            (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
        {
            PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                     pPortInfo->u4IfIndex, VlanId,
                                     AST_PORT_STATE_FORWARDING);
        }
#endif /* NPAPI_WANTED */
        return PVRST_SUCCESS;
    }

    if ((u1OperStatus == (UINT1) AST_PORT_OPER_UP) &&
        (AST_MODULE_STATUS == PVRST_ENABLED))
    {
        PvrstReInitPvrstInstInfo ((UINT2) u4PortNo, VlanId);

        if (PvrstPortRoleSelectionMachine
            (PVRST_PROLESELSM_EV_BEGIN, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Role Selection Machine returned error"
                          "for instance %d !!!\n", VlanId);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Role Selection Failure for Instance = %d"
                          "!!!\n", VlanId);

            return PVRST_FAILURE;
        }
        if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                  AST_STP_PORT_UP) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "Module Enable: Unable to Enable port = "
                          "%d for instance= %d in set pvrst status!!!\n",
                          u4PortNo, VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS:  Unable to enable port = %d for"
                          " instance = %d in set pvrst status!!!\n",
                          u4PortNo, VlanId);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstAddAccessPort                         */
/*                                                                           */
/*    Description               : This function makes the given Port Access  */
/*                                for the given InstanceId                   */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                PvrstInst- Instance Id to which the given  */
/*                                           Port will belong to             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstAddAccessPort (UINT4 u4PortNo, tVlanId VlanId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             LocalVlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;

    pPortInfo = AST_GET_PORTENTRY ((UINT2) u4PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);

    if ((pPortInfo->bPTypeInconsistent == AST_TRUE))
    {
        pPortInfo->bPTypeInconsistent = AST_FALSE;
        if (PvrstStopTimer ((VOID *) pPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                            (UINT1) AST_TMR_TYPE_INC_RECOVERY) == PVRST_FAILURE)
        {
            return PVRST_FAILURE;
        }
       /**Recovering from Type inconsistancy : Port Un blocked**/
        PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId, AST_EXT_PORT_UP);
    }
    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((LocalVlanId =
             PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        if (VlanId == LocalVlanId)
        {
            continue;
        }
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), LocalVlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }

        if (PvrstPerStPortDelete ((UINT2) u4PortNo, LocalVlanId) !=
            PVRST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }

    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) == OSIX_TRUE)
    {
        if (PvrstPerStPortCreate ((UINT2) u4PortNo, VlanId) != PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Unable to create port = %d for"
                          "instance = %d in Add Access Port!!!\n",
                          u4PortNo, VlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS:  Unable to create port = %d for"
                          "instance = %d in Add Access Port!!!\n",
                          u4PortNo, VlanId);
            return PVRST_FAILURE;
        }

        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);

        AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                   &u1OperStatus);

        if ((pPortInfo->bPathCostInitialized == PVRST_FALSE) &&
            (u1OperStatus == AST_PORT_OPER_UP))
        {
            pPortInfo->u4PathCost = AstCalculatePathcost ((UINT2) u4PortNo);
            pPortInfo->bPathCostInitialized = PVRST_TRUE;
        }

        if ((PvrstPathcostConfiguredFlag ((UINT2) u4PortNo, u2InstIndex) ==
             PVRST_FALSE) && (pPortInfo->bPathCostInitialized == PVRST_TRUE))
        {
            pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (u4PortNo, u2InstIndex);
            /* Instance is getting newly created on this port
             * and the port is already oper up.
             * Calculate the path cost for the port */
            pPerStPvrstRstPortInfo->u4PathCost = pPortInfo->u4PathCost;
        }

        if (pCommPortInfo->bPortPvrstStatus == PVRST_FALSE)
        {
            AstSetInstPortStateToL2Iwf (u2InstIndex, (UINT2) u4PortNo,
                                        AST_PORT_STATE_FORWARDING);
            AST_GET_LAST_PROGRMD_STATE (u2InstIndex, u4PortNo) =
                AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_PVRST_ENABLED () &&
                (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         pPortInfo->u4IfIndex, VlanId,
                                         AST_PORT_STATE_FORWARDING);
            }
#endif /* NPAPI_WANTED */

            return PVRST_SUCCESS;
        }

        if ((u1OperStatus == (UINT1) AST_PORT_OPER_UP) &&
            (AST_MODULE_STATUS == PVRST_ENABLED))
        {
            PvrstReInitPvrstInstInfo ((UINT2) u4PortNo, VlanId);

            if (PvrstPortRoleSelectionMachine
                (PVRST_PROLESELSM_EV_BEGIN, VlanId) != PVRST_SUCCESS)
            {

                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Role Selection Machine returned error"
                              "for instance %d !!!\n", VlanId);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Role Selection Failure for Instance = %d"
                              "!!!\n", VlanId);
                return PVRST_FAILURE;
            }

            if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                      AST_STP_PORT_UP) != PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "Module Enable: Unable to Enable port = "
                              "%d for instance= %d in set pvrst status!!!\n",
                              u4PortNo, VlanId);
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS:  Unable to enable port = %d for"
                              " instance = %d in set pvrst status!!!\n",
                              u4PortNo, VlanId);
                return PVRST_FAILURE;
            }
        }
    }
    if (pCommPortInfo->bRootGuard == PVRST_TRUE)
    {
        pCommPortInfo->bRootGuard = PVRST_FALSE;
    }
    return PVRST_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*    Function Name             : PvrstAddHybridPort                           */
/*                                                                             */
/*    Description               : This function removes the port from instances*/
/*                                corresponding to VLAN for which the port is  */
/*                                not member and adds it to instance           */
/*                                corresponding to default VLAN. This function */
/*                                is invoked when port type is changed to      */
/*                                Hybrid                                       */
/*                                                                             */
/*    Input(s)                  : u4PortNo -  Port Number of the Port          */
/*                                            which is to be made Hybrid       */
/*                                                                             */
/*    Output(s)                 : None                                         */
/*                                                                             */
/*    Global Variables Referred : None                                         */
/*                                                                             */
/*    Global Variables Modified : None.                                        */
/*                                                                             */
/*    Exceptions or Operating                                                  */
/*    System Error Handling     : None.                                        */
/*                                                                             */
/*    Use of Recursion          : None.                                        */
/*                                                                             */
/*    Returns                   : PVRST_SUCCESS On Success.                    */
/*                                PVRST_FAILURE On Failure.                    */
/*                                                                             */
/*******************************************************************************/

INT4
PvrstAddHybridPort (UINT4 u4PortNo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             LocalVlanId = RST_DEFAULT_INSTANCE;
    tVlanId             VlanId = AST_DEF_VLAN_ID ();
    UINT2               u2InstIndex = AST_INIT_VAL;

    pPortInfo = AST_GET_PORTENTRY ((UINT2) u4PortNo);

    if ((pPortInfo->bPTypeInconsistent == AST_TRUE))
    {
        pPortInfo->bPTypeInconsistent = AST_FALSE;
        if (PvrstStopTimer ((VOID *) pPortInfo, (UINT2) RST_DEFAULT_INSTANCE,
                            (UINT1) AST_TMR_TYPE_INC_RECOVERY) == PVRST_FAILURE)
        {
            return PVRST_FAILURE;
        }
       /**Recovering from Type inconsistancy : Port Un blocked**/
        PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId, AST_EXT_PORT_UP);
    }
    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((LocalVlanId =
             PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        if (AstL2IwfMiIsVlanActive (AST_DEFAULT_CONTEXT, LocalVlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }
        if (L2IwfMiIsVlanMemberPort (AST_DEFAULT_CONTEXT,
                                     LocalVlanId,
                                     AST_GET_IFINDEX (u4PortNo)) == OSIX_TRUE)
        {
            continue;
        }

        if (PvrstPerStPortDelete ((UINT2) u4PortNo, LocalVlanId) !=
            PVRST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstConfigPvrstStatusPerPort              */
/*                                                                           */
/*    Description               : This function configures the given Port    */
/*                                Pvrst Status                               */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstConfigPvrstStatusPerPort (UINT4 u4PortNo, tAstBoolean bStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    UINT4               u4ActiveInstanceCount = 1;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pPortInfo = AST_GET_PORTENTRY ((UINT2) u4PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO ((UINT2) u4PortNo);

    if (pCommPortInfo->bPortPvrstStatus == bStatus)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "PvrstStatus: Port Pvrst Status is same as"
                      " set value for port = %d !!!\n", u4PortNo);
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS:  Port Pvrst Status is same as"
                      " set value for port = %d !!!\n", u4PortNo);
        return PVRST_SUCCESS;
    }

    if (bStatus == AST_FALSE)
    {
        pCommPortInfo->bPortPvrstStatus = bStatus;

        for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
        {
            if (u4ActiveInstanceCount > AST_MAX_PVRST_INSTANCES)
            {
                break;
            }
            u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                        VlanId);
            if (u2InstIndex == INVALID_INDEX)
            {
                continue;
            }

            if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                OSIX_FALSE)
            {
                continue;
            }

            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);

            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != VlanId)
                {
                    continue;
                }
            }

            if (PvrstPerStDisablePort
                ((UINT2) u4PortNo, VlanId, AST_STP_PORT_DOWN) != PVRST_SUCCESS)
            {
                return PVRST_FAILURE;
            }
            u4ActiveInstanceCount++;
        }
        PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PORTDISABLED,
                                  (UINT2) u4PortNo);
    }
    else
    {
        pCommPortInfo->bPortPvrstStatus = bStatus;

        for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
        {
            if (u4ActiveInstanceCount > AST_MAX_PVRST_INSTANCES)
            {
                break;
            }
            u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                        VlanId);
            if (u2InstIndex == INVALID_INDEX)
            {
                continue;
            }

            if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                OSIX_FALSE)
            {
                continue;
            }

            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);

            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != VlanId)
                {
                    continue;
                }
            }

            PvrstReInitPvrstInstInfo ((UINT2) u4PortNo, VlanId);

            if (PvrstPortRoleSelectionMachine
                (PVRST_PROLESELSM_EV_BEGIN, VlanId) != PVRST_SUCCESS)
            {

                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Role Selection Machine returned error"
                              "for instance %d !!!\n", VlanId);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Role Selection Failure for Instance = %d"
                              "!!!\n", VlanId);

                return PVRST_FAILURE;
            }
            if ((pCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE) ||
                ((pCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE) &&
                 (AST_GBL_BPDUGUARD_STATUS == PVRST_TRUE)))
            {
                if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                          AST_EXT_PORT_UP) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "Module Enable: Unable to Enable port = "
                                  "%d in set pvrst status!!!\n", u4PortNo);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS:  Unable to enable port = %d "
                                  "in set pvrst status!!!\n", u4PortNo);
                    return PVRST_FAILURE;
                }

            }
            else
            {
                if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                          AST_STP_PORT_UP) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "Module Enable: Unable to Enable port = "
                                  "%d in set pvrst status!!!\n", u4PortNo);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS:  Unable to enable port = %d "
                                  "in set pvrst status!!!\n", u4PortNo);
                    return PVRST_FAILURE;
                }
            }
            u4ActiveInstanceCount++;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstConfigPvrstInstStatusPerPort          */
/*                                                                           */
/*    Description               : This function configures the given Port    */
/*                                Status for the given Instance              */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number                    */
/*                                VlanId - Vlan Instance Identifier          */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstConfigPvrstInstStatusPerPort (tVlanId VlanId, UINT4 u4PortNo,
                                   tAstBoolean bStatus)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_PORT_INFO ((UINT2) u4PortNo, u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo =
        PVRST_GET_PERST_RST_PORT_INFO ((UINT2) u4PortNo, u2InstIndex);
    if (pPerStRstPortInfo->bPortEnabled == bStatus)
    {
        return PVRST_SUCCESS;
    }

    if (bStatus == AST_FALSE)
    {
        pPerStRstPortInfo->bPortEnabled = (tAstBoolean) PVRST_FALSE;
        if (PvrstPerStDisablePort ((UINT2) u4PortNo, VlanId, AST_STP_PORT_DOWN)
            != PVRST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }
    else
    {
        PvrstReInitPvrstInstInfo ((UINT2) u4PortNo, VlanId);

        pPerStRstPortInfo->bPortEnabled = (tAstBoolean) PVRST_TRUE;

        if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_BEGIN, VlanId) !=
            PVRST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Role Selection Machine returned error"
                          "for instance %d !!!\n", VlanId);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Role Selection Failure for Instance = %d"
                          "!!!\n", VlanId);

            return PVRST_FAILURE;
        }

        if (PvrstPerStEnablePort ((UINT2) u4PortNo, VlanId,
                                  AST_STP_PORT_UP) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "Module Enable: Unable to Enable port = "
                          "%d in set pvrst status!!!\n", u4PortNo);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS:  Unable to enable port = %d "
                          "in set pvrst status!!!\n", u4PortNo);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetPortGuards                         */
/*                                                                           */
/*    Description               : This function reselects topology for each  */
/*                                instance running for this port when root   */
/*                                guard is set by management configures the  */
/*                                given Port Pvrst Status                    */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType -Root or BPDU guard              */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortGuards (UINT4 u4PortNo, UINT4 u4GuardType, tAstBoolean bStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);

    if (u4GuardType == AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG)
    {
        if (pCommPortInfo->bRootGuard == bStatus)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: PvrstStatus: Root Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Root Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            return PVRST_SUCCESS;
        }
        pCommPortInfo->bRootGuard = bStatus;
        /* RootInconsistent reset when root guard is disabled */

        if (bStatus == PVRST_FALSE)
        {
            pCommPortInfo->bRootInconsistent = PVRST_FALSE;
        }

    }
    else
    {
        return PVRST_SUCCESS;
    }

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) == AST_INIT_VAL)
        {
            continue;
        }
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }

        if (bStatus == PVRST_TRUE)
        {
            if (PVRST_GET_PERST_INFO (u2InstIndex) != NULL)
            {
                if (PVRST_GET_PERST_RST_PORT_INFO
                    ((UINT2) u4PortNo, u2InstIndex) != NULL)
                {
                    pRstPortInfo =
                        PVRST_GET_PERST_RST_PORT_INFO ((UINT2) u4PortNo,
                                                       u2InstIndex);
                    pRstPortInfo->bSelected = PVRST_FALSE;
                    pRstPortInfo->bReSelect = PVRST_TRUE;

                    if (PvrstPortRoleSelectionMachine ((UINT2)
                                                       PVRST_PROLESELSM_EV_RESELECT,
                                                       (UINT2) VlanId) !=
                        PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                      AST_CONTROL_PATH_TRC,
                                      "Module Enable: port Role Selection "
                                      "returned error for instance %d!!!\n",
                                      VlanId);
                        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "SYS: port Role Selection returned"
                                      " error for instance %d!!!\n", VlanId);
                        return PVRST_FAILURE;
                    }
                }
            }
        }
        else
        {
            pPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO ((UINT2) u4PortNo, u2InstIndex);

            if (NULL == pPerStPortInfo)
            {
                continue;
            }

            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO ((UINT2) u4PortNo,
                                                     u2InstIndex);

            if (pPerStPvrstRstPortInfo == NULL)
            {
                continue;
            }

            if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
            {

                if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                                    (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "SYS: PvrstStopTimer for Root Inconsistent Tmr FAILED!\n");
                    return PVRST_FAILURE;
                }

                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s for Vlan : %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), VlanId,
                                      au1TimeStr);
                }
            }
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstReCalculatePathcost                   */
/*                                                                           */
/*    Description               : This function calculates path cost for the */
/*                                given port number                          */
/*                                                                           */
/*    Input(s)                 :  u4IfIndex - Interface for which path cost  */
/*                                is to be calculated                        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstReCalculatePathcost (UINT4 u4IfIndex)
{
    tVlanId             VlanId;
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    UINT1               au1ChangeFlag[AST_MAX_PVRST_INSTANCES + 1];
    /* Flag used to indicate role selection 
       is needed for each instance or not */
    UINT4               u4PathCost = 0;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    u2PortNum = (UINT2) u4IfIndex;

    MEMSET (au1ChangeFlag, 0, (AST_MAX_PVRST_INSTANCES + 1));

    if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        return PVRST_FAILURE;
    }
    pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
    for (u2Inst = 1; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
    {
        pPerStInfo = PVRST_GET_PERST_INFO (u2Inst);
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2Inst);
        if (NULL == pPerStPvrstRstPortInfo)
        {
            continue;
        }
        u4PathCost = pPerStPvrstRstPortInfo->u4PathCost;

        if (PvrstPathcostConfiguredFlag (u2PortNum, u2Inst) == PVRST_FALSE)
        {
            pPerStPvrstRstPortInfo->u4PathCost =
                AstCalculatePathcost (u2PortNum);
        }

        if (u4PathCost != pPerStPvrstRstPortInfo->u4PathCost)
        {
            if (AST_IS_PVRST_ENABLED ())
            {
                au1ChangeFlag[u2Inst] = PVRST_TRUE;
                pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                pPerStRstPortInfo->bSelected = PVRST_FALSE;
                pPerStRstPortInfo->bReSelect = PVRST_TRUE;
            }
        }
    }
    if (AST_IS_PVRST_ENABLED ())
    {
        /* Trigger RoleSelection once for each instance */

        for (u2Inst = 0; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
        {
            if (au1ChangeFlag[u2Inst] == PVRST_TRUE)
            {
                if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2Inst)) == AST_INIT_VAL)
                {
                    continue;
                }
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                    OSIX_FALSE)
                {
                    continue;
                }

                pPerStInfo = AST_GET_PERST_INFO (u2Inst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                if (PvrstPortRoleSelectionMachine
                    ((UINT1) PVRST_PROLESELSM_EV_RESELECT,
                     VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    return PVRST_FAILURE;
                }
            }
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetPortBPDUGuard                      */
/*                                                                           */
/*    Description               : This function reselects topology for each  */
/*                                instance running for this port when root   */
/*                                guard is set by management configures the  */
/*                                given Port Pvrst Status                    */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType - BPDU guard                     */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortBPDUGuard (UINT4 u4PortNo, UINT4 u4GuardType,
                       UINT4 u4BpduGuardStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2InstIndex = RST_DEFAULT_INSTANCE;
    tAstCfaIfInfo       IfInfo;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (&IfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT1               u1OperStatus = AST_PORT_OPER_DOWN;
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);

    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return PVRST_FAILURE;
    }
    if (u4GuardType == AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG)
    {
        if (pCommPortInfo->u4BpduGuard == u4BpduGuardStatus)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                          "SYS: PvrstStatus: Bpdu Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG,
                          "SYS: Bpdu Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            return PVRST_SUCCESS;
        }
        pCommPortInfo->u4BpduGuard = u4BpduGuardStatus;

        if (u4BpduGuardStatus != PVRST_TRUE)
        {
            /* BpduInconsistent reset when Bpdu guard is disabled */
            if ((pCommPortInfo->bBpduInconsistent == RST_TRUE) &&
                (pCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN))
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_UP;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u4PortNo);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
            }
            else
            {
                AstL2IwfGetPortOperStatus (STP_MODULE, u4PortNo, &u1OperStatus);
                if (u1OperStatus == AST_PORT_OPER_UP)
                {
                    AstEnablePort ((UINT2) u4PortNo, u2InstIndex,
                                   AST_EXT_PORT_UP);
                }
            }

            if (pCommPortInfo->bBpduInconsistent == PVRST_TRUE)
            {

                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                  AST_GET_IFINDEX_STR ((UINT2) u4PortNo),
                                  au1TimeStr);

                pCommPortInfo->bBpduInconsistent = PVRST_FALSE;
            }

            /*Stop the Error Disable Recovery timer if it is running */
            if (pCommPortInfo->pDisableRecoveryTmr != NULL)
            {
                if (PvrstStopTimer (pPortEntry, u2InstIndex,
                                    AST_TMR_TYPE_ERROR_DISABLE_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ERROR"
                             " DISABLE RECOVERY Timer!\n");
                    return PVRST_FAILURE;
                }
            }
        }

    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetPortBPDUGuardAction                */
/*                                                                           */
/*    Description               : This function reselects topology for each  */
/*                                instance running for this port when root   */
/*                                guard is set by management configures the  */
/*                                given Port Pvrst Status                    */
/*                                                                           */
/*    Input(s)                  : u4PortNo -  Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType - BPDU guard                     */
/*                                u1BpduGuardAction - BPDU Action            */
/*                                to be set                                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);

    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return PVRST_FAILURE;
    }

    pCommPortInfo->u1BpduGuardAction = u1BpduGuardAction;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstAssertBegin                           */
/*                                                                           */
/*    Description               : This function reselects topology for each  */
/*                                instance running for this port when root   */
/*                                guard is set by management configures the  */
/*                                given Port Pvrst Status                    */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType - BPDU guard                     */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS On Success.                  */
/*                                PVRST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT1
PvrstAssertBegin ()
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Enabling Pvrst Module.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Enabling Pvrst Module.\n");
    PvrstReInitPvrstInfo ();

    /* In Active & Standby node PVRST can be directly enabled.
     * When NodeStatus is in IDLE State,PVRST should not be enabled.
     * PVRST can be enabled only when NodeStatus is in ACTIVE or STANDBY State.
     */

    if (AST_NODE_STATUS () == RED_AST_IDLE)
    {
        return PVRST_SUCCESS;
    }

    /*Enable Instance for all the vlan's present */
    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
    {
        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
            OSIX_FALSE)
        {
            continue;
        }
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId);
        if (u2InstIndex == INVALID_INDEX)
        {
#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif

            if ((PVRST_GET_NO_OF_ACTIVE_INSTANCES == AST_MAX_PVRST_INSTANCES)
                || (PVRST_GET_NO_OF_ACTIVE_INSTANCES == AST_INIT_VAL))
            {
                return PVRST_FAILURE;
            }
            return PVRST_FAILURE;
        }
        if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
        {
            continue;
        }
        if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_BEGIN, VlanId) !=
            PVRST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Role Selection Machine returned error"
                          "for instance %d !!!\n", VlanId);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Role Selection Failure for Instance = %d"
                          "!!!\n", VlanId);
#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif

            return PVRST_FAILURE;
        }
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);
                if (PVID != VlanId)
                {
                    continue;
                }
            }

            AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                       &u1OperStatus);

            if (u1OperStatus == AST_UP)
            {
                pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;

                if (PvrstStartSemsForPort (pPerStPortInfo) == PVRST_FAILURE)
                {
                    AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "EnablePort: Port %u: StartSems"
                                  "returned failure !\n", u2PortNum);

#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif

                    return PVRST_FAILURE;
                }

                /*Restart Bridge Detection state machine on this port */
                PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_BEGIN, u2PortNum);

                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if ((pPortInfo->CommPortInfo.bPortPvrstStatus == PVRST_FALSE) ||
                    (pRstPortInfo->bPortEnabled != PVRST_TRUE))
                {
                    continue;
                }

                AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                              "SYS: Port %u enabled. Triggering Enabled event"
                              "for Port Mig and Port Info SEM\n", u2PortNum);

                if (PvrstPortMigrationMachine
                    ((UINT2) PVRST_PMIGSM_EV_PORT_ENABLED, u2PortNum,
                     VlanId) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "SYS: Port %u: Port Protocol Migration Machine"
                                  "Returned failure  !!!\n", u2PortNum);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %u: Port Protocol Migration Machine"
                                  "Returned failure  !!!\n", u2PortNum);
#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    return PVRST_FAILURE;
                }

                if (PvrstPortInfoMachine ((UINT2) PVRST_PINFOSM_EV_PORT_ENABLED,
                                          pPerStPortInfo,
                                          NULL) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "SYS: Port %u: Port Info Machine Returned"
                                  "failure  !!!\n", u2PortNum);
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %u: Port Info Machine Returned"
                                  "failure  !!!\n", u2PortNum);
#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    return PVRST_FAILURE;
                }
            }

        }
    }

    AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
             "SYS: PVRST Module Enabled.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: PVRST Module Enabled.\n");
    return PVRST_SUCCESS;
}
#endif /* PVRST_WANTED */
