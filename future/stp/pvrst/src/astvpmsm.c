/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvpmsm.c,v 1.12 2017/12/29 09:30:39 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Protocol Migration State Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"
#include "astvinc.h"

/*****************************************************************************/
/* Function Name      : PvrstPortMigrationMachine                            */
/*                                                                           */
/* Description        : This is the main routine for the Port Protocol       */
/*                      Migration State Machine.                             */
/*                      This routine calls the action routine for the event- */
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*            VlanId - Vlan Instance Identifier             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortMigrationMachine (UINT2 u2Event, UINT2 u2PortNum, tVlanId VlanId)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RetVal = 0;
    UINT2               u2State;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (NULL == pAstPortEntry)
    {
        AST_DBG_ARG1 (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                      "PMSM: Event %s: Invalid parameter passed to"
                      "event handler routine\n",
                      gaaau1AstSemEvent[AST_PMSM][u2Event]);
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (u2PortNum, u2InstIndex);

    u2State = (UINT2) pPerStPvrstRstPortInfo->u1PmigSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "PMSM: Port %u: Protocol Migr Machine Called with"
                  "Event: %s,State: %s\n", u2PortNum,
                  gaaau1AstSemEvent[AST_PMSM][u2Event],
                  gaaau1AstSemState[AST_PMSM][u2State]);

    AST_DBG_ARG3 (AST_PMSM_DBG,
                  "PMSM: Port %u: Protocol Migr Machine Called with"
                  "Event: %s,State: %s\n", u2PortNum,
                  gaaau1AstSemEvent[AST_PMSM][u2Event],
                  gaaau1AstSemState[AST_PMSM][u2State]);

    if (PVRST_PORT_MIG_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_PMSM_DBG, "PMSM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "Value of Force Version is %u\n", AST_FORCE_VERSION);
    i4RetVal = (*(PVRST_PORT_MIG_MACHINE[u2Event][u2State].pAction))
        (u2PortNum, u2InstIndex);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: Event routine returned failure !!!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmPortEnabledInit                           */
/*                                                                           */
/* Description        : This routine is called when the 'PORT_ENABLED' event */
/*                      occurs in 'INIT' state.                              */
/*                      This routine changes the state machine state to      */
/*                      'SEND_RSTP' when the ForceVersion parameter is       */
/*                      greater than or equal to 2. Else Changes to          */
/*                      'SEND_STP' state.                                    */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmPortEnabledInit (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstContextInfo    *pContextInfo = NULL;
    pContextInfo = AST_CURR_CONTEXT_INFO ();
    if (pContextInfo->u1ForceVersion >= (UINT1) AST_VERSION_2)
    {
        return (PvrstPmigSmMakeSendRstp (u2PortNum, u2InstIndex));
    }
    else
    {
        return (PvrstPmigSmMakeSendStp (u2PortNum, u2InstIndex));
    }
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmRcvdStpInSendingRstp                      */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SEND_STP' state if mdelayWhile timer is not running.*/
/*                      Else changes to 'SENDING_RSTP' state.                */
/*                      Called when a config bpdu or tcn bpdu is received in */
/*                      'SENDING_RSTP' state.                                */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmRcvdStpInSendingRstp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr == NULL)
    {
        return (PvrstPmigSmMakeSendStp (u2PortNum, u2InstIndex));
    }
    else
    {
        return (PvrstPmigSmMakeSendingRstp (u2PortNum, u2InstIndex));
    }
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmRcvdRstpInSendingRstp                     */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SENDING_RSTP'.                                      */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmRcvdRstpInSendingRstp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    return (PvrstPmigSmMakeSendingRstp (u2PortNum, u2InstIndex));
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmRcvdRstpInSendingStp                      */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SEND_RSTP' state if mdelayWhile timer is not        */
/*                      running. Else changes to 'SENDING_STP' state.        */
/*                      Called when a rstp bpdu is received in 'SENDING_STP' */
/*                      state.                                               */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmRcvdRstpInSendingStp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo->pMdWhilePvrstTmr == NULL)
    {
        return (PvrstPmigSmMakeSendRstp (u2PortNum, u2InstIndex));
    }
    else
    {
        return (PvrstPmigSmMakeSendingStp (u2PortNum, u2InstIndex));
    }
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmRcvdStpInSendingStp                       */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SENDING_STP'.                                       */
/*                      Called when a config bpdu or Tcn bpdu is received    */
/*                      in the SENDING_STP state.                            */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmRcvdStpInSendingStp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    return (PvrstPmigSmMakeSendingStp (u2PortNum, u2InstIndex));
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmMakeInit                                  */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'INIT'.                                              */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmMakeInit (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstContextInfo    *pContextInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pContextInfo = AST_CURR_CONTEXT_INFO ();

    pPerStPvrstRstPortInfo->bInitPm = PVRST_TRUE;
    pPerStPvrstRstPortInfo->bMCheck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->u1PmigSmState = (UINT1) PVRST_PMIGSM_STATE_INIT;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %u: Moved to state INIT \n", u2PortNum);

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (pPerStPortInfo != NULL)
    {
        pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        if ((AST_IS_PORT_UP (u2PortNum)) &&
            (pPerStRstPortInfo->bPortEnabled == PVRST_TRUE))
        {
            if (pContextInfo->u1ForceVersion >= (UINT1) AST_VERSION_2)
            {
                return (PvrstPmigSmMakeSendRstp (u2PortNum, u2InstIndex));
            }
            else
            {
                return (PvrstPmigSmMakeSendStp (u2PortNum, u2InstIndex));
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmMakeSendRstp                              */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SEND_RSTP'.                                         */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmMakeSendRstp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2Duration = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pBrgInfo = AST_GET_BRGENTRY ();

    u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_MDELAYWHILE,
                         u2Duration) != PVRST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: PvrstStartTimer for MdWhileTmr FAILED!\n");
        return PVRST_FAILURE;
    }

    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE)
    {
        /* Incrementing the Protocol Migration Count */
        pPerStPvrstRstPortInfo->u4ProtocolMigrationCount++;
        AstPvrstProtocolMigrationTrap (VlanId, u2PortNum, AST_VERSION_2,
                                       AST_TRAP_SEND_RSTP,
                                       (INT1 *) AST_PVRST_TRAPS_OID,
                                       PVRST_BRG_TRAPS_OID_LEN);
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Compatability change received for Port %s at %s Node operating mode :%u  Port operating mode : RSTP",
                          AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr,
                          AST_FORCE_VERSION);
    }

    pPerStPvrstRstPortInfo->bMCheck = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bInitPm = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bSendRstp = PVRST_TRUE;
    /*Removing the ports from default instance because force version has changed to RST */
    if (L2IwfMiIsVlanMemberPort
        (AST_CURR_CONTEXT_ID (), AST_DEF_VLAN_ID (), u2PortNum) == OSIX_FALSE)
    {
        if (pPerStPortInfo->bDisableInProgress == AST_FALSE)
        {
            if (PvrstPerStPortDelete (u2PortNum, AST_DEF_VLAN_ID ()) !=
                PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "DeletePort: Port %s Inst %d: PvrstPerStPortDelete"
                              "returned failure !\n",
                              AST_GET_IFINDEX_STR (u2PortNum),
                              AST_DEF_VLAN_ID ());
                return PVRST_FAILURE;
            }
        }
    }

    /*Call the BDSM with SENDRSTP_SET */
    PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_SENDRSTP_SET, u2PortNum);

    pPerStPvrstRstPortInfo->u1PmigSmState = (UINT1)
        PVRST_PMIGSM_STATE_SEND_RSTP;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %u: Moved to state SEND_RSTP \n", u2PortNum);

    return (PvrstPmigSmMakeSendingRstp (u2PortNum, u2InstIndex));
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmMakeSendingRstp                           */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SENDING_RSTP'.                                      */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmMakeSendingRstp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstContextInfo    *pContextInfo = NULL;

    if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
    {
        return PVRST_SUCCESS;
    }

    pContextInfo = AST_CURR_CONTEXT_INFO ();
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_SUCCESS;
    }
    pPerStPvrstRstPortInfo->bSendRstp = PVRST_TRUE;
    pPerStPvrstRstPortInfo->u1PmigSmState =
        (UINT1) PVRST_PMIGSM_STATE_SENDING_RSTP;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %u: Moved to state SENDING_RSTP \n", u2PortNum);

    if (pContextInfo->u1ForceVersion < (UINT1) AST_VERSION_2)
    {
        return (PvrstPmigSmMakeSendStp (u2PortNum, u2InstIndex));
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmMakeSendStp                               */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SEND_STP'.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmMakeSendStp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2Duration = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);

    if (pPerStInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    VlanId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

    u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_MDELAYWHILE,
                         u2Duration) != PVRST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: PvrstStartTimer for MdWhileTmr FAILED!\n");
        return PVRST_FAILURE;
    }
    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE)
    {
        /* Incrementing the Protocol Migration Count */
        pPerStPvrstRstPortInfo->u4ProtocolMigrationCount++;

        AstPvrstProtocolMigrationTrap (VlanId, u2PortNum, AST_VERSION_0,
                                       AST_TRAP_SEND_STP,
                                       (INT1 *) AST_PVRST_TRAPS_OID,
                                       PVRST_BRG_TRAPS_OID_LEN);
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Compatability change received for Port %s at %s Node operating mode : %u Port operating mode : STP",
                          AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr,
                          AST_FORCE_VERSION);

    }
    /* On recieving the STP PDUs, Changing the force version to STP
     * and adding the ports into the default instance*/
    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE)
    {
        pPerStPvrstRstPortInfo->bSendRstp = PVRST_FALSE;
        if (pPerStInfo->ppPerStPortInfo[u2PortNum - 1] == NULL)
        {

            if (PvrstPerStPortCreate (u2PortNum, AST_DEF_VLAN_ID ()) !=
                PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Unable to create port = %d"
                              "for instance = %d !!!\n", u2PortNum,
                              AST_DEF_VLAN_ID ());
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Unable to create port = "
                              "%d for instance = %d !!!\n", u2PortNum,
                              AST_DEF_VLAN_ID ());
                return PVRST_FAILURE;
            }
            if ((PvrstPerStEnablePort (u2PortNum, AST_DEF_VLAN_ID (),
                                       AST_STP_PORT_UP)) != PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Unable to enable port = %d"
                              "for instance = %d !!!\n", u2PortNum,
                              AST_DEF_VLAN_ID ());
            }

        }
        PvrstCopyStpParametersToNonDefInst (u2PortNum);
    }

    pPerStPvrstRstPortInfo->bInitPm = PVRST_FALSE;

    pPerStPvrstRstPortInfo->u1PmigSmState = (UINT1) PVRST_PMIGSM_STATE_SEND_STP;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %u: Moved to state SEND_STP \n", u2PortNum);

    return (PvrstPmigSmMakeSendingStp (u2PortNum, u2InstIndex));
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmMakeSendingStp                            */
/*                                                                           */
/* Description        : This routine Changes the state machine state to      */
/*                      'SENDING_STP'.                                       */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPmigSmMakeSendingStp (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStPvrstRstPortInfo->bSendRstp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->u1PmigSmState =
        (UINT1) PVRST_PMIGSM_STATE_SENDING_STP;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %u: Moved to state SENDING_STP \n", u2PortNum);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPmigSmEventImpossible                           */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination.*/
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                        u2InstIndex - Instance Identifier Index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_FAILURE                                        */
/*****************************************************************************/
INT4
PvrstPmigSmEventImpossible (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "PMSM: IMPOSSIBLE EVENT/STATE Combination Occured !!!\n");
    AST_DBG (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
             "PMSM: IMPOSSIBLE EVENT/STATE Combination Occured !!!\n");

    AST_UNUSED (u2PortNum);
    AST_UNUSED (u2InstIndex);
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstInitProtocolMigrationMachine                    */
/*                                                                           */
/* Description        : Initialises the Port Protocol Migration State Machine*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PvrstInitProtocolMigrationMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_PMIGSM_MAX_EVENTS][20] = {
        "BEGIN", "PORT_ENABLED",
        "PORT_DISABLED", "MCHECK",
        "FORCE_VER_LT_2", "RCVD_STP",
        "RCVD_RSTP"
    };
    UINT1               aau1SemState[PVRST_PMIGSM_MAX_STATES][20] = {
        "INIT", "SEND_RSTP", "SENDING_RSTP",
        "SEND_STP", "SENDING_STP"
    };
    for (i4Index = 0; i4Index < PVRST_PMIGSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PMSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PMSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_PMIGSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PMSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PMSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* Event 0 - PVRST_PMIGSM_EV_BEGIN */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_BEGIN]
        [PVRST_PMIGSM_STATE_INIT].pAction = PvrstPmigSmMakeInit;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_BEGIN]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_BEGIN]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction = PvrstPmigSmMakeInit;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_BEGIN]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_BEGIN]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction = PvrstPmigSmMakeInit;

    /* Event 1 - PVRST_PMIGSM_EV_PORT_ENABLED */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_ENABLED]
        [PVRST_PMIGSM_STATE_INIT].pAction = PvrstPmigSmPortEnabledInit;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_ENABLED]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_ENABLED]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction = NULL;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_ENABLED]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_ENABLED]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction = NULL;

    /* Event 2 - PVRST_PMIGSM_EV_PORT_DISABLED */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_DISABLED]
        [PVRST_PMIGSM_STATE_INIT].pAction = PvrstPmigSmMakeInit;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_DISABLED]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_DISABLED]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction = PvrstPmigSmMakeInit;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_DISABLED]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_PORT_DISABLED]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction = PvrstPmigSmMakeInit;

    /* Event 3 - PVRST_PMIGSM_EV_MCHECK */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_MCHECK]
        [PVRST_PMIGSM_STATE_INIT].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_MCHECK]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_MCHECK]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction = PvrstPmigSmMakeSendRstp;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_MCHECK]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_MCHECK]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction = PvrstPmigSmMakeSendRstp;

    /* Event 4 - PVRST_PMIGSM_EV_FORCE_VER_LT_2 */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_FORCE_VER_LT_2]
        [PVRST_PMIGSM_STATE_INIT].pAction = NULL;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_FORCE_VER_LT_2]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_FORCE_VER_LT_2]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction = PvrstPmigSmMakeSendStp;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_FORCE_VER_LT_2]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_FORCE_VER_LT_2]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction = NULL;

    /* Event 5 - PVRST_PMIGSM_EV_RCVD_STP */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_STP]
        [PVRST_PMIGSM_STATE_INIT].pAction = NULL;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_STP]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_STP]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction =
        PvrstPmigSmRcvdStpInSendingRstp;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_STP]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_STP]
        [PVRST_PMIGSM_STATE_SENDING_STP].pAction =
        PvrstPmigSmRcvdStpInSendingStp;

    /* Event 6 - PVRST_PMIGSM_EV_RCVD_RSTP */
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_RSTP]
        [PVRST_PMIGSM_STATE_INIT].pAction = NULL;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_RSTP]
        [PVRST_PMIGSM_STATE_SEND_RSTP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_RSTP]
        [PVRST_PMIGSM_STATE_SENDING_RSTP].pAction =
        PvrstPmigSmRcvdRstpInSendingRstp;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_RSTP]
        [PVRST_PMIGSM_STATE_SEND_STP].pAction = PvrstPmigSmEventImpossible;
    PVRST_PORT_MIG_MACHINE[PVRST_PMIGSM_EV_RCVD_RSTP]
        [PVRST_PMIGSM_STATE_SENDING_STP].
        pAction = PvrstPmigSmRcvdRstpInSendingStp;

    AST_DBG (AST_TXSM_DBG, "PMSM: Loaded PMSM SEM successfully\n");

    return;
}
