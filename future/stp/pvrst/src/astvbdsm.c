/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvbdsm.c,v 1.10 2016/09/30 10:54:30 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Bridge Detection State Machine.
 *
 *****************************************************************************/

#define _ASTVBDSM_C_
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : PvrstBrgDetectionMachine                             */
/*                                                                           */
/* Description        : This is the main routine for the Port Bridge         */
/*                      Detection State Machine.                             */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      u2PortNum - The Port Number.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBrgDetectionMachine (UINT2 u2Event, UINT2 u2PortNum)
{
    INT4                i4RetVal;
    UINT2               u2State;

    if (NULL == AST_GET_PORTENTRY (u2PortNum))
    {
        AST_DBG_ARG1 (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                      "BDSM: Event %s: Invalid parameter passed to"
                      "event handler routine\n",
                      gaaau1AstSemEvent[AST_BDSM][u2Event]);
        return PVRST_FAILURE;
    }
    u2State = (UINT2) (AST_GET_COMM_PORT_INFO (u2PortNum))->u1BrgDetSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "BDSM: Port %u: Bridge Detection Machine Called"
                  "with Event: %s, State: %s\n",
                  u2PortNum,
                  gaaau1AstSemEvent[AST_BDSM][u2Event],
                  gaaau1AstSemState[AST_BDSM][u2State]);

    AST_DBG_ARG3 (AST_BDSM_DBG,
                  "BDSM: Port %u: Bridge Detection Machine Called with"
                  "Event: %s, State: %s\n",
                  u2PortNum,
                  gaaau1AstSemEvent[AST_BDSM][u2Event],
                  gaaau1AstSemState[AST_BDSM][u2State]);

    if (PVRST_BRG_DET_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                 "BDSM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    i4RetVal = (*(PVRST_BRG_DET_MACHINE[u2Event][u2State].pAction)) (u2PortNum);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                 "BDSM: Event routine returned FAILURE !!!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmMakeInit                                    */
/*                                                                           */
/* Description        : This routine initializes the operational value of    */
/*                      the Edge Port parameter to TRUE.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmMakeInit (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /*Check for AdminEdge and call the appropriate functions */
    if (pAstPortEntry->bAdminEdgePort == PVRST_TRUE)
    {
        if (PvrstBdSmMakeEdge (u2PortNum) != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_BDSM_DBG,
                          "BDSM: Port %u: PvrstBdSmMakeEdge() call "
                          "returned failure\n", u2PortNum);
        }
    }
    else
    {
        PvrstBdSmMakeNotEdge (u2PortNum);
    }
    if (pAstPortEntry->bOperEdgePort == PVRST_FALSE)
    {
        /* L2Iwf will indicate to PNAC after updating the common 
           database */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_FALSE);
    }
    else
    {
        /* L2Iwf will indicate to PNAC after updating the common 
           database */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_TRUE);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmAdminEdgeSetPortDisabled                    */
/*                                                                           */
/* Description        : This routine is called when Admin Edge status is     */
/*                      configured but the port is in disabled state         */
/*                      (oper down)                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmAdminEdgeSetPortDisabled (UINT2 u2PortNum)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN)
    {
        /*Check for AdminEdge and call the appropriate functions */
        if (pAstPortEntry->bAdminEdgePort == PVRST_TRUE)
        {
            if (PvrstBdSmMakeEdge (u2PortNum) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %u: MakeEdge returned FAILURE",
                              u2PortNum);
                return PVRST_FAILURE;
            }
        }
        else
        {
            PvrstBdSmMakeNotEdge (u2PortNum);
        }
    }
    else
    {
        AST_DBG_ARG1 (AST_BDSM_DBG,
                      "BDSM: Port %u: Port not disabled\n", u2PortNum);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmChkMakeEdge                                 */
/*                                                                           */
/* Description        : This routine checks if the port can transition to    */
/*                      Edge state.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmChkMakeEdge (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1IsNotEdge = AST_INIT_VAL;


    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }
        pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
        pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
            (u2PortNum, u2InstIndex);

        if (!((pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE) &&
            (pPerStRstPortInfo->bProposing == PVRST_TRUE)
            && (pCommPortInfo->bRootGuard != PVRST_TRUE)
            && (pPortEntry->bAutoEdge==PVRST_TRUE)
            &&(pCommPortInfo->pEdgeDelayWhileTmr==NULL)))

        {
            u1IsNotEdge = AST_TRUE;
        }
    }
    if(u1IsNotEdge != AST_TRUE)
    {

        if (PvrstBdSmMakeEdge (u2PortNum) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                    "BDSM: Port %u: MakeEdge returned FAILURE",
                    u2PortNum);
            return PVRST_FAILURE;

        }
        return PVRST_SUCCESS;
    }
    
    if ((pPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN) &&
        (pPortEntry->bAdminEdgePort == PVRST_TRUE))
    {
        if (PvrstBdSmMakeEdge (u2PortNum) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "BDSM: Port %u: MakeEdge returned FAILURE",
                          u2PortNum);
            return PVRST_FAILURE;
        }
        return PVRST_SUCCESS;
    }

    AST_DBG_ARG1 (AST_BDSM_DBG,
                  "BDSM: Port %u: All the conditions to"
                  "move to Edge port are NOT PRESENT\n", u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmMakeEdge                                    */
/*                                                                           */
/* Description        : This routine initializes the operational value of    */
/*                      the Edge Port parameter to TRUE.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmMakeEdge (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    AST_DBG_ARG1 (AST_BDSM_DBG, "BDSM: Port %u: Moved to state EDGE\n",
                  u2PortNum);
    if (pPortEntry->bOperEdgePort != PVRST_TRUE)
    {
        /* Indicate the L2Iwf Common Database to update the info. 
         * L2Iwf takes care of indicating PNAC about Bridge
         * Detection */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_TRUE);
    }

    pCommPortInfo->u1BrgDetSmState = (UINT1) PVRST_BRGDETSM_STATE_EDGE;
    pPortEntry->bOperEdgePort = PVRST_TRUE;

    if (AST_IS_PVRST_ENABLED ())
    {
        for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
             u2InstIndex++)
        {
            pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
            if (pPerStInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            if (PvrstPortRoleTrMachine
                (PVRST_PROLETRSM_EV_OPEREDGE_SET, pPerStPortInfo)
                != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "BDSM: Port %u: Role Transition Machine"
                              "returned FAILURE!\n", u2PortNum);
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_BDSM_DBG | AST_ALL_FAILURE_DBG,
                              "BDSM: Port %u: Role Transition Machine"
                              "returned FAILURE!\n", u2PortNum);
                return PVRST_FAILURE;
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmMakeNotEdge                                 */
/*                                                                           */
/* Description        : This routine is called whenever a BPDU is received   */
/*                      on this Port. This sets the Operational value of the */
/*                      Edge Port parameter to be False.                     */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmMakeNotEdge (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    AST_DBG_ARG1 (AST_BDSM_DBG, "BDSM: Port %u: Moved to state NOT_EDGE\n",
                  u2PortNum);

    /* MakeNotEdge is called under 3 conditions - 
     *   1) when port oper status is down and Admin edge status is made false
     *   2) bpdu is received
     *   3) port is enabled or rstp module is enabled and Admin edge status 
     *      is false.
     * In all these cases we can directly set operEdge as false 
     * No other check needs to be made here.*/

    if (pPortEntry->bOperEdgePort != PVRST_FALSE)
    {
        /* Indicate the L2Iwf Common Database to update the info. 
         * L2Iwf takes care of indicating PNAC about Bridge
         * Detection */
        AstSetPortOperEdgeStatusToL2Iwf (u2PortNum, OSIX_FALSE);
    }

    /* Because we do not have a port receive sem for rstp we cannot make 
     * operEdge false when bpdu is received. It will be done here. 
     */
    pPortEntry->bOperEdgePort = PVRST_FALSE;
    pCommPortInfo->u1BrgDetSmState = (UINT1) PVRST_BRGDETSM_STATE_NOT_EDGE;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstBdSmStartEdgeDelayWhile                         */
/*                                                                           */
/* Description        : This routine starts the EdgeDelayWhile timer. The    */
/*                      duration is based on the value of operPointToPoint   */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstBdSmStartEdgeDelayWhile (UINT2 u2PortNum,UINT2 u2InstIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT2               u2EdgeDelay;

    pBrgInfo = AST_GET_BRGENTRY ();
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /* Start EdgeDelayWhile for a duration of 
     * 1) Migrate time if OperPointToPoint is true.
     * 2) Max Age component of the DesgTimes for the port 
     *    if OperPointToPoint is false.
     */
    if (pAstPortEntry->bOperPointToPoint == PVRST_TRUE)
    {
        u2EdgeDelay = (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    }
    else
    {
        u2EdgeDelay = PVRST_DEFAULT_BRG_MAX_AGE;
    }

    /*Start the EdgeDelayWhileTimer */
    if (PvrstStartTimer ((VOID *) pAstPortEntry, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                         u2EdgeDelay) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "BDSM: Port %u: PvrstStartTimer for EdgeDelayWhileTmr "
                      "FAILED!\n", u2PortNum);
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitBrgDetectionStateMachine                    */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstInitBrgDetectionStateMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_BRGDETSM_MAX_EVENTS][20] = {
        "BEGIN", "ADMIN_EDGE_SET", "BPDU_RCVD",
        "EDGEDELAYWHILE_EXP", "SENDRSTP_SET", "PROPOSING_SET",
        "PORTDISABLED"
    };
    UINT1               aau1SemState[PVRST_BRGDETSM_MAX_STATES][20] = {
        "EDGE", "NOT_EDGE"
    };
    /* Bridge Detection SEM */

    for (i4Index = 0; i4Index < PVRST_BRGDETSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_BDSM][i4Index],
                    aau1SemEvent[i4Index], STRLEN(aau1SemEvent[i4Index]));
	gaaau1AstSemEvent[AST_BDSM][i4Index][STRLEN(aau1SemEvent[i4Index])] = '\0';
    }
    for (i4Index = 0; i4Index < PVRST_BRGDETSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_BDSM][i4Index],
                    aau1SemState[i4Index], STRLEN(aau1SemState[i4Index]));
	gaaau1AstSemState[AST_BDSM][i4Index][STRLEN(aau1SemState[i4Index])] = '\0';
    }
    /* Event 1 BEGIN Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_BEGIN]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = PvrstBdSmMakeInit;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_BEGIN]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction = PvrstBdSmMakeInit;

    /* Event 2 ADMIN_EDGE Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_ADMIN_SET]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = PvrstBdSmAdminEdgeSetPortDisabled;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_ADMIN_SET]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction =
        PvrstBdSmAdminEdgeSetPortDisabled;

    /* Event 3 BPDU_RCVD Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_BPDU_RCVD]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = PvrstBdSmMakeNotEdge;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_BPDU_RCVD]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction = NULL;

    /* Event 4 EDGE_DELAY_WHILE_EXP Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_EDGEDELAYWHILE_EXP]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction = PvrstBdSmChkMakeEdge;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_EDGEDELAYWHILE_EXP]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 5 SENDRSTP_SET Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_SENDRSTP_SET]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction = PvrstBdSmChkMakeEdge;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_SENDRSTP_SET]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 6 PROPOSING_SET Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_PROPOSING_SET]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction = NULL;
    /*       PvrstBdSmStartEdgeDelayWhile; */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_PROPOSING_SET]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = NULL;

    /* Event 7 PORT_DISABLED Event */
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_PORTDISABLED]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = PvrstBdSmAdminEdgeSetPortDisabled;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_PORTDISABLED]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction =
        PvrstBdSmAdminEdgeSetPortDisabled;

    /*Event 8 PORT_AUTOEDGE_SET Event*/
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_AUTOEDGE_SET]
        [PVRST_BRGDETSM_STATE_EDGE].pAction = NULL;
    PVRST_BRG_DET_MACHINE[PVRST_BRGDETSM_EV_AUTOEDGE_SET]
        [PVRST_BRGDETSM_STATE_NOT_EDGE].pAction =
        PvrstBdSmChkMakeEdge;


    AST_DBG (AST_TXSM_DBG, "BDSM: Loaded BDSM SEM successfully\n");

    return;
}

/* End of File */
