/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvsnmp.c,v 1.30 2017/12/11 09:58:31 siva Exp $
 *
 * Description: This file contains PVRST routines which are called  
 *              by low level routines.  
 *
 *****************************************************************************/

#ifdef PVRST_WANTED
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : PvrstSetInstPortPathCost                             */
/*                                                                           */
    /* Description        : This function is the event handler whenever the      */
/*                      Path Cost on a port for an instance is set through   */
/*                      management and the corresponding event is generated. */
/*                                                                           */
/* Input(s)           : PathCost -Path cost to be set                 */
/*            VlanId- Vlan Instance for which path cost is given   */
/*            PortNum- Port number for which path cost is given    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstSetInstPortPathCost (tVlanId VlanId, UINT2 u2PortNum, UINT4 u4PathCost)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (NULL == pPerStPortInfo)
    {
        return PVRST_FAILURE;
    }
    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                                  u2InstIndex);
    if ((pPerStPvrstRstPortInfo->u4PathCost == u4PathCost) &&
        ((pPerStPortInfo->u4PortAdminPathCost == u4PathCost) ||
         (pPerStPortInfo->u4PortAdminPathCost == 0)))
    {
        pPerStPortInfo->u4PortAdminPathCost = u4PathCost;
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port PathCost\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port PathCost\n");
        return PVRST_SUCCESS;
    }
    pPerStPvrstRstPortInfo->u4PathCost = u4PathCost;
    PvrstSetPortPathcostConfigured (u2PortNum, u2InstIndex, u4PathCost);

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4PathCost);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4PathCost);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPortInfo->PerStRstPortInfo.bSelected = PVRST_FALSE;
        pPerStPortInfo->PerStRstPortInfo.bReSelect = PVRST_TRUE;
        if (PvrstPortRoleSelectionMachine ((UINT1) PVRST_PROLESELSM_EV_RESELECT,
                                           VlanId) != PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstSetInstZeroPortPathCost                         */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Path Cost on a port for an instance is set through   */
/*                      management and the corresponding event is generated. */
/*                                                                           */
/* Input(s)           : VlanId-Vlan Instance for which zero Admin path is set*/
/*                      PortNum-Port number for which zero Admin path is set */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstSetInstZeroPortPathCost (tVlanId VlanId, UINT2 u2PortNum)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4TempPathCost = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (NULL == pPerStPortInfo)
    {
        return PVRST_FAILURE;
    }
    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                                  u2InstIndex);
    PvrstSetPortPathcostConfigured (u2PortNum, u2InstIndex, 0);
    u4TempPathCost = AstCalculatePathcost (u2PortNum);

    if (pPerStPvrstRstPortInfo->u4PathCost == u4TempPathCost)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port PathCost\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port PathCost\n");
        return PVRST_SUCCESS;
    }

    pPerStPvrstRstPortInfo->u4PathCost = u4TempPathCost;
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4TempPathCost);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4TempPathCost);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPortInfo->PerStRstPortInfo.bSelected = PVRST_FALSE;
        pPerStPortInfo->PerStRstPortInfo.bReSelect = PVRST_TRUE;
        if (PvrstPortRoleSelectionMachine ((UINT1) PVRST_PROLESELSM_EV_RESELECT,
                                           VlanId) != PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstSetPortPathcostConfigured                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathcostConfigured          */
/*                        parameter                                          */
/*                                                                           */
/*     INPUT            : u4PortNum - PortNumber                             */
/*                        u2InstIndex - Instance Index of Spanning Tree Inst */
/*                        u4Val - PVRST_TRUE/PVRST_FALSE                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : PVRST_SUCCESS/PVRST_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortPathcostConfigured (UINT2 u2PortNum, UINT2 u2InstIndex, UINT4 u4Val)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    pAstPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    /* Check if this Port isn't a member of this instance then 
     * move onto the next Instance */
    if (pAstPerStPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pAstPerStPortInfo->u4PortAdminPathCost = u4Val;
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetDynamicPathcostCalc                */
/*                                                                           */
/*    Description               : This function is the event handler whenever*/
/*                                dynamic pathcost calculation is set or     */
/*                                reset by  management and the               */
/*                                corresponding event is generated.          */
/*                                                                           */
/*    Input(s)                  : u1DynamicPathcostCalc - PVRST_FALSE/       */
/*                                                        PVRST_TRUE         */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetDynamicPathcostCalc (UINT1 u1DynamicPathcostCalc)
{
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT1               au1ChangeFlag[AST_MAX_PVRST_INSTANCES + 1];
    /* Flag used to indicate role selection 
       is needed for each instance or not */
    UINT4               u4PathCost = 0;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == u1DynamicPathcostCalc)
    {
        return (INT1) PVRST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalculation = u1DynamicPathcostCalc;

    MEMSET (au1ChangeFlag, 0, AST_MAX_PVRST_INSTANCES);

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == PVRST_TRUE)
    {
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            for (u2Inst = 1; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2Inst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2Inst);

                u4PathCost = pPerStPvrstRstPortInfo->u4PathCost;

                if (PvrstPathcostConfiguredFlag (u2PortNum, u2Inst) ==
                    PVRST_FALSE)
                {
                    pPerStPvrstRstPortInfo->u4PathCost =
                        AstCalculatePathcost (u2PortNum);
                }

                if (u4PathCost != pPerStPvrstRstPortInfo->u4PathCost)
                {
                    if (AST_IS_PVRST_ENABLED ())
                    {
                        au1ChangeFlag[u2Inst] = PVRST_TRUE;
                        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                        pPerStRstPortInfo->bSelected = PVRST_FALSE;
                        pPerStRstPortInfo->bReSelect = PVRST_TRUE;
                    }
                }
            }
        }
        if (AST_IS_PVRST_ENABLED ())
        {
            /* Trigger RoleSelection once for each instance */
            for (u2Inst = 1; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
            {
                if (au1ChangeFlag[u2Inst] == PVRST_TRUE)
                {
                    if ((VlanId =
                         PVRST_VLAN_TO_INDEX_MAP (u2Inst)) == AST_INIT_VAL)
                    {
                        continue;
                    }

                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId)
                        == OSIX_FALSE)
                    {
                        continue;
                    }

                    pPerStInfo = AST_GET_PERST_INFO (u2Inst);

                    if (pPerStInfo == NULL)
                    {
                        continue;
                    }

                    if (PvrstPortRoleSelectionMachine
                        ((UINT1) PVRST_PROLESELSM_EV_RESELECT, VlanId)
                        != PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                                 AST_MGMT_TRC,
                                 "SNMP: RoleSelectionMachine returned FAILURE!\n");
                        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                                 "SNMP: RoleSelectionMachine returned FAILURE!\n");
                        return PVRST_FAILURE;
                    }
                }
            }
        }

    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetDynaPathcostCalcLagg               */
/*                                                                           */
/*    Description               : This function is the event handler whenever*/
/*                                dynamic pathcost calculation is set or     */
/*                                reset by  management and the               */
/*                                corresponding event is generated.          */
/*                                                                           */
/*    Input(s)                  : u1DynamicPathcostCalcLagg - RST_FALSE/     */
/*                                                            RST_TRUE       */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetDynaPathcostCalcLagg (UINT1 u1DynamicPathcostCalcLagg)
{
    tAstCfaIfInfo       CfaIfInfo;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT4               u4PathCost = 0;
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;

    /* Flag used to indicate role selection 
       is needed for each instance or not */
    UINT1               au1ChangeFlag[AST_MAX_PVRST_INSTANCES + 1];
    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tAstCfaIfInfo));
    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalcLagg == u1DynamicPathcostCalcLagg)
    {
        return (INT1) PVRST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalcLagg = u1DynamicPathcostCalcLagg;

    MEMSET (au1ChangeFlag, 0, AST_MAX_PVRST_INSTANCES);

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pPortEntry),
                             &CfaIfInfo) == CFA_FAILURE)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                          AST_MGMT_TRC,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }

        if (CfaIfInfo.u1IfType != CFA_LAGG)
        {
            continue;
        }

        for (u2Inst = 1; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
        {
            pPerStInfo = PVRST_GET_PERST_INFO (u2Inst);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2Inst);

            u4PathCost = pPerStPvrstRstPortInfo->u4PathCost;

            if (PvrstPathcostConfiguredFlag (u2PortNum, u2Inst) == PVRST_FALSE)
            {
                pPerStPvrstRstPortInfo->u4PathCost = AstCalculatePathcost
                    (u2PortNum);
                if (u4PathCost != pPerStPvrstRstPortInfo->u4PathCost)
                {
                    if (AST_IS_PVRST_ENABLED ())
                    {
                        au1ChangeFlag[u2Inst] = PVRST_TRUE;
                        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                        pPerStRstPortInfo->bSelected = PVRST_FALSE;
                        pPerStRstPortInfo->bReSelect = PVRST_TRUE;
                    }
                }
            }
        }
    }
    if (AST_IS_PVRST_ENABLED ())
    {
        /* Trigger RoleSelection once for each instance */
        for (u2Inst = 1; u2Inst <= AST_MAX_PVRST_INSTANCES; u2Inst++)
        {
            if (au1ChangeFlag[u2Inst] == PVRST_TRUE)
            {
                if ((VlanId = PVRST_VLAN_TO_INDEX_MAP (u2Inst)) == AST_INIT_VAL)
                {
                    continue;
                }

                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId) ==
                    OSIX_FALSE)
                {
                    continue;
                }

                pPerStInfo = AST_GET_PERST_INFO (u2Inst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                if (PvrstPortRoleSelectionMachine
                    ((UINT1) PVRST_PROLESELSM_EV_RESELECT, VlanId)
                    != PVRST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    return PVRST_FAILURE;
                }
            }
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetAdminPointToPoint                  */
/*                                                                           */
/*    Description               : This function sets the Adminstrative Point */
/*                                to Point status of the port                */
/*                                                                           */
/*    Input(s)                  : u2PortNo   - Port Number of the Port.      */
/*                                bAdminP2P  - Point to Point to status      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetAdminPointToPoint (UINT2 u2PortNo, UINT1 u1AdminPToP)
{
    if (RstSetAdminPointToPoint (u2PortNo, u1AdminPToP) != RST_SUCCESS)
    {
        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: RstSetAdminPointToPoint function returned FAILURE!\n");
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetProtocolMigration                  */
/*                                                                           */
/*    Description               : This function sets the protocol migration  */
/*                                for specific instance. It clears the       */
/*                                STP compatibility.                         */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier          */
/*                                Port No - Interface index                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetProtocolMigration (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2InstIndex = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT4                i4RetVal = RST_FAILURE;

    i4RetVal =
        AstGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId);
    if (i4RetVal != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    if (AstL2IwfMiIsVlanActive (u4ContextId, (tVlanId) u2VlanId) == OSIX_FALSE)
    {
        return PVRST_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId, u2VlanId);

    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex);
    if (pPerStPortInfo != NULL)
    {

        PvrstPmigSmMakeSendingRstp (u2LocalPortId, u2InstIndex);
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetBridgePriority                     */
/*                                                                           */
/*    Description               : This function sets the Priority of the     */
/*                                Bridge for specific instance.              */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier for      */
/*                                               which the Priority is going */
/*                                               to be set.                  */
/*                                u2Priority  - Priority value of the Bridge.*/
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetBridgePriority (tVlanId VlanId, UINT2 u2Priority)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;

    /* Check if this Instance is mapped on to the Array */
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    /* Check if this Instance doesn't exist return Failure */
    if ((PVRST_GET_PERST_INFO (u2InstIndex)) == NULL)
    {
        return PVRST_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    /* Check if Bridge Priority is already updated then return Success */
    if ((pAstPerStBridgeInfo->u2BrgPriority & 0xF000) == u2Priority)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Bridge Priority\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Bridge Priority\n");
        return PVRST_SUCCESS;
    }
    PVRST_SET_BRIDGE_PRIORITY (u2Priority, pAstPerStBridgeInfo->u2BrgPriority);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Inst %d: Bridge Priority Set as :%u\n",
                  VlanId, u2Priority);

    /* If PVRST is Enabled then the change in Bridge Priority needs to be 
     * taken care of by reconsidering it while forming new Topology */
    if (AST_IS_PVRST_ENABLED ())
    {
        /* Traverse through all Ports on the bridge */
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

            /* If this Port is a member of this Instance then reconsider 
             * this port for Topology formation */
            if (pPerStPortInfo != NULL)
            {
                pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                pPerStRstPortInfo->bSelected = PVRST_FALSE;
                pPerStRstPortInfo->bReSelect = PVRST_TRUE;
            }                    /* End of If Loop */
        }                        /* End of For Loop */
        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP:Inst %d: ...Recomputing roles..\n", VlanId);
        if (PvrstPortRoleSelectionMachine ((UINT1) PVRST_PROLESELSM_EV_RESELECT,
                                           VlanId) != PVRST_SUCCESS)
        {
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: PvrstPortRoleSelectionMachine function returned"
                     "FAILURE!\n");
            return PVRST_FAILURE;
        }                        /* End of If Loop */
    }                            /* End of If Loop */
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstSetHelloTime                                    */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Hello Time interval is set through management and    */
/*                      the corresponding event is generated.                */
/*                                                                           */
/* Input(s)           : VlanId - The Vlan Id of the Spanning Tree Instance   */
/*                      u2HelloTime - The hello time value to be set         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PvrstSetHelloTime (tVlanId VlanId, UINT2 u2HelloTime)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return;
    }
    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    /* If Hello Time for this Instance is already updated then return */
    if (pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime == u2HelloTime)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in HelloTime value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in HelloTime value\n");
        return;
    }
    pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime = u2HelloTime;
    pBrgInfo->u1MigrateTime = (UINT1) ((u2HelloTime / AST_CENTI_SECONDS) +
                                       ((u2HelloTime / 2) / AST_CENTI_SECONDS));
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Instance %u: Set Hello Time as :%u...\n",
                  VlanId, u2HelloTime);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Instance %u: Set Hello Time as :%u...\n",
                  VlanId, u2HelloTime);

    if (AST_IS_PVRST_ENABLED ())
    {
        /* Updating the Root Times, only if this Bridge is the Root Bridge */
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            pPerStPvrstBridgeInfo->RootTimes.u2HelloTime =
                pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime;
        }
    }

    /* Updating the Desg Times and Port Times if this Bridge 
     * is the Root Bridge and when working in RSTP/STP compatible
     * mode */

    if (AST_IS_PVRST_ENABLED () && !(AST_FORCE_VERSION == AST_VERSION_3))
    {
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                /* If this port isn't a member of this instance then move
                 * onto next Instance */
                if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
                {
                    continue;
                }
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                         u2InstIndex);

                pPerStPvrstRstPortInfo->DesgPvrstTimes.u2HelloTime =
                    pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime =
                    pPerStPvrstBridgeInfo->BridgeTimes.u2HelloTime;
            }                    /* End of For Loop */
        }                        /* End of this Bridge being the Root Bridge */
    }
}

/*****************************************************************************/
/* Function Name      : PvrstSetTxHoldCount                                  */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      Transmit Hold Count value is set through management  */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u1TxHoldCount - The value of the Transmit Hold Count */
/*                                      to be set                            */
/*            VlanId - The Vlan Id of the Spanning Tree Instance   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstSetTxHoldCount (UINT1 u1TxHoldCount, tVlanId VlanId)
{
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    /* Get the Index of Array for this Instance to which it is mapped */
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return;
    }

    if (NULL == PVRST_GET_PERST_INFO (u2InstIndex))
    {
        return;
    }
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    /* Update TxHoldCount on Bridge level for this Instance */
    pPerStPvrstBridgeInfo->u1TxHoldCount = u1TxHoldCount;
    if (AST_IS_PVRST_STARTED ())
    {
        /* Traverse through all Ports to update TxCount to Init value */
        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {
            /* If this port is not a member of this Instance then move onto
             * the next Port */
            if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
            {
                continue;
            }
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
            pPerStPvrstRstPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;
        }
    }
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Set Tx Hold Count as :%u...\n", u1TxHoldCount);
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Set Tx Hold Count as :%u...\n", u1TxHoldCount);

}

/*****************************************************************************/
/* Function Name      : PvrstSetForwardDelay                                 */
/*                                                                           */
/* Description        : This function is the event handler whenever the      */
/*                      forward delay time is set through management and the */
/*                      corresponding event is generated.                    */
/*                                                                           */
/* Input(s)           : VlanId - The Vlan Id of the Spanning Tree Instance   */
/*                      u2ForwardDelay - The forward delay value to be set   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PvrstSetForwardDelay (tVlanId VlanId, UINT2 u2ForwardDelay)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    /* Get the Index of Array for this Instance to which it is mapped */
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return;
    }

    if (NULL == PVRST_GET_PERST_INFO (u2InstIndex))
    {
        return;
    }
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    /* If Forward Delay at Bridge level is already updated then return */
    if (pPerStPvrstBridgeInfo->BridgeTimes.u2ForwardDelay == u2ForwardDelay)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in ForwardDelay value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in ForwardDelay value\n");
        return;
    }
    /* Update Forward Delay */
    pPerStPvrstBridgeInfo->BridgeTimes.u2ForwardDelay = u2ForwardDelay;

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Instance %u: Set Forward Delay as :%u...\n",
                  VlanId, u2ForwardDelay);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Instance %u: Set Forward Delay as :%u...\n",
                  VlanId, u2ForwardDelay);

    /* Updating the Root Times, Desg Times and Port Times if this Bridge 
     * is the Root Bridge */
    if (AST_IS_PVRST_ENABLED ())
    {
        if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
        {
            /* This Bridge is the Root Bridge */
            pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay =
                pPerStPvrstBridgeInfo->BridgeTimes.u2ForwardDelay;

            /* Traverse through all Ports and update Forward Delay if it is 
             * a member of this Instance */
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
                {
                    continue;
                }
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                         u2InstIndex);
                pPerStPvrstRstPortInfo->DesgPvrstTimes.u2ForwardDelay =
                    pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay =
                    pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay;
            }                    /* End of For Loop */
        }                        /* End of this Bridge being the Root Bridge */
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetPortPriority                       */
/*                                                                           */
/*    Description               : This function sets the Priority of the     */
/*                                Port for the given Instance.               */
/*                                                                           */
/*    Input(s)                  : u2PortNo     - Port Number of the Port.    */
/*                                VlanId - Vlan Instance Identifier          */
/*                                u1Priority   - Priority value of the Port. */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortPriority (tVlanId VlanId, UINT2 u2PortNo, UINT1 u1Priority)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    /* Get Index of the Array for this Instance */
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    /* Check if this Instance is not mapped to the array then return Failure */
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    /* If the Pointer for this Instance returns Null then return Failure */
    if ((PVRST_GET_PERST_INFO (u2InstIndex)) == NULL)
    {
        return PVRST_FAILURE;
    }

    AST_DBG_ARG3 (AST_MGMT_DBG,
                  "SNMP: Port %s: Inst %d: Setting port priority as :%u ...\n",
                  AST_GET_IFINDEX_STR (u2PortNo), VlanId, u1Priority);

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNo, u2InstIndex);

    /* If Port is not a member of this Instance then return Failure */
    if (pPerStPortInfo == NULL)
    {
        AST_DBG_ARG2 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                      "SNMP: Port %s: Inst %d: Port does not exist"
                      "in this instance context!\n",
                      AST_GET_IFINDEX_STR (u2PortNo), VlanId);
        return PVRST_FAILURE;
    }

    /* If the Port Priority is equal to the given then return Success */
    if (pPerStPortInfo->u1PortPriority == u1Priority)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port Priority\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port Priority\n");
        return PVRST_SUCCESS;
    }
    PVRST_SET_PORT_PRIORITY (u1Priority, pPerStPortInfo->u1PortPriority);

    /* If PVRST is enabled then reconsider this port for Topology formation */
    if (AST_IS_PVRST_ENABLED ())
    {
        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP:Inst %d: ...Recomputing roles..\n", VlanId);
        pPerStPortInfo->PerStRstPortInfo.bSelected = PVRST_FALSE;
        pPerStPortInfo->PerStRstPortInfo.bReSelect = PVRST_TRUE;

        if (PvrstPortRoleSelectionMachine ((UINT1) PVRST_PROLESELSM_EV_RESELECT,
                                           VlanId) != PVRST_SUCCESS)
        {
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: PvrstPortRoleSelectionMachine function"
                     "returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstSetBrgMaxAge                     */
/*                                                                           */
/*    Description               : This function sets the Max Age of the      */
/*                                Bridge for specific instance.              */
/*                                                                           */
/*    Input(s)                  : VlanId - Vlan Instance Identifier for      */
/*                                               which the Priority is going */
/*                                               to be set.                  */
/*                                u2MaxAge  - Max Age value of the Bridge.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetBrgMaxAge (tVlanId VlanId, UINT2 u2MaxAge)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBrgInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;

    /* Get Index of the Array for this Instance */
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    /* Check if this Instance is not mapped to the array then return Failure */
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    /* If the Pointer for this Instance returns Null then return Failure */
    if ((PVRST_GET_PERST_INFO (u2InstIndex)) == NULL)
    {
        return PVRST_FAILURE;
    }

    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBrgInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    /* If MaxAge at Bridge level is already updated then return Success */
    if (pPerStPvrstBrgInfo->BridgeTimes.u2MaxAge == u2MaxAge)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Max Age\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Max Age\n");
        return PVRST_SUCCESS;
    }
    /* Update Bridge Times Max Age */
    pPerStPvrstBrgInfo->BridgeTimes.u2MaxAge = u2MaxAge;

    if (AST_IS_PVRST_ENABLED ())
    {
        /* Updating Root Times, only if this Bridge is Root Bridge */
        if (pAstPerStBridgeInfo->u2RootPort == AST_INIT_VAL)
        {
            pPerStPvrstBrgInfo->RootTimes.u2MaxAge =
                pPerStPvrstBrgInfo->BridgeTimes.u2MaxAge;
        }
    }
    if (AST_IS_PVRST_ENABLED () && !(AST_FORCE_VERSION == AST_VERSION_3))
    {
        if (pAstPerStBridgeInfo->u2RootPort == AST_INIT_VAL)
        {
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                if (PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex) == NULL)
                {
                    continue;
                }
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                         u2InstIndex);

                /* Updating the Desg Times and Port Times if this Bridge 
                 * is the Root Bridge and when working in RSTP/STP compatible
                 * mode */
                pPerStPvrstRstPortInfo->DesgPvrstTimes.u2MaxAge =
                    pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge =
                    pPerStPvrstBrgInfo->RootTimes.u2MaxAge;
            }                    /* End of For Loop */
        }                        /* End of this Bridge being the Root Bridge */
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstChangePVID                           */
/*                                                                           */
/*    Description               : This function changes PVID on the given    */
/*                                port                                       */
/*                                                                           */
/*    Input(s)                  : u4PortNum - Port no. whose Native Vlan is  */
/*                                            changed                        */
/*                              OldVlanId - Previour PVID set for the port */
/*                      NewVlanId - New PVID set on the port       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstChangePVID (UINT4 u4PortNum, tVlanId OldVlanId, tVlanId NewVlanId)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType = VLAN_HYBRID_PORT;
    UINT1               u1OperStatus = AST_INIT_VAL;
    INT4                i4RetVal = PVRST_TRUE;
    INT4                i4Val = AST_INIT_VAL;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (gPvrstVlanIdList, 0, VLAN_MAX_COMMUNITY_VLANS + 1);

    u2LocalPortId = (UINT2) u4PortNum;
    pPortInfo = AST_GET_PORTENTRY (u2LocalPortId);
    if ((pPortInfo->bPVIDInconsistent == AST_TRUE))
    {
        pPortInfo->bPVIDInconsistent = AST_FALSE;
        if (PvrstStopTimer ((VOID *) pPortInfo, (UINT2) 0,
                            (UINT1) AST_TMR_TYPE_INC_RECOVERY) == PVRST_FAILURE)
        {
            return PVRST_FAILURE;
        }
       /**Recovering from Type inconsistancy : Port Un blocked**/
        PvrstPerStEnablePort (u2LocalPortId, NewVlanId, AST_EXT_PORT_UP);

    }

    AstL2IwfGetVlanPortType ((UINT2) (pPortInfo->u4IfIndex), &u1PortType);

    i4Val = AstSelectContext (AST_DEFAULT_CONTEXT);

    if (u1PortType == VLAN_TRUNK_PORT)
    {
        return PVRST_SUCCESS;
    }

    /* Get the VLAN type and the associated VLAN */
    L2PvlanMappingInfo.u4ContextId = AST_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;
    L2PvlanMappingInfo.InVlanId = NewVlanId;
    L2PvlanMappingInfo.pMappedVlans = gPvrstVlanIdList;
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if ((L2PvlanMappingInfo.pMappedVlans[0] != 0) &&
        (L2PvlanMappingInfo.u1VlanType != L2IWF_PRIMARY_VLAN))
    {
        NewVlanId = L2PvlanMappingInfo.pMappedVlans[0];
    }

    /* If Old or new PVID recieved is ZERO then its invalid VLAN ,
     * Map port to Default VLAN in the system*/
    if (OldVlanId == AST_INIT_VAL)
    {
        OldVlanId = AST_DEF_VLAN_ID ();
    }
    if (NewVlanId == AST_INIT_VAL)
    {
        NewVlanId = AST_DEF_VLAN_ID ();
    }

    i4RetVal = VlanCheckPortType (OldVlanId, (UINT2) u4PortNum);

    /*For Access Port, delete the instance running for OldVlanId.
     *But For Hybrid port, OldVlanId value should not be a default vlan
     *and it should be untagged member for that OldVlanIdi then delete
     *the instance*/
    if ((u1PortType == VLAN_ACCESS_PORT) ||
        ((u1PortType == VLAN_HYBRID_PORT) &&
         (OldVlanId != AST_DEF_VLAN_ID ()) && (i4RetVal == VLAN_FALSE)))
    {
        if (PvrstPerStPortDelete ((UINT2) u2LocalPortId, OldVlanId) !=
            PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SNMP: Unable to delete port = %d for"
                          "instance = %d in PvrstChangePVID!!!\n",
                          pPortInfo->u4IfIndex, OldVlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SNMP:  Unable to delete port = %d for"
                          "instance = %d in PvrstChangePVID!!!\n",
                          pPortInfo->u4IfIndex, OldVlanId);
            return PVRST_FAILURE;
        }
    }

    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), NewVlanId) == OSIX_TRUE)
    {
        if (PvrstPerStPortCreate ((UINT2) u2LocalPortId, NewVlanId) !=
            PVRST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SNMP: Unable to create port = %d for"
                          "instance = %d in PvrstChangePVID!!!\n",
                          pPortInfo->u4IfIndex, NewVlanId);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SNMP:  Unable to create port = %d for"
                          "instance = %d in PvrstChangePVID!!!\n",
                          pPortInfo->u4IfIndex, NewVlanId);
            return PVRST_FAILURE;
        }

        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    NewVlanId);
        AstL2IwfGetPortOperStatus (STP_MODULE, pPortInfo->u4IfIndex,
                                   &u1OperStatus);
        if ((pPortInfo->bPathCostInitialized == PVRST_FALSE) &&
            (u1OperStatus == AST_PORT_OPER_UP))
        {
            pPortInfo->u4PathCost =
                AstCalculatePathcost ((UINT2) u2LocalPortId);
            pPortInfo->bPathCostInitialized = PVRST_TRUE;
        }

        if ((PvrstPathcostConfiguredFlag ((UINT2) u2LocalPortId, u2InstIndex) ==
             PVRST_FALSE) && (pPortInfo->bPathCostInitialized == PVRST_TRUE))
        {
            pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (u2LocalPortId, u2InstIndex);
            /* Instance is getting newly created on this port
             * and the port is already oper up.
             * Calculate the path cost for the port */
            pPerStPvrstRstPortInfo->u4PathCost = pPortInfo->u4PathCost;
        }

        if ((u1OperStatus == (UINT1) AST_PORT_OPER_UP) &&
            (AST_MODULE_STATUS == PVRST_ENABLED))
        {
            PvrstReInitPvrstInstInfo ((UINT2) u2LocalPortId, NewVlanId);

            if (PvrstPortRoleSelectionMachine
                (PVRST_PROLESELSM_EV_BEGIN, NewVlanId) != PVRST_SUCCESS)
            {

                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SNMP: Role Selection Machine returned error"
                              "for instance %d !!!\n", NewVlanId);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SNMP: Role Selection Failure for Instance = %d"
                              "!!!\n", NewVlanId);

                return PVRST_FAILURE;
            }

            if (PvrstPerStEnablePort ((UINT2) u2LocalPortId, NewVlanId,
                                      AST_STP_PORT_UP) != PVRST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SNMP: Unable to Enable port = "
                              "%d for instance= %d in set pvrst status!!!\n",
                              pPortInfo->u4IfIndex, NewVlanId);
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SNMP:  Unable to enable port = %d for"
                              " instance = %d in set pvrst status!!!\n",
                              pPortInfo->u4IfIndex, NewVlanId);
                return PVRST_FAILURE;
            }
        }
    }
    UNUSED_PARAM (i4Val);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  PvrstSetPortLoopGuard                               */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      loopguard value is set through management.  It       */
/*                      sets/resets port state and role field for given port */
/*                      and takes action accoridingly.                       */
/*                      If loopGuard is set, then this port should not       */
/*                      become forwarding for any PVRST instance             */
/*                      If loopGuard is reset, then check that the port      */
/*                      can go to forwarding state                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port for which Guard to be set           */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Loop Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             PrevVlanId = RST_DEFAULT_INSTANCE;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
#if defined(RSTP_DEBUG)
    UINT1               aau1LoopGuardEffectiveStatus[2][10] = {
        "FALSE", "TRUE"
    };
#endif

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pAstPortEntry->bLoopGuard == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in LoopGuard  Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in LoopGuard Status\n");
        return PVRST_SUCCESS;
    }

    pAstPortEntry->bLoopGuard = bStatus;

    if (bStatus == PVRST_FALSE)
    {
        pAstPortEntry->bLoopInconsistent = PVRST_FALSE;
    }
    /* In case the port is a trunk port, then loop-guard status must 
     * be set in each of the Per VLAN instance*/
    while (L2IwfMiGetNextActiveVlan (AST_CURR_CONTEXT_ID (), PrevVlanId,
                                     &VlanId) == L2IWF_SUCCESS)
    {
        u2InstIndex =
            AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);

        PrevVlanId = VlanId;
        if (u2InstIndex == 0)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo->bLoopGuardStatus = bStatus;

        if (bStatus == PVRST_FALSE)
        {
            if (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE)
            {
                if (PvrstPortInfoSmMakeAged (pPerStPortInfo, NULL) !=
                    PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %u: MakeAged returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             VlanId,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan :%d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2InstIndex, au1TimeStr);
                }
            }

            pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
        }

        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
        if (pPerStRstPortInfo == NULL)
        {
            continue;
        }

        /* In case of Designated Port set with Loop-guard,
         * no effect to be made*/

        if (((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) ||
             (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED))
            && (bStatus == PVRST_TRUE))
        {
            pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
            pPerStPortInfo->bLoopGuardStatus = PVRST_FALSE;
        }
        continue;

    }
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Loop Guard set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1LoopGuardEffectiveStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG, "SNMP: Port %s: Loop Guard set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1LoopGuardEffectiveStatus[bStatus]);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  PvrstSetAutoEdgePort                                */
/*                                                                           */
/* Description        : This function triggers BRIDGE DETECTION STATE MACHINE*/
/*                      with event PVRST_BRGDETSM_EV_AUTOEDGE_SET if bStatus */
/*                      is PVRST_TRUE                                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port for which Auto Edge to set          */
/*                      bStatus - A Boolean value indicating AutoEdge to     */
/*                      be enabled or not                                    */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }

    pAstPortEntry->bAutoEdge = bStatus;

    if (bStatus == PVRST_TRUE)
    {
        /*Indicating Bridge Detection State Machine with Event
           PVRST_BRGDETSM_EV_AUTOEDGE_SET */
        if (PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_AUTOEDGE_SET, u2PortNum)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SNMP: Bridge Detection State Machine returned Error"
                          "for u2PortNum %d !!!\n", u2PortNum);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

#endif /* PVRST_WANTED */
