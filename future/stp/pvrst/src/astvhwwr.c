/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvhwwr.c,v 1.2 2012/08/08 10:36:23 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              PVRST Module. 
 *
 *****************************************************************************/

#ifndef _ASTVHWWR_C_
#define _ASTVHWWR_C_
#include "asthdrs.h"
#include "astvinc.h"

#include "pvrstminp.h"
#include "npapi.h"

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstHwSetVlanPortState                    */
/*                                                                           */
/*    Description               : This function takes care of updating the   */
/*                                port state in hardware for the given port  */
/*                                and vlan. If the given vlan is of primary  */
/*                                vlan, the this fucntion will update the    */
/*                                port state in primary as well for the      */
/*                                associated secondary vlans in hardware.    */
/*    Input(s)                 :                                             */
/*                                ContextId - Virtual Switch ID              */
/*                                u4IfIndex - Interface index of whose       */
/*                                            Vlan state is to be updated    */
/*                                VlanId    - Vlan ID.                       */
/*                                u1PortState - Value of Port State.         */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstHwSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                         tVlanId VlanId, UINT1 u1PortState)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2VlanCount = 0;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (gPvrstVlanIdList, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));

    /* Get the VLAN type */

    L2PvlanMappingInfo.u4ContextId = u4ContextId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;
    L2PvlanMappingInfo.InVlanId = VlanId;
    L2PvlanMappingInfo.pMappedVlans = gPvrstVlanIdList;

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* program NP for the primary VLAN */
    if (PvrstFsMiPvrstNpSetVlanPortState (u4ContextId, u4IfIndex,
                                     VlanId, u1PortState) == FNP_FAILURE)
    {
        return PVRST_FAILURE;
    }

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_PRIMARY_VLAN)
    {
        /* program NP for all the secondary VLAN */

        for (u2VlanCount = 0; u2VlanCount < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanCount++)
        {
            if (PvrstFsMiPvrstNpSetVlanPortState (u4ContextId, u4IfIndex,
                                             L2PvlanMappingInfo.
                                             pMappedVlans[u2VlanCount],
                                             u1PortState) == FNP_FAILURE)
            {
                return PVRST_FAILURE;
            }
        }
    }

    return PVRST_SUCCESS;
}
#endif

#endif
