/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvred.c,v 1.8 2014/11/23 09:32:51 siva Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for PVRST module.   
 *
 *******************************************************************/

#ifdef L2RED_WANTED
#include "asthdrs.h"
#include "astvinc.h"
#include "cli.h"

/*****************************************************************************/
/* Function Name      : :PvrstRedHandlePvrstPdus                             */
/*                                                                           */
/* Description        : Stores the latest received PVRST PDUs in Standby     */
/* Input(s)           : pData - Pointer to the Received Data                 */
/*                      pu4Offset - Offset from which current data starts    */
/*                      u2Totallen - Length of data to process               */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstRedHandlePvrstPdus (VOID *pData, UINT4 *pu4Offset, UINT2 u2TotalLen)
{
    tAstRedPdu          RedPvrstPdu;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT2               u2ProtocolPort = AST_INIT_VAL;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (u2TotalLen != RED_AST_PVRST_BPDU_LEN)
    {
        *pu4Offset = *pu4Offset + u2TotalLen;
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC
                 | AST_RED_DBG, "PvrstRedHandlePvrstPdus: Incorrect Len !\n");
        ASSERT ();
        return PVRST_FAILURE;
    }

    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                u2InstanceId);
    if (u2InstIndex == AST_INIT_VAL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: "
                      "Vlan instance mapping is not present for instance %d\n",
                      u2InstanceId);
        return PVRST_FAILURE;
    }

    /* Store the Data Buffer */
    AST_MEMSET (AST_RED_PVRST_PDU_DATA (RedPvrstPdu), AST_INIT_VAL,
                sizeof (tPvrstBpdu));

    AST_RM_GET_N_BYTE (pData, AST_RED_PVRST_PDU_DATA (RedPvrstPdu), pu4Offset,
                       sizeof (tPvrstBpdu));

    if ((u2InstanceId > VLAN_MAX_VLAN_ID) || (u2LocalPort > AST_MAX_NUM_PORTS))
    {
        ASSERT ();
        AST_DBG_ARG2 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: "
                      "Instance id %d or local port %d invalid \n",
                      u2InstanceId, u2LocalPort);
        return PVRST_FAILURE;
    }

    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return PVRST_FAILURE;
    }

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: Port Down "
                      "for port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
        return PVRST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                    u2ProtocolPort,
                                                    &u2LocalPort);

        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: "
                          "PB C-VLAN selection failed for"
                          " port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
            return PVRST_FAILURE;
        }
    }

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: "
                      "ASTP Perst info is not present for instance %d\n",
                      u2InstIndex);
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_PORT_INFO (u2LocalPort, u2InstIndex) == NULL)
    {
        AST_DBG_ARG2 (AST_RED_DBG, "PvrstRedHandlePvrstPdus: "
                      "ASTP Perst port info is not present for "
                      "port %s instance %d\n",
                      AST_GET_IFINDEX_STR (u2LocalPort), u2InstIndex);
        return PVRST_FAILURE;
    }

    if (AST_IS_PVRST_ENABLED ())
    {
        /* Store PvrstPdu in Standby */
        PvrstRedStorePduInStandby (u2LocalPort,
                                   AST_RED_PVRST_PDU_DATA (RedPvrstPdu),
                                   sizeof (tPvrstBpdu));

        if (PvrstHandleInBpdu (AST_RED_PVRST_PDU_DATA (RedPvrstPdu),
                               u2LocalPort) != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                          AST_RED_DBG,
                          "PvrstRedHandlePvrstPdus: Handling Pvrst Bpdu "
                          "failed for port %s\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            return PVRST_FAILURE;
        }
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedStorePduInStandby                            */
/*                                                                           */
/* Description        : Stores the latest PDUs in Standby                    */
/* Input(s)           : u2LocalPort - Local potindex                         */
/*                    : pData - Pointer to BPDU Data                         */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstRedStorePduInStandby (UINT2 u2LocalPort, VOID *pData, UINT2 u2Len)
{
    tAstPvrstRedPdu    *pPdu = NULL;

    if (u2Len > AST_RED_DATA_MEMBLK_SIZE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Incorrect Len !\n");
        ASSERT ();
        return PVRST_FAILURE;
    }
    pPdu = AST_RED_PVRST_PDU_PTR (u2LocalPort);
    if (NULL == pPdu)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID,
                                 pPdu, tAstPvrstRedPdu);
        if (pPdu == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Message Memory Block Allocation FAILED!\n");
            return PVRST_FAILURE;
        }
        AST_RED_PVRST_PDU_PTR (u2LocalPort) = pPdu;
    }
    AST_MEMSET (pPdu, AST_INIT_VAL, AST_RED_DATA_MEMBLK_SIZE);
    /* Store the Data Buffer */
    AST_MEMCPY (pPdu, pData, u2Len);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedClearPduOnActive                             */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           :                                                      */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstRedClearPduOnActive (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (u2PortNum > AST_MAX_NUM_PORTS)
    {
        ASSERT ();
        return PVRST_FAILURE;
    }

    if (NULL != (pPortEntry = AST_GET_PORTENTRY (u2PortNum)))
    {
        if (NULL != AST_RED_PVRST_PDU_PTR (u2PortNum))
        {
            AST_FREE_RED_MEM_BLOCK (AST_RED_PVRST_PDU_MEMPOOL_ID,
                                    AST_RED_PVRST_PDU_PTR (u2PortNum));
            AST_RED_PVRST_PDU_PTR (u2PortNum) = NULL;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstRedClearAllBpdusOnActive              */
/*                                                                           */
/*    Description               : This function clears all the stored BPDU   */
/*                                information on ACTIVE node, called when the*/
/*                                protocol is disabled or Shutdown on Active.*/
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstRedClearAllBpdusOnActive ()
{
    UINT2               u2PortNum = AST_INIT_VAL;
    for (u2PortNum = 1; u2PortNum < AST_MAX_NUM_PORTS; u2PortNum++)

    {
        PvrstRedClearPduOnActive (u2PortNum);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstRedClearAllSyncUpDataInInst           */
/*                                                                           */
/*    Description               : This function clears all the stored Sync up*/
/*                                information on Standby node for an PVRST   */
/*                                instance, called when an Instance becomes  */
/*                                inactive on Active.                        */
/*                                                                           */
/*    Input(s)                  : u2PvrstInst - PVRST Instance identifier    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstRedClearAllSyncUpDataInInst (UINT2 u2PvrstInst)
{
    AST_UNUSED (u2PvrstInst);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedProtocolRestart                              */
/*                                                                           */
/* Description        : Restart PVRST protocol on a particular Port & Instance*/
/* Input(s)           : u2Port - Port                                        */
/*                      u2InstanceId - PVRST Instance identifier             */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstRedProtocolRestart (UINT2 u2Port, UINT2 u2InstanceId)
{
    if (PvrstPerStDisablePort (u2Port, u2InstanceId, AST_STP_PORT_DOWN)
        != PVRST_SUCCESS)

    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Disable Port Failed \n");
        return PVRST_FAILURE;
    }
    if (PvrstPerStEnablePort (u2Port, u2InstanceId, AST_STP_PORT_UP)
        != PVRST_SUCCESS)

    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Enabling Port Failed \n");
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedSyncUpPdu                                    */
/*                                                                           */
/* Description        : Restart PVRST protocol on a particular Port & Instance*/
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/*                      u2InstanceId - Instance Identifier                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstRedSyncUpPdu (UINT2 u2PortNum, tPvrstBpdu * pData, UINT2 u2Len)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        return PVRST_SUCCESS;
    }

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if (NULL == (pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex)))
        {
            continue;
        }
        if ((u2InstanceId = PVRST_VLAN_TO_INDEX_MAP (u2InstIndex)) ==
            AST_INIT_VAL)
        {
            continue;
        }

        /* Check the Sync Flag for all the instances before 
         * storing the BPDU */

        if (OSIX_TRUE == AST_RED_SYNC_FLAG (u2InstIndex))
        {
            if (pData != NULL)
            {
                PvrstRedStorePduInStandby (u2PortNum, pData, u2Len);
            }

            AstRedSendSyncMessages (u2InstanceId, u2PortNum, RED_PVRST_PDU, 0);

            AST_RED_RESET_SYNC_FLAG (u2InstIndex);
        }
    }                            /* End of for u2InstanceId */
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedGetNextTimer                                 */
/*                                                                           */
/* Description        : Get the next timer to be applied and the duration    */
/*                      for which it has to be run                           */
/*                                                                           */
/* Input(s)           : u2InstanceId - Instance Identifier                   */
/*                      u2PortIfIndex - Port Index                           */
/*                      u1TimerType - Type of the Timer                      */
/*                                                                           */
/* Output(s)          : pu1TimerType - Pointer to the Timer Structure        */
/*                      pu2Duration - Duration of Timer                      */
/*                      ppPortPtr - Pointer to the corresponding Port entry  */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstRedGetNextTimer (UINT2 u2InstanceId, UINT2 u2PortIfIndex,
                      UINT1 u1TimerType, UINT1 *pu1TimerType,
                      UINT2 *pu2Duration, tAstTimer ** ppPortPtr)
{
    tAstBridgeEntry    *pBrgEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstRedTimes       *pRedTimes = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tOsixSysTime        CurrTime = AST_INIT_VAL;
    tOsixSysTime        StoredTime = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstTimer          *pAstTimer = NULL;
    UINT4               u4MaxTime = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2RetVal = 0;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                u2InstanceId);
    if (u2InstIndex == AST_INIT_VAL)
    {
        return PVRST_FAILURE;
    }

    pRedTimes = AST_RED_TIMES_PTR (u2InstIndex, u2PortIfIndex);

    if (NULL == pRedTimes)
        return PVRST_FAILURE;

    AST_GET_TIME_STAMP (&CurrTime);
    switch (u1TimerType)
    {
        case AST_TMR_TYPE_FDWHILE:
            *pu1TimerType = AST_TMR_TYPE_RBWHILE;
            StoredTime = pRedTimes->u4FdWhileExpTime;
            pPerStPvrstBridgeInfo =
                PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
            u4MaxTime = pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex))->
                pFdWhileTmr;
            break;

        case AST_TMR_TYPE_RBWHILE:
            *pu1TimerType = AST_TMR_TYPE_RCVDINFOWHILE;
            StoredTime = pRedTimes->u4RbWhileExpTime;
            pPerStPvrstBridgeInfo =
                PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
            u4MaxTime = 2 * pPerStPvrstBridgeInfo->RootTimes.u2HelloTime;
            *ppPortPtr =
                (PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex))->
                pRbWhileTmr;
            break;

        case AST_TMR_TYPE_RCVDINFOWHILE:
            *pu1TimerType = AST_TMR_TYPE_RRWHILE;
            StoredTime = pRedTimes->u4RcvdInfoWhileExpTime;
            pPerStPvrstBridgeInfo =
                PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
            u4MaxTime = (pPerStPvrstBridgeInfo->RootTimes.u2HelloTime);
            *ppPortPtr =
                (PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex))->
                pRcvdInfoTmr;
            break;

        case AST_TMR_TYPE_RRWHILE:
            *pu1TimerType = AST_TMR_TYPE_TCWHILE;
            StoredTime = pRedTimes->u4RrWhileExpTime;
            pPerStPvrstBridgeInfo =
                PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
            u4MaxTime = pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex))->
                pRrWhileTmr;
            break;

        case AST_TMR_TYPE_TCWHILE:
            *pu1TimerType = AST_TMR_TYPE_MDELAYWHILE;
            StoredTime = pRedTimes->u4TcWhileExpTime;
            pPerStPvrstBridgeInfo =
                PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
            u4MaxTime = pPerStPvrstBridgeInfo->RootTimes.u2ForwardDelay;
            *ppPortPtr =
                (PVRST_GET_PERST_RST_PORT_INFO (u2PortIfIndex, u2InstIndex))->
                pTcWhileTmr;
            break;

        case AST_TMR_TYPE_MDELAYWHILE:
            *pu1TimerType = AST_TMR_TYPE_EDGEDELAYWHILE;
            StoredTime = pRedTimes->u4MdelayWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = (UINT4) pBrgEntry->u1MigrateTime;
            *ppPortPtr =
                (PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortIfIndex,
                                                      u2InstIndex))->
                pMdWhilePvrstTmr;
            break;

        case AST_TMR_TYPE_EDGEDELAYWHILE:
            *pu1TimerType = AST_TMR_TYPE_HOLD;
            StoredTime = pRedTimes->u4EdgeDelayWhileExpTime;
            pBrgEntry = AST_GET_BRGENTRY ();
            u4MaxTime = (UINT4) pBrgEntry->u1MigrateTime;
            pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortIfIndex);
            *ppPortPtr = pCommPortInfo->pEdgeDelayWhileTmr;
            break;

        case AST_TMR_TYPE_HOLD:
            *pu1TimerType = AST_TMR_TYPE_HELLOWHEN;

            /* Release the allocated memory for Hold Timer.
             * Hold Timer will start in Tx State Machine */
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortIfIndex,
                                                     u2InstIndex);
            if (pPerStPvrstRstPortInfo->pHoldTmr != NULL)
            {
                if (pPerStPvrstRstPortInfo->pHoldTmr->u1IsTmrStarted ==
                    RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER
                        (AST_TMR_LIST_ID,
                         &(pPerStPvrstRstPortInfo->pHoldTmr->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Hold Timer FAILED!\n");
                    }
                    pPerStPvrstRstPortInfo->pHoldTmr->u1IsTmrStarted =
                        RST_FALSE;
                }
                if (AST_RELEASE_TMR_MEM_BLOCK (pPerStPvrstRstPortInfo->pHoldTmr)
                    != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Hold Timer Memory Release FAILED!\n");
                }
                pPerStPvrstRstPortInfo->pHoldTmr = NULL;
            }
            *pu2Duration = 0;
            *ppPortPtr = pPerStPvrstRstPortInfo->pHoldTmr;
            return PVRST_SUCCESS;

        case AST_TMR_TYPE_HELLOWHEN:
            *pu1TimerType = AST_RED_TIMER_TYPE_INVALID;

            /* Release the allocated memory for Hello Timer.
             * Hello Timer will start in Tx State Machine */
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortIfIndex,
                                                     u2InstIndex);
            if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
            {
                if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr->
                    u1IsTmrStarted == RST_TRUE)
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pPerStPvrstRstPortInfo->
                                          pHelloWhenPvrstTmr->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping Hello Timer FAILED!\n");
                    }
                    pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr->
                        u1IsTmrStarted = RST_FALSE;
                }

                if (AST_RELEASE_TMR_MEM_BLOCK (pPerStPvrstRstPortInfo->
                                               pHelloWhenPvrstTmr)
                    != AST_MEM_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Hello Timer Memory Release FAILED!\n");
                }
                pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr = NULL;
            }
            *pu2Duration = 0;
            *ppPortPtr = pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr;

            /* Send PVRST_PTXSM_EV_BEGIN Event To PortTx state machine to 
             * start sending Hello Bpdus */
            pPortEntry = AST_GET_PORTENTRY (u2PortIfIndex);
            if (pPortEntry != NULL)
            {
                pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortIfIndex,
                                                            u2InstIndex);
                pPerStPvrstRstPortInfo->u1PortTxSmState =
                    (UINT1) PVRST_PTXSM_STATE_TRANSMIT_INIT;
                PvrstPortTransmitMachine (PVRST_PTXSM_EV_BEGIN, pPerStPortInfo);
            }
            return PVRST_SUCCESS;

        case AST_RED_TIMER_TYPE_INVALID:

            if (NULL != pRedTimes)
            {
                AST_FREE_RED_MEM_BLOCK (AST_RED_TIMES_MEMPOOL_ID, pRedTimes);
                AST_RED_TIMES_PTR (u2InstIndex, u2PortIfIndex) = NULL;
            }
            return PVRST_FAILURE;

            break;

        default:
            break;
    }

    if (0 == StoredTime)
    {
        *pu2Duration = 0;
    }
    else
    {
        if (StoredTime > CurrTime)
        {
            /* Expected Expiry Time - current Time 
               will give the duration for which the timer 
               has to be run in standby Node */
            *pu2Duration = (UINT2) (StoredTime - CurrTime);
            *pu2Duration =
                (UINT2) (*pu2Duration % (u4MaxTime * SYS_TIME_TICKS_IN_A_SEC));
        }
        else
        {
            /* This can happen if the timer Stop/Expiry packet 
               has been Missed */
            *pu2Duration = 0;

            /* This case comes only when FD_While expiry has been missed 
             * during SyncUp. FD_While Timer needs to be started in this case. 
             * So send an expiry event to State Machine to restart the timer. */

            if (u1TimerType == AST_TMR_TYPE_FDWHILE)
            {
                if ((pAstTimer != NULL)
                    && (pAstTimer->u1IsTmrStarted == RST_TRUE))
                {
                    /* Stop the Timer */
                    if (AST_STOP_TIMER (AST_TMR_LIST_ID,
                                        &(pAstTimer->AstAppTimer))
                        == AST_TMR_FAILURE)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Stopping FD while Timer FAILED!\n");
                    }
                    pAstTimer->u1IsTmrStarted = RST_FALSE;
                }

                pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortIfIndex,
                                                            u2InstIndex);
                 
                if (pPerStPortInfo != NULL)
                { 
                  pAstTimer = pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr;
                  pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;

                 if (AST_IS_PVRST_ENABLED ())
                 {
                    u2RetVal = (INT4)PvrstPortRoleTrMachine ((UINT2)
                                            PVRST_PROLETRSM_EV_FDWHILE_EXP,
                                            pPerStPortInfo);
                 }
                }
                if (pAstTimer != NULL)
                {
                    if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) !=
                        AST_MEM_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: Expired Timer Memory Block Release FAILED!\n");
                    }
                }
            }
        }
    }
    UNUSED_PARAM (u2RetVal);
    return PVRST_SUCCESS;
}

#endif /*  */
