/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvtrap.c,v 1.18 2017/11/30 06:29:20 siva Exp $
 *
 * Description: This file contains routines for handling       
 *              different traps 
 *
 *****************************************************************************/

#include "asthdrs.h"
#include "astvinc.h"
#include "snmctdfs.h"
#include "snmcport.h"
#ifdef SNMP_2_WANTED
#include "fspvst.h"
#endif

#ifdef SNMP_2_WANTED
static INT1         ai1TempBuffer[SNMP_MAX_OID_LENGTH + 1];
#endif

/*****************************************************************************/
/* Function Name      : AstPvrstInstUpTrap                                   */
/*                                                                           */
/* Description        : This routine generates trap message when an MSTP     */
/*                      instance is enabled                                  */
/*                                                                           */
/* Input(s)           : u2InstId - Instance Identifier                       */
/*                      pi1TrapsOid - OID of associated Trap Object.         */
/*                      u1TrapOidLen - Length of the Trap OID.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstPvrstInstUpTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgGenTraps     astBrgGenTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType = AST_UP_TRAP;

    AST_MEMCPY (&(astBrgGenTraps.BrgAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    astBrgGenTraps.u1GenTrapType = PVRST_INST_UP_TRAP;
    astBrgGenTraps.u2TrapMsgVal = u2InstId;

    nmhGetFsPvrstInstInstanceUpCount ((INT4) u2InstId,
                                      &(astBrgGenTraps.u4InstUpCount));
    nmhGetFsPvrstInstInstanceDownCount ((INT4) u2InstId,
                                        &(astBrgGenTraps.u4InstDownCount));

    AstSnmpIfSendTrap (AST_BRG_GEN_TRAP_VAL, pi1TrapsOid,
                       u1TrapOidLen, (VOID *) &astBrgGenTraps);
}

/*****************************************************************************/
/* Function Name      : AstPvrstInstDownTrap                                 */
/*                                                                           */
/* Description        : This routine generates trap message when an MSTP     */
/*                      instance is disabled                                 */
/*                                                                           */
/* Input(s)           : u2InstId - Instance Identifier                       */
/*                      pi1TrapsOid - OID of associated Trap Object.         */
/*                      u1TrapOidLen - Length of the Trap OID.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstPvrstInstDownTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgGenTraps     astBrgGenTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType = AST_DOWN_TRAP;

    AST_MEMCPY (&(astBrgGenTraps.BrgAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    astBrgGenTraps.u1GenTrapType = PVRST_INST_DOWN_TRAP;
    astBrgGenTraps.u2TrapMsgVal = u2InstId;

    nmhGetFsPvrstInstInstanceUpCount ((INT4) u2InstId,
                                      &(astBrgGenTraps.u4InstUpCount));
    nmhGetFsPvrstInstInstanceDownCount ((INT4) u2InstId,
                                        &(astBrgGenTraps.u4InstDownCount));
    AstSnmpIfSendTrap (AST_BRG_GEN_TRAP_VAL, pi1TrapsOid,
                       u1TrapOidLen, (VOID *) &astBrgGenTraps);
}

/*****************************************************************************/
/* Function Name      : AstPvrstProtocolMigrationTrap                        */
/*                                                                           */
/* Description        : This routine generates trap message when protocol    */
/*                      migration occurs                                     */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Instance Identifier               */
/*                    u2PortNum -port number                               */
/*                      u1Version - curremt STP Version                      */
/*                      u1SendVersion - sending STP version                  */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstPvrstProtocolMigrationTrap (tVlanId VlanId, UINT2 u2PortNum,
                               UINT1 u1Version, UINT1 u1SendVersion,
                               INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tPvrstBrgProtocolMigrationTrap PvrstBrgProtocolMigrationTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_ERR_EVNT ())
    {
        AST_MEMCPY (&(PvrstBrgProtocolMigrationTrap.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        PvrstBrgProtocolMigrationTrap.u2InstanceId = VlanId;
        PvrstBrgProtocolMigrationTrap.u2Port = u2PortNum;
        PvrstBrgProtocolMigrationTrap.u1Version = u1Version;
        PvrstBrgProtocolMigrationTrap.u1GenTrapType = u1SendVersion;
        AstSnmpIfSendTrap (AST_PROTOCOL_MIGRATION_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen,
                           (VOID *) &PvrstBrgProtocolMigrationTrap);
    }
    (AST_GET_PORTENTRY (u2PortNum))->u1MigrationType = u1SendVersion;
}

/* 
 * 1) PvrstGlobalMemFailTrap () can be called from outside the protocol context
 *    without having selected a virtual-context - 
 *    For e.g. when allocation fails during a message post.
 * 2) The other Trap routines should be called only within the protocol 
 *    context after a AstSelectContext () call has been invoked.
 *
 */

/******************************************************************************
* Function : PvrstSnmpIfSendTrap 
* Input    : u1TrapId - Trap Identifier
*           pi1TrapOid -pointer to Trap OID
*           u1OidLen - OID Length 
*           pTrapInfo - void pointer to the trap information 
* Output   : None
* Returns  : None
*******************************************************************************/

VOID
PvrstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                     VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tAstBrgGenTraps    *pAstBrgGenTraps = NULL;
    tAstBrgErrTraps    *pAstBrgErrTraps = NULL;
    tAstBrgNewRootTrap *pAstBrgNewRootTrap = NULL;
    tAstBrgTopologyChgTrap *pAstBrgTopologyChgTrap = NULL;
    tPvrstBrgProtocolMigrationTrap *pAstBrgProtocolMigrationTrap = NULL;
    tAstBrgInvalidBpduRxdTrap *pAstBrgInvalidBpduRxdTrap = NULL;
    tAstNewPortRoleTrap *pAstNewPortRoleTrap = NULL;
    tAstBrgHwFailTrap  *pAstBrgHwFailTrap = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring, *pContextName = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[256];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4SysMode;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT4               u4TopChanges = AST_INIT_VAL;
    tAstBrgLoopIncStateChange *pAstBrgLoopIncStateChange = NULL;
    tAstBrgBPDUIncStateChange *pAstBrgBPDUIncStateChange = NULL;
    tAstBrgRootIncStateChange *pAstBrgRootIncStateChange = NULL;
    tAstBrgPortStateChange *pAstBrgPortStateChange = NULL;

    UNUSED_PARAM (u1OidLen);
    pEnterpriseOid = SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case AST_BRG_GEN_TRAP_VAL:

            pAstBrgGenTraps = (tAstBrgGenTraps *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgGenTraps->BrgAddr,
                                          (INT4) AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_GEN_TRAP_TYPE);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u1GenTrapType,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_INSTUP_COUNT);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgGenTraps->u2TrapMsgVal;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u4InstUpCount,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_INSTDOWN_COUNT);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgGenTraps->u2TrapMsgVal;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u4InstDownCount,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;

        case AST_BRG_ERR_TRAP_VAL:

            pAstBrgErrTraps = (tAstBrgErrTraps *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgErrTraps->BrgAddr,
                                          AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_ERR_TRAP_TYPE);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgErrTraps->
                                      u1ErrTrapType, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_ROOT_TRAP_VAL:

            pAstBrgNewRootTrap = (tAstBrgNewRootTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgNewRootTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_DESIG_ROOT);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Pvrst Instance Index */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgNewRootTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", PVRST_MIB_OBJ_OLD_DESIG_ROOT);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgNewRootTrap->u2MstInst;
                pOid->u4_Length++;
            }
            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstOldRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_PORTROLE_TRAP_VAL:

            pAstNewPortRoleTrap = (tAstNewPortRoleTrap *) pTrapInfo;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_BASE_BRIDGE_ADDR)),
                      PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstNewPortRoleTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_PREVIOUS_ROLE)),
                      PVRST_MIB_OBJ_PREVIOUS_ROLE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1PreviousRole, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_SELECTED_ROLE)),
                      PVRST_MIB_OBJ_SELECTED_ROLE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1SelectedRole, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_TOPOLOGY_CHG_TRAP_VAL:

            pAstBrgTopologyChgTrap = (tAstBrgTopologyChgTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgTopologyChgTrap->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_TRUE);

            nmhGetFsPvrstInstTopChanges ((INT4) pAstBrgTopologyChgTrap->
                                         u2MstInst, &u4TopChanges);
            pAstBrgTopologyChgTrap->u4TopChanges = u4TopChanges;

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_TOPOLOGY_CHGS);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    pAstBrgTopologyChgTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgTopologyChgTrap->
                                      u4TopChanges, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_PROTOCOL_MIGRATION_TRAP_VAL:

            pAstBrgProtocolMigrationTrap =
                (tPvrstBrgProtocolMigrationTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgProtocolMigrationTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_MIGRATION_TYPE);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    AST_GET_IFINDEX (pAstBrgProtocolMigrationTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgProtocolMigrationTrap->
                                      u1GenTrapType, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_INVALID_BPDU_RXD_TRAP_VAL:

            pAstBrgInvalidBpduRxdTrap = (tAstBrgInvalidBpduRxdTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgInvalidBpduRxdTrap->BrgAddr,
                                          AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                            0, pOstring, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_FALSE);

            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_PKT_ERR_TYPE);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u1ErrTrapType, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_PKT_ERR_VAL);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* The Object is tabular. Hence append the Index value
             * */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u2ErrVal, NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;
        case AST_HW_FAIL_TRAP_VAL:

            pAstBrgHwFailTrap = (tAstBrgHwFailTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgHwFailTrap->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, SnmpCounter64Type, AST_TRUE);
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_PORT_STATE)),
                      PVRST_MIB_OBJ_PORT_STATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgHwFailTrap->u2Port);
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4)
                    PVRST_VLAN_TO_INDEX_MAP (pAstBrgHwFailTrap->u2MstInst);

                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgHwFailTrap->
                                      u2PortState, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_LOOP_INCSTATE_CHANGE_TRAP_VAL:
            pAstBrgLoopIncStateChange = (tAstBrgLoopIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_LOOP_INCSTATE_CHANGE_TRAP_VAL;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_BASE_BRIDGE_ADDR)),
                      PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgLoopIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_LOOP_INCSTATE)),
                      PVRST_MIB_OBJ_LOOP_INCSTATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgLoopIncStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4)
                    PVRST_VLAN_TO_INDEX_MAP (pAstBrgLoopIncStateChange->
                                             u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgLoopIncStateChange->
                                      u4LoopInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;

        case AST_BPDU_INCSTATE_CHANGE_TRAP_VAL:

            pAstBrgBPDUIncStateChange = (tAstBrgBPDUIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_BPDU_INCSTATE_CHANGE_TRAP_VAL;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_BASE_BRIDGE_ADDR)),
                      PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgBPDUIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "%s", PVRST_MIB_OBJ_BPDU_INCSTATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgBPDUIncStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgBPDUIncStateChange->
                                      u4BPDUInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_ROOT_INCSTATE_CHANGE_TRAP_VAL:
            pAstBrgRootIncStateChange = (tAstBrgRootIncStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_ROOT_INCSTATE_CHANGE_TRAP_VAL;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_BASE_BRIDGE_ADDR)),
                      PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgRootIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_ROOT_INCSTATE)),
                      PVRST_MIB_OBJ_ROOT_INCSTATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgRootIncStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4)
                    PVRST_VLAN_TO_INDEX_MAP (pAstBrgRootIncStateChange->
                                             u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgRootIncStateChange->
                                      u4RootInconsistentState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_PORTSTATE_CHANGE_TRAP_VAL:

            pAstBrgPortStateChange = (tAstBrgPortStateChange *) pTrapInfo;
            u4SpecTrapType = (UINT4) RST_PORTSTATE_CHANGE_TRAP_VAL;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_BASE_BRIDGE_ADDR)),
                      PVRST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgPortStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_PORT_STATE)),
                      PVRST_MIB_OBJ_PORT_STATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2InstId;
                pOid->u4_Length++;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u2PortState, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SNPRINTF ((char *) au1Buf,
                      (256 - sizeof (PVRST_MIB_OBJ_OLD_PORTSTATE)),
                      PVRST_MIB_OBJ_OLD_PORTSTATE);

            pOid = PvrstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4)
                    PVRST_VLAN_TO_INDEX_MAP (pAstBrgPortStateChange->u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u4OldPortState, NULL, NULL,
                                      SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
    }

    u4SysMode = AstVcmGetSystemModeExt (AST_PROTOCOL_ID);
    if (u4SysMode == VCM_MI_MODE)
    {
        /* In case of SI, the context name pContextName should be NULL. */
        AstVcmGetAliasName (AST_CURR_CONTEXT_ID (), au1ContextName);
        pContextName =
            SNMP_AGT_FormOctetString (au1ContextName, (VCM_ALIAS_MAX_LEN - 1));
    }

/* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4SpecTrapType, pStartVb, pContextName);

    if (u4SysMode == VCM_MI_MODE)
    {
        SNMP_AGT_FreeOctetString (pContextName);
    }
#else /* SNMP_2_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_2_WANTED */

}

#ifdef SNMP_2_WANTED
/******************************************************************************
* Function : PvrstMakeObjIdFromDotNew                                         *
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
PvrstMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    UINT4               u4OidLen = 0;
    UINT4               u4Len = 0;

    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE)
    {
        porig_mib_oid_table = orig_mib_oid_table_SI;
        u4OidLen = sizeof (orig_mib_oid_table_SI);
    }
    else
    {
        porig_mib_oid_table = orig_mib_oid_table_MI;
        u4OidLen = sizeof (orig_mib_oid_table_MI);
    }

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; ((u2Index <
                            (u4OidLen /
                             sizeof (struct MIB_OID)))
                           && ((porig_mib_oid_table + u2Index)->pName != NULL));
             u2Index++)
        {
            if ((STRCMP
                 ((porig_mib_oid_table + u2Index)->pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN ((porig_mib_oid_table + u2Index)->pName)))
            {
                u4Len =
                    MEM_MAX_BYTES (STRLEN
                                   ((porig_mib_oid_table + u2Index)->pNumber),
                                   (sizeof (ai1TempBuffer) - 1));
                STRNCPY ((INT1 *) ai1TempBuffer,
                         (porig_mib_oid_table + u2Index)->pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }
        if (u2Index < (u4OidLen / sizeof (struct MIB_OID)))
        {
            if ((porig_mib_oid_table + u2Index)->pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 STRLEN ((INT1 *) pi1TextStr));
        ai1TempBuffer[STRLEN ((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         (u2Index <= SNMP_MAX_OID_LENGTH && (ai1TempBuffer[u2Index] != '\0'));
         u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }

    MEMSET (pOidPtr->pu4_OidList, 0, SNMP_MAX_OID_LENGTH);
    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; ((u2Index < SNMP_MAX_OID_LENGTH) &&
                       (u2Index < u2DotCount + 1)); u2Index++)
    {
        if ((pi1TempPtr =
             (PvrstParseSubIdNew (pi1TempPtr,
                                  &pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

#endif
/******************************************************************************
* Function : PvrstParseSubIdNew
* Input    : pi1TmpPtr
* Output   : None
* Returns  : i4Value of pi1TmpPtr or -1
*******************************************************************************/
INT1               *
PvrstParseSubIdNew (INT1 *pi1TmpPtr, UINT4 *pu4Value)
{
    INT1               *pi1Tmp;
    UINT4               u4Value = 0;

    for (pi1Tmp = pi1TmpPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                              ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                              ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F'))); pi1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pi1Tmp & 0xf);
    }

    if (pi1TmpPtr == pi1Tmp)
    {
        pi1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pi1Tmp;
}

/*************************** END OF FILE(astvtrap.c) ***********************/
