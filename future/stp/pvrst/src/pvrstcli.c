/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: pvrstcli.c,v 1.76 2017/12/01 13:24:42 siva Exp $
 *
 * Description: Function Definition for CLI PVRST Commands
 *
 *****************************************************************************/

#ifdef PVRST_WANTED
#include "asthdrs.h"
#include "astvinc.h"
#include "stpcli.h"
#include "stpclipt.h"
#ifdef MSTP_WANTED
#include "astmlow.h"
#endif
#include "astpbcli.h"
/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : PvrstCliSetSystemShut                             */
/*                                                                          */
/*     DESCRIPTION      : This function sets the system control to shut     */
/*                                                                          */
/*     INPUT            :  CliHandle- Handle to the Cli Context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
PvrstCliSetSystemShut (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPvrstSystemControl (&u4ErrCode, PVRST_SNMP_SHUTDOWN) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstSystemControl (PVRST_SNMP_SHUTDOWN) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "\rSpanning Tree is being shutdown \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetSystemCtrl                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the system control (start )     */
/*                         of the PVRST after Shutting down the RSTP         */
/*                          and  also enables the PVRST Module.              */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to CLI Context                    */
/*                        u4SystemControl- System Control Value              */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCliSetSystemCtrl (tCliHandle CliHandle)
{
    UINT1               u1RstStart = AST_FALSE;
    UINT1               u1MstStart = AST_FALSE;
    UINT4               u4ErrCode = 0;

    if (MrpApiIsMvrpEnabled (AST_CURR_CONTEXT_ID ()) == OSIX_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% MVRP Module is Enabled.It should be disabled before "
                   "enabling PVRST\r\n");
        return CLI_FAILURE;
    }
    else if (GvrpIsGvrpEnabledInContext (AST_CURR_CONTEXT_ID ()) == GVRP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% GVRP Module is Enabled.It should be disabled before "
                   "enabling PVRST\r\n");
        return CLI_FAILURE;
    }

    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_BRG_MODE_ERR);
        return CLI_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        u1RstStart = AST_TRUE;
        /* Shutting Down RSTP */

        if (nmhTestv2FsRstSystemControl (&u4ErrCode, RST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsRstSystemControl (RST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
#ifdef MSTP_WANTED
    if (AST_IS_MST_STARTED ())
    {
        u1MstStart = AST_TRUE;
        /* Shutting Down MSTP */
        if (nmhTestv2FsMstSystemControl (&u4ErrCode, MST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsMstSystemControl (MST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
#endif

    if (nmhTestv2FsPvrstSystemControl (&u4ErrCode, PVRST_SNMP_START) ==
        SNMP_FAILURE)
    {
        if (u1RstStart == AST_TRUE)
        {
            if (nmhSetFsRstSystemControl (RST_SNMP_START) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            RstCliSetModStatus (CliHandle, RST_ENABLED);
        }
#ifdef MSTP_WANTED
        else if (u1MstStart == AST_TRUE)
        {
            if (nmhSetFsMstSystemControl (MST_SNMP_START) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            MstCliSetModStatus (CliHandle, MST_ENABLED);
        }
#endif
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstSystemControl (PVRST_SNMP_START) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (AST_TRUE == u1RstStart)
    {
        CliPrintf (CliHandle,
                   "\rSpanning Tree enabled protocol is RSTP, now RSTP is being"
                   " shutdown \r\n");

    }
    if (AST_TRUE == u1MstStart)
    {
        CliPrintf (CliHandle,
                   "\rSpanning Tree enabled protocol is MSTP, now MSTP is being"
                   " shutdown \r\n");
    }
    CliPrintf (CliHandle, "\rPVRST is started.\r\n");

    /*Enable PVRST Module is started and enabled */

    if (PvrstCliSetModStatus (CliHandle, PVRST_ENABLED) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetModStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the module status (             */
/*                        Enabled/Disabled) of the PVRST Module.             */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                          u4PvrstModStatus--PVRST Module Status.             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliSetModStatus (tCliHandle CliHandle, UINT4 u4PvrstModStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPvrstModuleStatus (&u4ErrCode, u4PvrstModStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstModuleStatus (u4PvrstModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstResetCounters                                   */
/*                                                                           */
/* Description        : This function resets all bridge and port statistics  */
/*                      counters across all instances                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstResetCounters (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT2               u2InstIndex = (UINT2) AST_INIT_VAL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();

    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;

    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

        pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
        pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
        pPerStBrgInfo->u4TotalFlushCount = AST_INIT_VAL;

        MEMSET (AST_CURR_CONTEXT_INFO ()->pInstanceUpCount, AST_INIT_VAL,
                (AST_MAX_PVRST_INSTANCES * sizeof (UINT4)));

    }

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
        pPortInfo->u4NumRstRxdInfoWhileExpCount = AST_INIT_VAL;
        pPortInfo->u4NumRstImpossibleStateOcc = AST_INIT_VAL;

        for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
             u2InstIndex++)
        {
            pPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pAstPerStPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) u2InstIndex);
            if (NULL == pAstPerStPortInfo)
            {
                continue;
            }
            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                     (UINT2) u2InstIndex);
            pPerStPvrstRstPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
            pPerStPvrstRstPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
            pAstPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
            pAstPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
            pAstPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
            pAstPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4ProposalTxCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4ProposalRcvdCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4AgreementTxCount = AST_INIT_VAL;
            pAstPerStPortInfo->u4AgreementRcvdCount = AST_INIT_VAL;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetHelloTime                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        PVRST module                                       */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                            u4InstanceId - Identifier of Spanning Tree         */
/*                        u4HelloTime-HelloTime                              */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/
INT4
PvrstCliSetHelloTime (tCliHandle CliHandle, UINT4 u4InstanceId,
                      UINT4 u4HelloTime)
{
    UINT4               u4ErrCode = 0;

    u4HelloTime = AST_SYS_TO_MGMT (u4HelloTime);

    if (nmhTestv2FsPvrstInstBridgeHelloTime (&u4ErrCode, (INT4) u4InstanceId,
                                             (INT4) u4HelloTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstInstBridgeHelloTime ((INT4) u4InstanceId,
                                          (INT4) u4HelloTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\rHello Time for the given instance is set \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetForwardDelay                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Forward Time for            */
/*                        PVRST module                                       */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                          u4InstanceId - Identifier of Spanning Tree         */
/*                        u4FwdTime-ForwardDelay                             */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/
INT4
PvrstCliSetForwardDelay (tCliHandle CliHandle, UINT4 u4InstanceId,
                         UINT4 u4FwdTime)
{
    UINT4               u4ErrCode = 0;

    u4FwdTime = AST_SYS_TO_MGMT (u4FwdTime);

    if (nmhTestv2FsPvrstInstBridgeForwardDelay (&u4ErrCode, (INT4) u4InstanceId,
                                                (INT4) u4FwdTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstInstBridgeForwardDelay
        ((INT4) u4InstanceId, (INT4) u4FwdTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\rForward Time for the given instance is set \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetMaxAge                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Max Age for                 */
/*                        PVRST module                                       */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                          u4InstanceId - Identifier of Spanning Tree         */
/*                        u4MaxAge - Max Age                                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/
INT4
PvrstCliSetMaxAge (tCliHandle CliHandle, UINT4 u4InstanceId, UINT4 u4MaxAge)
{
    UINT4               u4ErrCode = 0;

    u4MaxAge = AST_SYS_TO_MGMT (u4MaxAge);

    if (nmhTestv2FsPvrstInstBridgeMaxAge (&u4ErrCode, (INT4) u4InstanceId,
                                          (INT4) u4MaxAge) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstInstBridgeMaxAge ((INT4) u4InstanceId, u4MaxAge) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\rMax Age for the given instance is set \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetHoldCount                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hold Count for              */
/*                        PVRST module                                       */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                      u4InstanceId - Identifier of Spanning Tree         */
/*                        u4HoldCount-Hold Count                             */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/
INT4
PvrstCliSetHoldCount (tCliHandle CliHandle, UINT4 u4InstanceId,
                      UINT4 u4HoldCount)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPvrstInstTxHoldCount (&u4ErrCode, (INT4) u4InstanceId,
                                         u4HoldCount) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstInstTxHoldCount ((INT4) u4InstanceId, u4HoldCount) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\rHold Count for the given instance is set \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetBrgPriority                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Bridge Priority for the     */
/*                        PVRST module.                                      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                          u4InstanceId - Identifier of Spanning Tree         */
/*                        u4BrgPriority-BridgePriority                       */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCliSetBrgPriority (tCliHandle CliHandle, UINT4 u4InstanceId,
                        UINT4 u4Priority)
{
    tSNMP_OCTET_STRING_TYPE RootBridgeId;
    tAstMacAddr         MacAddress;
    UINT4               u4ErrCode = 0;
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];

    if ((INT4) u4Priority == CLI_STP_PVRST_ROOT_PRIMARY)
    {
        if (PvrstValidateInstanceEntry ((INT4) u4InstanceId) != PVRST_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "LW:Such a Instance Does not exist!\n");

            CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
            return CLI_FAILURE;
        }

                /** Get the Bridge Identifier of the current bridge **/
        AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
        RstGetBridgeAddr (&MacAddress);
        MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
        RootBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
        RootBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;
                /** Get the root bridge info **/
        nmhGetFsPvrstInstDesignatedRoot ((INT4) u4InstanceId, &RootBridgeId);
                /** If this bridge is root do nothing **/
        if (AST_MEMCMP ((RootBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                        MacAddress, AST_MAC_ADDR_SIZE) == 0)
        {
            return CLI_SUCCESS;
        }

        u4Priority = AstGetBrgPrioFromBrgId (RootBridgeId);
        u4Priority = (u4Priority - u4InstanceId) - 4096;
        if ((INT4) u4Priority < 0)
        {
            CLI_SET_ERR (CLI_STP_ROOT_PRIMARY_ERR);
            return CLI_FAILURE;
        }
    }
    else if ((INT4) u4Priority == CLI_STP_PVRST_ROOT_SECONDARY)
    {
        if (PvrstValidateInstanceEntry ((INT4) u4InstanceId) != PVRST_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "LW:Such a Instance Does not exist!\n");

            CLI_SET_ERR (CLI_STP_NO_VLAN_EXISTS_ERR);
            return CLI_FAILURE;

        }
        MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
        RootBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
        RootBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;
        /** Get the root bridge info **/
        nmhGetFsPvrstInstDesignatedRoot ((INT4) u4InstanceId, &RootBridgeId);
        u4Priority = AstGetBrgPrioFromBrgId (RootBridgeId);
        u4Priority = (u4Priority - u4InstanceId) + 4096;
        if ((INT4) u4Priority < 0)

        {
            CLI_SET_ERR (CLI_STP_ROOT_SECONDARY_ERR);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsPvrstInstBridgePriority (&u4ErrCode, (INT4) u4InstanceId,
                                            (INT4) u4Priority) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Bridge Priority must be in increments of 4096"
                   " and can be upto 61440\r\n");
        CliPrintf (CliHandle, "\r%% Allowed values are:\r\n"
                   "0     4096  8192  12288 16384 20480 24576 28672\r\n"
                   "32768 36864 40960 45056 49152 53248 57344 61440\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsPvrstInstBridgePriority ((INT4) u4InstanceId, (INT4) u4Priority)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);

        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowSpanningTreeDetail                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Information for         */
/*                        "Show Spanning Tree Detail" Command                */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliShowSpanningTreeDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4InstanceId, UINT4 u4Type)
{
    INT4                i4CurrentPort = (INT4) AST_INIT_VAL;
    INT4                i4NextPort = (INT4) AST_INIT_VAL;
    INT4                i4PortState = (INT4) AST_INIT_VAL;
    INT1                i1OutCome = (INT1) AST_INIT_VAL;
    UINT4               u4PagingStatus = (UINT4) CLI_SUCCESS;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    INT4                i4FlushInterval = AST_INIT_VAL;
    UINT4               u4FlushCount = AST_INIT_VAL;
    INT4                i4FlushThreshold = AST_INIT_VAL;

    /* Display the Bridge Info */
    PvrstCliDisplayBridgeDetails (CliHandle, u4ContextId, u4InstanceId);

    /* Get the Current Port from the Table */
    i1OutCome = nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort);
    while (i1OutCome != SNMP_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != AST_PORT_OPER_DOWN)
        {
            if (u4Type == CLI_STP_VLAN_ACTIVE)
            {
                nmhGetFsMIPvrstFlushInterval ((INT4) u4ContextId,
                                              &i4FlushInterval);
                nmhGetFsMIPvrstInstTotalFlushCount ((INT4) u4ContextId,
                                                    i4NextPort, &u4FlushCount);
                nmhGetFsMIPvrstInstFlushIndicationThreshold ((INT4) u4ContextId,
                                                             i4NextPort,
                                                             &i4FlushThreshold);
                CliPrintf (CliHandle, "Flush Interval %d centi-sec,",
                           i4FlushInterval);
                CliPrintf (CliHandle, "Flush Invocations %d \r\n",
                           u4FlushCount);
                CliPrintf (CliHandle, "Flush Indication threshold %d \r\n",
                           i4FlushThreshold);
                nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, i4NextPort,
                                              &i4PortState);
                if ((i4PortState == AST_PORT_STATE_LEARNING)
                    || (i4PortState == AST_PORT_STATE_FORWARDING))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                /* Display Port Details */
                PvrstCliDisplayPortDetails (CliHandle, u4ContextId,
                                            u4InstanceId, i4NextPort);

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }
            }
        }
        u1Flag = 0;
        /*Get the next Port from the Table */
        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                     &i4NextPort);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayBridgeDetails                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Bridge Details         */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
PvrstCliDisplayBridgeDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4InstanceId)
{

    tAstMacAddr         MacAddress;
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4RootFwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4RootHelloTime = (INT4) AST_INIT_VAL;
    INT4                i4RootMaxAge = (INT4) AST_INIT_VAL;
    INT4                i4ModStatus = (INT4) AST_INIT_VAL;
    INT4                i4Priority = (INT4) AST_INIT_VAL;
    INT4                i4HoldTime = (INT4) AST_INIT_VAL;
    UINT4               u4TopChange = (UINT4) AST_INIT_VAL;
    UINT4               u4TopTime = (UINT4) AST_INIT_VAL;
    INT4                i4Version = (INT4) AST_VERSION_0;
    INT4                i4DynamicPathCost = (INT4) AST_INIT_VAL;
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4FlushInterval = AST_INIT_VAL;
    INT4                i4FlushThreshold = AST_INIT_VAL;
    UINT4               u4FlushCount = AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    tSNMP_OCTET_STRING_TYPE RetBridgeId;

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /*Spanning Tree Bridge Version */

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    nmhGetFsMIPvrstModuleStatus ((INT4) u4ContextId, &i4ModStatus);
    if (i4ModStatus != PVRST_DISABLED)
    {
        i4Version = AST_VERSION_2;
    }

    nmhGetFsMIPvrstGlobalBpduGuard ((INT4) u4ContextId, &i4BpduGuard);
    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard enable\r\n");
    }

    /* Bridge Priority */

    nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId, (INT4) u4InstanceId,
                                       &i4Priority);

    /*Bridge Address */
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    RstGetBridgeAddr (&MacAddress);

    /*Hello Time */

    nmhGetFsMIPvrstInstBridgeHelloTime ((INT4) u4ContextId, (INT4) u4InstanceId,
                                        &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */

    nmhGetFsMIPvrstInstBridgeMaxAge ((INT4) u4ContextId, (INT4) u4InstanceId,
                                     &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsMIPvrstInstBridgeForwardDelay ((INT4) u4ContextId,
                                           (INT4) u4InstanceId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Dynamic Path Cost */

    nmhGetFsMIPvrstDynamicPathCostCalculation (u4ContextId, &i4DynamicPathCost);

    /* Flush Interval */

    nmhGetFsMIPvrstFlushInterval ((INT4) u4ContextId, &i4FlushInterval);
    nmhGetFsMIPvrstInstTotalFlushCount ((INT4) u4ContextId,
                                        (INT4) u4InstanceId, &u4FlushCount);
    nmhGetFsMIPvrstInstFlushIndicationThreshold ((INT4) u4ContextId,
                                                 (INT4) u4InstanceId,
                                                 &i4FlushThreshold);

    /* To Check  if the  Bridge is a ROOT Bridge */

    /* Get the RootBridge ID- Priority and MacAddress */

    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId, (INT4) u4InstanceId,
                                       &RetBridgeId);

    /* Topology Changes */

    nmhGetFsMIPvrstInstTopChanges ((INT4) u4ContextId, (INT4) u4InstanceId,
                                   &u4TopChange);

    /* Time Since Topology Changes */

    nmhGetFsMIPvrstInstTimeSinceTopologyChange ((INT4) u4ContextId,
                                                (INT4) u4InstanceId,
                                                &u4TopTime);
    u4TopTime = u4TopTime / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    u4TopTime = AST_MGMT_TO_SYS (u4TopTime);

    /*Hold Time */

    nmhGetFsMIPvrstInstTxHoldCount ((INT4) u4ContextId, (INT4) u4InstanceId,
                                    &i4HoldTime);
    /*Root Bridge Hello time */

    nmhGetFsMIPvrstInstRootHelloTime ((INT4) u4ContextId, (INT4) u4InstanceId,
                                      &i4RootHelloTime);
    i4RootHelloTime = AST_MGMT_TO_SYS (i4RootHelloTime);

    /*root  Bridge MaxAge */

    nmhGetFsMIPvrstInstRootMaxAge ((INT4) u4ContextId, (INT4) u4InstanceId,
                                   &i4RootMaxAge);
    i4RootMaxAge = AST_MGMT_TO_SYS (i4RootMaxAge);

    /* Root Bridge Forward Delay */

    nmhGetFsMIPvrstInstRootForwardDelay ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4RootFwdDelay);
    i4RootFwdDelay = AST_MGMT_TO_SYS (i4RootFwdDelay);
    AstReleaseContext ();
    if (i4ModStatus == PVRST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nPer-VLAN Rapid Spanning tree Protocol has been disabled\r\n");
    }

    else
    {
        nmhGetFsMIPvrstForceProtocolVersion ((INT4) u4ContextId, &i4Version);
        if (i4Version == AST_VERSION_2)
        {
            CliPrintf (CliHandle,
                       "\r\nSpanning-tree for VLAN %d\r\n", u4InstanceId);
            CliPrintf (CliHandle,
                       "\r\nBridge is executing the rstp compatible PVRST Protocol\r\n");
        }
        else
        {
            if (AST_VERSION_0 == i4Version)
            {
                CliPrintf (CliHandle,
                           "\r\nBridge is executing the stp compatible PVRST Protocol\r\n");
            }
        }

    }

    CliPrintf (CliHandle, "Bridge Identifier has priority %d, ", i4Priority);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "Address %s\r\n", au1BrgAddr);
    CliPrintf (CliHandle, "Configured Hello time %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, " Max Age %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, " Forward Delay %d sec %d cs \r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    if (i4DynamicPathCost == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Dynamic Path Cost is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Dynamic Path Cost is Disabled\r\n");
    }

    CliPrintf (CliHandle, "Flush Interval %d centi-sec,", i4FlushInterval);
    CliPrintf (CliHandle, " Flush Invocations %d \r\n", u4FlushCount);
    CliPrintf (CliHandle, "Flush Indication threshold %d \r\n",
               i4FlushThreshold);
    /* Root Bridge Info */

    if (AST_MEMCMP ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                    MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "We are the root of the spanning tree\r\n");
    }

    CliPrintf (CliHandle, "Number of Topology Changes %d \r\n", u4TopChange);

    CliPrintf (CliHandle, "Time since topology Change %d seconds ago\r\n",
               u4TopTime);
    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4RetVal = AstCfaCliGetIfName (gu4RecentTopoChPort, (INT1 *) au1IntfName);
    CliPrintf (CliHandle, "Port which caused last topology change : %s\r\n",
               au1IntfName);

    CliPrintf (CliHandle, "Transmit Hold-Count %d \r\n", i4HoldTime);
    CliPrintf (CliHandle, "Root Times: Max Age %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4RootMaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootMaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4RootFwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootFwdDelay));
    CliPrintf (CliHandle, "Hello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4RootHelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4RootHelloTime));
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowSpanningTree                           */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the RootBridge information */
/*                          the bridge details and also the  information of  */
/*                          all the active ports                             */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Val-Info to be displayed                         */
/*                        u4InstanceId - Instance Identifier                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
PvrstCliShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4InstanceId, UINT4 u4Active)
{
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    INT4                i4Priority = (INT4) AST_INIT_VAL;
    INT4                i4ModStatus = (INT4) AST_INIT_VAL;
    INT4                i4Version = (INT4) AST_INIT_VAL;
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4PortRole = (INT4) AST_INIT_VAL;
    INT4                i4PortState = (INT4) AST_INIT_VAL;
    INT4                i4PortCost = (INT4) AST_INIT_VAL;
    INT4                i4PortPriority = (INT4) AST_INIT_VAL;
    INT4                i4LinkType = (INT4) AST_INIT_VAL;

    tAstMacAddr         MacAddress;
    UINT4               u4IfIndex = (UINT4) AST_INIT_VAL;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4CurrentPort, i4NextPort;
    INT1                i1OutCome = (INT1) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT4               u4Quit = (UINT4) CLI_SUCCESS;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    INT4                i4DynamicPathCost = AST_INIT_VAL;
    INT4                i4DynamicPathCostLag = AST_INIT_VAL;
    INT4                i4RootInconsistentState = AST_FALSE;
    INT4                i4BpduInconsistentState = PVRST_FALSE;
    INT4                i4PortTypeInconsistentState = PVRST_FALSE;
    INT4                i4PortPVIDInconsistentState = PVRST_FALSE;

    UNUSED_PARAM (i4RetVal);

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }
    nmhGetFsMIPvrstModuleStatus ((INT4) u4ContextId, &i4ModStatus);
    if (i4ModStatus != PVRST_DISABLED)
    {
        i4Version = AST_VERSION_2;
    }

    if (VlanGetEnabledStatus () == VLAN_FALSE)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle,
                   "\r\nSpanning-tree for VLAN %d is inactive\r\n",
                   u4InstanceId);
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle,
               "\r\n------------------------------------------------------------------- \r\n");
    CliPrintf (CliHandle, "\r\nSpanning-tree for VLAN %d \r\n", u4InstanceId);

    /*Display Root Bridge Details */
    PvrstCliShowRootBridgeInfo (CliHandle, u4ContextId, u4InstanceId);

    /* Bridge Priority */

    nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId, (INT4) u4InstanceId,
                                       &i4Priority);
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /*Bridge Address */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    RstGetBridgeAddr (&MacAddress);

    /*Hello Time */

    nmhGetFsMIPvrstInstBridgeHelloTime ((INT4) u4ContextId, (INT4) u4InstanceId,
                                        &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Max Age */

    nmhGetFsMIPvrstInstBridgeMaxAge ((INT4) u4ContextId, (INT4) u4InstanceId,
                                     &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsMIPvrstInstBridgeForwardDelay ((INT4) u4ContextId,
                                           (INT4) u4InstanceId, &i4FwdDelay);
    nmhGetFsMIPvrstDynamicPathCostCalculation ((INT4) u4ContextId,
                                               &i4DynamicPathCost);
    nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg ((INT4) u4ContextId,
                                               &i4DynamicPathCostLag);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    nmhGetFsMIPvrstForceProtocolVersion ((INT4) u4ContextId, &i4Version);

    nmhGetFsMIPvrstModuleStatus ((INT4) u4ContextId, &i4ModStatus);

    if (i4ModStatus == PVRST_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\nPer-VLAN Rapid Spanning tree Protocol has been disabled\r\n");
    }
    else
    {
        if (i4Version == AST_VERSION_2)
        {
            CliPrintf (CliHandle,
                       "\r\nSpanning Tree Enabled Protocol PVRST \r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n Vlan %d is executing the stp compatible PVRST\r\n",
                       u4InstanceId);
        }

    }

    CliPrintf (CliHandle, "Bridge Id       Priority %d\r\n ", i4Priority);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "               Address %s\r\n", au1BrgAddr);

    CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "Max Age %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs \r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    if (i4DynamicPathCost == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Disabled\r\n");
    }

    if (i4DynamicPathCostLag == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Disabled\r\n");
    }

    /* Display the Port Role,PortState,PortCost,PortPriority,PortLinkType */

    CliPrintf (CliHandle,
               "%-9s%-13s%-13s%-9s%-7s%-8s\r\n",
               "Name", "Role", "State", "Cost", "Prio", "Type");
    CliPrintf (CliHandle,
               "%-9s%-13s%-13s%-9s%-7s%-8s\r\n",
               "----", "----", "-----", "----", "----", "------");

    /* Get The Current Port */

    i1OutCome = nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort);
    while (i1OutCome != SNMP_FAILURE)
    {
        /* u4IfIndex = AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY((UINT2)i4NextPort)); */
        u4IfIndex = i4NextPort;

        /* Get the port oper status for PVRST */
        AstL2IwfGetPortOperStatus (STP_MODULE, u4IfIndex, &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (u4Active == CLI_STP_VLAN_ACTIVE)
            {
                if (nmhGetFsMIPvrstInstPortState
                    ((INT4) u4InstanceId, i4NextPort,
                     &i4PortState) == SNMP_FAILURE)
                {
                    i4CurrentPort = i4NextPort;
                    i1OutCome =
                        nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                                 &i4NextPort);
                    continue;
                }
                if ((i4PortState == AST_PORT_STATE_LEARNING) ||
                    (i4PortState == AST_PORT_STATE_FORWARDING))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            /* If the port(i4NextPort) is not a member of the u4InstanceId
             * continue the loop*/
            if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                         (tVlanId) u4InstanceId,
                                         (UINT4) i4NextPort) == OSIX_FALSE)
            {
                i4CurrentPort = i4NextPort;
                i1OutCome =
                    nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                             &i4NextPort);
                continue;
            }

            if (u1Flag)
            {
                i4RetVal = AstCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

                /*PortRole */
                if (nmhGetFsMIPvrstInstPortRole
                    ((INT4) u4InstanceId, i4NextPort,
                     &i4PortRole) == SNMP_FAILURE)
                {
                    i4CurrentPort = i4NextPort;
                    i1OutCome =
                        nmhGetNextIndexFsMIFuturePvrstPortTable
                        (i4CurrentPort, &i4NextPort);
                    continue;
                }

                /*Port State */
                nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, i4NextPort,
                                              &i4PortState);
                /*Port Cost */
                nmhGetFsMIPvrstInstPortPathCost ((INT4) u4InstanceId,
                                                 i4NextPort, &i4PortCost);
                /*PortPriority */
                nmhGetFsMIPvrstInstPortPriority ((INT4) u4InstanceId,
                                                 i4NextPort, &i4PortPriority);
                /* Link Type */
                nmhGetFsMIPvrstPortOperPointToPoint (i4NextPort, &i4LinkType);

                CliPrintf (CliHandle, "%-9s", au1IntfName);

                PvrstCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");

                if (i4ModStatus == PVRST_DISABLED)
                {
                    i4PortState = AST_INIT_VAL;
                }
                PvrstCliDisplayPortState (CliHandle, i4PortState);
                CliPrintf (CliHandle, "%-3s", " ");

                CliPrintf (CliHandle, "%-9d", i4PortCost);

                CliPrintf (CliHandle, "%-7d", i4PortPriority);

                if (i4LinkType == AST_SNMP_TRUE)
                {
                    /* u4Quit contains the paging status */
                    u4Quit = CliPrintf (CliHandle, "P2P");
                }
                else
                {
                    u4Quit = CliPrintf (CliHandle, "SharedLan");
                }

                PvrstGetInstPortRootInconsistentState ((UINT2) i4NextPort,
                                                       (UINT2) u4InstanceId,
                                                       &i4RootInconsistentState);

                if (i4RootInconsistentState == PVRST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                }

                nmhGetFsMIPvrstPortBpduInconsistentState (i4NextPort,
                                                          &i4BpduInconsistentState);

                if (i4BpduInconsistentState == PVRST_TRUE)
                {
                    CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
                }

                nmhGetFsMIPvrstPortTypeInconsistentState (i4NextPort,
                                                          &i4PortTypeInconsistentState);
                if (i4PortTypeInconsistentState == PVRST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Type-Inc)\r\n");
                }
                nmhGetFsMIPvrstPortPVIDInconsistentState (i4NextPort,
                                                          &i4PortPVIDInconsistentState);
                if (i4PortPVIDInconsistentState == PVRST_TRUE)
                {
                    CliPrintf (CliHandle, "(*PVID-Inc)\r\n");
                }

                PvrstCliShowLoopIncState (CliHandle, u4ContextId,
                                          (UINT4) i4NextPort, u4InstanceId);

            }
            if (u4Quit == CLI_FAILURE)
            {
                break;
            }
        }
        u1Flag = 0;
        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                     &i4NextPort);

    }                            /*while */
    AstReleaseContext ();
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayBlockedPorts                        */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the BlockedPorts       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
PvrstCliDisplayBlockedPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4InstanceId)
{

    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    INT4                i4CurrentPort, i4NextPort;
    UINT1               i1OutCome = (INT1) AST_INIT_VAL;
    UINT4               u4BlockedPort = (UINT4) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    CliPrintf (CliHandle, "\r\nBlocked Interfaces List:\r\n");

    i1OutCome = nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort);

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        return CLI_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    while (i1OutCome != SNMP_FAILURE)
    {
        nmhGetFsMIPvrstPortEnabledStatus (i4NextPort, &i4RetVal);
        if (i4RetVal == AST_TRUE)
        {
            nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, i4NextPort,
                                          &i4RetVal);
        }
        else
        {
            i4RetVal = AST_PORT_STATE_DISABLED;
        }

        if (i4RetVal == AST_PORT_STATE_DISCARDING)
        {
            i4RetVal =
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

            CliPrintf (CliHandle, "%s,", au1IntfName);

            u4BlockedPort++;
        }

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                     &i4NextPort);
    }

    CliPrintf (CliHandle, "\b");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "\rThe Number of Blocked Ports in the system is :%d\r\n\r\n",
               u4BlockedPort);
    AstReleaseContext ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowSpanningTreeMethod                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Spanning Tree method    */
/*                        for PVRST either 16bit(short) or 32bitpathcost(Long)*/
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
PvrstCliShowSpanningTreeMethod (tCliHandle CliHandle, UINT4 u4ContextId)
{
    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        return CLI_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nSpanning Tree port pathcost method is Long\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstCliShowSummary                                  */
/*                                                                           */
/* Description        : Displays the spanningtree port states and  port roles*/
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context                */
/*                      u4InstanceId- Instance Identifier                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
PvrstCliShowSummary (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 u4InstanceId)
{
    INT4                i4ModStatus = (INT4) AST_INIT_VAL;
    INT4                i4PortRole = (INT4) AST_INIT_VAL;
    INT4                i4PortState = (INT4) AST_INIT_VAL;
    INT4                i4PortStatus = (INT4) AST_INIT_VAL;

    UINT4               u4IfIndex = (UINT4) AST_INIT_VAL;
    INT4                i4CurrentPort, i4NextPort;
    UINT1               i1OutCome = (INT1) AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    AST_MEMSET (au1IntfName, 0, sizeof (au1IntfName));

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        return CLI_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (VlanGetEnabledStatus () == VLAN_FALSE)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle,
                   "\r\nVLAN module is not enabled in this context.\r\n");
        return CLI_SUCCESS;
    }
    nmhGetFsMIPvrstModuleStatus ((INT4) u4ContextId, &i4ModStatus);

    /* Displays the Spanning Tree PathCost Method ,PortStates 
       and Roles */

    CliPrintf (CliHandle, "\r\nSpanning tree enabled protocol is PVRST\r\n");

    CliPrintf (CliHandle, "Spanning-tree pathcost method is long\r\n");

    CliPrintf (CliHandle, "\r\nPVRST Port Roles and States\r\n");
    CliPrintf (CliHandle,
               "%-12s%-13s%-13s%-13s\r\n",
               "Port-Index", "Port-Role", "Port-State", "Port-Status");
    CliPrintf (CliHandle,
               "%-12s%-13s%-13s%-13s\r\n",
               "----------", "---------", "----------", "-----------");

    i1OutCome = nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort);
    while (i1OutCome != SNMP_FAILURE)
    {
        /* u4IfIndex = AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY((UINT2)i4NextPort)); */
        u4IfIndex = i4NextPort;

        /* Get the port oper status for PVRST */
        AstL2IwfGetPortOperStatus (STP_MODULE, u4IfIndex, &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (AstL2IwfMiIsVlanActive (u4ContextId, (tVlanId) u4InstanceId)
                == OSIX_TRUE)
            {
                if (nmhGetFsMIPvrstInstPortState
                    ((INT4) u4InstanceId, i4NextPort,
                     &i4PortState) == SNMP_FAILURE)
                {
                    i4CurrentPort = i4NextPort;
                    i1OutCome =
                        nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                                 &i4NextPort);
                    continue;
                }
                u1Flag = 1;
            }
            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {

                /*PortRole */
                if (nmhGetFsMIPvrstInstPortRole
                    ((INT4) u4InstanceId, i4NextPort,
                     &i4PortRole) == SNMP_FAILURE)
                {
                    i4CurrentPort = i4NextPort;
                    i1OutCome =
                        nmhGetNextIndexFsMIFuturePvrstPortTable
                        (i4CurrentPort, &i4NextPort);
                    continue;
                }

                /*Port State */
                nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, i4NextPort,
                                              &i4PortState);
                nmhGetFsMIPvrstInstPortEnableStatus ((INT4) u4InstanceId,
                                                     i4NextPort, &i4PortStatus);
                AstCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);
                CliPrintf (CliHandle, "%-12s", au1IntfName);

                PvrstCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");

                if (i4ModStatus == PVRST_DISABLED)
                {
                    i4PortState = AST_INIT_VAL;
                }
                PvrstCliDisplayPortState (CliHandle, i4PortState);
                CliPrintf (CliHandle, "%-3s", " ");

                if (i4PortStatus == AST_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, "Enabled\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "Disabled\r\n");
                }
                /* Get the Next Port from the Table */
            }
        }
        u1Flag = 0;
        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort,
                                                     &i4NextPort);
    }
    CliPrintf (CliHandle, "\r\n");
    AstReleaseContext ();
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowSpanningTreeBrgDet                     */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  Bridge information    */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                        u4Val-Bridge Info to be displayed                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliShowSpanningTreeBrgDet (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4InstanceId, UINT4 u4Val)
{

    tAstMacAddr         MacAddress;
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4Priority = (INT4) AST_INIT_VAL;

    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];

    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        return CLI_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    switch (u4Val)
    {
        case CLI_STP_VLAN_BRIDGE_ADDRESS:

            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_FAILURE;
            }
            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            RstGetBridgeAddr (&MacAddress);
            PrintMacAddress (MacAddress, au1BrgAddr);
            break;

        case CLI_STP_VLAN_BRIDGE_DETAIL:
            /* Bridge Priority */

            nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId,
                                               (INT4) u4InstanceId,
                                               &i4Priority);

            /*Bridge Address */
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_FAILURE;
            }

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            RstGetBridgeAddr (&MacAddress);
            PrintMacAddress (MacAddress, au1BrgAddr);

            /*Hello Time */

            nmhGetFsMIPvrstInstBridgeHelloTime ((INT4) u4ContextId,
                                                (INT4) u4InstanceId,
                                                &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

            /*Max Age */

            nmhGetFsMIPvrstInstBridgeMaxAge ((INT4) u4ContextId,
                                             (INT4) u4InstanceId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            /*Forward Delay */

            nmhGetFsMIPvrstInstBridgeForwardDelay ((INT4) u4ContextId,
                                                   (INT4) u4InstanceId,
                                                   &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            break;

        case CLI_STP_VLAN_BRIDGE_FORWARD_TIME:

            nmhGetFsMIPvrstInstBridgeForwardDelay ((INT4) u4ContextId,
                                                   (INT4) u4InstanceId,
                                                   &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            break;
        case CLI_STP_VLAN_BRIDGE_HELLO_TIME:

            nmhGetFsMIPvrstInstBridgeHelloTime ((INT4) u4ContextId,
                                                (INT4) u4InstanceId,
                                                &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);
            break;

        case CLI_STP_VLAN_BRIDGE_ID:

            /* Bridge ID */

            nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId,
                                               (INT4) u4InstanceId,
                                               &i4Priority);

            /* Valid values of priorities in hex are 0x1000(4096), 0x2000 (8192)
             * ,..0xF000(61440).
             * So Bridge Priority is Priority + VlanId
             */
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_FAILURE;
            }

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            RstGetBridgeAddr (&MacAddress);
            PrintMacAddress (MacAddress, au1BrgAddr);

            break;
        case CLI_STP_VLAN_BRIDGE_MAX_AGE:

            nmhGetFsMIPvrstInstBridgeMaxAge ((INT4) u4ContextId,
                                             (INT4) u4InstanceId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
            break;

        case CLI_STP_VLAN_BRIDGE_PRIORITY_SYSTEM:

            nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId,
                                               (INT4) u4InstanceId,
                                               &i4Priority);
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_FAILURE;
            }
            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            RstGetBridgeAddr (&MacAddress);
            PrintMacAddress (MacAddress, au1BrgAddr);
            break;

        case CLI_STP_VLAN_BRIDGE_PRIORITY:

            nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId,
                                               (INT4) u4InstanceId,
                                               &i4Priority);
            break;

        default:

            /* Bridge ID */

            nmhGetFsMIPvrstInstBridgePriority ((INT4) u4ContextId,
                                               (INT4) u4InstanceId,
                                               &i4Priority);

            /* Valid values of priorities in hex are 0x1000(4096), 0x2000 (8192)
             * ,..0xF000(61440).
             * So Bridge Priority is Priority + VlanId
             */
            if (AstSelectContext (u4ContextId) != RST_SUCCESS)
            {
                return CLI_FAILURE;
            }

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            RstGetBridgeAddr (&MacAddress);
            PrintMacAddress (MacAddress, au1BrgAddr);

            /*HelloTime */
            nmhGetFsMIPvrstInstBridgeHelloTime ((INT4) u4ContextId,
                                                (INT4) u4InstanceId,
                                                &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

            /*MaxAge */
            nmhGetFsMIPvrstInstBridgeMaxAge ((INT4) u4ContextId,
                                             (INT4) u4InstanceId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            /* Forward Delay */
            nmhGetFsMIPvrstInstBridgeForwardDelay ((INT4) u4ContextId,
                                                   (INT4) u4InstanceId,
                                                   &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            break;
    }

    /* Since Lock needs to be released before printing the above information, all the
     * Cliprintfs are grouped in the below switch-case */

    switch (u4Val)
    {

        case CLI_STP_VLAN_BRIDGE_ADDRESS:

            CliPrintf (CliHandle, "\r\nBridge Address is %s\r\n", au1BrgAddr);
            break;

        case CLI_STP_VLAN_BRIDGE_DETAIL:

            CliPrintf (CliHandle, "\r\nBridge Id       Priority %d,\r\n ",
                       i4Priority);

            CliPrintf (CliHandle, "               Address %s\r\n", au1BrgAddr);

            CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            CliPrintf (CliHandle, "Max Age %d sec %d cs, ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

            CliPrintf (CliHandle, "Forward Delay %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

            break;

        case CLI_STP_VLAN_BRIDGE_FORWARD_TIME:

            CliPrintf (CliHandle,
                       "\r\nBridge Forward delay is  %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            break;

        case CLI_STP_VLAN_BRIDGE_HELLO_TIME:

            CliPrintf (CliHandle, "\r\nBridge Hello Time is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
            break;

        case CLI_STP_VLAN_BRIDGE_ID:

            CliPrintf (CliHandle, "\r\nBridge ID is %x:00:",
                       i4Priority / (0x100));

            CliPrintf (CliHandle, "%s\r\n", au1BrgAddr);
            break;

        case CLI_STP_VLAN_BRIDGE_MAX_AGE:

            CliPrintf (CliHandle, "\r\nBridge Max Age is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            break;

        case CLI_STP_VLAN_BRIDGE_PROTOCOL:

            CliPrintf (CliHandle, "\r\nBridge Protocol Running is PVRST\r\n");

            break;

        case CLI_STP_VLAN_BRIDGE_PRIORITY:

            CliPrintf (CliHandle, "\r\nBridge Priority is %5d\r\n", i4Priority);
            break;

        case CLI_STP_VLAN_BRIDGE_PRIORITY_SYSTEM:

            CliPrintf (CliHandle, "\r\nBridge Address is %s\r\n", au1BrgAddr);
            break;

        default:

            CliPrintf (CliHandle,
                       "\r\n%-25s%-13s%-13s%-15s%-8s\r\n", "Bridge ID",
                       "  HelloTime", "MaxAge", "FwdDly", "Protocol");

            CliPrintf (CliHandle,
                       "%-25s%-13s%-13s%-15s%-8s\r\n",
                       "---------", "  ---------", "------", "------",
                       "--------");

            /*Bridge ID */

            CliPrintf (CliHandle, "%2x:00:", i4Priority / (0x100));

            CliPrintf (CliHandle, "%s ", au1BrgAddr);

            /*Hello Time */
            CliPrintf (CliHandle, "%-1d sec %-2d cs",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            /*MaxAge */
            CliPrintf (CliHandle, "%-2d sec %-2d cs",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

            /*FwdDelay */
            CliPrintf (CliHandle, "%-2d sec %-2d cs",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

            /*Protocol */
            CliPrintf (CliHandle, "%-8s\r\n", "Pvrst");

            break;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowSpanningTreeRootDet                    */
/*                                                                           */
/*     DESCRIPTION      : This function  dispalys the root bridge parameters */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                        u4Val-Info to be displayed                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
PvrstCliShowSpanningTreeRootDet (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4InstanceId, UINT4 u4Val)
{
    tSNMP_OCTET_STRING_TYPE DesigRoot;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4RootCost = (INT4) AST_INIT_VAL;
    INT4                i4RootPort = (INT4) AST_INIT_VAL;
    UINT2               u2RootPrio = (UINT2) AST_INIT_VAL;
    UINT2               u2RootPriority = (UINT2) AST_INIT_VAL;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);

    DesigRoot.pu1_OctetList = &au1BrgIdBuf[0];
    DesigRoot.i4_Length = AST_BRG_ADDR_DIS_LEN;

    switch (u4Val)
    {
        case CLI_STP_VLAN_ROOT_ADDRESS:

            AST_MEMSET (au1BrgAddr, 0, AST_BRG_ADDR_DIS_LEN);
            nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId,
                                               (INT4) u4InstanceId, &DesigRoot);
            break;

        case CLI_STP_VLAN_ROOT_COST:

            nmhGetFsMIPvrstInstRootCost ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4RootCost);
            break;

        case CLI_STP_VLAN_ROOT_DETAIL:
            /*Root Bridge Priority */
            PvrstCliShowRootBridgeInfo (CliHandle, u4ContextId, u4InstanceId);
            break;

        case CLI_STP_VLAN_ROOT_FORWARD_TIME:

            nmhGetFsMIPvrstInstRootForwardDelay ((INT4) u4ContextId,
                                                 (INT4) u4InstanceId,
                                                 &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            break;

        case CLI_STP_VLAN_ROOT_HELLO_TIME:

            nmhGetFsMIPvrstInstRootHelloTime ((INT4) u4ContextId,
                                              (INT4) u4InstanceId,
                                              &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);
            break;

        case CLI_STP_VLAN_ROOT_ID:

            nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId,
                                               (INT4) u4InstanceId, &DesigRoot);
            break;

        case CLI_STP_VLAN_ROOT_MAX_AGE:

            nmhGetFsMIPvrstInstRootMaxAge ((INT4) u4ContextId,
                                           (INT4) u4InstanceId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
            break;

        case CLI_STP_VLAN_ROOT_PORT:

            nmhGetFsMIPvrstInstRootPort ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4RootPort);
            break;

        case CLI_STP_VLAN_ROOT_PRIORITY:

            nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId,
                                               (INT4) u4InstanceId, &DesigRoot);
            break;

        case CLI_STP_VLAN_ROOT_PRIORITY_SYSTEM:

            nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId,
                                               (INT4) u4InstanceId, &DesigRoot);
            break;
        default:
            /* Display Root Bridge Info in Table Format */
            PvrstCliShowRootBridgeTable (CliHandle, u4ContextId, u4InstanceId);
            break;
    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been gruped under the below
     *  switch-case */

    switch (u4Val)
    {
        case CLI_STP_VLAN_ROOT_ADDRESS:

            /* First 2 bytes contain the bridge priority and the
             * next 6 bytes contain the Bridge MAC*/
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (DesigRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                             au1BrgAddr);
            CliPrintf (CliHandle, "\r\nRoot Bridge Address is %s\r\n",
                       au1BrgAddr);
            break;

        case CLI_STP_VLAN_ROOT_COST:

            CliPrintf (CliHandle, "\r\nRoot Cost is %d \r\n", i4RootCost);
            break;

        case CLI_STP_VLAN_ROOT_FORWARD_TIME:

            CliPrintf (CliHandle, "\r\nForward delay is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            break;

        case CLI_STP_VLAN_ROOT_HELLO_TIME:

            CliPrintf (CliHandle, "\r\nHello Time is %d sec %d cs\r\n ",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
            break;

        case CLI_STP_VLAN_ROOT_ID:

            AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
            CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length,
                           au1BrgId, AST_BRG_ID_DIS_LEN);

            CliPrintf (CliHandle, "\r\nRoot Bridge Id is %s \r\n", au1BrgId);
            break;

        case CLI_STP_VLAN_ROOT_MAX_AGE:

            CliPrintf (CliHandle, "\r\nRoot MaxAge is %d sec %d cs\n ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            break;

        case CLI_STP_VLAN_ROOT_PORT:

            AstCfaCliGetIfName (i4RootPort, (INT1 *) au1IntfName);
            CliPrintf (CliHandle, "\r\nRoot Port is %s \r\n ", au1IntfName);
            break;

        case CLI_STP_VLAN_ROOT_PRIORITY:

            u2RootPrio = AstGetBrgPrioFromBrgId (DesigRoot);
            PVRST_GET_BRIDGE_PRIORITY (u2RootPrio, u2RootPriority);
            CliPrintf (CliHandle, "\r\nRoot Priority is %5d \r\n",
                       u2RootPriority);
            break;

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowRootBridgeInfo                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
PvrstCliShowRootBridgeInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4InstanceId)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tAstMacAddr         MacAddress;

    INT4                i4RetVal = 0;
    INT4                i4RootCost = (INT4) AST_INIT_VAL;
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    UINT2               u2RootPrio = (UINT2) AST_INIT_VAL;
    UINT2               u2RootPriority = (UINT2) AST_INIT_VAL;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4RootPort = (INT4) AST_INIT_VAL;
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);

    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /* To Check  if the  Bridge is a ROOT Bridge */
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    RstGetBridgeAddr (&MacAddress);

    /*Displays the Root Id,Root Timers */

    /* Get The  Root ID-Priority and MacAddress */

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId, (INT4) u4InstanceId,
                                       &RetBridgeId);

    /*Root Cost */

    nmhGetFsMIPvrstInstRootCost ((INT4) u4ContextId, (INT4) u4InstanceId,
                                 &i4RootCost);

    /*Root Port */
    nmhGetFsMIPvrstInstRootPort ((INT4) u4ContextId, (INT4) u4InstanceId,
                                 &i4RootPort);
    i4RetVal = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);

    /* Root HelloTime */

    nmhGetFsMIPvrstInstRootHelloTime ((INT4) u4ContextId, (INT4) u4InstanceId,
                                      &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Root MaxAge */

    nmhGetFsMIPvrstInstRootMaxAge ((INT4) u4ContextId, (INT4) u4InstanceId,
                                   &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsMIPvrstInstRootForwardDelay ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (AST_MEMCMP
        ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
         MacAddress, AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "\r\nWe are the root of the Spanning Tree\r\n");

    }

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    PVRST_GET_BRIDGE_PRIORITY (u2RootPrio, u2RootPriority);
    CliPrintf (CliHandle, "Root Id         Priority   %d\r\n", u2RootPriority);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress ((RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                     au1BrgAddr);
    CliPrintf (CliHandle, "                Address    %s\r\n ", au1BrgAddr);

    CliPrintf (CliHandle, "               Cost       %d\r\n", i4RootCost);

    /* Get the Interface Name for the Port */
    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   " %%PortName for index %d  is not found \r\n", i4RootPort);
    }

    CliPrintf (CliHandle, "                Port       %-9s\r\n", au1IntfName);

    CliPrintf (CliHandle, "                Hello Time %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "Max Age %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Forward Delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowRootBridgeTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information                                        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4InstanceId- Instance Identifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
PvrstCliShowRootBridgeTable (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4InstanceId)
{
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4HelloTime = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4Intf = (INT4) AST_INIT_VAL;
    INT4                i4RootPort = (INT4) AST_INIT_VAL;
    INT4                i4RootCost = (INT4) AST_INIT_VAL;

    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE DesigRoot;
    UINT1              *pu1Temp = NULL;

    AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    DesigRoot.pu1_OctetList = &au1BrgIdBuf[0];
    DesigRoot.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /*Root Bridge Id */
    nmhGetFsMIPvrstInstDesignatedRoot ((INT4) u4ContextId, (INT4) u4InstanceId,
                                       &DesigRoot);

    /* Root Cost */

    nmhGetFsMIPvrstInstRootCost ((INT4) u4ContextId, (INT4) u4InstanceId,
                                 &i4RootCost);

    /* Root HelloTime */

    nmhGetFsMIPvrstInstRootHelloTime ((INT4) u4ContextId, (INT4) u4InstanceId,
                                      &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Root MaxAge */

    nmhGetFsMIPvrstInstRootMaxAge ((INT4) u4ContextId, (INT4) u4InstanceId,
                                   &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsMIPvrstInstRootForwardDelay ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* Root Port */
    nmhGetFsMIPvrstInstRootPort ((INT4) u4ContextId, (INT4) u4InstanceId,
                                 &i4RootPort);

    pu1Temp = &au1IntfName[0];
    AST_MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4Intf = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) pu1Temp);

    /*Displays the Root Id,Root Timers,RootCost,RootPort  in the table format */

    CliPrintf (CliHandle,
               "\r\n%-25s%-9s%-13s%-13s%-15s%-9s\r\n",
               "Root ID", "RootCost", "HelloTime", "MaxAge", "FwdDly",
               "RootPort");
    CliPrintf (CliHandle,
               "%-25s%-9s%-13s%-13s%-15s%-9s\r\n",
               "-------", "--------", "---------", "------", "------",
               "--------");

    AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
    CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length, au1BrgId,
                   AST_BRG_ID_DIS_LEN);
    CliPrintf (CliHandle, "%-25s", au1BrgId);

    CliPrintf (CliHandle, "%-9d", i4RootCost);
    CliPrintf (CliHandle, "%d sec %d cs  ",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle, "%d sec %d cs  ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "%d s %d cs  ",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    /* Get the Interface Name for the Port */
    if (i4Intf == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%PortName for index %d  is not found \r\n",
                   i4Intf);
    }
    CliPrintf (CliHandle, "%12s\r\n", pu1Temp);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayInterfaceDetails                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface Details      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Value-Interface Info to be displayed             */
/*                        u4Index-Interface Index                            */
/*                        u4InstanceId- Instance Idnetifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliDisplayInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4InstanceId, UINT4 u4Index, UINT4 u4Val)
{

    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    INT4                i4FwdTransition = (INT4) AST_INIT_VAL;
    INT4                i4PortRole = (INT4) AST_INIT_VAL;
    INT4                i4PortState = (INT4) AST_INIT_VAL;
    INT4                i4RootCost = (INT4) AST_INIT_VAL;
    INT4                i4MigrnCnt = (INT4) AST_INIT_VAL;
    INT4                i4PortPriority = (INT4) AST_INIT_VAL;
    INT4                i4RstBpduCnt = (INT4) AST_INIT_VAL;
    INT4                i4ConfigBpduCnt = (INT4) AST_INIT_VAL;
    INT4                i4TcnBpduCnt = (INT4) AST_INIT_VAL;
    INT4                i4RstBpdu = (INT4) AST_INIT_VAL;
    INT4                i4ConfigBpdu = (INT4) AST_INIT_VAL;
    INT4                i4TcnBpdu = (INT4) AST_INIT_VAL;
    INT4                i4PortCost = (INT4) AST_INIT_VAL;
    INT4                i4PathCost = (INT4) AST_INIT_VAL;

    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4Index != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4Index);
        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }

    switch (u4Val)
    {

        case CLI_STP_VLAN_INTF_COST:

            nmhGetFsMIPvrstInstPortPathCost ((INT4) u4InstanceId, u4Index,
                                             &i4PathCost);
            break;

        case CLI_STP_VLAN_INTF_PRIORITY:

            nmhGetFsMIPvrstInstPortPriority ((INT4) u4InstanceId, u4Index,
                                             &i4PortPriority);
            break;

        case CLI_STP_VLAN_INTF_DET:

            PvrstCliDisplayPortDetails (CliHandle, u4ContextId, u4InstanceId,
                                        u4Index);
            break;

        case CLI_STP_VLAN_INTF_STATE:

            nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, u4Index,
                                          &i4PortState);
            break;

        case CLI_STP_VLAN_INTF_ROOT_COST:

            nmhGetFsMIPvrstInstRootCost ((INT4) u4ContextId,
                                         (INT4) u4InstanceId, &i4RootCost);
            break;

        case CLI_STP_VLAN_INTF_STATS:

            i4RetVal = AstCfaCliGetIfName (u4Index, (INT1 *) au1IntfName);

            /* Forward Transitions */

            nmhGetFsMIPvrstInstPortForwardTransitions ((INT4) u4InstanceId,
                                                       (INT4) u4Index,
                                                       (UINT4 *)
                                                       &i4FwdTransition);

            /* PortReceived RSTP BPDU Count */

            nmhGetFsMIPvrstInstPortReceivedBpdus ((INT4) u4InstanceId,
                                                  (INT4) u4Index,
                                                  (UINT4 *) &i4RstBpduCnt);

            /* Port Received Config BPDU Count */

            nmhGetFsMIPvrstInstPortRxConfigBpduCount ((INT4) u4InstanceId,
                                                      (INT4) u4Index,
                                                      (UINT4 *)
                                                      &i4ConfigBpduCnt);

            /* Port Received TCN BPDU Count */
            nmhGetFsMIPvrstInstPortRxTcnBpduCount ((INT4) u4InstanceId,
                                                   (INT4) u4Index,
                                                   (UINT4 *) &i4TcnBpduCnt);

            /* Port Transmitted RSTP BPDU Count */
            nmhGetFsMIPvrstInstPortTransmittedBpdus ((INT4) u4InstanceId,
                                                     (INT4) u4Index,
                                                     (UINT4 *) &i4RstBpdu);

            /* Port Transmitted Config BPDU Count */
            nmhGetFsMIPvrstInstPortTxConfigBpduCount ((INT4) u4InstanceId,
                                                      (INT4) u4Index,
                                                      (UINT4 *) &i4ConfigBpdu);

            /* Port Transmitted TCN BPDU Count */
            nmhGetFsMIPvrstInstPortTxTcnBpduCount ((INT4) u4InstanceId,
                                                   (INT4) u4Index,
                                                   (UINT4 *) &i4TcnBpdu);

            /* Port Protocol Migration Count */
            nmhGetFsMIPvrstInstProtocolMigrationCount ((INT4) u4InstanceId,
                                                       (INT4) u4Index,
                                                       (UINT4 *) &i4MigrnCnt);

            break;

        default:

            /*PortRole */
            nmhGetFsMIPvrstInstPortRole ((INT4) u4InstanceId,
                                         (INT4) u4Index, &i4PortRole);
            /*Port State */
            nmhGetFsMIPvrstInstPortState ((INT4) u4InstanceId, u4Index,
                                          &i4PortState);
            /*Port Cost */
            nmhGetFsMIPvrstInstPortPathCost ((INT4) u4InstanceId, u4Index,
                                             &i4PortCost);
            /*PortPriority */
            nmhGetFsMIPvrstInstPortPriority ((INT4) u4InstanceId, u4Index,
                                             &i4PortPriority);

            break;

    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been gruped under the below
     *  switch-case */

    switch (u4Val)
    {
        case CLI_STP_VLAN_INTF_ACTIVE:
            CliPrintf (CliHandle,
                       "\r\n%-10s%-13s%-13s%-9s%-5s%\r\n", "VLAN", "Role",
                       "State", "Cost", "Prio");

            CliPrintf (CliHandle, "%-10s%-13s%-13s%-9s%-5s%\r\n", "----",
                       "----", "-----", "----", "----");

            CliPrintf (CliHandle, "%3d", u4InstanceId);
            CliPrintf (CliHandle, "%-7s", " ");

            PvrstCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");

            PvrstCliDisplayPortState (CliHandle, i4PortState);
            CliPrintf (CliHandle, "%-3s", " ");

            CliPrintf (CliHandle, "%-9d", i4PortCost);

            CliPrintf (CliHandle, "%-5d", i4PortPriority);
            break;

        case CLI_STP_VLAN_INTF_COST:

            CliPrintf (CliHandle, "\r\nPort cost is %-9d \r\n", i4PathCost);
            break;

        case CLI_STP_VLAN_INTF_PRIORITY:

            CliPrintf (CliHandle, "\r\nPort Priority is %d \r\n",
                       i4PortPriority);
            break;

        case CLI_STP_VLAN_INTF_DET:

            /* Already displayed */
            break;

        case CLI_STP_VLAN_INTF_STATE:

            CliPrintf (CliHandle, "\r\n");
            PvrstCliDisplayPortState (CliHandle, i4PortState);
            CliPrintf (CliHandle, "\r\n");
            break;

        case CLI_STP_VLAN_INTF_ROOT_COST:

            CliPrintf (CliHandle, "\r\nRoot Cost is %d\r\n", i4RootCost);
            break;

        case CLI_STP_VLAN_INTF_STATS:

            /* Get the Interface Name for the Port */
            if (i4RetVal == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%% PortName for index %d  is not found \r\n",
                           u4Index);
            }
            CliPrintf (CliHandle, "\r\nStatistics for Port %-4s\r\n",
                       au1IntfName);

            CliPrintf (CliHandle,
                       "Number of Transitions to forwarding State : %d\r\n",
                       i4FwdTransition);

            CliPrintf (CliHandle,
                       "Number of PVRST BPDU Count received       : %d\r\n",
                       i4RstBpduCnt);

            CliPrintf (CliHandle,
                       "Number of Config BPDU Count received      : %d\r\n",
                       i4ConfigBpduCnt);

            CliPrintf (CliHandle,
                       "Number of TCN BPDU Count received         : %d\r\n",
                       i4TcnBpduCnt);

            CliPrintf (CliHandle,
                       "Number of PVRST BPDU Count Transmitted    : %d\r\n",
                       i4RstBpdu);

            CliPrintf (CliHandle,
                       "Number of Config BPDU Count Transmitted   : %d\r\n",
                       i4ConfigBpdu);

            CliPrintf (CliHandle,
                       "Number of TCN BPDU Count Transmitted      : %d\r\n",
                       i4TcnBpdu);

            CliPrintf (CliHandle,
                       "Port Protocol Migration Count             : %d\r\n",
                       i4MigrnCnt);
            break;

        default:

            CliPrintf (CliHandle,
                       "\r\n%-13s%-13s%-9s%-5s%\r\n", "Role", "State",
                       "Cost", "Prio");

            CliPrintf (CliHandle, "%-13s%-13s%-9s%-5s%\r\n", "----", "-----",
                       "----", "----");

            PvrstCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");

            PvrstCliDisplayPortState (CliHandle, i4PortState);
            CliPrintf (CliHandle, "%-3s", " ");

            CliPrintf (CliHandle, "%-9d", i4PortCost);

            CliPrintf (CliHandle, "%-5d", i4PortPriority);

            break;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayPortDetails                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Port Details           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4NextPort- Port Index                             */
/*                        u4InstanceId- Instance Identifier                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
PvrstCliDisplayPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4InstanceId, INT4 i4NextPort)
{
    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;

    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4TxBpduCount = AST_INIT_VAL;
    UINT4               u4RxBpduCount = AST_INIT_VAL;
    INT4                i4FilterEnabled = (INT4) AST_INIT_VAL;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4RootGuard = (INT4) AST_INIT_VAL;

    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    UINT2               u2RootPriority = (UINT2) AST_INIT_VAL;
    UINT2               u2RootPrio = (UINT2) AST_INIT_VAL;
    UINT2               u2BrgPriority = (UINT2) AST_INIT_VAL;
    UINT2               u2BrgPrio = (UINT2) AST_INIT_VAL;
    UINT2               u2PortPrio = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = (UINT2) AST_INIT_VAL;
    INT4                i4Intf = (INT4) AST_INIT_VAL;
    INT4                i4PortState = (INT4) AST_INIT_VAL;
    INT4                i4PortRole = (INT4) AST_INIT_VAL;
    INT4                i4PathCost = (INT4) AST_INIT_VAL;
    INT4                i4Priority = (INT4) AST_INIT_VAL;
    INT4                i4FwdTransition = (INT4) AST_INIT_VAL;
    INT4                i4Hello = (INT4) AST_INIT_VAL;
    INT4                i4FwdDelay = (INT4) AST_INIT_VAL;
    INT4                i4MaxAge = (INT4) AST_INIT_VAL;
    INT4                i4Hold = (INT4) AST_INIT_VAL;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];

    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_RSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_RSTP_MAX_PORTID_BUFFER];
    tPerStPvrstRstPortInfo *pPvrstPerStRstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4TxProposCount = 0;
    UINT4               u4RxProposCount = 0;
    UINT4               u4TxAgreementCount = 0;
    UINT4               u4RxAgreementCount = 0;
    UINT4               u4TxProposTimeStamp = 0;
    UINT4               u4RxProposTimeStamp = 0;
    UINT4               u4TxAgreementTimeStamp = 0;
    UINT4               u4RxAgreementTimeStamp = 0;
    UINT4               u4TopChRxd = 0;
    UINT4               u4DetecTopCh = 0;
    UINT4               u4DetecTopChTimeStamp = 0;
    UINT4               u4ImpStateOccTimeStamp = 0;
    UINT4               u4TopChRxdTimeStamp = 0;
    UINT4               u4ImpossibleStateOcc = 0;
    UINT4               u4RcvInfoCount = 0;
    UINT4               u4RcvInfoTimeStamp = 0;
    UINT1               au1Date[AST_ARRAY_TIMESTAMP] = { 0 };

    AST_MEMSET (au1Date, AST_INIT_VAL, sizeof (au1Date));

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    if ((pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4NextPort)) == NULL)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, "\rPort %d deleted \r\n", i4NextPort);
        return;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                (UINT2) u4InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        AstReleaseContext ();
        return;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        AstReleaseContext ();
        return;
    }
    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                   u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return;
    }

    MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RootBrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetRootBridgeId.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    RetPortId.pu1_OctetList = &au1PortIdBuf[0];
    RetPortId.i4_Length = CLI_RSTP_MAX_PORTID_BUFFER;

    /* Displaying Port info */

    /* Get the interface name for this Port Number */

    i4Intf = AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

    if (i4Intf == CLI_FAILURE)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle,
                   "\r\n%% Port Name for Index %d is not  found \r\n",
                   i4NextPort);
        return;
    }

    nmhGetFsMIPvrstBpduGuard (i4NextPort, &i4BpduGuard);
    nmhGetFsMIPvrstRootGuard (i4NextPort, &i4RootGuard);
    nmhGetFsMIPvrstBpduFilter (i4NextPort, &i4FilterEnabled);

    /* Port Roles */
    if (nmhGetFsMIPvrstInstPortRole
        ((INT4) u4InstanceId, i4NextPort, &i4PortRole) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }
    /* PORT STATES */
    if (nmhGetFsMIPvrstInstPortState
        ((INT4) u4InstanceId, i4NextPort, &i4PortState) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /*PortPathCost */
    if (nmhGetFsMIPvrstInstPortPathCost
        ((INT4) u4InstanceId, i4NextPort, &i4PathCost) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /* Port priority */
    if (nmhGetFsMIPvrstInstPortPriority
        ((INT4) u4InstanceId, i4NextPort, &i4Priority) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /*Designated Root */
    AST_MEMSET (RetRootBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    if (nmhGetFsMIPvrstInstPortDesignatedRoot
        ((INT4) u4InstanceId, i4NextPort, &RetRootBridgeId) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /*Designated Bridge  */
    AST_MEMSET (RetBridgeId.pu1_OctetList, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
    if (nmhGetFsMIPvrstInstPortDesignatedBridge
        ((INT4) u4InstanceId, i4NextPort, &RetBridgeId) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /*Designated Port */
    MEMSET (au1PortIdBuf, 0, CLI_RSTP_MAX_PORTID_BUFFER);
    if (nmhGetFsMIPvrstInstPortDesignatedPort
        ((INT4) u4InstanceId, i4NextPort, &RetPortId) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /* Forward Transitions */

    if (nmhGetFsMIPvrstInstPortForwardTransitions
        ((INT4) u4InstanceId, i4NextPort,
         (UINT4 *) &i4FwdTransition) == SNMP_FAILURE)
    {
        AstReleaseContext ();
        return;
    }

    /*BPDUS TRANSMITTED */

    u4TxBpduCount = 0;
    u4TxBpduCount = PvrstCliPortTxBpduCount (u4InstanceId, i4NextPort);

    /*Topology changes received */

    nmhGetFsMIPvrstInstPortTCReceivedCount ((INT4) u4InstanceId, i4NextPort,
                                            &u4TopChRxd);

    /*Topology changes received time stamp */

    nmhGetFsMIPvrstInstPortTCReceivedTimeStamp ((INT4) u4InstanceId, i4NextPort,
                                                &u4TopChRxdTimeStamp);

    /*Topology change detected */

    nmhGetFsMIPvrstInstPortTCDetectedCount ((INT4) u4InstanceId, i4NextPort,
                                            &u4DetecTopCh);

    /*Topology change detected time stamp */

    nmhGetFsMIPvrstInstPortTCDetectedTimeStamp ((INT4) u4InstanceId, i4NextPort,
                                                &u4DetecTopChTimeStamp);

    /*Proposal packets sent */

    nmhGetFsMIPvrstInstPortProposalPktsSent ((INT4) u4InstanceId, i4NextPort,
                                             &u4TxProposCount);

    /*Proposal packets received */

    nmhGetFsMIPvrstInstPortProposalPktsRcvd ((INT4) u4InstanceId, i4NextPort,
                                             &u4RxProposCount);

    /*Proposal packets sent time stamp */

    nmhGetFsMIPvrstInstPortProposalPktSentTimeStamp ((INT4) u4InstanceId,
                                                     i4NextPort,
                                                     &u4TxProposTimeStamp);

    /*Proposal packets received time stamp */

    nmhGetFsMIPvrstInstPortProposalPktRcvdTimeStamp ((INT4) u4InstanceId,
                                                     i4NextPort,
                                                     &u4RxProposTimeStamp);

    /*Agreement packtes sent */

    nmhGetFsMIPvrstInstPortAgreementPktSent ((INT4) u4InstanceId, i4NextPort,
                                             &u4TxAgreementCount);

    /*Agreement packets received */

    nmhGetFsMIPvrstInstPortAgreementPktRcvd ((INT4) u4InstanceId, i4NextPort,
                                             &u4RxAgreementCount);

    /*Agreement packtes sent time stamp */

    nmhGetFsMIPvrstInstPortAgreementPktSentTimeStamp ((INT4) u4InstanceId,
                                                      i4NextPort,
                                                      &u4TxAgreementTimeStamp);

    /*Agreement packets received time stamp */

    nmhGetFsMIPvrstInstPortAgreementPktRcvdTimeStamp ((INT4) u4InstanceId,
                                                      i4NextPort,
                                                      &u4RxAgreementTimeStamp);

    /*Impossible State count */

    nmhGetFsMIPvrstPortImpStateOccurCount (i4NextPort, &u4ImpossibleStateOcc);

    /*Impossible State Timestamp */

    nmhGetFsMIPvrstPortImpStateOccurTimeStamp (i4NextPort,
                                               &u4ImpStateOccTimeStamp);

    /*RcvInfoWhile Count */

    nmhGetFsMIPvrstPortRcvInfoWhileExpCount (i4NextPort, &u4RcvInfoCount);

    /* Rcvinfowhile Timestamp */

    nmhGetFsMIPvrstPortRcvInfoWhileExpTimeStamp (i4NextPort,
                                                 &u4RcvInfoTimeStamp);

    /* BPDUs RECIEVED */

    u4RxBpduCount = 0;
    u4RxBpduCount = PvrstCliPortRxBpduCount (u4InstanceId, i4NextPort);

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    pPvrstPerStRstPortEntry =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (AST_IFENTRY_LOCAL_PORT
                                             (pAstPortEntry), u2InstIndex);

    if (pPvrstPerStRstPortEntry != NULL)
    {
        nmhGetFsMIPvrstInstPortHelloTime ((INT4) u4InstanceId, i4NextPort,
                                          &i4Hello);
        i4Hello = AST_MGMT_TO_SYS (i4Hello);
        nmhGetFsMIPvrstInstPortMaxAge ((INT4) u4InstanceId, i4NextPort,
                                       &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
        nmhGetFsMIPvrstInstPortForwardDelay ((INT4) u4InstanceId, i4NextPort,
                                             &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
        nmhGetFsMIPvrstInstPortHoldTime ((INT4) u4InstanceId, i4NextPort,
                                         &i4Hold);
        i4Hold = AST_MGMT_TO_SYS (i4Hold);
    }
    AstReleaseContext ();
    CliPrintf (CliHandle, "\r\nPort %d [%s] of VLAN %d is ", i4NextPort,
               au1IntfName, u4InstanceId);
    PvrstCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
    CliPrintf (CliHandle, ", ");

    PvrstCliDisplayPortState (CliHandle, i4PortState);
    PvrstCliShowLoopIncState (CliHandle, u4ContextId,
                              (UINT4) i4NextPort, u4InstanceId);

    CliPrintf (CliHandle, "Port PathCost %-9d, ", i4PathCost);
    CliPrintf (CliHandle, "Port Priority %-9d, ", i4Priority);
    CliPrintf (CliHandle, "Port Identifier  %3d.%d \r\n", i4Priority,
               i4NextPort);

    u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);
    PVRST_GET_BRIDGE_PRIORITY (u2RootPrio, u2RootPriority);

    AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
    PrintMacAddress (RetRootBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1RootBrgAddr);
    CliPrintf (CliHandle, "Designated Root has priority %d,", u2RootPriority);
    CliPrintf (CliHandle, " address %s \r\n", au1RootBrgAddr);

    u2BrgPrio = 0;
    u2BrgPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1BrgAddr);
    PVRST_GET_BRIDGE_PRIORITY (u2BrgPrio, u2BrgPriority);
    CliPrintf (CliHandle, "Designated Bridge has priority %d,", u2BrgPriority);
    CliPrintf (CliHandle, " address %s \r\n", au1BrgAddr);

    u2PortPrio = 0;
    u2Val = 0;
    u2PortPrio = RetPortId.pu1_OctetList[0] & (UINT2) AST_PORTPRIORITY_MASK;
    u2Val = AstCliGetPortIdFromOctetList (RetPortId);
    u2Val = u2Val & (UINT2) AST_PORTNUM_MASK;
    CliPrintf (CliHandle, "Designated Port Id is %d.%d,", u2PortPrio, u2Val);

    CliPrintf (CliHandle, " Designated PathCost %d \r\n",
               pPerStPortInfo->u4RootCost);
    CliPrintf (CliHandle,
               "Timers: Hello Time - %d sec %d cs, MaxAge - %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4Hello),
               AST_PROT_TO_BPDU_CENTI_SEC (i4Hello),
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle,
               "Forward Delay - %d sec %d cs, Hold - %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_SEC (i4Hold),
               AST_PROT_TO_BPDU_CENTI_SEC (i4Hold));
    CliPrintf (CliHandle, "No of Transitions to forwarding State :%d\r\n",
               i4FwdTransition);
    /* Loop Guard */
    PvrstCliShowLoopGuardState (CliHandle, u4ContextId,
                                (UINT4) i4NextPort, u4InstanceId);

    CliPrintf (CliHandle, "BPDUs : sent %d ,", u4TxBpduCount);
    CliPrintf (CliHandle, " received %d\r\n", u4RxBpduCount);
    /*Bpdu Guard  */
    if (i4BpduGuard == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Bpdu Guard  is Enabled \r\n");
    }
    else if (i4BpduGuard == AST_BPDUGUARD_DISABLE)
    {
        CliPrintf (CliHandle, "Bpdu Guard  is Disabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Bpdu Guard  is None \r\n");
    }
    /*Root Guard  */
    if (i4RootGuard == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Root Guard  is Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Root Guard  is Disabled \r\n");
    }
    /*Bpdu Filter  */
    if (i4FilterEnabled == AST_BPDUFILTER_ENABLE)
    {
        CliPrintf (CliHandle, "BPDU filter is Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "BPDU filter is Disabled \r\n");
    }
    CliPrintf (CliHandle, "TC detected count                : %d \r\n",
               u4DetecTopCh);
    if (u4DetecTopChTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC detected Timestamp            : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4DetecTopChTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC detected Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "TC received count                : %d \r\n",
               u4TopChRxd);
    if (u4TopChRxdTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC received Timestamp            : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4TopChRxdTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC received Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "RcvInfoWhile expiry count        : %d  \r\n",
               u4RcvInfoCount);
    if (u4RcvInfoTimeStamp == 0)
    {
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4RcvInfoTimeStamp, au1Date);
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Impossible state occurence count : %d \r\n",
               u4ImpossibleStateOcc);
    if (u4ImpStateOccTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Impossible state Timestamp       : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4ImpStateOccTimeStamp, au1Date);
        CliPrintf (CliHandle, "Impossible State Timestamp       : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Proposal BPDUs\r\n");
    CliPrintf (CliHandle, "---------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4TxProposCount);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4RxProposCount);
    if (u4TxProposTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4TxProposTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4RxProposTimeStamp == 0)
    {

        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4RxProposTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Agreement BPDUs\r\n");
    CliPrintf (CliHandle, "----------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4TxAgreementCount);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4RxAgreementCount);
    if (u4TxAgreementTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4TxAgreementTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4RxAgreementTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        PvrstUtilTicksToDate (u4RxAgreementTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliPortTxBpduCount                            */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of transmitted RSTP  */
/*                        config  and TCN BPDU s count                       */
/*                                                                           */
/*     INPUT            : u4InstanceId- Instance Identifier                  */
/*                        i4NextPort - PortIndex                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4TxBpduCount                                      */
/*****************************************************************************/

UINT4
PvrstCliPortTxBpduCount (UINT4 u4InstanceId, INT4 i4NextPort)
{
    UINT4               u4Count = (UINT4) AST_INIT_VAL;
    UINT4               u4TxBpduCount = (UINT4) AST_INIT_VAL;

    /* Rst BPDU */
    nmhGetFsMIPvrstInstPortTransmittedBpdus ((INT4) u4InstanceId, i4NextPort,
                                             &u4Count);
    u4TxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIPvrstInstPortTxConfigBpduCount ((INT4) u4InstanceId, i4NextPort,
                                              &u4Count);
    u4TxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIPvrstInstPortTxTcnBpduCount ((INT4) u4InstanceId, i4NextPort,
                                           &u4Count);
    u4TxBpduCount += u4Count;

    return u4TxBpduCount;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliPortRxBpduCount                            */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of received  RSTP    */
/*                        config  and TCN BPDU s count                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4InstanceId- Instance Identifier                  */
/*                        i4NextPort - PortIndex                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : u4RxBpduCount                                      */
/*****************************************************************************/

UINT4
PvrstCliPortRxBpduCount (UINT4 u4InstanceId, INT4 i4NextPort)
{
    UINT4               u4Count = AST_INIT_VAL;
    UINT4               u4RxBpduCount;

    /* Rst BPDU */
    nmhGetFsMIPvrstInstPortReceivedBpdus ((INT4) u4InstanceId, i4NextPort,
                                          &u4Count);
    u4RxBpduCount = u4Count;

    /* Configrn BPDUs */
    nmhGetFsMIPvrstInstPortRxConfigBpduCount ((INT4) u4InstanceId, i4NextPort,
                                              &u4Count);
    u4RxBpduCount += u4Count;

    /* TCN BPDUs */
    nmhGetFsMIPvrstInstPortRxTcnBpduCount ((INT4) u4InstanceId, i4NextPort,
                                           &u4Count);
    u4RxBpduCount += u4Count;

    return u4RxBpduCount;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayPortRole                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the PVRST Port Roles       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4Val- Integer Value for the Role                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
PvrstCliDisplayPortRole (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val)
{

    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        switch (i4Val)
        {
            case AST_PORT_ROLE_DISABLED:
                CliPrintf (CliHandle, "Disabled  ");
                break;
            case AST_PORT_ROLE_ALTERNATE:
                CliPrintf (CliHandle, "Alternate ");
                break;
            case AST_PORT_ROLE_BACKUP:
                CliPrintf (CliHandle, "Back Up   ");
                break;
            case AST_PORT_ROLE_ROOT:
                CliPrintf (CliHandle, "Root      ");
                break;
            case AST_PORT_ROLE_DESIGNATED:
                CliPrintf (CliHandle, "Designated");
                break;
            default:
                CliPrintf (CliHandle, "Unknown   ");
                break;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliDisplayPortState                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the   PVRST                */
/*                         port States                                       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4Val- Integer Value for the State                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
PvrstCliDisplayPortState (tCliHandle CliHandle, INT4 i4Val)
{

    switch (i4Val)
    {
        case AST_PORT_STATE_DISCARDING:
            CliPrintf (CliHandle, "Discarding");
            break;
        case AST_PORT_STATE_LEARNING:
            CliPrintf (CliHandle, "Learning  ");
            break;
        case AST_PORT_STATE_FORWARDING:
            CliPrintf (CliHandle, "Forwarding");
            break;
        default:
            CliPrintf (CliHandle, "Unknown   ");
            break;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstBpduguardDisable                              */
/*                                                                           */
/*     DESCRIPTION      : This function disables bpdu-guard on the given     */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstBpduguardDisable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstBpduGuard (&u4ErrCode,
                                   i4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u4Value == 2)
    {
        CliPrintf (CliHandle, "PvrstBpduguard Disabled \r\n");
    }
    else if (u4Value == 0)
    {
        CliPrintf (CliHandle, "PvrstBpduguard Removed\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstBpduguardEnable                               */
/*                                                                           */
/*     DESCRIPTION      : This function enables bpdu-guard on the given      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstBpduguardEnable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,
                      UINT1 u1BpduGuardAction)
{
    UINT4               u4ErrCode = 0;

    if (u1BpduGuardAction == AST_INIT_VAL)
    {
        /*Default BPDU Guard Action is Disabling Spanning-tree */
        u1BpduGuardAction = AST_PORT_STATE_DISABLED;
    }

    if (nmhTestv2FsPvrstBpduGuard (&u4ErrCode, i4Index,
                                   u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2FsPvrstPortBpduGuardAction (&u4ErrCode, i4Index,
                                             (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPvrstBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstPortBpduGuardAction (i4Index, (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "PvrstBpduguard Enabled \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstGuardRoot                                     */
/*                                                                           */
/*     DESCRIPTION      : This function enables root-guard on the given      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstGuardRoot (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstRootGuard (&u4ErrCode, i4Index,
                                   AST_SNMP_TRUE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstRootGuard (i4Index, AST_SNMP_TRUE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst RootGuard is Enabled \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstNoGuard                                       */
/*                                                                           */
/*     DESCRIPTION      : This function disables root-guard on the given     */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstNoGuard (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstRootGuard (&u4ErrCode, i4Index,
                                   AST_SNMP_FALSE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstRootGuard (i4Index, AST_SNMP_FALSE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst RootGuard is disabled  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstEncapDot1q                                    */
/*                                                                           */
/*     DESCRIPTION      : This function enables Dot1q encapsulation on the   */
/*                        given interface                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstEncapDot1q (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPvrstEncapType (&u4ErrorCode, i4Index,
                                   PVRST_DOT1Q) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstEncapType (i4Index, PVRST_DOT1Q) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst Encapsulation Dot1q is set.  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstEncapISL                                      */
/*                                                                           */
/*     DESCRIPTION      : This function enables ISL encapsulation on the     */
/*                        given interface                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstEncapISL (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPvrstEncapType (&u4ErrorCode, i4Index,
                                   PVRST_ISL) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstEncapType (i4Index, PVRST_ISL) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst Encapsulation ISL is set.  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstSetPortProp                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the properties for an Interface */
/*                        independent of instances                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        u4PortNum - PortNumber                             */
/*                        u1Flag    - Flag to Check which property to be set */
/*                        u4Val     - PVRST Property                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetPortProp (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4CmdType,
                  UINT4 u4Val)
{
    UINT4               u4ErrCode = 0;
    UINT1               u1OperStatus;

    if (u4CmdType == AST_PORT_STATE_DISABLED)
    {
        if (nmhTestv2FsPvrstPortEnabledStatus (&u4ErrCode, u4PortNum,
                                               u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPvrstPortEnabledStatus (u4PortNum, u4Val) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4CmdType == STP_LINK_TYPE)
    {
        if (nmhTestv2FsPvrstPortAdminPointToPoint
            (&u4ErrCode, u4PortNum, u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPvrstPortAdminPointToPoint (u4PortNum, u4Val) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4CmdType == STP_PORTFAST)
    {
        if (nmhTestv2FsPvrstPortAdminEdgeStatus (&u4ErrCode, u4PortNum,
                                                 u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPvrstPortAdminEdgeStatus (u4PortNum, u4Val) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        AstL2IwfGetPortOperStatus (STP_MODULE, u4PortNum, &u1OperStatus);

        if (u4Val != AST_SNMP_FALSE)
        {

            CliPrintf (CliHandle,
                       "\rWarning: portfast should only be enabled on ports "
                       "connected to a single host.\r\nConnecting hubs, concentrators, "
                       "switches, bridges, etc... to this interface\r\nwhen portfast "
                       "is enabled, can cause temporary bridging loops.\r\n"
                       "Use with CAUTION\r\n\r\n");

            if (u1OperStatus != CFA_IF_DOWN)
            {
                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will  "
                           "have effect \r\nonly when the interface is shutdown\r\n");
            }
        }
        else if (u4Val == AST_SNMP_FALSE)
        {
            if (u1OperStatus != CFA_IF_DOWN)
            {
                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will  "
                           "have effect  \r\nonly when the interface is shutdown\r\n");
            }
        }
    }
    /* Validating Parameters and setting PortPathCost */
    else if ((u4CmdType == STP_PORT_COST) || (u4CmdType == STP_NO_PORT_COST))
    {
        CLI_SET_ERR (CLI_STP_SPECIFY_VLAN_ERR);
        return CLI_FAILURE;
    }
    /* Validating Parameters and setting PortPriority */
    else if ((u4CmdType == STP_PORT_PRIORITY))
    {
        CLI_SET_ERR (CLI_STP_SPECIFY_VLAN_ERR);
        return CLI_FAILURE;

    }
    else if ((u4CmdType == CLI_AST_ENABLED) || (u4CmdType == CLI_AST_DISABLED))
    {

        if (nmhTestv2FsPvrstPortRowStatus (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rUnable to create/delete PVRST port.\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsPvrstPortRowStatus (u4PortNum, u4Val) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstInstStatusOnPort                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets Instance status of the given    */
/*                        instance                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index   - Port Index                             */
/*                        u4Instance- Instance Index                         */
/*                        u4Value   - Status to be set                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstInstStatusOnPort (tCliHandle CliHandle, UINT4 u4Index,
                       UINT4 u4Instance, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstInstPortEnableStatus (&u4ErrCode, (INT4) u4Instance,
                                              u4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstInstPortEnableStatus ((INT4) u4Instance, u4Index, u4Value)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst Instance Status is updated.  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstVlanPortPriority                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets port priority of the given      */
/*                        instance                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index   - Port Index                             */
/*                        u4Instance- Instance Index                         */
/*                        u4Value   - Value to be set                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstVlanPortPriority (tCliHandle CliHandle, UINT4 u4Index,
                       UINT4 u4Instance, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstInstPortPriority
        (&u4ErrCode, (INT4) u4Instance, u4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstInstPortPriority ((INT4) u4Instance, u4Index, u4Value)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst Vlan Port Priority is set  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstVlanCost                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets port cost for the given         */
/*                        instance                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index   - Port Index                             */
/*                        u4Instance- Instance Index                         */
/*                        u4Value   - Value to be set                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstVlanCost (tCliHandle CliHandle, UINT4 u4Index,
               UINT4 u4Instance, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPvrstInstPortAdminPathCost
        (&u4ErrCode, (INT4) u4Instance, u4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstInstPortAdminPathCost ((INT4) u4Instance, u4Index, u4Value)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst Vlan Cost is set  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  PvrstCliSetPathCostCalculation                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathcostCalc-PathCostCalcMethod                  */
/*                        (Dynamic/Manual)                                   */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCliSetPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc)
{
    UINT4               u4ErrCode = 0;

    if (u4PathcostCalc == AST_PATHCOST_CALC_DYNAMIC)
    {
        u4PathcostCalc = AST_SNMP_TRUE;
    }
    else if (u4PathcostCalc == AST_PATHCOST_CALC_MANUAL)
    {
        u4PathcostCalc = AST_SNMP_FALSE;
    }
    if (nmhTestv2FsPvrstDynamicPathCostCalculation (&u4ErrCode,
                                                    u4PathcostCalc) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstDynamicPathCostCalculation (u4PathcostCalc)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "Pvrst PathCost Calculation is set  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  PvrstCliSetLaggPathCostCalculation                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                        for Link Aggregated port                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathcostCalc-Enable/Disable                      */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCliSetLaggPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPvrstCalcPortPathCostOnSpeedChg (&u4ErrCode,
                                                    u4PathcostCalc) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsPvrstCalcPortPathCostOnSpeedChg (u4PathcostCalc);

    CliPrintf (CliHandle, "Pvrst Lagg PathCost Calculation is set  \r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstSetDebug                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Debugging support           */
/*                        for the PVRST                                      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  - Handle to the CLI Context            */
/*                        u4RstDebug  - Debug Value                          */
/*                        u1Action    - Set/No Command                       */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstSetDebug (tCliHandle CliHandle, UINT4 u4RstDbg, UINT1 u1Action)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4DbgVal = 0;

    if (u1Action == PVRST_NO_CMD)
    {
        /* For Debug Disable, Get the existing Debug Value 
         * and negate that value
         */
        nmhGetFsPvrstDebug ((INT4 *) &u4DbgVal);
        u4DbgVal = u4DbgVal & (~u4RstDbg);
    }
    else if (u1Action == PVRST_SET_CMD)
    {
        u4DbgVal = u4RstDbg;
    }

    if (nmhTestv2FsPvrstDebug (&u4ErrCode, u4DbgVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsPvrstDebug (u4DbgVal);

    CliPrintf (CliHandle, "\r Debug Support in PVRST is updated.\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstSetPortAutoEdge                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the auto edge status for a port.*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        u4Status - enable/disable                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/
INT4
PvrstSetPortAutoEdge (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPvrstPortAutoEdge
        (&u4ErrCode, (INT4) u4PortNum, (INT4) u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsPvrstPortAutoEdge ((INT4) u4PortNum, (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstShowRunningConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  PVRST Configuration   */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Module)
{

    UINT4               u4SysMode = (UINT4) AST_INIT_VAL;

    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    u4SysMode = AstVcmGetSystemMode (AST_PROTOCOL_ID);

    if (PvrstShowRunningConfigScalars (CliHandle, u4ContextId) == CLI_SUCCESS)
    {
        PvrstShowRunningConfigTables (CliHandle, u4ContextId);

        if (u4SysMode == VCM_MI_MODE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }

        if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG)
        {
            PvrstShowRunningConfigInterface (CliHandle, u4ContextId);
        }
    }
    else
    {
        if (u4SysMode == VCM_MI_MODE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }
    }

    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigScalars                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in pvrst for */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4SysMode = (UINT4) AST_INIT_VAL;
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];
    INT4                i4ModStatus = (INT4) AST_INIT_VAL;
    INT4                i4SystemControl = (INT4) AST_INIT_VAL;
    INT4                i4DynamicPathcostCalc = (INT4) AST_SNMP_FALSE;
    INT4                i4DynPathCostCalcSpeedChng = (INT4) AST_SNMP_FALSE;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4Version = (INT4) AST_VERSION_0;
    INT4                i4FlushInterval = AST_INIT_VAL;

    /*SystemControl */
    nmhGetFsMIPvrstSystemControl (i4ContextId, &i4SystemControl);
    nmhGetFsMIPvrstModuleStatus (i4ContextId, &i4ModStatus);
    /*Dynamic Pathcost calculation */

    nmhGetFsMIPvrstDynamicPathCostCalculation (i4ContextId,
                                               &i4DynamicPathcostCalc);
    nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg (i4ContextId,
                                               &i4DynPathCostCalcSpeedChng);
    nmhGetFsMIPvrstGlobalBpduGuard (i4ContextId, &i4BpduGuard);
    u4SysMode = AstVcmGetSystemModeExt (AST_PROTOCOL_ID);

    if (i4SystemControl == PVRST_SNMP_SHUTDOWN)
    {
        return CLI_FAILURE;
    }
    else if (i4SystemControl == PVRST_SNMP_START)
    {
        if (u4SysMode == VCM_MI_MODE)
        {
            AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

            AstVcmGetAliasName (u4ContextId, au1ContextName);
            if (STRLEN (au1ContextName) != 0)
            {

                CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
            }

        }
        CliPrintf (CliHandle, "spanning-tree mode pvrst\r\n");
    }

    nmhGetFsMIPvrstModuleStatus (i4ContextId, &i4ModStatus);

    /*Module Status */

    if (i4ModStatus != PVRST_ENABLED)
    {
        CliPrintf (CliHandle, "no spanning-tree\r\n");
    }

    nmhGetFsMIPvrstForceProtocolVersion (i4ContextId, &i4Version);
    if (AST_VERSION_0 == i4Version)
    {
        CliPrintf (CliHandle, "spanning-tree compatibility stp\r\n");
    }

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((AST_BRIDGE_MODE () == AST_PROVIDER_EDGE_BRIDGE_MODE) ||
        (AST_BRIDGE_MODE () == AST_PROVIDER_CORE_BRIDGE_MODE))
    {
        RstpPbPrintModuleStatus (CliHandle, u4ContextId);
    }

    AstReleaseContext ();

    /*Dynamic Pathcost calculation */

    nmhGetFsMIPvrstDynamicPathCostCalculation (i4ContextId,
                                               &i4DynamicPathcostCalc);
    nmhGetFsMIPvrstCalcPortPathCostOnSpeedChg (i4ContextId,
                                               &i4DynPathCostCalcSpeedChng);
    if (i4DynamicPathcostCalc == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic\r\n");
    }

    if (i4DynPathCostCalcSpeedChng == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic lag-speed\r\n");
    }

    nmhGetFsMIPvrstGlobalBpduGuard (i4ContextId, &i4BpduGuard);
    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard enable\r\n");
    }

    /* Flush interval duration */
    nmhGetFsMIPvrstFlushInterval (i4ContextId, &i4FlushInterval);

    if (i4FlushInterval != PVRST_DEFAULT_FLUSH_INTERVAL)
    {
        CliPrintf (CliHandle, "spanning-tree flush-interval %d\r\n",
                   i4FlushInterval);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigTables                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays instance to vlan mapping    */
/*                        objects scanning the Pvrst table                   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
PvrstShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4NextInst = (INT4) AST_INIT_VAL;
    INT4                i4CurrentInst = (INT4) AST_INIT_VAL;
    INT4                i4BridgePriority = (INT4) AST_INIT_VAL;
    INT4                i4BridgeMaxAge = (INT4) AST_INIT_VAL;
    INT4                i4BridgeHelloTime = (INT4) AST_INIT_VAL;
    INT4                i4BridgeForwardDelay = (INT4) AST_INIT_VAL;
    INT4                i4BridgeHoldCount = (INT4) AST_INIT_VAL;
    INT1                i1OutCome = (INT1) AST_INIT_VAL;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4NextContextId;
    INT4                i4Threshold = (INT4) AST_INIT_VAL;
    /*Instance Specific Priority */

    i1OutCome =
        nmhGetFirstIndexFsMIPvrstInstBridgeTable (&i4NextContextId,
                                                  &i4NextInst);
    while (i1OutCome != SNMP_FAILURE)
    {

        nmhGetFsMIPvrstInstBridgePriority (i4NextContextId, i4NextInst,
                                           &i4BridgePriority);
        nmhGetFsMIPvrstInstBridgeMaxAge (i4NextContextId, i4NextInst,
                                         &i4BridgeMaxAge);
        nmhGetFsMIPvrstInstFlushIndicationThreshold (i4NextContextId,
                                                     i4NextInst, &i4Threshold);
        nmhGetFsMIPvrstInstBridgeHelloTime (i4NextContextId, i4NextInst,
                                            &i4BridgeHelloTime);
        nmhGetFsMIPvrstInstBridgeForwardDelay (i4NextContextId, i4NextInst,
                                               &i4BridgeForwardDelay);
        nmhGetFsMIPvrstInstTxHoldCount (i4NextContextId, i4NextInst,
                                        &i4BridgeHoldCount);
        if (i4BridgePriority != (PVRST_DEFAULT_BRG_PRIORITY + i4NextInst))
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d brg-priority %d\r\n",
                       i4NextInst, (i4BridgePriority - i4NextInst));
        }
        if (i4Threshold != PVRST_DEFAULT_FLUSH_IND_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d flush-indication-threshold %d\r\n",
                       i4NextInst, i4Threshold);
        }

        i4BridgeMaxAge = AST_MGMT_TO_SYS (i4BridgeMaxAge);
        if ((i4BridgeMaxAge) != PVRST_DEFAULT_BRG_MAX_AGE)
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d max-age %d\r\n",
                       i4NextInst, (i4BridgeMaxAge / AST_CENTI_SECONDS));
        }
        i4BridgeHelloTime = AST_MGMT_TO_SYS (i4BridgeHelloTime);
        if ((i4BridgeHelloTime) != PVRST_DEFAULT_BRG_HELLO_TIME)
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d hello-time %d\r\n",
                       i4NextInst, (i4BridgeHelloTime / AST_CENTI_SECONDS));
        }

        i4BridgeForwardDelay = AST_MGMT_TO_SYS (i4BridgeForwardDelay);
        if ((i4BridgeForwardDelay) != PVRST_DEFAULT_BRG_FWD_DELAY)
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d forward-time %d\r\n",
                       i4NextInst, (i4BridgeForwardDelay / AST_CENTI_SECONDS));
        }
        if (i4BridgeHoldCount != PVRST_DEFAULT_BRG_TX_LIMIT)
        {
            CliPrintf (CliHandle,
                       "spanning-tree vlan %d hold-count %d\r\n",
                       i4NextInst, i4BridgeHoldCount);
        }
        i4CurrentInst = i4NextInst;
        i1OutCome =
            nmhGetNextIndexFsMIPvrstInstBridgeTable (i4ContextId,
                                                     &i4NextContextId,
                                                     i4CurrentInst,
                                                     &i4NextInst);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigInterface                    */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface table for  Pvrst */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tAstCfaIfInfo       CfaIfInfo;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               pu1Flag;
    INT4                i4Val = 0;

    AST_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while ((i4OutCome != RST_FAILURE)
           && (i4NextPort <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))

    {
        if (AstCfaGetIfInfo (i4NextPort, &CfaIfInfo) != CFA_SUCCESS)
        {
            return;
        }

        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        pu1Flag = AST_FALSE;
        PvrstShowRunningConfigInterfaceDetails (CliHandle, i4NextPort,
                                                &pu1Flag);
        PvrstShowRunningConfigInstInterface (CliHandle, i4NextPort, &pu1Flag);

        CliRegisterLock (CliHandle, AstLock, AstUnLock);
        AST_LOCK ();
        if (pu1Flag == AST_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);
    }
    return;
    UNUSED_PARAM (i4Val);
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigInstInterface                */
/*                                                                           */
/*     DESCRIPTION      : This function scans the Instance specific interface*/
/*                        table in PVRST                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
PvrstShowRunningConfigInstInterface (tCliHandle CliHandle, INT4 i4Index,
                                     UINT1 *pu1Flag)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = (UINT4) AST_INIT_VAL;
    UINT2               u2LocalPortId = (UINT2) AST_INIT_VAL;
    UINT4               u4InstanceId = (UINT4) AST_INIT_VAL;
    UINT2               u2InstIndex = (UINT2) AST_INIT_VAL;
    INT4                i4Retval = RST_FAILURE;

    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    i4Retval =
        AstGetContextInfoFromIfIndex (i4Index, &u4ContextId, &u2LocalPortId);

    UNUSED_PARAM (i4Retval);
    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
        SNMP_SUCCESS)
    {
        pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);

        if (pAstPortEntry == NULL)
        {
            AST_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }
        for (u4InstanceId = 1; u4InstanceId <= VLAN_MAX_VLAN_ID; u4InstanceId++)
        {
            if (AstL2IwfMiIsVlanActive (u4ContextId, (tVlanId) u4InstanceId)
                == OSIX_FALSE)
            {
                continue;
            }
            u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                        (tVlanId) u4InstanceId);
            if (u2InstIndex == INVALID_INDEX)
            {
                break;
            }
            if (AstSelectContext (u4ContextId) == RST_FAILURE)
            {
                break;
            }
            if ((PVRST_GET_PERST_INFO (u2InstIndex) != NULL)
                && (PVRST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex) !=
                    NULL))

            {
                if (nmhValidateIndexInstanceFsMIPvrstInstPortTable
                    ((INT4) u4InstanceId, i4Index) != SNMP_FAILURE)
                {
                    AST_UNLOCK ();
                    PvrstShowRunningConfigInstInterfaceDetails (CliHandle,
                                                                i4Index,
                                                                u4InstanceId,
                                                                pu1Flag);
                    AST_LOCK ();
                }
            }
        }
    }

    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigInstInterfaceDetails         */
/*                                                                           */
/*     DESCRIPTION      : This function displays Instance specific interface */
/*                        objects in PVRST                                   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
PvrstShowRunningConfigInstInterfaceDetails (tCliHandle CliHandle,
                                            INT4 i4InstPort, INT4 i4Inst,
                                            UINT1 *pu1Flag)
{
    INT4                i4PortEnableStatus = (INT4) AST_INIT_VAL;
    INT4                i4PortPriority = (INT4) AST_INIT_VAL;
    INT4                i4PortPathCost = (INT4) AST_INIT_VAL;
    INT4                i4PortStatus = (INT4) AST_INIT_VAL;
    INT4                i4Status = 0;
    UINT4               u4ContextId = (UINT4) AST_INIT_VAL;
    UINT4               u4IcclIfIndex = (UINT4) AST_INIT_VAL;
    UINT2               u2LocalPortId = (UINT2) AST_INIT_VAL;
    INT4                i4Retval = RST_FAILURE;
    UINT1               u1PathCostSet = AST_SNMP_FALSE;

    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    i4Retval =
        AstGetContextInfoFromIfIndex (i4InstPort, &u4ContextId, &u2LocalPortId);

    UNUSED_PARAM (i4Retval);
    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    nmhGetFsMIPvrstPortEnabledStatus (i4InstPort, &i4PortStatus);
    if (i4PortStatus == AST_SNMP_TRUE)
    {
        nmhGetFsMIPvrstInstPortEnableStatus (i4Inst, i4InstPort,
                                             &i4PortEnableStatus);

    }
    if (PvrstIsPathcostConfigured ((UINT2) i4InstPort, (UINT2) i4Inst)
        == PVRST_TRUE)
    {
        u1PathCostSet = AST_SNMP_TRUE;
    }

    /*PortPriority */
    nmhGetFsMIPvrstInstPortPriority (i4Inst, i4InstPort, &i4PortPriority);

    AstIcchGetIcclIfIndex (&u4IcclIfIndex);

    if (((i4PortPriority != PVRST_DEFAULT_PORT_PRIORITY)
         || (u1PathCostSet == AST_SNMP_TRUE)
         || (i4PortEnableStatus != AST_SNMP_TRUE)) && (*pu1Flag == AST_FALSE)
        && (u4IcclIfIndex != (UINT4) i4InstPort))
    {
        CfaCliConfGetIfName ((UINT4) i4InstPort, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        *pu1Flag = AST_TRUE;
    }

    if (i4PortPriority != PVRST_DEFAULT_PORT_PRIORITY)
    {
        CliPrintf (CliHandle,
                   "spanning-tree vlan %d port-priority %d\r\n",
                   i4Inst, i4PortPriority);
    }

    if (u1PathCostSet == AST_SNMP_TRUE)
    {
        /*PortPathCost */
        nmhGetFsMIPvrstInstPortAdminPathCost (i4Inst, i4InstPort,
                                              &i4PortPathCost);
        CliPrintf (CliHandle, "spanning-tree vlan %d cost %d\r\n",
                   i4Inst, i4PortPathCost);
    }
    nmhGetFsMIPvrstPortEnabledStatus (i4InstPort, &i4Status);
    /*When spanning-tree is disabled for the port no need to display spanning-tree
     *instance status as disabled*/
    if (i4Status == AST_SNMP_TRUE)
    {
        if ((i4PortEnableStatus != AST_SNMP_TRUE)
            && (u4IcclIfIndex != (UINT4) i4InstPort))
        {
            CliPrintf (CliHandle, "spanning-tree vlan %d status disable\r\n",
                       i4Inst);
        }
    }
    if (i4PortPriority != PVRST_DEFAULT_PORT_PRIORITY)
    {
        CliPrintf (CliHandle,
                   "spanning-tree vlan %d port-priority %d\r\n",
                   i4Inst, i4PortPriority);
    }

    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstDisplayInterfacePortFast                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface Port Fast Status  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstDisplayInterfacePortFast (tCliHandle CliHandle, UINT4 u4ContextId,
                               INT4 i4Index)
{
    INT4                i4EdgePort = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    /* Needed this check when called from Cfa */

    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
            SNMP_SUCCESS)
        {
            if (i4Index != 0)
            {
                pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
                if (pAstPortEntry == NULL)
                {
                    return;
                }
            }

            /*EdgePort */
            nmhGetFsMIPvrstPortAdminEdgeStatus (i4Index, &i4EdgePort);
            if (i4EdgePort != AST_SNMP_FALSE)
            {
                CliPrintf (CliHandle, "spanning-tree portfast is Enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "spanning-tree portfast is Disabled\r\n");
            }
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstDisplayInterfaceBpduGuard                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays Bpdu Guard Status on the    */
/*                        Interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
        SNMP_SUCCESS)
    {
        pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
        if (pAstPortEntry == NULL)
        {
            return;
        }

        nmhGetFsMIPvrstBpduGuard (i4Index, &i4BpduGuard);
        /*Bpdu Guard  */
        if (i4BpduGuard == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is Enabled \r\n");
        }
        else if (i4BpduGuard == AST_BPDUGUARD_DISABLE)
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is Disabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is None \r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstDisplayInterfaceRootGuard                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays Root Guard Status on the    */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstDisplayInterfaceRootGuard (tCliHandle CliHandle, UINT4 u4ContextId,
                                INT4 i4Index)
{
    INT4                i4RootGuard = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
            SNMP_SUCCESS)
        {
            if (i4Index != 0)
            {
                pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
                if (pAstPortEntry == NULL)
                {
                    return;
                }
            }

            nmhGetFsMIPvrstRootGuard (i4Index, &i4RootGuard);

            /*Root Guard  */
            if (i4RootGuard == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nRoot Guard  is Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRoot Guard  is Disabled \r\n");
            }
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstDisplayInterfaceEncapType                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays  Encap Type Status on the   */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstDisplayInterfaceEncapType (tCliHandle CliHandle, UINT4 u4ContextId,
                                INT4 i4Index)
{
    INT4                i4EncapType = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
            SNMP_SUCCESS)
        {
            if (i4Index != 0)
            {
                pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
                if (pAstPortEntry == NULL)
                {
                    return;
                }
            }
            nmhGetFsMIPvrstEncapType (i4Index, &i4EncapType);

            if (i4EncapType == PVRST_ISL)
            {
                CliPrintf (CliHandle, "\r\nEncapsulation Type is ISL \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nEncapsulation Type is Dot1q \r\n");
            }
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstShowRunningConfigInterfaceDetails             */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in Pvrst  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index,
                                        UINT1 *pu1Flag)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4LinkType = (INT4) AST_INIT_VAL;
    INT4                i4AdminEdgeStatus = (INT4) AST_INIT_VAL;
    INT4                i4PortStatus = (INT4) AST_INIT_VAL;
    INT4                i4RootGuard = (INT4) AST_INIT_VAL;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4BpduFilter = (INT4) AST_INIT_VAL;
    INT4                i4EncapType = (INT4) AST_INIT_VAL;
    INT4                i4LoopGuard = (INT4) AST_INIT_VAL;
    UINT4               u4ContextId = (UINT4) AST_INIT_VAL;
    UINT4               u4IcclIfIndex = (UINT4) AST_INIT_VAL;
    UINT2               u2LocalPortId = (UINT2) AST_INIT_VAL;
    UINT1               u1PbPortType = VLAN_INVALID_PROVIDER_PORT;
    UINT1               u1LinkTypeP2P = AST_SNMP_FALSE;
    INT4                i4Retval = RST_FAILURE;
    INT4                i4AutoEdge = PVRST_FALSE;
    INT4                i4BpduRx = (INT4) AST_INIT_VAL;
    INT4                i4BpduTx = (INT4) AST_INIT_VAL;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4BpduGuardAction = AST_INIT_VAL;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    /* Needed this check when called from Cfa */
    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    i4Retval =
        AstGetContextInfoFromIfIndex ((UINT4) i4Index, &u4ContextId,
                                      &u2LocalPortId);

    /* Needed this check when called from Cfa */
    if (!AstIsPvrstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Get the Bridge Port-type */
    AstGetPbPortType ((UINT4) i4Index, &u1PbPortType);

    if (nmhValidateIndexInstanceFsMIFuturePvrstPortTable (i4Index) ==
        SNMP_SUCCESS)
    {
        if (i4Index != 0)
        {
            pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
            if (pAstPortEntry == NULL)
            {
                AST_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return;
            }
        }

        nmhGetFsMIPvrstPortAdminEdgeStatus (i4Index, &i4AdminEdgeStatus);
        nmhGetFsMIPvrstPortEnabledStatus (i4Index, &i4PortStatus);
        nmhGetFsMIPvrstRootGuard (i4Index, &i4RootGuard);
        nmhGetFsMIPvrstBpduGuard (i4Index, &i4BpduGuard);
        nmhGetFsMIPvrstBpduFilter (i4Index, &i4BpduFilter);

        nmhGetFsMIPvrstEncapType (i4Index, &i4EncapType);
        /*LinkType */
        nmhGetFsMIPvrstPortAdminPointToPoint (i4Index, &i4LinkType);

        if (i4LinkType != PVRST_P2P_AUTO)
        {
            if (i4LinkType == PVRST_P2P_FORCETRUE)
            {
                u1LinkTypeP2P = AST_SNMP_TRUE;

            }
        }
        /* Loop Guard */
        nmhGetFsMIPvrstPortLoopGuard (i4Index, &i4LoopGuard);

        /*Auto Edge */
        nmhGetFsMIPvrstPortAutoEdge (i4Index, &i4AutoEdge);
        nmhGetFsMIPvrstPortEnableBPDURx (i4Index, &i4BpduRx);
        nmhGetFsMIPvrstPortEnableBPDUTx (i4Index, &i4BpduTx);

        AstIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (((i4AdminEdgeStatus == AST_SNMP_TRUE)
             || (i4PortStatus != AST_SNMP_TRUE)
             || (i4RootGuard == AST_SNMP_TRUE)
             || (i4BpduGuard != AST_BPDUGUARD_NONE)
             || (i4BpduFilter == AST_BPDUFILTER_ENABLE)
             || (i4EncapType == PVRST_ISL) || (u1LinkTypeP2P == AST_SNMP_TRUE)
             || (i4LoopGuard == PVRST_TRUE)
             || (i4AutoEdge != PVRST_DEFAULT_AUTOEDGE_VALUE)
             || (i4BpduRx == AST_SNMP_FALSE) || (i4BpduTx == AST_SNMP_FALSE))
            && (*pu1Flag == AST_FALSE) && (u4IcclIfIndex != (UINT4) i4Index))

        {
            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            *pu1Flag = AST_TRUE;

        }

        if (i4AdminEdgeStatus == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree portfast\r\n");
        }
        if ((i4PortStatus != AST_SNMP_TRUE)
            && (u4IcclIfIndex != (UINT4) i4Index))
        {
            CliPrintf (CliHandle, "spanning-tree disable\r\n");
        }

        if (i4RootGuard == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree guard root\r\n");
        }

        if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
        {
            nmhGetFsMIPvrstPortBpduGuardAction (i4Index, &i4BpduGuardAction);
            if (i4BpduGuardAction == AST_PORT_OPER_DOWN)
            {
                CliPrintf (CliHandle,
                           "spanning-tree bpduguard enable admin-down\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "spanning-tree bpduguard enable\r\n");
            }
        }

        if (i4BpduFilter == AST_BPDUFILTER_ENABLE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdufilter enable\r\n");
        }
        if (i4BpduRx == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-receive disabled\r\n");
        }

        if (i4BpduTx == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-transmit disabled\r\n");
        }

        if (i4EncapType == PVRST_ISL)
        {
            CliPrintf (CliHandle, "spanning-tree encap ISL\r\n");
        }

        if (u1LinkTypeP2P == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree link-type point-to-point\r\n");
        }

        if (i4LoopGuard == PVRST_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree guard loop\r\n");
        }

        if (i4AutoEdge != PVRST_DEFAULT_AUTOEDGE_VALUE)
        {
            CliPrintf (CliHandle, "no spanning-tree auto-edge\r\n");
        }
    }
    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    UNUSED_PARAM (i4Retval);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetBpduGuardStatus                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the global  status(             */
/*                        Enabled/Disabled) of the BPDU Guard Feature.       */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4GblBpduGuardStatus -PVRST Module Status.         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliSetBpduGuardStatus (tCliHandle CliHandle, UINT4 u4GblBpduGuardStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPvrstGlobalBpduGuard
        (&u4ErrCode, (INT4) u4GblBpduGuardStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsPvrstGlobalBpduGuard ((INT4) u4GblBpduGuardStatus) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetPortLoopGuard                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Loop Guard status           */
/*                        on an interface                                    */
/*                                                                           */
/*     INPUT            : CliHandle- handle to the CLI Context               */
/*                        PortNum - Local Port ID value                      */
/*                        Value -PVRST Loop guard value.                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PvrstCliSetPortLoopGuard (tCliHandle CliHandle,
                          UINT2 u2PortNum, INT4 i4LoopGuardStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMIPvrstPortLoopGuard (&u4ErrCode, u2PortNum,
                                         i4LoopGuardStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMIPvrstPortLoopGuard ((INT4) u2PortNum,
                                      i4LoopGuardStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowLoopIncState                           */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Loop-Inconsistent status   */
/*                        on an interface                                    */
/*                                                                           */
/*     INPUT            : CliHandle- handle to the CLI Context               */
/*                        PortNum - Interface Index                          */
/*                        InstanceId -VLAN Instance number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
VOID
PvrstCliShowLoopIncState (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PortNum, UINT4 u4InstanceId)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    if ((pAstPortEntry = AstGetIfIndexEntry (u4PortNum)) == NULL)
    {
        AstReleaseContext ();
        return;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                (UINT2) u4InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        AstReleaseContext ();
        return;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        AstReleaseContext ();
        return;
    }
    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                   u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return;
    }

    if (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE)
    {
        CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }

    AstReleaseContext ();
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliShowLoopGuardState                         */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Loop Guard status          */
/*                        on an interface, Per VLAN instance                 */
/*                                                                           */
/*     INPUT            : CliHandle- handle to the CLI Context               */
/*                        ContextId - Virtual Context Id                     */
/*                        PortNum - Interface Index                          */
/*                        InstanceId -VLAN Instance number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
VOID
PvrstCliShowLoopGuardState (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4PortNum, UINT4 u4InstanceId)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return;
    }
    if ((pAstPortEntry = AstGetIfIndexEntry (u4PortNum)) == NULL)
    {
        AstReleaseContext ();
        return;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (u4ContextId,
                                                (UINT2) u4InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        AstReleaseContext ();
        return;
    }
    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        AstReleaseContext ();
        return;
    }
    pPerStPortInfo =
        PVRST_GET_PERST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                   u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return;
    }

    if (pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
    {
        CliPrintf (CliHandle, "Loopguard is enabled on the port\r\n");
    }

    AstReleaseContext ();
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstDisplayPortInconsistency                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays port-level inconsistency    */
/*                        due to root guard or loop guard or Bpdu guard.     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PvrstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tVlanId             PrevVlanId = (tVlanId) AST_INIT_VAL;
    tVlanId             VlanId = (tVlanId) AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    INT4                i4Inconsistency = AST_INIT_VAL;
    INT4                i4BpduInconsistentState = PVRST_FALSE;
    INT4                i4PortTypeInconsistentState = PVRST_FALSE;
    INT4                i4PortPVIDInconsistentState = PVRST_FALSE;
    UINT2               u2InstanceId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;

    if (nmhGetFsMIPvrstRootInconsistentState (i4Index,
                                              &i4Inconsistency) != SNMP_FAILURE)
    {
        if (i4Inconsistency == AST_TRUE)
        {
            CliPrintf (CliHandle, "VLAN0001\tRoot Inconsistent\r\n");
        }
    }

    if (nmhGetFsMIPvrstPortBpduInconsistentState
        (i4Index, &i4BpduInconsistentState) != SNMP_FAILURE)
    {
        if (i4BpduInconsistentState == PVRST_TRUE)
        {
            CliPrintf (CliHandle, "VLAN0001\tBpduGuard Inconsistent\r\n");
        }
    }

    if (nmhGetFsMIPvrstPortTypeInconsistentState
        (i4Index, &i4PortTypeInconsistentState) != SNMP_FAILURE)
    {
        if (i4PortTypeInconsistentState == PVRST_TRUE)
        {
            CliPrintf (CliHandle, "VLAN0001\tPortType Inconsistent\r\n");
        }
    }

    if (nmhGetFsMIPvrstPortPVIDInconsistentState
        (i4Index, &i4PortPVIDInconsistentState) != SNMP_FAILURE)
    {
        if (i4PortPVIDInconsistentState == PVRST_TRUE)
        {
            CliPrintf (CliHandle, "VLAN0001\tPortPVID Inconsistent\r\n");
        }
    }

    if (AstGetContextInfoFromIfIndex ((UINT4) i4Index,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return;
    }

    if (AstSelectContext (u4ContextId) != PVRST_SUCCESS)
    {
        return;
    }
    while (L2IwfMiGetNextActiveVlan (AST_CURR_CONTEXT_ID (), PrevVlanId,
                                     &VlanId) == L2IWF_SUCCESS)
    {
        u2InstanceId =
            AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);

        PrevVlanId = VlanId;
        if (u2InstanceId == 0)
        {
            continue;
        }

        pPerStPortInfo =
            AST_GET_PERST_PORT_INFO ((UINT2) i4Index, u2InstanceId);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        i4Inconsistency = (INT4) pPerStPortInfo->bLoopIncStatus;
        if (i4Inconsistency == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "VLAN%4d\tLoop Inconsistent\r\n",
                       (UINT4) VlanId);
        }
        else if (i4Inconsistency == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "VLAN%4d\tNone\r\n", (UINT4) VlanId);
        }
    }

    AstReleaseContext ();
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstSetBpduRx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu             */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpdu - bpdu status of give port             */
/*                                      TRUE - bpdu can be received on port  */
/*                                      FALSE- bpdus will be ignored*/
/*                                             on this port.                 */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
PvrstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduRx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPvrstPortEnableBPDURx (&u4ErrCode, (INT4) u4PortNum,
                                          i4EnableBpduRx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPvrstPortEnableBPDURx ((INT4) u4PortNum, i4EnableBpduRx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstSetBpduTx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu             */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpdu - bpdu status of give port             */
/*                                      TRUE - bpdu can be received on port  */
/*                                      FALSE- bpdus will be ignored*/
/*                                             on this port.                 */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
PvrstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduTx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPvrstPortEnableBPDUTx (&u4ErrCode, (INT4) u4PortNum,
                                          i4EnableBpduTx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPvrstPortEnableBPDUTx ((INT4) u4PortNum, i4EnableBpduTx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstSetBpduFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu transmit    */
/*                        or receive status for a port.                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduFilter - bpdu transmit status of give   */
/*                                            port                           */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
PvrstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                    INT4 i4EnableBpduFilter)
{
    tAstPortEntry      *pPortEntry = NULL;

    pPortEntry = AST_GET_PORTENTRY (u4PortNum);
    pPortEntry->u1EnableBPDUFilter = (UINT1) i4EnableBpduFilter;
    if (PvrstSetBpduTx (CliHandle, u4PortNum, i4EnableBpduFilter) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (PvrstSetBpduRx (CliHandle, u4PortNum, i4EnableBpduFilter) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstCliSetFlushInterval                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the flush interval              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        bEnableBpduFilter - bpdu transmit status of give   */
/*                                            port                           */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/
INT4
PvrstCliSetFlushInterval (tCliHandle CliHandle, UINT4 u4Value)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIPvrstFlushInterval (&u4ErrCode,
                                         (INT4) u4CurrContextId,
                                         (INT4) u4Value) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIPvrstFlushInterval ((INT4) u4CurrContextId, (INT4) u4Value)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetBrgVersion                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PVRST mode to STP           */
/*                          Compatible or RSTP Compatible.                   */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4Version - STP Version                            */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstCliSetBrgVersion (tCliHandle CliHandle, UINT4 u4Version)
{

    UINT4               u4ErrCode = 0;

    if (SNMP_FAILURE ==
        nmhTestv2FsPvrstForceProtocolVersion (&u4ErrCode, (INT4) u4Version))
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot set the PVRST mode enabled bridge to be MST compatible\r\n");
        return (CLI_FAILURE);
    }
    if (SNMP_FAILURE == nmhSetFsPvrstForceProtocolVersion ((INT4) u4Version))
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PvrstCliSetFlushIndThreshold                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the flush indication threshold  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        bEnableBpduFilter - bpdu transmit status of give   */
/*                                            port                           */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
PvrstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Vlan, UINT4 u4Value)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIPvrstInstFlushIndicationThreshold (&u4ErrCode,
                                                        (INT4) u4CurrContextId,
                                                        (INT4) u4Vlan,
                                                        (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIPvrstInstFlushIndicationThreshold
        ((INT4) u4CurrContextId, (INT4) u4Vlan, (INT4) u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetPotocolMigration                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port Protocol Migration i.e.*/
/*                        mcheck variable on all ports.                      */
/*                                                                           */
/*     INPUT            : tCliHandle   -Handle to the CLI context            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstCliSetProtocolMigration (tCliHandle CliHandle)
{

    INT4                i4CurPort = 0;
    INT4                i4NextPort = 0;
    UINT4               u4TempContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT4                i4OutCome = 0;
    UINT1               u1OperStatus = 0;

    i4OutCome = AstGetFirstPortInCurrContext (&i4NextPort);

    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_PROTO_MIG_ERR);
        return CLI_FAILURE;
    }

    if (i4OutCome == RST_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No PortEntry from the table \r\n");
        return CLI_FAILURE;
    }

    do
    {
        if (AstGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4TempContextId,
                                          &u2LocalPortId) != RST_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%Retrieving Context ID fails\r\n");
            return CLI_FAILURE;
        }
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);
        if (CFA_IF_DOWN == u1OperStatus)
        {
            i4CurPort = i4NextPort;
            continue;
        }

        if (PvrstCliSetPortProtocolMigration ((UINT4) u2LocalPortId) !=
            CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%Protocol Migration on Port fails\r\n");
            return CLI_FAILURE;
        }

        i4CurPort = i4NextPort;
    }
    while (AstGetNextPortInCurrContext (i4CurPort, &i4NextPort) != RST_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PvrstCliSetPortProtocolMigration                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Protocol Migration on       */
/*                        a single port   i.e. mcheck variable on one port   */
/*                                                                           */
/*     INPUT            : tCliHandle   -  Handle to the CLiContext           */
/*                        u4IfIndex    -  Port Number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstCliSetPortProtocolMigration (UINT4 u4IfIndex)
{
    UINT4               u4InstanceId = 0;
    UINT4               u4ErrCode = 0;

    for (u4InstanceId = 1; u4InstanceId <= VLAN_MAX_VLAN_ID; u4InstanceId++)
    {
        if (nmhTestv2FsPvrstInstPortProtocolMigration
            (&u4ErrCode, (INT4) u4InstanceId, (INT4) u4IfIndex, AST_SNMP_TRUE)
            != SNMP_FAILURE)
        {
            if (nmhSetFsPvrstInstPortProtocolMigration
                ((INT4) u4InstanceId, (INT4) u4IfIndex, AST_SNMP_TRUE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

#endif /*PVRST_WANTED */
