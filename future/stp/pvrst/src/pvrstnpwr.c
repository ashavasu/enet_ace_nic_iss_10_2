/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved]
 *
 * $Id: pvrstnpwr.c,v 1.3 2013/11/14 11:33:14 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              Hardware API's w.r.t PVRST
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __PVRST_NP_WR_C
#define __PVRST_NP_WR_C

#include "asthdrs.h"
#include "astvinc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : PvrstNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tPvrstNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
PvrstNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pPvrstNpModInfo = &(pFsHwNp->PvrstNpModInfo);

    if (NULL == pPvrstNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE:
        {
            tPvrstNpWrFsMiPvrstNpCreateVlanSpanningTree *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpCreateVlanSpanningTree;
            u1RetVal =
                FsMiPvrstNpCreateVlanSpanningTree (pEntry->u4ContextId,
                                                   pEntry->VlanId);
            break;
        }
        case FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE:
        {
            tPvrstNpWrFsMiPvrstNpDeleteVlanSpanningTree *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeleteVlanSpanningTree;
            u1RetVal =
                FsMiPvrstNpDeleteVlanSpanningTree (pEntry->u4ContextId,
                                                   pEntry->VlanId);
            break;
        }
        case FS_MI_PVRST_NP_INIT_HW:
        {
            tPvrstNpWrFsMiPvrstNpInitHw *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpInitHw;
            u1RetVal = FsMiPvrstNpInitHw (pEntry->u4ContextId);
            break;
        }
        case FS_MI_PVRST_NP_DE_INIT_HW:
        {
            tPvrstNpWrFsMiPvrstNpDeInitHw *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeInitHw;
            FsMiPvrstNpDeInitHw (pEntry->u4ContextId);
            break;
        }
        case FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpSetVlanPortState *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpSetVlanPortState;
            u1RetVal =
                FsMiPvrstNpSetVlanPortState (pEntry->u4ContextId,
                                             pEntry->u4IfIndex, pEntry->VlanId,
                                             pEntry->u1PortState);
            break;
        }
        case FS_MI_PVRST_NP_GET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpGetVlanPortState *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpGetVlanPortState;
            u1RetVal =
                FsMiPvrstNpGetVlanPortState (pEntry->u4ContextId,
                                             pEntry->VlanId, pEntry->u4IfIndex,
                                             pEntry->pu1Status);
            break;
        }
#ifdef  MBSM_WANTED
        case FS_MI_PVRST_MBSM_NP_SET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstMbsmNpSetVlanPortState *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstMbsmNpSetVlanPortState;
            u1RetVal =
                PvrstFsMiPvrstMbsmNpSetVlanPortState (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->VlanId,
                                                      pEntry->u1PortState,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_PVRST_MBSM_NP_INIT_HW:
        {
            tPvrstNpWrFsMiPvrstMbsmNpInitHw *pEntry = NULL;
            pEntry = &pPvrstNpModInfo->PvrstNpFsMiPvrstMbsmNpInitHw;
            u1RetVal =
                PvrstFsMiPvrstMbsmNpInitHw (pEntry->u4ContextId,
                                            pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __PVRST_NP_WR_C */
