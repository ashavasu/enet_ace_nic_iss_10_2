/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvrdstb.c,v 1.1 2007/11/19 07:21:54 iss Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for PVRST module.   
 *
 *******************************************************************/
#include "asthdrs.h"
#include "astvinc.h"
#ifndef L2RED_WANTED

/*****************************************************************************/
/* Function Name      : PvrstRedStorePduInActive                             */
/*                                                                           */
/* Description        : Stores the latest PDUs on the Active Node            */
/* Input(s)           : u2PortIfIndex - Port on which the PDU was received   */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the PDU                            */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstRedStorePduInActive (UINT2 u2PortNum, tPvrstBpdu * pData, UINT2 u2Len,
                          UINT2 u2InstanceId)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u2InstanceId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstRedClearAllBpdusOnActive              */
/*                                                                           */
/*    Description               : This function clears all the stored BPDU   */
/*                                information on ACTIVE node, called when the*/
/*                                protocol is disabled or Shutdown on Active.*/
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstRedClearAllBpdusOnActive ()
{
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstRedClearAllSyncUpDataInInst           */
/*                                                                           */
/*    Description               : This function clears all the stored Sync up*/
/*                                information on Standby node for an PVRST   */
/*                                instance, called when an Instance becomes  */
/*                                inactive on Active.                        */
/*                                                                           */
/*    Input(s)                  : u2PvrstInst - PVRST Instance identifier    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS on Success.                  */
/*                                PVRST_FAILURE on Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstRedClearAllSyncUpDataInInst (UINT2 u2PvrstInst)
{
    AST_UNUSED (u2PvrstInst);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedProtocolRestart                              */
/*                                                                           */
/* Description        : Restart PVRST protocol on a particular Port & Instance*/
/* Input(s)           : u2Port - Port                                        */
/*                      u2InstanceId - PVRST Instance identifier             */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstRedProtocolRestart (UINT2 u2Port, UINT2 u2InstanceId)
{
    AST_UNUSED (u2Port);
    AST_UNUSED (u2InstanceId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedClearPduOnActive                             */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           :                                                      */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstRedClearPduOnActive (UINT2 u2PortNum)
{
    AST_UNUSED (u2PortNum);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstRedSyncUpPdu                                    */
/*                                                                           */
/* Description        : Restart PVRST protocol on a particular Port & Instance*/
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/*                      u2InstanceId - Instance Identifier                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstRedSyncUpPdu (UINT2 u2PortNum, tPvrstBpdu * pData, UINT2 u2Len)
{
    AST_UNUSED (u2PortNum);
    AST_UNUSED (pData);
    AST_UNUSED (u2Len);
    return PVRST_SUCCESS;
}
#endif
