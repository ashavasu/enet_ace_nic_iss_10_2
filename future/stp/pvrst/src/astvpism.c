/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvpism.c,v 1.27 2017/12/11 09:58:31 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Information State Event Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"
#include "astvred.h"

/*****************************************************************************/
/* Function Name      : PvrstPortInfoMachine                                 */
/*                                                                           */
/* Description        : This is the main routine for the Port Information    */
/*                      State Machine. This function is responsible for      */
/*                      processing the newly received information in a bpdu  */
/*                      and for calling the Role Selection State Machine.    */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      pRcvdBpdu - Pointer to the Received BPDU structure.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortInfoMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo,
                      tPvrstBpdu * pRcvdBpdu)
{
    UINT2               u2State = (UINT2) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) PVRST_SUCCESS;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Event %s: Invalid parameter passed to "
                      "event handler routine\n",
                      gaaau1AstSemEvent[AST_PISM][u2Event]);
        return PVRST_FAILURE;
    }
    u2State = (UINT2) pPerStPortInfo->u1PinfoSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "PISM: Port %u: Port Info Machine Called "
                  "with Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_PISM][u2Event],
                  gaaau1AstSemState[AST_PISM][u2State]);

    AST_DBG_ARG3 (AST_PISM_DBG,
                  "PISM: Port %u: Port Info Machine Called with"
                  " Event: %s, State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_PISM][u2Event],
                  gaaau1AstSemState[AST_PISM][u2State]);

    if (PVRST_PORT_INFO_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_PISM_DBG, "PISM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    i4RetVal = (*(PVRST_PORT_INFO_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo, pRcvdBpdu);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: Event routine returned FAILURE\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRiWhileExpCurrent                     */
/*                                                                           */
/* Description        : This routine is called when the received info while  */
/*                      timer expires in the 'CURRENT' state.                */
/*                      This routine moves the state machine state to 'AGED' */
/*                      when the port has received information.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                  tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: RCVDINFO expiry, "
                  "taking action...\n", pPerStPortInfo->u2PortNo);
    AST_INCR_RCVDINFOWHILE_COUNT (u2PortNum);
    AST_GET_SYS_TIME (&u4Ticks);
    pAstPortEntry->u4RcvInfoWhileExpTimeStamp = u4Ticks;
    if ((pPerStRstPortInfo->u1InfoIs == (UINT1) PVRST_INFOIS_RECEIVED) &&
        (pPerStRstPortInfo->bUpdtInfo == (tAstBoolean) PVRST_FALSE) &&
        (pPerStPvrstRstPortInfo->bRcvdBpdu == (tAstBoolean) PVRST_FALSE))
    {
        if (PvrstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: MakeAged returned FAILURE\n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmUpdtInfoCurrent                       */
/*                                                                           */
/* Description        : This routine is called when updt info variable is    */
/*                      set by 'Role Transition State Machine' when this     */
/*                      state machine is in 'CURRENT' state.                 */
/*                      This routine Updates the port information's Port     */
/*                      Priority Vector by changing the state to 'UPDATE'.   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pPerStRstPortInfo->bSelected == PVRST_TRUE)
    {
        if (PvrstPortInfoSmMakeUpdate (pPerStPortInfo,
                                       pRcvdBpdu) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: MakeUpdate returned FAILURE\n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeDisabled                          */
/*                                                                           */
/* Description        : This routine changes the state of this state machine */
/*                      to DISABLED state.                                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeDisabled (tAstPerStPortInfo * pPerStPortInfo,
                             tPvrstBpdu * pRcvdBpdu)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstContextInfo    *pContextInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    UNUSED_PARAM (pRcvdBpdu);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pContextInfo = AST_CURR_CONTEXT_INFO ();
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moving to state DISABLED \n",
                  pPerStPortInfo->u2PortNo);

    pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }

    /* On disabling STP LoopInc state to be cleared in case enabled */
    pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
    pPerStPortInfo->bRootInconsistent = PVRST_FALSE;

    /* Copy the Designated Priority Vector to the Port Priority Vector */
    if ((pPerStBrgInfo->u2RootPort == AST_INIT_VAL) ||
        (pPerStBrgInfo->u2RootPort == u2PortNum))
    {
        /* This Bridge is the Root Bridge */
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &(pContextInfo->BridgeEntry.BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStPortInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStPortInfo->u4RootCost = AST_NO_VAL;
    }
    else
    {
        pPerStPortInfo->RootId = pPerStBrgInfo->RootId;
        pPerStPortInfo->u4RootCost = pPerStBrgInfo->u4RootCost;
    }

    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &(pContextInfo->BridgeEntry.BridgeAddr), AST_MAC_ADDR_SIZE);

    pPerStPortInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    pPerStPortInfo->u2DesgPortId =
        (UINT2) ((AST_GET_PROTOCOL_PORT (pPerStPortInfo->u2PortNo) &
                  AST_PORTNUM_MASK) | u2Val);

    pPerStPvrstRstPortInfo->DesgPvrstTimes = pPerStPvrstBridgeInfo->RootTimes;
    pPerStPvrstRstPortInfo->PortPvrstTimes =
        pPerStPvrstRstPortInfo->DesgPvrstTimes;
    pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;
    pPerStRstPortInfo->bProposing = PVRST_FALSE;
    pPerStRstPortInfo->bAgreed = PVRST_FALSE;
    pPerStRstPortInfo->bProposed = PVRST_FALSE;
    if (pPerStRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                            (UINT1) AST_TMR_TYPE_RCVDINFOWHILE)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Stop RcvdInfoWhile Timer "
                          "returned FAILURE\n", u2PortNum);
            return PVRST_FAILURE;
        }
    }

    pPerStRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_DISABLED;
    pPerStRstPortInfo->bReSelect = (tAstBoolean) PVRST_TRUE;
    pPerStRstPortInfo->bSelected = (tAstBoolean) PVRST_FALSE;
    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_DISABLED;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state DISABLED \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeAged                              */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'AGED'.                                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeAged (tAstPerStPortInfo * pPerStPortInfo,
                         tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_AGED;
    pPerStRstPortInfo->bReSelect = PVRST_TRUE;
    pPerStRstPortInfo->bSelected = PVRST_FALSE;
    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_AGED;

    /* As the earlier received superior information is aged out, clear the 
     * same from the redundancy database. Otherwise, the old information will 
     * get applied on the standby during bulk updated and the spanning 
     * tree may misbehave for some time.*/
    PVRST_CLEAR_PDU (pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state AGED \n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_RESELECT,
                                       pPerStPortInfo->u2Inst) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %u: Role Selection Machine "
                      "returned FAILURE !!!\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: Role Selection Machine"
                      " returned FAILURE !!!\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (pPerStRstPortInfo->bUpdtInfo == PVRST_TRUE)
    {
        if (PvrstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: PortInfo Make"
                          " Update returned FAILURE\n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeUpdate                            */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'UPDATE'                                             */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeUpdate (tAstPerStPortInfo * pPerStPortInfo,
                           tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tAstContextInfo    *pContextInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pContextInfo = AST_CURR_CONTEXT_INFO ();
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: UPDATING port info \n",
                  pPerStPortInfo->u2PortNo);

    /* Copy the Designated Priority Vector to the Port Priority Vector */

    if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
    {
        /* This Bridge is the Root Bridge */
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &(pContextInfo->BridgeEntry.BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStPortInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStPortInfo->u4RootCost = AST_NO_VAL;

    }
    else
    {
        pPerStPortInfo->RootId = pPerStBrgInfo->RootId;
        pPerStPortInfo->u4RootCost = pPerStBrgInfo->u4RootCost;
    }
    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &(pContextInfo->BridgeEntry.BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStPortInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;

    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    pPerStPortInfo->u2DesgPortId =
        (UINT2) ((AST_GET_PROTOCOL_PORT (pPerStPortInfo->u2PortNo) &
                  AST_PORTNUM_MASK) | u2Val);

    if (pPerStPvrstBridgeInfo->RootTimes.u2HelloTime
        != pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime)
    {
        /* If the value of Hello time has changed, then re-start the HelloWhen 
         * timer for the new duration so that BPDUs are now propagated based on
         * the new periodic interval. 
         * In PvrstStartTimer function, if the timer is already running, then it 
         * is stopped before starting it again. */
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Starting Hello timer "
                      "with updated value\n", pPerStPortInfo->u2PortNo);
        if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_HELLOWHEN,
                             pPerStPvrstBridgeInfo->
                             RootTimes.u2HelloTime) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Start HelloWhen "
                          "Timer returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    pPerStPvrstRstPortInfo->DesgPvrstTimes = pPerStPvrstBridgeInfo->RootTimes;
    pPerStPvrstRstPortInfo->PortPvrstTimes =
        pPerStPvrstRstPortInfo->DesgPvrstTimes;

    pPerStRstPortInfo->bUpdtInfo = PVRST_FALSE;

#ifdef IEEE8021Y_Z11_WANTED
    if (pRcvdBpdu != NULL)
    {
        /*Dont Clear Agreed if the Newport Information is getting better.
         * Recomendation in accordance with 802.1D/D4 2003 for Annex Z.10 and Z.11*/
        pPerStRstPortInfo->bAgreed =
            ((pPerStRstPortInfo->bAgreed == PVRST_TRUE) &&
             PvrstPortInfoSmBetterOrSameInfo (pPerStPortInfo, pRcvdBpdu));
    }
#else
    pPerStRstPortInfo->bAgreed = PVRST_FALSE;
#endif /*End of IEEE8021Y_Z11_WANTED */

    pPerStRstPortInfo->bSynced = PVRST_FALSE;
    pPerStRstPortInfo->bProposed = PVRST_FALSE;
    pPerStRstPortInfo->bProposing = PVRST_FALSE;
    pPerStRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_MINE;
    pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_UPDATE;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to"
                  "state UPDATE \n", pPerStPortInfo->u2PortNo);

    if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_UPDTINFO_RESET,
                                pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "PISM: Port %u: Role Transition "
                      "machine Returned Failure!!!\n",
                      pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Role Transition "
                      "machine Returned Failure!!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (pPerStPvrstRstPortInfo->bNewInfo == PVRST_TRUE)
    {
        if (PvrstPortTransmitMachine ((UINT2)
                                      PVRST_PTXSM_EV_NEWINFO_SET,
                                      pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Transmit "
                          "machine returned FAILURE !!! \n",
                          pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Transmit "
                          "machine returned FAILURE !!! \n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    if (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu) != PVRST_SUCCESS)
    {

        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: MakeCurrent "
                      "returned FAILURE\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeCurrent                           */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'CURRENT'                                            */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeCurrent (tAstPerStPortInfo * pPerStPortInfo,
                            tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_CURRENT;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to "
                  "ate CURRENT \n", pPerStPortInfo->u2PortNo);

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if ((pPerStRstPortInfo->bSelected == PVRST_TRUE) &&
        (pPerStRstPortInfo->bUpdtInfo == PVRST_TRUE))
    {
        if (PvrstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: MakeCurrent "
                          "returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeReceive                           */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'RECEIVE'                                            */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeReceive (tAstPerStPortInfo * pPerStPortInfo,
                            tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RootInconsistentState = PVRST_FALSE;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1RcvdInfo = (UINT2) AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pPerStRstPortInfo->bUpdtInfo != PVRST_FALSE)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port%u: bUpdtInfo is "
                      "FALSE, exiting SEM... \n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }
    u1RcvdInfo = (UINT1) PvrstPortInfoSmRcvBpdu (pPerStPortInfo, pRcvdBpdu);
    pPerStRstPortInfo->u1RcvdInfo = u1RcvdInfo;
    if (PvrstPortInfoSmUpdtBpduVersion (pPerStPortInfo, pRcvdBpdu)
        != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: UpdtBpduVersion"
                      "returned FAILURE\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    if (PvrstPortInfoSmSetTcFlags (pPerStPortInfo, pRcvdBpdu) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: SetTcFlags Returned FAILURE\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    pPerStPvrstRstPortInfo->bRcvdBpdu = PVRST_FALSE;

    pPerStPortInfo->u1PinfoSmState = (INT1) PVRST_PINFOSM_STATE_RECEIVE;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to"
                  "state RECEIVE \n", pPerStPortInfo->u2PortNo);

    switch (u1RcvdInfo)
    {
        case PVRST_SUPERIOR_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: SUPERIOR DESIGNATED "
                          "message Received\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: SUPERIOR DESIGNATED "
                          "message Received\n", pPerStPortInfo->u2PortNo);

            /* If Root-Guard is enabled, on reception of Superior information
             * the port's state will be maintained in Discarding  until
             * the superior information ceases. */
            if (pAstCommPortInfo->bRootGuard == PVRST_TRUE)
            {
                /* RootInconsistent set, when the port transitions to
                 * Discarding state.*/
                pAstCommPortInfo->bRootInconsistent = PVRST_TRUE;
                pPerStRstPortInfo->bLearn = PVRST_FALSE;
                pPerStRstPortInfo->bForward = PVRST_FALSE;

                PvrstGetInstPortRootInconsistentState ((UINT2)
                                                       (pAstPortEntry->
                                                        u4IfIndex),
                                                       pPerStPortInfo->u2Inst,
                                                       &i4RootInconsistentState);

                if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                                     (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY,
                                     PVRST_ROOT_INC_RECOVERY_TIME) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "PISM: PvrstStartTimer for Root Inconsistent Tmr FAILED!\n");
                    return PVRST_FAILURE;
                }

                if (i4RootInconsistentState == PVRST_FALSE)
                {
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Root_Guard_block : Root Guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                        AstRootIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pAstCommPortInfo->
                                                   bRootInconsistent,
                                                   pPerStPortInfo->u2Inst,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }

                if (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu) !=
                    PVRST_SUCCESS)
                {

                    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                  "PISM: Port %u: MakeCurrent "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
                return PVRST_SUCCESS;
            }

            if (PvrstPortInfoSmMakeSuperior (pPerStPortInfo,
                                             pRcvdBpdu) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %u: MakeSuperior "
                              "returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
            AST_RED_SET_SYNC_FLAG (u2InstIndex);
            break;

        case PVRST_REPEATED_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: REPEATED DESIGNATED"
                          "message Received\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: REPEATED DESIGNATED"
                          "message Received\n", pPerStPortInfo->u2PortNo);
            if (PvrstPortInfoSmMakeRepeat (pPerStPortInfo,
                                           pRcvdBpdu) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %u: MakeRepeat returned"
                              "FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
            break;

            /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
             * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
        case PVRST_INFERIOR_DESG_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: INFERIOR DESIGNATED "
                          "message Received\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: INFERIOR DESIGNATED "
                          "message Received\n", pPerStPortInfo->u2PortNo);
            if ((AST_PORT_ROOT_GUARD (pAstPortEntry) == RST_TRUE)
                && (pAstPortEntry->bRootInconsistent == RST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, pPerStPortInfo->u2Inst,
                     (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                }

                pAstPortEntry->bRootInconsistent = RST_FALSE;
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s for Vlan : %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
            }

            if (PvrstPortInfoSmMakeInferiorDesg (pPerStPortInfo, pRcvdBpdu)
                != PVRST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeInferiorDesg returned FAILURE\n");
                return PVRST_FAILURE;
            }
            break;
#endif /* IEEE8021Y_Z12_WANTED */

#ifdef IEEE8021Y_Z11_WANTED
            /* Added as per 802.1D 2003, D4 for handling 802.1Y D4 Annex Z.2 */
        case PVRST_INFERIOR_ROOT_ALT_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: INFERIOR ROOT_ALT "
                          "message Received\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: INFERIOR ROOT_ALT"
                          "message Received\n", pPerStPortInfo->u2PortNo);
            if ((AST_PORT_ROOT_GUARD (pAstPortEntry) == RST_TRUE)
                && (pAstPortEntry->bRootInconsistent == RST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (PvrstStopTimer
                    ((VOID *) pPerStPortInfo, pPerStPortInfo->u2Inst,
                     (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                }

                pAstPortEntry->bRootInconsistent = RST_FALSE;
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s for Vlan : %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
            }
            if (PvrstPortInfoSmMakeNotDesg (pPerStPortInfo, pRcvdBpdu)
                != PVRST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeNotDesg returned FAILURE\n");
                return PVRST_FAILURE;
            }
            if (pPerStRstPortInfo->u1InfoIs != (UINT1) RST_INFOIS_RECEIVED)
            {
                AST_RED_SET_SYNC_FLAG (u2InstIndex);
            }
            break;
#else
        case PVRST_CONFIRMED_ROOT_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: CONFIRMED ROOT "
                          "Message Received\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: CONFIRMED ROOT"
                          " Message Received\n", pPerStPortInfo->u2PortNo);
            if (PvrstPortInfoSmMakeAgreement (pPerStPortInfo,
                                              pRcvdBpdu) != PVRST_SUCCESS)
            {
                AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                         "PISM: MakeAgreement returned FAILURE\n");
                return PVRST_FAILURE;
            }
            break;
#endif /*End of IEEE8021Y_Z11_WANTED */

        case PVRST_OTHER_MSG:

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "PISM: Port %u: OTHER Message Received\n",
                          pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: OTHER Message Received\n",
                          pPerStPortInfo->u2PortNo);

            PvrstGetInstPortRootInconsistentState (pPerStPortInfo->u2PortNo,
                                                   pPerStPortInfo->u2Inst,
                                                   &i4RootInconsistentState);

            if ((pAstCommPortInfo->bRootGuard == RST_TRUE) &&
                (i4RootInconsistentState == PVRST_TRUE))
            {
                if (PvrstPortInfoSmGetMsgType (pPerStPortInfo,
                                               pRcvdBpdu) == PVRST_INFERIOR_MSG)
                {
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                                        (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY)
                        != PVRST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_TMR_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "PISM: PvrstStopTimer for Root Inconsistent Tmr FAILED!\n");
                        return PVRST_FAILURE;
                    }

                    if (PvrstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu) !=
                        PVRST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                 "PISM: Role Transition Machine returned FAILURE\n");
                        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                 "PISM: Role Transition Machine returned FAILURE\n");
                        return PVRST_FAILURE;
                    }
                }
            }
            else
            {
                if (PvrstPortInfoSmMakeCurrent (pPerStPortInfo,
                                                pRcvdBpdu) != PVRST_SUCCESS)
                {
                    AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                             "PISM: MakeCurrent returned FAILURE\n");
                    return PVRST_FAILURE;
                }
            }
            break;
        default:
            return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeSuperior                          */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'SUPERIOR'.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeSuperior (tAstPerStPortInfo * pPerStPortInfo,
                             tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    /* Copy the Designated Priority Vector to the Port Priority Vector */

    AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                &(pRcvdBpdu->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStPortInfo->RootId.u2BrgPriority = pRcvdBpdu->RootId.u2BrgPriority;

    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &(pRcvdBpdu->DesgBrgId.BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStPortInfo->DesgBrgId.u2BrgPriority =
        pRcvdBpdu->DesgBrgId.u2BrgPriority;
    pPerStPortInfo->u4RootCost = pRcvdBpdu->u4RootPathCost;
    pPerStPortInfo->u2DesgPortId = pRcvdBpdu->u2PortId;

    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.5 */
    /* The actual changes to be made in recordTimes () is as per 802.1D 
     * 2003 Edition as there is a contradiction between 802.1D and 802.1y */

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    PvrstPortInfoSmRecordTimes (pPerStPortInfo, pRcvdBpdu);

    if (PvrstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: UpdtRcvdInfoWhile"
                      " returned FAILURE\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo->bAgreed = PVRST_FALSE;
    pPerStRstPortInfo->bProposing = PVRST_FALSE;
    /* Do not clear Synced if the Newport Information is getting better.
     * This is a recomendation in accordance with 802.1D 2003 D4, for 
     * Annex Z.10 and Z.11 of 802.1Y D4. */
#ifdef IEEE8021Y_Z11_WANTED

    pPerStRstPortInfo->bSynced = ((pPerStRstPortInfo->bSynced == PVRST_TRUE)
                                  &&
                                  PvrstPortInfoSmBetterOrSameInfo
                                  (pPerStPortInfo, pRcvdBpdu));
#else
    pPerStRstPortInfo->bSynced = PVRST_FALSE;
#endif /*End of IEEE8021Y_Z11_WANTED */

    if (PvrstPortInfoSmRecordProposed (pPerStPortInfo,
                                       pRcvdBpdu) == PVRST_SUCCESS)
    {
        PVRST_INCR_RX_PROPOSAL_COUNT (pPerStPortInfo->u2PortNo, u2InstIndex);
        AST_GET_SYS_TIME (&u4Ticks);
        pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: PROPOSAL FLAG SET in"
                      " the Received BPDU\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Setting PROPOSED "
                      "variable !!!\n", pPerStPortInfo->u2PortNo);
        pPerStRstPortInfo->bProposed = PVRST_TRUE;
    }
    pPerStRstPortInfo->u1InfoIs = (UINT1) PVRST_INFOIS_RECEIVED;
    pPerStRstPortInfo->bReSelect = PVRST_TRUE;
    pPerStRstPortInfo->bSelected = PVRST_FALSE;

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_SUPERIOR;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state SUPERIOR \n",
                  pPerStPortInfo->u2PortNo);

    /* Change the State as current here itself */
    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_CURRENT;

    /* If RcvdInfoWhile timer is not running then move to the aged state */
    if (pPerStRstPortInfo->pRcvdInfoTmr == NULL)
    {

        pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
        if (pPortInfo != NULL)
        {
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE)
                && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
#ifndef L2RED_WANTED
                u2RiWhileDuration =
                    (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
                u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                     (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
                     u2RiWhileDuration) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Start RcvdInfoWhile Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return PVRST_FAILURE;
                }
                AST_DBG_ARG2 (AST_SM_VAR_DBG,
                              "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2RiWhileDuration);
            }
            else
            {
                /* If RcvdInfoWhile timer is not running then move to the aged state */
                return PvrstPortInfoSmRiWhileExpCurrent (pPerStPortInfo,
                                                         pRcvdBpdu);
            }
        }
    }

    if (PvrstPortRoleSelectionMachine (PVRST_PROLESELSM_EV_RESELECT,
                                       pPerStPortInfo->u2Inst) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %u: Role Selection Machine "
                      "returned FAILURE\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %u: Role Selection Machine"
                      " returned FAILURE\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (pPerStRstPortInfo->bProposed == PVRST_TRUE)
    {
        if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_PROPOSED_SET,
                                    pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "PISM: Role Transition Machine returned FAILURE\n");
            AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                     "PISM: Role Transition Machine returned FAILURE\n");
            return PVRST_FAILURE;
        }
    }
    return (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeRepeat                            */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'REPEAT'.                                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeRepeat (tAstPerStPortInfo * pPerStPortInfo,
                           tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;

    if (pPerStPortInfo == NULL)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: PvrstPortInfoSmMakeRepeat returned FAILURE\n");
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pPerStRstPortInfo == NULL)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: PvrstPortInfoSmMakeRepeat returned FAILURE\n");
        return PVRST_FAILURE;
    }

    if (PvrstPortInfoSmRecordProposed (pPerStPortInfo, pRcvdBpdu)
        == PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: PROPOSAL FLAG SET "
                      "in the Received BPDU\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: SETTING PROPOSED FLAG !!!\n",
                      pPerStPortInfo->u2PortNo);
        pPerStRstPortInfo->bProposed = PVRST_TRUE;
    }

    if (PvrstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                 "PISM: UpdtRcvdInfoWhile returned FAILURE\n");
        return PVRST_FAILURE;
    }

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_REPEAT;

    /* If RcvdInfoWhile timer is not running then move to the aged state */
    if (pPerStRstPortInfo->pRcvdInfoTmr == NULL)
    {
        pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

        if ((pPortInfo == NULL) || (pAstCommPortInfo == NULL))
        {
            return PVRST_FAILURE;
        }
        if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
            && (pPortInfo->bOperPointToPoint == RST_TRUE)
            && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
#ifndef L2RED_WANTED
            u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
            u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
            if (AstStartTimer
                ((VOID *) pPerStPortInfo, RST_DEFAULT_INSTANCE,
                 (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
                 u2RiWhileDuration) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Start RcvdInfoWhile Timer returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return PVRST_FAILURE;
            }

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "UpdtRcvdInfoWhile: Port %s: rcvdInfoWhile = %u\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2RiWhileDuration);
        }
        else
        {
            return PvrstPortInfoSmRiWhileExpCurrent (pPerStPortInfo, pRcvdBpdu);
        }
    }

    if (pPerStRstPortInfo->bProposed == PVRST_TRUE)
    {
        if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_PROPOSED_SET,
                                    pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state REPEAT \n",
                  pPerStPortInfo->u2PortNo);

    return (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

#ifdef IEEE8021Y_Z11_WANTED
/* The following functions are added as per 802.1D 2003, D4 for handling 802.1Y 
 * D4 Annex Z.2 */
/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeNotDesg                           */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'NOT_DESIGNATED'. Introduced in line with 802.1D D4  */
/*                       2003. When the rcvd bpdu is of type InferiorRootAlt */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeNotDesg (tAstPerStPortInfo * pPerStPortInfo,
                            tPvrstBpdu * pRcvdBpdu)
{
    /* Setting of rcvdMsg to FALSE has not been done currently. This will be
     * done when RSTP state machines will be changed to be in sync with MSTP
     * state machines. Not setting rcvdMsg to FALSE, will not affect the 
     * functionality as far as changes for Z.11 are concerned. */

    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    PvrstPortInfoSmRecordAgreement (pPerStPortInfo, pRcvdBpdu);

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_NOT_DESG;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state NOT_DESIGNATED \n",
                  pPerStPortInfo->u2PortNo);
    if (pPerStRstPortInfo->bAgreed == PVRST_TRUE)
    {
        if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_AGREED_SET,
                                    pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Role Transition Machine "
                          "returned FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    return (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRecordAgreement                       */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordAgreement'procedure as given in     */
/*                      P802.1D/D1 - 2003 Edition.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                     port information.                     */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                                tPvrstBpdu * pRcvBpdu)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT1               u1RcvdPortRole = 0;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* This check is added apart from 802.1w, since, RSTP packets are 
     * accepted in StpCompatible Mode.
     * In StpCompatible mode Proposal/Agreement does not make sense */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        return;
    }

    if ((pAstPortEntry->bOperPointToPoint == PVRST_TRUE) &&
        (pRcvBpdu->u1Flags & PVRST_SET_AGREEMENT_FLAG))
    {
        u1RcvdPortRole = pRcvBpdu->u1Flags & PVRST_FLAG_MASK_PORT_ROLE;
        if (((u1RcvdPortRole == (UINT1) PVRST_SET_ROOT_PROLE_FLAG) &&
             (PvrstPortInfoSmGetMsgType (pPerStPortInfo, pRcvBpdu)
              != PVRST_BETTER_MSG)) ||
            ((u1RcvdPortRole == (UINT1) PVRST_SET_DESG_PROLE_FLAG) &&
             (PvrstPortInfoSmGetMsgType (pPerStPortInfo, pRcvBpdu)
              != PVRST_INFERIOR_MSG)))
        {
            PVRST_INCR_RX_AGREEMENT_COUNT (pPerStPortInfo->u2PortNo,
                                           u2InstIndex);
            AST_GET_SYS_TIME (&u4Ticks);
            pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: AGREEMENT Received for Port %u\n", u2PortNum);
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: SETTING AGREED FLAG for Port %u \n",
                          u2PortNum);
            pPerStRstPortInfo->bAgreed = PVRST_TRUE;

            pPerStRstPortInfo->bProposing = PVRST_FALSE;
        }
        else
        {
            pPerStRstPortInfo->bAgreed = PVRST_FALSE;
        }
    }
}
#else
/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeAgreement                         */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'AGREEMENT'.                                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeAgreement (tAstPerStPortInfo * pPerStPortInfo,
                              tPvrstBpdu * pRcvdBpdu)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    /* This check is added apart from 802.1w, since, RSTP packets are 
     * accepted in StpCompatible Mode.
     * In StpCompatible mode Proposal/Agreement does not make sense */

    if (AST_FORCE_VERSION != (UINT1) AST_VERSION_0)
    {

        pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pPerStRstPortInfo->bAgreed = PVRST_TRUE;
        pPerStRstPortInfo->bProposing = PVRST_FALSE;
        pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_AGREEMENT;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Moved to state AGREEMENT \n",
                      pPerStPortInfo->u2PortNo);
        PVRST_INCR_RX_AGREEMENT_COUNT (pPerStPortInfo->u2PortNo, u2InstIndex);
        AST_GET_SYS_TIME (&u4Ticks);
        pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;
        if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_AGREED_SET,
                                    pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Role Transition Machine "
                          "returned FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }

    }

    return (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}

    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#endif /*End of IEEE8021Y_Z11_WANTED */

#ifdef IEEE8021Y_Z12_WANTED
/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmMakeInferiorDesg                      */
/*                                                                           */
/* Description        : This routine changes the state machine state to      */
/*                      'INFERIOR_DESIGNATED'.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmMakeInferiorDesg (tAstPerStPortInfo * pPerStPortInfo,
                                 tPvrstBpdu * pRcvdBpdu)
{
    /* Setting of rcvdMsg to FALSE has not been done currently. This will be
     * done when RSTP state machines will be changed to be in sync with MSTP
     * state machines. Not setting rcvdMsg to FALSE, will not affect the 
     * functionality as far as changes for Z.12 are concerned. */

    PvrstPortInfoSmRecordDispute (pPerStPortInfo, pRcvdBpdu);

    pPerStPortInfo->u1PinfoSmState = (UINT1) PVRST_PINFOSM_STATE_INFERIOR_DESG;
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Moved to state INFERIOR_DESIGNATED \n",
                  pPerStPortInfo->u2PortNo);

    if (pPerStPortInfo->bDisputed == PVRST_TRUE)
    {
        if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_DISPUTED_SET,
                                    pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Role Transition Machine"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    return (PvrstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu));
}
#endif /* IEEE8021Y_Z12_WANTED */

#ifdef IEEE8021Y_Z11_WANTED
/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmBetterOrSameInfo                      */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'betterorsameinfo' procedure.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUPERIOR_DESG_MSG (or) PVRST_REPEATED_DESG_MSG */
/*                      (or) PVRST_OTHER_MSG (or) PVRST_FAILURE              */
/*****************************************************************************/
INT4
PvrstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                                 tPvrstBpdu * pRcvdBpdu)
{
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    i4RetVal = PvrstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);
    switch (i4RetVal)
    {
        case PVRST_BETTER_MSG:
            return PVRST_TRUE;
        case PVRST_SAME_MSG:
            return PVRST_TRUE;

        default:
            /* This condition is not possible */
            return PVRST_FALSE;
    }
    return PVRST_FALSE;
}
#endif /*End of IEEE8021Y_Z11_WANTED */

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRcvBpdu                               */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'rcvdBpdu' procedure.                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUPERIOR_DESG_MSG (or) PVRST_REPEATED_DESG_MSG */
/*                      (or) PVRST_OTHER_MSG (or) PVRST_FAILURE              */
/*****************************************************************************/
INT4
PvrstPortInfoSmRcvBpdu (tAstPerStPortInfo * pPerStPortInfo,
                        tPvrstBpdu * pRcvdBpdu)
{
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT1               u1RcvdPortRole = (UINT1) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT2               u2RcvPortNum = 0;
    UINT2               u2DesgPortNum = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pPerStPvrstBridgeInfo = pPerStBrgInfo->pPerStPvrstBridgeInfo;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM : Message age as conveyed in the packet is  %d\n",
                  pRcvdBpdu->u2MessageAge);

    if (AST_MEMCMP (&(pRcvdBpdu->RootId.BridgeAddr[0]),
                    &(pBrgInfo->BridgeAddr[0]), AST_MAC_ADDR_SIZE) == 0)
    {
        if (pRcvdBpdu->RootId.u2BrgPriority != pPerStBrgInfo->u2BrgPriority)
        {
            return PVRST_OTHER_MSG;
        }

    }

    switch (pRcvdBpdu->u1BpduType)
    {
        case AST_BPDU_TYPE_RST:
            u1RcvdPortRole =
                (UINT1) (pRcvdBpdu->u1Flags & PVRST_FLAG_MASK_PORT_ROLE);
            if (u1RcvdPortRole == (UINT1) PVRST_SET_DESG_PROLE_FLAG)
            {
                /* Check if the message is 'Superior Designated' or
                 * 'Repeated Designated'.
                 */
                i4RetVal =
                    (INT4) PvrstPortInfoSmGetMsgType (pPerStPortInfo,
                                                      pRcvdBpdu);
                switch (i4RetVal)
                {
                    case PVRST_BETTER_MSG:
                        return PVRST_SUPERIOR_DESG_MSG;

                    case PVRST_INFERIOR_MSG:
                        /* Classify inferior info received from previous
                           Designated port as Superior */
                        if (AST_MEMCMP (&(pRcvdBpdu->DesgBrgId.BridgeAddr[0]),
                                        &(pPerStPortInfo->DesgBrgId.
                                          BridgeAddr[0]),
                                        AST_MAC_ADDR_SIZE) == 0)
                        {
                            u2RcvPortNum =
                                (UINT2) (pRcvdBpdu->
                                         u2PortId & AST_PORTNUM_MASK);
                            u2DesgPortNum =
                                (UINT2) (pPerStPortInfo->
                                         u2DesgPortId & AST_PORTNUM_MASK);
                            if (u2RcvPortNum == u2DesgPortNum)
                            {
                                return PVRST_SUPERIOR_DESG_MSG;
                            }
                        }

                        /* Changes for IEEE P802.1D/D1 - 2003 Edition 
                         * and IEEE DRAFT P802.1y/D4 
                         * Annex Z.12 */
                        return PVRST_OTHER_MSG;

                    case PVRST_SAME_MSG:

                        if ((pRcvdBpdu->u2MessageAge !=
                             pPerStPvrstRstPortInfo->PortPvrstTimes.
                             u2MsgAgeOrHopCount)
                            || (pRcvdBpdu->u2MaxAge !=
                                pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge)
                            || (pRcvdBpdu->u2FwdDelay !=
                                pPerStPvrstRstPortInfo->PortPvrstTimes.
                                u2ForwardDelay)
                            || (pRcvdBpdu->u2HelloTime !=
                                pPerStPvrstBridgeInfo->RootTimes.u2HelloTime))
                        {
                            return PVRST_SUPERIOR_DESG_MSG;
                        }

                        return PVRST_REPEATED_DESG_MSG;

                    default:
                        /* This condition is not possible */
                        return PVRST_FAILURE;
                }
            }
            /*Changes inline with 802.1D/D4 2003, recommendation in Z2 of 802.1Y D4. */
            else if ((u1RcvdPortRole == (UINT1) PVRST_SET_ROOT_PROLE_FLAG) ||
                     (u1RcvdPortRole == (UINT1) PVRST_SET_ALTBACK_PROLE_FLAG))
            {
                /* Check if the message is 'Confirmed Root'.
                 */
                if ((pAstPortEntry->bOperPointToPoint == PVRST_TRUE) &&
                    ((pRcvdBpdu->u1Flags & PVRST_FLAG_MASK_AGREEMENT)
                     != (UINT1) AST_NO_VAL))
                {
                    i4RetVal =
                        PvrstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);
                    if ((i4RetVal == PVRST_SAME_MSG)
                        || (i4RetVal == PVRST_INFERIOR_MSG))
                    {
                        return PVRST_CONFIRMED_ROOT_MSG;
                    }
                }
                return PVRST_OTHER_MSG;
            }
            else
            {
                return PVRST_OTHER_MSG;
            }

        case AST_BPDU_TYPE_CONFIG:
            i4RetVal = PvrstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu);
            switch (i4RetVal)
            {
                case PVRST_BETTER_MSG:
                    return PVRST_SUPERIOR_DESG_MSG;

                case PVRST_INFERIOR_MSG:
                    /* Classify inferior info received from previous
                       Designated port as Superior */
                    if (AST_MEMCMP (&(pRcvdBpdu->DesgBrgId.BridgeAddr[0]),
                                    &(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                    AST_MAC_ADDR_SIZE) == 0)
                    {
                        u2RcvPortNum =
                            (UINT2) (pRcvdBpdu->u2PortId & AST_PORTNUM_MASK);
                        u2DesgPortNum =
                            (UINT2) (pPerStPortInfo->
                                     u2DesgPortId & AST_PORTNUM_MASK);
                        if (u2RcvPortNum == u2DesgPortNum)
                        {
                            return PVRST_SUPERIOR_DESG_MSG;
                        }
                    }

                    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
                     * Annex Z.12 */
                    return PVRST_OTHER_MSG;

                case PVRST_SAME_MSG:
                    if ((pRcvdBpdu->u2MessageAge !=
                         pPerStPvrstRstPortInfo->PortPvrstTimes.
                         u2MsgAgeOrHopCount)
                        || (pRcvdBpdu->u2MaxAge !=
                            pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge)
                        || (pRcvdBpdu->u2FwdDelay !=
                            pPerStPvrstRstPortInfo->PortPvrstTimes.
                            u2ForwardDelay)
                        || (pRcvdBpdu->u2HelloTime !=
                            pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime))
                    {
                        return PVRST_SUPERIOR_DESG_MSG;
                    }

                    return PVRST_REPEATED_DESG_MSG;

                default:
                    return PVRST_FAILURE;
            }

        default:
            return PVRST_OTHER_MSG;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmGetMsgType                            */
/*                                                                           */
/* Description        : This routine calculates the message type and returns */
/*                      if the message is superior or inferior or same.      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_BETTER_MSG / PVRST_SAME_MSG /                     */
/*                        PVRST_INFERIOR_MSG                                     */
/*****************************************************************************/
INT4
PvrstPortInfoSmGetMsgType (tAstPerStPortInfo * pPerStPortInfo,
                           tPvrstBpdu * pRcvdBpdu)
{
    INT4                i4RetVal = 0;

    i4RetVal = RstCompareBrgId (&(pRcvdBpdu->RootId),
                                &(pPerStPortInfo->RootId));
    switch (i4RetVal)
    {
        case PVRST_BRGID1_SUPERIOR:
            return PVRST_BETTER_MSG;

        case PVRST_BRGID1_INFERIOR:
            return PVRST_INFERIOR_MSG;

        default:
            break;
    }
    /* Now PVRST_BRGID1_SAME */
    if (pRcvdBpdu->u4RootPathCost < pPerStPortInfo->u4RootCost)
    {
        return PVRST_BETTER_MSG;
    }
    else if (pRcvdBpdu->u4RootPathCost > pPerStPortInfo->u4RootCost)
    {
        return PVRST_INFERIOR_MSG;
    }
    i4RetVal = RstCompareBrgId (&(pRcvdBpdu->DesgBrgId),
                                &(pPerStPortInfo->DesgBrgId));
    switch (i4RetVal)
    {
        case PVRST_BRGID1_SUPERIOR:
            return PVRST_BETTER_MSG;
        case PVRST_BRGID1_INFERIOR:
            return PVRST_INFERIOR_MSG;
        default:
            break;
    }

    if (pRcvdBpdu->u2PortId < pPerStPortInfo->u2DesgPortId)
    {
        return PVRST_BETTER_MSG;
    }

    if (pRcvdBpdu->u2PortId > pPerStPortInfo->u2DesgPortId)
    {
        return PVRST_INFERIOR_MSG;
    }

    return PVRST_SAME_MSG;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmUpdtBpduVersion                       */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'updtBpduVersion' procedure.               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*****************************************************************************/
INT4
PvrstPortInfoSmUpdtBpduVersion (tAstPerStPortInfo * pPerStPortInfo,
                                tPvrstBpdu * pRcvdBpdu)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstContextInfo    *pContextInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    pContextInfo = AST_CURR_CONTEXT_INFO ();
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);

    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Updating BPDU version\n",
                  pPerStPortInfo->u2PortNo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
    pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;

    if ((pRcvdBpdu->u1Version == (UINT1) AST_VERSION_0) &&
        ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG) ||
         (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN)))
    {

        pPerStPvrstRstPortInfo->bRcvdStp = PVRST_TRUE;
        pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_FALSE;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Calling Port Migration Machine "
                      "with RCVD_STP event\n", pPerStPortInfo->u2PortNo);

        if (PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_RCVD_STP,
                                       u2PortNum,
                                       pPerStPortInfo->u2Inst) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Migration Machine returned "
                          "FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Migration Machine returned "
                          "FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    else if ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST) &&
             (pContextInfo->u1ForceVersion >= (UINT1) AST_VERSION_2))
    {
        if (pRcvdBpdu->u1IsPvrstPDU != PVRST_TRUE)
        {
            pPerStPvrstRstPortInfo->bRcvdRstp = PVRST_TRUE;
            pPerStPvrstRstPortInfo->bRcvdStp = PVRST_FALSE;
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: Calling Port Migration Machine with "
                          "RCVD_RSTP event\n", pPerStPortInfo->u2PortNo);

            if (PvrstPortMigrationMachine ((UINT2) PVRST_PMIGSM_EV_RCVD_RSTP,
                                           u2PortNum,
                                           pPerStPortInfo->u2Inst) !=
                PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %u: Migration Machine returned FAILURE\n",
                              pPerStPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %u: Migration Machine returned FAILURE\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
    }
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Successfully updated BPDU version\n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmSetTcFlags                            */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'setTcFlags' procedure.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
PvrstPortInfoSmSetTcFlags (tAstPerStPortInfo * pPerStPortInfo,
                           tPvrstBpdu * pRcvdBpdu)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Setting Tc flags\n",
                  pPerStPortInfo->u2PortNo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (((pRcvdBpdu->u1Version == (UINT1) AST_VERSION_0) &&
         (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG)) ||
        ((pRcvdBpdu->u1Version >= (UINT1) AST_VERSION_2) &&
         (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)))

    {
        if ((pRcvdBpdu->u1Flags & PVRST_FLAG_MASK_TC) != (UINT1) AST_NO_VAL)
        {

            pPerStRstPortInfo->bRcvdTc = PVRST_TRUE;
            PVRST_INCR_TC_RX_COUNT (u2PortNum, u2InstIndex);
            AST_GET_SYS_TIME (&u4Ticks);
            pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %u: Calling Topology Change Machine "
                          "with RCVDTC event\n", pPerStPortInfo->u2PortNo);

            if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_RCVDTC,
                                    pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %u: Topology Change Machine Returned"
                              "FAILURE\n", pPerStPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %u: Topology Change Machine Returned"
                              " FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
        if ((pRcvdBpdu->u1Flags & PVRST_FLAG_MASK_TCACK) != (UINT1) AST_NO_VAL)
        {

            pPerStPvrstRstPortInfo->bRcvdTcAck = PVRST_TRUE;
            AST_DBG (AST_PISM_DBG,
                     "PISM: Calling Topology Change Machine with "
                     "RCVDTCACK event\n");

            if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_RCVDTCACK,
                                    pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %u: Topology Change Machine "
                              "Returned FAILURE\n", pPerStPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %u: Topology Change Machine"
                              " Returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
    }
    else if ((pRcvdBpdu->u1Version == (UINT1) AST_VERSION_0) &&
             (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN))
    {

        pPerStPvrstRstPortInfo->bRcvdTcn = PVRST_TRUE;
        AST_DBG (AST_PISM_DBG,
                 "PISM: Calling Topology Change Machine with"
                 " RCVDTCN event\n");

        if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_RCVDTCN,
                                pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Topology Change Machine"
                          " Returned FAILURE\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %u: Topology Change Machine "
                          "Returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Successfully set Tc flags\n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRecordProposed                        */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordProposed' procedure.                */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmRecordProposed (tAstPerStPortInfo * pPerStPortInfo,
                               tPvrstBpdu * pRcvdBpdu)
{
    UINT1               u1TmpFlags = (UINT1) AST_INIT_VAL;

    /* This check is added apart from 802.1w, since, RSTP packets are 
     * accepted in StpCompatible Mode.
     * In StpCompatible mode Proposal/Agreement does not make sense */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        return PVRST_FAILURE;
    }

    /* Oper PointToPoint check has been removed as mentioned in Draft4 of 
     * 8021D 2003. This is necessary for the proper operation of the D4 2003
     * version of BDSM. Once the draft becomes a standard this thread will be 
     * analyzed more thoroughly and necessary changes made. The P2P check on
     * the designated port side will anyway make sure that a non-p2p link will
     * not transition to forwarding rapidly */
    if (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)
    {
        u1TmpFlags = (UINT1) (PVRST_SET_PROPOSAL_FLAG |
                              PVRST_SET_DESG_PROLE_FLAG);
        if ((pRcvdBpdu->u1Flags & u1TmpFlags) == u1TmpFlags)
        {
            return PVRST_SUCCESS;
        }
    }

    AST_UNUSED (pPerStPortInfo);
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmUpdtRcvdInfoWhile                     */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'updtRcvdInfoWhile' procedure.             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo * pPerStPortInfo)
{

    UINT2               u2EffectiveAge = 0;
    UINT2               u2MsgAgeIncrement = 0;
    UINT2               u2RiWhileDuration = 0;
    UINT2               u2ModVal;
    UINT2               u2PortNum = 0;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %u: Updating RcvdInfoWhile\n",
                  pPerStPortInfo->u2PortNo);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    /* Below calculation is done in terms of Centi-Seconds */
    u2ModVal = (UINT2) (pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge %
                        (16 * AST_CENTI_SECONDS));
    u2ModVal = (u2ModVal >= 800) ? (UINT2) (1 * AST_CENTI_SECONDS) : (UINT2) 0;
    u2MsgAgeIncrement =
        (UINT2) (pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge /
                 (16 * AST_CENTI_SECONDS));
    u2MsgAgeIncrement = (UINT2) (u2MsgAgeIncrement + u2ModVal);
    u2MsgAgeIncrement = (u2MsgAgeIncrement == 0) ?
        (UINT2) (1 * AST_CENTI_SECONDS) : u2MsgAgeIncrement;
    u2EffectiveAge =
        (UINT2) (pPerStPvrstRstPortInfo->PortPvrstTimes.u2MsgAgeOrHopCount
                 + u2MsgAgeIncrement);
    if (u2EffectiveAge < pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge)
    {
        u2RiWhileDuration =
            (UINT2) (3 * pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime);

        if (PvrstStartTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             (UINT1) AST_TMR_TYPE_RCVDINFOWHILE,
             u2RiWhileDuration) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %u: Start RcvdInfoWhile Timer "
                          "returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    else
    {
        if (pPerStRstPortInfo->pRcvdInfoTmr != NULL)
        {
            if (PvrstStopTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                                (UINT1) AST_TMR_TYPE_RCVDINFOWHILE)
                != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %u: Stop RcvdInfoWhile Timer "
                              "returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: Effective Age Exceeds MaxAge,"
                      " Message to be aged out !!!\n",
                      pPerStPortInfo->u2PortNo);
        /* u2RiWhileDuration = 0; */
    }
    return PVRST_SUCCESS;
}

/* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
* Annex Z.5 */
/* The actual changes to be made in recordTimes () is as per 802.1D/D1 
* 2003 Edition as there is a contradiction between 802.1D and 802.1y */

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRecordTimes                           */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordTimes' procedure as given in        */
/*                      P802.1D/D1 - 2003 Edition.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                     */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstPortInfoSmRecordTimes (tAstPerStPortInfo * pPerStPortInfo,
                            tPvrstBpdu * pRcvdBpdu)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT2               u2PortNum = pPerStPortInfo->u2PortNo;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return;
    }
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge = pRcvdBpdu->u2MaxAge;
    if (pRcvdBpdu->u2HelloTime <= AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL))
    {
        pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime =
            AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL);
    }
    else
    {
        pPerStPvrstRstPortInfo->PortPvrstTimes.u2HelloTime =
            pRcvdBpdu->u2HelloTime;
    }
    pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay =
        pRcvdBpdu->u2FwdDelay;
    pPerStPvrstRstPortInfo->PortPvrstTimes.u2MsgAgeOrHopCount =
        pRcvdBpdu->u2MessageAge;
    pPerStPvrstRstPortInfo->PortPvrstTimes.u2MaxAge = pRcvdBpdu->u2MaxAge;
    return;
}

    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmRecordDispute                         */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordDispute' procedure as given in      */
/*                      P802.1D/D1 - 2003 Edition.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                     port information.                     */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                              tPvrstBpdu * pRcvdBpdu)
{
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    if (pRcvdBpdu->u1Flags & PVRST_FLAG_MASK_LEARNING)
    {
        pPerStPortInfo->bDisputed = PVRST_TRUE;
        AST_DBG_ARG1 (AST_PISM_DBG,
                      "PISM: Port %u: DISPUTED Set...\n",
                      pPerStPortInfo->u2PortNo);
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     VlanId,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "VLAN %u Port:%s Disputed state occured at : %s\n",
                              VlanId,
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              au1TimeStr);
            pPerStPortInfo->PerStRstPortInfo.bAgreed = PVRST_FALSE;
        }
    }
}
#endif /* IEEE8021Y_Z12_WANTED */

/*****************************************************************************/
/* Function Name      : PvrstPortInfoSmEventImpossible                       */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_FAILURE                                        */
/*****************************************************************************/
INT4
PvrstPortInfoSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo,
                                tPvrstBpdu * pRcvdBpdu)
{
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                  "PISM-PVRST: Port %u: IMPOSSIBLE EVENT/STATE "
                  "Combination Occurred\n", pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                  "PISM-PVRST: Port %u: IMPOSSIBLE EVENT/STATE "
                  "Combination Occurred\n", pPerStPortInfo->u2PortNo);
    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return PVRST_FAILURE;
    }

    AST_UNUSED (pRcvdBpdu);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitPortInfoMachine                             */
/*                                                                           */
/* Description        : Initialises the Port Information State Machine       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstInitPortInfoMachine (VOID)
{
/*#if defined(RST_TRACE_WANTED) || defined(RST_DEBUG)*/
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_PINFOSM_MAX_EVENTS][20] = {
        "BEGIN", "PORT_ENABLED", "PORT_DISABLED",
        "RCVD_BPDU", "RCVDINFOWHILE_EXP",
        "UPDATE_INFO"
    };
#ifdef IEEE8021Y_Z12_WANTED
#ifdef IEEE8021Y_Z11_WANTED
    UINT1               aau1SemState[PVRST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "NOT_DESG",
        "CURRENT", "RECEIVE", "INFERIOR_DESG"
    };
#else
    UINT1               aau1SemState[PVRST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "AGREEMENT" "CURRENT",
        "RECEIVE", "INFERIOR_DESG"
    };
#endif /*End of IEEE8021Y_Z11_WANTED */

#else /* IEEE8021Y_Z12_WANTED */
#ifdef IEEE8021Y_Z11_WANTED
    UINT1               aau1SemState[PVRST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "NOT_DESG",
        "CURRENT", "RECEIVE"
    };
#else
    UINT1               aau1SemState[PVRST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEAT", "AGREEMENT" "CURRENT", "RECEIVE"
    };
#endif /*End of IEEE8021Y_Z11_WANTED */

#endif /* IEEE8021Y_Z12_WANTED */

    for (i4Index = 0; i4Index < PVRST_PINFOSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PISM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PISM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_PINFOSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PISM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PISM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /*#endif *//* PVRST_TRACE_WANTED || PVRST_DEBUG */
    /* Event 0 - PVRST_PINFOSM_EV_BEGIN */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_CURRENT].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_BEGIN]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    /* Event 1 - PVRST_PINFOSM_EV_PORT_ENABLED */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmMakeAged;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_CURRENT].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_ENABLED]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    /* Event 2 - PVRST_PINFOSM_EV_PORT_DISABLED */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_CURRENT].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_PORT_DISABLED]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    /* Event 3 - PVRST_PINFOSM_EV_RCVD_BPDU */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_CURRENT].pAction = PvrstPortInfoSmMakeReceive;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVD_BPDU]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    /* Event 4 - PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_CURRENT].pAction =
        PvrstPortInfoSmRiWhileExpCurrent;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    /* Event 5 - PVRST_PINFOSM_EV_UPDATEINFO */
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_DISABLED].pAction = PvrstPortInfoSmMakeDisabled;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_AGED].pAction = PvrstPortInfoSmMakeUpdate;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_UPDATE].pAction = PvrstPortInfoSmEventImpossible;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_SUPERIOR].pAction = NULL;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_REPEAT].pAction = PvrstPortInfoSmEventImpossible;
#ifdef IEEE8021Y_Z11_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_NOT_DESG].pAction = PvrstPortInfoSmEventImpossible;
#else
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_AGREEMENT].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /*End of IEEE8021Y_Z11_WANTED */

    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_CURRENT].pAction = PvrstPortInfoSmUpdtInfoCurrent;
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_RECEIVE].pAction = PvrstPortInfoSmEventImpossible;
    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    PVRST_PORT_INFO_MACHINE[PVRST_PINFOSM_EV_UPDATEINFO]
        [PVRST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        PvrstPortInfoSmEventImpossible;
#endif /* IEEE8021Y_Z12_WANTED */

    AST_DBG (AST_TCSM_DBG, "PISM: Loaded PISM SEM successfully\n");
    return;
}
