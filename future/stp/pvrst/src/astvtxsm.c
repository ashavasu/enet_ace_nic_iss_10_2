/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvtxsm.c,v 1.33.2.1 2018/04/26 12:31:07 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Transmit State Event Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : PvrstPortTransmitMachine                             */
/*                                                                           */
/* Description        : This is the main routine for the Port Transmit State */
/*                      Machine. This function is called whenever a BPDU     */
/*                      (TCN Bpdu, CONFIG Bpdu, RST Bpdu or PVRST Bpdu) needs*/
/*                      to be transmitted.                                   */
/*                                                                           */
/* Input(s)           : u1Event - The Event that has caused the Port Transmit*/
/*                                Machine to be called                       */
/*                      pPerStPortInfo - The pointer to the instance-         */
/*                                       specific Port information              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortTransmitMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2State = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Event %s: Invalid parameter passed "
                      "to event handler routine\n",
                      gaaau1AstSemEvent[AST_PISM][u2Event]);
        return PVRST_FAILURE;
    }
    VlanId = pPerStPortInfo->u2Inst;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);
    if (NULL == pPerStPvrstRstPortInfo)
    {
        return PVRST_FAILURE;
    }

    u2State = (UINT2) pPerStPvrstRstPortInfo->u1PortTxSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "TXSM: Port %u: Tx Machine Called with Event:"
                  " %s,State: %s of Instance %u\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_TXSM][u2Event],
                  gaaau1AstSemState[AST_TXSM][u2State], VlanId);

    AST_DBG_ARG4 (AST_TXSM_DBG,
                  "TXSM: Port %u: Tx Machine Called with Event:"
                  " %s,State: %s of Instance %u\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_TXSM][u2Event],
                  gaaau1AstSemState[AST_TXSM][u2State], VlanId);

    if ((PVRST_PORT_TX_MACHINE[u2Event][u2State].pAction) == NULL)
    {
        AST_DBG (AST_TXSM_DBG, "TXSM: No Operations to perform\n");
        return PVRST_SUCCESS;
    }
    if ((*(PVRST_PORT_TX_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: Port Transmit Event Function returned " "FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeTransmitInit                        */
/*                                                                           */
/* Description        : This function is called during initialisation of the */
/*                      Port Transmit State Machine. This changes the state  */
/*                      of the Port Transmit state machine to TRANSMIT_INIT  */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmMakeTransmitInit (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    }

    if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
    {
        if (PvrstStopTimer
            ((VOID *) pPerStPortInfo, u2InstIndex,
             (UINT1) AST_TMR_TYPE_HELLOWHEN) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstStopTimer for HelloWhenTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    pPerStPvrstRstPortInfo->u1TxCount = (UINT1) AST_INIT_VAL;

    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_INIT;

    AST_DBG_ARG1 (AST_TXSM_DBG, "TXSM: Port %u: Moved to "
                  "state TRANSMIT_INIT\n", u2PortNum);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) ||
        (pAstPortEntry->u1EntryStatus == CFA_IF_DOWN) ||
        (pRstPortInfo->bPortEnabled == RST_FALSE))
    {
        return RST_SUCCESS;
    }

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function " "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeTransmitConfig                      */
/*                                                                           */
/* Description        : This function is called whenever a Config BPDU needs */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TRANSMIT_CONFIG state.      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortTxSmMakeTransmitConfig (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    }
    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        return PVRST_SUCCESS;
    }
    if (PvrstPortTxSmTxConfig (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmConfig function returned" " FAILURE!\n");
        return PVRST_FAILURE;
    }

    /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
    (pPerStPvrstRstPortInfo->u1TxCount)++;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Config BPDU sent, incremented TxCount is: %u\n",
                  pPerStPvrstRstPortInfo->u1TxCount);

    pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;

    if (pPerStPvrstRstPortInfo->pHoldTmr == NULL)
    {
        if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_HOLD,
                             AST_HOLD_TIME) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstStartTimer  for HoldTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_CONFIG;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %u: Moved to state TRANSMIT_CONFIG\n", u2PortNum);

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeTransmitTcn                         */
/*                                                                           */
/* Description        : This function is called whenever a TCN BPDU needs    */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TANSMIT_TCN state.          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmMakeTransmitTcn (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    }
    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        return PVRST_SUCCESS;
    }

    if (PvrstPortTxSmTxTcn (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmTcn function returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
    (pPerStPvrstRstPortInfo->u1TxCount)++;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: TCN BPDU sent, incremented TxCount is: %u\n",
                  pPerStPvrstRstPortInfo->u1TxCount);

    if (pPerStPvrstRstPortInfo->pHoldTmr == NULL)
    {
        if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_HOLD,
                             AST_HOLD_TIME) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstStartTimer for HoldTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }

    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_TCN;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %u: Moved to state TRANSMIT_TCN\n", u2PortNum);

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function returned" " FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeTransmitRstp                        */
/*                                                                           */
/* Description        : This function is called whenever a RST BPDU needs    */
/*                      to be transmitted. This is called when the Port Tx   */
/*                      state machine is in the IDLE state and this function */
/*                      changes the state to the TANSMIT_RSTP state.         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortTxSmMakeTransmitRstp (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        return PVRST_SUCCESS;
    }
    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
        if (PvrstPortTxSmTxRstp (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstPortTxSmRstp function returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }

    /* Increment the TxCount by 1 since 1 BPDU has been transmitted */
    (pPerStPvrstRstPortInfo->u1TxCount)++;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: RST BPDU sent, incremented TxCount is: %u\n",
                  pPerStPvrstRstPortInfo->u1TxCount);

    pPerStPvrstRstPortInfo->bTcAck = PVRST_FALSE;

    if (pPerStPvrstRstPortInfo->pHoldTmr == NULL)
    {
        if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                             (UINT1) AST_TMR_TYPE_HOLD,
                             AST_HOLD_TIME) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstStartTimer for HoldTmr FAILED!\n");
            return PVRST_FAILURE;
        }
    }
    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_RSTP;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %u: Moved to state TRANSMIT_RSTP\n", u2PortNum);

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeTransmitPeriodic                    */
/*                                                                           */
/* Description        : This function is called whenever the Hellowhen timer */
/*                      for this port expires and this might result in the   */
/*                      periodic transmission of Hello messages on this port */
/*                      This is called when the Port Tx state machine is in  */
/*                      the IDLE state and this function changes the state to*/
/*                      the TRANSMIT_PERIODIC state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmMakeTransmitPeriodic (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2HwDuration = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);
    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);

    if (AST_IS_PVRST_ENABLED ())
    {
        u2HwDuration = pPerStPvrstBridgeInfo->RootTimes.u2HelloTime;
    }

    /* The starting of the Hello When Timer has been done at the beginning
     * of this function, even before any of the other conditions have been
     * checked, in order to ensure that even if other conditions are not met,
     * the Hello When timer for this port is still re-started. */
    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_HELLOWHEN,
                         u2HwDuration) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstStartTimer for HelloWhenTmr FAILED!\n");
        return PVRST_FAILURE;
    }
    AST_DBG (AST_TXSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
             "TXSM: PvrstStartTimer for HelloWhenTmr STARTED!\n");

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG (AST_TXSM_DBG,
                 "TXSM: Selected,UpdtInfo is not Valid - Exiting.\n");
        return PVRST_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if ((AST_IS_PVRST_ENABLED ()) &&
        (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))

    {
        /* Any failed NPAPI calls are retried every hello time. */
        if (pPerStPortInfo->i4NpFailRetryCount < AST_MAX_RETRY_COUNT)
        {
            pPerStPortInfo->i4NpFailRetryCount++;

            if (pPerStPortInfo->i4NpPortStateStatus == AST_BLOCK_FAILURE)
            {
                PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                         AST_GET_IFINDEX (u2PortNum),
                                         VlanId, AST_PORT_STATE_DISCARDING);
            }
            else if (pPerStPortInfo->i4NpPortStateStatus == AST_FORWARD_FAILURE)
            {
                if (pPerStPortInfo->i4NpFailRetryCount < AST_MAX_RETRY_COUNT)
                {
                    PvrstHwSetVlanPortState (AST_CURR_CONTEXT_ID (),
                                             AST_GET_IFINDEX (u2PortNum),
                                             VlanId, AST_PORT_STATE_FORWARDING);
                }
            }
        }
    }
#endif /* NPAPI_WANTED */
    if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED) ||
        ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT) &&
         (pPerStRstPortInfo->pTcWhileTmr != NULL)))
    {
        if (AST_IS_PVRST_ENABLED ())
        {
            pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
        }
    }
#ifdef NPAPI_WANTED
    else if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
    {
        /* If setting port state to blocking in hardware has failed, 
         * then NewInfo should be set even on Alternate or Backup ports
         * in order to send inferior info to the peer */
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
    }
#endif /* NPAPI_WANTED */

    pPerStPvrstRstPortInfo->u1PortTxSmState =
        (UINT1) PVRST_PTXSM_STATE_TRANSMIT_PERIODIC;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %u: Moved to state TRANSMIT_PERIODIC\n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function returned FAILURE!\n");
        return PVRST_FAILURE;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmMakeIdle                                */
/*                                                                           */
/* Description        : This function is called from all the other states of */
/*                      the Port Transmit state machine. This is called when */
/*                      the Port Tx State machine is in any of the other     */
/*                      states (TRANSMIT_INIT, TRANSMIT_PERIODIC, TRANSMIT_  */
/*                      CONFIG, TRANSMIT_TCN or TRANSMIT_RSTP) and this      */
/*                      function changes the state to the IDLE state.        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmMakeIdle (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBoolean         bTmpNewInfo = PVRST_FALSE;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStPvrstRstPortInfo->u1PortTxSmState = (UINT1) PVRST_PTXSM_STATE_IDLE;

    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: Port %u: Moved to state IDLE\n", u2PortNum);

    if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr == NULL)
    {
        if (PvrstPortTxSmMakeTransmitPeriodic (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstPortTxSmMakeTransmitPeriodic function "
                     "returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }

    if (AST_IS_PVRST_ENABLED ())
    {
        bTmpNewInfo = pPerStPvrstRstPortInfo->bNewInfo;
    }

    if (bTmpNewInfo == PVRST_TRUE)
    {

        if (PvrstPortTxSmNewInfoSetIdle (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: PvrstPortTxSmNewInfoSetIdle function "
                     "returned FAILURE!\n");
            return PVRST_FAILURE;
        }
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmNewInfoSetIdle                          */
/*                                                                           */
/* Description        : This function is called when a NewInfo set event is  */
/*                      received from the Port Information state machine or  */
/*                      the Port Role Transtion state machine, when the Port */
/*                      Transmit State Machine is in the IDLE state.         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmNewInfoSetIdle (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstBridgeInfo *pPerStPvrstBridgeInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    BOOL1               bTunnelPort = OSIX_FALSE;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pAstPortEntry->u1EntryStatus != AST_PORT_OPER_UP)
    {
        return PVRST_SUCCESS;
    }

    /* Checking for VLAN status becos VlanIsTunnellingEnabled does not check
     * for it.
     */
    AstL2IwfGetPortVlanTunnelStatus (pAstPortEntry->u4IfIndex, &bTunnelPort);

    if (bTunnelPort == OSIX_TRUE)
    {
        /* STP BPDUs should not be sent on VLAN tunnel ports. So
         * just return.  */
        AST_DBG_ARG1 (AST_TXSM_DBG,
                      "VLAN Tunnelling enabled on port - %d. BPDU not"
                      "transmitted \n", u2PortNum);
        return PVRST_SUCCESS;
    }
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG (AST_TXSM_DBG,
                 "TXSM: Selected,UpdtInfo is not Valid - Exiting.\n");
        return PVRST_SUCCESS;
    }

    pPerStPvrstBridgeInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE)
    {
        if (pPerStPvrstRstPortInfo->u1TxCount <
            pPerStPvrstBridgeInfo->u1TxHoldCount)
        {
            if (AST_IS_PVRST_ENABLED ())
            {
                if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
                {

                    if (PvrstPortTxSmMakeTransmitRstp
                        (pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: PvrstPortTxSmMakeTransmitRstp function "
                                 "returned FAILURE!\n");
                        return PVRST_FAILURE;
                    }
                }
            }
        }                        /* Tx Count within Hold Count limits */
        else
        {
            AST_DBG_ARG1 (AST_TXSM_DBG,
                          "TXSM: Port %u: Hold Rate EXCEEDED, Not Sending"
                          " BPDU...\n", u2PortNum);
        }

    }                            /* End of bSendRstp is PVRST_TRUE */
    else
    {                            /* bSendRstp is PVRST_FALSE */
        if (pPerStPvrstRstPortInfo->u1TxCount <
            pPerStPvrstBridgeInfo->u1TxHoldCount)
        {
            if (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL)
            {
                if ((pPerStPortInfo->u1PortRole == (UINT1)
                     AST_PORT_ROLE_ROOT)
                    && (pPerStRstPortInfo->pTcWhileTmr != NULL))
                {
                    if (PvrstPortTxSmMakeTransmitTcn (pPerStPortInfo)
                        != PVRST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: PvrstPortTxSmMakeTransmitTcn function "
                                 "returned FAILURE!\n");
                        return PVRST_FAILURE;
                    }            /* Root Port Role */
                }
                else if (pPerStPortInfo->u1PortRole
                         == (UINT1) AST_PORT_ROLE_DESIGNATED)
                {
                    if (PvrstPortTxSmMakeTransmitConfig
                        (pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                 "TXSM: PvrstPortTxSmMakeTransmitConfig function "
                                 "returned FAILURE\n");
                        return PVRST_FAILURE;
                    }
                }                /* Designated Port Role */
            }                    /* HelloWhenTmr is running */
        }                        /* Tx Count within Hold Count limits */
        else
        {
            AST_DBG_ARG1 (AST_TXSM_DBG,
                          "TXSM: Port %u: Hold Rate EXCEEDED, Not "
                          "Sending BPDU...\n", u2PortNum);
        }

    }                            /* End of bSendRstp is PVRST_FALSE */

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmHoldTmrExpIdle                          */
/*                                                                           */
/* Description        : This function is called when Hold timer expiry event */
/*                      is received from the Timer Submodule, when the Port  */
/*                      Transmit State Machine is in the IDLE state.         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortTxSmHoldTmrExpIdle (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstBoolean         bTmpNewInfo = PVRST_FALSE;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (AST_IS_PVRST_ENABLED ())
    {
        bTmpNewInfo = pPerStPvrstRstPortInfo->bNewInfo;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG (AST_TXSM_DBG,
                 "TXSM: Selected,UpdtInfo is not Valid - Exiting.\n");
        return PVRST_SUCCESS;
    }

    if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_TRUE)
    {
        if ((bTmpNewInfo == PVRST_TRUE) &&
            (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL) &&
            ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED) ||
             (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)))
        {
            if (PvrstPortTxSmMakeTransmitRstp (pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                         "TXSM: PvrstPortTxSmMakeTransmitRstp function "
                         "returned FAILURE!\n");
                return PVRST_FAILURE;
            }
        }
    }
    else
    {                            /* bSendRstp is PVRST_FALSE */
        if ((bTmpNewInfo == PVRST_TRUE) &&
            (pPerStPvrstRstPortInfo->pHelloWhenPvrstTmr != NULL))
        {
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                if (PvrstPortTxSmMakeTransmitTcn (pPerStPortInfo)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                             "TXSM: PvrstPortTxSmMakeTransmitTcn function "
                             "returned FAILURE!\n");
                    return PVRST_FAILURE;
                }                /* Root Port Role */
            }
            else if (pPerStPortInfo->u1PortRole ==
                     (UINT1) AST_PORT_ROLE_DESIGNATED)
            {
                if (PvrstPortTxSmMakeTransmitConfig (pPerStPortInfo)
                    != PVRST_SUCCESS)
                {
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                             "TXSM: PvrstPortTxSmMakeTransmitConfig function "
                             "returned FAILURE\n");
                    return PVRST_FAILURE;
                }
            }                    /* Designated Port Role */
        }
    }                            /* bSendRstp is PVRST_FALSE */
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmTxConfig                                */
/*                                                                           */
/* Description        : This function is called by PvrstPortTxSmMakeTransmit */
/*                      Config function in order to transmit a Config BPDU.  */
/*                                                                           */
/* Input(s)           :pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortTxSmTxConfig (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBufChainHeader *pConfigBpdu = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }
    if (pPerStPortInfo->bRootInconsistent == PVRST_TRUE)
    {
        return PVRST_SUCCESS;
    }
    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (pRstPortInfo->pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        return PVRST_SUCCESS;
    }
    if ((u2InstIndex != AST_DEF_VLAN_ID ())
        && ((AST_FORCE_VERSION == AST_VERSION_0)
            || (pRstPortInfo->pPerStPvrstRstPortInfo->bSendRstp ==
                PVRST_FALSE)))
    {
        return PVRST_SUCCESS;
    }

    if ((pConfigBpdu = AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_CONFIG +
                                          AST_ENET_LLC_HEADER_SIZE +
                                          PVRST_ENCAP_OVERHEADS,
                                          AST_OFFSET)) == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        AST_INCR_BUFFER_FAILURE_COUNT ();
        AstBufferFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                           PVRST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: Config Bpdu Allocation of CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: Config Bpdu Allocation of CRU Buffer FAILED!\n");

        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "TXSM-PVRST: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return PVRST_FAILURE;
        }

        return PVRST_SUCCESS;
    }

    if (PvrstFormBpdu (pPerStPortInfo, (UINT1) AST_BPDU_TYPE_CONFIG,
                       pConfigBpdu, &u4DataLength, &u1Flag) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pConfigBpdu, PVRST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: Config Bpdu Release CRU Buffer FAILED!\n");
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: Config Bpdu Release CRU Buffer FAILED!\n");
        }
        return PVRST_FAILURE;
    }
    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
             AST_BUFFER_TRC, "TXSM: PvrstFormBpdu(Config) Success!\n");

    if (PvrstFillEnetHeader (pConfigBpdu, &u4DataLength,
                             pPerStPortInfo) == PVRST_FAILURE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                      AST_BUFFER_TRC,
                      "TXSM: Port %u: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Port %u: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n", pPerStPortInfo->u2PortNo);

        AST_RELEASE_CRU_BUF (pConfigBpdu, PVRST_FALSE);
        return PVRST_FAILURE;
    }

    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
#ifdef NPAPI_WANTED
        if (gAstAllowDirectNpTx == RST_TRUE)
        {
            if (AstHandlePacketToNP (pConfigBpdu,
                                     pAstPortEntry->u4IfIndex,
                                     u4DataLength) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                              "Unable to transmit packet\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
                return PVRST_FAILURE;
            }
        }
        else
        {
            AstHandleOutgoingPktOnPort (pConfigBpdu, pAstPortEntry,
                                        u4DataLength, AST_PROT_BPDU,
                                        (UINT1) AST_ENCAP_NONE);
        }
#else

        AstHandleOutgoingPktOnPort (pConfigBpdu, pAstPortEntry, u4DataLength,
                                    AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);
#endif
    }
    else
    {
        AST_RELEASE_CRU_BUF (pConfigBpdu, PVRST_FALSE);
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TXSM: <<<<< Port %u: Config BPDU handed over "
                  "to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: <<<<< Port %u: Config BPDU handed over "
                  "to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);

    /* Incrementing the Config BPDU Transmit count */
    PVRST_INCR_TX_CONFIG_BPDU_COUNT (pPerStPortInfo->u2PortNo, u2InstIndex);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmTxTcn                                   */
/*                                                                           */
/* Description        : This function is called by                             */
/*                        PvrstPortTxSmMakeTransmitTcn                         */
/*                      function in order to transmit a TCN BPDU.            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmTxTcn (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBufChainHeader *pTcnBpdu = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }

    if ((pTcnBpdu = AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_TCN +
                                       AST_ENET_LLC_HEADER_SIZE
                                       + PVRST_ENCAP_OVERHEADS, AST_OFFSET))
        == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        AST_INCR_BUFFER_FAILURE_COUNT ();
        AstBufferFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                           PVRST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: TCN Bpdu Allocation of CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: TCN Bpdu Allocation of CRU Buffer FAILED!\n");
        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "TXSM-PVRST: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return PVRST_FAILURE;
        }

        return PVRST_SUCCESS;
    }

    if (PvrstFormBpdu (pPerStPortInfo, (UINT1) AST_BPDU_TYPE_TCN,
                       pTcnBpdu, &u4DataLength, &u1Flag) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pTcnBpdu, PVRST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: TCN Bpdu Release CRU Buffer FAILED!\n");
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                     "TXSM: TCN Bpdu Release CRU Buffer FAILED!\n");
        }
        return PVRST_FAILURE;
    }

    if (PvrstFillEnetHeader (pTcnBpdu, &u4DataLength, pPerStPortInfo)
        == PVRST_FAILURE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                      AST_BUFFER_TRC,
                      "TXSM: Port %u: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                      "TXSM: Port %u: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n", pPerStPortInfo->u2PortNo);

        AST_RELEASE_CRU_BUF (pTcnBpdu, PVRST_FALSE);
        return PVRST_FAILURE;
    }

    PvrstEncapsulateBpdu (VlanId, pTcnBpdu, &u4DataLength,
                          pPerStPortInfo->u2PortNo);

    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
#ifdef NPAPI_WANTED
        if (gAstAllowDirectNpTx == RST_TRUE)
        {
            if (AstHandlePacketToNP (pTcnBpdu,
                                     pAstPortEntry->u4IfIndex,
                                     u4DataLength) != RST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                              "Unable to transmit packet\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
                return PVRST_FAILURE;
            }
        }
        else
        {
            AstHandleOutgoingPktOnPort (pTcnBpdu, pAstPortEntry, u4DataLength,
                                        AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);

        }
#else
        AstHandleOutgoingPktOnPort (pTcnBpdu, pAstPortEntry, u4DataLength,
                                    AST_PROT_BPDU, (UINT1) AST_ENCAP_NONE);
#endif
    }
    else
    {
        AST_RELEASE_CRU_BUF (pTcnBpdu, PVRST_FALSE);
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "TXSM: <<<<< Port %u: TCN BPDU handed "
                  "over to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_TXSM_DBG,
                  "TXSM: <<<<< Port %u: TCN BPDU handed "
                  "over to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);

    /* Incrementing the TCN BPDU Transmit count */
    PVRST_INCR_TX_TCN_BPDU_COUNT (pPerStPortInfo->u2PortNo, u2InstIndex);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmTxRstp                                  */
/*                                                                           */
/* Description        : This function is called by PvrstPortTxSmMakeTransmit */
/*                      Rstp function in order to transmit a RST BPDU.       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmTxRstp (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstBufChainHeader *pRstBpdu = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1               u1PortType;
    UINT2               u2Inst = AST_INIT_VAL;
    BOOL1               bSendRstp = AST_FALSE;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT4               u4Ticks = AST_INIT_VAL;
    BOOL1               bSendPdu = AST_TRUE;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    AstL2IwfGetVlanPortType (pPerStPortInfo->u2PortNo, &u1PortType);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }
    if (L2IwfMiIsVlanMemberPort
        (AST_CURR_CONTEXT_ID (), AST_DEF_VLAN_ID (),
         pAstPortEntry->u2PortNo) == OSIX_TRUE)
    {
        u2Inst =
            AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                          AST_DEF_VLAN_ID ());
        if (INVALID_INDEX == u2Inst)
        {
            return PVRST_FAILURE;
        }
        if (NULL !=
            PVRST_GET_PERST_RST_PORT_INFO (pAstPortEntry->u2PortNo, u2Inst))
        {
            /* The port acting in STP version should not be sending RSTP BPDU's.
             * In case, the port is not a member of default VLAN then
             * skipping the verification of STP version */

            pPerStPvrstRstPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                     u2Inst);
            if (pPerStPvrstRstPortInfo == NULL)
            {
                return PVRST_FAILURE;
            }
            if (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE)
            {
                bSendPdu = PVRST_FALSE;
            }
        }
    }

    if (bSendPdu == AST_TRUE)
    {
        /*Send RSTP PDU and PVRST PDU */
        do
        {
            if ((pRstBpdu =
                 AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_RST +
                                    AST_ENET_LLC_HEADER_SIZE +
                                    PVRST_ENCAP_OVERHEADS, AST_OFFSET)) == NULL)
            {
                /* Incrementing the Buffer Failure Count */
                AST_INCR_BUFFER_FAILURE_COUNT ();
                AstBufferFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                                   PVRST_BRG_TRAPS_OID_LEN);
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                         AST_BUFFER_TRC,
                         "TXSM: RST Bpdu Allocation of CRU Buffer FAILED!\n");
                AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                         "TXSM: RST Bpdu Allocation of CRU Buffer FAILED!\n");
                AST_DBG_ARG1 (AST_ALL_FLAG,
                              "TXSM-PVRST: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                              AST_CONTEXT_STR ());

                if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) !=
                    RST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG |
                                  AST_TMR_DBG,
                                  "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                                  AST_GET_IFINDEX_STR (pAstPortEntry->
                                                       u2PortNo));

                    return PVRST_FAILURE;
                }

                return PVRST_SUCCESS;
            }

            pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

            if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) ||
                (pAstPortEntry->u1EntryStatus == CFA_IF_DOWN) ||
                (pRstPortInfo->bPortEnabled == RST_FALSE) ||
                (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_FALSE))
            {
                if (AST_RELEASE_CRU_BUF (pRstBpdu, PVRST_FALSE) !=
                    AST_CRU_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_BUFFER_TRC,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                    return PVRST_FAILURE;
                }

                return PVRST_SUCCESS;
            }

            if (PvrstFormBpdu (pPerStPortInfo, (UINT1) AST_BPDU_TYPE_RST,
                               pRstBpdu, &u4DataLength,
                               &u1Flag) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                         "TXSM: PvrstFormBpdu function returned FAILURE!\n");

                if (AST_RELEASE_CRU_BUF (pRstBpdu, PVRST_FALSE) !=
                    AST_CRU_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_BUFFER_TRC,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                }
                return PVRST_FAILURE;
            }

            if (PvrstFillEnetHeader (pRstBpdu, &u4DataLength, pPerStPortInfo)
                == PVRST_FAILURE)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                              AST_BUFFER_TRC,
                              "TXSM: Port %u: Fill Ethernet Header FAILED!."
                              "Unable to transmit packet\n",
                              pPerStPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Port %u: Fill Ethernet Header FAILED!."
                              "Unable to transmit packet\n",
                              pPerStPortInfo->u2PortNo);

                if (AST_RELEASE_CRU_BUF (pRstBpdu, PVRST_FALSE) !=
                    AST_CRU_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_BUFFER_TRC,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                             "TXSM: RST Bpdu Release CRU Buffer FAILED!\n");
                }

                return PVRST_FAILURE;
            }

            /* If Vlan Id is 1(default vlan) and port is not a access port.
               send two frames
               1. PVRST packet
               2. RSTP packet
             */

            if (bSendRstp == AST_FALSE)
            {
                PvrstEncapsulateBpdu (VlanId, pRstBpdu, &u4DataLength,
                                      pPerStPortInfo->u2PortNo);
            }

            if (AST_NODE_STATUS () == RED_AST_ACTIVE)
            {
                pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
#ifdef NPAPI_WANTED
                if (gAstAllowDirectNpTx == RST_TRUE)
                {
                    if (AstHandlePacketToNP (pRstBpdu,
                                             pAstPortEntry->u4IfIndex,
                                             u4DataLength) != RST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                                      "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                                      "Unable to transmit packet\n",
                                      AST_GET_IFINDEX_STR (pAstPortEntry->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
                else
                {
                    AstHandleOutgoingPktOnPort (pRstBpdu, pAstPortEntry,
                                                u4DataLength, AST_PROT_BPDU,
                                                (UINT1) AST_ENCAP_NONE);

                }
#else
                AstHandleOutgoingPktOnPort (pRstBpdu, pAstPortEntry,
                                            u4DataLength, AST_PROT_BPDU,
                                            (UINT1) AST_ENCAP_NONE);
#endif
            }
            else
            {
                AST_RELEASE_CRU_BUF (pRstBpdu, PVRST_FALSE);
            }

            pRstBpdu = NULL;

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "TXSM: <<<<< Port %u: RST BPDU handed over "
                          "to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_TXSM_DBG,
                          "TXSM: <<<<< Port %u: RST BPDU handed over "
                          "to Bridge >>>>>\n", pPerStPortInfo->u2PortNo);

            /* Incrementing the RST BPDU Transmit count */
            PVRST_INCR_TX_INST_BPDU_COUNT (pPerStPortInfo->u2PortNo,
                                           u2InstIndex);
            /* Incrementing the proposal and agreement packet transmitted count */
            if (u1Flag == (UINT1) PVRST_SET_AGREEMENT_FLAG)
            {
                AST_GET_SYS_TIME (&u4Ticks);
                PVRST_INCR_TX_AGREEMENT_COUNT (pPerStPortInfo->u2PortNo,
                                               u2InstIndex);
                pPerStPortInfo->u4AgreementTxTimeStamp = u4Ticks;
            }
            if (u1Flag == (UINT1) PVRST_SET_PROPOSAL_FLAG)
            {
                AST_GET_SYS_TIME (&u4Ticks);
                PVRST_INCR_TX_PROPOSAL_COUNT (pPerStPortInfo->u2PortNo,
                                              u2InstIndex);
                pPerStPortInfo->u4ProposalTxTimeStamp = u4Ticks;
            }

            if ((VlanId == AST_DEF_VLAN_ID ())
                && (u1PortType != VLAN_ACCESS_PORT))
            {
                if (bSendRstp == AST_FALSE)
                {
                    /* PVRST PDU sent and RST need to be sent for
                       VLAN ID 1(default vlan).
                     */
                    bSendRstp = AST_TRUE;
                }
                else
                {
                    /* PVRST and RST PDU sent for 
                       VLAN ID 1(default vlan).
                     */
                    bSendRstp = AST_FALSE;
                }
            }

        }
        while (bSendRstp == AST_TRUE);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstFormBpdu                                        */
/*                                                                           */
/* Description        : This function is called by PvrstPortTxSmTxConfig,    */
/*                      PvrstPortTxSmTxTcn or PvrstPortTxSmTxRstp functions  */
/*                      in order to form a Config BPDU, TCN BPDU or RST BPDU.*/
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                      u1MsgType - Indicates the type of BPDU to be formed  */
/*                                  AST_BPDU_TYPE_TCN or                     */
/*                                  AST_BPDU_TYPE_CONFIG or                  */
/*                                  AST_BPDU_TYPE_RST                        */
/*                      pBuf - Pointer to the CRU Buffer in which the BPDU   */
/*                             is to be formed                               */
/*                      pu4DataLength - The number of bytes that have been   */
/*                                      filled in the BPDU                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstFormBpdu (tAstPerStPortInfo * pPerStPortInfo,
               UINT1 u1MsgType, tAstBufChainHeader * pBuf, UINT4 *pu4DataLength,
               UINT1 *pu1Flag)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT4               u4Length = AST_INIT_VAL;
    UINT2               u2ProtocolId = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1Val = (UINT1) AST_INIT_VAL;
    UINT1               u1Version = (UINT1) AST_INIT_VAL;
    UINT1               u1Version1Length = (UINT1) AST_INIT_VAL;
    UINT1               u1BpduType = (UINT1) AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                pPerStPortInfo->u2Inst);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    AST_MEMSET (gAstGlobalInfo.gpu1AstBpdu, AST_MEMSET_VAL,
                PVRST_MAX_BPDU_SIZE);
    pu1BpduStart = pu1BpduEnd = gAstGlobalInfo.gpu1AstBpdu;

    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    u2ProtocolId = (UINT2) AST_PROTOCOL_ID;

    switch (u1MsgType)
    {
        case AST_BPDU_TYPE_TCN:

            u1Version = (UINT1) AST_VERSION_0;
            u1BpduType = (UINT1) AST_BPDU_TYPE_TCN;

            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Transmitting TCN BPDU\n");
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        case AST_BPDU_TYPE_CONFIG:

            u1Version = (UINT1) AST_VERSION_0;
            u1BpduType = (UINT1) AST_BPDU_TYPE_CONFIG;

            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Transmitting CONFIG BPDU\n");
            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            if (pPerStRstPortInfo->pTcWhileTmr != NULL)
            {
                u1Val = (UINT1) (u1Val | PVRST_SET_TOPOCH_FLAG);
            }

            if (pPerStPvrstRstPortInfo->bTcAck == PVRST_TRUE)
            {
                u1Val = (UINT1) (u1Val | PVRST_SET_CONFIG_TOPOCH_ACK_FLAG);
            }

            AST_PUT_1BYTE (pu1BpduEnd, u1Val);

            PvrstFillBpdu (pPerStPortInfo, &pu1BpduEnd);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        case AST_BPDU_TYPE_RST:

            u1Version = (UINT1) AST_VERSION_2;
            u1BpduType = (UINT1) AST_BPDU_TYPE_RST;

            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Transmitting RST BPDU\n");
            AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
            AST_PUT_1BYTE (pu1BpduEnd, u1Version);
            AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

            u1Val = (UINT1) (u1Val | PVRST_SET_RST_TOPOCH_ACK_FLAG);
            {
                if (pPerStRstPortInfo->bSynced == PVRST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_AGREEMENT_FLAG);
                    *pu1Flag = (UINT1) PVRST_SET_AGREEMENT_FLAG;
                }
            }
#ifdef NPAPI_WANTED
            if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
            {
                /* Inferior info should have Desig role, Learning flag
                 * set to make peer port move to discarding state */
                u1Val = (UINT1) (u1Val | PVRST_SET_FORWARDING_FLAG);
                u1Val = (UINT1) (u1Val | PVRST_SET_LEARNING_FLAG);
                u1Val = (UINT1) (u1Val | PVRST_SET_DESG_PROLE_FLAG);
            }
            else
#endif
            {
                if (pPerStRstPortInfo->bForwarding == PVRST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_FORWARDING_FLAG);
                }
                if (pPerStRstPortInfo->bLearning == PVRST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_LEARNING_FLAG);
                }

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DESIGNATED)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_DESG_PROLE_FLAG);
                }
                else if (pPerStPortInfo->u1PortRole ==
                         (UINT1) AST_PORT_ROLE_ROOT)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_ROOT_PROLE_FLAG);
                }
                else if ((pPerStPortInfo->u1PortRole ==
                          (UINT1) AST_PORT_ROLE_ALTERNATE) ||
                         (pPerStPortInfo->u1PortRole ==
                          (UINT1) AST_PORT_ROLE_BACKUP))
                {
                    u1Val = (UINT1) (u1Val | RST_SET_ALTBACK_PROLE_FLAG);
                }

                else
                {
                    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                             "TXSM: Impossible Port Role while Transmitting BPDU\n");
                    return PVRST_FAILURE;
                }

                if (pPerStRstPortInfo->bProposing == PVRST_TRUE)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_PROPOSAL_FLAG);
                    *pu1Flag = (UINT1) PVRST_SET_PROPOSAL_FLAG;
                }

                if (pPerStRstPortInfo->pTcWhileTmr != NULL)
                {
                    u1Val = (UINT1) (u1Val | PVRST_SET_TOPOCH_FLAG);
                }

                AST_PUT_1BYTE (pu1BpduEnd, u1Val);

            }
            PvrstFillBpdu (pPerStPortInfo, &pu1BpduEnd);

            u1Version1Length = (UINT1) AST_VERSION_1_LENGTH;
            AST_PUT_1BYTE (pu1BpduEnd, u1Version1Length);

            *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

            break;

        default:

            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Invalid Type of BPDU to be sent\n");
            return PVRST_FAILURE;
    }

    u4Length = *pu4DataLength;
    if (AST_COPY_OVER_CRU_BUF (pBuf, gAstGlobalInfo.gpu1AstBpdu,
                               AST_ENET_LLC_HEADER_SIZE,
                               u4Length) == AST_CRU_FAILURE)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstFillBpdu                                        */
/*                                                                           */
/* Description        : This function is called by PvrstFormBpdu function in */
/*                      order to fill in the contents of the BPDU in the CRU */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                      *pu1Bpdu - Pointer to the linear attay in which the  */
/*                                 contents of the BPDU are to be filled     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
PvrstFillBpdu (tAstPerStPortInfo * pPerStPortInfo, UINT1 **ppu1Bpdu)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT4               u4Val = AST_INIT_VAL;
    UINT2               u2PortId = AST_INIT_VAL;
    UINT2               u2Val = AST_INIT_VAL;
    UINT1               u1Val = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT1              *pu1TmpBpdu = NULL;
    tPerStPvrstBridgeInfo *pBrgInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pBrgInfo = PVRST_GET_PERST_PVRST_BRG_INFO (u2InstIndex);
    pAstPerStBridgeInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    pu1TmpBpdu = *ppu1Bpdu;

#ifdef NPAPI_WANTED
    if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
    {
        /* Transmitting the Self Information  on all ports */
        u2Val = pPerStPortInfo->DesgBrgId.u2BrgPriority;
        AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

        AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->DesgBrgId.BridgeAddr);

        u4Val = AST_INIT_VAL;    /* Root cost set to 0 */
        AST_PUT_4BYTE (pu1TmpBpdu, u4Val);

        u2Val = pPerStPortInfo->DesgBrgId.u2BrgPriority;
        AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

        AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->DesgBrgId.BridgeAddr);

        u2PortId = u2PortId | pPerStPortInfo->u1PortPriority;
        u2PortId = (UINT2) (u2PortId << 8);
        u2PortId = u2PortId | AST_GET_PROTOCOL_PORT (pPerStPortInfo->u2PortNo);

        AST_PUT_2BYTE (pu1TmpBpdu, u2PortId);

        /* Total 2 Bytes of the timer were splitted into Seconds 
         * and Centi-Seconds and filled in the packet as follows.
         */
        /*****************************************
         |Seconds (8 bits)|Centi-Seconds (8 bits)|
         *****************************************/
        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u2Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.
                                        u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u2Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val =
            (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);
        *ppu1Bpdu = pu1TmpBpdu;
    }
    else
#endif
    {
        UNUSED_PARAM (pBrgInfo);
        u2Val = pPerStPortInfo->RootId.u2BrgPriority;
        AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

        AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->RootId.BridgeAddr);

        u4Val = pPerStPortInfo->u4RootCost;
        AST_PUT_4BYTE (pu1TmpBpdu, u4Val);
        if (pPerStRstPortInfo->bSynced == PVRST_TRUE)
        {
            u2Val = pPerStPortInfo->DesgBrgId.u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);
            AST_PUT_MAC_ADDR (pu1TmpBpdu, pPerStPortInfo->DesgBrgId.BridgeAddr);
        }
        else
        {
            u2Val = pAstPerStBridgeInfo->u2BrgPriority;
            AST_PUT_2BYTE (pu1TmpBpdu, u2Val);

            AST_PUT_MAC_ADDR (pu1TmpBpdu, pAstPerStBridgeInfo->
                              pPerStPvrstBridgeInfo->bridgeId.BridgeAddr);
        }
        u2PortId = pPerStPortInfo->u2DesgPortId;

        AST_PUT_2BYTE (pu1TmpBpdu, u2PortId);

        /* Total 2 Bytes of the timer were splitted into Seconds 
         * and Centi-Seconds and filled in the packet as follows.
         */
        /*****************************************
         |Seconds (8 bits)|Centi-Seconds (8 bits)|
         *****************************************/

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPerStPvrstRstPortInfo->
                                              PortPvrstTimes.
                                              u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC (pPerStPvrstRstPortInfo->
                                                    PortPvrstTimes.
                                                    u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPerStPvrstRstPortInfo->
                                              PortPvrstTimes.u2MaxAge);

        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC (pPerStPvrstRstPortInfo->
                                                    PortPvrstTimes.u2MaxAge);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPerStPvrstRstPortInfo->
                                              PortPvrstTimes.u2HelloTime);

        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC (pPerStPvrstRstPortInfo->
                                                    PortPvrstTimes.u2HelloTime);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPerStPvrstRstPortInfo->
                                              PortPvrstTimes.u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC (pPerStPvrstRstPortInfo->
                                                    PortPvrstTimes.
                                                    u2ForwardDelay);
        AST_PUT_1BYTE (pu1TmpBpdu, u1Val);
        *ppu1Bpdu = pu1TmpBpdu;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PvrstPortTxSmEventImpossible                         */
/*                                                                           */
/* Description        : This function is called on the occurence of an event */
/*                      that is not possible in the present state of the Port*/
/*                      Transmit State Machine.                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/

INT4
PvrstPortTxSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo)
{

    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "TXSM-PVRST: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
             "TXSM-PVRST: This is an IMPOSSIBLE EVENT in this State!\n");

    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR-PVRST: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstInitPortTxStateMachine                          */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortTxMachine                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
PvrstInitPortTxStateMachine (VOID)
{

/*#if defined(TRACE_WANTED) || defined(RST_DEBUG)*/
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[PVRST_PTXSM_MAX_EVENTS][20] = {
        "BEGIN", "NEWINFO_SET", "HELLOWHEN_EXP", "HOLDTMR_EXP", "TX_ENABLED",
        "TX_DISABLED"
    };
    UINT1               aau1SemState[PVRST_PTXSM_MAX_STATES][20] = {
        "TRANSMIT_INIT", "TRANSMIT_PERIODIC",
        "TRANSMIT_CONFIG", "TRANSMIT_TCN",
        "TRANSMIT_RSTP", "IDLE"
    };

    for (i4Index = 0; i4Index < PVRST_PTXSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_TXSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_TXSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_PTXSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_TXSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_TXSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /*#endif *//* PVRST_TRACE_WANTED || PVRST_DEBUG */
    /* Port Transmit SEM */
    /* BEGIN Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction =
        PvrstPortTxSmMakeTransmitInit;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].
        pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].
        pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_BEGIN]
        [PVRST_PTXSM_STATE_IDLE].pAction = PvrstPortTxSmEventImpossible;

    /* NEWINFO_SET Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_NEWINFO_SET]
        [PVRST_PTXSM_STATE_IDLE].pAction = PvrstPortTxSmNewInfoSetIdle;

    /* HELLOWHEN_EXP Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HELLOWHEN_EXP]
        [PVRST_PTXSM_STATE_IDLE].pAction = PvrstPortTxSmMakeTransmitPeriodic;

    /* HOLDTMR_EXP Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_HOLDTMR_EXP]
        [PVRST_PTXSM_STATE_IDLE].pAction = PvrstPortTxSmHoldTmrExpIdle;

    /* PORT_ENABLED Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction = PvrstPortTxSmCheckIdle;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_ENABLED][PVRST_PTXSM_STATE_IDLE].
        pAction = PvrstPortTxSmEventImpossible;

    /* PORTISABLED Event */
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_INIT].pAction = NULL;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_PERIODIC].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_CONFIG].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_TCN].pAction = PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED]
        [PVRST_PTXSM_STATE_TRANSMIT_RSTP].pAction =
        PvrstPortTxSmEventImpossible;
    PVRST_PORT_TX_MACHINE[PVRST_PTXSM_EV_TX_DISABLED][PVRST_PTXSM_STATE_IDLE].
        pAction = PvrstPortTxSmMakeTransmitInit;

    AST_DBG (AST_TXSM_DBG, "TXSM: Loaded TXSM SEM successfully\n");

    return;
}

/*****************************************************************************/
/* Function Name      : PvrstFillEnetHeader                                  */
/*                                                                           */
/* Description        : Forms the ethernet header and fills it in the packet */
/*                      CRU buffer passed in.                                */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the packet CRU buffer .            */
/*                      pPerStPortInfo - The pointer to the instance-         */
/*                                specific Port information (Instance 0 for  */
/*                                RSTP)                                         */
/*                      pu4DataLength - Length of the packet before adding   */
/*                                      the ethernet header                  */
/*                                                                           */
/* Output(s)          : pu4DataLength - Length of the packet after  adding   */
/*                                      the ethernet header                  */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS (or) PVRST_FAILURE                     */
/*****************************************************************************/
INT4
PvrstFillEnetHeader (tAstBufChainHeader * pBpdu, UINT4 *pu4DataLength,
                     tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT1               au1Llc[AST_LLC_HEADER_SIZE] = { 0x42, 0x42, 0x03 };
    UINT1               au1EnetLLCHeader[AST_ENET_LLC_HEADER_SIZE];
    INT4                i4Index = 0;
    UINT2               u2TypeLen;
    UINT4               u4DataLength = *pu4DataLength;

    AST_MEMSET (au1EnetLLCHeader, AST_MEMSET_VAL, sizeof (au1EnetLLCHeader));

    /* Filling in the Bridge Group Address */
    au1EnetLLCHeader[0] = (UINT1) 0x01;
    au1EnetLLCHeader[1] = (UINT1) 0x80;
    au1EnetLLCHeader[2] = (UINT1) 0xC2;
    au1EnetLLCHeader[3] = (UINT1) 0x00;
    au1EnetLLCHeader[4] = (UINT1) 0x00;
    au1EnetLLCHeader[5] = (UINT1) 0x00;
    i4Index = 6;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    MEMCPY (&au1EnetLLCHeader[i4Index], pPortInfo->au1PortMACAddr,
            AST_MAC_ADDR_SIZE);

    i4Index += AST_MAC_ADDR_SIZE;

    u2TypeLen = (UINT2) (u4DataLength + AST_LLC_HEADER_SIZE);
    u2TypeLen = (UINT2) (OSIX_HTONS (u2TypeLen));
    MEMCPY (&au1EnetLLCHeader[i4Index], &u2TypeLen, AST_TYPE_LEN_SIZE);
    i4Index += AST_TYPE_LEN_SIZE;

    MEMCPY (&au1EnetLLCHeader[i4Index], au1Llc, AST_LLC_HEADER_SIZE);
    i4Index += AST_LLC_HEADER_SIZE;

    *pu4DataLength = u4DataLength + AST_ENET_LLC_HEADER_SIZE;

    if (AST_COPY_OVER_CRU_BUF (pBpdu, au1EnetLLCHeader, AST_OFFSET,
                               AST_ENET_LLC_HEADER_SIZE) == AST_CRU_FAILURE)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstEncapsulateBpdu                                 */
/*                                                                           */
/* Description        : It encapsulates Pvrst Bpdu.                          */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Instance Identifier                    */
/*                      pBuf - Pointer to the packet CRU buffer .            */
/*                      pu4DataLength - Length of the packet before adding   */
/*                                      the ethernet header                  */
/*                      u2PortNum - Port Number                              */
/* Output(s)          : pu4DataLength - Length of the packet after  adding   */
/*                                      the ethernet header                  */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS (or) PVRST_FAILURE                     */
/*****************************************************************************/
INT4
PvrstEncapsulateBpdu (tVlanId VlanId, tAstBufChainHeader * pBuf,
                      UINT4 *pu4DataLength, UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tVlanId             PVID = RST_DEFAULT_INSTANCE;
    UINT1               u1PortType = AST_INIT_VAL;
    UINT1               au1Dot1qAddress[AST_MAC_ADDR_SIZE] = { 0x01, 0x00, 0x0C,
        0xCC, 0xCC, 0xCD
    };
    UINT1               au1CiscoDot1qData[AST_MAC_ADDR_SIZE] =
        { 0x00, 0x00, 0x00, 0x02, 0x00, 0x00 };

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    AstL2IwfGetVlanPortType ((UINT2) pPortInfo->u4IfIndex, &u1PortType);

    /* If port is access port, no need to send PVRST packet */
    if (u1PortType == VLAN_ACCESS_PORT)
    {
        return PVRST_SUCCESS;
    }

    if ((AST_GET_COMM_PORT_INFO (u2PortNum))->eEncapType == PVRST_DOT1Q)
    {

        AstL2IwfGetVlanPortPvid ((UINT2) pPortInfo->u4IfIndex, &PVID);

        AST_COPY_OVER_CRU_BUF (pBuf, au1Dot1qAddress,
                               AST_OFFSET, AST_MAC_ADDR_SIZE);
        if (PVID != VlanId)
        {
            if ((u1PortType != VLAN_HOST_PORT)
                && (u1PortType != VLAN_ACCESS_PORT))
            {
                PvrstVlanTagFrame (pBuf, VlanId, VLAN_HIGHEST_PRIORITY);

                *pu4DataLength +=
                    DOT1Q_TAG_LEN + ORG_CODE_SIZE + PROTOCOL_ID_LEN;
                /* 4 bytes for Tag
                 * 3 bytes for Organization Code
                 * 2 bytes for Protocol Id */

                /* To make it Cisco Compatible in Dot1q mode
                 * extra 6 bytes are filled as per cisco bpdus observations
                 * First 4 bytes are kept as 00000002 and last 2 represents
                 * Vlan Id */

                VlanId = OSIX_HTONS (VlanId);
                MEMCPY (&au1CiscoDot1qData[4], &VlanId, VLAN_TAG_SIZE);

                AST_COPY_OVER_CRU_BUF (pBuf, au1CiscoDot1qData,
                                       DOT1Q_CISCO_DATA_OFFSET,
                                       AST_MAC_ADDR_SIZE);

                *pu4DataLength += 6;    /* To make it Cisco Compatible */
            }
        }
        return PVRST_SUCCESS;
    }
    else if ((AST_GET_COMM_PORT_INFO (u2PortNum))->eEncapType == PVRST_ISL)
    {
        PvrstTagISLHeader (pBuf, VlanId, u2PortNum);
        *pu4DataLength += ISL_HEADER_SIZE + TRAILER_LEN + CRC_SIZE;
        /* 26 bytes of ISL Header
         *  7 bytes of Trailer
         *  4 bytes of CRC */
        return PVRST_SUCCESS;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PvrstVlanTagFrame                                */
/*                                                                           */
/*    Description         : This function takes care of inserting the VLAN   */
/*                          tag to the given frame.                          */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstVlanTagFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId,
                   UINT1 u1Priority)
{
    UINT1               au1Buf[DOT1Q_DATA_HDR_SIZE];
    /* It prepares first 26 bytes of pFrame, 
     * which is followed by Data (Bpdu).
     * 17 bytes of Enet Header
     * 4 bytes of 802.1q Tag
     * 3 bytes of Organization Code
     * 2 bytes of Protocol Id */
    UINT1               au1TmpBuf[DOT1Q_DATA_LEN];    /* It saves 42 bytes of Data 
                                                     * 36 bytes of Rst Bpdu &
                                                     * 6 bytes are used to make  
                                                     * it Cisco compatible */
    UINT2               u2Tag = AST_INIT_VAL;
    UINT2               u2Protocol;
    UINT2               u2Length = AST_LLC_HEADER_SIZE + ORG_CODE_SIZE +
        PROTOCOL_ID_LEN + DOT1Q_DATA_LEN;
    /* 3 bytes of LLC Header + 
     * 3 bytes of Organization Code
     * 2 bytes of Protocol Id
     * 42 bytes of Data(Bpdu)- Cisco compatible */

    /* Copy RST Bpdu from pFrame to au1TmpBuf */
    CRU_BUF_Copy_FromBufChain (pFrame, au1TmpBuf, AST_ENET_LLC_HEADER_SIZE,
                               DOT1Q_DATA_LEN);
    u2Protocol = OSIX_HTONS (VLAN_PROTOCOL_ID);

    /* copying source and dest mac address */
    CRU_BUF_Copy_FromBufChain (pFrame, au1Buf, AST_OFFSET,
                               AST_ENET_DEST_SRC_ADDR_SIZE);

    u2Tag = (UINT2) u1Priority;

    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);

    u2Tag = (UINT2) (u2Tag | (VlanId & VLAN_ID_MASK));

    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));

    MEMCPY (&au1Buf[12], (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);

    MEMCPY (&au1Buf[14], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);

    u2Length = OSIX_HTONS (u2Length);
    MEMCPY (&au1Buf[16], &u2Length, AST_TYPE_LEN_SIZE);

    au1Buf[18] = 0xaa;            /* DSAP */
    au1Buf[19] = 0xaa;            /* SSAP */
    au1Buf[20] = 0x03;            /* Control Field */
    au1Buf[21] = 0x00;            /* Organization Code: Cisco (0x00000c) */
    au1Buf[22] = 0x00;
    au1Buf[23] = 0x0c;
    au1Buf[24] = 0x01;            /* Protocol ID: 0x010b */
    au1Buf[25] = 0x0b;

    /* Copy au1Buf contents to pFrame after inserting Vlan Tag */
    AST_COPY_OVER_CRU_BUF (pFrame, au1Buf, AST_OFFSET, DOT1Q_DATA_HDR_SIZE);

    /* Concatenate Data (RST bpdu) contents to pFrame  */
    AST_COPY_OVER_CRU_BUF (pFrame, au1TmpBuf, DOT1Q_DATA_HDR_SIZE,
                           DOT1Q_DATA_LEN);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PvrstTagISLHeader                                */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u2PortNum - Port Num                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstTagISLHeader (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId,
                   UINT2 u2PortNum)
{
    tAstCfaIfInfo       CfaIfInfo;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Length = 76;    /*  3 bytes (DSAP, SSAP, CR bit)
                                         *  3 bytes of HSA 
                                         *  2 bytes of VLAN ID
                                         *  2 bytes of Index
                                         *  2 bytes of Reserved field
                                         *  4 bytes of CRC
                                         * 60 bytes of Non-Encapsulated Pkt */

    UINT1               au1Buf[NON_ENCAP_PKT_LEN + PVRST_ENCAP_OVERHEADS];

    /* {0x03, 0x00, 0x0c, 0x00, 0x00} is the other ISL address.
     * While sending ISL Encapsulated packet only 1 address is being
     * used now */
    UINT1               au1ISLDestAddress[5] = { 0x01, 0x00, 0x0c, 0x00, 0x00 };

    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tAstCfaIfInfo));
    MEMCPY (au1Buf, au1ISLDestAddress, PVRST_ISL_ADDRESS_LENGTH);

    au1Buf[5] = 0;                /*Type Ethernet(0000), User(Ethernet Priority)- (0000) */

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (CfaGetIfInfo (pPortInfo->u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: CfaGetIfInfo call FAILED!\n");
        return;
    }

    MEMCPY (au1Buf + AST_MAC_ADDR_SIZE, CfaIfInfo.au1MacAddr,
            AST_MAC_ADDR_SIZE);

    u2Length = OSIX_HTONS (u2Length);
    MEMCPY (&au1Buf[12], &u2Length, AST_TYPE_LEN_SIZE);

    /*Add Standard SNAP 802.2 LLC header */
    au1Buf[14] = 0xAA;
    au1Buf[15] = 0xAA;
    au1Buf[16] = 0x03;

    MEMCPY (au1Buf + PVRST_INCR_LENGTH_SEVENTEEN,
            au1Buf + PVRST_INCR_LENGTH_SIX, PVRST_LENGTH_LLC_HEADER);

    /*Add 15-bit VLAN ID, where Only the lower 10 bits are used for 1024 VLANs */
    VlanId = (UINT2) (VlanId << 1);
    VlanId = (tVlanId) (VlanId | 0x0001);
    VlanId = OSIX_HTONS (VlanId);
    MEMCPY (&au1Buf[20], &VlanId, VLAN_TAG_SIZE);

    au1Buf[22] = 0x0;
    au1Buf[23] = 0x0;
    au1Buf[24] = 0x0;
    au1Buf[25] = 0x0;
    CRU_BUF_Prepend_BufChain (pFrame, au1Buf, ISL_HEADER_SIZE);

    /* Form Trailer */
    au1Buf[0] = 0x0;
    au1Buf[1] = 0x0;
    au1Buf[2] = 0x0;
    au1Buf[3] = 0x0;
    au1Buf[4] = 0x0;
    au1Buf[5] = 0x0;
    au1Buf[6] = 0x0;

    AST_COPY_OVER_CRU_BUF (pFrame, au1Buf,
                           ISL_HEADER_SIZE + AST_BPDU_LENGTH_RST +
                           AST_ENET_LLC_HEADER_SIZE, TRAILER_LEN);

    CRU_BUF_Copy_FromBufChain (pFrame, au1Buf, ISL_HEADER_SIZE,
                               NON_ENCAP_PKT_LEN);

    /* Calculate CRC of Non Encapsulated packet */
    crc32_isl_calc (au1Buf, NON_ENCAP_PKT_LEN, au1Buf + NON_ENCAP_PKT_LEN);

    AST_COPY_OVER_CRU_BUF (pFrame, au1Buf, ISL_HEADER_SIZE,
                           NON_ENCAP_PKT_LEN + CRC_SIZE);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PvrstPortTxSmCheckIdle                           */
/*                                                                           */
/*    Description         : This function takes care of checking conditions  */
/*                          to  move port to idle state                      */
/*                                                                           */
/*    Input(s)            : pPerStPortInfo-instance-port info              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PvrstPortTxSmCheckIdle (tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = RST_DEFAULT_INSTANCE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    VlanId = pPerStPortInfo->u2Inst;
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (AST_IS_PVRST_ENABLED ())
    {
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_FALSE;
    }

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) ||
        (pAstPortEntry->u1EntryStatus == CFA_IF_DOWN) ||
        (pRstPortInfo->bPortEnabled == RST_FALSE) ||
        (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_FALSE))
    {
        return RST_SUCCESS;
    }

    if (PvrstPortTxSmMakeIdle (pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: PvrstPortTxSmMakeIdle function " "returned FAILURE!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/* End of File */
