/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvutil.c,v 1.17 2017/11/30 06:29:20 siva Exp $
 *
 * Description: This file contains utility routines.
 *
 *****************************************************************************/

#ifdef PVRST_WANTED

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : PvrstGetFreeInstIndex                                */
/*                                                                           */
/* Description        : This returns the free Index                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstGetFreeInstIndex (UINT2 *u2FreeIndex)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        if (PVRST_VLAN_TO_INDEX_MAP (u2InstIndex) != AST_INIT_VAL)
        {
            continue;
        }
        else
        {
            *u2FreeIndex = u2InstIndex;
            return PVRST_SUCCESS;
        }
    }
    *u2FreeIndex = INVALID_INDEX;
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstInitInstIndexMap                                */
/*                                                                           */
/* Description        : This allocates meemory to the Instance to Vlan       */
/*                      mapping pointer                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstInitInstIndexMap (VOID)
{
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2              *pu2IndexMap = NULL;

    if (AST_ALLOC_PVRST_VLAN_TO_INDEX_MAP_BLOCK (pu2IndexMap) == NULL)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for PVRST VLAN to Index Mapping "
                 "failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for PVRST VLAN to Index Mapping "
                 "failed !!!\n");
        return PVRST_FAILURE;
    }

    PVRST_VLAN_TO_INDEX_MAP_PTR = pu2IndexMap;

    /* Initialize Instance to Index Mapping array with 0 InstanceId */
    for (u2InstIndex = 1; u2InstIndex <= AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        PVRST_VLAN_TO_INDEX_MAP (u2InstIndex) = AST_INIT_VAL;
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstDeInitInstIndexMap                              */
/*                                                                           */
/* Description        : This releases meemory to the Instance to Vlan        */
/*                      mapping pointer                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstDeInitInstIndexMap (VOID)
{
    if (PVRST_VLAN_TO_INDEX_MAP_PTR != NULL)
    {
        AST_RELEASE_PVRST_VLAN_TO_INDEX_MAP_BLOCK (PVRST_VLAN_TO_INDEX_MAP_PTR);

        PVRST_VLAN_TO_INDEX_MAP_PTR = NULL;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstPathcostConfiguredFlag                          */
/*                                                                           */
/* Description        : Indicates whether the pathcost has been configured   */
/*                      on this port for this instance using AdminPathCost   */
/*                                                                           */
/* Input(s)           : u2LocalPortId - Local Context based port number      */
/*                      u2InstIndex  - Index of Spanning Tree                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_TRUE/PVRST_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstPathcostConfiguredFlag (UINT2 u2LocalPortId, UINT2 u2InstIndex)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (PVRST_GET_PERST_INFO (u2InstIndex) == NULL)
    {
        return PVRST_FALSE;
    }
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        return PVRST_FALSE;
    }

    if (pPerStPortInfo->u4PortAdminPathCost == 0)
    {
        return PVRST_FALSE;
    }

    return PVRST_TRUE;
}

/*****************************************************************************/
/* Function Name      : PvrstGetNextIndexPvrstInstBridgeTable                */
/*                                                                           */
/* Description        : Gets the Next Instance Index for the given Instance  */
/*                                                                           */
/* Input(s)           : i4FsPvrstInstVlanId - Vlan Instance Index            */
/*                                                                           */
/* Output(s)          : pi4NextFsPvrstInstVlanId- Pointer to next instance   */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS or PVRST_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT1
PvrstGetNextIndexPvrstInstBridgeTable (INT4 i4FsPvrstInstVlanId,
                                       INT4 *pi4NextFsPvrstInstVlanId)
{
    tAstPerStInfo      *pAstPerStInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        return PVRST_FAILURE;
    }
    if (i4FsPvrstInstVlanId != 0)
    {
        u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    (tVlanId)
                                                    i4FsPvrstInstVlanId);
        if (INVALID_INDEX == u2InstIndex)
        {
            return PVRST_FAILURE;
        }
    }
    for (u2InstIndex = (UINT2) (u2InstIndex + 1);
         u2InstIndex <= (UINT2) AST_MAX_PVRST_INSTANCES; u2InstIndex++)
    {
        pAstPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);

        if (pAstPerStInfo == NULL)
        {
            continue;
        }

        *pi4NextFsPvrstInstVlanId = pAstPerStInfo->u2InstanceId;
        return PVRST_SUCCESS;
    }
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstGetNextIndexFsPvrstInstPort                     */
/*                                                                           */
/* Description        : Gets the Next Port Index for the given Instance      */
/*                                                                           */
/* Input(s)           : i4FsPvrstInstVlanId - Vlan Instance Index            */
/*                    : u2PortIndex - Port Index                             */
/*                                                                           */
/* Output(s)          : pi4NextPortIndex- Pointer to next Port Index         */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS or PVRST_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT1
PvrstGetNextIndexFsPvrstInstPort (INT4 i4FsPvrstInstVlanId,
                                  UINT2 u2PortIndex, INT4 *pi4NextPortIndex)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* Get the VLAN type */
    L2PvlanMappingInfo.u4ContextId = AST_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
    L2PvlanMappingInfo.InVlanId = (tVlanId) i4FsPvrstInstVlanId;
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }
    for (u2PortNum = (UINT2) (u2PortIndex + 1);
         u2PortNum <= (UINT2) AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        if (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN ||
            L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN)
        {
            if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                         (tVlanId) i4FsPvrstInstVlanId,
                                         AST_GET_IFINDEX (u2PortIndex))
                == OSIX_FALSE)
            {
                continue;
            }
        }

        *pi4NextPortIndex = (UINT4) u2PortNum;
        return PVRST_SUCCESS;
    }
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstGetNextIndexFsMIPvrstInstPort                   */
/*                                                                           */
/* Description        : Gets the Next Port Index for the given Instance      */
/*                                                                           */
/* Input(s)           : i4FsPvrstInstVlanId - Vlan Instance Index            */
/*                    : uuIfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : pi4NextPortIndex- Pointer to next Interface Index    */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS or PVRST_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT1
PvrstGetNextIndexFsMIPvrstInstPort (INT4 i4FsPvrstInstVlanId,
                                    UINT2 u2PortIndex, INT4 *pi4NextPortIndex)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4FsPvrstInstVlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }

    if (u2PortIndex != 0)
    {
        if (AstGetContextInfoFromIfIndex ((INT4) u2PortIndex,
                                          &(AST_CURR_CONTEXT_ID ()),
                                          &u2LocalPortId) != RST_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    for (u2PortNum = (UINT2) (u2LocalPortId + 1);
         u2PortNum <= (UINT2) AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPortEntry = AST_GET_PORTENTRY (u2PortNum);

        *pi4NextPortIndex = (INT4) (pPortEntry->u4IfIndex);
        return PVRST_SUCCESS;
    }
    return PVRST_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PvrstValidateInstanceEntry                           */
/*                                                                           */
/* Description        : The Routines Validates the Given Instance.           */
/*                                                                           */
/* Input(s)           : i4InstanceId - Instance Index                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS or PVRST_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstValidateInstanceEntry (INT4 i4InstanceId)
{
    tAstPerStInfo      *pAstPerStInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    if (AstSelectContext (AST_DEFAULT_CONTEXT) == RST_FAILURE)
    {
        return PVRST_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);

    if (pAstPerStInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    else
    {
        return PVRST_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstValidatePortEntry                               */
/*                                                                           */
/* Description        : The Routines Validates the Given PortId for given    */
/*                      InstanceId.                                          */
/*                                                                           */
/* Input(s)           : i4PortId - Port Number                               */
/*                      i4InstanceId - Instance Index                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS or PVRST_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstValidatePortEntry (INT4 i4PortId, INT4 i4InstanceId)
{
    tAstPerStInfo      *pAstPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2InstIndex = AST_INIT_VAL;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* Get the VLAN type */
    L2PvlanMappingInfo.u4ContextId = AST_CURR_CONTEXT_ID ();
    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
    L2PvlanMappingInfo.InVlanId = (tVlanId) i4InstanceId;
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if ((i4PortId < AST_MIN_NUM_PORTS) || (i4PortId > AST_MAX_NUM_PORTS))
    {
        return PVRST_FAILURE;
    }
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) i4InstanceId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstPerStInfo = PVRST_GET_PERST_INFO (u2InstIndex);
    if (pAstPerStInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN ||
        L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN)
    {
        if (L2IwfMiIsVlanMemberPort (AST_CURR_CONTEXT_ID (),
                                     (tVlanId) i4InstanceId,
                                     AST_GET_IFINDEX (i4PortId)) == OSIX_FALSE)
        {
            return PVRST_FAILURE;
        }
    }

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO ((UINT2) i4PortId, u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    else
    {
        return PVRST_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstReleasePortMemBlocks                            */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                       PortEntry , PB Port Information and PerSt Port Info */
/*                                                                           */
/* Input(s)           : pPortInfo - Port Entry whose memory block should be  */
/*                      released                                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstReleasePortMemBlocks (tAstPortEntry * pPortInfo)
{

    /* Remove this node from the IfIndex RBTree and the PerContext DLL */
    /* For CVLAN component Ports, RBTree Remove will give errors */
    AstRemoveFromIfIndexTable (pPortInfo);

    if (AST_PB_PORT_INFO (pPortInfo) != NULL)
    {

        AST_PB_RELEASE_PB_PORT_INFO_MEM_BLOCK (AST_PB_PORT_INFO (pPortInfo));

        /*If this port is Part of CVLAN component, then remove the 
         *        * CVLAN Port 2 Index Mapping from the RBTree*/
        if (AST_COMP_TYPE () == AST_PB_C_VLAN)
        {
            /* If this port is Part of CVLAN component, then remove the 
             * CVLAN Port 2 Index Mapping from the RBTree*/
            AstPbCVlanRemovePort2IndexMapBasedOnProtPort (pPortInfo->
                                                          u2ProtocolPort);
        }

    }

    AST_PB_PORT_INFO (pPortInfo) = NULL;

    AST_GET_PORTENTRY (pPortInfo->u2PortNo) = NULL;

    /* Releasing tAstPortEntry Memory Block */
    (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstCreateSpanningTreeInst                          */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      PerStInfo and array for PerStPortInfo                */
/*                                                                           */
/* Input(s)           : u2InstIndex - Instance Index to be memory allocated  */
/*                                                                           */
/* Output(s)          : ppPerStInfo - Allocated Memory Block pointer for stInfo*/
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstCreateSpanningTreeInst (UINT2 u2InstIndex, tAstPerStInfo ** ppPerStInfo)
{

    /* Creating PerStInfo for Default instance */
    if ((AST_ALLOC_PERST_INFO_MEM_BLOCK (*ppPerStInfo)) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID, PVRST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for Spanning Tree Information "
                 "failed !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for Spanning Tree Information "
                 "failed !!!\n");
        return PVRST_FAILURE;
    }

    AST_MEMSET (*ppPerStInfo, AST_INIT_VAL, sizeof (tAstPerStInfo));

    PVRST_GET_PERST_INFO (u2InstIndex) = *ppPerStInfo;

    /* For CVLAN component, there is a separate mempool for the Perst Port table
     * and PerSt Info blocks. For SVLAN component, there is a separate mempool
     * for in the Global Mempools. To differentiate the allocation, the
     * component type maintained in the context is used.*/
    if (AST_COMP_TYPE () == AST_PB_C_VLAN)
    {
        if (AST_ALLOC_PERST_PORT_TBL_BLOCK
            (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID,
             PVRST_GET_PERST_INFO (u2InstIndex)->ppPerStPortInfo) == NULL)
        {
            AST_RELEASE_PERST_INFO_MEM_BLOCK (*ppPerStInfo);
            PVRST_GET_PERST_INFO (u2InstIndex) = NULL;

            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                            PVRST_BRG_TRAPS_OID_LEN);
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: C-VLAN component with CEP: %d : Memory Allocation "
                          "for Spanning Tree Information failed !!!\n",
                          (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);

            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: C-VLAN component with CEP: %d : Memory Allocation "
                          "for Spanning Tree Information failed !!!\n",
                          (AST_CURR_CONTEXT_INFO ())->u4CepIfIndex);

            return PVRST_FAILURE;
        }

        AST_MEMSET (AST_PERST_PORT_INFO_TBL (u2InstIndex), AST_INIT_VAL,
                    (AST_MAX_NUM_PORTS * sizeof (tAstPerStPortInfo *)));
    }
    else
    {
        if (AST_ALLOC_PERST_PORT_TBL_BLOCK (AST_PERST_PORT_TBL_MEMPOOL_ID,
                                            PVRST_GET_PERST_INFO
                                            (u2InstIndex)->
                                            ppPerStPortInfo) == NULL)
        {
            AST_RELEASE_PERST_INFO_MEM_BLOCK (*ppPerStInfo);
            PVRST_GET_PERST_INFO (u2InstIndex) = NULL;

            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_PVRST_TRAPS_OID,
                            PVRST_BRG_TRAPS_OID_LEN);

            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     "SYS: Memory Allocation for Spanning Tree Information "
                     "failed !!!\n");
            AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                     "SYS: Memory Allocation for Spanning Tree Information "
                     "failed !!!\n");

            return PVRST_FAILURE;
        }

        AST_MEMSET (AST_PERST_PORT_INFO_TBL (u2InstIndex), AST_INIT_VAL,
                    (AST_MAX_NUM_PORTS * sizeof (tAstPerStPortInfo *)));
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstDeleteMstInstance                               */
/*                                                                           */
/* Description        : This functions release the memory block for the      */
/*                      PerStInfo and array of PerStPortInfo                 */
/*                                                                           */
/* Input(s)           : u2InstIndex - Instance Index to be memory allocated  */
/*                                                                           */
/* Output(s)          : ppPerStInfo - Memory Block pointer to be released    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PvrstDeleteMstInstance (UINT2 u2InstIndex, tAstPerStInfo * pPerStInfo)
{
    if (pPerStInfo != NULL)
    {
        if (pPerStInfo->ppPerStPortInfo != NULL)
        {
            /* Release the PerSt Port Table from PerStInfo. */
            if (AST_COMP_TYPE () == AST_PB_C_VLAN)
            {
                AST_RELEASE_PERST_PORT_TBL_BLOCK
                    (AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID,
                     pPerStInfo->ppPerStPortInfo);
            }
            else
            {
                AST_RELEASE_PERST_PORT_TBL_BLOCK
                    (AST_PERST_PORT_TBL_MEMPOOL_ID,
                     pPerStInfo->ppPerStPortInfo);
            }

        }

        /* Release PerStInfo. */
        AST_RELEASE_PERST_INFO_MEM_BLOCK (pPerStInfo);
    }
    PVRST_GET_PERST_INFO (u2InstIndex) = NULL;

    return;

}

/*****************************************************************************/
/* Function Name      : PvrstGetInstPortRootInconsistentState                */
/*                                                                           */
/* Description        : This functions returns Instance based Root           */
/*                      Inconsistent state for the given port                */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                      VlanId - Vlan Id                                     */
/*                                                                           */
/* Output(s)          : pi4InstPortRootIncStatus - Instance based state      */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PvrstGetInstPortRootInconsistentState (UINT2 u2PortNum, UINT2 VlanId,
                                       INT4 *pi4InstPortRootIncStatus)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT2               u2InstIndex;

    if (AstGetContextInfoFromIfIndex (u2PortNum,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return PVRST_FAILURE;
    }

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                (tVlanId) VlanId);
    if (INVALID_INDEX == u2InstIndex)
    {
        return PVRST_FAILURE;
    }

    if (PVRST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2LocalPortId, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
    {
        *pi4InstPortRootIncStatus = PVRST_TRUE;
    }
    else
    {
        *pi4InstPortRootIncStatus = PVRST_FALSE;
    }

    return PVRST_SUCCESS;
}
extern UINT4        gu4Stups;
/****************************************************************************
*
*    FUNCTION NAME    : PvrstUtilTicksToDate
*
*    DESCRIPTION      : This function converts the Ticks to Date Format
*
*    INPUT            : u4Ticks
*
*    OUTPUT           : u1DateFormat- Formatted Date
*
*    RETURNS          : None
****************************************************************************/
PUBLIC VOID
PvrstUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks (u4Ticks, &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

#endif /* PVRST_WANTED */
