/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astvrtsm.c,v 1.32 2017/12/29 09:30:39 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Role Transition State Machine.
 *
 *****************************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : PvrstPortRoleTrMachine                               */
/*                                                                           */
/* Description        : This is the main routine for the Port Role Transition*/
/*                      State Machine.                                       */
/*                      This routine calls the action routines for the event-*/
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstPortRoleTrMachine (UINT2 u2Event, tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2State;
    INT4                i4RetVal = PVRST_SUCCESS;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Event %s: Invalid parameter passed to event "
                      "handler routine\n",
                      gaaau1AstSemEvent[AST_RTSM][u2Event]);
        return PVRST_FAILURE;
    }
    u2State = (UINT2) pPerStPortInfo->u1ProleTrSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %u: Role Tr Machine Called with Event: %s,"
                  "State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_RTSM][u2Event],
                  gaaau1AstSemState[AST_RTSM][u2State]);

    AST_DBG_ARG3 (AST_RTSM_DBG,
                  "RTSM: Port %u: Role Tr Machine Called with Event: %s, "
                  "State: %s\n",
                  pPerStPortInfo->u2PortNo,
                  gaaau1AstSemEvent[AST_RTSM][u2Event],
                  gaaau1AstSemState[AST_RTSM][u2State]);

    if (PVRST_PORT_ROLE_TR_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_RTSM_DBG, "RTSM: No Operations to Perform\n");
        return PVRST_SUCCESS;
    }

    i4RetVal = (*(PVRST_PORT_ROLE_TR_MACHINE[u2Event][u2State].pAction))
        (pPerStPortInfo);

    if (i4RetVal != PVRST_SUCCESS)
    {
        AST_DBG (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                 "RTSM: Event routine returned FAILURE !!!\n");
        return PVRST_FAILURE;
    }

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmUpdtInfoReset                          */
/*                                                                           */
/* Description        : This routine is called whenever the SELECTED variable*/
/*                      gets set and the Port role is Blocked, Root or       */
/*                      Designated.                                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmUpdtInfoReset (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstBoolean         bAllSynced = PVRST_FALSE;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_UPDATE_INFO_RESET,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pAstCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected, UpdtInfo is not Valid - "
                      "EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }
    if ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE) ||
        ((pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE) &&
         (AST_GBL_BPDUGUARD_STATUS == PVRST_TRUE)))
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RTSM: Port %u: Role Change Detected... taking"
                      "action ... \n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Role Change Detected... taking"
                      "action ... \n", pPerStPortInfo->u2PortNo);
        return (PvrstProleTrSmRoleChanged (pPerStPortInfo));
    }
    else
    {
        if (pPerStPortInfo->u1PortRole != pPerStPortInfo->u1SelectedPortRole)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "RTSM: Port %u: Role Change Detected... taking"
                          "action ... \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Role Change Detected... taking"
                          "action ... \n", pPerStPortInfo->u2PortNo);
            return (PvrstProleTrSmRoleChanged (pPerStPortInfo));
        }
    }
    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT)
    {
        if (pPerStRstPortInfo->bProposed == PVRST_TRUE)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "RTSM: Port %u: Proposed Set ... taking"
                          "action ...\n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Proposed Set ... taking"
                          "action ...\n", pPerStPortInfo->u2PortNo);
            if (PvrstProleTrSmProposedSetRootPort (pPerStPortInfo)
                != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: ProposedSetRootPort returned"
                              "FAILURE !!!\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
        else if (pPerStRstPortInfo->bSynced == PVRST_FALSE)
        {
            bAllSynced =
                PvrstIsAllOtherPortsSynced (pPerStPortInfo->u2PortNo,
                                            u2InstIndex);

            if (bAllSynced == PVRST_TRUE)
            {
                pPerStRstPortInfo->bProposed = PVRST_FALSE;
                pPerStRstPortInfo->bSync = PVRST_FALSE;
                pPerStRstPortInfo->bSynced = PVRST_TRUE;

                u2PortNum = pPerStPortInfo->u2PortNo;
                pPerStPvrstRstPortInfo =
                    PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum,
                                                         u2InstIndex);
                if (pPerStPvrstRstPortInfo == NULL)
                {
                    return PVRST_FAILURE;
                }

                pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Moved to state ROOT_AGREED \n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                              pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Transmit machine"
                                  "returneFAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Transmit machine"
                                  "returnedFAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }

    }

    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
    {
        if (PvrstProleTrSmMakeDesgPort (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: PvrstProleTrSmMakeDesgPort function"
                          "returned FAILURE !!!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: UpdateInfo reset event handled "
                  "successfully\n", pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRerootSetRootPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine when the reRoot variable is set in the */
/*                      RootPort state.                                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRerootSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    if ((PvrstHandleStpCompatabilityCh (PVRST_REROOT_SET_ROOTPORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not Valid"
                      " - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (pPerStRstPortInfo->bForward == PVRST_TRUE)
    {
        /* Move to REROOTED state */
        pPerStRstPortInfo->bReRoot = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state REROOTED \n",
                      pPerStPortInfo->u2PortNo);
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRerootSetDesgPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the reRoot variable is set  */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRerootSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_SYNC_SET_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);

    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is"
                      " not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPerStRstPortInfo->pRrWhileTmr != NULL)
    {
        if ((pPortInfo->bOperEdgePort == PVRST_FALSE) &&
            ((pPerStRstPortInfo->bLearn == PVRST_TRUE) ||
             (pPerStRstPortInfo->bForward == PVRST_TRUE)))
        {
            if (PvrstProleTrSmMakeDesignatedListen (pPerStPortInfo) !=
                PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: MakeDesignatedListen function"
                              " returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
            if ((pPerStRstPortInfo->bForward == PVRST_FALSE) &&
                (pPerStRstPortInfo->bAgreed == PVRST_FALSE) &&
                (pPerStRstPortInfo->bProposing == PVRST_FALSE) &&
                (pPortInfo->bOperEdgePort == PVRST_FALSE))
            {

                pPerStRstPortInfo->bProposing = PVRST_TRUE;

                if (PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PROPOSING_SET,
                                              pPerStPortInfo->u2PortNo) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_BDSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: BridgeDetection Machine "
                                  "returned failure!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }

                PvrstBdSmStartEdgeDelayWhile (pPerStPortInfo->u2PortNo,
                                              u2InstIndex);

                pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Moved to state "
                              "DESIGNATED_PROPOSE \n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstPortTransmitMachine
                    (PVRST_PTXSM_EV_NEWINFO_SET,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Transmit machine "
                                  "returned FAILURE !!! \n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            if (((((pPerStRstPortInfo->bLearning == PVRST_FALSE) &&
                   (pPerStRstPortInfo->bForwarding == PVRST_FALSE)) ||
                  (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
                  (pPortInfo->bOperEdgePort == PVRST_TRUE))
                 &&
                 (pPerStRstPortInfo->bSynced == PVRST_FALSE))
                ||
                ((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
                 (pPerStRstPortInfo->bSynced == PVRST_TRUE)))
            {

               /***************************************/
                /* Stop rrWhile Timer if it is running */
               /***************************************/
                if (pPerStRstPortInfo->pRrWhileTmr != NULL)
                {
                    if (PvrstStopTimer
                        ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %u: Stop Timer for "
                                      "RrWhileTmr Failed !!!\n",
                                      pPerStPortInfo->u2PortNo);
                        return PVRST_FAILURE;
                    }
                }

                pPerStRstPortInfo->bSynced = PVRST_TRUE;
                pPerStRstPortInfo->bSync = PVRST_FALSE;
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Moved to state "
                              "DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);

            }
        }
    }                            /* End of RrWhile Timer is running */

    if (pPerStRstPortInfo->pRrWhileTmr == NULL)
    {
        pPerStRstPortInfo->bReRoot = PVRST_FALSE;

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_RETIRED \n", pPerStPortInfo->u2PortNo);

        /* Check to transition to Learning and Forwarding */
        if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
             (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
             (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
            (pPerStRstPortInfo->bSync == PVRST_FALSE))
        {
            if (((pPerStRstPortInfo->bLearn == PVRST_FALSE)
                 || ((pPerStRstPortInfo->bLearn == PVRST_TRUE) &&
                     (pPerStRstPortInfo->bForward == PVRST_FALSE))) &&
                ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                 && (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                 (pCommPortInfo->pEdgeDelayWhileTmr == NULL)))

            {
                if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX (u2PortNum)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for vlan :%d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }

            }
            else if ((pPerStRstPortInfo->bLearn == PVRST_FALSE) &&
                     (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL) &&
                     (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE))
            {
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (u2PortNum)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s for vlan :%d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            else
            {
                if ((pPerStRstPortInfo->bLearn == PVRST_TRUE) &&
                    (pPerStRstPortInfo->bForward == PVRST_FALSE))
                {
                    if (PvrstProleTrSmMakeForward (pPerStPortInfo) !=
                        PVRST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %u: MakeForward "
                                      "function returned FAILURE\n",
                                      pPerStPortInfo->u2PortNo);
                        return PVRST_FAILURE;
                    }

                }
            }
        }
    }                            /* End of RrWhile Timer is not running */

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state DESIGNATED_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmSyncSetDesgPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Sync variable is set in */
/*                      the DesignatedPort state.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmSyncSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (((pPortInfo->bOperEdgePort == PVRST_FALSE) &&
         ((pPerStRstPortInfo->bLearn == PVRST_TRUE) ||
          (pPerStRstPortInfo->bForward == PVRST_TRUE)))
        &&
        ((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
         (pPerStRstPortInfo->bSynced == PVRST_FALSE)))
    {
        if (PvrstProleTrSmMakeDesignatedListen (pPerStPortInfo) !=
            PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: MakeDesignatedListen "
                          "returned failure\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    if ((pPerStRstPortInfo->bForward == PVRST_FALSE) &&
        (pPerStRstPortInfo->bAgreed == PVRST_FALSE) &&
        (pPerStRstPortInfo->bProposing == PVRST_FALSE) &&
        (pPortInfo->bOperEdgePort == PVRST_FALSE))
    {

        pPerStRstPortInfo->bProposing = PVRST_TRUE;

        if (PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PROPOSING_SET,
                                      pPerStPortInfo->u2PortNo) !=
            PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_BDSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: BridgeDetection Machine "
                          "returned failure!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }

        PvrstBdSmStartEdgeDelayWhile (pPerStPortInfo->u2PortNo, u2InstIndex);

        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_PROPOSE \n", pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine
            (PVRST_PTXSM_EV_NEWINFO_SET, pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    if (((((pPerStRstPortInfo->bLearning == PVRST_FALSE) &&
           (pPerStRstPortInfo->bForwarding == PVRST_FALSE)) ||
          (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
          (pPortInfo->bOperEdgePort == PVRST_TRUE))
         &&
         (pPerStRstPortInfo->bSynced == PVRST_FALSE))
        ||
        ((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
         (pPerStRstPortInfo->bSynced == PVRST_TRUE)))
    {

       /***************************************/
        /* Stop rrWhile Timer if it is running */
       /***************************************/
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer for "
                              "RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state DESIGNATED_SYNCED \n",
                      pPerStPortInfo->u2PortNo);
    }

    if ((pPerStRstPortInfo->pRrWhileTmr == NULL) &&
        (pPerStRstPortInfo->bReRoot == PVRST_TRUE))
    {
        pPerStRstPortInfo->bReRoot = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state DESIGNATED_RETIRED \n",
                      pPerStPortInfo->u2PortNo);
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {
        if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE) &&
            (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
        {
            if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pCommPortInfo->pEdgeDelayWhileTmr == NULL))

            {
                pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                pPerStRstPortInfo->bLearn = PVRST_FALSE;
                pPerStRstPortInfo->bForward = PVRST_FALSE;
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (u2PortNum)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AstLoopIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pPerStPortInfo->
                                                                u2PortNo),
                                               pPerStPortInfo->bLoopIncStatus,
                                               u2InstIndex,
                                               (INT1 *) AST_PVRST_TRAPS_OID,
                                               PVRST_BRG_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature blocking Port: %s for Vlan :%d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                if (PvrstPortStateTrMachine
                    (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return PVRST_FAILURE;
                }

            }
            else
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward function"
                                  " returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state "
                  "DESIGNATED_PORT \n", pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmFwdExpRootPort                         */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Forward Delay timer     */
/*                      expires in the RootPort state.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmFwdExpRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT2               u2InstIndex = AST_INIT_VAL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_FWD_EXP_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is"
                      " not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
    {
        if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: MakeLearn function"
                          " returned FAILURE\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    else if (pPerStRstPortInfo->bForward == PVRST_FALSE)
    {
        /* Root-port forwarding is restricted when the BPDU is not received 
         * till the expiry of edge-delaywhile timer if the loop-guard is enabled
         * on that port */
        if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
            && (pPortInfo->bOperPointToPoint == PVRST_TRUE)
            && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
            pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
            pPerStRstPortInfo->bLearn = PVRST_FALSE;
            pPerStRstPortInfo->bForward = PVRST_FALSE;
            if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                         (tVlanId) pPerStPortInfo->u2Inst,
                                         AST_GET_IFINDEX (pPerStPortInfo->
                                                          u2PortNo)) ==
                OSIX_TRUE)
            {
                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2InstIndex,
                                           (INT1 *) AST_PVRST_TRAPS_OID,
                                           PVRST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  pPerStPortInfo->u2Inst, au1TimeStr);
            }
            if (PvrstPortStateTrMachine (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                         pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return PVRST_FAILURE;
            }

        }
        else
        {
            if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: MakeForward function "
                              "returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        /* After calling Make Forward, this Port would have transitioned to
         * the Forwarding state (i.e.) bForward would be True */

        /* Shifting to ReRooted state */
        if (pPerStRstPortInfo->bReRoot == PVRST_TRUE)
        {
            pPerStRstPortInfo->bReRoot = PVRST_FALSE;
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Moved to state REROOTED \n",
                          pPerStPortInfo->u2PortNo);

        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmFwdExpDesgPort                         */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Forward Delay timer     */
/*                      expires in the DesignatedPort state.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmFwdExpDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex =
        AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                      pPerStPortInfo->u2Inst);

    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if ((PvrstHandleStpCompatabilityCh (PVRST_FWD_EXP_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is "
                      "not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pAstCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (pPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (((pPerStRstPortInfo->pRrWhileTmr == NULL) ||
         (pPerStRstPortInfo->bReRoot == PVRST_FALSE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {

        if ((pPerStRstPortInfo->bLearn == PVRST_FALSE) &&
            (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
        {
            /* Designated-port learning is restricted when the BPDU is not received 
             * till the expiry of edge-delaywhile timer if the loop-guard is enabled
             * on that port - This is because the port might otherwise take 
             * Forward delay time in Learning state & then re-converge to Discarding 
             * state*/
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                {
                    /* LoopInconsitent state is set when the port transtions
                     * to Designated/Discarding because of loop-guard feature*/
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }

            }
            else
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
        else if ((pPerStRstPortInfo->bForward == PVRST_FALSE) &&
                 (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
        {
            /* Designated-port forwarding is restricted when the BPDU is not received 
             * till the expiry of edge-delaywhile timer if the loop-guard is enabled
             * on that port */
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                /* LoopInconsitent state is set when the port transtions
                 * to Designated/Discarding because of loop-guard feature*/
                pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                pPerStRstPortInfo->bLearn = PVRST_FALSE;
                pPerStRstPortInfo->bForward = PVRST_FALSE;
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AstLoopIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pPerStPortInfo->
                                                                u2PortNo),
                                               pPerStPortInfo->bLoopIncStatus,
                                               u2InstIndex,
                                               (INT1 *) AST_PVRST_TRAPS_OID,
                                               PVRST_BRG_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                if (PvrstPortStateTrMachine
                    (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return PVRST_FAILURE;
                }

            }
            else
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state DESIGNATED_PORT \n",
                  pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRrWhileExpDesgPort                     */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the rrWhile timer expires   */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRrWhileExpDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pRootPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    INT4                i4ReRooted = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if ((PvrstHandleStpCompatabilityCh (PVRST_WHILE_EXP_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo->bReRoot = PVRST_FALSE;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state DESIGNATED_RETIRED \n",
                  pPerStPortInfo->u2PortNo);

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {

        if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
        {
            /* Designated-port forwarding is restricted when the BPDU is not received 
             * till the expiry of edge-delaywhile timer if the loop-guard is enabled
             * on that port */
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
            }
            else if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL)
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE))
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }

            }
        }
    }

    /* Check if RrWhile Timer is not running for all other ports. 
     * If so, trigger the Root Port with Rerooted event */
    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

    if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
    {
        i4ReRooted =
            PvrstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort, u2InstIndex);
        if (i4ReRooted == PVRST_SUCCESS)
        {

            pRootPortInfo = PVRST_GET_PERST_PORT_INFO
                (pPerStBrgInfo->u2RootPort, u2InstIndex);

            if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_REROOTED_SET,
                                        pRootPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Port Role Transition Machine"
                              " returned FAILURE!\n", pPerStPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: Port Role Transition Machine "
                              "returned FAILURE!\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }                        /* All Ports are synced */
    }                            /* Root Port exists */

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state DESIGNATED_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmProposedSetRootPort                    */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the RootPort state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmProposedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstBoolean         bAllSynced = PVRST_FALSE;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_PROPOSED_SET_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is "
                      "not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (pPerStRstPortInfo->bSynced == PVRST_FALSE)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state ROOT_PROPOSED \n",
                      pPerStPortInfo->u2PortNo);

        if (PvrstProleTrSmSetSyncBridge (u2PortNum, u2InstIndex)
            != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: SetSyncBridge returned "
                          "FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }

        pPerStRstPortInfo->bProposed = PVRST_FALSE;
    }

    bAllSynced =
        PvrstIsAllOtherPortsSynced (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (((pPerStRstPortInfo->bProposed == PVRST_TRUE) ||
         (pPerStRstPortInfo->bSynced == PVRST_FALSE)) &&
        (bAllSynced == PVRST_TRUE))
    {

        pPerStRstPortInfo->bProposed = PVRST_FALSE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        pPerStRstPortInfo->bSynced = PVRST_TRUE;

        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state ROOT_AGREED \n",
                      pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                      pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit machine returned "
                          "FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmAgreedSetDesgPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when agreed Variable is set      */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmAgreedSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_AGREED_SET_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (pPerStRstPortInfo->bSynced == PVRST_FALSE)
    {
        /* Stop rrWhile Timer */
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer for"
                              " RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);
    }

    if (((pPerStRstPortInfo->pRrWhileTmr == NULL) ||
         (pPerStRstPortInfo->bReRoot == PVRST_FALSE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {
        if ((pPerStRstPortInfo->bLearn == PVRST_FALSE) &&
            (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
        {
            if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: MakeLearn function "
                              "returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
    }

    if (((pPerStRstPortInfo->pRrWhileTmr == NULL) ||
         (pPerStRstPortInfo->bReRoot == PVRST_FALSE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
            }
            else
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward function"
                                  " returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state"
                  " DESIGNATED_PORT \n", pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRbWhileExpRootPort                     */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the rbWhile timer expires   */
/*                      in the RootPort state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRbWhileExpRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    INT4                i4ReRooted = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      u2PortNum);
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Since Rapid Transition is Possible only if the version is >= 2 
     * check for it.
     */
    if ((PvrstHandleStpCompatabilityCh (PVRST_WHILE_EXP_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);

    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)
    {
        i4ReRooted = PvrstProleTrSmIsReRooted (u2PortNum, u2InstIndex);
        if (i4ReRooted == PVRST_SUCCESS)
        {
            if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn function "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }

        if ((i4ReRooted == PVRST_SUCCESS) && (pPerStRstPortInfo->bLearn
                                              == PVRST_TRUE))
        {
            if (pPerStRstPortInfo->bForward == PVRST_FALSE)
            {
                if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                    && (pPortInfo->bOperPointToPoint == PVRST_TRUE)
                    && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
                {
                    if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                    {
                        pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                        pPerStRstPortInfo->bLearn = PVRST_FALSE;
                        pPerStRstPortInfo->bForward = PVRST_FALSE;
                        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                     (tVlanId) pPerStPortInfo->
                                                     u2Inst,
                                                     AST_GET_IFINDEX
                                                     (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
                        {
                            UtlGetTimeStr (au1TimeStr);
                            AstLoopIncStateChangeTrap ((UINT2)
                                                       AST_GET_IFINDEX
                                                       (pPerStPortInfo->
                                                        u2PortNo),
                                                       pPerStPortInfo->
                                                       bLoopIncStatus,
                                                       u2InstIndex,
                                                       (INT1 *)
                                                       AST_PVRST_TRAPS_OID,
                                                       PVRST_BRG_TRAPS_OID_LEN);
                            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                              "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                              AST_GET_IFINDEX_STR
                                              (pPerStPortInfo->u2PortNo),
                                              pPerStPortInfo->u2Inst,
                                              au1TimeStr);
                        }
                        if (PvrstPortStateTrMachine
                            (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                             pPerStPortInfo) != PVRST_SUCCESS)
                        {
                            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                          AST_ALL_FAILURE_TRC,
                                          "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo));
                            AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                          AST_ALL_FAILURE_DBG,
                                          "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo));
                            return PVRST_FAILURE;
                        }
                    }
                }
                else
                {
                    if (PvrstProleTrSmMakeForward (pPerStPortInfo) !=
                        PVRST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %u: MakeForward function"
                                      " returned FAILURE\n",
                                      pPerStPortInfo->u2PortNo);
                        return PVRST_FAILURE;
                    }
                }
            }
            if (pPerStRstPortInfo->bReRoot == PVRST_TRUE)
            {
                pPerStRstPortInfo->bReRoot = PVRST_FALSE;
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Moved to state "
                              "REROOTED \n", pPerStPortInfo->u2PortNo);
            }
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;
}

/* Changes for IEEE P802.1D/D1 - 2003 Edition a
 * nd IEEE DRAFT P802.1y/D4 
 * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
/*****************************************************************************/
/* Function Name      : PvrstProleTrSmDisputedSetDesgPort                    */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Disputed variable is set*/
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmDisputedSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_DISPUTED_SET_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if ((pPortInfo->bOperEdgePort == PVRST_FALSE) &&
        ((pPerStRstPortInfo->bLearn == PVRST_TRUE) ||
         (pPerStRstPortInfo->bForward == PVRST_TRUE)))
    {
        if (PvrstProleTrSmMakeDesignatedListen (pPerStPortInfo)
            != PVRST_SUCCESS)
        {
            return PVRST_FAILURE;
        }
    }

    if ((pPerStRstPortInfo->bForward == PVRST_FALSE) &&
        (pPerStRstPortInfo->bAgreed == PVRST_FALSE) &&
        (pPerStRstPortInfo->bProposing == PVRST_FALSE) &&
        (pPortInfo->bOperEdgePort == PVRST_FALSE))
    {
        pPerStRstPortInfo->bProposing = PVRST_TRUE;

        if (PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PROPOSING_SET,
                                      pPerStPortInfo->u2PortNo)
            != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_BDSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: BridgeDetection Machine"
                          " returned failure!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }

        PvrstBdSmStartEdgeDelayWhile (pPerStPortInfo->u2PortNo, u2InstIndex);

        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_PROPOSE \n", pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                      pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit"
                          " machine returned FAILURE !!! \n",
                          pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit"
                          " machine returned FAILURE !!! \n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    if (((((pPerStRstPortInfo->bLearning == PVRST_FALSE) &&
           (pPerStRstPortInfo->bForwarding == PVRST_FALSE)) ||
          (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
          (pPortInfo->bOperEdgePort == PVRST_TRUE))
         &&
         (pPerStRstPortInfo->bSynced == PVRST_FALSE))
        ||
        ((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
         (pPerStRstPortInfo->bSynced == PVRST_TRUE)))
    {

       /***************************************/
        /* Stop rrWhile Timer if it is running */
       /***************************************/
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer"
                              " for RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state"
                      " DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);
    }

    if ((pPerStRstPortInfo->pRrWhileTmr == NULL) &&
        (pPerStRstPortInfo->bReRoot == PVRST_TRUE))
    {
        pPerStRstPortInfo->bReRoot = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state"
                      " DESIGNATED_RETIRED \n", pPerStPortInfo->u2PortNo);
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {
        if ((pPerStRstPortInfo->bLearn == PVRST_FALSE) &&
            (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
        {
            if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
            {
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s for vlan :%d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn"
                                  " function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
                && (pPortInfo->bOperPointToPoint == PVRST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            {
                if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap (AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan :%d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst), au1TimeStr;
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
            }
            else
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
        if ((pPerStRstPortInfo->bLearn == AST_TRUE) &&
            (pPerStRstPortInfo->bForward == AST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == AST_TRUE))
        {
            pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
        }

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_PORT \n", pPerStPortInfo->u2PortNo);

    }
    return PVRST_SUCCESS;
}
#endif /* IEEE8021Y_Z12_WANTED */

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmIsReRooted                             */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      rrWhile timer is not running for any of the other    */
/*                      ports except this RootPort.                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of this Port             */
/*                        u2InstIndex - Index of Spanning Tree Identifier         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmIsReRooted (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2Count;

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_IS_RE_ROOTED,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    for (u2Count = 1; u2Count <= AST_MAX_NUM_PORTS; u2Count++)
    {
        if ((pPortInfo = AST_GET_PORTENTRY (u2Count)) != NULL)
        {
            pPerStRstPortInfo = PVRST_GET_PERST_RST_PORT_INFO (u2Count,
                                                               u2InstIndex);
            if (u2Count == u2PortNum || pPerStRstPortInfo == NULL)
            {
                continue;
            }
            if (pPerStRstPortInfo->pRrWhileTmr != NULL)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: RrWhileTmr Still Running...\n",
                              u2Count);
                return PVRST_FAILURE;
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRoleChanged                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state as per the role changed          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRoleChanged (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_ROLE_CHANGED,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not"
                      " Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    switch (pPerStPortInfo->u1SelectedPortRole)
    {
        case AST_PORT_ROLE_DISABLED:
        case AST_PORT_ROLE_ALTERNATE:
            /* If this Port Role is changing from Backup Port Role 
             * to Alternate or Disabled Port Role, Start the Recent 
             * Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port"
                              " Role is BACKUP\n", pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) u2InstIndex,
                     (UINT1) AST_TMR_TYPE_RBWHILE,
                     (UINT2) (2 * pPerStPvrstRstPortInfo->PortPvrstTimes.
                              u2HelloTime)) != (INT4) PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer of "
                                  "RbWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }

            /* Fall through and continue processing for Backup Role */
        case AST_PORT_ROLE_BACKUP:

            return (PvrstProleTrSmMakeBlockPort (pPerStPortInfo));

        case AST_PORT_ROLE_ROOT:

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: New Port Role "
                          "selected is ROOT\n", pPerStPortInfo->u2PortNo);

            /* If this Port Role is changing from Backup Port Role to Root Port
             * Role, Start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port Role is BACKUP\n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) u2InstIndex,
                     (UINT1) AST_TMR_TYPE_RBWHILE,
                     (UINT2) (2 * pPerStPvrstRstPortInfo->PortPvrstTimes.
                              u2HelloTime)) != (INT4) PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer for "
                                  "RbWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            /* If the port role is changing from Designated to Root
             * and loop guard is enabled on this port, set the loop guard
             * status in per spanning tree data structure*/
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
                && (pPortInfo->bLoopGuard == AST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE))
            {
                pPerStPortInfo->bLoopGuardStatus = AST_TRUE;
            }

            /* If this Port Role is changing from Backup or Alternate or Disabled
             * Port Roles to Root Port Role, Start the Forward Delay timer.
             */
            if (((pPerStPortInfo->u1PortRole ==
                  (UINT1) AST_PORT_ROLE_DISABLED)
                 || (pPerStPortInfo->u1PortRole ==
                     (UINT1) AST_PORT_ROLE_ALTERNATE)
                 || (pPerStPortInfo->u1PortRole ==
                     (UINT1) AST_PORT_ROLE_BACKUP))
                && ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE) &&
                    (pPortInfo->bOperPointToPoint == PVRST_TRUE)))
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port Role is "
                              "DISABLED/BACKUP/ALTERNATE\n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, u2InstIndex,
                     (UINT1) AST_TMR_TYPE_FDWHILE,
                     pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay)
                    != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer for "
                                  "FdWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }

            return (PvrstProleTrSmMakeRootPort (pPerStPortInfo));

        case AST_PORT_ROLE_DESIGNATED:

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: New Port Role selected "
                          "is DESIGNATED\n", pPerStPortInfo->u2PortNo);

            /* If this Port Role is changing from Root Port Role to Designated Port
             * Role, then Start the Recent Root While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port Role is ROOT\n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, u2InstIndex,
                     (UINT1) AST_TMR_TYPE_RRWHILE,
                     pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay)
                    != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer for "
                                  "RrWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }

            }
            /* If this Port Role is changing from Backup Port Role to Designated
             * Port Role, then start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port Role is BACKUP\n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, (UINT2) u2InstIndex,
                     (UINT1) AST_TMR_TYPE_RBWHILE,
                     (UINT2) (2 * pPerStPvrstRstPortInfo->PortPvrstTimes.
                              u2HelloTime)) != (INT4) PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer "
                                  "for RbWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            /* If this Port Role is changing from Backup or Alternate or Disabled
             * Port Role to Designated Port Role, then start Forward Delay While
             * Timer.
             */
            if ((pPerStPortInfo->u1PortRole ==
                 (UINT1) AST_PORT_ROLE_DISABLED)
                || (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP))
            {

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Old Port Role is"
                              " DISABLED/BACKUP/ALTERNATE\n",
                              pPerStPortInfo->u2PortNo);

                if (PvrstStartTimer
                    ((VOID *) pPerStPortInfo, u2InstIndex,
                     (UINT1) AST_TMR_TYPE_FDWHILE,
                     pPerStPvrstRstPortInfo->PortPvrstTimes.u2ForwardDelay)
                    != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Start Timer for "
                                  "FdWhileTmr Failed !!!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
            return (PvrstProleTrSmMakeDesgPort (pPerStPortInfo));

        default:
            /* Selected Port Role is unknown, so return failure */
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Unknown Port Role selected\n",
                          pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeInitPort                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to INIT_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeInitPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DISABLED;
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_INIT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %u: Port Role Made as -> DISABLED\n",
                  pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %u: Port Role Made as -> DISABLED\n",
                  pPerStPortInfo->u2PortNo);

    pPerStRstPortInfo->bSynced = PVRST_FALSE;
    pPerStRstPortInfo->bSync = PVRST_TRUE;
    pPerStRstPortInfo->bReRoot = PVRST_TRUE;

    pPerStPortInfo->u1ProleTrSmState = (UINT1) PVRST_PROLETRSM_STATE_INIT_PORT;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state INIT_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return (PvrstProleTrSmMakeBlockPort (pPerStPortInfo));
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeBlockPort                          */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to BLOCK_PORT.                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeBlockPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;
    if ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ALTERNATE) &&
        (pAstPortEntry->bLoopGuard == PVRST_TRUE) &&
        (pAstPortEntry->bOperPointToPoint == PVRST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == PVRST_FALSE))
    {
        pPerStPortInfo->bLoopGuardStatus = PVRST_TRUE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Port Role Made as -> SELECTED ROLE \n",
                  pPerStPortInfo->u2PortNo);
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_BLOCK_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo->bLearn = PVRST_FALSE;
    pPerStRstPortInfo->bForward = PVRST_FALSE;

    pPerStPortInfo->u1ProleTrSmState = (UINT1) PVRST_PROLETRSM_STATE_BLOCK_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state BLOCK_PORT \n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstPortStateTrMachine (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                 pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Port State Transition "
                      "chine returned FAILURE !\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Port State Transition "
                      "chine returned FAILURE !\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if ((pPerStRstPortInfo->bLearning == PVRST_FALSE) &&
        (pPerStRstPortInfo->bForwarding == PVRST_FALSE))
        return (PvrstProleTrSmMakeBlockedPort (pPerStPortInfo));

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeBlockedPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to BLOCKED_PORT.                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeBlockedPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstRootPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    tAstBoolean         bPrevSynced;
    tPerStPvrstRstPortInfo *pPerStPvrstRstRootPortInfo = NULL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2RootPort = 0;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_BLOCKED_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    bPrevSynced = pPerStRstPortInfo->bSynced;
    pPerStRstPortInfo->bSynced = PVRST_TRUE;

    /* If Forward Delay While timer is running stop it.
     * This timer will again be started only when the PortRole changes to 
     * RootPort or DesignatedPort.
     */
    if (pPerStRstPortInfo->pFdWhileTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                            u2InstIndex, (UINT1) AST_TMR_TYPE_FDWHILE)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Stop Timer for "
                          "FdWhileTmr Failed !!!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    /* If Recent Root While timer is running stop it */
    if (pPerStRstPortInfo->pRrWhileTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                            u2InstIndex, (UINT1) AST_TMR_TYPE_RRWHILE)
            != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Stop Timer for "
                          "RrWhileTmr Failed !!!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    pPerStRstPortInfo->bSync = PVRST_FALSE;
    pPerStRstPortInfo->bReRoot = PVRST_FALSE;
    pPerStPortInfo->u1ProleTrSmState =
        (UINT1) PVRST_PROLETRSM_STATE_BLOCKED_PORT;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state BLOCKED_PORT \n",
                  pPerStPortInfo->u2PortNo);

    /*Resetting Loop Inconsistency in case of Alternate Port moving to 
     * Discarding state*/
    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) &&
        (pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
        (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
        (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE))
    {
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     (tVlanId) pPerStPortInfo->u2Inst,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan :%d at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              pPerStPortInfo->u2Inst, au1TimeStr);
        }
        pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
    }

    if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_BLOCKEDPORT,
                            pPerStPortInfo) != PVRST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
    u2RootPort = pPerStBrgInfo->u2RootPort;

    if ((u2RootPort != AST_INIT_VAL) && (bPrevSynced == PVRST_FALSE))
    {
        pPerStRootPortInfo =
            PVRST_GET_PERST_PORT_INFO (u2RootPort, u2InstIndex);
        pPerStRstRootPortInfo =
            PVRST_GET_PERST_RST_PORT_INFO (u2RootPort, u2InstIndex);

        pPerStRstRootPortInfo->bProposed = PVRST_FALSE;
        pPerStRstRootPortInfo->bSync = PVRST_FALSE;
        pPerStRstRootPortInfo->bSynced = PVRST_TRUE;

        if (pPerStRootPortInfo != NULL)
        {
            pPerStPvrstRstRootPortInfo =
                PVRST_GET_PERST_PVRST_RST_PORT_INFO
                (pPerStRootPortInfo->u2PortNo, u2InstIndex);
            if (pPerStPvrstRstRootPortInfo == NULL)
            {
                return PVRST_FAILURE;
            }

            pPerStPvrstRstRootPortInfo->bNewInfo = PVRST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Moved to state ROOT_AGREED \n",
                          pPerStPortInfo->u2PortNo);

            if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                          pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Port Transmit machine "
                              "returned FAILURE !!! \n",
                              pPerStRootPortInfo->u2PortNo);
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: Port Transmit machine "
                              "returned FAILURE !!! \n",
                              pPerStRootPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

    }

    PVRST_SET_CHANGED_FLAG (u2InstIndex, pPerStPortInfo->u2PortNo) = PVRST_TRUE;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeRootPort                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstBoolean         bAllSynced = PVRST_FALSE;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    INT4                i4ReRooted = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;

    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    tAstPortEntry      *pPortInfo = NULL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      u2PortNum);
        return PVRST_FAILURE;
    }

    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_ROOT;
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %u: Port Role Made as -> ROOT\n",
                  pPerStPortInfo->u2PortNo);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) PVRST_PROLETRSM_STATE_ROOT_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_ROOTPORT,
                            pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if ((pPerStRstPortInfo->bProposed == PVRST_TRUE) &&
        (pPerStRstPortInfo->bSynced == PVRST_FALSE))
    {

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "ROOT_PROPOSED \n", pPerStPortInfo->u2PortNo);

        if (PvrstProleTrSmSetSyncBridge (u2PortNum, u2InstIndex)
            != PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: SetSyncBridge "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }

        pPerStRstPortInfo->bProposed = PVRST_FALSE;
    }

    if (pPerStRstPortInfo->bForward == PVRST_FALSE)
    {
        if (pPerStRstPortInfo->bReRoot == PVRST_FALSE)
        {

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Moved to state REROOT \n",
                          pPerStPortInfo->u2PortNo);
            if (PvrstProleTrSmSetReRootBridge (u2InstIndex) != PVRST_SUCCESS)
            {
                return PVRST_FAILURE;
            }
        }
    }

    i4ReRooted = PvrstProleTrSmIsReRooted (u2PortNum, u2InstIndex);
    if (i4ReRooted == PVRST_SUCCESS)
    {
        PvrstProleTrSmRerootedSetRootPort (pPerStPortInfo);
    }                            /* All Ports are Rerooted  */

    if (pPerStRstPortInfo->bForward == PVRST_TRUE)
    {
        if (pPerStRstPortInfo->bReRoot == PVRST_TRUE)
        {
            pPerStRstPortInfo->bReRoot = PVRST_FALSE;
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Moved to state REROOTED \n",
                          pPerStPortInfo->u2PortNo);
        }
    }

    bAllSynced = PvrstIsAllOtherPortsSynced
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (((pPerStRstPortInfo->bProposed == PVRST_TRUE) ||
         (pPerStRstPortInfo->bSynced == PVRST_FALSE)) &&
        (bAllSynced == PVRST_TRUE))
    {

        pPerStRstPortInfo->bProposed = PVRST_FALSE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        pPerStRstPortInfo->bSynced = PVRST_TRUE;

        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO
            (pPerStPortInfo->u2PortNo, u2InstIndex);

        if (pPerStPvrstRstPortInfo == NULL)
        {
            return PVRST_FAILURE;
        }

        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state ROOT_AGREED \n",
                      pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                      pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    /*Resetting Loop Inconsistency in case of Root Port moving to 
     * Forwarding state*/
    if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
        (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
        (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE))
    {
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     (tVlanId) pPerStPortInfo->u2Inst,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan :%d at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              pPerStPortInfo->u2Inst, au1TimeStr);
        }
        pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);

    PVRST_SET_CHANGED_FLAG (u2InstIndex, pPerStPortInfo->u2PortNo) = PVRST_TRUE;
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstIsAllOtherPortsSynced                           */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      Synced variable has been set for all the Ports other */
/*                      than this Port.                                      */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of this Port             */
/*                        u2InstIndex - Index of Spanning Tree Identifier         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
tAstBoolean
PvrstIsAllOtherPortsSynced (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2Count = 0;
    UINT1               u1OperStatus = AST_PORT_OPER_DOWN;

    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
    if (pPerStPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if ((PvrstHandleStpCompatabilityCh (PVRST_IS_ALL_OTHER_PORTS_SYNCED,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    for (u2Count = 1; u2Count <= AST_MAX_NUM_PORTS; u2Count++)
    {

        if ((u2Count == u2PortNum) ||
            ((pPortInfo = AST_GET_PORTENTRY (u2Count)) == NULL))
        {
            continue;
        }

        pPerStRstPortInfo =
            PVRST_GET_PERST_RST_PORT_INFO (u2Count, u2InstIndex);
        if (pPerStRstPortInfo == NULL ||
            pPerStRstPortInfo->bPortEnabled != PVRST_TRUE)

        {
            continue;
        }
        AstL2IwfGetPortOperStatus (STP_MODULE,
                                   (UINT4) AST_GET_IFINDEX (u2Count),
                                   &u1OperStatus);

        if (u1OperStatus == AST_PORT_OPER_DOWN)
        {
            continue;
        }
        if (pPerStRstPortInfo->bSynced == PVRST_TRUE)
        {
            continue;
        }
        else
        {
            return PVRST_FALSE;
        }
    }
    return PVRST_TRUE;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmAllSyncedSetRootPort                   */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. all the other ports are in sync */
/*                      with the current spanning tree information and the   */
/*                      Root Port may now signal an agreement to the         */
/*                      neighbouring Bridge.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmAllSyncedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_ALL_SYNCED_SET_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo =
        PVRST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is "
                      "not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if ((pPerStRstPortInfo->bProposed == PVRST_TRUE) ||
        (pPerStRstPortInfo->bSynced == PVRST_FALSE))
    {
        pPerStRstPortInfo->bProposed = PVRST_FALSE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        pPerStRstPortInfo->bSynced = PVRST_TRUE;

        pPerStPvrstRstPortInfo =
            PVRST_GET_PERST_PVRST_RST_PORT_INFO
            (pPerStPortInfo->u2PortNo, u2InstIndex);
        if (pPerStPvrstRstPortInfo == NULL)
        {
            return PVRST_FAILURE;
        }

        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state"
                      " ROOT_AGREED \n", pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                      pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    /*Resetting Loop Inconsistency in case of Root Port moving to 
     * Forwarding state*/
    if (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE)
    {
        if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                     (tVlanId) pPerStPortInfo->u2Inst,
                                     AST_GET_IFINDEX (pPerStPortInfo->
                                                      u2PortNo)) == OSIX_TRUE)
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan :%d at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              pPerStPortInfo->u2Inst, au1TimeStr);
        }
        pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmRerootedSetRootPort                    */
/*                                                                           */
/* Description        : This routine is called whenever the Rerooted event   */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. none of the other ports have the*/
/*                      RrWhile Timer running and hence the Root Port may now*/
/*                      transition to Forwarding.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmRerootedSetRootPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_REROOTED_SET_ROOT_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo =
        PVRST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is "
                      "not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if ((AST_FORCE_VERSION >= (UINT1) AST_VERSION_2) &&
        (pPerStRstPortInfo->pRbWhileTmr == NULL))
    {
        if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
        {
            if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: MakeLearn function "
                              "returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
    }

    if ((AST_FORCE_VERSION >= (UINT1) AST_VERSION_2) &&
        (pPerStRstPortInfo->pRbWhileTmr == NULL) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            /*Resetting Loop Inconsistency in case of Root Port moving to 
             * Forwarding state*/
            if (pPerStPortInfo->bLoopIncStatus == PVRST_TRUE)
            {
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {

                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s for Vlan :%d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                pPerStPortInfo->bLoopIncStatus = PVRST_FALSE;
            }

            if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %u: MakeForward function"
                              " returned FAILURE\n", pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }
    }

    if (pPerStRstPortInfo->bForward == PVRST_TRUE)
    {
        if (pPerStRstPortInfo->bReRoot == PVRST_TRUE)
        {
            pPerStRstPortInfo->bReRoot = PVRST_FALSE;
            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: Moved to state REROOTED \n",
                          pPerStPortInfo->u2PortNo);
        }
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state ROOT_PORT \n",
                  pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeDesgPort                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DESG_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstBoolean         bAllSynced = PVRST_FALSE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pRootPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2InstIndexFound = AST_INIT_VAL;
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->
                                             u2PortNo, u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DESIGNATED;
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %u: Port Role Made as -> DESIGNATED\n",
                  pPerStPortInfo->u2PortNo);
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Port Role Made as -> DESIGNATED\n",
                  pPerStPortInfo->u2PortNo);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) PVRST_PROLETRSM_STATE_DESG_PORT;
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state " "DESIGNATED_PORT \n",
                  pPerStPortInfo->u2PortNo);

    if (PvrstTopoChMachine (PVRST_TOPOCHSM_EV_DESGPORT,
                            pPerStPortInfo) != PVRST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE!!! \n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Topology Change Machine "
                      "returned FAILURE!!! \n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is not "
                      "Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }
    if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE)
        && (pPortInfo->bOperPointToPoint == PVRST_TRUE)
        && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
    {
        if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
        {
            pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
            pPerStRstPortInfo->bLearn = PVRST_FALSE;
            pPerStRstPortInfo->bForward = PVRST_FALSE;
            if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                         (tVlanId) pPerStPortInfo->u2Inst,
                                         AST_GET_IFINDEX (pPerStPortInfo->
                                                          u2PortNo)) ==
                OSIX_TRUE)
            {
                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2InstIndex,
                                           (INT1 *) AST_PVRST_TRAPS_OID,
                                           PVRST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  pPerStPortInfo->u2Inst, au1TimeStr);
            }
            if (PvrstPortStateTrMachine
                (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                 pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return PVRST_FAILURE;
            }
        }

    }

    if ((((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
          (pPerStRstPortInfo->bSynced == PVRST_FALSE))
         ||
         ((pPerStRstPortInfo->bReRoot == PVRST_TRUE) &&
          (pPerStRstPortInfo->pRrWhileTmr != NULL)))
        &&
        (pPortInfo->bOperEdgePort == PVRST_FALSE)
        &&
        ((pPerStRstPortInfo->bLearn == PVRST_TRUE) ||
         (pPerStRstPortInfo->bForward == PVRST_TRUE)))
    {
        if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE)
            && (pPortInfo->bOperPointToPoint == PVRST_TRUE))
        {
            if (PvrstProleTrSmMakeDesignatedListen (pPerStPortInfo)
                != PVRST_SUCCESS)
            {
                return PVRST_FAILURE;
            }
        }
    }

    if ((pPerStRstPortInfo->bForward == PVRST_FALSE) &&
        (pPerStRstPortInfo->bAgreed == PVRST_FALSE) &&
        (pPerStRstPortInfo->bProposing == PVRST_FALSE) &&
        (pPortInfo->bOperEdgePort == PVRST_FALSE))
    {
        pPerStRstPortInfo->bProposing = PVRST_TRUE;

        if (PvrstBrgDetectionMachine (PVRST_BRGDETSM_EV_PROPOSING_SET,
                                      pPerStPortInfo->u2PortNo) !=
            PVRST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_BDSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: BridgeDetection Machine "
                          "returned failure!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
        PvrstBdSmStartEdgeDelayWhile (pPerStPortInfo->u2PortNo, u2InstIndex);
        pPerStPvrstRstPortInfo->bNewInfo = PVRST_TRUE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_PROPOSE \n", pPerStPortInfo->u2PortNo);

        if (PvrstPortTransmitMachine
            (PVRST_PTXSM_EV_NEWINFO_SET, pPerStPortInfo) != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %u: Port Transmit machine "
                          "returned FAILURE !!! \n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }

    if (((((pPerStRstPortInfo->bLearning == PVRST_FALSE) &&
           (pPerStRstPortInfo->bForwarding == PVRST_FALSE)) ||
          (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
          (pPortInfo->bOperEdgePort == PVRST_TRUE))
         &&
         (pPerStRstPortInfo->bSynced == PVRST_FALSE))
        ||
        ((pPerStRstPortInfo->bSync == PVRST_TRUE) &&
         (pPerStRstPortInfo->bSynced == PVRST_TRUE)))
    {

       /***************************************/
        /* Stop rrWhile Timer if it is running */
       /***************************************/
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer for "
                              "RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);

        /* Check if all other ports are synced. If so, trigger the Root Port 
         * with Synced set event */
        pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {

            bAllSynced = PvrstIsAllOtherPortsSynced
                (pPerStBrgInfo->u2RootPort, u2InstIndex);
            if (bAllSynced == PVRST_TRUE)
            {

                pRootPortInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                               u2InstIndex);

                if (PvrstPortRoleTrMachine
                    (PVRST_PROLETRSM_EV_ALLSYNCED_SET,
                     pRootPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Role Transition "
                                  "Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Role Transition "
                                  "Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }                    /* All Ports are synced */
        }                        /* Root Port exists */
    }

    if ((pPerStRstPortInfo->pRrWhileTmr == NULL) &&
        (pPerStRstPortInfo->bReRoot == PVRST_TRUE))
    {
        pPerStRstPortInfo->bReRoot = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_RETIRED \n", pPerStPortInfo->u2PortNo);
    }

    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {
        if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE) &&
                (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL))
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn function"
                                  " returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
         (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
         (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
        (((pPerStRstPortInfo->bReRoot == PVRST_FALSE) ||
          (pPerStRstPortInfo->pRrWhileTmr == NULL)) &&
         (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
                (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                (pCommPortInfo->pEdgeDelayWhileTmr == NULL))

            {
                pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                pPerStRstPortInfo->bLearn = PVRST_FALSE;
                pPerStRstPortInfo->bForward = PVRST_FALSE;
                if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                             (tVlanId) pPerStPortInfo->u2Inst,
                                             AST_GET_IFINDEX (pPerStPortInfo->
                                                              u2PortNo)) ==
                    OSIX_TRUE)
                {

                    UtlGetTimeStr (au1TimeStr);
                    AstLoopIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pPerStPortInfo->
                                                                u2PortNo),
                                               pPerStPortInfo->bLoopIncStatus,
                                               u2InstIndex,
                                               (INT1 *) AST_PVRST_TRAPS_OID,
                                               PVRST_BRG_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }
                if (PvrstPortStateTrMachine
                    (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                     pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return PVRST_FAILURE;
                }

            }
            else
            {
                if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE) &&
                    (pPortInfo->bOperPointToPoint == PVRST_TRUE))
                {
                    u2InstIndexFound = 1;
                    if (PvrstProleTrSmMakeForward (pPerStPortInfo) !=
                        PVRST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %u: MakeForward function "
                                      "returned FAILURE\n",
                                      pPerStPortInfo->u2PortNo);
                        return PVRST_FAILURE;
                    }
                }
            }
        }
    }

    if (u2InstIndexFound == 0)
    {
        if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
            (pPerStRstPortInfo->bForward == PVRST_TRUE) &&
            (pPortInfo->bOperPointToPoint == PVRST_TRUE))

        {
            pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
            pPerStRstPortInfo->bLearn = PVRST_FALSE;
            pPerStRstPortInfo->bForward = PVRST_FALSE;
            if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                         (tVlanId) pPerStPortInfo->u2Inst,
                                         AST_GET_IFINDEX (pPerStPortInfo->
                                                          u2PortNo)) ==
                OSIX_TRUE)
            {

                UtlGetTimeStr (au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2InstIndex,
                                           (INT1 *) AST_PVRST_TRAPS_OID,
                                           PVRST_BRG_TRAPS_OID_LEN);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  pPerStPortInfo->u2Inst, au1TimeStr);
            }
            if (PvrstPortStateTrMachine
                (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                 pPerStPortInfo) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return PVRST_FAILURE;
            }

        }

    }

    if ((pPerStRstPortInfo->bLearn == AST_TRUE) &&
        (pPerStRstPortInfo->bForward == AST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == AST_TRUE))
    {
        pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state DESIGNATED_PORT \n",
                  pPerStPortInfo->u2PortNo);

    PVRST_SET_CHANGED_FLAG (u2InstIndex, pPerStPortInfo->u2PortNo) = PVRST_TRUE;
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeForward                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_FORWARD or DESG_FORWARD  */
/*                      if the present state is ROOT_PORT or DESG_PORT       */
/*                      respectively.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeForward (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pAstCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_FORWARD,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    AST_DBG_ARG1 (AST_RTSM_DBG, "RTSM: Port %u: Moving to state FORWARD\n",
                  pPerStPortInfo->u2PortNo);

    if (pPerStRstPortInfo->pFdWhileTmr != NULL)
    {
        if (PvrstStopTimer ((VOID *) pPerStPortInfo,
                            u2InstIndex, (UINT1) AST_TMR_TYPE_FDWHILE)
            != PVRST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %u: Stop Timer for "
                          "FdWhileTmr Failed !!!\n", pPerStPortInfo->u2PortNo);
            return PVRST_FAILURE;
        }
    }
    pPerStRstPortInfo->bForward = PVRST_TRUE;

    /* Disabling transmission of inferior info in case the blocking the port
       had failed earlier.
       Now that the port can be forwarding there is no need to maintain the
       peer port in discarding state by sending inferior BPDUs any longer. */
    pPerStPortInfo->i4TransmitSelfInfo = PVRST_FALSE;

    if (PvrstPortStateTrMachine (PVRST_PSTATETRSM_EV_FORWARD,
                                 pPerStPortInfo) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Port State Transition "
                      "Machine returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Port State Transition "
                      "Machine returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state FORWARD \n",
                  pPerStPortInfo->u2PortNo);

    /* RootInconsistent reset when the port transtions to Designated/Forwarding
     *          * on expiry of rcvdInfoWhile timer.*/
    if (pAstCommPortInfo->bRootGuard == PVRST_TRUE)
    {
        if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 AST_TMR_TYPE_ROOT_INC_RECOVERY) != PVRST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "RTSM: PvrstStopTimer for ROOT INC RECOVERY FAILED!\n");
                return PVRST_FAILURE;
            }

        }
        pAstCommPortInfo->bRootInconsistent = PVRST_FALSE;
    }
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeLearn                              */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_LEARN or DESG_LEARN      */
/*                      if the present state is ROOT_PORT or DESG_PORT       */
/*                      respectively.                                        */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeLearn (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moving to state LEARN\n",
                  pPerStPortInfo->u2PortNo);

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_LEARN,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPerStRstPortInfo->bLearn = PVRST_TRUE;

    if (PvrstPortStateTrMachine (PVRST_PSTATETRSM_EV_LEARN,
                                 pPerStPortInfo) != PVRST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Port State Transition "
                      "Machine returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Port State Transition Machine "
                      "returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_FDWHILE,
                         pPerStPvrstRstPortInfo->PortPvrstTimes.
                         u2ForwardDelay) != PVRST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Start Timer for "
                      "FdWhileTmr Failed !!!\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state LEARN \n",
                  pPerStPortInfo->u2PortNo);

    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmMakeDesignatedListen                   */
/*                                                                           */
/* Description        : This routine performs the action for the state       */
/*                      machine state DESG_LISTEN.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmMakeDesignatedListen (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPvrstRstPortInfo = PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_MAKE_DESIGNATED_LISTEN,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moving to state DESIGNATED_LISTEN\n",
                  pPerStPortInfo->u2PortNo);

    pPerStRstPortInfo->bLearn = PVRST_FALSE;
    pPerStRstPortInfo->bForward = PVRST_FALSE;

    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    pPerStPortInfo->bDisputed = PVRST_FALSE;
#endif /* IEEE8021Y_Z12_WANTED */

    if (PvrstStartTimer ((VOID *) pPerStPortInfo, u2InstIndex,
                         (UINT1) AST_TMR_TYPE_FDWHILE,
                         pPerStPvrstRstPortInfo->PortPvrstTimes.
                         u2ForwardDelay) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Start Timer for FdWhileTmr"
                      " Failed !!!\n", pPerStPortInfo->u2PortNo);

        return PVRST_FAILURE;
    }

    if (PvrstPortStateTrMachine (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                 pPerStPortInfo) != PVRST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %u: Port State Transition Machine "
                      "returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %u: Port State Transition Machine "
                      "returned FAILURE!\n", pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Port %u: Moved to state "
                  "DESIGNATED_LISTEN \n", pPerStPortInfo->u2PortNo);
    return PVRST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmSetSyncBridge                          */
/*                                                                           */
/* Description        : This routine performs the setSyncBridge() procedure  */
/*                      of the Port Role Transition State Machine. Calls the */
/*                      PvrstPortRoleTrMachine() routine for all the ports   */
/*                        with 'SYNC_SET'.             */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number from which the sync set      */
/*                                  is initiated.                            */
/*            u2InstIndex - Index of Spanning Tree Identifier         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmSetSyncBridge (UINT2 u2PortNum, UINT2 u2InstIndex)
{
    UINT2               u2Count = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    for (u2Count = 1; u2Count <= AST_MAX_NUM_PORTS; u2Count++)
    {
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2Count, u2InstIndex);
        if (pPerStPortInfo != NULL)
        {
            if ((PvrstHandleStpCompatabilityCh (PVRST_SET_SYNC_BRIDGE,
                                                pPerStPortInfo)) !=
                PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Failure in handling "
                              "compatibility change.\n",
                              pPerStPortInfo->u2PortNo);
            }

            pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pPerStRstPortInfo->bSync = PVRST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG, "RTSM: Port %u: Sync Set...\n",
                          u2Count);

            /* For all Ports other than this Root Port, call the Role Transition
             * Machine with the SYNC_SET event */
            if (u2PortNum != u2Count)
            {
                if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_SYNC_SET,
                                            pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Role Transition "
                                  "Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Role Transition "
                                  "Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmSetReRootBridge                        */
/*                                                                           */
/* Description        : This routine sets the reRoot variable to TRUE for all*/
/*                      Ports of the Bridge.                                 */
/*                                                                           */
/* Input(s)           : u2InstIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmSetReRootBridge (UINT2 u2InstIndex)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {
        pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);
        if (pPerStPortInfo != NULL)
        {
            if ((PvrstHandleStpCompatabilityCh (PVRST_SET_REROOT_BRIDGE,
                                                pPerStPortInfo)) !=
                PVRST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %u: Failure in handling "
                              "compatibility change.\n",
                              pPerStPortInfo->u2PortNo);
            }

            pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pPerStRstPortInfo->bReRoot = PVRST_TRUE;

            AST_DBG_ARG1 (AST_RTSM_DBG,
                          "RTSM: Port %u: ReRoot Set...\n", u2PortNum);

            pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);

            if (u2PortNum != pPerStBrgInfo->u2RootPort)
            {
                if (PvrstPortRoleTrMachine (PVRST_PROLETRSM_EV_REROOT_SET,
                                            pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %u: Port Role Transition "
                                  "Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: Port Role Transition"
                                  " Machine returned FAILURE!\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmOperEdgeSetDesgPort                    */
/*                                                                           */
/* Description        : This routine is called whenever the operEdge variable*/
/*                      is set to TRUE for any port on the bridge            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstProleTrSmOperEdgeSetDesgPort (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }

    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);

    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_OPER_EDGE_SET_DESG_PORT,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return PVRST_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if (RST_IS_NOT_SELECTED (pPerStRstPortInfo))
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Selected,UpdtInfo is "
                      "not Valid - EXITING.\n", pPerStPortInfo->u2PortNo);
        return PVRST_SUCCESS;
    }

    if (pPerStRstPortInfo->bSynced == PVRST_FALSE)
    {
        /* Stop rrWhile Timer */
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer "
                              "for RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);
    }

    if (((pPerStRstPortInfo->pRrWhileTmr == NULL) ||
         (pPerStRstPortInfo->bReRoot == PVRST_FALSE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE))
    {
        if (pPerStRstPortInfo->bLearn == PVRST_FALSE)
        {
            if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
            {
                if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
                    (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                    (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
                else
                {
                    if (pPerStPvrstRstPortInfo->pRootIncRecoveryTmr == NULL)
                    {

                        if (PvrstProleTrSmMakeLearn (pPerStPortInfo) !=
                            PVRST_SUCCESS)
                        {
                            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                          "RTSM: Port %u: MakeLearn function "
                                          "returned FAILURE\n",
                                          pPerStPortInfo->u2PortNo);
                            return PVRST_FAILURE;
                        }
                    }
                }
            }
        }
    }

    if (((pPerStRstPortInfo->pRrWhileTmr == NULL) ||
         (pPerStRstPortInfo->bReRoot == PVRST_FALSE)) &&
        (pPerStRstPortInfo->bSync == PVRST_FALSE) &&
        (pPerStRstPortInfo->bLearn == PVRST_TRUE))
    {
        if (pPerStRstPortInfo->bForward == PVRST_FALSE)
        {
            if ((pPerStPortInfo->bLoopGuardStatus != PVRST_TRUE) &&
                (pPortInfo->bOperPointToPoint == PVRST_TRUE))
            {
                if (PvrstProleTrSmMakeForward (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward function"
                                  " returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstProleTrSmLearningFwdingReset                    */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Learning and Forwarding */
/*                      variables have been reset after the Hardware port    */
/*                      state is put to Blocking.This function will be called*/
/*                      only when the NPAPI programming mode is asynchronous */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
PvrstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstRootPortInfo = NULL;
    tPerStPvrstRstPortInfo *pPerStPvrstRstRootPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstBoolean         bAllSynced = PVRST_FALSE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    INT4                i4ReRooted = AST_INIT_VAL;
    UINT2               u2InstIndex = AST_INIT_VAL;
    UINT2               u2RootPort = 0;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    UINT4               u4ContextId = AST_CURR_CONTEXT_ID ();
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if ((PvrstHandleStpCompatabilityCh (PVRST_LEARNING_FORWARDING_RESET,
                                        pPerStPortInfo)) != PVRST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Failure in handling "
                      "compatibility change.\n", pPerStPortInfo->u2PortNo);
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    pPerStRstPortInfo = PVRST_GET_PERST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    pPerStPvrstRstRootPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO
        (pPerStPortInfo->u2PortNo, u2InstIndex);

    if (pPerStPvrstRstRootPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if (pCommPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
    {
        return PvrstProleTrSmMakeBlockedPort (pPerStPortInfo);
    }
    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
             (pPerStRstPortInfo->bSynced == RST_FALSE))
    {
         /***************************************/
        /* Stop rrWhile Timer if it is running */
         /***************************************/
        if (pPerStRstPortInfo->pRrWhileTmr != NULL)
        {
            if (PvrstStopTimer
                ((VOID *) pPerStPortInfo, u2InstIndex,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != PVRST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %u: Stop Timer for "
                              "RrWhileTmr Failed !!!\n",
                              pPerStPortInfo->u2PortNo);
                return PVRST_FAILURE;
            }
        }

        pPerStRstPortInfo->bSynced = PVRST_TRUE;
        pPerStRstPortInfo->bSync = PVRST_FALSE;
        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_SYNCED \n", pPerStPortInfo->u2PortNo);
        if (pPerStRstPortInfo->pRrWhileTmr == NULL)
            pPerStRstPortInfo->bReRoot = PVRST_FALSE;

        AST_DBG_ARG1 (AST_RTSM_DBG,
                      "RTSM: Port %u: Moved to state "
                      "DESIGNATED_RETIRED \n", pPerStPortInfo->u2PortNo);

        pPerStBrgInfo = PVRST_GET_PERST_BRG_INFO (u2InstIndex);
        u2RootPort = pPerStBrgInfo->u2RootPort;

        if (u2RootPort != AST_INIT_VAL)
        {
            pPerStRootPortInfo =
                PVRST_GET_PERST_PORT_INFO (u2RootPort, u2InstIndex);
            if (pPerStRootPortInfo != NULL)
            {
                pPerStRstRootPortInfo =
                    PVRST_GET_PERST_RST_PORT_INFO (pPerStRootPortInfo->u2PortNo,
                                                   u2InstIndex);

                i4ReRooted =
                    PvrstProleTrSmIsReRooted (pPerStBrgInfo->u2RootPort,
                                              u2InstIndex);
                if (i4ReRooted == PVRST_SUCCESS)
                {
                    PvrstProleTrSmRerootedSetRootPort (pPerStRootPortInfo);
                }

                if (pPerStRstRootPortInfo->bForward == PVRST_TRUE)
                {
                    if (pPerStRstRootPortInfo->bReRoot == PVRST_TRUE)
                    {
                        pPerStRstRootPortInfo->bReRoot = PVRST_FALSE;
                        AST_DBG_ARG1 (AST_RTSM_DBG,
                                      "RTSM: Port %u: Moved to state REROOTED \n",
                                      pPerStBrgInfo->u2RootPort);
                    }
                }

                bAllSynced =
                    PvrstIsAllOtherPortsSynced (pPerStPortInfo->u2PortNo,
                                                u2InstIndex);

                if (bAllSynced == RST_TRUE)
                {
                    pPerStRstRootPortInfo->bProposed = PVRST_FALSE;
                    pPerStRstRootPortInfo->bSync = PVRST_FALSE;
                    pPerStRstRootPortInfo->bSynced = PVRST_TRUE;

                    pPerStPvrstRstRootPortInfo->bNewInfo = PVRST_TRUE;

                    AST_DBG_ARG1 (AST_RTSM_DBG,
                                  "RTSM: Port %u: Moved to state ROOT_AGREED \n",
                                  pPerStBrgInfo->u2RootPort);

                    if (PvrstPortTransmitMachine (PVRST_PTXSM_EV_NEWINFO_SET,
                                                  pPerStRootPortInfo) !=
                        PVRST_SUCCESS)

                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %u: Port Transmit"
                                      "machine returned FAILURE !!! \n",
                                      pPerStBrgInfo->u2RootPort);
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %u: Port Transmit"
                                      "machine returned FAILURE !!! \n",
                                      pPerStBrgInfo->u2RootPort);
                        return RST_FAILURE;
                    }
                }
            }
        }

        if (((pPerStRstPortInfo->pFdWhileTmr == NULL) ||
             (pPerStRstPortInfo->bAgreed == PVRST_TRUE) ||
             (pPortInfo->bOperEdgePort == PVRST_TRUE)) &&
            (pPerStRstPortInfo->bSync == PVRST_FALSE))
        {
            if ((pPerStRstPortInfo->bLearn == PVRST_FALSE) &&
                (pPerStPvrstRstRootPortInfo->pRootIncRecoveryTmr == NULL))
            {
                if (PvrstProleTrSmMakeLearn (pPerStPortInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return PVRST_FAILURE;
                }
            }
        }

        if ((pPerStRstPortInfo->bLearn == PVRST_TRUE) &&
            (pPerStRstPortInfo->bForward == PVRST_FALSE))
        {
            if (pPerStPortInfo->bLoopIncStatus != PVRST_TRUE)
            {
                if ((pPerStPortInfo->bLoopGuardStatus == PVRST_TRUE) &&
                    (pPortInfo->bOperPointToPoint == PVRST_TRUE) &&
                    (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
                {
                    pPerStPortInfo->bLoopIncStatus = PVRST_TRUE;
                    pPerStRstPortInfo->bLearn = PVRST_FALSE;
                    pPerStRstPortInfo->bForward = PVRST_FALSE;
                    if (L2IwfMiIsVlanMemberPort (u4ContextId,
                                                 (tVlanId) pPerStPortInfo->
                                                 u2Inst,
                                                 AST_GET_IFINDEX
                                                 (pPerStPortInfo->u2PortNo)) ==
                        OSIX_TRUE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AstLoopIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bLoopIncStatus, u2InstIndex,
                                                   (INT1 *) AST_PVRST_TRAPS_OID,
                                                   PVRST_BRG_TRAPS_OID_LEN);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature blocking Port: %s for Vlan: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          pPerStPortInfo->u2Inst, au1TimeStr);
                    }
                    if (PvrstPortStateTrMachine
                        (PVRST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                         pPerStPortInfo) != PVRST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return PVRST_FAILURE;
                    }
                }
                else
                {
                    if (PvrstProleTrSmMakeForward (pPerStPortInfo) !=
                        PVRST_SUCCESS)
                    {
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %u: MakeForward "
                                      "function returned FAILURE\n",
                                      pPerStPortInfo->u2PortNo);
                        return PVRST_FAILURE;
                    }
                }
            }
        }
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstHandleStpCompatabilityCh                        */
/*                                                                           */
/* Description        : This routine handles the change in compatability     */
/*         to STP.                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*         i4FunctionNo - Appropriate state machine function    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS / PVRST_FAILURE                        */
/*****************************************************************************/
INT4
PvrstHandleStpCompatabilityCh (INT4 i4FunctionNo,
                               tAstPerStPortInfo * pPerStPortInfo)
{
    tPerStPvrstRstPortInfo *pPerStPvrstRstPortInfo = NULL;
    tVlanId             VlanId = pPerStPortInfo->u2Inst;
    UINT2               u2InstIndex = AST_INIT_VAL;

    u2InstIndex = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);
    if (u2InstIndex == INVALID_INDEX)
    {
        return PVRST_FAILURE;
    }
    if (PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                   u2InstIndex) == NULL)
    {
        return PVRST_FAILURE;
    }

    pPerStPvrstRstPortInfo =
        PVRST_GET_PERST_PVRST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                             u2InstIndex);
    if (pPerStPvrstRstPortInfo == NULL)
    {
        return PVRST_FAILURE;
    }
    if ((pPerStPortInfo->u2Inst == AST_DEF_VLAN_ID ())
        && ((AST_FORCE_VERSION == AST_VERSION_0)
            || (pPerStPvrstRstPortInfo->bSendRstp == PVRST_FALSE)))
    {
        PvrstCopyStpParametersToAllInst (i4FunctionNo, pPerStPortInfo);
    }
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstCopyStpParametersToAllInst                      */
/*                                                                           */
/* Description        : When there is a change in compatability to STP       */
/*                      This routine copies all the prameter of default      */
/*             instance to all other instances.                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      i4FunctionNo - Appropriate state machine function    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstCopyStpParametersToAllInst (INT4 i4FunctionNo,
                                 tAstPerStPortInfo * pPerStPortInfo)
{
    UINT2               u2TmpPvrstInst = AST_INIT_VAL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pInfo = NULL;
    UINT1               u1DefaultRole = 0;
    pInfo = PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo, 1);
    u1DefaultRole = pInfo->u1SelectedPortRole;

    switch (i4FunctionNo)
    {

        case PVRST_UPDATE_INFO_RESET:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmUpdtInfoReset (pInfo);
            }
            break;
        case PVRST_REROOT_SET_ROOTPORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;

                PvrstProleTrSmRerootSetRootPort (pInfo);
            }

            break;
        case PVRST_RE_ROOT_SET_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;

                PvrstProleTrSmRerootSetDesgPort (pInfo);
            }
            break;
        case PVRST_SYNC_SET_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmSyncSetDesgPort (pInfo);
            }
            break;
        case PVRST_FWD_EXP_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmFwdExpRootPort (pInfo);
            }
            break;
        case PVRST_FWD_EXP_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;

                PvrstProleTrSmFwdExpDesgPort (pInfo);
            }
            break;
        case PVRST_WHILE_EXP_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmRrWhileExpDesgPort (pInfo);
            }
            break;
        case PVRST_PROPOSED_SET_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmProposedSetRootPort (pInfo);
            }
            break;
        case PVRST_AGREED_SET_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;

                PvrstProleTrSmAgreedSetDesgPort (pInfo);
            }
            break;
        case PVRST_WHILE_EXP_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmRbWhileExpRootPort (pInfo);
            }
            break;
#ifdef IEEE8021Y_Z12_WANTED
        case PVRST_DISPUTED_SET_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmDisputedSetDesgPort (pInfo);
            }
            break;
#endif /* IEEE8021Y_Z12_WANTED */
        case PVRST_IS_RE_ROOTED:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmIsReRooted (pInfo->u2PortNo, u2TmpPvrstInst)
                    != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: PvrstProleTrSmIsReRooted "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_ROLE_CHANGED:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmRoleChanged (pInfo);
            }
            break;
        case PVRST_MAKE_INIT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmMakeInitPort (pInfo);
            }
            break;
        case PVRST_MAKE_BLOCK_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmMakeBlockPort (pInfo);
            }
            break;
        case PVRST_MAKE_BLOCKED_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmMakeBlockedPort (pInfo);
            }
            break;
        case PVRST_MAKE_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);

                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmMakeRootPort (pInfo);
            }
            break;
        case PVRST_IS_ALL_OTHER_PORTS_SYNCED:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstIsAllOtherPortsSynced (pInfo->u2PortNo, u2TmpPvrstInst)
                    != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: AllOtherPortsSynced "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_ALL_SYNCED_SET_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmAllSyncedSetRootPort (pInfo);
            }
            break;
        case PVRST_REROOTED_SET_ROOT_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmRerootedSetRootPort (pInfo);
            }
            break;
        case PVRST_MAKE_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmMakeDesgPort (pInfo);
            }
            break;
        case PVRST_MAKE_FORWARD:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmMakeForward (pInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeForward "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_MAKE_LEARN:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }

                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmMakeLearn (pInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeLearn "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_MAKE_DESIGNATED_LISTEN:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmMakeDesignatedListen (pInfo) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: MakeDesignatedListen function"
                                  " returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_SET_SYNC_BRIDGE:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmSetSyncBridge
                    (pInfo->u2PortNo, u2TmpPvrstInst) != PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: SetSyncBridge returned "
                                  "FAILURE !!! \n", pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_SET_REROOT_BRIDGE:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                if (PvrstProleTrSmSetReRootBridge (u2TmpPvrstInst) !=
                    PVRST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %u: ReRootBridge "
                                  "function returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                }
            }
            break;
        case PVRST_OPER_EDGE_SET_DESG_PORT:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmOperEdgeSetDesgPort (pInfo);
            }
            break;
        case PVRST_LEARNING_FORWARDING_RESET:
            for (u2TmpPvrstInst = 2; u2TmpPvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2TmpPvrstInst++)
            {
                pPerStInfo = PVRST_GET_PERST_INFO (u2TmpPvrstInst);
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pInfo =
                    PVRST_GET_PERST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                               u2TmpPvrstInst);
                if (pInfo == NULL)
                {
                    continue;
                }
                pInfo->u1SelectedPortRole = u1DefaultRole;
                PvrstProleTrSmLearningFwdingReset (pInfo);
            }
            break;
        default:
            return;
    }
}

/*****************************************************************************/
/* Function Name      : PvrstCopyStpParametersToNonDefInst                   */
/*                                                                           */
/* Description        : When there is a change in compatability to STP       */
/*                      This routine copies all the prameter of default      */
/*                      instance to all non default instances.               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortNo - port no on which STP PDU is being recieved*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstCopyStpParametersToNonDefInst (UINT2 u2PortNo)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    pPerStPortInfo = PVRST_GET_PERST_PORT_INFO (u2PortNo, AST_DEF_VLAN_ID ());
    if (pPerStPortInfo == NULL)
    {
        return;
    }
    if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT)
    {
        PvrstCopyStpParametersToAllInst (PVRST_MAKE_ROOT_PORT, pPerStPortInfo);
    }
    else if (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
    {
        PvrstCopyStpParametersToAllInst (PVRST_MAKE_DESG_PORT, pPerStPortInfo);
    }
    else
    {
        PvrstCopyStpParametersToAllInst (PVRST_MAKE_BLOCK_PORT, pPerStPortInfo);
        PvrstCopyStpParametersToAllInst (PVRST_MAKE_BLOCKED_PORT,
                                         pPerStPortInfo);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PvrstInitPortRoleTrMachine                           */
/*                                                                           */
/* Description        : This function is called during initialisation to     */
/*                      load the function pointers in the State-Event Machine*/
/*                      array.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.aaPortRoleTrMachine                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gAstGlobalInfo.aaPortRoleTrMachine                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PvrstInitPortRoleTrMachine (VOID)
{

/*#if defined(TRACE_WANTED) || defined(RST_DEBUG)*/
    INT4                i4Index = 0;
#ifdef IEEE8021Y_Z12_WANTED
    UINT1               aau1SemEvent[PVRST_PROLETRSM_MAX_EVENTS][20] = {
        "BEGIN", "FDWHILE_EXP", "RRWHILE_EXP",
        "RBWHILE_EXP", "SYNC_SET", "REROOT_SET", "SELECTED_SET",
        "PROPOSED_SET", "AGREED_SET", "ALLSYNCED_SET", "REROOTED_SET",
        "OPEREDGE_SET", "DISPUTED_SET"
    };
    UINT1               aau1SemState[PVRST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "BLOCK_PORT",
        "BLOCKED_PORT", "ROOT_PORT", "DESG_PORT"
    };
#else /* IEEE8021Y_Z12_WANTED */
    UINT1               aau1SemEvent[PVRST_PROLETRSM_MAX_EVENTS][20] = {
        "BEGIN", "FDWHILE_EXP", "RRWHILE_EXP",
        "RBWHILE_EXP", "SYNC_SET", "REROOT_SET", "SELECTED_SET",
        "PROPOSED_SET", "AGREED_SET", "ALLSYNCED_SET", "REROOTED_SET",
        "OPEREDGE_SET"
    };
    UINT1               aau1SemState[PVRST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "BLOCK_PORT",
        "BLOCKED_PORT", "ROOT_PORT", "DESG_PORT"
    };
#endif /* IEEE8021Y_Z12_WANTED */

    for (i4Index = 0; i4Index < PVRST_PROLETRSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_RTSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_RTSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < PVRST_PROLETRSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_RTSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_RTSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }
    /*#endif *//* PVRST_TRACE_WANTED || PVRST_DEBUG */

    /* BEGIN Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_BEGIN]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = PvrstProleTrSmMakeInitPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_BEGIN]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = PvrstProleTrSmMakeInitPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_BEGIN]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction =
        PvrstProleTrSmMakeInitPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_BEGIN]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = PvrstProleTrSmMakeInitPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_BEGIN]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = PvrstProleTrSmMakeInitPort;

    /* FDWHILE_EXP Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_FDWHILE_EXP]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_FDWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_FDWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_FDWHILE_EXP]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmFwdExpRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_FDWHILE_EXP]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmFwdExpDesgPort;

    /* RRWHILE_EXP Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RRWHILE_EXP]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RRWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RRWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RRWHILE_EXP]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RRWHILE_EXP]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmRrWhileExpDesgPort;

    /* RBWHILE_EXP Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RBWHILE_EXP]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RBWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RBWHILE_EXP]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RBWHILE_EXP]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmRbWhileExpRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_RBWHILE_EXP]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* SYNC_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SYNC_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SYNC_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SYNC_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction =
        PvrstProleTrSmMakeBlockedPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SYNC_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SYNC_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmSyncSetDesgPort;

    /* REROOT_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOT_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOT_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOT_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction =
        PvrstProleTrSmMakeBlockedPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOT_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmRerootSetRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOT_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmRerootSetDesgPort;

    /* SELECTED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SELECTED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = PvrstProleTrSmUpdtInfoReset;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SELECTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction =
        PvrstProleTrSmUpdtInfoReset;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SELECTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction =
        PvrstProleTrSmUpdtInfoReset;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SELECTED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = PvrstProleTrSmUpdtInfoReset;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_SELECTED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = PvrstProleTrSmUpdtInfoReset;

    /* PROPOSED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_PROPOSED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_PROPOSED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_PROPOSED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_PROPOSED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmProposedSetRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_PROPOSED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* AGREED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_AGREED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_AGREED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_AGREED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_AGREED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_AGREED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmAgreedSetDesgPort;

    /* ALLSYNCED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_ALLSYNCED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_ALLSYNCED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_ALLSYNCED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_ALLSYNCED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmAllSyncedSetRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_ALLSYNCED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* REROOTED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOTED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOTED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction =
        PvrstProleTrSmRerootedSetRootPort;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_REROOTED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction = NULL;

    /* OPEREDGE_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_OPEREDGE_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_OPEREDGE_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_OPEREDGE_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_OPEREDGE_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_OPEREDGE_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmOperEdgeSetDesgPort;

    /* Changes for IEEE P802.1D/D1 - 2003 Edition and IEEE DRAFT P802.1y/D4 
     * Annex Z.12 */
#ifdef IEEE8021Y_Z12_WANTED
    /* DISPUTED_SET Event */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_DISPUTED_SET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_DISPUTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_DISPUTED_SET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_DISPUTED_SET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_DISPUTED_SET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmDisputedSetDesgPort;
#endif /* IEEE8021Y_Z12_WANTED */

    /* PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET */
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [PVRST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [PVRST_PROLETRSM_STATE_BLOCK_PORT].pAction =
        PvrstProleTrSmLearningFwdingReset;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [PVRST_PROLETRSM_STATE_BLOCKED_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [PVRST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    PVRST_PORT_ROLE_TR_MACHINE[PVRST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [PVRST_PROLETRSM_STATE_DESG_PORT].pAction =
        PvrstProleTrSmLearningFwdingReset;

    AST_DBG (AST_RTSM_DBG, "RTSM: Loaded RTSM SEM successfully\n");

    return;
}
