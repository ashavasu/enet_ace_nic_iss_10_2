#####################################################################
#### Copyright (C) Future Software, 2001-2002                    ####
#### Licensee Future Communications Software, 2001-2002          ####
#####################################################################
##|                                                               |##
##|###############################################################|##
##|                                                               |##
##|    FILE NAME               ::  make.h                         |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Future Software Pvt. Ltd.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  PVRST                          |##
##|                                                               |##
##|    MODULE NAME             ::  PVRST                          |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  ANY                            |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  27 Feb 2007                    |##
##|                                                               |##
##|    DESCRIPTION             ::  Include file for the PVRST     |##
##|                                Makefile.                      |## 
##|                                                               |##
##|###############################################################|##

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

PVRST_SWITCHES = -Di386 -DRSTP_DEBUG -DPVRSTP_TRAP_WANTED 


TOTAL_OPNS = ${PVRST_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

PVRST_BASE_DIR    = ${BASE_DIR}/stp/pvrst
RST_BASE_DIR    = ${BASE_DIR}/stp/rstp
MST_BASE_DIR    = ${BASE_DIR}/stp/mstp
PVRST_SRC_DIR     = ${PVRST_BASE_DIR}/src
PVRST_INC_DIR     = ${PVRST_BASE_DIR}/inc
PVRST_OBJ_DIR     = ${PVRST_BASE_DIR}/obj
RST_INC_DIR     = ${RST_BASE_DIR}/inc
MST_INC_DIR     = ${MST_BASE_DIR}/inc
CFA_INCD        = ${CFA_BASE_DIR}/inc

VLAN_INCL_DIR = ${BASE_DIR}/vlangarp/vlan/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${PVRST_INC_DIR} -I${VLAN_INCL_DIR} -I${RST_INC_DIR} -I${MST_INC_DIR} 

INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
