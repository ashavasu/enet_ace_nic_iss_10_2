/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmhwwr.c,v 1.5 2015/07/27 07:01:10 siva Exp $
 *
 * Description: This file contains all NPAPI wrapper used by MSTP     
 *              module.
 *
 *******************************************************************/

#ifndef __ASTMHWWR_C__
#define __ASTMHWWR_C__

#include "astminc.h"
/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpWrSetInstancePortState                      */
/*                                                                       */
/* DESCRIPTION   : Sets the MSTP Port State in the Hardware for given    */
/*                 INSTANCE. When MSTP is operating in MSTP mode or MSTP */
/*                 in RSTP compatible mode or MSTP in STP compatible mode*/
/*                 Calls the NPAPI, if Spanning tree is port state       */
/*                 controller for this port.                             */
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                 u4IfIndex   - Interface index of whose STP state      */
/*                 is to be  updated                                     */
/*                 u2InstanceId- Instance ID ranges <1-64>.              */
/*                 u1PortState - Value of Port State can take any of the */
/*                 following                                             */
/*                 AST_PORT_STATE_FORWARDING,                            */
/*                 AST_PORT_STATE_LEARNING,                              */
/*                 AST_PORT_STATE_DISCARDING,                            */
/*                 AST_PORT_STATE_DISABLED                               */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during setting                    */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*                                                                       */
/*************************************************************************/
INT4
FsMiMstpNpWrSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2InstanceId, UINT1 u1PortState)
{
    INT4                i4RetVal = FNP_FAILURE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT4               u4CtxtId = AST_INIT_VAL;
    UINT1               u1ProtocolId = 0;

    if (L2IwfGetPortStateCtrlOwner (u4IfIndex, &u1ProtocolId,OSIX_TRUE) != L2IWF_SUCCESS)
    {
        return i4RetVal;
    }

    if (u1ProtocolId != STP_MODULE)
    {
        return FNP_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4CtxtId, &u2PortNum) ==
        MST_SUCCESS)
    {
        if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == MST_TRUE)
        {
            AST_DBG (AST_STSM_DBG,
                     "CEP Port state cannot be configured in case of MSTP SVLAN context!!!\n");
            return FNP_SUCCESS;
        }
    }

    i4RetVal = MstpFsMiMstpNpSetInstancePortState (u4ContextId, u4IfIndex,
                                                   u2InstanceId, u1PortState);

    return i4RetVal;
}

#endif /*__ASTMHWWR_C__*/
