/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmsnmp.c,v 1.78 2018/02/01 11:02:42 siva Exp $
 *
 * Description: This file contains MST routines which are called  
 *              by low level routines.            
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetMaxInstance                          */
/*                                                                           */
/*    Description               : This function sets the Maximum Instance.   */
/*                                                                           */
/*    Input(s)                  : u2MaxInstance - Maximum Instance be set.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetMaxInstance (UINT2 u2MaxInstance)
#else
INT4
MstSetMaxInstance (u2MaxInstance)
     UINT2               u2MaxInstance;
#endif
{
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    pAstMstBridgeEntry->u2MaxMstInstances = u2MaxInstance;

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetMaxHopCount                          */
/*                                                                           */
/*    Description               : This function sets the Maximum Hop count.  */
/*                                                                           */
/*    Input(s)                  : u1MaxHopCount - Maximum Hop Count be set.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetMaxHopCount (UINT1 u1MaxHopCount)
#else
INT4
MstSetMaxHopCount (u1MaxHopCount)
     UINT1               u1MaxHopCount;
#endif
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2Inst = AST_INIT_VAL;
    tAstPortEntry      *pPortEntry = NULL;

    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    if (pAstMstBridgeEntry->u1MaxHopCount == u1MaxHopCount)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in MaxHopCount value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in MaxHopCount value\n");
        return MST_SUCCESS;
    }
    pAstMstBridgeEntry->u1MaxHopCount = u1MaxHopCount;

    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2Inst);
        pPerStBrgInfo->u1BrgRemainingHops = pAstMstBridgeEntry->u1MaxHopCount;

        /* Updating the Root Times, Desg Times and Port Times if this Bridge 
         * is the Root Bridge */
        if (AST_IS_MST_ENABLED ())
        {
            if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
            {
                /* This Bridge is the Root Bridge for this Instance */

                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                              "SNMP: Inst %u: This bridge is ROOT "
                              "Setting Root, Desg and Port Remaining Hops as :%u...\n",
                              u2Inst, u1MaxHopCount);
                AST_DBG_ARG2 (AST_MGMT_DBG,
                              "SNMP: Inst %u: This bridge is ROOT "
                              "Setting Root, Desg and Port Remaining Hops as :%u...\n",
                              u2Inst, u1MaxHopCount);

                pPerStBrgInfo->u1RootRemainingHops =
                    pPerStBrgInfo->u1BrgRemainingHops;

                u2PortNum = 1;
                AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
                {
                    if (pPortEntry == NULL)
                    {
                        continue;
                    }
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                    if (pPerStPortInfo != NULL)
                    {
                        pPerStPortInfo->PerStCistMstiCommPortInfo.
                            u1DesgRemainingHops =
                            pPerStPortInfo->PerStCistMstiCommPortInfo.
                            u1PortRemainingHops =
                            pPerStBrgInfo->u1RootRemainingHops;
                    }
                }                /* End of For Loop for Ports */
            }                    /* End of this Bridge being the Root Bridge */
        }
    }                            /* End of For Loop for Instances */
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetConfigId                             */
/*                                                                           */
/*    Description               : This function sets the Configuration       */
/*                                Identifier value.                          */
/*                                                                           */
/*    Input(s)                  : u1ConfigId - Value of the Configuration    */
/*                                             to be set.                    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetConfigId (UINT1 u1ConfigId)
#else
INT4
MstSetConfigId (u1ConfigId)
     UINT1               u1ConfigId;
#endif
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    if (pMstConfigIdInfo->u1ConfigurationId == u1ConfigId)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Configuration ID\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Configuration ID\n");
        return MST_SUCCESS;
    }
    pMstConfigIdInfo->u1ConfigurationId = u1ConfigId;

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetRegionName                           */
/*                                                                           */
/*    Description               : This function sets the Region name for     */
/*                                an MST Region.                             */
/*                                                                           */
/*    Input(s)                  : au1ConfigName - Name of the MST Region     */
/*                                                to be set.                 */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetRegionName (UINT1 *pu1ConfigName, UINT2 u2NameLen)
#else
INT4
MstSetRegionName (pu1ConfigName, u2NameLen)
     UINT1              *pu1ConfigName;
     UINT2               u2NameLen;
#endif
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AST_MEMSET (pMstConfigIdInfo->au1ConfigName, AST_INIT_VAL,
                MST_CONFIG_NAME_LEN);

    AST_MEMCPY (pMstConfigIdInfo->au1ConfigName, pu1ConfigName, u2NameLen);

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (AstAssertBegin () == MST_FAILURE)
    {
        AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetRegionVersion                        */
/*                                                                           */
/*    Description               : This function sets the Region Version for  */
/*                                an MST Region.                             */
/*                                                                           */
/*    Input(s)                  : au1ConfigName - Name of the MST Region     */
/*                                                to be set.                 */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetRegionVersion (UINT2 u2RegionVersion)
#else
INT4
MstSetRegionVersion (u2RegionVersion)
     UINT2               u2RegionVersion;
#endif
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    if (pMstConfigIdInfo->u2ConfigRevLevel == u2RegionVersion)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Region Version\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Region Version\n");
        return MST_SUCCESS;
    }
    pMstConfigIdInfo->u2ConfigRevLevel = u2RegionVersion;

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (AstAssertBegin () == MST_FAILURE)
    {
        AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetBridgePriority                       */
/*                                                                           */
/*    Description               : This function sets the Priority of the     */
/*                                Bridge for specific instance.              */
/*                                                                           */
/*    Input(s)                  : u2InstanceId - Instance Identifier for     */
/*                                               which the Priority is going */
/*                                               to be set.                  */
/*                                u2Priority  - Priority value of the Bridge.*/
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetBridgePriority (UINT2 u2InstanceId, UINT2 u2Priority)
#else
INT4
MstSetBridgePriority (u2InstanceId, u2Priority)
     UINT2               u2InstanceId;
     UINT2               u2Priority;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2PortNum = 0;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    if (u2InstanceId == AST_TE_MSTID)
    {
        return MST_SUCCESS;
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return MST_FAILURE;
    }
    if (u2InstanceId == MST_CIST_CONTEXT)
    {

        pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);

        if (pAstPerStBridgeInfo->u2BrgPriority == u2Priority)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                     "SNMP: No change in Bridge Priority\n");
            AST_DBG (AST_MGMT_DBG, "SNMP: No change in Bridge Priority\n");
            return MST_SUCCESS;
        }
        pAstPerStBridgeInfo->u2BrgPriority = u2Priority;

        pBrgInfo = AST_GET_BRGENTRY ();
        if (AST_MEMCMP
            (&(pBrgInfo->MstBridgeEntry.RegionalRootId.BridgeAddr),
             pBrgInfo->BridgeAddr, sizeof (tAstMacAddr)) == 0)
        {

            pBrgInfo->MstBridgeEntry.RegionalRootId.u2BrgPriority = u2Priority;
        }

        for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
        {

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      MST_CIST_CONTEXT);

            if (pPerStPortInfo != NULL)
            {

                pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                pPerStRstPortInfo->bSelected = RST_FALSE;
                MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);
                pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                if (pPortInfo != NULL)
                {
                    pPortInfo->bAllTransmitReady = MST_FALSE;
                }
                pPerStRstPortInfo->bReSelect = RST_TRUE;
                if (pPerStPortInfo->bIsPseudoRootIdConfigured == RST_FALSE)
                {
                    pPerStPortInfo->PseudoRootId.u2BrgPriority = u2Priority;
                }
            }
        }                        /* End of For Loop */
    }                            /* End of If loop */

    else
    {
        pAstPerStBridgeInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);

        if (pAstPerStBridgeInfo->u2BrgPriority == u2Priority)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                     "SNMP: No change in Bridge Priority\n");
            AST_DBG (AST_MGMT_DBG, "SNMP: No change in Bridge Priority\n");
            return MST_SUCCESS;
        }
        MST_SET_BRIDGE_PRIORITY (u2Priority,
                                 pAstPerStBridgeInfo->u2BrgPriority);

        if (AST_IS_MST_ENABLED ())
        {
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                pPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

                if (pPerStPortInfo != NULL)
                {

                    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                    pPerStRstPortInfo->bSelected = RST_FALSE;
                    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);

                    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                    if (pPortInfo != NULL)
                    {
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                    }
                    pPerStRstPortInfo->bReSelect = RST_TRUE;
                    if (pPerStPortInfo->bIsPseudoRootIdConfigured == RST_FALSE)
                    {
                        pPerStPortInfo->PseudoRootId.u2BrgPriority = u2Priority;
                    }
                }
            }                    /* End of For Loop */
        }                        /* End of For Loop */
    }                            /* End of If Loop */

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Inst %d: Bridge Priority Set as :%u\n",
                  u2InstanceId, u2Priority);
    if (AST_IS_MST_ENABLED ())
    {
        AST_DBG_ARG1 (AST_MGMT_DBG,
                      "SNMP:Inst %d: ...Recomputing roles..\n", u2InstanceId);
        if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                         u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RstPortRoleSelectionMachine function returned FAILURE!\n");
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetDynamicPathcostCalc                  */
/*                                                                           */
/*    Description               : This function is the event handler whenever*/
/*                                dynamic pathcost calculation is set or     */
/*                                reset by  management and the               */
/*                                corresponding event is generated.          */
/*                                                                           */
/*    Input(s)                  : u1DynamicPathcostCalc - RST_FALSE/RST_TRUE */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetDynamicPathcostCalc (UINT1 u1DynamicPathcostCalc)
#else
INT4
MstSetDynamicPathcostCalc (u1DynamicPathcostCalc)
     UINT1               u1DynamicPathcostCalc;
#endif
{
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    UINT1               au1ChangeFlag[AST_MAX_MST_INSTANCES];
    /* Flag used to indicate role selection 
       is needed for each instance or not */
    UINT4               u4PathCost = 0;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == u1DynamicPathcostCalc)
    {
        return (INT1) MST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalculation = u1DynamicPathcostCalc;

    MEMSET (au1ChangeFlag, 0, AST_MAX_MST_INSTANCES);

    if (pAstBridgeEntry->u1DynamicPathcostCalculation == MST_TRUE)
    {
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
        {
            if (pPortEntry == NULL)
            {
                continue;
            }

            pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
            u2Inst = AST_INIT_VAL;
            AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                u4PathCost = pPerStPortInfo->u4PortPathCost;

                if (AstPathcostConfiguredFlag (u2PortNum, u2Inst) == RST_FALSE)
                {
                    pPerStPortInfo->u4PortPathCost =
                        AstCalculatePathcost (u2PortNum);
                }

                if (u4PathCost != pPerStPortInfo->u4PortPathCost)
                {
                    if (AST_IS_MST_ENABLED ())
                    {
                        au1ChangeFlag[u2Inst] = MST_TRUE;
                        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                        pPerStRstPortInfo->bSelected = MST_FALSE;
                        MstPUpdtAllSyncedFlag (u2Inst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPerStRstPortInfo->bReSelect = MST_TRUE;
                        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                        if (pPortInfo != NULL)
                        {
                            pPortInfo->bAllTransmitReady = MST_FALSE;
                        }
                    }
                }
            }
        }
        if (AST_IS_MST_ENABLED ())
        {
            /* Trigger RoleSelection once for each instance */
            u2Inst = 0;
            AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
            {
                if (au1ChangeFlag[u2Inst] == MST_TRUE)
                {
                    if (pPerStInfo == NULL)
                    {
                        continue;
                    }
                    if (RstPortRoleSelectionMachine
                        ((UINT1) RST_PROLESELSM_EV_RESELECT, u2Inst)
                        != RST_SUCCESS)
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                                 AST_MGMT_TRC,
                                 "SNMP: RoleSelectionMachine returned FAILURE!\n");
                        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                                 "SNMP: RoleSelectionMachine returned FAILURE!\n");
                        return MST_FAILURE;
                    }
                }
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetDynaPathcostCalcLagg                 */
/*                                                                           */
/*    Description               : This function is the event handler whenever*/
/*                                dynamic pathcost calculation is set or     */
/*                                reset by  management and the               */
/*                                corresponding event is generated. It will  */
/*                                take care to recalculate pathcost for Link */
/*                                Aggregated Port. In case pathcost          */
/*                                calculated is different from the existing  */
/*                                value then  Spnnaing Tree to which lagg    */
/*                                belong will be triggered to re-converged   */
/*                                                                           */
/*    Input(s)                  : u1DynamicPathcostCalcLagg -                */
/*                                       RST_FALSE/RST_TRUE                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetDynaPathcostCalcLagg (UINT1 u1DynamicPathcostCalcLagg)
#else
INT4
MstSetDynamicPathcostCalclagg (u1DynamicPathcostCalcLagg)
     UINT1               u1DynamicPathcostCalcLagg;
#endif
{
    tAstCfaIfInfo       CfaIfInfo;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT4               u4PathCost = 0;
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    /* Flag used to indicate role selection 
       is needed for each instance or not */
    UINT1               au1ChangeFlag[AST_MAX_MST_INSTANCES];

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    if (pAstBridgeEntry->u1DynamicPathcostCalcLagg == u1DynamicPathcostCalcLagg)
    {
        return (INT1) MST_SUCCESS;
    }

    pAstBridgeEntry->u1DynamicPathcostCalcLagg = u1DynamicPathcostCalcLagg;

    MEMSET (au1ChangeFlag, 0, AST_MAX_MST_INSTANCES);

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortEntry)
    {
        if (pPortEntry == NULL)
        {
            continue;
        }

        if (AstCfaGetIfInfo (AST_IFENTRY_IFINDEX (pPortEntry),
                             &CfaIfInfo) == CFA_FAILURE)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                          AST_MGMT_TRC,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Getting Cfa Information for Port %s returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return RST_FAILURE;
        }

        if (CfaIfInfo.u1IfType != CFA_LAGG)
        {
            continue;
        }
        pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);

        u2Inst = 0;
        AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            u4PathCost = pPerStPortInfo->u4PortPathCost;

            if (AstPathcostConfiguredFlag (u2PortNum, u2Inst) == RST_FALSE)
            {
                pPerStPortInfo->u4PortPathCost = AstCalculatePathcost
                    (u2PortNum);
                if (u4PathCost != pPerStPortInfo->u4PortPathCost)
                {
                    if (AST_IS_MST_ENABLED ())
                    {
                        au1ChangeFlag[u2Inst] = MST_TRUE;
                        pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                        pPerStRstPortInfo->bSelected = MST_FALSE;
                        MstPUpdtAllSyncedFlag (u2Inst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPerStRstPortInfo->bReSelect = MST_TRUE;
                        pPortEntry->bAllTransmitReady = MST_FALSE;
                    }
                }
            }
        }
    }
    if (AST_IS_MST_ENABLED ())
    {
        /* Trigger RoleSelection once for each instance */
        u2Inst = 0;
        AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
        {
            if (au1ChangeFlag[u2Inst] == MST_TRUE)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                if (RstPortRoleSelectionMachine
                    ((UINT1) RST_PROLESELSM_EV_RESELECT, u2Inst) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    return MST_FAILURE;
                }
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortPathCost                         */
/*                                                                           */
/*    Description               : This function sets the Port path cost      */
/*                                                                           */
/*    Input(s)                  : u2PortNo - The Port Number for which cost  */
/*                                           is set.                         */
/*                                u2InstanceId  - Instance Identifier        */
/*                                u4PathCost    - Path cost value to be set  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetPortPathCost (UINT2 u2PortNo, UINT2 u2InstanceId, UINT4 u4PathCost)
#else
INT4
MstSetPortPathCost (u2PortNo, u2InstanceId, u4PathCost)
     UINT2               u2PortNo;
     UINT2               u2InstanceId;
     UINT4               u4PathCost;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4RetVal = MST_SUCCESS;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);

    /* Incase of customer edge port, update the port path cost
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        i4RetVal = AstPbSetPortPathCostInCvlanComp (pAstPortEntry, u4PathCost);

        return i4RetVal;
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return MST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return MST_FAILURE;
    }
    if ((pPerStPortInfo->u4PortPathCost == u4PathCost) &&
        ((pPerStPortInfo->u4PortAdminPathCost == u4PathCost) ||
         (pPerStPortInfo->u4PortAdminPathCost == 0)))

    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Port PathCost\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port PathCost\n");
        return MST_SUCCESS;
    }
    MstUpdatePortPathCost (u2PortNo, u2InstanceId, u4PathCost);

    AST_DBG_ARG3 (AST_MGMT_DBG,
                  "SNMP: Port %s: Inst %d: Pathcost Set as :%u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId, u4PathCost);

    if (AST_IS_MST_ENABLED ())
    {
        /* Trigger Role selection sem */
        i4RetVal = AstTriggerRoleSeletionMachine (u2PortNo, u2InstanceId);
    }

    if (i4RetVal == RST_FAILURE)
    {
        return MST_FAILURE;
    }

    return MST_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetZeroPortPathCost                     */
/*                                                                           */
/*    Description               : This function sets the Port path cost      */
/*                                                                           */
/*    Input(s)                  : u2PortNo - The Port Number for which cost  */
/*                                           is set.                         */
/*                                u2InstanceId  - Instance Identifier        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/

#ifdef __STDC__
INT4
MstSetZeroPortPathCost (UINT2 u2PortNo, UINT2 u2InstanceId)
#else
INT4
MstSetZeroPortPathCost (UINT2 u2PortNo, UINT2 u2InstanceId)
     UINT2               u2PortNo;
     UINT2               u2InstanceId;
#endif

{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    INT4                i4RetVal = MST_SUCCESS;
    UINT4               u4PathCost = AST_INIT_VAL;
    UINT1               u1ChangeFlag = AST_INIT_VAL;

    /* Flag used to indicate role selection
     *        is needed for each instance or not */

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return MST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        return MST_FAILURE;
    }
    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        return MST_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);

    /* Incase of customer edge port, update the port path cost
     * in the C-VLAN component CEP. */
    if ((pAstPortEntry != NULL)
        && (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT))
    {
        i4RetVal = AstPbSetPortPathCostInCvlanComp (pAstPortEntry, u4PathCost);
        return i4RetVal;
    }
    else
    {

        u4PathCost = AstCalculatePathcost (u2PortNo);
        AstSetPortPathcostConfigured (u2PortNo, u2InstanceId, 0);

        if (u4PathCost != pPerStPortInfo->u4PortPathCost)
        {
            pPerStPortInfo->u4PortPathCost = u4PathCost;

            if (AST_IS_MST_ENABLED ())
            {
                u1ChangeFlag = MST_TRUE;
                pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                pPerStRstPortInfo->bSelected = MST_FALSE;
                MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);
                pPerStRstPortInfo->bReSelect = MST_TRUE;
                if (pAstPortEntry != NULL)
                {
                    pAstPortEntry->bAllTransmitReady = MST_FALSE;
                }
            }
        }

        if (AST_IS_MST_ENABLED ())
        {
            if (u1ChangeFlag == MST_TRUE)
            {
                pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
                if (pPerStInfo == NULL)
                {
                    return MST_FAILURE;
                }
                if (RstPortRoleSelectionMachine
                    ((UINT1) RST_PROLESELSM_EV_RESELECT,
                     u2InstanceId) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    return SNMP_FAILURE;
                }
            }
        }

        return i4RetVal;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstUpdatePortPathCost                              */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port path cost for the      */
/*                        given port and for the given mst instance and also */
/*                        sets the port path cost configured parameter true. */
/*                                                                           */
/*     INPUT            : u4PortNum - PortNumber                             */
/*                        u4PortPathCost - Path cost to be set.              */
/*                        u2InstId   - Mst Instance Id.                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : MST_SUCCESS/MST_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdatePortPathCost (UINT2 u2PortNum, UINT2 u2InstId, UINT4 u4PortPathCost)
{

    INT4                i4RetVal = MST_SUCCESS;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstId);

    pPerStPortInfo->u4PortPathCost = u4PortPathCost;

    i4RetVal =
        AstSetPortPathcostConfigured (u2PortNum, u2InstId, u4PortPathCost);

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4PortPathCost);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Path Cost Set as :%u...Recomputing roles..\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u4PortPathCost);

    if (i4RetVal == RST_FAILURE)
    {
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortPriority                         */
/*                                                                           */
/*    Description               : This function sets the Priority of the     */
/*                                Port.                                      */
/*                                                                           */
/*    Input(s)                  : u2PortNo     - Port Number of the Port.    */
/*                                u2InstanceId - Instance Identifier         */
/*                                u1Priority   - Priority value of the Port. */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetPortPriority (UINT2 u2InstanceId, UINT2 u2PortNo, UINT1 u1Priority)
#else
INT4
MstSetPortPriority (u2InstanceId, u2PortNo, u1Priority)
     UINT2               u2PortNo;
     UINT2               u2InstanceId;
     UINT1               u1Priority;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        return MST_FAILURE;
    }

    AST_DBG_ARG3 (AST_MGMT_DBG,
                  "SNMP: Port %s: Inst %d: Setting port priority as :%u ...\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId, u1Priority);

    pPortInfo = AST_GET_PORTENTRY (u2PortNo);
    if (u2InstanceId == MST_CIST_CONTEXT)
    {

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, MST_CIST_CONTEXT);

        if (pPerStPortInfo->u1PortPriority == u1Priority)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                     "SNMP: No change in Port Priority\n");
            AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port Priority\n");
            return MST_SUCCESS;
        }
        pPerStPortInfo->u1PortPriority = u1Priority;
        if (AST_IS_MST_ENABLED ())
        {
            AST_DBG_ARG1 (AST_MGMT_DBG,
                          "SNMP:Inst %d: ...Recomputing roles..\n",
                          u2InstanceId);
            pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;
            pPortInfo->bAllTransmitReady = MST_FALSE;

            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             MST_CIST_CONTEXT) != RST_SUCCESS)
            {

                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RstPortRoleSelectionMachine function returned "
                         "FAILURE!\n");
                return MST_FAILURE;
            }
        }
    }
    else
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, u2InstanceId);
        if (pPerStPortInfo == NULL)
        {
            AST_DBG_ARG2 (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Port %s: Inst %d: Port does not exist in this "
                          "instance context!\n",
                          AST_GET_IFINDEX_STR (u2PortNo), u2InstanceId);
            return MST_FAILURE;
        }

        if (pPerStPortInfo->u1PortPriority == u1Priority)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                     "SNMP: No change in Port Priority\n");
            AST_DBG (AST_MGMT_DBG, "SNMP: No change in Port Priority\n");
            return MST_SUCCESS;
        }
        MST_SET_PORT_PRIORITY (u1Priority, pPerStPortInfo->u1PortPriority);

        if (AST_IS_MST_ENABLED ())
        {
            AST_DBG_ARG1 (AST_MGMT_DBG,
                          "SNMP:Inst %d: ...Recomputing roles..\n",
                          u2InstanceId);
            pPerStPortInfo->PerStRstPortInfo.bSelected = RST_FALSE;
            MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pPerStPortInfo->PerStRstPortInfo.bReSelect = RST_TRUE;
            pPortInfo->bAllTransmitReady = MST_FALSE;

            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             u2InstanceId) != RST_SUCCESS)
            {

                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RstPortRoleSelectionMachine function returned "
                         "FAILURE!\n");
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetAdminPointToPoint                    */
/*                                                                           */
/*    Description               : This function sets the Adminstrative Point */
/*                                to Point status of the port                */
/*                                                                           */
/*    Input(s)                  : u2PortNo   - Port Number of the Port.      */
/*                                bAdminP2P  - Point to Point to status      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetAdminPointToPoint (UINT2 u2PortNo, UINT1 u1AdminPToP)
#else
INT4
MstSetAdminPointToPoint (u2PortNo, u1AdminPToP)
     UINT2               u2PortNo;
     UINT1               u1AdminPToP;
#endif
{
    if (RstSetAdminPointToPoint (u2PortNo, u1AdminPToP) != RST_SUCCESS)
    {

        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: RstSetAdminPointToPoint function returned FAILURE!\n");
        return MST_FAILURE;
    }

    if (AST_IS_SISP_LOGICAL (u2PortNo) == MST_TRUE)
    {
        MstSispCopyPortPropToSispLogical (AST_GET_IFINDEX (u2PortNo),
                                          MST_ADMIN_P2P,
                                          (tAstBoolean) u1AdminPToP);
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetAdminEdgePort                        */
/*                                                                           */
/*    Description               : This function sets the Adminstrative Edge  */
/*                                status of the port                         */
/*                                                                           */
/*    Input(s)                  : u2PortNo       - Port Number of the Port.  */
/*                                bAdminEdgePort - Adminstrative Edge Status */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetAdminEdgePort (UINT2 u2PortNo, tAstBoolean bAdminEdgePort)
#else
INT4
MstSetAdminEdgePort (u2PortNo, bAdminEdgePort)
     UINT2               u2PortNo;
     tAstBoolean         bAdminEdgePort;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);

    if (pAstPortEntry == NULL)
    {
        return MST_FAILURE;
    }

    /* Incase of customer edge port, update the port Admin edge status
     * in the C-VLAN component CEP. */
    if (AST_GET_BRG_PORT_TYPE (pAstPortEntry) == AST_CUSTOMER_EDGE_PORT)
    {
        return (AstPbSetPortAdminEdgeInCvlanComp
                (pAstPortEntry, bAdminEdgePort));
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, MST_CIST_CONTEXT);

    if (pPerStPortInfo == NULL)
    {
        return MST_FAILURE;
    }
    if (pAstPortEntry->bAdminEdgePort == bAdminEdgePort)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in AdminEdge Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in AdminEdge Status\n");
        return MST_SUCCESS;
    }
    pAstPortEntry->bAdminEdgePort = bAdminEdgePort;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Admin Edge Status set as :%u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), bAdminEdgePort);

    if (AST_IS_MST_ENABLED ())
    {
        RstBrgDetectionMachine (RST_BRGDETSM_EV_ADMIN_SET, u2PortNo);
    }

    if (AST_IS_SISP_LOGICAL (u2PortNo) == MST_TRUE)
    {
        MstSispCopyPortPropToSispLogical (AST_GET_IFINDEX (u2PortNo),
                                          MST_ADMIN_EDGE, bAdminEdgePort);
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstMapVlanList                                       */
/*                                                                           */
/* Description        : This function maps the given group vlans to given    */
/*                      instance identifier.                                 */
/*                                                                           */
/* Input(s)           : MstVlanList  - List of Vlan Id's that are to be      */
/*                                     Mapped to Spanning Tree Instance.     */
/*                                                                           */
/*                      u2Instance   - Instance Id for which the Vlan to be  */
/*                                     Mapped.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE.                             */
/*****************************************************************************/
INT4
MstMapVlanList (tAstMstVlanList MstVlanList, UINT2 u2InstanceId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tMstVlanList       *pFdbList = NULL;
    tMstVlanList       *pNewVlanList = NULL;
    tMstVlanList       *pActiveVlanList = NULL;
    UINT4               u4FdbId;
    tMstVlanId          VlanId = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanFlag;
    UINT2               u2OldInstance = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1Result = MST_FALSE;    /* Initialised to zero */
    UINT2               u2NumVlans = 0;
    UINT2               u2ActiveVlans = 0;
    UINT2               u2VlanCount = 0;
    UINT2               u2LastVlan = 0;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    BOOL1               bInstUp = OSIX_FALSE;

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pFdbList) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pNewVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pActiveVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }

    MEMSET (pFdbList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pNewVlanList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pActiveVlanList->au1List, 0, MST_VLAN_LIST_SIZE);

    /* First program the L2IWF */
    for (u2ByteInd = 0; u2ByteInd < MST_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (MstVlanList[u2ByteInd] != 0)
        {
            u2VlanFlag = MstVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) && (u2VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }
                    if (AstL2IwfMiGetVlanInstMapping
                        (AST_CURR_CONTEXT_ID (), VlanId) != u2InstanceId)
                    {
                        u2NumVlans++;
                        OSIX_BITLIST_SET_BIT (pNewVlanList->au1List,
                                              VlanId, MST_VLAN_LIST_SIZE);
                        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                    VlanId) == OSIX_TRUE)
                        {
                            u2ActiveVlans++;
                            OSIX_BITLIST_SET_BIT (pActiveVlanList->au1List,
                                                  VlanId, MST_VLAN_LIST_SIZE);
                        }
                    }
                    /* During vlan to instance mapping through the IEEE object,
                     * in the NOT_READY state L2IWF is updated with the
                     * vlan to instance mapping. Hence in the ACTIVE state,to
                     * program the mapping in hardware row status is checked for
                     * NOT_READY state and the active vlan count is incremented.
                     * */

                    if (u2InstanceId != AST_TE_MSTID)
                    {
                        pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);

                        if ((pPerStInfo != NULL)
                            && (pPerStInfo->u1RowStatus == NOT_READY))
                        {
                            u2NumVlans++;
                            OSIX_BITLIST_SET_BIT (pNewVlanList->au1List,
                                                  VlanId, MST_VLAN_LIST_SIZE);
                            if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                        VlanId) == OSIX_TRUE)
                            {
                                u2ActiveVlans++;
                                OSIX_BITLIST_SET_BIT (pActiveVlanList->au1List,
                                                      VlanId,
                                                      MST_VLAN_LIST_SIZE);
                            }
                        }
                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }

    /* For proper working of SISP, whenever an instance is created, then the 
     * instance to vlan mapping should be properly programmed in L2IWF before
     * creating the instance. Hence, Creating MSTP instance, is done after 
     * populating in L2IWF. The only way this call fails is because of memory
     * issue. Hence, the same need not be restored & the debug messages are 
     * also properly displayed.
     * */

    if (u2InstanceId == AST_TE_MSTID)
    {
        if ((AST_CURR_CONTEXT_INFO ()->u1TEMSTIDValid) != MST_TRUE)
        {
            if (MstCreateInstance (u2InstanceId, MstVlanList) == MST_FAILURE)
            {
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);

                return MST_FAILURE;
            }
        }
    }
    else
    {
        pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);

        /* MstCreateInstance() must be invoked for all non active instances */
        if (((pPerStInfo == NULL) || (pPerStInfo->u1RowStatus != ACTIVE))
            && (u2InstanceId != AST_TE_MSTID))
        {
            if (MstCreateInstance (u2InstanceId, MstVlanList) == MST_FAILURE)
            {
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);

                return MST_FAILURE;
            }
            bInstUp = OSIX_TRUE;
        }
    }

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        if (AST_IS_MST_ENABLED ())
        {
            if (FNP_SUCCESS !=
                MstpFsMiMstpNpAddVlanListInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pActiveVlanList->au1List,
                                                      u2InstanceId,
                                                      u2ActiveVlans,
                                                      &u2LastVlan))
            {
                AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                              AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG,
                              "Creating mapping for vlan %d to instance %d in the hardware failed\n",
                              u2LastVlan, u2InstanceId);

                if (bInstUp == OSIX_TRUE)
                {
                    if (MstDeleteInstance (u2InstanceId) == MST_FAILURE)
                    {
                        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                      AST_EVENT_HANDLING_DBG |
                                      AST_INIT_SHUT_DBG,
                                      "Deleting instance %d failed in hardware\n",
                                      u2InstanceId);
                    }
                }
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);

                return MST_FAILURE;
            }
            else
            {
                AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                              AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG,
                              "Active Vlan's:%d, Mapping of vlan to instance %d in the hardware "
                              "completed successfully!! \n",
                              u2ActiveVlans, u2InstanceId);

            }
        }
    }
#endif /* NPAPI_WANTED */
    u2ActiveVlans = 0;
    for (u2ByteInd = 0; ((u2ByteInd < MST_VLAN_LIST_SIZE)
                         && (u2VlanCount < u2NumVlans)
                         && (u2LastVlan != VlanId)); u2ByteInd++)
    {
        if (pNewVlanList->au1List[u2ByteInd] != 0)
        {
            u2VlanFlag = pNewVlanList->au1List[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) && (u2VlanFlag != 0)
                  && (u2VlanCount < u2NumVlans)); u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);
                    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId)
                        == OSIX_TRUE)
                    {
                        u2ActiveVlans++;
                    }
                    u2VlanCount++;
                    u2OldInstance = AstL2IwfMiGetVlanInstMapping
                        (AST_CURR_CONTEXT_ID (), VlanId);
                    if (u2InstanceId != AST_TE_MSTID)
                    {
                        AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                    VlanId, u2InstanceId);
                    }
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId)
                        == OSIX_TRUE)
                    {

                        u4FdbId = AstL2IwfMiGetVlanFdbId
                            (AST_CURR_CONTEXT_ID (), VlanId);
                        OSIX_BITLIST_IS_BIT_SET (pFdbList->au1List,
                                                 u4FdbId,
                                                 MST_FDB_LIST_SIZE, bResult);
                        if (bResult == OSIX_FALSE)
                        {
                            OSIX_BITLIST_SET_BIT (pFdbList->au1List,
                                                  u4FdbId, MST_FDB_LIST_SIZE);
                        }
                        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                                    MST_UPDATE_INST_VLAN_MAP_MSG,
                                                    u2InstanceId,
                                                    u2OldInstance, VlanId,
                                                    MST_MAP_INST_VLAN_LIST_MSG,
                                                    &u1Result);

                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    u2InstanceId,
                                    u2OldInstance, VlanId,
                                    MST_MAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    MstTriggerDigestCalc ();

    /* Incrementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (bInstUp == OSIX_TRUE)
    {
        /* Incrementing the Instance UP Count */
        AST_INCR_INSTANCE_UP_COUNT (u2InstanceId);
        AstMstInstUpTrap (u2InstanceId, (INT1 *) AST_MST_TRAPS_OID,
                          AST_MST_TRAPS_OID_LEN);
    }
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Vlans successfully mapped to Instance %d \n",
                  u2InstanceId);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Vlans successfully mapped to Instance %d \n",
                  u2InstanceId);

    if (u2InstanceId != AST_TE_MSTID)
    {
        /* As per 802.1Q 2005 BEGIN event should be triggered, whenever there
         * is a change in Configuration Identifier [region name, revision no. &
         * vlan to instance mapping
         * */
        if (AstAssertBegin () == MST_FAILURE)

        {
            AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_INIT_SHUT_TRC, "SYS: Asserting BEGIN failed!\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "SYS: Asserting BEGIN failed!\n");
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);

            return MST_FAILURE;
        }
    }
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstUnMapVlanList                                     */
/*                                                                           */
/* Description        : This function unmaps the given group vlans to given  */
/*                      instance identifier.                                 */
/*                                                                           */
/* Input(s)           : MstVlanList  - List of Vlan Id's that are to be      */
/*                                     UnMapped from Spanning Tree Instance. */
/*                                                                           */
/*                      u2Instance   - Instance Id from which the Vlan is to */
/*                                     be unmapped.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE.                             */
/*****************************************************************************/
INT4
MstUnMapVlanList (tAstMstVlanList MstVlanList, UINT2 u2InstanceId)
{
    tMstVlanList       *pFdbList = NULL;
    tMstVlanList       *pNewVlanList = NULL;
    tMstVlanList       *pActiveVlanList = NULL;
    tMstVlanList       *pTmpVlanList = NULL;
    UINT4               u4FdbId;
    tMstVlanId          VlanId = 0;
    UINT2               u2ByteInd;
    UINT2               u2VlanId;
    UINT2               u2BitIndex;
    UINT2               u2VlanFlag;
    UINT2               u2NumVlans = 0;
    UINT2               u2ActiveVlans = 0;
    UINT2               u2VlanCount = 0;
    UINT2               u2LastVlan = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1Result = MST_FALSE;    /* Initialised to zero */
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pFdbList) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pNewVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pActiveVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pTmpVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }
    MEMSET (pFdbList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pNewVlanList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pActiveVlanList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pTmpVlanList->au1List, 0, MST_VLAN_LIST_SIZE);

    for (u2ByteInd = 0; u2ByteInd < MST_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (MstVlanList[u2ByteInd] != 0)
        {
            u2VlanFlag = MstVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) && (u2VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }

                    if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                      VlanId) == u2InstanceId)
                    {
                        u2NumVlans++;
                        OSIX_BITLIST_SET_BIT (pNewVlanList->au1List,
                                              VlanId, MST_VLAN_LIST_SIZE);
                        if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                    VlanId) == OSIX_TRUE)
                        {
                            u2ActiveVlans++;
                            OSIX_BITLIST_SET_BIT (pActiveVlanList->au1List,
                                                  VlanId, MST_VLAN_LIST_SIZE);
                        }
                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }

    for (u2VlanId = 1; u2VlanId <= VLAN_DEV_MAX_VLAN_ID; u2VlanId++)
    {
        if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                          u2VlanId) == u2InstanceId)
        {
            OSIX_BITLIST_SET_BIT (pTmpVlanList->au1List, u2VlanId,
                                  MST_VLAN_LIST_SIZE);
        }
    }

    if (MEMCMP
        (pTmpVlanList->au1List, pNewVlanList->au1List, MST_VLAN_LIST_SIZE) == 0)
    {
        if (MstDeleteInstance (u2InstanceId) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "API:  instance %d deletion failed.. \n",
                          u2InstanceId);
            AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                          AST_EVENT_HANDLING_DBG,
                          "API: instance %d deletion failed.. \n",
                          u2InstanceId);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

            return (MST_FAILURE);
        }
        if (AstAssertBegin () == MST_FAILURE)
        {
            AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_INIT_SHUT_TRC, "SYS: Asserting BEGIN failed!\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "SYS: Asserting BEGIN failed!\n");
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);
            return MST_FAILURE;
        }
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

        return MST_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {

        if (AST_IS_MST_ENABLED ())
        {
            if (FNP_SUCCESS !=
                MstpFsMiMstpNpDelVlanListInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pActiveVlanList->au1List,
                                                      u2InstanceId,
                                                      u2ActiveVlans,
                                                      &u2LastVlan))
            {
                AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                              u2LastVlan, u2InstanceId);
                AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                              AST_EVENT_HANDLING_DBG,
                              "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                              u2LastVlan, u2InstanceId);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
                AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

                return MST_FAILURE;
            }
        }
    }
#endif /*NPAPI_WANTED */
    u2ActiveVlans = 0;
    for (u2ByteInd = 0; ((u2ByteInd < MST_VLAN_LIST_SIZE) &&
                         (u2VlanCount < u2NumVlans) &&
                         (u2LastVlan != VlanId)); u2ByteInd++)
    {
        if (pNewVlanList->au1List[u2ByteInd] != 0)
        {
            u2VlanFlag = pNewVlanList->au1List[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE)
                  && (u2VlanFlag != 0)
                  && (u2VlanCount < u2NumVlans)); u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);
                    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId)
                        == OSIX_TRUE)
                    {
                        u2ActiveVlans++;
                    }
                    u2VlanCount++;
#ifdef NPAPI_WANTED
                    if (u2LastVlan == VlanId)
                    {
                        if (u2ActiveVlans == 1)
                        {
                            AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                     "API: Mapping failed.. \n");
                            AST_DBG (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                     AST_EVENT_HANDLING_DBG,
                                     "API: Mapping failed.. \n");
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK
                                (pActiveVlanList);
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

                            return MST_FALSE;
                        }
                        break;
                    }
#endif /*NPAPI_WANTED */
                    AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId,
                                                AST_INIT_VAL);
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), VlanId)
                        == OSIX_TRUE)
                    {
                        u4FdbId = AstL2IwfMiGetVlanFdbId
                            (AST_CURR_CONTEXT_ID (), VlanId);
                        OSIX_BITLIST_IS_BIT_SET (pFdbList->au1List,
                                                 u4FdbId,
                                                 MST_FDB_LIST_SIZE, bResult);

                        if (bResult == OSIX_FALSE)
                        {
                            AstVlanMiFlushFdbId (AST_CURR_CONTEXT_ID (),
                                                 u4FdbId);
                            OSIX_BITLIST_SET_BIT (pFdbList->au1List,
                                                  u4FdbId, MST_FDB_LIST_SIZE);
                        }
                        if (GARP_STAP_CIST_ID != u2InstanceId)
                        {
                            AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                                        MST_UPDATE_INST_VLAN_MAP_MSG,
                                                        MST_CIST_CONTEXT,
                                                        u2InstanceId, VlanId,
                                                        MST_UNMAP_INST_VLAN_LIST_MSG,
                                                        &u1Result);
                        }
                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    MST_CIST_CONTEXT,
                                    u2InstanceId, VlanId,
                                    MST_UNMAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    MstTriggerDigestCalc ();

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (AstAssertBegin () == MST_FAILURE)
    {
        AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

        return MST_FAILURE;
    }
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pNewVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pTmpVlanList);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortHelloTime                        */
/*                                                                           */
/*    Description               : This function sets the Hello Time for Port */
/*                                                                           */
/*    Input(s)                  : u2PortNo - Port Number for which the Hello */
/*                                           to be set.                      */
/*                                u2HelloTime - Hello Time value to be set.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetPortHelloTime (UINT2 u2PortNo, UINT2 u2HelloTime)
#else
INT4
MstSetPortHelloTime (u2PortNo, u2HelloTime)
     UINT2               u2PortNo;
     UINT2               u2HelloTime;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);

    if (pAstPortEntry->u2HelloTime == u2HelloTime)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in HelloTime value\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in HelloTime value\n");
        return MST_SUCCESS;
    }
    pAstPortEntry->u2HelloTime = u2HelloTime;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Hello time set as :%u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2HelloTime);

    /* Updating the Desg Times and Port Times */
    if (AST_FORCE_VERSION == AST_VERSION_3)
    {
        pAstPortEntry->DesgTimes.u2HelloTime = pAstPortEntry->u2HelloTime;

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNo, MST_CIST_CONTEXT);

        if (pPerStPortInfo->PerStRstPortInfo.u1InfoIs == MST_INFOIS_MINE)
        {
            AST_DBG_ARG2 (AST_MGMT_DBG,
                          "SNMP: Port %s: This port is a designated port."
                          "Setting Port Hello time as :%u\n",
                          AST_GET_IFINDEX_STR (u2PortNo), u2HelloTime);

            /* Update the port times only if the port
             * is a designated port */
            pAstPortEntry->PortTimes.u2HelloTime =
                pAstPortEntry->DesgTimes.u2HelloTime;
        }

    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortErrorRecovery                    */
/*                                                                           */
/*    Description               : This function sets the Error Recovery Time */
/*                      for Port                      */
/*                                                                           */
/*    Input(s)                  : u2PortNo - Port Number for which the Hello */
/*                                           to be set.                      */
/*                                u4ErrorRecovery - Error Recovery Time value*/
/*                  to be set.                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstSetPortErrorRecovery (UINT2 u2PortNo, UINT4 u4ErrorRecovery)
#else
INT4
MstSetPortErrorRecovery (u2PortNo, u4ErrorRecovery)
     UINT2               u2PortNo;
     UINT4               u4ErrorRecovery;
#endif
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNo);
    if (pAstPortEntry == NULL)
    {
        return MST_FAILURE;
    }

    if (pAstPortEntry->u4ErrorRecovery == u4ErrorRecovery)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Error Recovery Time value\n");
        AST_DBG (AST_MGMT_DBG,
                 "SNMP: No change in Error Recovery Time value\n");
        return MST_SUCCESS;
    }
    pAstPortEntry->u4ErrorRecovery = u4ErrorRecovery;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Error Recovery time set as :%u\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u4ErrorRecovery);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSetAutoEdgePort                                   */
/*                                                                           */
/* Description        : This function is the event handler whenever the Port */
/*                      AutoEdgePort value is set through management         */
/*                      and the corresponding event is generated.            */
/*                                                                           */
/* Input(s)           : u2PortNum - The port number of the port which is to  */
/*                                  be made as an Edge Port                  */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should function as an Edge Port or not     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT1               aau1AutoEdgePortStatus[2][10] = {
        "FALSE", "TRUE"
    };

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if (pAstPortEntry->bAutoEdge == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Auto Edge Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Auto Edge Status\n");
        return MST_SUCCESS;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE)
    {
        return (AstCVlanSetAutoEdge (u2PortNum, bStatus));
    }

    pAstPortEntry->bAutoEdge = bStatus;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Auto Edge Status Set as :%s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AutoEdgePortStatus[bStatus]);

    if (AST_IS_MST_ENABLED ())
    {
        if (bStatus == MST_TRUE)
        {
            /* If Edgedelay While is not running start it */
            if (pCommPortInfo->pEdgeDelayWhileTmr == NULL)
            {
                if (RstBrgDetectionMachine (RST_BRGDETSM_EV_AUTOEDGE_SET,
                                            u2PortNum) != RST_SUCCESS)
                {
                    AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: BridgeDetection Machine returned failure!\n");
                    return MST_FAILURE;
                }
            }
        }
        else
        {
            if (pCommPortInfo->pEdgeDelayWhileTmr != NULL)
            {
                if (AstStopTimer
                    ((VOID *) pAstPortEntry,
                     (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE) != MST_SUCCESS)
                {
                    AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: AstStopTimer for EdgeDelayWhile FAILED!\n");
                    return MST_FAILURE;
                }
            }
        }
    }

    if (AST_IS_SISP_LOGICAL (u2PortNum) == MST_TRUE)
    {
        MstSispCopyPortPropToSispLogical (AST_GET_IFINDEX (u2PortNum),
                                          MST_AUTO_EDGE, bStatus);
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Auto Edge set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AutoEdgePortStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG, "SNMP: Port %s: Auto Edge set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1AutoEdgePortStatus[bStatus]);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSetPortRestrictedRole                             */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      restrictedRole value is set through management.      */
/*                      It sets/resets restrictedRole field for given port   */
/*                      and takes action accoridingly.                       */
/*                      If restrictedRole is set, then this port should not  */
/*                      become a root port for any MST instance. Hence check */
/*                      that this port is a root port for any instance. If   */
/*                      so then trigger role selection for that instance.    */
/*                      If restrictedRole is reset, then check that the port */
/*                      is an alternate port for any MST instance. If so     */
/*                      then trigger role selection for that Mst Instance.   */
/*                                                                           */
/* Input(s)           : pAstPortEntry  - Pointer to Port Entry               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Root Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstSetPortRestrictedRole (tAstPortEntry * pPortEntry, tAstBoolean bStatus)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    INT4                i4RetVal = MST_SUCCESS;
    UINT2               u2Inst;

    UINT1               aau1PortRootGuardStatus[2][10] = {
        "FALSE", "TRUE"
    };

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);

    AST_PORT_RESTRICTED_ROLE (pPortEntry) = bStatus;
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (pPortEntry->u2PortNo);

    if (!(AST_IS_MST_ENABLED ()))
    {
        return MST_SUCCESS;
    }

    u2Inst = 0;
    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo, u2Inst);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        if (bStatus == MST_TRUE)
        {
            /* While setting restrictedRole (root guard), if the port is not 
             * root port then we need not trigger Role selection.
             * Even otherwise, if the port is at the region boundary 
             * then CIST role selection will trigger role selection 
             * for all MSTIs. Hence need not do it here */
            if ((pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ROOT)
                ||
                ((pCistMstiPortInfo->bRcvdInternal == MST_FALSE) &&
                 (u2Inst != MST_CIST_CONTEXT)))
            {
                continue;
            }
        }
        else
        {
            /* While resetting restrictedRole (root guard), if port is not 
             * alternate port then we need not trigger Role selection 
             * Even otherwise, if the port is at the region boundary 
             * then CIST role selection will trigger role selection 
             * for all MSTIs. Hence need not do it here */
            if ((pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ALTERNATE)
                ||
                ((pCistMstiPortInfo->bRcvdInternal == MST_FALSE) &&
                 (u2Inst != MST_CIST_CONTEXT)))
            {
                continue;
            }

        }

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pRstPortInfo->bSelected = MST_FALSE;
        MstPUpdtAllSyncedFlag (u2Inst, pPerStPortInfo, MST_SYNC_BSELECT_UPDATE);
        pRstPortInfo->bReSelect = MST_TRUE;
        pPortEntry->bAllTransmitReady = MST_FALSE;

        if (RstPortRoleSelectionMachine
            ((UINT1) RST_PROLESELSM_EV_RESELECT, u2Inst) != RST_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_MGMT_TRC,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");
            AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: RoleSelectionMachine returned FAILURE!\n");

            i4RetVal = MST_FAILURE;
            continue;
        }
    }
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : MstSetPortRootGuard                                  */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      restrictedRole value is set through management.      */
/*                      It sets/resets restrictedRole field for given port   */
/*                      and takes action accoridingly.                       */
/*                      If restrictedRole is set, then this port should not  */
/*                      become a root port for any MST instance. Hence check */
/*                      that this port is a root port for any instance. If   */
/*                      so then trigger role selection for that instance.    */
/*                      If restrictedRole is reset, then check that the port */
/*                      is an alternate port for any MST instance. If so     */
/*                      then trigger role selection for that Mst Instance.   */
/*                                                                           */
/* Input(s)           : pAstPortEntry  - Pointer to Port Entry               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Root Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstSetPortRootGuard (tAstPortEntry * pPortEntry, tAstBoolean bStatus)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT4                i4RetVal = MST_SUCCESS;
    UINT2               u2Inst;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               aau1PortRootGuardStatus[2][10] = {
        "FALSE", "TRUE"
    };
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Set PortRootGuardStatus as: %s...\n",
                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo),
                  aau1PortRootGuardStatus[bStatus]);

    AST_PORT_ROOT_GUARD (pPortEntry) = bStatus;

    u2Inst = 0;
    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (pPortEntry->u2PortNo, u2Inst);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }
/* RootInconsistent reset when root guard is disabled */
        if (bStatus == MST_FALSE)
        {
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            if (NULL == pRstPortInfo)
            {
                continue;
            }
            if (pRstPortInfo->pRootIncRecTmr != NULL)
            {
                if (AstStopTimer
                    ((VOID *) pPerStPortInfo,
                     AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: AstStopTimer for Root Inconsistency"
                             " Recovery timer FAILED!\n");
                }
                if ((MstIsPortMemberOfInst
                     ((INT4) (AST_GET_IFINDEX (pPerStPortInfo->u2PortNo)),
                      (INT4) u2Inst) != MST_FALSE)
                    || (u2Inst == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port:"
                                      "%s Instance : %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), u2Inst,
                                      au1TimeStr);
                }
            }
            pPerStPortInfo->bRootInconsistent = MST_FALSE;
            pRstPortInfo->bSelected = MST_FALSE;
            pRstPortInfo->bReSelect = MST_TRUE;

            if (RstPortRoleSelectionMachine ((UINT1) RST_PROLESELSM_EV_RESELECT,
                                             u2Inst) != RST_SUCCESS)
            {
                AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                         "SNMP: RstPortRoleSelectionMachine function returned FAILURE!\n");
                return MST_FAILURE;
            }
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : MstSetPortLoopGuard                             */
/*                                                                           */
/* Description        : This function is the event handler whenever port     */
/*                      loopguard value is set through management.  It       */
/*                      sets/resets port state and role field for given port */
/*                      and takes action accoridingly.                       */
/*                      If loopGuard is set, then this port should not       */
/*                      become forwarding for any instacne then trigger the  */
/*                      bridge detection state machine with auto edge false  */
/*                      to restrict the port state to go forwarding.       */
/*                      If loopGuard is reset, then check that the port      */
/*                      can go to forwarding state                           */
/*                                                                           */
/* Input(s)           : pAstPortEntry  - Pointer to Port Entry               */
/*                      bStatus - A Boolean value indicating whether the Port*/
/*                                should have Loop Guard enabled             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pTmpPerStInfo = NULL;
    UINT2               u2TmpMstInst = RST_DEFAULT_INSTANCE;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

#if defined(MSTP_TRACE_WANTED) || defined(RSTP_DEBUG)
    UINT1               aau1LoopGuardEffectiveStatus[2][10] = {
        "FALSE", "TRUE"
    };
#endif

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry->bLoopGuard == bStatus)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                 "SNMP: No change in Loop Guard Status\n");
        AST_DBG (AST_MGMT_DBG, "SNMP: No change in Loop Guard Status\n");
        return MST_SUCCESS;
    }

    pAstPortEntry->bLoopGuard = bStatus;

    AST_DBG_ARG2 (AST_MGMT_DBG,
                  "SNMP: Port %s: Loop Guard Status Set as :%s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1LoopGuardEffectiveStatus[bStatus]);

    if (AST_IS_MST_ENABLED ())
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

        if (pPerStPortInfo == NULL)
        {
            return MST_FAILURE;
        }
        pPerStPortInfo->bLoopGuardStatus = bStatus;
        if (bStatus == MST_FALSE)
        {
            if (pAstPortEntry->bLoopInconsistent == MST_TRUE)
            {

                MstPortInfoSmMakeAged (pPerStPortInfo,
                                       (tMstBpdu *) NULL, MST_CIST_CONTEXT);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  MST_CIST_CONTEXT, au1TimeStr);

            }
            pPerStPortInfo->bLoopIncStatus = MST_FALSE;
            pAstPortEntry->bLoopInconsistent = MST_FALSE;
            pAstPortEntry->bAutoEdge = MST_TRUE;

        }

        if (((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) ||
             (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED))
            && (bStatus == MST_TRUE))
        {
            pPerStPortInfo->bLoopIncStatus = MST_FALSE;
            pPerStPortInfo->bLoopGuardStatus = MST_FALSE;
        }
        if (RstBrgDetectionMachine (RST_BRGDETSM_EV_AUTOEDGE_SET,
                                    u2PortNum) != RST_SUCCESS)
        {
            AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "SNMP: BridgeDetection Machine returned failure!\n");
            return MST_FAILURE;
        }
    }

    /* Loop Inconsistent to be set in all 
       MSTI instances
     */
    u2TmpMstInst = 1;
    AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
    {
        if (pTmpPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2TmpMstInst);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo->bLoopGuardStatus = bStatus;

        if (bStatus == MST_FALSE)
        {
            if (pPerStPortInfo->bLoopIncStatus == MST_TRUE)
            {
                MstPortInfoSmMakeAged (pPerStPortInfo,
                                       (tMstBpdu *) NULL, u2TmpMstInst);
                if ((MstIsPortMemberOfInst
                     ((INT4) (AST_GET_IFINDEX (pPerStPortInfo->u2PortNo)),
                      (INT4) u2TmpMstInst) != MST_FALSE)
                    || (u2TmpMstInst == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2TmpMstInst, au1TimeStr);
                }

            }
            pPerStPortInfo->bLoopIncStatus = bStatus;
        }

        /* In case of Designated Port set with Loop-guard,
         * no effect to be made*/

        if (((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) ||
             (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED)) &&
            (bStatus == MST_TRUE))
        {
            pPerStPortInfo->bLoopIncStatus = MST_FALSE;
            pPerStPortInfo->bLoopGuardStatus = MST_FALSE;
        }
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
                  "SNMP: Port %s: Loop Guard set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1LoopGuardEffectiveStatus[bStatus]);
    AST_DBG_ARG2 (AST_MGMT_DBG, "SNMP: Port %s: Loop Guard set as %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  aau1LoopGuardEffectiveStatus[bStatus]);

    return MST_SUCCESS;
}

#endif /* MSTP_WANTED */
