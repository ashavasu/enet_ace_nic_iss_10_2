/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmtxsm.c,v 1.43 2017/11/30 06:29:48 siva Exp $
 *
 * Description: This file contains Transmit Module Related routines.
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/* Function Name      : MstPortTxSmTxMstp                                    */
/*                                                                           */
/* Description        : This function is called by RstPortTxSmMakeTransmit   */
/*                      Rstp function in order to transmit a MST BPDU.       */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/

INT4
MstPortTxSmTxMstp (tAstPortEntry * pAstPortEntry, UINT2 u2MstInst)
{

    UNUSED_PARAM (pAstPortEntry);
    UNUSED_PARAM (u2MstInst);
    pAstPortEntry->bTransmitBpdu = MST_TRUE;
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPortTxSmTxMstpWr                                  */
/*                                                                           */
/* Description        : This function is called by AstTransmitMstBpdu        */
/*                      function in order to transmit the Consolidated       */
/*                      MST BPDU.                                            */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId - The Id of the Spanning Tree Instance  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPortTxSmTxMstpWr (tAstPortEntry * pAstPortEntry, UINT2 u2MstInst)
{
    tAstBufChainHeader *pMstBpdu = NULL;
    UINT4               u4DataLength = AST_INIT_VAL;
    UINT1               au1Llc[AST_LLC_HEADER_SIZE] = { 0x42, 0x42, 0x03 };
    UINT1               au1EnetLLCHeader[AST_ENET_LLC_HEADER_SIZE];
    INT4                i4Index;
    UINT2               u2TypeLen;
    UINT1               u1Flag = (UINT1) AST_INIT_VAL;
    UINT4               u4Ticks = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                              MST_CIST_CONTEXT);

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        return MST_SUCCESS;
    }

    if ((pMstBpdu =
         AST_ALLOC_CRU_BUF (AST_BPDU_LENGTH_MST + AST_ENET_LLC_HEADER_SIZE,
                            AST_OFFSET)) == NULL)
    {
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: MST Bpdu Allocation of CRU Buffer FAILED!\n");
        AST_DBG_ARG1 (AST_ALL_FLAG,
                      "SYS: Restarting State Machines for context: %s due to TX CRU Buffer allocation failure\n",
                      AST_CONTEXT_STR ());

        if (AstHandleImpossibleState (pAstPortEntry->u2PortNo) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                          "TMR: Port %s: Starting RESTART Timer Failed\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));

            return MST_FAILURE;
        }

        return MST_SUCCESS;
    }

    if (MstFormBpdu (pAstPortEntry, u2MstInst, (UINT1) AST_BPDU_TYPE_MST,
                     pMstBpdu, &u4DataLength, &u1Flag) != MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "TXSM: MstFormBpdu function returned FAILURE!\n");

        if (AST_RELEASE_CRU_BUF (pMstBpdu, MST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: MST Bpdu Release CRU Buffer FAILED!\n");
        }
        return MST_FAILURE;
    }

    AST_MEMSET (au1EnetLLCHeader, AST_MEMSET_VAL, sizeof (au1EnetLLCHeader));
    /* The destination address will be stored in the AstPortEntry depending
     * upon the Port Type. Hence copying the same here
     * */

    AST_MEMCPY (&au1EnetLLCHeader[0], &(pAstPortEntry->au1DestMACAddr),
                AST_MAC_ADDR_SIZE);

    i4Index = 6;

    AST_MEMCPY (&au1EnetLLCHeader[i4Index], &(pAstPortEntry->au1PortMACAddr),
                AST_MAC_ADDR_SIZE);

    i4Index += AST_MAC_ADDR_SIZE;

    u4DataLength = u4DataLength + AST_LLC_HEADER_SIZE;
    u2TypeLen = (UINT2) (OSIX_HTONS (u4DataLength));
    MEMCPY (&au1EnetLLCHeader[i4Index], &u2TypeLen, AST_TYPE_LEN_SIZE);
    i4Index += AST_TYPE_LEN_SIZE;

    MEMCPY (&au1EnetLLCHeader[i4Index], au1Llc, AST_LLC_HEADER_SIZE);
    i4Index += AST_LLC_HEADER_SIZE;

    if (u4DataLength > MST_MAX_BPDU_SIZE)
    {
        AST_RELEASE_CRU_BUF (pMstBpdu, MST_FALSE);
        AST_TRC (AST_CONTROL_PATH_TRC | ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: Invalid DataLength!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: Invalid DataLength!\n");
        return MST_FAILURE;
    }

    if (AST_COPY_OVER_CRU_BUF (pMstBpdu, au1EnetLLCHeader, 0,
                               AST_ENET_LLC_HEADER_SIZE) == AST_CRU_FAILURE)
    {
        if (AST_RELEASE_CRU_BUF (pMstBpdu, MST_FALSE) != AST_CRU_SUCCESS)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_BUFFER_TRC,
                     "TXSM: MST Bpdu Release CRU Buffer FAILED!\n");
        }
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return MST_FAILURE;
    }

    u4DataLength = u4DataLength + AST_ENET_HEADER_SIZE;
#ifdef NPAPI_WANTED
    if (gAstAllowDirectNpTx == RST_TRUE)
    {
        if (AstHandlePacketToNP
            (pMstBpdu, pAstPortEntry->u4IfIndex, u4DataLength) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "TXSM: Port %s: AstHandlePacketToNP FAILED!."
                          "Unable to transmit packet\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo));
            return MST_FAILURE;
        }
    }
    else
    {
        AstHandleOutgoingPktOnPort (pMstBpdu, pAstPortEntry,
                                    u4DataLength, AST_PROT_BPDU,
                                    (UINT1) AST_ENCAP_NONE);
    }
#else
    AstHandleOutgoingPktOnPort (pMstBpdu, pAstPortEntry,
                                u4DataLength, AST_PROT_BPDU,
                                (UINT1) AST_ENCAP_NONE);
#endif
    AST_TRC (AST_CONTROL_PATH_TRC, "TXSM: MST BPDU handed over to Bridge\n");

    /* Incrementing the MST BPDU Transmitted count */
    AST_INCR_TX_MST_BPDU_COUNT (MST_CIST_CONTEXT, pAstPortEntry->u2PortNo);
    /* Incrementing the proposal and agreement packet transmitted count */
    if (u1Flag == (UINT1) MST_SET_AGREEMENT_FLAG)
    {
        AST_GET_SYS_TIME (&u4Ticks);
        MST_INCR_TX_AGREEMENT_COUNT (pAstPortEntry->u2PortNo, MST_CIST_CONTEXT);
        pPerStPortInfo->u4AgreementTxTimeStamp = u4Ticks;
    }
    if (u1Flag == (UINT1) MST_SET_PROPOSAL_FLAG)
    {
        AST_GET_SYS_TIME (&u4Ticks);
        MST_INCR_TX_PROPOSAL_COUNT (pAstPortEntry->u2PortNo, MST_CIST_CONTEXT);
        pPerStPortInfo->u4ProposalTxTimeStamp = u4Ticks;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstFormBpdu                                          */
/*                                                                           */
/* Description        : This function is called by MstPortTxSmTxMstp         */
/*                      in order to form  MST BPDU.                          */
/*                                                                           */
/* Input(s)           : pAstPortEntry - The pointer to the Port Entry        */
/*                      u2InstanceId  - The Id of the Spanning Tree Instance */
/*                      u1MsgType     - Indicates the type of BPDU to form   */
/*                                      AST_BPDU_TYPE_MST                    */
/*                      pBuf - Pointer to the CRU Buffer in which the BPDU   */
/*                             is to be formed                               */
/*                      pu4DataLength - The number of bytes that have been   */
/*                                      filled in the BPDU                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstFormBpdu (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId,
             UINT1 u1MsgType, tAstBufChainHeader * pBuf, UINT4 *pu4DataLength,
             UINT1 *pu1Flag)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1Val = 0;
    UINT1               u1Version = 0;
    UINT1               u1BpduType = 0;
    UINT1               u1TmpFlag = AST_INIT_VAL;
    UINT2               u2ProtocolId = 0;
    UINT4               u4DataPaddLen = 0;
    AST_UNUSED (u2InstanceId);
    AST_UNUSED (u1MsgType);

    pu1BpduStart = pu1BpduEnd = gAstGlobalInfo.gpu1AstBpdu;
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                              MST_CIST_CONTEXT);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    u2ProtocolId = (UINT2) AST_PROTOCOL_ID;

    u1Version = (UINT1) AST_VERSION_3;
    u1BpduType = (UINT1) AST_BPDU_TYPE_RST;

    AST_PUT_2BYTE (pu1BpduEnd, u2ProtocolId);
    AST_PUT_1BYTE (pu1BpduEnd, u1Version);
    AST_PUT_1BYTE (pu1BpduEnd, u1BpduType);

    u1Val = (UINT1) (u1Val | MST_SET_MST_TOPOCH_ACK_FLAG);

#ifdef NPAPI_WANTED
    if (pPerStPortInfo->i4TransmitSelfInfo == MST_TRUE)
    {
        u1Val = (UINT1) (u1Val | MST_SET_FORWARDING_FLAG);
        u1Val = (UINT1) (u1Val | MST_SET_LEARNING_FLAG);
        u1Val = (UINT1) (u1Val | MST_SET_DESG_PROLE_FLAG);
    }
    else
#endif
    {
        if (pPerStRstPortInfo->bAgree == MST_TRUE)
        {
            AST_DBG_ARG2 (AST_TXSM_DBG,
                          "TXSM: SETTING AGREEMENT FLAG in Tx for Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          (UINT2) MST_CIST_CONTEXT);
            u1Val = (UINT1) (u1Val | MST_SET_AGREEMENT_FLAG);
            *pu1Flag = MST_SET_AGREEMENT_FLAG;
        }

        if (pPerStRstPortInfo->bForwarding == MST_TRUE)
        {
            u1TmpFlag = MST_SISP_GET_FWDFLAG (pAstPortEntry->u2PortNo);
            u1Val = u1Val | u1TmpFlag;
        }
        if (pPerStRstPortInfo->bLearning == MST_TRUE)
        {
            u1TmpFlag = MST_SISP_GET_LRNFLAG (pAstPortEntry->u2PortNo);
            u1Val = u1Val | u1TmpFlag;
        }

        if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
        {
            u1Val = (UINT1) (u1Val | MST_SET_DESG_PROLE_FLAG);
        }
        else if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
        {
            u1Val = (UINT1) (u1Val | MST_SET_ROOT_PROLE_FLAG);
        }
        else if ((pPerStPortInfo->u1PortRole ==
                  (UINT1) AST_PORT_ROLE_ALTERNATE)
                 || (pPerStPortInfo->u1PortRole ==
                     (UINT1) AST_PORT_ROLE_BACKUP))
        {
            u1Val = (UINT1) (u1Val | MST_SET_ALTBACK_PROLE_FLAG);
        }
        else
        {
            AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                     "TXSM: Impossible Port Role while Transmitting BPDU\n");
            return MST_FAILURE;
        }

        if (pPerStRstPortInfo->bProposing == MST_TRUE)
        {
            AST_DBG_ARG2 (AST_TXSM_DBG,
                          "TXSM: SETTING PROPOSAL FLAG in Tx for Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                          (UINT2) MST_CIST_CONTEXT);
            u1Val = (UINT1) (u1Val | MST_SET_PROPOSAL_FLAG);
            *pu1Flag = MST_SET_PROPOSAL_FLAG;
        }

        if (pPerStRstPortInfo->pTcWhileTmr != NULL)
        {
            u1Val = (UINT1) (u1Val | MST_SET_TOPOCH_FLAG);
        }
    }

    AST_PUT_1BYTE (pu1BpduEnd, u1Val);

    MstFillBpdu (pPerStPortInfo, &pu1BpduEnd);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    if (*pu4DataLength > MST_MAX_BPDU_SIZE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: Invalid DataLength!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: Invalid DataLength!\n");
        return MST_FAILURE;
    }

    /* Fill the next u4DataPaddLength as all zeros 0 to make PDU total
       of  MST_MAX_BPDU_SIZE */
    u4DataPaddLen = MST_MAX_BPDU_SIZE - (*pu4DataLength);
    AST_MEMSET (pu1BpduEnd, AST_MEMSET_VAL, u4DataPaddLen);
    pu1BpduEnd = pu1BpduEnd + u4DataPaddLen;

    if (AST_COPY_OVER_CRU_BUF
        (pBuf, gAstGlobalInfo.gpu1AstBpdu, AST_ENET_LLC_HEADER_SIZE,
         (*pu4DataLength)) == AST_CRU_FAILURE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | ALL_FAILURE_TRC | AST_BUFFER_TRC,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        AST_DBG (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                 "TXSM: BPDU Copy Over CRU Buffer FAILED!\n");
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstFillBpdu                                          */
/*                                                                           */
/* Description        : This function is called by MstFormBpdu function in   */
/*                      order to fill in the contents of the BPDU in the CRU */
/*                      Buffer.                                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information (Instance 0 for    */
/*                                       RSTP & CIST and non-zero for MSTP   */
/*                      *pu1Bpdu - Pointer to the linear attay in which the  */
/*                                 contents of the BPDU are to be filled     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/

VOID
MstFillBpdu (tAstPerStPortInfo * pPerStPortInfo, UINT1 **ppu1Bpdu)
{
    tPerStCistMstiCommInfo *pPerStCistMstiCommInfo = NULL;
    tAstPerStPortInfo  *pPerStMstPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStMstRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tMstInstInfo       *pMstInstInfo = NULL;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    tAstMacAddr         NullMac;
    UINT1              *pu1TmpVer3Len = NULL;
    UINT1               u1Version1Length = 0;
    UINT1               u1Val = 0;
    UINT2               u2PortId = AST_INIT_VAL;
    UINT1               u1ConfigurationId = AST_INIT_VAL;
    UINT2               u2ConfigRevLevel = AST_INIT_VAL;
    UINT2               u2MstInst = 1;
    UINT2               u2PortNum = 0;
    UINT2               u2TmpVal = 0;
    UINT2               u2TmpVer3Len = 0;
    UINT4               u4TmpVal = 0;
    UINT4               u4Ticks = AST_INIT_VAL;
    UINT1               u1ProposalFlag = AST_INIT_VAL;
    UINT1               u1AggFlag = AST_INIT_VAL;

    AST_MEMSET (&NullMac, AST_INIT_VAL, sizeof (tAstMacAddr));

    u2PortNum = pPerStPortInfo->u2PortNo;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPerStCistMstiCommInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);

#ifdef NPAPI_WANTED
    if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
    {
        u2TmpVal = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);
        AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->BridgeAddr);

        /* Root cost set to 0 */
        u4TmpVal = 0;
        AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

        u2TmpVal = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);
        AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->BridgeAddr);

        u2PortId = u2PortId | pPerStPortInfo->u1PortPriority;
        u2PortId = (UINT2) (u2PortId << 8);
        u2PortId = u2PortId | AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);

        AST_PUT_2BYTE (*ppu1Bpdu, u2PortId);
        /* Total 2 Bytes of the timer were splitted into Seconds 
         * and Centi-Seconds and filled in the packet as follows.
         */
        /*****************************************
         |Seconds (8 bits)|Centi-Seconds (8 bits)|
         *****************************************/
        u1Val =
            (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.
                                          u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.
                                        u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2MaxAge);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2HelloTime);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pBrgInfo->BridgeTimes.u2ForwardDelay);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
    }
    else
#endif
    {

        /* Transmitting the Designated Priority vector on all ports including
         * the Root port (similar to what is given in 802.1D/D4 for rstp).
         * Hence using the information stored at the Bridge level, rather than
         * the information stored at the port level, because Root ports
         * contain only received information and not designated information */
        u2TmpVal = MST_SISP_GET_ROOT_PRIO (u2PortNum,
                                           pPerStBrgInfo->RootId.u2BrgPriority);
        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);

        AST_PUT_MAC_ADDR (*ppu1Bpdu, pPerStBrgInfo->RootId.BridgeAddr);

        u4TmpVal = pPerStBrgInfo->u4RootCost;
        AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

        u2TmpVal = MST_SISP_GET_ROOT_PRIO
            (u2PortNum, pBrgInfo->MstBridgeEntry.RegionalRootId.u2BrgPriority);

        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);

        AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->MstBridgeEntry.RegionalRootId.
                          BridgeAddr);

        /* The Port Id is the transmitting port id as in the Designated Priority
         * vector, even if the port is a Root port */
        u2PortId = u2PortId |
            (MST_SISP_GET_PORT_PRIO
             (u2PortNum, pPerStPortInfo->u1PortPriority));

        u2PortId = (UINT2) (u2PortId << 8);
        u2PortId = u2PortId | AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);

        AST_PUT_2BYTE (*ppu1Bpdu, u2PortId);

        /* Transmitting the Designated Times on all ports according to 
         * Sec 13.24.3 designatedTimes of 802.1Q 2005
         * HelloTime alone is taken from Port Times according to
         * Sec 13.24.13 portTimes
         * */
        /* Total 2 Bytes of the timer were splitted into Seconds 
         * and Centi-Seconds and filled in the packet as follows.
         */
        /*****************************************
         |Seconds (8 bits)|Centi-Seconds (8 bits)|
         *****************************************/
        u1Val =
            (UINT1) AST_PROT_TO_BPDU_SEC (pPortInfo->DesgTimes.
                                          u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1)
            AST_PROT_TO_BPDU_CENTI_SEC (pPortInfo->DesgTimes.
                                        u2MsgAgeOrHopCount);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPortInfo->DesgTimes.u2MaxAge);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC
            (pPortInfo->DesgTimes.u2MaxAge);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC (pPortInfo->PortTimes.u2HelloTime);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC
            (pPortInfo->PortTimes.u2HelloTime);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_SEC
            (pPortInfo->DesgTimes.u2ForwardDelay);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

        u1Val = (UINT1) AST_PROT_TO_BPDU_CENTI_SEC
            (pPortInfo->DesgTimes.u2ForwardDelay);
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
    }

    u1Version1Length = (UINT1) AST_VERSION_1_LENGTH;
    AST_PUT_1BYTE (*ppu1Bpdu, u1Version1Length);

    pu1TmpVer3Len = *ppu1Bpdu;
    (*ppu1Bpdu) += 2;

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    u1ConfigurationId = pMstConfigIdInfo->u1ConfigurationId;
    AST_PUT_1BYTE (*ppu1Bpdu, u1ConfigurationId);

    AST_MEMCPY (*ppu1Bpdu, (pMstConfigIdInfo->au1ConfigName),
                MST_CONFIG_NAME_LEN);
    *ppu1Bpdu += MST_CONFIG_NAME_LEN;

    u2ConfigRevLevel = pMstConfigIdInfo->u2ConfigRevLevel;
    AST_PUT_2BYTE (*ppu1Bpdu, u2ConfigRevLevel);

    AST_MEMCPY (*ppu1Bpdu, (pMstConfigIdInfo->au1ConfigDigest),
                MST_CONFIG_DIGEST_LEN);
    *ppu1Bpdu += MST_CONFIG_DIGEST_LEN;

#ifdef NPAPI_WANTED
    if (pPerStPortInfo->i4TransmitSelfInfo == MST_TRUE)
    {
        /* Internal root cost */
        AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

        u2TmpVal = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);
        AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->BridgeAddr);
        u1Val = pPerStBrgInfo->u1BrgRemainingHops;
        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
    }
    else
#endif
    {
        u4TmpVal = pPerStBrgInfo->u4CistInternalRootCost;
        AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

        /* The Bridge Id is the transmitting bridge id as in the
         * Designated Priority vector, even if the port is a Root port */
        u2TmpVal = pPerStBrgInfo->u2BrgPriority;
        AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);
        AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->BridgeAddr);

        u1Val = pPerStCistMstiCommInfo->u1DesgRemainingHops;

        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
    }

    u1Val = 0;
    /* Increment the Version 3 Length by 64 for CIST information */
    u2TmpVer3Len += 64;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    /* Bpdus will transmit Msti Info on ports even if the port role for that
     * instance is alternate or backup. This is to make sure that the master
     * flag is always transmitted. This behaviour has been verified with CISCO
     * and HP */
    AST_SCAN_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList,
                       pMstInstInfo, tMstInstInfo *)
    {
        pPerStInfo = pMstInstInfo->pPerStInfo;
        u2MstInst = pMstInstInfo->u2MstInstance;
        if (u2MstInst == 0)
        {
            continue;
        }

        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStMstPortInfo = AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum,
                                                                u2MstInst);
        if (pPerStMstPortInfo == NULL)
        {
            continue;
        }
        pPerStCistMstiCommInfo =
            MST_GET_CIST_MSTI_PORT_INFO (pPerStMstPortInfo);
        pPerStMstRstPortInfo = &(pPerStMstPortInfo->PerStRstPortInfo);

        if (MstIsPortMemberOfInst
            ((INT4) AST_GET_IFINDEX ((INT4) u2PortNum),
             (INT4) u2MstInst) == MST_FALSE)
        {
            continue;
        }
        u1Val = 0;

#ifdef NPAPI_WANTED
        if (pPerStMstPortInfo->i4TransmitSelfInfo == MST_TRUE)
        {
            u1Val = (UINT1) (u1Val | MST_SET_FORWARDING_FLAG);
            u1Val = (UINT1) (u1Val | MST_SET_LEARNING_FLAG);
            u1Val = (UINT1) (u1Val | MST_SET_DESG_PROLE_FLAG);
        }
        else
#endif
        {
            if (pPerStMstPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
            {
                u1Val = (UINT1) (u1Val | MST_SET_DESG_PROLE_FLAG);
            }
            else if (pPerStMstPortInfo->u1PortRole == MST_PORT_ROLE_ROOT)
            {
                u1Val = (UINT1) (u1Val | MST_SET_ROOT_PROLE_FLAG);
            }
            else if (pPerStMstPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)
            {
                u1Val = (UINT1) (u1Val | MST_SET_MASTER_PROLE_FLAG);
            }
            else if ((pPerStMstPortInfo->u1PortRole == MST_PORT_ROLE_ALTERNATE)
                     || (pPerStMstPortInfo->u1PortRole == MST_PORT_ROLE_BACKUP))
            {
                u1Val = (UINT1) (u1Val | MST_SET_ALTBACK_PROLE_FLAG);
            }
            else
            {
                AST_DBG_ARG2 (AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                              "TXSM: Inst %u Port %s: Impossible Port Role while Transmitting BPDU\n",
                              u2MstInst, AST_GET_IFINDEX_STR (u2PortNum));
                continue;
            }

            if (pPerStMstRstPortInfo->bAgree == MST_TRUE)
            {
                AST_DBG_ARG2 (AST_TXSM_DBG,
                              "TXSM: SETTING AGREEMENT FLAG in Tx for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                u1Val = (UINT1) (u1Val | MST_SET_AGREEMENT_FLAG);
                u1AggFlag = MST_TRUE;
            }

            if (pPerStMstRstPortInfo->bForwarding == MST_TRUE)
            {
                u1Val = (UINT1) (u1Val | MST_SET_FORWARDING_FLAG);
            }
            if (pPerStMstRstPortInfo->bLearning == MST_TRUE)
            {
                u1Val = (UINT1) (u1Val | MST_SET_LEARNING_FLAG);
            }

            if (pPerStMstRstPortInfo->bProposing == MST_TRUE)
            {
                AST_DBG_ARG2 (AST_TXSM_DBG,
                              "TXSM: SETTING PROPOSAL FLAG in Tx for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                u1Val = (UINT1) (u1Val | MST_SET_PROPOSAL_FLAG);
                u1ProposalFlag = MST_TRUE;
            }

            if (pPerStMstRstPortInfo->pTcWhileTmr != NULL)
            {
                u1Val = (UINT1) (u1Val | MST_SET_TOPOCH_FLAG);
            }
            if (pPerStMstPortInfo->PerStMstiOnlyInfo.bMaster == MST_TRUE)
            {
                u1Val = (UINT1) (u1Val | MST_SET_MASTER_FLAG);
            }

        }

        AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

#ifdef NPAPI_WANTED
        if (pPerStMstPortInfo->i4TransmitSelfInfo == MST_TRUE)
        {
            u2TmpVal = pPerStBrgInfo->u2BrgPriority;
            pMstBrgEntry = AST_GET_MST_BRGENTRY ();

            if (pMstBrgEntry->bExtendedSysId == MST_FALSE)
            {
                u2TmpVal |= u2MstInst;
            }

            AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);
            AST_PUT_MAC_ADDR (*ppu1Bpdu, pBrgInfo->BridgeAddr);

            /* MSTI Internal path Cost */
            u4TmpVal = AST_INIT_VAL;
            AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

            /*MSTI Bridge Priority */
            /* The Bridge Id is the transmitting bridge id as in the Designated 
             * Priority vector, even if the port is a Root port */
            u2TmpVal = pPerStBrgInfo->u2BrgPriority;
            u1Val = (UINT1) (u2TmpVal >> 8);
            AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

            /*MSTI Port Priority */
            /* The Port Id is the transmitting port id as in the Designated 
             * Priority vector, even if the port is a Root port */
            u1Val = pPerStMstPortInfo->u1PortPriority;
            AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
            u1Val = pPerStBrgInfo->u1BrgRemainingHops;
            AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
        }
        else
#endif
        {
            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
            u2TmpVal = pPerStBrgInfo->RootId.u2BrgPriority;

            pMstBrgEntry = AST_GET_MST_BRGENTRY ();

            if (pMstBrgEntry->bExtendedSysId == MST_FALSE)
            {
                u2TmpVal |= u2MstInst;
            }
            AST_PUT_2BYTE (*ppu1Bpdu, u2TmpVal);

            AST_PUT_MAC_ADDR (*ppu1Bpdu, pPerStBrgInfo->RootId.BridgeAddr);

            /* MSTI Internal path Cost */
            u4TmpVal = pPerStBrgInfo->u4RootCost;
            AST_PUT_4BYTE (*ppu1Bpdu, u4TmpVal);

            /*MSTI Bridge Priority */
            /* The Bridge Id is the transmitting bridge id as in the Designated 
             * Priority vector, even if the port is a Root port */
            u2TmpVal = pPerStBrgInfo->u2BrgPriority;
            u1Val = (UINT1) (u2TmpVal >> 8);
            AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

            /*MSTI Port Priority */
            /* The Port Id is the transmitting port id as in the Designated 
             * Priority vector, even if the port is a Root port */
            u1Val = pPerStMstPortInfo->u1PortPriority;
            AST_PUT_1BYTE (*ppu1Bpdu, u1Val);

            /* MSTI Remaining Hops */
            /* Transmitting the Designated Times on all ports including the 
             * Root Port (similar to what is given in 802.1D/D4 for Rstp). 
             * Hence using the Port Times for Designated ports will contain the
             * Designated Times including the value of the Hello time for the 
             * port, if this Bridge is the Root Bridge. Thus for Root ports, 
             * the Times values present in Root Times is used (which is 
             * equivalent to the Designated Times, containing value of HelloTime
             * from the Root Bridge). */
            if (pPerStMstPortInfo->u1PortRole ==
                (UINT1) AST_PORT_ROLE_DESIGNATED)
            {
                u1Val = pPerStCistMstiCommInfo->u1PortRemainingHops;
                AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
            }
            else
            {
                u1Val = pPerStBrgInfo->u1RootRemainingHops;
                AST_PUT_1BYTE (*ppu1Bpdu, u1Val);
            }
        }

        /* Increment version 3 length by 16 for an instance 
         * information */
        u2TmpVer3Len += 16;

        /* Incrementing the MST BPDU Transmitted count */
        AST_INCR_TX_MST_BPDU_COUNT (u2MstInst, u2PortNum);

        if (u1ProposalFlag == MST_TRUE)
        {
            MST_INCR_TX_PROPOSAL_COUNT (u2PortNum, u2MstInst);
            AST_GET_SYS_TIME (&u4Ticks);
            pPerStMstPortInfo->u4ProposalTxTimeStamp = u4Ticks;
        }
        if (u1AggFlag == MST_TRUE)
        {
            MST_INCR_TX_AGREEMENT_COUNT (u2PortNum, u2MstInst);
            AST_GET_SYS_TIME (&u4Ticks);
            pPerStMstPortInfo->u4AgreementTxTimeStamp = u4Ticks;
        }
    }
    AST_PUT_2BYTE (pu1TmpVer3Len, u2TmpVer3Len);
    return;
}

/*****************************************************************************/
/* Function Name      : MstInitPortTxStateMachine                            */
/*                                                                           */
/* Description        : Initialises the Port Transmission State Machine      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitPortTxStateMachine (VOID)
{
    RstInitPortTxStateMachine ();
}

/*****************************************************************************/
/* Function Name      : MstIsPortMemberOfInst                                */
/*                                                                           */
/* Description        : Used to check whether a specific port is a member of */
/*                      VLAN and that specific VLAN is a member of an        */
/*                      instance.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
UINT1
MstIsPortMemberOfInst (INT4 i4PortNum, INT4 i4MstInst)
{
    UINT2               u2LocalPortId = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = MST_FAILURE;
    i4RetVal = AstGetContextInfoFromIfIndex ((UINT4) i4PortNum, &u4ContextId,
                                             &u2LocalPortId);
    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return MST_FALSE;
    }
    /*By default, all ports will be member of all instances. For the spanning-tree topology formation and settlement based on the 
     * vlan-instance mapping information, this verification is done. In this case, only the member ports of the vlan's 
     * mapped to the instance will be part of that instance. 
     *
     * If AST_INST_PORT_MAP is enabled, instance will have only the member ports of the vlan's mapped. 
     *
     * If AST_INST_PORT_MAP is disabled, instance will have all the ports in the system
     */

    if (AST_INST_PORT_MAP == MST_INST_MAP_DISABLED)
    {
        return MST_TRUE;
    }
    if (MST_FAILURE == i4RetVal)
    {
        AST_TRC (AST_CONTROL_PATH_TRC, "TXSM:Port number is invalid!\n");
        return MST_FALSE;
    }

    if (AstL2IwfIsPortInInst (u4ContextId, (UINT4) i4PortNum, i4MstInst) ==
        L2IWF_SUCCESS)
    {
        return MST_TRUE;

    }
    else
    {
        return MST_FALSE;

    }
}

#endif /* MSTP_WANTED */
