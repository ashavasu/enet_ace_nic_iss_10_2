/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmpism.c,v 1.72 2017/11/21 13:06:50 siva Exp $ 
 *
 * Description: This file contains MST Port Information SEM        
 *              related routines.
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoMachine                         */
/*                                                                           */
/*    Description               : This function implements Port Information  */
/*                                State Machine. Called when a MST BPDU is   */
/*                                received on the port.                      */
/*                                                                           */
/*    Input(s)                  : u1Event        - Event occured related to  */
/*                                                 this Machine.             */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortInfoMachine (UINT1 u1Event,
                    tAstPerStPortInfo * pPerStPortInfo,
                    tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
#else
INT4
MstPortInfoMachine (u1Event, pPerStPortInfo, pRcvdBpdu, u2MstInst)
     UINT1               u1Event;
     tAstPerStPortInfo  *pPerStPortInfo;
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2MstInst;
#endif
{

    UINT1               u1State = 0;
    INT4                i4RetVal = 0;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM:: Port Info State Machine called with Invalid Params\n",
                      gaaau1AstSemEvent[AST_PRSM][u1Event]);
        return MST_FAILURE;
    }

    u1State = pPerStPortInfo->u1PinfoSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "Port %s: Inst %d: Port Info Machine Called with Event: %s, and State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst,
                  gaaau1AstSemEvent[AST_PISM][u1Event],
                  gaaau1AstSemState[AST_PISM][u1State]);
    AST_DBG_ARG4 (AST_PISM_DBG,
                  "Port %s: Inst %d: Port Info Machine Called with Event: %s, and State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst,
                  gaaau1AstSemEvent[AST_PISM][u1Event],
                  gaaau1AstSemState[AST_PISM][u1State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == MST_TRUE) &&
        (u1Event != MST_PINFOSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_PISM_DBG,
                 "PISM: Ignoring event since BEGIN is asserted\n");
        return MST_SUCCESS;
    }

    if (MST_PORT_INFO_MACHINE[u1Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_PISM_DBG, "PISM: No Operation to Perform\n");
        return MST_SUCCESS;
    }
    i4RetVal = (*(MST_PORT_INFO_MACHINE[u1Event][u1State].pAction))
        (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    if (i4RetVal != MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "PINFO SEM: Event Handler returned failure \n");
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeDisabled                  */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                DISABLED State.                            */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeDisabled (tAstPerStPortInfo * pPerStPortInfo,
                           tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    AST_UNUSED (pRcvdBpdu);

    u2PortNum = pPerStPortInfo->u2PortNo;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

    pRstPortInfo->bRcvdMsg = MST_FALSE;
    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pRstPortInfo->bProposing = RST_FALSE;
    pRstPortInfo->bAgree = RST_FALSE;
    pRstPortInfo->bAgreed = RST_FALSE;
    pRstPortInfo->bProposed = RST_FALSE;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }
    /* On disabling STP LoopInc state to be cleared in case enabled */
    pPerStPortInfo->bLoopIncStatus = MST_FALSE;
    pPerStPortInfo->bRootInconsistent = MST_FALSE;
    if (pRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_RCVDINFOWHILE) !=
            RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                          "PISM: Port %s: Inst %d: Stop RcvdInfoWhile Timer returned failure",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    pRstPortInfo->u1InfoIs = MST_INFOIS_DISABLED;

    /* Triggering the Role Selection State Machine */
    pRstPortInfo->bReSelect = MST_TRUE;
    pRstPortInfo->bSelected = MST_FALSE;

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_DISABLED;

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state DISABLED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_RESELECT,
                                     u2MstInst) != RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    /* 
     * When Module is disabled, port also considered as disabled and
     * state machine should not moved to aged for disabled port
     * Also, when SISP is enabled on port and the port is not member of the
     * VLAN and instance, spaning instance on that port must be disabled
     * Port role reselection should not be triggered even though all other
     * conditions are met
     * */

    if ((AST_IS_PORT_UP (u2PortNum)) &&
        (pRstPortInfo->bPortEnabled == MST_TRUE) &&
        ((AST_CURR_CONTEXT_INFO ())->bBegin == MST_FALSE) &&
        (AST_GET_SYSTEMACTION == MST_ENABLED) &&
        (AST_GET_SISP_STATUS (u2PortNum) == MST_FALSE))
    {
        if (MstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu, u2MstInst)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %u: MakeAged returned FAILURE",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeAged                      */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                AGED State.                                */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeAged (tAstPerStPortInfo * pPerStPortInfo,
                       tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pRstPortInfo->u1InfoIs = MST_INFOIS_AGED;
    pRstPortInfo->bReSelect = MST_TRUE;
    pRstPortInfo->bSelected = MST_FALSE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSELECT_UPDATE);

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (pPortInfo != NULL)
    {
        pPortInfo->bAllTransmitReady = MST_FALSE;
    }

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_AGED;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state AGED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    /* As the earlier received superior information is aged out, clear the 
     * same from the redundancy database. Otherwise, the old information will 
     * get applied on the standby during bulk updated and the spanning 
     * tree may misbehave for some time.*/

    MST_CLEAR_INSTANCE (u2MstInst, pPerStPortInfo->u2PortNo);

    if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_RESELECT, u2MstInst) !=
        RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    /* Checking the conditions to transition to UPDATE State
     * selected && updtInfo
     * selected will be set by the Role Selection State Machine
     * */
    if (pRstPortInfo->bUpdtInfo == MST_TRUE)
    {
        if (MstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
            MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: PortInfo Make Update returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeUpdate                    */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                UPDATE State.                              */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeUpdate (tAstPerStPortInfo * pPerStPortInfo,
                         tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStCistMstiCommInfo *pPerStCistMstiCommInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2Val = 0;
    UINT2               u2PortNum = 0;
    tAstBoolean         bBetterOrSameInfo = MST_FALSE;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: UPDATING port info \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->bProposed = MST_FALSE;
    pRstPortInfo->bProposing = MST_FALSE;

    /* Disabling transmission of inferior info in case the blocking the port
       had failed earlier.
       Now that the port has become designated it should start sending proper
       spanning tree information so that the toploogy converges. */
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    pPerStCistMstiCommInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

    bBetterOrSameInfo = MstPortInfoSmBetterOrSameInfo (pPerStPortInfo,
                                                       NULL, u2MstInst);
    if ((pRstPortInfo->bAgreed == MST_TRUE) && (bBetterOrSameInfo == MST_TRUE))
    {
        pRstPortInfo->bAgreed = MST_TRUE;
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %d: Agreed set\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }
    else
    {
        pRstPortInfo->bAgreed = MST_FALSE;
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %d: Agreed reset\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }

    if ((pRstPortInfo->bAgreed == MST_TRUE) &&
        (pRstPortInfo->bSynced == MST_TRUE))
    {
        pRstPortInfo->bSynced = MST_TRUE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "PISM_Update: Port %s Inst %u: Synced = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }
    else
    {
        pRstPortInfo->bSynced = MST_FALSE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "PISM_Update: Port %s Inst %u: Synced = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }

    /* Updating the details
     * portPriority = designatedPriority
     * portTimes = designatedTimes
     * */
    if (pPerStBrgInfo->u2RootPort == AST_INIT_VAL)
    {
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.BridgeAddr),
                    AST_MAC_ADDR_SIZE);
        pPerStPortInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStPortInfo->u4RootCost = AST_NO_VAL;

    }
    else
    {
        pPerStPortInfo->RootId = pPerStBrgInfo->RootId;
        pPerStPortInfo->u4RootCost = pPerStBrgInfo->u4RootCost;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (pCommPortInfo->bSendRstp == RST_FALSE)
        {
            /* If a port is connected to a legacy STP bridge, the port's 
             * DesignatedPriorityVector will have it's own BridgeId in the 
             * RegionalRootId field - Sec 13.10 of 802.1s */

            pPerStPortInfo->RegionalRootId.u2BrgPriority =
                (AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT))->u2BrgPriority;
            AST_MEMCPY (&(pPerStPortInfo->RegionalRootId.BridgeAddr),
                        &((AST_GET_BRGENTRY ())->BridgeAddr),
                        AST_MAC_ADDR_SIZE);
        }
        else
        {
            pPerStPortInfo->RegionalRootId =
                (AST_GET_MST_BRGENTRY ())->RegionalRootId;
        }

        pPerStPortInfo->u4IntRootPathCost =
            pPerStBrgInfo->u4CistInternalRootCost;
    }

    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.BridgeAddr),
                AST_MAC_ADDR_SIZE);
    pPerStPortInfo->DesgBrgId.u2BrgPriority =
        AST_GET_PERST_INFO (u2MstInst)->PerStBridgeInfo.u2BrgPriority;

    u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
    pPerStPortInfo->u2DesgPortId =
        (UINT2) ((AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo) &
                  AST_PORTNUM_MASK) | u2Val);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pPerStCistMstiCommInfo->u1PortRemainingHops
        = pPerStCistMstiCommInfo->u1DesgRemainingHops;
    if (u2MstInst == MST_CIST_CONTEXT)
    {
        pPortInfo->PortTimes = pPortInfo->DesgTimes;
    }

    pRstPortInfo->bUpdtInfo = MST_FALSE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSELECT_UPDATE);
    pRstPortInfo->u1InfoIs = (UINT1) MST_INFOIS_MINE;
    if (pRstPortInfo->bSelected == MST_TRUE)
    {
        pPortInfo->bAllTransmitReady = MST_TRUE;
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_TRUE;
    }
    else
    {
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti = MST_TRUE;
    }

    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_UPDTINFO_SET,
                                   pPerStPortInfo, u2MstInst) != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for UPDTINFO_SET event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for UPDTINFO_SET event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_UPDATE;

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state UPDATE \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo == MST_TRUE) ||
        (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti == MST_TRUE))
    {
        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    u2MstInst) != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeCurrent                   */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                CURRENT State.                             */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeCurrent (tAstPerStPortInfo * pPerStPortInfo,
                          tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_CURRENT;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state CURRENT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if ((pRstPortInfo->bSelected == MST_TRUE) &&
        (pRstPortInfo->bUpdtInfo == MST_TRUE))
    {
        if (MstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
            MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: PortInfo Make Update returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeSuperiorDesig             */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                SUPERIOR_DESIGNATED State.                 */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeSuperiorDesig (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStRstPortInfo *pTmpRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBoolean         bBetterOrSameInfo = MST_FALSE;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2TmpInst = 0;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: SUPERIOR_DESIGNATED message received\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bInfoInternal =
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal;
    pRstPortInfo->bProposing = MST_FALSE;
    pRstPortInfo->bAgreed = MST_FALSE;

    pRstPortInfo->bRcvdMsg = MST_FALSE;
    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    MstPortInfoSmRecordProposal (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    if (MstPortInfoSmSetTcFlags (pPerStPortInfo,
                                 pRcvdBpdu, u2MstInst) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    /* agree = agree & (betterorsameInfo(Received)
     * */
    bBetterOrSameInfo = MstPortInfoSmBetterOrSameInfo (pPerStPortInfo,
                                                       pRcvdBpdu, u2MstInst);
    if ((pRstPortInfo->bAgree == MST_TRUE) && (bBetterOrSameInfo == MST_TRUE))
    {
        pRstPortInfo->bAgree = MST_TRUE;
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %d: Agree set\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }
    else
    {
        pRstPortInfo->bAgree = MST_FALSE;
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %d: Agree reset\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }
    /* recordAgreement () */
    MstPortInfoSmRecordAgreement (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* synced = synced & agreed */
    if ((pRstPortInfo->bSynced == MST_TRUE) &&
        (pRstPortInfo->bAgreed == MST_TRUE))
    {
        pRstPortInfo->bSynced = MST_TRUE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);

        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %d: Synced set\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }
    else
    {
        pRstPortInfo->bSynced = MST_FALSE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        AST_DBG_ARG2 (AST_PISM_DBG, "PISM: Port %s: Inst %d: Synced reset\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }

    MstPortInfoSmRecordPriority (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    MstPortInfoSmRecordTimes (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    if (MstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo, pRcvdBpdu,
                                        u2MstInst) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_SUPERIOR;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state SUPERIOR_DESIGNATED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->u1InfoIs = MST_INFOIS_RECEIVED;

    pRstPortInfo->bReSelect = MST_TRUE;
    pRstPortInfo->bSelected = MST_FALSE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSELECT_UPDATE);

    if (pPortInfo != NULL)
    {
        pPortInfo->bAllTransmitReady = MST_FALSE;
    }

    /* Change the state here itself to CURRENT */
    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_CURRENT;

    /* If RcvdInfoWhile timer is not running then move to the aged state
     * untill forwarding state for the port is restricted(Loop-guard). 
     * The RcvdInfoWhile timer is restarted and the forwarding state is restricted,
     * if BPDU is not received till the expiry of edge-delay while timer*/

    if (pRstPortInfo->pRcvdInfoTmr == NULL)
    {
        if ((pPortInfo != NULL) && (pPortInfo->bLoopGuard == RST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
            (pPortInfo->bOperPointToPoint == RST_TRUE)
            && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
#ifndef L2RED_WANTED
            u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
            u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
            if (AstStartTimer (pPerStPortInfo, u2MstInst,
                               AST_TMR_TYPE_RCVDINFOWHILE, u2RiWhileDuration)
                != RST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                         "PISM: Start RcvdInfoWhile Timer returned failure\n");
                return MST_FAILURE;
            }

            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: Start RcvdInfoWhile Timer\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
        }
        /* If RcvdInfoWhile timer is not running then move to the aged state */
        else
        {
            return MstPortInfoSmRiWhileExpCurrent (pPerStPortInfo, pRcvdBpdu,
                                                   u2MstInst);
        }
    }

    if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_RESELECT, u2MstInst) !=
        MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Inst %d: Role Selection Machine returned FAILURE for RESELECT event\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    /* Will take care of triggering RTSM,for all the instances, 
     * when the received information with proposed set is 
     * from an external region */
    if (u2MstInst == MST_CIST_CONTEXT)
    {
        AST_GET_NEXT_BUF_INSTANCE (u2TmpInst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }
            pTmpPerStPortInfo =
                AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2TmpInst);
            if (pTmpPerStPortInfo != NULL)
            {
                pTmpRstPortInfo = AST_GET_RST_PORT_INFO (pTmpPerStPortInfo);

                if (pTmpRstPortInfo->bProposed == MST_TRUE)
                {
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_PROPOSED_SET, pTmpPerStPortInfo,
                         u2TmpInst) != MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        return MST_FAILURE;
                    }
                }
                if (pTmpRstPortInfo->bAgreed == MST_TRUE)
                {
                    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                                   pTmpPerStPortInfo,
                                                   u2TmpInst) != MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }
    else
    {
        /* Process the received mstiMastered flag to calculate 
         * whether to set or clear mstiMaster for this instance, 
         * before transmitting any bpdu */

        MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        if (pRstPortInfo->bProposed == MST_TRUE)
        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_PROPOSED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
        if (pRstPortInfo->bAgreed == MST_TRUE)
        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {

        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeRepeatDesig               */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                REPEATED_DESIGNATED State.                 */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeRepeatDesig (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStRstPortInfo *pTmpRstPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2TmpInst = 0;
    UINT2               u2PortNum = 0;
    UINT2               u2RiWhileDuration = AST_INIT_VAL;
    tAstPortEntry      *pPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Received REPEATED_DESIGNATED message\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bInfoInternal =
        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pRstPortInfo->bRcvdMsg = MST_FALSE;

    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* recordProposal */
    MstPortInfoSmRecordProposal (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* setTcFlags */
    if (MstPortInfoSmSetTcFlags (pPerStPortInfo,
                                 pRcvdBpdu, u2MstInst) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    /* recordAgreement */
    MstPortInfoSmRecordAgreement (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* updtRcvInfoWhile */
    if (MstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo, pRcvdBpdu, u2MstInst)
        != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "PISM: Port %s: Inst %d: PortInfo UpdtInfoWhile returned FAILURE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_REPEAT;

    /* If RcvdInfoWhile timer is not running then move to the aged state
     * untill forwarding state for the port is restricted(Loop-guard). 
     * The RcvdInfoWhile timer is restarted and the forwarding state is restricted,
     * if BPDU is not received till the expiry of edge-delay while timer*/

    if (pRstPortInfo->pRcvdInfoTmr == NULL)
    {
        if ((pPortInfo->bLoopGuard == RST_TRUE) &&
            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
            (pPortInfo->bOperPointToPoint == RST_TRUE)
            && (pAstCommPortInfo->pEdgeDelayWhileTmr == NULL))
        {
#ifndef L2RED_WANTED
            u2RiWhileDuration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#else
            u2RiWhileDuration = (UINT2) (pPortInfo->PortTimes.u2HelloTime);
#endif
            if (AstStartTimer (pPerStPortInfo, u2MstInst,
                               AST_TMR_TYPE_RCVDINFOWHILE, u2RiWhileDuration)
                != RST_SUCCESS)
            {

                AST_TRC (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                         "PISM: Start RcvdInfoWhile Timer returned failure\n");
                return MST_FAILURE;
            }

            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: Start RcvdInfoWhile Timer\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

        }
        /* If RcvdInfoWhile timer is not running then move to the aged state */
        else
        {
            return MstPortInfoSmRiWhileExpCurrent (pPerStPortInfo, pRcvdBpdu,
                                                   u2MstInst);
        }
    }

    /* Will take care for all instances when the received 
     * information with proposed set is from an external region */

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        AST_GET_NEXT_BUF_INSTANCE (u2TmpInst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }
            pTmpPerStPortInfo =
                AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2TmpInst);
            if (pTmpPerStPortInfo != NULL)
            {
                pTmpRstPortInfo = AST_GET_RST_PORT_INFO (pTmpPerStPortInfo);

                if (pTmpRstPortInfo->bProposed == MST_TRUE)
                {
                    /* According to recordAgreement procedure in IEEE 802.1q 2005,
                     * only if the Force-Version is greater than or equal to 
                     * AST_VERSION_2(rstpVersion), Role Transition needs to be 
                     * triggered here. Otherwise, Agreed must be reset.
                     */
                    if ((AST_FORCE_VERSION == AST_VERSION_0) &&
                        (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST))
                    {
                        if (pTmpPerStPortInfo->u1PortRole ==
                            pTmpPerStPortInfo->u1SelectedPortRole)
                        {
                            pTmpRstPortInfo->bProposed = MST_FALSE;
                            pTmpRstPortInfo->bAgreed = MST_FALSE;
                            continue;
                        }
                    }            /*End if (Force-Version) */
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_PROPOSED_SET, pTmpPerStPortInfo,
                         u2TmpInst) != MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        return MST_FAILURE;
                    }
                }
                if (pTmpRstPortInfo->bAgreed == MST_TRUE)
                {
                    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                                   pTmpPerStPortInfo,
                                                   u2TmpInst) != MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }
    else
    {
        /* Process the received mstiMastered flag to calculate 
         * whether to set or clear mstiMaster for this instance, 
         * before transmitting any bpdu */
        MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        if (pRstPortInfo->bProposed == MST_TRUE)
        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_PROPOSED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for PROPOSED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
        if (pRstPortInfo->bAgreed == MST_TRUE)
        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state REPEATED_DESIGNATED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {

        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeInferiorDesig             */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                INFERIOR_DESIGNATED State.                 */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeInferiorDesig (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pTempPerStPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    UINT2               u2MstInstance = 1;
    UINT2               u2PortNum = 0;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;

    AST_DBG_ARG1 (AST_PISM_DBG,
                  "PISM: Port %s: Moved to state INFERIOR_DESIGNATED \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

    pRstPortInfo->bRcvdMsg = MST_FALSE;

    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pPerStPortInfo->u1PinfoSmState = (UINT1) MST_PINFOSM_STATE_INFERIOR_DESG;

    MstPortInfoSmRecordDispute (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* Process the received mstiMastered flag to calculate
     * whether to set or clear mstiMaster for this instance,
     * before transmitting any bpdu */
    MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);

    if (pRstPortInfo->bDisputed == MST_TRUE)
    {
        if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_DISPUTED_SET,
                                       pPerStPortInfo, u2MstInst)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }

        if ((u2MstInst == MST_CIST_CONTEXT)
            && (pCistMstiPortInfo->bRcvdInternal == MST_FALSE))
        {
            AST_GET_NEXT_BUF_INSTANCE (u2MstInstance, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pTempPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                              u2MstInstance);
                if (pTempPerStPortInfo == NULL)
                {
                    continue;
                }
                if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_DISPUTED_SET,
                                               pTempPerStPortInfo,
                                               u2MstInstance) != MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum),
                                  u2MstInstance);
                    AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                  "PISM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum),
                                  u2MstInstance);
                    return MST_FAILURE;
                }
            }
        }

        AST_RED_SET_SYNC_FLAG (u2MstInst);

    }

    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeNotDesig                  */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                NOT_DESIGNATED                             */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeNotDesig (tAstPerStPortInfo * pPerStPortInfo,
                           tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStRstPortInfo *pTmpRstPortInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2TmpInst = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Received NOT_DESIG message\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pRstPortInfo->bRcvdMsg = MST_FALSE;
    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* recordAgreement */
    MstPortInfoSmRecordAgreement (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* setTcFlags () */
    if (MstPortInfoSmSetTcFlags (pPerStPortInfo,
                                 pRcvdBpdu, u2MstInst) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_NOT_DESG;

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state NOT_DESIG\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    /* Will take care for all instances when the received 
     * information with Agreed set is from an external region */

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        AST_GET_NEXT_BUF_INSTANCE (u2TmpInst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }
            pTmpPerStPortInfo =
                AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2TmpInst);
            if (pTmpPerStPortInfo != NULL)
            {
                pTmpRstPortInfo = AST_GET_RST_PORT_INFO (pTmpPerStPortInfo);

                if (pTmpRstPortInfo->bAgreed == MST_TRUE)
                {
                    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                                   pTmpPerStPortInfo,
                                                   u2TmpInst) != MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2TmpInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }
    else
    {
        /* Process the received mstiMastered flag to calculate 
         * whether to set or clear mstiMaster for this instance, 
         * before transmitting any bpdu */
        MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        if (pRstPortInfo->bAgreed == MST_TRUE)
        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_AGREED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: Role Transition Machine returned FAILURE for AGREED_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {

        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeOther                     */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                OTHER State.                               */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeOther (tAstPerStPortInfo * pPerStPortInfo,
                        tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pTempPerStPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    UINT2               u2Inst = 1;
    UINT2               u2PortNum = 0;
    UINT4               u4Ticks = 0;
    u2PortNum = pPerStPortInfo->u2PortNo;

    (AST_GET_RST_PORT_INFO (pPerStPortInfo))->bRcvdMsg = MST_FALSE;
    /* Clear RcvdAnyMsg also, since it will be true only if RcvdMsg is true 
     * */
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PISM_DBG, "PISM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* This will be used for info only and no events will occur in this state */
    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_OTHER;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state OTHER \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    /* Process the received mstiMastered flag to calculate 
     * whether to set or clear mstiMaster for this instance, 
     * before transmitting any bpdu */
    MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);

    /* Process here if the received message is a TCN Message
     * */
    if (pRcvdBpdu->u1BpduType == AST_BPDU_TYPE_TCN)
    {
        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "Port %s: RcvdTcn = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
        pCommPortInfo->bRcvdTcn = MST_TRUE;
        AST_DBG (AST_PISM_DBG,
                 "PISM: Calling Topology Change Machine with RCVDTCN event\n");

        if (MstTopologyChMachine (MST_TOPOCHSM_EV_RCVDTCN, u2MstInst,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }

        /* If TCN is received, then the adjacent switch should
         * be surely in another region. Legacy bridges are considered
         * to be a part of seperate region as per the standard
         * */
        AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }
            pTempPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
            if (pTempPerStPortInfo == NULL)
            {
                continue;
            }
            pRstPortInfo = AST_GET_RST_PORT_INFO (pTempPerStPortInfo);
            pRstPortInfo->bRcvdTc = MST_TRUE;
            MST_INCR_TC_RX_COUNT (pPerStPortInfo->u2PortNo, u2MstInst);
            AST_GET_SYS_TIME (&u4Ticks);
            pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
            if (MstTopologyChMachine (MST_TOPOCHSM_EV_RCVDTCN, u2MstInst,
                                      pTempPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTCN event\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                              "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTCN event\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    if (MstPortInfoSmMakeCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
        MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmMakeReceive                   */
/*                                                                           */
/*    Description               : This function changes the state of the     */
/*                                Port Information State Machine to          */
/*                                RECEIVE State.                             */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmMakeReceive (tAstPerStPortInfo * pPerStPortInfo,
                          tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT1               u1RcvdInfo = 0;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT1               u1RootInConState = (UINT1) AST_INIT_VAL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pRstPortInfo->bUpdtInfo != MST_FALSE)
    {

        return MST_SUCCESS;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pPortInfo == NULL)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      u2PortNum);
        return MST_FAILURE;
    }

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPortInfo->u2PortNo);

    if (pCommPortInfo == NULL)
    {
        return MST_FAILURE;
    }

    MstPortInfoSmRecordMastered (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    /* The Msti Mastered flag that is set or reset here will be processed 
     *  1) after role selection if the active MstiMastered count changes,
     *  2) in MakeSuperior,
     *  3) in MakeRepeat,
     *  3) in MakeRoot,
     *  4) in MakeOther */
    u1RcvdInfo = MstPortInfoSmRcvInfo (pPerStPortInfo, pRcvdBpdu, u2MstInst);

    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_RECEIVE;
    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Moved to state RECEIVE \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    switch (u1RcvdInfo)
    {
        case MST_SUPERIOR_DESG_MSG:

            /* If Root-Guard is enabled, on reception of Superior information
             * the port's state will be maintained in Discarding  until
             * the superior information ceases. */
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == MST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr != NULL))
            {
                /* RootInconsistent set, when the port transitions to
                 * Discarding state.*/
                pPerStPortInfo->bRootInconsistent = MST_TRUE;
                pRstPortInfo->bLearn = MST_FALSE;
                pRstPortInfo->bForward = MST_FALSE;

                if (pRstPortInfo->pRootIncRecTmr != NULL)
                {
                    /*It means Root Inconsistency timer is running, it is
                     *already in root statistency state*/
                    u1RootInConState = MST_TRUE;
                }

                /*Start the Root Inconsistency Recovery Timer */
                if (AstStartTimer ((VOID *) pPerStPortInfo, MST_CIST_CONTEXT,
                                   AST_TMR_TYPE_ROOT_INC_RECOVERY,
                                   AST_ROOT_INC_RECOVERY_TIME) != RST_SUCCESS)
                {

                    AST_TRC (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                             "PISM: Start RcvdInfoWhile Timer"
                             " returned failure\n");
                    return MST_FAILURE;
                }

                if (u1RootInConState != MST_TRUE)
                {
                    /* RootInconsistent set, when the port transitions to
                     * Discarding state.*/
                    pPerStPortInfo->bRootInconsistent = MST_TRUE;
                    pRstPortInfo->bLearn = MST_FALSE;
                    pRstPortInfo->bForward = MST_FALSE;
                    if ((MstIsPortMemberOfInst
                         ((INT4)
                          (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
                          (INT4) u2MstInst) != MST_FALSE)
                        || (u2MstInst == MST_CIST_CONTEXT))
                    {
                        AstRootIncStateChangeTrap ((UINT2)
                                                   AST_GET_IFINDEX
                                                   (pPerStPortInfo->u2PortNo),
                                                   pPerStPortInfo->
                                                   bRootInconsistent, u2MstInst,
                                                   (INT1 *) AST_MST_TRAPS_OID,
                                                   AST_MST_TRAPS_OID_LEN);
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Root_Guard_Block : Root Guard"
                                          " feature blocking Port: %s: Inst %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          u2MstInst, au1TimeStr);
                    }
                    if (RstPortStateTrMachine
                        (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition"
                                      " Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition"
                                      " Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return MST_FAILURE;
                    }
                }
                pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
                pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;

                if (MstPortInfoSmMakeCurrent
                    (pPerStPortInfo, pRcvdBpdu, u2MstInst) != MST_SUCCESS)
                {

                    AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                                  "PISM: Port %u: MakeCurrent "
                                  "returned FAILURE\n",
                                  pPerStPortInfo->u2PortNo);
                    return MST_FAILURE;
                }
                return MST_SUCCESS;
            }

            if (MstPortInfoSmMakeSuperiorDesig (pPerStPortInfo,
                                                pRcvdBpdu,
                                                u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: PortInfo MakeSuperiorDesig returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            AST_RED_SET_SYNC_FLAG (u2MstInst);
            break;

        case MST_REPEATED_DESG_MSG:

            if (MstPortInfoSmMakeRepeatDesig (pPerStPortInfo,
                                              pRcvdBpdu,
                                              u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: PortInfo MakeRepeatDesig returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
            break;

        case MST_INFERIOR_DESG_MSG:

            /* RootInconsistent is reset upon reception of inferior BPDU. */
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == MST_TRUE)
                && (pPerStPortInfo->bRootInconsistent == MST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (AstStopTimer ((VOID *) pPerStPortInfo,
                                  (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return MST_FAILURE;
                }

                pPerStPortInfo->bRootInconsistent = MST_FALSE;
                if ((MstIsPortMemberOfInst
                     ((INT4)
                      (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
                      (INT4) u2MstInst) != MST_FALSE)
                    || (u2MstInst == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_UnBlock : Root Guard feature unblocking Port: %s: Inst %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), u2MstInst,
                                      au1TimeStr);
                }
            }

            if (MstPortInfoSmMakeInferiorDesig (pPerStPortInfo,
                                                pRcvdBpdu,
                                                u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: PortInfo MakeInferiorDesig returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            break;

        case MST_INFERIOR_ROOT_ALT_MSG:
            /* RootInconsistent is reset upon reception of inferior BPDU. */
            if ((AST_PORT_ROOT_GUARD (pPortInfo) == MST_TRUE)
                && (pPerStPortInfo->bRootInconsistent == MST_TRUE))
            {
                /*Stop the root inconsistent recovery timer */
                if (AstStopTimer ((VOID *) pPerStPortInfo,
                                  (UINT1) AST_TMR_TYPE_ROOT_INC_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "PISM: Port %s: Stop Root Inconsistency Recovery"
                                  " Timer returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                }
                pPerStPortInfo->bRootInconsistent = MST_FALSE;
                if ((MstIsPortMemberOfInst
                     ((INT4)
                      (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
                      (INT4) u2MstInst) != MST_FALSE)
                    || (u2MstInst == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_UnBlock : Root Guard feature unblocking Port: %s: Inst %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), u2MstInst,
                                      au1TimeStr);
                }
            }
            if (MstPortInfoSmMakeNotDesig (pPerStPortInfo,
                                           pRcvdBpdu, u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: PortInfo MakeRoot returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            if (pRstPortInfo->u1InfoIs != (UINT1) RST_INFOIS_RECEIVED)
            {
                AST_RED_SET_SYNC_FLAG (u2MstInst);
            }
            break;

        case MST_OTHER_MSG:

            if (MstPortInfoSmMakeOther (pPerStPortInfo,
                                        pRcvdBpdu, u2MstInst) != MST_SUCCESS)
            {

                return MST_FAILURE;
            }
            break;

        default:
            return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRiWhileExpCurrent             */
/*                                                                           */
/*    Description               : Called when RecvInfoWhile Timer Expired    */
/*                                in Current State.Checks the conditions and */
/*                                moves the state to AGED State.             */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    UINT4               u4Ticks = 0;
    tAstPortEntry      *pPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    AST_INCR_RCVDINFOWHILE_COUNT (u2PortNum);
    AST_GET_SYS_TIME (&u4Ticks);
    pPortInfo->u4RcvInfoWhileExpTimeStamp = u4Ticks;

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: RCVDINFO expiry, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if ((u2MstInst == MST_CIST_CONTEXT) &&
        (pRstPortInfo->u1InfoIs == MST_INFOIS_RECEIVED) &&
        (pRstPortInfo->bUpdtInfo == MST_FALSE) &&
        (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdCistInfo == MST_FALSE))
    {

        if (MstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu, u2MstInst) !=
            MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: RiWhileExpCurrent: PortInfo MakeAged returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            return MST_FAILURE;
        }

    }
    else
    {
        if ((pRstPortInfo->u1InfoIs == MST_INFOIS_RECEIVED)
            && (pRstPortInfo->bUpdtInfo == MST_FALSE)
            && (pPerStMstiOnlyInfo->bRcvdMstiInfo == MST_FALSE))
        {
            if (MstPortInfoSmMakeAged (pPerStPortInfo, pRcvdBpdu, u2MstInst)
                != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %d: RiWhileExpCurrent: PortInfo MakeAged returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                return MST_FAILURE;
            }

        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmUpdtInfoCurrent               */
/*                                                                           */
/*    Description               : This function handles the UpdateInfo event */
/*                                in CURRENT State.                          */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
#else
INT4
MstPortInfoSmUpdtInfoCurrent (pPerStPortInfo, pRcvdBpdu, u2MstInst)
     tAstPerStPortInfo  *pPerStPortInfo;
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo->bSelected == MST_TRUE)
    {
        if (MstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu, u2MstInst)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: UpdtInfoCurrent: PortInfo MakeUpdate returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmUpdtInfoAged                  */
/*                                                                           */
/*    Description               :This function handles the UpdtInfo event    */
/*                               in the AGED State                           */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortInfoSmUpdtInfoAged (tAstPerStPortInfo * pPerStPortInfo,
                           tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
#else
INT4
MstPortInfoSmUpdtInfoAged (pPerStPortInfo, pRcvdBpdu, u2MstInst)
     tAstPerStPortInfo  *pPerStPortInfo;
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (pRstPortInfo->bSelected == MST_TRUE)
    {
        if (MstPortInfoSmMakeUpdate (pPerStPortInfo, pRcvdBpdu, u2MstInst)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %d: UpdtInfoAged: PortInfo MakeUpdate returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmUpdtRcvdInfoWhile             */
/*                                                                           */
/*    Description               : Function updates the received Info while   */
/*                                Timer and restarts the same                */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tPerStCistMstiCommInfo *pCistMstiPortInfo = NULL;
    tAstBoolean         bRcvdInternal = MST_FALSE;
    UINT2               u2EffectiveAge = 0;
    UINT2               u2PortNum = 0;
    UINT2               u2Duration = 0;

    AST_UNUSED (pRcvdBpdu);

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %d: Updating RcvdInfoWhile\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCistMstiPortInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

    bRcvdInternal = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal;

    if (bRcvdInternal == MST_FALSE)
    {
        /* Different Region */
        u2EffectiveAge = (UINT2) (pPortInfo->PortTimes.u2MsgAgeOrHopCount +
                                  (1 * AST_CENTI_SECONDS));
        if (u2EffectiveAge <= pPortInfo->PortTimes.u2MaxAge)
        {
            /* Keeping the changes done for L2RED_WANTED
             * */
            u2Duration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#ifdef L2RED_WANTED
            u2Duration = (UINT2) (u2Duration / AST_NUM_TMR_INTERVAL_SPLITS);
#endif
        }
    }
    else
    {
        /* Same Region */
        /* rcvdInternal is true, so calculate for hopcount */
        if (((pCistMstiPortInfo->u1PortRemainingHops) - 1) > 0)
        {
            u2Duration = (UINT2) (3 * pPortInfo->PortTimes.u2HelloTime);
#ifdef L2RED_WANTED
            u2Duration = (UINT2) (u2Duration / AST_NUM_TMR_INTERVAL_SPLITS);
#endif
        }
    }

    if (u2Duration != 0)
    {
        if (AstStartTimer (pPerStPortInfo, u2MstInst,
                           AST_TMR_TYPE_RCVDINFOWHILE, u2Duration)
            != RST_SUCCESS)
        {

            AST_TRC (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                     "PISM: Start RcvdInfoWhile Timer returned failure\n");
            return MST_FAILURE;
        }
    }
    else
    {
        if (pRstPortInfo->pRcvdInfoTmr != NULL)
        {
            if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_RCVDINFOWHILE) !=
                RST_SUCCESS)
            {

                AST_TRC (AST_CONTROL_PATH_TRC | AST_OS_RESOURCE_TRC,
                         "PISM: Stop RcvdInfoWhile Timer returned failure\n");
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRcvInfo                       */
/*                                                                           */
/*    Description               : This function is used by the Port          */
/*                                Information state machine to determine the */
/*                                type of received information.              */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
UINT1
MstPortInfoSmRcvInfo (tAstPerStPortInfo * pPerStPortInfo,
                      tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBridgeId        DesgBrgId;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    tPerStCistMstiCommInfo *pCistMstiPortInfo = NULL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2RcvPortNum = 0;
    UINT2               u2DesgPortNum = 0;
    UINT2               u2TmpVal = 0;
    UINT1               u1RcvdPortRole = (UINT1) AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Checking for Bridge's own Old superior Info */

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (AST_MEMCMP (&(pRcvdBpdu->CistRootId.BridgeAddr[0]),
                        &(pBrgInfo->BridgeAddr[0]), AST_MAC_ADDR_SIZE) == 0)
        {
            if (pRcvdBpdu->CistRootId.u2BrgPriority !=
                pPerStBrgInfo->u2BrgPriority)
            {
                return MST_OTHER_MSG;
            }
        }

        u1RcvdPortRole =
            (UINT1) (pRcvdBpdu->u1CistFlags & MST_FLAG_MASK_PORT_ROLE);
    }
    else
    {
        AST_MEMSET (&DesgBrgId, AST_INIT_VAL, sizeof (DesgBrgId));
        AST_MEMCPY (&(DesgBrgId.BridgeAddr),
                    &(pRcvdBpdu->CistBridgeId.BridgeAddr), AST_MAC_ADDR_SIZE);

        u2TmpVal =
            (UINT2) pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiBrgPriority;
        u2TmpVal = (UINT2) (u2TmpVal << 8);

        pMstBrgEntry = AST_GET_MST_BRGENTRY ();

        if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
        {
            u2TmpVal = u2TmpVal | u2MstInst;
        }
        DesgBrgId.u2BrgPriority = u2TmpVal;

        if (AST_MEMCMP
            (&(pRcvdBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.
               BridgeAddr[0]), &(pBrgInfo->BridgeAddr[0]),
             AST_MAC_ADDR_SIZE) == 0)
        {
            if (pRcvdBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.
                u2BrgPriority != pPerStBrgInfo->u2BrgPriority)
            {
                return MST_OTHER_MSG;
            }
        }

        u1RcvdPortRole =
            (UINT1) (pRcvdBpdu->aMstConfigMsg[u2MstInst].
                     u1MstiFlags & MST_FLAG_MASK_PORT_ROLE);
    }

    /* Sec 13.26.6 rcvInfo () 
     * NOTE::
     * A Config BPDU denotes a Designated Port Role explicitly */

    if ((u1RcvdPortRole == (UINT1) RST_SET_DESG_PROLE_FLAG) ||
        (pRcvdBpdu->u1BpduType == AST_BPDU_TYPE_CONFIG))
    {
        /* Check if the message is 'Superior Designated',
         * 'Inferior Designated' or 'Repeated Designated'.
         */
        i4RetVal = MstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu,
                                            u2MstInst);
        switch (i4RetVal)
        {
            case MST_BETTER_MSG:

                return MST_SUPERIOR_DESG_MSG;

            case MST_INFERIOR_MSG:

                /* Accept the message sent by the previous superior
                 * Switch as such
                 * */
                if (AST_MEMCMP (&(pRcvdBpdu->CistBridgeId.BridgeAddr[0]),
                                &(pPerStPortInfo->DesgBrgId.
                                  BridgeAddr[0]), AST_MAC_ADDR_SIZE) == 0)
                {
                    u2RcvPortNum =
                        (UINT2) (pRcvdBpdu->u2CistPortId & AST_PORTNUM_MASK);
                    u2DesgPortNum =
                        (UINT2) (pPerStPortInfo->
                                 u2DesgPortId & AST_PORTNUM_MASK);
                    if (u2RcvPortNum == u2DesgPortNum)
                    {
                        /* Classify inferior info received from 
                         * previous Designated port as Superior */
                        return MST_SUPERIOR_DESG_MSG;
                    }
                }

                return MST_INFERIOR_DESG_MSG;

            case MST_SAME_MSG:

                /* Message Priority Vector and Port Priority Vector are same. 
                 * Now check for the change in the Timer Values
                 * */
                pCistMstiPortInfo =
                    MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);
                if (u2MstInst == MST_CIST_CONTEXT)
                {
                    if ((pPortInfo->PortTimes.u2MaxAge != pRcvdBpdu->u2MaxAge)
                        || (pPortInfo->PortTimes.u2HelloTime
                            != pRcvdBpdu->u2HelloTime)
                        || (pPortInfo->PortTimes.u2ForwardDelay !=
                            pRcvdBpdu->u2FwdDelay)
                        || (pPortInfo->PortTimes.u2MsgAgeOrHopCount !=
                            pRcvdBpdu->u2MessageAge)
                        || (pCistMstiPortInfo->u1PortRemainingHops !=
                            pRcvdBpdu->u1CistRemainingHops))
                    {

                        return MST_SUPERIOR_DESG_MSG;
                    }
                    else
                    {
                        if (AST_GET_CIST_MSTI_PORT_INFO
                            (u2PortNum)->bInfoInternal !=
                            AST_GET_CIST_MSTI_PORT_INFO
                            (u2PortNum)->bRcvdInternal)
                        {
                            return MST_SUPERIOR_DESG_MSG;
                        }

                    }
                }
                else
                {
                    if (pCistMstiPortInfo->u1PortRemainingHops !=
                        pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiRemainingHops)
                    {
                        return MST_SUPERIOR_DESG_MSG;
                    }
                }

                /* Selection logic: 
                 * If the inferior info (other than previous ROOT info) is received in a ROOT port (particularly), 
                 * then the bridge advertises itself as ROOT bridge.
                 * Normally this scenario will happen after the max-age expiry of old ROOT information. 
                 */
                if ((pPerStBrgInfo->u2InferiorRcvdRootPort != AST_INIT_VAL)
                    && (pPerStBrgInfo->u2InferiorRcvdRootPort == u2PortNum))
                {
                    pPerStBrgInfo->u2InferiorRcvdRootPort = AST_INIT_VAL;
                    return MST_SUPERIOR_DESG_MSG;
                }

                /*Compare the Port Priority vector with the Bridge's Root Info */
                if (AST_COMPARE_BRGID (&(pPerStPortInfo->RootId),
                                       &(pPerStBrgInfo->RootId)) ==
                    RST_BRGID1_SUPERIOR)
                {
                    return MST_SUPERIOR_DESG_MSG;
                }

                /* Timer Values are also same. Check for the infoIs
                 * */
                if (pRstPortInfo->u1InfoIs == (UINT1) MST_INFOIS_RECEIVED)
                {
                    return MST_REPEATED_DESG_MSG;
                }
                else
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_BPDU_DBG | AST_PISM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "PISM: Port %s: Port's own BPDU received, LOOPBACK EXISTS!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    return MST_INFERIOR_DESG_MSG;
                }

            default:
                /* This condition is not possible */
                return MST_OTHER_MSG;
        }
    }
    /* Received Message does not convey a designated port role
     * */
    else if ((u1RcvdPortRole == (UINT1) MST_SET_ROOT_PROLE_FLAG) ||
             (u1RcvdPortRole == (UINT1) MST_SET_ALTBACK_PROLE_FLAG))
    {
        i4RetVal = MstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu,
                                            u2MstInst);
        if ((i4RetVal == MST_SAME_MSG) || (i4RetVal == MST_INFERIOR_MSG))
        {
            return MST_INFERIOR_ROOT_ALT_MSG;
        }
    }

    return MST_OTHER_MSG;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRecordAgreement               */
/*                                                                           */
/*    Description               : This function is used by the Port          */
/*                                Information state machine.                 */
/*                                If operating in CIST context and the       */
/*                                received message has Agreement flag set &  */
/*                                if sendRstp is TRUE and operP2P is TRUE,   */
/*                                then agreed is set and proposed is cleared.*/
/*                                                                           */
/*                                While operating in MSTI context, then the  */
/*                                same is done for that particular context   */
/*                                only.                                      */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
MstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pCistPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortEntry = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2Inst = 1;
    UINT4               u4Ticks = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    /* SendRSTP check */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        return;
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if ((pAstPortEntry->bOperPointToPoint == MST_TRUE) &&
            (pRcvdBpdu->u1CistFlags & MST_SET_AGREEMENT_FLAG))
        {
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: AGREEMENT Received for Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: SETTING AGREED FLAG for Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            pRstPortInfo->bAgreed = MST_TRUE;
            pRstPortInfo->bProposing = MST_FALSE;
        }
        else
        {
            pRstPortInfo->bAgreed = MST_FALSE;
        }

        if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal)
            == MST_FALSE)
        {
            AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortEntry =
                    AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortEntry != NULL)
                {
                    pPerStPortEntry->PerStRstPortInfo.bAgreed =
                        pRstPortInfo->bAgreed;
                    pPerStPortEntry->PerStRstPortInfo.bProposing =
                        pRstPortInfo->bProposing;
                    if (pRstPortInfo->bProposing == MST_TRUE)
                    {
                        MST_INCR_RX_PROPOSAL_COUNT (u2PortNum, u2MstInst);
                        AST_GET_SYS_TIME (&u4Ticks);
                        pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;
                        AST_DBG_ARG2 (AST_PISM_DBG,
                                      "PISM: SETTING PROPOSAL FLAG for Port %s and Instance %d\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2MstInst);
                    }

                }
            }
        }
    }
    else
    {
        pCistPerStPortInfo =
            AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

        if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal)
            == MST_TRUE)
        {
            if ((pAstPortEntry->bOperPointToPoint == MST_TRUE) &&
                (AST_COMPARE_BRGID (&(pRcvdBpdu->CistRootId),
                                    &(pCistPerStPortInfo->RootId)) ==
                 AST_BRGID1_SAME)
                && (pRcvdBpdu->u4CistExtPathCost ==
                    pCistPerStPortInfo->u4RootCost)
                &&
                (AST_COMPARE_BRGID
                 (&(pRcvdBpdu->CistRegionalRootId),
                  &(pCistPerStPortInfo->RegionalRootId)) == AST_BRGID1_SAME)
                && (pRcvdBpdu->aMstConfigMsg[u2MstInst].
                    u1MstiFlags & MST_SET_AGREEMENT_FLAG))

            {
                AST_DBG_ARG2 (AST_PISM_DBG,
                              "PISM: AGREEMENT Received for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_PISM_DBG,
                              "PISM: SETTING AGREED FLAG for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                MST_INCR_RX_AGREEMENT_COUNT (u2PortNum, u2MstInst);
                AST_GET_SYS_TIME (&u4Ticks);
                pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;
                pRstPortInfo->bAgreed = MST_TRUE;
                pRstPortInfo->bProposing = MST_FALSE;

            }
            else if ((AST_IS_PORT_SISP_LOGICAL (u2PortNum) == MST_TRUE) &&
                     (pAstPortEntry->bOperPointToPoint == MST_TRUE))
            {
                /* In case of SISP logical interfaces CIST Information will 
                 * never be valid. Hence, do not care about the CIST 
                 * information
                 * */
                pRstPortInfo->bAgreed = MST_TRUE;
            }
            else
            {
                pRstPortInfo->bAgreed = MST_FALSE;
            }
        }                        /*RcvdInternal Chk */
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRecordPriority                */
/*                                                                           */
/*    Description               : This function is used by the Port          */
/*                                Information state machine to record the    */
/*                                Parameters present in the received bpdu    */
/*                                to the current database for the specific   */
/*                                Instance.                                  */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstPortInfoSmRecordPriority (tAstPerStPortInfo * pPerStPortInfo,
                             tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    UINT2               u2TmpVal = 0;
    UINT2               u2TmpPortNo = 0;
    UINT2               u2MstDesgPortId = 0;

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        /* CIST ROOT Id
         * Ext Path Cost
         * RR Id
         * Internal Path Cost
         * Desig Brg Id
         * Desig Port Id
         * */
        /* Copy the CIST Message Priority Vector to the Port Priority Vector */
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &(pRcvdBpdu->CistRootId.BridgeAddr), AST_MAC_ADDR_SIZE);

        pPerStPortInfo->RootId.u2BrgPriority =
            pRcvdBpdu->CistRootId.u2BrgPriority;

        pPerStPortInfo->RegionalRootId = pRcvdBpdu->CistRegionalRootId;
        pPerStPortInfo->u4IntRootPathCost = pRcvdBpdu->u4CistIntRootPathCost;

        AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                    &(pRcvdBpdu->CistBridgeId.BridgeAddr), AST_MAC_ADDR_SIZE);
        pPerStPortInfo->DesgBrgId.u2BrgPriority
            = pRcvdBpdu->CistBridgeId.u2BrgPriority;

        pPerStPortInfo->u4RootCost = pRcvdBpdu->u4CistExtPathCost;
        pPerStPortInfo->u2DesgPortId = pRcvdBpdu->u2CistPortId;

    }
    else
    {
        AST_MEMCPY (&(pPerStPortInfo->RootId.BridgeAddr),
                    &(pRcvdBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.
                      BridgeAddr), AST_MAC_ADDR_SIZE);

        pPerStPortInfo->RootId.u2BrgPriority
            = pRcvdBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.
            u2BrgPriority;

        AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                    &(pRcvdBpdu->CistBridgeId.BridgeAddr), AST_MAC_ADDR_SIZE);

        u2TmpVal =
            (UINT2) pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiBrgPriority;
        u2TmpVal = (UINT2) (u2TmpVal << 8);
        pMstBrgEntry = AST_GET_MST_BRGENTRY ();

        if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
        {
            u2TmpVal = u2TmpVal | u2MstInst;
        }

        pPerStPortInfo->DesgBrgId.u2BrgPriority = u2TmpVal;

        pPerStPortInfo->u4RootCost =
            pRcvdBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost;

        u2MstDesgPortId =
            pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiPortPriority;
        u2MstDesgPortId =
            (UINT2) (u2MstDesgPortId << AST_PORTPRIORITY_BIT_OFFSET);
        u2TmpPortNo = (UINT2) (pRcvdBpdu->u2CistPortId & 0x0fff);
        u2MstDesgPortId = u2MstDesgPortId | u2TmpPortNo;
        pPerStPortInfo->u2DesgPortId = u2MstDesgPortId;

    }
}

/*****************************************************************************/
/* Function Name      : MstPortInfoSmRecordTimes                             */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'recordTimes' procedure as given in        */
/*                      Sec 13.26.11 of 802.1Q 2005 Edition                  */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to the port information.         */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                      u2MstInst - Instance Info                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstPortInfoSmRecordTimes (tAstPerStPortInfo * pPerStPortInfo,
                          tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPortEntry      *pPortInfo = NULL;
    tPerStCistMstiCommInfo *pCistMstiPortInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCistMstiPortInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        pPortInfo = AST_GET_PORTENTRY (u2PortNum);
        if (pPortInfo != NULL)
        {
            pPortInfo->PortTimes.u2MaxAge = pRcvdBpdu->u2MaxAge;

            if (pRcvdBpdu->u2HelloTime <=
                AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL))
            {
                pPortInfo->PortTimes.u2HelloTime =
                    AST_MGMT_TO_SYS (AST_MIN_HELLOTIME_VAL);
            }
            else
            {
                pPortInfo->PortTimes.u2HelloTime = pRcvdBpdu->u2HelloTime;
            }

            pPortInfo->PortTimes.u2ForwardDelay = pRcvdBpdu->u2FwdDelay;
            pPortInfo->PortTimes.u2MsgAgeOrHopCount = pRcvdBpdu->u2MessageAge;

            pCistMstiPortInfo->u1PortRemainingHops =
                pRcvdBpdu->u1CistRemainingHops;
        }
    }
    else
    {
        pCistMstiPortInfo->u1PortRemainingHops =
            pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiRemainingHops;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRecordProposal                */
/*                                                                           */
/*    Description               : This function is used by the Port          */
/*                                Information state machine to process the   */
/*                                proposing flag set in the received BPDU.   */
/*                                If the CIST Message has the Proposal flag  */
/*                                set, the CIST proposed flag is set.        */
/*                                Otherwise the CIST proposed flag is cleared*/
/*                                                                           */
/*                                If the received message conveys Proposal   */
/*                                set for a MSTI, then Proposed flag is set  */
/*                                for that MSTI alone, if                    */
/*                                rcvdInternal == TRUE                       */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
MstPortInfoSmRecordProposal (tAstPerStPortInfo * pPerStPortInfo,
                             tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortEntry = NULL;
    UINT2               u2PortNum = 0;
    UINT2               u2Inst = 1;
    UINT4               u4Ticks = 0;

    /* If received BPDU type is not RSTP or MSTP Type, then donot
     * proceed furthur
     * */
    if (pRcvdBpdu->u1BpduType != (UINT1) AST_BPDU_TYPE_RST)
    {
        return;
    }
    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        /* Proposal should be accepted in StpCompatible mode. It will cause newInfo
         * to be set on this port which will inturn cause a TCN to be sent out thus
         * forcing the adjacent port to step down */

        if ((pRcvdBpdu->u1CistFlags & MST_SET_PROPOSAL_FLAG) &&
            (pRcvdBpdu->u1CistFlags & MST_SET_DESG_PROLE_FLAG))
        {
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: PROPOSAL FLAG SET in the Received BPDU For Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: SETTING PROPOSED FLAG for Port %s and Instance %d\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            pRstPortInfo->bProposed = MST_TRUE;
        }

        if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal)
            == MST_FALSE)
        {
            AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortEntry =
                    AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2Inst);
                if (pPerStPortEntry != NULL)
                {
                    pPerStPortEntry->PerStRstPortInfo.bProposed =
                        pRstPortInfo->bProposed;
                }
            }
        }
    }
    else
    {
        if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal)
            == MST_TRUE)
        {
            if ((pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiFlags &
                 MST_SET_PROPOSAL_FLAG) &&
                (pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiFlags &
                 MST_SET_DESG_PROLE_FLAG))
            {
                MST_INCR_RX_PROPOSAL_COUNT (u2PortNum, u2MstInst);
                AST_GET_SYS_TIME (&u4Ticks);
                pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;
                AST_DBG_ARG2 (AST_PISM_DBG,
                              "PISM: PROPOSAL FLAG SET in the Received BPDU for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_PISM_DBG,
                              "PISM: SETTING PROPOSED FLAG for Port %s and Instance %d\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                pRstPortInfo->bProposed = MST_TRUE;

            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRecordDispute                 */
/*                                                                           */
/*    Description               : This routine performs the Port Information */
/*                                State Machine's 'recordDispute' procedure. */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                            tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    UINT2               u2PortNum;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstPerStPortInfo  *pTempPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2MstInstance = 1;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

    if (pPortInfo == NULL)
    {
        return MST_FAILURE;
    }
    if ((u2MstInst == MST_CIST_CONTEXT) &&
        (pRcvdBpdu->u1CistFlags & MST_SET_LEARNING_FLAG))
    {
        if (pPerStPortInfo->bLoopGuardStatus == AST_TRUE)
        {
            pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
            pPortInfo->bLoopInconsistent = AST_FALSE;
            pPerStPortInfo->bLoopIncStatus = AST_FALSE;
        }
        pRstPortInfo->bDisputed = MST_TRUE;
        pRstPortInfo->bAgreed = MST_FALSE;
        if ((MstIsPortMemberOfInst
             ((INT4) (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
              (INT4) u2MstInst) != MST_FALSE)
            || (u2MstInst == MST_CIST_CONTEXT))
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Disputed State occured : Port %s Instance %u at %s",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, au1TimeStr);
        }
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Instance %u: DISPUTED Set...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        if (pCistMstiPortInfo->bRcvdInternal == MST_FALSE)
        {
            AST_GET_NEXT_BUF_INSTANCE (u2MstInstance, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pTempPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                              u2MstInstance);
                if (pTempPerStPortInfo == NULL)
                {
                    continue;
                }
                pRstPortInfo = AST_GET_RST_PORT_INFO (pTempPerStPortInfo);
                if (pRstPortInfo == NULL)
                {
                    continue;
                }
                if (pTempPerStPortInfo->bLoopGuardStatus == AST_TRUE)
                {
                    pTempPerStPortInfo->bLoopGuardStatus = AST_FALSE;
                    pPortInfo->bLoopInconsistent = AST_FALSE;
                    pTempPerStPortInfo->bLoopIncStatus = AST_FALSE;
                }
                pRstPortInfo->bDisputed = MST_TRUE;
                pRstPortInfo->bAgreed = MST_FALSE;
                if ((MstIsPortMemberOfInst
                     ((INT4)
                      (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
                      (INT4) u2MstInst) != MST_FALSE)
                    || (u2MstInst == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Disputed State occured : Port %s Instance %u at %s",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), u2MstInst,
                                      au1TimeStr);
                }
                AST_DBG_ARG2 (AST_PISM_DBG,
                              "PISM: Port %s: Instance %u: DISPUTED Set...\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInstance);
            }
        }
    }
    else if ((u2MstInst != MST_CIST_CONTEXT) &&
             (pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiFlags
              & MST_SET_LEARNING_FLAG))
    {
        if (pPerStPortInfo->bLoopGuardStatus == AST_TRUE)
        {
            pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
            pPerStPortInfo->bLoopIncStatus = AST_FALSE;
            pPortInfo->bLoopInconsistent = AST_FALSE;
        }
        pRstPortInfo->bDisputed = MST_TRUE;
        pRstPortInfo->bAgreed = MST_FALSE;
        if ((MstIsPortMemberOfInst
             ((INT4) (AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo)),
              (INT4) u2MstInst) != MST_FALSE)
            || (u2MstInst == MST_CIST_CONTEXT))
        {
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Disputed State occured : Port %s Instance %u at %s",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, au1TimeStr);
        }
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Instance %u: DISPUTED Set...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }

    /*The processing of Inferior BPDU with learning flag reset behavior is
     * not mentioned in the standard IEEE 801.1D. So the below coding is the
     * implementation dependent. The behaviour implemented is as follows:
     * When the received BPDU is inferior and the learning flag is reset,
     * the state machine does not updated with the inferior information instead
     * MSTP sets  newinfo with proposal flag set and sends  proposal 
     * BPDU with the its own information to the peer.*/
    if ((u2MstInst == MST_CIST_CONTEXT) &&
        ((pRcvdBpdu->u1CistFlags & MST_SET_LEARNING_FLAG) == 0))
    {
        pRstPortInfo->bAgreed = MST_FALSE;
        pRstPortInfo->bProposing = MST_TRUE;

        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo = MST_TRUE;

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    u2MstInst) != MST_SUCCESS)
        {
            return MST_FAILURE;
        }

    }

    if ((u2MstInst != MST_CIST_CONTEXT) &&
        ((pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiFlags
          & MST_SET_LEARNING_FLAG) == 0))
    {
        pRstPortInfo->bAgreed = MST_FALSE;
        pRstPortInfo->bProposing = MST_TRUE;

        AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti = MST_TRUE;

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    u2MstInst) != MST_SUCCESS)
        {
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmBetterOrSameInfo              */
/*                                                                           */
/*    Description               : This function is used by the Port          */
/*                                Information state machine to compare two   */
/*                                priority vectors before overwriting        */
/*                                one with the other. If called from the     */
/*                                SUPERIOR state it compares rcvd Message    */
/*                                with Port Priority vector otherwise if     */
/*                                called from the UPDATE state it compares   */
/*                                the Designated Priority vector with the    */
/*                                Port Priority vector.                      */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS - If the Priority Vector       */
/*                                              is better than or the same   */
/*                                              as the port priority         */
/*                                              vector.                      */
/*                                MST_FAILURE - Otherwise.                   */
/*                                                                           */
/*****************************************************************************/
tAstBoolean
MstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                               tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstBridgeId        MyBridgeId;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    INT4                i4RetVal = 0;
    INT4                i4MsgType = 0;
    UINT2               u2MyPortId = 0;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);

    /* This version of BetterOrSameInfo does not need the NewInfoIs parameter
     * as mentioned in 802.1Q 2005. The parameter pRcvdBpdu is sufficient to 
     * identify whether the value of NewInfoIs is received or mine.
     */

    if (pRcvdBpdu != NULL)
    {
        /* Called from PIM: SUPERIOR */

        if (pRstPortInfo->u1InfoIs != MST_INFOIS_RECEIVED)
        {
            return MST_FALSE;
        }

        i4MsgType = MstPortInfoSmGetMsgType (pPerStPortInfo, pRcvdBpdu,
                                             u2MstInst);
        if ((i4MsgType == MST_SUPERIOR_DESG_MSG) || (i4MsgType == MST_SAME_MSG))
        {
            return MST_TRUE;
        }
        else
        {
            return MST_FALSE;
        }
    }
    else
    {
        /* Called from PIM: UPDATE */

        if (pRstPortInfo->u1InfoIs != MST_INFOIS_MINE)
        {
            return MST_FALSE;
        }

        if (u2MstInst == MST_CIST_CONTEXT)
        {

            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

            MyBridgeId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
            AST_MEMCPY (&(MyBridgeId.BridgeAddr),
                        &((AST_GET_BRGENTRY ())->BridgeAddr),
                        AST_MAC_ADDR_SIZE);

            u2MyPortId = (UINT2) pPerStPortInfo->u1PortPriority;
            u2MyPortId = (UINT2) (u2MyPortId << AST_PORTPRIORITY_BIT_OFFSET);
            u2MyPortId = u2MyPortId |
                AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);

            i4RetVal = AST_COMPARE_BRGID (&(pPerStBrgInfo->RootId),
                                          &(pPerStPortInfo->RootId));
            if (i4RetVal == AST_BRGID1_SUPERIOR)
            {
                return MST_TRUE;
            }
            if (i4RetVal == AST_BRGID1_INFERIOR)
            {
                return MST_FALSE;
            }

            if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
            {
                return MST_TRUE;
            }
            if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
            {
                return MST_FALSE;
            }

            pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
            if (pCommPortInfo->bSendRstp == RST_FALSE)
            {
                /* If a port is connected to a legacy STP bridge, the port's 
                 * DesignatedPriorityVector will have it's own BridgeId in the 
                 * RegionalRootId field */
                i4RetVal = AST_COMPARE_BRGID (&(MyBridgeId),
                                              &(pPerStPortInfo->
                                                RegionalRootId));
            }
            else
            {
                i4RetVal = AST_COMPARE_BRGID (&((AST_GET_MST_BRGENTRY ())->
                                                RegionalRootId),
                                              &(pPerStPortInfo->
                                                RegionalRootId));
            }
            if (i4RetVal == AST_BRGID1_SUPERIOR)
            {
                return MST_TRUE;
            }
            if (i4RetVal == AST_BRGID1_INFERIOR)
            {
                return MST_FALSE;
            }

            if (pPerStBrgInfo->u4CistInternalRootCost <
                pPerStPortInfo->u4IntRootPathCost)
            {
                return MST_TRUE;
            }
            if (pPerStBrgInfo->u4CistInternalRootCost >
                pPerStPortInfo->u4IntRootPathCost)
            {
                return MST_FALSE;
            }

            i4RetVal = AST_COMPARE_BRGID (&(MyBridgeId),
                                          &(pPerStPortInfo->DesgBrgId));
            if (i4RetVal == AST_BRGID1_SUPERIOR)
            {
                return MST_TRUE;
            }
            if (i4RetVal == AST_BRGID1_INFERIOR)
            {
                return MST_FALSE;
            }

            if (u2MyPortId < pPerStPortInfo->u2DesgPortId)
            {
                return MST_TRUE;
            }
            if (u2MyPortId > pPerStPortInfo->u2DesgPortId)
            {
                return MST_FALSE;
            }
            else
            {
                return MST_TRUE;
            }
        }
        else
        {
            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

            MyBridgeId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
            AST_MEMCPY (&(MyBridgeId.BridgeAddr),
                        &((AST_GET_BRGENTRY ())->BridgeAddr),
                        AST_MAC_ADDR_SIZE);

            u2MyPortId = (UINT2) pPerStPortInfo->u1PortPriority;
            u2MyPortId = (UINT2) (u2MyPortId << AST_PORTPRIORITY_BIT_OFFSET);
            u2MyPortId = u2MyPortId |
                AST_GET_LOCAL_PORT (pPerStPortInfo->u2PortNo);

            i4RetVal = AST_COMPARE_BRGID (&(pPerStBrgInfo->RootId),
                                          &(pPerStPortInfo->RootId));
            if (i4RetVal == AST_BRGID1_SUPERIOR)
            {
                return MST_TRUE;
            }
            if (i4RetVal == AST_BRGID1_INFERIOR)
            {
                return MST_FALSE;
            }

            if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
            {
                return MST_TRUE;
            }
            if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
            {
                return MST_FALSE;
            }

            i4RetVal = AST_COMPARE_BRGID (&(MyBridgeId),
                                          &(pPerStPortInfo->DesgBrgId));
            if (i4RetVal == AST_BRGID1_SUPERIOR)
            {
                return MST_TRUE;
            }
            if (i4RetVal == AST_BRGID1_INFERIOR)
            {
                return MST_FALSE;
            }

            if (u2MyPortId < pPerStPortInfo->u2DesgPortId)
            {
                return MST_TRUE;
            }
            if (u2MyPortId > pPerStPortInfo->u2DesgPortId)
            {
                return MST_FALSE;
            }
            else
            {
                return MST_TRUE;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmGetMsgType                    */
/*                                                                           */
/*    Description               : This routine calculates the message type   */
/*                                and returns if the message is superior or  */
/*                                inferior or same.                          */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2MstInst      - MST Instance Id for which */
/*                                                 the SEM Operates.         */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Return Value(s)           : RST_BETTER_MSG / RST_SAME_MSG /            */
/*                                RST_INFERIOR_MSG                           */
/*****************************************************************************/
INT4
MstPortInfoSmGetMsgType (tAstPerStPortInfo * pPerStPortInfo,
                         tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    UINT2               u2TmpVal;
    UINT2               u2MstiPortNo;
    UINT2               u2MstiPortId;
    tAstBridgeId        MstiDesgBrgId;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    INT4                i4RetVal = 0;

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        /* CIST Prority Vector
         * RootId
         * ExtRootPathCost
         * RegionalRootId
         * IntRootPathCost
         * DesignatedBridgeId
         * DesignatedPortId
         * RcvPortId -> Not used here
         */

        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistRootId),
                                      &(pPerStPortInfo->RootId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return MST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return MST_INFERIOR_MSG;

            default:
                break;
        }

        /* Bridge Id is same
         * */
        if (pRcvdBpdu->u4CistExtPathCost < pPerStPortInfo->u4RootCost)
        {
            return MST_BETTER_MSG;
        }
        if (pRcvdBpdu->u4CistExtPathCost > pPerStPortInfo->u4RootCost)
        {
            return MST_INFERIOR_MSG;
        }

        /* Path Cost is also same
         * */
        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistRegionalRootId),
                                      &(pPerStPortInfo->RegionalRootId));

        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return MST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return MST_INFERIOR_MSG;

            default:
                break;
        }

        if (pRcvdBpdu->u4CistIntRootPathCost <
            pPerStPortInfo->u4IntRootPathCost)
        {
            return MST_BETTER_MSG;
        }
        if (pRcvdBpdu->u4CistIntRootPathCost >
            pPerStPortInfo->u4IntRootPathCost)
        {
            return MST_INFERIOR_MSG;
        }

        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistBridgeId),
                                      &(pPerStPortInfo->DesgBrgId));

        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return MST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return MST_INFERIOR_MSG;

            default:
                break;
        }

        if (pRcvdBpdu->u2CistPortId < pPerStPortInfo->u2DesgPortId)
        {
            return MST_BETTER_MSG;
        }
        if (pRcvdBpdu->u2CistPortId > pPerStPortInfo->u2DesgPortId)
        {
            return MST_INFERIOR_MSG;
        }

        return MST_SAME_MSG;
    }
    else
    {
        /* MSTI Prority Vector
         * RootId
         * IntRootPathCost
         * DesignatedBridgeId
         * DesignatedPortId
         * RcvPortId -> Not used here
         */
        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->aMstConfigMsg[u2MstInst].
                                        MstiRegionalRootId),
                                      &(pPerStPortInfo->RootId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return MST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return MST_INFERIOR_MSG;

            default:
                break;
        }

        if (pRcvdBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost <
            (pPerStPortInfo->u4RootCost))
        {

            return MST_BETTER_MSG;

        }
        if (pRcvdBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost >
            (pPerStPortInfo->u4RootCost))
        {

            return MST_INFERIOR_MSG;
        }

        u2TmpVal =
            (UINT2) (pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiBrgPriority);
        u2TmpVal = (UINT2) (u2TmpVal << 8);
        MstiDesgBrgId.u2BrgPriority = u2TmpVal;

        pMstBrgEntry = AST_GET_MST_BRGENTRY ();

        if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
        {
            MstiDesgBrgId.u2BrgPriority =
                MstiDesgBrgId.u2BrgPriority | u2MstInst;
        }

        AST_MEMCPY (&(MstiDesgBrgId.BridgeAddr),
                    &(pRcvdBpdu->CistBridgeId.BridgeAddr), AST_MAC_ADDR_SIZE);

        i4RetVal = AST_COMPARE_BRGID (&(MstiDesgBrgId),
                                      &(pPerStPortInfo->DesgBrgId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return MST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return MST_INFERIOR_MSG;

            default:
                break;
        }

        u2MstiPortId = (UINT2) (pRcvdBpdu->aMstConfigMsg[u2MstInst].
                                u1MstiPortPriority & AST_PORTPRIORITY_MASK);
        u2MstiPortId = (UINT2) (u2MstiPortId << AST_PORTPRIORITY_BIT_OFFSET);
        u2MstiPortNo = (UINT2) (pRcvdBpdu->u2CistPortId & AST_PORTNUM_MASK);
        /* 12 bit port number */
        u2MstiPortId = u2MstiPortId | u2MstiPortNo;

        if (u2MstiPortId < pPerStPortInfo->u2DesgPortId)
        {
            return MST_BETTER_MSG;
        }
        if (u2MstiPortId > pPerStPortInfo->u2DesgPortId)
        {
            return MST_INFERIOR_MSG;
        }
        return MST_SAME_MSG;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortInfoSmRecordMastered                */
/*                                                                           */
/*    Description               : This routine clears or sets the            */
/*                                mastered flag according to the SEM         */
/*                                Procedure recordMastered flag              */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu     - Pointer to the received    */
/*                                                BPDU                       */
/*                                u2MstInst     - Instance Info              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstPortInfoSmRecordMastered (tAstPerStPortInfo * pPerStPortInfo,
                             tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    tAstBoolean         bPrevMastered = MST_FALSE;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum))->bRcvdInternal
            == MST_FALSE)
        {
            AST_DBG_ARG1 (AST_PISM_DBG,
                          "PISM: Port %s: Clearing mstiMastered for all instances\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            u2MstInst = 1;
            AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
                pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
                bPrevMastered = pPerStMstiOnlyInfo->bMastered;

                pPerStMstiOnlyInfo->bMastered = MST_FALSE;

                /* The MasteredCount is maintained only for Active Ports */
                if ((bPrevMastered == MST_TRUE) &&
                    (pPerStBrgInfo->u2MasteredCount > 0) &&
                    ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                     (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)))
                {
                    /* Reduce the count for the no. of ports 
                     * that have mstiMastered set */
                    pPerStBrgInfo->u2PrevMasteredCount =
                        pPerStBrgInfo->u2MasteredCount;
                    pPerStBrgInfo->u2MasteredCount--;
                }
            }
        }
    }
    else
    {
        pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

        if ((pAstPortEntry->bOperPointToPoint == MST_TRUE) &&
            (pRcvdBpdu->aMstConfigMsg[u2MstInst].
             u1MstiFlags & MST_FLAG_MASK_MASTER) != (UINT1) AST_NO_VAL)

        {
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: Port %s: Inst %d: Received Master flag. "
                          " Mastered set\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
            /* Master Flag set in bpdu received on P2P link */
            pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
            bPrevMastered = pPerStMstiOnlyInfo->bMastered;

            pPerStMstiOnlyInfo->bMastered = MST_TRUE;

            /* The MasteredCount is maintained only for Active Ports */
            if (bPrevMastered == MST_FALSE &&
                ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                 (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)))
            {
                /* Increase the count of the no. of ports that have 
                 * Mastered set */
                pPerStBrgInfo->u2PrevMasteredCount =
                    pPerStBrgInfo->u2MasteredCount;
                pPerStBrgInfo->u2MasteredCount++;
            }
        }
        else
        {
            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: Port %s: Inst %d: Mastered cleared\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            /* Non-P2P link or no Master flag in rcvd bpdu */
            pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
            pPerStMstiOnlyInfo->bMastered = MST_FALSE;

            /* The MasteredCount is maintained only for Active Ports */
            if ((bPrevMastered == MST_TRUE) &&
                (pPerStBrgInfo->u2MasteredCount > 0) &&
                ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                 (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)))
            {
                /* Reduce the count of the no. of ports that have 
                 * Mastered set */
                pPerStBrgInfo->u2PrevMasteredCount =
                    pPerStBrgInfo->u2MasteredCount;
                pPerStBrgInfo->u2MasteredCount--;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstProcessMstiMasteredFlag                 */
/*                                                                           */
/*    Description               : This routine calculates whether the        */
/*                                mstiMaster flag needs to be set or reset   */
/*                                for this instance based on the presence or */
/*                                absence of mstiMastered flag on active     */
/*                                ports.                                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 specific Information      */
/*                                pRcvdBpdu      - Pointer to the BPDU       */
/*                                                 Information.              */
/*                                u2MstInst      - MST Instance Number for   */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstProcessMstiMasteredFlag (tAstPerStPortInfo * pPerStPortInfo,
                            UINT2 u2InstanceId)
{
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStBridgeInfo *pCistBrgInfo = NULL;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    tAstBridgeId        MyBridgeId;
    UINT2               u2TmpPortNum;
    UINT2               u2PortNum;

    tRstPortInfo       *pRstPortEntry = NULL;
    tRstPortInfo       *pRstTmpPortEntry = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCistBrgInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);
    MyBridgeId.u2BrgPriority = pCistBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(MyBridgeId.BridgeAddr),
                &((AST_GET_BRGENTRY ())->BridgeAddr), AST_MAC_ADDR_SIZE);

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);

    /* Do everything only if the bridge has no master ports */

    if ((AST_COMPARE_BRGID (&(pCistBrgInfo->RootId),
                            &MyBridgeId) == AST_BRGID1_SAME) ||
        (AST_COMPARE_BRGID (&((AST_GET_MST_BRGENTRY ())->RegionalRootId),
                            &MyBridgeId) != AST_BRGID1_SAME))
    {
        /* This bridge is the CIST Root Bridge or 
         * it is not the CIST Regional Root for this region.
         * Hence this bridge does NOT have a Master port */

        if (pPerStBrgInfo->u2MasteredCount > pPerStBrgInfo->u2PrevMasteredCount)
        {
            /* MstiMastered set for this port -
             * mstiMaster to be set for all ports of this instance except this */

            AST_DBG_ARG1 (AST_PRSM_DBG,
                          "PRSM: Inst %d: MstiMastered set. Setting mstiMaster "
                          "for all other Root or Designated ports in this instance\n",
                          u2InstanceId);

            AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                               pRstPortEntry, tRstPortInfo *)
            {
                u2TmpPortNum = pRstPortEntry->u2PortNum;

                if (u2TmpPortNum == 0)
                {
                    continue;
                }

                if ((AST_GET_PORTENTRY (u2TmpPortNum)) == NULL)
                {
                    continue;
                }

                if (u2TmpPortNum == u2PortNum)
                {
                    continue;
                }

                pTmpPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2TmpPortNum, u2InstanceId);
                if (pTmpPerStPortInfo == NULL)
                {
                    /* Port not created or is not a member of this instance */
                    continue;
                }
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pTmpPerStPortInfo);

                if ((pTmpPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
                    (pTmpPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED))
                {
                    pPerStMstiOnlyInfo->bMaster = MST_TRUE;
                }
            }
        }

        if (pPerStBrgInfo->u2MasteredCount == 0 &&
            pPerStBrgInfo->u2PrevMasteredCount > 0)
        {
            /* None of the Active ports for this instance has mstiMastered set -
             * mstiMaster to be reset for all ports of this instance */
            AST_DBG_ARG1 (AST_PRSM_DBG,
                          "PRSM: Inst %d: MstiMastered cleared on all ports. "
                          "Clearing mstiMaster for all ports in this instance\n",
                          u2InstanceId);
            AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                               pRstTmpPortEntry, tRstPortInfo *)
            {
                u2TmpPortNum = pRstTmpPortEntry->u2PortNum;

                if (u2TmpPortNum == 0)
                {
                    continue;
                }

                if ((AST_GET_PORTENTRY (u2TmpPortNum)) == NULL)
                {
                    continue;
                }

                pTmpPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2TmpPortNum, u2InstanceId);
                if (pTmpPerStPortInfo == NULL)
                {
                    /* Port not created or is not a member of this instance */
                    continue;
                }
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pTmpPerStPortInfo);
                pPerStMstiOnlyInfo->bMaster = MST_FALSE;
            }
        }
        pPerStBrgInfo->u2PrevMasteredCount = pPerStBrgInfo->u2MasteredCount;

        /* If this is the only active port having a mstiMastered set
         * then clear the mstiMaster flag on this port. */
        pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
        if ((pPerStBrgInfo->u2MasteredCount == 1) &&
            (pPerStMstiOnlyInfo->bMastered == MST_TRUE) &&
            ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
             (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)))
        {
            pPerStMstiOnlyInfo->bMaster = MST_FALSE;
        }
    }
    else
    {
        /* If there are master ports in the bridge make the previous count 0
         * so that when the master port changes role the MasterFlag will again
         * be calculated from the MstiMastered count. */
        pPerStBrgInfo->u2PrevMasteredCount = 0;
    }
}

/*****************************************************************************/
/* Function Name      : MstPortInfoSmSetTcFlags                              */
/*                                                                           */
/* Description        : This routine performs the Port Information State     */
/*                      Machine's 'setTcFlags' procedure.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/*                      u2MstInst - Instance Info                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE                              */
/*****************************************************************************/
INT4
MstPortInfoSmSetTcFlags (tAstPerStPortInfo * pPerStPortInfo,
                         tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2Inst = 1;
    BOOL1               bRcvdInternal = MST_FALSE;
    UINT4               u4Ticks = 0;
    u2PortNum = pPerStPortInfo->u2PortNo;

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %u: Setting Tc flags\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pRcvdBpdu->u1CistFlags & RST_FLAG_MASK_TCACK) != (UINT1) AST_NO_VAL)
    {
        pCommPortInfo->bRcvdTcAck = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "SetTcFlags: Port %s: RcvdTcAck = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG (AST_PISM_DBG,
                 "PISM: Calling Topology Change Machine with RCVDTCACK event\n");

        if (MstTopologyChMachine (MST_TOPOCHSM_EV_RCVDTCACK, MST_CIST_CONTEXT,
                                  pPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                          "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    /*IEEE 802.1Q 13.26.16 setTcFlags ()
     * a) If the TcAck flag is set for the CIST in the received BPDU, sets rcvdTcAck
     *    TRUE.
     * b) If rcvdInternal is clear and the Topology Change Flag is set for the CIST
     *    in the received BPDU, sets rcvdTc TRUE for the CIST and for each and every
     *    MSTI.
     * c) If rcvdInternal is set, sets rcvdTc for the CIST if the Topology Change
     *    flag is set for the CIST in the received BPDU.
     *
     * For MSTI & Port, sets rcvdTc for this MSTI if the Topology Change Flag is set
     * in the corresponding MSTI message.
     * */

    bRcvdInternal = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bRcvdInternal;

    if (u2MstInst == MST_CIST_CONTEXT)
    {

        if ((pRcvdBpdu->u1CistFlags & RST_FLAG_MASK_TC) != (UINT1) AST_NO_VAL)
        {
            pRstPortInfo->bRcvdTc = MST_TRUE;

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "SetTcFlags: Port %s: Inst %u: RcvdTc = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            AST_DBG_ARG2 (AST_PISM_DBG,
                          "PISM: Port %s: Inst %u: Calling Topology Change Machine with RCVDTC event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (MstTopologyChMachine (MST_TOPOCHSM_EV_RCVDTC, u2MstInst,
                                      pPerStPortInfo) != RST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                              "PISM: Port %s: Inst %u: Topology Change Machine Returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            if (bRcvdInternal == MST_FALSE)
            {
                /* Other region has sent TC. Set TC for all the instances */

                AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
                {
                    pPerStInfo = AST_GET_PERST_INFO (u2Inst);
                    if (pPerStInfo == NULL)
                    {
                        continue;
                    }
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
                    if (pPerStPortInfo == NULL)
                    {
                        continue;
                    }
                    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                    pRstPortInfo->bRcvdTc = MST_TRUE;
                    MST_INCR_TC_RX_COUNT (pPerStPortInfo->u2PortNo, u2MstInst);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
                    if (MstTopologyChMachine
                        (MST_TOPOCHSM_EV_RCVDTC, u2Inst,
                         pPerStPortInfo) != RST_SUCCESS)
                    {
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTCN event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum), u2Inst);
                        AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                                      "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTCN event\n",
                                      AST_GET_IFINDEX_STR (u2PortNum), u2Inst);
                        return MST_FAILURE;
                    }
                }
            }
        }
        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %u: Successfully set Tc flags\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        return MST_SUCCESS;
    }                            /* FOR CIST */

    /* For NON-CIST instances (i.e MSTI's) */
    pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
    if (pPerStInfo != NULL)
    {
        if ((pRcvdBpdu->aMstConfigMsg[u2MstInst].
             u1MstiFlags & RST_FLAG_MASK_TC) != (UINT1) AST_NO_VAL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            if (pPerStPortInfo != NULL)
            {
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                pRstPortInfo->bRcvdTc = MST_TRUE;
                MST_INCR_TC_RX_COUNT (pPerStPortInfo->u2PortNo, u2MstInst);
                AST_GET_SYS_TIME (&u4Ticks);
                pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
                if (MstTopologyChMachine (MST_TOPOCHSM_EV_RCVDTC,
                                          u2MstInst,
                                          pPerStPortInfo) != RST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTC event\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                                  "PRSM: Port %s: Inst %d: Topology Change Machine Returned FAILURE for RCVDTC event\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG2 (AST_PISM_DBG,
                  "PISM: Port %s: Inst %u: Successfully set Tc flags\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPortInfoSmClearedBegin                            */
/*                                                                           */
/* Description        : This routine exits from the DISABLED state machine   */
/*                      state when the BEGIN global variable is cleared.     */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                                                                           */
/*                      u2MstInst - Instance Info                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPortInfoSmClearedBegin (tAstPerStPortInfo * pPerStPortInfo,
                           tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    AST_UNUSED (pRcvdBpdu);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((AST_IS_PORT_UP (pPerStPortInfo->u2PortNo)) &&
        (pRstPortInfo->bPortEnabled == MST_TRUE))
    {
        pRstPortInfo->u1InfoIs = (UINT1) MST_INFOIS_AGED;
        pRstPortInfo->bReSelect = MST_TRUE;
        pRstPortInfo->bSelected = MST_FALSE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);

        pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
        if (pPortInfo != NULL)
        {
            pPortInfo->bAllTransmitReady = MST_FALSE;
        }

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "PISM_Aged: Port %s: InfoIs = AGED Reselect = TRUE Selected = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        pPerStPortInfo->u1PinfoSmState = (UINT1) MST_PINFOSM_STATE_AGED;

        AST_DBG_ARG2 (AST_PISM_DBG,
                      "PISM: Port %s: Inst %u: Moved to state AGED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        /* Role selection is triggered once after all ports have handled
         * the BEGIN_CLEARED event so that roles need not be 
         * calculated seperately for each port */
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPortInfoSmEventImpossible                         */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to the Instance Specific    */
/*                                       port information.                   */
/*                      pRcvdBpdu - Pointer to the received BPDU.            */
/*                      u2MstInst - Instance Info                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE                                          */
/*****************************************************************************/
INT4
MstPortInfoSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst)
{
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                  "PISM: Port %s: Inst %u: IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    AST_DBG_ARG2 (AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                  "PISM: Port %s: Inst %u: IMPOSSIBLE EVENT/STATE Combination Occurred\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    AST_UNUSED (pRcvdBpdu);
    AST_UNUSED (u2MstInst);

    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != MST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PISM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return MST_FAILURE;
    }
    return MST_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MstInitPortInfoMachine                               */
/*                                                                           */
/* Description        : Initialises the Port Information State Machine       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstInitPortInfoMachine (VOID)
#else
VOID
MstInitPortInfoMachine (VOID)
#endif
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[MST_PINFOSM_MAX_EVENTS][20] = {
        "BEGIN", "BEGIN_CLEARED", "PORT_ENABLED", "PORT_DISABLED", "RCVD_BPDU",
        "RCVDINFOWHILE_EXP", "UPDATE_INFO"
    };
    UINT1               aau1SemState[MST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "AGED", "UPDATE",
        "SUPERIOR", "REPEATED", "INFERIOR",
        "NOT_DESG", "CURRENT", "RECEIVE"
    };

    for (i4Index = 0; i4Index < MST_PINFOSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PISM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PISM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < MST_PINFOSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PISM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PISM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* 802.1Q Changes */

    /* Event 0 - MST_PINFOSM_EV_BEGIN */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_DISABLED].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction = MstPortInfoSmMakeDisabled;

    /* Event 1 - MST_PINFOSM_EV_BEGIN_CLEARED */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_DISABLED].pAction = MstPortInfoSmClearedBegin;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_AGED].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_UPDATE].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_REPEAT].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_CURRENT].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_RECEIVE].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_BEGIN_CLEARED]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction = NULL;

    /* Event 2 - MST_PINFOSM_EV_PORT_ENABLED */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_DISABLED].pAction = MstPortInfoSmMakeAged;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_ENABLED]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        MstPortInfoSmEventImpossible;

    /* Event 3 - MST_PINFOSM_EV_PORT_DISABLED */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_DISABLED].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_PORT_DISABLED]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        MstPortInfoSmEventImpossible;

    /* Event 4 - MST_PINFOSM_EV_RCVD_BPDU */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_DISABLED].pAction = MstPortInfoSmMakeDisabled;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmMakeReceive;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVD_BPDU]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        MstPortInfoSmEventImpossible;

    /* Event 5 - MST_PINFOSM_EV_RCVDINFOWHILE_EXP */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_DISABLED].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmRiWhileExpCurrent;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_RCVDINFOWHILE_EXP]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        MstPortInfoSmEventImpossible;

    /* Event 6 - MST_PINFOSM_EV_UPDATEINFO */
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_DISABLED].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_AGED].pAction = MstPortInfoSmMakeUpdate;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_UPDATE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_SUPERIOR].pAction = NULL;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_REPEAT].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_NOT_DESG].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_CURRENT].pAction = MstPortInfoSmUpdtInfoCurrent;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_RECEIVE].pAction = MstPortInfoSmEventImpossible;
    MST_PORT_INFO_MACHINE[MST_PINFOSM_EV_UPDATEINFO]
        [MST_PINFOSM_STATE_INFERIOR_DESG].pAction =
        MstPortInfoSmEventImpossible;

    AST_DBG (AST_TCSM_DBG, "PISM: Loaded PISM SEM successfully\n");
    return;
}

#endif /* MSTP_WANTED */
