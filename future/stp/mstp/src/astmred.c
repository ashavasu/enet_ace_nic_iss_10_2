/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmred.c,v 1.26 2016/03/19 12:58:22 siva Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for MSTP module.   
 *
 *******************************************************************/

#include "astminc.h"

#ifdef L2RED_WANTED
#include "asthdrs.h"
#include "cli.h"

/*****************************************************************************/
/* Function Name      : MstRedHandleMstPdus                                  */
/*                                                                           */
/* Description        : Stores the latest received MST  PDUs in Standby      */
/* Input(s)           : pData - Pointer to the Received Data                 */
/*                      pu4Offset - Offset from which current data starts    */
/*                      u2Totallen - Length of data to process               */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*****************************************************************************/
INT4
MstRedHandleMstPdus (VOID *pData, UINT4 *pu4Offset, UINT2 u2TotalLen)
{
    tAstRedPdu          RedMstPdu;
    INT4                i4RetVal = RST_SUCCESS;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT2               u2ProtocolPort = AST_INIT_VAL;
    UINT2               u2InstanceId = RST_DEFAULT_INSTANCE;

    if (u2TotalLen != RED_AST_MST_BPDU_LEN)
    {
        *pu4Offset = *pu4Offset + u2TotalLen;
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC
                 | AST_MGMT_TRC | AST_RED_DBG,
                 "MstRedHandleMstPdus: Incorrect Len !\n");
        ASSERT ();
        return MST_FAILURE;
    }

    /* Get Indices */
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2InstanceId);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPort);
    AST_RM_GET_2_BYTE (pData, pu4Offset, u2ProtocolPort);

    /* Store the Data Buffer */
    AST_MEMSET (AST_RED_MST_PDU_DATA (RedMstPdu), AST_INIT_VAL,
                sizeof (tMstBpdu));

    AST_RM_GET_N_BYTE (pData, AST_RED_MST_PDU_DATA (RedMstPdu), pu4Offset,
                       sizeof (tMstBpdu));

    if ((u2InstanceId > AST_MAX_MST_INSTANCES) ||
        (u2LocalPort > AST_MAX_NUM_PORTS))
    {
        ASSERT ();
        AST_DBG_ARG2 (AST_RED_DBG, "MstRedHandleMstPdus: "
                      "Instance id %d or local port %d invalid \n",
                      u2InstanceId, u2LocalPort);
        return MST_FAILURE;
    }

    if (AST_GET_PORTENTRY (u2LocalPort) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "MstRedHandleMstPdus: No Port Entry "
                      "for local port %d\n", u2LocalPort);
        return MST_FAILURE;
    }

    if (!AST_IS_PORT_UP (u2LocalPort))
    {
        AST_DBG_ARG1 (AST_RED_DBG, "MstRedHandleMstPdus: Port Down "
                      "for port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
        return MST_FAILURE;
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is C-VLAN component message. So u2LocalPort points to the
         * CEP local port number in the parent context. */

        /* Select C-VLAN context (including Red C-VLAN context) and update
         * the u2LocalPort so that it points the local port of the port
         * whose protocol port is u2ProtocolPort. */
        i4RetVal = AstRedPbSelCvlanAndGetLocalPort (u2LocalPort,
                                                    u2ProtocolPort,
                                                    &u2LocalPort);

        if (i4RetVal == RST_FAILURE)
        {
            AST_DBG_ARG1 (AST_RED_DBG, "MstRedHandleMstPdus: "
                          "PB C-VLAN selection failed for port %s\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            return RST_FAILURE;
        }
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        AST_DBG_ARG1 (AST_RED_DBG, "MstRedHandleMstPdus: "
                      "ASTP Perst info is not present for instance %d\n",
                      u2InstanceId);
        return RST_FAILURE;
    }
    if (AST_GET_PERST_PORT_INFO (u2LocalPort, u2InstanceId) == NULL)
    {
        AST_DBG_ARG2 (AST_RED_DBG, "MstRedHandleMstPdus: "
                      "ASTP Perst port info is not present for "
                      "port %s instance %d\n",
                      AST_GET_IFINDEX_STR (u2LocalPort), u2InstanceId);
        return RST_FAILURE;
    }

    if (AST_IS_MST_ENABLED ())
    {
        /* Store MstPdu in Standby */
        MstRedStorePduInStandby (u2LocalPort,
                                 AST_RED_MST_PDU_DATA (RedMstPdu),
                                 sizeof (tMstBpdu));

        if (MstHandleInBpdu (AST_RED_MST_PDU_DATA (RedMstPdu), u2LocalPort)
            != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                          AST_RED_DBG,
                          "MstRedHandleMstPdus: Handling Mst BPDU failed "
                          "for port %s\n", AST_GET_IFINDEX_STR (u2LocalPort));
            return MST_FAILURE;
        }
    }

    if (u2ProtocolPort != AST_INIT_VAL)
    {
        /* This is a C-VLAN component message, so restore the context to
         * the parent context. */
        AstPbRestoreContext ();
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedStorePduInStandby                              */
/*                                                                           */
/* Description        : Stores the latest PDUs in Standby                    */
/* Input(s)           : u2LocalPort - Local potindex                         */
/*                    : pData - Pointer to BPDU Data                         */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedStorePduInStandby (UINT2 u2LocalPort, VOID *pData, UINT2 u2Len)
{
    tAstMstRedPdu      *pPdu = NULL;

    if (u2Len > AST_RED_DATA_MEMBLK_SIZE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Incorrect Len !\n");
        ASSERT ();
        return RST_FAILURE;
    }
    pPdu = AST_RED_MST_PDU_PTR (u2LocalPort);
    if (NULL == pPdu)
    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_DATA_MEMPOOL_ID, pPdu, tAstMstRedPdu);

        if (pPdu == NULL)
        {
            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Message Memory Block Allocation FAILED!\n");
            return RST_FAILURE;
        }
        AST_RED_MST_PDU_PTR (u2LocalPort) = pPdu;
    }
    AST_MEMSET (pPdu, AST_INIT_VAL, AST_RED_DATA_MEMBLK_SIZE);
    /* Store the Data Buffer */
    AST_MEMCPY (pPdu, pData, u2Len);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedClearPduOnActive                               */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           :                                                      */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedClearPduOnActive (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (u2PortNum > AST_MAX_NUM_PORTS)
    {
        ASSERT ();
        return MST_FAILURE;
    }

    if (NULL != (pPortEntry = AST_GET_PORTENTRY (u2PortNum)))
    {
        if (NULL != AST_RED_MST_PDU_PTR (u2PortNum))
        {
            AST_FREE_RED_MEM_BLOCK (AST_RED_MST_PDU_MEMPOOL_ID,
                                    AST_RED_MST_PDU_PTR (u2PortNum));
            AST_RED_MST_PDU_PTR (u2PortNum) = NULL;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedClearInstanceOnActive                          */
/* Description        : Clears that particular instance information          */
/*                      in the stored PDU in Active Node                     */
/* Input(s)           : u2InstanceId - Instance id which needs to be cleared */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedClearInstanceOnActive (UINT2 u2InstanceId, UINT2 u2PortNum)
{
    tAstMstRedPdu      *pRedMstPdu = NULL;

    if (u2PortNum > AST_MAX_NUM_PORTS)
    {
        ASSERT ();
        return MST_FAILURE;
    }

    pRedMstPdu = AST_RED_MST_PDU_PTR (u2PortNum);

    if (pRedMstPdu != NULL)
    {
        if (u2InstanceId == RST_DEFAULT_INSTANCE)
        {
            AST_MEMSET (pRedMstPdu, AST_INIT_VAL, sizeof (tAstRedCistPdu));
        }

        AST_MEMSET (&(pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].
                      MstiRegionalRootId), AST_INIT_VAL, sizeof (tAstBridgeId));

        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].
            u4MstiIntRootPathCost = 0;
        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiFlags = 0;
        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiBrgPriority = 0;
        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiPortPriority =
            0;
        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiRemainingHops =
            0;
        pRedMstPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1ValidFlag = 0;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstRedClearAllBpdusOnActive                */
/*                                                                           */
/*    Description               : This function clears all the stored BPDU   */
/*                                information on ACTIVE node, called when the*/
/*                                protocol is disabled or Shutdown on Active.*/
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstRedClearAllBpdusOnActive ()
#else /*  */
INT4
MstRedClearAllBpdusOnActive ()
#endif                            /*  */
{
    UINT2               u2PortNum = AST_INIT_VAL;
    for (u2PortNum = 1; u2PortNum < AST_MAX_NUM_PORTS; u2PortNum++)

    {
        MstRedClearPduOnActive (u2PortNum);
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstRedClearAllSyncUpDataInInst             */
/*                                                                           */
/*    Description               : This function clears all the stored Sync up*/
/*                                information on Standby node for an MST     */
/*                                instance, called when an Instance becomes  */
/*                                inactive on Active.                        */
/*                                                                           */
/*    Input(s)                  : u2MstInst - MST Instance identifier        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstRedClearAllSyncUpDataInInst (UINT2 u2MstInst)
{
    AST_UNUSED (u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedProtocolRestart                                */
/*                                                                           */
/* Description        : Restart MSTP protocol on a particular Port & Instance*/
/* Input(s)           : u2Port - Port                                        */
/*                      u2InstanceId - MSTP Instance identifier              */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*****************************************************************************/
INT4
MstRedProtocolRestart (UINT2 u2Port, UINT2 u2InstanceId)
{
    if (MstDisablePort (u2Port, u2InstanceId, AST_STP_PORT_DOWN) != MST_SUCCESS)

    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Disable Port Failed \n");
        return MST_FAILURE;
    }
    if (MstEnablePort (u2Port, u2InstanceId, AST_STP_PORT_UP) != MST_SUCCESS)

    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_MGMT_TRC,
                 "MSG: Enabling Port Failed \n");
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedStorePduInActive                               */
/*                                                                           */
/* Description        : Stores the latest PDUs on the Active Node            */
/* Input(s)           : u2PortNum - Port on which the PDU was received       */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/*                      u2InstanceId - Instance Identifier                   */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedStorePduInActive (UINT2 u2PortNum, tMstBpdu * pData, UINT2 u2Len,
                        UINT2 u2InstanceId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstMstRedPdu      *pRedPdu = NULL;
    AST_UNUSED (u2Len);

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        return RST_SUCCESS;
    }
    if ((u2PortNum > AST_MAX_NUM_PORTS)
        || (u2InstanceId >= AST_MAX_MST_INSTANCES))

    {
        ASSERT ();
        return MST_FAILURE;
    }

    /* If Allocated Update the fields */
    pRedPdu = AST_RED_MST_PDU_PTR (u2PortNum);
    if (pRedPdu == NULL)

    {
        AST_ALLOC_RED_MEM_BLOCK (AST_RED_MST_PDU_MEMPOOL_ID, pRedPdu,
                                 tAstMstRedPdu);

        if (pRedPdu == NULL)
        {

            /* Incrementing the Memory Failure Count */
            AST_INCR_MEMORY_FAILURE_COUNT ();
            AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Message Memory Block Allocation FAILED!\n");
            return MST_FAILURE;
        }
        MEMSET (pRedPdu, 0, sizeof (tAstMstRedPdu));
        AST_RED_MST_PDU_PTR (u2PortNum) = pRedPdu;
    }
    if (NULL == (pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)))

    {
        return MST_FAILURE;
    }

    if (AST_IS_PORT_SISP_LOGICAL (u2PortNum) == MST_TRUE)
    {
        AST_MEMSET (AST_RED_MST_PDU_PTR (u2PortNum), AST_INIT_VAL,
                    sizeof (tAstMstRedPdu));
    }

    if (u2InstanceId == MST_CIST_CONTEXT)

    {
        if (pData->u1Version == AST_VERSION_3)
        {
            AST_MEMSET (pRedPdu, AST_INIT_VAL, sizeof (tAstRedCistPdu));
            AST_MEMCPY (&pRedPdu->MstRedPdu, pData, sizeof (tAstRedCistPdu));
            return MST_SUCCESS;
        }

        else if (pData->u1Version == AST_VERSION_2)
        {
            AST_MEMSET (pRedPdu, AST_INIT_VAL, sizeof (tMstBpdu));
            AST_MEMCPY (pRedPdu, pData, sizeof (tMstBpdu));
            return MST_SUCCESS;
        }

        else if (pData->u1Version == AST_VERSION_0)
        {
            AST_MEMSET (pRedPdu, AST_INIT_VAL, sizeof (tMstBpdu));
            AST_MEMCPY (pRedPdu, pData, sizeof (tMstBpdu));
            return MST_SUCCESS;
        }

        else
        {
            ASSERT ();
            return MST_FAILURE;
        }
    }

    else
    {
        /* Check if CIST Information is present or not */
        if (pRedPdu->MstRedPdu.u2CistPortId == 0)
        {
            AST_MEMSET (pRedPdu, AST_INIT_VAL, sizeof (tAstRedCistPdu));
            AST_MEMCPY (&pRedPdu->MstRedPdu, pData, sizeof (tAstRedCistPdu));
        }
        else
        {
            AST_MEMCPY (&pRedPdu->MstRedPdu.CistBridgeId,
                        &pData->CistBridgeId, sizeof (tAstBridgeId));
            pRedPdu->MstRedPdu.u2CistPortId = pData->u2CistPortId;
        }

        AST_MEMCPY (&pRedPdu->MstRedPdu.
                    aMstConfigMsg[u2InstanceId].MstiRegionalRootId,
                    &pData->aMstConfigMsg[u2InstanceId].MstiRegionalRootId,
                    sizeof (tAstBridgeId));
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u4MstiIntRootPathCost =
            pData->aMstConfigMsg[u2InstanceId].u4MstiIntRootPathCost;
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiFlags =
            pData->aMstConfigMsg[u2InstanceId].u1MstiFlags;
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiBrgPriority =
            pData->aMstConfigMsg[u2InstanceId].u1MstiBrgPriority;
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiPortPriority =
            pData->aMstConfigMsg[u2InstanceId].u1MstiPortPriority;
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1MstiRemainingHops =
            pData->aMstConfigMsg[u2InstanceId].u1MstiRemainingHops;
        pRedPdu->MstRedPdu.aMstConfigMsg[u2InstanceId].u1ValidFlag = MST_TRUE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedSyncUpPdu                                      */
/*                                                                           */
/* Description        : Restart MSTP protocol on a particular Port & Instance*/
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/*                      u2InstanceId - Instance Identifier                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*****************************************************************************/
INT4
MstRedSyncUpPdu (UINT2 u2PortNum, tMstBpdu * pData, UINT2 u2Len)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2InstanceId = MST_CIST_CONTEXT;

    if (AST_NODE_STATUS () != RED_AST_ACTIVE)
    {
        return RST_SUCCESS;
    }

    /* As sisp ports does not participate in cist, ignore the 
     * red sync flag for cist for sisp ports */
    if (AST_IS_PORT_SISP_LOGICAL (u2PortNum) == MST_TRUE)
    {
        AST_RED_RESET_SYNC_FLAG (MST_CIST_CONTEXT);
    }

    for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES; u2InstanceId++)
    {
        if (NULL == (pPerStInfo = AST_GET_PERST_INFO (u2InstanceId)))

        {
            continue;
        }

        /* Sisp ports will not be present in all MSTI instances. So before 
         * sync up, check whether it is present in this mst instance.*/
        if (AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId) == NULL)
        {
            continue;
        }

        /* Check the Sync Flag for all the instances including CIST before 
         * storing the BPDU */

        if (OSIX_TRUE == AST_RED_SYNC_FLAG (u2InstanceId))
        {
            MstRedStorePduInActive (u2PortNum, pData, u2Len, u2InstanceId);

            AstRedSendSyncMessages (u2InstanceId, u2PortNum, RED_MST_PDU, 0);

            AST_RED_RESET_SYNC_FLAG (u2InstanceId);
        }
    }                            /* End of for u2InstanceId */
    return MST_SUCCESS;
}

VOID
RedDumpMstPdu ()
{
    UINT2               u2PortIfIndex;
    UINT2               u2InstanceId;
    tAstBpdu           *pPdu = NULL;
    tMstBpdu           *pMstBpdu;
    tAstMstRedPdu      *pAstMstBpdu = NULL;
    UINT1               au1MacAddr[21];
    UINT4               u4ContextId;

    for (u4ContextId = 0; u4ContextId < AST_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        for (u2PortIfIndex = 0; u2PortIfIndex < AST_MAX_NUM_PORTS;
             u2PortIfIndex++)

        {
            if (NULL != (pPdu = AST_RED_RST_PDU_PTR (u2PortIfIndex)))

            {
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dumping Data On Port %d\n", u2PortIfIndex);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"------------------------\n");
                for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
                     u2InstanceId++)

                {
                    if (u2InstanceId == 0)

                    {
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"CIST Instance\n");
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"RootId 0x%x:", (pPdu->RootId.u2BrgPriority));
                        PrintMacAddress ((UINT1 *) &(pPdu->RootId.BridgeAddr),
                                         au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Designated BrId 0x%x:",
                                (pPdu->DesgBrgId.u2BrgPriority));
                        PrintMacAddress ((UINT1 *)
                                         &(pPdu->DesgBrgId.BridgeAddr),
                                         au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Root path Cost %x\n",
                                (int) pPdu->u4RootPathCost);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Length %x\n", pPdu->u2Length);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Protocol Id %x\n", pPdu->u2ProtocolId);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Port Id %x\n", pPdu->u2PortId);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Message Age %x\n", pPdu->u2MessageAge);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Max Age %x\n", pPdu->u2MaxAge);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Hello Time %x\n", pPdu->u2HelloTime);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Fwd Delay Time %x \n", pPdu->u2FwdDelay);
                        PrintMacAddress ((UINT1 *) &(pPdu->DestAddr),
                                         au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dest Address %s \n", au1MacAddr);
                        PrintMacAddress ((UINT1 *) &(pPdu->SrcAddr),
                                         au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Src Address %s \n", au1MacAddr);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version Length %x \n", pPdu->u1Version1Len);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version %x \n", pPdu->u1Version);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"BPDU Type %x \n", pPdu->u1BpduType);
                        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Flags %x \n", pPdu->u1Flags);
                    }

                    else

                    {
                        pAstMstBpdu = (tAstMstRedPdu *) pPdu;
                        pMstBpdu = &pAstMstBpdu->MstRedPdu;
                        if (pMstBpdu->aMstConfigMsg[u2InstanceId].u1ValidFlag ==
                            MST_TRUE)

                        {
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Instance %d\n", u2InstanceId);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"-------\n");
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%x %x %x %x %x %x %x\n",
                                    pMstBpdu->
                                    CistBridgeId.u2BrgPriority,
                                    pMstBpdu->
                                    CistBridgeId.BridgeAddr[0],
                                    pMstBpdu->
                                    CistBridgeId.BridgeAddr[1],
                                    pMstBpdu->
                                    CistBridgeId.BridgeAddr[2],
                                    pMstBpdu->
                                    CistBridgeId.BridgeAddr[3],
                                    pMstBpdu->
                                    CistBridgeId.BridgeAddr[4],
                                    pMstBpdu->CistBridgeId.BridgeAddr[5]);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Regional Root Id %d",
                                    pMstBpdu->
                                    aMstConfigMsg[u2InstanceId].
                                    MstiRegionalRootId.u2BrgPriority);
                            PrintMacAddress ((UINT1 *)
                                             &(pMstBpdu->
                                               aMstConfigMsg[u2InstanceId].
                                               MstiRegionalRootId), au1MacAddr);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Flags %d\n",
                                    pMstBpdu->
                                    aMstConfigMsg[u2InstanceId].u1MstiFlags);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Brg Priority %d\n",
                                    pMstBpdu->
                                    aMstConfigMsg[u2InstanceId].
                                    u1MstiBrgPriority);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Port Priority %d\n",
                                    pMstBpdu->aMstConfigMsg[u2InstanceId].
                                    u1MstiPortPriority);
                            AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MST remaining Hops %d\n",
                                    pMstBpdu->aMstConfigMsg[u2InstanceId].
                                    u1MstiRemainingHops);
                        }
                    }
                }
            }
        }
        AstReleaseContext ();
    }
}
VOID
RedDumpMstPduOnPort (UINT2 u2PortIfIndex)
{
    UINT2               u2InstanceId;
    tAstBpdu           *pPdu = NULL;
    tMstBpdu           *pMstBpdu;
    tAstMstRedPdu      *pAstMstBpdu = NULL;
    UINT1               au1MacAddr[21];

    if (NULL != (pPdu = AST_RED_RST_PDU_PTR (u2PortIfIndex)))

    {
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dumping Data On Port %d\n", u2PortIfIndex);
        AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"------------------------\n");
        for (u2InstanceId = 0; u2InstanceId < AST_MAX_MST_INSTANCES;
             u2InstanceId++)

        {
            if (u2InstanceId == 0)

            {
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"CIST Instance\n");
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"RootId %x:", (pPdu->RootId.u2BrgPriority));
                PrintMacAddress ((UINT1 *) &(pPdu->RootId.BridgeAddr),
                                 au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Designated BrId %x:", (pPdu->DesgBrgId.u2BrgPriority));
                PrintMacAddress ((UINT1 *) &(pPdu->DesgBrgId.BridgeAddr),
                                 au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Root path Cost %x\n", (int) pPdu->u4RootPathCost);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Length %x\n", pPdu->u2Length);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Protocol Id %x\n", pPdu->u2ProtocolId);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Port Id %x\n", pPdu->u2PortId);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Message Age %x\n", pPdu->u2MessageAge);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Max Age %x\n", pPdu->u2MaxAge);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Hello Time %x\n", pPdu->u2HelloTime);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Fwd Delay Time %x \n", pPdu->u2FwdDelay);
                PrintMacAddress ((UINT1 *) &(pPdu->DestAddr), au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Dest Address %s \n", au1MacAddr);
                PrintMacAddress ((UINT1 *) &(pPdu->SrcAddr), au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Src Address %s \n", au1MacAddr);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version Length %x \n", pPdu->u1Version1Len);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Version %x \n", pPdu->u1Version);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"BPDU Type %x \n", pPdu->u1BpduType);
                AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Flags %x \n", pPdu->u1Flags);
            }

            else

            {
                pAstMstBpdu = (tAstMstRedPdu *) pPdu;
                pMstBpdu = &pAstMstBpdu->MstRedPdu;
                if (pMstBpdu->aMstConfigMsg[u2InstanceId].u1ValidFlag ==
                    MST_TRUE)

                {
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Instance %d\n", u2InstanceId);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"-------\n");
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%x %x %x %x %x %x %x\n",
                            pMstBpdu->
                            CistBridgeId.u2BrgPriority,
                            pMstBpdu->
                            CistBridgeId.BridgeAddr[0],
                            pMstBpdu->
                            CistBridgeId.BridgeAddr[1],
                            pMstBpdu->
                            CistBridgeId.BridgeAddr[2],
                            pMstBpdu->
                            CistBridgeId.BridgeAddr[3],
                            pMstBpdu->
                            CistBridgeId.BridgeAddr[4],
                            pMstBpdu->CistBridgeId.BridgeAddr[5]);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Regional Root Id %d",
                            pMstBpdu->
                            aMstConfigMsg[u2InstanceId].
                            MstiRegionalRootId.u2BrgPriority);
                    PrintMacAddress ((UINT1 *)
                                     &(pMstBpdu->
                                       aMstConfigMsg[u2InstanceId].
                                       MstiRegionalRootId), au1MacAddr);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"%s \n", au1MacAddr);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Flags %d\n",
                            pMstBpdu->aMstConfigMsg[u2InstanceId].u1MstiFlags);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Brg Priority %d\n",
                            pMstBpdu->
                            aMstConfigMsg[u2InstanceId].u1MstiBrgPriority);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MSTI Port Priority %d\n",
                            pMstBpdu->
                            aMstConfigMsg[u2InstanceId].u1MstiPortPriority);
                    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"MST remaining Hops %d\n",
                            pMstBpdu->
                            aMstConfigMsg[u2InstanceId].u1MstiRemainingHops);
                }
            }
        }
    }

}
#endif /*  */
