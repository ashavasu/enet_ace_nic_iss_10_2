/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
* $Id: astmplow.c,v 1.131 2018/01/09 11:02:04 siva Exp $
 *
 * Description: This file contains MST low level routines.         
 *
 *******************************************************************/

#ifdef MSTP_WANTED

# include  "astminc.h"
# include "stpcli.h"
# include "cli.h"
# include "fsmpmscli.h"

extern UINT4        fsmpms[8];
PRIVATE VOID        MstpNotifyProtocolShutdownStatus (INT4);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstSystemControl
 Input       :  The Indices

                The Object 
                retValFsMstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstSystemControl (INT4 *pi4RetValFsMstSystemControl)
{
    if (AST_IS_MST_STARTED ())
    {
        *pi4RetValFsMstSystemControl = (INT4) MST_SNMP_START;
    }
    else
    {
        *pi4RetValFsMstSystemControl = (INT4) MST_SNMP_SHUTDOWN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstModuleStatus
 Input       :  The Indices

                The Object 
                retValFsMstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstModuleStatus (INT4 *pi4RetValFsMstModuleStatus)
{
    /* In case of provider bridges, AST_IS_MST_ENABLED checks the
     * S_VLAN components module status. So based on that we can fill
     * *pi4RetValFsMstModuleStatus. So directly get the information
     * from gau1AstModuleStatus. */
    if (AST_IS_MST_STARTED ())
    {
        if ((AST_NODE_STATUS () == RED_AST_ACTIVE) ||
            (AST_NODE_STATUS () == RED_AST_STANDBY))
        {
            *pi4RetValFsMstModuleStatus =
                gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()];
            return (INT1) SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsMstModuleStatus = (INT4) AST_ADMIN_STATUS;
            return (INT1) SNMP_SUCCESS;
        }
    }
    *pi4RetValFsMstModuleStatus = (INT4) MST_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMaxMstInstanceNumber
 Input       :  The Indices

                The Object 
                retValFsMstMaxMstInstanceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMaxMstInstanceNumber (INT4 *pi4RetValFsMstMaxMstInstanceNumber)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMaxMstInstanceNumber = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsMstMaxMstInstanceNumber = AST_GET_MAX_MST_INSTANCES;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstNoOfMstiSupported
 Input       :  The Indices

                The Object 
                retValFsMstNoOfMstiSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstNoOfMstiSupported (INT4 *pi4RetValFsMstNoOfMstiSupported)
{
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstNoOfMstiSupported = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    *pi4RetValFsMstNoOfMstiSupported = pAstMstBridgeEntry->u2NoOfActiveMsti;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMaxHopCount
 Input       :  The Indices

                The Object 
                retValFsMstMaxHopCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMaxHopCount (INT4 *pi4RetValFsMstMaxHopCount)
{
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMaxHopCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    *pi4RetValFsMstMaxHopCount = (INT4)
        ((AST_SYS_TO_MGMT (pAstMstBridgeEntry->u1MaxHopCount)) *
         AST_CENTI_SECONDS);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstBrgAddress
 Input       :  The Indices

                The Object 
                retValFsMstBrgAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstBrgAddress (tMacAddr * pRetValFsMstBrgAddress)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");

        AST_MEMSET ((UINT1 *) *pRetValFsMstBrgAddress, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    AST_MEMCPY ((UINT1 *) *pRetValFsMstBrgAddress,
                &(pAstBridgeEntry->BridgeAddr), AST_MAC_ADDR_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistRoot
 Input       :  The Indices

                The Object 
                retValFsMstCistRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistRoot (tSNMP_OCTET_STRING_TYPE * pRetValFsMstCistRoot)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    UINT1              *pu1List;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");

        pu1List = pRetValFsMstCistRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    pRetValFsMstCistRoot->pu1_OctetList[0] =
        (UINT1) ((pAstPerStBridgeInfo->RootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistRoot->pu1_OctetList[1] =
        (UINT1) (pAstPerStBridgeInfo->RootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistRoot->pu1_OctetList + 2,
                &(pAstPerStBridgeInfo->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstCistRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistRegionalRoot
 Input       :  The Indices

                The Object 
                retValFsMstCistRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistRegionalRoot (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMstCistRegionalRoot)
{
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    UINT1              *pu1List;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");

        pu1List = pRetValFsMstCistRegionalRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistRegionalRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    pRetValFsMstCistRegionalRoot->pu1_OctetList[0] =
        (UINT1) ((pAstMstBridgeEntry->RegionalRootId.
                  u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistRegionalRoot->pu1_OctetList[1] =
        (UINT1) (pAstMstBridgeEntry->RegionalRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistRegionalRoot->pu1_OctetList + 2,
                &(pAstMstBridgeEntry->RegionalRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);

    pRetValFsMstCistRegionalRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistRootCost
 Input       :  The Indices

                The Object 
                retValFsMstCistRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistRootCost (INT4 *pi4RetValFsMstCistRootCost)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pi4RetValFsMstCistRootCost = pAstPerStBridgeInfo->u4RootCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistRegionalRootCost
 Input       :  The Indices

                The Object 
                retValFsMstCistRegionalRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistRegionalRootCost (INT4 *pi4RetValFsMstCistRegionalRootCost)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistRegionalRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pi4RetValFsMstCistRegionalRootCost =
        pAstPerStBridgeInfo->u4CistInternalRootCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistRootPort
 Input       :  The Indices

                The Object 
                retValFsMstCistRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistRootPort (INT4 *pi4RetValFsMstCistRootPort)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistRootPort = AST_NO_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pi4RetValFsMstCistRootPort = pAstPerStBridgeInfo->u2RootPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistBridgePriority
 Input       :  The Indices

                The Object 
                retValFsMstCistBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistBridgePriority (INT4 *pi4RetValFsMstCistBridgePriority)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistBridgePriority = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);
    if (pAstPerStBridgeInfo == NULL)
    {
        *pi4RetValFsMstCistBridgePriority = 0;
        return SNMP_FAILURE;
    }
    *pi4RetValFsMstCistBridgePriority = pAstPerStBridgeInfo->u2BrgPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistBridgeMaxAge
 Input       :  The Indices

                The Object 
                retValFsMstCistBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistBridgeMaxAge (INT4 *pi4RetValFsMstCistBridgeMaxAge)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistBridgeMaxAge = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValFsMstCistBridgeMaxAge
        = AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2MaxAge);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistBridgeForwardDelay
 Input       :  The Indices

                The Object 
                retValFsMstCistBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistBridgeForwardDelay (INT4 *pi4RetValFsMstCistBridgeForwardDelay)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistBridgeForwardDelay = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValFsMstCistBridgeForwardDelay
        = AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2ForwardDelay);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistHoldTime
 Input       :  The Indices

                The Object 
                retValFsMstCistHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistHoldTime (INT4 *pi4RetValFsMstCistHoldTime)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistHoldTime = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstCistHoldTime = (INT4) AST_SYS_TO_MGMT (AST_HOLD_TIME);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistMaxAge
 Input       :  The Indices

                The Object 
                retValFsMstCistMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistMaxAge (INT4 *pi4RetValFsMstCistMaxAge)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistMaxAge = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstCistMaxAge = AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2MaxAge);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistForwardDelay
 Input       :  The Indices

                The Object 
                retValFsMstCistForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistForwardDelay (INT4 *pi4RetValFsMstCistForwardDelay)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistForwardDelay = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstCistForwardDelay
        = AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2ForwardDelay);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstpUpCount
 Input       :  The Indices

                The Object 
                retValFsMstMstpUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstpUpCount (UINT4 *pu4RetValFsMstMstpUpCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstMstpUpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    *pu4RetValFsMstMstpUpCount = pAstBridgeEntry->u4AstpUpCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstpDownCount
 Input       :  The Indices

                The Object 
                retValFsMstMstpDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstpDownCount (UINT4 *pu4RetValFsMstMstpDownCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstMstpDownCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    *pu4RetValFsMstMstpDownCount = pAstBridgeEntry->u4AstpDownCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstTrace
 Input       :  The Indices

                The Object 
                retValFsMstTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstTrace (INT4 *pi4RetValFsMstTrace)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstTrace = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstTrace = AST_TRACE_OPTION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstDebug
 Input       :  The Indices

                The Object 
                retValFsMstDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstDebug (INT4 *pi4RetValFsMstDebug)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstDebug = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsMstDebug = (INT4) AST_DEBUG_OPTION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstForceProtocolVersion
 Input       :  The Indices

                The Object 
                retValFsMstForceProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstForceProtocolVersion (INT4 *pi4RetValFsMstForceProtocolVersion)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstForceProtocolVersion = AST_VERSION_3;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstForceProtocolVersion = AST_FORCE_VERSION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstTxHoldCount
 Input       :  The Indices

                The Object 
                retValFsMstTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstTxHoldCount (INT4 *pi4RetValFsMstTxHoldCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstTxHoldCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();

    *pi4RetValFsMstTxHoldCount = (INT4) pAstBridgeEntry->u1TxHoldCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiConfigIdSel
 Input       :  The Indices

                The Object 
                retValFsMstMstiConfigIdSel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiConfigIdSel (INT4 *pi4RetValFsMstMstiConfigIdSel)
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiConfigIdSel = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    *pi4RetValFsMstMstiConfigIdSel = pMstConfigIdInfo->u1ConfigurationId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiRegionName
 Input       :  The Indices

                The Object 
                retValFsMstMstiRegionName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiRegionName (tSNMP_OCTET_STRING_TYPE * pRetValFsMstMstiRegionName)
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    UINT1              *pu1List;

    pu1List = pRetValFsMstMstiRegionName->pu1_OctetList;
    AST_MEMSET (pu1List, AST_INIT_VAL, MST_CONFIG_NAME_LEN);
    pRetValFsMstMstiRegionName->i4_Length = MST_CONFIG_NAME_LEN;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_SUCCESS;
    }

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    pRetValFsMstMstiRegionName->i4_Length = MST_CONFIG_NAME_LEN;

    AST_MEMCPY (pRetValFsMstMstiRegionName->pu1_OctetList,
                pMstConfigIdInfo->au1ConfigName,
                pRetValFsMstMstiRegionName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiRegionVersion
 Input       :  The Indices

                The Object 
                retValFsMstMstiRegionVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiRegionVersion (INT4 *pi4RetValFsMstMstiRegionVersion)
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiRegionVersion = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    *pi4RetValFsMstMstiRegionVersion = pMstConfigIdInfo->u2ConfigRevLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiConfigDigest
 Input       :  The Indices

                The Object 
                retValFsMstMstiConfigDigest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiConfigDigest (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMstMstiConfigDigest)
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    UINT1              *pu1List;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");

        pu1List = pRetValFsMstMstiConfigDigest->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, MST_CONFIG_DIGEST_LEN);

        pRetValFsMstMstiConfigDigest->i4_Length = AST_INIT_VAL;

        return SNMP_SUCCESS;
    }

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    pRetValFsMstMstiConfigDigest->i4_Length = MST_CONFIG_DIGEST_LEN;

    AST_MEMCPY (pRetValFsMstMstiConfigDigest->pu1_OctetList,
                pMstConfigIdInfo->au1ConfigDigest,
                pRetValFsMstMstiConfigDigest->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstBufferOverFlowCount
 Input       :  The Indices

                The Object 
                retValFsMstBufferOverFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstBufferOverFlowCount (UINT4 *pu4RetValFsMstBufferOverFlowCount)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstBufferOverFlowCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsMstBufferOverFlowCount = gAstGlobalInfo.u4BufferFailureCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMemAllocFailureCount
 Input       :  The Indices

                The Object 
                retValFsMstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMemAllocFailureCount (UINT4 *pu4RetValFsMstMemAllocFailureCount)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstMemAllocFailureCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsMstMemAllocFailureCount = gAstGlobalInfo.u4MemoryFailureCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstRegionConfigChangeCount
 Input       :  The Indices

                The Object 
                retValFsMstRegionConfigChangeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstRegionConfigChangeCount (UINT4
                                    *pu4RetValFsMstRegionConfigChangeCount)
{
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstRegionConfigChangeCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    *pu4RetValFsMstRegionConfigChangeCount =
        pMstBridgeEntry->u4RegionCfgChangeCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistBridgeRoleSelectionSemState
 Input       :  The Indices

                The Object 
                retValFsMstCistBridgeRoleSelectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMstCistBridgeRoleSelectionSemState
    (INT4 *pi4RetValFsMstCistBridgeRoleSelectionSemState)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistBridgeRoleSelectionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pi4RetValFsMstCistBridgeRoleSelectionSemState =
        pAstPerStBridgeInfo->u1ProleSelSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistTimeSinceTopologyChange
 Input       :  The Indices

                The Object 
                retValFsMstCistTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistTimeSinceTopologyChange (UINT4
                                        *pu4RetValFsMstCistTimeSinceTopologyChange)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tAstSysTime         CurrSysTime;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistTimeSinceTopologyChange = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    AST_MEMSET (&CurrSysTime, AST_MEMSET_VAL, sizeof (tAstSysTime));

    if (pAstPerStBridgeInfo->u4NumTopoCh != 0)
    {
        AST_GET_SYS_TIME (&CurrSysTime);

        *pu4RetValFsMstCistTimeSinceTopologyChange =
            (UINT4) (CurrSysTime -
                     (tAstSysTime) pAstPerStBridgeInfo->u4TimeSinceTopoCh);
        *pu4RetValFsMstCistTimeSinceTopologyChange =
            AST_CONVERT_STUPS_2_CENTI_SEC
            (*pu4RetValFsMstCistTimeSinceTopologyChange);
        *pu4RetValFsMstCistTimeSinceTopologyChange =
            AST_MGMT_TO_SYS (*pu4RetValFsMstCistTimeSinceTopologyChange);
    }
    else
    {
        *pu4RetValFsMstCistTimeSinceTopologyChange = 0;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistTopChanges
 Input       :  The Indices

                The Object 
                retValFsMstCistTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistTopChanges (UINT4 *pu4RetValFsMstCistTopChanges)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistTopChanges = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pu4RetValFsMstCistTopChanges = pAstPerStBridgeInfo->u4NumTopoCh;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistHelloTime
 Input       :  The Indices

                The Object 
                retValFsMstCistHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistHelloTime (INT4 *pi4RetValFsMstCistHelloTime)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    pBrgInfo = AST_GET_BRGENTRY ();

    if (!AST_IS_MST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module is not Enabled!\n");
        *pi4RetValFsMstCistHelloTime =
            (INT4) AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2HelloTime);
        return SNMP_SUCCESS;
    }
    *pi4RetValFsMstCistHelloTime =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2HelloTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistBridgeHelloTime
 Input       :  The Indices

                The Object 
                retValFsMstCistBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistBridgeHelloTime (INT4 *pi4RetValFsMstCistBridgeHelloTime)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistBridgeHelloTime = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValFsMstCistBridgeHelloTime
        = AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2HelloTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistNewRootBridgeCount
 Input       :  The Indices

                The Object 
                retValFsMstCistNewRootBridgeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistNewRootBridgeCount (UINT4 *pu4RetValFsMstCistNewRootBridgeCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistNewRootBridgeCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pu4RetValFsMstCistNewRootBridgeCount =
        pAstPerStBridgeInfo->u4NewRootIdCount;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstSystemControl
 Input       :  The Indices

                The Object 
                setValFsMstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstSystemControl (INT4 i4SetValFsMstSystemControl)
{
    tAstMsgNode        *pNode = NULL;
    UINT4               u4MsgType = AST_INIT_VAL;
    UINT4               u4SeqNum = 0;
    UINT4               u4IcclIfIndex = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (i4SetValFsMstSystemControl == (INT4) MST_SNMP_START)
    {
        if (AST_IS_MST_STARTED ())
        {
            AST_TRC (AST_MGMT_TRC, "MGMT: MSTP is already Started.\n");

            return SNMP_SUCCESS;
        }
        u4MsgType = AST_START_MST_MSG;
    }
    else
    {
        if (!AST_IS_MST_STARTED ())
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "SNMP: MSTP Module is not started to shut down !!!\n");

            return SNMP_SUCCESS;
        }
        u4MsgType = AST_STOP_MST_MSG;

    }
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pNode->MsgType = u4MsgType;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSystemControl, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstSystemControl));

    /* When MCLAG is enabled and ICCL interface is present,
     * MSTP is disabled on the ICCL interface and on the MC-LAG
     * interface*/
    if ((AST_IS_MST_STARTED ()) &&
        (AstLaGetMCLAGSystemStatus () == AST_MCLAG_ENABLED))
    {
        AstIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex != 0)
        {
            AST_UNLOCK ();
            /* Disable MST on ICCL */
            if (AstDisableStpOnPort (u4IcclIfIndex) != OSIX_SUCCESS)
            {
                i1RetVal = SNMP_FAILURE;
            }
            /* Disable MST on MC-LAG interfaces */
            AstPortLaDisableOnMcLagIf ();
            AST_LOCK ();
        }
    }

    AstSelectContext (u4CurrContextId);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4SetValFsMstSystemControl == (INT4) MST_SNMP_SHUTDOWN)
        {
            /* Notify MSR with the Mstp oids */
            MstpNotifyProtocolShutdownStatus (AST_CURR_CONTEXT_ID ());
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstModuleStatus
 Input       :  The Indices

                The Object 
                setValFsMstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstModuleStatus (INT4 i4SetValFsMstModuleStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((i4SetValFsMstModuleStatus ==
         (INT4) gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()])
        && (i4SetValFsMstModuleStatus == (INT4) AST_ADMIN_STATUS))
    {
        AST_TRC (AST_MGMT_TRC, "MGMT: RSTP already has the same Status\n");
        return (INT1) SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstModuleStatus == MST_ENABLED)
    {
        pAstMsgNode->MsgType = AST_ENABLE_MST_MSG;
    }
    else
    {
        pAstMsgNode->MsgType = AST_DISABLE_MST_MSG;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstModuleStatus, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstModuleStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMaxMstInstanceNumber
 Input       :  The Indices

                The Object 
                setValFsMstMaxMstInstanceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMaxMstInstanceNumber (INT4 i4SetValFsMstMaxMstInstanceNumber)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_MAX_INST_NUMBER_MSG;
    pAstMsgNode->uMsg.u2MaxInstNumber =
        (UINT2) i4SetValFsMstMaxMstInstanceNumber;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMaxMstInstanceNumber, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstMaxMstInstanceNumber));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMaxHopCount
 Input       :  The Indices

                The Object 
                setValFsMstMaxHopCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMaxHopCount (INT4 i4SetValFsMstMaxHopCount)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_MAX_HOP_COUNT_MSG;
    pAstMsgNode->uMsg.u1MaxHopCount = (UINT1)
        (i4SetValFsMstMaxHopCount / AST_CENTI_SECONDS);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMaxHopCount, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstMaxHopCount));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistBridgePriority
 Input       :  The Indices

                The Object 
                setValFsMstCistBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistBridgePriority (INT4 i4SetValFsMstCistBridgePriority)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_BRIDGE_PRIORITY_MSG;
    pAstMsgNode->uMsg.u2BridgePriority =
        (UINT2) i4SetValFsMstCistBridgePriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistBridgePriority, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCistBridgePriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistBridgeMaxAge
 Input       :  The Indices

                The Object 
                setValFsMstCistBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistBridgeMaxAge (INT4 i4SetValFsMstCistBridgeMaxAge)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_MAXAGE_MSG;
    pAstMsgNode->uMsg.u2MaxAge =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsMstCistBridgeMaxAge);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistBridgeMaxAge, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCistBridgeMaxAge));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistBridgeForwardDelay
 Input       :  The Indices

                The Object 
                setValFsMstCistBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistBridgeForwardDelay (INT4 i4SetValFsMstCistBridgeForwardDelay)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    AstSelectContext (u4CurrContextId);

    pAstMsgNode->MsgType = AST_FORWARDDELAY_MSG;
    pAstMsgNode->uMsg.u2ForwardDelay =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsMstCistBridgeForwardDelay);
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);
    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistBridgeForwardDelay,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCistBridgeForwardDelay));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstTrace
 Input       :  The Indices

                The Object 
                setValFsMstTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstTrace (INT4 i4SetValFsMstTrace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_TRACE_OPTION = (UINT4) i4SetValFsMstTrace;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Management SET of Trace Option as %d Success\n",
                  i4SetValFsMstTrace);

    AST_DEBUG_OPTION = (UINT4) AST_INIT_VAL;
    AST_DBG (AST_MGMT_DBG, "SNMP: Resetting the Debug Option...\n");

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstTrace, u4SeqNum, FALSE,
                          AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstTrace));
    AstSelectContext (u4CurrContextId);
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMstDebug
 Input       :  The Indices

                The Object 
                setValFsMstDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstDebug (INT4 i4SetValFsMstDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_DEBUG_OPTION = (UINT4) i4SetValFsMstDebug;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Management SET of Debug Option as %d Success\n",
                  i4SetValFsMstDebug);

    AST_TRACE_OPTION = AST_INIT_VAL;
    AST_DBG (AST_MGMT_DBG, "SNMP: Resetting the Trace Option...\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstDebug, u4SeqNum, FALSE,
                          AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstDebug));
    AstSelectContext (u4CurrContextId);
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMstForceProtocolVersion
 Input       :  The Indices

                The Object 
                setValFsMstForceProtocolVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstForceProtocolVersion (INT4 i4SetValFsMstForceProtocolVersion)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_FORCE_VERSION_MSG;
    pAstMsgNode->uMsg.u1ForceVersion =
        (UINT1) i4SetValFsMstForceProtocolVersion;
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstForceProtocolVersion, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstForceProtocolVersion));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstTxHoldCount
 Input       :  The Indices

                The Object 
                setValFsMstTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstTxHoldCount (INT4 i4SetValFsMstTxHoldCount)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_TX_HOLDCOUNT_MSG;
    pAstMsgNode->uMsg.u1TxHoldCount = (UINT1) i4SetValFsMstTxHoldCount;
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstTxHoldCount, u4SeqNum, FALSE,
                          AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstTxHoldCount));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiConfigIdSel
 Input       :  The Indices

                The Object 
                setValFsMstMstiConfigIdSel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiConfigIdSel (INT4 i4SetValFsMstMstiConfigIdSel)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_CONFIG_ID_MSG;
    pAstMsgNode->uMsg.u1ConfigIdSel = (UINT1) i4SetValFsMstMstiConfigIdSel;
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiConfigIdSel, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstMstiConfigIdSel));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiRegionName
 Input       :  The Indices

                The Object 
                setValFsMstMstiRegionName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiRegionName (tSNMP_OCTET_STRING_TYPE * pSetValFsMstMstiRegionName)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1               au1ConfigName[MST_CONFIG_NAME_LEN];
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AST_MEMSET (au1ConfigName, AST_INIT_VAL, MST_CONFIG_NAME_LEN);
    AST_MEMCPY (au1ConfigName, pSetValFsMstMstiRegionName->pu1_OctetList,
                pSetValFsMstMstiRegionName->i4_Length);

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    /* Compare the existing MST region name, 
     * if both configuring and existing are same return from here */
    if (AST_MEMCMP (pMstConfigIdInfo->au1ConfigName, au1ConfigName,
                    MST_CONFIG_NAME_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_REGION_NAME_MSG;
    AST_MEMCPY (pAstMsgNode->uMsg.ConfigName.au1Octets,
                pSetValFsMstMstiRegionName->pu1_OctetList,
                pSetValFsMstMstiRegionName->i4_Length);
    pAstMsgNode->uMsg.ConfigName.u2OctLen =
        (UINT2) pSetValFsMstMstiRegionName->i4_Length;
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiRegionName, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", AST_CURR_CONTEXT_ID (),
                      pSetValFsMstMstiRegionName));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiRegionVersion
 Input       :  The Indices

                The Object 
                setValFsMstMstiRegionVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiRegionVersion (INT4 i4SetValFsMstMstiRegionVersion)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_REGION_VERSION_MSG;
    pAstMsgNode->uMsg.u2ConfigRevLevel = (UINT2) i4SetValFsMstMstiRegionVersion;
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiRegionVersion, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstMstiRegionVersion));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistBridgeHelloTime
 Input       :  The Indices

                The Object 
                setValFsMstCistBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistBridgeHelloTime (INT4 i4SetValFsMstCistBridgeHelloTime)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_HELLOTIME_MSG;
    pAstMsgNode->uMsg.u2HelloTime =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsMstCistBridgeHelloTime);
    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistBridgeHelloTime, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCistBridgeHelloTime));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMstSystemControl
 Input       :  The Indices

                The Object 
                testValFsMstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMstSystemControl)
{
    UINT4               u4BridgeMode = AST_INVALID_BRIDGE_MODE;

    if (AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module already Enabled.. Shutdown RSTP and Start MSTP!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_IS_PVRST_STARTED ())
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: PVRST Module already Enabled.. Shutdown PVRST and Start MSTP!\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstSystemControl != (INT4) MST_SNMP_START) &&
        (i4TestValFsMstSystemControl != (INT4) MST_SNMP_SHUTDOWN))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsMstSystemControl == (INT4) MST_SNMP_START)
    {
        /* Check whether the bridge mode is set. If not set
         * then return failure. */
        if (AstL2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &u4BridgeMode)
            != L2IWF_SUCCESS)
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        if (u4BridgeMode == AST_INVALID_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_STP_BRG_MODE_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
        if (u4BridgeMode == AST_ICOMPONENT_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_STP_MST_BRG_INVALID_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_STP_BASE_BRIDGE_MST_ENABLED);
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstModuleStatus
 Input       :  The Indices

                The Object 
                testValFsMstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsMstModuleStatus)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: MSTP Module not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstModuleStatus != MST_ENABLED) &&
        (i4TestValFsMstModuleStatus != MST_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMaxMstInstanceNumber
 Input       :  The Indices

                The Object 
                testValFsMstMaxMstInstanceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMaxMstInstanceNumber (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsMstMaxMstInstanceNumber)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMstMaxMstInstanceNumber < AST_MIN_MST_INSTANCES) ||
        (i4TestValFsMstMaxMstInstanceNumber >= AST_MAX_MST_INSTANCES))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_MAX_INST_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValFsMstMaxMstInstanceNumber < AST_GET_NO_OF_ACTIVE_INSTANCES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_MAX_INST_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMaxHopCount
 Input       :  The Indices

                The Object 
                testValFsMstMaxHopCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMaxHopCount (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstMaxHopCount)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMstMaxHopCount < AST_MIN_HOP_COUNT) ||
        (i4TestValFsMstMaxHopCount > AST_MAX_HOP_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstMaxHopCount) != AST_OK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistBridgePriority
 Input       :  The Indices

                The Object 
                testValFsMstCistBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistBridgePriority (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsMstCistBridgePriority)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistBridgePriority < AST_MIN_BRIDGE_PRIORITY) ||
        (i4TestValFsMstCistBridgePriority > AST_MAX_BRIDGE_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMstCistBridgePriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistBridgeMaxAge
 Input       :  The Indices

                The Object 
                testValFsMstCistBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistBridgeMaxAge (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMstCistBridgeMaxAge)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortIndex;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstCistBridgeMaxAge) != AST_OK)
        || (i4TestValFsMstCistBridgeMaxAge < AST_MIN_MAXAGE_VAL)
        || (i4TestValFsMstCistBridgeMaxAge > AST_MAX_MAXAGE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!(AST_BRG_FWDDELAY_MAXAGE_STD
          (pBrgInfo->BridgeTimes.u2ForwardDelay,
           AST_MGMT_TO_SYS (i4TestValFsMstCistBridgeMaxAge))))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
    u2PortIndex = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortIndex, pAstPortEntry)
    {
        if (pAstPortEntry != NULL)
        {
            if (!(AST_BRG_HELLOTIME_MAXAGE_STD
                  (pAstPortEntry->u2HelloTime,
                   AST_MGMT_TO_SYS (i4TestValFsMstCistBridgeMaxAge))))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistBridgeForwardDelay
 Input       :  The Indices

                The Object 
                testValFsMstCistBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMstCistBridgeForwardDelay)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstCistBridgeForwardDelay) !=
         AST_OK)
        || (i4TestValFsMstCistBridgeForwardDelay < AST_MIN_FWDDELAY_VAL)
        || (i4TestValFsMstCistBridgeForwardDelay > AST_MAX_FWDDELAY_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_BRG_FWDDELAY_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValFsMstCistBridgeForwardDelay),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsMstTrace
 Input       :  The Indices

                The Object 
                testValFsMstTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstTrace (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstTrace)
{
    if ((i4TestValFsMstTrace < AST_MIN_TRACE_VAL) ||
        (i4TestValFsMstTrace > AST_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstDebug
 Input       :  The Indices

                The Object 
                testValFsMstDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstDebug)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstDebug < AST_MIN_DEBUG_VAL) ||
        (i4TestValFsMstDebug > MST_MAX_DEBUG_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_DEBUG_OPTION_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstForceProtocolVersion
 Input       :  The Indices

                The Object 
                testValFsMstForceProtocolVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsMstForceProtocolVersion)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: MSTP Module not Started\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstForceProtocolVersion != AST_VERSION_0) &&
        (i4TestValFsMstForceProtocolVersion != AST_VERSION_2) &&
        (i4TestValFsMstForceProtocolVersion != AST_VERSION_3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* If the context has SISP enabled ports mapped to it, than RSTP cannot
     * be the operating mode for the context
     * */
    if (i4TestValFsMstForceProtocolVersion != AST_VERSION_3)
    {
        if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
        {
            CLI_SET_ERR (CLI_STP_SISP_COMPATIBLE_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstTxHoldCount
 Input       :  The Indices

                The Object 
                testValFsMstTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstTxHoldCount (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstTxHoldCount)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstTxHoldCount < AST_TXHOLDCOUNT_MIN_VAL) ||
        (i4TestValFsMstTxHoldCount > AST_TXHOLDCOUNT_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiConfigIdSel
 Input       :  The Indices

                The Object 
                testValFsMstMstiConfigIdSel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiConfigIdSel (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMstMstiConfigIdSel)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiConfigIdSel < MST_MIN_CONFIG_ID) ||
        (i4TestValFsMstMstiConfigIdSel > MST_MAX_CONFIG_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiRegionName
 Input       :  The Indices

                The Object 
                testValFsMstMstiRegionName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiRegionName (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsMstMstiRegionName)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if ((pTestValFsMstMstiRegionName->i4_Length < MST_MIN_CONFIG_NAME_LEN) ||
        (pTestValFsMstMstiRegionName->i4_Length > MST_MAX_CONFIG_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiRegionVersion
 Input       :  The Indices

                The Object 
                testValFsMstMstiRegionVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiRegionVersion (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMstMstiRegionVersion)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMstMstiRegionVersion < MST_MIN_REGION_VERSION) ||
        (i4TestValFsMstMstiRegionVersion > MST_MAX_REGION_VERSION))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistBridgeHelloTime
 Input       :  The Indices

                The Object 
                testValFsMstCistBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistBridgeHelloTime (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMstCistBridgeHelloTime)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstCistBridgeHelloTime) !=
         AST_OK)
        || (i4TestValFsMstCistBridgeHelloTime < AST_MIN_HELLOTIME_VAL)
        || (i4TestValFsMstCistBridgeHelloTime > AST_MAX_HELLOTIME_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_BRG_HELLOTIME_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValFsMstCistBridgeHelloTime),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsMstCistDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                retValFsMstCistDynamicPathcostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMstCistDynamicPathcostCalculation
    (INT4 *pi4RetValFsMstCistDynamicPathcostCalculation)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistDynamicPathcostCalculation = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstBridgeEntry = AST_GET_BRGENTRY ();
    if (pAstBridgeEntry->u1DynamicPathcostCalculation == MST_TRUE)
    {
        *pi4RetValFsMstCistDynamicPathcostCalculation = AST_SNMP_TRUE;
    }
    else if (pAstBridgeEntry->u1DynamicPathcostCalculation == MST_FALSE)
    {
        *pi4RetValFsMstCistDynamicPathcostCalculation = AST_SNMP_FALSE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                retValFsMstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCalcPortPathCostOnSpeedChg (INT4
                                       *pi4RetValFsMstCalcPortPathCostOnSpeedChg)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCalcPortPathCostOnSpeedChg = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstCalcPortPathCostOnSpeedChg =
        (AST_GET_BRGENTRY ())->u1DynamicPathcostCalcLagg;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstRcvdEvent
 Input       :  The Indices

                The Object 
                retValFsMstRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstRcvdEvent (INT4 *pi4RetValFsMstRcvdEvent)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstRcvdEvent = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pi4RetValFsMstRcvdEvent = (AST_GET_BRGENTRY ())->i4RcvdEvent;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstRcvdEventSubType
 Input       :  The Indices

                The Object 
                retValFsMstRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstRcvdEventSubType (INT4 *pi4RetValFsMstRcvdEventSubType)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstRcvdEventSubType = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pi4RetValFsMstRcvdEventSubType = (AST_GET_BRGENTRY ())->i4RcvdEventSubType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstRcvdEventTimeStamp
 Input       :  The Indices

                The Object 
                retValFsMstRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstRcvdEventTimeStamp (UINT4 *pu4RetValFsMstRcvdEventTimeStamp)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstRcvdEventTimeStamp = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsMstRcvdEventTimeStamp =
        (AST_GET_BRGENTRY ())->u4RcvdEventTimeStamp;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstPortStateChangeTimeStamp
 Input       :  The Indices

                The Object 
                retValFsMstPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMstPortStateChangeTimeStamp
    (UINT4 *pu4RetValFsMstPortStateChangeTimeStamp)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstPortStateChangeTimeStamp = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsMstPortStateChangeTimeStamp =
        (AST_GET_BRGENTRY ())->u4PortStateChangeTimeStamp;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMstCistDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                setValFsMstCistDynamicPathcostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMstCistDynamicPathcostCalculation
    (INT4 i4SetValFsMstCistDynamicPathcostCalculation)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_DYNAMIC_PATHCOST_MSG;

    if (i4SetValFsMstCistDynamicPathcostCalculation == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = MST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = MST_FALSE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistDynamicPathcostCalculation,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCistDynamicPathcostCalculation));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                setValFsMstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCalcPortPathCostOnSpeedChg (INT4
                                       i4SetValFsMstCalcPortPathCostOnSpeedChg)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ENABLE_PORT_SPEED_CHG_MSG;

    pAstMsgNode->uMsg.u1DynamicPathcostCalculation =
        (UINT1) i4SetValFsMstCalcPortPathCostOnSpeedChg;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCalcPortPathCostOnSpeedChg,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstCalcPortPathCostOnSpeedChg));
    AstSelectContext (u4CurrContextId);

    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMstCistDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                testValFsMstCistDynamicPathcostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsMstCistDynamicPathcostCalculation
    (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstCistDynamicPathcostCalculation)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistDynamicPathcostCalculation != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistDynamicPathcostCalculation != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                testValFsMstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMstCalcPortPathCostOnSpeedChg)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCalcPortPathCostOnSpeedChg != AST_SNMP_TRUE) &&
        (i4TestValFsMstCalcPortPathCostOnSpeedChg != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstCistDynamicPathcostCalculation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistDynamicPathcostCalculation (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstCalcPortPathCostOnSpeedChg
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstModuleStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstMaxMstInstanceNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMaxMstInstanceNumber (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstMaxHopCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMaxHopCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstCistBridgePriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistBridgePriority (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstCistBridgeMaxAge
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistBridgeMaxAge (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstCistBridgeForwardDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstPathCostDefaultType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstPathCostDefaultType (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstForceProtocolVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstTxHoldCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstTxHoldCount (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstMstiConfigIdSel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMstiConfigIdSel (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstMstiRegionName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMstiRegionName (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstMstiRegionVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMstiRegionVersion (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstCistBridgeHelloTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistBridgeHelloTime (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstMstiBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstMstiBridgeTable
 Input       :  The Indices
                FsMstMstiInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMstMstiBridgeTable (INT4 i4FsMstMstiInstanceIndex)
{

    if ((i4FsMstMstiInstanceIndex < AST_MIN_MST_INSTANCES) ||
        (i4FsMstMstiInstanceIndex >= AST_MAX_MST_INSTANCES))
    {
        /* For TE_MSTID, Validation should return success */
        if (i4FsMstMstiInstanceIndex != AST_TE_MSTID)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstMstiBridgeTable
 Input       :  The Indices
                FsMstMstiInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMstMstiBridgeTable (INT4 *pi4FsMstMstiInstanceIndex)
{
    return (nmhGetNextIndexFsMstMstiBridgeTable (0, pi4FsMstMstiInstanceIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstMstiBridgeTable
 Input       :  The Indices
                FsMstMstiInstanceIndex
                nextFsMstMstiInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstMstiBridgeTable (INT4 i4FsMstMstiInstanceIndex,
                                     INT4 *pi4NextFsMstMstiInstanceIndex)
{
    tAstPerStInfo      *pAstPerStInfo = NULL;
    UINT2               u2InstIndex = 0;
    UINT1               u1Flag = MST_FALSE;

    if (i4FsMstMstiInstanceIndex < 0 ||
        i4FsMstMstiInstanceIndex > AST_MAX_MST_INSTANCES)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    u2InstIndex = (UINT2) i4FsMstMstiInstanceIndex;
    AST_GET_NEXT_BUF_INSTANCE (u2InstIndex, pAstPerStInfo)
    {

        if ((pAstPerStInfo == NULL)
            || (pAstPerStInfo->u2InstanceId == MST_CIST_CONTEXT))
        {
            continue;
        }

        if (pAstPerStInfo->u2InstanceId > i4FsMstMstiInstanceIndex)
        {
            if (u1Flag == MST_FALSE)
            {
                *pi4NextFsMstMstiInstanceIndex = pAstPerStInfo->u2InstanceId;
                u1Flag = MST_TRUE;
            }
            else
            {
                if (*pi4NextFsMstMstiInstanceIndex >
                    pAstPerStInfo->u2InstanceId)
                {
                    *pi4NextFsMstMstiInstanceIndex =
                        pAstPerStInfo->u2InstanceId;
                }
            }
        }
    }

    /* The Next Instance is TE_MSTID if it has created
     * for current context*/
    if (u1Flag == MST_FALSE)
    {
        if (AST_IS_TE_MSTID_VALID (i4FsMstMstiInstanceIndex))
        {
            *pi4NextFsMstMstiInstanceIndex = AST_TE_MSTID;
            return SNMP_SUCCESS;

        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstMstiBridgeRegionalRoot
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiBridgeRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiBridgeRegionalRoot (INT4 i4FsMstMstiInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMstMstiBridgeRegionalRoot)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstMstiBridgeRegionalRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiBridgeRegionalRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        pu1List = pRetValFsMstMstiBridgeRegionalRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiBridgeRegionalRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }
    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMstMstiBridgeRegionalRoot->pu1_OctetList[0] =
        (UINT1) ((pAstPerStBridgeInfo->RootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstMstiBridgeRegionalRoot->pu1_OctetList[1] =
        (UINT1) (pAstPerStBridgeInfo->RootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstMstiBridgeRegionalRoot->pu1_OctetList + 2,
                (&pAstPerStBridgeInfo->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstMstiBridgeRegionalRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstMstiBridgePriority
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiBridgePriority (INT4 i4FsMstMstiInstanceIndex,
                               INT4 *pi4RetValFsMstMstiBridgePriority)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT2               u2Priority = 0;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiBridgePriority = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiBridgePriority = u2Priority;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMstBrgEntry = AST_GET_MST_BRGENTRY ();

    if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
    {
        MST_GET_EXT_BRIDGE_PRIORITY (pAstPerStBridgeInfo->u2BrgPriority,
                                     u2Priority);
    }
    else
    {
        MST_GET_BRIDGE_PRIORITY (pAstPerStBridgeInfo->u2BrgPriority,
                                 u2Priority);
    }

    *pi4RetValFsMstMstiBridgePriority = u2Priority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiRootCost
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiRootCost (INT4 i4FsMstMstiInstanceIndex,
                         INT4 *pi4RetValFsMstMstiRootCost)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiRootCost = pAstPerStBridgeInfo->u4RootCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiRootPort
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiRootPort (INT4 i4FsMstMstiInstanceIndex,
                         INT4 *pi4RetValFsMstMstiRootPort)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiRootPort = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiRootPort = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiRootPort = pAstPerStBridgeInfo->u2RootPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiTimeSinceTopologyChange
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiTimeSinceTopologyChange (INT4 i4FsMstMstiInstanceIndex,
                                        UINT4
                                        *pu4RetValFsMstMstiTimeSinceTopologyChange)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tAstSysTime         CurrSysTime;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiTimeSinceTopologyChange = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiTimeSinceTopologyChange = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    AST_MEMSET (&CurrSysTime, AST_MEMSET_VAL, sizeof (tAstSysTime));

    if (pAstPerStBridgeInfo->u4NumTopoCh != 0)
    {
        AST_GET_SYS_TIME (&CurrSysTime);

        *pu4RetValFsMstMstiTimeSinceTopologyChange =
            (UINT4) (CurrSysTime -
                     (tAstSysTime) pAstPerStBridgeInfo->u4TimeSinceTopoCh);
        *pu4RetValFsMstMstiTimeSinceTopologyChange =
            AST_CONVERT_STUPS_2_CENTI_SEC
            (*pu4RetValFsMstMstiTimeSinceTopologyChange);
    }
    else
    {
        *pu4RetValFsMstMstiTimeSinceTopologyChange = 0;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiTopChanges
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiTopChanges (INT4 i4FsMstMstiInstanceIndex,
                           UINT4 *pu4RetValFsMstMstiTopChanges)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiTopChanges = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiTopChanges = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    *pu4RetValFsMstMstiTopChanges = pAstPerStBridgeInfo->u4NumTopoCh;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiNewRootBridgeCount
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiNewRootBridgeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiNewRootBridgeCount (INT4 i4FsMstMstiInstanceIndex,
                                   UINT4 *pu4RetValFsMstMstiNewRootBridgeCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiNewRootBridgeCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiNewRootBridgeCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    *pu4RetValFsMstMstiNewRootBridgeCount =
        pAstPerStBridgeInfo->u4NewRootIdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstBridgeMstiRoleSelectionSemState
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstMstiBridgeRoleSelectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiBridgeRoleSelectionSemState (INT4 i4FsMstMstiInstanceIndex,
                                            INT4
                                            *pi4RetValFsMstMstiBridgeRoleSelectionSemState)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiBridgeRoleSelectionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiBridgeRoleSelectionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    *pi4RetValFsMstMstiBridgeRoleSelectionSemState =
        pAstPerStBridgeInfo->u1ProleSelSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceUpCount
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstInstanceUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceUpCount (INT4 i4FsMstMstiInstanceIndex,
                            UINT4 *pu4RetValFsMstInstanceUpCount)
{

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstInstanceUpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstMstiInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:INVALID Instance \n");
        return MST_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstInstanceUpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pu4RetValFsMstInstanceUpCount =
        AST_INST_UP_COUNT (i4FsMstMstiInstanceIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceDownCount
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstInstanceDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceDownCount (INT4 i4FsMstMstiInstanceIndex,
                              UINT4 *pu4RetValFsMstInstanceDownCount)
{

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstInstanceDownCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (MST_IS_INSTANCE_VALID (i4FsMstMstiInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:INVALID Instance \n");
        return MST_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstInstanceDownCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4RetValFsMstInstanceDownCount =
            AST_INST_UP_COUNT (i4FsMstMstiInstanceIndex);
    }
    else
    {
        *pu4RetValFsMstInstanceDownCount =
            (AST_INST_UP_COUNT (i4FsMstMstiInstanceIndex) - 1);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstOldDesignatedRoot
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                retValFsMstOldDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstOldDesignatedRoot (INT4 i4FsMstMstiInstanceIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMstOldDesignatedRoot)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstOldDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstOldDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        pu1List = pRetValFsMstOldDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstOldDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMstMstiInstanceIndex);

    pRetValFsMstOldDesignatedRoot->pu1_OctetList[0] =
        (UINT1) ((pAstPerStBridgeInfo->OldRootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstOldDesignatedRoot->pu1_OctetList[1] =
        (UINT1) (pAstPerStBridgeInfo->OldRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstOldDesignatedRoot->pu1_OctetList + 2,
                (&pAstPerStBridgeInfo->OldRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);

    pRetValFsMstOldDesignatedRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstMstiBridgePriority
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                setValFsMstMstiBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiBridgePriority (INT4 i4FsMstMstiInstanceIndex,
                               INT4 i4SetValFsMstMstiBridgePriority)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_BRIDGE_PRIORITY_MSG;
    pAstMsgNode->uMsg.u2BridgePriority =
        (UINT2) i4SetValFsMstMstiBridgePriority;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstMstiInstanceIndex;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiBridgePriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", AST_CURR_CONTEXT_ID (),
                      i4FsMstMstiInstanceIndex,
                      i4SetValFsMstMstiBridgePriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiBridgePriority
 Input       :  The Indices
                FsMstMstiInstanceIndex

                The Object 
                testValFsMstMstiBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiBridgePriority (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMstMstiInstanceIndex,
                                  INT4 i4TestValFsMstMstiBridgePriority)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (i4FsMstMstiInstanceIndex == MST_CIST_CONTEXT)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Instance Id is Zero !\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsMstMstiBridgePriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_BRIDGE_PRIORITY_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiBridgePriority < AST_MIN_BRIDGE_PRIORITY) ||
        (i4TestValFsMstMstiBridgePriority > AST_MAX_BRIDGE_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstMstiBridgeTable
 Input       :  The Indices
                FsMstMstiInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMstiBridgeTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstVlanInstanceMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMstInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMstVlanInstanceMappingTable (INT4
                                                       i4FsMstInstanceIndex)
{
    if ((i4FsMstInstanceIndex < AST_MIN_MST_INSTANCES) ||
        (i4FsMstInstanceIndex >= AST_MAX_MST_INSTANCES))
    {
        /* For Instance TE_MSTID */
        if (i4FsMstInstanceIndex != AST_TE_MSTID)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMstInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMstVlanInstanceMappingTable (INT4 *pi4FsMstInstanceIndex)
{
    return (nmhGetNextIndexFsMstVlanInstanceMappingTable
            (0, pi4FsMstInstanceIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMstInstanceIndex
                nextFsMstInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstVlanInstanceMappingTable (INT4 i4FsMstInstanceIndex,
                                              INT4 *pi4NextFsMstInstanceIndex)
{

    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2InstIndex = 0;

    if (i4FsMstInstanceIndex < 0 || i4FsMstInstanceIndex >
        AST_MAX_MST_INSTANCES)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    u2InstIndex = (UINT2) (i4FsMstInstanceIndex + 1);
    AST_GET_NEXT_BUF_INSTANCE (u2InstIndex, pPerStInfo)
    {
        if (pPerStInfo != NULL)
        {
            *pi4NextFsMstInstanceIndex = u2InstIndex;
            return SNMP_SUCCESS;
        }
    }

    /* The next instance should be TE_MSTID, if it has created
     * for current context  */
    if (AST_IS_TE_MSTID_VALID (i4FsMstInstanceIndex))
    {
        *pi4NextFsMstInstanceIndex = AST_TE_MSTID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsMstMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstMapVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMapVlanIndex (INT4 i4FsMstInstanceIndex,
                         INT4 *pi4RetValFsMstMapVlanIndex)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMapVlanIndex = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsMstMapVlanIndex = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstUnMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstUnMapVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstUnMapVlanIndex (INT4 i4FsMstInstanceIndex,
                           INT4 *pi4RetValFsMstUnMapVlanIndex)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstUnMapVlanIndex = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsMstUnMapVlanIndex = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstSetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstSetVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstSetVlanList (INT4 i4FsMstInstanceIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMstSetVlanList)
{
    AST_MEMSET (pRetValFsMstSetVlanList->pu1_OctetList,
                AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_SUCCESS;
    }
    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pRetValFsMstSetVlanList->i4_Length = MST_VLAN_LIST_SIZE;
    AST_MEMSET (pRetValFsMstSetVlanList->pu1_OctetList,
                AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    MstGetVfiVlanList ((UINT2) i4FsMstInstanceIndex,
                       pRetValFsMstSetVlanList->pu1_OctetList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstResetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstResetVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstResetVlanList (INT4 i4FsMstInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMstResetVlanList)
{
    pRetValFsMstResetVlanList->i4_Length = AST_INIT_VAL;
    AST_MEMSET (pRetValFsMstResetVlanList->pu1_OctetList,
                AST_INIT_VAL, MST_VLAN_LIST_SIZE);
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pRetValFsMstResetVlanList->i4_Length = AST_INIT_VAL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceVlanMapped
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstInstanceVlanMapped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceVlanMapped (INT4 i4FsMstInstanceIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMstInstanceVlanMapped)
{
    tAstVlanList        VlanList;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        pRetValFsMstInstanceVlanMapped->i4_Length = AST_INIT_VAL;
        AST_MEMSET (pRetValFsMstInstanceVlanMapped->pu1_OctetList,
                    AST_INIT_VAL, MST_VLANMAP_LIST_SIZE);
        return SNMP_SUCCESS;
    }
    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (VlanList, AST_INIT_VAL, sizeof (tAstVlanList));
    MstGetVlanList ((UINT2) i4FsMstInstanceIndex, VlanList,
                    MST_VLANS_INSTANCE_1K);
    pRetValFsMstInstanceVlanMapped->i4_Length = MST_VLANMAP_LIST_SIZE;
    AST_MEMCPY (pRetValFsMstInstanceVlanMapped->pu1_OctetList,
                VlanList, pRetValFsMstInstanceVlanMapped->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceVlanMapped2k
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstInstanceVlanMapped2k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceVlanMapped2k (INT4 i4FsMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstInstanceVlanMapped2k)
{
    tAstVlanList        VlanList;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        pRetValFsMstInstanceVlanMapped2k->i4_Length = AST_INIT_VAL;
        AST_MEMSET (pRetValFsMstInstanceVlanMapped2k->pu1_OctetList,
                    AST_INIT_VAL, MST_VLANMAP_LIST_SIZE);
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (VlanList, AST_INIT_VAL, sizeof (tAstVlanList));
    MstGetVlanList ((UINT2) i4FsMstInstanceIndex, VlanList,
                    MST_VLANS_INSTANCE_2K);
    pRetValFsMstInstanceVlanMapped2k->i4_Length = MST_VLANMAP_LIST_SIZE;
    AST_MEMCPY (pRetValFsMstInstanceVlanMapped2k->pu1_OctetList,
                VlanList, pRetValFsMstInstanceVlanMapped2k->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceVlanMapped3k
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstInstanceVlanMapped3k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceVlanMapped3k (INT4 i4FsMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstInstanceVlanMapped3k)
{
    tAstVlanList        VlanList;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        pRetValFsMstInstanceVlanMapped3k->i4_Length = AST_INIT_VAL;
        AST_MEMSET (pRetValFsMstInstanceVlanMapped3k->pu1_OctetList,
                    AST_INIT_VAL, MST_VLANMAP_LIST_SIZE);
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (VlanList, AST_INIT_VAL, sizeof (tAstVlanList));
    MstGetVlanList ((UINT2) i4FsMstInstanceIndex, VlanList,
                    MST_VLANS_INSTANCE_3K);
    pRetValFsMstInstanceVlanMapped3k->i4_Length = MST_VLANMAP_LIST_SIZE;
    AST_MEMCPY (pRetValFsMstInstanceVlanMapped3k->pu1_OctetList,
                VlanList, pRetValFsMstInstanceVlanMapped3k->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstanceVlanMapped4k
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                retValFsMstInstanceVlanMapped4k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstanceVlanMapped4k (INT4 i4FsMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstInstanceVlanMapped4k)
{
    tAstVlanList        VlanList;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        pRetValFsMstInstanceVlanMapped4k->i4_Length = AST_INIT_VAL;
        AST_MEMSET (pRetValFsMstInstanceVlanMapped4k->pu1_OctetList,
                    AST_INIT_VAL, MST_VLANMAP_LIST_SIZE);
        return SNMP_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (VlanList, AST_INIT_VAL, sizeof (tAstVlanList));
    MstGetVlanList ((UINT2) i4FsMstInstanceIndex, VlanList,
                    MST_VLANS_INSTANCE_4K);
    pRetValFsMstInstanceVlanMapped4k->i4_Length = MST_VLANMAP_LIST_SIZE;
    AST_MEMCPY (pRetValFsMstInstanceVlanMapped4k->pu1_OctetList,
                VlanList, pRetValFsMstInstanceVlanMapped4k->i4_Length);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                setValFsMstMapVlanIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMapVlanIndex (INT4 i4FsMstInstanceIndex,
                         INT4 i4SetValFsMstMapVlanIndex)
{
    if (MstSetMstMapVlanIndex (i4FsMstInstanceIndex,
                               i4SetValFsMstMapVlanIndex) == MST_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMstUnMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                setValFsMstUnMapVlanIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstUnMapVlanIndex (INT4 i4FsMstInstanceIndex,
                           INT4 i4SetValFsMstUnMapVlanIndex)
{
    if (MstSetMstUnMapVlanIndex (i4FsMstInstanceIndex,
                                 i4SetValFsMstUnMapVlanIndex) == MST_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMstSetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                setValFsMstSetVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstSetVlanList (INT4 i4FsMstInstanceIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsMstSetVlanList)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1              *pMstVlanList = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    /* We add this check here in this set routine because it can happen
     * that a config-save was done with VLAN learning mode to be IVL 
     * after creating some MSTP instances.The switch could have been
     * restarted with vlan learning admin mode set to SVL and restarted
     * without saving the configuration in which case we should not restore
     * the MSTP instances */

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) ==
         VLAN_SHARED_LEARNING))
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:If Shared VLAN learning only the default CIST"
                 "context can exist.\n");

        return SNMP_FAILURE;
    }

    pMstVlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

    if (pMstVlanList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pMstVlanList:Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pMstVlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));
    AST_MEMCPY (pMstVlanList, pSetValFsMstSetVlanList->pu1_OctetList,
                pSetValFsMstSetVlanList->i4_Length);

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        UtilVlanReleaseVlanListSize (pMstVlanList);
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_INST_VLANLIST_MAP_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    AST_MEMCPY (pAstMsgNode->uMsg.MstVlanList, pMstVlanList,
                pSetValFsMstSetVlanList->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSetVlanList, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s", AST_CURR_CONTEXT_ID (),
                      i4FsMstInstanceIndex, pSetValFsMstSetVlanList));
    AstSelectContext (u4CurrContextId);
    UtilVlanReleaseVlanListSize (pMstVlanList);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstResetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                setValFsMstResetVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstResetVlanList (INT4 i4FsMstInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsMstResetVlanList)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSNMP_OCTET_STRING_TYPE *pOctetVlanList = NULL;
    UINT1              *pMstVlanList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1              *pu1VlanList = NULL;

    pMstVlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

    if (pMstVlanList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pMstVlanList:Memory allocation for Vlan List failed.\n");

        return SNMP_FAILURE;
    }

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (pMstVlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));
    AST_MEMCPY (pMstVlanList, pSetValFsMstResetVlanList->pu1_OctetList,
                pSetValFsMstResetVlanList->i4_Length);

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        UtilVlanReleaseVlanListSize (pMstVlanList);
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_INST_VLANLIST_UNMAP_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    AST_MEMCPY (pAstMsgNode->uMsg.MstVlanList, pMstVlanList,
                pSetValFsMstResetVlanList->i4_Length);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;

        RM_GET_SEQ_NUM (&u4SeqNum);

        /*Allocating memory for Octet string to store vlan list */
        if ((pOctetVlanList =
             allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
        {
            UtilVlanReleaseVlanListSize (pMstVlanList);
            return (SNMP_FAILURE);
        }
        MEMSET (pOctetVlanList->pu1_OctetList, AST_INIT_VAL,
                MST_VLAN_LIST_SIZE);

        pu1VlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

        if (pu1VlanList == NULL)
        {
            UtilVlanReleaseVlanListSize (pMstVlanList);
            free_octetstring (pOctetVlanList);
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "pMstVlanList:Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        AST_MEMSET (pu1VlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));

        L2IwfGetVlanListInInstance (i4FsMstInstanceIndex, pu1VlanList);

        MEMCPY (pOctetVlanList->pu1_OctetList, pu1VlanList, MST_VLAN_LIST_SIZE);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSetVlanList, u4SeqNum,
                              FALSE, AstLock, AstUnLock, 2, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s", AST_CURR_CONTEXT_ID (),
                          i4FsMstInstanceIndex, pOctetVlanList));

        UtilVlanReleaseVlanListSize (pu1VlanList);
        free_octetstring (pOctetVlanList);

    }

    UtilVlanReleaseVlanListSize (pMstVlanList);
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMstMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                testValFsMstMapVlanIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMapVlanIndex (UINT4 *pu4ErrorCode,
                            INT4 i4FsMstInstanceIndex,
                            INT4 i4TestValFsMstMapVlanIndex)
{
    if (MstTestMstMapVlanIndex (pu4ErrorCode, i4FsMstInstanceIndex,
                                i4TestValFsMstMapVlanIndex) == MST_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstUnMapVlanIndex
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                testValFsMstUnMapVlanIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstUnMapVlanIndex (UINT4 *pu4ErrorCode,
                              INT4 i4FsMstInstanceIndex,
                              INT4 i4TestValFsMstUnMapVlanIndex)
{
    if (MstTestMstUnMapVlanIndex (pu4ErrorCode, i4FsMstInstanceIndex,
                                  i4TestValFsMstUnMapVlanIndex) == MST_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstSetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                testValFsMstSetVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstSetVlanList (UINT4 *pu4ErrorCode,
                           INT4 i4FsMstInstanceIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsMstSetVlanList)
{

    UINT4               u4FdbId;
    UINT2               u2VlanIndex;
    tMstVlanId          VlanId = AST_INIT_VAL;
    UINT2               u2ByteInd = AST_INIT_VAL;
    UINT2               u2BitIndex = AST_INIT_VAL;
    UINT2               u2VlanFlag = AST_INIT_VAL;
    UINT2               u2NumVlans = 0;
    UINT2               u2MappedVlans = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1              *pu1FdbList = NULL;
    UINT2               u2CurrInstanceId = MST_CIST_CONTEXT;

    pu1FdbList = UtlShMemAllocVlanList ();

    if (pu1FdbList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "nmhTestv2FsMstSetVlanList:Memory allocation"
                 " for Vlan List failed.\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pu1FdbList, 0, MST_FDB_LIST_SIZE);

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        UtlShMemFreeVlanList (pu1FdbList);
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        UtlShMemFreeVlanList (pu1FdbList);
        return SNMP_FAILURE;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Instance Id Range is Invalid!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        UtlShMemFreeVlanList (pu1FdbList);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_NO_OF_ACTIVE_INSTANCES >= AST_GET_MAX_MST_INSTANCES)
    {
        if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "ERROR: No Of Active Instances cant exceed MaxMstInst - %d \n",
                          AST_GET_NO_OF_ACTIVE_INSTANCES);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            UtlShMemFreeVlanList (pu1FdbList);
            return (INT1) SNMP_FAILURE;
        }
    }

    if ((pTestValFsMstSetVlanList->i4_Length < AST_INIT_VAL) ||
        (pTestValFsMstSetVlanList->i4_Length > MST_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_STP_INVALID_VLAN_ID_ERR);
        UtlShMemFreeVlanList (pu1FdbList);
        return SNMP_FAILURE;
    }

    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:For Instance to be mapped to VLAN, VLAN should be enabled!!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        UtlShMemFreeVlanList (pu1FdbList);
        return (INT1) SNMP_FAILURE;
    }

    /* This check is added here because when the learning mode is
     * shared only the default CIST context should exist.*/

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) ==
         VLAN_SHARED_LEARNING))
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:If Shared VLAN learning only the default CIST"
                 "context can exist.\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_SHARED_VLAN_ERR);
        UtlShMemFreeVlanList (pu1FdbList);
        return SNMP_FAILURE;
    }

    /* To check whether all the VLANS belonging to the FIDs in this
       Vlan List are either already mapped to i4FsMstInstanceIndex
       or they are part of the input Vlan List */

    for (u2ByteInd = 0; (u2ByteInd < MST_VLAN_LIST_SIZE); u2ByteInd++)
    {
        if (pTestValFsMstSetVlanList->pu1_OctetList[u2ByteInd] != 0)
        {
            u2VlanFlag = pTestValFsMstSetVlanList->pu1_OctetList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) && (u2VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (MST_IS_VALID_VLANID (VlanId) == MST_TRUE)
                    {
                        u2NumVlans++;
                        u2CurrInstanceId = AstL2IwfMiGetVlanInstMapping
                            (AST_CURR_CONTEXT_ID (), (tVlanId) VlanId);

                        if (u2CurrInstanceId == i4FsMstInstanceIndex)
                        {
                            u2MappedVlans++;
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;

                        }
                        else if (u2CurrInstanceId != MST_CIST_CONTEXT)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                            CLI_SET_ERR (CLI_STP_MST_INVALID_VLAN_LIST_ERR);
                            UtlShMemFreeVlanList (pu1FdbList);
                            return SNMP_FAILURE;
                        }

                        if ((AstVlanMiGetVlanLearningMode
                             (AST_CURR_CONTEXT_ID ()) != VLAN_INDEP_LEARNING))
                        {
                            u4FdbId =
                                AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                                        VlanId);

                            OSIX_BITLIST_IS_BIT_SET
                                (pu1FdbList, u4FdbId, MST_FDB_LIST_SIZE,
                                 bResult);
                            if (bResult == OSIX_TRUE)
                            {
                                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                continue;
                            }

                            OSIX_BITLIST_SET_BIT (pu1FdbList, u4FdbId,
                                                  MST_FDB_LIST_SIZE);

                            for (u2VlanIndex = 1;
                                 u2VlanIndex <= AST_MAX_NUM_VLANS;
                                 u2VlanIndex++)
                            {
                                if (u2VlanIndex != (UINT4) VlanId)
                                {
                                    OSIX_BITLIST_IS_BIT_SET
                                        (pTestValFsMstSetVlanList->
                                         pu1_OctetList, u2VlanIndex,
                                         MST_VLAN_LIST_SIZE, bResult);

                                    if (bResult == OSIX_TRUE)
                                    {
                                        continue;
                                    }

                                }
                            }
                        }
                    }

                    if (MstSispValidateVlantoInstanceMapping (VlanId,
                                                              (UINT2)
                                                              i4FsMstInstanceIndex)
                        != MST_SUCCESS)
                    {
                        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                 "SNMP: In contexts with SISP enabled ports mapped to it, Same port cannot be member of same instance in two different contexts\n");

                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        /* Determine the CLI error */
                        CLI_SET_ERR (CLI_STP_NO_SAME_INST_IN_DIFF_CTXT);
                        UtlShMemFreeVlanList (pu1FdbList);
                        return SNMP_FAILURE;
                    }

                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    if (u2MappedVlans == u2NumVlans)
    {

        UtlShMemFreeVlanList (pu1FdbList);
        return SNMP_SUCCESS;
    }
    UtlShMemFreeVlanList (pu1FdbList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstResetVlanList
 Input       :  The Indices
                FsMstInstanceIndex

                The Object 
                testValFsMstResetVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstResetVlanList (UINT4 *pu4ErrorCode,
                             INT4 i4FsMstInstanceIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsMstResetVlanList)
{
    UINT4               u4FdbId;
    UINT2               u2VlanIndex;
    tMstVlanId          VlanId = AST_INIT_VAL;
    UINT2               u2ByteInd = AST_INIT_VAL;
    UINT2               u2BitIndex = AST_INIT_VAL;
    UINT2               u2VlanFlag = AST_INIT_VAL;
    BOOL1               bResult = OSIX_FALSE;
    UINT2               u2NumVlans = 0;
    UINT2               u2NotMappedVlans = 0;
    UINT1              *pu1FdbList = NULL;

    pu1FdbList = UtlShMemAllocVlanList ();

    if (pu1FdbList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pu1FdbList:Memory allocation for Vlan List failed.\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pu1FdbList, 0, MST_FDB_LIST_SIZE);

    if (!(AST_IS_MST_STARTED ()))
    {
        UtlShMemFreeVlanList (pu1FdbList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        UtlShMemFreeVlanList (pu1FdbList);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        UtlShMemFreeVlanList (pu1FdbList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Invalid Instance!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return SNMP_FAILURE;
    }
    if ((pTestValFsMstResetVlanList->i4_Length < AST_INIT_VAL) ||
        (pTestValFsMstResetVlanList->i4_Length > MST_VLAN_LIST_SIZE))
    {
        UtlShMemFreeVlanList (pu1FdbList);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_STP_INVALID_VLAN_ID_ERR);
        return SNMP_FAILURE;
    }

    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_FALSE)
    {
        UtlShMemFreeVlanList (pu1FdbList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:For Instance to be mapped to VLAN, VLAN should be enabled!!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    /* To check whether all the VLANS belonging to the FIDs in this
       Vlan List are either already mapped to CIST or they are part 
       of the input Vlan List */

    for (u2ByteInd = 0; (u2ByteInd < MST_VLAN_LIST_SIZE); u2ByteInd++)
    {
        if (pTestValFsMstResetVlanList->pu1_OctetList[u2ByteInd] != 0)
        {
            u2VlanFlag = pTestValFsMstResetVlanList->pu1_OctetList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) && (u2VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (MST_IS_VALID_VLANID (VlanId) == MST_TRUE)
                    {
                        u2NumVlans++;
                        if (AstL2IwfMiGetVlanInstMapping
                            (AST_CURR_CONTEXT_ID (), VlanId)
                            != i4FsMstInstanceIndex)
                        {
                            u2NotMappedVlans++;
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;
                        }
                        if ((AstVlanMiGetVlanLearningMode
                             (AST_CURR_CONTEXT_ID ()) != VLAN_INDEP_LEARNING))
                        {
                            u4FdbId = AstL2IwfMiGetVlanFdbId
                                (AST_CURR_CONTEXT_ID (), VlanId);

                            OSIX_BITLIST_IS_BIT_SET
                                (pu1FdbList, u4FdbId, MST_FDB_LIST_SIZE,
                                 bResult);
                            if (bResult == OSIX_TRUE)
                            {
                                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                continue;
                            }

                            OSIX_BITLIST_SET_BIT (pu1FdbList, u4FdbId,
                                                  MST_FDB_LIST_SIZE);

                            for (u2VlanIndex = 1;
                                 u2VlanIndex <= AST_MAX_NUM_VLANS;
                                 u2VlanIndex++)
                            {
                                if (u2VlanIndex != (UINT4) VlanId)
                                {
                                    OSIX_BITLIST_IS_BIT_SET
                                        (pTestValFsMstResetVlanList->
                                         pu1_OctetList, u2VlanIndex,
                                         MST_VLAN_LIST_SIZE, bResult);

                                    if (bResult == OSIX_TRUE)
                                    {
                                        continue;
                                    }

                                    if ((AstL2IwfMiGetVlanFdbId
                                         (AST_CURR_CONTEXT_ID (),
                                          (UINT2) u2VlanIndex)
                                         == u4FdbId) &&
                                        (AstL2IwfMiGetVlanInstMapping
                                         (AST_CURR_CONTEXT_ID (),
                                          (tVlanId) u2VlanIndex)
                                         != MST_CIST_CONTEXT))
                                    {
                                        UtlShMemFreeVlanList (pu1FdbList);
                                        AST_TRC (AST_ALL_FAILURE_TRC |
                                                 AST_MGMT_TRC,
                                                 "SNMP: Inconsistent list of vlans\n");

                                        *pu4ErrorCode =
                                            SNMP_ERR_INCONSISTENT_NAME;
                                        CLI_SET_ERR (CLI_STP_VLAN_LIST_ERR);
                                        return SNMP_FAILURE;
                                    }
                                }
                            }
                        }
                    }
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    if (u2NumVlans == u2NotMappedVlans)
    {
        UtlShMemFreeVlanList (pu1FdbList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Given list of vlans are not mapped to the specified"
                 "Instance\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_VLANS_NOT_MAPPED_ERR);
        return SNMP_FAILURE;
    }
    UtlShMemFreeVlanList (pu1FdbList);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMstInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstVlanInstanceMappingTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstCistPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstCistPortTable
 Input       :  The Indices
                FsMstCistPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMstCistPortTable (INT4 i4FsMstCistPort)
{
    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstCistPortTable
 Input       :  The Indices
                FsMstCistPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMstCistPortTable (INT4 *pi4FsMstCistPort)
{
    return (nmhGetNextIndexFsMstCistPortTable (0, pi4FsMstCistPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstCistPortTable
 Input       :  The Indices
                FsMstCistPort
                nextFsMstCistPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstCistPortTable (INT4 i4FsMstCistPort,
                                   INT4 *pi4NextFsMstCistPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortIndex;
    UINT1               u1Flag = MST_FALSE;

    if (i4FsMstCistPort < 0)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }
    u2PortIndex = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortIndex, pAstPortEntry)
    {

        if (pAstPortEntry == NULL)
        {

            continue;
        }
        if (pAstPortEntry->u2PortNo > i4FsMstCistPort)
        {

            if (u1Flag == MST_FALSE)
            {

                *pi4NextFsMstCistPort = pAstPortEntry->u2PortNo;
                u1Flag = MST_TRUE;
            }
            else
            {

                if (*pi4NextFsMstCistPort > pAstPortEntry->u2PortNo)
                {

                    *pi4NextFsMstCistPort = pAstPortEntry->u2PortNo;
                }

            }
        }
    }

    if (u1Flag == MST_FALSE)
    {

        return SNMP_FAILURE;
    }
    else
    {

        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstCistPortPathCost
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortPathCost (INT4 i4FsMstCistPort,
                             INT4 *pi4RetValFsMstCistPortPathCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortPathCost = pAstPerStPortInfo->u4PortPathCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortPriority
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortPriority (INT4 i4FsMstCistPort,
                             INT4 *pi4RetValFsMstCistPortPriority)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortPriority = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortPriority = pAstPerStPortInfo->u1PortPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortDesignatedRoot
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortDesignatedRoot (INT4 i4FsMstCistPort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMstCistPortDesignatedRoot)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstCistPortDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    pRetValFsMstCistPortDesignatedRoot->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->RootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistPortDesignatedRoot->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->RootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistPortDesignatedRoot->pu1_OctetList + 2,
                (&pAstPerStPortInfo->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstCistPortDesignatedRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortDesignatedBridge
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortDesignatedBridge (INT4 i4FsMstCistPort,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMstCistPortDesignatedBridge)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstCistPortDesignatedBridge->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    pRetValFsMstCistPortDesignatedBridge->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistPortDesignatedBridge->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistPortDesignatedBridge->pu1_OctetList + 2,
                (&pAstPerStPortInfo->DesgBrgId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstCistPortDesignatedBridge->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortDesignatedPort
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortDesignatedPort (INT4 i4FsMstCistPort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMstCistPortDesignatedPort)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstCistPortDesignatedPort->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);

        pRetValFsMstCistPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    pu1List = pRetValFsMstCistPortDesignatedPort->pu1_OctetList;
    u2Val = pAstPerStPortInfo->u2DesgPortId;
    AST_PUT_2BYTE (pu1List, u2Val);

    pu1List -= 2;
    pRetValFsMstCistPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortAdminP2P
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortAdminP2P
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortAdminP2P (INT4 i4FsMstCistPort,
                             INT4 *pi4RetValFsMstCistPortAdminP2P)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortAdminP2P = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortAdminP2P = pAstPortEntry->u1AdminPointToPoint;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortOperP2P
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortOperP2P
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortOperP2P (INT4 i4FsMstCistPort,
                            INT4 *pi4RetValFsMstCistPortOperP2P)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortOperP2P = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bOperPointToPoint == MST_TRUE)
    {
        *pi4RetValFsMstCistPortOperP2P = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortOperP2P = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortAdminEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortAdminEdgeStatus (INT4 i4FsMstCistPort,
                                    INT4 *pi4RetValFsMstCistPortAdminEdgeStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortAdminEdgeStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bAdminEdgePort == MST_TRUE)
    {
        *pi4RetValFsMstCistPortAdminEdgeStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortAdminEdgeStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortOperEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortOperEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortOperEdgeStatus (INT4 i4FsMstCistPort,
                                   INT4 *pi4RetValFsMstCistPortOperEdgeStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortOperEdgeStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bOperEdgePort == MST_TRUE)
    {
        *pi4RetValFsMstCistPortOperEdgeStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortOperEdgeStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortProtocolMigration
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortProtocolMigration (INT4 i4FsMstCistPort,
                                      INT4
                                      *pi4RetValFsMstCistPortProtocolMigration)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortProtocolMigration = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsMstCistPort);

    if (pAstCommPortInfo->bMCheck == MST_TRUE)
    {
        *pi4RetValFsMstCistPortProtocolMigration = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortProtocolMigration = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortState (INT4 i4FsMstCistPort,
                          INT4 *pi4RetValFsMstCistPortState)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (i4FsMstCistPort))
        != AST_CUSTOMER_EDGE_PORT)
    {
        *pi4RetValFsMstCistPortState =
            (INT4) AstL2IwfGetInstPortState (MST_CIST_CONTEXT,
                                             (UINT2)
                                             AST_GET_IFINDEX (i4FsMstCistPort));
    }
    else
    {
        *pi4RetValFsMstCistPortState = AST_PORT_STATE_DISCARDING;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistForcePortState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistForcePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistForcePortState (INT4 i4FsMstCistPort,
                               INT4 *pi4RetValFsMstCistForcePortState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistForcePortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstCistPort,
                                              MST_CIST_CONTEXT);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo->bAdminPortEnabled == (tAstBoolean) MST_TRUE)
    {
        *pi4RetValFsMstCistForcePortState = MST_FORCE_STATE_ENABLED;
    }
    else
    {
        *pi4RetValFsMstCistForcePortState = MST_FORCE_STATE_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortEffectivePortState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortEffectivePortState (INT4 i4FsMstCistPort,
                                       INT4
                                       *pi4RetValFsMstCistPortEffectivePortState)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortEffectivePortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstCistPort,
                                              MST_CIST_CONTEXT);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (((pAstPortEntry->u1EntryStatus) == AST_PORT_OPER_UP)
        && (pRstPortInfo->bPortEnabled) == MST_TRUE)

    {
        *pi4RetValFsMstCistPortEffectivePortState = MST_PORT_OPER_ENABLED;
    }
    else
    {
        *pi4RetValFsMstCistPortEffectivePortState = MST_PORT_OPER_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortForwardTransitions
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortForwardTransitions (INT4 i4FsMstCistPort,
                                       UINT4
                                       *pu4RetValFsMstCistPortForwardTransitions)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortForwardTransitions = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortForwardTransitions =
        pAstPerStPortInfo->u4NumFwdTransitions;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRxMstBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRxMstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRxMstBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortRxMstBpduCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortRxMstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortRxMstBpduCount = pAstPerStPortInfo->u4NumBpdusRx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRxRstBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRxRstBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortRxRstBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortRxRstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortRxRstBpduCount = pAstPortEntry->u4NumRstBpdusRxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRxConfigBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRxConfigBpduCount (INT4 i4FsMstCistPort,
                                      UINT4
                                      *pu4RetValFsMstCistPortRxConfigBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortRxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortRxConfigBpduCount =
        pAstPortEntry->u4NumConfigBpdusRxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRxTcnBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRxTcnBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortRxTcnBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortRxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortRxTcnBpduCount = pAstPortEntry->u4NumTcnBpdusRxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTxMstBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTxMstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTxMstBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortTxMstBpduCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortTxMstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortTxMstBpduCount = pAstPerStPortInfo->u4NumBpdusTx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTxRstBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTxRstBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortTxRstBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortTxRstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortTxRstBpduCount = pAstPortEntry->u4NumRstBpdusTxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTxConfigBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTxConfigBpduCount (INT4 i4FsMstCistPort,
                                      UINT4
                                      *pu4RetValFsMstCistPortTxConfigBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortTxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortTxConfigBpduCount =
        pAstPortEntry->u4NumConfigBpdusTxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTxTcnBpduCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTxTcnBpduCount (INT4 i4FsMstCistPort,
                                   UINT4 *pu4RetValFsMstCistPortTxTcnBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortTxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortTxTcnBpduCount = pAstPortEntry->u4NumTcnBpdusTxd;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortInvalidMstBpduRxCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortInvalidMstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortInvalidMstBpduRxCount (INT4 i4FsMstCistPort,
                                          UINT4
                                          *pu4RetValFsMstCistPortInvalidMstBpduRxCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortInvalidMstBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortInvalidMstBpduRxCount =
        pAstPerStPortInfo->u4NumInvalidBpdusRx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortInvalidRstBpduRxCount (INT4 i4FsMstCistPort,
                                          UINT4
                                          *pu4RetValFsMstCistPortInvalidRstBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortInvalidRstBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortInvalidRstBpduRxCount =
        pAstPortEntry->u4InvalidRstBpdusRxdCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortInvalidConfigBpduRxCount (INT4 i4FsMstCistPort,
                                             UINT4
                                             *pu4RetValFsMstCistPortInvalidConfigBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortInvalidConfigBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortInvalidConfigBpduRxCount =
        pAstPortEntry->u4InvalidConfigBpdusRxdCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortInvalidTcnBpduRxCount (INT4 i4FsMstCistPort,
                                          UINT4
                                          *pu4RetValFsMstCistPortInvalidTcnBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortInvalidTcnBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortInvalidTcnBpduRxCount =
        pAstPortEntry->u4InvalidTcnBpdusRxdCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTransmitSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTransmitSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTransmitSemState (INT4 i4FsMstCistPort,
                                     INT4
                                     *pi4RetValFsMstCistPortTransmitSemState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortTransmitSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsMstCistPort);

    *pi4RetValFsMstCistPortTransmitSemState = pAstCommPortInfo->u1PortTxSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortReceiveSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortReceiveSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortReceiveSemState (INT4 i4FsMstCistPort,
                                    INT4 *pi4RetValFsMstCistPortReceiveSemState)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortReceiveSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortReceiveSemState =
        (AST_GET_COMM_PORT_INFO (i4FsMstCistPort))->u1PortRcvSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortProtMigrationSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortProtMigrationSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortProtMigrationSemState (INT4 i4FsMstCistPort,
                                          INT4
                                          *pi4RetValFsMstCistPortProtMigrationSemState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortProtMigrationSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsMstCistPort);

    *pi4RetValFsMstCistPortProtMigrationSemState =
        pAstCommPortInfo->u1PmigSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistProtocolMigrationCount
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistProtocolMigrationCount (INT4 i4FsMstCistPort,
                                       UINT4
                                       *pu4RetValFsMstCistProtocolMigrationCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistProtocolMigrationCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistProtocolMigrationCount =
        pAstPortEntry->u4ProtocolMigrationCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortDesignatedCost
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortDesignatedCost (INT4 i4FsMstCistPort,
                                   INT4 *pi4RetValFsMstCistPortDesignatedCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortDesignatedCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortDesignatedCost = pAstPerStPortInfo->u4RootCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRegionalRoot
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRegionalRoot (INT4 i4FsMstCistPort,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstCistPortRegionalRoot)
{
    tAstMstBridgeEntry *pAstMstBridgeEntry;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstCistPortRegionalRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistPortRegionalRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    pRetValFsMstCistPortRegionalRoot->pu1_OctetList[0] =
        (UINT1) ((pAstMstBridgeEntry->RegionalRootId.
                  u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistPortRegionalRoot->pu1_OctetList[1] =
        (UINT1) (pAstMstBridgeEntry->RegionalRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistPortRegionalRoot->pu1_OctetList + 2,
                (&pAstMstBridgeEntry->RegionalRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);
    pRetValFsMstCistPortRegionalRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRegionalPathCost
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRegionalPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRegionalPathCost (INT4 i4FsMstCistPort,
                                     INT4
                                     *pi4RetValFsMstCistPortRegionalPathCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortRegionalPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortRegionalPathCost =
        pAstPerStPortInfo->u4IntRootPathCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistSelectedPortRole
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistSelectedPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistSelectedPortRole (INT4 i4FsMstCistPort,
                                 INT4 *pi4RetValFsMstCistSelectedPortRole)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistSelectedPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistSelectedPortRole = pAstPerStPortInfo->u1SelectedPortRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistCurrentPortRole
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistCurrentPortRole (INT4 i4FsMstCistPort,
                                INT4 *pi4RetValFsMstCistCurrentPortRole)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistCurrentPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistCurrentPortRole = pAstPerStPortInfo->u1PortRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortInfoSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortInfoSemState (INT4 i4FsMstCistPort,
                                 INT4 *pi4RetValFsMstCistPortInfoSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortInfoSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortInfoSemState = pAstPerStPortInfo->u1PinfoSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRoleTransitionSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRoleTransitionSemState (INT4 i4FsMstCistPort,
                                           INT4
                                           *pi4RetValFsMstCistPortRoleTransitionSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortRoleTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortRoleTransitionSemState =
        pAstPerStPortInfo->u1ProleTrSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortStateTransitionSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortStateTransitionSemState (INT4 i4FsMstCistPort,
                                            INT4
                                            *pi4RetValFsMstCistPortStateTransitionSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortStateTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortStateTransitionSemState =
        pAstPerStPortInfo->u1PstateTrSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortTopologyChangeSemState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortTopologyChangeSemState (INT4 i4FsMstCistPort,
                                           INT4
                                           *pi4RetValFsMstCistPortTopologyChangeSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortTopologyChangeSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortTopologyChangeSemState =
        pAstPerStPortInfo->u1TopoChSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortHelloTime
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortHelloTime (INT4 i4FsMstCistPort,
                              INT4 *pi4RetValFsMstCistPortHelloTime)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortHelloTime = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortHelloTime
        = AST_SYS_TO_MGMT (pAstPortEntry->u2HelloTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortOperVersion
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortOperVersion (INT4 i4FsMstCistPort,
                                INT4 *pi4RetValFsMstCistPortOperVersion)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortOperVersion = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsMstCistPort);

    /* Check to avoid showing port operational version
     * as RSTP when the bridge level configuration (StpVersion) is STPCompatible.
     * This happens when the port is operationally down and StpVersion is set to STPCompatible.
     */
    /* Return operational version as STP compatible when 
     * the global configuration is STPCompatible.
     */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        *pi4RetValFsMstCistPortOperVersion = AST_STP_COMPATIBLE_MODE;
        return (INT1) SNMP_SUCCESS;
    }
    if (pAstCommPortInfo->bSendRstp == MST_FALSE)
    {
        *pi4RetValFsMstCistPortOperVersion = AST_STP_COMPATIBLE_MODE;
    }
    else
    {
        if (AST_FORCE_VERSION == AST_VERSION_2)
        {
            *pi4RetValFsMstCistPortOperVersion = AST_RST_MODE;
        }
        else
        {
            *pi4RetValFsMstCistPortOperVersion = AST_MST_MODE;
        }
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRestrictedRole
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRestrictedRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRestrictedRole (INT4 i4FsMstCistPort,
                                   INT4 *pi4RetValFsMstCistPortRestrictedRole)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortRestrictedRole = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_RESTRICTED_ROLE (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortRestrictedRole = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortRestrictedRole = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRestrictedTCN
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRestrictedTCN (INT4 i4FsMstCistPort,
                                  INT4 *pi4RetValFsMstCistPortRestrictedTCN)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortRestrictedTCN = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_RESTRICTED_TCN (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortRestrictedTCN = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortRestrictedTCN = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortAdminPathCost
 Input       :  The Indices
                FsMstCistPort
                The Object 
                retValFsMstCistPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMstCistPortAdminPathCost (INT4 i4FsMstCistPort,
                                  INT4 *pi4RetValFsMstCistPortAdminPathCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortAdminPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);
    if (pAstPerStPortInfo == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;

    }

    *pi4RetValFsMstCistPortAdminPathCost =
        (INT4) pAstPerStPortInfo->u4PortAdminPathCost;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortEnableBPDURx (INT4 i4FsMstCistPort,
                                 INT4 *pi4RetValFsMstCistPortEnableBPDURx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortEnableBPDURx = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_ENABLE_BPDU_RX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortEnableBPDURx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortEnableBPDURx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortEnableBPDUTx (INT4 i4FsMstCistPort,
                                 INT4 *pi4RetValFsMstCistPortEnableBPDUTx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortEnableBPDUTx = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortEnableBPDUTx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortEnableBPDUTx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortPseudoRootId
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortPseudoRootId (INT4 i4FsMstCistPort,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstCistPortPseudoRootId)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");

        pu1List = pRetValFsMstCistPortPseudoRootId->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstCistPortPseudoRootId->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);
    if (pAstPerStPortInfo == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;

    }

    pRetValFsMstCistPortPseudoRootId->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstCistPortPseudoRootId->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstCistPortPseudoRootId->pu1_OctetList +
                AST_BRG_PRIORITY_SIZE,
                (&pAstPerStPortInfo->PseudoRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);

    pRetValFsMstCistPortPseudoRootId->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortIsL2Gp
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortIsL2Gp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortIsL2Gp (INT4 i4FsMstCistPort,
                           INT4 *pi4RetValFsMstCistPortIsL2Gp)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortIsL2Gp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_IS_L2GP (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortIsL2Gp = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortIsL2Gp = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortLoopGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                retValFsMstCistPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortLoopGuard (INT4 i4FsMstCistPort,
                              INT4 *pi4RetValFsMstCistPortLoopGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortLoopGuard = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_LOOP_GUARD (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsMstCistPortLoopGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortLoopGuard = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRcvdEvent
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRcvdEvent (INT4 i4FsMstCistPort,
                              INT4 *pi4RetValFsMstCistPortRcvdEvent)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortRcvdEvent = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pi4RetValFsMstCistPortRcvdEvent = pAstPortEntry->i4RcvdEvent;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRcvdEventSubType
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRcvdEventSubType (INT4 i4FsMstCistPort,
                                     INT4
                                     *pi4RetValFsMstCistPortRcvdEventSubType)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstCistPortRcvdEventSubType = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pi4RetValFsMstCistPortRcvdEventSubType = pAstPortEntry->i4RcvdEventSubType;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRcvdEventTimeStamp
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRcvdEventTimeStamp (INT4 i4FsMstCistPort,
                                       UINT4
                                       *pu4RetValFsMstCistPortRcvdEventTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstCistPortRcvdEventTimeStamp = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pu4RetValFsMstCistPortRcvdEventTimeStamp =
        pAstPortEntry->u4RcvdEventTimeStamp;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortBpduGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                retValFsMstCistPortBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortBpduGuard (INT4 i4FsMstCistPort,
                              INT4 *pi4RetValFsMstCistPortBpduGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: MST Module not Started!\n");
        *pi4RetValFsMstCistPortBpduGuard = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortBpduGuard = pAstPortEntry->CommPortInfo.u4BpduGuard;

    return SNMP_SUCCESS;
}

/****************************************************************************
   Function    :  nmhGetFsMstCistPortErrorRecovery
   Input       :  The Indices
                  FsMstCistPort

                  The Object
                  retValFsMstCistPortErrorRecovery
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortErrorRecovery (INT4 i4FsMstCistPort,
                                  INT4 *pi4RetValFsMstCistPortErrorRecovery)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pi4RetValFsMstCistPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {
        *pi4RetValFsMstCistPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortErrorRecovery
        = (INT4) AST_SYS_TO_MGMT (pAstPortEntry->u4ErrorRecovery);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstPortBpduInconsistentState
 Input       :  The Indices
                FsMstCistPort

                The Object
                retValFsMstPortBpduInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPortBpduInconsistentState (INT4 i4FsMstCistPort,
                                      INT4
                                      *pi4RetValFsMstPortBpduInconsistentState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsMstCistPort);

    if (NULL != pAstCommPortInfo)
    {
        *pi4RetValFsMstPortBpduInconsistentState =
            pAstCommPortInfo->bBpduInconsistent;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstPortBpduGuardAction
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPortBpduGuardAction (INT4 i4FsMstCistPort,
                                INT4 *pi4RetValFsMstPortBpduGuardAction)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MST Module not Started!\n");
        *pi4RetValFsMstPortBpduGuardAction = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    *pi4RetValFsMstPortBpduGuardAction =
        pAstPortEntry->CommPortInfo.u1BpduGuardAction;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMstPortBpduGuardAction
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstPortBpduGuardAction (INT4 i4FsMstCistPort,
                                INT4 i4SetValFsMstPortBpduGuardAction)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->uMsg.u1BpduGuardAction =
        (UINT1) i4SetValFsMstPortBpduGuardAction;

    pAstMsgNode->MsgType = AST_MST_BPDUGUARD_ACTION_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstPortBpduGuardAction,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstPortBpduGuardAction));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstPortBpduGuardAction
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstPortBpduGuardAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstPortBpduGuardAction (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                   INT4 i4TestValFsMstPortBpduGuardAction)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValFsMstPortBpduGuardAction != AST_PORT_ADMIN_DOWN) &&
        (i4TestValFsMstPortBpduGuardAction != AST_PORT_STATE_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
  Function    :  nmhGetFsMstCistPortRootGuard
  Input       :  The Indices
                 FsMstCistPort

                 The Object
                 retValFsMstCistPortRootGuard
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRootGuard (INT4 i4FsMstCistPort,
                              INT4 *pi4RetValFsMstCistPortRootGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortRootGuard = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstCistPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AST_PORT_ROOT_GUARD (pAstPortEntry) == MST_TRUE)
    {
        *pi4RetValFsMstCistPortRootGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortRootGuard = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortRootInconsistentState
 Input       :  The Indices
                FsMstCistPort

                The Object
                retValFsMstCistPortRootInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortRootInconsistentState (INT4 i4FsMstCistPort,
                                          INT4
                                          *pi4RetValFsMstCistPortRootInconsistentState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortRootInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAstPerStPortInfo->bRootInconsistent == MST_TRUE)
    {
        *pi4RetValFsMstCistPortRootInconsistentState = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortRootInconsistentState = AST_SNMP_FALSE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhGetFsMstMstiPortRootInconsistentState
  Input       :  The Indices
                 FsMstMstiPort
                 FsMstInstanceIndex
 
                 The Object
                 retValFsMstMstiPortRootInconsistentState
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortRootInconsistentState (INT4 i4FsMstMstiPort,
                                          INT4 i4FsMstInstanceIndex,
                                          INT4
                                          *pi4RetValFsMstMstiPortRootInconsistentState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiPortRootInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    /*  if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
       {
       AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
       "SNMP:Such a Instance Does not exist!\n");
       return (INT1) SNMP_FAILURE;
       } */

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAstPerStPortInfo->bRootInconsistent == MST_TRUE)
    {
        *pi4RetValFsMstMstiPortRootInconsistentState = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstMstiPortRootInconsistentState = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstCistPortProtocolMigration
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortProtocolMigration (INT4 i4FsMstCistPort,
                                      INT4
                                      i4SetValFsMstCistPortProtocolMigration)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (i4SetValFsMstCistPortProtocolMigration != AST_SNMP_TRUE)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PROTOCOL_MIGRATION_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortProtocolMigration,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", AST_CURR_CONTEXT_ID (),
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortProtocolMigration));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortRestrictedRole
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortRestrictedRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortRestrictedRole (INT4 i4FsMstCistPort,
                                   INT4 i4SetValFsMstCistPortRestrictedRole)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstBoolean         bStatus = RST_TRUE;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (i4SetValFsMstCistPortRestrictedRole == AST_SNMP_TRUE)
    {
        bStatus = RST_TRUE;
        i4SetValFsMstCistPortRestrictedRole = RST_TRUE;
    }
    else
    {
        bStatus = RST_FALSE;
        i4SetValFsMstCistPortRestrictedRole = RST_FALSE;
    }

    if (AST_IS_MST_STARTED ())
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (MstSetPortRestrictedRole (pPortEntry, bStatus) != RST_SUCCESS)
        {
            i1RetVal = SNMP_FAILURE;
        }
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortRestrictedRole,
                              u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          AST_GET_IFINDEX (i4FsMstCistPort),
                          i4SetValFsMstCistPortRestrictedRole));
        AstSelectContext (u4CurrContextId);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortRestrictedTCN
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortRestrictedTCN (INT4 i4FsMstCistPort,
                                  INT4 i4SetValFsMstCistPortRestrictedTCN)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFsMstCistPortRestrictedTCN == AST_SNMP_TRUE)
    {
        AST_PORT_RESTRICTED_TCN (pPortEntry) = RST_TRUE;
    }
    else
    {
        AST_PORT_RESTRICTED_TCN (pPortEntry) = RST_FALSE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortRestrictedTCN,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortRestrictedTCN));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortAdminPathCost
 Input       :  The Indices
                FsMstCistPort
 
                The Object 
                setValFsMstCistPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMstCistPortAdminPathCost (INT4 i4FsMstCistPort,
                                  INT4 i4SetValFsMstCistPortAdminPathCost)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    i1RetVal = nmhSetDot1dStpPortAdminPathCost (i4FsMstCistPort,
                                                i4SetValFsMstCistPortAdminPathCost);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortAdminPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortEnableBPDURx (INT4 i4FsMstCistPort,
                                 INT4 i4SetValFsMstCistPortEnableBPDURx)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsMstCistPortEnableBPDURx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsMstCistPortEnableBPDURx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstCistPortEnableBPDURx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_RX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortEnableBPDURx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortEnableBPDURx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortEnableBPDUTx (INT4 i4FsMstCistPort,
                                 INT4 i4SetValFsMstCistPortEnableBPDUTx)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsMstCistPortEnableBPDUTx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsMstCistPortEnableBPDUTx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstCistPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_TX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortEnableBPDUTx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortEnableBPDUTx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortPseudoRootId
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortPseudoRootId (INT4 i4FsMstCistPort,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsMstCistPortPseudoRootId)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeId        PseudoRootId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (!(AST_IS_MST_STARTED ()))
    {
        return SNMP_FAILURE;
    }

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (PseudoRootId.BridgeAddr, 0, AST_MAC_ADDR_SIZE);

    PseudoRootId.u2BrgPriority = 0;

    PseudoRootId.u2BrgPriority =
        (UINT2) (((pSetValFsMstCistPortPseudoRootId->pu1_OctetList[0] << 8) |
                  pSetValFsMstCistPortPseudoRootId->pu1_OctetList[1]));

    AST_MEMCPY (PseudoRootId.BridgeAddr,
                pSetValFsMstCistPortPseudoRootId->pu1_OctetList +
                AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstCistPort,
                                              MST_CIST_CONTEXT);

    if (AST_COMPARE_BRGID (&pPerStPortInfo->PseudoRootId, &PseudoRootId)
        == RST_BRGID1_SAME)
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PSEUDO_ROOTID_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    pAstMsgNode->uMsg.BridgeId.u2BrgPriority = PseudoRootId.u2BrgPriority;

    AST_MEMCPY (pAstMsgNode->uMsg.BridgeId.BridgeAddr, PseudoRootId.BridgeAddr,
                AST_MAC_ADDR_SIZE);

    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortPseudoRootId, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      pSetValFsMstCistPortPseudoRootId));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortIsL2Gp
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortIsL2Gp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortIsL2Gp (INT4 i4FsMstCistPort,
                           INT4 i4SetValFsMstCistPortIsL2Gp)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (((i4SetValFsMstCistPortIsL2Gp == AST_SNMP_TRUE) &&
         (AST_PORT_IS_L2GP (pPortEntry) == MST_TRUE)) ||
        ((i4SetValFsMstCistPortIsL2Gp == AST_SNMP_FALSE) &&
         (AST_PORT_IS_L2GP (pPortEntry) == MST_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstCistPortIsL2Gp == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bPortL2gp = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bPortL2gp = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_L2GP_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    pAstMsgNode->u2InstanceId = MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortIsL2Gp, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortIsL2Gp));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortLoopGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                setValFsMstCistPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortLoopGuard (INT4 i4FsMstCistPort,
                              INT4 i4SetValFsMstCistPortLoopGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_LOOP_GUARD_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    if (i4SetValFsMstCistPortLoopGuard == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bLoopGuard = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bLoopGuard = RST_FALSE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortLoopGuard, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortLoopGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortBpduGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                setValFsMstCistPortBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortBpduGuard (INT4 i4FsMstCistPort,
                              INT4 i4SetValFsMstCistPortBpduGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_MST_CONFIG_BPDUGUARD_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    pAstMsgNode->uMsg.u4BpduGuardStatus = i4SetValFsMstCistPortBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortBpduGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
   Function    :  nmhSetFsMstCistPortErrorRecovery
   Input       :  The Indices
                  FsMstCistPort

                  The Object
                  setValFsMstCistPortErrorRecovery
   Output      :  The Set Low Lev Routine Take the Indices &
                  Sets the Value accordingly.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMstCistPortErrorRecovery (INT4 i4FsMstCistPort,
                                  INT4 i4SetValFsMstCistPortErrorRecovery)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_ERROR_RECOVERY_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    pAstMsgNode->uMsg.u4ErrorRecovery =
        (UINT4) AST_MGMT_TO_SYS (i4SetValFsMstCistPortErrorRecovery);

    RM_GET_SEQ_NUM (&u4SeqNum);
    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortErrorRecovery,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortErrorRecovery));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhSetFsMstCistPortRootGuard
  Input       :  The Indices
                 FsMstCistPort

                 The Object
                 setValFsMstCistPortRootGuard
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMstCistPortRootGuard (INT4 i4FsMstCistPort,
                              INT4 i4SetValFsMstCistPortRootGuard)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstBoolean         bStatus = RST_TRUE;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstValidatePortEntry (i4FsMstCistPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (i4SetValFsMstCistPortRootGuard == AST_SNMP_TRUE)
    {
        bStatus = RST_TRUE;
        i4SetValFsMstCistPortRootGuard = MST_TRUE;
    }
    else
    {
        bStatus = RST_FALSE;
        i4SetValFsMstCistPortRootGuard = MST_FALSE;
    }

    if (AST_IS_MST_STARTED ())
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (MstSetPortRootGuard (pPortEntry, bStatus) != MST_SUCCESS)
        {
            i1RetVal = SNMP_FAILURE;
        }
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortRootGuard,
                              u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          AST_GET_IFINDEX (i4FsMstCistPort),
                          i4SetValFsMstCistPortRootGuard));
        AstSelectContext (u4CurrContextId);
    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortPathCost
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortPathCost (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                INT4 i4TestValFsMstCistPortPathCost)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortPathCost < 1) ||
        (i4TestValFsMstCistPortPathCost > AST_PORT_PATHCOST_100KBPS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_PORT_PATHCOST_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortPriority
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                INT4 i4TestValFsMstCistPortPriority)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortPriority < AST_MIN_PORT_PRIORITY) ||
        (i4TestValFsMstCistPortPriority > AST_MAX_PORT_PRIORITY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsMstCistPortPriority & AST_PORTPRIORITY_PERMISSIBLE_MASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_STP_PORT_PRIORITY_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortAdminP2P
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortAdminP2P
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortAdminP2P (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                INT4 i4TestValFsMstCistPortAdminP2P)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_PORT_SISP_LOGICAL ((UINT2) i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_SISP_INVALID_PROP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortAdminP2P != AST_FORCE_TRUE) &&
        (i4TestValFsMstCistPortAdminP2P != AST_FORCE_FALSE) &&
        (i4TestValFsMstCistPortAdminP2P != AST_AUTO))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortAdminEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortAdminEdgeStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMstCistPort,
                                       INT4
                                       i4TestValFsMstCistPortAdminEdgeStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsMstCistPort);
    if (pAstPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortAdminEdgeStatus != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortAdminEdgeStatus != AST_SNMP_FALSE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_IS_PORT_SISP_LOGICAL ((UINT2) i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_SISP_INVALID_PROP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortProtocolMigration
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortProtocolMigration (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMstCistPort,
                                         INT4
                                         i4TestValFsMstCistPortProtocolMigration)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortProtocolMigration != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortProtocolMigration != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (AstSispIsSispPortPresentInCtxt (pPortEntry->u4ContextId) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_PROTO_MIG_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstCistPort,
                                              MST_CIST_CONTEXT);
    if (pPerStPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pPortEntry->u1EntryStatus == (UINT1) AST_PORT_OPER_UP) &&
        (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_TRUE))
    {
        return (INT1) SNMP_SUCCESS;
    }

    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC, "Port not Enabled..!\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistForcePortState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistForcePortState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistForcePortState (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMstCistPort,
                                  INT4 i4TestValFsMstCistForcePortState)
{
    tAstPortEntry      *pAstPortEntry = NULL;
#ifdef ICCH_WANTED
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1IsMclag = 0;
#endif
    UINT1               u1TunnelStatus;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*Check for Pseudo/AC Interface, no validation for Pseudo/AC Interface */
    /* Modified for Attachment Circuit interface */
    if (AstIsExtInterface (i4FsMstCistPort) == AST_TRUE)
    {
        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsMstCistPort);
        if ((i4TestValFsMstCistForcePortState == MST_FORCE_STATE_DISABLED) &&
            (pAstPortEntry == NULL))
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "Such a Port DOES NOT exist!\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistForcePortState != MST_FORCE_STATE_DISABLED) &&
        (i4TestValFsMstCistForcePortState != MST_FORCE_STATE_ENABLED))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsMstCistPort);
    if (pAstPortEntry == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsMstCistForcePortState == MST_FORCE_STATE_ENABLED)
    {
        if ((AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE) ||
            (AST_IS_CUSTOMER_EDGE_PORT ((UINT2) i4FsMstCistPort) == RST_TRUE))
        {
            /* Enabling the protocol status on a port is not allowed 
             * when the protocol tunnel status is set to Tunnel
             * on a port.*/
            AstL2IwfGetProtocolTunnelStatusOnPort (pAstPortEntry->u4IfIndex,
                                                   L2_PROTO_STP,
                                                   &u1TunnelStatus);

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_TUNNEL_PROTO_ERR);
                return SNMP_FAILURE;
            }
        }

        if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
        {
            AstL2IwfGetPortVlanTunnelStatus (pAstPortEntry->u4IfIndex,
                                             (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_PORT_TYPE_ERR);
                return SNMP_FAILURE;
            }
        }

#ifdef ICCH_WANTED
        if (i4TestValFsMstCistForcePortState == MST_FORCE_STATE_ENABLED)
        {
            IcchGetIcclIfIndex (&u4IcclIfIndex);

            if (u4IcclIfIndex == pAstPortEntry->u4IfIndex)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_NO_MSTP_ON_ICCL_ERR);
                return SNMP_FAILURE;
            }

            LaApiIsMclagInterface (pAstPortEntry->u4IfIndex, &u1IsMclag);

            if (u1IsMclag == OSIX_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_NO_MSTP_ON_MCLAG_ERR);
                return SNMP_FAILURE;
            }
        }
#endif
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortHelloTime
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortHelloTime (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMstCistPort,
                                 INT4 i4TestValFsMstCistPortHelloTime)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstCistPortHelloTime) != AST_OK)
        || (i4TestValFsMstCistPortHelloTime < AST_MIN_HELLOTIME_VAL)
        || (i4TestValFsMstCistPortHelloTime > AST_MAX_HELLOTIME_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_BRG_HELLOTIME_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValFsMstCistPortHelloTime),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortRestrictedRole
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortRestrictedRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortRestrictedRole (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMstCistPort,
                                      INT4 i4TestValFsMstCistPortRestrictedRole)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortRestrictedRole != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortRestrictedRole != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, restrcitedRole cannot be set to TRUE. */
    if (i4TestValFsMstCistPortRestrictedRole == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == MST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP restrictedRole can not be set to false. */
    if (i4TestValFsMstCistPortRestrictedRole == AST_SNMP_FALSE)
    {

        if ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsMstCistPort) == RST_TRUE) ||
            (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
             AST_PROP_CUSTOMER_NETWORK_PORT)
            || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                AST_PROP_CUSTOMER_EDGE_PORT))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortRestrictedTCN
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortRestrictedTCN (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMstCistPort,
                                     INT4 i4TestValFsMstCistPortRestrictedTCN)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortRestrictedTCN != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortRestrictedTCN != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, restrcitedTcn cannot be set to TRUE. */
    if (i4TestValFsMstCistPortRestrictedTCN == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == MST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP restrictedTcn can not be set to false. */
    if (i4TestValFsMstCistPortRestrictedTCN == AST_SNMP_FALSE)
    {

        if ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsMstCistPort) == RST_TRUE) ||
            (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
             AST_PROP_CUSTOMER_NETWORK_PORT)
            || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                AST_PROP_CUSTOMER_EDGE_PORT))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortAdminPathCost
 Input       :  The Indices
                FsMstCistPort
 
                The Object 
                testValFsMstCistPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMstCistPortAdminPathCost (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMstCistPort,
                                     INT4 i4TestValFsMstCistPortAdminPathCost)
{

    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstCistPort,
                                              MST_CIST_CONTEXT);
    if (pPerStPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortAdminPathCost > AST_PORT_PATHCOST_100KBPS) ||
        (i4TestValFsMstCistPortAdminPathCost < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortEnableBPDURx (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                    INT4 i4TestValFsMstCistPortEnableBPDURx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortEnableBPDURx != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortEnableBPDURx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortEnableBPDUTx (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                    INT4 i4TestValFsMstCistPortEnableBPDUTx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortEnableBPDUTx != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortEnableBPDUTx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /*
     * enableBPDUTx can not be set to TRUE on L2GP Ports 
     * and Customer Backbone Ports
     * */

    if (i4TestValFsMstCistPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }

        if (AST_IS_CUSTOMER_BACKBONE_PORT (i4FsMstCistPort) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortPseudoRootId
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortPseudoRootId (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsMstCistPortPseudoRootId)
{
    UINT2               u2BrgPriority = 0;
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pTestValFsMstCistPortPseudoRootId->i4_Length !=
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u2BrgPriority =
        (UINT2) (((pTestValFsMstCistPortPseudoRootId->pu1_OctetList[0] << 8) |
                  pTestValFsMstCistPortPseudoRootId->pu1_OctetList[1]));

    if (u2BrgPriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_BRIDGE_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortIsL2Gp
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortIsL2Gp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortIsL2Gp (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                              INT4 i4TestValFsMstCistPortIsL2Gp)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortIsL2Gp != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortIsL2Gp != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMstCistPortIsL2Gp == AST_SNMP_TRUE)
    {
        if (AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            return SNMP_FAILURE;
        }
        /* if restrcitedRole or restrictedTcn is set,
         * L2gp con not set to TRUE.
         * */
        if ((AST_PORT_RESTRICTED_ROLE (pPortEntry) == RST_TRUE) ||
            (AST_PORT_RESTRICTED_TCN (pPortEntry) == RST_TRUE))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortLoopGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                testValFsMstCistPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortLoopGuard (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                 INT4 i4TestValFsMstCistPortLoopGuard)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortLoopGuard != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortLoopGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (pPortEntry->bRootGuard == AST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_ROOTGUARD_ENABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortBpduGuard
 Input       :  The Indices
                FsMstCistPort

                The Object
                testValFsMstCistPortBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortBpduGuard (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMstCistPort,
                                 INT4 i4TestValFsMstCistPortBpduGuard)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: MST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsMstCistPortBpduGuard != AST_BPDUGUARD_DISABLE) &&
        (i4TestValFsMstCistPortBpduGuard != AST_BPDUGUARD_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
   Function    :  nmhTestv2FsMstCistPortErrorRecovery
   Input       :  The Indices
                  FsMstCistPort

                  The Object
                  testValFsMstCistPortErrorRecovery
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
   Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMstCistPortErrorRecovery (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                     INT4 i4TestValFsMstCistPortErrorRecovery)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsMstCistPortErrorRecovery) !=
         AST_OK) || (i4TestValFsMstCistPortErrorRecovery < AST_MIN_ERRRECOVERY)
        || (i4TestValFsMstCistPortErrorRecovery > AST_MAX_ERRRECOVERY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhTestv2FsMstCistPortRootGuard
  Input       :  The Indices
                 FsMstCistPort

                 The Object
                 testValFsMstCistPortRootGuard
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsMstCistPort,
                                 INT4 i4TestValFsMstCistPortRootGuard)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortRootGuard != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortRootGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstCistPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstCistPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pPortEntry->bLoopGuard == AST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_LOOPGUARD_ENABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, Root Guard cannot be set to TRUE. */
    if (i4TestValFsMstCistPortRootGuard == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == MST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP Root Guard can not be set to false. */
    if (i4TestValFsMstCistPortRootGuard == AST_SNMP_FALSE)
    {

        if ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsMstCistPort) == MST_TRUE) ||
            (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
             AST_PROP_CUSTOMER_NETWORK_PORT)
            || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                AST_PROP_CUSTOMER_EDGE_PORT))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FsMstCistPortTable
 Input       :  The Indices
                FsMstCistPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstCistPortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstMstiPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstMstiPortTable
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMstMstiPortTable (INT4 i4FsMstMstiPort,
                                            INT4 i4FsMstInstanceIndex)
{
    if ((i4FsMstInstanceIndex < AST_MIN_MST_INSTANCES) ||
        (i4FsMstInstanceIndex >= AST_MAX_MST_INSTANCES))
    {
        if (i4FsMstInstanceIndex != AST_TE_MSTID)
        {
            return SNMP_FAILURE;
        }
    }

    if ((i4FsMstMstiPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstMstiPort > AST_MAX_NUM_PORTS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstMstiPortTable
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMstMstiPortTable (INT4 *pi4FsMstMstiPort,
                                    INT4 *pi4FsMstInstanceIndex)
{
    return (nmhGetNextIndexFsMstMstiPortTable (0, pi4FsMstMstiPort,
                                               0, pi4FsMstInstanceIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstMstiPortTable
 Input       :  The Indices
                FsMstMstiPort
                nextFsMstMstiPort
                FsMstInstanceIndex
                nextFsMstInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstMstiPortTable (INT4 i4FsMstMstiPort,
                                   INT4 *pi4NextFsMstMstiPort,
                                   INT4 i4FsMstInstanceIndex,
                                   INT4 *pi4NextFsMstInstanceIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStInfo      *pAstPerStInfo = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT2               u2PortIndex;
    UINT2               u2InstIndex;
    UINT1               u1Flag = MST_FALSE;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (i4FsMstMstiPort < 0 || i4FsMstMstiPort > AST_MAX_NUM_PORTS)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMstMstiPort == 0) || (i4FsMstInstanceIndex < 0))
    {
        i4FsMstInstanceIndex = 0;
        i4FsMstMstiPort++;
    }

    u2PortIndex = (UINT2) i4FsMstMstiPort;
    AST_GET_NEXT_PORT_ENTRY (u2PortIndex, pAstPortEntry)
    {
        if (pAstPortEntry == NULL)
        {
            i4FsMstInstanceIndex = 0;
            continue;
        }

        u2InstIndex = (UINT2) (i4FsMstInstanceIndex + 1);
        AST_GET_NEXT_BUF_INSTANCE (u2InstIndex, pAstPerStInfo)
        {
            if (pAstPerStInfo == NULL)
            {
                continue;
            }
            pAstPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortIndex, u2InstIndex);
            if (pAstPerStPortInfo != NULL)
            {
                if (u1Flag == MST_FALSE)
                {
                    *pi4NextFsMstMstiPort = pAstPortEntry->u2PortNo;
                    *pi4NextFsMstInstanceIndex = pAstPerStInfo->u2InstanceId;
                    u1Flag = MST_TRUE;
                }
            }
        }

        /*Next instance should be TE_MSTID, if it has created for 
         * current context */
        if (u1Flag == MST_FALSE)
        {
            if (AST_IS_TE_MSTID_VALID (i4FsMstInstanceIndex))
            {
                *pi4NextFsMstMstiPort = pAstPortEntry->u2PortNo;
                *pi4NextFsMstInstanceIndex = AST_TE_MSTID;
                return SNMP_SUCCESS;
            }

            else
            {
                i4FsMstInstanceIndex = 0;
            }
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortPathCost (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                             INT4 *pi4RetValFsMstMstiPortPathCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiPortPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /*For Instance TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortPathCost = pAstPerStPortInfo->u4PortPathCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortPriority
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortPriority (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                             INT4 *pi4RetValFsMstMstiPortPriority)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1               u1Priority = 0;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiPortPriority = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortPriority = u1Priority;
        return SNMP_SUCCESS;

    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    MST_GET_PORT_PRIORITY (pAstPerStPortInfo->u1PortPriority, u1Priority);

    *pi4RetValFsMstMstiPortPriority = u1Priority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortDesignatedRoot
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortDesignatedRoot (INT4 i4FsMstMstiPort,
                                   INT4 i4FsMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMstMstiPortDesignatedRoot)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstMstiPortDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        pu1List = pRetValFsMstMstiPortDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    pRetValFsMstMstiPortDesignatedRoot->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->RootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstMstiPortDesignatedRoot->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->RootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstMstiPortDesignatedRoot->pu1_OctetList + 2,
                (&pAstPerStPortInfo->RootId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstMstiPortDesignatedRoot->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortDesignatedBridge
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortDesignatedBridge (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMstMstiPortDesignatedBridge)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstMstiPortDesignatedBridge->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        pu1List = pRetValFsMstMstiPortDesignatedBridge->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    pRetValFsMstMstiPortDesignatedBridge->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstMstiPortDesignatedBridge->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->DesgBrgId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstMstiPortDesignatedBridge->pu1_OctetList + 2,
                (&pAstPerStPortInfo->DesgBrgId.BridgeAddr), AST_MAC_ADDR_SIZE);

    pRetValFsMstMstiPortDesignatedBridge->i4_Length = AST_MAC_ADDR_SIZE + 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortDesignatedPort
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortDesignatedPort (INT4 i4FsMstMstiPort,
                                   INT4 i4FsMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMstMstiPortDesignatedPort)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        pu1List = pRetValFsMstMstiPortDesignatedPort->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);

        pRetValFsMstMstiPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        pu1List = pRetValFsMstMstiPortDesignatedPort->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);

        pRetValFsMstMstiPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    pu1List = pRetValFsMstMstiPortDesignatedPort->pu1_OctetList;
    u2Val = pAstPerStPortInfo->u2DesgPortId;
    AST_PUT_2BYTE (pu1List, u2Val);

    pu1List -= 2;
    pRetValFsMstMstiPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortState (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                          INT4 *pi4RetValFsMstMstiPortState)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (i4FsMstMstiPort))
        != AST_CUSTOMER_EDGE_PORT)
    {
        *pi4RetValFsMstMstiPortState =
            (INT4) AstL2IwfGetInstPortState ((UINT2) i4FsMstInstanceIndex,
                                             (UINT2)
                                             AST_GET_IFINDEX (i4FsMstMstiPort));
    }
    else
    {
        *pi4RetValFsMstMstiPortState = AST_PORT_STATE_DISCARDING;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiForcePortState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiForcePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiForcePortState (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                               INT4 *pi4RetValFsMstMstiForcePortState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiForcePortState = MST_FORCE_STATE_DISABLED;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);
    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo->bAdminPortEnabled == (tAstBoolean) MST_TRUE)
    {
        *pi4RetValFsMstMstiForcePortState = MST_FORCE_STATE_ENABLED;
    }
    else
    {
        *pi4RetValFsMstMstiForcePortState = MST_FORCE_STATE_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortEffectivePortState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortEffectivePortState (INT4 i4FsMstMstiPort,
                                       INT4 i4FsMstInstanceIndex,
                                       INT4
                                       *pi4RetValFsMstMstiPortEffectivePortState)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortEffectivePortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortEffectivePortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstMstiPort);
    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (((pAstPortEntry->u1EntryStatus) == AST_PORT_OPER_UP)
        && (pRstPortInfo->bPortEnabled) == MST_TRUE)

    {
        *pi4RetValFsMstMstiPortEffectivePortState = MST_PORT_OPER_ENABLED;
    }
    else
    {
        *pi4RetValFsMstMstiPortEffectivePortState = MST_PORT_OPER_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
 
                The Object 
                retValFsMstMstiPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMstMstiPortAdminPathCost (INT4 i4FsMstMstiPort,
                                  INT4 i4FsMstInstanceIndex,
                                  INT4 *pi4RetValFsMstMstiPortAdminPathCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortAdminPathCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);
    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortAdminPathCost =
        (INT4) pAstPerStPortInfo->u4PortAdminPathCost;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortForwardTransitions
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortForwardTransitions (INT4 i4FsMstMstiPort,
                                       INT4 i4FsMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMstMstiPortForwardTransitions)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiPortForwardTransitions = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiPortForwardTransitions = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstMstiPortForwardTransitions =
        pAstPerStPortInfo->u4NumFwdTransitions;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortReceivedBPDUs
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortReceivedBPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortReceivedBPDUs (INT4 i4FsMstMstiPort,
                                  INT4 i4FsMstInstanceIndex,
                                  UINT4 *pu4RetValFsMstMstiPortReceivedBPDUs)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiPortReceivedBPDUs = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiPortReceivedBPDUs = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstMstiPortReceivedBPDUs = pAstPerStPortInfo->u4NumBpdusRx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortTransmittedBPDUs
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortTransmittedBPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortTransmittedBPDUs (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMstMstiPortTransmittedBPDUs)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiPortTransmittedBPDUs = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiPortTransmittedBPDUs = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstMstiPortTransmittedBPDUs = pAstPerStPortInfo->u4NumBpdusTx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortInvalidBPDUsRcvd
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortInvalidBPDUsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortInvalidBPDUsRcvd (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMstMstiPortInvalidBPDUsRcvd)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiPortInvalidBPDUsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4RetValFsMstMstiPortInvalidBPDUsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstMstiPortInvalidBPDUsRcvd =
        pAstPerStPortInfo->u4NumInvalidBpdusRx;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortDesignatedCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortDesignatedCost (INT4 i4FsMstMstiPort,
                                   INT4 i4FsMstInstanceIndex,
                                   INT4 *pi4RetValFsMstMstiPortDesignatedCost)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortDesignatedCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortDesignatedCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortDesignatedCost = pAstPerStPortInfo->u4RootCost;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiSelectedPortRole
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiSelectedPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiSelectedPortRole (INT4 i4FsMstMstiPort,
                                 INT4 i4FsMstInstanceIndex,
                                 INT4 *pi4RetValFsMstMstiSelectedPortRole)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiSelectedPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiSelectedPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiSelectedPortRole = pAstPerStPortInfo->u1SelectedPortRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiCurrentPortRole
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiCurrentPortRole (INT4 i4FsMstMstiPort,
                                INT4 i4FsMstInstanceIndex,
                                INT4 *pi4RetValFsMstMstiCurrentPortRole)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiCurrentPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiCurrentPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiCurrentPortRole = pAstPerStPortInfo->u1PortRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortInfoSemState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortInfoSemState (INT4 i4FsMstMstiPort,
                                 INT4 i4FsMstInstanceIndex,
                                 INT4 *pi4RetValFsMstMstiPortInfoSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortInfoSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortInfoSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortInfoSemState = pAstPerStPortInfo->u1PinfoSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortRoleTransitionSemState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortRoleTransitionSemState (INT4 i4FsMstMstiPort,
                                           INT4 i4FsMstInstanceIndex,
                                           INT4
                                           *pi4RetValFsMstMstiPortRoleTransitionSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortRoleTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortRoleTransitionSemState =
            RST_PROLETRSM_STATE_DISABLED_PORT;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortRoleTransitionSemState =
        pAstPerStPortInfo->u1ProleTrSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortStateTransitionSemState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortStateTransitionSemState (INT4 i4FsMstMstiPort,
                                            INT4 i4FsMstInstanceIndex,
                                            INT4
                                            *pi4RetValFsMstMstiPortStateTransitionSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortStateTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortStateTransitionSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortStateTransitionSemState =
        pAstPerStPortInfo->u1PstateTrSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortTopologyChangeSemState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortTopologyChangeSemState (INT4 i4FsMstMstiPort,
                                           INT4 i4FsMstInstanceIndex,
                                           INT4
                                           *pi4RetValFsMstMstiPortTopologyChangeSemState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstMstiPortTopologyChangeSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pi4RetValFsMstMstiPortTopologyChangeSemState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortTopologyChangeSemState =
        pAstPerStPortInfo->u1TopoChSmState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstMstiPortPseudoRootId (INT4 i4FsMstMstiPort,
                                 INT4 i4FsMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMstMstiPortPseudoRootId)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");

        pu1List = pRetValFsMstMstiPortPseudoRootId->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsMstMstiPortPseudoRootId->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        AST_MEMSET (pRetValFsMstMstiPortPseudoRootId->pu1_OctetList
                    + AST_BRG_PRIORITY_SIZE, 0, AST_MAC_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMstMstiPortPseudoRootId->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsMstMstiPortPseudoRootId->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsMstMstiPortPseudoRootId->pu1_OctetList
                + AST_BRG_PRIORITY_SIZE,
                (&pAstPerStPortInfo->PseudoRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);

    pRetValFsMstMstiPortPseudoRootId->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiPortPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                testValFsMstMstiPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiPortPathCost (UINT4 *pu4ErrorCode, INT4 i4FsMstMstiPort,
                                INT4 i4FsMstInstanceIndex,
                                INT4 i4TestValFsMstMstiPortPathCost)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstMstiPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist for this Instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_PORT_INST_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiPortPathCost < AST_MIN_PORT_PATH_COST) ||
        (i4TestValFsMstMstiPortPathCost > AST_MAX_PORT_PATH_COST))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiPortPriority
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                testValFsMstMstiPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsMstMstiPort,
                                INT4 i4FsMstInstanceIndex,
                                INT4 i4TestValFsMstMstiPortPriority)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstMstiPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist for this Instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_PORT_INST_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiPortPriority < AST_MIN_PORT_PRIORITY) ||
        (i4TestValFsMstMstiPortPriority > AST_MAX_PORT_PRIORITY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMstMstiPortPriority & AST_PORTPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_PORT_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiForcePortState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                testValFsMstMstiForcePortState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiForcePortState (UINT4 *pu4ErrorCode, INT4 i4FsMstMstiPort,
                                  INT4 i4FsMstInstanceIndex,
                                  INT4 i4TestValFsMstMstiForcePortState)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist for this Instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_PORT_INST_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiForcePortState != MST_FORCE_STATE_DISABLED) &&
        (i4TestValFsMstMstiForcePortState != MST_FORCE_STATE_ENABLED))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
  
                The Object 
                testValFsMstMstiPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMstMstiPortAdminPathCost (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     INT4 i4TestValFsMstMstiPortAdminPathCost)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstMstiPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist for this Instance!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_PORT_INST_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsMstMstiPortAdminPathCost > AST_PORT_PATHCOST_100KBPS) ||
        (i4TestValFsMstMstiPortAdminPathCost < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                testValFsMstMstiPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstMstiPortPseudoRootId (UINT4 *pu4ErrorCode, INT4 i4FsMstMstiPort,
                                    INT4 i4FsMstInstanceIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsMstMstiPortPseudoRootId)
{
    UINT2               u2BrgPriority = 0;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        return SNMP_FAILURE;
    }

    if (i4FsMstInstanceIndex == MST_CIST_CONTEXT)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Instance Id is Zero !\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstMstiPort < AST_MIN_NUM_PORTS) ||
        (i4FsMstMstiPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsMstMstiPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (pTestValFsMstMstiPortPseudoRootId->i4_Length
        != AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u2BrgPriority = (UINT2)
        ((pTestValFsMstMstiPortPseudoRootId->pu1_OctetList[0] << 8) |
         (pTestValFsMstMstiPortPseudoRootId->pu1_OctetList[1]));

    if (u2BrgPriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_BRIDGE_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstMstiPortStateChangeTimeStamp
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                retValFsMstMstiPortLastStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMstMstiPortStateChangeTimeStamp
    (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
     UINT4 *pu4RetValFsMstMstiPortStateChangeTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMstMstiPortStateChangeTimeStamp = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMstMstiPortStateChangeTimeStamp =
        pAstPerStPortInfo->u4PortStateChangeTimeStamp;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstMstiPortTable
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstMstiPortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstPortExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstPortExtTable
 Input       :  The Indices
                FsMstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMstPortExtTable (INT4 i4FsMstPort)
{
    if ((i4FsMstPort < AST_MIN_NUM_PORTS) || (i4FsMstPort > AST_MAX_NUM_PORTS))
    {

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstPortExtTable
 Input       :  The Indices
                FsMstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMstPortExtTable (INT4 *pi4FsMstPort)
{
    return (nmhGetNextIndexFsMstPortExtTable (0, pi4FsMstPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstPortExtTable
 Input       :  The Indices
                FsMstPort
                nextFsMstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstPortExtTable (INT4 i4FsMstPort, INT4 *pi4NextFsMstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortIndex = 0;
    UINT1               u1Flag = MST_FALSE;

    if (i4FsMstPort < 0)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }
    u2PortIndex = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortIndex, pAstPortEntry)
    {
        if (pAstPortEntry == NULL)
        {
            continue;
        }
        if (pAstPortEntry->u2PortNo > i4FsMstPort)
        {

            if (u1Flag == MST_FALSE)
            {

                *pi4NextFsMstPort = pAstPortEntry->u2PortNo;
                u1Flag = MST_TRUE;
            }
            else
            {

                if (*pi4NextFsMstPort > pAstPortEntry->u2PortNo)
                {
                    *pi4NextFsMstPort = pAstPortEntry->u2PortNo;
                }

            }
        }
    }

    if (u1Flag == MST_FALSE)
    {

        return SNMP_FAILURE;
    }
    else
    {

        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstPortRowStatus
 Input       :  The Indices
                FsMstPort

                The Object 
                retValFsMstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPortRowStatus (INT4 i4FsMstPort, INT4 *pi4RetValFsMstPortRowStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstPortRowStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }
    *pi4RetValFsMstPortRowStatus = pAstPortEntry->i4PortRowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstPortRowStatus
 Input       :  The Indices
                FsMstPort

                The Object 
                setValFsMstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstPortRowStatus (INT4 i4FsMstPort, INT4 i4SetValFsMstPortRowStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1OperStatus = AST_INIT_VAL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, i4FsMstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fetching the external port \n");
        return SNMP_FAILURE;

    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->u4PortNo = u4IfIndex;
    pAstMsgNode->u4ContextId = u4ContextId;
    pAstMsgNode->uMsg.u2LocalPortId = i4FsMstPort;
    pPortEntry = AST_GET_PORTENTRY (i4FsMstPort);
    if ((i4SetValFsMstPortRowStatus != CREATE_AND_WAIT)
        && (i4SetValFsMstPortRowStatus != CREATE_AND_GO)
        && (pPortEntry == NULL))
    {
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }
        return SNMP_FAILURE;
    }
#ifdef L2RED_WANTED
    if ((pPortEntry != NULL)
        && ((i4SetValFsMstPortRowStatus == CREATE_AND_WAIT)
            || (i4SetValFsMstPortRowStatus == CREATE_AND_GO))
        && (AstRmGetNodeState () == RM_STANDBY))
    {

        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }

        return SNMP_SUCCESS;

    }
#endif

    switch (i4SetValFsMstPortRowStatus)
    {
        case ACTIVE:
            AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
            break;
        case NOT_IN_SERVICE:
            AST_PORT_ROW_STATUS (pPortEntry) = NOT_IN_SERVICE;
            break;
        case NOT_READY:
            AST_PORT_ROW_STATUS (pPortEntry) = NOT_READY;
            break;
        case CREATE_AND_WAIT:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
            if (AstHandleCreatePort (pAstMsgNode) != MST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            pPortEntry = AST_GET_PORTENTRY (i4FsMstPort);

            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = NOT_READY;
            }
            break;
        case CREATE_AND_GO:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
            if (AstHandleCreatePort (pAstMsgNode) != MST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            pPortEntry = AST_GET_PORTENTRY (i4FsMstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
            }
            break;
        case DESTROY:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_MSG;
            pAstMsgNode->u4PortNo = i4FsMstPort;
            if (AstHandleDeletePort (pAstMsgNode) != MST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            break;

        default:
            if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
            {
                AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                         "MGMT: Release of Local Message Memory Block FAILED!\n");
            }
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Invalid Row status value for MstPortRowStatus!\n");

            return SNMP_FAILURE;
    }

    if (AstCfaGetIfOperStatus (u4IfIndex, &u1OperStatus) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfOperStatus for Port %u\n", u4IfIndex);
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Message Memory Block FAILED!\n");
        }
        return SNMP_FAILURE;

    }
    pPortEntry = AST_GET_PORTENTRY (i4FsMstPort);
    if (pPortEntry != NULL)
    {
        if (u1OperStatus == AST_UP)
        {
            if (pPortEntry->i4PortRowStatus == ACTIVE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_UP);
            }
            else if (pPortEntry->i4PortRowStatus == NOT_IN_SERVICE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_DOWN);
            }
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    u4ContextId = AST_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstPortRowStatus,
                          u4SeqNum, TRUE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex,
                      i4SetValFsMstPortRowStatus));
    AstSelectContext (u4ContextId);

    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Message Memory Block FAILED!\n");
        return MST_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMstPortRowStatus
 Input       :  The Indices
                FsMstPort

                The Object 
                testValFsMstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMstPort,
                             INT4 i4TestValFsMstPortRowStatus)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;

    if (AstIsExtInterface (i4FsMstPort) == AST_FALSE)
    {
        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstPortRowStatus < ACTIVE) ||
        (i4TestValFsMstPortRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMstPort < AST_MIN_NUM_PORTS || i4FsMstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, i4FsMstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fetching the external port \n");
        return SNMP_FAILURE;

    }

    if (AstL2IwfIsPortInPortChannel (u4IfIndex) == RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (IssGetAutoPortCreateFlag () != ISS_ENABLE)
    {
        /* To avoid deletion of port which is already deleted/not created , 
           when AutoPortCreate flag is disabled */
        if ((i4TestValFsMstPortRowStatus == DESTROY)
            && (AST_GET_PORTENTRY (i4FsMstPort) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        /* To avoid addition of port which is already created , when AutoPortCreate flag is disabled */
        if (((i4TestValFsMstPortRowStatus == CREATE_AND_GO)
             || (i4TestValFsMstPortRowStatus == CREATE_AND_WAIT))
            && (AST_GET_PORTENTRY (i4FsMstPort) != NULL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstPortExtTable
 Input       :  The Indices
                FsMstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstPortExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstSetTraps
 Input       :  The Indices

                The Object 
                retValFsMstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstSetTraps (INT4 *pi4RetValFsMstSetTraps)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstSetTraps = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstSetTraps = AST_TRAP_TYPE;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstGenTrapType
 Input       :  The Indices

                The Object 
                retValFsMstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstGenTrapType (INT4 *pi4RetValFsMstGenTrapType)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstGenTrapType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstGenTrapType = AST_GEN_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstErrTrapType
 Input       :  The Indices

                The Object 
                retValFsMstErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstErrTrapType (INT4 *pi4RetValFsMstErrTrapType)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstErrTrapType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMstErrTrapType = AST_ERR_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMstSetTraps
 Input       :  The Indices

                The Object 
                setValFsMstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstSetTraps (INT4 i4SetValFsMstSetTraps)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_TRAP_TYPE = (UINT4) i4SetValFsMstSetTraps;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSetTraps, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstSetTraps));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMstSetTraps
 Input       :  The Indices

                The Object 
                testValFsMstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstSetTraps (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstSetTraps)
{
    if ((i4TestValFsMstSetTraps < 0) || (i4TestValFsMstSetTraps > AST_TRAP_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMstSetTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstSetTraps (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstPortTrapNotificationTable
 Input       :  The Indices
                FsMstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMstPortTrapNotificationTable (INT4
                                                        i4FsMstPortTrapIndex)
{
    if (AstSnmpLowValidatePortIndex (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstPortTrapNotificationTable
 Input       :  The Indices
                FsMstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMstPortTrapNotificationTable (INT4 *pi4FsMstPortTrapIndex)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstPortTrapNotificationTable
 Input       :  The Indices
                FsMstPortTrapIndex
                nextFsMstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsMstPortTrapNotificationTable (INT4 i4FsMstPortTrapIndex,
                                               INT4 *pi4NextFsMstPortTrapIndex)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (i4FsMstPortTrapIndex < 0)
    {
        return (INT1) SNMP_FAILURE;
    }
    if (AstSnmpLowGetNextValidIndex
        (i4FsMstPortTrapIndex, pi4NextFsMstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMstPortMigrationType
 Input       :  The Indices
                FsMstPortTrapIndex

                The Object 
                retValFsMstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPortMigrationType (INT4 i4FsMstPortTrapIndex,
                              INT4 *pi4RetValFsMstPortMigrationType)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstPortMigrationType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsMstPortMigrationType =
        (AST_GET_PORTENTRY (i4FsMstPortTrapIndex))->u1MigrationType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstPktErrType
 Input       :  The Indices
                FsMstPortTrapIndex

                The Object 
                retValFsMstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPktErrType (INT4 i4FsMstPortTrapIndex,
                       INT4 *pi4RetValFsMstPktErrType)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstPktErrType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsMstPktErrType =
        (INT4) (AST_GET_PORTENTRY (i4FsMstPortTrapIndex))->u1PktErrType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstPktErrVal
 Input       :  The Indices
                FsMstPortTrapIndex

                The Object 
                retValFsMstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPktErrVal (INT4 i4FsMstPortTrapIndex, INT4 *pi4RetValFsMstPktErrVal)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstPktErrVal = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsMstPktErrVal =
        (INT4) (AST_GET_PORTENTRY (i4FsMstPortTrapIndex))->u2PktErrValue;
    return (INT1) SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMstPortRoleTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMstPortTrapIndex
                FsMstMstiInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMstPortRoleTrapNotificationTable (INT4
                                                            i4FsMstPortTrapIndex,
                                                            INT4
                                                            i4FsMstMstiInstanceIndex)
{
    /* This table holds the Port role information necessary to *
     * generate trap during role changes. Here, we directly   *
     * call the corresponding Validate, getfirst and Getnext   *
     * routine for another table, having the same Indices.     */

    return (nmhValidateIndexInstanceFsMstMstiPortTable
            (i4FsMstPortTrapIndex, i4FsMstMstiInstanceIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMstPortRoleTrapNotificationTable
 Input       :  The Indices
        FsMstPortTrapIndex
        FsMstMstiInstanceIndex

 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMstPortRoleTrapNotificationTable (INT4 *pi4FsMstPortTrapIndex,
                                                    INT4
                                                    *pi4FsMstMstiInstanceIndex)
{
    UINT1               i1RetVal;

    i1RetVal = nmhGetFirstIndexFsMstMstiPortTable (pi4FsMstPortTrapIndex,
                                                   pi4FsMstMstiInstanceIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMstPortTrapIndex
                nextFsMstPortTrapIndex
                FsMstMstiInstanceIndex
                nextFsMstMstiInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMstPortRoleTrapNotificationTable (INT4 i4FsMstPortTrapIndex,
                                                   INT4
                                                   *pi4NextFsMstPortTrapIndex,
                                                   INT4
                                                   i4FsMstMstiInstanceIndex,
                                                   INT4
                                                   *pi4NextFsMstMstiInstanceIndex)
{
    UINT1               i1RetVal;

    i1RetVal = nmhGetNextIndexFsMstMstiPortTable (i4FsMstPortTrapIndex,
                                                  pi4NextFsMstPortTrapIndex,
                                                  i4FsMstMstiInstanceIndex,
                                                  pi4NextFsMstMstiInstanceIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMstPortRoleType 
 Input       :  The Indices
                FsMstPortTrapIndex
        FsMstMstiInstanceIndex

                The Object 
                retValFsMstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstPortRoleType (INT4 i4FsMstPortTrapIndex,
                         INT4 i4FsMstMstiInstanceIndex,
                         INT4 *pi4RetValFsMstPortRole)
{
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValFsMstPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstMstiInstanceIndex < 0) ||
        (i4FsMstMstiInstanceIndex >= AST_MAX_MST_INSTANCES))
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMstPortRole =
        (INT4) (AST_GET_SELECTED_PORT_ROLE (i4FsMstMstiInstanceIndex,
                                            i4FsMstPortTrapIndex));
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstOldPortRoleType 
 Input       :  The Indices
                FsMstPortTrapIndex
        FsMstMstiInstanceIndex

                The Object 
                retValFsMstOldPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstOldPortRoleType (INT4 i4FsMstPortTrapIndex,
                            INT4 i4FsMstMstiInstanceIndex,
                            INT4 *pi4RetValFsMstOldPortRole)
{
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsMstOldPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsMstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsMstMstiInstanceIndex < 0) ||
        (i4FsMstMstiInstanceIndex >= AST_MAX_MST_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstOldPortRole =
        (INT4) (AST_GET_PORT_ROLE (i4FsMstMstiInstanceIndex,
                                   i4FsMstPortTrapIndex));
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MstValidateInstanceEntry
 Input       :  i4FsMstInstanceIndex
                                                         
 Output      :  None.                                           
 Returns     :  MST_SUCCESS or MST_FAILURE
****************************************************************************/
INT4
MstValidateInstanceEntry (INT4 i4FsMstInstanceIndex)
{
    tAstPerStInfo      *pAstPerStInfo = NULL;

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {

        return MST_FAILURE;
    }
    /* For Instnce TE_MSTID */
    if (i4FsMstInstanceIndex == AST_TE_MSTID)
    {
        return MST_SUCCESS;
    }
    pAstPerStInfo = AST_GET_PERST_INFO (i4FsMstInstanceIndex);

    if (pAstPerStInfo != NULL)
    {

        return MST_SUCCESS;
    }
    else
    {

        return MST_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                retValFsMstCistPortAutoEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstCistPortAutoEdgeStatus (INT4 i4FsMstCistPort,
                                   INT4 *pi4RetValFsMstCistPortAutoEdgeStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortAutoEdgeStatus = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bAutoEdge == MST_TRUE)
    {
        *pi4RetValFsMstCistPortAutoEdgeStatus = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMstCistPortAutoEdgeStatus = AST_SNMP_FALSE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                testValFsMstCistPortAutoEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstCistPortAutoEdgeStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMstCistPort,
                                      INT4 i4TestValFsMstCistPortAutoEdgeStatus)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_PORT_SISP_LOGICAL ((UINT2) i4FsMstCistPort) == MST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_SISP_INVALID_PROP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstCistPortAutoEdgeStatus != AST_SNMP_TRUE) &&
        (i4TestValFsMstCistPortAutoEdgeStatus != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortAutoEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortAutoEdgeStatus (INT4 i4FsMstCistPort,
                                   INT4 i4SetValFsMstCistPortAutoEdgeStatus)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_AUTO_EDGEPORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    if (i4SetValFsMstCistPortAutoEdgeStatus == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bAutoEdgePort = MST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bAutoEdgePort = MST_FALSE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortAutoEdgeStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortAutoEdgeStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortPathCost
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortPathCost (INT4 i4FsMstCistPort,
                             INT4 i4SetValFsMstCistPortPathCost)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PATH_COST_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    pAstMsgNode->uMsg.u4PathCost = i4SetValFsMstCistPortPathCost;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortPriority
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortPriority (INT4 i4FsMstCistPort,
                             INT4 i4SetValFsMstCistPortPriority)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PRIORITY_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    pAstMsgNode->uMsg.u1PortPriority = (UINT1) i4SetValFsMstCistPortPriority;
    pAstMsgNode->u2InstanceId = (UINT2) MST_CIST_CONTEXT;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortPriority, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortPriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortAdminP2P
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortAdminP2P
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortAdminP2P (INT4 i4FsMstCistPort,
                             INT4 i4SetValFsMstCistPortAdminP2P)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ADMIN_PTOP_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    pAstMsgNode->uMsg.u1AdminPToP = (UINT1) i4SetValFsMstCistPortAdminP2P;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortAdminP2P, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortAdminP2P));
    AstSelectContext (u4CurrContextId);

    if (AstSetCistPortMacEnabled ((UINT2) i4FsMstCistPort,
                                  i4SetValFsMstCistPortAdminP2P)
        == SNMP_FAILURE)
    {
        AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: AstSetCistPortMacEnabled function returned FAILURE!\n");
        i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortAdminEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortAdminEdgeStatus (INT4 i4FsMstCistPort,
                                    INT4 i4SetValFsMstCistPortAdminEdgeStatus)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ADMIN_EDGEPORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    if (i4SetValFsMstCistPortAdminEdgeStatus == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bAdminEdgePort = MST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bAdminEdgePort = MST_FALSE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortAdminEdgeStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortAdminEdgeStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistForcePortState
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistForcePortState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistForcePortState (INT4 i4FsMstCistPort,
                               INT4 i4SetValFsMstCistForcePortState)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    /* Modified for Attachment Circuit interface */
    if ((AstIsExtInterface (i4FsMstCistPort) == AST_TRUE) &&
        (i4SetValFsMstCistForcePortState == MST_FORCE_STATE_ENABLED))
    {
        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4FsMstCistPort);
        if ((pAstPortEntry == NULL) &&
            (AstCreateExtInterface (i4FsMstCistPort) == RST_FAILURE))
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstCistForcePortState == MST_FORCE_STATE_ENABLED)
    {

        pAstMsgNode->MsgType = AST_ENABLE_PORT_MSG;
        pAstMsgNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_UP;
    }
    else
    {

        pAstMsgNode->MsgType = AST_DISABLE_PORT_MSG;
        pAstMsgNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_DOWN;
    }

    pAstMsgNode->u2InstanceId = (UINT2) MST_CIST_CONTEXT;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistForcePortState,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistForcePortState));
    AstSelectContext (u4CurrContextId);

    if ((AstIsExtInterface (i4FsMstCistPort) == AST_TRUE) &&
        (i4SetValFsMstCistForcePortState == MST_FORCE_STATE_DISABLED))
    {
        if (AstDeleteExtInterface (i4FsMstCistPort) == RST_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstCistPortHelloTime
 Input       :  The Indices
                FsMstCistPort

                The Object 
                setValFsMstCistPortHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstCistPortHelloTime (INT4 i4FsMstCistPort,
                              INT4 i4SetValFsMstCistPortHelloTime)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_PORT_HELLO_TIME_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstCistPort;
    pAstMsgNode->uMsg.u2HelloTime =
        (UINT2) AST_MGMT_TO_SYS (i4SetValFsMstCistPortHelloTime);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistPortHelloTime,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsMstCistPort),
                      i4SetValFsMstCistPortHelloTime));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiPortPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                setValFsMstMstiPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiPortPathCost (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                             INT4 i4SetValFsMstMstiPortPathCost)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Inst DOES NOT exist \n");

        return SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PATH_COST_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;
    pAstMsgNode->uMsg.u4PathCost = (UINT4) i4SetValFsMstMstiPortPathCost;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      AST_GET_IFINDEX (i4FsMstMstiPort), i4FsMstInstanceIndex,
                      i4SetValFsMstMstiPortPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiPortPriority
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                setValFsMstMstiPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiPortPriority (INT4 i4FsMstMstiPort, INT4 i4FsMstInstanceIndex,
                             INT4 i4SetValFsMstMstiPortPriority)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Inst DOES NOT exist \n");

        return SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PRIORITY_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;
    pAstMsgNode->uMsg.u1PortPriority = (UINT1) i4SetValFsMstMstiPortPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiPortPriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      AST_GET_IFINDEX (i4FsMstMstiPort), i4FsMstInstanceIndex,
                      i4SetValFsMstMstiPortPriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                setValFsMstMstiPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiPortPseudoRootId (INT4 i4FsMstMstiPort,
                                 INT4 i4FsMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsMstMstiPortPseudoRootId)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeId        PseudoRootId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (!(AST_IS_MST_STARTED ()))
    {
        return SNMP_FAILURE;
    }

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    AST_MEMSET (PseudoRootId.BridgeAddr, 0, AST_MAC_ADDR_SIZE);

    PseudoRootId.u2BrgPriority = 0;

    PseudoRootId.u2BrgPriority =
        (UINT2) (((pSetValFsMstMstiPortPseudoRootId->pu1_OctetList[0] << 8) |
                  pSetValFsMstMstiPortPseudoRootId->pu1_OctetList[1]));

    AST_MEMCPY (PseudoRootId.BridgeAddr,
                pSetValFsMstMstiPortPseudoRootId->pu1_OctetList +
                AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE);

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Inst DOES NOT exist \n");

        return SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsMstMstiPort,
                                              i4FsMstInstanceIndex);

    if (pPerStPortInfo == NULL)
    {
        /* Port may not be part of instance */
        return MST_SUCCESS;
    }
    if (AST_COMPARE_BRGID (&pPerStPortInfo->PseudoRootId, &PseudoRootId)
        == RST_BRGID1_SAME)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PSEUDO_ROOTID_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;

    pAstMsgNode->uMsg.BridgeId.u2BrgPriority = PseudoRootId.u2BrgPriority;

    AST_MEMCPY (pAstMsgNode->uMsg.BridgeId.BridgeAddr, PseudoRootId.BridgeAddr,
                AST_MAC_ADDR_SIZE);

    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiPortPseudoRootId,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s",
                      AST_GET_IFINDEX (i4FsMstMstiPort), i4FsMstInstanceIndex,
                      pSetValFsMstMstiPortPseudoRootId));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiForcePortState
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex

                The Object 
                setValFsMstMstiForcePortState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstMstiForcePortState (INT4 i4FsMstMstiPort,
                               INT4 i4FsMstInstanceIndex,
                               INT4 i4SetValFsMstMstiForcePortState)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Inst DOES NOT exist \n");

        return SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstMstiForcePortState == MST_FORCE_STATE_ENABLED)
    {

        pAstMsgNode->MsgType = AST_ENABLE_PORT_MSG;
        pAstMsgNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_UP;
    }
    else
    {

        pAstMsgNode->MsgType = AST_DISABLE_PORT_MSG;
        pAstMsgNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_DOWN;
    }

    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiForcePortState,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      AST_GET_IFINDEX (i4FsMstMstiPort), i4FsMstInstanceIndex,
                      i4SetValFsMstMstiForcePortState));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMstMstiPort
                FsMstInstanceIndex
 
                The Object 
                setValFsMstMstiPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMstMstiPortAdminPathCost (INT4 i4FsMstMstiPort,
                                  INT4 i4FsMstInstanceIndex,
                                  INT4 i4SetValFsMstMstiPortAdminPathCost)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Inst DOES NOT exist \n");

        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (MstValidateInstanceEntry (i4FsMstInstanceIndex) != MST_SUCCESS)
    {
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (0 == i4SetValFsMstMstiPortAdminPathCost)
    {
        pAstMsgNode->MsgType = AST_ZERO_PATHCOST_MSG;
        pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
        pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;
    }
    else
    {
        pAstMsgNode->MsgType = AST_PATH_COST_MSG;
        pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
        pAstMsgNode->u4PortNo = (UINT4) i4FsMstMstiPort;
        pAstMsgNode->uMsg.u4PathCost =
            (UINT4) i4SetValFsMstMstiPortAdminPathCost;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                      AST_GET_IFINDEX (i4FsMstMstiPort), i4FsMstInstanceIndex,
                      i4SetValFsMstMstiPortAdminPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  MstpNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
MstpNotifyProtocolShutdownStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsmpms, (sizeof (fsmpms) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsMIMstSystemControl,
                      (sizeof (FsMIMstSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the Mstp shutdown, with 
     * Mstp oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, i4ContextId);
#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}

/****************************************************************************
 Function    :  nmhGetFsMstBpduGuard
 Input       :  The Indices

                The Object
                retValFsMstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstBpduGuard (INT4 *pi4RetValFsMstBpduGuard)
{
    if (AST_IS_MST_STARTED ())
    {
        *pi4RetValFsMstBpduGuard = (INT4) AST_GBL_BPDUGUARD_STATUS;
    }
    else
    {
        *pi4RetValFsMstBpduGuard = AST_INIT_VAL;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMstBpduGuard
 Input       :  The Indices

                The Object
                setValFsMstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstBpduGuard (INT4 i4SetValFsMstBpduGuard)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsMstBpduGuard == AST_BPDUGUARD_ENABLE)
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_ENABLE_MSG;
    }
    else
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_DISABLE_MSG;
    }

    pNode->uMsg.u4BpduGuardStatus = i4SetValFsMstBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstBpduGuard));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstBpduGuard
 Input       :  The Indices

                The Object
                testValFsMstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstBpduGuard (UINT4 *pu4ErrorCode, INT4 i4TestValFsMstBpduGuard)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: MST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMstBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsMstBpduGuard != AST_BPDUGUARD_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMstBpduGuard
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstBpduGuard (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstStpPerfStatus
 Input       :  The Indices
 
                The Object
                retValFsMstStpPerfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMstStpPerfStatus (INT4 *pi4RetValFsMstStpPerfStatus)
{
    UINT4               u4ContextId = 0;
    BOOL1               bPerfDataFlag;

    if (AstL2IwfGetPerformanceDataStatus (u4ContextId, &bPerfDataFlag) ==
        L2IWF_FAILURE)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (bPerfDataFlag == (BOOL1) AST_PERFORMANCE_STATUS_ENABLE)
    {
        *pi4RetValFsMstStpPerfStatus = (INT4) AST_PERFORMANCE_STATUS_ENABLE;
    }
    else
    {
        *pi4RetValFsMstStpPerfStatus = (INT4) AST_PERFORMANCE_STATUS_DISABLE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMstStpPerfStatus
 Input       :  The Indices
 
                The Object
                setValFsMstStpPerfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsMstStpPerfStatus (INT4 i4SetValFsMstStpPerfStatus)
{

    UINT4               u4ContextId = 0;

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstL2IwfSetPerformanceDataStatus
        (u4ContextId, (BOOL1) (i4SetValFsMstStpPerfStatus)) == L2IWF_FAILURE)
    {
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMstStpPerfStatus
 Input       :  The Indices
                The Object
                testValFsMstStpPerfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                                                         SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)                                                                   SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                                                  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/

INT1
nmhTestv2FsMstStpPerfStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMstStpPerfStatus)
{
    if ((i4TestValFsMstStpPerfStatus == AST_PERFORMANCE_STATUS_ENABLE)
        || (i4TestValFsMstStpPerfStatus == AST_PERFORMANCE_STATUS_DISABLE))
    {
        return (INT1) SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhDepv2FsMstStpPerfStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsMstStpPerfStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMstInstPortsMap
 Input       :  The Indices

                The Object
                retValFsMstInstPortsMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMstInstPortsMap (INT4 *pi4RetValFsMstInstPortsMap)
{
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstInstPortsMap = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsMstInstPortsMap = (INT4) AST_INST_PORT_MAP;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMstInstPortsMap
 Input       :  The Indices

                The Object
                setValFsMstInstPortsMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMstInstPortsMap (INT4 i4SetValFsMstInstPortsMap)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_INST_PORT_MAP = (UINT1) i4SetValFsMstInstPortsMap;

    AST_DBG (AST_MGMT_DBG, "MSTP: Instance Port Mapping ..\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstInstPortsMap, u4SeqNum, FALSE,
                          AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMstInstPortsMap));
    AstSelectContext (u4CurrContextId);
    return ((INT1) SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsMstInstPortsMap
 Input       :  The Indices

                The Object
                testValFsMstInstPortsMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMstInstPortsMap (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsMstInstPortsMap)
{

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: MSTP Module not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValFsMstInstPortsMap == MST_INST_MAP_ENABLED)
        || (i4TestValFsMstInstPortsMap == MST_INST_MAP_DISABLED))
    {
        return (INT1) SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhDepv2FsMstInstPortsMap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMstInstPortsMap (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortTCDetectedCount
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortTCDetectedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *                  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *                  ****************************************************************************/
INT1
nmhGetFsMstCistPortTCDetectedCount (INT4 i4FsMstCistPort,
                                    UINT4
                                    *pu4RetValFsMstCistPortTCDetectedCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortTCDetectedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortTCDetectedCount =
        pAstPerStPortInfo->u4TcDetectedCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortTCReceivedCount
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortTCReceivedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortTCReceivedCount (INT4 i4FsMstCistPort,
                                    UINT4
                                    *pu4RetValFsMstCistPortTCReceivedCount)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortTCReceivedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortTCReceivedCount = pAstPerStPortInfo->u4TcRcvdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortTCDetectedTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortTCDetectedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortTCDetectedTimeStamp (INT4 i4FsMstCistPort,
                                        UINT4
                                        *pu4RetValFsMstCistPortTCDetectedTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortTCDetectedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortTCDetectedTimeStamp =
        pAstPerStPortInfo->u4TcDetectedTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortTCReceivedTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortTCReceivedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortTCReceivedTimeStamp (INT4 i4FsMstCistPort,
                                        UINT4
                                        *pu4RetValFsMstCistPortTCReceivedTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortTCReceivedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortTCReceivedTimeStamp =
        pAstPerStPortInfo->u4TcRcvdTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortRcvInfoWhileExpCount
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortRcvInfoWhileExpCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortRcvInfoWhileExpCount (INT4 i4FsMstCistPort,
                                         UINT4
                                         *pu4RetValFsMstCistPortRcvInfoWhileExpCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortRcvInfoWhileExpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortRcvInfoWhileExpCount =
        pAstPortEntry->u4NumRstRxdInfoWhileExpCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortRcvInfoWhileExpTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortRcvInfoWhileExpTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortRcvInfoWhileExpTimeStamp (INT4 i4FsMstCistPort,
                                             UINT4
                                             *pu4RetValFsMstCistPortRcvInfoWhileExpTimeStamp)
{

    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortRcvInfoWhileExpTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortRcvInfoWhileExpTimeStamp =
        pAstPortEntry->u4RcvInfoWhileExpTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortProposalPktsSent
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortProposalPktsSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortProposalPktsSent (INT4 i4FsMstCistPort,
                                     UINT4
                                     *pu4RetValFsMstCistPortProposalPktsSent)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortProposalPktsSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortProposalPktsSent =
        pAstPerStPortInfo->u4ProposalTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortProposalPktsRcvd
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortProposalPktsRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortProposalPktsRcvd (INT4 i4FsMstCistPort,
                                     UINT4
                                     *pu4RetValFsMstCistPortProposalPktsRcvd)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortProposalPktsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortProposalPktsRcvd =
        pAstPerStPortInfo->u4ProposalRcvdCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortProposalPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortProposalPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortProposalPktSentTimeStamp (INT4 i4FsMstCistPort,
                                             UINT4
                                             *pu4RetValFsMstCistPortProposalPktSentTimeStamp)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortProposalPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortProposalPktSentTimeStamp =
        pAstPerStPortInfo->u4ProposalTxTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortProposalPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortProposalPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortProposalPktRcvdTimeStamp (INT4 i4FsMstCistPort,
                                             UINT4
                                             *pu4RetValFsMstCistPortProposalPktRcvdTimeStamp)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortProposalPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortProposalPktRcvdTimeStamp =
        pAstPerStPortInfo->u4ProposalRcvdTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortAgreementPktSent
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortAgreementPktSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortAgreementPktSent (INT4 i4FsMstCistPort,
                                     UINT4
                                     *pu4RetValFsMstCistPortAgreementPktSent)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortAgreementPktSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortAgreementPktSent =
        pAstPerStPortInfo->u4AgreementTxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortAgreementPktRcvd
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortAgreementPktRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortAgreementPktRcvd (INT4 i4FsMstCistPort,
                                     UINT4
                                     *pu4RetValFsMstCistPortAgreementPktRcvd)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortAgreementPktRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortAgreementPktRcvd =
        pAstPerStPortInfo->u4AgreementRcvdCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortAgreementPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortAgreementPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortAgreementPktSentTimeStamp (INT4 i4FsMstCistPort,
                                              UINT4
                                              *pu4RetValFsMstCistPortAgreementPktSentTimeStamp)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortAgreementPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortAgreementPktSentTimeStamp =
        pAstPerStPortInfo->u4AgreementTxTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortAgreementPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortAgreementPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortAgreementPktRcvdTimeStamp (INT4 i4FsMstCistPort,
                                              UINT4
                                              *pu4RetValFsMstCistPortAgreementPktRcvdTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortAgreementPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstCistPort, MST_CIST_CONTEXT);

    if (pAstPerStPortInfo == NULL)
    {

        return SNMP_FAILURE;
    }
    *pu4RetValFsMstCistPortAgreementPktRcvdTimeStamp =
        pAstPerStPortInfo->u4AgreementRcvdTimeStamp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortImpStateOccurCount
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortImpStateOccurCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortImpStateOccurCount (INT4 i4FsMstCistPort,
                                       UINT4
                                       *pu4RetValFsMstCistPortImpStateOccurCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortImpStateOccurCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortImpStateOccurCount =
        pAstPortEntry->u4NumRstImpossibleStateOcc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortImpStateOccurTimeStamp
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortImpStateOccurTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortImpStateOccurTimeStamp (INT4 i4FsMstCistPort,
                                           UINT4
                                           *pu4RetValFsMstCistPortImpStateOccurTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMstCistPortImpStateOccurTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pu4RetValFsMstCistPortImpStateOccurTimeStamp =
        pAstPortEntry->u4ImpStateOccurTimeStamp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortOldPortState
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortOldPortState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortOldPortState (INT4 i4FsMstCistPort,
                                 INT4 *pi4RetValFsMstCistPortOldPortState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortOldPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    *pi4RetValFsMstCistPortOldPortState = pAstPortEntry->i4OldPortState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstCistPortLoopInconsistentState
 * * Input       :  The Indices
 * *                FsMstCistPort
 * *
 * *                The Object
 * *                retValFsMstCistPortLoopInconsistentState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstCistPortLoopInconsistentState (INT4 i4FsMstCistPort,
                                          INT4
                                          *pi4RetValFsMstCistPortLoopInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstCistPortLoopInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsMstCistPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsMstCistPort);

    if (pAstPortEntry == NULL)
    {

        return SNMP_FAILURE;
    }
    *pi4RetValFsMstCistPortLoopInconsistentState =
        pAstPortEntry->bLoopInconsistent;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortLoopInconsistentState
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortLoopInconsistentState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortLoopInconsistentState (INT4 i4FsMstMstiPort,
                                          INT4 i4FsMstInstanceIndex,
                                          INT4
                                          *pi4RetValFsMstMstiPortLoopInconsistentState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiPortLoopInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortLoopInconsistentState =
        pAstPerStPortInfo->bLoopIncStatus;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * *Function    :  nmhGetFsMstMstiPortTCDetectedCount
 * *Input       :  The Indices
 * *               FsMIMstMstiPort
 * *               FsMstInstanceIndex
 * *
 * *               The Object
 * *               retValFsMstMstiPortTCDetectedCount
 * *Output      :  The Get Low Lev Routine Take the Indices &
 * *               store the Value requested in the Return val.
 * *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortTCDetectedCount (INT4 i4FsMstMstiPort,
                                    INT4 i4FsMstInstanceIndex,
                                    UINT4
                                    *pu4RetValFsMIMstMstiPortTCDetectedCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortTCDetectedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortTCDetectedCount =
        pAstPerStPortInfo->u4TcDetectedCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortTCReceivedCount
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortTCReceivedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortTCReceivedCount (INT4 i4FsMstMstiPort,
                                    INT4 i4FsMstInstanceIndex,
                                    UINT4
                                    *pu4RetValFsMIMstMstiPortTCReceivedCount)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortTCReceivedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortTCReceivedCount = pAstPerStPortInfo->u4TcRcvdCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortTCDetectedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortTCDetectedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortTCDetectedTimeStamp (INT4 i4FsMstMstiPort,
                                        INT4 i4FsMstInstanceIndex,
                                        UINT4
                                        *pu4RetValFsMIMstMstiPortTCDetectedTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortTCDetectedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortTCDetectedTimeStamp =
        pAstPerStPortInfo->u4TcDetectedTimeStamp;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortTCReceivedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortTCReceivedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortTCReceivedTimeStamp (INT4 i4FsMstMstiPort,
                                        INT4 i4FsMstInstanceIndex,
                                        UINT4
                                        *pu4RetValFsMIMstMstiPortTCReceivedTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortTCReceivedTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortTCReceivedTimeStamp =
        pAstPerStPortInfo->u4TcRcvdTimeStamp;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortProposalPktsSent
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortProposalPktsSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortProposalPktsSent (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMIMstMstiPortProposalPktsSent)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortProposalPktsSent = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortProposalPktsSent =
        pAstPerStPortInfo->u4ProposalTxCount;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortProposalPktsRcvd
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortProposalPktsRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortProposalPktsRcvd (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMIMstMstiPortProposalPktsRcvd)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortProposalPktsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortProposalPktsRcvd =
        pAstPerStPortInfo->u4ProposalRcvdCount;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortProposalPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortProposalPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortProposalPktSentTimeStamp (INT4 i4FsMstMstiPort,
                                             INT4 i4FsMstInstanceIndex,
                                             UINT4
                                             *pu4RetValFsMIMstMstiPortProposalPktSentTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortProposalPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortProposalPktSentTimeStamp =
        pAstPerStPortInfo->u4ProposalTxTimeStamp;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortProposalPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortProposalPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortProposalPktRcvdTimeStamp (INT4 i4FsMstMstiPort,
                                             INT4 i4FsMstInstanceIndex,
                                             UINT4
                                             *pu4RetValFsMIMstMstiPortProposalPktRcvdTimeStamp)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortProposalPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortProposalPktRcvdTimeStamp =
        pAstPerStPortInfo->u4ProposalRcvdTimeStamp;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortAgreementPktSent
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortAgreementPktSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortAgreementPktSent (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMIMstMstiPortAgreementPktSent)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortAgreementPktSent = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortAgreementPktSent =
        pAstPerStPortInfo->u4AgreementTxCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortAgreementPktRcvd
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortAgreementPktRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortAgreementPktRcvd (INT4 i4FsMstMstiPort,
                                     INT4 i4FsMstInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMIMstMstiPortAgreementPktRcvd)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortAgreementPktRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortAgreementPktRcvd =
        pAstPerStPortInfo->u4AgreementRcvdCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortAgreementPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortAgreementPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortAgreementPktSentTimeStamp (INT4 i4FsMstMstiPort,
                                              INT4 i4FsMstInstanceIndex,
                                              UINT4
                                              *pu4RetValFsMIMstMstiPortAgreementPktSentTimeStamp)
{

    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortAgreementPktSentTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortAgreementPktSentTimeStamp =
        pAstPerStPortInfo->u4AgreementTxTimeStamp;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortAgreementPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortAgreementPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortAgreementPktRcvdTimeStamp (INT4 i4FsMstMstiPort,
                                              INT4 i4FsMstInstanceIndex,
                                              UINT4
                                              *pu4RetValFsMIMstMstiPortAgreementPktRcvdTimeStamp)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4RetValFsMIMstMstiPortAgreementPktRcvdTimeStamp = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIMstMstiPortAgreementPktRcvdTimeStamp =
        pAstPerStPortInfo->u4AgreementRcvdTimeStamp;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 * * Function    :  nmhGetFsMstMstiPortOldPortState
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMstMstiPortOldPortState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMstMstiPortOldPortState (INT4 i4FsMstMstiPort,
                                 INT4 i4FsMstInstanceIndex,
                                 INT4 *pi4RetValFsMstMstiPortOldPortState)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMstMstiPortOldPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;

    }

    if (AstValidatePortEntry (i4FsMstMstiPort) != MST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsMstMstiPort, i4FsMstInstanceIndex);

    if (pAstPerStPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMstMstiPortOldPortState = pAstPerStPortInfo->i4OldPortState;

    return (INT1) SNMP_SUCCESS;
}

#endif /* MSTP_WANTED */
