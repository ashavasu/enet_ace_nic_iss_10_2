/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmprsm.c,v 1.37 2017/11/21 13:06:50 siva Exp $
 *
 * Description: This file contains MST Port Receive SEM           
 *              related routines.
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstHandleInBpdu                            */
/*                                                                           */
/*    Description               : This function Handles the received BPDU    */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu - Pointer to the received BPDU   */
/*                                            message structure.             */
/*                                u2PortNum - Port Number of the Port on     */
/*                                            which BPDU is received.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstHandleInBpdu (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstHandleInBpdu (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstBpduType        AstBpduType;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2Duration = AST_INIT_VAL;
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT2               u2InstanceId = 1;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    /* When the BPDU is received on a port, the following actions would happen 
     * 1. if (globally enabled, per port enabled) - disable MSTP on the port
     * 2. else if (globally enabled, per port disabled) - disable MSTP if non-edge port or no action if edge-port
     * 3. else if(globally disabled, per port enabled) - disable MSTP on the port
     * 4. otherwise - do not take any action, because no expectation related to BPDU guard is set for this port
     * */

    if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_NONE)
    {
        if ((AST_GBL_BPDUGUARD_STATUS == AST_TRUE)
            && (pAstPortEntry->bAdminEdgePort == AST_TRUE))
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);
            AST_DBG_ARG1 (AST_BPDU_DBG,
                          "SYS: BPDU received on BpduGuard enabled "
                          "edge port ,Port Number  = %d ...\n", u2PortNum);
            if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
            {
                if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
                {
                    NotifyProtoToApp.STPNotify.u1AdminStatus =
                        AST_PORT_ADMIN_DOWN;
                    NotifyProtoToApp.STPNotify.u4IfIndex =
                        AST_GET_IFINDEX (u2PortNum);
                    STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                    pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "BPDU Inconsistent received  Port %s at %s \n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      au1TimeStr);
                    AstBPDUIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (u2PortNum),
                                               pAstCommPortInfo->
                                               bBpduInconsistent, u2InstanceId,
                                               (INT1 *) AST_MST_TRAPS_OID,
                                               AST_MST_TRAPS_OID_LEN);
                }
            }

            else
            {

                if (MstDisablePort
                    (u2PortNum, MST_CIST_CONTEXT,
                     AST_EXT_PORT_DOWN) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                                  "SYS: MstDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    AST_DBG_ARG1 (AST_BPDU_DBG,
                                  "SYS: MstDisablePort returned "
                                  "failure on admin edge port. Port Number = %d ...\n",
                                  u2PortNum);
                    return MST_FAILURE;
                }

                /* BpduInconsitent state is set when port transtions
                 * Happens due to Bpdu Guard Feature enabled on that
                 *  port*/
                if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
                {
                    pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "BPDU Inconsistent received  Port %s at %s \n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      au1TimeStr);
                    AstBPDUIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (u2PortNum),
                                               pAstCommPortInfo->
                                               bBpduInconsistent, u2InstanceId,
                                               (INT1 *) AST_MST_TRAPS_OID,
                                               AST_MST_TRAPS_OID_LEN);
                }
                u2Duration = (UINT2) pAstPortEntry->u4ErrorRecovery;
                if (AstStartTimer ((VOID *) pAstPortEntry, MST_CIST_CONTEXT,
                                   (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                                   u2Duration) != MST_SUCCESS)
                {
                    AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TCSM: AstStartTimer for disable recovery duration FAILED!\n");
                    return MST_FAILURE;
                }

                return MST_SUCCESS;
            }
        }
    }
    else if (pAstCommPortInfo->u4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        AST_DBG_ARG1 (AST_BPDU_DBG,
                      "SYS: BPDU received on BpduGuard enabled "
                      "port ,Port Number  = %d ...\n", u2PortNum);
        if (pAstCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN)
        {
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_DOWN;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u2PortNum);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "BPDU Inconsistent received  Port %s at %s \n",
                                  AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           u2InstanceId,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
            }
        }

        else
        {

            if (MstDisablePort (u2PortNum, MST_CIST_CONTEXT, AST_EXT_PORT_DOWN)
                != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                              "SYS: MstDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                AST_DBG_ARG1 (AST_BPDU_DBG,
                              "SYS: MstDisablePort returned "
                              "failure on Port Number = %d ...\n", u2PortNum);
                return MST_FAILURE;
            }

            /* BpduInconsitent state is set when port transtions
             * Happens due to Bpdu Guard Feature enabled on that
             *  port*/
            if (pAstCommPortInfo->bBpduInconsistent == AST_FALSE)
            {
                pAstCommPortInfo->bBpduInconsistent = AST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                  "BPDU Inconsistent received  Port %s at %s \n",
                                  AST_GET_IFINDEX_STR (u2PortNum), au1TimeStr);
                AstBPDUIncStateChangeTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                           pAstCommPortInfo->bBpduInconsistent,
                                           u2InstanceId,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
            }
            u2Duration = (UINT2) pAstPortEntry->u4ErrorRecovery;
            if (AstStartTimer ((VOID *) pAstPortEntry, MST_CIST_CONTEXT,
                               (UINT1) AST_TMR_TYPE_ERROR_DISABLE_RECOVERY,
                               u2Duration) != MST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: AstStartTimer for disable recovery duration FAILED!\n");
                return MST_FAILURE;
            }

            return MST_SUCCESS;
        }
    }
    pAstCommPortInfo->bRcvdBpdu = MST_TRUE;

    AST_MEMSET (&AstBpduType, AST_INIT_VAL, sizeof (tAstBpduType));

    AstBpduType.BpduType = AST_MST_MODE;

    AST_MEMCPY (&AstBpduType.uBpdu.MstBpdu, pRcvdBpdu, sizeof (tMstBpdu));

    if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_RCVD_BPDU,
                              u2PortNum, &AstBpduType) != MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RECV: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                      "RECV: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;

    }

    /* ForceVersion check mentioned in 802.1s as part of definition for 
     * rcvdBPDU (802.1w) is removed here as per 802.1D Draft4 
     * recommendation so that all BPDUs are accepted regardless of the 
     * bridge version */
    MstPortRcvdFlagsStatsUpdt (pRcvdBpdu, u2PortNum, RST_DEFAULT_INSTANCE);

    if (MstPortReceiveMachine ((UINT1) MST_PRCVSM_EV_RCVD_BPDU, pRcvdBpdu,
                               u2PortNum) != MST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RECV: Port %s: MstPortReceiveMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "RECV: Port %s: MstPortReceiveMachine function returned FAILURE for RCVD_BPDU event!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;
    }

    /* BPDU would have been processed in the physical port + primary context
     * for the corresponding instances. Now, process for the secondary 
     * contexts
     * */
    if (AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)
    {
        /* BPDU is received in SISP enabled port. SISP logic should
         * be applied during processing of this bpdu. Hence, invoke
         * the corresponding MSTP-SISP routine
         * */

        return (MstSispHandleBpduInSisp (pRcvdBpdu, u2PortNum));
    }

    return MST_SUCCESS;
}

VOID
MstPortRcvdFlagsStatsUpdt (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum,
                           UINT2 u2InstanceId)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1               u1TmpFlag = (UINT1) AST_INIT_VAL;
    UINT4               u4Ticks = 0;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo != NULL)
    {
        if (AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE) != NULL)
        {
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

            if (pPerStPortInfo != NULL)
            {
                /* Reception of TC,Proposal and agreements packet  */
                u1TmpFlag = MST_SET_PROPOSAL_FLAG;

                if ((pRcvdBpdu->u1CistFlags & u1TmpFlag) ==
                    MST_SET_PROPOSAL_FLAG)
                {
                    MST_INCR_RX_PROPOSAL_COUNT (u2PortNum,
                                                RST_DEFAULT_INSTANCE);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;
                }
                u1TmpFlag = MST_SET_AGREEMENT_FLAG;
                if ((pRcvdBpdu->u1CistFlags & u1TmpFlag) ==
                    MST_SET_AGREEMENT_FLAG)
                {
                    MST_INCR_RX_AGREEMENT_COUNT (u2PortNum,
                                                 RST_DEFAULT_INSTANCE);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;

                }
                u1TmpFlag = MST_SET_TOPOCH_FLAG;
                if ((pRcvdBpdu->u1CistFlags & u1TmpFlag) == MST_SET_TOPOCH_FLAG)
                {
                    MST_INCR_TC_RX_COUNT (u2PortNum, RST_DEFAULT_INSTANCE);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;

                }
            }
        }
    }
    for (u2InstanceId = 1; u2InstanceId < AST_MAX_MST_INSTANCES; u2InstanceId++)
    {

        if (AST_GET_PERST_INFO (u2InstanceId) != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);

            if (pPerStPortInfo != NULL)

            {
                u1TmpFlag = MST_SET_PROPOSAL_FLAG;
                if ((pRcvdBpdu->aMstConfigMsg[u2InstanceId].
                     u1MstiFlags & u1TmpFlag) == MST_SET_PROPOSAL_FLAG)
                {
                    MST_INCR_RX_PROPOSAL_COUNT (u2PortNum, u2InstanceId);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4ProposalRcvdTimeStamp = u4Ticks;

                }

                u1TmpFlag = MST_SET_AGREEMENT_FLAG;
                if ((pRcvdBpdu->aMstConfigMsg[u2InstanceId].
                     u1MstiFlags & u1TmpFlag) == MST_SET_AGREEMENT_FLAG)
                {
                    MST_INCR_RX_AGREEMENT_COUNT (u2PortNum, u2InstanceId);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4AgreementRcvdTimeStamp = u4Ticks;

                }
                u1TmpFlag = MST_SET_TOPOCH_FLAG;
                if ((pRcvdBpdu->aMstConfigMsg[u2InstanceId].
                     u1MstiFlags & u1TmpFlag) == MST_SET_TOPOCH_FLAG)
                {
                    MST_INCR_TC_RX_COUNT (u2PortNum, u2InstanceId);
                    AST_GET_SYS_TIME (&u4Ticks);
                    pPerStPortInfo->u4TcRcvdTimeStamp = u4Ticks;
                }
            }
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortReceiveMachine                      */
/*                                                                           */
/*    Description               : This function implements the Port Receive  */
/*                                State Machine. Called when a BPDU is       */
/*                                received on the port.                      */
/*                                                                           */
/*    Input(s)                  : u1Event        - Event occured related to  */
/*                                                 this Machine.             */
/*                                pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortReceiveMachine (UINT1 u1Event, tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortReceiveMachine (u1Event, pRcvdBpdu, u2PortNum)
     UINT1               u1Event;
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1               u1State = 0;
    INT4                i4RetVal = 0;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
    if (NULL == pRcvdBpdu)
    {
        AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Event %s: Port %u Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_PRSM][u1Event],
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Event %s: Port %s: Port does not exist in CIST\n",
                      gaaau1AstSemEvent[AST_PRSM][u1Event],
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    u1State = (AST_GET_COMM_PORT_INFO (u2PortNum))->u1PortRcvSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  " Port Receive Machine Called with Event: %s, State: %s for Port: %s\n",
                  gaaau1AstSemEvent[AST_PRSM][u1Event],
                  gaaau1AstSemState[AST_PRSM][u1State],
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_DBG_ARG3 (AST_PRSM_DBG,
                  " Port Receive Machine Called with Event: %s, State: %s for Port: %s\n",
                  gaaau1AstSemEvent[AST_PRSM][u1Event],
                  gaaau1AstSemState[AST_PRSM][u1State],
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (MST_PORT_RECEIVE_MACHINE[u1Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_PRSM_DBG,
                 "RECV: PortReceiveStateMachine No Action to Perform!\n");

        return MST_SUCCESS;
    }

    i4RetVal = (*(MST_PORT_RECEIVE_MACHINE[u1Event][u1State].pAction))
        (pRcvdBpdu, u2PortNum);

    if (i4RetVal != MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "RECV: PortReceiveStateMachine Action returned FAILURE!\n");
        AST_DBG (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                 "RECV: PortReceiveStateMachine Action returned FAILURE!\n");
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSmMakeDiscard                    */
/*                                                                           */
/*    Description               : This function changes the Port Receive     */
/*                                State Machine to DISCARD state.            */
/*                                                                           */
/*    Input(s)                    pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSmMakeDiscard (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSmMakeDiscard (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    UNUSED_PARAM (pRcvdBpdu);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pAstCommPortInfo->bRcvdStp = MST_FALSE;
    pAstCommPortInfo->bRcvdRstp = MST_FALSE;
    pAstCommPortInfo->bRcvdBpdu = MST_FALSE;

    MstPortRcvClearAllRcvdMsgs (u2PortNum);

    /* The earlier part of the code included a trigger to move to 
     * RECEIVE state directly by the condn check (portEnabled && !rcvdAnyMsg)
     * using the fn: MstPortRcvSmMakeReceive. But the reception of any bpdu or
     * portenabled event is present seperately and that should be called
     * during the respective event occurrences
     * */
    pAstCommPortInfo->u1PortRcvSmState = MST_PRCVSM_STATE_DISCARD;
    AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                  "PRSM: Port %s: Moved to state DISCARD\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvClearAllRcvdMsgs                 */
/*                                                                           */
/*    Description               : This function clears the RcvdMsg flags for */
/*                                CIST and all MSTIs for all ports.          */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
MstPortRcvClearAllRcvdMsgs (UINT2 u2PortNum)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    UINT2               u2MstIndex = AST_INIT_VAL;
    UINT2               u2MaxInst = AST_INIT_VAL;

    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    /* As per ClearAllRcvdMsgs of 802.1Q it is enough to clear the 
     * messages of all the instances for this port only
     * */

    u2MaxInst = pMstBridgeEntry->u2MaxMstInstances;
    AST_GET_NEXT_BUF_INSTANCE_MAX (u2MstIndex, u2MaxInst, pPerStInfo)
    {
        if ((AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        if (pPerStInfo == NULL)
        {

            continue;
        }
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstIndex);
        if (pPerStPortInfo == NULL)
        {

            continue;
        }

        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        pRstPortInfo->bRcvdMsg = MST_FALSE;
    }
    /* rcvdMsg has been set as false for all the Instances including
     * CIST. Hence making rcvdAnyMsg as FALSE
     * */
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pCistMstiPortInfo->bRcvdAnyMsg = MST_FALSE;
    AST_DBG_ARG1 (AST_PRSM_DBG, "PRSM: Port %s: RcvdAnyMsg = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSmMakeReceive                    */
/*                                                                           */
/*    Description               : This function changes the Port Receive     */
/*                                State Machine to RECEIVE state.            */
/*                                                                           */
/*    Input(s)                    pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port.  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSmMakeReceive (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSmMakeReceive (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstBoolean         bOldRcvdInternal = MST_TRUE;
    UINT2               u2Duration = AST_INIT_VAL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pAstCommPortInfo->u1PortRcvSmState = MST_PRCVSM_STATE_RECEIVE;

    /* Setting OperEdge in L2Iwf will happen in BDSM */
    pPortEntry->bOperEdgePort = RST_FALSE;
    pPortEntry->u2LastInst = 0;

    MstPortRcvUpdtBpduVersion (pRcvdBpdu, u2PortNum);
    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PRSM: Port %s: Moved to state RECEIVE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    u2Duration =
        (UINT2) (((AST_GET_BRGENTRY ())->u1MigrateTime) * AST_CENTI_SECONDS);
    /*Start the EdgeDelayWhileTimer */
    if (AstStartTimer ((VOID *) pPortEntry, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE, u2Duration) !=
        RST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        AST_DBG (AST_BDSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
        return RST_FAILURE;
    }
    /*Call the BDSM with RCVD bpdu event */
    RstBrgDetectionMachine (RST_BRGDETSM_EV_BPDU_RCVD, u2PortNum);

    bOldRcvdInternal = pCistMstiPortInfo->bRcvdInternal;

    /* Any BPDU received when Force Version is 0 or 2 should be considered as
     * being received from different region */
    if ((pRcvdBpdu->u1Version < (UINT1) AST_VERSION_3) ||
        (AST_FORCE_VERSION != (UINT1) AST_VERSION_3))
    {
        pCistMstiPortInfo->bRcvdInternal = MST_FALSE;
        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: ForceVersion or BPDU version not MSTP.\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: BPDU taken as received from Different MST Region!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    else
    {
        pCistMstiPortInfo->bRcvdInternal =
            MstPortRcvFromSameRegion (pRcvdBpdu, u2PortNum);
    }

    if (bOldRcvdInternal != pCistMstiPortInfo->bRcvdInternal)
    {
        AST_GET_INST_TRIGGER_FLAG = MST_TRUE;
    }

    if (MstPortRcvSetRcvdMsgs (pRcvdBpdu, u2PortNum) != MST_SUCCESS)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RECV: Port %s: MstPortRcvSetRcvdMsgs function returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;
    }

    pAstCommPortInfo->bRcvdBpdu = MST_FALSE;

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvUpdtBpduVersion                  */
/*                                                                           */
/*    Description               : This function updates the flags by the     */
/*                                type of BPDU received.                     */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstPortRcvUpdtBpduVersion (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
VOID
MstPortRcvUpdtBpduVersion (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PISM: Port %s: Updating BPDU version\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if ((pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_CONFIG) ||
        (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_TCN))
    {
        pAstCommPortInfo->bRcvdStp = MST_TRUE;
        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: Calling Port Migration Machine with RCVD_STP event\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_STP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_STP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_STP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return;
        }
    }
    /* As mentioned in Sec 14.4 of 802.1s, all bpdus with higher version to be
     * processed as if they carry the current supported version number - 3 */
    else if (pRcvdBpdu->u1BpduType == (UINT1) AST_BPDU_TYPE_RST)
    {
        pAstCommPortInfo->bRcvdRstp = MST_TRUE;

        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: Calling Port Migration Machine with RCVD_RSTP event\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_RCVD_RSTP, u2PortNum)
            != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "PRSM: Port %s: Migration Machine returned FAILURE for RCVD_RSTP event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return;
        }
    }
    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PRSM: Port %s: Successfully updated BPDU version\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvFromSameRegion                   */
/*                                                                           */
/*    Description               : This function checks whether BPDU is       */
/*                                received from same MST region              */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_TRUE - if the BPDU is received from    */
/*                                           same MST Region.                */
/*                                MST_FALSE- if the BPDU is not received     */
/*                                           from same MST Region.           */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
tAstBoolean
MstPortRcvFromSameRegion (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
tAstBoolean
MstPortRcvFromSameRegion (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();

    if ((pMstConfigIdInfo->u1ConfigurationId ==
         pRcvdBpdu->MstConfigIdInfo.u1ConfigurationId) &&
        (AST_MEMCMP (pMstConfigIdInfo->au1ConfigName,
                     pRcvdBpdu->MstConfigIdInfo.au1ConfigName,
                     MST_CONFIG_NAME_LEN) == 0)
        && (pMstConfigIdInfo->u2ConfigRevLevel ==
            pRcvdBpdu->MstConfigIdInfo.u2ConfigRevLevel)
        &&
        (AST_MEMCMP
         (pMstConfigIdInfo->au1ConfigDigest,
          pRcvdBpdu->MstConfigIdInfo.au1ConfigDigest,
          MST_CONFIG_DIGEST_LEN) == 0))
    {
        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: BPDU is received from Same MST Region!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_TRUE;
    }

    AST_DBG_ARG1 (AST_PRSM_DBG,
                  "PRSM: Port %s: BPDU is received from Different MST Region!\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    return MST_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSetRcvdMsgs                      */
/*                                                                           */
/*    Description               : This function sets the received message    */
/*                                flag for CIST and all MSTIs                */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port Number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSetRcvdMsgs (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSetRcvdMsgs (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2InstanceId = 1;
    INT2                i2Flag = MST_TRUE;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pPortInfo->u2LastInst = 0;
    if (pRcvdBpdu->u1Version > AST_VERSION_3)
    {
        pRcvdBpdu->u1Version = AST_VERSION_3;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
    /* Here setting of TC Flags has been removed for other versions
     * since the same will be done in the PISM as part of 
     * 802.1Q 2005. 
     * */

    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    if (pCistMstiPortInfo->bRcvdInternal != MST_TRUE)
    {
        /* Dont Decode CIST Internal Root Path Cost since the
         * bpdu is from external region 
         */
        pRcvdBpdu->u4CistIntRootPathCost = 0;
    }

    /* Setting the rcvdMsg flag for CIST */
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Port %s: Port does not exist for CIST",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;
    }

    (AST_GET_RST_PORT_INFO (pPerStPortInfo))->bRcvdMsg = MST_TRUE;

    /* Update the value of rcvdAnyMsg as per sec 13.25.11 of 802.1Q */
    pCistMstiPortInfo->bRcvdAnyMsg = MST_TRUE;
    AST_DBG_ARG1 (AST_PRSM_DBG, "PRSM: Port %s: RcvdAnyMsg = TRUE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if ((pPortInfo->bRootGuard != MST_TRUE)
        || ((pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ROOT)
            && (pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ALTERNATE)))
    {
        if (MstPortInfoMachine
            ((UINT1) MST_PINFOSM_EV_RCVD_BPDU, pPerStPortInfo, pRcvdBpdu,
             MST_CIST_CONTEXT) == MST_FAILURE)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PRSM: Port %s: Port Info Machine returned FAILURE for RCVD_MSG event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "PRSM: Port %s: Port Info Machine returned FAILURE for RCVD_MSG event\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            i2Flag = MST_FALSE;
        }
    }
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

    if (pCistMstiPortInfo->bRcvdInternal == MST_TRUE)
    {
        for (u2InstanceId = 1; u2InstanceId < AST_MAX_MST_INSTANCES;
             u2InstanceId++)
        {
            if (pRcvdBpdu->aMstConfigMsg[u2InstanceId].u1ValidFlag !=
                (UINT1) RST_TRUE)
            {
                continue;
            }
            pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
            if (pPerStPortInfo == NULL)
            {
                /* This represents a SISP enabled physical interface. This
                 * will not be participating in all the instances. Hence, 
                 * ignore this instance.
                 * */

                continue;
            }
            (AST_GET_RST_PORT_INFO (pPerStPortInfo))->bRcvdMsg = MST_TRUE;
            pCistMstiPortInfo->bRcvdAnyMsg = MST_TRUE;
            AST_DBG_ARG1 (AST_PRSM_DBG,
                          "PRSM: Port %s: RcvdAnyMsg = TRUE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            AST_INCR_RX_MST_BPDU_COUNT (u2InstanceId, u2PortNum);

            /* Here setting of TC Flags has been removed for other versions
             * since the same will be done in the PISM as part of 
             * 802.1Q 2005. 
             * */
            if ((pPortInfo->bRootGuard != MST_TRUE)
                || ((pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ROOT)
                    && (pPerStPortInfo->u1PortRole != AST_PORT_ROLE_ALTERNATE)))
            {
                if (u2InstanceId != MST_CIST_CONTEXT)
                {
                    if (MstPortInfoMachine
                        ((UINT1) MST_PINFOSM_EV_RCVD_BPDU, pPerStPortInfo,
                         pRcvdBpdu, u2InstanceId) == MST_FAILURE)

                    {
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "PRSM: Port %s: Inst %d: MstPortInfoMachine returned FAILURE for RCVD_MSG event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2InstanceId);
                        AST_DBG_ARG2 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                                      "PRSM: Port %s: Inst %d: MstPortInfoMachine returned FAILURE for RCVD_MSG event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2InstanceId);
                        i2Flag = MST_FALSE;
                    }
                }
            }
        }                        /* End of FOR Loop */
    }

    if (i2Flag == MST_FALSE)
    {
        return MST_FAILURE;
    }
    else
    {
        AST_DBG_ARG1 (AST_PRSM_DBG,
                      "PRSM: Port %s: Successfully extracted bpdu information\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSmChkRcvdBpdu                    */
/*                                                                           */
/*    Description               : This function processes the RCVD_BPDU      */
/*                                Event when the Port Receive State Machine  */
/*                                is in Discard State or Receive State.      */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSmChkRcvdBpdu (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSmChkRcvdBpdu (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;

    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
    pPerStRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

    if (pPerStRstPortInfo->bPortEnabled == MST_FALSE)
    {
        AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                      "PRSM: Port %s: Received BPDU is discarded Port is Disabled\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        if (MstPortRcvSmMakeDiscard (pRcvdBpdu, u2PortNum) != MST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RECV: Port %s: MstPortRcvSmMakeDiscard function returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }
    else
    {
        pPortEntry = AST_GET_PORTENTRY (u2PortNum);

        if (((pCistMstiPortInfo->bRcvdAnyMsg == MST_FALSE)
             || ((pPortEntry->bRootGuard != MST_FALSE)))
            && (AST_PORT_ENABLE_BPDU_RX (pPortEntry) == MST_TRUE)
            && (AST_PORT_IS_L2GP (pPortEntry) == MST_FALSE))
        {
            if (MstPortRcvSmMakeReceive (pRcvdBpdu, u2PortNum) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RECV: Port %s: MstPortRcvSmMakeReceive function returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return MST_FAILURE;
            }
        }
        else
        {
            if (pCistMstiPortInfo->bRcvdAnyMsg == MST_TRUE)
            {
                if (AstHandleImpossibleState (pPortEntry->u2PortNo) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG |
                                  AST_TMR_DBG,
                                  "TMR: Port %s: Starting RESTART Timer Failed\n",
                                  AST_GET_IFINDEX_STR (pPortEntry->u2PortNo));

                    return MST_FAILURE;
                }

            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSmChkRcvdNoMsg                   */
/*                                                                           */
/*    Description               : This function processes the RCVD_NO_MSG    */
/*                                Event when the Port Receive State Machine  */
/*                                is in Discard State or Receive State.      */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSmChkRcvdNoMsg (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSmChkRcvdNoMsg (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pPerStRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

    if (pPerStRstPortInfo->bPortEnabled == MST_TRUE)
    {

        if (pAstCommPortInfo->bRcvdBpdu == MST_TRUE)
        {

            if (MstPortRcvSmMakeReceive (pRcvdBpdu, u2PortNum) != MST_SUCCESS)
            {

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RECV: Port %s: MstPortRcvSmMakeReceive function returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRcvSmChkPortEnabled                 */
/*                                                                           */
/*    Description               : This function Processes the PORT_ENABLED   */
/*                                Event when the Port Receive State Machine  */
/*                                is in Discard State or Receive State.      */
/*                                                                           */
/*    Input(s)                  : pRcvdBpdu       - Pointer to the Received   */
/*                                                 BPDU Information.         */
/*                                u2PortNum      - Port number of the Port   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRcvSmChkPortEnabled (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
#else
INT4
MstPortRcvSmChkPortEnabled (pRcvdBpdu, u2PortNum)
     tMstBpdu           *pRcvdBpdu;
     UINT2               u2PortNum;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

    if (pAstCommPortInfo->bRcvdBpdu == MST_TRUE)
    {
        if (pCistMstiPortInfo->bRcvdAnyMsg == MST_FALSE)
        {

            if (MstPortRcvSmMakeReceive (pRcvdBpdu, u2PortNum) != MST_SUCCESS)
            {

                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RECV: Port %s: MstPortRcvSmMakeReceive function returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstInitPortReceiveMachine                            */
/*                                                                           */
/* Description        : Initialises the Port Received State Machine.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstInitPortRxStateMachine (VOID)
#else
VOID
MstInitPortRxStateMachine (VOID)
#endif
{
    INT4                i4Index = 0;

    UINT1               aau1SemEvent[MST_PRCVSM_MAX_EVENTS][20] = {
        "BEGIN", "RCVD_BPDU", "PORT_ENABLED", "NOT_RCVD_ANY_MSG"
    };
    UINT1               aau1SemState[MST_PRCVSM_MAX_STATES][20] = {
        "DISCARD", "RECEIVE"
    };
    for (i4Index = 0; i4Index < MST_PRCVSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PRSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PRSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < MST_PRCVSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PRSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PRSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* Event 0 - MST_PRCVSM_EV_BEGIN */
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_BEGIN]
        [MST_PRCVSM_STATE_DISCARD].pAction = MstPortRcvSmMakeDiscard;
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_BEGIN]
        [MST_PRCVSM_STATE_RECEIVE].pAction = NULL;

    /* Event 1 - MST_PRCVSM_EV_RCVD_BPDU */
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_RCVD_BPDU]
        [MST_PRCVSM_STATE_DISCARD].pAction = MstPortRcvSmChkRcvdBpdu;
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_RCVD_BPDU]
        [MST_PRCVSM_STATE_RECEIVE].pAction = MstPortRcvSmChkRcvdBpdu;

    /* Event 2 - MST_PRCVSM_EV_PORT_ENABLED */
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_PORT_ENABLED]
        [MST_PRCVSM_STATE_DISCARD].pAction = MstPortRcvSmChkPortEnabled;
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_PORT_ENABLED]
        [MST_PRCVSM_STATE_RECEIVE].pAction = MstPortRcvSmChkPortEnabled;

    /* Event 3 - MST_PRCVSM_EV_NOT_RCVD_ANY_MSG */
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_NOT_RCVD_ANY_MSG]
        [MST_PRCVSM_STATE_DISCARD].pAction = MstPortRcvSmChkRcvdNoMsg;
    MST_PORT_RECEIVE_MACHINE[MST_PRCVSM_EV_NOT_RCVD_ANY_MSG]
        [MST_PRCVSM_STATE_RECEIVE].pAction = MstPortRcvSmChkRcvdNoMsg;

}

#endif /* MSTP_WANTED */
