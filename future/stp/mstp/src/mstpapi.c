/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 * 
 * $Id: mstpapi.c,v 1.27 2016/02/11 06:43:31 siva Exp $
 *
 * Description: This file contains the interface modules used by other
 *              modules.
 *
 *******************************************************************/

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiDeleteAllVlans                        */
/*                                                                           */
/*    Description               : This routine is called when VLAN module is */
/*                                shutdown. It unmaps all active Vlans from  */
/*                                the instance to which they are mapped in   */
/*                                the hardware.                              */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/

INT4
MstMiDeleteAllVlans (UINT4 u4ContextId)
{
#ifdef NPAPI_WANTED
    tVlanId             PrevVlanId;
    tVlanId             VlanId;
    UINT2               u2MstInst;
    INT4                i4HwRetVal;
#endif
    INT4                i4RetVal = MST_SUCCESS;

    if (!AstIsMstEnabledInContext (u4ContextId))
    {
        return MST_SUCCESS;
    }

#ifdef NPAPI_WANTED
    /* Immediately after this funtion returns all Vlans are deleted in
     * L2Iwf. Hence any Vlan related information has to be obtained in the 
     * same context without any message posts */

    PrevVlanId = 0;
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        while (L2IwfMiGetNextActiveVlan (u4ContextId, PrevVlanId,
                                         &VlanId) == L2IWF_SUCCESS)
        {
            u2MstInst = AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

            PrevVlanId = VlanId;
            if (u2MstInst == 0)
            {
                continue;
            }

            i4HwRetVal = MstpFsMiMstpNpDelVlanInstMapping (u4ContextId, VlanId,
                                                           u2MstInst);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_GLOBAL_TRC (u4ContextId,
                                AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                                VlanId, u2MstInst);
                AST_GLOBAL_DBG (u4ContextId,
                                AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                AST_EVENT_HANDLING_DBG,
                                "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                                VlanId, u2MstInst);
                i4RetVal = MST_FAILURE;
            }
        }

    }

#endif

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiVlanDeleteIndication                  */
/*                                                                           */
/*    Description               : This routine is called whenever            */
/*                                a vlan is deleted. It unmaps this vlan     */
/*                                from the instance to which it is mapped in */
/*                                the hardware.                              */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is  deleted.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/
INT4
MstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    UINT2               u2MstInst = 0;
    INT4                i4HwRetVal;

    /* AST Lock need not be taken here since it does not access
     * any RSTP/ MSTP data structure */

    AST_GLOBAL_DBG (u4ContextId, AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
                    "API: Deleting Vlan %d .. \n", VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Deleting Vlan %d .. \n", VlanId);
    if (AST_IS_NP_PROGRAMMING_ALLOWED () != AST_TRUE)
    {
        return MST_SUCCESS;
    }

    if (!AstIsMstEnabledInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Mstp Module is not Enabled.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG,
                        "API: Mstp Module is not Enabled.. \n");
        return MST_SUCCESS;
    }

    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Invalid Vlan ID.. \n");
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG, "API: Invalid Vlan ID.. \n");
        return MST_FAILURE;
    }

    u2MstInst = AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    i4HwRetVal =
        MstpFsMiMstpNpDelVlanInstMapping (u4ContextId, VlanId, u2MstInst);
    if (i4HwRetVal != FNP_SUCCESS)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                        VlanId, u2MstInst);
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                        AST_EVENT_HANDLING_DBG,
                        "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                        VlanId, u2MstInst);
        return MST_FAILURE;
    }
#else
    AST_UNUSED (VlanId);
    AST_UNUSED (u4ContextId);
#endif /*NPAPI_WANTED */

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiVlanCreateIndication                  */
/*                                                                           */
/*    Description               : This routine is called  whenever           */
/*                                a vlan is created. If this vlan is mapped  */
/*                                to any instance in the software, then it   */
/*                                maps this vlan to that instance in the     */
/*                                hardware.                                  */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is created          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*    Called By                 : L2Iwf                                      */
/*****************************************************************************/
INT4
MstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    UINT2               u2MstInst = 0;
    INT4                i4HwRetVal;

    if (AST_IS_NP_PROGRAMMING_ALLOWED () != AST_TRUE)
    {
        return MST_SUCCESS;
    }

    if (!AstIsMstEnabledInContext (u4ContextId))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Mstp Module is not Enabled.. \n");
        return MST_SUCCESS;
    }

    if (MST_IS_VALID_VLANID (VlanId) == MST_FALSE)
    {
        return MST_FAILURE;
    }

    AST_GLOBAL_DBG (u4ContextId, AST_EVENT_HANDLING_DBG,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Obtained Vlan %d creation indication ... \n", VlanId);

    u2MstInst = AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    if (u2MstInst == 0)
    {
        /* If No Mapping - create hardware mapping to CIST */
        i4HwRetVal = MstpFsMiMstpNpAddVlanInstMapping (u4ContextId, VlanId,
                                                       MST_CIST_CONTEXT);
    }
    else
    {
        i4HwRetVal = MstpFsMiMstpNpAddVlanInstMapping (u4ContextId, VlanId,
                                                       u2MstInst);
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        AST_GLOBAL_DBG (u4ContextId,
                        AST_ALL_FAILURE_DBG | AST_EVENT_HANDLING_DBG,
                        "API: Hardware call to map Vlan %d to CIST failed\n",
                        VlanId);
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                        "API: Hardware call to map Vlan %d to CIST failed\n",
                        VlanId);
        return MST_FAILURE;
    }

    AST_GLOBAL_DBG (u4ContextId, AST_MGMT_DBG | AST_EVENT_HANDLING_DBG,
                    "API: Successfully handled vlan %d creation indication \n",
                    VlanId);
    AST_GLOBAL_TRC (u4ContextId, AST_CONTROL_PATH_TRC,
                    "API: Successfully handled vlan %d creation indication \n",
                    VlanId);
#else
    AST_UNUSED (VlanId);
    AST_UNUSED (u4ContextId);
#endif /*NPAPI_WANTED */
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsMstStartedInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is started in the given context         */
/*                      0 - No, Mstp is NOT started in the given context     */
/*****************************************************************************/
INT4
AstIsMstStartedInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return 0;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == MST_START))
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : AstIsMstEnabledInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is enabled   */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is enabled in the given context         */
/*                      0 - No, Mstp is NOT enabled in the given context     */
/*****************************************************************************/
INT4
AstIsMstEnabledInContext (UINT4 u4ContextId)
{
    if (u4ContextId >= AST_SIZING_CONTEXT_COUNT)
        return AST_FALSE;

    if ((gu1IsAstInitialised == RST_TRUE) &&
        (gau1AstSystemControl[u4ContextId] == MST_START) &&
        (gau1AstModuleStatus[u4ContextId] == MST_ENABLED))
    {
        return AST_TRUE;
    }
    return AST_FALSE;
}

/*****************************************************************************/
/* Function Name      : MstGetEnabledStatus                                  */
/*                                                                           */
/* Description        : Get the MSTP module status. Called by other modules  */
/*                      to know if MSTP is enabled or disabled.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_ENABLED (or) MST_DISABLED                        */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/* Calling Functions  : BrgGetPortStpState                                   */
/*                      BrgGetPortStpEnableStatus                            */
/*                      TpIsPortOkForForwarding                              */
/*****************************************************************************/
INT4
MstGetEnabledStatus (VOID)
{
    if (AstIsMstEnabledInContext (AST_DEFAULT_CONTEXT))
    {
        return MST_ENABLED;
    }

    return MST_DISABLED;
}

/*****************************************************************************/
/* Function Name      : MstpMapVidToMstId                                    */
/*                                                                           */
/* Description        : This API is called to map a Vlan ID to a MST-ID.     */
/*                      The MST-ID can also be the CIST in which case this   */
/*                      routine removes the Vlan from the previously mapped  */
/*                      MSTI. This routine performs the same operation as    */
/*                      that when the instance to Vlan mapping is configured */
/*                      from management.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Called By          :                                                      */
/* Calling Functions  :                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstpMapVidToMstId (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstId)
{
    UINT4               u4ErrorCode = 0;
    UINT2               u2OldMstInst = 0;

    if (!AstIsMstStartedInContext (u4ContextId))
    {
        return MST_SUCCESS;
    }

    AST_LOCK ();

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    if (u2MstId == MST_CIST_CONTEXT)
    {
        /* If the VLAN is mapped to CIST, UnMap the vlan from the previously
           mapped instance */

        u2OldMstInst = AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);
        if (u2OldMstInst == u2MstId)
        {
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_SUCCESS;
        }
        if (MstTestMstUnMapVlanIndex (&u4ErrorCode,
                                      (INT4) u2OldMstInst, (INT4) VlanId)
            == MST_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId, AST_MGMT_TRC, "API: Testing of "
                            "unmapping of MSTP Instance Failed for VLAN :%d\n",
                            VlanId);
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }
        if (MstSetMstUnMapVlanIndex ((INT4) u2OldMstInst, (INT4) VlanId) ==
            MST_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId, AST_MGMT_TRC, "API: UnMapping of MSTP "
                            "Instance Failed for VLAN :%d\n", VlanId);
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }
    }
    else
    {
        if (MstTestMstMapVlanIndex (&u4ErrorCode, (INT4) u2MstId,
                                    (INT4) VlanId) == MST_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId, AST_MGMT_TRC,
                            "API:Mapping of MSTP instance Fail for VLAN :%d\n",
                            VlanId);
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }
        if (MstSetMstMapVlanIndex ((INT4) u2MstId, (INT4) VlanId) ==
            MST_FAILURE)
        {
            AST_GLOBAL_TRC (u4ContextId, AST_MGMT_TRC, "API: Mapping of MSTP "
                            "Instance Failed for VLAN :%d\n", VlanId);
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }
    }
    AstReleaseContext ();
    AST_UNLOCK ();
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstUpdateVlanPortList                                */
/*                                                                           */
/* Description        : This MSTP API will be invoked from VLAN, whenever    */
/*                      interfaces are either added to VLAN or deleted from  */
/*                      the VLAN. This API will post a message to MSTP, which*/
/*                      will add the SISP enabled ports to corresponding     */
/*                      instances or delete the SISP enabled ports from      */
/*                      instances depending upon the configuration.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - Vlan Identifier                        */
/*                      AddedPorts  - Port list bit map for ports that are   */
/*                                    added to the VLAN.                     */
/*                      DeletedPorts- Port list bit map for ports that are   */
/*                                    deleted from the VLAN.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Called By          :                                                      */
/* Calling Functions  :                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdateVlanPortList (UINT4 u4ContextId, tVlanId VlanId,
                       tLocalPortList AddedPorts, tLocalPortList DeletedPorts)
{
    tAstMsgNode        *pNode = NULL;

    if (!((AstIsRstStartedInContext (u4ContextId)) ||
          (AstIsMstStartedInContext (u4ContextId)) ||
          (AstIsPvrstStartedInContext (u4ContextId))))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: ASTP Module has been SHUTDOWN!\n");

        return MST_FAILURE;
    }

    if (!(AstIsMstStartedInContext (u4ContextId)))
    {
        /* This message should be handled only when MSTP is enabled */
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: VLAN to port map message should be handled only when MSTP is the operating mode!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: VLAN to port map message should be handled only when MSTP is the operating mode!\n");

        return MST_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);
        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        return MST_FAILURE;
    }

    if ((AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId)) ==
        MST_CIST_CONTEXT)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: VLAN to port map cannot be processed when the vlan is mapped to CIST context!\n");

        AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode);

        return MST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_SISP_VLAN_UPDT_MSG;

    pNode->u4ContextId = u4ContextId;
    pNode->uMsg.VlanId = VlanId;
    AST_MEMCPY (pNode->AddedPorts, AddedPorts, CONTEXT_PORT_LIST_SIZE);
    AST_MEMCPY (pNode->DeletedPorts, DeletedPorts, CONTEXT_PORT_LIST_SIZE);

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) RST_SUCCESS)
    {
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispValidateInstRestriction                       */
/*                                                                           */
/* Description        : This MSTP API will be invoked from L2IWF,whenever    */
/*                      interfaces are added to VLAN. This API will check    */
/*                      against the restriction that the same port cannot be */
/*                      present in same instance in two different contexts.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u4PortNum   - Port number                            */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Calling Functions  : MstSispIsPerStPortCreationValid                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstSispValidateInstRestriction (UINT4 u4ContextId, UINT4 u4PortNum,
                                tVlanId VlanId)
{
    tAstPortEntry      *pPhyPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4PhyPort = AST_INVALID_PORT_NUM;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2MstInst = AST_INIT_VAL;
    UINT1               u1Status = AST_INIT_VAL;
    BOOL1               bResult = MST_TRUE;

    if (!(AstIsRstStartedInContext (u4ContextId) ||
          AstIsMstStartedInContext (u4ContextId) ||
          AstIsPvrstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: MSTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: MSTP Module has been SHUTDOWN!\n");

        return MST_SUCCESS;
    }

    L2IwfGetSispPortCtrlStatus (u4PortNum, &u1Status);

    if (u1Status != SISP_ENABLE)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: SISP is disabled in port!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: SISP is disabled in port!\n");

        return MST_SUCCESS;
    }

    AST_LOCK ();

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: MSTP is not the operating mode!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: MSTP is not the operating mode!\n");

        AstReleaseContext ();
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    u2MstInst = AstL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        /* Vlan is not mapped to any of the instances. It is GUIDELINE
         * to map the vlans to MSTIs. Hence not proceeding with the check
         * */
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: VLAN is not mapped to any inst!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: VLAN is not mapped to any inst!\n");
        AstReleaseContext ();
        AST_UNLOCK ();

        return MST_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4PortNum, &u4ContextId, &u2LocalPort)
        != RST_SUCCESS)
    {
        /* Invalid Port */
        AstReleaseContext ();
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPort, u2MstInst);
    if (pPerStPortInfo != NULL)
    {
        /* This port is already member of this instance
         * in this context only -- VLAN Membership updation.
         * No need to check the bitmap in this case
         * */
        AstReleaseContext ();
        AST_UNLOCK ();
        return MST_SUCCESS;
    }

    u4PhyPort = AST_GET_PHY_PORT (u2LocalPort);

    if (u4PhyPort == AST_INIT_VAL)
    {
        /* Invalid Port */
        AstReleaseContext ();
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    if (AstGetContextInfoFromIfIndex (u4PhyPort, &u4ContextId, &u2PortNum)
        != RST_SUCCESS)
    {
        AstReleaseContext ();
        AST_UNLOCK ();
        return MST_FAILURE;
    }

    /* Switch to primary context id
     * */
    AstReleaseContext ();
    AstSelectContext (u4ContextId);

    if ((pPhyPortEntry = AST_GET_PORTENTRY (u2PortNum)) != NULL)
    {
        AST_IS_INST_SET_IN_LIST (pPhyPortEntry->InstList,
                                 u2MstInst, MST_INST_LIST_SIZE, bResult);

        if (bResult == MST_TRUE)
        {
            /* Instance already have this port as member */
            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }
    }

    AstReleaseContext ();

    AST_UNLOCK ();

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstUpdateSispStatusOnPort                            */
/*                                                                           */
/* Description        : This MSTP API will be invoked by SISP, whenever SISP */
/*                      status is updated on the interface. This API will    */
/*                      post a message to MSTP, which will handle this event.*/
/*                                                                           */
/* Input(s)           : u4IfIndex   - Interface Index.                       */
/*                      u4ContextId - Context Id of Interface Index.         */
/*                      bStatus     - SISP Status,can be either,             */
/*                                        1) SISP_ENABLE                     */
/*                                        1) SISP_DISABLE                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Called By          :                                                      */
/* Calling Functions  :                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdateSispStatusOnPort (UINT4 u4IfIndex, UINT4 u4ContextId,
                           tAstBoolean bStatus)
{
    tAstMsgNode        *pNode = NULL;

    if (!(AstIsMstStartedInContext (u4ContextId)))
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: MSTP Module has been SHUTDOWN!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: MSTP Module has been SHUTDOWN!\n");

        return MST_FAILURE;
    }

    AST_LOCK ();

    if (AstSelectContext (u4ContextId) != RST_FAILURE)
    {
        if (!(AST_IS_MST_ENABLED ()) && (AST_FORCE_VERSION != AST_VERSION_3))
        {
            AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                            "MGMT: MSTP is not the operating mode!\n");
            AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                            "MGMT: MSTP is not the operating mode!\n");

            AstReleaseContext ();
            AST_UNLOCK ();
            return MST_FAILURE;
        }

        AstReleaseContext ();
    }
    else
    {
        AST_GLOBAL_TRC (u4ContextId, AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                        "MGMT: Invalid Context Identifier!\n");
        AST_GLOBAL_DBG (u4ContextId, AST_ALL_FAILURE_DBG,
                        "MGMT: Invalid Context Identifier!\n");

        AST_UNLOCK ();
        return MST_FAILURE;
    }

    AST_UNLOCK ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();

        AstGlobalMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN,
                              u4ContextId);

        AST_GLOBAL_DBG (u4ContextId,
                        AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");

        return MST_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = (tAstLocalMsgType) AST_SISP_STATUS_MSG;

    pNode->u4PortNo = u4IfIndex;
    pNode->u4ContextId = u4ContextId;

    if (bStatus == SISP_ENABLE)
    {
        pNode->uMsg.bSispStatus = MST_ENABLED;
    }
    else
    {
        pNode->uMsg.bSispStatus = MST_DISABLED;
    }

    if (AstSendEventToAstTask (pNode, u4ContextId) != (INT4) MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}
