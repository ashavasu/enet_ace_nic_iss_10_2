/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmrdstb.c,v 1.8 2007/02/01 15:07:58 iss Exp $
 *
 * Description: This file contains the functions to support High 
 *              Availability for MSTP module.   
 *
 *******************************************************************/

#include "astminc.h"

#ifndef L2RED_WANTED
#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : MstRedStorePduInActive                               */
/*                                                                           */
/* Description        : Stores the latest PDUs on the Active Node            */
/* Input(s)           : u2PortIfIndex - Port on which the PDU was received   */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the PDU                            */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/

INT4
MstRedStorePduInActive (UINT2 u2PortNum, tMstBpdu * pData, UINT2 u2Len,
                        UINT2 u2InstanceId)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u2InstanceId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstRedClearAllBpdusOnActive                */
/*                                                                           */
/*    Description               : This function clears all the stored BPDU   */
/*                                information on ACTIVE node, called when the*/
/*                                protocol is disabled or Shutdown on Active.*/
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstRedClearAllBpdusOnActive ()
#else
INT4
MstRedClearAllBpdusOnActive ()
#endif
{
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstRedClearAllSyncUpDataInInst             */
/*                                                                           */
/*    Description               : This function clears all the stored Sync up*/
/*                                information on Standby node for an MST     */
/*                                instance, called when an Instance becomes  */
/*                                inactive on Active.                        */
/*                                                                           */
/*    Input(s)                  : u2MstInst - MST Instance identifier        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstRedClearAllSyncUpDataInInst (UINT2 u2MstInst)
{
    AST_UNUSED (u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedProtocolRestart                                */
/*                                                                           */
/* Description        : Restart MSTP protocol on a particular Port & Instance*/
/* Input(s)           : u2Port - Port                                        */
/*                      u2InstanceId - MSTP Instance identifier              */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*****************************************************************************/
INT4
MstRedProtocolRestart (UINT2 u2Port, UINT2 u2InstanceId)
{
    AST_UNUSED (u2Port);
    AST_UNUSED (u2InstanceId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedClearPduOnActive                               */
/* Description        : Invalidates the PDU in Active Node                   */
/* Input(s)           :                                                      */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedClearPduOnActive (UINT2 u2PortNum)
{
    AST_UNUSED (u2PortNum);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedClearInstanceOnActive                          */
/* Description        : Clears that particular instance information          */
/*                      in the stored PDU in Active Node                     */
/* Input(s)           : u2InstanceId - Instance id which needs to be cleared */
/*                    : u2PortNum - Port Number of Port on which PDU needs to*/
/*                                  be cleared.                              */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tAstRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tAstRedGlobalInfo.                                   */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstRedClearInstanceOnActive (UINT2 u2InstanceId, UINT2 u2PortNum)
{
    AST_UNUSED (u2InstanceId);
    AST_UNUSED (u2PortNum);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstRedSyncUpPdu                                      */
/*                                                                           */
/* Description        : Restart MSTP protocol on a particular Port & Instance*/
/* Input(s)           : u2PortNum - Port Number to Sync up data on.          */
/*                      pData - The Received PDU                             */
/*                      u2Len - Length of the BPDU                           */
/*                      u2InstanceId - Instance Identifier                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*****************************************************************************/
INT4
MstRedSyncUpPdu (UINT2 u2PortNum, tMstBpdu * pData, UINT2 u2Len)
{
    AST_UNUSED (u2PortNum);
    AST_UNUSED (pData);
    AST_UNUSED (u2Len);
    return MST_SUCCESS;
}
#endif
