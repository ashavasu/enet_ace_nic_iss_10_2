/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmrtsm.c,v 1.55 2017/11/21 13:06:51 siva Exp $
 *
 * Description: This file contains MST Port Role Transition       
 *              related routines.                 
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPortRoleTransitMachine                  */
/*                                                                           */
/*    Description               : This function is the entry point function  */
/*                                for the Port Role Transition State Machine.*/
/*                                Based on the Received event the respective */
/*                                SEM routines are called.                   */
/*                                                                           */
/*    Input(s)                  : u1Event - Triggering Event                 */
/*                                pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPortRoleTransitMachine (UINT1 u1Event,
                           tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
#else
INT4
MstPortRoleTransitMachine (u1Event, pPerStPortInfo, u2MstInst)
     UINT1               u1Event;
     tAstPerStPortInfo  *pPerStPortInfo;
     UINT2               u2MstInst;
#endif
{
    INT4                i4RetVal = 0;
    UINT1               u1State = 0;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Event %s: Inst %d: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_RTSM][u1Event], u2MstInst);
        return MST_FAILURE;
    }

    u1State = pPerStPortInfo->u1ProleTrSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "Port %s: Inst %d: Role Transition Machine Called with Event: %s, and State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst,
                  gaaau1AstSemEvent[AST_RTSM][u1Event],
                  gaaau1AstSemState[AST_RTSM][u1State]);
    AST_DBG_ARG4 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %d: Role Tr Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst,
                  gaaau1AstSemEvent[AST_RTSM][u1Event],
                  gaaau1AstSemState[AST_RTSM][u1State]);

    if (MST_PORT_ROLE_TR_MACHINE[u1Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_RTSM_DBG, "RTSM: No Operations to Perform\n");
        return MST_SUCCESS;
    }

    i4RetVal = (*(MST_PORT_ROLE_TR_MACHINE[u1Event][u1State].pAction))
        (pPerStPortInfo, u2MstInst);

    if (i4RetVal != MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "PROLETRSM: Event routine returned failure\n");
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmUpdateInfoReset                */
/*                                                                           */
/*    Description               : This function handles the UpdtInfo reset   */
/*                                Event.                                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmUpdateInfoReset (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %d: Selected, Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    if (pPerStPortInfo->u1PortRole != pPerStPortInfo->u1SelectedPortRole)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "RTSM: Port %s: Inst %u: Role Change Detected... taking action ... \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Role Change Detected... taking action ... \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        return (MstPRoleTrSmRoleChanged (pPerStPortInfo, u2MstInst));
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT)
    {
        /* Try all conditions out of the ROOT_PORT state */
        if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_FALSE,
                                             u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: RootPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ALTERNATE) ||
        (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_BACKUP))
    {
        /* Try all conditions out of the ALTERNATE_PORT state */
        if (MstProleTrSmAltPortTransitions
            (pPerStPortInfo, MST_FALSE, u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: AltPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
    {
        /* Try all conditions out of the DESIGNATED_PORT state */
        if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE,
                                              u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)
    {
        /* Try all conditions out of the MASTER_PORT state */
        if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE,
                                               u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MasterPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    /* After BEGIN is cleared, and roles selected, the UCT transition from 
     * INIT state to DISABLE state, for any Disabled port is handled below */
    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED) &&
        (pPerStPortInfo->u1ProleTrSmState == RST_PROLETRSM_STATE_INIT_PORT))
    {
        return MstPRoleTrSmMakeDisablePort (pPerStPortInfo, u2MstInst);
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %d: UpdateInfo reset event handled successfully\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmRoleChanged                              */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state as per the role changed          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmRoleChanged (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    INT4                i4RetVal = MST_SUCCESS;
    UINT2               u2Duration;

#ifdef MRP_WANTED
    UINT1               u1CurrPortRole = 0;
    tMrpInfo            MrpStpInfo;
    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));
#endif

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

#ifdef MRP_WANTED
    u1CurrPortRole = pPerStPortInfo->u1PortRole;
#endif

    switch (pPerStPortInfo->u1SelectedPortRole)
    {
        case AST_PORT_ROLE_DISABLED:

            /* If this Port Role is changing from Root Port Role to Disabled Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u:Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Root->Disabled: Port %s: Inst %u: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            i4RetVal = MstPRoleTrSmMakeDisablePort (pPerStPortInfo, u2MstInst);
            break;

        case AST_PORT_ROLE_ALTERNATE:
            /* If this Port Role is changing from Backup Port Role 
             * to Alternate Port Role, Start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u: Start Timer of RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Alternate: Port %s: Inst %u: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If the port role is changing from Designated to Alternate
             * and loop guard is enabled on this port, set the loop guard
             * status in per spanning tree data structure*/
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
                && (pPortInfo->bLoopGuard == AST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE))
            {
                pPerStPortInfo->bLoopGuardStatus = AST_TRUE;
            }

            /* Fall through and continue processing for Backup Role */
        case AST_PORT_ROLE_BACKUP:

            /* If this Port Role is changing from Root Port Role to Alternate/Backup Port
             * Role, then Start the Recent Root While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u:Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Root->Alternate: Port %s: Inst %u: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            i4RetVal = MstPRoleTrSmMakeBlockPort (pPerStPortInfo, u2MstInst);
            break;

        case AST_PORT_ROLE_ROOT:

            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: New Port Role selected is ROOT\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            /* If this Port Role is changing from Backup Port Role to Root Port
             * Role, Start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Start Timer for RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Root: Port %s: Inst %u: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If the port role is changing from Designated to Root
             * and loop guard is enabled on this port, set the loop guard
             * status in per spanning tree data structure*/
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED)
                && (pPortInfo->bLoopGuard == AST_TRUE)
                && (pPortInfo->bOperPointToPoint == RST_TRUE))
            {
                pPerStPortInfo->bLoopGuardStatus = AST_TRUE;
            }

            /* If this Port Role is changing from Backup or Alternate or Disabled
             * Port Roles to Root Port Role, Start the Forward Delay timer.
             */
            if (((pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_ROOT)
                 && (pPerStPortInfo->u1PortRole !=
                     (UINT1) MST_PORT_ROLE_MASTER))
                &&
                !((pPerStPortInfo->u1PortRole ==
                   (UINT1) AST_PORT_ROLE_DESIGNATED)
                  && (pPortInfo->bLoopGuard != AST_TRUE)
                  && (pPerStPortInfo->bLoopGuardStatus != AST_TRUE)))
            {

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DISABLED)
                {
                    /* DISABLED -> ROOT - Will never happen in our 
                     * implementation */

                    u2Duration = pPortInfo->DesgTimes.u2MaxAge;

                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Disable->Root: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }
                else
                {
                    /* ALTERNATE-> ROOT */

                    /* On an MSTP link, the ports connected to shared LANs
                     * can be put to forwarding hop by hop (wait until
                     * neighbouring bridge receives bpdu i.e. wait for a
                     * multiple of hello time) rather than waiting for the
                     * entire network to converge (i.e. wait for FwdDelay).
                     * This is because even shared links act on proposals
                     * received on root ports and sync (block) their
                     * designated ports.
                     */

                    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->
                        bSendRstp == MST_TRUE)
                    {
                        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                    }
                    else
                    {
                        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                    }

                    AST_DBG_ARG3 (AST_SM_VAR_DBG,
                                  "RTSM_Alternate->Root: Port %s: Inst %u: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  u2MstInst, u2Duration);
                }

                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is DISABLED/BACKUP/ALTERNATE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u: Start Timer for FdWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }

            i4RetVal = MstPRoleTrSmMakeRootPort (pPerStPortInfo, u2MstInst);
            break;

        case AST_PORT_ROLE_DESIGNATED:

            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: New Port Role selected is DESIGNATED\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            /* If this Port Role is changing from Root Port Role to Designated Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u:Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Root->Designated: Port %s: Inst %u: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If this Port Role is changing from Backup Port Role to Designated
             * Port Role, then start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u Start Timer for RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Designated: Port %s: Inst %u: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If this Port Role is changing from Backup or Alternate or 
             * Disabled Port Role to Designated Port Role, then start 
             * Forward Delay While Timer.
             */
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DISABLED)
                || (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP))
            {

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DISABLED)
                {
                    /* DISABLED -> DESIGNATED */

                    /* If the port is newly enabled then it should go to 
                     * learning only after MaxAge since this is the maximum 
                     * time taken by the neighbouring bridge to age out it's 
                     * old information if it is a legacy STP bridge */

                    u2Duration = pPortInfo->DesgTimes.u2MaxAge;

                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Disable->Designated: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }
                else
                {
                    /* ALTERNATE-> DESIGNATED */

                    /* On an MSTP link, the ports connected to shared LANs
                     * can be put to forwarding hop by hop (wait until
                     * neighbouring bridge receives bpdu i.e. wait for a
                     * multiple of hello time) rather than waiting for the
                     * entire network to converge (i.e. wait for FwdDelay).
                     * This is because even shared links act on proposals
                     * received on root ports and sync (block) their
                     * designated ports.
                     */

                    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->
                        bSendRstp == MST_TRUE)
                    {
                        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                    }
                    else
                    {
                        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                    }

                    AST_DBG_ARG3 (AST_SM_VAR_DBG,
                                  "RTSM_Alternate->Designated: Port %s: Inst %u:fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  u2MstInst, u2Duration);
                }

                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is DISABLED/BACKUP/ALTERNATE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u Start Timer for FdWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }
            i4RetVal = MstPRoleTrSmMakeDesigPort (pPerStPortInfo, u2MstInst);
            break;

        case MST_PORT_ROLE_MASTER:

            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: New Port Role selected is MASTER\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            /* If this Port Role is changing from Root Port Role to Master Port
             * Role, then Start the Recent Root While timer.
             */

            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is ROOT\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2ForwardDelay);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RRWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u:Start Timer for RrWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Root->Master: Port %s: Inst %u: rrWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If this Port Role is changing from Backup Port Role to Master
             * Port Role, then start the Recent Backup While timer.
             */
            if (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is BACKUP\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                u2Duration = (UINT2) (2 * pPortInfo->DesgTimes.u2HelloTime);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_RBWHILE, u2Duration) !=
                    (INT4) MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u Start Timer for RbWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }

                AST_DBG_ARG3 (AST_SM_VAR_DBG,
                              "RTSM_Backup->Master: Port %s: Inst %u: rbWhile = %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, u2Duration);
            }

            /* If this Port Role is changing from Backup or Alternate or 
             * Disabled Port Role to Master Port Role, then start 
             * Forward Delay While Timer.
             */
            if ((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DISABLED)
                || (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_ALTERNATE)
                || (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_BACKUP))
            {

                if (pPerStPortInfo->u1PortRole ==
                    (UINT1) AST_PORT_ROLE_DISABLED)
                {
                    /* DISABLED -> Master */

                    /* If the port is newly enabled then it should go to 
                     * learning only after MaxAge since this is the maximum 
                     * time taken by the neighbouring bridge to age out it's 
                     * old information if it is a legacy STP bridge */

                    u2Duration = pPortInfo->DesgTimes.u2MaxAge;

                    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                                  "RTSM_Disable->Master: Port %s: fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2Duration);
                }
                else
                {
                    /* ALTERNATE-> Master */

                    /* As per 802.1Q rev, the value of FdWhile timer 
                     * should be maintained as 
                     * (i)  HelloTime, if sendRstp is true
                     * (ii) ForwardDelay, if sendRstp is false
                     * This is devised so because, even in case of sharedLANS,
                     * the peers are going to process the proposal.
                     * Hence ignoring the agreement, but reducing FdWhile
                     * */

                    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->
                        bSendRstp == MST_TRUE)
                    {
                        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
                    }
                    else
                    {
                        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
                    }

                    AST_DBG_ARG3 (AST_SM_VAR_DBG,
                                  "RTSM_Alternate->Master: Port %s: Inst %u:fdWhile = %u\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo),
                                  u2MstInst, u2Duration);
                }

                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %u: Old Port Role is DISABLED/BACKUP/ALTERNATE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);

                if (AstStartTimer
                    ((VOID *) pPerStPortInfo, u2MstInst,
                     (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u Start Timer for FdWhileTmr Failed !!!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }
            i4RetVal = MstPRoleTrSmMakeMasterPort (pPerStPortInfo, u2MstInst);
            break;

        default:
            /* Selected Port Role is unknown, so return failure */
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Unknown Port Role selected\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            i4RetVal = MST_FAILURE;
            break;
    }
#ifdef MRP_WANTED
    if (i4RetVal == MST_SUCCESS)
    {
        MrpStpInfo.u1Flag = MSG_PORT_ROLE_CHANGE;
        MrpStpInfo.u4IfIndex = AST_IFENTRY_IFINDEX (pPortInfo);
        MrpStpInfo.u2MapId = u2MstInst;
        MrpStpInfo.u1OldRole = u1CurrPortRole;
        MrpStpInfo.u1NewRole = pPerStPortInfo->u1SelectedPortRole;

        AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
    }
#endif
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmMakeBlockPort                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to BLOCK_PORT.                   */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeBlockPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    if (pAstPortEntry == NULL)
    {
        return PVRST_FAILURE;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;

    if ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ALTERNATE) &&
        (pAstPortEntry->bLoopGuard == RST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == RST_FALSE) &&
        (pAstPortEntry->bOperPointToPoint == RST_TRUE))
    {
        pPerStPortInfo->bLoopGuardStatus = RST_TRUE;
    }

    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Port Role Made as -> ALTERNATE/BACKUP\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->bLearn = RST_FALSE;
    pRstPortInfo->bForward = RST_FALSE;

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_BlockPort: Port %s: Inst %u:Role = ALT/BACKUP Learn = Forward = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state BLOCK_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) MST_PROLETRSM_STATE_BLOCK_PORT;

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state ALTERNATE_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo, u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %u: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: Port State Transition Machine returned FAILURE !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if (MstTopologyChMachine (MST_TOPOCHSM_EV_NOT_DESG_ROOT, u2MstInst,
                              pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %u: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: Topology Change Machine returned FAILURE !!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if ((pRstPortInfo->bLearning == RST_FALSE) &&
        (pRstPortInfo->bForwarding == RST_FALSE))
    {
        return MstPRoleTrSmMakeAlternatePort (pPerStPortInfo, u2MstInst);
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmMakeDesigPort                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DESG_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeDesigPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               u1PrevPortRole = AST_PORT_ROLE_DISABLED;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u1PrevPortRole = AST_GET_PORT_ROLE (u2MstInst, pPerStPortInfo->u2PortNo);
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_DESIGNATED;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_RTSM_DBG,
                  "RTSM_DesignatedPort: Port %s: Inst %u: Role = DESIGNATED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (u1PrevPortRole != AST_PORT_ROLE_DESIGNATED)
    {
        AST_SET_CHANGED_FLAG (u2MstInst, pPerStPortInfo->u2PortNo) = MST_TRUE;
    }
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Inst %u: Port Role Made as -> DESIGNATED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) MST_PROLETRSM_STATE_DESIG_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmMakeMasterPort                            */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to DESG_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeMasterPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               u1PrevPortRole = AST_PORT_ROLE_DISABLED;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u1PrevPortRole = AST_GET_PORT_ROLE (u2MstInst, pPerStPortInfo->u2PortNo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) MST_PORT_ROLE_MASTER;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_RTSM_DBG,
                  "RTSM_MasternatedPort: Port %s: Inst %u: Role = DESIGNATED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (u1PrevPortRole != MST_PORT_ROLE_MASTER)
    {
        AST_SET_CHANGED_FLAG (u2MstInst, pPerStPortInfo->u2PortNo) = MST_TRUE;
    }

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Inst %u: Port Role Made as -> MASTER\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) MST_PROLETRSM_STATE_MASTER_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state MASTER\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the Master_PORT state */
    if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE,
                                           u2MstInst) != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmFwdExpRootPort                 */
/*                                                                           */
/*    Description               : This function handles the forward Delay    */
/*                                timer expiry event for the Root Port       */
/*                                State.                                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmFwdExpRootPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %d: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }
    if (pPerStPortInfo->bRootInconsistent != MST_TRUE)
    {
        if (pRstPortInfo->bLearn == MST_FALSE)
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootLearn: Port %s: Learn = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return MST_FAILURE;
            }
        }

        else if (pRstPortInfo->bForward == MST_FALSE)
        {
            if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE) &&
                 (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (pRstPortEntry->bOperPointToPoint == RST_TRUE)) ||
                ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
                 && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
                 (MstIsPortMemberOfInst
                  ((INT4) (AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo))),
                   (INT4) u2MstInst) == MST_TRUE)))

            {
                /* LoopInconsitent state is set when the port transtions
                 * to Designated/Discarding because of loop-guard feature*/
                pRstPortEntry->bLoopInconsistent = RST_TRUE;
                pPerStPortInfo->bLoopIncStatus = RST_TRUE;

                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2MstInst,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     u2MstInst) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RTSM_RootForward: Port %s: Forward = TRUE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeForward function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return MST_FAILURE;
                }
            }

            /* After calling Make Forward, this Port would have transitioned to
             * the Forwarding state (i.e.) bForward would be True */

            /* Shifting to ReRooted state */
            if (pRstPortInfo->bReRoot == MST_TRUE)
            {
                pRstPortInfo->bReRoot = MST_FALSE;

                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RTSM_Rerooted: Port %s: ReRoot = FALSE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                AST_DBG_ARG1 (AST_RTSM_DBG,
                              "RTSM: Port %s: Moved to state REROOTED \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmFwdExpDesigOrMasterPort        */
/*                                                                           */
/*    Description               : This function handles the forward Delay    */
/*                                timer expiry event for the Root Port       */
/*                                State.                                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmFwdExpDesigOrMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                     UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %d: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }
    if (pPerStPortInfo->bRootInconsistent != MST_TRUE)
    {
        if (pRstPortInfo->bLearn == MST_FALSE)
        {
            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootLearn: Port %s: Learn = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (pRstPortEntry->bOperPointToPoint == RST_TRUE)) ||
                ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
                 (MstIsPortMemberOfInst
                  (((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo))),
                   (INT4) u2MstInst) == MST_TRUE)))
            {
                /* LoopInconsitent state is set when the port transtions  
                 * to Designated/Discarding because of loop-guard feature*/
                pRstPortEntry->bLoopInconsistent = RST_TRUE;
                pPerStPortInfo->bLoopIncStatus = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2MstInst,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     u2MstInst) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeLearn function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return MST_FAILURE;
                }
            }
        }

        else if (pRstPortInfo->bForward == MST_FALSE)
        {
            if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (pRstPortEntry->bOperPointToPoint == RST_TRUE)
                 && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)) ||
                ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
                 && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
                 (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                 (MstIsPortMemberOfInst
                  (((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo))),
                   (INT4) u2MstInst) == MST_TRUE)
                 && (pRstPortEntry->bOperPointToPoint == RST_TRUE)))

            {
                /* LoopInconsitent state is set when the port transtions  
                 * to Designated/Discarding because of loop-guard feature*/
                pRstPortEntry->bLoopInconsistent = RST_TRUE;
                pPerStPortInfo->bLoopIncStatus = RST_TRUE;
                pRstPortInfo->bLearn = RST_FALSE;
                pRstPortInfo->bForward = RST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2MstInst,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
                if (RstPortStateTrMachine
                    (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                     u2MstInst) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }

            }
            else
            {
                AST_DBG_ARG1 (AST_SM_VAR_DBG,
                              "RTSM_RootForward: Port %s: Forward = TRUE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

                if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeForward function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return MST_FAILURE;
                }
            }

            pRstPortInfo->bAgreed =
                (AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmRrWhileExpDesigPort            */
/*                                                                           */
/*    Description               : This function handles the Recent root while*/
/*                                timer expiry in Desig Port State.          */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmRrWhileExpDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4ReRooted;
    tAstPerStPortInfo  *pRootPortInfo = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %d: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    pRstPortInfo->bReRoot = MST_FALSE;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state REROOTED for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    if (((u2MstInst == 0) && (pPortInfo->bLoopGuard == RST_TRUE) &&
         (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
         (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
         (pPortInfo->bOperPointToPoint == RST_TRUE)) ||
        ((u2MstInst != 0) && (pPortInfo->bLoopGuard == RST_TRUE)
         && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
         (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
         (pPortInfo->bOperPointToPoint == RST_TRUE) &&
         (MstIsPortMemberOfInst
          ((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
           (INT4) u2MstInst) == MST_TRUE)))
    {
        /* LoopInconsitent state is set when the port transtions
         * to Designated/Discarding because of loop-guard feature*/
        pPortInfo->bLoopInconsistent = RST_TRUE;
        pPerStPortInfo->bLoopIncStatus = RST_TRUE;

        pRstPortInfo->bLearn = RST_FALSE;
        pRstPortInfo->bForward = RST_FALSE;
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst, au1TimeStr);
        AstLoopIncStateChangeTrap ((UINT2)
                                   AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                                   pPerStPortInfo->bLoopIncStatus, u2MstInst,
                                   (INT1 *) AST_MST_TRAPS_OID,
                                   AST_MST_TRAPS_OID_LEN);
        if (RstPortStateTrMachine
            (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
             u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }

    }

    if (((pRstPortInfo->pFdWhileTmr == NULL) ||
         (pRstPortInfo->bAgreed == MST_TRUE) ||
         (pPortInfo->bOperEdgePort == MST_TRUE)) &&
        (pRstPortInfo->bSync == MST_FALSE))
    {
        if ((pRstPortInfo->bLearn == MST_FALSE)
            && (pPerStPortInfo->bRootInconsistent != MST_TRUE))
        {
            if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) !=
                MST_SUCCESS)
            {

                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeLearn function returned FAILURE for inst %d\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
        else if ((pRstPortInfo->bLearn == MST_TRUE) &&
                 (pRstPortInfo->bForward == MST_FALSE))
        {
            if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                MST_SUCCESS)
            {

                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: MakeForward function returned FAILURE for inst %d\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

    /* Triggering the Root Port if RrWhile Timer is not running
     * for any other ports present
     * */
    if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
    {
        i4ReRooted =
            MstPRoleTrSmIsReRooted (pPerStBrgInfo->u2RootPort, u2MstInst);
        if (i4ReRooted == MST_SUCCESS)
        {

            pRootPortInfo = AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                                     u2MstInst);

            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_REROOTED_SET,
                                           pRootPortInfo,
                                           u2MstInst) != MST_SUCCESS)
            {

                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_PORT for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmRbWhileExpRootPort             */
/*                                                                           */
/*    Description               : This function handles the event of Recent  */
/*                                Backup While timer expiry in Active Port   */
/*                                state.                                     */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmRbWhileExpRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG (AST_RTSM_DBG,
                 "RTSM: Selected,Update Info is not Valid - EXITING.\n");
        return MST_SUCCESS;
    }
    /* Since Rapid Transition is Possible only if the version is >= 2 
     * check for it.
     */
    if ((AST_CURR_CONTEXT_INFO ())->u1ForceVersion >= AST_VERSION_2)
    {
        if (MstPRoleTrSmIsReRooted (u2PortNum, u2MstInst) == MST_SUCCESS)
        {
            if (pRstPortInfo->bLearn == MST_FALSE)
            {
                if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {

                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: MakeLearn function returned FAILURE for inst %d\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }

            if (pRstPortInfo->bForward == MST_FALSE)
            {
                if ((pRstPortEntry->bLoopGuard == RST_TRUE) &&
                    (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                    && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
                    && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
                    (MstIsPortMemberOfInst
                     ((INT4)
                      AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
                      (INT4) u2MstInst) == MST_TRUE))
                {
                    /* LoopInconsitent state is set when the port transtions
                     * to Designated/Discarding because of loop-guard feature*/
                    pRstPortEntry->bLoopInconsistent = RST_TRUE;
                    pPerStPortInfo->bLoopIncStatus = RST_TRUE;
                    UtlGetTimeStr (au1TimeStr);
                    pRstPortInfo->bLearn = RST_FALSE;
                    pRstPortInfo->bForward = RST_FALSE;
                    AstLoopIncStateChangeTrap ((UINT2)
                                               AST_GET_IFINDEX (pPerStPortInfo->
                                                                u2PortNo),
                                               pPerStPortInfo->bLoopIncStatus,
                                               u2MstInst,
                                               (INT1 *) AST_MST_TRAPS_OID,
                                               AST_MST_TRAPS_OID_LEN);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo), u2MstInst,
                                      au1TimeStr);

                    if (RstPortStateTrMachine
                        (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }

                }
                else
                {

                    if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                        MST_SUCCESS)
                    {

                        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: MakeForward function returned FAILURE for inst %d\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
            }
            /* If this Root Port has set ReRoot, then revert back 
             * Not Checking the condn. for forwarding as the above call
             * would have moed the port to forwarding */
            if ((pRstPortInfo->bReRoot == MST_TRUE))
            {
                pRstPortInfo->bReRoot = MST_FALSE;
            }
        }
    }
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT %d\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmOperEdgeSetDesigOrMasterPort             */
/*                                                                           */
/* Description        : This routine is called whenever the operEdge variable*/
/*                      is set to TRUE for any port on the bridge            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmOperEdgeSetDesigOrMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                          UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
    {
        /* Try all conditions out of the DESIGNATED_PORT state */
        if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE,
                                              u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesigPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }
    else if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)
    {
        /* Try all conditions out of the MASTER_PORT state */
        if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE,
                                               u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesigPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmMakeInitPort                   */
/*                                                                           */
/*    Description               : This function makes the state of the State */
/*                                machine to INIT PORT.                      */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleTrSmMakeInitPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
#else
INT4
MstPRoleTrSmMakeInitPort (pPerStPortInfo, u2MstInst)
     tAstPerStPortInfo  *pPerStPortInfo;
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole =
        MST_PORT_ROLE_DISABLED;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Port Role Made as -> DISABLED for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->bLearn = MST_FALSE;
    pRstPortInfo->bForward = MST_FALSE;

    pRstPortInfo->bSynced = MST_FALSE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bSync = MST_TRUE;
    pRstPortInfo->bReRoot = MST_TRUE;
    pRstPortInfo->bReSelect = MST_TRUE;

    pRstPortInfo->bAgree = MST_FALSE;

    pPerStPortInfo->u1ProleTrSmState = MST_PROLETRSM_STATE_INIT_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state INIT_PORT for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return (MstPRoleTrSmMakeDisablePort (pPerStPortInfo, u2MstInst));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmMakeDisablePort                */
/*                                                                           */
/*    Description               : This function make the State Machine to    */
/*                                DISABLE PORT State.                        */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeDisablePort (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = pPerStPortInfo->u1SelectedPortRole;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);
    pRstPortInfo->bLearn = MST_FALSE;
    pRstPortInfo->bForward = MST_FALSE;

    pPerStPortInfo->u1ProleTrSmState = MST_PROLETRSM_STATE_DISABLE_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DISABLE_PORT for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo, u2MstInst) != RST_SUCCESS)
    {

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %d: Port State Transition Machine returned FAILURE  for LEARN_FWD_DISABLED event!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %d: Port State Transition Machine returned FAILURE for LEARN_FWD_DISABLED event !\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if (MstTopologyChMachine (MST_TOPOCHSM_EV_NOT_DESG_ROOT, u2MstInst,
                              pPerStPortInfo) != MST_SUCCESS)
    {

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %d: Topology Change Machine returned FAILURE  for NOT_DESGPORT event!!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %d: Topology Change Machine returned FAILURE  for NOT_DESGPORT event!!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if ((pRstPortInfo->bLearning == RST_FALSE) &&
        (pRstPortInfo->bForwarding == RST_FALSE))
    {
        return (MstPRoleTrSmMakeDisabledPort (pPerStPortInfo, u2MstInst));
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmMakeDisabledPort               */
/*                                                                           */
/*    Description               : This function sets the State Machine       */
/*                                to DISABLED PORT State.                    */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeDisabledPort (tAstPerStPortInfo * pPerStPortInfo,
                              UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstBoolean         bAlreadySynced = MST_FALSE;
    tAstBoolean         bAllSynced = MST_FALSE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4ReRooted = MST_FAILURE;
    tAstPerStPortInfo  *pRootPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    bAlreadySynced = pRstPortInfo->bSynced;

    pRstPortInfo->bSynced = MST_TRUE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSYNCED_UPDATE);

    /* if FwdWhile Timer is running, then stop the timer
     * It should be run, when this port comes out of the 
     * DISABLED state
     * */
    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_FDWHILE) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Inst %d: Stop Timer for FdWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    pRstPortInfo->bSync = MST_FALSE;
    pRstPortInfo->bReRoot = MST_FALSE;
    pPerStPortInfo->u1ProleTrSmState = MST_PROLETRSM_STATE_DISABLED_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DISABLED_PORT for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    /* Stop the rrWhile timer */
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_RRWHILE) != RST_SUCCESS)
        {

            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Inst %d: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

        /* Triggering the Root Port if RrWhile Timer is not running
         * for any other ports present
         * */
        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {
            pRootPortInfo = AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                                     u2MstInst);

            if (pRootPortInfo->u1SelectedPortRole == pRootPortInfo->u1PortRole)
            {
                i4ReRooted =
                    MstPRoleTrSmIsReRooted (pPerStBrgInfo->u2RootPort,
                                            u2MstInst);

                if (i4ReRooted == MST_SUCCESS)
                {
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_REROOTED_SET, pRootPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {

                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }

    /* synced is set for this port
     * If this operation results in all synced, then trigger using
     * SYNCED_SET event
     * */
    if (bAlreadySynced == MST_FALSE)
    {
        bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
        if (bAllSynced == MST_TRUE)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %d: Triggering ALLSYNCED set event for all active ports\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            /* Trigger all Root/Alt/Backup && MASTER  ports except this port 
             * with AllSynced set event. AllSynced for this port will be
             * handled below. 
             * */
            if (MstProleTrSmIndicateAllSyncedSet (u2PortNum, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmMakeAlternatePort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ALTERNATE_PORT.               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeAlternatePort (tAstPerStPortInfo * pPerStPortInfo,
                               UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstBoolean         bAlreadySynced = MST_FALSE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    INT4                i4ReRooted = MST_FAILURE;
    tAstPerStPortInfo  *pRootPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    u2PortNum = pPerStPortInfo->u2PortNo;
    bAlreadySynced = pRstPortInfo->bSynced;
    pRstPortInfo->bSynced = MST_TRUE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bSync = MST_FALSE;
    pRstPortInfo->bReRoot = MST_FALSE;

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_AlternatePort: Port %s: Inst %u: rrWhile = 0 Synced = TRUE Sync = ReRoot = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    /* If Recent Root While timer is running stop it */
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, (UINT1) AST_TMR_TYPE_RRWHILE)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Inst %u: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

        /* Triggering the Root Port if RrWhile Timer is not running
         * for any other ports present
         * */
        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {
            pRootPortInfo = AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                                     u2MstInst);

            if (pRootPortInfo->u1SelectedPortRole == pRootPortInfo->u1PortRole)
            {
                i4ReRooted =
                    MstPRoleTrSmIsReRooted (pPerStBrgInfo->u2RootPort,
                                            u2MstInst);

                if (i4ReRooted == MST_SUCCESS)
                {
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_REROOTED_SET, pRootPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {

                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }
    pPerStPortInfo->u1ProleTrSmState =
        (UINT1) MST_PROLETRSM_STATE_ALTERNATE_PORT;

    /* If this port was already synced then don't bother with allSynced */
    if (bAlreadySynced == MST_FALSE)
    {
        if ((MstIsAllOtherPortsSynced (u2PortNum, u2MstInst)) == MST_TRUE)
        {
            /* Trigger all Root/Alt/Backup && MASTER  ports except this port 
             * with AllSynced set event. AllSynced for this port will be
             * handled below. 
             * */
            if (MstProleTrSmIndicateAllSyncedSet (u2PortNum, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (MstProleTrSmAltPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: AltPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }
    AST_SET_CHANGED_FLAG (u2MstInst, pPerStPortInfo->u2PortNo) = MST_TRUE;
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmAltPortTransitions                       */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state ALTERNATE_PORT and executes those that         */
/*                      evaluate to TRUE                                     */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst     - InstanceInfo                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmAltPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                tAstBoolean bAllSyncedEvent, UINT2 u2MstInst)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = MST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* When this function is called to handle the AllSynced set event,
     * the parameter bAllSyncedEvent will be set to TRUE. Hence allSynced 
     * can be considered true even without calling IsTreeAllSynced () */
    if (bAllSyncedEvent == MST_TRUE)
    {
        bAllSynced = MST_TRUE;
    }
    /* Otherwise calculate allSynced only if Agree is false */
    else if (pRstPortInfo->bAgree == MST_FALSE)
    {
        bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
    }

    /* Condn. check for Alt_Proposed State */
    if ((pRstPortInfo->bProposed == MST_TRUE) &&
        (pRstPortInfo->bAgree == MST_FALSE))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state ALTERNATE_PROPOSED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        /* If AllSynced is TRUE directly move to the ALTERNATE_AGREED state */
        if (bAllSynced == MST_FALSE)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: Moved to state ALTERNATE_PROPOSED \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_AlternateProposed: Sync = TRUE for ALL ports Inst %u\n",
                          u2MstInst);

            pRstPortInfo->bProposed = MST_FALSE;

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "RTSM_AlternateProposed: Port %s: Inst %u: Proposed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (MstPRoleTrSmSetSyncTree (pPerStPortInfo->u2PortNo, u2MstInst)
                != MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: SetSyncTree returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            /* Recompute allSynced now that sync has been processed by all 
             * ports*/
            bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
        }
    }

    /* Condition check for Alt_Agreed state */

    if (((pRstPortInfo->bProposed == MST_TRUE) &&
         (pRstPortInfo->bAgree == MST_TRUE))
        || ((pRstPortInfo->bAgree == MST_FALSE) && (bAllSynced == MST_TRUE)))
    {
        pRstPortInfo->bProposed = MST_FALSE;
        pRstPortInfo->bAgree = MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) = MST_TRUE;
        }
        else
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) = MST_TRUE;
        }

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_AlternateAgreed: Port %s: Inst %u: Proposed = FALSE Agree = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state ALTERNATE_AGREED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, u2MstInst) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return RST_FAILURE;
        }
    }

    /* Condn. check for Alt_Port State */

    if ((pRstPortInfo->bSynced == MST_FALSE) ||
        (pRstPortInfo->bSync == MST_TRUE) ||
        (pRstPortInfo->bReRoot == MST_TRUE) ||
        (pRstPortInfo->pRrWhileTmr != NULL))
    {
        pRstPortInfo->bSynced = MST_TRUE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        pRstPortInfo->bSync = MST_FALSE;
        pRstPortInfo->bReRoot = MST_FALSE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_AlternatePort: Port %s: Inst %u: rrWhile = 0 Synced = TRUE Sync = ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        /* If Recent Root While timer is running stop it */
        if (pRstPortInfo->pRrWhileTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 (UINT1) AST_TMR_TYPE_RRWHILE) != RST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %u: Stop Timer for RrWhileTmr Failed !!!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    /* LoopInconsitent state is reset when the port transtions  
     * from Designated/Discarding to ALTERNATE/DISCARDING state.Before resetting
     * check whether the Loop Inc state is set for that port. */
    if ((pPortInfo->bLoopGuard == MST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
        && (pPortInfo->bOperPointToPoint == RST_TRUE) &&
        ((pPortInfo->bLoopInconsistent == MST_TRUE) ||
         (pPerStPortInfo->bLoopIncStatus == MST_TRUE)))
    {
        pPortInfo->bLoopInconsistent = MST_FALSE;
        pPerStPortInfo->bLoopIncStatus = MST_FALSE;
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst, au1TimeStr);
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state ALTERNATE_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmMakeRootPort                             */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state to ROOT_PORT.                    */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst     - Instance Info                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmMakeRootPort (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    u2PortNum = pPerStPortInfo->u2PortNo;

    pPerStPortInfo->u1OldPortRole = pPerStPortInfo->u1PortRole;
    pPerStPortInfo->u1PortRole = (UINT1) AST_PORT_ROLE_ROOT;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_RootPort: Port %s: Inst %u: Role = ROOT\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                  "RTSM: Port %s: Inst %u: Port Role Made as -> ROOT\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    pPerStPortInfo->u1ProleTrSmState = (UINT1) RST_PROLETRSM_STATE_ROOT_PORT;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    /* Try all conditions out of the ROOT_PORT state */
    if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    AST_SET_CHANGED_FLAG (u2MstInst, pPerStPortInfo->u2PortNo) = MST_TRUE;
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state ROOT_PORT\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmSyncSetRootPort                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Sync variable is set in */
/*                      the ROOT_PORT state.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst - Instance Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmSyncSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                             UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ROOT_PORT state */
    if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_FALSE,
                                         u2MstInst) != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s :Inst %u : Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmRootPortTransitions                      */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state ROOT_PORT and executes those that evaluate     */
/*                      TRUE                                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst - Instance Info                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmRootPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                 tAstBoolean bAllSyncedEvent, UINT2 u2MstInst)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = MST_FALSE;
    tAstBoolean         bAlreadySynced = MST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    INT4                i4ReRooted = AST_INIT_VAL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    /* When this function is called to handle the AllSynced set event,
     * the parameter bAllSyncedEvent will be set to TRUE. Hence allSynced 
     * can be considered true even without calling IsTreeAllSynced () */
    if (bAllSyncedEvent == MST_TRUE)
    {
        bAllSynced = MST_TRUE;
    }

    /* Otherwise calculate allSynced only if Agree is false */
    else if (pRstPortInfo->bAgree == MST_FALSE)
    {
        bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
    }

    /* If Agree is TRUE directly move to the ROOT_AGREED state */

    /* Checking Conditions for ROOT_PROPOSED State */
    if ((pRstPortInfo->bProposed == MST_TRUE) &&
        (pRstPortInfo->bAgree == MST_FALSE))
    {
        /* If AllSynced is TRUE directly move to the ROOT_AGREED state */
        if (bAllSynced == MST_FALSE)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: Moved to state ROOT_PROPOSED \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            AST_DBG (AST_SM_VAR_DBG,
                     "RTSM_RootProposed: Sync = TRUE for ALL ports\n");

            pRstPortInfo->bProposed = MST_FALSE;
            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "RTSM_RootProposed: Port %s: Inst %u: Proposed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (MstPRoleTrSmSetSyncTree (pPerStPortInfo->u2PortNo, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: SetSyncTree returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            /* Recompute allSynced now that sync has been processed by all 
             * ports*/
            bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
        }
    }

    /* Checking Conditions for ROOT_AGREED state */
    if (((pRstPortInfo->bProposed == MST_TRUE) &&
         (pRstPortInfo->bAgree == MST_TRUE))
        || ((pRstPortInfo->bAgree == MST_FALSE) && (bAllSynced == MST_TRUE)))
    {
        pRstPortInfo->bProposed = MST_FALSE;
        pRstPortInfo->bSync = MST_FALSE;
        pRstPortInfo->bAgree = MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) = MST_TRUE;
        }
        else
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) = MST_TRUE;
        }
        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_RootAgreed: Port %s: Inst %u: Proposed = Sync = FALSE Agree = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state ROOT_AGREED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        /* TXSM will be triggered after the the transition to ROOT_FORWARDING
         * happens because the bpdu will then have the forwarding flag and the 
         * topology change flag set. */
    }

    if (pRstPortInfo->bForward == MST_FALSE)
    {
        i4ReRooted = MstPRoleTrSmIsReRooted (u2PortNum, u2MstInst);

        /* If already ReRooted directly move to the REROOTED state */

        /* Checking conditions for REROOT state */
        if ((i4ReRooted != MST_SUCCESS) && (pRstPortInfo->bReRoot == MST_FALSE))
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: Moved to state REROOT \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (MstPRoleTrSmSetRerootTree (u2MstInst) != MST_SUCCESS)
            {
                return MST_FAILURE;
            }

            i4ReRooted = MstPRoleTrSmIsReRooted (u2PortNum, u2MstInst);
        }

        if (i4ReRooted == MST_SUCCESS)
        {
            MstPRoleTrSmRerootedSetRootPort (pPerStPortInfo, u2MstInst);
        }

    }
    else if (pRstPortInfo->bReRoot == MST_TRUE)
    {
        pRstPortInfo->bReRoot = MST_FALSE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_Rerooted: Port %s: Inst %u: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state REROOTED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }

    if (((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) == MST_TRUE) ||
        ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) == MST_TRUE))
    {
        /* Following check added to handle the scenario when Force-Version is 
         * set to STP and the newInfo is set for CIST, Transmit State machine
         * to be invoked with the newInfo set event, so as to decide on
         * transmitting Config BPDU or TCN BPDU.
         * This is as per IEEE 802.1q 2005 standard, Clause 13.31 Port Transmit 
         * State machine Conditions for TRANSMIT_CONFIG & TRANSMIT_TCN on the 
         * basis of newInfo - Figure 13-21
         */
        if (AST_FORCE_VERSION >= AST_VERSION_2)
        {
            if (pRstPortInfo->bAgree == MST_TRUE)
            {
                if ((pRstPortInfo->bProposing != MST_TRUE) &&
                    (AST_FORCE_VERSION >= AST_VERSION_2) &&
                    (pAstCommPortInfo->pHelloWhenTmr != NULL))
                {

                    if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                                pPortInfo,
                                                u2MstInst) != MST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo));
                        return RST_FAILURE;
                    }
                }
            }
            else
            {
                if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                            pPortInfo,
                                            u2MstInst) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo));
                    return RST_FAILURE;
                }
            }

        }
        else if ((AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) ==
                 MST_TRUE)
        {
            if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                        pPortInfo, u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
    }
    /* Checking conditions for ROOT_SYNCED state */

    if (((pRstPortInfo->bAgreed == MST_TRUE) &&
         (pRstPortInfo->bSynced == MST_FALSE)) ||
        ((pRstPortInfo->bSync == MST_TRUE) &&
         (pRstPortInfo->bSynced == MST_TRUE)))
    {
        bAlreadySynced = pRstPortInfo->bSynced;

        pRstPortInfo->bSynced = MST_TRUE;
        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSYNCED_UPDATE);
        pRstPortInfo->bSync = MST_FALSE;

        /* synced is set for this port
         * If this operation results in all synced, then trigger using
         * SYNCED_SET event
         * */
        if (bAlreadySynced == MST_FALSE)
        {
            bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
            if (bAllSynced == MST_TRUE)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: Inst %d: Triggering ALLSYNCED set event for all active ports\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                if (MstProleTrSmIndicateAllSyncedSet (u2PortNum, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmMasterPortTransitions                     */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state DESG_PORT and executes those that evaluate to  */
/*                      TRUE                                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmMasterPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                   tAstBoolean bAllSyncedEvent, UINT2 u2MstInst)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = MST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Checking all the conditions for transitioning to
     * MASTER_DISCARD state
     * */
    if ((((pRstPortInfo->bSync == MST_TRUE) &&
          (pRstPortInfo->bSynced == MST_FALSE))
         ||
         ((pRstPortInfo->bReRoot == MST_TRUE) &&
          (pRstPortInfo->pRrWhileTmr != NULL))
         ||
         (pRstPortInfo->bDisputed == MST_TRUE))
        &&
        (pPortInfo->bOperEdgePort == MST_FALSE)
        &&
        ((pRstPortInfo->bLearn == MST_TRUE) ||
         (pRstPortInfo->bForward == MST_TRUE)))
    {
        if (MstProleTrSmMakeDesignatedorMasterDiscard (pPerStPortInfo,
                                                       u2MstInst)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeDiscard function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * MASTER_SYNCED state
     * */
    if (((((pRstPortInfo->bLearning == MST_FALSE) &&
           (pRstPortInfo->bForwarding == MST_FALSE)) ||
          (pRstPortInfo->bAgreed == MST_TRUE) ||
          (pPortInfo->bOperEdgePort == MST_TRUE))
         &&
         (pRstPortInfo->bSynced == MST_FALSE))
        ||
        ((pRstPortInfo->bSync == MST_TRUE) &&
         (pRstPortInfo->bSynced == MST_TRUE)))
    {
        if (MstProleTrSmMakeDesignatedorMasterSynced (pPerStPortInfo, u2MstInst)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeSynced function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * MASTER_RETIRED state
     * */
    if ((pRstPortInfo->pRrWhileTmr == NULL) &&
        (pRstPortInfo->bReRoot == MST_TRUE))
    {
        pRstPortInfo->bReRoot = MST_FALSE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_DesgRetired: Port %s: Inst %u: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state MASTER_RETIRED \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }

    /* Checking all the conditions for transitioning to
     * MASTER_LEARN or MASTER_FORWARD states
     * */
    if (bAllSyncedEvent == MST_TRUE)
    {
        /* AllSynced has been set, no need to calculate it seperately
         * */
        bAllSynced = MST_TRUE;
    }
    else
    {
        bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
    }

    if ((pRstPortInfo->pFdWhileTmr == NULL) || (bAllSynced == MST_TRUE))
    {
        if (MstProleTrSmMakeDesignatedorMasterLearnorFwd (pPerStPortInfo,
                                                          u2MstInst)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeLearnorFwd function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * MASTER_PROPOSED state
     * */
    if ((pRstPortInfo->bProposed == MST_TRUE) &&
        (pRstPortInfo->bAgree == MST_FALSE))
    {
        /* If AllSynced is TRUE directly move to the ROOT_AGREED state */
        if (bAllSynced == MST_FALSE)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %u: Moved to state ROOT_PROPOSED \n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            AST_DBG (AST_SM_VAR_DBG,
                     "RTSM_RootProposed: Sync = TRUE for ALL ports\n");
            pRstPortInfo->bProposed = MST_FALSE;

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "RTSM_RootProposed: Port %s: Inst %u: Proposed = FALSE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (MstPRoleTrSmSetSyncTree (pPerStPortInfo->u2PortNo, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: SetSyncTree returned FAILURE !!! \n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }

            /* Recompute allSynced now that sync has been processed by all 
             * ports*/
            bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
        }
    }

    /* Checking all the conditions for transitioning to
     * MASTER_AGREED state
     * */
    if (((pRstPortInfo->bProposed == MST_TRUE) &&
         (pRstPortInfo->bAgree == MST_TRUE))
        || ((pRstPortInfo->bAgree == MST_FALSE) && (bAllSynced == MST_TRUE)))
    {
        pRstPortInfo->bProposed = MST_FALSE;
        pRstPortInfo->bSync = MST_FALSE;
        pRstPortInfo->bAgree = MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) = MST_TRUE;
        }
        else
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) = MST_TRUE;
        }

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_RootAgreed: Port %s: Inst %u: Proposed = Sync = FALSE Agree = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Moved to state ROOT_AGREED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        /* TXSM will be triggered after the the transition to ROOT_FORWARDING
         * happens because the bpdu will then have the forwarding flag and the 
         * topology change flag set. */
    }
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state MASTER_PORT \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmDesigPortTransitions                     */
/*                                                                           */
/* Description        : This routine checks all conditions going out from    */
/*                      state DESG_PORT and executes those that evaluate to  */
/*                      TRUE                                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      bAllSyncedEvent - Event Indicating AllSynced status  */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmDesigPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                  tAstBoolean bAllSyncedEvent, UINT2 u2MstInst)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAllSynced = MST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT2               u2Duration = AST_INIT_VAL;
    UINT2               u2InstIndexFound = 0;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    u2PortNum = pPerStPortInfo->u2PortNo;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Optimization done to reduce the convergence time 
     * when Bridge priroity is changed. This is effective when a port
     * transitions from Alternate/Discarding to Designated/Discarding 
     * state on further reception of inferior messages will clear
     * the Dispute Flag.*/

    if ((pRstPortInfo->bDisputed == MST_TRUE) &&
        ((pRstPortInfo->bLearn == MST_FALSE) ||
         (pRstPortInfo->bForward == MST_FALSE)))
    {
        if (MstProleTrSmMakeDesignatedorMasterDiscard
            (pPerStPortInfo, u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeDiscard function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }

    }
    /* Checking all the conditions for transitioning to
     * DESIGNATED_DISCARD state
     * */
    if ((((pRstPortInfo->bSync == MST_TRUE) &&
          (pRstPortInfo->bSynced == MST_FALSE))
         ||
         ((pRstPortInfo->bReRoot == MST_TRUE) &&
          (pRstPortInfo->pRrWhileTmr != NULL))
         ||
         (pRstPortInfo->bDisputed == MST_TRUE))
        &&
        (pPortInfo->bOperEdgePort == MST_FALSE)
        &&
        ((pRstPortInfo->bLearn == MST_TRUE) ||
         (pRstPortInfo->bForward == MST_TRUE)))
    {
        if (MstProleTrSmMakeDesignatedorMasterDiscard
            (pPerStPortInfo, u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeDiscard function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * DESIGNATED_SYNCED state
     * */
    if (((((pRstPortInfo->bLearning == MST_FALSE) &&
           (pRstPortInfo->bForwarding == MST_FALSE)) ||
          (pRstPortInfo->bAgreed == MST_TRUE) ||
          (pPortInfo->bOperEdgePort == MST_TRUE))
         &&
         (pRstPortInfo->bSynced == MST_FALSE))
        ||
        ((pRstPortInfo->bSync == MST_TRUE) &&
         (pRstPortInfo->bSynced == MST_TRUE)))
    {
        if (MstProleTrSmMakeDesignatedorMasterSynced (pPerStPortInfo, u2MstInst)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeSynced function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * DESIG_RETIRED state
     * */
    if ((pRstPortInfo->pRrWhileTmr == NULL) &&
        (pRstPortInfo->bReRoot == MST_TRUE))
    {
        pRstPortInfo->bReRoot = MST_FALSE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_DesgRetired: Port %s: Inst %u: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state MASTER_RETIRED \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }

    /* Checking all the conditions for transitioning to
     * DESIGNATED_LEARN or DESIGNATED_FORWARD states
     * */
    if (((pRstPortInfo->pFdWhileTmr == NULL) ||
         (pRstPortInfo->bAgreed == MST_TRUE) ||
         (pPortInfo->bOperEdgePort == MST_TRUE))
        &&
        ((pRstPortInfo->bReRoot == MST_FALSE) ||
         (pRstPortInfo->pRrWhileTmr == NULL))
        && (pRstPortInfo->bSync == MST_FALSE))
    {
        u2InstIndexFound = 1;
        if (MstProleTrSmMakeDesignatedorMasterLearnorFwd (pPerStPortInfo,
                                                          u2MstInst)
            != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeLearnorFwd function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * DESIGNATED_PROPOSED state
     * */
    if ((pRstPortInfo->bForward == MST_FALSE) &&
        (pRstPortInfo->bAgreed == MST_FALSE) &&
        (pRstPortInfo->bProposing == MST_FALSE) &&
        (pPortInfo->bOperEdgePort == MST_FALSE))
    {
        pRstPortInfo->bProposing = MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            /* Start the EdgeDelayWhileTimer for the CIST */
            if (pPortInfo->bOperPointToPoint == MST_TRUE)
            {
                u2Duration =
                    (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
            }
            else
            {
                u2Duration = (UINT2) pPortInfo->DesgTimes.u2MaxAge;
            }

            if (AstStartTimer ((VOID *) pPortInfo, u2MstInst,
                               (UINT1) AST_TMR_TYPE_EDGEDELAYWHILE,
                               u2Duration) != MST_SUCCESS)
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
                AST_DBG (AST_RTSM_DBG | AST_BDSM_DBG | AST_TMR_DBG |
                         AST_ALL_FAILURE_DBG,
                         "MSG: AstStartTimer for EdgeDelayWhileTmr FAILED!\n");
                return MST_FAILURE;
            }

            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) = MST_TRUE;
        }
        else
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) = MST_TRUE;
        }

        /* Need not trigger BridgeDetectionMachine with ProposingSet 
         * event since EdgeDelayWhile timer has just been started */

        AST_DBG_ARG3 (AST_SM_VAR_DBG,
                      "RTSM_DesgPropose: Port %s: Inst %u: edgeDelayWhile = %u Proposing = NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2Duration, u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state DESIGNATED_PROPOSE \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }

    /* Checking all the conditions for transitioning to
     * DESIGNATED_AGREED state
     * */
    if (bAllSyncedEvent == MST_TRUE)
    {
        bAllSynced = MST_TRUE;
    }
    else
    {
        bAllSynced = MstIsAllOtherPortsSynced (u2PortNum, u2MstInst);
    }

    if ((bAllSynced == MST_TRUE) &&
        ((pRstPortInfo->bAgree == MST_FALSE) ||
         (pRstPortInfo->bProposed == MST_TRUE)))
    {
        pRstPortInfo->bProposed = MST_FALSE;
        pRstPortInfo->bSync = MST_FALSE;
        pRstPortInfo->bAgree = MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfo) = MST_TRUE;
        }
        else
        {
            (AST_GET_CIST_MSTI_PORT_INFO (u2PortNum)->bNewInfoMsti) = MST_TRUE;
        }

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET, pPortInfo,
                                    u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_TXSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Port Transmit machine returned FAILURE !!! \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }
    if (u2InstIndexFound == 0)
    {
        if ((pPerStPortInfo->bLoopGuardStatus == AST_TRUE) &&
            (pRstPortInfo->bForward == MST_TRUE) &&
            (pPortInfo->bOperPointToPoint == MST_TRUE))

        {
            pPerStPortInfo->bLoopIncStatus = RST_TRUE;
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            if (MstIsPortMemberOfInst
                ((INT4) AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo),
                 (INT4) u2MstInst) != MST_FALSE)
            {
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2MstInst,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
            }
            if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                       pPerStPortInfo,
                                       u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
    }

    if ((pRstPortInfo->bLearn == MST_TRUE) &&
        (pRstPortInfo->bForward == MST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == AST_TRUE)
        && (pRstPortInfo->pRcvdInfoTmr != NULL))
    {
        pPerStPortInfo->bLoopGuardStatus = AST_FALSE;
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmSyncSetDesigPort                          */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Sync variable is set in */
/*                      the DesignatedPort state.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmSyncSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                              UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state DESIGNATED_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmSyncSetMasterPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Sync variable is set in */
/*                      the Master Port state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmSyncSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                               UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the MASTER_PORT state */
    if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state MASTER_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmMakeDesignatedorMasterSynced             */
/*                                                                           */
/* Description        : This routine performs the action for the state       */
/*                      machine state DESG_SYNCED.                           */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Information                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmMakeDesignatedorMasterSynced (tAstPerStPortInfo * pPerStPortInfo,
                                          UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bAlreadySynced = RST_FALSE;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pRootPortInfo = NULL;
    INT4                i4ReRooted;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    bAlreadySynced = pRstPortInfo->bSynced;
    pRstPortInfo->bSynced = MST_TRUE;
    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo, MST_SYNC_BSYNCED_UPDATE);
    pRstPortInfo->bSync = MST_FALSE;

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "RTSM_DesgSynced: Port %s: Inst %u: rrWhile = 0 Synced = TRUE Sync = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state DESIGNATED_SYNCED \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    /***************************************/
    /* Stop rrWhile Timer if it is running */
    /***************************************/
    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPerStPortInfo,
             (UINT1) AST_TMR_TYPE_RRWHILE) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Inst %u: Stop Timer for RrWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

        /* Triggering the Root Port if RrWhile Timer is not running
         * for any other ports present
         * */
        if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
        {
            pRootPortInfo = AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                                     u2MstInst);

            if (pRootPortInfo->u1SelectedPortRole == pRootPortInfo->u1PortRole)
            {
                i4ReRooted =
                    MstPRoleTrSmIsReRooted (pPerStBrgInfo->u2RootPort,
                                            u2MstInst);

                if (i4ReRooted == MST_SUCCESS)
                {
                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_REROOTED_SET, pRootPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }

    /* If this port was already synced then don't bother with allSynced */
    if (bAlreadySynced == MST_FALSE)
    {
        if ((MstIsAllOtherPortsSynced (u2PortNum, u2MstInst)) == MST_TRUE)
        {
            if (MstProleTrSmIndicateAllSyncedSet (u2PortNum, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmMakeDesignatedorMasterLearnorFwd         */
/*                                                                           */
/* Description        : This routine performs the action for the state       */
/*                      machine state DESIG_LEARN or DESIG_FWD.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Information                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmMakeDesignatedorMasterLearnorFwd (tAstPerStPortInfo *
                                              pPerStPortInfo, UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    u2PortNum = pPerStPortInfo->u2PortNo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pPerStPortInfo->bRootInconsistent != MST_TRUE)
    {
        if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE) &&
             (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
             && (pCommPortInfo->pEdgeDelayWhileTmr == NULL))
            || ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
                && (pPerStPortInfo->bLoopGuardStatus == RST_TRUE)
                &&
                (MstIsPortMemberOfInst
                 ((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
                  u2MstInst) == MST_TRUE)
                && (pCommPortInfo->pEdgeDelayWhileTmr == NULL)))
        {
            /* LoopInconsitent state is set when the port transtions
             * to Designated/Discarding because of loop-guard feature*/
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            if (pPerStPortInfo->bLoopIncStatus != RST_TRUE)
            {
                pRstPortEntry->bLoopInconsistent = RST_TRUE;
                pPerStPortInfo->bLoopIncStatus = RST_TRUE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);
                AstLoopIncStateChangeTrap ((UINT2)
                                           AST_GET_IFINDEX (pPerStPortInfo->
                                                            u2PortNo),
                                           pPerStPortInfo->bLoopIncStatus,
                                           u2MstInst,
                                           (INT1 *) AST_MST_TRAPS_OID,
                                           AST_MST_TRAPS_OID_LEN);
            }
            if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                                       pPerStPortInfo,
                                       u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }
        }
        else if (pRstPortInfo->bLearn == MST_FALSE)
        {
            if (((pRstPortEntry->bOperEdgePort == MST_TRUE)
                 && (pPerStPortInfo->bLoopIncStatus != RST_TRUE))
                || (pRstPortEntry->bOperEdgePort == MST_FALSE))
            {
                if ((pRstPortEntry->bLoopGuard == MST_TRUE) &&
                    (pRstPortEntry->bOperPointToPoint == RST_TRUE))
                {
                    if (MstIsPortMemberOfInst
                        ((INT4)
                         AST_GET_IFINDEX ((INT4) pPerStPortInfo->u2PortNo),
                         (INT4) u2MstInst) != MST_FALSE)
                    {
                        pRstPortEntry->bLoopInconsistent = MST_FALSE;
                        pPerStPortInfo->bLoopIncStatus = MST_FALSE;
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                                          AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                               u2PortNo),
                                          u2MstInst, au1TimeStr);
                    }
                }

                if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %u: MakeLearn function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
    }

    if ((pRstPortInfo->bForward == MST_FALSE) &&
        (pRstPortInfo->bLearn == MST_TRUE))
    {
        if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
             && (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
             (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
             (pCommPortInfo->pEdgeDelayWhileTmr == NULL)) ||
            ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
             && (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
             (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
             (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
             (MstIsPortMemberOfInst
              ((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
               (INT4) u2MstInst) == MST_TRUE)))
        {
            /* LoopInconsitent state is set when the port transtions
             * to Designated/Discarding because of loop-guard feature*/
            pRstPortEntry->bLoopInconsistent = RST_TRUE;
            pPerStPortInfo->bLoopIncStatus = RST_TRUE;

            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, au1TimeStr);
            AstLoopIncStateChangeTrap ((UINT2)
                                       AST_GET_IFINDEX (pPerStPortInfo->
                                                        u2PortNo),
                                       pPerStPortInfo->bLoopIncStatus,
                                       u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                                       AST_MST_TRAPS_OID_LEN);
            if (RstPortStateTrMachine
                (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                 u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

        }
        else
        {
            if (pPerStPortInfo->bLoopIncStatus != RST_TRUE)
            {

                if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %u: MakeForward function returned FAILURE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
                if ((pRstPortEntry->bLoopGuard == MST_TRUE) &&
                    (pRstPortEntry->bOperPointToPoint == RST_TRUE))
                {
                    if (MstIsPortMemberOfInst
                        ((INT4) AST_GET_IFINDEX (u2PortNum),
                         (INT4) u2MstInst) != MST_FALSE)
                    {
                        UtlGetTimeStr (au1TimeStr);
                        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                          "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                                          AST_GET_IFINDEX_STR (u2PortNum),
                                          u2MstInst, au1TimeStr);
                    }
                }
            }
        }

        pRstPortInfo->bAgreed =
            (AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp;
        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_Agreed: Port %s: Inst %u: Agreed = sendRSTP\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        if ((pRstPortInfo->bAgreed == MST_TRUE) &&
            (pRstPortInfo->bSynced == MST_FALSE))
        {
            /* Agreed is now set since the port has gone to 
             * forwarding */
            pRstPortInfo->bSynced = MST_TRUE;
            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                   MST_SYNC_BSYNCED_UPDATE);
            pRstPortInfo->bSync = MST_FALSE;

            AST_DBG_ARG2 (AST_SM_VAR_DBG,
                          "RTSM_DesgSynced: Port %s: Inst %u: Synced = TRUE Sync = FALSE\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            if ((MstIsAllOtherPortsSynced (u2PortNum, u2MstInst)) == MST_TRUE)
            {
                if (MstProleTrSmIndicateAllSyncedSet (u2PortNum, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %u: IndicateAllSyncedSet returned FAILURE!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
            }                    /* AllSynced */
        }                        /* Not Synced */
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmReRootSetDesigOrMasterPort     */
/*                                                                           */
/*    Description               : This function handles the Reroot Set Event */
/*                                for Designated and Master Ports.           */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmReRootSetDesigOrMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                        UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
    {
        /* Try all conditions out of the DESG_PORT state */
        if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE,
                                              u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    else if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)
    {
        /* Try all conditions out of the Master_PORT state */
        if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE,
                                               u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmProposedSetRootPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the RootPort state.                               */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmProposedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ROOT_PORT state */
    if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmProposedSetAlternatePort                 */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the AlternatePort state.                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmProposedSetAlternatePort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (MstProleTrSmAltPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: AltPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmProposedSetMasterPort                    */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when Proposed Variable is set    */
/*                      in the Master Port state.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmProposedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                   UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmAgreedSetDesigPort                       */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when agreed Variable is set      */
/*                      in the DesignatedPort state.                         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmAgreedSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the DESG_PORT state */
    if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmAgreedSetMasterPort                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when agreed Variable is set      */
/*                      in the Master Port State.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmAgreedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the MASTER_PORT state */
    if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmAgreedSetRootPort                        */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when agreed Variable is set      */
/*                      in the Root Port State.                              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmAgreedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                               UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the MASTER_PORT state */
    if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_FALSE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmAllSyncedSetRootPort                     */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. all the other ports are in sync */
/*                      with the current spanning tree information and the   */
/*                      Root Port may now signal an agreement to the         */
/*                      neighbouring Bridge.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                                                                           */
/*                      u2MstInst      - Instance Info                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmAllSyncedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                  UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                u2MstInst);

    if ((MST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ROOT_PORT state */
    if (MstPRoleTrSmRootPortTransitions (pPerStPortInfo, MST_TRUE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: RootPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmAllSyncedSetDesigPort                    */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Root Port state i.e. all the other ports are in sync */
/*                      with the current spanning tree information and the   */
/*                      Root Port may now signal an agreement to the         */
/*                      neighbouring Bridge.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                                                                           */
/*                      u2MstInst      - Instance Info                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmAllSyncedSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                   UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                u2MstInst);

    if ((MST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the DESIG_PORT state */
    if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_TRUE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: DesigPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmAllSyncedSetAltPort                      */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Alt Port state i.e. all the other ports are in sync  */
/*                      with the current spanning tree information and the   */
/*                      Alt  Port may now signal an agreement to the         */
/*                      neighbouring Bridge.                                 */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                                                                           */
/*                      u2MstInst      - Instance Info                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmAllSyncedSetAltPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                u2MstInst);

    if ((MST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (MstProleTrSmAltPortTransitions (pPerStPortInfo, MST_TRUE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: AlternatePortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmAllSyncedSetMasterPort                   */
/*                                                                           */
/* Description        : This routine is called whenever the AllSynced event  */
/*                      triggered when the Role Transition machine is in the */
/*                      Master Port state i.e. all the other ports are in    */
/*                      sync with the current spanning tree information and  */
/*                      Master Port may now move to forwarding state         */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                                                                           */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmAllSyncedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                u2MstInst);
    if ((MST_IS_NOT_SELECTED (pRstPortInfo)) ||
        (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    /* Try all conditions out of the ALTERNATE_PORT state */
    if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_TRUE, u2MstInst)
        != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %u: MasterPortTransitions returned FAILURE !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmMakeForward                    */
/*                                                                           */
/*    Description               : This function sets the State Machine state */
/*                                to FORWARD.                                */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleTrSmMakeForward (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
#else
INT4
MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst)
     tAstPerStPortInfo  *pPerStPortInfo;
     UINT2               u2MstInst;
#endif
{

    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    if ((NULL == pRstPortEntry) || (NULL == pCommPortInfo))
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Local Port %u: Port entry is not present  !!!\n",
                      pPerStPortInfo->u2PortNo);
        return MST_FAILURE;
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state FORWARD for Inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_FDWHILE) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RTSM: Port %s: Inst %d: Stop Timer for FdWhileTmr Failed !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    pRstPortInfo->bForward = MST_TRUE;

    /* Disabling transmission of inferior info in case the blocking the port
       had failed earlier.
       Now that the port can be forwarding there is no need to maintain the
       peer port in discarding state by sending inferior BPDUs any longer. */
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_FORWARD, pPerStPortInfo,
                               u2MstInst) != RST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %d: Port State Transition Machine returned FAILURE for FORWARD event!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if (MstTopologyChMachine (MST_TOPOCHSM_EV_FORWARD, u2MstInst,
                              pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %d: Topology Change Machine returned FAILURE  for FORWARD event!!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %d: Topology Change Machine returned FAILURE  for FORWARD event!!! \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    /* RootInconsistent reset when the port transtions to Designated/Forwarding 
     * on expiry of rcvdInfoWhile timer.*/
    if ((AST_PORT_ROOT_GUARD (pRstPortEntry) == MST_TRUE)
        && (pPerStPortInfo->bRootInconsistent != MST_FALSE))
    {
        if (pRstPortInfo->pRootIncRecTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for Root Inconsistency"
                         " Recovery timer FAILED!\n");
            }
        }

        pPerStPortInfo->bRootInconsistent = MST_FALSE;
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Root_Guard_UnBlock : Root Guard feature unblocking Port: %s: Inst %d at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst, au1TimeStr);

    }
    /* LoopInconsitent state is reset when the port transtions
     * from Designated/Discarding to Root/Forwarding state */
    if ((pRstPortEntry->bLoopGuard == MST_TRUE) &&
        (pPerStPortInfo->bLoopGuardStatus == MST_TRUE) &&
        (pRstPortEntry->bOperPointToPoint == RST_TRUE) &&
        (pPerStPortInfo->bLoopIncStatus == MST_TRUE) &&
        (MstIsPortMemberOfInst
         ((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
          (INT4) u2MstInst) == MST_TRUE))
    {
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst, au1TimeStr);
        pRstPortEntry->bLoopInconsistent = MST_FALSE;
        pPerStPortInfo->bLoopIncStatus = MST_FALSE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmMakeLearn                      */
/*                                                                           */
/*    Description               : This function sets the State Machine state */
/*                                to LEARN.                                  */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Instance Specific Port    */
/*                                                 Information               */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleTrSmMakeLearn (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst)
#else
INT4
MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst)
     tAstPerStPortInfo  *pPerStPortInfo;
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Duration = 0;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state LEARN for Inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->bLearn = MST_TRUE;

    /* Before triggering STSM check whether Disputed is set.
     * If so do not allow the port to move to learning */

    if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
        || (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER))
    {
        if ((pRstPortInfo->bDisputed == MST_TRUE) &&
            (pPortInfo->bOperEdgePort == RST_FALSE))
        {
            if (MstProleTrSmMakeDesignatedorMasterDiscard (pPerStPortInfo,
                                                           u2MstInst)
                != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %d: MakeListen call failed !!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
            return MST_SUCCESS;
        }
    }
    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN, pPerStPortInfo,
                               u2MstInst) != RST_SUCCESS)
    {

        AST_DBG_ARG2 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Inst %d: Port State Transition Machine returned FAILURE for LEARN event!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    /* Getting appropriate Time Duration for fdWhile
     * */
    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp
        == RST_TRUE)
    {
        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
    }
    else
    {
        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
    }

    if (AstStartTimer (pPerStPortInfo, u2MstInst, AST_TMR_TYPE_FDWHILE,
                       u2Duration) != MST_SUCCESS)
    {

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %d: Start Timer for FdWhileTmr Failed !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    if (MstTopologyChMachine (MST_TOPOCHSM_EV_LEARN_SET, u2MstInst,
                              pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s: Topology Change Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmMakeDesignatedorMasterDiscard            */
/*                                                                           */
/* Description        : This routine performs the action for the state       */
/*                      machine state DESG_DISCARD.                          */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Information                */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmMakeDesignatedorMasterDiscard (tAstPerStPortInfo * pPerStPortInfo,
                                           UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Duration;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moving to state LISTEN\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    pRstPortInfo->bLearn = MST_FALSE;
    pRstPortInfo->bForward = MST_FALSE;
    pRstPortInfo->bDisputed = MST_FALSE;

    if ((AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo))->bSendRstp
        == MST_TRUE)
    {
        u2Duration = pPortInfo->DesgTimes.u2HelloTime;
    }
    else
    {
        u2Duration = pPortInfo->DesgTimes.u2ForwardDelay;
    }

    if (AstStartTimer ((VOID *) pPerStPortInfo, u2MstInst,
                       (UINT1) AST_TMR_TYPE_FDWHILE, u2Duration) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s: Inst %u Start Timer for FdWhileTmr Failed !!!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        return MST_FAILURE;
    }

    AST_DBG_ARG3 (AST_SM_VAR_DBG,
                  "RTSM_DesgListen: Port %s: Inst %u fdWhile = %u Learn = Forward = Disputed = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst,
                  u2Duration);

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state DESIGNATED_LISTEN \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    if (RstPortStateTrMachine (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED,
                               pPerStPortInfo, u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "RTSM: Port %s :Inst %u : Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        AST_DBG_ARG2 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "RTSM: Port %s :Inst %u : Port State Transition Machine returned FAILURE!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmSetSyncTree                    */
/*                                                                           */
/*    Description               : This function set the Sync flag to TRUE for*/
/*                                this tree (CIST or given MSTI) for all     */
/*                                ports of the Bridge.                       */
/*                                                                           */
/*    Input(s)                  : u2PortNo - Port Number                     */
/*                                u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleTrSmSetSyncTree (UINT2 u2PortNo, UINT2 u2MstInst)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %d: SetSyncTree called ...\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2MstInst);
    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
        pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2MstInst);

        if ((pPerStInfo != NULL) && (pPerStPortInfo != NULL))
        {
            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %d: Sync Set...\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            pPerStRstPortInfo->bSync = MST_TRUE;
            if (u2PortNum != u2PortNo)
            {
                if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_SYNC_SET,
                                               pPerStPortInfo,
                                               u2MstInst) != MST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for SYNC_SET event!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
            }

        }
    }
    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %d: Completed setting SYNC for all ports\n",
                  AST_GET_IFINDEX_STR (u2PortNo), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleTrSmSetRerootTree                  */
/*                                                                           */
/*    Description               : This function set the Reroot flag to TRUE  */
/*                                for this tree (CIST or given MSTI) for all */
/*                                ports of the Bridge.                       */
/*                                                                           */
/*    Input(s)                  : u2MstInst - MST Istance Number for which   */
/*                                            the SEM operates.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleTrSmSetRerootTree (UINT2 u2MstInst)
#else
INT4
MstPRoleTrSmSetRerootTree (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    UINT2               u2PortNum = 0;

    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Inst %d: SetReRootTree called ...\n", u2MstInst);

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }
        pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
        pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2MstInst);
        if ((pPerStInfo != NULL) && (pPerStPortInfo != NULL))
        {
            pPerStRstPortInfo->bReRoot = MST_TRUE;

            AST_DBG_ARG2 (AST_RTSM_DBG,
                          "RTSM: Port %s: Inst %d ReRoot Set...\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

            if (pPerStBrgInfo->u2RootPort != u2PortNum)
            {
                if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_REROOT_SET,
                                               pPerStPortInfo,
                                               u2MstInst) != MST_SUCCESS)
                {

                    AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for REROOT_SET event!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
    }
    AST_DBG_ARG1 (AST_RTSM_DBG,
                  "RTSM: Inst %d: Completed setting REROOT for all ports\n",
                  u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstIsAllOtherPortsSynced                             */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      Synced variable has been set for all the Ports.      */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_TRUE / MST_FALSE                                 */
/*****************************************************************************/
tAstBoolean
MstIsAllOtherPortsSynced (UINT2 u2PortNum, UINT2 u2MstInst)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStPortInfo  *pPerStRootPortInfo = NULL;
    UINT1               u1RootPort = MST_FALSE;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UNUSED_PARAM (u2PortNum);

    if ((pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst)) == NULL)
    {
        return MST_FALSE;
    }

    if ((pPerStPortInfo =
         AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst)) == NULL)
    {
        return MST_FALSE;
    }

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE))
    {
        u1RootPort = MST_TRUE;
    }

    if (pPerStBrgInfo->u2MstSelectedSyncedFlag != AST_INIT_VAL)
    {
        return MST_FALSE;
    }
    else if (pPerStBrgInfo->u2RoleSyncedFlag != AST_INIT_VAL)
    {
        return MST_FALSE;
    }
    else if (pPerStBrgInfo->u2SyncedFlag != AST_INIT_VAL)
    {
        if (u1RootPort == MST_TRUE)
        {
            /* AllSynced called from either Root port or Alternate port
             * Ignore about the synced status of the Root Port
             * */
            if (pPerStBrgInfo->u2RootPort != AST_INIT_VAL)
                pPerStRootPortInfo =
                    AST_GET_PERST_PORT_INFO (pPerStBrgInfo->u2RootPort,
                                             u2MstInst);
            else
                return MST_FALSE;

            if (!((pPerStBrgInfo->u2SyncedFlag == MST_TRUE)
                  && (pPerStRootPortInfo->bSyncedFlag == MST_TRUE)))
            {
                return MST_FALSE;
            }
        }

        else if (u1RootPort == MST_FALSE)
        {
            if (!((pPerStBrgInfo->u2SyncedFlag == MST_TRUE)
                  && (pPerStPortInfo->bSyncedFlag == MST_TRUE)))

            {
                return MST_FALSE;
            }
        }
    }
    return MST_TRUE;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmIsReRooted                               */
/*                                                                           */
/* Description        : This routine performs the action of checking if the  */
/*                      rrWhile timer is not running for any of the other    */
/*                      ports except this RootPort.                          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of this Port             */
/*                      u2MstInst - Instance Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleTrSmIsReRooted (UINT2 u2PortNum, UINT2 u2MstInst)
#else
INT4
MstPRoleTrSmIsReRooted (u2PortNum, u2MstInst)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    UINT2               u2Count = 0;

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2Count = pRstPortEntry->u2PortNum;

        if (u2Count == 0)
        {
            continue;
        }
        if ((AST_GET_PORTENTRY (u2Count)) != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2Count, u2MstInst);
            if (pPerStPortInfo == NULL)
            {

                continue;
            }
            pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2Count, u2MstInst);
            if (u2Count == u2PortNum)
            {

                continue;
            }
            if ((pRstPortInfo->pRrWhileTmr != NULL) ||
                (pPerStPortInfo->u1SelectedPortRole !=
                 pPerStPortInfo->u1PortRole))
            {
                AST_DBG_ARG2 (AST_RTSM_DBG,
                              "RTSM: Port %s: RrWhileTmr Still Running for inst %d ...\n",
                              AST_GET_IFINDEX_STR (u2Count), u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmRerootedSetRootPort                      */
/*                                                                           */
/* Description        : This routine is called whenever the Rerooted event   */
/*                      triggered when the Role Transition machine is in the */
/*                      ROOT i.e. none of the other ports have the RrWhile   */
/*                      Timer running and hence the Root Port may now        */
/*                      transition to Forwarding.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to Per Spanning Tree        */
/*                                       information.                        */
/*                      u2MstInst - Instance Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmRerootedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (MST_IS_NOT_SELECTED (pRstPortInfo))
    {
        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %d: Selected,Update Info is not Valid - EXITING.\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
        return MST_SUCCESS;
    }

    if ((AST_FORCE_VERSION >= (UINT1) AST_VERSION_2) &&
        (pRstPortInfo->pRbWhileTmr == NULL))
    {
        return MstProleTrSmTransitionToRootFwding (pPerStPortInfo, u2MstInst);
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Moved to state ROOT_PORT role ROOT for inst %d \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmTransitionToRootFwding                   */
/*                                                                           */
/* Description        : This routine is called to transition the root port   */
/*                      to LEARN and FORWARD states. It is called when       */
/*                      ReRooted evaluates to TRUE or when rbWhile timer     */
/*                      expires.                                             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2MstInst      - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmTransitionToRootFwding (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPortEntry      *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    pCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (pPerStPortInfo->u2PortNo,
                                                u2MstInst);

    if (pRstPortInfo->bLearn == MST_FALSE)
    {
        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_RootLearn: Port %s: Inst %u: Learn = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        if (MstPRoleTrSmMakeLearn (pPerStPortInfo, u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: Inst %u: MakeLearn function returned FAILURE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);
            return MST_FAILURE;
        }
    }

    if ((pRstPortInfo->bForward == MST_FALSE) &&
        (pRstPortInfo->bLearn == MST_TRUE))
    {
        if (((u2MstInst == 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
             && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
             && (pCommPortInfo->pEdgeDelayWhileTmr == NULL)) ||
            ((u2MstInst != 0) && (pRstPortEntry->bLoopGuard == RST_TRUE)
             && (pRstPortEntry->bOperPointToPoint == RST_TRUE)
             && (pCommPortInfo->pEdgeDelayWhileTmr == NULL) &&
             (MstIsPortMemberOfInst
              ((INT4) AST_GET_IFINDEX ((INT4) (pPerStPortInfo->u2PortNo)),
               (INT4) u2MstInst) == MST_TRUE)))

        {
            /* LoopInconsitent state is set when the port transtions
             * to Designated/Discarding because of loop-guard feature*/
            pRstPortEntry->bLoopInconsistent = RST_TRUE;
            pPerStPortInfo->bLoopIncStatus = RST_TRUE;
            UtlGetTimeStr (au1TimeStr);
            pRstPortInfo->bLearn = RST_FALSE;
            pRstPortInfo->bForward = RST_FALSE;
            AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                              "Spanning-tree Loop-guard feature blocking Port: %s for Instance: %d at %s\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst, au1TimeStr);
            AstLoopIncStateChangeTrap ((UINT2)
                                       AST_GET_IFINDEX (pPerStPortInfo->
                                                        u2PortNo),
                                       pPerStPortInfo->bLoopIncStatus,
                                       u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                                       AST_MST_TRAPS_OID_LEN);
            if (RstPortStateTrMachine
                (RST_PSTATETRSM_EV_LEARN_FWD_DISABLED, pPerStPortInfo,
                 u2MstInst) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                AST_DBG_ARG1 (AST_RTSM_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Port State Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
                return RST_FAILURE;
            }

        }
        else
        {
            /* LoopInconsitent state is reset when the port transtions 
             * from Designated/Discarding to Root/Forwarding state */
            if ((pRstPortEntry->bLoopGuard == MST_TRUE) &&
                (pRstPortEntry->bOperPointToPoint == RST_TRUE))
            {
                pRstPortEntry->bLoopInconsistent = MST_FALSE;
                pPerStPortInfo->bLoopIncStatus = MST_FALSE;
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                  "Spanning-tree Loop-guard feature unblocking Port: %s for Instance: %d at %s\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst,
                                  au1TimeStr);

            }

            AST_DBG_ARG1 (AST_SM_VAR_DBG,
                          "RTSM_RootForward: Port %s: Forward = TRUE\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

            if (MstPRoleTrSmMakeForward (pPerStPortInfo, u2MstInst) !=
                MST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: MakeForward function returned FAILURE\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }

    if ((pRstPortInfo->bForward == MST_TRUE) &&
        (pRstPortInfo->bReRoot == MST_TRUE))
    {
        pRstPortInfo->bReRoot = MST_FALSE;

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "RTSM_Rerooted: Port %s: Inst %u: ReRoot = FALSE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);

        AST_DBG_ARG2 (AST_RTSM_DBG,
                      "RTSM: Port %s: Inst %u: Moved to state REROOTED \n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2MstInst);
    }

    AST_DBG_ARG2 (AST_RTSM_DBG,
                  "RTSM: Port %s: Inst %u: Moved to state ROOT_PORT \n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleTrSmDisputedSetDesigOrMasterPort             */
/*                                                                           */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Disputed variable is set*/
/*                      in the Designated or Master Port state.              */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Pointer to Per Spanning Tree        */
/*                                       information.                        */
/*                      u2MstInst - Instance Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleTrSmDisputedSetDesigOrMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                          UINT2 u2MstInst)
{
    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
    {
        /* Try all conditions out of the DESIGNATED_PORT state */
        if (MstProleTrSmDesigPortTransitions (pPerStPortInfo, MST_FALSE,
                                              u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesigPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)
    {
        /* Try all conditions out of the MASTER_PORT state */
        if (MstProleTrSmMasterPortTransitions (pPerStPortInfo, MST_FALSE,
                                               u2MstInst) != MST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MasterPortTransitions returned FAILURE !!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmIndicateAllSyncedSet                     */
/*                                                                           */
/* Description        : This routine is called when allSynced becomes TRUE.  */
/*                      It triggers the allSynced event to all Root and      */
/*                      Alternate/Backup ports.                              */
/*                                                                           */
/* Input(s)           : u2PortNo  - The port which caused  this trigger      */
/*                      u2MstInst - Instance Info                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmIndicateAllSyncedSet (UINT2 u2PortNo, UINT2 u2MstInst)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum;

    /* Allsynced is an important info as far as Master Port is 
     * concerned and so no check like that of rstp (proceed only if there
     * is a root port) is needed here
     * */

    tRstPortInfo       *pRstPortEntry = NULL;

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }

        /* AllSynced for this port will be handled as part of the port's
         * role transitions */
        if (u2PortNum == u2PortNo)
        {
            continue;
        }

        if ((pPerStPortInfo =
             AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst)) == NULL)
        {
            continue;
        }

        if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT) ||
            (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
            (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP) ||
            (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER))

        {
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_ALLSYNCED_SET,
                                           pPerStPortInfo,
                                           u2MstInst) != RST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "RTSM: Port %s: Inst %u: Port Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "RTSM: Port %s: Inst %u: Port Role Transition Machine returned FAILURE!\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                   u2PortNo), u2MstInst);
                return MST_FAILURE;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstProleTrSmLearningFwdingReset                      */
/*                                                                           */
/* Description        : This routine performs the action for changing the    */
/*                      state machine state when the Learning and Forwarding */
/*                      variables have been reset after the Hardware port    */
/*                      state is put to Blocking.                            */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information (Instance 0 for    */
/*                                       RSTP)                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP))
    {
        return MstPRoleTrSmMakeAlternatePort (pPerStPortInfo, u2MstInst);
    }
    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED))
    {
        if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
        {
            return MstPRoleTrSmMakeDisablePort (pPerStPortInfo, u2MstInst);
        }
        else
        {
            return MstPRoleTrSmMakeDisabledPort (pPerStPortInfo, u2MstInst);
        }
    }

    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED) &&
             (pRstPortInfo->bSynced == MST_FALSE))
    {
        if (MstProleTrSmDesigPortTransitions
            (pPerStPortInfo, MST_FALSE, u2MstInst) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: DesgPortTransitions returned FAILURE !!!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
    }

    else if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_MASTER) &&
             (pRstPortInfo->bSynced == MST_FALSE))
    {

        if (MstProleTrSmMasterPortTransitions
            (pPerStPortInfo, MST_FALSE, u2MstInst) != RST_SUCCESS)
        {
            AST_DBG_ARG1 (AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                          "RTSM: Port %s: MasterPortTransitions returned FAILURE !!!!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
            return MST_FAILURE;
        }
        return RST_SUCCESS;

    }
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstInitPortRoleTrMachine                             */
/*                                                                           */
/* Description        : Initialises the Port Role Transition State Machine.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstInitPortRoleTrMachine (VOID)
#else
VOID
MstInitPortRoleTrMachine (VOID)
#endif
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[MST_PROLETRSM_MAX_EVENTS][20] = {
        "BEGIN", "FDWHILE_EXPIRED", "RRWHILE_EXPIRED",
        "RBWHILE_EXPIRED", "SYNC_SET", "REROOT_SET",
        "SELECTED_SET", "PROPOSED_SET", "AGREED_SET",
        "ALLSYNCED_SET", "REROOTED_SET", "OPEREDGE_SET", "DISPUTED_SET"
    };
    UINT1               aau1SemState[MST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "DISABLE_PORT", "DISABLED_PORT", "ROOT_PORT",
        "DESIG_PORT", "ALTERNATE_PORT", "MASTER_PORT", "BLOCK_PORT"
    };

    for (i4Index = 0; i4Index < MST_PROLETRSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_RTSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_RTSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < MST_PROLETRSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_RTSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_RTSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* Event 1 - MST_PROLETRSM_EV_BEGIN */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = MstPRoleTrSmMakeInitPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_BEGIN]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction = MstPRoleTrSmMakeInitPort;

    /* Event 2 - MST_PROLETRSM_EV_FDWHILE_EXPIRED */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = MstPRoleTrSmFwdExpRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmFwdExpDesigOrMasterPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_FDWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmFwdExpDesigOrMasterPort;

    /* Event 3 - MST_PROLETRSM_EV_RRWHILE_EXPIRED */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmRrWhileExpDesigPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RRWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction = NULL;

    /* Event 4 - MST_PROLETRSM_EV_RBWHILE_EXPIRED */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction =
        MstPRoleTrSmRbWhileExpRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_RBWHILE_EXPIRED]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction = NULL;

    /* Event 5 - MST_PROLETRSM_EV_SYNC_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction =
        MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = MstProleTrSmSyncSetRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = MstPRoleTrSmSyncSetDesigPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        MstPRoleTrSmMakeAlternatePort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SYNC_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmSyncSetMasterPort;

    /* Event 6 - MST_PROLETRSM_EV_REROOT_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction =
        MstPRoleTrSmMakeDisabledPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmReRootSetDesigOrMasterPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        MstPRoleTrSmMakeAlternatePort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOT_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmReRootSetDesigOrMasterPort;

    /* Event 7 - MST_PROLETRSM_EV_SELECTED_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction =
        MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction =
        MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        MstPRoleTrSmUpdateInfoReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_SELECTED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction = MstPRoleTrSmUpdateInfoReset;

    /* Event 8 - MST_PROLETRSM_EV_PROPOSED_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction =
        MstProleTrSmProposedSetRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        MstProleTrSmProposedSetAlternatePort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_PROPOSED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstProleTrSmProposedSetMasterPort;

    /* Event 9 - MST_PROLETRSM_EV_AGREED_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = MstProleTrSmAgreedSetRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstProleTrSmAgreedSetDesigPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_AGREED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstProleTrSmAgreedSetMasterPort;

    /* Event 10 - MST_PROLETRSM_EV_ALLSYNCED_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction =
        MstPRoleTrSmAllSyncedSetRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmAllSyncedSetDesigPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction =
        MstPRoleTrSmAllSyncedSetAltPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_ALLSYNCED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmAllSyncedSetMasterPort;

    /* Event 11 - MST_PROLETRSM_EV_REROOTED_SET */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction =
        MstPRoleTrSmRerootedSetRootPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_REROOTED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction = NULL;

    /* Event 12 - MST_PROLETRSM_EV_OPEREDGE_SET Event */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmOperEdgeSetDesigOrMasterPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_OPEREDGE_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmOperEdgeSetDesigOrMasterPort;

    /* Event 13 - MST_PROLETRSM_EV_DISPUTED_SET Event */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstPRoleTrSmDisputedSetDesigOrMasterPort;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_DISPUTED_SET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstPRoleTrSmDisputedSetDesigOrMasterPort;

    /* Event 14 - MST_PROLETRSM_EV_LEARNING_FWDING_RESET Event */
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_INIT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_DISABLE_PORT].pAction =
        MstProleTrSmLearningFwdingReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_DISABLED_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_BLOCK_PORT].pAction =
        MstProleTrSmLearningFwdingReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_ROOT_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_DESIG_PORT].pAction =
        MstProleTrSmLearningFwdingReset;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_ALTERNATE_PORT].pAction = NULL;
    MST_PORT_ROLE_TR_MACHINE[MST_PROLETRSM_EV_LEARNING_FWDING_RESET]
        [MST_PROLETRSM_STATE_MASTER_PORT].pAction =
        MstProleTrSmLearningFwdingReset;
}
#endif /* MSTP_WANTED */
