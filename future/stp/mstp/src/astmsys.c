/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmsys.c,v 1.193 2018/01/22 09:40:29 siva Exp $ 
 *
 * Description: This file contains initialisation routines for the
 *              MSTP Module.                                      
 *
 *******************************************************************/

#ifdef MSTP_WANTED
#define _ASTMSYS_C

#include "astminc.h"

#ifdef NPAPI_WANTED
extern INT4         gi4AstInitProcess;
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstModuleInit                              */
/*                                                                           */
/*    Description               : Calls the Component Initialisation of the  */
/*                                Bridge depending upon the type of the bridge*/
/*                                Mode.                                      */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstModuleInit (VOID)
#else
INT4
MstModuleInit ()
#endif
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    INT4                i4Mode = AST_INIT_VAL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RegionCfgChangeCount = AST_INIT_VAL;
    UINT4               u4PortsCreated = AST_INIT_VAL;
    UINT2               u2PrevPort = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT1               u1OperStatus;
    UINT1              *pu1DigestInput = NULL;

    /* Assumption:
     * 1) The appropriate context is selected before calling this function*/

    if (AST_IS_MST_STARTED ())
    {
        return MST_SUCCESS;
    }

    (AST_CURR_CONTEXT_INFO ())->u1ForceVersion = AST_VERSION_3;
    (AST_CURR_CONTEXT_INFO ())->bBegin = MST_FALSE;

    /*State-Machine variable changes Debug is not currently supported 
       in MSTP. This check is put because if RSTP is shut down and 
       MSTP is started, then the debug option cannot be more than the
       maximum supported by MSTP debug option. */
    if ((AST_CURR_CONTEXT_INFO ())->u4DebugOption > MST_MAX_DEBUG_VAL)
    {
        (AST_CURR_CONTEXT_INFO ())->u4DebugOption = MST_MAX_DEBUG_VAL;
    }

    /* By Default MST INSTANCE PORT MAPPING ENABLED */
    AST_INST_PORT_MAP = MST_INST_MAP_DISABLED;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             " Initializing MSTP Module ... \n");
    AST_DBG (AST_INIT_SHUT_DBG, " Initializing MSTP Module ... \n");

    if (AstDeriveOperatingMode (&i4Mode) == RST_FAILURE)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Bridge Mode retrieving from l2iwf Failed !!!!\n  quitting...\n");
        (VOID) MstModuleShutdown ();
        return MST_FAILURE;
    }
    /* The value of i4Mode will be used during ASTP module initialisation only.
     * Hence ignoring the same 
     * */
    if (AST_ALLOC_DIGEST_INPUT (pu1DigestInput) == NULL)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " Memory Allocation for Digest Input failed...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Memory Allocation for Digest Input failed... \n");

        return MST_FAILURE;
    }
    AST_GET_MST_DIGEST_INPUT () = pu1DigestInput;

    /*For SVLAN component, the Max Number of Ports Per Context is the Port
       Table size of the context */
    AST_MAX_NUM_PORTS = AST_MAX_PORTS_PER_CONTEXT;

    if (AstAllocPortAndPerStInfoBlock (AST_MSTP_MODULE) != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " SYS: Allocation of PortInfo and PerStInfo Table array"
                 "failed ...\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 " SYS: Allocation of PortInfo and PerStInfo Table array"
                 "failed ...\n");
        (VOID) MstModuleShutdown ();

        return MST_FAILURE;
    }

    AstL2IwfVlanToInstMappingInit (AST_CURR_CONTEXT_ID ());

    /* Initialize the MST State Machines */
    MstInitStateMachines ();

    if (AstCreateSpanningTreeInst (MST_CIST_CONTEXT, &pPerStInfo)
        != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " SYS : Creation of Default Instance Failed in MSTP !!!\n");
        AST_DBG (AST_INIT_SHUT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: Creation of Default instance failed in MSTP !!!\n");

        AST_MODULE_STATUS_SET (MST_DISABLED);
        (VOID) MstModuleShutdown ();
        return MST_FAILURE;
    }

    /* Updating the Instance as Active in  L2IWF */
    L2IwfCreateSpanningTreeInstance (AST_CURR_CONTEXT_ID (), MST_CIST_CONTEXT);

    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);
    pBrgInfo = AST_GET_BRGENTRY ();

    MstInitGlobalBrgInfo ();

    AstIssGetContextMacAddress (AST_CURR_CONTEXT_ID (), pBrgInfo->BridgeAddr);

    /* For SVLAN component, CEP ifIndex will be NULL */
    AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = AST_INIT_VAL;

    AST_MAX_NUM_PORTS = AST_MAX_PORTS_PER_CONTEXT;

    AST_PB_CVLAN_INFO () = NULL;

    AST_MEMCPY (&(pBrgInfo->MstBridgeEntry.RegionalRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pBrgInfo->MstBridgeEntry.RegionalRootId.u2BrgPriority
        = MST_DEFAULT_BRG_PRIORITY;

    u4RegionCfgChangeCount = pBrgInfo->MstBridgeEntry.u4RegionCfgChangeCount;
    MstInitMstBridgeEntry ();
    pBrgInfo->MstBridgeEntry.u4RegionCfgChangeCount = u4RegionCfgChangeCount;

    MstInitPerStBrgInfo (pPerStBrgInfo);

    if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
    {
        while (AstL2IwfGetNextValidPortForContext
               (AST_CURR_CONTEXT_ID (), u2PrevPort, &u2PortNum, &u4IfIndex)
               == L2IWF_SUCCESS)
        {
            if (AstL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
            {
                u2PrevPort = u2PortNum;
                continue;
            }

            if (AstIsExtInterface (u2PortNum) == AST_TRUE)
            {
                u2PrevPort = u2PortNum;
                continue;
            }

            /* Port creation should be blocked if the number of ports created has reached
             * max ports in system. */

            AstGetNumOfPortsCreated (&u4PortsCreated);

            if (u4PortsCreated >= AST_SIZING_PORT_COUNT)
            {
                break;
            }

            if (MstCreatePort (u4IfIndex, u2PortNum) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "Port %u: CreatePort returned failure  !!!\n",
                              u4IfIndex);
                (VOID) MstModuleShutdown ();
                AST_RELEASE_PERST_INFO_MEM_BLOCK (pPerStInfo);
                return MST_FAILURE;
            }

#ifdef L2RED_WANTED
            if (AstRedCreatePortAndPerPortInst (u2PortNum) == RST_FAILURE)
            {
                (VOID) MstModuleShutdown ();
                return MST_FAILURE;
            }
#endif

            pPortInfo = AST_GET_PORTENTRY (u2PortNum);
            if (pPortInfo == NULL)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "Local Port %u: Port entry is not present  !!!\n",
                              u2PortNum);
                (VOID) MstModuleShutdown ();
                return MST_FAILURE;
            }

            /* Oper status returned here is sum of all lower layer oper status */
            AstGetPortOperStatusFromL2Iwf (STP_MODULE, u2PortNum,
                                           &u1OperStatus);

            /* The Port path cost is updated if the link is already UP
             * when the Spanning Tree module is STARTED
             */
            if ((pPortInfo->bPathCostInitialized == MST_FALSE) &&
                (u1OperStatus == AST_UP))
            {
                pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);
                pPortInfo->bPathCostInitialized = MST_TRUE;

                pTmpPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                             MST_CIST_CONTEXT);

                if (AstPathcostConfiguredFlag (u2PortNum, MST_CIST_CONTEXT)
                    == MST_FALSE)
                {
                    pTmpPerStPortInfo->u4PortPathCost = pPortInfo->u4PathCost;
                }
            }
            /* Derive the MAC Address type for the specific port */
            AstDeriveMacAddrFromPortType (u2PortNum);

            u2PrevPort = u2PortNum;
        }
    }

    AST_MODULE_STATUS_SET (MST_DISABLED);

    AST_ADMIN_STATUS = (UINT1) MST_DISABLED;

    AST_SYSTEM_CONTROL_SET (MST_START);

    /* Make S-VLAN component status as enabled. This makes the
     * MstModuleEnable to enable the S-VLAN spanning tree during
     * system start up. */
    AST_SVLAN_ADMIN_STATUS () = RST_ENABLED;

    /* When system comes up as a 1ad bridge, Ast will come up before
     * vlan and hence the port types in l2iwf will be customer bridge
     * port. So if AstPbCheckAllPortTypes is called, it will return 
     * failure. So don't call that if vlan is in shutdown state. */
    if (AstVlanGetStartedStatus (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        /* In case of 1ad bridge, initialize the C-VLAN components. */
        if (AST_IS_1AD_BRIDGE () == RST_TRUE
            || AST_IS_1AH_BRIDGE () == RST_TRUE)
        {
            if (AstPbCheckAllPortTypes () == RST_FAILURE)
            {
                AST_DBG (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                         AST_ALL_FAILURE_TRC,
                         "SYS: Mstp Initialization in PbCheckAll Port Types"
                         "Failed \r\n");
                (VOID) MstModuleShutdown ();
                AST_SVLAN_ADMIN_STATUS () = RST_DISABLED;
                return MST_FAILURE;
            }
        }
    }

    AST_MST_TRAP_TYPE = AST_TRAP_ABNORMAL | AST_TRAP_NORMAL;

    /* Set the flush interval count to default */
    if (gu1EnablePortVlanFlushOpt == AST_TRUE)
    {
        AST_FLUSH_INTERVAL = AST_TMR_TYPE_MST_FLUSH_OPT;
    }
    else
    {

        AST_FLUSH_INTERVAL = AST_INIT_VAL;
    }
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Initialized MSTP Module SUCCESSFULLY... \n");
    AST_DBG (AST_INIT_SHUT_DBG,
             "SYS: Initialized MSTP Module SUCCESSFULLY... \n");
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstModuleShutdown                          */
/*                                                                           */
/*    Description               : DeInitialises the CVLAN & SVLAN MST module */
/*                                structures and deletes memory pools. Also  */
/*                                deletes timer list.                        */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstModuleShutdown (VOID)
#else
INT4
MstModuleShutdown ()
#endif
{
    INT4                i4RetVal = MST_SUCCESS;
    tAstBridgeEntry    *pBrgInfo = NULL;
    INT2                i2MstInst = 0;
    UINT2               u2PortNum = 0;
    UINT1               u1Result = MST_FALSE;    /* Initialised to zero */
    UINT2               u2VlanId = 0;

    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Shutting down MSTP Module ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Shutting down MSTP Module ...\n");

    if (AstPbAllCVlanCompShutdown () != RST_SUCCESS)
    {
        i4RetVal = MST_FAILURE;
    }

    if (AST_MODULE_STATUS == MST_ENABLED)
    {
        (VOID) MstModuleDisable ();
    }

    /* Set module status as disabled. */
    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = MST_DISABLED;

    for (u2VlanId = 1; u2VlanId <= AST_MAX_NUM_VLANS; u2VlanId++)
    {
        i2MstInst = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                  u2VlanId);

        if (i2MstInst != GARP_STAP_CIST_ID)
        {
            AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                        MST_UPDATE_INST_VLAN_MAP_MSG,
                                        MST_CIST_CONTEXT,
                                        i2MstInst,
                                        u2VlanId,
                                        MST_UNMAP_INST_VLAN_LIST_MSG,
                                        &u1Result);
        }
    }
    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    MST_CIST_CONTEXT,
                                    i2MstInst,
                                    u2VlanId,
                                    MST_UNMAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    for (i2MstInst = AST_MAX_MST_INSTANCES - 1; i2MstInst >= 0; i2MstInst--)
    {
        pPerStInfo = AST_GET_PERST_INFO (i2MstInst);
        if (pPerStInfo == NULL)
        {
            continue;
        }
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
            AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst) = NULL;
        }

        L2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (),
                                         (UINT2) i2MstInst);

        AstDeleteMstInstance (i2MstInst, pPerStInfo);
    }

    AST_SYSTEM_CONTROL_SET ((UINT1) MST_SHUTDOWN);

    if (AST_COUNT_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList) != 0)
    {
        while ((pRstPortInfo = AST_GET_FIRST (&(AST_CURR_CONTEXT_INFO ())->
                                              PortList)) != NULL)
        {
            AST_DELETE_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                                 pRstPortInfo);
            AST_RELEASE_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);
        }

    }

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {

        AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                u2PortNum, L2_PROTO_STP,
                                                OSIX_DISABLED);
        if (pPortInfo == NULL)
        {
            continue;
        }

        /* Remove this node from the IfIndex RBTree and 
         * the PerContext DLL */
        AstReleasePortMemBlocks (pPortInfo);

        if (AST_NODE_STATUS () == RED_AST_ACTIVE)
        {
            AstRedClearPduOnActive (u2PortNum);
        }

#ifdef L2RED_WANTED
        AstRedDeletePortAndPerPortInst (u2PortNum);
#endif
    }

    /* Clean-up of TE-MSTID Instance */
    (AST_CURR_CONTEXT_INFO ()->u1TEMSTIDValid) = MST_FALSE;

    AstL2IwfVlanToInstMappingInit (AST_CURR_CONTEXT_ID ());

    AST_CURR_CONTEXT_INFO ()->u4CepIfIndex = AST_INIT_VAL;
    AST_PB_CVLAN_INFO () = NULL;

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    pMstConfigIdInfo = &(pAstMstBridgeEntry->MstConfigIdInfo);
    AST_MEMSET ((pMstConfigIdInfo->au1ConfigDigest),
                AST_INIT_VAL, MST_CONFIG_DIGEST_LEN);

    if (AST_GET_MST_DIGEST_INPUT () != NULL)
    {
        AST_RELEASE_MST_DIGEST_INPUT (AST_GET_MST_DIGEST_INPUT ());
    }

    AstReleasePortAndPerStInfoBlock ();

    pBrgInfo = AST_GET_BRGENTRY ();
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;

    AST_BRIDGE_MODE () = AST_INVALID_BRIDGE_MODE;
    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: MSTP Module Shutdown SUCCESSFULLY\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: MSTP Module Shutdown SUCCESSFULLY\n");

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstModuleEnable                            */
/*                                                                           */
/*    Description               : Updates the MST module status              */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstModuleEnable (VOID)
#else
INT4
MstModuleEnable ()
#endif
{

    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_ENABLED)
    {
        return MST_SUCCESS;
    }

    AST_ADMIN_STATUS = MST_ENABLED;

    if (AST_IS_1AD_BRIDGE () == RST_TRUE)
    {
        /* If S-VLAN component is configured as enabled, then enable the 
         * S-VLAN component spanning tree. */
        if (AST_SVLAN_ADMIN_STATUS () == RST_ENABLED)
        {
            if (MstComponentEnable () != MST_SUCCESS)
            {
                return MST_FAILURE;
            }
        }
    }
    else
    {
        /* In case of customer / provider bridge just enable the module. */
        if (MstComponentEnable () != MST_SUCCESS)
        {
            return MST_FAILURE;
        }
    }

    if (AstPbAllCVlanCompEnable () != RST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: CVLAN RSTP Module Enable is not proper...Failures"
                 "Occurred !!!\n");

        return MST_FAILURE;
    }

    if (AST_NODE_STATUS () != RED_AST_IDLE)
    {
        gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = MST_ENABLED;
    }
#ifdef ECFM_WANTED
    EcfmMstpEnableIndication (AST_CURR_CONTEXT_ID ());
#endif
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstComponentEnable                         */
/*                                                                           */
/*    Description               : Enables the MST module makes it operational*/
/*                                Called by management action to enable the  */
/*                                MST Module.Then calls the CFA Module and   */
/*                                allocates memory for all ports that are    */
/*                                created already. Also for all the ports    */
/*                                that are operationally up it initialises   */
/*                                the MST state machines.                    */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstComponentEnable (VOID)
#else
INT4
MstComponentEnable ()
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Enabling MSTP Module.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Enabling MSTP Module.\n");

    if (AST_MODULE_STATUS == MST_ENABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Enabled...Quitting without processing.\n");
        return MST_SUCCESS;
    }
    AST_GET_SYSTEMACTION = MST_ENABLED;

    /* In Active & Standby node MSTP can be directly enabled.
     * When NodeStatus is in IDLE State,MSTP should not be enabled.
     * MSTP can be enabled only when NodeStatus is in ACTIVE or STANDBY State.
     */

    if (AST_NODE_STATUS () <= RED_AST_IDLE)
    {
        return MST_SUCCESS;
    }
    AST_MODULE_STATUS_SET (MST_ENABLED);

    pBrgInfo = AST_GET_BRGENTRY ();

    AstIssGetContextMacAddress (AST_CURR_CONTEXT_ID (), pBrgInfo->BridgeAddr);

    AST_MEMCPY (&(pBrgInfo->MstBridgeEntry.RegionalRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    pBrgInfo->MstBridgeEntry.RegionalRootId.u2BrgPriority
        = MST_DEFAULT_BRG_PRIORITY;

    MstInitConfigName (MST_FALSE);

    MstReInitMstInfo ();

    if (MstHwReInit () != MST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                 " Hardware Reinitialization failed !!!\n");
        return MST_FAILURE;
    }
#ifdef NPAPI_WANTED
    gi4AstInitProcess = 1;
#endif

    if (AstAssertBegin () == MST_FAILURE)
    {

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");

#ifdef NPAPI_WANTED
        gi4AstInitProcess = 0;
#endif
        return MST_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        {
            VlanMiDeleteAllFdbEntries (AST_CURR_CONTEXT_ID ());
            gi4AstInitProcess = 0;
        }
    }
#endif

    /* When System comes up default digest should be 
     * 0xAC36177F50283CD4B83821D8AB26DE62 as mentioned in 802.1sD15 
     * Table 13.2*/
    MstTriggerDigestCalc ();
    /* Incrementing the MSTP Up Count */
    AST_INCR_ASTP_UP_COUNT ();

    AST_INST_PORT_MAP = MST_INST_MAP_DISABLED;

    AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
             "SYS: MSTP Module Enabled.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: MSTP Module Enabled.\n");
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstAssertBegin                             */
/*                                                                           */
/*    Description               : This function initialises all the State    */
/*                                Machines. It is called when the global     */
/*                                condition BEGIN is asserted.               */
/*                                                                           */
/*    Input(s)                 :  None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

INT4
MstAssertBegin (VOID)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT1               u1OperStatus = (UINT1) AST_INIT_VAL;
    UINT2               u2PortNum = 0;
    INT2                i2MstInst = 0;

    (AST_CURR_CONTEXT_INFO ())->bBegin = MST_TRUE;

    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }
        if (pPerStInfo->pFlushTgrTmr != NULL)
        {
            if (AstStopTimer ((VOID *) pPerStInfo, AST_TMR_TYPE_FLUSHTGR)
                != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for FlushTimer FAILED!\n");
            }

        }

        if (RstPortRoleSelectionMachine (RST_PROLESELSM_EV_BEGIN,
                                         i2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "Module Enable: Port Role Selection Machine Returned failure for instance %d for BEGIN event!!!\n",
                          i2MstInst);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port Role Selection Machine Returned failure for instance %d for BEGIN event!!!\n",
                          i2MstInst);
#ifdef NPAPI_WANTED
            gi4AstInitProcess = 0;
#endif
            AST_MODULE_STATUS_SET (MST_DISABLED);
            return MST_FAILURE;
        }

        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            /* Oper status returned here is sum of all LL oper status */

            AstGetPortOperStatusFromL2Iwf (STP_MODULE, u2PortNum,
                                           &u1OperStatus);
            if (AST_IS_1AD_BRIDGE () == RST_TRUE)
            {
                if (AST_IS_CVLAN_COMPONENT () == RST_TRUE)
                {
                    if (AST_GET_BRG_PORT_TYPE (pPortInfo) ==
                        AST_PROVIDER_EDGE_PORT)
                    {
                        /* Determine Oper Point to Point for PEP from L2IWF */
                        if (RstUpdatePepOperPtoP (pPortInfo->u2PortNo,
                                                  u1OperStatus) != RST_SUCCESS)
                        {
                            /* FATAL ERROR !!!!!!!!!!!!!!!!! */
                            continue;
                        }
                    }
                }
                else
                {
                    /* SVLAN context */

                    /* SVLAN context CEP should be blocked */
                    if (AST_IS_CUSTOMER_EDGE_PORT (pPortInfo->u2PortNo) ==
                        RST_TRUE)
                    {
                        /* Set the port oper status as down in the S-VLAN 
                         * component. This makes the sem to be in diabled 
                         * state in S-VLAN comp for this port. */
                        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;
                        continue;
                    }

                }
                /* For CNP/PNP/PPNP/PCEP/PCNP in SVLAN context &
                 * For CEP/PEP in CVLAN context Proceed on triggering SEMs*/
            }
#ifdef NPAPI_WANTED
            if (gi4AstInitProcess == 1)
#endif
            {

                if (u1OperStatus == AST_UP)
                {
                    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;

                }
            }
            if (MstStartSemsForPort (u2PortNum, i2MstInst) == MST_FAILURE)
            {
                AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "Module Enable: StartSems returned failure for port %s instance %d!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), i2MstInst);
#ifdef NPAPI_WANTED
                gi4AstInitProcess = 0;
#endif
                AST_MODULE_STATUS_SET (MST_DISABLED);
                return MST_FAILURE;
            }

            pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, i2MstInst);

            if (pRstPortInfo->bPortEnabled != MST_TRUE)
            {
                continue;
            }

            AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                          "SYS: Inst %d: Port %s enabled. Triggering Enabled event for Port Mig and Port Info SEM\n",
                          i2MstInst, AST_GET_IFINDEX_STR (u2PortNum));

            if (i2MstInst == MST_CIST_CONTEXT)
            {
                if (RstPortMigrationMachine
                    ((UINT2) RST_PMIGSM_EV_PORT_ENABLED,
                     u2PortNum) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                  "SYS: Port %s: Port Protocol Migration Machine Returned failure  for PORT_ENABLED event !!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %s: Port Protocol Migration Machine Returned failure  for PORT_ENABLED event !!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

#ifdef NPAPI_WANTED
                    gi4AstInitProcess = 0;
#endif
                    AST_MODULE_STATUS_SET (MST_DISABLED);

                    return RST_FAILURE;
                }
            }
            if (MstPortInfoMachine
                (MST_PINFOSM_EV_PORT_ENABLED, pPerStPortInfo, NULL,
                 i2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Port %s Inst %d: Port Info Machine Returned failure  for PORT_ENABLED event !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), i2MstInst);
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s Inst %d: Port Info Machine Returned failure  for PORT_ENABLED event !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), i2MstInst);

#ifdef NPAPI_WANTED
                gi4AstInitProcess = 0;
#endif
                AST_MODULE_STATUS_SET (MST_DISABLED);

                return MST_FAILURE;
            }
            /* Check for CIST has been removed, since, BEGIN will cause
             * all the State M/cs including per port ones to initialise
             * Reference :: IEEE 802.1Q 2005 13.23.1 BEGIN
             * */

            if (RstPortTransmitMachine (RST_PTXSM_EV_TX_ENABLED,
                                        pPortInfo, RST_DEFAULT_INSTANCE)
                != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                return MST_FAILURE;
            }
        }
    }
#ifdef NPAPI_WANTED
    VlanMiDeleteAllFdbEntries (AST_CURR_CONTEXT_ID ());
#endif
    (AST_CURR_CONTEXT_INFO ())->bBegin = MST_FALSE;

    i2MstInst = 0;
    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, i2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            /* Since the priority vectors are not yet initialized no bpdus
             * should be transmitted as yet. This is achieved because - 
             * Selected is false for all ports at this time, and hence 
             * all state transitions in TXSM are disabled. 
             * So the below trigger merely serves to transition the TXSM 
             * to the IDLE state - ready for transmission when Selected is set 
             * by RSSM */
            if (RstPortTransmitMachine
                ((UINT2) RST_PTXSM_EV_BEGIN_CLEARED, pPortInfo,
                 (UINT2) i2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return MST_FAILURE;
            }

            if (MstPortInfoMachine ((UINT2) MST_PINFOSM_EV_BEGIN_CLEARED,
                                    pPerStPortInfo, NULL, (UINT2) i2MstInst)
                != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                              AST_ALL_FAILURE_TRC,
                              "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s: Port Info Machine Returned failure  !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                return MST_FAILURE;
            }
        }

        /* Triggering RSSM will inturn make RTSM, STSM, TCSM for all ports
         * and they will come out of their init states. 
         * At last, it will also trigger TXSM for each port with SELECTED_SET 
         * event, thus causing the first bpdu to be transmitted out 
         * from the port.
         */

        if (RstPortRoleSelectionMachine
            ((UINT2) RST_PROLESELSM_EV_BEGIN_CLEARED,
             (UINT2) i2MstInst) != RST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                     AST_ALL_FAILURE_TRC,
                     " SYS: Role Selection Machine Returned failure !!!\n");
            AST_DBG (AST_INIT_SHUT_TRC | AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                     " SYS: Role Selection Machine Returned failure !!!\n");

            return MST_FAILURE;
        }
    }
    /* The rest of the state machines (BDSM, PMSM, PRSM) can remain in their
     * init states until any external events occur */

    AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
             "SYS: MSTP Module Enabled.\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: MSTP Module Enabled.\n");
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstModuleDisable                           */
/*                                                                           */
/*    Description               : It disables all the CVLAN component and    */
/*                                SVLAN component from the Provider Edge Bridge*/
/*                                In Case of other Bridge modes, it does normal */
/*                                disable processing.                        */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstModuleDisable (VOID)
#else
INT4
MstModuleDisable ()
#endif
{
#ifdef L2RED_WANTED
    if ((gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] ==
         (UINT1) MST_DISABLED) && (AST_ADMIN_STATUS == (UINT1) MST_DISABLED))
#else
    if (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == (UINT1) MST_DISABLED)
#endif
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without "
                 "processing.\n");
        return MST_SUCCESS;
    }

    if (AstPbAllCVlanCompDisable () != RST_SUCCESS)
    {
        return MST_FAILURE;
    }

    /*This function is called for the SVLAN component Disable */
    if (MstComponentDisable () != MST_SUCCESS)
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN MSTP Module Disable is not proper...Failures Occurred !!!\n");

        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: SVLAN MSTP Module Disable is not proper...Failures Occurred !!!\n");

        return MST_FAILURE;
    }

    AST_ADMIN_STATUS = (UINT1) MST_DISABLED;
    gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] = MST_DISABLED;
#ifdef ECFM_WANTED
    EcfmMstpDisableIndication (AST_CURR_CONTEXT_ID ());
#endif

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstComponentDisable                        */
/*                                                                           */
/*    Description               : It disables the MST Module by disabling all*/
/*                                State Machines and releases all the memory */
/*                                occupied by the MST module. Stop all the   */
/*                                timers that are running.                   */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstComponentDisable (VOID)
#else
INT4
MstComponentDisable ()
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2PortNum = 0;
    INT2                i2MstInst = 0;
    INT4                i4RetVal = MST_SUCCESS;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
             "SYS: Disabling MSTP Module...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Disabling MSTP Module...\n");

    if (AST_MODULE_STATUS == MST_DISABLED)
    {
        AST_DBG (AST_INIT_SHUT_DBG,
                 "SYS: Module Already Disabled...Quitting without processing.\n");
        return MST_SUCCESS;
    }
    AST_GET_SYSTEMACTION = MST_DISABLED;

    /* CIST info should be deleted last */
    for (i2MstInst = AST_MAX_MST_INSTANCES - 1; i2MstInst >= 0; i2MstInst--)
    {
        pPerStInfo = AST_GET_PERST_INFO (i2MstInst);
        if (pPerStInfo == NULL)
        {
            continue;
        }

        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
            {
                if (i2MstInst == MST_CIST_CONTEXT)
                {
                    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;
                }

                if (pPortInfo->bLoopInconsistent == MST_TRUE)
                {
                    pPortInfo->bLoopInconsistent = MST_FALSE;
                }

                pPerStPortInfo->bLoopIncStatus = MST_FALSE;

                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if (pRstPortInfo->bPortEnabled == MST_TRUE)
                {
                    if (i2MstInst == MST_CIST_CONTEXT)
                    {
                        RstPortMigrationMachine (RST_PMIGSM_EV_PORT_DISABLED,
                                                 u2PortNum);
                        RstPortTransmitMachine (RST_PTXSM_EV_TX_DISABLED,
                                                pPortInfo, MST_CIST_CONTEXT);
                    }

                    if (MstPortInfoMachine (MST_PINFOSM_EV_PORT_DISABLED,
                                            pPerStPortInfo, NULL,
                                            (UINT2) i2MstInst) != MST_SUCCESS)
                    {
                        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                                      AST_CONTROL_PATH_TRC,
                                      "SYS: Port Info Machine Returned failure for Port %s Instance %d for PORT_DISABLED event!!!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      i2MstInst);
                        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "SYS: Port Info Machine Returned failure for Port %s Instance %d for PORT_DISABLED event!!!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      i2MstInst);
                        i4RetVal = MST_FAILURE;
                    }
                    if (i2MstInst == MST_CIST_CONTEXT)
                    {
                        RstBrgDetectionMachine (RST_BRGDETSM_EV_PORTDISABLED,
                                                u2PortNum);
                    }
                }
            }
            if (RstStopAllRunningTimers (pPerStInfo, pPortInfo, pPerStPortInfo)
                != RST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG |
                              AST_INIT_SHUT_DBG,
                              "SYS: Unable to stop the running timers for port %s instance %d!!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), i2MstInst);
                i4RetVal = MST_FAILURE;
            }
        }
    }

    /* Incrementing the MSTP Down Count */
    AST_INCR_ASTP_DOWN_COUNT ();

    AST_MODULE_STATUS_SET (MST_DISABLED);

    if (i4RetVal != MST_SUCCESS)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: MSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");
        AST_TRC (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                 "SYS: MSTP Module Disable is not proper...Failures Occurred !!!\n"
                 "          Still Continuing ...\n");
    }
    else
    {
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: MSTP Module Disabled.\n");
        AST_DBG (AST_INIT_SHUT_DBG, "SYS: MSTP Module Disabled.\n");
        if (MstHwDeInit () != MST_SUCCESS)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "SYS: HwDeInit call FAILED...!!!\n");
            i4RetVal = MST_FAILURE;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstCreateInstance                          */
/*                                                                           */
/*    Description               : This function  creates a new instance and  */
/*                                all the ports for that instance            */
/*                                                                           */
/*    Input(s)                  : u2MstInst - Instance Identifirer           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstCreateInstance (UINT2 u2MstInst, tAstMstVlanList MstVlanList)
#else
INT4
MstCreateInstance (u2MstInst, MstVlanList)
     UINT2               u2MstInst;
     tAstMstVlanList     MstVlanList;
#endif
{
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = 0;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStRstPortInfo *pCistRstPortInfo = NULL;
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
#endif /* NPAPI_WANTED */

    if (!AST_IS_MST_STARTED ())
    {
        return MST_SUCCESS;
    }

    if (u2MstInst != AST_TE_MSTID)
    {
        pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
        /* Return success if the instance is already existing
         * and it is active */
        if (pPerStInfo != NULL && pPerStInfo->u1RowStatus == ACTIVE)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                          " Spanning Tree Instance already active - %d \n",
                          u2MstInst);
            return MST_SUCCESS;
        }

        if (AST_GET_NO_OF_ACTIVE_INSTANCES > AST_GET_MAX_MST_INSTANCES)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          " Maximum Spanning Tree Instances already active - %d \n",
                          AST_GET_NO_OF_ACTIVE_INSTANCES);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          " Maximum Spanning Tree Instances already active - %d \n",
                          AST_GET_NO_OF_ACTIVE_INSTANCES);
            return MST_FAILURE;
        }
    }

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        if ((AST_IS_MST_ENABLED ()))
        {
            i4HwRetVal = MstpFsMiMstpNpCreateInstance (AST_CURR_CONTEXT_ID (),
                                                       u2MstInst);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              " Creating instance %d in the hardware failed! \n",
                              u2MstInst);
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                              " Creating instance %d in the hardware failed! \n",
                              u2MstInst);
                return MST_FAILURE;
            }

        }
    }
#endif /* NPAPI_WANTED */

    /* For Active instances, Not for TE_MSTID */
    if (u2MstInst != AST_TE_MSTID)
    {
        /* MstInitInstance function() must be invoked only once for each
         * instance. Later VLAN mapping changes should not invoke
         * this function*/
        if (pPerStInfo == NULL)
        {
            if (MstInitInstance (u2MstInst) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              " Creating instance %d failed! \n", u2MstInst);
#ifdef NPAPI_WANTED
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    if ((AST_IS_MST_ENABLED ()))
                    {
                        i4HwRetVal =
                            MstpFsMiMstpNpDeleteInstance (AST_CURR_CONTEXT_ID
                                                          (), u2MstInst);
                    }
                }
#endif /* NPAPI_WANTED */
                return MST_FAILURE;
            }
        }

        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }
            /* If the port is SISP enabled, then it should be part of the 
             * instance if and only if the interface is associated with the 
             * vlan to which this instance is mapped to.
             * */

            if (AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)
            {
                if (MstSispIsVlansPresentInInst (u2PortNum, u2MstInst, NULL,
                                                 MstVlanList) == MST_FALSE)
                {
                    AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

                    AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                                  "API: Port %s: Validation for Instance to port"
                                  " returned failure for Inst %s!!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    continue;
                }

            }

            if (MstCreatePerStPortEntry (u2PortNum, u2MstInst, MST_FALSE) !=
                MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                              "API: Port %s Inst %d:  Creating Instance Specific info Failed...\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                return MST_FAILURE;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2MstInst);
            pCistRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum,
                                                            MST_CIST_CONTEXT);

            if (pCistRstPortInfo->bPortEnabled == MST_FALSE)
            {
                /* Set the port as disabled for this instance if the port
                 * is disabled for CIST */
                pRstPortInfo->bPortEnabled = MST_FALSE;
            }

            /* Instance is getting newly created on this port
             * and the port pathcost has already been calculated.
             * Copy the calculate path cost for the port
             * for the instance*/
            pPerStPortInfo->u4PortPathCost = pPortInfo->u4PathCost;

            if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
            {
                if (pRstPortInfo->bPortEnabled == MST_FALSE)
                {
                    if (AstL2IwfGetInstPortState
                        (u2MstInst,
                         AST_IFENTRY_IFINDEX (pPortInfo)) !=
                        AST_PORT_STATE_FORWARDING)
                    {
                        /* The port is disabled in spanning tree for this instance.
                         * Put the port in FORWARDING state */
                        AstSetInstPortStateToL2Iwf (u2MstInst, u2PortNum,
                                                    AST_PORT_STATE_FORWARDING);

                        AST_GET_LAST_PROGRMD_STATE (u2MstInst, u2PortNum) =
                            AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                        {
                            FsMiMstpNpWrSetInstancePortState
                                (AST_CURR_CONTEXT_ID (),
                                 AST_GET_PHY_PORT (u2PortNum), u2MstInst,
                                 AST_PORT_STATE_FORWARDING);
                        }
#endif /* NPAPI_WANTED */
                    }
                    continue;
                }
            }
        }

        L2IwfCreateSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst);
        pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
        pPerStInfo->u1RowStatus = ACTIVE;
    }                            /* End of Active Instnaces creation */

    else                        /* For TE_MSTID, the port state should be forwarding */
    {
        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                                  AST_GET_PHY_PORT (u2PortNum),
                                                  u2MstInst,
                                                  AST_PORT_STATE_FORWARDING);
            }
#endif /* NPAPI_WANTED */
            /*Non-Blocking Vlan suppose to be updated for Static Configuration.
             * So no need of port-state updation in GARP for TE_MSTID.*/

            AstSetInstPortStateToL2Iwf (u2MstInst, u2PortNum,
                                        AST_PORT_STATE_FORWARDING);
        }

        (AST_CURR_CONTEXT_INFO ()->u1TEMSTIDValid) = MST_TRUE;
    }                            /* End of TE_MSTID Instance creation */

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDeleteInstance                          */
/*                                                                           */
/*    Description               : This function will unmap all the Vlans from*/
/*                                given spanning tree instance and deletes   */
/*                                the instance.                              */
/*                                                                           */
/*    Input(s)                  : u2MstInst - Instance Identifirer           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstDeleteInstance (UINT2 u2MstInst)
#else
INT4
MstDeleteInstance (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2PortNum = 0;
    tAstPortEntry      *pPortInfo = NULL;

#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
#endif /* NPAPI_WANTED */

    if (!AST_IS_MST_STARTED ())
    {
        return MST_SUCCESS;
    }
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  " Deleting Spanning Tree Instance %d \n", u2MstInst);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  " Deleting Spanning Tree Instance %d \n", u2MstInst);

    if (u2MstInst == AST_TE_MSTID)
    {
        MstVlanInstUnMap (u2MstInst);
        (AST_CURR_CONTEXT_INFO ()->u1TEMSTIDValid) = MST_FALSE;
    }
    pPerStInfo = AST_GET_PERST_INFO (u2MstInst);
    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                      " Spanning Tree Instance not active - %d \n", u2MstInst);
        return MST_SUCCESS;
    }

    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }

        if (MstDeletePerStPortEntry (u2PortNum, u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "DeletePort: Port %s Inst %d: DeletePerStPortEntry returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        }
    }
    /* Unmap all vlans mapped to this instance and remap to CIST */
    MstVlanInstUnMap (u2MstInst);

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        if ((AST_IS_MST_ENABLED ()))
        {
            i4HwRetVal = MstpFsMiMstpNpDeleteInstance (AST_CURR_CONTEXT_ID (),
                                                       u2MstInst);

            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                              " Deleting instance %d in the hardware failed! \n",
                              u2MstInst);
                return MST_FAILURE;
            }
        }
    }
#endif /* NPAPI_WANTED */

    if (u2MstInst != AST_TE_MSTID)
    {
        L2IwfDeleteSpanningTreeInstance (AST_CURR_CONTEXT_ID (), u2MstInst);
    }

    if (AST_GET_NO_OF_ACTIVE_INSTANCES)
    {
        AST_GET_NO_OF_ACTIVE_INSTANCES--;
    }
    AST_MEMSET (pPerStBrgInfo, AST_INIT_VAL, sizeof (tAstPerStBridgeInfo));

    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
    {
        return MST_SUCCESS;
    }
    AstDeleteMstInstance (u2MstInst, pPerStInfo);

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: Instance %d deleted successfully\n", u2MstInst);
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Instance %d deleted successfully\n", u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstCreatePort                              */
/*                                                                           */
/*    Description               : It Allocates memory for the port.          */
/*                                Initialises all the State Machines related */
/*                                to the port as per the operational status  */
/*                                of the port.                               */
/*                                                                           */
/*    Input(s)                  : u4IfIndex - Global IfIndex of the port     */
/*                                u2PortNum - Local per context Port Number  */
/*                                            of the port that is to be      */
/*                                            created.                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortInfo = NULL;
    tAstCfaIfInfo       CfaIfInfo;
    UINT1               u1IfType = CFA_ENET;

    UINT2               u2MstInst = 1;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    AST_MEMSET (au1IfName, AST_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    AST_MEMSET (&CfaIfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Creating port %u ... \n", u4IfIndex);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Creating port %u ... \n", u4IfIndex);

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) != NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Already created \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;
    }

    /* Check whether the interface type is valid. Currently 
     * ethernet or PPP or ATMVC are valid interfaces.
     */
    if (AstCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfType for Port %u\n", u4IfIndex);
        return MST_SUCCESS;
    }

    u1IfType = CfaIfInfo.u1IfType;

    if ((AST_INTF_TYPE_VALID (u1IfType)) == RST_FALSE)
    {
        return MST_SUCCESS;
    }

    if (CfaIfInfo.u1BrgPortType == AST_PROVIDER_INSTANCE_PORT)
    {
        return MST_SUCCESS;
    }

    if (AST_ALLOC_PORT_INFO_MEM_BLOCK (pPortInfo) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                 " Port Info Memory Block Allocation failed!!!\n");
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %u: Port Info Memory Block Allocation failed !!!\n",
                      u4IfIndex);
        return MST_FAILURE;
    }

    AST_MEMSET (pPortInfo, AST_INIT_VAL, sizeof (tAstPortEntry));
    AST_GET_PORTENTRY (u2PortNum) = pPortInfo;

    pPortInfo->u4ContextId = AST_CURR_CONTEXT_ID ();
    pPortInfo->u4IfIndex = u4IfIndex;
    CfaCliConfGetIfName (u4IfIndex, piIfName);
#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
    MEMSET (pPortInfo->au1StrIfIndex, 0, CFA_MAX_PORT_NAME_LENGTH);
    SNPRINTF ((CHR1 *) pPortInfo->au1StrIfIndex, CFA_MAX_PORT_NAME_LENGTH, "%s",
              piIfName);
#endif

    MEMCPY (pPortInfo->au1PortMACAddr, CfaIfInfo.au1MacAddr, AST_MAC_ADDR_SIZE);
    AstAddToIfIndexTable (pPortInfo);

    MstInitGlobalPortInfo (u2PortNum, pPortInfo);
    /* Path cost will be calculated from speed when the link is up.
     * Until then mark the path cost as uninitialised */
    pPortInfo->bPathCostInitialized = MST_FALSE;

    /* Depending on the Bridge Mode, the Port type should be assigned. */
    AstUpdatePortType (pPortInfo, CfaIfInfo.u1BrgPortType);
    pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;
    /* For protocol operations (comparisons etc.,) too, the same
     * port number will be used. */
    pPortInfo->u2ProtocolPort = u2PortNum;

    if (MstSispDeriveSispDefaults (u2PortNum) != MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      " CreatePort: Port %s: DeriveSispDefaults returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);
        AST_GET_PORTENTRY (u2PortNum) = NULL;

        return MST_FAILURE;
    }

    if (MstCreatePerStPortEntry (u2PortNum, MST_CIST_CONTEXT, MST_TRUE) !=
        MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      " CreatePort: Port %s: CreatePerStPortEntry returned failure for CIST !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        (VOID) AST_RELEASE_PORT_INFO_MEM_BLOCK (pPortInfo);
        AST_GET_PORTENTRY (u2PortNum) = NULL;

        return MST_FAILURE;
    }

    if (MstCreateInstancesForPort (u2PortNum) != MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      " CreatePort: Port %s: CreateInstanesForPort returned failure  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_ENABLED);

    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                  "SYS: CreatePort: Completed creating port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Completed creating port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_ALLOC_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);
    if (pRstPortInfo == NULL)
    {
        return RST_FAILURE;
    }
    AST_MEMSET (pRstPortInfo, 0, sizeof (tRstPortInfo));
    pRstPortInfo->pPortInfo = pPortInfo;
    pRstPortInfo->u2PortNum = u2PortNum;
    AST_ADD_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                      (tTMO_SLL_NODE *) & (pRstPortInfo->NextNode));

    for (u2MstInst = 0; u2MstInst <= AST_GET_NO_OF_ACTIVE_INSTANCES;
         u2MstInst++)
    {
        if (AST_GET_PERST_INFO (u2MstInst) == NULL)
            continue;

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
        if (pPerStPortInfo == NULL)
        {
            /* Port already part of this instance */
            continue;
        }

        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                               MST_SYNC_BSELECT_UPDATE);
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstCreatePerStPortEntry                    */
/*                                                                           */
/*    Description               : It Allocates memory for the port in        */
/*                                the specified instance's context.          */
/*                                Calls MstStartSemsForPort to               */
/*                                initialise  all the State Machines related */
/*                                to the port as per the instance.           */
/*                                                                           */
/*    Input(s)                 :  u2PortNum - Port Number of the port that   */
/*                                            is to be created.              */
/*                                u2MstInst  - Instance Identifier           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstCreatePerStPortEntry (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1IsStartSems)
#else
INT4
MstCreatePerStPortEntry (u2PortNum, u2MstInst, u1IsStartSems)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4IfIndex;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1L2IwfPortState = AST_PORT_STATE_DISCARDING;
#endif

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Creating Per Spanning Tree Port %s for instance %d ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Creating Per Spanning Tree Port %s for instance %d ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    if ((AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: CreatePerStPortEntry: Port Entry not created \n");
        return MST_FAILURE;
    }
    if (AST_ALLOC_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo) == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC |
                 AST_OS_RESOURCE_TRC,
                 " CreatePerStPortEntry: PerSt Port Entry Memory Block allocation failed \n");
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG | AST_MEM_DBG,
                      "SYS: Port %s: PerSt Port Info Memory Block Allocation failed !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        return MST_FAILURE;
    }

    AST_MEMSET (pPerStPortInfo, AST_INIT_VAL, sizeof (tAstPerStPortInfo));
    AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = pPerStPortInfo;

#ifdef NPAPI_WANTED
    if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE))
    {
        if (AstVcmGetIfIndexFromLocalPort
            (AST_CURR_CONTEXT_ID (), u2PortNum, &u4IfIndex) == VCM_FAILURE)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "CreatePerStPortEntry: Port %s Inst %d: VlanVcmGetIfIndexFromLocalPort returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
            AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

            return MST_FAILURE;

        }
        if (MstpFsMiMstNpGetPortState (AST_CURR_CONTEXT_ID (), u2MstInst,
                                       u4IfIndex, &u1PortState) == FNP_SUCCESS)
        {
            u1L2IwfPortState = AstGetInstPortStateFromL2Iwf
                (u2MstInst, u2PortNum);
            if (u1L2IwfPortState != u1PortState)
            {
                /*update hardware with the control plane port state */
                RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                            u4IfIndex, u1L2IwfPortState);
            }
        }
    }
#endif

    MstInitPerStPortInfo (u2PortNum, u2MstInst, pPerStPortInfo);
    /* Path cost will be calculated from speed when the link comes up. */

    /* Fix for Silver Creek: If it does Get on any port 
     * that is down we will return 0 which is outside the mib range */
    pPerStPortInfo->u4PortPathCost = AstCalculatePathcost (u2PortNum);
    if (u2MstInst != MST_CIST_CONTEXT)
    {
        if (MstSispUpdatePortInstList (u2PortNum, u2MstInst, MST_ADD) ==
            MST_FAILURE)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "CreatePerStPortEntry: Port %s Inst %d: UpdtPortInstList returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
            AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

            return MST_FAILURE;

        }
    }

    if (AST_MODULE_STATUS == MST_ENABLED)
    {
        /* For a CEP Port, the State machines need not be restarted */
        if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) != MST_TRUE)
        {
            if (u1IsStartSems == MST_TRUE)
            {
                if (MstStartSemsForPort (u2PortNum, u2MstInst) == MST_FAILURE)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "CreatePerStPortEntry: Port %s Inst %d: StartSems returned failure !\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    (VOID)
                        AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
                    AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

                    return MST_FAILURE;
                }
            }
        }
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Completed creating port %s for Instance %d\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: CreatePerStPortEntry: Completed creating port %s for Instance %d\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDeletePort                              */
/*                                                                           */
/*    Description               : Disables the port if port Operational      */
/*                                status is UP. Then it releases the memory  */
/*                                allocated for the port. Stops all the      */
/*                                timer which are running for this port.     */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the port that   */
/*                                            is to be deleted.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstDeletePort (UINT2 u2PortNum)
#else
INT4
MstDeletePort (u2PortNum)
     UINT2               u2PortNum;
#endif
{
    tAstPortEntry      *pPortInfo = NULL;
    tRstPortInfo       *pRstPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    INT2                i2MstInst = 0;
    UINT4               u4IfIndex;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Deleting Port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleting Port %s...\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    if (AST_IS_PORT_VALID (u2PortNum) == AST_FALSE)
    {
        /* Port number exceeds the allowed range */
        AST_TRC (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                 "SYS: Delete Port for invalid port number !!!\n");
        return MST_FAILURE;
    }

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: DeletePort: Port does not exist !!!\n");
        return MST_FAILURE;
    }

    /* If the Delete Port indication comes for a CEP, delete the CVLAN component
     * and its all PEPs and also delete the port from the SVLAN context*/
    /* The Deletion of Port in CVLAN component should be called from SVLAN
     * context*/
    if (AstPbCVlanCompShutdown (pPortInfo) != RST_SUCCESS)
    {
        return MST_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);
    pPerStRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    pPerStRstPortInfo->bPortEnabled = (tAstBoolean) RST_FALSE;

    for (i2MstInst = AST_MAX_MST_INSTANCES - 1; i2MstInst >= 0; i2MstInst--)
    {
        if (AST_GET_PERST_INFO (i2MstInst) == NULL)
        {
            continue;
        }
        if (MstDeletePerStPortEntry (u2PortNum, (UINT2) i2MstInst) !=
            MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "DeletePort: Port %s Inst %d: DeletePerStPortEntry returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), i2MstInst);
        }
    }

    pCommPortInfo = &(pPortInfo->CommPortInfo);
    if (pCommPortInfo->pHelloWhenTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_HELLOWHEN)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for HelloWhenTmr FAILED!\n");
            return MST_FAILURE;
        }
    }

    if (pCommPortInfo->pRapidAgeDurtnTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPortInfo, AST_TMR_TYPE_RAPIDAGE_DURATION) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RapidAgeDuration timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    if (pCommPortInfo->pMdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_MDELAYWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for MdWhileTmr FAILED!\n");
            return MST_FAILURE;
        }
    }

    if (pCommPortInfo->pHoldTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_HOLD) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for HoldTmr FAILED!\n");
            return MST_FAILURE;
        }
    }

    if (pCommPortInfo->pEdgeDelayWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_EDGEDELAYWHILE)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for EdgeDelayWhile Timer FAILED!\n");
            return MST_FAILURE;
        }
    }

    if (pCommPortInfo->pPseudoInfoHelloWhenTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_PSEUDOINFOHELLOWHEN)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for PseudoInfoHelloWhen Timer FAILED!\n");
            return MST_FAILURE;
        }

    }

    if (pCommPortInfo->pDisableRecoveryTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo,
                          AST_TMR_TYPE_ERROR_DISABLE_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "TMR: AstStopTimer for disable recovery  Timer FAILED!\n");
            return MST_FAILURE;
        }

    }
    if (pCommPortInfo->pRapidAgeDurtnTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPortInfo, AST_TMR_TYPE_RAPIDAGE_DURATION) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for RapidAgeDuration timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    u4IfIndex = AST_IFENTRY_IFINDEX (pPortInfo);

    if (AST_COUNT_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList) != 0)
    {
        AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                           pRstPortInfo, tRstPortInfo *)
        {
            if (pRstPortInfo->pPortInfo == pPortInfo)
            {
                AST_DELETE_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                                     pRstPortInfo);
                AST_RELEASE_RST_PORTINFO_MEM_BLOCK (pRstPortInfo);
                break;
            }
        }

    }

    /* Remove this node from the IfIndex RBTree and the PerContext DLL */
    AstReleasePortMemBlocks (pPortInfo);

    AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                            u2PortNum, L2_PROTO_STP,
                                            OSIX_DISABLED);

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Deleted Port %u\n", u4IfIndex);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Deleted Port %u\n", u4IfIndex);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDeletePerStPortEntry                    */
/*                                                                           */
/*    Description               : Disables the PerStPortEntry if port        */
/*                                Operational status is UP. Then it releases */
/*                                the memory allocated for the PerStPortInfo.*/
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number of the port that   */
/*                                            is to be deleted.              */
/*                                u2MstInst - Spanning Tree Instance         */
/*                                            Identifier                     */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstDeletePerStPortEntry (UINT2 u2PortNum, UINT2 u2MstInst)
#else
INT4
MstDeletePerStPortEntry (u2PortNum, u2MstInst)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s Inst %d: Deleting Instance Specific info ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s Inst %d: Deleting Instance Specific info ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        AST_DBG (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                 "SYS: DeletePerStPortEntry: Port does not exist !!!\n");
        return MST_FAILURE;
    }
    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: DeletePerStPortEntry: Instance %d does not exist !!!\n",
                      u2MstInst);
        return MST_SUCCESS;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
    if (pPerStPortInfo == NULL)
    {
        return MST_SUCCESS;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if (AST_MODULE_STATUS == MST_ENABLED)
    {
        if (pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP)
        {
            if (u2MstInst == MST_CIST_CONTEXT)
            {
                if (RstPortMigrationMachine
                    ((UINT2) RST_PMIGSM_EV_PORT_DISABLED,
                     u2PortNum) != RST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                                  "SYS: Port %s: Port Protocol Migration Machine Returned failure for PORT_DISABLED event !!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                                  AST_ALL_FAILURE_DBG,
                                  "SYS: Port %s: Port Protocol Migration Machine Returned failure for PORT_DISABLED event !!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                }
            }
            if (MstPortInfoMachine
                (MST_PINFOSM_EV_PORT_DISABLED, pPerStPortInfo, NULL, u2MstInst))
            {
                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_DISABLED event !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                              AST_ALL_FAILURE_DBG,
                              "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_DISABLED event !!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            }
            if (u2MstInst == MST_CIST_CONTEXT)
            {
                RstBrgDetectionMachine (RST_BRGDETSM_EV_PORTDISABLED,
                                        u2PortNum);
            }
        }
    }

    if (pRstPortInfo->pFdWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_FDWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for FdWhileTmr FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    if (pRstPortInfo->pRbWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RBWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for RbWhileTmr FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    if (pRstPortInfo->pRcvdInfoTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RCVDINFOWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for RcvdInfoTmr FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    if (pRstPortInfo->pRrWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_RRWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for RrWhileTmr FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    if (pRstPortInfo->pTcWhileTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_TCWHILE)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for TcWhileTmr FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    if (u2MstInst != MST_CIST_CONTEXT)
    {
        if (MstSispUpdatePortInstList (u2PortNum, u2MstInst, MST_DELETE) ==
            MST_FAILURE)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "CreatePerStPortEntry: Port %s Inst %d: UpdtPortInstList returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            return MST_FAILURE;
        }
    }
    if (pRstPortInfo->pRootIncRecTmr != NULL)
    {
        if (AstStopTimer
            ((VOID *) pPerStPortInfo,
             AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for Root Inconsistency Recovery FAILED!\n");
            return RST_FAILURE;
        }
    }

#ifdef MRP_WANTED
    if (pRstPortInfo->pTcDetectedTmr != NULL)
    {
        if (AstStopTimer ((VOID *) pPerStPortInfo, AST_TMR_TYPE_TCDETECTED)
            != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TMR_DBG | AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                          "TMR: Port %s Inst %d: AstStopTimer for TcDetectedTmr"
                          " FAILED !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }
#endif
    if (AST_CURR_CONTEXT_INFO ()->pRestartTimer != NULL)
    {
        if (AstStopTimer ((VOID *) pPortInfo, AST_TMR_TYPE_RESTART)
            != RST_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: AstStopTimer for Restart Timer FAILED!\n");
            return RST_FAILURE;
        }
    }

    (VOID) AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK (pPerStPortInfo);
    AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s Inst %d: Successfully deleted Instance Specific info \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s Inst %d: Successfully deleted Instance Specific info \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstEnablePort                              */
/*                                                                           */
/*    Description               : Enables the state machine for the port     */
/*                                with 'Port Enabled' event.                 */
/*                                                                           */
/*    Input(s)                  : u2PortNum    - Port Number of the port     */
/*                                               which is to be enabled.     */
/*                                u2MstInst  - Spanning Tree Instance Id.    */
/*                                                                           */
/*                                u1TrigType - Indicates whether the Port    */
/*                                              is enabled at Bridge/CFA     */
/*                                              Level or at STP Level        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstEnablePort (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1TrigType)
#else
INT4
MstEnablePort (u2PortNum, u2MstInst, u1TrigType)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
     UINT1               u1TrigType;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pTmpRstPortInfo = NULL;
    tAstPerStInfo      *pTmpPerStInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstBoolean         portUpdated = RST_FALSE;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2TmpMstInst;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Enabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Enabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }
    else
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Port %s enabled in CFA. Enabling Port in MSTP...\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Port %s enabled in CFA. Enabling Port in MSTP...\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (MstSispDeriveSispDefaults (u2PortNum) != MST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      " CreatePort: Port %s: DeriveSispDefaults returned failure !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        return MST_FAILURE;
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
    }
    else                        /* u1TrigType == AST_STP_PORT_UP */
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
    }

    if (pPerStPortInfo == NULL)
    {
        /* This port is not part of the instance */
        return MST_SUCCESS;
    }

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        if (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_TRUE)
        {
            return MST_SUCCESS;
        }

        pRstPortInfo->bAdminPortEnabled = (tAstBoolean) MST_TRUE;

        if (u2MstInst != MST_CIST_CONTEXT)
        {
            pTmpPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                         MST_CIST_CONTEXT);

            pTmpRstPortInfo = AST_GET_RST_PORT_INFO (pTmpPerStPortInfo);

            if (pTmpRstPortInfo->bPortEnabled == MST_FALSE)
            {
                /* Port is still disabled in CIST. 
                 * Do not process MSTI Port enable */
                return MST_SUCCESS;
            }
        }

        pRstPortInfo->bPortEnabled = (tAstBoolean) MST_TRUE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            u2TmpMstInst = 1;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                pTmpRstPortInfo =
                    AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2TmpMstInst);
                if (pTmpRstPortInfo == NULL)
                {
                    continue;
                }
                pTmpRstPortInfo->bPortEnabled =
                    pTmpRstPortInfo->bAdminPortEnabled;
            }
        }
        MstUpdtProtoPortStateInL2Iwf (u2PortNum, u1TrigType);

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return MST_SUCCESS;
        }
    }

    /* The value of Oper P2P is updated while enabling the port if Admin P2P
     * is Auto, by triggering LA, as the aggregatable status of the port may
     * have changed in LA from Aggregatable to Individual in which case,
     * first a Down indication will be given followed by an Up indication.*/
    portUpdated = pPortInfo->bIEEE8021apAdminP2P;
    AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);
    pPortInfo->bIEEE8021apAdminP2P = portUpdated;
    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_DOWN)
        {
            return MST_SUCCESS;
        }
        if (pCommPortInfo != NULL)
        {
            pCommPortInfo->bBpduInconsistent = AST_FALSE;
        }
        /* The Port path cost is updated if there is any change in link speed
         * when a link up trigger is received - 
         * if the pathcost has NOT already been configured for this port
         * and if either dynamic pathcost calculation is enabled
         *        or if the port is coming up for the first time after creation
         */
        pAstBridgeEntry = AST_GET_BRGENTRY ();

        if ((pAstBridgeEntry->u1DynamicPathcostCalculation == MST_TRUE) ||
            (pPortInfo->bPathCostInitialized == MST_FALSE))
        {
            pPortInfo->u4PathCost = AstCalculatePathcost (u2PortNum);

            pPortInfo->bPathCostInitialized = MST_TRUE;

            u2TmpMstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                pTmpPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                             u2TmpMstInst);
                if (pTmpPerStPortInfo == NULL)
                {
                    continue;
                }

                if (AstPathcostConfiguredFlag (u2PortNum, u2TmpMstInst)
                    == MST_FALSE)
                {
                    pTmpPerStPortInfo->u4PortPathCost = pPortInfo->u4PathCost;
                }
            }
        }
    }

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC | AST_SM_VAR_DBG,
                  "MstEnablePort: Port %s: AutoEdge = TRUE; AdminEdge = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if (!(AST_IS_MST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port Up event should be processed.
         * If Cfa Port Up event comes, directly set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_UP)
        {
            AstSetInstPortStateToL2Iwf (MST_CIST_CONTEXT, u2PortNum,
                                        AST_PORT_STATE_FORWARDING);

            AST_GET_LAST_PROGRMD_STATE (MST_CIST_CONTEXT, u2PortNum) =
                AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                            AST_GET_PHY_PORT (pPortInfo->
                                                              u2PortNo),
                                            AST_PORT_STATE_FORWARDING);
            }
#endif /*NPAPI_WANTED */
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module disabled. Returning...\n");
        return MST_SUCCESS;
    }

    if (u1TrigType == (UINT1) AST_STP_PORT_UP)
    {
        if (AstL2IwfGetInstPortState (u2MstInst,
                                      AST_IFENTRY_IFINDEX (pPortInfo))
            != AST_PORT_STATE_DISCARDING)
        {
            /* 
             * Spanning tree is now being enabled on this port. 
             * Put the port in DISCARDING state (previously it could
             * have been in FORWARDING since spanning tree was disabled 
             * on this port) 
             */
            AstSetInstPortStateToL2Iwf (u2MstInst, pPortInfo->u2PortNo,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (u2MstInst, pPortInfo->u2PortNo) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                                  AST_GET_PHY_PORT
                                                  (pPortInfo->u2PortNo),
                                                  u2MstInst,
                                                  AST_PORT_STATE_DISCARDING);
            }
#endif /* NPAPI_WANTED */

        }

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            u2TmpMstInst = 1;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                pTmpRstPortInfo =
                    AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2TmpMstInst);
                if (pTmpRstPortInfo == NULL)
                {
                    continue;
                }
                if (pTmpRstPortInfo->bPortEnabled == MST_FALSE)
                {
                    continue;
                }

                if (AstL2IwfGetInstPortState
                    (u2TmpMstInst,
                     AST_IFENTRY_IFINDEX (pPortInfo)) !=
                    AST_PORT_STATE_DISCARDING)
                {
                    /* 
                     * Spanning tree is now being enabled on this port. 
                     * Put the port in DISCARDING state (previously it could
                     * have been in FORWARDING since spanning tree was disabled 
                     * on this port) 
                     */
                    AstSetInstPortStateToL2Iwf (u2TmpMstInst,
                                                pPortInfo->u2PortNo,
                                                AST_PORT_STATE_DISCARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2TmpMstInst,
                                                pPortInfo->u2PortNo) =
                        AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID
                                                          (),
                                                          AST_GET_PHY_PORT
                                                          (pPortInfo->u2PortNo),
                                                          u2TmpMstInst,
                                                          AST_PORT_STATE_DISCARDING);
                    }
#endif /* NPAPI_WANTED */
                }
            }
            /*Flush port for FIDs belonging to CIST and hence for FIDs 
               belonging to all instances */
            MstTopologyChSmFlushPort (pPerStPortInfo);
        }
        else
        {
            /*Flush port for a particular instance's fid */
            MstTopologyChSmFlushFdb (pPerStPortInfo,
                                     u2MstInst, VLAN_NO_OPTIMIZE);
        }
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_UP;

        if (pRstPortInfo->bPortEnabled == MST_FALSE)
        {
            /* STP Port Status is disabled in CIST and hence 
             * in all instances */

            u2TmpMstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                if (AstL2IwfGetInstPortState
                    (u2TmpMstInst, AST_IFENTRY_IFINDEX (pPortInfo)) !=
                    AST_PORT_STATE_FORWARDING)
                {
                    /* 
                     * The port is now being enabled at the bridge level. 
                     * But the port is disabled in spanning tree for this 
                     * instance. Put the port in FORWARDING state (previously 
                     * it could have been in DISCARDING since the port was 
                     * disabled at the bridge level) 
                     */
                    AstSetInstPortStateToL2Iwf (u2TmpMstInst,
                                                pPortInfo->u2PortNo,
                                                AST_PORT_STATE_FORWARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2TmpMstInst,
                                                pPortInfo->u2PortNo) =
                        AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID
                                                          (),
                                                          AST_GET_PHY_PORT
                                                          (pPortInfo->u2PortNo),
                                                          u2TmpMstInst,
                                                          AST_PORT_STATE_FORWARDING);
                    }
#endif /* NPAPI_WANTED */

                }

            }

            return MST_SUCCESS;
        }
        else
        {
            /* STP Port Status is enabled in CIST.
             * So change the STP Port Status for all instances
             * according to the admin port state value */
            u2TmpMstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                pTmpRstPortInfo =
                    AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2TmpMstInst);

                if (pTmpRstPortInfo == NULL)
                {
                    continue;
                }

                if (pTmpRstPortInfo->bPortEnabled == MST_TRUE)
                {
                    continue;
                }
                if (AstL2IwfGetInstPortState
                    (u2TmpMstInst, AST_IFENTRY_IFINDEX (pPortInfo)) !=
                    AST_PORT_STATE_FORWARDING)
                {
                    /* 
                     * The port is now being enabled at the bridge level. 
                     * But the port is disabled in spanning tree for this 
                     * instance. Put the port in FORWARDING state (previously 
                     * it could have been in DISCARDING since the port was 
                     * disabled at the bridge level) 
                     */
                    AstSetInstPortStateToL2Iwf (u2TmpMstInst,
                                                pPortInfo->u2PortNo,
                                                AST_PORT_STATE_FORWARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2TmpMstInst,
                                                pPortInfo->u2PortNo) =
                        AST_PORT_STATE_FORWARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID
                                                          (),
                                                          AST_GET_PHY_PORT
                                                          (pPortInfo->u2PortNo),
                                                          u2TmpMstInst,
                                                          AST_PORT_STATE_FORWARDING);
                    }
#endif /* NPAPI_WANTED */

                }

            }

        }
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_PORT_ENABLED,
                                     u2PortNum) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure for PORT_ENABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure for PORT_ENABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }

    if (MstPortInfoMachine (MST_PINFOSM_EV_PORT_ENABLED, pPerStPortInfo,
                            NULL, u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_ENABLED event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_ENABLED event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    /* Enable this port for all instances 
     * if this is a link up event from the lower layer module or
     * if the spanning tree is enabled for the CIST (by configuration)
     */
    if ((u1TrigType == (UINT1) AST_EXT_PORT_UP) ||
        (u2MstInst == MST_CIST_CONTEXT))
    {
        if (MstEnableInstancesForPort (u2PortNum) != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          " Enabling Instances for Port %s Failed!!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (RstPortTransmitMachine (RST_PTXSM_EV_TX_ENABLED, pPortInfo,
                                    RST_DEFAULT_INSTANCE) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Port Transmit Machine Returned failure  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
        if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_PORT_ENABLED, u2PortNum,
                                  NULL) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for Port Enabled event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for Port Enabled event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;

        }

    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (AstSetCistPortMacEnabled (u2PortNum,
                                      pPortInfo->u1AdminPointToPoint)
            == SNMP_FAILURE)
        {
            return MST_FAILURE;
        }
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Port %s successfully enabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s successfully enabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDisablePort                             */
/*                                                                           */
/*    Description               : Disables the state machine for the port    */
/*                                with 'Port Disabled' event.                */
/*                                                                           */
/*    Input(s)                  : u2PortNum    - Port Number of the port     */
/*                                               which is to be disabled.    */
/*                                u2MstInst  - Spanning Tree Instance Id.    */
/*                                u1TrigType - Indicates whether the Port    */
/*                                              is disabled at Bridge/CFA    */
/*                                              Level or at STP Level        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstDisablePort (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1TrigType)
#else
INT4
MstDisablePort (u2PortNum, u2MstInst, u1TrigType)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
     UINT1               u1TrigType;
#endif
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pTmpRstPortInfo = NULL;
    tAstPerStInfo      *pTmpPerStInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2TmpMstInst;
#ifdef ICCH_WANTED
    UINT1               u1IsMclagEnabled = 0;
#endif
    BOOL1               bOperEdge = OSIX_FALSE;

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Disabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Disabling Port %s for Instance %d...\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }
    else
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                      "SYS: Port %s disabled in CFA. Disabling Port in MSTP...\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                      "SYS: Port %s disabled in CFA. Disabling Port in MSTP...\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }

    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG | AST_INIT_SHUT_DBG,
                      "SYS: Instance %d does not exist !!!\n", u2MstInst);
        return MST_SUCCESS;
    }

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    if (pPortInfo == NULL)
    {
        return MST_FAILURE;
    }

    pPortInfo->bLoopInconsistent = MST_FALSE;
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    if (pAstCommPortInfo == NULL)
    {
        return MST_FAILURE;
    }

    if (u1TrigType == (UINT1) AST_EXT_PORT_UP)
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
    }
    else                        /* u1TrigType == AST_STP_PORT_UP */
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
    }

    if (pPerStPortInfo == NULL)
    {
        /* SISP interfaces will not be part of all the instances */
        return MST_SUCCESS;
    }

    pPerStPortInfo->bLoopIncStatus = MST_FALSE;
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (u1TrigType == (UINT1) AST_STP_PORT_DOWN)
    {
        pRstPortInfo->bAdminPortEnabled = (tAstBoolean) MST_FALSE;

        if (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_FALSE)
        {
            return MST_SUCCESS;
        }

        pRstPortInfo->bPortEnabled = (tAstBoolean) MST_FALSE;

        if (u2MstInst == MST_CIST_CONTEXT)
        {
            /* Port is disabled in MSTP for CIST. 
             * Hence set this port as disabled in all MSTI instances */
            u2TmpMstInst = 1;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                pTmpRstPortInfo
                    = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2TmpMstInst);

                if (pTmpRstPortInfo == NULL)
                {
                    continue;
                }
                pTmpRstPortInfo->bPortEnabled = MST_FALSE;
            }
        }

        MstUpdtProtoPortStateInL2Iwf (u2PortNum, u1TrigType);

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return MST_SUCCESS;
        }
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        /* If the module is disabled only Stp Port Down event should be processed.
         * If Cfa Port Down event comes, directly set the port state in h/w and 
         * return */
        if (u1TrigType == AST_EXT_PORT_DOWN)
        {
            AstSetInstPortStateToL2Iwf (MST_CIST_CONTEXT,
                                        pPortInfo->u2PortNo,
                                        AST_PORT_STATE_DISCARDING);

            AST_GET_LAST_PROGRMD_STATE (MST_CIST_CONTEXT, pPortInfo->u2PortNo) =
                AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
            if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
            {
                RstpFsMiRstpNpSetPortState (AST_CURR_CONTEXT_ID (),
                                            AST_GET_PHY_PORT (pPortInfo->
                                                              u2PortNo),
                                            AST_PORT_STATE_DISCARDING);
            }
#endif /*NPAPI_WANTED */
            if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ())
                == VLAN_TRUE)
            {
                AstVlanDeleteFdbEntries
                    ((UINT2) AST_IFENTRY_IFINDEX (pPortInfo), VLAN_NO_OPTIMIZE);
            }
            else
            {
                /* Inform the bridge to delete in its MAC table
                 * all the entries learned for this port.
                 */
#ifdef BRIDGE_WANTED
                BrgDeleteFwdEntryForPort (AST_IFENTRY_IFINDEX (pPortInfo));

#endif
            }
        }
        AST_DBG (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                 "SYS: Module disabled. Returning...\n");
        return MST_SUCCESS;
    }
    if (u1TrigType == (UINT1) AST_EXT_PORT_DOWN)
    {
        /*When STP Receives port oper down indication 
           set bpdu inconsistent state to false. 
           When BPDU is received on the port in which bpdu guard is enable, 
           the caller of this function will set the flag */
        if ((pAstCommPortInfo->bBpduInconsistent != RST_TRUE) ||
            (pAstCommPortInfo->u1BpduGuardAction != AST_PORT_ADMIN_DOWN))
        {
            pAstCommPortInfo->bBpduInconsistent = AST_FALSE;
        }
        /*Also set root inconsistency flag to false */
        pPerStPortInfo->bRootInconsistent = AST_FALSE;

        /*Stop the root inconsistency recovery timer if it is running */
        if (pRstPortInfo->pRootIncRecTmr != NULL)
        {
            if (AstStopTimer
                ((VOID *) pPerStPortInfo,
                 AST_TMR_TYPE_ROOT_INC_RECOVERY) != RST_SUCCESS)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: AstStopTimer for Root Inconsistency"
                         " Recovery timer FAILED!\n");
                return MST_FAILURE;
            }
        }

        if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
        {
            return MST_SUCCESS;
        }

        pPortInfo->u1EntryStatus = (UINT1) AST_PORT_OPER_DOWN;

        /* The value of Oper P2P is updated while disabling the port if 
         * Admin P2P is Auto, by triggering LA, because if the aggregatable
         * status of any port other than the lowest numbered port changes 
         * from Individual to Aggregatable, then only a Down indication is 
         * given and an Up indication is not given for that port. */
        AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);

        if (pRstPortInfo->bPortEnabled == MST_FALSE)
        {
            /* STP Port Status is disabled in CIST and hence 
             * in all instances. */
            u2TmpMstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (u2TmpMstInst, pTmpPerStInfo)
            {
                if (pTmpPerStInfo == NULL)
                {
                    continue;
                }

                if (AstL2IwfGetInstPortState
                    (u2TmpMstInst, AST_IFENTRY_IFINDEX (pPortInfo)) !=
                    AST_PORT_STATE_DISCARDING)
                {
                    /* 
                     * The port is now being disabled at the bridge level. 
                     * But the port is already disabled in spanning tree for 
                     * this instance.
                     * Put the port in DISCARDING state (previously it would 
                     * have been in FORWARDING since the port was disabled in 
                     * spanning tree) 
                     */
                    AstSetInstPortStateToL2Iwf (u2TmpMstInst,
                                                pPortInfo->u2PortNo,
                                                AST_PORT_STATE_DISCARDING);

                    AST_GET_LAST_PROGRMD_STATE (u2TmpMstInst,
                                                pPortInfo->u2PortNo) =
                        AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
                    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                    {
                        FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID
                                                          (),
                                                          AST_GET_PHY_PORT
                                                          (pPortInfo->u2PortNo),
                                                          u2TmpMstInst,
                                                          AST_PORT_STATE_DISCARDING);
                    }
#endif /* NPAPI_WANTED */

#ifdef ICCH_WANTED
                    /* Dont delete FDB entries learnt on MC-LAG enabled
                     * interface */
                    LaApiIsMclagInterface
                        (pPortInfo->u4IfIndex, &u1IsMclagEnabled);
                    if ((u1IsMclagEnabled == OSIX_TRUE)
                        && (IcchGetPeerNodeState () == ICCH_PEER_UP))
                    {
                        AST_TRC (AST_CONTROL_PATH_TRC | AST_DATA_PATH_TRC,
                                 "Deletion of FDB entries skipped for MC-LAG enabled"
                                 "interface\n");
                        return MST_SUCCESS;
                    }
#endif
                    /* Delete the Fdb Entries Learnt on the Port */
                    AstVlanDeleteFdbEntries (pPortInfo->u4IfIndex,
                                             VLAN_OPTIMIZE);
                }
            }

            /* Port is already disabled in MSTP for CIST and hence in all 
             * instances. Hence need not trigger the State machines for any of
             * the instances when port oper down event is received */

            return MST_SUCCESS;
        }
    }

    /* Disable this port for all instances 
     * if this is a link down event from the lower layer module OR
     * if the spanning tree is disabled for the CIST (by configuration)
     */
    if ((u1TrigType == (UINT1) AST_EXT_PORT_DOWN) ||
        (u2MstInst == MST_CIST_CONTEXT))
    {
        MstDisableInstancesForPort (u2PortNum, u1TrigType);
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        (VOID) RstPortMigrationMachine (RST_PMIGSM_EV_PORT_DISABLED, u2PortNum);

        RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_TX_DISABLED, pPortInfo,
                                MST_CIST_CONTEXT);

        if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_PORT_DISABLED, u2PortNum,
                                  NULL) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for Port Disabled event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for Port Disabled event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        }

    }
    bOperEdge = pPortInfo->bOperEdgePort;
/*     BrgDetectionMachine is triggered before the Port Info state machine 
       because Flushing is not done for the Non-Edge port since 
       BrgDetectionMachine moves the edgeport to nonedge port(i.e AutoEdge 
       is set) when the port is disabled */

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        RstBrgDetectionMachine (RST_BRGDETSM_EV_PORTDISABLED, u2PortNum);
    }

    if (MstPortInfoMachine (MST_PINFOSM_EV_PORT_DISABLED, pPerStPortInfo,
                            NULL, u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_DISABLED event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_DISABLED event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    }

    /* Here Bridge Detection State Machine trigger is purposely 
     * put in the last place. The reason being, this trigger will put
     * the port state as NOT_EDGE. Had this trigger been done
     * earlier,then if a Bpdu is generated it will be transmitted with
     * TC flag set(Port is in nonedge state). To avoid that this trigger
     * is left as last one
     * */
    /* Here Flushing is triggered for all MSTI's when the port is made ADMIN_DOWN */
    if (u1TrigType == (UINT1) AST_EXT_PORT_DOWN)
    {
        if (pPortInfo->bAdminEdgePort == MST_TRUE)
        {
            u2MstInst = 0;

            AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) ==
                    VLAN_TRUE)
                {
                    AstTopologyChSmFlushEntries (pPerStPortInfo);
                }

            }

        }
        if ((bOperEdge == OSIX_TRUE)
            && (pPortInfo->bAdminEdgePort == MST_FALSE))
        {
            u2MstInst = 1;

            AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) ==
                    VLAN_TRUE)
                {
                    AstTopologyChSmFlushEntries (pPerStPortInfo);
                }

            }

        }

    }

    if ((u2MstInst == MST_CIST_CONTEXT) && (u1TrigType == AST_EXT_PORT_DOWN))
    {
        if (AstSetCistPortMacEnabled (u2PortNum,
                                      pPortInfo->u1AdminPointToPoint)
            == SNMP_FAILURE)
        {
            return MST_FAILURE;
        }
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s successfully disabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s successfully disabled for Instance %d...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstEnableExtendedSysId                     */
/*                                                                           */
/*    Description               : This function enables the extended         */
/*                                system id feature.                         */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
MstEnableExtendedSysId (VOID)
{
    UINT2               u2Inst = 0;
    UINT2               u2PortNum;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;

    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    if (pMstBridgeEntry->bExtendedSysId == MST_TRUE)
    {
        return;
    }

    pMstBridgeEntry->bExtendedSysId = MST_TRUE;

    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2Inst);
        pPerStBrgInfo->u2BrgPriority = pPerStBrgInfo->u2BrgPriority | u2Inst;

        if (AST_IS_MST_ENABLED ())
        {
            /* Adding the instance component to the RootId  
             * element of the root priority vector for this instance */
            pPerStBrgInfo->RootId.u2BrgPriority =
                pPerStBrgInfo->RootId.u2BrgPriority | u2Inst;
            pPerStBrgInfo->OldRootId.u2BrgPriority =
                pPerStBrgInfo->OldRootId.u2BrgPriority | u2Inst;

            /* Adding the instance component to the RootId and Designated BrgId 
             * element of all the port priority vectors for this instance */
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);

                if (pPerStPortInfo != NULL)
                {
                    pPerStPortInfo->RootId.u2BrgPriority =
                        pPerStPortInfo->RootId.u2BrgPriority | u2Inst;
                    pPerStPortInfo->DesgBrgId.u2BrgPriority =
                        pPerStPortInfo->DesgBrgId.u2BrgPriority | u2Inst;
                }
            }                    /* End of For Loop */
        }
    }
    AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
             "SYS: Extended System Id enabled...."
             "Instance component added to all bridge ids \n");
    AST_DBG (AST_MGMT_DBG,
             "SYS: Extended System Id enabled...."
             "Instance component added to all bridge ids \n");
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDisableExtendedSysId                    */
/*                                                                           */
/*    Description               : This function disables the extended        */
/*                                system id feature.                         */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
MstDisableExtendedSysId (VOID)
{
    UINT2               u2Inst = 0;
    UINT2               u2PortNum;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;

    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    if (pMstBridgeEntry->bExtendedSysId == MST_FALSE)
    {
        return;
    }

    pMstBridgeEntry->bExtendedSysId = MST_FALSE;

    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2Inst);
        MST_GET_BRIDGE_PRIORITY (pPerStBrgInfo->u2BrgPriority,
                                 pPerStBrgInfo->u2BrgPriority);

        if (AST_IS_MST_ENABLED ())
        {
            /* Removing the instance component from the RootId  
             * element of the root priority vector for this instance */
            MST_GET_BRIDGE_PRIORITY (pPerStBrgInfo->RootId.u2BrgPriority,
                                     pPerStBrgInfo->RootId.u2BrgPriority);
            MST_GET_BRIDGE_PRIORITY (pPerStBrgInfo->OldRootId.u2BrgPriority,
                                     pPerStBrgInfo->OldRootId.u2BrgPriority);

            /* Removing the instance component from the RootId and Designated 
             * BrgId element of all the port priority vectors for this 
             * instance */
            for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);

                if (pPerStPortInfo != NULL)
                {
                    MST_GET_BRIDGE_PRIORITY
                        (pPerStPortInfo->RootId.u2BrgPriority,
                         pPerStPortInfo->RootId.u2BrgPriority);
                    MST_GET_BRIDGE_PRIORITY
                        (pPerStPortInfo->DesgBrgId.u2BrgPriority,
                         pPerStPortInfo->DesgBrgId.u2BrgPriority);

                }
            }
        }
    }
    AST_TRC (AST_CONTROL_PATH_TRC | AST_MGMT_TRC,
             "SYS: Extended System Id disabled...."
             "Instance component removed from all bridge ids \n");
    AST_DBG (AST_MGMT_DBG,
             "SYS: Extended System Id disabled...."
             "Instance component removed from all bridge ids \n");
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitGlobalBrgInfo                       */
/*                                                                           */
/*    Description               : This function initialises the global       */
/*                                Bridge information .                       */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstInitGlobalBrgInfo (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    AST_MEMSET (&(pBrgInfo->BridgeAddr), AST_INIT_VAL, AST_MAC_ADDR_SIZE);
    pBrgInfo->u1MigrateTime = RST_DEFAULT_MIGRATE_TIME;
    pBrgInfo->u1TxHoldCount = MST_DEFAULT_BRG_TX_LIMIT;
    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->BridgeTimes.u2MaxAge = MST_DEFAULT_BRG_MAX_AGE;
    pBrgInfo->BridgeTimes.u2ForwardDelay = MST_DEFAULT_BRG_FWD_DELAY;
    pBrgInfo->BridgeTimes.u2HelloTime = MST_DEFAULT_BRG_HELLO_TIME;
    pBrgInfo->BridgeTimes.u2MsgAgeOrHopCount = AST_INIT_VAL;
    pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;
    pBrgInfo->u1DynamicPathcostCalculation = AST_FALSE;
    pBrgInfo->u1DynamicPathcostCalcLagg = AST_SNMP_FALSE;
    pBrgInfo->bBridgeClearStats = MST_DISABLED;

    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitPerStBrgInfo                        */
/*                                                                           */
/*    Description               : This function initialises the Instance     */
/*                                specific Bridge Information.               */
/*                                                                           */
/*    Input(s)                  : pPerStBrgInfo - Pointer to information     */
/*                                                that needs to be initialised*/
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstInitPerStBrgInfo (tAstPerStBridgeInfo * pPerStBrgInfo)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    AST_MEMCPY (&(pPerStBrgInfo->RootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    AST_MEMCPY (&(pPerStBrgInfo->DesgBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStBrgInfo->u2BrgPriority = MST_DEFAULT_BRG_PRIORITY;
    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    pPerStBrgInfo->u1BrgRemainingHops = pAstMstBridgeEntry->u1MaxHopCount;

    pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStBrgInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStBrgInfo->u1RootRemainingHops = pPerStBrgInfo->u1BrgRemainingHops;
    AST_MEMSET (pPerStBrgInfo->au2TcDetPorts, AST_INIT_VAL,
                sizeof (pPerStBrgInfo->au2TcDetPorts));
    AST_MEMSET (pPerStBrgInfo->au2TcRecvPorts, AST_INIT_VAL,
                sizeof (pPerStBrgInfo->au2TcDetPorts));
    pPerStBrgInfo->u1DetHead = AST_INIT_VAL;
    pPerStBrgInfo->u1RcvdHead = AST_INIT_VAL;

    pPerStBrgInfo->u1ProleSelSmState = RST_PROLESELSM_STATE_INIT_BRIDGE;
    pPerStBrgInfo->u2RootPort = AST_NO_VAL;
    pPerStBrgInfo->u1RootPriority = AST_ROOT_DEFAULT;
    pPerStBrgInfo->u2DesgPort = AST_NO_VAL;
    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    if (gu1EnablePortVlanFlushOpt == AST_TRUE)
    {
        pPerStBrgInfo->u4FlushIndThreshold =
            MST_DEFAULT_OPT_FLUSH_IND_THRESHOLD;
    }
    else
    {
        pPerStBrgInfo->u4FlushIndThreshold = MST_DEFAULT_FLUSH_IND_THRESHOLD;

    }

    pPerStBrgInfo->u4RootCost = AST_INIT_VAL;

    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStBrgInfo->u4TimeSinceTopoCh)));
    pPerStBrgInfo->u2MasteredCount = 0;
    pPerStBrgInfo->u2PrevMasteredCount = 0;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitGlobalPortInfo                      */
/*                                                                           */
/*    Description               : This function initialises the global port  */
/*                                information.                               */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port that has to be initialised*/
/*                                pPortInfo - Pointer to Port Information.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/

VOID
MstInitGlobalPortInfo (UINT2 u2PortNum, tAstPortEntry * pPortInfo)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstBoolean         portUpdated = RST_FALSE;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo->u2PortNo = u2PortNum;
    pPortInfo->bAdminEdgePort = MST_FALSE;
    pPortInfo->bOperEdgePort = MST_FALSE;
    pPortInfo->bAllTransmitReady = MST_TRUE;
    pPortInfo->bLoopInconsistent = MST_FALSE;
    pPortInfo->u1RecScenario = AST_INIT_VAL;

    /* The Common database is intimated about the Operational Edge Status */
    AstL2IwfSetPortOperEdgeStatus (AST_CURR_CONTEXT_ID (),
                                   u2PortNum, OSIX_FALSE);

    pPortInfo->u1AdminPointToPoint = RST_P2P_AUTO;
    /* The value of Oper Point-To-Point will be updated after triggering LA and
     * getting the status */
    portUpdated = pPortInfo->bIEEE8021apAdminP2P;
    AST_UPDATE_OPER_P2P (u2PortNum, pPortInfo->u1AdminPointToPoint);
    pPortInfo->bIEEE8021apAdminP2P = portUpdated;
    pPortInfo->u2HelloTime = MST_DEFAULT_BRG_HELLO_TIME;
    pPortInfo->u4ErrorRecovery = AST_DEFAULT_ERROR_RECOVERY;
    pCommPortInfo = &(pPortInfo->CommPortInfo);
    pCommPortInfo->bMCheck = MST_FALSE;
    pCommPortInfo->bRcvdBpdu = MST_FALSE;
    pCommPortInfo->bRcvdRstp = MST_FALSE;
    pCommPortInfo->bRcvdStp = MST_FALSE;
    pCommPortInfo->bRcvdTcAck = MST_FALSE;
    pCommPortInfo->bRcvdTcn = MST_FALSE;
    pCommPortInfo->bSendRstp = MST_TRUE;
    pCommPortInfo->bTcAck = MST_FALSE;
    pCommPortInfo->u1PortRcvSmState = MST_PRCVSM_STATE_DISCARD;
    pCommPortInfo->u1PmigSmState = RST_PMIGSM_STATE_CHECKING_RSTP;
    pCommPortInfo->u1PortTxSmState = RST_PTXSM_STATE_TRANSMIT_INIT;
    pCommPortInfo->u1TxCount = AST_INIT_VAL;
    pCommPortInfo->bNewInfo = MST_FALSE;
    pCommPortInfo->u4BpduGuard = AST_BPDUGUARD_NONE;
    pCommPortInfo->bCistPortMacEnabled = AST_SNMP_FALSE;
    pCommPortInfo->bCistPortMacOperational = AST_SNMP_FALSE;
    pCommPortInfo->bBpduInconsistent = AST_FALSE;

    pPortInfo->CistMstiPortInfo.bInfoInternal = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bNewInfo = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bNewInfoMsti = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bRcvdInternal = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bCist = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bCistRootPort = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bCistDesignatedPort = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bMstiRootPort = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bMstiDesignatedPort = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bRcvdAnyMsg = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bRcvdCistInfo = MST_FALSE;
    pPortInfo->CistMstiPortInfo.bUpdtCistInfo = MST_FALSE;
    /* As per 802.1Q, updating the following default values
     * AdminEdge = FALSE
     * AutoEdge  = TRUE
     * */
    pPortInfo->bAutoEdge = AST_DEFAULT_AUTOEDGE_VALUE;
    pPortInfo->DesgTimes = pBrgInfo->RootTimes;
    pPortInfo->PortTimes = pPortInfo->DesgTimes;

    pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
    pPortInfo->bIEEE8021apAdminP2P = MST_FALSE;
    pPortInfo->bPVIDInconsistent = MST_FALSE;
    pPortInfo->bPTypeInconsistent = MST_FALSE;
    pPortInfo->bPVIDIncExists = MST_FALSE;
    pPortInfo->bPTypeIncExists = MST_FALSE;

    AST_PB_PORT_INFO (pPortInfo) = NULL;
    pPortInfo->bRootGuard = MST_FALSE;
    AST_PORT_RESTRICTED_ROLE (pPortInfo) = (tAstBoolean) RST_FALSE;
    AST_PORT_RESTRICTED_TCN (pPortInfo) = (tAstBoolean) RST_FALSE;
    pPortInfo->u1EnableBPDUFilter = AST_BPDUFILTER_DISABLE;
    AST_PORT_ENABLE_BPDU_RX (pPortInfo) = RST_TRUE;
    AST_PORT_ENABLE_BPDU_TX (pPortInfo) = RST_TRUE;
    AST_PORT_IS_L2GP (pPortInfo) = RST_FALSE;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitPerStPortInfo                       */
/*                                                                           */
/*    Description               : This function initialises the instance     */
/*                                specific port information.                 */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number to be initialised  */
/*                                u1InstanceId - Instance for which the Port */
/*                                              needs to be initialised.     */
/*                                pPerStPortInfo - Instance Specific Port    */
/*                                                 information.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
VOID
MstInitPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId,
                      tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    tPerStCistMstiCommInfo *pPerStCistMstiCommInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2Val = 0;
    UINT1               u1TunnelStatus = 0;

    pBrgInfo = AST_GET_BRGENTRY ();
    AST_MEMCPY (&(pPerStPortInfo->DesgBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

    if ((pPerStPortInfo == NULL) || (pAstPortEntry == NULL))
    {
        return;
    }
    pPerStPortInfo->DesgBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
    pPerStPortInfo->RegionalRootId = pPerStPortInfo->DesgBrgId;
    pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_DISABLED;
    pPerStPortInfo->u1ProleTrSmState = MST_PROLETRSM_STATE_INIT_PORT;
    pPerStPortInfo->u1PstateTrSmState = RST_PSTATETRSM_STATE_DISCARDING;
    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_INACTIVE;
    pPerStPortInfo->u1SelectedPortRole = AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u1PortRole = AST_PORT_ROLE_DISABLED;
    pPerStPortInfo->u2PortNo = u2PortNum;
    pPerStPortInfo->u2Inst = u2InstanceId;
    pPerStPortInfo->bRootInconsistent = MST_FALSE;
    pPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
    pPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
    AST_GET_SYS_TIME ((tAstSysTime
                       *) (&(pPerStPortInfo->u4TcDetectedTimestamp)));
    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStPortInfo->u4TcRcvdTimestamp)));
    u2Val = (UINT2) MST_DEFAULT_PORT_PRIORITY;
    u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);

    pPerStPortInfo->u2DesgPortId = AST_GET_PROTOCOL_PORT (u2PortNum) | u2Val;
    pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
    pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
    pPerStPortInfo->u4RootCost = AST_INIT_VAL;
    pPerStPortInfo->u1PortPriority = MST_DEFAULT_PORT_PRIORITY;
    pPerStPortInfo->u4PortAdminPathCost = 0;

    /* Copy the Global Loop guard status to per port */
    pPerStPortInfo->bLoopGuardStatus = MST_FALSE;
    pPerStPortInfo->bLoopIncStatus = MST_FALSE;

    AST_MEMCPY (&(pPerStPortInfo->PseudoRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    pPerStPortInfo->PseudoRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    pPerStPortInfo->bIsPseudoRootIdConfigured = RST_FALSE;
    pPerStPortInfo->i4TransmitSelfInfo = RST_FALSE;
    pPerStPortInfo->i4NpPortStateStatus = AST_BLOCK_SUCCESS;
    AST_GET_LAST_PROGRMD_STATE (u2InstanceId, u2PortNum) =
        AstGetInstPortStateFromL2Iwf (u2InstanceId, u2PortNum);

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    pPerStCistMstiCommInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);
    pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
    if (pPerStCistMstiCommInfo == NULL)
    {
        return;
    }
    pPerStCistMstiCommInfo->u1DesgRemainingHops =
        pPerStBrgInfo->u1RootRemainingHops;
    pPerStCistMstiCommInfo->u1PortRemainingHops =
        pPerStCistMstiCommInfo->u1DesgRemainingHops;

    pRstPortInfo->bAgreed = MST_FALSE;
    pRstPortInfo->bForward = MST_FALSE;
    pRstPortInfo->bForwarding = MST_FALSE;
    pRstPortInfo->bLearn = MST_FALSE;
    pRstPortInfo->bLearning = MST_FALSE;
    pRstPortInfo->bProposed = MST_FALSE;
    pRstPortInfo->bProposing = MST_FALSE;
    pRstPortInfo->bRcvdTc = MST_FALSE;
    pRstPortInfo->bReRoot = MST_FALSE;
    pRstPortInfo->bReSelect = MST_FALSE;
    pRstPortInfo->bSelected = MST_FALSE;
    pRstPortInfo->bSync = MST_FALSE;
    pRstPortInfo->bSynced = MST_FALSE;
    pRstPortInfo->bTc = MST_FALSE;
    pRstPortInfo->bTcProp = MST_FALSE;
    pRstPortInfo->bUpdtInfo = MST_FALSE;
    pRstPortInfo->u1InfoIs = MST_INFOIS_DISABLED;
    pRstPortInfo->u1RcvdInfo = MST_OTHER_MSG;
    pRstPortInfo->bRcvdMsg = MST_FALSE;
    pRstPortInfo->bAgree = MST_FALSE;
    pRstPortInfo->bPortEnabled = MST_TRUE;
    pRstPortInfo->bAdminPortEnabled = MST_TRUE;

    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo,
                           MST_SYNC_BSYNCED_UPDATE);
    MstPUpdtAllSyncedFlag (u2InstanceId, pPerStPortInfo, MST_SYNC_ROLE_UPDATE);

    if (u2InstanceId == MST_CIST_CONTEXT)
    {
        /* In case of PEB/PCB and Customer Bridge, STP tunnel status should   
         * not be enabled on the port for enabling STP on that port */
        if ((AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == RST_TRUE) ||
            (AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE))
        {
            AstL2IwfGetProtocolTunnelStatusOnPort (AST_GET_IFINDEX (u2PortNum),
                                                   L2_PROTO_STP,
                                                   &u1TunnelStatus);

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                pRstPortInfo->bAdminPortEnabled = MST_FALSE;
                pRstPortInfo->bPortEnabled = MST_FALSE;
            }
        }

        /* In case of q-in-q bridge mode, Tunnel Status should not be enabled   
         * on the port for enabling STP on that port */
        if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
        {
            AstL2IwfGetPortVlanTunnelStatus
                ((UINT2) AST_GET_IFINDEX (u2PortNum),
                 (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                pRstPortInfo->bAdminPortEnabled = MST_FALSE;
                pRstPortInfo->bPortEnabled = MST_FALSE;
            }
        }
    }

    pRstPortInfo->bDisputed = MST_FALSE;

    if (AST_IS_MST_ENABLED ())
    {
        if (pPerStCistMstiCommInfo != NULL)
        {
            pPerStCistMstiCommInfo->bAllSynced = MST_FALSE;
            pPerStCistMstiCommInfo->bReRooted = MST_FALSE;
        }
        if (pPerStMstiOnlyInfo != NULL)
        {
            pPerStMstiOnlyInfo->bRcvdMstiInfo = MST_FALSE;
            pPerStMstiOnlyInfo->bUpdtMstiInfo = MST_FALSE;
            pPerStMstiOnlyInfo->bMaster = MST_FALSE;
            pPerStMstiOnlyInfo->bMastered = MST_FALSE;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitMstBridgeEntry                      */
/*                                                                           */
/*    Description               : This function initilalises the MST         */
/*                                Bridge Entry.                              */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstInitMstBridgeEntry (VOID)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();
    pAstMstBridgeEntry = &(pBrgInfo->MstBridgeEntry);
    pAstMstBridgeEntry->u2MaxMstInstances = AST_MAX_MST_INSTANCES - 1;
    pAstMstBridgeEntry->u2NoOfActiveMsti = AST_INIT_VAL;
    pAstMstBridgeEntry->u1MaxHopCount = MST_DEFAULT_MAX_HOPCOUNT;
    pAstMstBridgeEntry->u4RegionCfgChangeCount = AST_INIT_VAL;
    pAstMstBridgeEntry->bFlagInstTrigger = MST_FALSE;
    pAstMstBridgeEntry->bExtendedSysId = MST_FALSE;

    pAstMstBridgeEntry->MstConfigIdInfo.u1ConfigurationId
        = MST_DEFAULT_CONFIG_ID;
    pAstMstBridgeEntry->MstConfigIdInfo.u2ConfigRevLevel
        = MST_DEFAULT_CONFIG_LEVEL;
    MstInitConfigName (MST_TRUE);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitConfigName                          */
/*                                                                           */
/*    Description               : This function initialises the region       */
/*                                name to a default value.                   */
/*                                                                           */
/*    Input(s)                  : bOverWrite: Overwrite ConfigName or not    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
MstInitConfigName (tAstBoolean bOverWrite)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;
    UINT1              *pu1ConfigName;
    UINT1              *pu1NullString;
    UINT1               u1Indx = 0;
    UINT1               au1NullString[MST_CONFIG_NAME_LEN];
    UINT1               u1Null = 0;

    pBrgInfo = AST_GET_BRGENTRY ();
    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    AST_MEMSET (au1NullString, AST_INIT_VAL, MST_CONFIG_NAME_LEN);
    pu1ConfigName = pAstMstBridgeEntry->MstConfigIdInfo.au1ConfigName;

    if (!bOverWrite)
    {
        /* Prepare null string for comparison */
        pu1NullString = au1NullString;
        for (u1Indx = 0; u1Indx < AST_MAC_ADDR_SIZE; u1Indx++)
        {
            SPRINTF ((CHR1 *) pu1NullString, "%02x:", u1Null);
            pu1NullString += 3;
        }
        pu1NullString -= 1;
        *pu1NullString = '\0';
        /* Check whether Config name has been changed */
        if (AST_MEMCMP (pu1ConfigName, au1NullString, MST_CONFIG_NAME_LEN) != 0)
        {
            /* Config Name has been configured. Do not overwrite */
            return;
        }
    }
    for (u1Indx = 0; u1Indx < AST_MAC_ADDR_SIZE; u1Indx++)
    {
        SPRINTF ((CHR1 *) pu1ConfigName, "%02x:", pBrgInfo->BridgeAddr[u1Indx]);
        pu1ConfigName += 3;
    }
    pu1ConfigName -= 1;
    *pu1ConfigName = '\0';
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstReInitMstInfo                           */
/*                                                                           */
/*    Description               : This function initilalises the MST         */
/*                                port, perstport, perst dynamic informations*/
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
MstReInitMstInfo (VOID)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    tPerStCistMstiCommInfo *pPerStCistMstiCommInfo = NULL;
    INT2                i2MstInst = AST_INIT_VAL;
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();
    pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;

    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        /* Initializing the instance specific bridge information */
        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (i2MstInst);
        pPerStBrgInfo->RootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        pPerStBrgInfo->u1RootRemainingHops = pPerStBrgInfo->u1BrgRemainingHops;

        pPerStBrgInfo->u1ProleSelSmState = RST_PROLESELSM_STATE_INIT_BRIDGE;
        pPerStBrgInfo->u2RootPort = AST_NO_VAL;
        pPerStBrgInfo->u4RootCost = AST_INIT_VAL;
        pPerStBrgInfo->u4TimeSinceTopoCh = AST_INIT_VAL;

        u2PortNum = 1;
        AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
        {
            if (pPortInfo == NULL)
            {
                continue;
            }
            pPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);
            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            /* Initializing the instance specific port information */
            pPerStPortInfo->DesgBrgId.u2BrgPriority =
                pPerStBrgInfo->u2BrgPriority;
            pPerStPortInfo->RootId = pPerStPortInfo->DesgBrgId;
            pPerStPortInfo->RegionalRootId = pPerStPortInfo->DesgBrgId;
            pPerStPortInfo->u1PinfoSmState = MST_PINFOSM_STATE_DISABLED;
            pPerStPortInfo->u1ProleTrSmState = MST_PROLETRSM_STATE_INIT_PORT;
            pPerStPortInfo->u1PstateTrSmState = RST_PSTATETRSM_STATE_DISCARDING;
            pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_INACTIVE;
            pPerStPortInfo->u1SelectedPortRole = AST_PORT_ROLE_DISABLED;
            MstPUpdtAllSyncedFlag ((UINT2) i2MstInst, pPerStPortInfo,
                                   MST_SYNC_ROLE_UPDATE);
            pPerStPortInfo->u1PortRole = AST_PORT_ROLE_DISABLED;
            MstPUpdtAllSyncedFlag ((UINT2) i2MstInst, pPerStPortInfo,
                                   MST_SYNC_ROLE_UPDATE);
            pPerStPortInfo->u4RootCost = AST_INIT_VAL;
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pPerStCistMstiCommInfo =
                MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);
            pPerStMstiOnlyInfo = AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);

            if (i2MstInst == MST_CIST_CONTEXT)
            {
                pPortInfo->DesgTimes = pBrgInfo->RootTimes;
                pPortInfo->PortTimes = pPortInfo->DesgTimes;
            }
            if (pPerStCistMstiCommInfo != NULL)
            {
                pPerStCistMstiCommInfo->u1DesgRemainingHops =
                    pPerStBrgInfo->u1RootRemainingHops;
                pPerStCistMstiCommInfo->u1PortRemainingHops =
                    pPerStCistMstiCommInfo->u1DesgRemainingHops;
            }
            pRstPortInfo->bAgreed = MST_FALSE;
            pRstPortInfo->bForward = MST_FALSE;
            pRstPortInfo->bForwarding = MST_FALSE;
            pRstPortInfo->bLearn = MST_FALSE;
            pRstPortInfo->bLearning = MST_FALSE;
            pRstPortInfo->bProposed = MST_FALSE;
            pRstPortInfo->bProposing = MST_FALSE;
            pRstPortInfo->bRcvdTc = MST_FALSE;
            pRstPortInfo->bReRoot = MST_FALSE;
            pRstPortInfo->bReSelect = MST_FALSE;
            pRstPortInfo->bSelected = MST_FALSE;
            MstPUpdtAllSyncedFlag ((UINT2) i2MstInst, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pRstPortInfo->bSync = MST_FALSE;
            pRstPortInfo->bSynced = MST_FALSE;
            MstPUpdtAllSyncedFlag ((UINT2) i2MstInst, pPerStPortInfo,
                                   MST_SYNC_BSYNCED_UPDATE);
            pRstPortInfo->bTc = MST_FALSE;
            pRstPortInfo->bTcProp = MST_FALSE;
            pRstPortInfo->bUpdtInfo = MST_FALSE;
            MstPUpdtAllSyncedFlag ((UINT2) i2MstInst, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);
            pRstPortInfo->u1InfoIs = MST_INFOIS_DISABLED;
            pRstPortInfo->u1RcvdInfo = MST_OTHER_MSG;
            pRstPortInfo->bRcvdMsg = MST_FALSE;
            pRstPortInfo->bAgree = MST_FALSE;
            pRstPortInfo->bDisputed = MST_FALSE;
            pPortInfo->bIEEE8021apAdminP2P = MST_FALSE;
            pPortInfo->bLoopInconsistent = MST_FALSE;
            pPortInfo->u1RecScenario = AST_INIT_VAL;
            pPortInfo->bAllTransmitReady = MST_TRUE;
            if (AST_IS_MST_ENABLED ())
            {
                if (pPerStCistMstiCommInfo != NULL)
                {
                    pPerStCistMstiCommInfo->bAllSynced = MST_FALSE;
                    pPerStCistMstiCommInfo->bReRooted = MST_FALSE;
                }
                if (pPerStMstiOnlyInfo != NULL)
                {
                    pPerStMstiOnlyInfo->bRcvdMstiInfo = MST_FALSE;
                    pPerStMstiOnlyInfo->bUpdtMstiInfo = MST_FALSE;
                }
            }

            /* Initializing the global port information */
            if (i2MstInst == MST_CIST_CONTEXT)
            {
                pCommPortInfo = &(pPortInfo->CommPortInfo);
                pCommPortInfo->bMCheck = MST_FALSE;
                pCommPortInfo->bRcvdBpdu = MST_FALSE;
                pCommPortInfo->bRcvdRstp = MST_FALSE;
                pCommPortInfo->bRcvdStp = MST_FALSE;
                pCommPortInfo->bRcvdTcAck = MST_FALSE;
                pCommPortInfo->bRcvdTcn = MST_FALSE;
                pCommPortInfo->bTcAck = MST_FALSE;
                pCommPortInfo->u1PortRcvSmState = RST_PRCVSM_STATE_DISCARD;
                pCommPortInfo->u1PmigSmState = RST_PMIGSM_STATE_CHECKING_RSTP;
                pCommPortInfo->u1PortTxSmState = RST_PTXSM_STATE_TRANSMIT_INIT;
                pCommPortInfo->u1TxCount = AST_INIT_VAL;
                pCommPortInfo->bNewInfo = MST_FALSE;
                pCommPortInfo->bCistPortMacEnabled = AST_SNMP_FALSE;
                pCommPortInfo->bCistPortMacOperational = AST_SNMP_FALSE;
                pCommPortInfo->bBpduInconsistent = AST_SNMP_FALSE;

                pPortInfo->CistMstiPortInfo.bInfoInternal = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bNewInfo = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bNewInfoMsti = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bRcvdInternal = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bCist = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bCistRootPort = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bCistDesignatedPort = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bMstiRootPort = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bMstiDesignatedPort = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bRcvdAnyMsg = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bRcvdCistInfo = MST_FALSE;
                pPortInfo->CistMstiPortInfo.bUpdtCistInfo = MST_FALSE;
                pPortInfo->bPVIDInconsistent = MST_FALSE;
                pPortInfo->bPTypeInconsistent = MST_FALSE;
                pPortInfo->bPVIDIncExists = MST_FALSE;
                pPortInfo->bPTypeIncExists = MST_FALSE;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstHwReInit                                */
/*                                                                           */
/*    Description               : This function Reinitializes the Hardware   */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : gAstGlobalInfo                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstHwReInit (VOID)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    INT2                i2MstInst = AST_INIT_VAL;
    UINT2               u2MapIndex = AST_INIT_VAL;
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal = AST_INIT_VAL;
    UINT2               u2LastVlan;
#endif
    UINT1              *pu1MstVlanList = NULL;
    UINT2               u2NumVlans = 0;

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Reinitializing Hardware ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Reinitializing Hardware ...\n");

#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        MstpFsMiMstpNpInitHw (AST_CURR_CONTEXT_ID ());
    }
#endif

    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }
#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            i4HwRetVal = MstpFsMiMstpNpCreateInstance (AST_CURR_CONTEXT_ID (),
                                                       i2MstInst);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Hardware Create Instance %d failed !!!\n",
                              i2MstInst);
                return MST_FAILURE;
            }
        }
#endif /* NPAPI_WANTED */

        pu1MstVlanList = UtlShMemAllocVlanList ();
        if (pu1MstVlanList == NULL)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "pu1MstVlanList:Memory allocation for Vlan List failed.\n");

            return MST_FAILURE;
        }

        MEMSET (pu1MstVlanList, 0, MST_VLAN_LIST_SIZE);
        for (u2MapIndex = 1; u2MapIndex <= AST_MAX_NUM_VLANS; u2MapIndex++)
        {
            if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                              u2MapIndex) == i2MstInst)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                            u2MapIndex) == OSIX_TRUE)
                {
                    u2NumVlans++;
                    OSIX_BITLIST_SET_BIT (pu1MstVlanList,
                                          u2MapIndex, MST_VLAN_LIST_SIZE);
                }
            }
        }
#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            i4HwRetVal =
                MstpFsMiMstpNpAddVlanListInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pu1MstVlanList,
                                                      i2MstInst,
                                                      u2NumVlans, &u2LastVlan);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC |
                              AST_CONTROL_PATH_TRC,
                              " Mapping vlans to instance %d in the hardware failed",
                              i2MstInst);
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG |
                              AST_INIT_SHUT_DBG,
                              "SYS: Mapping Vlans to instance %d in the hardware failed",
                              i2MstInst);
            }
        }
#endif
        UtlShMemFreeVlanList (pu1MstVlanList);
    }

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Hardware Reinitialized\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Hardware Reinitialized\n");

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstHwDeInit                                */
/*                                                                           */
/*    Description               : This function Resets the Mstp information  */
/*                                in the Hardware                            */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : gAstGlobalInfo                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstHwDeInit (VOID)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    INT2                i2MstInst = AST_INIT_VAL;
    UINT2               u2MapIndex = AST_INIT_VAL;
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal = AST_INIT_VAL;
    UINT2               u2LastVlan;
#endif
    UINT1              *pu1MstVlanList = NULL;
    UINT2               u2NumVlans = 0;

    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Deinitializing Hardware ...\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Deinitializing Hardware ...\n");

    pu1MstVlanList = UtlShMemAllocVlanList ();
    if (pu1MstVlanList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pu1MstVlanList:Memory allocation for Vlan List failed.\n");

        return MST_FAILURE;
    }

    for (i2MstInst = AST_MAX_MST_INSTANCES - 1; i2MstInst >= 0; i2MstInst--)
    {
        pPerStInfo = AST_GET_PERST_INFO (i2MstInst);
        if (pPerStInfo == NULL)
        {
            continue;
        }
        MEMSET (pu1MstVlanList, 0, MST_VLAN_LIST_SIZE);

        for (u2MapIndex = 1; u2MapIndex <= AST_MAX_NUM_VLANS; u2MapIndex++)
        {
            if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                              u2MapIndex) == i2MstInst)
            {
                if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                            u2MapIndex) == OSIX_TRUE)
                {
                    u2NumVlans++;
                    OSIX_BITLIST_SET_BIT (pu1MstVlanList,
                                          u2MapIndex, MST_VLAN_LIST_SIZE);
                }
            }
        }
#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            i4HwRetVal =
                MstpFsMiMstpNpDelVlanListInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pu1MstVlanList,
                                                      i2MstInst,
                                                      u2NumVlans, &u2LastVlan);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                UtlShMemFreeVlanList (pu1MstVlanList);
                AST_DBG_ARG1 (AST_ALL_FAILURE_DBG |
                              AST_INIT_SHUT_DBG,
                              "SYS: Deleting Vlans to instance %d mapping in the hardware failed",
                              i2MstInst);
                return MST_FAILURE;
            }
        }
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            i4HwRetVal = MstpFsMiMstpNpDeleteInstance (AST_CURR_CONTEXT_ID (),
                                                       i2MstInst);
            if (i4HwRetVal != FNP_SUCCESS)
            {
                UtlShMemFreeVlanList (pu1MstVlanList);
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: Hardware Delete Instance %d failed !!!\n",
                              i2MstInst);
                return MST_FAILURE;
            }
        }
#endif /* NPAPI_WANTED */
    }
#ifdef NPAPI_WANTED
    if ((AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE) ||
        (AST_NODE_STATUS () == RED_AST_FORCE_SWITCHOVER_INPROGRESS))
    {
        MstpFsMiMstpNpDeInitHw (AST_CURR_CONTEXT_ID ());
    }
#endif /* NPAPI_WANTED */

    UtlShMemFreeVlanList (pu1MstVlanList);
    AST_TRC (AST_INIT_SHUT_TRC, "SYS: Hardware Deinitialized\n");
    AST_DBG (AST_INIT_SHUT_DBG, "SYS: Hardware Deinitialized\n");

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitStateMachines                       */
/*                                                                           */
/*    Description               : This routine initializes the MST State     */
/*                                machines with the proper State Event and   */
/*                                action rooutine combinations               */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
MstInitStateMachines (VOID)
{
    MEMSET (gaaau1AstSemState, 0, sizeof (gaaau1AstSemState));
    MEMSET (gaaau1AstSemEvent, 0, sizeof (gaaau1AstSemEvent));
    /* Port Info Machine */
    MstInitPortInfoMachine ();

    /* Port Role Selection Machine */
    MstInitPortRoleSelectionMachine ();

    /* Port Role Transition Machine */
    MstInitPortRoleTrMachine ();

    /* Port State Transition Machine */
    MstInitPortStateTrMachine ();

    /* Port Migration Machine */
    MstInitPortMigrationMachine ();

    /* Topology Change Machine */
    MstInitTopoChStateMachine ();

    /* Port Transmit Machine */
    MstInitPortTxStateMachine ();

    /* Port Receive Machine */
    MstInitPortRxStateMachine ();

    /* Bridge Detection Machine */
    MstInitBrgDetectionStateMachine ();

    /* Port Pseudo Info Machine */
    MstInitPseudoInfoMachine ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstInitInstance                            */
/*                                                                           */
/*    Description               : Allocates the memory block for given       */
/*                                instance and initialise the instance       */
/*                                specific bridge information.               */
/*                                                                           */
/*    Input(s)                  : u2MstInst - Instance Identifier            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstInitInstance (UINT2 u2MstInst)
#else
INT4
MstInitInstance (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tMstInstInfo       *pMstInstInfo = NULL;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    AST_DBG_ARG1 (AST_MGMT_DBG | AST_INIT_SHUT_DBG,
                  "SYS: Creating Instance %d...\n", u2MstInst);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                  "SYS: Creating Instance %d...\n", u2MstInst);

    if (AstCreateSpanningTreeInst (u2MstInst, &pPerStInfo) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC |
                      AST_OS_RESOURCE_TRC,
                      "SYS: Creation of Spanning tree MST Instance %d failed !!!\n",
                      u2MstInst);
        AST_DBG_ARG1 (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG |
                      AST_INIT_SHUT_DBG,
                      "SYS: Creation of Spanning Tree MST Instance %d failed !!!\n",
                      u2MstInst);

        return MST_FAILURE;
    }

    AST_ALLOC_MST_INSTINFO_MEM_BLOCK (pMstInstInfo);

    if (pMstInstInfo == NULL)
    {
        AST_RELEASE_PERST_INFO_MEM_BLOCK (pPerStInfo);
        AST_GET_PERST_INFO (u2MstInst) = NULL;
        return MST_FAILURE;
    }
    AST_MEMSET (pMstInstInfo, 0, sizeof (tMstInstInfo));
    pMstInstInfo->pPerStInfo = pPerStInfo;
    pMstInstInfo->u2MstInstance = u2MstInst;
    AST_ADD_INST_BUF (&(AST_CURR_CONTEXT_INFO ())->MstiList,
                      (tTMO_SLL_NODE *) & (pMstInstInfo->NextNode));

    AST_GET_NO_OF_ACTIVE_INSTANCES++;
    pPerStInfo->u2InstanceId = u2MstInst;
    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);

    MstInitPerStBrgInfo (pPerStBrgInfo);
    pMstBrgEntry = AST_GET_MST_BRGENTRY ();

    if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
    {
        pPerStBrgInfo->u2BrgPriority = pPerStBrgInfo->u2BrgPriority | u2MstInst;
    }

    if (AST_MODULE_STATUS == MST_DISABLED)
    {
        return MST_SUCCESS;
    }

    /* No need to do BEGIN to RSSM here for this instance. This is because as 
     * per 802.1Q Rev 2005, whenever there is a change in the configuration
     * Identifier, then BEGIN should be asserted. Hence no need to do BEGIN 
     * seperately
     * */

    AST_DBG_ARG1 (AST_MGMT_DBG | AST_INIT_SHUT_DBG,
                  "SYS: Instance %d created\n", u2MstInst);
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Instance %d created\n", u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstInitPortMigrationMachine                          */
/*                                                                           */
/* Description        : Initialises the Port Protocol Migration State        */
/*                      Machine                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitPortMigrationMachine (VOID)
{
    RstInitProtocolMigrationMachine ();
}

/*****************************************************************************/
/* Function Name      : MstInitPortStateTrMachine                            */
/*                                                                           */
/* Description        : Initialises the Port State Transition State          */
/*                      Machine                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitPortStateTrMachine (VOID)
{
    RstInitPortStateTrMachine ();
}

/*****************************************************************************/
/* Function Name      : MstInitBrgDetectionStateMachine                      */
/*                                                                           */
/* Description        : Initialises the Bridge Detection State               */
/*                      Machine                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitBrgDetectionStateMachine (VOID)
{
    RstInitBrgDetectionStateMachine ();
}

/*****************************************************************************/
/* Function Name      : MstInitPseudoInfoMachine                             */
/*                                                                           */
/* Description        : Initialises the Port Pseudo Root State Machine       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitPseudoInfoMachine (VOID)
{
    RstInitPseudoInfoMachine ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstCreateInstancesForPort                  */
/*                                                                           */
/*    Description               : This function will check if any of the     */
/*                                any of the active vlan is mapped to any    */
/*                                spanning tree instance. If Vlan is mapped  */
/*                                then it will create instance specific port */
/*                                information provided that the given port is*/
/*                                member of that Vlan.                       */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstCreateInstancesForPort (UINT2 u2PortNum)
#else
INT4
MstCreateInstancesForPort (u2PortNum)
     UINT2               u2PortNum;
#endif
{
    UINT2               u2MstInst = 1;
    UINT1              *pVlanListInPort = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tAstPerStInfo      *pPerStInfo = NULL;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Creating instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Creating instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* If the port is SISP enabled, then it should be part of the instance
     * if and only if the interface is associated with the vlan to which 
     * this instance is mapped to. Get the set of vlans associated with 
     * this port
     * */
    pVlanListInPort = UtilVlanAllocVlanListSize (sizeof (tVlanList));

    if (pVlanListInPort == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pVlanListInPort:Memory allocation for Vlan List failed.\n");

        return MST_FAILURE;
    }

    AST_MEMSET (&OctetStr, AST_INIT_VAL, sizeof (OctetStr));

    AST_MEMSET (pVlanListInPort, AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    if (AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)
    {
        OctetStr.pu1_OctetList = pVlanListInPort;
        OctetStr.i4_Length = MST_VLAN_LIST_SIZE;

        /* Get the list of VLANs for this port using VLAN API */
        AstL2IwfGetMemberVlanList (AST_CURR_CONTEXT_ID (),
                                   AST_GET_IFINDEX (u2PortNum), &OctetStr);

    }

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        if ((AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst)) != NULL)
        {
            /* Port already part of this instance */
            continue;
        }

        if (AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)
        {
            if (MstSispIsVlansPresentInInst (u2PortNum, u2MstInst, &OctetStr,
                                             NULL) == MST_FALSE)
            {
                AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) = NULL;

                AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                              "API: Port %s: Validation for Instance to port returned failure for Inst %s!!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                continue;
            }
        }

        if (MstCreatePerStPortEntry (u2PortNum, u2MstInst, MST_TRUE) ==
            MST_FAILURE)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          " Create Per Instance Port Info for Instance %d, Port %s Failed.. \n",
                          u2MstInst, AST_GET_IFINDEX_STR (u2PortNum));
            UtilVlanReleaseVlanListSize (pVlanListInPort);
            return MST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG,
                  "SYS: Successfully created instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Successfully created instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    UtilVlanReleaseVlanListSize (pVlanListInPort);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstEnableInstancesForPort                  */
/*                                                                           */
/*    Description               : This function will check if any of the     */
/*                                any of the active vlan is mapped to any    */
/*                                spanning tree instance. If Vlan is mapped  */
/*                                then it will trigger all the instance      */
/*                                specific state machines with enable event  */
/*                                provided that the given port is member of  */
/*                                that Vlan.                                 */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstEnableInstancesForPort (UINT2 u2PortNum)
#else
INT4
MstEnableInstancesForPort (u2PortNum)
     UINT2               u2PortNum;
#endif
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2MstInst = 1;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Enabling instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Enabling instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        if ((pRstPortInfo == NULL) ||
            (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_FALSE))
        {
            continue;
        }

        if (MstPortInfoMachine (MST_PINFOSM_EV_PORT_ENABLED, pPerStPortInfo,
                                NULL, u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_ENABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                          "SYS: Port %s Inst %d: Port Info Machine Returned failure for PORT_ENABLED event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Successfully enabled instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Successfully enabled instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstDisableInstancesForPort                 */
/*                                                                           */
/*    Description               : This function will check if any of the     */
/*                                any of the active vlan is mapped to any    */
/*                                spanning tree instance. If Vlan is mapped  */
/*                                then it will trigger all the instance      */
/*                                specific state machines with disable event */
/*                                provided that the given port is member of  */
/*                                that Vlan.                                 */
/*                                                                           */
/*    Input(s)                  : u2PortNum - Port Number for which Inatance */
/*                                            needs to be disabled.          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstDisableInstancesForPort (UINT2 u2PortNum, UINT1 u1TrigType)
#else
INT4
MstDisableInstancesForPort (u2PortNum, u1TrigType)
     UINT2               u2PortNum;
     UINT1               u1TrigType;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst = 1;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Disabling instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Disabling instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo->bLoopIncStatus = MST_FALSE;

        pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2MstInst);

        if ((u1TrigType == AST_EXT_PORT_DOWN) &&
            (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_FALSE))
        {
            /* Port is already disabled in MSTP for this instance. Hence 
             * need not trigger the State machines for this instance
             * when port oper down event is received */
            if (AstL2IwfGetInstPortState
                (u2MstInst, AST_GET_IFINDEX (u2PortNum))
                != AST_PORT_STATE_DISCARDING)
            {
                /* 
                 * The port is now being disabled at the bridge level. 
                 * But the port is already disabled in spanning tree for 
                 * this instance.
                 * Put the port in DISCARDING state (previously it would 
                 * have been in FORWARDING since the port was disabled in 
                 * spanning tree) 
                 */
                AstSetInstPortStateToL2Iwf (u2MstInst, u2PortNum,
                                            AST_PORT_STATE_DISCARDING);

                AST_GET_LAST_PROGRMD_STATE (u2MstInst, u2PortNum) =
                    AST_PORT_STATE_DISCARDING;
#ifdef NPAPI_WANTED
                if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
                {
                    FsMiMstpNpWrSetInstancePortState (AST_CURR_CONTEXT_ID (),
                                                      AST_GET_PHY_PORT
                                                      (u2PortNum), u2MstInst,
                                                      AST_PORT_STATE_DISCARDING);
                }
#endif /* NPAPI_WANTED */

            }
            continue;
        }

        (VOID) MstPortInfoMachine (MST_PINFOSM_EV_PORT_DISABLED,
                                   pPerStPortInfo, NULL, u2MstInst);
    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Successfully disabled instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Successfully disabled instances for Port %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstVlanIdInstUnMap                                   */
/*                                                                           */
/* Description        : This function unmaps the given VLAN from the         */
/*                      instance and recalculates the config digest.         */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance id whose vlan mapping needs to  */
/*                                  be deleted.                              */
/*                      u2VlanId  - VLAN ID which needs to be unmapped       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE                              */
/*****************************************************************************/
INT4
MstVlanIdInstUnMap (UINT2 u2MstInst, UINT2 u2VlanId)
{
    UINT4               u4FdbId = 0;
    UINT2               u2NumVlans = 0;
    UINT1               u1Result = 0;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), u2VlanId) == OSIX_TRUE)
    {
#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            if (AST_IS_MST_ENABLED ())
            {
                if (FNP_SUCCESS !=
                    MstpFsMiMstpNpDelVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                      u2VlanId, u2MstInst))
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                                  u2VlanId, u2MstInst);
                    AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                  AST_EVENT_HANDLING_DBG,
                                  "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                                  u2VlanId, u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
#endif /*NPAPI_WANTED */
        u4FdbId = AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (), u2VlanId);
        AstVlanMiFlushFdbId (AST_CURR_CONTEXT_ID (), u4FdbId);
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_UPDATE_INST_VLAN_MAP_MSG,
                                    MST_CIST_CONTEXT,
                                    u2MstInst,
                                    u2VlanId,
                                    MST_UNMAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId, AST_INIT_VAL);

    /*When this is the last vlan in this instance, then delete that instance. */

    for (u2VlanId = 1; u2VlanId <= AST_MAX_NUM_VLANS; u2VlanId++)
    {
        if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                          u2VlanId) == u2MstInst)
        {
            u2NumVlans++;
        }
    }

    if (u2NumVlans == 0)
    {
        if (MstDeleteInstance (u2MstInst) != MST_SUCCESS)
        {
            return MST_FAILURE;
        }
    }

    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    MST_CIST_CONTEXT,
                                    u2MstInst,
                                    u2VlanId,
                                    MST_UNMAP_INST_VLAN_LIST_MSG, &u1Result);
    }
    MstTriggerDigestCalc ();

    if (AstAssertBegin () == MST_FAILURE)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_INIT_SHUT_TRC,
                 "SYS: Asserting BEGIN failed!\n");
        AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                 "SYS: Asserting BEGIN failed!\n");
        return MST_FAILURE;
    }

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstVlanInstUnMap                                     */
/*                                                                           */
/* Description        : This function unmaps all vlans mapped to the given   */
/*                      instance and recalculates the config digest.         */
/*                                                                           */
/* Input(s)           : u2MstInst - Instance id whose vlan mapping needs to  */
/*                                  be deleted.                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE                              */
/*****************************************************************************/
INT4
MstVlanInstUnMap (UINT2 u2MstInst)
{
    tMstVlanList       *pFdbList = NULL;
    tMstVlanList       *pVlanList = NULL;
    tMstVlanList       *pActiveVlanList = NULL;
    UINT2               u2VlanId = 0;
    UINT2               u2NumVlans = 0;
    UINT2               u2ActiveVlans = 0;
    UINT2               u2LastVlan = 0;
    UINT4               u4FdbId = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1Result = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanFlag;
    UINT2               u2VlanCount = 0;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pFdbList) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pActiveVlanList) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanList);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FAILURE;
    }

    MEMSET (pFdbList->au1List, 0, MST_FDB_LIST_SIZE);
    MEMSET (pVlanList->au1List, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pActiveVlanList->au1List, 0, MST_VLAN_LIST_SIZE);

    for (u2VlanId = 1; u2VlanId <= AST_MAX_NUM_VLANS; u2VlanId++)
    {
        if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                          u2VlanId) == u2MstInst)
        {
            u2NumVlans++;
            OSIX_BITLIST_SET_BIT (pVlanList->au1List, u2VlanId,
                                  MST_VLAN_LIST_SIZE);
            if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), u2VlanId) ==
                OSIX_TRUE)
            {
                u2ActiveVlans++;
                OSIX_BITLIST_SET_BIT (pActiveVlanList->au1List,
                                      u2VlanId, MST_VLAN_LIST_SIZE);
            }
        }
    }
    if (u2NumVlans == 0)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        return MST_SUCCESS;
    }
#ifdef NPAPI_WANTED
    if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
    {
        if (AST_IS_MST_ENABLED ())
        {
            if (FNP_SUCCESS !=
                MstpFsMiMstpNpDelVlanListInstMapping (AST_CURR_CONTEXT_ID (),
                                                      pActiveVlanList->au1List,
                                                      u2MstInst, u2ActiveVlans,
                                                      &u2LastVlan))
            {
                AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                              u2LastVlan, u2MstInst);
                AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                              AST_EVENT_HANDLING_DBG,
                              "API: Vlan %d to instance %d delete mapping Hardware call failed.. \n",
                              u2LastVlan, u2MstInst);
            }
        }
    }
#endif /*NPAPI_WANTED */

    u2ActiveVlans = 0;
    for (u2ByteInd = 0; ((u2ByteInd < MST_VLAN_LIST_SIZE)
                         && (u2VlanCount < u2NumVlans)
                         && (u2LastVlan != u2VlanId)); u2ByteInd++)
    {
        if (pVlanList->au1List[u2ByteInd] != 0)
        {
            u2VlanFlag = pVlanList->au1List[u2ByteInd];

            for (u2BitIndex = 0; ((u2BitIndex < MST_PORTORVLAN_PER_BYTE) &&
                                  (u2VlanFlag != 0) &&
                                  (u2VlanCount < u2NumVlans)); u2BitIndex++)
            {
                if ((u2VlanFlag & MST_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * MST_PORTORVLAN_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (MST_IS_VALID_VLANID (u2VlanId) == MST_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                u2VlanId) == OSIX_TRUE)
                    {
                        u2ActiveVlans++;
                    }
                    u2VlanCount++;
#ifdef NPAPI_WANTED
                    if (u2VlanId == u2LastVlan)
                    {
                        if (u2ActiveVlans == 1)
                        {
                            AST_TRC
                                (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                 "API: Mapping failed.. \n");
                            AST_DBG (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                     AST_EVENT_HANDLING_DBG,
                                     "API: Mapping failed.. \n");
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanList);
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK
                                (pActiveVlanList);
                            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
                            return MST_FAILURE;
                        }
                        break;
                    }
#endif /*NPAPI_WANTED */
                    AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                u2VlanId, AST_INIT_VAL);
                    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (),
                                                u2VlanId) == OSIX_FALSE)
                    {
                        u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        continue;
                    }
                    u4FdbId = AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                                      u2VlanId);

                    OSIX_BITLIST_IS_BIT_SET (pFdbList->au1List,
                                             u4FdbId,
                                             MST_FDB_LIST_SIZE, bResult);

                    if (OSIX_FALSE == bResult)
                    {
                        AstVlanMiFlushFdbId (AST_CURR_CONTEXT_ID (), u4FdbId);
                        OSIX_BITLIST_SET_BIT (pFdbList->au1List,
                                              u4FdbId, MST_FDB_LIST_SIZE);
                    }
                    AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                                MST_UPDATE_INST_VLAN_MAP_MSG,
                                                MST_CIST_CONTEXT,
                                                u2MstInst,
                                                u2VlanId,
                                                MST_UNMAP_INST_VLAN_LIST_MSG,
                                                &u1Result);
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    MST_CIST_CONTEXT,
                                    u2MstInst,
                                    u2VlanId,
                                    MST_UNMAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    MstTriggerDigestCalc ();

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
        /* Unmap has been called after instance has been deleted */
        return MST_SUCCESS;
    }

    AstMstInstDownTrap (u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                        AST_MST_TRAPS_OID_LEN);

    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pActiveVlanList);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pFdbList);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstVlanIdInstMap                           */
/*                                                                           */
/*    Description               : This routine creates the given instance if */
/*                                it is not already created and then maps    */
/*                                the given Vlan to it. It then triggers     */
/*                                the config digest calculation and generates*/
/*                                the region change trap.                    */
/*                                                                           */
/*    Input(s)                  : u2VlanId  - VlanId which is to be mapped.  */
/*                                u2MstInst - Instance Id to which mapping   */
/*                                            needs to be done.              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstVlanIdInstMap (UINT2 u2MstInst, UINT2 u2VlanId)
{
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    UINT1              *pMstVlanList = NULL;
    UINT2               u2OldInstance;
    UINT1               u1Result = MST_INST_VLAN_MAP_MSG_POSTED;
    UINT4               u4FdbId;
    BOOL1               bInstUp = OSIX_FALSE;

    pMstVlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

    if (pMstVlanList == NULL)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pMstVlanList:Memory allocation for Vlan List failed.\n");

        return MST_FAILURE;
    }

    u2OldInstance = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                  u2VlanId);

    AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId, u2MstInst);

    AST_MEMSET (pMstVlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));

    OSIX_BITLIST_SET_BIT (pMstVlanList, u2VlanId, sizeof (tAstMstVlanList));

    if (u2MstInst == AST_TE_MSTID)
    {
        if (MstCreateInstance (u2MstInst, pMstVlanList) == MST_FAILURE)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SYS: CreateInstance Return Failure for Inst: %u\n",
                          u2MstInst);

            AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId,
                                        u2OldInstance);
            UtilVlanReleaseVlanListSize (pMstVlanList);
            return MST_FAILURE;
        }
    }
    else
    {
        if (AST_GET_PERST_INFO (u2MstInst) == NULL)
        {
            if (MstCreateInstance (u2MstInst, pMstVlanList) == MST_FAILURE)
            {
                AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                              "SYS: CreateInstance Return Failure for Inst: %u\n",
                              u2MstInst);

                AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId,
                                            u2OldInstance);
                UtilVlanReleaseVlanListSize (pMstVlanList);
                return MST_FAILURE;
            }
            /* Only for Active Instances, Not for TE_MSTID */
            bInstUp = OSIX_TRUE;
        }
    }

    if (AstL2IwfMiIsVlanActive (AST_CURR_CONTEXT_ID (), u2VlanId) == OSIX_TRUE)
    {

#ifdef NPAPI_WANTED
        if (AST_IS_NP_PROGRAMMING_ALLOWED () == AST_TRUE)
        {
            if (AST_IS_MST_ENABLED ())
            {
                if (FNP_SUCCESS !=
                    MstpFsMiMstpNpAddVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                      u2VlanId, u2MstInst))
                {
                    AST_DBG_ARG2 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                  AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG,
                                  "Creating mapping for vlan %d instance %d in the hardware failed\n",
                                  u2VlanId, u2MstInst);
                    UtilVlanReleaseVlanListSize (pMstVlanList);
                    return MST_FAILURE;
                }
            }
        }
#endif /* NPAPI_WANTED */
        u4FdbId = AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (), u2VlanId);
        AstVlanMiFlushFdbId (AST_CURR_CONTEXT_ID (), u4FdbId);
        /* GARP Updation - Pass VLAN Identifier */

        if (u2MstInst != AST_TE_MSTID)
        {
            AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                        MST_UPDATE_INST_VLAN_MAP_MSG,
                                        u2MstInst,
                                        u2OldInstance, u2VlanId,
                                        MST_MAP_INST_VLAN_LIST_MSG, &u1Result);
            /*Non-Blocking Vlan suppose to be updated only  for Static Configuration.
             * So no need of updation of Vlan to Instance mapping in GARP for TE_MSTID. */
        }

    }

    if (u2MstInst != AST_TE_MSTID)
    {
        AstL2IwfSetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId,
                                    u2MstInst);
    }

    if (MST_INST_VLAN_MAP_MSG_UPDATED == u1Result)
    {
        /* Since its msg post only the first parameter is considered */
        AstAttrRPUpdateInstVlanMap (AST_CURR_CONTEXT_ID (),
                                    MST_POST_INST_VLAN_MAP_MSG,
                                    u2MstInst,
                                    u2OldInstance, u2VlanId,
                                    MST_MAP_INST_VLAN_LIST_MSG, &u1Result);
    }

    MstTriggerDigestCalc ();

    /* Only for Active Instances, Not for TE_MSTID */
    if (u2MstInst != AST_TE_MSTID)
    {
        if (AstAssertBegin () == MST_FAILURE)
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                     AST_INIT_SHUT_TRC, "SYS: Asserting BEGIN failed!\n");
            AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                     "SYS: Asserting BEGIN failed!\n");
            UtilVlanReleaseVlanListSize (pMstVlanList);
            return MST_FAILURE;
        }
    }

    /* Incerementing the MST Region Configuration Identifier Change Count */
    AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT ();
    pMstConfigIdInfo = AST_GET_MST_CONFIGID_ENTRY ();
    AstMstRegionConfigChangeTrap (pMstConfigIdInfo, (INT1 *) AST_MST_TRAPS_OID,
                                  AST_MST_TRAPS_OID_LEN);

    if (bInstUp == OSIX_TRUE)
    {
        /* Incrementing the Instance UP Count */
        AST_INCR_INSTANCE_UP_COUNT (u2MstInst);
        AstMstInstUpTrap (u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                          AST_MST_TRAPS_OID_LEN);
    }
    AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_MGMT_DBG,
                  "SYS: Vlan %d successfully mapped to Instance %d \n",
                  u2VlanId, u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Vlan %d successfully mapped to Instance %d \n",
                  u2VlanId, u2MstInst);
    UtilVlanReleaseVlanListSize (pMstVlanList);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstResetCounters                                     */
/*                                                                           */
/* Description        : This function resets all bridge and port statistics  */
/*                      counters across all instances                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstResetCounters ()
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT2                i2MstInst = 0;
    UINT2               u2PortNum;

    pBrgInfo = AST_GET_BRGENTRY ();
    if (NULL == pBrgInfo)
    {
        return;
    }

    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    if (NULL == pMstBridgeEntry)
    {
        return;
    }

    pBrgInfo->u4AstpDownCount = AST_INIT_VAL;
    pBrgInfo->u4AstpUpCount = AST_INIT_VAL;
    pMstBridgeEntry->u4RegionCfgChangeCount = AST_INIT_VAL;

    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (i2MstInst);
        if (NULL == pPerStBrgInfo)
        {
            return;
        }

        pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
        pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
        pPerStBrgInfo->u4TotalFlushCount = AST_INIT_VAL;
        pPerStBrgInfo->u4FlushIndCount = AST_INIT_VAL;
        MEMSET (AST_CURR_CONTEXT_INFO ()->pInstanceUpCount, AST_INIT_VAL,
                (AST_MAX_MST_INSTANCES * sizeof (UINT4)));
    }

    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pPortInfo)
    {
        if (pPortInfo == NULL)
        {
            continue;
        }
        /* Resets the CEP Port statistics counters */
        if (pPortInfo->u1PortType == AST_CUSTOMER_EDGE_PORT)
        {
            if (RST_FAILURE == RstResetCepPortCounters (pPortInfo))
            {
                AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                         "Clearing CEP Port Counters Returned failure !!!\n");
                continue;
            }
        }
        else
        {

            pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
            pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
            pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
            pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
            pPortInfo->u4NumRstImpossibleStateOcc = AST_INIT_VAL;
            pPortInfo->u4NumRstRxdInfoWhileExpCount = AST_INIT_VAL;
            i2MstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
                pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
                pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
                pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
                pPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
                pPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
                pPerStPortInfo->u4ProposalTxCount = AST_INIT_VAL;
                pPerStPortInfo->u4ProposalRcvdCount = AST_INIT_VAL;
                pPerStPortInfo->u4AgreementTxCount = AST_INIT_VAL;
                pPerStPortInfo->u4AgreementRcvdCount = AST_INIT_VAL;
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : MstResetPerStCounters                                */
/*                                                                           */
/* Description        : This function resets all bridge and port statistics  */
/*                      counters for the specific instance                   */
/*                                                                           */
/* Input(s)           : Instance Index                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstResetPerStCounters (UINT2 u2InstIndex)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum;

    pPerStInfo = AST_GET_PERST_INFO (u2InstIndex);

    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Instance %d not present \n", u2InstIndex);
        return;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstIndex);
    if (NULL == pPerStBrgInfo)
    {
        return;
    }

    pPerStBrgInfo->u4NewRootIdCount = AST_INIT_VAL;
    pPerStBrgInfo->u4NumTopoCh = AST_INIT_VAL;
    pPerStBrgInfo->u4TotalFlushCount = AST_INIT_VAL;
    pPerStBrgInfo->u4FlushIndCount = AST_INIT_VAL;
    MEMSET (AST_CURR_CONTEXT_INFO ()->pInstanceUpCount, AST_INIT_VAL,
            (AST_MAX_MST_INSTANCES * sizeof (UINT4)));

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
        pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
        pPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
        pPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
        pPerStPortInfo->u4ProposalTxCount = AST_INIT_VAL;
        pPerStPortInfo->u4ProposalRcvdCount = AST_INIT_VAL;
        pPerStPortInfo->u4AgreementTxCount = AST_INIT_VAL;
        pPerStPortInfo->u4AgreementRcvdCount = AST_INIT_VAL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : MstResetPerStPortCounters                            */
/*                                                                           */
/* Description        : This function resets all  port statistics            */
/*                      counters for the specific instance                   */
/*                                                                           */
/* Input(s)           : u2InstIndex - Instance Index                         */
/*                     u2PortNum - Port Number                               */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstResetPerStPortCounters (UINT2 u2InstIndex, UINT2 u2PortNum)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    pPerStInfo = AST_GET_PERST_INFO (u2InstIndex);
    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Instance %d not present \n", u2InstIndex);
        return;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstIndex);

    if (pPerStPortInfo == NULL)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s not present for Instance %d \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2InstIndex);
        return;
    }

    pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
    pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
    pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
    return;
}

/*****************************************************************************/
/* Function Name      : MstResetPortCounters                                 */
/*                                                                           */
/* Description        : This function resets all port statistics             */
/*                      counters across all instances                        */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Identifier                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstResetPortCounters (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    INT2                i2MstInst;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (NULL == pPortInfo)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s not present \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return;
    }
    /* Resets the CEP Port statistics counters */
    if (pPortInfo->u1PortType == AST_CUSTOMER_EDGE_PORT)
    {
        if (RST_FAILURE == RstResetCepPortCounters (pPortInfo))
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                     "Clearing CEP Port Counters Returned failure !!!\n");
            return;
        }
    }

    pPortInfo->u4NumRstBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusRxd = AST_INIT_VAL;
    pPortInfo->u4NumRstBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumConfigBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4NumTcnBpdusTxd = AST_INIT_VAL;
    pPortInfo->u4InvalidRstBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidConfigBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4InvalidTcnBpdusRxdCount = AST_INIT_VAL;
    pPortInfo->u4ProtocolMigrationCount = AST_INIT_VAL;
    pPortInfo->u4NumRstImpossibleStateOcc = AST_INIT_VAL;
    pPortInfo->u4NumRstRxdInfoWhileExpCount = AST_INIT_VAL;

    i2MstInst = 0;
    AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, (UINT2) i2MstInst);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo->u4NumBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumBpdusTx = AST_INIT_VAL;
        pPerStPortInfo->u4NumInvalidBpdusRx = AST_INIT_VAL;
        pPerStPortInfo->u4NumFwdTransitions = AST_INIT_VAL;
        pPerStPortInfo->u4TcDetectedCount = AST_INIT_VAL;
        pPerStPortInfo->u4TcRcvdCount = AST_INIT_VAL;
        pPerStPortInfo->u4ProposalTxCount = AST_INIT_VAL;
        pPerStPortInfo->u4ProposalRcvdCount = AST_INIT_VAL;
        pPerStPortInfo->u4AgreementTxCount = AST_INIT_VAL;
        pPerStPortInfo->u4AgreementRcvdCount = AST_INIT_VAL;
    }
    return;

}

/*MSTP clear statistics feature*/
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstStartSemsForPort                        */
/*                                                                           */
/*    Description               : This function initialises all the State    */
/*                                Machines relatd to the port as per the     */
/*                                instance.                                  */
/*                                                                           */
/*    Input(s)                 :  u2PortNum - Port Number of the port that   */
/*                                            is to be created.              */
/*                                u2MstInst - Instance Identifier            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstStartSemsForPort (UINT2 u2PortNum, UINT2 u2MstInst)
#else
INT4
MstStartSemsForPort (u2PortNum, u2MstInst)
     UINT2               u2PortNum;
     UINT2               u2MstInst;
#endif
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s Inst %d: Starting SEMs ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s Inst %d: Starting SEMs ...\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    if (AST_GET_PERST_INFO (u2MstInst) == NULL)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Instance %d not created \n", u2MstInst);
        return MST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
    if (pPerStPortInfo == NULL)
    {
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s not present for Instance %d \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        RstBrgDetectionMachine (RST_BRGDETSM_EV_BEGIN, u2PortNum);

        if (RstPortMigrationMachine ((UINT2) RST_PMIGSM_EV_BEGIN, u2PortNum) !=
            RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure for BEGIN event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_PMSM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s: Port Protocol Migration Machine Returned failure for BEGIN event !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }
    }

    if (RstPortStateTrMachine ((UINT2) RST_PSTATETRSM_EV_BEGIN, pPerStPortInfo,
                               u2MstInst) != RST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port State Transition Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_STSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port State Transition Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_BEGIN, pPerStPortInfo,
                                   u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port Role Transition Machine Returned failure  for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_RTSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port Role Transition Machine Returned failure  for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    if (MstTopologyChMachine (MST_TOPOCHSM_EV_BEGIN, u2MstInst,
                              pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port Topology Change Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port Topology Change Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    if (MstPortInfoMachine (MST_PINFOSM_EV_BEGIN, pPerStPortInfo,
                            NULL, u2MstInst) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                      AST_CONTROL_PATH_TRC,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for BEGIN event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Port %s Inst %d: Port Info Machine Returned failure for BEGIN event  !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        return MST_FAILURE;
    }

    if (RstPortTransmitMachine ((UINT2) RST_PTXSM_EV_BEGIN, pPortInfo,
                                u2MstInst) != RST_SUCCESS)
    {
        AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                      "SYS: Port %s: Port Transmit Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_TXSM_DBG |
                      AST_ALL_FAILURE_DBG,
                      "SYS: Port %s: Port Transmit Machine Returned failure for BEGIN event !!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        return MST_FAILURE;
    }

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_BEGIN, u2PortNum, NULL) !=
            RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for BEGIN event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            AST_DBG_ARG2 (AST_INIT_SHUT_DBG | AST_PISM_DBG |
                          AST_ALL_FAILURE_DBG,
                          "SYS: Port %s Inst %d: Pseudo Info Machine Returned failure for BEGIN event  !!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;

        }
    }

    AST_DBG_ARG2 (AST_INIT_SHUT_DBG,
                  "SYS: Port %s Inst %d: StartSemsForPort: Completed starting SEMs \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Port %s Inst %d: StartSemsForPort: Completed starting SEMs \n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstUpdtProtoPortStateInL2Iwf               */
/*                                                                           */
/*    Description               : This function updates the spanning tree    */
/*                                status of this port in l2iwf.              */
/*                                                                           */
/*    Input(s)                 :  u2Port - Port where this status change has */
/*                                         occured.                          */
/*                                u1TrigType - AST_STP_PORT_DOWN or          */
/*                                             AST_STP_PORT_UP               */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdtProtoPortStateInL2Iwf (UINT2 u2Port, UINT1 u1TrigType)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    INT2                i2MstInst;

    switch (u1TrigType)
    {
        case AST_STP_PORT_UP:
            /* Spanning tree status is up for this port and for some
             * msti. */
            AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_STP,
                                                    OSIX_ENABLED);
            break;
        case AST_STP_PORT_DOWN:
            /* If Spanning tree is enabled for atleast MST instance, then
             * set the stp status for this port as enabled in l2iwf.
             * otherwise set stp status for this port as disabled in l2iwf. */
            i2MstInst = 0;
            AST_GET_NEXT_BUF_INSTANCE (i2MstInst, pPerStInfo)
            {
                if (pPerStInfo == NULL)
                {
                    continue;
                }
                pPerStPortInfo =
                    AST_GET_PERST_PORT_INFO (u2Port, (UINT2) i2MstInst);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_TRUE)
                {
                    /* Spanning tree is enabled for an MST instance. 
                     * So don't do any thing here.*/
                    return MST_SUCCESS;
                }
            }

            /* Spanning tree is disabled on this port for all Mst instances,
             * hence set the stp status of this port as disabled in l2iwf. */
            AstL2IwfSetProtocolEnabledStatusOnPort (AST_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_STP,
                                                    OSIX_DISABLED);

            break;
        default:
            break;
    }

    return MST_SUCCESS;
}

#ifdef NPAPI_WANTED
VOID
MstpRetryNpapiForAllInstances (tAstPortEntry * pPortEntry)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2InstanceId = 0;
    UINT2               u2PortNum = pPortEntry->u2PortNo;

    AST_GET_NEXT_BUF_INSTANCE (u2InstanceId, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        if (pPerStPortInfo->i4NpFailRetryCount < AST_MAX_RETRY_COUNT)
        {
            pPerStPortInfo->i4NpFailRetryCount++;

            if (pPerStPortInfo->i4NpPortStateStatus == AST_BLOCK_FAILURE)
            {
                if (FsMiMstpNpWrSetInstancePortState
                    (AST_CURR_CONTEXT_ID (), AST_GET_PHY_PORT (u2PortNum),
                     u2InstanceId, AST_PORT_STATE_DISCARDING) == FNP_FAILURE)
                {
                    AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                               AST_PORT_STATE_DISCARDING);
                }
                else if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_SYNCHRONOUS)
                {
                    AstHwPortStateUpdtSuccess (pPortEntry, u2InstanceId,
                                               AST_PORT_STATE_DISCARDING);
                }
            }
            else if (pPerStPortInfo->i4NpPortStateStatus == AST_FORWARD_FAILURE)
            {
                if (FsMiMstpNpWrSetInstancePortState
                    (AST_CURR_CONTEXT_ID (), AST_GET_PHY_PORT (u2PortNum),
                     u2InstanceId, AST_PORT_STATE_FORWARDING) == FNP_FAILURE)
                {
                    AstHwPortStateUpdtFailure (pPortEntry, u2InstanceId,
                                               AST_PORT_STATE_FORWARDING);
                }
                else if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_SYNCHRONOUS)
                {
                    AstHwPortStateUpdtSuccess (pPortEntry, u2InstanceId,
                                               AST_PORT_STATE_FORWARDING);
                }
            }
        }
    }
}
#endif /* NPAPI_WANTED */

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstReCalculatePathcost                     */
/*                                                                           */
/*    Description               : This function calculates path cost for the */
/*                                given port number                          */
/*                                                                           */
/*    Input(s)                 :  u4IfIndex - Interface for which path cost  */
/*                                is to be calculated                        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstReCalculatePathcost (UINT4 u4IfIndex)
{
    UINT2               u2PortNum;
    UINT2               u2Inst = 0;
    UINT4               u4PathCost = 0;
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    u2PortNum = (UINT2) u4IfIndex;

    if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        return MST_FAILURE;
    }

    pPortEntry->u4PathCost = AstCalculatePathcost (u2PortNum);
    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);
        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        u4PathCost = pPerStPortInfo->u4PortPathCost;

        if ((AST_GET_BRG_PORT_TYPE (pPortEntry) == AST_CUSTOMER_EDGE_PORT) &&
            (u2Inst != MST_CIST_CONTEXT))
        {
            break;
        }
        if (AstPathcostConfiguredFlag (u2PortNum, u2Inst) == RST_FALSE)
        {
            pPerStPortInfo->u4PortPathCost = AstCalculatePathcost (u2PortNum);
        }

        if (u4PathCost != pPerStPortInfo->u4PortPathCost)
        {
            if (AST_IS_MST_ENABLED ())
            {
                pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
                pPerStRstPortInfo->bSelected = MST_FALSE;
                MstPUpdtAllSyncedFlag (u2Inst, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);
                pPerStRstPortInfo->bReSelect = MST_TRUE;

                pPortEntry->bAllTransmitReady = MST_FALSE;

                if (RstPortRoleSelectionMachine
                    ((UINT1) RST_PROLESELSM_EV_RESELECT, u2Inst) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_MGMT_TRC,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    AST_DBG (AST_MGMT_DBG | AST_ALL_FAILURE_DBG,
                             "SNMP: RoleSelectionMachine returned FAILURE!\n");
                    return MST_FAILURE;
                }
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortBPDUGuard                        */
/*                                                                           */
/*    Description               : This function reselects topology when      */
/*                                bpdu guard is set by management and        */
/*                                configures the port mstp status            */
/*                                                                           */
/*    Input(s)                  : u4PortNum - Port Number of the Port        */
/*                                            which is to be made Access     */
/*                                GuardType - BPDU guard                     */
/*                                bStatus- Status of the port to be set      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                  */
/*                                MST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
MstSetPortBPDUGuard (UINT4 u4PortNo, UINT4 u4GuardType, UINT4 u4BpduGuardStatus)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT1               u1OperStatus = 0;
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);

    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return MST_FAILURE;
    }
    if (u4GuardType == AST_MST_CONFIG_BPDUGUARD_PORT_MSG)
    {
        if (pCommPortInfo->u4BpduGuard == u4BpduGuardStatus)
        {
            AST_TRC_ARG1 (AST_INIT_SHUT_TRC | AST_CONTROL_PATH_TRC,
                          "SYS: MstpStatus: Bpdu Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_RSSM_DBG,
                          "SYS: Bpdu Guard Status is same as"
                          " set value for port = %d !!!\n", u4PortNo);
            return MST_SUCCESS;
        }
        pCommPortInfo->u4BpduGuard = u4BpduGuardStatus;

        if (u4BpduGuardStatus != AST_TRUE)
        {
            if ((pCommPortInfo->bBpduInconsistent == RST_TRUE) &&
                (pCommPortInfo->u1BpduGuardAction == AST_PORT_ADMIN_DOWN))
            {
                NotifyProtoToApp.STPNotify.u1AdminStatus = AST_PORT_ADMIN_UP;
                NotifyProtoToApp.STPNotify.u4IfIndex =
                    AST_GET_IFINDEX (u4PortNo);
                STPCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
            }
            else
            {
                AstL2IwfGetPortOperStatus (STP_MODULE, u4PortNo, &u1OperStatus);
                if (u1OperStatus == AST_PORT_OPER_UP)
                {

                    AstEnablePort ((UINT2) u4PortNo, MST_CIST_CONTEXT,
                                   AST_EXT_PORT_UP);
                }
            }
            /* BpduInconsistent reset when Bpdu guard is disabled */
            pCommPortInfo->bBpduInconsistent = AST_FALSE;

            /*Stop the Error Disable Recovery timer if it is running */
            if (pCommPortInfo->pDisableRecoveryTmr != NULL)
            {
                if (AstStopTimer (pPortEntry,
                                  AST_TMR_TYPE_ERROR_DISABLE_RECOVERY)
                    != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Unable to stop the running ERROR"
                             " DISABLE RECOVERY Timer!\n");
                    return RST_FAILURE;
                }
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstSetPortBPDUGuardAction                  */
/*                                                                           */
/*    Description               : This function reselects topology when      */
/*                                bpdu guard is set by management and        */
/*                                configures the port mstp status            */
/*                                                                           */
/*    Input(s)                  : u4PortNo - Port Number of the Port         */
/*                                            which is to be made Access     */
/*                                u1BpduGuardAction - BPDU guard action      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                  */
/*                                MST_FAILURE On Failure.                  */
/*                                                                           */
/*****************************************************************************/
INT4
MstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tAstCfaIfInfo       IfInfo;
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u4PortNo);
    pPortEntry = AST_GET_PORTENTRY (u4PortNo);
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    AST_MEMSET (&IfInfo, AST_INIT_VAL, sizeof (tCfaIfInfo));
    if ((pCommPortInfo == NULL) || (pPortEntry == NULL))
    {
        return MST_FAILURE;
    }
    pCommPortInfo->u1BpduGuardAction = u1BpduGuardAction;

    return MST_SUCCESS;
}

#endif /* MSTP_WANTED */
