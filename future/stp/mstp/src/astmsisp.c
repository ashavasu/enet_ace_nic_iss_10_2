/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmsisp.c,v 1.16 2016/05/11 11:42:52 siva Exp $
*
* Description: File containing routines that will be used to realise
*              SISP functionality in STP module
*********************************************************************/

#ifndef _ASTMSISP_C_
#define _ASTMSISP_C_

#include "astminc.h"

/*****************************************************************************/
/* Function Name      : MstSispHandleUpdateSispStatus                        */
/*                                                                           */
/* Description        : This function will update the SISP enabled or        */
/*                      disabled status on a port. MSTP will begin to process*/
/*                      the SISP interface applying the SISP logic on the    */
/*                      interface when SISP is enabled. And MSTP will        */
/*                      relinquish the SISP logic on the interface when SISP */
/*                      is disabled.                                         */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to the Msg passed to ASTP.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispHandleUpdateSispStatus (tAstMsgNode * pMsgNode)
{
    INT4                i4RetVal = RST_FAILURE;

    /* Operating mode (MSTP/RSTP/STP compatible) checks would have been 
     * performed in the API level itself.
     * */

    switch (pMsgNode->uMsg.bSispStatus)
    {
        case AST_TRUE:

            i4RetVal =
                MstSispEnableSispOnInterface ((UINT2) pMsgNode->u4PortNo);
            break;

        case AST_FALSE:

            i4RetVal =
                MstSispDisableSispOnInterface ((UINT2) pMsgNode->u4PortNo);
            break;

        default:
            /* Cannot have a message type other than enable or disable
             * */
            i4RetVal = RST_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : MstSispEnableSispOnInterface                         */
/*                                                                           */
/* Description        : This utility will enable SISP logic on the given     */
/*                      interface.                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Identifier on which SISP      */
/*                                  should be enabled.                       */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstSispEnableSispOnInterface (UINT2 u2PortNum)
{
    tMstVlanList       *pVlanListInInst = NULL;
    tMstVlanList       *pVlanListInPort = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst = 1;
    UINT1               u1Result = MST_FALSE;

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pVlanListInInst) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return RST_FAILURE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pVlanListInPort) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return RST_FAILURE;
    }

    /* When SISP is enabled on an interface, the following should be taken care
     * 1) This port should not be made member of instances that are mapped 
     *    to vlans for which this port is not member of.
     * 2) SISP BPDU reception and transmission logic should be enabled on this
     *    port.
     * This function should return RST_SUCCESS/RST_FAILURE as it is being 
     * invoked by astpmsg.c
     * */

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Enabling SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Enabling SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_MEMSET (&OctetStr, AST_INIT_VAL, sizeof (OctetStr));

    AST_MEMSET (pVlanListInPort->au1List, AST_INIT_VAL,
                VLAN_DEV_VLAN_LIST_SIZE);

    /* The port should be present in this instance, if and only if this port
     * is a member of any one of the vlans that are mapped to this instance
     * */

    OctetStr.pu1_OctetList = pVlanListInPort->au1List;
    OctetStr.i4_Length = MST_VLAN_LIST_SIZE;

    AST_GET_SISP_STATUS (u2PortNum) = MST_TRUE;

    /* Get the list of VLANs for this port using VLAN API */
    if (AstL2IwfGetMemberVlanList (AST_CURR_CONTEXT_ID (),
                                   AST_GET_IFINDEX (u2PortNum),
                                   &OctetStr) != L2IWF_SUCCESS)
    {
        /* Port should not be member of any of the instances
         * */
        AST_MEMSET (&OctetStr, AST_INIT_VAL, sizeof (OctetStr));
    }

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {
        AST_MEMSET (pVlanListInInst->au1List, AST_INIT_VAL, MST_VLAN_LIST_SIZE);
        u1Result = MST_FALSE;

        if (pPerStInfo == NULL)
        {
            /* Instance is not created */
            continue;
        }

        AstL2IwfMiGetVlanListInInstance (AST_CURR_CONTEXT_ID (), u2MstInst,
                                         pVlanListInInst->au1List);

        AST_IS_VLANS_MATCH (pVlanListInPort->au1List, pVlanListInInst->au1List,
                            u1Result);

        if (u1Result == MST_TRUE)
        {
            /* This port should be member of this instance */
            if (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) == NULL)
            {
                /* Port is not a member of this instance, Create the port */
                if (MstCreatePerStPortEntry (u2PortNum, u2MstInst,MST_TRUE)
                    != MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  " CreatePort: Port %s: CreatePerStPortEntry returned failure for Inst %s!!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                }
            }
        }
        else
        {
            /* This port should not be member of this instance */
            if (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) != NULL)
            {
                /* Port is already part of this instance, delete the same */
                if (MstDeletePerStPortEntry (u2PortNum, u2MstInst) !=
                    MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  "DeletePort: Port %s Inst %d: DeletePerStPortEntry returned failure !\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                }
            }
        }
    }

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Enabled SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Enabled SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispDisableSispOnInterface                        */
/*                                                                           */
/* Description        : This utility will disable SISP logic on the given    */
/*                      interface.                                           */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Identifier on which SISP      */
/*                                  should be enabled.                       */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstSispDisableSispOnInterface (UINT2 u2PortNum)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst = 1;

    /* When SISP is disabled on an interface, the following should be taken care
     * 1) This port should be made member of all the active instances.
     * 2) SISP BPDU reception and transmission logic should be disable on this
     *    port.
     * This function should return RST_SUCCESS/RST_FAILURE as it is being 
     * invoked by astpmsg.c
     * */

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Disabling SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Disabling SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    /* If SISP is not enabled on this interface, return success */
    if (AST_GET_SISP_STATUS (u2PortNum) == MST_FALSE)
    {
        return RST_SUCCESS;
    }

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            /* Instance is not created */
            continue;
        }

        /* This port should be member of all the active instances */
        if (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) == NULL)
        {
            /* Port is not a member of this instance, Create the port */
            if (MstCreatePerStPortEntry (u2PortNum, u2MstInst,MST_TRUE) != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              " CreatePort: Port %s: CreatePerStPortEntry returned failure for Inst %s!!!\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            }
        }
    }

    AST_GET_SISP_STATUS (u2PortNum) = MST_FALSE;

    AST_DBG_ARG1 (AST_INIT_SHUT_DBG, "SYS: Disabled SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Disabled SISP in port %u ... \n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispHandleUpdateVlanPortList                      */
/*                                                                           */
/* Description        : This function will will add the SISP enabled ports to*/
/*                      corresponding instances or delete the SISP enabled   */
/*                      ports from instances to which the given vlan is      */
/*                      mapped to depending upon the configuration.          */
/*                                                                           */
/* Input(s)           : pMsgNode - Pointer to the Msg passed to ASTP.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispHandleUpdateVlanPortList (tAstMsgNode * pMsgNode)
{
    UINT2               u2PortNum = AST_INIT_VAL;
    tAstBoolean         bResult = OSIX_FALSE;

    /* Process for the added ports in the vlan */
    for (u2PortNum = 1; u2PortNum <= AST_MAX_PORTS_PER_CONTEXT; u2PortNum++)
    {
        bResult = OSIX_FALSE;

        if (AST_GET_PORTENTRY (u2PortNum) == NULL)
        {
            continue;
        }

        if (AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)
        {
            AST_IS_PORTBITLIST_SET (pMsgNode->AddedPorts, u2PortNum,
                                    CONTEXT_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                /* This is a SISP enabled port and has been added to a vlan. 
                 * Make  this port member of the corresponding instance, to 
                 * which it is mapped to.
                 * */
                if (MstSispVlanAddPort (u2PortNum, pMsgNode->uMsg.VlanId) !=
                    MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  " Vlan-Port: Port %s: MstSispVlanAddPort returned failure for Vlan %s!!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum),
                                  pMsgNode->uMsg.VlanId);
                }
            }                    /* Port is added to vLAN */

            AST_IS_PORTBITLIST_SET (pMsgNode->DeletedPorts, u2PortNum,
                                    CONTEXT_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                /* This is a SISP enabled port and has been removed from a vlan.
                 * Make this port member of the corresponding instance, to 
                 * which  it is mapped to.
                 * */

                if (MstSispVlanDelPort (u2PortNum, pMsgNode->uMsg.VlanId) !=
                    MST_SUCCESS)
                {
                    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                                  " Vlan-Port: Port %s: MstSispVlanAddPort returned failure for Inst %s!!!\n",
                                  AST_GET_IFINDEX_STR (u2PortNum),
                                  pMsgNode->uMsg.VlanId);
                }
            }                    /* Port is deleted from Vlan */
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispVlanAddPort                                   */
/*                                                                           */
/* Description        : This function will make the port member of the       */
/*                      instance to which this vlan is mapped to. This will  */
/*                      also validate this configuration request against the */
/*                      restriction that "same interface cannot be mapped to */
/*                      the same MSTI in two different virtual contexts.     */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      VlanId    - Vlan Identifier.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispVlanAddPort (UINT2 u2PortNum, tVlanId VlanId)
{
    UINT2               u2MstInst = AST_INIT_VAL;

    /* When the SISP enabled port is made member of the vlan, first the 
     * following validations should be done
     * 1) Any other logical port, that runs over the same physical should not 
     *    be a member of the same instance in other context.
     * 
     * If this validation is passed, then this port should be created in the
     * instance that has the mapping to this vlan.
     * */

    u2MstInst = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);

    /* This vlan cannot be mapped to CIST, as CIST cannot be used to carry vlan
     * traffic over shared ports. This validation would have been done in API
     * part itself. Hence no need to do once again.
     * */

    if (MstSispIsVlansPresentInInst (u2PortNum, u2MstInst, NULL, NULL)
        == MST_FALSE)
    {
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "Vlan-Port: Port %s: Validation for Instance to port "
                      "returned failure for Inst %s!!!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        return MST_FAILURE;
    }

    /* Validation passed.Create the port in this instance */

    if (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) == NULL)
    {
        if (MstCreatePerStPortEntry (u2PortNum, u2MstInst,MST_TRUE) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          " CreatePort: Port %s: CreatePerStPortEntry returned failure for Inst %s!!!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispVlanDelPort                                   */
/*                                                                           */
/* Description        : This function will remove the port from the          */
/*                      instance to which this vlan is mapped to.            */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      VlanId    - Vlan Identifier.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispVlanDelPort (UINT2 u2PortNum, tVlanId VlanId)
{
    UINT2               u2MstInst = AST_INIT_VAL;

    u2MstInst = AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), VlanId);

    /* This vlan cannot be mapped to CIST, as CIST cannot be used to carry vlan
     * traffic over shared ports. This validation would have been done in API
     * part itself. Hence no need to do once again.
     * */

    if ((u2MstInst != MST_CIST_CONTEXT) &&
        (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst) != NULL))
    {
        if (MstDeletePerStPortEntry (u2PortNum, u2MstInst) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "DeletePort: Port %s Inst %d: DeletePerStPortEntry returned failure !\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispValidateVlantoInstanceMapping                 */
/*                                                                           */
/* Description        : This function will validate the vlan to instance     */
/*                      mapping request against the restriction, if this     */
/*                      context has sisp enabled ports mapped to the same.   */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Identifier.                            */
/*                      u2MstInst - Instance to which this vlan is to be     */
/*                                  mapped.                                  */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispValidateVlantoInstanceMapping (tVlanId VlanId, UINT2 u2MstInst)
{
    UINT1              *pEgressPorts = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tPortList          *pEgressIfPorts = NULL;
    UINT2               u2PortNum;
    BOOL1               bResult = MST_FALSE;

    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) != VCM_TRUE)
    {
        /* No SISP enabled ports is present in this context
         * return MST_SUCCESS
         * */
        return MST_SUCCESS;
    }

    if (MST_IS_INSTANCE_VALID ((INT4) u2MstInst) == MST_FALSE)
    {
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: MstSispValidateVlantoInstanceMapping "
                      "invoked with invalid inst %d\n", u2MstInst);

        return MST_FAILURE;
    }

    pEgressIfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressIfPorts == NULL)
    {
        AST_DBG (AST_ALL_FAILURE_DBG,
                 "Error in Allocating memory for bitlist\n");
        AST_TRC (AST_ALL_FAILURE_TRC,
                 "Error in Allocating memory for bitlist\n");
        return MST_FAILURE;
    }

    AST_MEMSET (pEgressIfPorts, AST_INIT_VAL, BRG_PORT_LIST_SIZE);

    /* ***************** INST VALIDATION *******************************
     * Following is one of the restrictions that have been imposed on config
     * for realising SISP functionality in spanning tree protocol operation.
     * "Same interface cannot be mapped to same instance in two different
     *  virtual contexts." 
     * The reason being,
     * The Instance-Vlan mapping along with the VLAN port membership holds the 
     * key in deciding the virtual contexts that will process the received MSTI
     * information. Hence, the vlan to instance mapping should be unique across
     * the contexts. But if this is unique, then there may be problem when the 
     * peer is SISP unaware bridge. In such a case, the restriction that the 
     * vlan forwarding path should be in the same region will break. Hence, the 
     * above restriction is derived.
     * */
    AstL2IwfMiGetVlanEgressPorts (AST_CURR_CONTEXT_ID (), VlanId,
                                  *pEgressIfPorts);
    pEgressPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pEgressPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);
        AST_TRC (AST_ALL_FAILURE_TRC,
                 "MstSispValidateVlantoInstanceMapping : Error in allocating memory "
                 "for pEgressPorts\r\n");
        return MST_FAILURE;
    }
    AST_MEMSET (pEgressPorts, AST_INIT_VAL, CONTEXT_PORT_LIST_SIZE);

    AstConvertToLocalPortList (*pEgressIfPorts, pEgressPorts);

    FsUtilReleaseBitList ((UINT1 *) pEgressIfPorts);

    for (u2PortNum = 1; u2PortNum <= AST_MAX_PORTS_PER_CONTEXT; u2PortNum++)
    {
        OSIX_BITLIST_IS_BIT_SET (pEgressPorts, u2PortNum,
                                 CONTEXT_PORT_LIST_SIZE, bResult);

        if (bResult != OSIX_TRUE)
        {
            continue;
        }

        if (AST_GET_SISP_STATUS (u2PortNum) == MST_FALSE)
        {
            /* SISP Disabled port */
            continue;
        }

        pPortEntry = AST_GET_PORTENTRY (u2PortNum);

        if (pPortEntry == NULL)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            /* Port entry cannot be null. Return failure */
            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "VLAN-Port: Invalid Port %s \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }

        if (MstSispIsPortPresentInInst (u2PortNum, u2MstInst) == MST_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                          "VLAN-Port: SISP enabled Port %s is already member of"
                          "Inst %d: in another context!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            return MST_FAILURE;
        }
    }                            /* for all the ports */

    UtilPlstReleaseLocalPortList (pEgressPorts);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispHandleBpduInSisp                              */
/*                                                                           */
/* Description        : This routine will be used to process the BPDU        */
/*                      received in a SISP enabled interface. This will      */
/*                      perform all the validations necessary for processing */
/*                      a BPDU on a SISP enabled interface and will process  */
/*                      the instance information in the corresponding context*/
/*                      port set.                                            */
/*                                                                           */
/* Input(s)           : *pRcvdBpdu  - CRU buffer containing BPDU.            */
/*                      u2PortNum   - Interface index in which the BPDU is   */
/*                                    received.                              */
/*                                                                           */
/* Output(s)          : pu1Result - MST_TRUE if config allowed & MST_FALSE,  */
/*                                  otherwise.                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispHandleBpduInSisp (tMstBpdu * pRcvdBpdu, UINT2 u2PortNum)
{
    tAstBpduType        Bpdu;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4PortCnt = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT1               u4TempPortCnt = AST_INIT_VAL;

    /* If the BPDU is received on a SISP enabled interface, this function will
     * be invoked. This function does the following
     * 1) Invoke SISP to get the list of logical interfaces.
     * 2) For each of the logical interfaces running over the received 
     *    interface,
     * 3) Get the context Info from the port and select the context.
     * 4) If the configuration digest matches, 
     * 6) For each valid instance present in the BPDU, 
     * 7) Is the logical port in the context is member of the vlan to which 
     *    this instance is mapped to.
     * 8) If yes, then extract this instance from the received BPDU and add to 
     *    the local BPDU.
     * 6) After all the valid instances have been scanned, send the bpdu for
     *    processing with this interface index and extracted BPDU
     * */

    AST_MEMSET (&Bpdu, AST_INIT_VAL, sizeof (tAstBpduType));

    AST_MEMSET (&au2SispPorts, AST_INIT_VAL, sizeof (au2SispPorts));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    /* Invoke the API provided by SISP to get the set of lo ports
     * */
    if (MstVcmSispGetSispPortsInfoOfPhysicalPort (AST_GET_IFINDEX (u2PortNum),
                                                  VCM_TRUE,
                                                  (VOID *) au2SispPorts,
                                                  &u4PortCnt) != VCM_SUCCESS)
    {
        AST_DBG (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG
                 | AST_BPDU_DBG,
                 "SISP-BPDU:GetSispPortsInfoOfPhysicalPort returned Failure\n");
        return MST_SUCCESS;
    }

    AST_MEMCPY (&(Bpdu.uBpdu.MstBpdu), pRcvdBpdu, sizeof (tMstBpdu));

    /* As sisp ports should not process the CIST information, Make the 
     * cisp information inferior and give to sisp ports */
    MstSispMakeCistInfoInferior (&(Bpdu.uBpdu.MstBpdu));

    for (u4ContextId = 0; u4TempPortCnt < u4PortCnt; u4ContextId++)
    {
        u2LocalPort = au2SispPorts[u4ContextId];

        if (u2LocalPort == AST_INIT_VAL)
        {
            continue;
        }

        u4TempPortCnt++;

        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        if (!(AST_IS_MST_ENABLED ()))
        {
            continue;
        }

        if ((pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort)) == NULL)
        {
            continue;
        }

        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC
                      | AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                      "SISP-BPDU: Switching to context %d ......\n",
                      u4ContextId);

        if (MstPortRcvFromSameRegion (&(Bpdu.uBpdu.MstBpdu), u2LocalPort) !=
            MST_TRUE)
        {
            /* Configuration digest does not match */
            AST_DBG_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC
                          | AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                          "SISP-BPDU: Configuration Digest does not match for port %d, Context %d \n",
                          AST_GET_PHY_PORT (u2LocalPort), u4ContextId);

            continue;
        }

        AST_DBG_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC
                      | AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                      "SISP-BPDU: Configuration Digest matches for port %d, Context %d \n",
                      AST_GET_PHY_PORT (u2LocalPort), u4ContextId);

        if (RstPseudoInfoMachine (RST_PSEUDO_INFO_EV_RCVD_BPDU,
                                  u2LocalPort, &Bpdu) != RST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RECV: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                          "RECV: Port %s: RstPseudoInfoMachine function returned FAILURE for RCVD_BPDU event!\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));

            continue;

        }

        if (MstPortReceiveMachine ((UINT1) MST_PRCVSM_EV_RCVD_BPDU,
                                   &(Bpdu.uBpdu.MstBpdu),
                                   u2LocalPort) != MST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "RECV: Port %s: MstPortReceiveMachine function returned FAILURE for RCVD_BPDU event!\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            AST_DBG_ARG1 (AST_PRSM_DBG | AST_ALL_FAILURE_DBG,
                          "RECV: Port %s: MstPortReceiveMachine function returned FAILURE for RCVD_BPDU event!\n",
                          AST_GET_IFINDEX_STR (u2LocalPort));
            continue;
        }

        MstRedSyncUpPdu (u2LocalPort, &(Bpdu.uBpdu.MstBpdu), sizeof (tMstBpdu));

        AstReleaseContext ();
    }                            /* Next valid port over this interface */

    /* Restore the correct context
     * */

    AstSelectContext (u4CurrContextId);

    AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC
                  | AST_INIT_SHUT_DBG | AST_BPDU_DBG,
                  "SISP-BPDU: Restoring context %d ......\n", u4ContextId);

    AST_DBG (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG,
             "SISP-BPDU: BPDU received on SISP enabled interface processed....\n");

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispDeriveSispDefaults                            */
/*                                                                           */
/* Description        : This routine will invoke SISP API to get the SISP    */
/*                      status for the created interface. If it is SISP      */
/*                      enabled, it would update its data structure          */
/*                      accordingly, so that by default this port does not   */
/*                      become member of all the instances.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Identifier on which SISP      */
/*                                  should be enabled.                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispDeriveSispDefaults (UINT2 u2PortNum)
{
    tAstPortEntry      *pPhyPortEntry = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4PhyIndex = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_INVALID_CONTEXT;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    UINT1               u1Status = MST_FALSE;
    INT4                i4Retval = AST_INIT_VAL;

    if (AST_IS_PORT_SISP_LOGICAL (u2PortNum) == MST_FALSE)
    {
        u1Status = AstSispIsSispEnabledOnPort (AST_GET_IFINDEX (u2PortNum));

        AST_GET_SISP_STATUS (u2PortNum) = u1Status;

        AST_GET_PHY_PORT (u2PortNum) = AST_GET_IFINDEX (u2PortNum);
    }
    else
    {
        /* This is a logical port, SISP Status will always be true */
        AST_GET_SISP_STATUS (u2PortNum) = MST_TRUE;

        AST_GET_PHY_PORT (u2PortNum) = AST_INIT_VAL;

        if (MstVcmSispGetPhysicalPortOfSispPort (AST_GET_IFINDEX (u2PortNum),
                                                 &u4PhyIndex) != VCM_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_TRC |
                          AST_CONTROL_PATH_TRC,
                          "SYS: SISP Phy port retrieval failed for %u \n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            return MST_FAILURE;
        }

        pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);

        if (pAstPortEntry == NULL)
        {
            return MST_FAILURE;
        }

        AST_GET_PHY_PORT (u2PortNum) = u4PhyIndex;

        u4CurrContextId = AST_CURR_CONTEXT_ID ();

        i4Retval =
            AstGetContextInfoFromIfIndex (u4PhyIndex, &u4ContextId,
                                          &u2LocalPort);
        UNUSED_PARAM (i4Retval);

        AstSelectContext (u4ContextId);

        pPhyPortEntry = AST_GET_PORTENTRY (u2LocalPort);

        if (pPhyPortEntry == NULL)
        {
            /* Port may not be created in the primary ctxt or spanning-tree 
             * may in shut down state */
            AstSelectContext (u4CurrContextId);

            return MST_SUCCESS;
        }
        pAstPortEntry->u1AdminPointToPoint = pPhyPortEntry->u1AdminPointToPoint;

        pAstPortEntry->bAutoEdge = pPhyPortEntry->bAutoEdge;

        pAstPortEntry->bAdminEdgePort = pPhyPortEntry->bAdminEdgePort;

        AstSelectContext (u4CurrContextId);
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispUpdatePortInstList                            */
/*                                                                           */
/* Description        : This routine will update the port based instance list*/
/*                      based on u1Action. If the Action is ADD, then it will*/
/*                      set the bitmap for this instance both in the logical */
/*                      port and the corresponding physical port. If action  */
/*                      is DELETE, then it will reset the bitmap in both the */
/*                      interfaces.                                          */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Identifier on which SISP      */
/*                                  should be enabled.                       */
/*                      u2MstInst - Instance identifier.                     */
/*                      u1Action  - MST_ADD/MST_DELETE                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispUpdatePortInstList (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1Action)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPortEntry      *pPhyPortEntry = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4PhyIndex = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    /* Get the physical port and switch to physical port's context
     * */
    u4PhyIndex = AST_GET_PHY_PORT (u2PortNum);

    if (AstGetContextInfoFromIfIndex (u4PhyIndex, &u4ContextId,
                                      &u2LocalPort) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }
    /* Physical port's context has been selected */
    pPhyPortEntry = AST_GET_PORTENTRY (u2LocalPort);

    switch (u1Action)
    {
        case MST_ADD:
            /* Set the port based instance list for this instance
             * in the corresponding physical port and this logical
             * port also.
             * */
            AST_SISP_SET_INST_LIST (pPortEntry->InstList, u2MstInst);

            AST_SISP_SET_INST_LIST (pPhyPortEntry->InstList, u2MstInst);

            break;

        case MST_DELETE:
            /* Reset the port based instance list for this instance
             * in the corresponding physical port and this logical
             * port also.
             * */
            AST_SISP_RESET_INST_LIST (pPortEntry->InstList, u2MstInst);

            AST_SISP_RESET_INST_LIST (pPhyPortEntry->InstList, u2MstInst);
            break;

        default:
            /* Cannot come here */

            break;
    }

    AstReleaseContext ();

    /* Restore logical's context */
    AstSelectContext (u4CurrContextId);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispMakeCistInfoInferior                          */
/*                                                                           */
/* Description        : This routine will change the CIST information so that*/
/*                      it will be discarded while processing in the 2dary   */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Identifier on which SISP      */
/*                                  should be enabled.                       */
/*                      pRcvdBpdu - Received BPDU.     .                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MstSispMakeCistInfoInferior (tMstBpdu * pRcvdBpdu)
{
    pRcvdBpdu->CistRootId.u2BrgPriority = 0xFFFF;
    pRcvdBpdu->CistRegionalRootId.u2BrgPriority = 0xFFFF;
    pRcvdBpdu->u2CistPortId = 0xFFFF;

    pRcvdBpdu->u1CistFlags = pRcvdBpdu->u1CistFlags & MST_RESET_FORWARD_FLAG;

    pRcvdBpdu->u1CistFlags = pRcvdBpdu->u1CistFlags & MST_RESET_LEARN_FLAG;
}

/*****************************************************************************/
/* Function Name      : MstSispIsVlansPresentInInst                          */
/*                                                                           */
/* Description        : This function will validate the port to instance     */
/*                      association against the SISP restriction that the    */
/*                      same port cannot be member of same instance in two   */
/*                      different contexts and to be a member of this inst,  */
/*                      this port should be member of atleast one of the     */
/*                      vlans mapped to this instance.                       */
/*                                                                           */
/* Input(s)           : u2PortNum    - Port Number.                          */
/*                      u2MstInst    - Instance identifier.                  */
/*                      pVlanIdList  - Vlan List.If this is NULL, then this  */
/*                                     routine will get the list of vlans    */
/*                                     to which this port is member of.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_FALSE - If creation is invalid &                 */
/*                      MST_TRUE  - If Creation is valid                     */
/*****************************************************************************/
BOOL1
MstSispIsVlansPresentInInst (UINT2 u2PortNum, UINT2 u2MstInst,
                             tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                             tAstMstVlanList NewVlanList)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tMstVlanList       *pVlanListInInst = NULL;
    tMstVlanList       *pVlanListInPort = NULL;
    UINT1               u1Result = MST_FALSE;

    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pVlanListInInst) == NULL)
    {
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FALSE;
    }
    if (AST_ALLOC_MST_VLANLIST_MEM_BLOCK (pVlanListInPort) == NULL)
    {
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return MST_FALSE;
    }

    /* This validation is done in two parts
     * 1) Instance membership Validation
     * 2) Vlan membership validations &
     * */

    /* ***************** INST VALIDATION *******************************
     * Following is one of the restrictions that have been imposed on config
     * for realising SISP functionality in spanning tree protocol operation.
     * "Same interface cannot be mapped to same instance in two different
     *  virtual contexts." 
     * The reason being,
     * The Instance-Vlan mapping along with the VLAN port membership holds the 
     * key in deciding the virtual contexts that will process the received MSTI
     * information. Hence, the vlan to instance mapping should be unique across
     * the contexts. But if this is unique, then there may be problem when the 
     * peer is SISP unaware bridge. In such a case, the restriction that the 
     * vlan forwarding path should be in the same region will break. Hence, the 
     * above restriction is derived.
     * */

    pPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (pPortEntry == NULL)
    {
        /* Port entry cannot be null. Return failure */
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: Invalid Port %s \n",
                      AST_GET_IFINDEX_STR (u2PortNum));
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
        return MST_FALSE;
    }
    if ((MST_IS_INSTANCE_VALID ((INT4) u2MstInst) == MST_FALSE) ||
       ((MST_IS_INSTANCE_VALID ((INT4) u2MstInst) == MST_TRUE) &&
         u2MstInst == AST_TE_MSTID))
    {
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: SISP enabled Port %s is being mapped "
                      "to invaild inst %d\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
        return MST_FALSE;
    }

    if (MstSispIsPortPresentInInst (u2PortNum, u2MstInst) == MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: SISP enabled Port %s is already member of"
                      "Inst %d: in another context!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
        AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
        return MST_FALSE;
    }

    AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                  "VLAN-Port: SISP enabled Port %s is not part of Inst %d:"
                  "in any other context!\n",
                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

    /* ***************** VLAN VALIDATION *******************************
     * This port should not be made member of instances that are mapped 
     * to vlans for which this port is not member of.
     * */

    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                  "SYS: Validation for Instance %u membership "
                  "for port %s ... \n",
                  u2MstInst, AST_GET_IFINDEX_STR (u2PortNum));

    AST_MEMSET (pVlanListInInst->au1List, AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    AstL2IwfMiGetVlanListInInstance (AST_CURR_CONTEXT_ID (), u2MstInst,
                                     pVlanListInInst->au1List);
    /* Add Vlans into the list */
    if (NewVlanList != NULL)
    {
        OSIX_ADD_PORT_LIST (pVlanListInInst->au1List, NewVlanList,
                            sizeof (tVlanList), sizeof (tAstMstVlanList));
    }

    if (pVlanIdList == NULL)
    {
        /* Vlans for the port not provided by the calling function
         * Determine the list of vlans for which this port is 
         * member of
         * */
        AST_MEMSET (pVlanListInPort->au1List, AST_INIT_VAL, MST_VLAN_LIST_SIZE);
        OctetStr.i4_Length = AST_INIT_VAL;

        /* The port should be present in this instance, if and only if this port
         * is a member of any one of the vlans that are mapped to this instance
         * */

        OctetStr.pu1_OctetList = pVlanListInPort->au1List;
        OctetStr.i4_Length = MST_VLAN_LIST_SIZE;

        /* Get the list of VLANs for this port using VLAN API */
        if (AstL2IwfGetMemberVlanList (AST_CURR_CONTEXT_ID (),
                                       AST_GET_IFINDEX (u2PortNum),
                                       &OctetStr) != L2IWF_SUCCESS)
        {
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
            AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
            return MST_FALSE;
        }

        pVlanIdList = &OctetStr;
    }

    AST_IS_VLANS_MATCH (pVlanIdList->pu1_OctetList, pVlanListInInst->au1List,
                        u1Result);

    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInPort);
    AST_RELEASE_MST_VLANLIST_MEM_BLOCK (pVlanListInInst);
    return (u1Result);
}

/*****************************************************************************/
/* Function Name      : MstSispCopyPortPropToSispLogical                     */
/*                                                                           */
/* Description        : This function will copy the identified property for  */
/*                      the set of logical interfaces running over this      */
/*                      physical interface. The properties that are inherited*/
/*                      are AutoEdge, AdminEdge and AdminP2P status.         */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Port Number.                          */
/*                      u1Property   - Property to be copied.                */
/*                      u1Value      - New Value configured in physical port.*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/MST_FAILURE                              */
/*****************************************************************************/
INT4
MstSispCopyPortPropToSispLogical (UINT4 u4IfIndex, UINT1 u1Property,
                                  tAstBoolean bStatus)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4PortCnt = AST_INIT_VAL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT4               u4TempPortCnt = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];

    AST_MEMSET (au2SispPorts, AST_INIT_VAL, sizeof (au2SispPorts));

    /* This fn gets triggerred whenever set of params (AdminEdge, AutoEdge &
     * P2P Staus) are changed for primary. Hence no need 2 check for Module
     * status of primary
     * */
    if (MstVcmSispGetSispPortsInfoOfPhysicalPort (u4IfIndex, VCM_TRUE,
                                                  (VOID *) au2SispPorts,
                                                  &u4PortCnt) != VCM_SUCCESS)
    {
        AST_DBG (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC | AST_INIT_SHUT_DBG
                 | AST_BPDU_DBG,
                 "SISP-BPDU:GetSispPortsInfoOfPhysicalPort returned Failure\n");
        return MST_SUCCESS;
    }

    if (u4PortCnt == AST_INIT_VAL)
    {
        return MST_SUCCESS;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    for (u4ContextId = 0; u4TempPortCnt < u4PortCnt; u4ContextId++)
    {
        u2LocalPort = au2SispPorts[u4ContextId];

        if (u2LocalPort == AST_INIT_VAL)
        {
            continue;
        }

        u4TempPortCnt++;

        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            continue;
        }

        if ((pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort)) == NULL)
        {
            continue;
        }

        switch (u1Property)
        {
            case MST_AUTO_EDGE:

                AstSetAutoEdgePort (u2LocalPort, bStatus);
                break;

            case MST_ADMIN_EDGE:

                AstSetAdminEdgePort (u2LocalPort, bStatus);
                break;

            case MST_ADMIN_P2P:
                MstSetAdminPointToPoint (u2LocalPort, (UINT1) bStatus);
                break;

            default:
                /* Cannot come here */
                AstSelectContext (u4CurrContextId);
                return MST_FAILURE;
        }

        AstReleaseContext ();
    }

    AstSelectContext (u4CurrContextId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispIsPortPresentInInst                           */
/*                                                                           */
/* Description        : This function will check for the given SISP port or  */
/*                      SISP enabled port, does any of the associated port   */
/*                      is already member of the given instance in some other*/
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : u2PortNum - Local Port Number.                       */
/*                      u2MstInst - Instance to which this vlan is to be     */
/*                                  mapped.                                  */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE.                           */
/*****************************************************************************/
INT4
MstSispIsPortPresentInInst (UINT2 u2PortNum, UINT2 u2MstInst)
{
    tAstPortEntry      *pPhyPortEntry = NULL;
    UINT4               u4PhyPort = AST_INVALID_PORT_NUM;
    UINT4               u4ContextId = AST_INVALID_CONTEXT;
    UINT4               u4CurrContextId = AST_INVALID_CONTEXT;
    UINT2               u2LocalPort = AST_INVALID_PORT_NUM;
    BOOL1               bResult = MST_FALSE;

    /* Assumptions:
     * -----------
     * 1) Instance Validation should be done already
     * 2) Port Validation should be done already
     * 3) Lock should be taken
     * */

    u4PhyPort = AST_GET_PHY_PORT (u2PortNum);

    if (u4PhyPort == AST_INIT_VAL)
    {
        /* logical do not run over any physical */
        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: SISP enabled Port %s is not mapped to "
                      "physical port\n", AST_GET_IFINDEX_STR (u2PortNum));

        return MST_FAILURE;
    }

    /* We maintain an instance bit map per port. The physical port will have
     * the bit map corresponding to all the logical ports running over it. 
     * Hence, check this instance is already set in the bit map
     * */
    if (AstGetContextInfoFromIfIndex (u4PhyPort, &u4ContextId, &u2LocalPort)
        != RST_SUCCESS)
    {
        return MST_FAILURE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    /* Switch to primary context id
     * */

    AstSelectContext (u4ContextId);

    if ((pPhyPortEntry = AST_GET_PORTENTRY (u2LocalPort)) == NULL)
    {
        AstSelectContext (u4CurrContextId);
        return MST_FAILURE;
    }

    AST_IS_INST_SET_IN_LIST (pPhyPortEntry->InstList, u2MstInst,
                             MST_INST_LIST_SIZE, bResult);

    /* Restore the context */
    AstSelectContext (u4CurrContextId);

    if (bResult == MST_TRUE)
    {
        /* port is member of the instance in some other contexts, hence
         * return MST_FALSE
         * */
        AST_TRC_ARG2 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                      "VLAN-Port: SISP enabled Port %s is already member of"
                      "Inst %d: in another context!\n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

        return MST_SUCCESS;
    }

    return MST_FAILURE;
}
#endif /* _ASTMSISP_C_ */
