/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msminpwr.c,v 1.9 2015/07/27 07:01:10 siva Exp $ fsbrgnp.c
 *
 * Description: Stubs for network processor functions given here
 *
 *******************************************************************/

#include "astminc.h"

/*******************************************************************************
 * FsMiMstpNpCreateInstance
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * u2InstId - Instance id of the Spanning tree to be created.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during creating
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpCreateInstance (u2InstId);
}

/*******************************************************************************
 * FsMiMstpNpAddVlanInstMapping
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * u2VlanId - VLAN Id that has to be mapped.
 * u2InstId - Spanning tree instance Id.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during mapping
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpAddVlanInstMapping (VlanId, u2InstId);
}

/*******************************************************************************
 * FsMiMstpNpAddVlanListInstMapping
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID. 
 * pu1VlanList - List of VLANs to be mapped to the instance
 * u2InstId    - Spanning tree instance Id.
 * u2NumVlans  - Number of VLANs to be mapped.
 * pu2LastVlan - VLAN ID of last vlan mapped successfully
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during mapping
 *
 * COMMENTS:
 *  None
 *
*******************************************************************************/

PUBLIC INT1
FsMiMstpNpAddVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList,
                                  UINT2 u2InstId, UINT2 u2NumVlans,
                                  UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpAddVlanListInstMapping (pu1VlanList, u2InstId,
                                           u2NumVlans, pu2LastVlan);
}

/*******************************************************************************
 * FsMiMstpNpDeleteInstance
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * u2InstId - Instance id of the Spanning tree to be deleted
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during deleting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpNpDeleteInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpDeleteInstance (u2InstId);
}

/*******************************************************************************
 * FsMiMstpNpDelVlanInstanceMapping
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * u2VlanId - VLAN Id that has to be unmapped.
 * u2InstId - Spanning tree instance Id.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during deleting the mapping
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpNpDelVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpDelVlanInstMapping (VlanId, u2InstId);
}

/*******************************************************************************
 * FsMiMstpNpDelVlanListInstanceMapping
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * pu1VlanList - List of VLANs to be unmapped.
 * u2InstId    - Spanning tree instance Id.
 * u2NumVlans  - Number of VLANs to be unmapped.
 * pu2LastVlan - VLAN ID of last vlan unmapped successfully
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during deleting the mapping
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpNpDelVlanListInstMapping (UINT4 u4ContextId,
                                  UINT1 *pu1VlanList,
                                  UINT2 u2InstId,
                                  UINT2 u2NumVlans, UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstpNpDelVlanListInstMapping (pu1VlanList, u2InstId,
                                           u2NumVlans, pu2LastVlan);
}

/*******************************************************************************
 * FsMiMstpNpWrSetInstancePortState
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * u4IfIndex   - Interface index of whose STP state is to be updated
 * u2InstanceId- Instance ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * RST_SUCCESS - success
 * RST_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT4
FsMiMstpNpWrSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2InstanceId, UINT1 u1PortState)
{
    INT4                i4RetVal = FNP_FAILURE;
    UINT2               u2PortNum = AST_INIT_VAL;
    UINT4               u4CtxtId = AST_INIT_VAL;
    UINT1               u1ProtocolId = 0;
    /* SISP feature enables a physical port to be mapped to more than one
     * context. These ports will not be present in the same MSTI instance 
     * across  two different contexts. If the underlying hardware supports 
     * context based vlan or instance port state, then there will be no problem
     * But, if the hardware does not support so, then the following should be 
     * ensured.
     * 1) The port state should be programmed only for the vlans that are 
     *    mapped to this context, for which this port is member of in this 
     *    context. [H/W has vlan based port state programming]
     * 2) In doing so, if u4ContextId represents primary context identifier,
     *    then this port state should be programmed, if the PVID for this port
     *    is mapped to this instance. [For the vlan represented by PVID only]/
     * 3) For all other vlans this should not be programmed.   
     *    */

    UNUSED_PARAM (u4ContextId);
    if (L2IwfGetPortStateCtrlOwner (u4IfIndex, &u1ProtocolId,OSIX_TRUE) != L2IWF_SUCCESS)
    {
        return i4RetVal;
    }

    if (u1ProtocolId != STP_MODULE)
    {
        return FNP_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4CtxtId, &u2PortNum) ==
        MST_SUCCESS)
    {
        if (AST_IS_CUSTOMER_EDGE_PORT (u2PortNum) == MST_TRUE)
        {
            AST_DBG (AST_STSM_DBG,
                     "CEP Port state cannot be configured in case of MSTP SVLAN context!!!\n");
            return FNP_SUCCESS;
        }
    }

    i4RetVal = FsMstpNpSetInstancePortState
        (u4IfIndex, u2InstanceId, u1PortState);

    return i4RetVal;
}

/*****************************************************************************
 * FsMiMstpNpInitHw 
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

VOID
FsMiMstpNpInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    FsMstpNpInitHw ();
    return;
}

/*****************************************************************************
 * FsMiMstpNpDeInitHw 
 *
 * DESCRIPTION:
 * Stub provided for backward compatibility with MI-unaware hardware.
 *
 * INPUTS:
 * u4ContextId - Virtual Switch ID 
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

VOID
FsMiMstpNpDeInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    FsMstpNpDeInitHw ();
    return;                        /* stub */
}

/*****************************************************************************/
/* Function Name      : FsMiMstNpGetPortState                                */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      MI-unaware hardware.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u4IfIndex - Port Number.                             */
/*                      pu1Status  - Status returned from Hardware.          */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsMiMstNpGetPortState (UINT4 u4ContextId, UINT2 u2InstanceId, UINT4 u4IfIndex,
                       UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    return FsMstNpGetPortState (u2InstanceId, u4IfIndex, pu1Status);
}

#ifdef MBSM_WANTED

/*******************************************************************************
 * FsMiMstpMbsmNpCreateInstance
 *
 * DESCRIPTION:
 * Creates a Spanning Tree instance in the Hardware.
 *
 * INPUTS:
 * u2InstId - Instance id of the Spanning tree to be created.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during creating
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpMbsmNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId,
                              tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4ContextId);
    return FsMstpMbsmNpCreateInstance (u2InstId, pSlotInfo);
}

/*******************************************************************************
 * FsMiMstpMbsmNpAddVlanInstMapping
 *
 * DESCRIPTION:
 * Map a VLAN to the Spanning Tree Instance in the hardware.
 *
 * INPUTS:
 * VlanId - VLAN Id that has to be mapped.
 * u2InstId - Spanning tree instance Id.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during mapping
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsMiMstpMbsmNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4ContextId);
    return FsMstpMbsmNpAddVlanInstMapping (VlanId, u2InstId, pSlotInfo);

}

/* Program port state for instance */

/*******************************************************************************
 * FsMiMstpMbsmNpSetInstancePortState
 *
 * DESCRIPTION:
 * Sets the MSTP Port State in the Hardware for given INSTANCE. When MSTP is
 * operating in MSTP mode or MSTP in RSTP compatible mode or MSTP in STP
 * compatible mode.
 *
 * INPUTS:
 * u4IfIndex   - Interface index of whose STP state is to be updated
 * u2InstanceId- Instance ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT1
FsMiMstpMbsmNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2InstanceId, UINT1 u1PortState,
                                    tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4ContextId);
    return FsMstpMbsmNpSetInstancePortState (u4IfIndex, u2InstanceId,
                                             u1PortState, pSlotInfo);

}

/*****************************************************************************
 * Function Name                  :FsMiMstpMbsmNpInitHw                      *
 *                                                                           *
 * DESCRIPTION                    :This function performs any necessary      * 
 *                                 MSTP related initialisation in the        *
 *                                 Hardware                                  *
 *                                                                           *
 * INPUTS                         : pSlotInfo - Slot Information             *
 *                                : u4ContextId - Context ID                 *   *                                                                           *                                                                               *
 *                                                                           * 
 * OUTPUTS                        : None                                     *
 *                                                                           *
 * RETURNS                        : FNP_SUCCESS - On Success                 *
 *                                  FNP_FAILURE - On Failure                 *
 *                                                                           *
 *****************************************************************************/

INT1
FsMiMstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsMstpMbsmNpInitHw (pSlotInfo));
}
#endif
