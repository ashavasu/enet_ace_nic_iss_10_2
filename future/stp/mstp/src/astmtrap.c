/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: astmtrap.c,v 1.35 2017/11/21 13:06:51 siva Exp $
 *
 * Description:This file contains routines for handling       
 *             different traps                            
 *
 *******************************************************************/

#include "astminc.h"
#include "snmctdfs.h"
#include "snmcport.h"
#ifdef SNMP_3_WANTED
#include "fsmst.h"
#endif

#ifdef SNMP_3_WANTED
static INT1         ai1TempBuffer[256 + 1];
#endif
/*****************************************************************************/
/* Function Name      : AstMstInstUpTrap                                     */
/*                                                                           */
/* Description        : This routine generates trap message when an MSTP     */
/*                      instance is enabled                                  */
/*                                                                           */
/* Input(s)           : u2InstId - Instance Identifier                       */
/*                      pi1TrapsOid - OID of associated Trap Object.         */
/*                      u1TrapOidLen - Length of the Trap OID.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstMstInstUpTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgGenTraps     astBrgGenTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType = AST_UP_TRAP;
    if (IS_AST_BRG_GEN_EVNT ())
    {
        AST_MEMCPY (&(astBrgGenTraps.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

        astBrgGenTraps.u1GenTrapType = AST_INST_UP_TRAP;
        astBrgGenTraps.u2TrapMsgVal = u2InstId;

        nmhGetFsMstInstanceUpCount ((INT4) u2InstId,
                                    &(astBrgGenTraps.u4InstUpCount));
        nmhGetFsMstInstanceDownCount ((INT4) u2InstId,
                                      &(astBrgGenTraps.u4InstDownCount));

        AstSnmpIfSendTrap (AST_BRG_GEN_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &astBrgGenTraps);
    }
}

/*****************************************************************************/
/* Function Name      : AstMstInstDownTrap                                   */
/*                                                                           */
/* Description        : This routine generates trap message when an MSTP     */
/*                      instance is disabled                                 */
/*                                                                           */
/* Input(s)           : u2InstId - Instance Identifier                       */
/*                      pi1TrapsOid - OID of associated Trap Object.         */
/*                      u1TrapOidLen - Length of the Trap OID.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstMstInstDownTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstBrgGenTraps     astBrgGenTraps;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType = AST_DOWN_TRAP;
    if (IS_AST_BRG_GEN_EVNT ())
    {
        AST_MEMCPY (&(astBrgGenTraps.BrgAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        astBrgGenTraps.u1GenTrapType = AST_INST_DOWN_TRAP;
        astBrgGenTraps.u2TrapMsgVal = u2InstId;

        nmhGetFsMstInstanceUpCount ((INT4) u2InstId,
                                    &(astBrgGenTraps.u4InstUpCount));
        nmhGetFsMstInstanceDownCount ((INT4) u2InstId,
                                      &(astBrgGenTraps.u4InstDownCount));

        AstSnmpIfSendTrap (AST_BRG_GEN_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &astBrgGenTraps);
    }
}

/*****************************************************************************/
/* Function Name      : AstMstRegionConfigChangeTrap                         */
/*                                                                           */
/* Description        : This routine generates trap message when the MST     */
/*                      Region's configuration identifier changes            */
/*                                                                           */
/* Input(s)           : pMstConfigIdInfo - MST Configuration identifier.     */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AstMstRegionConfigChangeTrap (tMstConfigIdInfo * pMstConfigIdInfo,
                              INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tAstMstRegionConfigChangeTrap astRegionConfigChangeTrap;
    tAstBridgeEntry    *pBrgInfo = NULL;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (IS_AST_BRG_GEN_EVNT ())
    {
        AST_MEMCPY (&(astRegionConfigChangeTrap.BrgAddr),
                    (pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AST_MEMCPY (&(astRegionConfigChangeTrap.au1RegionName),
                    (pMstConfigIdInfo->au1ConfigName), MST_CONFIG_NAME_LEN);
        AST_MEMCPY (&(astRegionConfigChangeTrap.au1ConfigDigest),
                    (pMstConfigIdInfo->au1ConfigDigest), MST_CONFIG_DIGEST_LEN);
        astRegionConfigChangeTrap.u1ConfigId =
            pMstConfigIdInfo->u1ConfigurationId;
        astRegionConfigChangeTrap.u2RevisionLevel =
            pMstConfigIdInfo->u2ConfigRevLevel;
        AstSnmpIfSendTrap (AST_REGION_CONFIG_CHANGE_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &astRegionConfigChangeTrap);
    }
}

/******************************************************************************
* Function : MstSnmpIfSendTrap 
* Input    : u1TrapId - Trap Identifier
*           pi1TrapOid -pointer to Trap OID
*           u1OidLen - OID Length 
*           pTrapInfo - void pointer to the trap information 
* Output   : None
* Returns  : None
*******************************************************************************/
VOID
MstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tAstBrgGenTraps    *pAstBrgGenTraps;
    tAstBrgErrTraps    *pAstBrgErrTraps;
    tAstBrgNewRootTrap *pAstBrgNewRootTrap;
    tAstBrgTopologyChgTrap *pAstBrgTopologyChgTrap;
    tAstBrgProtocolMigrationTrap *pAstBrgProtocolMigrationTrap;
    tAstBrgInvalidBpduRxdTrap *pAstBrgInvalidBpduRxdTrap;
    tAstNewPortRoleTrap *pAstNewPortRoleTrap = NULL;
    tAstMstRegionConfigChangeTrap *pAstMstRegionConfigChangeTrap;
    tAstBrgHwFailTrap  *pAstBrgHwFailTrap = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring, *pContextName = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[256];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4TopChanges = AST_INIT_VAL;
    UINT4               u4SysMode;
    tSNMP_COUNTER64_TYPE snmpCounter64Type;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tAstBrgLoopIncStateChange *pAstBrgLoopIncStateChange = NULL;
    tAstBrgBPDUIncStateChange *pAstBrgBPDUIncStateChange = NULL;
    tAstBrgRootIncStateChange *pAstBrgRootIncStateChange = NULL;
    tAstBrgPortStateChange *pAstBrgPortStateChange = NULL;

    UNUSED_PARAM (u1OidLen);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    snmpCounter64Type.msn = 0;
    snmpCounter64Type.lsn = 0;

    /* In MI except trap AST_BRG_ERR_TRAP_VAL the context-name is getting 
     * filled, since it is global for the entire module */

    switch (u1TrapId)
    {
        case AST_BRG_GEN_TRAP_VAL:

            pAstBrgGenTraps = (tAstBrgGenTraps *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_BRG_GEN_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);

            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgGenTraps->BrgAddr,
                                          (INT4) AST_MAC_ADDR_SIZE);

            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_GEN_TRAP_TYPE);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);

            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u1GenTrapType,
                                      NULL, NULL, snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_INSTUP_COUNT);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgGenTraps->u2TrapMsgVal;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u4InstUpCount,
                                      NULL, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_INSTDOWN_COUNT);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgGenTraps->u2TrapMsgVal;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgGenTraps->u4InstDownCount,
                                      NULL, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_BRG_ERR_TRAP_VAL:

            pAstBrgErrTraps = (tAstBrgErrTraps *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_BRG_ERR_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pAstBrgErrTraps->BrgAddr,
                                          AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_ERR_TRAP_TYPE);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgErrTraps->
                                      u1ErrTrapType, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_ROOT_TRAP_VAL:

            pAstBrgNewRootTrap = (tAstBrgNewRootTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_NEW_ROOT_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgNewRootTrap->BrgAddr,
                                                 AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_OLD_DESIG_ROOT);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Mst Instance Index
                 * */

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgNewRootTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstOldRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_REG_ROOT);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgNewRootTrap->u2MstInst;
                pOid->u4_Length++;
            }
            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgNewRootTrap->AstRoot,
                                          sizeof (tAstBridgeId));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                      0, pOstring, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_NEW_PORTROLE_TRAP_VAL:

            pAstNewPortRoleTrap = (tAstNewPortRoleTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_NEW_PORTROLE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstNewPortRoleTrap->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_PREVIOUS_ROLE);

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> MstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1PreviousRole, NULL, NULL,
                                      snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_SELECTED_ROLE);

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> MstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2PortNum;
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstNewPortRoleTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstNewPortRoleTrap->
                                      u1SelectedRole, NULL, NULL,
                                      snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_TOPOLOGY_CHG_TRAP_VAL:

            pAstBrgTopologyChgTrap = (tAstBrgTopologyChgTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_TOPOLOGY_CHG_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgTopologyChgTrap->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            nmhGetFsMstMstiTopChanges ((INT4) pAstBrgTopologyChgTrap->
                                       u2MstInst, &u4TopChanges);
            pAstBrgTopologyChgTrap->u4TopChanges = u4TopChanges;

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_TOPOLOGY_CHGS);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length] =
                    pAstBrgTopologyChgTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgTopologyChgTrap->
                                      u4TopChanges, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_PROTOCOL_MIGRATION_TRAP_VAL:

            pAstBrgProtocolMigrationTrap =
                (tAstBrgProtocolMigrationTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_PROTOCOL_MIGRATION_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgProtocolMigrationTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_FORCE_PROT_VERSION);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgProtocolMigrationTrap->
                                      u1Version, NULL, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_PORT_MIGRATION_TYPE);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the index for this object Port Number
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgProtocolMigrationTrap->
                                             u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgProtocolMigrationTrap->
                                      u1GenTrapType, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_INVALID_BPDU_RXD_TRAP_VAL:

            pAstBrgInvalidBpduRxdTrap = (tAstBrgInvalidBpduRxdTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_INVALID_BPDU_RXD_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgInvalidBpduRxdTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_PKT_ERR_TYPE);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the index for this object Port Number
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u1ErrTrapType, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_PKT_ERR_VAL);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the index for this object Port Number
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgInvalidBpduRxdTrap->u2Port);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgInvalidBpduRxdTrap->
                                      u2ErrVal, NULL, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_REGION_CONFIG_CHANGE_TRAP_VAL:

            pAstMstRegionConfigChangeTrap =
                (tAstMstRegionConfigChangeTrap *) pTrapInfo;
            u4SpecTrapType = (UINT4) MST_REGION_CONFIG_CHANGE_TRAP_VAL;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstMstRegionConfigChangeTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_CONFIG_ID_SEL);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstMstRegionConfigChangeTrap->
                                      u1ConfigId, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_REGION_NAME);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstMstRegionConfigChangeTrap->
                                          au1RegionName, MST_CONFIG_NAME_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_REGION_VERSION);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstMstRegionConfigChangeTrap->
                                      u2RevisionLevel, NULL, NULL,
                                      snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_CONFIG_DIGEST);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstMstRegionConfigChangeTrap->
                                          au1ConfigDigest,
                                          MST_CONFIG_DIGEST_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case AST_HW_FAIL_TRAP_VAL:

            pAstBrgHwFailTrap = (tAstBrgHwFailTrap *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pAstBrgHwFailTrap->
                                          BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            AstTrapFillContextInfo (&pVbList, snmpCounter64Type, AST_FALSE);

            /* TODO - Change u4SpecTrapType based on whether CIST or MSTI */
            if (pAstBrgHwFailTrap->u2MstInst == MST_CIST_CONTEXT)
            {
                u4SpecTrapType = (UINT4) MST_CIST_HW_FAIL_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_PORT_STATE);
            }
            else
            {
                u4SpecTrapType = (UINT4) MST_MSTI_HW_FAIL_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_PORT_STATE);
            }

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * Index2 --> MstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Mst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) AST_GET_IFINDEX (pAstBrgHwFailTrap->u2Port);
                pOid->u4_Length++;

                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgHwFailTrap->u2MstInst;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pAstBrgHwFailTrap->
                                      u2PortState, NULL, NULL,
                                      snmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;
        case AST_LOOP_INCSTATE_CHANGE_TRAP_VAL:
            pAstBrgLoopIncStateChange = (tAstBrgLoopIncStateChange *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgLoopIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            /* TODO - Change u4SpecTrapType based on whether CIST or MSTI */
            if (pAstBrgLoopIncStateChange->u2InstId == MST_CIST_CONTEXT)
            {
                u4SpecTrapType = (UINT4) MST_CIST_LOOP_INCSTATE_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_LOOP_INCSTATE);
            }
            else
            {
                u4SpecTrapType = (UINT4) MST_MSTI_LOOP_INCSTATE_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_LOOP_INCSTATE);
            }

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgLoopIncStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) (pAstBrgLoopIncStateChange->u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgLoopIncStateChange->
                                      u4LoopInconsistentState, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_BPDU_INCSTATE_CHANGE_TRAP_VAL:

            pAstBrgBPDUIncStateChange = (tAstBrgBPDUIncStateChange *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgBPDUIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            u4SpecTrapType = (UINT4) MST_CIST_BPDU_INCSTATE_TRAP;
            SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_BPDU_INCSTATE);

            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgBPDUIncStateChange->u2Port;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgBPDUIncStateChange->
                                      u4BPDUInconsistentState, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_ROOT_INCSTATE_CHANGE_TRAP_VAL:
            pAstBrgRootIncStateChange = (tAstBrgRootIncStateChange *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgRootIncStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            /* TODO - Change u4SpecTrapType based on whether CIST or MSTI */
            if (pAstBrgRootIncStateChange->u2InstId == MST_CIST_CONTEXT)
            {
                u4SpecTrapType = (UINT4) MST_CIST_ROOT_INCSTATE_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_ROOT_INCSTATE);
            }
            else
            {
                u4SpecTrapType = (UINT4) MST_MSTI_ROOT_INCSTATE_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_ROOT_INCSTATE);
            }
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgRootIncStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) (pAstBrgRootIncStateChange->u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgRootIncStateChange->
                                      u4RootInconsistentState, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;
        case AST_PORTSTATE_CHANGE_TRAP_VAL:
            pAstBrgPortStateChange = (tAstBrgPortStateChange *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "%s", MST_MIB_OBJ_BASE_BRIDGE_ADDR);
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString ((UINT1 *)
                                                 &pAstBrgPortStateChange->
                                                 BrgAddr, AST_MAC_ADDR_SIZE);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, snmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            /* TODO - Change u4SpecTrapType based on whether CIST or MSTI */
            if (pAstBrgPortStateChange->u2InstId == MST_CIST_CONTEXT)
            {
                u4SpecTrapType = (UINT4) MST_CIST_STATE_CHG_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_OLDPORT_STATE);
            }
            else
            {
                u4SpecTrapType = (UINT4) MST_MSTI_STATE_CHG_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_OLDPORT_STATE);
            }
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) (pAstBrgPortStateChange->u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u4OldPortState, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            /* TODO - Change u4SpecTrapType based on whether CIST or MSTI */
            if (pAstBrgPortStateChange->u2InstId == MST_CIST_CONTEXT)
            {
                u4SpecTrapType = (UINT4) MST_CIST_STATE_CHG_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_CIST_PORT_STATE);
            }
            else
            {
                u4SpecTrapType = (UINT4) MST_MSTI_STATE_CHG_TRAP;
                SPRINTF ((CHR1 *) au1Buf, "%s", MST_MIB_OBJ_MSTI_PORT_STATE);
            }
            pOid = MstMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No
             * Index2 --> PvrstInstance
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * 2. Append the Pvrst Instance Index
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pAstBrgPortStateChange->u2Port;
                pOid->u4_Length++;
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) (pAstBrgPortStateChange->u2InstId);
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pAstBrgPortStateChange->
                                      u2PortState, NULL, NULL,
                                      snmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            break;

        default:
            SNMP_FreeOid (pEnterpriseOid);
            return;

    }

    u4SysMode = AstVcmGetSystemModeExt (AST_PROTOCOL_ID);
    if (u4SysMode == VCM_MI_MODE)
    {
        /* In case of SI, the context name pContextName should be NULL. */
        AstVcmGetAliasName (AST_CURR_CONTEXT_ID (), au1ContextName);
        pContextName =
            SNMP_AGT_FormOctetString (au1ContextName, (VCM_ALIAS_MAX_LEN - 1));
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4SpecTrapType, pStartVb, pContextName);

    if (u4SysMode == VCM_MI_MODE)
    {
        SNMP_AGT_FreeOctetString (pContextName);
    }
#else /* SNMP_2_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_2_WANTED */
}

/******************************************************************************
* Function : MstMakeObjIdFromDotNew                                         *
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
MstMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
#ifdef SNMP_3_WANTED
    INT1               *pi1TmpPtr;
    INT1               *pi1DotPtr;
    UINT2               u2Count;
    UINT2               u2DotCount;
    UINT4               u4OidLen = 0;
    if (AstVcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE)
    {
        porig_mib_oid_table = orig_mib_oid_table_SI;
        u4OidLen = sizeof (orig_mib_oid_table_SI);
    }
    else
    {
        porig_mib_oid_table = orig_mib_oid_table_MI;
        u4OidLen = sizeof (orig_mib_oid_table_MI);
    }

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TmpPtr = pi1TextStr;

        for (u2Count = 0; ((pi1TmpPtr < pi1DotPtr) && (u2Count < 256));
             u2Count++)
        {
            ai1TempBuffer[u2Count] = *pi1TmpPtr++;
        }
        ai1TempBuffer[u2Count] = '\0';

        for (u2Count = 0; ((u2Count <
                            (u4OidLen /
                             sizeof (struct MIB_OID)))
                           && ((porig_mib_oid_table + u2Count)->pName != NULL));
             u2Count++)
        {
            if ((STRCMP
                 ((porig_mib_oid_table + u2Count)->pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN ((porig_mib_oid_table + u2Count)->pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         (porig_mib_oid_table + u2Count)->pNumber, 257);
                ai1TempBuffer[STRLEN ((porig_mib_oid_table + u2Count)->pNumber)]
                    = '\0';
                break;
            }
        }
        if (u2Count < (u4OidLen / sizeof (struct MIB_OID)))
        {
            if ((porig_mib_oid_table + u2Count)->pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 257 - STRLEN (ai1TempBuffer));
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 STRLEN ((INT1 *) pi1TextStr));
        ai1TempBuffer[STRLEN ((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Count = 0;
         (u2Count < 257 && ai1TempBuffer[u2Count] != '\0'); u2Count++)
    {
        if (ai1TempBuffer[u2Count] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }

    MEMSET (pOidPtr->pu4_OidList, 0, SNMP_MAX_OID_LENGTH);
    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TmpPtr = ai1TempBuffer;
    for (u2Count = 0; ((u2Count < u2DotCount + 1) &&
                       (u2Count <= SNMP_MAX_OID_LENGTH)); u2Count++)
    {
        if ((pi1TmpPtr =
             (MstParseSubIdNew (pi1TmpPtr,
                                &pOidPtr->pu4_OidList[u2Count]))) == NULL)
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }

        if (*pi1TmpPtr == '.')
        {
            pi1TmpPtr++;        /* to skip over dot */
        }
        else if (*pi1TmpPtr != '\0')
        {
            SNMP_FreeOid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
#else
    UNUSED_PARAM (pi1TextStr);
#endif
    return (pOidPtr);
}

/******************************************************************************
* Function : MstParseSubIdNew
* Input    : pi1TmpPtr
* Output   : None
* Returns  : i4Value of pi1TmpPtr or -1
*******************************************************************************/

INT1               *
MstParseSubIdNew (INT1 *pi1TmpPtr, UINT4 *pu4Value)
{
    INT1               *pi1Tmp;
    UINT4               u4Value = 0;

    for (pi1Tmp = pi1TmpPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                              ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                              ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F'))); pi1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pi1Tmp & 0xf);
    }

    if (pi1TmpPtr == pi1Tmp)
    {
        pi1Tmp = NULL;
    }
    pi1TmpPtr = pi1Tmp;
    *pu4Value = u4Value;
    return pi1TmpPtr;
}

/*************************** END OF FILE(astmtrap.c) ***********************/
