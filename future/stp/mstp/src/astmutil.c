/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmutil.c,v 1.74 2017/11/21 13:06:51 siva Exp $
 *
 * Description: This file contains utility routines.                
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"
#include "fsmpmscli.h"

/*****************************************************************************/
/* Function Name      : MstUtlChkPriority                                    */
/*                                                                           */
/* Description        : Compares the Bridge priorities based on instance     */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - Instance Specific Port Information  */
/*                      pRcvdBpdu - Received priority information            */
/*                      u2MstInst - Instance identifier , 0 for CIST         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_PRIORITY_SUPERIOR (OR)                           */
/*                      MST_PRIORITY_INFERIOR (OR)                           */
/*                      MST_PRIORITY_SAME                                    */
/*****************************************************************************/
INT4
MstUtlChkPriority (tAstPerStPortInfo * pPerStPortInfo, tMstBpdu * pRcvdBpdu,
                   UINT2 u2MstInst)
{
    tAstBridgeId        MstBrgId;
    INT4                i4RetVal = 0;
    UINT2               u2TmpPortNo = 0;
    UINT2               u2MstDesgPortId = 0;
    tAstMstBridgeEntry *pMstBrgEntry = NULL;

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistRootId),
                                      &(pPerStPortInfo->RootId));
        if (i4RetVal == AST_BRGID1_SUPERIOR)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (i4RetVal == AST_BRGID1_INFERIOR)
        {
            return MST_PRIORITY_INFERIOR;
        }

        if (pRcvdBpdu->u4CistExtPathCost < pPerStPortInfo->u4RootCost)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (pRcvdBpdu->u4CistExtPathCost > pPerStPortInfo->u4RootCost)
        {
            return MST_PRIORITY_INFERIOR;
        }
        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistRegionalRootId),
                                      &(pPerStPortInfo->RegionalRootId));
        if (i4RetVal == AST_BRGID1_SUPERIOR)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (i4RetVal == AST_BRGID1_INFERIOR)
        {
            return MST_PRIORITY_INFERIOR;
        }

        if (pRcvdBpdu->u4CistIntRootPathCost <
            (pPerStPortInfo->u4IntRootPathCost))
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (pRcvdBpdu->u4CistIntRootPathCost >
            (pPerStPortInfo->u4IntRootPathCost))
        {
            return MST_PRIORITY_INFERIOR;
        }
        i4RetVal = AST_COMPARE_BRGID (&(pRcvdBpdu->CistBridgeId),
                                      &(pPerStPortInfo->DesgBrgId));
        if (i4RetVal == AST_BRGID1_SUPERIOR)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (i4RetVal == AST_BRGID1_INFERIOR)
        {
            return MST_PRIORITY_INFERIOR;
        }
        if (pRcvdBpdu->u2CistPortId < pPerStPortInfo->u2DesgPortId)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (pRcvdBpdu->u2CistPortId > pPerStPortInfo->u2DesgPortId)
        {
            return MST_PRIORITY_INFERIOR;
        }
        else
        {
            return MST_PRIORITY_SAME;
        }
    }
    else
    {                            /* MSTI Context */
        i4RetVal =
            AST_COMPARE_BRGID (&(pRcvdBpdu->aMstConfigMsg[u2MstInst].
                                 MstiRegionalRootId),
                               &(pPerStPortInfo->RootId));
        if (i4RetVal == AST_BRGID1_SUPERIOR)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (i4RetVal == AST_BRGID1_INFERIOR)
        {
            return MST_PRIORITY_INFERIOR;
        }
        if (pRcvdBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost <
            (pPerStPortInfo->u4RootCost))
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (pRcvdBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost >
            (pPerStPortInfo->u4RootCost))
        {
            return MST_PRIORITY_INFERIOR;
        }
        AST_MEMSET (&MstBrgId, AST_INIT_VAL, sizeof (MstBrgId));
        AST_MEMCPY (MstBrgId.BridgeAddr, pRcvdBpdu->CistBridgeId.BridgeAddr,
                    AST_MAC_ADDR_SIZE);

        MstBrgId.u2BrgPriority = (UINT2)
            pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiBrgPriority;
        MstBrgId.u2BrgPriority = (UINT2) (MstBrgId.u2BrgPriority << 8);
        pMstBrgEntry = AST_GET_MST_BRGENTRY ();

        if (pMstBrgEntry->bExtendedSysId == MST_TRUE)
        {
            MstBrgId.u2BrgPriority = (UINT2)
                (MstBrgId.u2BrgPriority | u2MstInst);
        }

        i4RetVal = AST_COMPARE_BRGID (&(MstBrgId),
                                      &(pPerStPortInfo->DesgBrgId));
        if (i4RetVal == AST_BRGID1_SUPERIOR)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (i4RetVal == AST_BRGID1_INFERIOR)
        {
            return MST_PRIORITY_INFERIOR;
        }

        u2MstDesgPortId =
            pRcvdBpdu->aMstConfigMsg[u2MstInst].u1MstiPortPriority;
        u2MstDesgPortId =
            (UINT2) (u2MstDesgPortId << AST_PORTPRIORITY_BIT_OFFSET);

        u2TmpPortNo = (UINT2) (pRcvdBpdu->u2CistPortId & 0x0fff);
        u2MstDesgPortId = u2MstDesgPortId | u2TmpPortNo;
        if (u2MstDesgPortId < pPerStPortInfo->u2DesgPortId)
        {
            return MST_PRIORITY_SUPERIOR;
        }
        if (u2MstDesgPortId > pPerStPortInfo->u2DesgPortId)
        {
            return MST_PRIORITY_INFERIOR;
        }
        else
        {
            return MST_PRIORITY_SAME;
        }
    }
}

/*****************************************************************************/
/* Function Name      : MstTmrExpiryHandler                                  */
/*                                                                           */
/* Description        : This function is called whenever any timer expires.  */
/*                      This extracts all the expired timers at any instant  */
/*                      of time and depending on the type of timer, it       */
/*                      performs the ncessary processing.                    */
/*                                                                           */
/* Input(s)           : pAstTimer - Timer node pointer                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gAstGlobalInfo.AstTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/

INT4
MstTmrExpiryHandler (tAstTimer * pAstTimer)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTmpPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstContextInfo    *pAstContextInfo = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    VOID               *pEntryPtr = NULL;

    UINT2               u2InstanceId = AST_INIT_VAL;
    UINT1               u1TimerType = (UINT1) AST_INIT_VAL;
    INT4                i4RetVal = MST_SUCCESS;

    INT1                u1TimerInfo;
    UINT2               u2PortNum = 0;
    UINT2               u1SyncFlag = RST_FALSE;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

#ifdef MRP_WANTED
    tMrpInfo            MrpStpInfo;
    MEMSET (&MrpStpInfo, 0, sizeof (tMrpInfo));
#endif

    u2InstanceId = pAstTimer->u2InstanceId;
    u1TimerType = pAstTimer->u1TimerType;
    pEntryPtr = pAstTimer->pEntry;
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

    if (pEntryPtr != NULL)
    {
        switch (u1TimerType)
        {
            case AST_TMR_TYPE_HELLOWHEN:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pHelloWhenTmr = NULL;
                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: HELLOWHEN Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pAstPortEntry->u2PortNo);

                if (RstPortTransmitMachine
                    ((UINT2) RST_PTXSM_EV_HELLOWHEN_EXP, pAstPortEntry,
                     u2InstanceId) != RST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Tx SEM returned FAILURE!\n");
                    i4RetVal = MST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_FDWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pFdWhileTmr = NULL;
                u2PortNum = pPerStPortInfo->u2PortNo;
                pTmpPortInfo = AST_GET_PORTENTRY (u2PortNum);
                if (pTmpPortInfo == NULL)
                {
                    return MST_FAILURE;
                }
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: FDWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pPerStPortInfo->u2PortNo);
                if (MstPortRoleTransitMachine
                    (MST_PROLETRSM_EV_FDWHILE_EXPIRED, pPerStPortInfo,
                     u2InstanceId) != MST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = MST_FAILURE;
                }
                break;

            case AST_TMR_TYPE_ERROR_DISABLE_RECOVERY:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pDisableRecoveryTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %u: ERROR DISABLE RECOVERY"
                              "Timer EXPIRED for Instance: %u\n",
                              pAstPortEntry->u2PortNo, u2InstanceId);
                /* During ISSU Maintenance Mode 
                 * restart the Error Disable recovery Timer */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                       u1TimerType,
                                       (UINT2) (pAstPortEntry->u4ErrorRecovery
                                                + ISSU_TIMER_VALUE))
                        != RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "MST_TMR: ISSU is in progress,"
                                 "Restarting AstStartTimer returned FAILURE!\n");
                        return MST_FAILURE;
                    }
                    AST_DBG (AST_TMR_DBG,
                             "MST_TMR: ISSU is in progress,"
                             "AstStartTimer Restarted!!!\n");
                    return MST_SUCCESS;
                }
                /* BpduInconsitent state is reset when the port transtions happens */
                if (pAstCommPortInfo->bBpduInconsistent == AST_TRUE)
                {
                    pAstCommPortInfo->bBpduInconsistent = AST_FALSE;
                    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                                      "Port:%s  BPDUInconsistent reset occur at : %s\r\n",
                                      AST_GET_IFINDEX_STR (pAstPortEntry->
                                                           u2PortNo),
                                      au1TimeStr);
                }

                AstEnablePort (u2PortNum, MST_CIST_CONTEXT, AST_EXT_PORT_UP);

                break;

            case AST_TMR_TYPE_MDELAYWHILE:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pMdWhileTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: MDWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pAstPortEntry->u2PortNo);

                if (RstPortMigrationMachine
                    ((UINT2) RST_PMIGSM_EV_MDELAYWHILE_EXP,
                     pAstPortEntry->u2PortNo) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Migration SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_PMSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Migration SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }
                break;

            case AST_TMR_TYPE_RBWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRbWhileTmr = NULL;

                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: RBWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pPerStPortInfo->u2PortNo);
                if (MstPortRoleTransitMachine
                    (MST_PROLETRSM_EV_RBWHILE_EXPIRED, pPerStPortInfo,
                     u2InstanceId) != MST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = MST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_RCVDINFOWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRcvdInfoTmr = NULL;
                pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

                if ((pAstPortEntry->bLoopGuard == MST_TRUE) &&
                    (pPerStPortInfo->bLoopGuardStatus == MST_TRUE)
                    && (pAstPortEntry->bOperPointToPoint == RST_TRUE))
                {
                    if (MstPortInfoMachine
                        (MST_PINFOSM_EV_RCVDINFOWHILE_EXP, pPerStPortInfo, NULL,
                         u2InstanceId) != MST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        i4RetVal = MST_FAILURE;
                    }
                }
                else
                {
#ifndef L2RED_WANTED
                    AST_DBG_ARG2 (AST_TMR_DBG,
                                  "TMR: RCVDINFOWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                                  u2InstanceId, pPerStPortInfo->u2PortNo);
                    if (MstPortInfoMachine (MST_PINFOSM_EV_RCVDINFOWHILE_EXP,
                                            pPerStPortInfo, NULL, u2InstanceId)
                        != MST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Information SEM returned FAILURE!\n");
                        i4RetVal = MST_FAILURE;
                    }
#else
                    pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount++;
                    u2PortNum = pPerStPortInfo->u2PortNo;
                    u1SyncFlag = MST_TRUE;
                    if (pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount >=
                        AST_NUM_TMR_INTERVAL_SPLITS)
                    {
                        AST_DBG_ARG2 (AST_TMR_DBG | AST_RED_DBG,
                                      "TMR: Port %u: RCVDINFOWHILE Timer EXPIRED for Instance: %u\n",
                                      pPerStPortInfo->u2PortNo, u2InstanceId);
                        AST_DBG_ARG2 (AST_TMR_DBG | AST_PISM_DBG,
                                      "TMR: Port %u: RCVDINFOWHILE Timer EXPIRED for Instance: %u\n",
                                      pPerStPortInfo->u2PortNo, u2InstanceId);

                        if (MstPortInfoMachine
                            (MST_PINFOSM_EV_RCVDINFOWHILE_EXP, pPerStPortInfo,
                             NULL, u2InstanceId) != MST_SUCCESS)
                        {
                            AST_DBG (AST_TMR_DBG | AST_PISM_DBG |
                                     AST_ALL_FAILURE_DBG,
                                     "TMR: Port Information SEM returned FAILURE!\n");
                            i4RetVal = MST_FAILURE;
                        }
                        pPerStPortInfo->PerStRstPortInfo.u1RcvdExpTimerCount =
                            0;
                    }
                    else
                    {
                        MstPortInfoSmUpdtRcvdInfoWhile (pPerStPortInfo,
                                                        NULL, u2InstanceId);
                    }
#endif
                }
                break;

            case AST_TMR_TYPE_RRWHILE:
                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRrWhileTmr = NULL;

                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: RRWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pPerStPortInfo->u2PortNo);
                if (MstPortRoleTransitMachine
                    (MST_PROLETRSM_EV_RRWHILE_EXPIRED, pPerStPortInfo,
                     u2InstanceId) != MST_SUCCESS)
                {
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = MST_FAILURE;
                }
                break;

            case AST_TMR_TYPE_TCWHILE:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr = NULL;
                pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
                if (pPerStInfo != NULL)
                {
                    /* Topology change count decremented during timer expiry when tcWhile timer 
                     * is not running for a particular port in the instance. */
                    pPerStInfo->u4TcWhileCount--;
                }
                u2PortNum = pPerStPortInfo->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: TCWHILE Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pPerStPortInfo->u2PortNo);
                break;

            case AST_TMR_TYPE_HOLD:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;

                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pHoldTmr = NULL;
                pAstBridgeEntry = AST_GET_BRGENTRY ();
                if (pAstCommPortInfo->u1TxCount > 0)
                {
                    (pAstCommPortInfo->u1TxCount)--;
                }

                AST_DBG_ARG3 (AST_TMR_DBG,
                              "TMR: HOLD Timer EXPIRED for Instance: %d, Port: %d, TxCount: %u\n",
                              u2InstanceId, pAstPortEntry->u2PortNo,
                              pAstCommPortInfo->u1TxCount);

                if (pAstCommPortInfo->u1TxCount ==
                    (pAstBridgeEntry->u1TxHoldCount - (UINT1) 1))
                {
                    if (RstPortTransmitMachine
                        ((UINT2) RST_PTXSM_EV_HOLDTMR_EXP, pAstPortEntry,
                         u2InstanceId) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_TXSM_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TMR: Port Transmit SEM returned FAILURE!!!\n");
                        i4RetVal = MST_FAILURE;
                    }
                }
                if (pAstCommPortInfo->u1TxCount > (UINT1) AST_INIT_VAL)
                {
                    if (AstStartTimer ((VOID *) pAstPortEntry, u2InstanceId,
                                       (UINT1) AST_TMR_TYPE_HOLD,
                                       (UINT2) AST_HOLD_TIME) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR: AstStartTimer routine returned FAILURE!!!\n");
                        i4RetVal = MST_FAILURE;
                    }
                }
                break;

            case AST_TMR_TYPE_EDGEDELAYWHILE:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pEdgeDelayWhileTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_BDSM_DBG,
                              "TMR: Port %u: EDGEDELAYWHILE Timer EXPIRED for Instance: %u\n",
                              pAstPortEntry->u2PortNo, u2InstanceId);
                RstBrgDetectionMachine ((UINT2)
                                        RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP,
                                        pAstPortEntry->u2PortNo);

                break;

            case AST_TMR_TYPE_RAPIDAGE_DURATION:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pRapidAgeDurtnTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = MST_TRUE;

                AST_DBG_ARG2 (AST_TMR_DBG | AST_TXSM_DBG,
                              "TMR: Port %s: RAPIDAGE DURATION Timer EXPIRED for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pAstPortEntry->u2PortNo),
                              u2InstanceId);

                if (AST_FORCE_VERSION == AST_VERSION_0)
                {
                    AstVlanResetShortAgeoutTime (pAstPortEntry);
                }

                break;

            case AST_TMR_TYPE_PSEUDOINFOHELLOWHEN:

                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                pAstCommPortInfo = &(pAstPortEntry->CommPortInfo);
                pAstCommPortInfo->pPseudoInfoHelloWhenTmr = NULL;
                u2PortNum = pAstPortEntry->u2PortNo;
                u1SyncFlag = MST_TRUE;
                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: PSEUDO HELLOWHEN Timer EXPIRED for Instance: %d, Port: %d\n",
                              u2InstanceId, pAstPortEntry->u2PortNo);

                if (RstPseudoInfoMachine
                    ((UINT2) RST_PSEUDO_INFO_EV_HELLO_EXPIRY,
                     pAstPortEntry->u2PortNo, NULL) != RST_SUCCESS)
                {
                    AST_DBG (AST_PSSM_DBG | AST_PSSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port PSEUDOINFO SEM returned FAILURE!\n");
                    i4RetVal = MST_FAILURE;
                }

                break;

#ifdef MRP_WANTED
            case AST_TMR_TYPE_TCDETECTED:

                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr = NULL;

                u2PortNum = pPerStPortInfo->u2PortNo;

                AST_DBG_ARG2 (AST_TMR_DBG,
                              "TMR: TCDETECTED Timer EXPIRED for Instance:"
                              " %d, Port: %d\n",
                              u2InstanceId, pPerStPortInfo->u2PortNo);

                MrpStpInfo.u1Flag = MSG_TC_DETECTED_TMR_STATUS;
                MrpStpInfo.u4IfIndex =
                    AST_IFENTRY_IFINDEX (AST_GET_PORTENTRY (u2PortNum));
                MrpStpInfo.u2MapId = u2InstanceId;
                MrpStpInfo.b1TruthVal = OSIX_FALSE;

                AstPortMrpApiNotifyStpInfo (&MrpStpInfo);
#endif
                break;

            case AST_TMR_TYPE_FLUSHTGR:

                pPerStInfo = (tAstPerStInfo *) pEntryPtr;
                pPerStInfo->pFlushTgrTmr = NULL;
                u2InstanceId = pPerStInfo->u2InstanceId;

                /* When the timer FlushInterval fires, then check the flag 
                 * PENDING FLUSHES.. Based on that take decisions as follows
                 *
                 * -- If (PENDING FLUSHES == TRUE) . 
                 * (a) Call the flushing call per instance . Flush (Instance)
                 *
                 * -- If (PENDING FLUSHES == FLASE) . 
                 * (a) No need for any action
                 */

                if (pPerStInfo->FlushFlag.u1PendingFlushes == AST_TRUE)
                {
                    MstFlushFdbOnInstance (u2InstanceId);
                    pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
                    (pPerStInfo->PerStBridgeInfo.u4TotalFlushCount)++;
                }
                pPerStInfo->PerStBridgeInfo.u4FlushIndCount = AST_INIT_VAL;

                AST_DBG_ARG1 (AST_TMR_DBG | AST_TCSM_DBG,
                              "TMR:  FLUSHTGR Timer EXPIRED for "
                              "Instance: %u\n", u2InstanceId);

                break;

            case AST_TMR_TYPE_RESTART:
                pAstPortEntry = (tAstPortEntry *) pEntryPtr;
                if (NULL == pAstPortEntry)
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "pAstPortEntry NULL \n");
                    return MST_FAILURE;
                }

                pAstContextInfo = AST_CURR_CONTEXT_INFO ();

                if (pAstContextInfo != NULL)
                {
                    pAstContextInfo->pRestartTimer = NULL;
                }
                else
                {
                    AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                             "TMR: Invalid Context !!!\n");
                    return MST_FAILURE;
                }
                if (pAstPortEntry->u1RecScenario == LOOP_INC_RECOVERY)
                {
                    pPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (pAstPortEntry->u2PortNo,
                                                 u2InstanceId);
                    if (NULL == pPerStPortInfo)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "pPerStPortInfo NULL \n");
                        return MST_FAILURE;
                    }

                    if ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DESIGNATED)
                        && (pAstPortEntry->bLoopInconsistent == RST_TRUE)
                        && (pAstPortEntry->bLoopGuard == MST_TRUE))
                    {
                        NotifyProtoToApp.STPNotify.u4IfIndex =
                            pAstPortEntry->u4IfIndex;
                        AstCfaNotifyProtoToApp (STP_NOTIFY, NotifyProtoToApp);
                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_INIT_SHUT_TRC,
                                      "SYS: Enabling Port %s for Instance %d...\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2InstanceId);
                    }
                    pAstPortEntry->u1RecScenario = AST_INIT_VAL;
                }
                else
                {

                    if (AstRestartStateMachines () != MST_SUCCESS)
                    {
                        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                                 "TMR:AstRestartStateMachines invocation failed!!!\n");

                        i4RetVal = MST_FAILURE;
                    }
                }
                AST_DBG (AST_TMR_DBG, "TMR:  Restart Timer EXPIRED");

                break;

            case AST_TMR_TYPE_ROOT_INC_RECOVERY:
                pPerStPortInfo = (tAstPerStPortInfo *) pEntryPtr;
                pPerStPortInfo->PerStRstPortInfo.pRootIncRecTmr = NULL;
                /* Set Root Inconsistency Flag to false */
                pPerStPortInfo->bRootInconsistent = AST_FALSE;
                u2PortNum = pPerStPortInfo->u2PortNo;
                AST_DBG_ARG2 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Port %s: Root Inconistency Recovery Timer EXPIRED"
                              " for Instance: %u\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);
                if ((MstIsPortMemberOfInst
                     ((INT4) AST_GET_IFINDEX (pPerStPortInfo->u2PortNo),
                      (INT4) pPerStPortInfo->u2Inst) != MST_FALSE)
                    || (u2InstanceId == MST_CIST_CONTEXT))
                {
                    UtlGetTimeStr (au1TimeStr);
                    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                                      "Spanning-tree Root_Guard_Unblock : Root Guard feature unblocking Port: %s Instance : %d at %s\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      pPerStPortInfo->u2Inst, au1TimeStr);
                }

                if (MstPortInfoSmMakeAged
                    (pPerStPortInfo, NULL, MST_CIST_CONTEXT) != RST_SUCCESS)
                {
                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    AST_DBG (AST_TMR_DBG | AST_RTSM_DBG |
                             AST_ALL_FAILURE_DBG,
                             "TMR: Port Role Transition SEM returned FAILURE!\n");
                    i4RetVal = RST_FAILURE;
                }

                break;

            case AST_TMR_TYPE_INST_CREATE:
                pAstContextInfo = AST_CURR_CONTEXT_INFO ();

                if (pAstContextInfo != NULL)
                {
                    pAstContextInfo->pInstCreate = NULL;
                }
                if (AstAssertBegin () == MST_FAILURE)
                {
                    AST_MODULE_STATUS_SET ((UINT1) RST_DISABLED);

                    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC |
                             AST_INIT_SHUT_TRC,
                             "SYS: Asserting BEGIN failed!\n");
                    AST_DBG (AST_ALL_FAILURE_DBG | AST_INIT_SHUT_DBG,
                             "SYS: Asserting BEGIN failed!\n");
                    return MST_FAILURE;
                }
                AST_DBG_ARG1 (AST_TMR_DBG | AST_RTSM_DBG,
                              "TMR: Instance create Timer EXPIRED"
                              " for Instance: %u\n", u2InstanceId);
                break;

            default:
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Invalid Timer Type\n");
                i4RetVal = MST_FAILURE;

        }                        /* End of switch */
        if (AST_IS_MST_ENABLED () && (u1SyncFlag == MST_TRUE)
            && (AST_NODE_STATUS () == RED_AST_ACTIVE))
        {
            u1TimerInfo = (UINT1) (u1TimerType | AST_RED_TIMER_EXPIRED);
            AstRedSendSyncMessages (u2InstanceId, u2PortNum,
                                    RED_AST_TIMES, u1TimerInfo);
            pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
            if (pPerStInfo == NULL)
            {
                AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TMR: Invalid Instance in expired Timer!!!\n");
                return MST_FAILURE;
            }

            AST_SET_CHANGED_FLAG (u2InstanceId, u2PortNum) = RST_FALSE;
            u1SyncFlag = MST_FALSE;
        }
    }
    else
    {
        AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "TMR: Corrupt entry pointer in timer node !!!\n");
        i4RetVal = MST_FAILURE;
    }

    pAstTimer->u1IsTmrStarted = RST_FALSE;
    if (AST_NODE_STATUS () == RED_AST_ACTIVE)
    {
        if (AST_RELEASE_TMR_MEM_BLOCK (pAstTimer) != AST_MEM_SUCCESS)
        {
            AST_DBG (AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "TMR: Expired Timer Memory Block Release FAILED!\n");
            i4RetVal = MST_FAILURE;
        }
    }
    AST_DBG (AST_TMR_DBG, "TMR: Expired Timer processed \n");

    if (i4RetVal == MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC, "TMR: Expired Timer(s) processed \n");
        AST_DBG (AST_TMR_DBG,
                 "TMR: Timer Expiry event successfully processed ...\n");
    }
    UNUSED_PARAM (u1SyncFlag);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : MstGetVlanList                                       */
/*                                                                           */
/* Description        : This Function returns the list of VlanId's that are  */
/*                      Mapped to given Instance.                            */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the Flag has to be */
/*                                  Has to be checked.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_TRUE (or) MST_FALSE                              */
/*****************************************************************************/
VOID
MstGetVlanList (UINT2 u2MstInst, tAstVlanList VlanList, INT4 i4Flag)
{
    UINT2               u2VlanIdStart = AST_INIT_VAL;
    UINT2               u2VlanIdEnd = AST_INIT_VAL;
    UINT2               u2VlanId = AST_INIT_VAL;
    UINT2               u2TempVlanId = AST_INIT_VAL;

    switch (i4Flag)
    {
        case MST_VLANS_INSTANCE_1K:
            u2VlanIdStart = 1;
            u2VlanIdEnd = 1024;
            break;

        case MST_VLANS_INSTANCE_2K:
            u2VlanIdStart = 1025;
            u2VlanIdEnd = 2048;
            break;

        case MST_VLANS_INSTANCE_3K:
            u2VlanIdStart = 2049;
            u2VlanIdEnd = 3072;
            break;
        case MST_VLANS_INSTANCE_4K:
            u2VlanIdStart = 3073;
            u2VlanIdEnd = 4095;
            break;

        default:
            return;
    }
    for (u2VlanId = u2VlanIdStart; u2VlanId <= u2VlanIdEnd; u2VlanId++)
    {
        if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId)
            == u2MstInst)
        {
            if (u2VlanId > MST_VLANS_RANGE_3K)
            {
                u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_3K);
            }
            else if (u2VlanId > MST_VLANS_RANGE_2K)
            {
                u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_2K);
            }
            else if (u2VlanId > MST_VLANS_RANGE_1K)
            {
                u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_1K);
            }
            else
            {
                u2TempVlanId = u2VlanId;
            }
            OSIX_BITLIST_SET_BIT (VlanList, u2TempVlanId,
                                  MST_VLANMAP_LIST_SIZE);
        }
    }
}

/*****************************************************************************/
/* Function Name      : MstGetVfiVlanList                                    */
/*                                                                           */
/* Description        : This Function returns the list of VlanId's that are  */
/*                      Mapped to given Instance. This is applicable only    */
/*                      for VFIs that is VLANs from 4k to 8k                 */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the Flag has to be */
/*                                  Has to be checked.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_TRUE (or) MST_FALSE                              */
/*****************************************************************************/
VOID
MstGetVfiVlanList (UINT2 u2MstInst, tAstMstVlanList VlanList)
{
    UINT2               u2VlanId = AST_INIT_VAL;

    for (u2VlanId = VLAN_VFI_MIN_ID; u2VlanId <= VLAN_VFI_MAX_ID; u2VlanId++)
    {
        if (IS_STANDARD_VLAN_ID (u2VlanId))
        {
            continue;
        }
        if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (), u2VlanId)
            == u2MstInst)
        {
            OSIX_BITLIST_SET_BIT (VlanList, u2VlanId, MST_VLAN_LIST_SIZE);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : MstTriggerDigestCalc                                 */
/*                                                                           */
/* Description        : This function calculates the config digest whenever  */
/*                      there is a change in the VLAN to Instance mapping on */
/*                      a system.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MstTriggerDigestCalc (VOID)
{
    tAstMstBridgeEntry *pAstMstBridgeEntry = NULL;
    tMstConfigIdInfo   *pMstConfigIdInfo = NULL;
    UINT1              *pu1ConfigDigest = NULL;
    UINT1               au1Key[16] =
        { 0x13, 0xAC, 0x06, 0xA6, 0x2E, 0x47, 0xFD, 0x51, 0xF9, 0x5D, 0x2B,
        0xA2, 0x43, 0xCD, 0x03, 0x46
    };
    UINT1               au1ConfigDigest[MST_CONFIG_DIGEST_LEN];
    UINT1              *pu1DigPtr = NULL;
    UINT2               u2TmpVar = AST_INIT_VAL;

    pu1ConfigDigest = au1ConfigDigest;
    AST_MEMSET (pu1ConfigDigest, 0, sizeof (au1ConfigDigest));

    pAstMstBridgeEntry = AST_GET_MST_BRGENTRY ();
    pMstConfigIdInfo = &(pAstMstBridgeEntry->MstConfigIdInfo);

    pu1DigPtr = AST_GET_MST_DIGEST_INPUT ();
    if (pu1DigPtr == NULL)
    {
        AST_DBG (AST_ALL_FAILURE_DBG, "UTIL: Invalid Digest Pointer !!!\n");
        return;
    }

    AST_MEMSET (pu1DigPtr, 0, (sizeof (UINT2) * AST_VID_MAP_LEN));

    /* Fill the first entry as 0 */
    u2TmpVar = 0;
    AST_MEMCPY (pu1DigPtr, &u2TmpVar, sizeof (UINT2));
    pu1DigPtr += 2;

    /* Fill the rest of the entries with the instances mapped for 
     * Vlans from 1 to 4094 */
    AstL2IwfFillConfigDigest (AST_CURR_CONTEXT_ID (), pu1DigPtr,
                              (AST_VID_MAP_LEN - 2));
    pu1DigPtr += 2 * (AST_VID_MAP_LEN - 2);

    /* Fill the last entry as 0 */
    u2TmpVar = 0;
    AST_MEMCPY (pu1DigPtr, &u2TmpVar, sizeof (UINT2));

    MST_CALCULATE_DIGEST (AST_GET_MST_DIGEST_INPUT (),
                          (sizeof (UINT2) * AST_VID_MAP_LEN),
                          au1Key, sizeof (au1Key), pu1ConfigDigest);
    AST_MEMCPY ((pMstConfigIdInfo->au1ConfigDigest),
                pu1ConfigDigest, sizeof (au1ConfigDigest));

    AST_DBG (AST_MGMT_DBG | AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG,
             "Digest calculated successfully\n");
    return;
}

/* New utils added for 802.1Q for 
 * Port Transmit State Machine
 * */
/*****************************************************************************/
/* Function Name      : MstPortTxSmAllTransmitReady                          */
/*                                                                           */
/* Description        : This function implements the SEM Procedure           */
/*                      allTransmitReady. This has been newly added as per   */
/*                      802.1Q 2005 standard.                                */
/*                                                                           */
/* Input(s)           : u2PortNum - PortNum for which this function has been */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
tAstBoolean
MstPortTxSmAllTransmitReady (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortEntry = NULL;

    if ((pPortEntry = AST_GET_PORTENTRY (u2PortNum)) == NULL)
    {
        return MST_FALSE;
    }

    return (pPortEntry->bAllTransmitReady);
}

/*****************************************************************************/
/* Function Name      : MstMstiDesignatedOrTcPropagatingRoot                 */
/*                                                                           */
/* Description        : This Function checks whether the Port is in Desigated*/
/*                      or Root Port Role and TcWhile is Running for any of  */
/*                      the Instance.                                        */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which the Flag has to be */
/*                                  Has to be checked.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_TRUE (or) MST_FALSE                              */
/*****************************************************************************/
tAstBoolean
MstMstiDesignatedOrTcPropagatingRoot (UINT2 u2PortNum)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2TmpInst = 1;

    AST_GET_NEXT_BUF_INSTANCE (u2TmpInst, pPerStInfo)
    {
        if (pPerStInfo != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2TmpInst);
            if (pPerStPortInfo != NULL)
            {
                if ((pPerStPortInfo->u1SelectedPortRole ==
                     (UINT1) MST_PORT_ROLE_DESIGNATED) ||
                    ((pPerStPortInfo->u1SelectedPortRole ==
                      (UINT1) MST_PORT_ROLE_ROOT) &&
                     (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)))
                {
                    return MST_TRUE;
                }
#ifdef NPAPI_WANTED
                else if (pPerStPortInfo->i4TransmitSelfInfo == RST_TRUE)
                {
                    /* If setting port state to blocking in hardware has
                     * failed, then NewInfo should be set even on Alternate or
                     * Backup ports in order to send inferior info to the peer*/
                    return MST_TRUE;

                }
#endif
            }
        }
    }
    return MST_FALSE;
}

/*****************************************************************************/
/* Function Name      : MstPreparePseudoInfo                                 */
/*                                                                           */
/* Description        : This function creates a Pseudo Info BPDU. This MSTP  */
/*                      BPDU contains CIST and all active MSTI fields but it */
/*                      does not contain Ethernet header. It will be         */
/*                      presented to spanning tree as if it received from    */
/*                      the Physical Port on which it was invoked.           */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
MstPreparePseudoInfo (UINT2 u2PortNum, tMstBpdu ** ppPeudoMstBpdu)
{
    tMstBpdu           *pMstBpdu = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstBridgeEntry    *pBrgEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;
    UINT2               u2MstInst = 1;

    pMstBpdu = (*ppPeudoMstBpdu);

    pBrgEntry = AST_GET_BRGENTRY ();

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);

    pMstBpdu->u2ProtocolId = AST_INIT_VAL;
    pMstBpdu->u1Version = AST_VERSION_3;
    pMstBpdu->u1BpduType = AST_BPDU_TYPE_RST;

    pMstBpdu->u1CistFlags =
        MST_SET_DESG_PROLE_FLAG | MST_SET_LEARNING_FLAG |
        MST_SET_FORWARDING_FLAG;

    pMstBpdu->CistRootId.u2BrgPriority =
        pPerStPortInfo->PseudoRootId.u2BrgPriority;
    AST_MEMCPY (pMstBpdu->CistRootId.BridgeAddr,
                pPerStPortInfo->PseudoRootId.BridgeAddr, AST_MAC_ADDR_LEN);

    pMstBpdu->u4CistExtPathCost = AST_INIT_VAL;

    pMstBpdu->CistRegionalRootId.u2BrgPriority =
        pPerStPortInfo->PseudoRootId.u2BrgPriority;
    AST_MEMCPY (pMstBpdu->CistRegionalRootId.BridgeAddr,
                pPerStPortInfo->PseudoRootId.BridgeAddr, AST_MAC_ADDR_LEN);

    pMstBpdu->u4CistIntRootPathCost = AST_INIT_VAL;

    pMstBpdu->u2CistPortId = AST_INIT_VAL;

    pMstBpdu->u2MessageAge = pBrgEntry->BridgeTimes.u2MsgAgeOrHopCount;
    pMstBpdu->u2MaxAge = pBrgEntry->BridgeTimes.u2MaxAge;
    pMstBpdu->u2HelloTime = pBrgEntry->BridgeTimes.u2HelloTime;
    pMstBpdu->u2FwdDelay = pBrgEntry->BridgeTimes.u2ForwardDelay;

    pMstBpdu->u1Version1Len = AST_INIT_VAL;

    /* u2Version3Len =
     * MST Configuration Identifier + CIST Internal Root Path Cost +
     * CIST Bridge Identifier + CIST Remaining Hops
     */

    pMstBpdu->u2Version3Len
        = sizeof (tMstConfigIdInfo) + 4 + sizeof (tAstBridgeId) + 1;

    pMstBpdu->u1CistRemainingHops = pPerStBridgeInfo->u1BrgRemainingHops;

    AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
    {

        if (pPerStInfo == NULL)
        {
            continue;
        }

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

        if (pPerStPortInfo == NULL)
        {
            continue;
        }

        pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (u2MstInst);

        pMstBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.u2BrgPriority =
            pPerStPortInfo->PseudoRootId.u2BrgPriority;

        AST_MEMCPY
            (pMstBpdu->aMstConfigMsg[u2MstInst].MstiRegionalRootId.BridgeAddr,
             pPerStPortInfo->PseudoRootId.BridgeAddr, AST_MAC_ADDR_LEN);

        pMstBpdu->aMstConfigMsg[u2MstInst].u1MstiFlags =
            MST_SET_DESG_PROLE_FLAG | MST_SET_LEARNING_FLAG |
            MST_SET_FORWARDING_FLAG;

        pMstBpdu->aMstConfigMsg[u2MstInst].u4MstiIntRootPathCost = AST_INIT_VAL;

        pMstBpdu->aMstConfigMsg[u2MstInst].u1MstiRemainingHops =
            pPerStBridgeInfo->u1BrgRemainingHops;

        pMstBpdu->aMstConfigMsg[u2MstInst].u1ValidFlag = RST_TRUE;

        pMstBpdu->u2Version3Len += MST_BPDU_MSTI_LENGTH;

    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstCheckBPDUConsistency                              */
/*                                                                           */
/* Description        : This function compares the message priority vector of*/
/*                      the information received on the port with the port   */
/*                      priority vector for the port, and                    */
/*                         a)If the received message priority vector is      */
/*                           superior to the port priority vector, and       */
/*                         b)The BPDU is an ST BPDU (version 0 or version 1) */
/*                           or                                              */
/*                         c)The BPDU is an RST or MST BPDU (type 2,         */
/*                           version 2 or above) and its Port Role values    */
/*                           indicates Designated and its Learning flag is   */
/*                           set.                                            */
/*                     Then on this port the disputed flag is set and agreed */
/*                     is cleared and trigger Role Transition State machine  */
/*                     to re-evaluate port state for CIST and all MSTIs.     */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number at which event has occuer.   */
/*                      pRcvdBpdu - Received BPDU to be processed by         */
/*                                  PseudoInfo State Machine.                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/

INT4
MstCheckBPDUConsistency (UINT2 u2PortNum, tMstBpdu * pRcvdBpdu)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT2               u2MstInst = 1;
    UINT1               u1RcvdInfo;

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);

    u1RcvdInfo =
        MstPortInfoSmRcvInfo (pPerStPortInfo, pRcvdBpdu, MST_CIST_CONTEXT);

    if (u1RcvdInfo == MST_SUPERIOR_DESG_MSG)
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, MST_CIST_CONTEXT);
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

        pRstPortInfo->bDisputed = MST_TRUE;
        pRstPortInfo->bAgreed = MST_FALSE;

        /* 
         * Trigger Role Transition Machine to Re-Evaluate Port State
         * */
        if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_DISPUTED_SET,
                                       pPerStPortInfo, MST_CIST_CONTEXT)
            != MST_SUCCESS)
        {
            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "PSEUDO_SEM: Port %s: CIST: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));
            AST_DBG_ARG1 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                          "PSEUDO_SEM: Port %s: CIST: Role Transition Machine returned FAILURE\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            return MST_FAILURE;
        }

        AST_GET_NEXT_BUF_INSTANCE (u2MstInst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

            pRstPortInfo->bDisputed = MST_TRUE;
            pRstPortInfo->bAgreed = MST_FALSE;

            /* 
             * Trigger Role Transition Machine to Re-Evaluate Port State
             * */
            if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_DISPUTED_SET,
                                           pPerStPortInfo, u2MstInst)
                != MST_SUCCESS)
            {
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "PSEUDO_SEM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_PSSM_DBG | AST_ALL_FAILURE_DBG,
                              "PSEUDO_SEM: Port %s: Instance %u: Role Transition Machine returned FAILURE\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                return MST_FAILURE;
            }

        }
    }

    return MST_SUCCESS;
}

#ifdef RSTP_DEBUG
/*****************************************************************************/
/* Function Name      : MstDumpPerStPortInfo                                 */
/*                                                                           */
/* Description        : Prints the instance specific Port Information        */
/*                      Structure contents for MSTP.                         */
/*                      This routine is used for Debugging.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      u2InstanceId - Instance id of the spanning tree.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
MstDumpPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId)
{
    UINT2               u2Val1 = 0;
    UINT2               u2Val2 = 0;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeId       *pBrgId = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;

    UINT1               aau1PortRole[6][20] = {
        "DISABLED", "ALTERNATE", "BACKUP",
        "ROOT_PORT", "DESIGNATED_PORT", "MASTER_PORT"
    };
    UINT1               aau1PinfoSmState[MST_PINFOSM_MAX_STATES][20] = {
        "DISABLED", "ENABLED", "AGED", "UPDATE",
        "SUPERIOR_DESG", "REPEAT_DESG", "NOT_DESG",
        "CURRENT", "RECEIVE"
    };
    UINT1               aau1ProleTrSmState[MST_PROLETRSM_MAX_STATES][20] = {
        "INIT_PORT", "BLOCK_PORT",
        "BLOCKED_PORT", "ACTIVE_PORT"
    };
    UINT1               aau1PstateTrSmState[RST_PSTATETRSM_MAX_STATES][20] = {
        "DISCARDING", "LEARNING", "FORWARDING"
    };
    UINT1               aau1TopoChSmState[MST_TOPOCHSM_MAX_STATES][20] = {
        "INIT", "INACTIVE", "ACTIVE", "DETECTED",
        "NOTIFIED_TCN", "NOTIFIED_TC", "PROPAGATING", "ACKNOWLEDGED"
    };

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        return;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pPerStPortInfo == NULL)
    {
        return;
    }

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "*******************************************************\n");

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "Dumping PerStPortInfo for Port: %u, Instance: %u...\n",
               u2PortNum, u2InstanceId);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME, "DesgBrgId...\n");
    pBrgId = &(pPerStPortInfo->DesgBrgId);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2BrgPriority: 0x%x ", pBrgId->u2BrgPriority);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pBrgId->BridgeAddr[0],
               pBrgId->BridgeAddr[1], pBrgId->BridgeAddr[2],
               pBrgId->BridgeAddr[3], pBrgId->BridgeAddr[4],
               pBrgId->BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME, "RootId...\n");
    pBrgId = &(pPerStPortInfo->RootId);
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u2BrgPriority: 0x%x ", pBrgId->u2BrgPriority);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "BridgeAddr: %2x:%2x:%2x:%2x:%2x:%2x \n",
               pBrgId->BridgeAddr[0],
               pBrgId->BridgeAddr[1], pBrgId->BridgeAddr[2],
               pBrgId->BridgeAddr[3], pBrgId->BridgeAddr[4],
               pBrgId->BridgeAddr[5]);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4NumFwdTransitions: 0x%x, u4NumBpdusRx: 0x%x, u4NumBpdusTx: 0x%x\n",
               pPerStPortInfo->u4NumFwdTransitions,
               pPerStPortInfo->u4NumBpdusRx, pPerStPortInfo->u4NumBpdusTx);

    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u4RootCost: 0x%x, u2DesgPortId: 0x%x, u2PortNo: 0x%x, u1PortPriority: 0x%2x\n",
               pPerStPortInfo->u4RootCost, pPerStPortInfo->u2DesgPortId,
               pPerStPortInfo->u2PortNo, pPerStPortInfo->u1PortPriority);

    u2Val1 = pPerStPortInfo->u1PortRole;
    u2Val2 = pPerStPortInfo->u1SelectedPortRole;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PortRole: %s, u1SelectedPortRole: %s\n",
               aau1PortRole[u2Val1], aau1PortRole[u2Val2]);

    u2Val1 = pPerStPortInfo->u1PinfoSmState;
    u2Val2 = pPerStPortInfo->u1ProleTrSmState;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PinfoSmState: %s, u1ProleTrSmState: %s \n",
               aau1PinfoSmState[u2Val1], aau1ProleTrSmState[u2Val2]);

    u2Val1 = pPerStPortInfo->u1PstateTrSmState;
    u2Val2 = pPerStPortInfo->u1TopoChSmState;
    AST_PRINT (AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,
               "u1PstateTrSmState: %s, u1TopoChSmState: %s \n",
               aau1PstateTrSmState[u2Val1], aau1TopoChSmState[u2Val2]);

    AST_PRINTF ("*******************************************************\n");
    return;
}
#endif /* RSTP_DEBUG */
/*****************************************************************************/
/* Function Name      : MstSetMstMapVlanIndex                                */
/*                                                                           */
/* Description        : This function is called from the MSTP API and from   */
/*                      the Low level Set routine. It maps the given vlan    */
/*                      index to the MST instance.                           */
/*                                                                           */
/* Input(s)           : i4FsMstInstanceIndex - Mst instance index.           */
/*                      i4SetValFsMstMapVlanIndex - Vlan index that is to    */
/*                                                   be mapped.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
MstSetMstMapVlanIndex (INT4 i4FsMstInstanceIndex,
                       INT4 i4SetValFsMstMapVlanIndex)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSNMP_OCTET_STRING_TYPE *pOctetVlanList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1              *pu1VlanList = NULL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                      (UINT2) i4SetValFsMstMapVlanIndex)
        == i4FsMstInstanceIndex)
    {
        AST_TRC (AST_MGMT_TRC,
                 "SNMP: VLAN ID is Already Mapped to Instance Id!!\n");
        return (INT1) MST_SUCCESS;
    }
    /* We add this check here in this set routine because it can happen
     * that a config-save was done with VLAN learning mode to be IVL
     * after creating some MSTP instances.The switch could have been
     * restarted with vlan learning admin mode set to SVL and restarted
     * without saving the configuration in which case we should not restore
     * the MSTP instances */

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) ==
         VLAN_SHARED_LEARNING))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:If Shared VLAN learning only the default CIST"
                 "context can exist.\n");

        return MST_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return MST_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_INST_VLANID_MAP_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    pAstMsgNode->uMsg.VlanId = (tMstVlanId) i4SetValFsMstMapVlanIndex;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMapVlanIndex, u4SeqNum,
                              FALSE, AstLock, AstUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", AST_CURR_CONTEXT_ID (),
                          i4FsMstInstanceIndex, i4SetValFsMstMapVlanIndex));
        AstSelectContext (u4CurrContextId);
        return MST_FAILURE;
    }

    /*Allocating Memory for the octet string to store vlan list for a particular instance */
    if ((pOctetVlanList = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        return (MST_FAILURE);
    }
    MEMSET (pOctetVlanList->pu1_OctetList, AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    pu1VlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

    if (pu1VlanList == NULL)
    {
        free_octetstring (pOctetVlanList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pMstVlanList:Memory Allocation Failed\n");
        return MST_FAILURE;
    }
    AST_MEMSET (pu1VlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));
    L2IwfGetVlanListInInstance ((UINT2) i4FsMstInstanceIndex, pu1VlanList);
    MEMCPY (pOctetVlanList->pu1_OctetList, pu1VlanList, MST_VLAN_LIST_SIZE);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSetVlanList, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s", AST_CURR_CONTEXT_ID (),
                      i4FsMstInstanceIndex, pOctetVlanList));
    UtilVlanReleaseVlanListSize (pu1VlanList);
    free_octetstring (pOctetVlanList);

    AstSelectContext (u4CurrContextId);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstTestMstMapVlanIndex                               */
/*                                                                           */
/* Description        : To test the vlan Index and  MST instance that        */
/*                      has to be mapped.                                    */
/*                                                                           */
/* Input(s)           : i4FsMstInstanceIndex - Mst instance index.           */
/*                      i4SetValFsMstMapVlanIndex - Vlan index that is to    */
/*                                                   be mapped.              */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - An integer that identifies the        */
/*                                     type of error occured.                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
MstTestMstMapVlanIndex (UINT4 *pu4ErrorCode,
                        INT4 i4FsMstInstanceIndex,
                        INT4 i4TestValFsMstMapVlanIndex)
{
    UINT4               u4FdbId = 0;
    UINT2               u2VlanIndex = 0;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return MST_FAILURE;
    }

    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Instance Id Range is Invalid!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) MST_FAILURE;
    }

    if (i4FsMstInstanceIndex != AST_TE_MSTID)
    {
        if (AST_GET_NO_OF_ACTIVE_INSTANCES >= AST_GET_MAX_MST_INSTANCES)
        {
            if (AST_GET_PERST_INFO (i4FsMstInstanceIndex) == NULL)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC,
                              "ERROR: No Of Active Instances cant exceed"
                              " MaxMstInst - %d \n",
                              AST_GET_NO_OF_ACTIVE_INSTANCES);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (INT1) MST_FAILURE;
            }
        }
    }

    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:For Instance to be mapped to VLAN, VLAN should "
                 "be enabledd!!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) MST_FAILURE;
    }
    /* We add this check here in this set routine because it can happen
     * that a config-save was done with VLAN learning mode to be IVL
     * after creating some MSTP instances.The switch could have been
     * restarted with vlan learning admin mode set to SVL and restarted
     * without saving the configuration in which case we should not restore
     * the MSTP instances */

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) ==
         VLAN_SHARED_LEARNING))
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:If Shared VLAN learning only the default CIST"
                 "context can exist.\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_STP_SHARED_VLAN_ERR);
        return (INT1) MST_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) i4TestValFsMstMapVlanIndex) == MST_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Enabled!!\n");

        CLI_SET_ERR (CLI_STP_INVALID_VLAN_ID_ERR);

        return (INT1) MST_FAILURE;
    }

    /* To check whether the first vlan belonging to the same FID as
       i4TestValFsMstMapVlanIndex is mapped to i4FsMstInstanceIndex */

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) !=
         VLAN_INDEP_LEARNING))
    {
        u4FdbId = AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                          (UINT2) i4TestValFsMstMapVlanIndex);

        for (u2VlanIndex = 1; u2VlanIndex <= AST_MAX_NUM_VLANS; u2VlanIndex++)
        {
            u4FdbId = AstL2IwfMiGetVlanFdbId
                (AST_CURR_CONTEXT_ID (), (UINT2) i4TestValFsMstMapVlanIndex);

            for (u2VlanIndex = 1; u2VlanIndex <= AST_MAX_NUM_VLANS;
                 u2VlanIndex++)
            {
                if (u2VlanIndex != (UINT4) i4TestValFsMstMapVlanIndex)
                {
                    if (AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                                (UINT2) u2VlanIndex) == u4FdbId)
                    {
                        if (AstL2IwfMiGetVlanInstMapping
                            (AST_CURR_CONTEXT_ID (), u2VlanIndex) !=
                            (UINT2) i4FsMstInstanceIndex)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                          "SNMP: VLAN ID with FID %d Cannot be"
                                          "mapped to Instance Id!!\n", u4FdbId);
                            CLI_SET_ERR (CLI_STP_VLAN_MAP_ERR);

                            return (INT1) MST_FAILURE;

                        }
                        break;
                    }
                }
            }
        }
    }

    if (MstSispValidateVlantoInstanceMapping ((tVlanId)
                                              i4TestValFsMstMapVlanIndex,
                                              (UINT2)
                                              i4FsMstInstanceIndex)
        != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: In contexts with SISP enabled ports mapped to it, Same port cannot be member of same instance in two different contexts\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        /* Determine the CLI error */
        CLI_SET_ERR (CLI_STP_NO_SAME_INST_IN_DIFF_CTXT);
        return SNMP_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSetMstUnMapVlanIndex                              */
/*                                                                           */
/* Description        : UnMaps the given vlan index to the MST instance.     */
/*                                                                           */
/* Input(s)           : i4FsMstInstanceIndex - Mst instance index.           */
/*                      i4SetValFsMstMapVlanIndex - Vlan index that is to    */
/*                                                   be mapped.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
MstSetMstUnMapVlanIndex (INT4 i4FsMstInstanceIndex,
                         INT4 i4SetValFsMstUnMapVlanIndex)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSNMP_OCTET_STRING_TYPE *pOctetVlanList = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT1              *pu1VlanList = NULL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return MST_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = MST_INST_VLANID_UNMAP_MSG;
    pAstMsgNode->u2InstanceId = (UINT2) i4FsMstInstanceIndex;
    pAstMsgNode->uMsg.VlanId = (tMstVlanId) i4SetValFsMstUnMapVlanIndex;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstUnMapVlanIndex, u4SeqNum,
                              FALSE, AstLock, AstUnLock, 2, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", AST_CURR_CONTEXT_ID (),
                          i4FsMstInstanceIndex, i4SetValFsMstUnMapVlanIndex));
        AstSelectContext (u4CurrContextId);
        return MST_FAILURE;
    }

    /*Allocating memory for Octet string to store vlan list */
    if ((pOctetVlanList = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        return (MST_FAILURE);
    }
    MEMSET (pOctetVlanList->pu1_OctetList, AST_INIT_VAL, MST_VLAN_LIST_SIZE);

    pu1VlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

    if (pu1VlanList == NULL)
    {
        free_octetstring (pOctetVlanList);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "pMstVlanList:Memory Allocation Failed\n");
        return MST_FAILURE;
    }

    AST_MEMSET (pu1VlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));
    L2IwfGetVlanListInInstance (i4FsMstInstanceIndex, pu1VlanList);
    MEMCPY (pOctetVlanList->pu1_OctetList, pu1VlanList, MST_VLAN_LIST_SIZE);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstSetVlanList, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s", AST_CURR_CONTEXT_ID (),
                      i4FsMstInstanceIndex, pOctetVlanList));
    UtilVlanReleaseVlanListSize (pu1VlanList);
    free_octetstring (pOctetVlanList);

    AstSelectContext (u4CurrContextId);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstTestMstUnMapVlanIndex                             */
/*                                                                           */
/* Description        : To test the vlan Index and  MST instance that        */
/*                      has to be unmapped.                                  */
/*                                                                           */
/* Input(s)           : i4FsMstInstanceIndex - Mst instance index.           */
/*                      i4SetValFsMstUnMapVlanIndex - Vlan index that is to  */
/*                                                   be unmapped.            */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - An integer that identifies the        */
/*                                     type of error occured.                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
MstTestMstUnMapVlanIndex (UINT4 *pu4ErrorCode, INT4 i4FsMstInstanceIndex,
                          INT4 i4TestValFsMstUnMapVlanIndex)
{
    UINT4               u4FdbId;
    UINT2               u2VlanIndex;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return MST_FAILURE;
    }
    if (MST_IS_INSTANCE_VALID (i4FsMstInstanceIndex) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Invalid Instance!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return MST_FAILURE;
    }
    if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                      (UINT2) i4TestValFsMstUnMapVlanIndex)
        != i4FsMstInstanceIndex)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Wrong Instance to Vlan combination!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return MST_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) i4TestValFsMstUnMapVlanIndex) == MST_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Active!!\n");
        return (INT1) MST_FAILURE;
    }

    /* To check whether the first vlan belonging to the same FID as 
       i4TestValFsMstUnMapVlanIndex is mapped to CIST */

    if ((AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ()) !=
         VLAN_INDEP_LEARNING))
    {
        u4FdbId = AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                          (UINT2) i4TestValFsMstUnMapVlanIndex);

        for (u2VlanIndex = 1; u2VlanIndex <= AST_MAX_NUM_VLANS; u2VlanIndex++)
        {
            if (u2VlanIndex != (UINT4) i4TestValFsMstUnMapVlanIndex)
            {
                if (AstL2IwfMiGetVlanFdbId (AST_CURR_CONTEXT_ID (),
                                            (UINT2) u2VlanIndex) == u4FdbId)
                {
                    if (AstL2IwfMiGetVlanInstMapping (AST_CURR_CONTEXT_ID (),
                                                      u2VlanIndex) !=
                        (UINT2) MST_CIST_CONTEXT)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                                      "SNMP: VLAN ID with FID %d Cannot be"
                                      " unmapped from Instance Id!!\n",
                                      u4FdbId);
                        CLI_SET_ERR (CLI_STP_VLAN_MAP_ERR);

                        return (INT1) MST_FAILURE;

                    }
                    break;
                }
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstConvertToLocalPortList                           */
/*                                                                           */
/* Description        : This routine converts global ifindex based           */
/*                      portlists to localport based port lists.             */
/*                                                                           */
/* Input(s)           : IfPortList                                           */
/*                                                                           */
/* Output(s)          : LocalPortList                                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
AstConvertToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList)
{
    UINT4               u4ContextId;
    UINT4               u4Port;
    UINT2               u2LocalPortId;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];

        for (u2BitIndex = 0; ((u2BitIndex < MST_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (AstGetContextInfoFromIfIndex (u4Port,
                                                  &u4ContextId,
                                                  &u2LocalPortId) !=
                    RST_SUCCESS)
                {
                    return MST_FAILURE;
                }

                if (u4ContextId != AST_CURR_CONTEXT_ID ())
                {
                    return MST_FAILURE;
                }

                MST_SET_MEMBER_PORT (LocalPortList, u2LocalPortId);
            }

            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstFlushFdbOnInstance                                */
/*                                                                           */
/* Description        : This routine invokes the flushes the entries         */
/*                      learnt on a particular instance.                     */
/*                                                                           */
/* Input(s)           : u2InstId - MST Instance ID                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstFlushFdbOnInstance (UINT2 u2InstId)
{
    UINT4               u4IfIndex = AST_INVALID_PORT_NUM;

    if (L2IwfFlushFdbForGivenInst
        (AST_CURR_CONTEXT_ID (), u4IfIndex, u2InstId,
         VLAN_OPTIMIZE, STP_MODULE, TRUE) == L2IWF_FAILURE)
    {
        AST_DBG ((AST_TCSM_DBG | AST_ALL_FAILURE_DBG),
                 "TCSM: L2IwfFlushFdbForGivenInst returned Failure\r\n");
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : MstGetInstPortLoopInconsistentState                  */
/*                                                                           */
/* Description        : This functions returns Instance based Loop           */
/*                      Inconsistent state for the given port                */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number                              */
/*                      u2InstanceId - Instance Identifier                   */
/*                                                                           */
/* Output(s)          : pi4InstPortLoopIncStatus - Instance based state      */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS/ MST_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
MstGetInstPortLoopInconsistentState (UINT2 u2PortNum, UINT2 u2InstanceId,
                                     INT4 *pi4InstPortLoopIncStatus)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (u2PortNum,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return MST_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return MST_FAILURE;
    }

    if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
    {
        AstReleaseContext ();
        return MST_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstanceId);

    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return MST_FAILURE;
    }

    if (pPerStPortInfo->bLoopIncStatus == MST_TRUE)
    {
        *pi4InstPortLoopIncStatus = MST_TRUE;
    }
    else
    {
        *pi4InstPortLoopIncStatus = MST_FALSE;
    }

    AstReleaseContext ();
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstConfigBridgePriority                              */
/*                                                                           */
/* Description        : This function is called from the set low level       */
/*                      routines for setting the given bridge priority.      */
/*                                                                           */
/* Input(s)           : i4InstId - MST Instance ID                           */
/*                      i4BridgePriority - Bridge Priority                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS or MST_FAILURE                           */
/*****************************************************************************/
INT1
MstConfigBridgePriority (INT4 i4InstId, INT4 i4BridgePriority)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = MST_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");

        return MST_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_BRIDGE_PRIORITY_MSG;
    pAstMsgNode->uMsg.u2BridgePriority = (UINT2) i4BridgePriority;
    pAstMsgNode->u2InstanceId = (UINT2) i4InstId;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = MST_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiBridgePriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 2, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", AST_CURR_CONTEXT_ID (),
                      i4InstId, i4BridgePriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}
extern UINT4        gu4Stups;
/****************************************************************************
 * *
 * *    FUNCTION NAME    : MstUtilTicksToDate
 * *
 * *    DESCRIPTION      : This function converts the milliseconds to Date Format
 * *
 * *    INPUT            : u4Ticks
 * *
 * *    OUTPUT           : u1DateFormat- Formatted Date
 * *
 * *    RETURNS          : None
 * ****************************************************************************/
PUBLIC VOID
MstUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks (u4Ticks, &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

#endif /* MSTP_WANTED */
