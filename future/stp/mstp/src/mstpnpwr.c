/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: mstpnpwr.c,v 1.3 2013/11/14 11:34:37 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              for Hardware API's w.r.t MSTP
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __MSTP_NP_WR_C
#define __MSTP_NP_WR_C

#include "astminc.h"
#include "mstpnpwr.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMstpNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
MstpNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMstpNpModInfo = &(pFsHwNp->MstpNpModInfo);

    if (NULL == pMstpNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_MSTP_NP_CREATE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpCreateInstance *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpCreateInstance;
            u1RetVal =
                FsMiMstpNpCreateInstance (pEntry->u4ContextId,
                                          pEntry->u2InstId);
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanInstMapping *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanInstMapping;
            u1RetVal =
                FsMiMstpNpAddVlanInstMapping (pEntry->u4ContextId,
                                              pEntry->VlanId, pEntry->u2InstId);
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanListInstMapping *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanListInstMapping;
            u1RetVal =
                FsMiMstpNpAddVlanListInstMapping (pEntry->u4ContextId,
                                                  pEntry->pu1VlanList,
                                                  pEntry->u2InstId,
                                                  pEntry->u2NumVlans,
                                                  pEntry->pu2LastVlan);
            break;
        }
        case FS_MI_MSTP_NP_DELETE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpDeleteInstance *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDeleteInstance;
            u1RetVal =
                FsMiMstpNpDeleteInstance (pEntry->u4ContextId,
                                          pEntry->u2InstId);
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanInstMapping *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanInstMapping;
            u1RetVal =
                FsMiMstpNpDelVlanInstMapping (pEntry->u4ContextId,
                                              pEntry->VlanId, pEntry->u2InstId);
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanListInstMapping *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanListInstMapping;
            u1RetVal =
                FsMiMstpNpDelVlanListInstMapping (pEntry->u4ContextId,
                                                  pEntry->pu1VlanList,
                                                  pEntry->u2InstId,
                                                  pEntry->u2NumVlans,
                                                  pEntry->pu2LastVlan);
            break;
        }
        case FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        {
            tMstpNpWrFsMiMstpNpSetInstancePortState *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpSetInstancePortState;
            u1RetVal =
                FsMiMstpNpSetInstancePortState (pEntry->u4ContextId,
                                                pEntry->u4IfIndex,
                                                pEntry->u2InstanceId,
                                                pEntry->u1PortState);
            break;
        }
        case FS_MI_MSTP_NP_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpInitHw *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpInitHw;
            FsMiMstpNpInitHw (pEntry->u4ContextId);
            break;
        }
        case FS_MI_MSTP_NP_DE_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpDeInitHw *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDeInitHw;
            FsMiMstpNpDeInitHw (pEntry->u4ContextId);
            break;
        }
        case FS_MI_MST_NP_GET_PORT_STATE:
        {
            tMstpNpWrFsMiMstNpGetPortState *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstNpGetPortState;
            u1RetVal =
                FsMiMstNpGetPortState (pEntry->u4ContextId,
                                       pEntry->u2InstanceId, pEntry->u4IfIndex,
                                       pEntry->pu1Status);
            break;
        }
#ifdef  MBSM_WANTED
        case FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE:
        {
            tMstpNpWrFsMiMstpMbsmNpCreateInstance *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpCreateInstance;
            u1RetVal =
                FsMiMstpMbsmNpCreateInstance (pEntry->u4ContextId,
                                              pEntry->u2InstId,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpMbsmNpAddVlanInstMapping *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpAddVlanInstMapping;
            u1RetVal =
                FsMiMstpMbsmNpAddVlanInstMapping (pEntry->u4ContextId,
                                                  pEntry->VlanId,
                                                  pEntry->u2InstId,
                                                  pEntry->pSlotInfo);
            break;
        }
        case FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE:
        {
            tMstpNpWrFsMiMstpMbsmNpSetInstancePortState *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpSetInstancePortState;
            u1RetVal =
                FsMiMstpMbsmNpSetInstancePortState (pEntry->u4ContextId,
                                                    pEntry->u4IfIndex,
                                                    pEntry->u2InstanceId,
                                                    pEntry->u1PortState,
                                                    pEntry->pSlotInfo);
            break;
        }
        case FS_MI_MSTP_MBSM_NP_INIT_HW:
        {
            tMstpNpWrFsMiMstpMbsmNpInitHw *pEntry = NULL;
            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpInitHw;
            u1RetVal =
                FsMiMstpMbsmNpInitHw (pEntry->u4ContextId, pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __MSTP_NP_WR_C */
