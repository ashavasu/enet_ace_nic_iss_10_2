/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1s1lw.c,v 1.19 2014/01/31 12:54:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "std1s1lw.h"
#include  "astminc.h"
/* LOW LEVEL Routines for Table : Ieee8021MstpCistTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpCistTable
 Input       :  The Indices
                Ieee8021MstpCistComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpCistTable (UINT4
                                               u4Ieee8021MstpCistComponentId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    return (nmhValidateIndexInstanceFsMIDot1sFutureMstTable ((INT4)
                                                             u4Ieee8021MstpCistComponentId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpCistTable
 Input       :  The Indices
                Ieee8021MstpCistComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpCistTable (UINT4 *pu4Ieee8021MstpCistComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    i4RetVal = nmhGetFirstIndexFsMIDot1sFutureMstTable ((INT4 *)
                                                        pu4Ieee8021MstpCistComponentId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021MstpCistComponentId));

    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpCistTable
 Input       :  The Indices
                Ieee8021MstpCistComponentId
                nextIeee8021MstpCistComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpCistTable (UINT4 u4Ieee8021MstpCistComponentId,
                                      UINT4 *pu4NextIeee8021MstpCistComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    i4RetVal = nmhGetNextIndexFsMIDot1sFutureMstTable ((INT4)
                                                       u4Ieee8021MstpCistComponentId,
                                                       (INT4 *)
                                                       pu4NextIeee8021MstpCistComponentId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021MstpCistComponentId));

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistBridgeIdentifier
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                retValIeee8021MstpCistBridgeIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistBridgeIdentifier (UINT4 u4Ieee8021MstpCistComponentId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValIeee8021MstpCistBridgeIdentifier)
{
    UINT1              *pu1List = NULL;
    tAstMacAddr         MacAddress;
    INT4                i4Priority;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_ENABLED ()))
    {
        pu1List = pRetValIeee8021MstpCistBridgeIdentifier->pu1_OctetList;
        /* KlocWorks warning removal */
        if (pu1List == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
        else
        {
            AST_MEMSET (pu1List, AST_INIT_VAL,
                        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);
            pRetValIeee8021MstpCistBridgeIdentifier->i4_Length =
                AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
            AstReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    nmhGetFsMIMstBrgAddress (u4Ieee8021MstpCistComponentId, &MacAddress);
    nmhGetFsMIMstCistBridgePriority (u4Ieee8021MstpCistComponentId,
                                     &i4Priority);
    /* First 2 bytes contain the bridge priority and the
     *                  * Next 6 bytes contain the Bridge MAC*/
    pRetValIeee8021MstpCistBridgeIdentifier->pu1_OctetList[0] =
        (UINT1) ((i4Priority & 0xff00) >> MSTP_STD_OCT);
    pRetValIeee8021MstpCistBridgeIdentifier->pu1_OctetList[1] =
        (UINT1) (i4Priority & 0x00ff);

    /*Merging Mac Address */
    AST_MEMCPY (pRetValIeee8021MstpCistBridgeIdentifier->pu1_OctetList +
                MSTP_STD_BIN, MacAddress, AST_MAC_ADDR_SIZE);
    pRetValIeee8021MstpCistBridgeIdentifier->i4_Length =
        AST_MAC_ADDR_SIZE + MSTP_STD_BIN;
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistTopologyChange
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                retValIeee8021MstpCistTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistTopologyChange (UINT4 u4Ieee8021MstpCistComponentId,
                                      INT4
                                      *pi4RetValIeee8021MstpCistTopologyChange)
{
    tAstPerStInfo      *pPerStInfo = NULL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistComponentId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021MstpCistTopologyChange = AST_SNMP_FALSE;

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    pPerStInfo = AST_GET_PERST_INFO (MST_CIST_CONTEXT);

    if (pPerStInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pPerStInfo->u4TcWhileCount != AST_INIT_VAL)
    {
        *pi4RetValIeee8021MstpCistTopologyChange = AST_SNMP_TRUE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistRegionalRootIdentifier
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                retValIeee8021MstpCistRegionalRootIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistRegionalRootIdentifier (UINT4
                                              u4Ieee8021MstpCistComponentId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021MstpCistRegionalRootIdentifier)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    return (nmhGetFsMIMstCistRegionalRoot ((INT4) u4Ieee8021MstpCistComponentId,
                                           pRetValIeee8021MstpCistRegionalRootIdentifier));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPathCost
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                retValIeee8021MstpCistPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPathCost (UINT4 u4Ieee8021MstpCistComponentId,
                                UINT4 *pu4RetValIeee8021MstpCistPathCost)
{

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    return (nmhGetFsMIMstCistRegionalRootCost
            ((INT4) u4Ieee8021MstpCistComponentId,
             (INT4 *) pu4RetValIeee8021MstpCistPathCost));

}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistMaxHops
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                retValIeee8021MstpCistMaxHops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistMaxHops (UINT4 u4Ieee8021MstpCistComponentId,
                               INT4 *pi4RetValIeee8021MstpCistMaxHops)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    return (nmhGetFsMIMstMaxHopCount ((INT4) u4Ieee8021MstpCistComponentId,
                                      pi4RetValIeee8021MstpCistMaxHops));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistMaxHops
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                setValIeee8021MstpCistMaxHops
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistMaxHops (UINT4 u4Ieee8021MstpCistComponentId,
                               INT4 i4SetValIeee8021MstpCistMaxHops)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    return (nmhSetFsMIMstMaxHopCount ((INT4) u4Ieee8021MstpCistComponentId,
                                      i4SetValIeee8021MstpCistMaxHops));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistMaxHops
 Input       :  The Indices
                Ieee8021MstpCistComponentId

                The Object 
                testValIeee8021MstpCistMaxHops
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistMaxHops (UINT4 *pu4ErrorCode,
                                  UINT4 u4Ieee8021MstpCistComponentId,
                                  INT4 i4TestValIeee8021MstpCistMaxHops)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /*Validating for minimum and maximum values as per the definition 
     * given for ieee8021MstpCistMaxHops */

    if ((i4TestValIeee8021MstpCistMaxHops < AST_MIN_IEEE_HOP_COUNT) ||
        (i4TestValIeee8021MstpCistMaxHops > AST_MAX_HOP_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (AST_IS_MGMT_TIME_IN_SEC (i4TestValIeee8021MstpCistMaxHops) != AST_OK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpCistTable
 Input       :  The Indices
                Ieee8021MstpCistComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpCistTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpTable
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpTable (UINT4 u4Ieee8021MstpComponentId,
                                           UINT4 u4Ieee8021MstpId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhValidateIndexInstanceFsMIMstMstiBridgeTable ((INT4)
                                                            u4Ieee8021MstpComponentId,
                                                            (INT4)
                                                            u4Ieee8021MstpId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpTable
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpTable (UINT4 *pu4Ieee8021MstpComponentId,
                                   UINT4 *pu4Ieee8021MstpId)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    i4RetVal = nmhGetFirstIndexFsMIMstMstiBridgeTable ((INT4 *)
                                                       pu4Ieee8021MstpComponentId,
                                                       (INT4 *)
                                                       pu4Ieee8021MstpId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021MstpComponentId));

    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpTable
 Input       :  The Indices
                Ieee8021MstpComponentId
                nextIeee8021MstpComponentId
                Ieee8021MstpId
                nextIeee8021MstpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpTable (UINT4 u4Ieee8021MstpComponentId,
                                  UINT4 *pu4NextIeee8021MstpComponentId,
                                  UINT4 u4Ieee8021MstpId,
                                  UINT4 *pu4NextIeee8021MstpId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    i4RetVal = nmhGetNextIndexFsMIMstMstiBridgeTable ((INT4)
                                                      u4Ieee8021MstpComponentId,
                                                      (INT4 *)
                                                      pu4NextIeee8021MstpComponentId,
                                                      (INT4) u4Ieee8021MstpId,
                                                      (INT4 *)
                                                      pu4NextIeee8021MstpId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021MstpComponentId));

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpBridgeId
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpBridgeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpBridgeId (UINT4 u4Ieee8021MstpComponentId,
                            UINT4 u4Ieee8021MstpId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIeee8021MstpBridgeId)
{
    UINT1              *pu1List = NULL;
    tAstMacAddr         MacAddress;
    INT4                i4Priority = 0;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    if (AstSelectContext (u4Ieee8021MstpComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_ENABLED ()))
    {
        pu1List = pRetValIeee8021MstpBridgeId->pu1_OctetList;
        /* KlocWorks warning removal */
        if (pu1List == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
        else
        {
            AST_MEMSET (pu1List, AST_INIT_VAL,
                        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);
            pRetValIeee8021MstpBridgeId->i4_Length =
                AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
            AstReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    nmhGetFsMIMstBrgAddress ((INT4) u4Ieee8021MstpComponentId, &MacAddress);
    nmhGetFsMIMstMstiBridgePriority ((INT4) u4Ieee8021MstpComponentId,
                                     (INT4) u4Ieee8021MstpId, &i4Priority);
    /* First 2 bytes contain the bridge priority and the
     *  *                  * Next 6 bytes contain the Bridge MAC*/
    pRetValIeee8021MstpBridgeId->pu1_OctetList[0] =
        (UINT1) ((i4Priority & 0xff00) >> MSTP_STD_OCT);
    pRetValIeee8021MstpBridgeId->pu1_OctetList[1] =
        (UINT1) (i4Priority & 0x00ff);
    /*Merging Mac Address */
    AST_MEMCPY (pRetValIeee8021MstpBridgeId->pu1_OctetList + MSTP_STD_BIN,
                MacAddress, AST_MAC_ADDR_SIZE);
    pRetValIeee8021MstpBridgeId->i4_Length = AST_MAC_ADDR_SIZE + MSTP_STD_BIN;
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpTimeSinceTopologyChange
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpTimeSinceTopologyChange (UINT4 u4Ieee8021MstpComponentId,
                                           UINT4 u4Ieee8021MstpId,
                                           UINT4
                                           *pu4RetValIeee8021MstpTimeSinceTopologyChange)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstMstiTimeSinceTopologyChange ((INT4)
                                                      u4Ieee8021MstpComponentId,
                                                      (INT4) u4Ieee8021MstpId,
                                                      pu4RetValIeee8021MstpTimeSinceTopologyChange));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpTopologyChanges
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpTopologyChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpTopologyChanges (UINT4 u4Ieee8021MstpComponentId,
                                   UINT4 u4Ieee8021MstpId,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValIeee8021MstpTopologyChanges)
{
    UINT4              *pu4lsn = NULL;

    pu8RetValIeee8021MstpTopologyChanges->lsn = 0;
    pu8RetValIeee8021MstpTopologyChanges->msn = 0;
    pu4lsn = &(pu8RetValIeee8021MstpTopologyChanges->lsn);

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    if ((nmhGetFsMIMstMstiTopChanges ((INT4) u4Ieee8021MstpComponentId,
                                      (INT4) u4Ieee8021MstpId,
                                      pu4lsn)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pu8RetValIeee8021MstpTopologyChanges->lsn = *pu4lsn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpTopologyChange
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpTopologyChange (UINT4 u4Ieee8021MstpComponentId,
                                  UINT4 u4Ieee8021MstpId,
                                  INT4 *pi4RetValIeee8021MstpTopologyChange)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    if (AstSelectContext (u4Ieee8021MstpComponentId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);

    if (pPerStInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /* If the TC while Timer has been started for this instance; return true.
     * else return false*/

    if (pPerStInfo->u4TcWhileCount > 0)
    {
        *pi4RetValIeee8021MstpTopologyChange = AST_SNMP_TRUE;
    }

    if (pPerStInfo->u4TcWhileCount == 0)
    {
        *pi4RetValIeee8021MstpTopologyChange = AST_SNMP_FALSE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpDesignatedRoot
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpDesignatedRoot (UINT4 u4Ieee8021MstpComponentId,
                                  UINT4 u4Ieee8021MstpId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValIeee8021MstpDesignatedRoot)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstMstiBridgeRegionalRoot ((INT4)
                                                 u4Ieee8021MstpComponentId,
                                                 (INT4) u4Ieee8021MstpId,
                                                 pRetValIeee8021MstpDesignatedRoot));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpRootPathCost
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpRootPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpRootPathCost (UINT4 u4Ieee8021MstpComponentId,
                                UINT4 u4Ieee8021MstpId,
                                INT4 *pi4RetValIeee8021MstpRootPathCost)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstMstiRootCost ((INT4) u4Ieee8021MstpComponentId,
                                       (INT4) u4Ieee8021MstpId,
                                       pi4RetValIeee8021MstpRootPathCost));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpRootPort
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpRootPort (UINT4 u4Ieee8021MstpComponentId,
                            UINT4 u4Ieee8021MstpId,
                            UINT4 *pu4RetValIeee8021MstpRootPort)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstMstiRootPort ((INT4) u4Ieee8021MstpComponentId,
                                       (INT4) u4Ieee8021MstpId,
                                       (INT4 *) pu4RetValIeee8021MstpRootPort));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpBridgePriority
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpBridgePriority (UINT4 u4Ieee8021MstpComponentId,
                                  UINT4 u4Ieee8021MstpId,
                                  INT4 *pi4RetValIeee8021MstpBridgePriority)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    /* Description of mib object ieee8021MstpBridgePriority does not specify any default value. 
     * But default value of 32768 (fsMIMstMstiBridgePriority has default value as 32768)  will be returned 
     * to maintain backward compatibility with existing code.  */

    return (nmhGetFsMIMstMstiBridgePriority ((INT4) u4Ieee8021MstpComponentId,
                                             (INT4) u4Ieee8021MstpId,
                                             pi4RetValIeee8021MstpBridgePriority));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpVids0
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpVids0
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpVids0 (UINT4 u4Ieee8021MstpComponentId,
                         UINT4 u4Ieee8021MstpId,
                         tSNMP_OCTET_STRING_TYPE * pRetValIeee8021MstpVids0)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstInstanceVlanMapped ((INT4) u4Ieee8021MstpComponentId,
                                             (INT4) u4Ieee8021MstpId,
                                             pRetValIeee8021MstpVids0));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpVids1
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpVids1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpVids1 (UINT4 u4Ieee8021MstpComponentId,
                         UINT4 u4Ieee8021MstpId,
                         tSNMP_OCTET_STRING_TYPE * pRetValIeee8021MstpVids1)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstInstanceVlanMapped2k ((INT4) u4Ieee8021MstpComponentId,
                                               (INT4) u4Ieee8021MstpId,
                                               pRetValIeee8021MstpVids1));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpVids2
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpVids2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpVids2 (UINT4 u4Ieee8021MstpComponentId,
                         UINT4 u4Ieee8021MstpId,
                         tSNMP_OCTET_STRING_TYPE * pRetValIeee8021MstpVids2)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstInstanceVlanMapped3k ((INT4) u4Ieee8021MstpComponentId,
                                               (INT4) u4Ieee8021MstpId,
                                               pRetValIeee8021MstpVids2));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpVids3
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpVids3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpVids3 (UINT4 u4Ieee8021MstpComponentId,
                         UINT4 u4Ieee8021MstpId,
                         tSNMP_OCTET_STRING_TYPE * pRetValIeee8021MstpVids3)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhGetFsMIMstInstanceVlanMapped4k ((INT4) u4Ieee8021MstpComponentId,
                                               (INT4) u4Ieee8021MstpId,
                                               pRetValIeee8021MstpVids3));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpRowStatus
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                retValIeee8021MstpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpRowStatus (UINT4 u4Ieee8021MstpComponentId,
                             UINT4 u4Ieee8021MstpId,
                             INT4 *pi4RetValIeee8021MstpRowStatus)
{
    tAstPerStInfo      *pPerStInfo = NULL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    if (AstSelectContext (u4Ieee8021MstpComponentId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);
    if (pPerStInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021MstpRowStatus = pPerStInfo->u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpBridgePriority
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                setValIeee8021MstpBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpBridgePriority (UINT4 u4Ieee8021MstpComponentId,
                                  UINT4 u4Ieee8021MstpId,
                                  INT4 i4SetValIeee8021MstpBridgePriority)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    return (nmhSetFsMIMstMstiBridgePriority ((INT4) u4Ieee8021MstpComponentId,
                                             (INT4) u4Ieee8021MstpId,
                                             i4SetValIeee8021MstpBridgePriority));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpRowStatus
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                setValIeee8021MstpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpRowStatus (UINT4 u4Ieee8021MstpComponentId,
                             UINT4 u4Ieee8021MstpId,
                             INT4 i4SetValIeee8021MstpRowStatus)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tSNMP_OCTET_STRING_TYPE MstVlanListAll;
    UINT4               u4ErrCode = 0;
    INT4                i4IsResult = MST_FALSE;
    UINT1               au1TmpAll[MST_VLAN_LIST_SIZE];

    MEMSET (au1TmpAll, 0, MST_VLAN_LIST_SIZE);

    MstVlanListAll.pu1_OctetList = &au1TmpAll[0];
    MstVlanListAll.i4_Length = MST_VLAN_LIST_SIZE;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);
    if (AstSelectContext (u4Ieee8021MstpComponentId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);

    if ((i4SetValIeee8021MstpRowStatus == CREATE_AND_WAIT) ||
        (i4SetValIeee8021MstpRowStatus == CREATE_AND_GO))
    {
        if (pPerStInfo != NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4SetValIeee8021MstpRowStatus != ACTIVE)
        {
            if (pPerStInfo == NULL)
            {
                AstReleaseContext ();
                return SNMP_FAILURE;
            }
        }
    }

    switch (i4SetValIeee8021MstpRowStatus)
    {
        case CREATE_AND_WAIT:
            if (MstInitInstance (u4Ieee8021MstpId) != MST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_ALL_FAILURE_TRC | AST_CONTROL_PATH_TRC |
                              AST_OS_RESOURCE_TRC,
                              "SYS: Creation of Spanning tree MST Instance %d"
                              " failed !!!\n", u4Ieee8021MstpId);
                AST_DBG_ARG1 (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG |
                              AST_INIT_SHUT_DBG,
                              "SYS: Creation of Spanning Tree MST Instance %d"
                              " failed !!!\n", u4Ieee8021MstpId);
                AstReleaseContext ();
                return SNMP_FAILURE;
            }
            pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);
            pPerStInfo->u1RowStatus = NOT_READY;

            break;

        case CREATE_AND_GO:
            /* Not supported. VLAN mapping is mandatory for isntance creation */
            AstReleaseContext ();
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            pPerStInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            if (pPerStInfo->u1RowStatus == NOT_READY)
            {
                AstL2IwfMiGetVlanMapInInstance (u4Ieee8021MstpComponentId,
                                                u4Ieee8021MstpId, au1TmpAll);
                /* Check if the Vlan lists are non empty */
                IS_MST_VLANLIST_NONEMPTY (au1TmpAll, i4IsResult);
                if (i4IsResult)
                {
                    if (nmhSetFsMstSetVlanList
                        (u4Ieee8021MstpId, &MstVlanListAll) == SNMP_FAILURE)
                    {
                        AstReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);
                    pPerStInfo->u1RowStatus = ACTIVE;
                }
                else
                {
                    AstReleaseContext ();
                    return SNMP_FAILURE;
                }
            }
            pPerStInfo->u1RowStatus = ACTIVE;
            break;

        case DESTROY:

            /* For Instnce TE_MSTID */
            if (u4Ieee8021MstpId == AST_TE_MSTID)
            {
                AstReleaseContext ();
                return SNMP_SUCCESS;
            }
            /* Delete the Instance */
            /* Get the VLAN mappings and reset all the vlan mapped
             * to this instance
             */
            AstL2IwfMiGetVlanMapInInstance (u4Ieee8021MstpComponentId,
                                            u4Ieee8021MstpId, au1TmpAll);
            /* If there is no vlan mapped to this specific instance,
             * reset vlan list need not called. In this case, the instance
             * can be directly deleted */
            /* Check if the Vlan lists are non empty */
            IS_MST_VLANLIST_NONEMPTY (au1TmpAll, i4IsResult);
            if (i4IsResult)
            {
                if (nmhTestv2FsMstResetVlanList
                    (&u4ErrCode, u4Ieee8021MstpId, &MstVlanListAll)
                    == SNMP_FAILURE)
                {
                    AstReleaseContext ();
                    return (SNMP_FAILURE);

                }

                if (nmhSetFsMstResetVlanList (u4Ieee8021MstpId,
                                              &MstVlanListAll) == SNMP_FAILURE)
                {
                    AstReleaseContext ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* If there is null vlan list mapped to a specific instance,
                 * reset vlan list need not called. Instead, that instance
                 * can be deleted directly */

                if (MstDeleteInstance ((UINT2) u4Ieee8021MstpId) != MST_SUCCESS)
                {
                    AST_TRC_ARG1 (AST_ALL_FAILURE_TRC |
                                  AST_CONTROL_PATH_TRC,
                                  "API:  instance %d deletion failed.. \n",
                                  u4Ieee8021MstpId);
                    AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                  AST_EVENT_HANDLING_DBG,
                                  "API: instance %d deletion failed.. \n",
                                  u4Ieee8021MstpId);
                    AstReleaseContext ();
                    return (SNMP_FAILURE);
                }
            }

            break;

        default:
            break;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpBridgePriority
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                testValIeee8021MstpBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpBridgePriority (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021MstpComponentId,
                                     UINT4 u4Ieee8021MstpId,
                                     INT4 i4TestValIeee8021MstpBridgePriority)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);
    if ((pPerStInfo->u1RowStatus == NOT_IN_SERVICE) ||
        (pPerStInfo->u1RowStatus == NOT_READY))
    {
        return (nmhTestv2FsMIMstMstiBridgePriority (pu4ErrorCode,
                                                    (INT4)
                                                    u4Ieee8021MstpComponentId,
                                                    (INT4) u4Ieee8021MstpId,
                                                    i4TestValIeee8021MstpBridgePriority));
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpRowStatus
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId

                The Object 
                testValIeee8021MstpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpRowStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4Ieee8021MstpComponentId,
                                UINT4 u4Ieee8021MstpId,
                                INT4 i4TestValIeee8021MstpRowStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    tAstPerStInfo      *pPerStInfo = NULL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpComponentId);

    if (AstSelectContext (u4Ieee8021MstpComponentId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP force version failed!\n");
        return SNMP_FAILURE;
    }

    if (MST_IS_INSTANCE_VALID (u4Ieee8021MstpId) == MST_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (AST_IS_VC_VALID ((INT4) u4Ieee8021MstpComponentId) != RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021MstpRowStatus < ACTIVE) ||
        (i4TestValIeee8021MstpRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pPerStInfo = AST_GET_PERST_INFO (u4Ieee8021MstpId);

    switch (i4TestValIeee8021MstpRowStatus)
    {
        case ACTIVE:
            if (pPerStInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            else if (pPerStInfo->u1RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
            if (pPerStInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            if (pPerStInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case CREATE_AND_WAIT:
            if (pPerStInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pPerStInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetVal = SNMP_FAILURE;
            }
            break;
    }

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpTable
 Input       :  The Indices
                Ieee8021MstpComponentId
                Ieee8021MstpId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpCistPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpCistPortTable
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpCistPortTable (UINT4
                                                   u4Ieee8021MstpCistPortComponentId,
                                                   UINT4
                                                   u4Ieee8021MstpCistPortNum)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceFsMIMstCistPortTable ((INT4)
                                                          u4Ieee8021MstpCistPortNum));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpCistPortTable
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpCistPortTable (UINT4
                                           *pu4Ieee8021MstpCistPortComponentId,
                                           UINT4 *pu4Ieee8021MstpCistPortNum)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4Retval = RST_FAILURE;

    if ((nmhGetFirstIndexFsMIMstCistPortTable ((INT4 *)
                                               pu4Ieee8021MstpCistPortNum)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Retval = AstGetContextInfoFromIfIndex (*pu4Ieee8021MstpCistPortNum,
                                             pu4Ieee8021MstpCistPortComponentId,
                                             &u2LocalPortId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021MstpCistPortComponentId));

    UNUSED_PARAM (i4Retval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpCistPortTable
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                nextIeee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum
                nextIeee8021MstpCistPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpCistPortTable (UINT4
                                          u4Ieee8021MstpCistPortComponentId,
                                          UINT4
                                          *pu4NextIeee8021MstpCistPortComponentId,
                                          UINT4 u4Ieee8021MstpCistPortNum,
                                          UINT4 *pu4NextIeee8021MstpCistPortNum)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4Retval = RST_FAILURE;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexFsMIMstCistPortTable ((INT4) u4Ieee8021MstpCistPortNum,
                                              (INT4 *)
                                              pu4NextIeee8021MstpCistPortNum))
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Retval = AstGetContextInfoFromIfIndex (*pu4NextIeee8021MstpCistPortNum,
                                             pu4NextIeee8021MstpCistPortComponentId,
                                             &u2LocalPortId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021MstpCistPortComponentId));

    UNUSED_PARAM (i4Retval);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortUptime
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortUptime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortUptime (UINT4 u4Ieee8021MstpCistPortComponentId,
                                  UINT4 u4Ieee8021MstpCistPortNum,
                                  UINT4 *pu4RetValIeee8021MstpCistPortUptime)
{
    UINT4               u4PortUpTime = 0;
    INT1                i1RetVal = 0;
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    i1RetVal = nmhGetFsMIMstCistPortRcvdEventTimeStamp ((INT4)
                                                        u4Ieee8021MstpCistPortNum,
                                                        &u4PortUpTime);

    /* As per the definition of ieee8021MstpCistPortUptime, it  requires the value to be returned in units of centiseconds.
     * But nmhGetFsMIMstCistPortRcvdEventTimeStamp gives the value in units of milliseconds.
     * So the value is converted from milliseconds to centiseconds */

    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4RetValIeee8021MstpCistPortUptime = AST_MS_TO_CS (u4PortUpTime);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortAdminPathCost
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortAdminPathCost (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         *pi4RetValIeee8021MstpCistPortAdminPathCost)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortAdminPathCost ((INT4)
                                                u4Ieee8021MstpCistPortNum,
                                                pi4RetValIeee8021MstpCistPortAdminPathCost));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortDesignatedRoot
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortDesignatedRoot (UINT4
                                          u4Ieee8021MstpCistPortComponentId,
                                          UINT4 u4Ieee8021MstpCistPortNum,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIeee8021MstpCistPortDesignatedRoot)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortDesignatedRoot ((INT4)
                                                 u4Ieee8021MstpCistPortNum,
                                                 pRetValIeee8021MstpCistPortDesignatedRoot));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortTopologyChangeAck
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortTopologyChangeAck
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortTopologyChangeAck (UINT4
                                             u4Ieee8021MstpCistPortComponentId,
                                             UINT4 u4Ieee8021MstpCistPortNum,
                                             INT4
                                             *pi4RetValIeee8021MstpCistPortTopologyChangeAck)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistPortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValIeee8021MstpCistPortTopologyChangeAck = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry ((INT4) u4Ieee8021MstpCistPortNum) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    pAstCommPortInfo =
        AST_GET_COMM_PORT_INFO ((UINT2) u4Ieee8021MstpCistPortNum);
    if (pAstCommPortInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    else
    {
        if (pAstCommPortInfo->bRcvdTcAck == MST_TRUE)
        {
            *pi4RetValIeee8021MstpCistPortTopologyChangeAck = AST_SNMP_TRUE;
        }
        else
        {
            *pi4RetValIeee8021MstpCistPortTopologyChangeAck = AST_SNMP_FALSE;
        }
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortHelloTime
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortHelloTime (UINT4 u4Ieee8021MstpCistPortComponentId,
                                     UINT4 u4Ieee8021MstpCistPortNum,
                                     INT4
                                     *pi4RetValIeee8021MstpCistPortHelloTime)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortHelloTime ((INT4) u4Ieee8021MstpCistPortNum,
                                            pi4RetValIeee8021MstpCistPortHelloTime));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortAdminEdgePort
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortAdminEdgePort (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         *pi4RetValIeee8021MstpCistPortAdminEdgePort)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    /* As per the IEEE MIB definition , the default value expected by ieee8021MstpCistPortAdminEdgePort is True. 
     * But this change is not done  :
     * to maintain backward compatibility with existing code (In existing code default value is false.) */

    return (nmhGetFsMIMstCistPortAdminEdgeStatus ((INT4)
                                                  u4Ieee8021MstpCistPortNum,
                                                  pi4RetValIeee8021MstpCistPortAdminEdgePort));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortOperEdgePort
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortOperEdgePort (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        INT4
                                        *pi4RetValIeee8021MstpCistPortOperEdgePort)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortOperEdgeStatus ((INT4)
                                                 u4Ieee8021MstpCistPortNum,
                                                 pi4RetValIeee8021MstpCistPortOperEdgePort));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortMacEnabled
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortMacEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortMacEnabled (UINT4 u4Ieee8021MstpCistPortComponentId,
                                      UINT4 u4Ieee8021MstpCistPortNum,
                                      INT4
                                      *pi4RetValIeee8021MstpCistPortMacEnabled)
{

    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistPortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValIeee8021MstpCistPortMacEnabled = AST_INIT_VAL;
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4Ieee8021MstpCistPortNum,
                                      &u4ContextId,
                                      &u2LocalPort) == MST_SUCCESS)
    {
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2LocalPort);

        if (pAstCommPortInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4RetValIeee8021MstpCistPortMacEnabled =
            (UINT4) pAstCommPortInfo->bCistPortMacEnabled;
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortMacOperational
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortMacOperational
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortMacOperational (UINT4
                                          u4Ieee8021MstpCistPortComponentId,
                                          UINT4 u4Ieee8021MstpCistPortNum,
                                          INT4
                                          *pi4RetValIeee8021MstpCistPortMacOperational)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistPortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValIeee8021MstpCistPortMacOperational = AST_INIT_VAL;
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4Ieee8021MstpCistPortNum,
                                      &u4ContextId,
                                      &u2LocalPort) == MST_SUCCESS)
    {
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2LocalPort);

        if (pAstCommPortInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4RetValIeee8021MstpCistPortMacOperational =
            (UINT4) pAstCommPortInfo->bCistPortMacOperational;
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;

    }
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortRestrictedRole
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortRestrictedRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortRestrictedRole (UINT4
                                          u4Ieee8021MstpCistPortComponentId,
                                          UINT4 u4Ieee8021MstpCistPortNum,
                                          INT4
                                          *pi4RetValIeee8021MstpCistPortRestrictedRole)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortRestrictedRole ((INT4)
                                                 u4Ieee8021MstpCistPortNum,
                                                 pi4RetValIeee8021MstpCistPortRestrictedRole));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortRestrictedTcn
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortRestrictedTcn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortRestrictedTcn (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         *pi4RetValIeee8021MstpCistPortRestrictedTcn)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortRestrictedTCN ((INT4)
                                                u4Ieee8021MstpCistPortNum,
                                                pi4RetValIeee8021MstpCistPortRestrictedTcn));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortRole
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortRole (UINT4 u4Ieee8021MstpCistPortComponentId,
                                UINT4 u4Ieee8021MstpCistPortNum,
                                INT4 *pi4RetValIeee8021MstpCistPortRole)
{
    INT4                i4PortRole = 0;
    INT1                i1RetVal = 0;

    /* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.
     * The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    i1RetVal =
        nmhGetFsMIMstCistCurrentPortRole ((INT4) u4Ieee8021MstpCistPortNum,
                                          &i4PortRole);

    /* As per the definition of  ieee8021MstpCistPortRole, the values required to be returned  are different 
     * from the values returned by nmhGetFsMIMstCistCurrentPortRole. So Appropriately mapped values
     *  are returned to the user */

    if (i1RetVal == SNMP_SUCCESS)
    {
        switch (i4PortRole)
        {
            case AST_PORT_ROLE_ROOT:
                *pi4RetValIeee8021MstpCistPortRole = AST_PORT_ROLE_IEEE_ROOT;
                break;

            case AST_PORT_ROLE_ALTERNATE:
                *pi4RetValIeee8021MstpCistPortRole =
                    AST_PORT_ROLE_IEEE_ALTERNATE;
                break;

            case AST_PORT_ROLE_DESIGNATED:
                *pi4RetValIeee8021MstpCistPortRole =
                    AST_PORT_ROLE_IEEE_DESIGNATED;
                break;

            case AST_PORT_ROLE_BACKUP:
                *pi4RetValIeee8021MstpCistPortRole = AST_PORT_ROLE_IEEE_BACKUP;
                break;

            default:
                break;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortDisputed
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortDisputed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortDisputed (UINT4 u4Ieee8021MstpCistPortComponentId,
                                    UINT4 u4Ieee8021MstpCistPortNum,
                                    INT4 *pi4RetValIeee8021MstpCistPortDisputed)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstSelectContext (u4Ieee8021MstpCistPortComponentId)) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValIeee8021MstpCistPortDisputed = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry ((INT4) u4Ieee8021MstpCistPortNum) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO ((UINT2) u4Ieee8021MstpCistPortNum,
                                              MST_CIST_CONTEXT);
    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    else
    {
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        if (pRstPortInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
        else
        {
            if (pRstPortInfo->bDisputed == MST_TRUE)
            {
                *pi4RetValIeee8021MstpCistPortDisputed = AST_SNMP_TRUE;
            }
            else
            {
                *pi4RetValIeee8021MstpCistPortDisputed = AST_SNMP_FALSE;
            }
        }
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortCistRegionalRootId
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortCistRegionalRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortCistRegionalRootId (UINT4
                                              u4Ieee8021MstpCistPortComponentId,
                                              UINT4 u4Ieee8021MstpCistPortNum,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021MstpCistPortCistRegionalRootId)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortRegionalRoot ((INT4) u4Ieee8021MstpCistPortNum,
                                               pRetValIeee8021MstpCistPortCistRegionalRootId));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortCistPathCost
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortCistPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortCistPathCost (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        UINT4
                                        *pu4RetValIeee8021MstpCistPortCistPathCost)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortRegionalPathCost ((INT4)
                                                   u4Ieee8021MstpCistPortNum,
                                                   (INT4 *)
                                                   pu4RetValIeee8021MstpCistPortCistPathCost));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortProtocolMigration
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortProtocolMigration (UINT4
                                             u4Ieee8021MstpCistPortComponentId,
                                             UINT4 u4Ieee8021MstpCistPortNum,
                                             INT4
                                             *pi4RetValIeee8021MstpCistPortProtocolMigration)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortProtocolMigration ((INT4)
                                                    u4Ieee8021MstpCistPortNum,
                                                    pi4RetValIeee8021MstpCistPortProtocolMigration));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortEnableBPDURx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortEnableBPDURx (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        INT4
                                        *pi4RetValIeee8021MstpCistPortEnableBPDURx)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    /* The default value expected by ieee8021MstpCistPortEnableBPDURx is False. But this change is not done for 2 reasons :
     * (1)  If packets are ignored during reception all the nodes will be Root and the ports will be 
     * in forwarding state. This will cause a loop. 
     * (2) To maintain backward compatibility with existing code - In existing code default value is true. */

    return (nmhGetFsMIMstCistPortEnableBPDURx ((INT4) u4Ieee8021MstpCistPortNum,
                                               pi4RetValIeee8021MstpCistPortEnableBPDURx));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortEnableBPDUTx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortEnableBPDUTx (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        INT4
                                        *pi4RetValIeee8021MstpCistPortEnableBPDUTx)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    /* The default value expected by ieee8021MstpCistPortEnableBPDUTx is False. But this change is not done for 2 reasons :
     * (1)  If packets are not transmitted until the value is changed to true by user, then  all the nodes will be Root and the ports will be 
     * in forwarding state. This will cause a loop. 
     * (2) To maintain backward compatibility with existing code - In existing code default value is true. */

    return (nmhGetFsMIMstCistPortEnableBPDUTx ((INT4) u4Ieee8021MstpCistPortNum,
                                               pi4RetValIeee8021MstpCistPortEnableBPDUTx));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortPseudoRootId
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortPseudoRootId (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValIeee8021MstpCistPortPseudoRootId)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortPseudoRootId ((INT4) u4Ieee8021MstpCistPortNum,
                                               pRetValIeee8021MstpCistPortPseudoRootId));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpCistPortIsL2Gp
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                retValIeee8021MstpCistPortIsL2Gp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpCistPortIsL2Gp (UINT4 u4Ieee8021MstpCistPortComponentId,
                                  UINT4 u4Ieee8021MstpCistPortNum,
                                  INT4 *pi4RetValIeee8021MstpCistPortIsL2Gp)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhGetFsMIMstCistPortIsL2Gp ((INT4) u4Ieee8021MstpCistPortNum,
                                         pi4RetValIeee8021MstpCistPortIsL2Gp));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortAdminPathCost
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortAdminPathCost (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         i4SetValIeee8021MstpCistPortAdminPathCost)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortAdminPathCost ((INT4)
                                                u4Ieee8021MstpCistPortNum,
                                                i4SetValIeee8021MstpCistPortAdminPathCost));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortAdminEdgePort
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortAdminEdgePort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortAdminEdgePort (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         i4SetValIeee8021MstpCistPortAdminEdgePort)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortAdminEdgeStatus ((INT4)
                                                  u4Ieee8021MstpCistPortNum,
                                                  i4SetValIeee8021MstpCistPortAdminEdgePort));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortMacEnabled
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortMacEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortMacEnabled (UINT4 u4Ieee8021MstpCistPortComponentId,
                                      UINT4 u4Ieee8021MstpCistPortNum,
                                      INT4
                                      i4SetValIeee8021MstpCistPortMacEnabled)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPort = AST_INIT_VAL;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if (AstSelectContext (u4Ieee8021MstpCistPortComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (AstGetContextInfoFromIfIndex (u4Ieee8021MstpCistPortNum,
                                      &u4ContextId,
                                      &u2LocalPort) == MST_SUCCESS)
    {
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2LocalPort);

        if (pAstCommPortInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        pAstCommPortInfo->bCistPortMacEnabled =
            i4SetValIeee8021MstpCistPortMacEnabled;

        /* Configuring MAC_Enabled parameter affects the MAC_Operational parameter value */
        pAstPortEntry = AST_GET_PORTENTRY (u2LocalPort);
        if (pAstPortEntry == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        if ((i4SetValIeee8021MstpCistPortMacEnabled == AST_SNMP_TRUE) &&
            (pAstPortEntry->bOperPointToPoint == RST_TRUE))
        {
            pAstCommPortInfo->bCistPortMacOperational = AST_SNMP_TRUE;
        }
        else
        {
            pAstCommPortInfo->bCistPortMacOperational = AST_SNMP_FALSE;
        }
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortRestrictedRole
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortRestrictedRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortRestrictedRole (UINT4
                                          u4Ieee8021MstpCistPortComponentId,
                                          UINT4 u4Ieee8021MstpCistPortNum,
                                          INT4
                                          i4SetValIeee8021MstpCistPortRestrictedRole)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortRestrictedRole ((INT4)
                                                 u4Ieee8021MstpCistPortNum,
                                                 i4SetValIeee8021MstpCistPortRestrictedRole));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortRestrictedTcn
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortRestrictedTcn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortRestrictedTcn (UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         i4SetValIeee8021MstpCistPortRestrictedTcn)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortRestrictedTCN ((INT4)
                                                u4Ieee8021MstpCistPortNum,
                                                i4SetValIeee8021MstpCistPortRestrictedTcn));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortProtocolMigration
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortProtocolMigration (UINT4
                                             u4Ieee8021MstpCistPortComponentId,
                                             UINT4 u4Ieee8021MstpCistPortNum,
                                             INT4
                                             i4SetValIeee8021MstpCistPortProtocolMigration)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortProtocolMigration ((INT4)
                                                    u4Ieee8021MstpCistPortNum,
                                                    i4SetValIeee8021MstpCistPortProtocolMigration));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortEnableBPDURx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortEnableBPDURx (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        INT4
                                        i4SetValIeee8021MstpCistPortEnableBPDURx)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortEnableBPDURx ((INT4) u4Ieee8021MstpCistPortNum,
                                               i4SetValIeee8021MstpCistPortEnableBPDURx));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortEnableBPDUTx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortEnableBPDUTx (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        INT4
                                        i4SetValIeee8021MstpCistPortEnableBPDUTx)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortEnableBPDUTx ((INT4) u4Ieee8021MstpCistPortNum,
                                               i4SetValIeee8021MstpCistPortEnableBPDUTx));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortPseudoRootId
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortPseudoRootId (UINT4 u4Ieee8021MstpCistPortComponentId,
                                        UINT4 u4Ieee8021MstpCistPortNum,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValIeee8021MstpCistPortPseudoRootId)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortPseudoRootId ((INT4) u4Ieee8021MstpCistPortNum,
                                               pSetValIeee8021MstpCistPortPseudoRootId));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpCistPortIsL2Gp
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                setValIeee8021MstpCistPortIsL2Gp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpCistPortIsL2Gp (UINT4 u4Ieee8021MstpCistPortComponentId,
                                  UINT4 u4Ieee8021MstpCistPortNum,
                                  INT4 i4SetValIeee8021MstpCistPortIsL2Gp)
{
/* The MstpCistPortComponentId is not needed in this table because MstpCistPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine.*/
    UNUSED_PARAM (u4Ieee8021MstpCistPortComponentId);

    return (nmhSetFsMIMstCistPortIsL2Gp ((INT4) u4Ieee8021MstpCistPortNum,
                                         i4SetValIeee8021MstpCistPortIsL2Gp));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortAdminPathCost
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortAdminPathCost (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021MstpCistPortComponentId,
                                            UINT4 u4Ieee8021MstpCistPortNum,
                                            INT4
                                            i4TestValIeee8021MstpCistPortAdminPathCost)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortAdminPathCost (pu4ErrorCode,
                                                   (INT4)
                                                   u4Ieee8021MstpCistPortNum,
                                                   i4TestValIeee8021MstpCistPortAdminPathCost));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortAdminEdgePort
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortAdminEdgePort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortAdminEdgePort (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021MstpCistPortComponentId,
                                            UINT4 u4Ieee8021MstpCistPortNum,
                                            INT4
                                            i4TestValIeee8021MstpCistPortAdminEdgePort)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortAdminEdgeStatus (pu4ErrorCode,
                                                     (INT4)
                                                     u4Ieee8021MstpCistPortNum,
                                                     i4TestValIeee8021MstpCistPortAdminEdgePort));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortMacEnabled
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortMacEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortMacEnabled (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021MstpCistPortComponentId,
                                         UINT4 u4Ieee8021MstpCistPortNum,
                                         INT4
                                         i4TestValIeee8021MstpCistPortMacEnabled)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (AstGetContextInfoFromIfIndex (u4Ieee8021MstpCistPortNum,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (u4Ieee8021MstpCistPortNum) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Validation for Input Values of ieee8021MstpCistPortMacEnabled 
     * as defined in IEEE standard mib - std1s1ap.mib */

    if ((i4TestValIeee8021MstpCistPortMacEnabled != AST_SNMP_TRUE) &&
        (i4TestValIeee8021MstpCistPortMacEnabled != AST_SNMP_FALSE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
    if (pAstPortEntry != NULL)
    {
        if ((i4TestValIeee8021MstpCistPortMacEnabled == AST_SNMP_TRUE) &&
            (pAstPortEntry->u1AdminPointToPoint != RST_P2P_FORCETRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

        if ((i4TestValIeee8021MstpCistPortMacEnabled == AST_SNMP_FALSE) &&
            (pAstPortEntry->u1AdminPointToPoint != RST_P2P_FORCEFALSE) &&
            (pAstPortEntry->u1AdminPointToPoint != RST_P2P_AUTO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            AstReleaseContext ();
            return SNMP_FAILURE;
        }

    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortRestrictedRole
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortRestrictedRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortRestrictedRole (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021MstpCistPortComponentId,
                                             UINT4 u4Ieee8021MstpCistPortNum,
                                             INT4
                                             i4TestValIeee8021MstpCistPortRestrictedRole)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);
    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortRestrictedRole (pu4ErrorCode,
                                                    (INT4)
                                                    u4Ieee8021MstpCistPortNum,
                                                    i4TestValIeee8021MstpCistPortRestrictedRole));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortRestrictedTcn
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortRestrictedTcn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortRestrictedTcn (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021MstpCistPortComponentId,
                                            UINT4 u4Ieee8021MstpCistPortNum,
                                            INT4
                                            i4TestValIeee8021MstpCistPortRestrictedTcn)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortRestrictedTCN (pu4ErrorCode,
                                                   (INT4)
                                                   u4Ieee8021MstpCistPortNum,
                                                   i4TestValIeee8021MstpCistPortRestrictedTcn));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortProtocolMigration
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortProtocolMigration (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021MstpCistPortComponentId,
                                                UINT4 u4Ieee8021MstpCistPortNum,
                                                INT4
                                                i4TestValIeee8021MstpCistPortProtocolMigration)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortProtocolMigration (pu4ErrorCode,
                                                       (INT4)
                                                       u4Ieee8021MstpCistPortNum,
                                                       i4TestValIeee8021MstpCistPortProtocolMigration));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortEnableBPDURx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortEnableBPDURx (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021MstpCistPortComponentId,
                                           UINT4 u4Ieee8021MstpCistPortNum,
                                           INT4
                                           i4TestValIeee8021MstpCistPortEnableBPDURx)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortEnableBPDURx (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021MstpCistPortNum,
                                                  i4TestValIeee8021MstpCistPortEnableBPDURx));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortEnableBPDUTx
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortEnableBPDUTx (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021MstpCistPortComponentId,
                                           UINT4 u4Ieee8021MstpCistPortNum,
                                           INT4
                                           i4TestValIeee8021MstpCistPortEnableBPDUTx)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortEnableBPDUTx (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021MstpCistPortNum,
                                                  i4TestValIeee8021MstpCistPortEnableBPDUTx));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortPseudoRootId
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortPseudoRootId (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021MstpCistPortComponentId,
                                           UINT4 u4Ieee8021MstpCistPortNum,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValIeee8021MstpCistPortPseudoRootId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortPseudoRootId (pu4ErrorCode,
                                                  (INT4)
                                                  u4Ieee8021MstpCistPortNum,
                                                  pTestValIeee8021MstpCistPortPseudoRootId));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpCistPortIsL2Gp
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum

                The Object 
                testValIeee8021MstpCistPortIsL2Gp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpCistPortIsL2Gp (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021MstpCistPortComponentId,
                                     UINT4 u4Ieee8021MstpCistPortNum,
                                     INT4 i4TestValIeee8021MstpCistPortIsL2Gp)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpCistPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpCistPortComponentId,
                                 u4Ieee8021MstpCistPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstCistPortIsL2Gp (pu4ErrorCode,
                                            (INT4) u4Ieee8021MstpCistPortNum,
                                            i4TestValIeee8021MstpCistPortIsL2Gp));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpCistPortTable
 Input       :  The Indices
                Ieee8021MstpCistPortComponentId
                Ieee8021MstpCistPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpCistPortTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpPortTable
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpPortTable (UINT4
                                               u4Ieee8021MstpPortComponentId,
                                               UINT4 u4Ieee8021MstpPortMstId,
                                               UINT4 u4Ieee8021MstpPortNum)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpPortComponentId,
                                 u4Ieee8021MstpPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceFsMIMstMstiPortTable ((INT4)
                                                          u4Ieee8021MstpPortNum,
                                                          (INT4)
                                                          u4Ieee8021MstpPortMstId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpPortTable
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpPortTable (UINT4 *pu4Ieee8021MstpPortComponentId,
                                       UINT4 *pu4Ieee8021MstpPortMstId,
                                       UINT4 *pu4Ieee8021MstpPortNum)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4Retval = RST_FAILURE;

    if ((nmhGetFirstIndexFsMIMstMstiPortTable ((INT4 *) pu4Ieee8021MstpPortNum,
                                               (INT4 *)
                                               pu4Ieee8021MstpPortMstId)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Retval = AstGetContextInfoFromIfIndex (*pu4Ieee8021MstpPortNum,
                                             pu4Ieee8021MstpPortComponentId,
                                             &u2LocalPortId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021MstpPortComponentId));

    UNUSED_PARAM (i4Retval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpPortTable
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                nextIeee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                nextIeee8021MstpPortMstId
                Ieee8021MstpPortNum
                nextIeee8021MstpPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpPortTable (UINT4 u4Ieee8021MstpPortComponentId,
                                      UINT4 *pu4NextIeee8021MstpPortComponentId,
                                      UINT4 u4Ieee8021MstpPortMstId,
                                      UINT4 *pu4NextIeee8021MstpPortMstId,
                                      UINT4 u4Ieee8021MstpPortNum,
                                      UINT4 *pu4NextIeee8021MstpPortNum)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4Retval = RST_FAILURE;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpPortComponentId,
                                 u4Ieee8021MstpPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((nmhGetNextIndexFsMIMstMstiPortTable ((INT4) u4Ieee8021MstpPortNum,
                                              (INT4 *)
                                              pu4NextIeee8021MstpPortNum,
                                              (INT4) u4Ieee8021MstpPortMstId,
                                              (INT4 *)
                                              pu4NextIeee8021MstpPortMstId)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Retval = AstGetContextInfoFromIfIndex (*pu4NextIeee8021MstpPortNum,
                                             pu4NextIeee8021MstpPortComponentId,
                                             &u2LocalPortId);
    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021MstpPortComponentId));

    UNUSED_PARAM (i4Retval);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortUptime
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortUptime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortUptime (UINT4 u4Ieee8021MstpPortComponentId,
                              UINT4 u4Ieee8021MstpPortMstId,
                              UINT4 u4Ieee8021MstpPortNum,
                              UINT4 *pu4RetValIeee8021MstpPortUptime)
{
    UINT4               u4PortUpTime = 0;
    INT4                i4EventSubType = 0;
    INT4                i4RetVal = 0;
    INT4                i4ReturnVal = 0;

    /* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.
     * The validation of MstpCistPortComponentId is done in the Validate routine.*/

    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);
    UNUSED_PARAM (u4Ieee8021MstpPortMstId);    /* MstpPortMstId is also not needed */

    /* Get the fsMIMstCistPortRcvdEventSubType ; If it is PortUp Event , return the timestamp 
     * of that event;
     * Else return Failure */
    i4RetVal =
        nmhGetFsMIMstCistPortRcvdEventSubType ((INT4) u4Ieee8021MstpPortNum,
                                               &i4EventSubType);

    if ((i4RetVal == SNMP_SUCCESS) && (i4EventSubType == 1))
    {
        i4ReturnVal = (nmhGetFsMIMstCistPortRcvdEventTimeStamp ((INT4)
                                                                u4Ieee8021MstpPortNum,
                                                                &u4PortUpTime));
        if (i4ReturnVal == SNMP_SUCCESS)
        {
            *pu4RetValIeee8021MstpPortUptime = AST_MS_TO_CS (u4PortUpTime);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortState
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortState (UINT4 u4Ieee8021MstpPortComponentId,
                             UINT4 u4Ieee8021MstpPortMstId,
                             UINT4 u4Ieee8021MstpPortNum,
                             INT4 *pi4RetValIeee8021MstpPortState)
{
    INT4                i4PortState = 0;
    INT1                i1RetVal = 0;

    /* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.
     * The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    i1RetVal = nmhGetFsMIMstMstiPortState ((INT4) u4Ieee8021MstpPortNum,
                                           (INT4) u4Ieee8021MstpPortMstId,
                                           &i4PortState);

    /* Values defined in ieee8021MstpPortState are different from the values
     * returned by nmhGetFsMIMstMstiPortState. So Appropriately mapped values are returned 
     * to the user 
     * Blocking is mapped to discarding ; Listening (not being a MSTP port state) is not 
     * mapped*/
    if (i1RetVal == SNMP_SUCCESS)
    {
        switch (i4PortState)
        {
            case AST_PORT_STATE_DISABLED:
                *pi4RetValIeee8021MstpPortState = AST_PORT_STATE_IEEE_DISABLED;
                break;

            case AST_PORT_STATE_DISCARDING:
                *pi4RetValIeee8021MstpPortState = AST_PORT_STATE_IEEE_BLOCKING;
                break;

            case AST_PORT_STATE_LEARNING:
                *pi4RetValIeee8021MstpPortState = AST_PORT_STATE_IEEE_LEARNING;
                break;

            case AST_PORT_STATE_FORWARDING:
                *pi4RetValIeee8021MstpPortState =
                    AST_PORT_STATE_IEEE_FORWARDING;
                break;

            default:
                break;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortPriority
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortPriority (UINT4 u4Ieee8021MstpPortComponentId,
                                UINT4 u4Ieee8021MstpPortMstId,
                                UINT4 u4Ieee8021MstpPortNum,
                                INT4 *pi4RetValIeee8021MstpPortPriority)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhGetFsMIMstMstiPortPriority ((INT4) u4Ieee8021MstpPortNum,
                                           (INT4) u4Ieee8021MstpPortMstId,
                                           pi4RetValIeee8021MstpPortPriority));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortPathCost
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortPathCost (UINT4 u4Ieee8021MstpPortComponentId,
                                UINT4 u4Ieee8021MstpPortMstId,
                                UINT4 u4Ieee8021MstpPortNum,
                                INT4 *pi4RetValIeee8021MstpPortPathCost)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhGetFsMIMstMstiPortPathCost ((INT4) u4Ieee8021MstpPortNum,
                                           (INT4) u4Ieee8021MstpPortMstId,
                                           pi4RetValIeee8021MstpPortPathCost));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortDesignatedRoot
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortDesignatedRoot (UINT4 u4Ieee8021MstpPortComponentId,
                                      UINT4 u4Ieee8021MstpPortMstId,
                                      UINT4 u4Ieee8021MstpPortNum,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValIeee8021MstpPortDesignatedRoot)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhGetFsMIMstMstiPortDesignatedRoot ((INT4) u4Ieee8021MstpPortNum,
                                                 (INT4) u4Ieee8021MstpPortMstId,
                                                 pRetValIeee8021MstpPortDesignatedRoot));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortDesignatedCost
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortDesignatedCost (UINT4 u4Ieee8021MstpPortComponentId,
                                      UINT4 u4Ieee8021MstpPortMstId,
                                      UINT4 u4Ieee8021MstpPortNum,
                                      INT4
                                      *pi4RetValIeee8021MstpPortDesignatedCost)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhGetFsMIMstMstiPortDesignatedCost ((INT4) u4Ieee8021MstpPortNum,
                                                 (INT4) u4Ieee8021MstpPortMstId,
                                                 pi4RetValIeee8021MstpPortDesignatedCost));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortDesignatedBridge
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortDesignatedBridge (UINT4 u4Ieee8021MstpPortComponentId,
                                        UINT4 u4Ieee8021MstpPortMstId,
                                        UINT4 u4Ieee8021MstpPortNum,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValIeee8021MstpPortDesignatedBridge)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhGetFsMIMstMstiPortDesignatedBridge ((INT4) u4Ieee8021MstpPortNum,
                                                   (INT4)
                                                   u4Ieee8021MstpPortMstId,
                                                   pRetValIeee8021MstpPortDesignatedBridge));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortDesignatedPort
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortDesignatedPort (UINT4 u4Ieee8021MstpPortComponentId,
                                      UINT4 u4Ieee8021MstpPortMstId,
                                      UINT4 u4Ieee8021MstpPortNum,
                                      UINT4
                                      *pu4RetValIeee8021MstpPortDesignatedPort)
{
    tSNMP_OCTET_STRING_TYPE pRetVal;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT2               u2Val = 0;
    UINT1               au1DesigPortVal[MSTP_STD_SQUAD];
    UINT1              *pu1List = NULL;
    INT1                i1RetVal = 0;

    MEMSET (au1DesigPortVal, 0, sizeof (au1DesigPortVal));
    pRetVal.i4_Length = MSTP_STD_SQUAD;
    pRetVal.pu1_OctetList = au1DesigPortVal;

/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    if (AstGetContextInfoFromIfIndex (u4Ieee8021MstpPortNum,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortDesignatedPort ((INT4) u2LocalPortId,
                                           u4Ieee8021MstpPortMstId, &pRetVal);

    pu1List = pRetVal.pu1_OctetList;
    AST_GET_2BYTE (u2Val, pu1List);
    *pu4RetValIeee8021MstpPortDesignatedPort = u2Val;

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortRole
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortRole (UINT4 u4Ieee8021MstpPortComponentId,
                            UINT4 u4Ieee8021MstpPortMstId,
                            UINT4 u4Ieee8021MstpPortNum,
                            INT4 *pi4RetValIeee8021MstpPortRole)
{
    INT4                i4PortRole = 0;
    INT1                i1RetVal = 0;

    /* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.
     * The validation of MstpCistPortComponentId is done in the Validate routine.*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    i1RetVal = nmhGetFsMIMstMstiCurrentPortRole ((INT4) u4Ieee8021MstpPortNum,
                                                 (INT4) u4Ieee8021MstpPortMstId,
                                                 &i4PortRole);

    /* Values defined in ieee8021MstpPortRole are different from the values
     * returned by nmhGetFsMIMstMstiCurrentPortRole.
     * So Appropriately mapped values are returned to the user */

    if (i1RetVal == SNMP_SUCCESS)
    {
        switch (i4PortRole)
        {
            case AST_PORT_ROLE_ROOT:
                *pi4RetValIeee8021MstpPortRole = AST_PORT_ROLE_IEEE_ROOT;
                break;

            case AST_PORT_ROLE_ALTERNATE:
                *pi4RetValIeee8021MstpPortRole = AST_PORT_ROLE_IEEE_ALTERNATE;
                break;

            case AST_PORT_ROLE_DESIGNATED:
                *pi4RetValIeee8021MstpPortRole = AST_PORT_ROLE_IEEE_DESIGNATED;
                break;

            case AST_PORT_ROLE_BACKUP:
                *pi4RetValIeee8021MstpPortRole = AST_PORT_ROLE_IEEE_BACKUP;
                break;

            default:
                break;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpPortDisputed
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                retValIeee8021MstpPortDisputed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpPortDisputed (UINT4 u4Ieee8021MstpPortComponentId,
                                UINT4 u4Ieee8021MstpPortMstId,
                                UINT4 u4Ieee8021MstpPortNum,
                                INT4 *pi4RetValIeee8021MstpPortDisputed)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Validate routine.*/
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpPortComponentId);

    UNUSED_PARAM (u4Ieee8021MstpPortMstId);    /* MstpPortMstId is also not needed */

    if ((AstSelectContext (u4Ieee8021MstpPortComponentId)) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pi4RetValIeee8021MstpPortDisputed = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry ((INT4) u4Ieee8021MstpPortNum) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO ((UINT2) u4Ieee8021MstpPortNum,
                                              MST_CIST_CONTEXT);
    if (pPerStPortInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    else
    {
        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
        if (pRstPortInfo == NULL)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
        else
        {
            if (pRstPortInfo->bDisputed == MST_TRUE)
            {
                *pi4RetValIeee8021MstpPortDisputed = AST_SNMP_TRUE;
            }
            else
            {
                *pi4RetValIeee8021MstpPortDisputed = AST_SNMP_FALSE;
            }
        }
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpPortPriority
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                setValIeee8021MstpPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpPortPriority (UINT4 u4Ieee8021MstpPortComponentId,
                                UINT4 u4Ieee8021MstpPortMstId,
                                UINT4 u4Ieee8021MstpPortNum,
                                INT4 i4SetValIeee8021MstpPortPriority)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhSetFsMIMstMstiPortPriority ((INT4) u4Ieee8021MstpPortNum,
                                           (INT4) u4Ieee8021MstpPortMstId,
                                           i4SetValIeee8021MstpPortPriority));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpPortPathCost
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                setValIeee8021MstpPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpPortPathCost (UINT4 u4Ieee8021MstpPortComponentId,
                                UINT4 u4Ieee8021MstpPortMstId,
                                UINT4 u4Ieee8021MstpPortNum,
                                INT4 i4SetValIeee8021MstpPortPathCost)
{
/* The MstpPortComponentId is not needed in this table because MstpPortNum itself provides required attributes.The validation of MstpCistPortComponentId is done in the Test routine*/
    UNUSED_PARAM (u4Ieee8021MstpPortComponentId);

    return (nmhSetFsMIMstMstiPortPathCost ((INT4) u4Ieee8021MstpPortNum,
                                           (INT4) u4Ieee8021MstpPortMstId,
                                           i4SetValIeee8021MstpPortPathCost));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpPortPriority
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                testValIeee8021MstpPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpPortPriority (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021MstpPortComponentId,
                                   UINT4 u4Ieee8021MstpPortMstId,
                                   UINT4 u4Ieee8021MstpPortNum,
                                   INT4 i4TestValIeee8021MstpPortPriority)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpPortComponentId,
                                 u4Ieee8021MstpPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsMIMstMstiPortPriority (pu4ErrorCode,
                                              (INT4) u4Ieee8021MstpPortNum,
                                              (INT4) u4Ieee8021MstpPortMstId,
                                              i4TestValIeee8021MstpPortPriority));
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpPortPathCost
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum

                The Object 
                testValIeee8021MstpPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpPortPathCost (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021MstpPortComponentId,
                                   UINT4 u4Ieee8021MstpPortMstId,
                                   UINT4 u4Ieee8021MstpPortNum,
                                   INT4 i4TestValIeee8021MstpPortPathCost)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpPortComponentId);

    if ((AstValidateComponentId (u4Ieee8021MstpPortComponentId,
                                 u4Ieee8021MstpPortNum)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhTestv2FsMIMstMstiPortPathCost (pu4ErrorCode,
                                              (INT4) u4Ieee8021MstpPortNum,
                                              (INT4) u4Ieee8021MstpPortMstId,
                                              i4TestValIeee8021MstpPortPathCost));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpPortTable
 Input       :  The Indices
                Ieee8021MstpPortComponentId
                Ieee8021MstpPortMstId
                Ieee8021MstpPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpPortTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpFidToMstiTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpFidToMstiTable
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpFidToMstiTable (UINT4
                                                    u4Ieee8021MstpFidToMstiComponentId,
                                                    UINT4
                                                    u4Ieee8021MstpFidToMstiFid)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpFidToMstiComponentId);

    if (AST_IS_VC_VALID ((INT4) u4Ieee8021MstpFidToMstiComponentId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) u4Ieee8021MstpFidToMstiFid) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Enabled!!\n");

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpFidToMstiTable
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpFidToMstiTable (UINT4
                                            *pu4Ieee8021MstpFidToMstiComponentId,
                                            UINT4 *pu4Ieee8021MstpFidToMstiFid)
{
    UINT4               u4ContextId = 0;
    UINT2               u2VlanIndex = 1;

    do
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            while (u2VlanIndex <= AST_MAX_NUM_VLANS)
            {
                if (AstL2IwfMiGetVlanInstMapping (u4ContextId, u2VlanIndex) !=
                    0)
                {
                    *pu4Ieee8021MstpFidToMstiComponentId = u4ContextId;
                    *pu4Ieee8021MstpFidToMstiFid = u2VlanIndex;
                    AST_CONVERT_CTXT_ID_TO_COMP_ID
                        ((*pu4Ieee8021MstpFidToMstiComponentId));
                    return SNMP_SUCCESS;
                }
                u2VlanIndex++;
            }
        }
        u4ContextId++;
        u2VlanIndex = 1;
    }
    while (u4ContextId < AST_SIZING_CONTEXT_COUNT);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpFidToMstiTable
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                nextIeee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid
                nextIeee8021MstpFidToMstiFid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpFidToMstiTable (UINT4
                                           u4Ieee8021MstpFidToMstiComponentId,
                                           UINT4
                                           *pu4NextIeee8021MstpFidToMstiComponentId,
                                           UINT4 u4Ieee8021MstpFidToMstiFid,
                                           UINT4
                                           *pu4NextIeee8021MstpFidToMstiFid)
{
    UINT4               u4ContextId = 0;
    UINT2               u2VlanIndex = 1;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpFidToMstiComponentId);

    u4ContextId = u4Ieee8021MstpFidToMstiComponentId;
    u2VlanIndex = u4Ieee8021MstpFidToMstiFid + 1;

    do
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            while (u2VlanIndex <= AST_MAX_NUM_VLANS)
            {
                if (AstL2IwfMiGetVlanInstMapping
                    (u4Ieee8021MstpFidToMstiComponentId, u2VlanIndex) != 0)
                {
                    *pu4NextIeee8021MstpFidToMstiComponentId =
                        u4Ieee8021MstpFidToMstiComponentId;
                    *pu4NextIeee8021MstpFidToMstiFid = u2VlanIndex;

                    AST_CONVERT_CTXT_ID_TO_COMP_ID
                        ((*pu4NextIeee8021MstpFidToMstiComponentId));
                    return SNMP_SUCCESS;
                }
                u2VlanIndex++;
            }
        }
        u4ContextId++;
        u2VlanIndex = 1;
    }
    while (u4ContextId < AST_SIZING_CONTEXT_COUNT);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpFidToMstiMstId
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid

                The Object 
                retValIeee8021MstpFidToMstiMstId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpFidToMstiMstId (UINT4 u4Ieee8021MstpFidToMstiComponentId,
                                  UINT4 u4Ieee8021MstpFidToMstiFid,
                                  UINT4 *pu4RetValIeee8021MstpFidToMstiMstId)
{
    /*In current code we have mapping for vlanid  to mstpid only. This will not 
       work properly for hybrid case. For time being this mapping is done. 
       It may show different in expectation */

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpFidToMstiComponentId);

    if (AstSelectContext (u4Ieee8021MstpFidToMstiComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) u4Ieee8021MstpFidToMstiFid) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Enabled!!\n");

        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pu4RetValIeee8021MstpFidToMstiMstId = AstL2IwfMiGetVlanInstMapping
        (u4Ieee8021MstpFidToMstiComponentId,
         (UINT2) u4Ieee8021MstpFidToMstiFid);
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpFidToMstiMstId
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid

                The Object 
                setValIeee8021MstpFidToMstiMstId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpFidToMstiMstId (UINT4 u4Ieee8021MstpFidToMstiComponentId,
                                  UINT4 u4Ieee8021MstpFidToMstiFid,
                                  UINT4 u4SetValIeee8021MstpFidToMstiMstId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    INT4                i4RetVal = L2IWF_FAILURE;
    UINT2               u2MstId = 0;
    UINT2               u2OldMstId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NumVlans = 0;

    /*"Here Fid is assumed to be same as VLAN Id with the assumption that only one VLAN is mapped to an Fid. 
     * In case of Shared VLAN Learning mode, all VLANs will be mapped to single Fid which would be mapped to single 
     * instance which is the case of CIST having all VLANs mapped. This is default mapping. 
     * Multiple VLANs mapped to a single Fid and mapping that Fid to a MST Instance is not supported, this is case of 
     * Hybrid VLAN Learning mode"
     */

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpFidToMstiComponentId);
    if (AstSelectContext (u4Ieee8021MstpFidToMstiComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4SetValIeee8021MstpFidToMstiMstId == 0)
    {
        u2MstId =
            AstL2IwfMiGetVlanInstMapping (u4Ieee8021MstpFidToMstiComponentId,
                                          u4Ieee8021MstpFidToMstiFid);
        u4SetValIeee8021MstpFidToMstiMstId = u2MstId;
    }

    pPerStInfo = AST_GET_PERST_INFO (u4SetValIeee8021MstpFidToMstiMstId);
    if (pPerStInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pPerStInfo->u1RowStatus == NOT_READY)
    {
        if (u2MstId != 0)
        {
            i4RetVal = AstL2IwfSetVlanInstMapping
                ((INT4) u4Ieee8021MstpFidToMstiComponentId,
                 (tVlanId) u4Ieee8021MstpFidToMstiFid, (UINT2) 0);
        }
        else
        {
            i4RetVal = AstL2IwfSetVlanInstMapping
                ((INT4) u4Ieee8021MstpFidToMstiComponentId,
                 (tVlanId) u4Ieee8021MstpFidToMstiFid,
                 (UINT2) u4SetValIeee8021MstpFidToMstiMstId);
        }
        if (i4RetVal == L2IWF_FAILURE)
        {
            AstReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    else if (pPerStInfo->u1RowStatus == ACTIVE)
    {
        if (u2MstId != 0)
        {
            i4RetVal = nmhSetFsMIMstUnMapVlanIndex ((INT4)
                                                    u4Ieee8021MstpFidToMstiComponentId,
                                                    (INT4)
                                                    u4SetValIeee8021MstpFidToMstiMstId,
                                                    (INT4)
                                                    u4Ieee8021MstpFidToMstiFid);
        }
        else
        {
            /* Get the original isntance mapping for the VLAN */
            u2OldMstId =
                AstL2IwfMiGetVlanInstMapping
                (u4Ieee8021MstpFidToMstiComponentId,
                 u4Ieee8021MstpFidToMstiFid);

            i4RetVal = nmhSetFsMIMstMapVlanIndex ((INT4)
                                                  u4Ieee8021MstpFidToMstiComponentId,
                                                  (INT4)
                                                  u4SetValIeee8021MstpFidToMstiMstId,
                                                  (INT4)
                                                  u4Ieee8021MstpFidToMstiFid);
            if (i4RetVal == SNMP_FAILURE)
            {
                AstReleaseContext ();
                return i4RetVal;
            }

            if (u2OldMstId != AST_INIT_VAL)
            {

                /*When this is the last vlan in this instance, then delete that instance. */
                for (u2VlanId = 1; u2VlanId <= AST_MAX_NUM_VLANS; u2VlanId++)
                {
                    if (AstL2IwfMiGetVlanInstMapping
                        (u4Ieee8021MstpFidToMstiComponentId,
                         u2VlanId) == u2OldMstId)
                    {
                        u2NumVlans++;
                    }
                }

                if (u2NumVlans == 0)
                {
                    AstSelectContext (u4Ieee8021MstpFidToMstiComponentId);
                    if (MstDeleteInstance (u2OldMstId) != MST_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }

        }
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpFidToMstiMstId
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid

                The Object 
                testValIeee8021MstpFidToMstiMstId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpFidToMstiMstId (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021MstpFidToMstiComponentId,
                                     UINT4 u4Ieee8021MstpFidToMstiFid,
                                     UINT4 u4TestValIeee8021MstpFidToMstiMstId)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    INT1                i1RetVal;
    UINT2               u2MstId = 0;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpFidToMstiComponentId);

    if (AstSelectContext (u4Ieee8021MstpFidToMstiComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021MstpFidToMstiMstId == 0)
    {
        u2MstId =
            AstL2IwfMiGetVlanInstMapping (u4Ieee8021MstpFidToMstiComponentId,
                                          u4Ieee8021MstpFidToMstiFid);
        i1RetVal =
            nmhTestv2FsMIMstUnMapVlanIndex (pu4ErrorCode,
                                            (INT4)
                                            u4Ieee8021MstpFidToMstiComponentId,
                                            (INT4) u2MstId,
                                            (INT4) u4Ieee8021MstpFidToMstiFid);
        AstSelectContext (u4Ieee8021MstpFidToMstiComponentId);
        pPerStInfo = AST_GET_PERST_INFO (u2MstId);
    }
    else
    {
        i1RetVal = nmhTestv2FsMIMstMapVlanIndex (pu4ErrorCode,
                                                 (INT4)
                                                 u4Ieee8021MstpFidToMstiComponentId,
                                                 (INT4)
                                                 u4TestValIeee8021MstpFidToMstiMstId,
                                                 (INT4)
                                                 u4Ieee8021MstpFidToMstiFid);
        AstSelectContext (u4Ieee8021MstpFidToMstiComponentId);
        pPerStInfo = AST_GET_PERST_INFO (u4TestValIeee8021MstpFidToMstiMstId);
    }

    if (pPerStInfo == NULL
        || !(pPerStInfo->u1RowStatus == ACTIVE
             || pPerStInfo->u1RowStatus == NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpFidToMstiTable
 Input       :  The Indices
                Ieee8021MstpFidToMstiComponentId
                Ieee8021MstpFidToMstiFid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpFidToMstiTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpVlanTable
 Input       :  The Indices
                Ieee8021MstpVlanComponentId
                Ieee8021MstpVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpVlanTable (UINT4
                                               u4Ieee8021MstpVlanComponentId,
                                               UINT4 u4Ieee8021MstpVlanId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpVlanComponentId);

    if (AST_IS_VC_VALID ((INT4) u4Ieee8021MstpVlanComponentId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) u4Ieee8021MstpVlanId) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Enabled!!\n");

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpVlanTable
 Input       :  The Indices
                Ieee8021MstpVlanComponentId
                Ieee8021MstpVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpVlanTable (UINT4 *pu4Ieee8021MstpVlanComponentId,
                                       UINT4 *pu4Ieee8021MstpVlanId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2VlanIndex = 1;

    do
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            while (u2VlanIndex <= AST_MAX_NUM_VLANS)
            {
                if (AstL2IwfMiGetVlanInstMapping (u4ContextId, u2VlanIndex) !=
                    0)
                {
                    *pu4Ieee8021MstpVlanComponentId = u4ContextId;
                    *pu4Ieee8021MstpVlanId = u2VlanIndex;
                    AST_CONVERT_CTXT_ID_TO_COMP_ID
                        ((*pu4Ieee8021MstpVlanComponentId));
                    return SNMP_SUCCESS;
                }
                u2VlanIndex++;
            }
        }
        u4ContextId++;
        u2VlanIndex = 1;
    }
    while (u4ContextId < AST_SIZING_CONTEXT_COUNT);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpVlanTable
 Input       :  The Indices
                Ieee8021MstpVlanComponentId
                nextIeee8021MstpVlanComponentId
                Ieee8021MstpVlanId
                nextIeee8021MstpVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpVlanTable (UINT4 u4Ieee8021MstpVlanComponentId,
                                      UINT4 *pu4NextIeee8021MstpVlanComponentId,
                                      UINT4 u4Ieee8021MstpVlanId,
                                      UINT4 *pu4NextIeee8021MstpVlanId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2VlanIndex = 1;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpVlanComponentId);

    u4ContextId = u4Ieee8021MstpVlanComponentId;
    u2VlanIndex = u4Ieee8021MstpVlanId + 1;

    do
    {
        if (AST_CONTEXT_INFO (u4ContextId) != NULL)
        {
            while (u2VlanIndex <= AST_MAX_NUM_VLANS)
            {
                if (AstL2IwfMiGetVlanInstMapping
                    (u4Ieee8021MstpVlanComponentId, u2VlanIndex) != 0)
                {
                    *pu4NextIeee8021MstpVlanComponentId =
                        u4Ieee8021MstpVlanComponentId;
                    *pu4NextIeee8021MstpVlanId = u2VlanIndex;

                    AST_CONVERT_CTXT_ID_TO_COMP_ID
                        ((*pu4NextIeee8021MstpVlanComponentId));
                    return SNMP_SUCCESS;
                }
                u2VlanIndex++;
            }
        }
        u4ContextId++;
        u2VlanIndex = 1;
    }
    while (u4ContextId < AST_SIZING_CONTEXT_COUNT);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpVlanMstId
 Input       :  The Indices
                Ieee8021MstpVlanComponentId
                Ieee8021MstpVlanId

                The Object 
                retValIeee8021MstpVlanMstId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpVlanMstId (UINT4 u4Ieee8021MstpVlanComponentId,
                             UINT4 u4Ieee8021MstpVlanId,
                             UINT4 *pu4RetValIeee8021MstpVlanMstId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpVlanComponentId);

    if (AstSelectContext (u4Ieee8021MstpVlanComponentId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_IS_VALID_VLANID ((UINT2) u4Ieee8021MstpVlanId) == MST_FALSE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: Invalid VLAN ID or VLAN ID not Enabled!!\n");

        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pu4RetValIeee8021MstpVlanMstId = AstL2IwfMiGetVlanInstMapping
        (u4Ieee8021MstpVlanComponentId, (UINT2) u4Ieee8021MstpVlanId);
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021MstpConfigIdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021MstpConfigIdTable
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021MstpConfigIdTable (UINT4
                                                   u4Ieee8021MstpConfigIdComponentId)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhValidateIndexInstanceFsMIDot1sFutureMstTable ((INT4)
                                                             u4Ieee8021MstpConfigIdComponentId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021MstpConfigIdTable
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021MstpConfigIdTable (UINT4
                                           *pu4Ieee8021MstpConfigIdComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    i4RetVal = nmhGetFirstIndexFsMIDot1sFutureMstTable ((INT4 *)
                                                        pu4Ieee8021MstpConfigIdComponentId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4Ieee8021MstpConfigIdComponentId));

    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021MstpConfigIdTable
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId
                nextIeee8021MstpConfigIdComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021MstpConfigIdTable (UINT4
                                          u4Ieee8021MstpConfigIdComponentId,
                                          UINT4
                                          *pu4NextIeee8021MstpConfigIdComponentId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    i4RetVal = nmhGetNextIndexFsMIDot1sFutureMstTable ((INT4)
                                                       u4Ieee8021MstpConfigIdComponentId,
                                                       (INT4 *)
                                                       pu4NextIeee8021MstpConfigIdComponentId);

    AST_CONVERT_CTXT_ID_TO_COMP_ID ((*pu4NextIeee8021MstpConfigIdComponentId));

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021MstpConfigIdFormatSelector
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                retValIeee8021MstpConfigIdFormatSelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpConfigIdFormatSelector (UINT4
                                          u4Ieee8021MstpConfigIdComponentId,
                                          INT4
                                          *pi4RetValIeee8021MstpConfigIdFormatSelector)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhGetFsMIMstMstiConfigIdSel ((INT4)
                                          u4Ieee8021MstpConfigIdComponentId,
                                          pi4RetValIeee8021MstpConfigIdFormatSelector));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpConfigurationName
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                retValIeee8021MstpConfigurationName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpConfigurationName (UINT4 u4Ieee8021MstpConfigIdComponentId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValIeee8021MstpConfigurationName)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhGetFsMIMstMstiRegionName ((INT4)
                                         u4Ieee8021MstpConfigIdComponentId,
                                         pRetValIeee8021MstpConfigurationName));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpRevisionLevel
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                retValIeee8021MstpRevisionLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpRevisionLevel (UINT4 u4Ieee8021MstpConfigIdComponentId,
                                 UINT4 *pu4RetValIeee8021MstpRevisionLevel)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhGetFsMIMstMstiRegionVersion ((INT4)
                                            u4Ieee8021MstpConfigIdComponentId,
                                            (INT4 *)
                                            pu4RetValIeee8021MstpRevisionLevel));
}

/****************************************************************************
 Function    :  nmhGetIeee8021MstpConfigurationDigest
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                retValIeee8021MstpConfigurationDigest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021MstpConfigurationDigest (UINT4 u4Ieee8021MstpConfigIdComponentId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValIeee8021MstpConfigurationDigest)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhGetFsMIMstMstiConfigDigest ((INT4)
                                           u4Ieee8021MstpConfigIdComponentId,
                                           pRetValIeee8021MstpConfigurationDigest));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021MstpConfigIdFormatSelector
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                setValIeee8021MstpConfigIdFormatSelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpConfigIdFormatSelector (UINT4
                                          u4Ieee8021MstpConfigIdComponentId,
                                          INT4
                                          i4SetValIeee8021MstpConfigIdFormatSelector)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhSetFsMIMstMstiConfigIdSel ((INT4)
                                          u4Ieee8021MstpConfigIdComponentId,
                                          i4SetValIeee8021MstpConfigIdFormatSelector));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpConfigurationName
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                setValIeee8021MstpConfigurationName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpConfigurationName (UINT4 u4Ieee8021MstpConfigIdComponentId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValIeee8021MstpConfigurationName)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhSetFsMIMstMstiRegionName ((INT4)
                                         u4Ieee8021MstpConfigIdComponentId,
                                         pSetValIeee8021MstpConfigurationName));
}

/****************************************************************************
 Function    :  nmhSetIeee8021MstpRevisionLevel
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                setValIeee8021MstpRevisionLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021MstpRevisionLevel (UINT4 u4Ieee8021MstpConfigIdComponentId,
                                 UINT4 u4SetValIeee8021MstpRevisionLevel)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhSetFsMIMstMstiRegionVersion ((INT4)
                                            u4Ieee8021MstpConfigIdComponentId,
                                            (INT4)
                                            u4SetValIeee8021MstpRevisionLevel));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpConfigIdFormatSelector
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                testValIeee8021MstpConfigIdFormatSelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpConfigIdFormatSelector (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021MstpConfigIdComponentId,
                                             INT4
                                             i4TestValIeee8021MstpConfigIdFormatSelector)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    if (AstSelectContext ((INT4) u4Ieee8021MstpConfigIdComponentId) !=
        RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /*Validation done for the value required as per the definition given for ieee8021MstpConfigIdFormatSelector */
    if (i4TestValIeee8021MstpConfigIdFormatSelector != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021MstpConfigurationName
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                testValIeee8021MstpConfigurationName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 Function    :  nmhTestv2Ieee8021MstpRevisionLevel
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpConfigurationName (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021MstpConfigIdComponentId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValIeee8021MstpConfigurationName)
{
    UINT4               u4ContextId = 0;

    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /* Validation done as per the definition given for ieee8021MstpConfigurationName */
    if (pTestValIeee8021MstpConfigurationName->i4_Length >
        MST_MAX_CONFIG_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhTestv2Ieee8021MstpRevisionLevel
 *
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId

                The Object 
                testValIeee8021MstpRevisionLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021MstpRevisionLevel (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021MstpConfigIdComponentId,
                                    UINT4 u4TestValIeee8021MstpRevisionLevel)
{
    AST_CONVERT_COMP_ID_TO_CTXT_ID (u4Ieee8021MstpConfigIdComponentId);

    return (nmhTestv2FsMIMstMstiRegionVersion (pu4ErrorCode,
                                               (INT4)
                                               u4Ieee8021MstpConfigIdComponentId,
                                               (INT4)
                                               u4TestValIeee8021MstpRevisionLevel));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021MstpConfigIdTable
 Input       :  The Indices
                Ieee8021MstpConfigIdComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021MstpConfigIdTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
