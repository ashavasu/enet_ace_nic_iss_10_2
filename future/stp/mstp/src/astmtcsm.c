/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmtcsm.c,v 1.60 2017/11/27 13:39:30 siva Exp $
 *
 * Description: This file contains MST related topology SEM routines.
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChMachine                       */
/*                                                                           */
/*    Description               : This function detect the Topology change,  */
/*                                notify the Topology change, propagate      */
/*                                the Topology change information and flush  */
/*                                the Filtering database Entries.            */
/*                                                                           */
/*    Input(s)                  : u1Event        - Event that has caused the */
/*                                                 Topology Change State     */
/*                                                 Machine.                  */
/*                                                                           */
/*                                u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChMachine (UINT1 u1Event,
                      UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChMachine (u1Event, u2InstanceId, pPerStPortInfo)
     UINT1               u1Event;
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    UINT1               u1State = 0;
    INT4                i4RetVal = 0;

    if (NULL == pPerStPortInfo)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Event %s: Inst %d: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[u1Event], u2InstanceId);
        return MST_FAILURE;
    }
    u1State = pPerStPortInfo->u1TopoChSmState;

    AST_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                  "Port %s: Inst %d: Topology Change Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId,
                  gaaau1AstSemEvent[AST_TCSM][u1Event],
                  gaaau1AstSemState[AST_TCSM][u1State]);
    AST_DBG_ARG4 (AST_TCSM_DBG,
                  "Port %s: Inst %d: Topology Change Machine Called with Event: %s, State: %s\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId,
                  gaaau1AstSemEvent[AST_TCSM][u1Event],
                  gaaau1AstSemState[AST_TCSM][u1State]);
    if (((AST_CURR_CONTEXT_INFO ())->bBegin == MST_TRUE) &&
        (u1Event != MST_TOPOCHSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_TCSM_DBG,
                 "TCSM: Ignoring event since BEGIN is asserted\n");
        return MST_SUCCESS;
    }
    if (MST_TOPO_CH_MACHINE[u1Event][u1State].pAction == NULL)
    {
        AST_DBG (AST_TCSM_DBG,
                 "TCSM: TopologyChangeStateMachine No Action to Perform!\n");

        return MST_SUCCESS;
    }

    i4RetVal = (*(MST_TOPO_CH_MACHINE[u1Event][u1State].pAction))
        (u2InstanceId, pPerStPortInfo);

    if (i4RetVal != MST_SUCCESS)
    {
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                 "TCSM: TopologyChangeStateMachine Action returned FAILURE!\n");
        AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                 "TCSM: TopologyChangeStateMachine Action returned FAILURE!\n");
        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstTopoChSmChkInactive                               */
/*                                                                           */
/* Description        : This function is called to check whether the port    */
/*                      can be moved to the INACTIVE state from the          */
/*                      LEARNING state.                                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2InstanceId   - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstTopoChSmChkInactive (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    /* Checking conditions for moving to INACTIVE state
     * !Root && !designated && !Master && !(learn || learning) &&
     *  !(rcvdTc || rcvdTcn || rcvdTcAck || tcProp)
     *  */
    if ((pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_ROOT) &&
        (pPerStPortInfo->u1PortRole != (UINT1) AST_PORT_ROLE_DESIGNATED) &&
        (pPerStPortInfo->u1PortRole != (UINT1) MST_PORT_ROLE_MASTER) &&
        ((pRstPortInfo->bLearn == RST_FALSE) &&
         (pRstPortInfo->bLearning == RST_FALSE))
        &&
        ((pRstPortInfo->bRcvdTc == RST_FALSE) &&
         (pAstCommPortInfo->bRcvdTcn == RST_FALSE) &&
         (pAstCommPortInfo->bRcvdTcAck == RST_FALSE) &&
         (pRstPortInfo->bTcProp == RST_FALSE)))
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Port %s: Inst %u: Role is NOT ROOT/DESIGNATED; Changing to INACTIVE state\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        if (MstTopoChSmMakeInActive (u2InstanceId, pPerStPortInfo) !=
            MST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: MstTopoChSmMakeInActive function returned FAILURE!\n");
            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopoChSmMakeInActive                    */
/*                                                                           */
/*    Description               : This function is called during the         */
/*                                initialisation of the Topology Change State*/
/*                                machine and also if the Port is not an Root*/
/*                                Port or Designated Port Roles. This changes*/
/*                                the state of Topology Change state machine */
/*                                to INACTIVE State.                         */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstTopoChSmMakeInActive (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /*This function can be called as a result of
     *  
     *  a)The port being operationally disabled 
     *        - entries on this port should be flushed for all FIDs.
     *        
     *  b)The port becoming an alternate port for a particular
     *  instance 
     *        - entries on this port should be flushed for FIDs belonging 
     *        to that instance alone.
     *
     *  c)The port is disabled in MSTP by management 
     *        - entries on this port should not be flushed since the port 
     *        will be put into forwarding 
     * */

    if (pAstPortEntry->u1EntryStatus == AST_PORT_OPER_DOWN)
    {
        MstTopologyChSmFlushPort (pPerStPortInfo);
    }
    else if (pRstPortInfo->bPortEnabled == MST_TRUE)
    {
#ifdef NPAPI_WANTED
        if ((AST_CURR_CONTEXT_INFO ())->bBegin != MST_TRUE)
        {
#endif
            MstTopologyChSmFlushFdb (pPerStPortInfo,
                                     u2InstanceId, VLAN_NO_OPTIMIZE);
#ifdef NPAPI_WANTED
        }
#endif
    }

    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_TCWHILE) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Tc While Timer Stopping is Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }

#ifdef MRP_WANTED
    if (pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_TCDETECTED)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: TcDetectedTmr Timer"
                          " Stopping is Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }
#endif
    if (u2InstanceId == MST_CIST_CONTEXT)
    {
        pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
        pAstCommPortInfo->bTcAck = MST_FALSE;
    }

    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_INACTIVE;

    AST_DBG_ARG2 (AST_TCSM_DBG, "TCSM: Port %s: Inst %d: Moved to state INIT\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if (MstTopoChSmChkLearning (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: MstTopoChSmChkLearning returned Failure!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopoChSmChkLearning                     */
/*                                                                           */
/*    Description               : This function is called by the INACTIVE    */
/*                                state to check the conditions for moving   */
/*                                to LEARNING state                          */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstTopoChSmChkLearning (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /* Checking the conditions for moving to learning state
     * learn && !fdbFlush
     * This function is called from INACTIVE state. In that state
     * flushing has been done already. Hence checking for learn alon
     * */
    if (pRstPortInfo->bLearn == MST_TRUE)
    {
        if (MstTopoChSmMakeLearning (u2InstanceId, pPerStPortInfo)
            != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: MstTopoChSmMakeLearning returned Failure!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopoChSmMakeLearning                    */
/*                                                                           */
/*    Description               : This function is called when the state is  */
/*                                changed to LEARNING state                  */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstTopoChSmMakeLearning (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pPerStRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    pPerStRstPortInfo->bRcvdTc = MST_FALSE;
    if (u2InstanceId == MST_CIST_CONTEXT)
    {
        pAstCommPortInfo->bRcvdTcn = MST_FALSE;
        pAstCommPortInfo->bRcvdTcAck = MST_FALSE;
    }

    AST_DBG_ARG2 (AST_SM_VAR_DBG,
                  "TCSM_Learning: Port %s: Inst %u:"
                  "RcvdTc = RcvdTcn = RcvdTcAck = Tc = TcProp = FALSE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    pPerStPortInfo->u1TopoChSmState = (UINT1) MST_TOPOCHSM_STATE_LEARNING;

    if (pPerStRstPortInfo->bTcProp == MST_TRUE)
    {
        /* setTcPropTree has set the value as TRUE. Change the same to false
         * and return from here itself, as there is no need to check for 
         * DETECTED & INACTIVE states
         * */
        pPerStRstPortInfo->bTcProp = MST_FALSE;

        return MST_SUCCESS;
    }
    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: Moved to state LEARNING\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if (MstTopoChSmChkDetected (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    if (MstTopoChSmChkInactive (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        return MST_FAILURE;
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstTopoChSmChkDetected                               */
/*                                                                           */
/* Description        : This function is called to check whether the port    */
/*                      can be moved to the DETECTED state from the          */
/*                      LEARNING state.                                      */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port information                    */
/*                      u2InstanceId   - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstTopoChSmChkDetected (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);

    if (((pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_ROOT) ||
         (pPerStPortInfo->u1PortRole == (UINT1) AST_PORT_ROLE_DESIGNATED) ||
         (pPerStPortInfo->u1PortRole == (UINT1) MST_PORT_ROLE_MASTER))
        &&
        (pRstPortInfo->bForward == RST_TRUE)
        && (pPortInfo->bOperEdgePort == RST_FALSE))
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Port %s: Inst %u: Role is ROOT/DESIGNATED; "
                      "Changing to DETECTED state\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        if (MstTopologyChSmMakeDetected (u2InstanceId, pPerStPortInfo) !=
            MST_SUCCESS)
        {
            AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                     "TCSM: MstTopologyChSmMakeDetected function returned FAILURE!\n");

            return MST_FAILURE;
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakeDetected                */
/*                                                                           */
/*    Description               : This function is called when the Port has  */
/*                                detected Topology Change. This is called   */
/*                                when the Topology change state machine in  */
/*                                ACTIVE state and change the state to       */
/*                                DETECTED State.                            */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakeDetected (UINT2 u2InstanceId,
                             tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakeDetected (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT4               u4Ticks = 0;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (pPerStPortInfo->u2PortNo);
    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo->u2PortNo);
    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    AST_GET_SYS_TIME ((tAstSysTime
                       *) (&(pPerStPortInfo->u4TcDetectedTimestamp)));
    pPerStPortInfo->u4TcDetectedCount = pPerStPortInfo->u4TcDetectedCount + 1;
    pPerStBrgInfo->au2TcDetPorts[pPerStBrgInfo->u1DetHead] =
        (UINT2) AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);
    pPerStBrgInfo->u1DetHead = (UINT1) (pPerStBrgInfo->u1DetHead + 1);
    if (pPerStBrgInfo->u1DetHead == AST_MAX_TC_MEM_LEN)
        pPerStBrgInfo->u1DetHead = AST_INIT_VAL;

    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: Moved to state DETECTED\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if (pAstCommPortInfo != NULL)
    {
        pAstCommPortInfo->bNewInfo = MST_TRUE;
    }

    if (pCistMstiPortInfo != NULL)
    {
        if (u2InstanceId == MST_CIST_CONTEXT)
        {
            pCistMstiPortInfo->bNewInfo = MST_TRUE;
        }
        else
        {
            pCistMstiPortInfo->bNewInfoMsti = MST_TRUE;
        }
    }

    if (MstTopologyChNewTcWhile (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: MstTopologyChNewTcWhile returned Failure!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }
    MST_INCR_TC_DETECTED_COUNT (pPerStPortInfo->u2PortNo, u2InstanceId);
    AST_GET_SYS_TIME (&u4Ticks);
    pPerStPortInfo->u4TcDetectedTimeStamp = u4Ticks;
    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: Topology Change is Detected!\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if ((pCistMstiPortInfo->bNewInfoMsti == MST_TRUE) ||
        (pCistMstiPortInfo->bNewInfo == MST_TRUE))
    {

        if ((u2InstanceId == pPortInfo->u2LastInst))
        {
            if ((pRstPortInfo->bAgree == MST_TRUE) &&
                (pRstPortInfo->bProposing != MST_TRUE) &&
                (AST_FORCE_VERSION > AST_VERSION_2) &&
                (pAstCommPortInfo->pHelloWhenTmr != NULL))
            {
                if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                            pPortInfo,
                                            u2InstanceId) != RST_SUCCESS)
                {
                    AST_DBG_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_DBG,
                                  "TCSM: Port %s: Inst %d: Port Transmit Machine "
                                  "returned FAILURE for NEWINFO_SET event\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstanceId);
                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "TCSM: Port %s: Inst %d: Port Transmit Machine "
                                  "returned FAILURE for NEWINFO_SET event\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2InstanceId);

                    /* Ignore this failure event, as this may impact the SEM State of 
                     * Topology change state machine
                     * */
                }
            }
        }

        else
        {
            if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                        pPortInfo, u2InstanceId) != RST_SUCCESS)
            {
                AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                              "TCSM: Port %s: Inst %d: Port Transmit Machine "
                              "returned FAILURE for NEWINFO_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);
                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "TCSM: Port %s: Inst %d: Port Transmit Machine "
                              "returned FAILURE for NEWINFO_SET event\n",
                              AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                              u2InstanceId);

                /* Ignore this failure event, as this may impact the SEM State of 
                 * Topology change state machine
                 * */
            }
        }
    }
    MstTopologyChSetTcPropTree (u2InstanceId, pPerStPortInfo);

#ifdef MRP_WANTED
    if (MstTopologyChNewTcDetected (u2InstanceId, pPerStPortInfo)
        != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: Tc DetectedTimer Start is "
                      " Failed!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }
#endif
    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_DETECTED;

    MstTopologyChSmMakeActive (u2InstanceId, pPerStPortInfo);
    return MST_SUCCESS;
}

#ifdef MRP_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChNewTcDetected                 */
/*                                                                           */
/*    Description               : This function starts the tcDetected timer  */
/*                                with duration equal to Hello Time + 1      */
/*                                seconds when partner bridge port is RSTP   */
/*                                capable or equal to Root Bridge MaxAge and */
/*                                Forward Delay value otherwise.             */
/*                                                                           */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 Tree                      */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChNewTcDetected (UINT2 u2InstanceId,
                            tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChNewTcDetected (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortEntry = NULL;
    UINT2               u2TcDetectedDuration = 0;
    UINT2               u2PortNum = 0;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: No Such Instance Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }

    pPerStPortEntry = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pPerStPortEntry == NULL)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: Per Spanning Info not Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }

    if (pAstCommPortInfo->bSendRstp == MST_TRUE)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Inst %d: Port %s Partner Bridge Port is RSTP"
                      " capable\n", u2InstanceId,
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        u2TcDetectedDuration = (UINT2) ((pPortInfo->PortTimes.u2HelloTime)
                                        + (1 * AST_CENTI_SECONDS));
    }
    else
    {
        u2TcDetectedDuration = (UINT2) ((pBrgInfo->RootTimes.u2MaxAge) +
                                        (pBrgInfo->RootTimes.u2ForwardDelay));
    }

    /* Start tcDetected only if it is not already running */
    if (pPerStPortInfo->PerStRstPortInfo.pTcDetectedTmr == NULL)
    {
        if (AstStartTimer (pPerStPortInfo, u2InstanceId,
                           AST_TMR_TYPE_TCDETECTED,
                           u2TcDetectedDuration) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Start TcDetected Timer"
                          " Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: TcDetected Timer Start is"
                          " Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChNewTcWhile                    */
/*                                                                           */
/*    Description               : This function starts the tcWhile timer     */
/*                                with duration equal to Hello Time + 1      */
/*                                seconds when partner bridge port is RSTP   */
/*                                capable or equal to Root Bridge MaxAge and */
/*                                Forward Delay value otherwise.             */
/*                                                                           */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 Tree                      */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChNewTcWhile (UINT2 u2InstanceId, tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChNewTcWhile (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;

    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStPortInfo  *pPerStPortEntry = NULL;
    UINT2               u2TcWhileDuration = 0;
    UINT2               u2PortNum = 0;
    tAstBoolean         bBridgeTcPresent = MST_FALSE;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;

    pPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (u2InstanceId);
    if (pPerStBridgeInfo == NULL)
    {
        return MST_FAILURE;
    }
    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: No Such Instance Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }
    if (pPerStInfo->u4TcWhileCount != 0)
    {
        bBridgeTcPresent = MST_TRUE;
    }

    if (bBridgeTcPresent == MST_FALSE)
    {
        /* TC is not present at the bridge level
         * so increment the topology change count and send trap.
         */
        AST_INCR_NUM_OF_TOPO_CHANGES (u2InstanceId);
        AST_GET_SYS_TIME ((tAstSysTime
                           *) (&(pPerStBridgeInfo->u4TimeSinceTopoCh)));
        AstTopologyChangeTrap (u2InstanceId, (INT1 *) AST_MST_TRAPS_OID,
                               AST_MST_TRAPS_OID_LEN);
    }

    u2PortNum = pPerStPortInfo->u2PortNo;
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pPerStInfo = AST_GET_PERST_INFO (u2InstanceId);
    if (pPerStInfo == NULL)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: No Such Instance Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }

    pPerStPortEntry = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    if (pPerStPortEntry == NULL)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: Per Spanning Info not Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }

    pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);

    if (pCistMstiPortInfo == NULL)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Cist Msti Port Info not Exist!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return MST_FAILURE;
    }

    if (pAstCommPortInfo->bSendRstp == MST_TRUE)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Inst %d: Port %s Partner Bridge Port is RSTP"
                      " capable\n", u2InstanceId,
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));
        u2TcWhileDuration = (UINT2) ((pPortInfo->PortTimes.u2HelloTime) +
                                     (1 * AST_CENTI_SECONDS));

        /* Set newInfo only if TcWhile timer is not running */
        if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr == NULL)
        {
            pAstCommPortInfo->bNewInfo = MST_TRUE;

            if (u2InstanceId == MST_CIST_CONTEXT)
            {
                pCistMstiPortInfo->bNewInfo = MST_TRUE;
            }
            else
            {
                pCistMstiPortInfo->bNewInfoMsti = MST_TRUE;
            }
        }
    }
    else
    {
        u2TcWhileDuration = (UINT2) ((pBrgInfo->RootTimes.u2MaxAge) +
                                     (pBrgInfo->RootTimes.u2ForwardDelay));
    }

    /* Start tcWhile only if it is not already running */
    if (pPerStPortInfo->PerStRstPortInfo.pTcWhileTmr == NULL)
    {
        if (AstStartTimer (pPerStPortInfo, u2InstanceId,
                           AST_TMR_TYPE_TCWHILE,
                           u2TcWhileDuration) != MST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Start TcWhile Timer Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: Tc While Timer Start is Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSetTcPropTree                 */
/*                                                                           */
/*    Description               : This function sets the Topology Change     */
/*                                Propagation to True for given Tree for all */
/*                                ports except the Port invoked the routine. */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 Tree                      */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstTopologyChSetTcPropTree (UINT2 u2InstanceId,
                            tAstPerStPortInfo * pPerStPortInfo)
#else
VOID
MstTopologyChSetTcPropTree (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = 0;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    /* restrictedTCN causes the Port not to propagate received topology change 
     * notifications and topology changes to other Ports.*/

    /* Hence if restrictedTCN is set for the port that is invoking this 
     * procedure, then do nothing. */
    if (AST_PORT_RESTRICTED_TCN (pAstPortEntry) == (tAstBoolean) RST_TRUE)
    {
        return;
    }

    /* Reinitializing for furthur usage */
    pAstPortEntry = NULL;
    u2PortNum = 1;
    AST_GET_NEXT_PORT_ENTRY (u2PortNum, pAstPortEntry)
    {
        if (pPerStPortInfo->u2PortNo == u2PortNum)
        {
            continue;
        }

        if (pAstPortEntry == NULL)
        {
            continue;
        }

        if (AST_GET_PERST_INFO (u2InstanceId) == NULL)
        {
            continue;
        }

        pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
        if (pAstPerStPortInfo == NULL)
        {
            continue;
        }

        if (!(AST_IS_PORT_UP (u2PortNum)))
        {
            continue;
        }

        pRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);
        if (pRstPortInfo->bPortEnabled != MST_TRUE)
        {
            continue;
        }

        pAstPerStPortInfo->PerStRstPortInfo.bTcProp = MST_TRUE;

        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "TCSM: Inst %d: Propagating Topology Change Info "
                      "(TcProp) on Port %s\n", u2InstanceId,
                      AST_GET_IFINDEX_STR (u2PortNum));

        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Inst %d: Propagating Topology Change Info "
                      "(TcProp) on Port %s\n", u2InstanceId,
                      AST_GET_IFINDEX_STR (u2PortNum));

        if (MstTopologyChMachine
            ((UINT1) MST_TOPOCHSM_EV_TCPROP, u2InstanceId,
             pAstPerStPortInfo) != RST_SUCCESS)
        {
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: MstTopoChMachine"
                          "Entry function "
                          "returned FAILURE for TCPROP event!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
            AST_DBG_ARG2 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: RstTopoChMachine"
                          "Entry function "
                          "returned FAILURE for TCPROP event!\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2InstanceId);
            return;
        }
    }                            /* End of FOR Loop */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakeNotifiedTcn             */
/*                                                                           */
/*    Description               : This function is called when the Port has  */
/*                                Received TCN Bpdu. This is called when     */
/*                                the Topology change state machine in       */
/*                                ACTIVE state and change the state to       */
/*                                NOTIFIED_TCN State.                        */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakeNotifiedTcn (UINT2 u2InstanceId,
                                tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakeNotifiedTcn (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPortEntry      *pPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: RCVDTCN has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if (MstTopologyChNewTcWhile (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        AST_DBG_ARG2 (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                      "TCSM: Port %s: Inst %d: Tc While Timer Start is Failed!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);
        return MST_FAILURE;
    }

    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_NOTIFIED_TCN;

    if ((AST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo->u2PortNo)->bNewInfo ==
         MST_TRUE)
        || (AST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo->u2PortNo)->
            bNewInfoMsti == MST_TRUE))
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM_RcvdTcn: Port %s: Inst %d: NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            return MST_FAILURE;
        }
    }

    MstTopologyChSmMakeNotifiedTc (u2InstanceId, pPerStPortInfo);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakeNotifiedTc              */
/*                                                                           */
/*    Description               : This function is called when the Port has  */
/*                                Received BPDU with Topology change flag    */
/*                                set. This is called unconditionally when   */
/*                                Topology Change State Machine is in        */
/*                                NOTIFIED_TCN State or from ACTIVE State    */
/*                                and this function changes the state to     */
/*                                NOTIFIED_TC.                               */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakeNotifiedTc (UINT2 u2InstanceId,
                               tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakeNotifiedTc (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStInfo = AST_GET_PERST_INFO (pPerStPortInfo->u2Inst);
    pPerStBrgInfo = &(pPerStInfo->PerStBridgeInfo);

    AST_GET_SYS_TIME ((tAstSysTime *) (&(pPerStPortInfo->u4TcRcvdTimestamp)));
    pPerStPortInfo->u4TcRcvdCount = pPerStPortInfo->u4TcRcvdCount + 1;
    pPerStBrgInfo->au2TcRecvPorts[pPerStBrgInfo->u1RcvdHead] =
        (UINT2) AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);
    pPerStBrgInfo->u1RcvdHead = (UINT1) (pPerStBrgInfo->u1RcvdHead + 1);
    if (pPerStBrgInfo->u1RcvdHead == AST_MAX_TC_MEM_LEN)
    {
        pPerStBrgInfo->u1RcvdHead = AST_INIT_VAL;
    }

    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: RCVDTC has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pPerStRstPortInfo->bRcvdTc = MST_FALSE;

    if (u2InstanceId == MST_CIST_CONTEXT)
    {
        pAstCommPortInfo->bRcvdTcn = MST_FALSE;

        if (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED)
        {
            pAstCommPortInfo->bTcAck = MST_TRUE;
        }
    }

    MstTopologyChSetTcPropTree (u2InstanceId, pPerStPortInfo);

    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_NOTIFIED_TC;

    MstTopologyChSmMakeActive (u2InstanceId, pPerStPortInfo);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakePropagating             */
/*                                                                           */
/*    Description               : This function is called if this port is    */
/*                                not an edge port and its bTcProp variable  */
/*                                has been set by some other port of this    */
/*                                bridge indicating the need to propagate a  */
/*                                Topology Change state machine is in the    */
/*                                Active state and this function changes the */
/*                                state to the Propagating State.            */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakePropagating (UINT2 u2InstanceId,
                                tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakePropagating (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2PortNum = AST_INIT_VAL;

    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);
    if (pAstPortEntry == NULL)
    {
        /* This pointer should not be null
         * */
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: PortEntry(AST_GET_PORTENTRY) pointer is NULL!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);
        return MST_FAILURE;
    }

    pPortInfo = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    u2PortNum = pPerStPortInfo->u2PortNo;

    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: TCPROP has been set, taking action...\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    /* Check for Edge Port added as per 802.1Q 2005
     * Refer Figure 13-21- Topology Change State Machine 
     * */
    if (pAstPortEntry->bOperEdgePort == MST_TRUE)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: Port is Edge Port Not Propagating!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_SUCCESS;
    }

    if (MstTopologyChNewTcWhile (u2InstanceId, pPerStPortInfo) != MST_SUCCESS)
    {
        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                      "TCSM: Port %s: Inst %d: Tc While Timer Start is Failed!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        return MST_FAILURE;
    }
#ifdef NPAPI_WANTED
    if ((AST_CURR_CONTEXT_INFO ())->bBegin != MST_TRUE)
    {
#endif
        MstTopologyChSmFlushFdb (pPerStPortInfo, u2InstanceId, VLAN_OPTIMIZE);
#ifdef NPAPI_WANTED
    }
#endif

    pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);
    pPerStRstPortInfo->bTcProp = MST_FALSE;

    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_PROPAGATING;

    if ((AST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo->u2PortNo)->bNewInfo ==
         MST_TRUE)
        || (AST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo->u2PortNo)->
            bNewInfoMsti == MST_TRUE))
    {
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM_Propagating: Port %s: Inst %d: NewInfo = TRUE\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);

        if (RstPortTransmitMachine (RST_PTXSM_EV_NEWINFO_SET,
                                    pPortInfo, u2InstanceId) != RST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Port Transmit Machine returned FAILURE for NEWINFO_SET event\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            return MST_FAILURE;
        }
    }

    MstTopologyChSmMakeActive (u2InstanceId, pPerStPortInfo);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakeAcknowledged            */
/*                                                                           */
/*    Description               : This function is called if a BPDU with TC  */
/*                                acknowledge flag set is received on this   */
/*                                port. This is called when the Topology     */
/*                                Change State Machine is in the Active state*/
/*                                and this function changes the state to the */
/*                                Acknowledged State.                        */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakeAcknowledged (UINT2 u2InstanceId,
                                 tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakeAcknowledged (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstCommPortInfo   *pAstCommPortInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);
    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: RCVDTCACK has been set, taking action..\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);

    if (pPerStRstPortInfo->pTcWhileTmr != NULL)
    {
        if (AstStopTimer (pPerStPortInfo, AST_TMR_TYPE_TCWHILE) != MST_SUCCESS)
        {

            AST_TRC_ARG2 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                          "TCSM: Port %s: Inst %d: Tc While Timer Stopping is Failed!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;

        }
    }

    pAstCommPortInfo->bRcvdTcAck = MST_FALSE;

    pPerStPortInfo->u1TopoChSmState = MST_TOPOCHSM_STATE_ACKNOWLEDGED;

    MstTopologyChSmMakeActive (u2InstanceId, pPerStPortInfo);

    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmMakeActive                  */
/*                                                                           */
/*    Description               : This function is called from DETECTED,     */
/*                                NOTIFIED_TC, PROPAGATING, ACKNOWLEDGED     */
/*                                state and moves to appropriate when the    */
/*                                Events are received.                       */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmMakeActive (UINT2 u2InstanceId,
                           tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmMakeActive (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    AST_UNUSED (u2InstanceId);

    AST_DBG_ARG2 (AST_TCSM_DBG,
                  "TCSM: Port %s: Inst %d: Moved to state ACTIVE\n",
                  AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo), u2InstanceId);
    pPerStPortInfo->u1TopoChSmState = (UINT1) MST_TOPOCHSM_STATE_ACTIVE;
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmNoOperEdgeInActive          */
/*                                                                           */
/*    Description               : This function is called when the OperEdge  */
/*                                Property is false for the port and the     */
/*                                current state is INACTIVE. Based on forward*/
/*                                and Port Role, this function moves the     */
/*                                machine to the DETECTED state.             */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmNoOperEdgeInActive (UINT2 u2InstanceId,
                                   tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmNoOperEdgeInActive (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2InstanceId);

    if ((pPerStRstPortInfo->bForward == MST_TRUE) &&
        ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED) ||
         (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT) ||
         (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)))
    {
        if (MstTopologyChSmMakeDetected (u2InstanceId, pPerStPortInfo) !=
            MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: MstTopologyChSmMakeDetected returned Failure!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmForwardInActive             */
/*                                                                           */
/*    Description               : This function is called when the Forward   */
/*                                is TRUE and the current state is INACTIVE. */
/*                                Based on operEdge and Port Role, this      */
/*                                function moves the machine to the          */
/*                                DETECTED state.                            */
/*                                                                           */
/*    Input(s)                  : u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                                                           */
/*                                pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS on Success.                    */
/*                                MST_FAILURE on Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstTopologyChSmForwardInActive (UINT2 u2InstanceId,
                                tAstPerStPortInfo * pPerStPortInfo)
#else
INT4
MstTopologyChSmForwardInActive (u2InstanceId, pPerStPortInfo)
     UINT2               u2InstanceId;
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;

    if ((AST_GET_PORTENTRY (u2PortNum)->bOperEdgePort == MST_FALSE) &&
        ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_DESIGNATED) ||
         (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT) ||
         (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)))
    {
        if (MstTopologyChSmMakeDetected (u2InstanceId, pPerStPortInfo) !=
            MST_SUCCESS)
        {
            AST_DBG_ARG2 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
                          "TCSM: Port %s: Inst %d: MstTopologyChSmMakeDetected returned Failure!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);

            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmFlushPort                   */
/*                                                                           */
/*    Description               : This function performs the functionality   */
/*                                of flushing the Filtering Database to      */
/*                                remove the information that was learnt on  */
/*                                this Port for all Fids.                    */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstTopologyChSmFlushPort (tAstPerStPortInfo * pPerStPortInfo)
#else
VOID
MstTopologyChSmFlushPort (pPerStPortInfo)
     tAstPerStPortInfo  *pPerStPortInfo;
#endif
{
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2PortNum = 0;

    u2PortNum = pPerStPortInfo->u2PortNo;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    if (pPortInfo->bOperEdgePort == MST_TRUE)
    {
        /* Donot Flush for Edge Ports */
        return;
    }

    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        AstTopologyChSmFlushEntries (pPerStPortInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstTopologyChSmFlushFdb                    */
/*                                                                           */
/*    Description               : This function performs the functionality   */
/*                                of flushing the Filtering Database to      */
/*                                remove the information that was learnt on  */
/*                                this Port for FIDs belonging to the given  */
/*                                instance.                                  */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                u2InstanceId   - The Id of the spanning    */
/*                                                 tree instance.            */
/*                                i4OptimizeFlag - Indicates whether this    */
/*                                                 call can be grouped with  */
/*                                                 the flush call for other  */
/*                                                 ports as a single bridge  */
/*                                                 flush                     */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstTopologyChSmFlushFdb (tAstPerStPortInfo * pPerStPortInfo,
                         UINT2 u2InstanceId, INT4 i4OptimizeFlag)
#else
VOID
MstTopologyChSmFlushFdb (pPerStPortInfo, u2InstanceId, i4OptimizeFlag)
     tAstPerStPortInfo  *pPerStPortInfo;
     UINT2               u2InstanceId;
     INT4                i4OptimizeFlag;
#endif
{
    UINT1               u1VlanLearningType;
    UINT4               u4IfIndex;
    UINT2               u2PortIndex = pPerStPortInfo->u2PortNo;
    UINT2               u2PortNum = 0;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    u2PortNum = pPerStPortInfo->u2PortNo;
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    if ((pPortInfo == NULL) || (pCommPortInfo == NULL))
    {
        return;
    }

    u4IfIndex = AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);

    if (AST_GET_PORTENTRY (u2PortIndex)->bOperEdgePort == MST_TRUE)
    {
        /* Donont process for edge ports
         * */
        return;
    }

    /* In Asynchronous NPAPI mode to avoid the stale FDB entires, flushing can be 
     * done after successful port state change indication from hardware */
    if (AST_ASYNC_MODE () == ISS_NPAPI_MODE_ASYNCHRONOUS)
    {
        if ((pPerStPortInfo->i4NpPortStateStatus == AST_WAITING_FOR_CALLBACK) &&
            (AST_GET_SYSTEMACTION != MST_DISABLED))
        {
            AST_DBG_ARG2 (AST_TCSM_DBG,
                          "TCSM: Flush on port %s for all vlans "
                          "mapped to instance %d is not done!\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2InstanceId);
            return;
        }
    }
    if (AstVlanIsVlanEnabledInContext (AST_CURR_CONTEXT_ID ()) == VLAN_TRUE)
    {
        if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
        {
            /* Need not set Short Ageout time again for this port 
             * when ShortAgeout Duration timer is running */
            if (pCommPortInfo->pRapidAgeDurtnTmr == NULL)
            {
                AstVlanSetShortAgeoutTime (pPortInfo);

            }

            /* FdbFlush has been set. Short Ageout time should be applied
             * for a duration of FwdDelay more seconds. Hence restart the 
             * duration timer (even if it is already running) */
            /* Start the duration for FwdDelay+(100 centi-seconds) so that the
             * forwarding module gets to ageout entries atleast once at
             * the end of FwdDelay seconds before reverting back to 
             * long ageout */
            if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                               (UINT1) AST_TMR_TYPE_RAPIDAGE_DURATION,
                               (UINT2) (pPortInfo->DesgTimes.u2ForwardDelay +
                                        (1 * AST_CENTI_SECONDS))) !=
                RST_SUCCESS)
            {
                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                         "TCSM: AstStartTimer for Rapid Age duration FAILED!\n");
            }
        }
        else
        {

            /* If the VLAN learning mode is shared VLAN learning (SVL)
             * only the default fdb id will be present.Hence we flush the
             * learnt entries in that fdb Id for the port.*/

            u1VlanLearningType =
                AstVlanMiGetVlanLearningMode (AST_CURR_CONTEXT_ID ());

            if (u1VlanLearningType == VLAN_SHARED_LEARNING)
            {
                AstVlanFlushFdbEntries (u4IfIndex,
                                        VLAN_SHARED_DEF_FDBID, i4OptimizeFlag);
            }
            else
            {
                AstTopologyChSmFlushEntries (pPerStPortInfo);
            }
        }
        AST_DBG_ARG2 (AST_TCSM_DBG,
                      "TCSM: Learnt Entries on port %s for all vlans "
                      "mapped to instance %d have been flushed!\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                      u2InstanceId);
    }

}

/*****************************************************************************/
/* Function Name      : MstTopoChSmEventImpossible                           */
/*                                                                           */
/* Description        : This function is called on the occurence of an event */
/*                      that is not possible in the present state of the     */
/*                      Topology Change State Machine.                       */
/*                                                                           */
/* Input(s)           : pPerStPortInfo - The pointer to the instance-specific*/
/*                                       Port Information                    */
/*                      u2InstanceId   - Instance Info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstTopoChSmEventImpossible (UINT2 u2InstanceId,
                            tAstPerStPortInfo * pPerStPortInfo)
{
    UINT4               u4Ticks = 0;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    tAstPortEntry      *pAstPortEntry = NULL;
    pAstPortEntry = AST_GET_PORTENTRY (pPerStPortInfo->u2PortNo);

    AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "TCSM: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_DBG (AST_TCSM_DBG | AST_ALL_FAILURE_DBG,
             "TCSM: This is an IMPOSSIBLE EVENT in this State!\n");
    AST_INCR_IMPSTATE_OCCUR_COUNT (pPerStPortInfo->u2PortNo);
    AST_GET_SYS_TIME (&u4Ticks);
    pAstPortEntry->u4ImpStateOccurTimeStamp = u4Ticks;
    AST_UNUSED (u2InstanceId);
    UtlGetTimeStr (au1TimeStr);
    AST_SYS_TRC_ARG2 (AST_CONTROL_PATH_TRC,
                      "IMPOSSIBLE STATE occured at Port %s at %s \n",
                      AST_GET_IFINDEX (pPerStPortInfo->u2PortNo), au1TimeStr);
    if (AstHandleImpossibleState (pPerStPortInfo->u2PortNo) != MST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_TCSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo));

        return MST_FAILURE;
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : AstTopologyChSmFlushEntries                   */
/*                                                                           */
/*    Description               : This function performs the functionality   */
/*                                of flushing the Filtering Database to      */
/*                                remove the information that was learnt on  */
/*                                this Port for all Fids.                    */
/*                                                                           */
/*    Input(s)                  : pPerStPortInfo - Pointer to the Instance   */
/*                                                 Specific Port Information */
/*                                                 structure.                */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
AstTopologyChSmFlushEntries (tAstPerStPortInfo * pPerStPortInfo)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT4               u4IfIndex = 0;
    UINT1              *pu1VlanList = NULL;
    UINT2               u2VlanIndex = 0;
    UINT1               b1Result = OSIX_FALSE;

    pPerStInfo = AST_GET_PERST_INFO (pPerStPortInfo->u2Inst);
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (pPerStPortInfo->u2Inst);

    if ((pPerStInfo == NULL) || (pPerStBrgInfo == NULL))
    {
        return;
    }

    u4IfIndex = AST_GET_IFINDEX (pPerStPortInfo->u2PortNo);

    /*
     * When flush interval is default value ( == 0), then normal
     * flow will continue. (i.e) Port,Instance based flushing
     * will be triggered.
     *
     * When flush interval is non-default value (!= 0), then
     * the following procedures will be taken place:
     *
     * If (Flush trigger timer == Running)
     * ==> Pending flushes will be updated as TRUE
     *
     * If (Flush Trigger timer == Not_Running)
     *
     * ==> If (Flush Interval Threshold == Default_Value(0))
     *        ==> Call Instance based flush. The same flush function
     *            will be invoked with invalid port num
     *        ==> Increment the instance based flush count after the
     *            last timer expiry
     *        ==> Increment the total flush count for the instance
     *
     * ==> If (Actual_Flush_Indication_Count_After_last_timer_expiry
     *         < Flush_Interval_Threshold)
     *        ==> Call port, instance based flushing with the actual
     *            port number value.
     *        ==> Increment the instance based flush count after the
     *            last timer expiry
     *        ==> Increment the total flush count for the instance
     *
     * ==> Else If (Flush INterval Threshold == Default value (0))
     *          || (Actual_Flush_Indication_Count_After_last_timer_expiry
     *          == Flush_Interval_Threshold)
     *        ==> Start flush trigger timer
     *        ==> Update pending flushes as false
     *
     *
     * During timer expiry, instance based flushing will be invoked by
     * filling the port number as invalid port identifier.
     *
     */
    if ((gu1EnablePortVlanFlushOpt == AST_TRUE) &&
        (pPerStBrgInfo->u4FlushIndThreshold != AST_INIT_VAL) &&
        (AST_FLUSH_INTERVAL != AST_INIT_VAL))
    {
        /*Get the vlans in instance */

        /*For each port in instance do port /vlan based flushing as follows */

        /*Check if the threshold count is reached already */
        if (pPerStInfo->pFlushTgrTmr == NULL)
        {
            /*If actual count for instance has not exceeded the threshold do port/vlan flush */
            pu1VlanList = UtilVlanAllocVlanListSize (sizeof (tAstMstVlanList));

            if (pu1VlanList == NULL)
            {
                AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                         "pMstVlanList:Memory Allocation Failed\n");
                return;
            }
            AST_MEMSET (pu1VlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));

            L2IwfGetVlanListInInstance (pPerStPortInfo->u2Inst, pu1VlanList);

            for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID;
                 u2VlanIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex,
                                         VLAN_LIST_SIZE, b1Result);
                if (b1Result == OSIX_TRUE)
                {
                    if (pPerStBrgInfo->u4FlushIndCount >=
                        pPerStBrgInfo->u4FlushIndThreshold)
                    {
                        pPerStInfo->FlushFlag.u1PendingFlushes = AST_TRUE;

                    }
                    if (pPerStBrgInfo->u4FlushIndCount <
                        pPerStBrgInfo->u4FlushIndThreshold)
                    {
                        AstVlanFlushFdbEntries (AST_GET_IFINDEX
                                                (pPerStPortInfo->u2PortNo),
                                                u2VlanIndex, VLAN_NO_OPTIMIZE);

                        (pPerStBrgInfo->u4FlushIndCount)++;
                        (pPerStBrgInfo->u4TotalFlushCount)++;

                    }
                    if (pPerStBrgInfo->u4FlushIndCount ==
                        pPerStBrgInfo->u4FlushIndThreshold)
                    {
                        if (pPerStInfo->pFlushTgrTmr == NULL)
                        {
                            if (AstStartTimer ((VOID *) pPerStInfo, u2VlanIndex,
                                               (UINT1) AST_TMR_TYPE_FLUSHTGR,
                                               (UINT2) AST_FLUSH_INTERVAL) !=
                                RST_SUCCESS)
                            {
                                AST_DBG (AST_TCSM_DBG | AST_TMR_DBG |
                                         AST_ALL_FAILURE_DBG,
                                         "TCSM: AstStartTimer for flush trigger FAILED!\n");
                            }
                            pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
                            break;
                        }

                    }

                }
            }
            UtilVlanReleaseVlanListSize (pu1VlanList);
        }
        else
        {
            pPerStInfo->FlushFlag.u1PendingFlushes = AST_TRUE;
        }
    }
    else
    {

        if (AST_FLUSH_INTERVAL == AST_INIT_VAL)
        {
            if (L2IwfFlushFdbForGivenInst
                (AST_CURR_CONTEXT_ID (), u4IfIndex,
                 pPerStPortInfo->u2Inst, VLAN_OPTIMIZE, STP_MODULE,
                 TRUE) == L2IWF_FAILURE)
            {
                return;
            }
            (pPerStBrgInfo->u4TotalFlushCount)++;
        }
        else
        {
            if (pPerStInfo->pFlushTgrTmr == NULL)
            {
                if (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL)
                {
                    u4IfIndex = AST_INVALID_PORT_NUM;
                }

                if ((pPerStBrgInfo->u4FlushIndCount <
                     pPerStBrgInfo->u4FlushIndThreshold) ||
                    (pPerStBrgInfo->u4FlushIndThreshold == AST_INIT_VAL))
                {
                    if (L2IwfFlushFdbForGivenInst
                        (AST_CURR_CONTEXT_ID (), u4IfIndex,
                         pPerStPortInfo->u2Inst, VLAN_OPTIMIZE, STP_MODULE,
                         TRUE) == L2IWF_FAILURE)
                    {
                        return;
                    }

                    (pPerStBrgInfo->u4FlushIndCount)++;
                    (pPerStBrgInfo->u4TotalFlushCount)++;
                }

                if (pPerStBrgInfo->u4FlushIndCount >=
                    pPerStBrgInfo->u4FlushIndThreshold)
                {
                    if (AstStartTimer
                        ((VOID *) pPerStInfo, pPerStPortInfo->u2Inst,
                         (UINT1) AST_TMR_TYPE_FLUSHTGR,
                         (UINT2) AST_FLUSH_INTERVAL) != RST_SUCCESS)
                    {
                        AST_DBG (AST_TCSM_DBG | AST_TMR_DBG |
                                 AST_ALL_FAILURE_DBG,
                                 "TCSM: AstStartTimer for flush trigger FAILED!\n");
                    }

                    pPerStInfo->FlushFlag.u1PendingFlushes = AST_FALSE;
                }
            }
            else
            {
                pPerStInfo->FlushFlag.u1PendingFlushes = AST_TRUE;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : MstInitTopologyChangeMachine                         */
/*                                                                           */
/* Description        : Initialises the Topology Change State Machine.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
#ifdef __STDC__
VOID
MstInitTopoChStateMachine (VOID)
#else
VOID
MstInitTopoChStateMachine (VOID)
#endif
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[MST_TOPOCHSM_MAX_EVENTS][20] = {
        "BEGIN", "LEARN_SET", "NOT_DESG_ROOT", "FORWARD", "OPEREDGE_RESET",
        "OPEREDGE_SET", "RCVD_TC", "RCVD_TCN", "RCVD_TCACK", "TC_PROP"
    };
    UINT1               aau1SemState[MST_TOPOCHSM_MAX_STATES][20] = {
        "INACTIVE", "LEARNING", "DETECTED", "ACTIVE", "NOTIFIED_TCN",
        "NOTIFIED_TC", "PROPAGATING", "ACKNOWLEDGED"
    };

    for (i4Index = 0; i4Index < MST_TOPOCHSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_TCSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_TCSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < MST_TOPOCHSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_TCSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_TCSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* 802.1Q Changes */

    /* BEGIN Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = MstTopoChSmMakeInActive;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmMakeInActive;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopoChSmMakeInActive;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_BEGIN]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* LEARN_SET  Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_LEARN_SET]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* NOT_DESG_ROOT  Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmChkInactive;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_NOT_DESG_ROOT]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* FORWARD Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmChkDetected;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_FORWARD]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* OPEREDGE_RESET Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmChkDetected;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_RESET]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* OPEREDGE_SET Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_OPEREDGE_SET]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* MST_TOPOCHSM_EV_RCVDTC Event */
    /* BUG_FIX */
    /* New features Testing */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopologyChSmMakeNotifiedTc;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTC]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* MST_TOPOCHSM_EV_RCVDTCN Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopologyChSmMakeNotifiedTcn;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCN]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* MST_TOPOCHSM_EV_RCVDTCACK Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopologyChSmMakeAcknowledged;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_RCVDTCACK]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;

    /* MST_TOPOCHSM_EV_TCPROP Event */
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_INACTIVE].pAction = NULL;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_LEARNING].pAction = MstTopoChSmMakeLearning;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_DETECTED].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_ACTIVE].pAction = MstTopologyChSmMakePropagating;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_NOTIFIED_TCN].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_NOTIFIED_TC].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_PROPAGATING].pAction = MstTopoChSmEventImpossible;
    MST_TOPO_CH_MACHINE[MST_TOPOCHSM_EV_TCPROP]
        [MST_TOPOCHSM_STATE_ACKNOWLEDGED].pAction = MstTopoChSmEventImpossible;
}

#endif /* MSTP_WANTED */
