/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmimplw.c,v 1.60 2017/12/29 09:31:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "fssnmp.h"
#include  "astmlow.h"
#include   "vcm.h"
#include "asthdrs.h"
#include "stpcli.h"
#include "cli.h"
#include "astminc.h"
#include "fsmpmscli.h"

/* LOW LEVEL Routines for Table : FsMIDot1sFutureMstTable. */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIMstGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstGlobalTrace (INT4 *pi4RetValFsMIMstGlobalTrace)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalTraceOption == AST_TRUE)
    {
        *pi4RetValFsMIMstGlobalTrace = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIMstGlobalTrace = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIMstGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsMIMstGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsMIMstGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstGlobalDebug (INT4 *pi4RetValFsMIMstGlobalDebug)
{
#ifdef TRACE_WANTED
    if (gAstGlobalInfo.u4GlobalDebugOption == AST_TRUE)
    {
        *pi4RetValFsMIMstGlobalDebug = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsMIMstGlobalDebug = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIMstGlobalDebug);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstGlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIMstGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstGlobalTrace (INT4 i4SetValFsMIMstGlobalTrace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstGlobalTrace,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);

#ifdef TRACE_WANTED
    if (i4SetValFsMIMstGlobalTrace == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalTraceOption = RST_FALSE;
    }

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ", i4SetValFsMIMstGlobalTrace));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIMstGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsMIMstGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsMIMstGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstGlobalDebug (INT4 i4SetValFsMIMstGlobalDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstGlobalDebug,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);

#ifdef TRACE_WANTED
    if (i4SetValFsMIMstGlobalDebug == AST_SNMP_TRUE)
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_TRUE;
    }
    else
    {
        gAstGlobalInfo.u4GlobalDebugOption = AST_FALSE;
    }
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ", i4SetValFsMIMstGlobalDebug));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIMstGlobalDebug);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIMstGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstGlobalTrace (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMIMstGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIMstGlobalTrace != AST_SNMP_TRUE) &&
        (i4TestValFsMIMstGlobalTrace != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIMstGlobalTrace);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsMIMstGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstGlobalDebug (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMIMstGlobalDebug)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIMstGlobalDebug != AST_SNMP_TRUE) &&
        (i4TestValFsMIMstGlobalDebug != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIMstGlobalDebug);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstGlobalTrace (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIMstGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstGlobalDebug (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1sFutureMstTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1sFutureMstTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1sFutureMstTable (INT4
                                                 i4FsMIDot1sFutureMstContextId)
{

    if (AST_IS_VC_VALID (i4FsMIDot1sFutureMstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1sFutureMstTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1sFutureMstTable (INT4 *pi4FsMIDot1sFutureMstContextId)
{
    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIDot1sFutureMstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1sFutureMstTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                nextFsMIDot1sFutureMstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1sFutureMstTable (INT4 i4FsMIDot1sFutureMstContextId,
                                        INT4
                                        *pi4NextFsMIDot1sFutureMstContextId)
{
    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIDot1sFutureMstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIDot1sFutureMstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstSystemControl
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstSystemControl (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 *pi4RetValFsMIMstSystemControl)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstSystemControl (pi4RetValFsMIMstSystemControl);

    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
Function    :  nmhGetFsMIMstModuleStatus
Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstModuleStatus (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 *pi4RetValFsMIMstModuleStatus)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstModuleStatus (pi4RetValFsMIMstModuleStatus);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
Function    :  nmhGetFsMIMstMaxMstInstanceNumber
Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMaxMstInstanceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMaxMstInstanceNumber (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 *pi4RetValFsMIMstMaxMstInstanceNumber)
{

    INT1                i1RetVal;
    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMaxMstInstanceNumber (pi4RetValFsMIMstMaxMstInstanceNumber);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstNoOfMstiSupported
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstNoOfMstiSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstNoOfMstiSupported (INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 *pi4RetValFsMIMstNoOfMstiSupported)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstNoOfMstiSupported (pi4RetValFsMIMstNoOfMstiSupported);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMaxHopCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMaxHopCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMaxHopCount (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 *pi4RetValFsMIMstMaxHopCount)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMaxHopCount (pi4RetValFsMIMstMaxHopCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstBrgAddress
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstBrgAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstBrgAddress (INT4 i4FsMIDot1sFutureMstContextId,
                         tMacAddr * pRetValFsMIMstBrgAddress)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstBrgAddress (pRetValFsMIMstBrgAddress);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistRoot
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistRoot (INT4 i4FsMIDot1sFutureMstContextId,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMIMstCistRoot)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistRoot (pRetValFsMIMstCistRoot);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistRegionalRoot
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistRegionalRoot (INT4 i4FsMIDot1sFutureMstContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIMstCistRegionalRoot)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistRegionalRoot (pRetValFsMIMstCistRegionalRoot);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistRootCost
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistRootCost (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 *pi4RetValFsMIMstCistRootCost)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistRootCost (pi4RetValFsMIMstCistRootCost);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistRegionalRootCost
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistRegionalRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistRegionalRootCost (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 *pi4RetValFsMIMstCistRegionalRootCost)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistRegionalRootCost (pi4RetValFsMIMstCistRegionalRootCost);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistRootPort
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistRootPort (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 *pi4RetValFsMIMstCistRootPort)
{

    INT1                i1RetVal;
    INT4                i4LocalPortNum;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistRootPort (&i4LocalPortNum);

    if ((i1RetVal != SNMP_FAILURE) && (i4LocalPortNum != 0))
    {
        *pi4RetValFsMIMstCistRootPort = (INT4) AST_GET_IFINDEX (i4LocalPortNum);
    }
    else
    {
        *pi4RetValFsMIMstCistRootPort = 0;
    }

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistBridgePriority (INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 *pi4RetValFsMIMstCistBridgePriority)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistBridgePriority (pi4RetValFsMIMstCistBridgePriority);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistBridgeMaxAge
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistBridgeMaxAge (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 *pi4RetValFsMIMstCistBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistBridgeMaxAge (pi4RetValFsMIMstCistBridgeMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistBridgeForwardDelay
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistBridgeForwardDelay (INT4 i4FsMIDot1sFutureMstContextId,
                                     INT4
                                     *pi4RetValFsMIMstCistBridgeForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistBridgeForwardDelay
        (pi4RetValFsMIMstCistBridgeForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistHoldTime
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistHoldTime (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 *pi4RetValFsMIMstCistHoldTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistHoldTime (pi4RetValFsMIMstCistHoldTime);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistMaxAge
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistMaxAge (INT4 i4FsMIDot1sFutureMstContextId,
                         INT4 *pi4RetValFsMIMstCistMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistMaxAge (pi4RetValFsMIMstCistMaxAge);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistForwardDelay
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistForwardDelay (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 *pi4RetValFsMIMstCistForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistForwardDelay (pi4RetValFsMIMstCistForwardDelay);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstpUpCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstpUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstpUpCount (INT4 i4FsMIDot1sFutureMstContextId,
                          UINT4 *pu4RetValFsMIMstMstpUpCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstpUpCount (pu4RetValFsMIMstMstpUpCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstpDownCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstpDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstpDownCount (INT4 i4FsMIDot1sFutureMstContextId,
                            UINT4 *pu4RetValFsMIMstMstpDownCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstpDownCount (pu4RetValFsMIMstMstpDownCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstTrace
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstTrace (INT4 i4FsMIDot1sFutureMstContextId,
                    INT4 *pi4RetValFsMIMstTrace)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstTrace (pi4RetValFsMIMstTrace);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstDebug
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstDebug (INT4 i4FsMIDot1sFutureMstContextId,
                    INT4 *pi4RetValFsMIMstDebug)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstDebug (pi4RetValFsMIMstDebug);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstForceProtocolVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstForceProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstForceProtocolVersion (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 *pi4RetValFsMIMstForceProtocolVersion)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstForceProtocolVersion (pi4RetValFsMIMstForceProtocolVersion);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstTxHoldCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstTxHoldCount (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 *pi4RetValFsMIMstTxHoldCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstTxHoldCount (pi4RetValFsMIMstTxHoldCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiConfigIdSel
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstiConfigIdSel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiConfigIdSel (INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 *pi4RetValFsMIMstMstiConfigIdSel)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiConfigIdSel (pi4RetValFsMIMstMstiConfigIdSel);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiRegionName
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstiRegionName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiRegionName (INT4 i4FsMIDot1sFutureMstContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIMstMstiRegionName)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiRegionName (pRetValFsMIMstMstiRegionName);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiRegionVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstiRegionVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiRegionVersion (INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 *pi4RetValFsMIMstMstiRegionVersion)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiRegionVersion (pi4RetValFsMIMstMstiRegionVersion);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiConfigDigest
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMstiConfigDigest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiConfigDigest (INT4 i4FsMIDot1sFutureMstContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIMstMstiConfigDigest)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiConfigDigest (pRetValFsMIMstMstiConfigDigest);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstBufferOverFlowCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstBufferOverFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstBufferOverFlowCount (INT4 i4FsMIDot1sFutureMstContextId,
                                  UINT4 *pu4RetValFsMIMstBufferOverFlowCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstBufferOverFlowCount (pu4RetValFsMIMstBufferOverFlowCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMemAllocFailureCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMemAllocFailureCount (INT4 i4FsMIDot1sFutureMstContextId,
                                   UINT4 *pu4RetValFsMIMstMemAllocFailureCount)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMemAllocFailureCount (pu4RetValFsMIMstMemAllocFailureCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstRegionConfigChangeCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstRegionConfigChangeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstRegionConfigChangeCount (INT4 i4FsMIDot1sFutureMstContextId,
                                      UINT4
                                      *pu4RetValFsMIMstRegionConfigChangeCount)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstRegionConfigChangeCount
        (pu4RetValFsMIMstRegionConfigChangeCount);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistBridgeRoleSelectionSemState
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistBridgeRoleSelectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistBridgeRoleSelectionSemState (INT4
                                              i4FsMIDot1sFutureMstContextId,
                                              INT4
                                              *pi4RetValFsMIMstCistBridgeRoleSelectionSemState)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistBridgeRoleSelectionSemState
        (pi4RetValFsMIMstCistBridgeRoleSelectionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistTimeSinceTopologyChange
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistTimeSinceTopologyChange (INT4 i4FsMIDot1sFutureMstContextId,
                                          UINT4
                                          *pu4RetValFsMIMstCistTimeSinceTopologyChange)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistTimeSinceTopologyChange
        (pu4RetValFsMIMstCistTimeSinceTopologyChange);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistTopChanges
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistTopChanges (INT4 i4FsMIDot1sFutureMstContextId,
                             UINT4 *pu4RetValFsMIMstCistTopChanges)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistTopChanges (pu4RetValFsMIMstCistTopChanges);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistNewRootBridgeCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistNewRootBridgeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistNewRootBridgeCount (INT4 i4FsMIDot1sFutureMstContextId,
                                     UINT4
                                     *pu4RetValFsMIMstCistNewRootBridgeCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistNewRootBridgeCount
        (pu4RetValFsMIMstCistNewRootBridgeCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistHelloTime
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistHelloTime (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 *pi4RetValFsMIMstCistHelloTime)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistHelloTime (pi4RetValFsMIMstCistHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistBridgeHelloTime
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistBridgeHelloTime (INT4 i4FsMIDot1sFutureMstContextId,
                                  INT4 *pi4RetValFsMIMstCistBridgeHelloTime)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistBridgeHelloTime
        (pi4RetValFsMIMstCistBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistDynamicPathcostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistDynamicPathcostCalculation (INT4 i4FsMIDot1sFutureMstContextId,
                                             INT4
                                             *pi4RetValFsMIMstCistDynamicPathcostCalculation)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistDynamicPathcostCalculation
        (pi4RetValFsMIMstCistDynamicPathcostCalculation);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstContextName
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                retValFsMIMstContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstContextName (INT4 i4FsMIDot1sFutureMstContextId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIMstContextName)
{
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];

    AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

    if (AST_IS_VC_VALID (i4FsMIDot1sFutureMstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    AstVcmGetAliasName (i4FsMIDot1sFutureMstContextId, au1ContextName);

    AST_MEMCPY (pRetValFsMIMstContextName->pu1_OctetList,
                au1ContextName, AST_STRLEN (au1ContextName));
    pRetValFsMIMstContextName->i4_Length = AST_STRLEN (au1ContextName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCalcPortPathCostOnSpeedChg (INT4 i4FsMIDot1sFutureMstContextId,
                                         INT4
                                         *pi4RetValFsMIMstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCalcPortPathCostOnSpeedChg
        (pi4RetValFsMIMstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstRcvdEvent
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstRcvdEvent (INT4 i4FsMIDot1sFutureMstContextId,
                        INT4 *pi4RetValFsMIMstRcvdEvent)
{
    INT1                i1RetVal = 0;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstRcvdEvent (pi4RetValFsMIMstRcvdEvent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstRcvdEventSubType
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstRcvdEventSubType (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 *pi4RetValFsMIMstRcvdEventSubType)
{
    INT1                i1RetVal = 0;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstRcvdEventSubType (pi4RetValFsMIMstRcvdEventSubType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstRcvdEventTimeStamp
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstRcvdEventTimeStamp (INT4 i4FsMIDot1sFutureMstContextId,
                                 UINT4 *pu4RetValFsMIMstRcvdEventTimeStamp)
{
    INT1                i1RetVal = 0;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstRcvdEventTimeStamp
        (pu4RetValFsMIMstRcvdEventTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstPortStateChangeTimeStamp
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPortStateChangeTimeStamp (INT4 i4FsMIDot1sFutureMstContextId,
                                       UINT4
                                       *pu4RetValFsMIMstPortStateChangeTimeStamp)
{
    INT1                i1RetVal = 0;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortStateChangeTimeStamp
        (pu4RetValFsMIMstPortStateChangeTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstFlushInterval
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstFlushInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstFlushInterval (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 *pi4RetValFsMIMstFlushInterval)
{
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMIMstFlushInterval = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsMIMstFlushInterval = (INT4) AST_FLUSH_INTERVAL;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistFlushIndicationThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistFlushIndicationThreshold (INT4 i4FsMIDot1sFutureMstContextId,
                                           INT4
                                           *pi4RetValFsMIMstCistFlushIndicationThreshold)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMIMstCistFlushIndicationThreshold = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pi4RetValFsMIMstCistFlushIndicationThreshold =
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistTotalFlushCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstCistTotalFlushCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistTotalFlushCount (INT4 i4FsMIDot1sFutureMstContextId,
                                  UINT4 *pu4RetValFsMIMstCistTotalFlushCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMIMstCistTotalFlushCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    *pu4RetValFsMIMstCistTotalFlushCount =
        pAstPerStBridgeInfo->u4TotalFlushCount;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstBpduGuard
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                retValFsMIMstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstBpduGuard (INT4 i4FsMIDot1sFutureMstContextId,
                        INT4 *pi4RetValFsMIMstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstBpduGuard (pi4RetValFsMIMstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function   :  nmhGetFsMIMstStpPerfStatus
 Input      :  The Indices
               FsMIDot1sFutureMstContextId
 
               The Object
               retValFsMIMstStpPerfStatus
 Output     :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
 Returns    :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstStpPerfStatus (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 *pi4RetValFsMIMstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1sFutureMstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstStpPerfStatus (pi4RetValFsMIMstStpPerfStatus);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstPortsMap
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                retValFsMIMstInstPortsMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstPortsMap (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 *pi4RetValFsMIMstInstPortsMap)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstPortsMap (pi4RetValFsMIMstInstPortsMap);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstSystemControl
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstSystemControl (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4SetValFsMIMstSystemControl)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstSystemControl (i4SetValFsMIMstSystemControl);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstModuleStatus
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstModuleStatus (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4SetValFsMIMstModuleStatus)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstModuleStatus (i4SetValFsMIMstModuleStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMaxMstInstanceNumber
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstMaxMstInstanceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMaxMstInstanceNumber (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4SetValFsMIMstMaxMstInstanceNumber)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMaxMstInstanceNumber
        (i4SetValFsMIMstMaxMstInstanceNumber);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMaxHopCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstMaxHopCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMaxHopCount (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 i4SetValFsMIMstMaxHopCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMaxHopCount (i4SetValFsMIMstMaxHopCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistBridgePriority (INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 i4SetValFsMIMstCistBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistBridgePriority
        (i4SetValFsMIMstCistBridgePriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistBridgeMaxAge
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistBridgeMaxAge (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4SetValFsMIMstCistBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistBridgeMaxAge (i4SetValFsMIMstCistBridgeMaxAge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistBridgeForwardDelay
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistBridgeForwardDelay (INT4 i4FsMIDot1sFutureMstContextId,
                                     INT4 i4SetValFsMIMstCistBridgeForwardDelay)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistBridgeForwardDelay
        (i4SetValFsMIMstCistBridgeForwardDelay);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstTrace
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstTrace (INT4 i4FsMIDot1sFutureMstContextId,
                    INT4 i4SetValFsMIMstTrace)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstTrace (i4SetValFsMIMstTrace);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstDebug
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstDebug (INT4 i4FsMIDot1sFutureMstContextId,
                    INT4 i4SetValFsMIMstDebug)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstDebug (i4SetValFsMIMstDebug);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstForceProtocolVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstForceProtocolVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstForceProtocolVersion (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4SetValFsMIMstForceProtocolVersion)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstForceProtocolVersion
        (i4SetValFsMIMstForceProtocolVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstTxHoldCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstTxHoldCount (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 i4SetValFsMIMstTxHoldCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstTxHoldCount (i4SetValFsMIMstTxHoldCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiConfigIdSel
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstMstiConfigIdSel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiConfigIdSel (INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 i4SetValFsMIMstMstiConfigIdSel)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMstiConfigIdSel (i4SetValFsMIMstMstiConfigIdSel);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiRegionName
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstMstiRegionName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiRegionName (INT4 i4FsMIDot1sFutureMstContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMIMstMstiRegionName)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMstiRegionName (pSetValFsMIMstMstiRegionName);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiRegionVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstMstiRegionVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiRegionVersion (INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 i4SetValFsMIMstMstiRegionVersion)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMstiRegionVersion (i4SetValFsMIMstMstiRegionVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistBridgeHelloTime
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistBridgeHelloTime (INT4 i4FsMIDot1sFutureMstContextId,
                                  INT4 i4SetValFsMIMstCistBridgeHelloTime)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistBridgeHelloTime
        (i4SetValFsMIMstCistBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistDynamicPathcostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistDynamicPathcostCalculation (INT4
                                             i4FsMIDot1sFutureMstContextId,
                                             INT4
                                             i4SetValFsMIMstCistDynamicPathcostCalculation)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistDynamicPathcostCalculation
        (i4SetValFsMIMstCistDynamicPathcostCalculation);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCalcPortPathCostOnSpeedChg (INT4 i4FsMIDot1sFutureMstContextId,
                                         INT4
                                         i4SetValFsMIMstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCalcPortPathCostOnSpeedChg
        (i4SetValFsMIMstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstFlushInterval
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstFlushInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstFlushInterval (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4SetValFsMIMstFlushInterval)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIMstFlushInterval == (INT4) AST_FLUSH_INTERVAL)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    AST_FLUSH_INTERVAL = (UINT4) i4SetValFsMIMstFlushInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstFlushInterval,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIMstFlushInterval));

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstCistFlushIndicationThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistFlushIndicationThreshold (INT4 i4FsMIDot1sFutureMstContextId,
                                           INT4
                                           i4SetValFsMIMstCistFlushIndicationThreshold)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo = AST_GET_PER_ST_BRG_INFO_PTR (MST_CIST_CONTEXT);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIMstCistFlushIndicationThreshold ==
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo->u4FlushIndThreshold =
        (UINT4) i4SetValFsMIMstCistFlushIndicationThreshold;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstCistFlushIndicationThreshold,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIMstCistFlushIndicationThreshold));

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstBpduGuard
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                setValFsMIMstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstBpduGuard (INT4 i4FsMIDot1sFutureMstContextId,
                        INT4 i4SetValFsMIMstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstBpduGuard (i4SetValFsMIMstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstStpPerfStatus
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 
                The Object
                setValFsMIMstStpPerfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstStpPerfStatus (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4SetValFsMIMstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1sFutureMstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstStpPerfStatus (i4SetValFsMIMstStpPerfStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstInstPortsMap
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                setValFsMIMstInstPortsMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstInstPortsMap (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4SetValFsMIMstInstPortsMap)
{

    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstInstPortsMap (i4SetValFsMIMstInstPortsMap);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstSystemControl
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstSystemControl (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4TestValFsMIMstSystemControl)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstSystemControl
        (pu4ErrorCode, i4TestValFsMIMstSystemControl);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstModuleStatus
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstModuleStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 i4TestValFsMIMstModuleStatus)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstModuleStatus
        (pu4ErrorCode, i4TestValFsMIMstModuleStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMaxMstInstanceNumber
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstMaxMstInstanceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMaxMstInstanceNumber (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIDot1sFutureMstContextId,
                                      INT4 i4TestValFsMIMstMaxMstInstanceNumber)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMaxMstInstanceNumber
        (pu4ErrorCode, i4TestValFsMIMstMaxMstInstanceNumber);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMaxHopCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstMaxHopCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMaxHopCount (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4TestValFsMIMstMaxHopCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMaxHopCount
        (pu4ErrorCode, i4TestValFsMIMstMaxHopCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistBridgePriority (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIDot1sFutureMstContextId,
                                    INT4 i4TestValFsMIMstCistBridgePriority)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistBridgePriority
        (pu4ErrorCode, i4TestValFsMIMstCistBridgePriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistBridgeMaxAge
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistBridgeMaxAge (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIDot1sFutureMstContextId,
                                  INT4 i4TestValFsMIMstCistBridgeMaxAge)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistBridgeMaxAge
        (pu4ErrorCode, i4TestValFsMIMstCistBridgeMaxAge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistBridgeForwardDelay
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIDot1sFutureMstContextId,
                                        INT4
                                        i4TestValFsMIMstCistBridgeForwardDelay)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistBridgeForwardDelay
        (pu4ErrorCode, i4TestValFsMIMstCistBridgeForwardDelay);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstTrace
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstTrace (UINT4 *pu4ErrorCode, INT4 i4FsMIDot1sFutureMstContextId,
                       INT4 i4TestValFsMIMstTrace)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstTrace (pu4ErrorCode, i4TestValFsMIMstTrace);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstDebug
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstDebug (UINT4 *pu4ErrorCode, INT4 i4FsMIDot1sFutureMstContextId,
                       INT4 i4TestValFsMIMstDebug)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstDebug (pu4ErrorCode, i4TestValFsMIMstDebug);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstForceProtocolVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstForceProtocolVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstForceProtocolVersion (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIDot1sFutureMstContextId,
                                      INT4 i4TestValFsMIMstForceProtocolVersion)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstForceProtocolVersion
        (pu4ErrorCode, i4TestValFsMIMstForceProtocolVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstTxHoldCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstTxHoldCount (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4TestValFsMIMstTxHoldCount)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstTxHoldCount
        (pu4ErrorCode, i4TestValFsMIMstTxHoldCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiConfigIdSel
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstMstiConfigIdSel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiConfigIdSel (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 i4TestValFsMIMstMstiConfigIdSel)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMstiConfigIdSel
        (pu4ErrorCode, i4TestValFsMIMstMstiConfigIdSel);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiRegionName
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstMstiRegionName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiRegionName (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIDot1sFutureMstContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsMIMstMstiRegionName)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMstiRegionName
        (pu4ErrorCode, pTestValFsMIMstMstiRegionName);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiRegionVersion
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstMstiRegionVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiRegionVersion (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4TestValFsMIMstMstiRegionVersion)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMstiRegionVersion
        (pu4ErrorCode, i4TestValFsMIMstMstiRegionVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistBridgeHelloTime
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistBridgeHelloTime (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIDot1sFutureMstContextId,
                                     INT4 i4TestValFsMIMstCistBridgeHelloTime)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistBridgeHelloTime
        (pu4ErrorCode, i4TestValFsMIMstCistBridgeHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistDynamicPathcostCalculation
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistDynamicPathcostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistDynamicPathcostCalculation (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsMIDot1sFutureMstContextId,
                                                INT4
                                                i4TestValFsMIMstCistDynamicPathcostCalculation)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistDynamicPathcostCalculation
        (pu4ErrorCode, i4TestValFsMIMstCistDynamicPathcostCalculation);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCalcPortPathCostOnSpeedChg
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1sFutureMstContextId,
                                            INT4
                                            i4TestValFsMIMstCalcPortPathCostOnSpeedChg)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCalcPortPathCostOnSpeedChg
        (pu4ErrorCode, i4TestValFsMIMstCalcPortPathCostOnSpeedChg);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstFlushInterval
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstFlushInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstFlushInterval (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4TestValFsMIMstFlushInterval)
{
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIMstFlushInterval < AST_FLUSH_INTERVAL_MIN_VAL) ||
        (i4TestValFsMIMstFlushInterval > AST_FLUSH_INTERVAL_MAX_VAL))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstCistFlushIndicationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistFlushIndicationThreshold (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4FsMIDot1sFutureMstContextId,
                                              INT4
                                              i4TestValFsMIMstCistFlushIndicationThreshold)
{
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIMstCistFlushIndicationThreshold < AST_MIN_FLUSH_IND) ||
        (i4TestValFsMIMstCistFlushIndicationThreshold > AST_MAX_FLUSH_IND))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstBpduGuard
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                                                                                                                               The Object
                testValFsMIMstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstBpduGuard (UINT4 *pu4ErrorCode,
                           INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4TestValFsMIMstBpduGuard)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstBpduGuard (pu4ErrorCode, i4TestValFsMIMstBpduGuard);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstStpPerfStatus
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                testValFsMIMstStpPerfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstStpPerfStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4TestValFsMIMstStpPerfStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i4FsMIDot1sFutureMstContextId = (INT4) AST_CURR_CONTEXT_ID ();

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstStpPerfStatus (pu4ErrorCode,
                                     i4TestValFsMIMstStpPerfStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstInstPortsMap
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object
                testValFsMIMstInstPortsMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstInstPortsMap (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 i4TestValFsMIMstInstPortsMap)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstInstPortsMap (pu4ErrorCode, i4TestValFsMIMstInstPortsMap);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1sFutureMstTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1sFutureMstTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstMstiBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstMstiBridgeTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstMstiBridgeTable (INT4
                                                i4FsMIDot1sFutureMstContextId,
                                                INT4 i4FsMIMstMstiInstanceIndex)
{

    if (AST_IS_VC_VALID (i4FsMIDot1sFutureMstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsMstMstiBridgeTable
            (i4FsMIMstMstiInstanceIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstMstiBridgeTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstMstiBridgeTable (INT4 *pi4FsMIDot1sFutureMstContextId,
                                        INT4 *pi4FsMIMstMstiInstanceIndex)
{
    return
        nmhGetNextIndexFsMIMstMstiBridgeTable (0,
                                               pi4FsMIDot1sFutureMstContextId,
                                               0, pi4FsMIMstMstiInstanceIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexFsMIMstMstiBridgeTable
Input       :  The Indices
                FsMIDot1sFutureMstContextId
                nextFsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex
                nextFsMIMstMstiInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIMstMstiBridgeTable (INT4 i4FsMIDot1sFutureMstContextId,
                                       INT4 *pi4NextFsMIDot1sFutureMstContextId,
                                       INT4 i4FsMIMstMstiInstanceIndex,
                                       INT4 *pi4NextFsMIMstMstiInstanceIndex)
{
    UINT4               u4PrevContextId;
    UINT4               u4ContextId = i4FsMIDot1sFutureMstContextId;

    if ((UINT4) i4FsMIDot1sFutureMstContextId >= AST_SIZING_CONTEXT_COUNT)
    {
        return SNMP_FAILURE;
    }

    if (AST_CONTEXT_INFO (i4FsMIDot1sFutureMstContextId) == NULL)
    {
        /* Given context is not valid - get the next valid context */

        if (AstGetNextActiveContext ((UINT4) i4FsMIDot1sFutureMstContextId,
                                     &u4ContextId) == RST_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIMstMstiInstanceIndex = 0;
    }

    do
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexFsMstMstiBridgeTable (i4FsMIMstMstiInstanceIndex,
                                                 pi4NextFsMIMstMstiInstanceIndex)
            == SNMP_SUCCESS)
        {
            *pi4NextFsMIDot1sFutureMstContextId = (INT4) u4ContextId;

            AstReleaseContext ();

            return SNMP_SUCCESS;
        }

        u4PrevContextId = u4ContextId;
        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIMstMstiInstanceIndex = 0;

        AstReleaseContext ();

    }
    while (AstGetNextActiveContext (u4PrevContextId, &u4ContextId)
           != RST_FAILURE);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiBridgeRegionalRoot
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiBridgeRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiBridgeRegionalRoot (INT4 i4FsMIDot1sFutureMstContextId,
                                     INT4 i4FsMIMstMstiInstanceIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIMstMstiBridgeRegionalRoot)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiBridgeRegionalRoot
        (i4FsMIMstMstiInstanceIndex, pRetValFsMIMstMstiBridgeRegionalRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiBridgePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiBridgePriority (INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 i4FsMIMstMstiInstanceIndex,
                                 INT4 *pi4RetValFsMIMstMstiBridgePriority)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiBridgePriority
        (i4FsMIMstMstiInstanceIndex, pi4RetValFsMIMstMstiBridgePriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiRootCost
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiRootCost (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4FsMIMstMstiInstanceIndex,
                           INT4 *pi4RetValFsMIMstMstiRootCost)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiRootCost
        (i4FsMIMstMstiInstanceIndex, pi4RetValFsMIMstMstiRootCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiRootPort
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiRootPort (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4FsMIMstMstiInstanceIndex,
                           INT4 *pi4RetValFsMIMstMstiRootPort)
{

    INT1                i1RetVal;
    INT4                i4LocalPortNum;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiRootPort
        (i4FsMIMstMstiInstanceIndex, &i4LocalPortNum);

    if ((i1RetVal != SNMP_FAILURE) && (i4LocalPortNum != 0))
    {
        *pi4RetValFsMIMstMstiRootPort = AST_GET_IFINDEX (i4LocalPortNum);
    }
    else
    {
        *pi4RetValFsMIMstMstiRootPort = 0;
    }

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiTimeSinceTopologyChange
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiTimeSinceTopologyChange (INT4 i4FsMIDot1sFutureMstContextId,
                                          INT4 i4FsMIMstMstiInstanceIndex,
                                          UINT4
                                          *pu4RetValFsMIMstMstiTimeSinceTopologyChange)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiTimeSinceTopologyChange
        (i4FsMIMstMstiInstanceIndex,
         pu4RetValFsMIMstMstiTimeSinceTopologyChange);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiTopChanges
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiTopChanges (INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4FsMIMstMstiInstanceIndex,
                             UINT4 *pu4RetValFsMIMstMstiTopChanges)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiTopChanges
        (i4FsMIMstMstiInstanceIndex, pu4RetValFsMIMstMstiTopChanges);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiNewRootBridgeCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiNewRootBridgeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiNewRootBridgeCount (INT4 i4FsMIDot1sFutureMstContextId,
                                     INT4 i4FsMIMstMstiInstanceIndex,
                                     UINT4
                                     *pu4RetValFsMIMstMstiNewRootBridgeCount)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiNewRootBridgeCount
        (i4FsMIMstMstiInstanceIndex, pu4RetValFsMIMstMstiNewRootBridgeCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiBridgeRoleSelectionSemState
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiBridgeRoleSelectionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiBridgeRoleSelectionSemState (INT4
                                              i4FsMIDot1sFutureMstContextId,
                                              INT4 i4FsMIMstMstiInstanceIndex,
                                              INT4
                                              *pi4RetValFsMIMstMstiBridgeRoleSelectionSemState)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiBridgeRoleSelectionSemState
        (i4FsMIMstMstiInstanceIndex,
         pi4RetValFsMIMstMstiBridgeRoleSelectionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceUpCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstInstanceUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceUpCount (INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 i4FsMIMstMstiInstanceIndex,
                              UINT4 *pu4RetValFsMIMstInstanceUpCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceUpCount
        (i4FsMIMstMstiInstanceIndex, pu4RetValFsMIMstInstanceUpCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceDownCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstInstanceDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceDownCount (INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 i4FsMIMstMstiInstanceIndex,
                                UINT4 *pu4RetValFsMIMstInstanceDownCount)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceDownCount
        (i4FsMIMstMstiInstanceIndex, pu4RetValFsMIMstInstanceDownCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstOldDesignatedRoot
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstOldDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstOldDesignatedRoot (INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 i4FsMIMstMstiInstanceIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIMstOldDesignatedRoot)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstOldDesignatedRoot
        (i4FsMIMstMstiInstanceIndex, pRetValFsMIMstOldDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiFlushIndicationThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiFlushIndicationThreshold (INT4 i4FsMIDot1sFutureMstContextId,
                                           INT4 i4FsMIMstMstiInstanceIndex,
                                           INT4
                                           *pi4RetValFsMIMstMstiFlushIndicationThreshold)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ())
        || (i4FsMIMstMstiInstanceIndex == AST_TE_MSTID))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMIMstMstiFlushIndicationThreshold = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    *pi4RetValFsMIMstMstiFlushIndicationThreshold =
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiTotalFlushCount
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiTotalFlushCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiTotalFlushCount (INT4 i4FsMIDot1sFutureMstContextId,
                                  INT4 i4FsMIMstMstiInstanceIndex,
                                  UINT4 *pu4RetValFsMIMstMstiTotalFlushCount)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4RetValFsMIMstMstiTotalFlushCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    *pu4RetValFsMIMstMstiTotalFlushCount =
        pAstPerStBridgeInfo->u4TotalFlushCount;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstiRootPriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object
                retValFsMIMstiRootPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstiRootPriority (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4FsMIMstMstiInstanceIndex,
                            INT4 *pi4RetValFsMIMstiRootPriority)
{

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pi4RetValFsMIMstiRootPriority = AST_INIT_VAL;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMIMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIMstiRootPriority = pAstPerStBridgeInfo->u1RootPriority;

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                setValFsMIMstMstiBridgePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiBridgePriority (INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 i4FsMIMstMstiInstanceIndex,
                                 INT4 i4SetValFsMIMstMstiBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstMstiBridgePriority
        (i4FsMIMstMstiInstanceIndex, i4SetValFsMIMstMstiBridgePriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                setValFsMIMstMstiFlushIndicationThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiFlushIndicationThreshold (INT4 i4FsMIDot1sFutureMstContextId,
                                           INT4 i4FsMIMstMstiInstanceIndex,
                                           INT4
                                           i4SetValFsMIMstMstiFlushIndicationThreshold)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIMstMstiFlushIndicationThreshold ==
        (INT4) pAstPerStBridgeInfo->u4FlushIndThreshold)
    {
        AstReleaseContext ();
        return SNMP_SUCCESS;
    }

    pAstPerStBridgeInfo->u4FlushIndThreshold =
        (UINT4) i4SetValFsMIMstMstiFlushIndicationThreshold;

    RM_GET_SEQ_NUM (&u4SeqNum);

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIMstMstiFlushIndicationThreshold,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsMIMstMstiFlushIndicationThreshold));

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstiRootPriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object
                setValFsMIMstiRootPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstiRootPriority (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4FsMIMstMstiInstanceIndex,
                            INT4 i4SetValFsMIMstiRootPriority)
{

    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tSNMP_OCTET_STRING_TYPE RegRoot;
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    tAstMacAddr         MacAddress;

    MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RegRoot.pu1_OctetList = &au1RootBrgIdBuf[0];
    RegRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;
    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIMstiRootPriority == CLI_STP_ROOT_PRIMARY)
    {
        pAstPerStBridgeInfo->u1RootPriority = CLI_STP_ROOT_PRIMARY;
        nmhGetFsMstBrgAddress (&MacAddress);
        nmhGetFsMstMstiBridgeRegionalRoot (i4FsMIMstMstiInstanceIndex,
                                           &RegRoot);
        if (AST_MEMCMP
            ((RegRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE), MacAddress,
             AST_MAC_ADDR_SIZE) == 0)
        {
            AstReleaseContext ();
            return SNMP_SUCCESS;

        }

        i4SetValFsMIMstiRootPriority = AstGetBrgPrioFromBrgId (RegRoot);
        if (i4SetValFsMIMstiRootPriority > CLI_STP_ROOT_PRIMARY_PRIORITY)
        {
            i4SetValFsMIMstiRootPriority = CLI_STP_ROOT_PRIMARY_PRIORITY;
        }
        else
        {
            i4SetValFsMIMstiRootPriority = i4SetValFsMIMstiRootPriority - 4096;
        }
    }
    else if (i4SetValFsMIMstiRootPriority == CLI_STP_ROOT_SECONDARY)
    {
        pAstPerStBridgeInfo->u1RootPriority = CLI_STP_ROOT_SECONDARY;
        i4SetValFsMIMstiRootPriority = CLI_STP_ROOT_SECONDARY_PRIORITY;
    }
    else
    {
        if ((pAstPerStBridgeInfo->u1RootPriority == CLI_STP_ROOT_PRIMARY) ||
            (pAstPerStBridgeInfo->u1RootPriority == CLI_STP_ROOT_SECONDARY))
        {
            i4SetValFsMIMstiRootPriority = CLI_STP_ROOT_DEFAULT_PRIORITY;
        }
        else
        {
            i4SetValFsMIMstiRootPriority = pAstPerStBridgeInfo->u2BrgPriority;
        }
        pAstPerStBridgeInfo->u1RootPriority = CLI_STP_ROOT_DEFAULT;

    }
    if (MstConfigBridgePriority
        (i4FsMIMstMstiInstanceIndex,
         i4SetValFsMIMstiRootPriority) == MST_FAILURE)
    {
        pAstPerStBridgeInfo->u1RootPriority = CLI_STP_ROOT_DEFAULT;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiBridgePriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                testValFsMIMstMstiBridgePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiBridgePriority (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIDot1sFutureMstContextId,
                                    INT4 i4FsMIMstMstiInstanceIndex,
                                    INT4 i4TestValFsMIMstMstiBridgePriority)
{
    INT1                i1RetVal;

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMstiBridgePriority
        (pu4ErrorCode, i4FsMIMstMstiInstanceIndex,
         i4TestValFsMIMstMstiBridgePriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiFlushIndicationThreshold
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                testValFsMIMstMstiFlushIndicationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiFlushIndicationThreshold (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4FsMIDot1sFutureMstContextId,
                                              INT4 i4FsMIMstMstiInstanceIndex,
                                              INT4
                                              i4TestValFsMIMstMstiFlushIndicationThreshold)
{
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMIMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AstReleaseContext ();
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIMstMstiFlushIndicationThreshold < AST_MIN_FLUSH_IND) ||
        (i4TestValFsMIMstMstiFlushIndicationThreshold > AST_MAX_FLUSH_IND))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstiRootPriority
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object
                testValFsMIMstiRootPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstiRootPriority (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4FsMIMstMstiInstanceIndex,
                               INT4 i4TestValFsMIMstiRootPriority)
{
    tAstPerStBridgeInfo *pAstPerStBridgeInfo = NULL;
    tSNMP_OCTET_STRING_TYPE RegRoot;
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    tAstMacAddr         MacAddress;

    MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RegRoot.pu1_OctetList = &au1RootBrgIdBuf[0];
    RegRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;
    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));

    if (AstSelectContext ((UINT4) i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pAstPerStBridgeInfo =
        AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);

    if (pAstPerStBridgeInfo == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (!(AST_IS_MST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: MSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_COMPATIBILITY_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4FsMIMstMstiInstanceIndex == MST_CIST_CONTEXT)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Instance Id is Zero !\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    /* For Instnce TE_MSTID */
    if (i4FsMIMstMstiInstanceIndex == AST_TE_MSTID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMIMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIMstiRootPriority != CLI_STP_ROOT_PRIMARY) &&
        (i4TestValFsMIMstiRootPriority != CLI_STP_ROOT_SECONDARY) &&
        (i4TestValFsMIMstiRootPriority != CLI_STP_ROOT_DEFAULT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if (i4TestValFsMIMstiRootPriority == CLI_STP_ROOT_PRIMARY)
    {
        nmhGetFsMstBrgAddress (&MacAddress);
        nmhGetFsMstMstiBridgeRegionalRoot (i4FsMIMstMstiInstanceIndex,
                                           &RegRoot);
        if (AST_MEMCMP
            ((RegRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE), MacAddress,
             AST_MAC_ADDR_SIZE) != 0)
        {
            i4TestValFsMIMstiRootPriority = AstGetBrgPrioFromBrgId (RegRoot);
            i4TestValFsMIMstiRootPriority =
                i4TestValFsMIMstiRootPriority - 4096;
            if (i4TestValFsMIMstiRootPriority < 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_ROOT_PRIMARY_ERR);
                AstReleaseContext ();
                return SNMP_FAILURE;
            }

        }
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstMstiBridgeTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstMstiBridgeTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstVlanInstanceMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstVlanInstanceMappingTable (INT4
                                                         i4FsMIDot1sFutureMstContextId,
                                                         INT4
                                                         i4FsMIMstInstanceIndex)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsMstVlanInstanceMappingTable
        (i4FsMIMstInstanceIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstVlanInstanceMappingTable (INT4
                                                 *pi4FsMIDot1sFutureMstContextId,
                                                 INT4 *pi4FsMIMstInstanceIndex)
{
    return
        nmhGetNextIndexFsMIMstVlanInstanceMappingTable (0,
                                                        pi4FsMIDot1sFutureMstContextId,
                                                        0,
                                                        pi4FsMIMstInstanceIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                nextFsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex
                nextFsMIMstInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIMstVlanInstanceMappingTable (INT4
                                                i4FsMIDot1sFutureMstContextId,
                                                INT4
                                                *pi4NextFsMIDot1sFutureMstContextId,
                                                INT4 i4FsMIMstInstanceIndex,
                                                INT4
                                                *pi4NextFsMIMstInstanceIndex)
{
    UINT4               u4PrevContextId;
    UINT4               u4ContextId = (UINT4) i4FsMIDot1sFutureMstContextId;

    if ((UINT4) i4FsMIDot1sFutureMstContextId >= AST_SIZING_CONTEXT_COUNT)
    {
        return SNMP_FAILURE;
    }

    if (AST_CONTEXT_INFO (u4ContextId) == NULL)
    {
        /* Given context is not valid - get the next valid context */

        if (AstGetNextActiveContext ((UINT4) i4FsMIDot1sFutureMstContextId,
                                     &u4ContextId) == RST_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIMstInstanceIndex = 0;
    }

    do
    {
        if (AstSelectContext (u4ContextId) != RST_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexFsMstVlanInstanceMappingTable
            (i4FsMIMstInstanceIndex,
             pi4NextFsMIMstInstanceIndex) == SNMP_SUCCESS)
        {

            *pi4NextFsMIDot1sFutureMstContextId = (INT4) u4ContextId;

            AstReleaseContext ();
            return SNMP_SUCCESS;

        }

        /* For a new context start searching for the Instance ID from the
         * beginning */
        i4FsMIMstInstanceIndex = 0;

        u4PrevContextId = u4ContextId;

        AstReleaseContext ();
    }

    while (AstGetNextActiveContext (u4PrevContextId, &u4ContextId)
           != RST_FAILURE);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMapVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMapVlanIndex (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4FsMIMstInstanceIndex,
                           INT4 *pi4RetValFsMIMstMapVlanIndex)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMapVlanIndex (i4FsMIMstInstanceIndex,
                                 pi4RetValFsMIMstMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstUnMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstUnMapVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstUnMapVlanIndex (INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4FsMIMstInstanceIndex,
                             INT4 *pi4RetValFsMIMstUnMapVlanIndex)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstUnMapVlanIndex (i4FsMIMstInstanceIndex,
                                          pi4RetValFsMIMstUnMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstSetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstSetVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstSetVlanList (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 i4FsMIMstInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIMstSetVlanList)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstSetVlanList (i4FsMIMstInstanceIndex,
                                       pRetValFsMIMstSetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstResetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstResetVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstResetVlanList (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4FsMIMstInstanceIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIMstResetVlanList)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstResetVlanList (i4FsMIMstInstanceIndex,
                                         pRetValFsMIMstResetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceVlanMapped
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstInstanceVlanMapped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceVlanMapped (INT4 i4FsMIDot1sFutureMstContextId,
                                 INT4 i4FsMIMstInstanceIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMIMstInstanceVlanMapped)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceVlanMapped (i4FsMIMstInstanceIndex,
                                              pRetValFsMIMstInstanceVlanMapped);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceVlanMapped2k
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstInstanceVlanMapped2k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceVlanMapped2k (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4FsMIMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstInstanceVlanMapped2k)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceVlanMapped2k (i4FsMIMstInstanceIndex,
                                                pRetValFsMIMstInstanceVlanMapped2k);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceVlanMapped3k
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstInstanceVlanMapped3k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceVlanMapped3k (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4FsMIMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstInstanceVlanMapped3k)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceVlanMapped3k (i4FsMIMstInstanceIndex,
                                                pRetValFsMIMstInstanceVlanMapped3k);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstInstanceVlanMapped4k
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstInstanceVlanMapped4k
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstInstanceVlanMapped4k (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4FsMIMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstInstanceVlanMapped4k)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstInstanceVlanMapped4k (i4FsMIMstInstanceIndex,
                                                pRetValFsMIMstInstanceVlanMapped4k);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMapVlanIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMapVlanIndex (INT4 i4FsMIDot1sFutureMstContextId,
                           INT4 i4FsMIMstInstanceIndex,
                           INT4 i4SetValFsMIMstMapVlanIndex)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstMapVlanIndex (i4FsMIMstInstanceIndex,
                                 i4SetValFsMIMstMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstUnMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstUnMapVlanIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstUnMapVlanIndex (INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4FsMIMstInstanceIndex,
                             INT4 i4SetValFsMIMstUnMapVlanIndex)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstUnMapVlanIndex (i4FsMIMstInstanceIndex,
                                   i4SetValFsMIMstUnMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstSetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstSetVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstSetVlanList (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 i4FsMIMstInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsMIMstSetVlanList)
{

    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstSetVlanList (i4FsMIMstInstanceIndex,
                                pSetValFsMIMstSetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstResetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstResetVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstResetVlanList (INT4 i4FsMIDot1sFutureMstContextId,
                            INT4 i4FsMIMstInstanceIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsMIMstResetVlanList)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstResetVlanList (i4FsMIMstInstanceIndex,
                                  pSetValFsMIMstResetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMapVlanIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMapVlanIndex (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIDot1sFutureMstContextId,
                              INT4 i4FsMIMstInstanceIndex,
                              INT4 i4TestValFsMIMstMapVlanIndex)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstMapVlanIndex (pu4ErrorCode, i4FsMIMstInstanceIndex,
                                           i4TestValFsMIMstMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstUnMapVlanIndex
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstUnMapVlanIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstUnMapVlanIndex (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIDot1sFutureMstContextId,
                                INT4 i4FsMIMstInstanceIndex,
                                INT4 i4TestValFsMIMstUnMapVlanIndex)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstUnMapVlanIndex (pu4ErrorCode, i4FsMIMstInstanceIndex,
                                      i4TestValFsMIMstUnMapVlanIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstSetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstSetVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstSetVlanList (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIDot1sFutureMstContextId,
                             INT4 i4FsMIMstInstanceIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsMIMstSetVlanList)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstSetVlanList (pu4ErrorCode, i4FsMIMstInstanceIndex,
                                          pTestValFsMIMstSetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstResetVlanList
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstResetVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstResetVlanList (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4FsMIMstInstanceIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsMIMstResetVlanList)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstResetVlanList (pu4ErrorCode, i4FsMIMstInstanceIndex,
                                     pTestValFsMIMstResetVlanList);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstVlanInstanceMappingTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstVlanInstanceMappingTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstCistPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstCistPortTable
 Input       :  The Indices
                FsMIMstCistPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstCistPortTable (INT4 i4FsMIMstCistPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsMstCistPortTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstCistPortTable
 Input       :  The Indices
                FsMIMstCistPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstCistPortTable (INT4 *pi4FsMIMstCistPort)
{
    return nmhGetNextIndexFsMIMstCistPortTable (0, pi4FsMIMstCistPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstCistPortTable
 Input       :  The Indices
                FsMIMstCistPort
                nextFsMIMstCistPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIMstCistPortTable (INT4 i4FsMIMstCistPort,
                                     INT4 *pi4NextFsMIMstCistPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIMstCistPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsMIMstCistPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIMstCistPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                && (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                              (pAstPortEntry))))
            {
                *pi4NextFsMIMstCistPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortPathCost
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortPathCost (INT4 i4FsMIMstCistPort,
                               INT4 *pi4RetValFsMIMstCistPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortPathCost ((INT4) u2LocalPortId,
                                            pi4RetValFsMIMstCistPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortPriority
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortPriority (INT4 i4FsMIMstCistPort,
                               INT4 *pi4RetValFsMIMstCistPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortPriority ((INT4) u2LocalPortId,
                                            pi4RetValFsMIMstCistPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortDesignatedRoot
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortDesignatedRoot (INT4 i4FsMIMstCistPort,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIMstCistPortDesignatedRoot)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortDesignatedRoot ((INT4) u2LocalPortId,
                                                  pRetValFsMIMstCistPortDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortDesignatedBridge
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortDesignatedBridge (INT4 i4FsMIMstCistPort,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIMstCistPortDesignatedBridge)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortDesignatedBridge ((INT4) u2LocalPortId,
                                                    pRetValFsMIMstCistPortDesignatedBridge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortDesignatedPort
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortDesignatedPort (INT4 i4FsMIMstCistPort,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIMstCistPortDesignatedPort)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortDesignatedPort ((INT4) u2LocalPortId,
                                                  pRetValFsMIMstCistPortDesignatedPort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortAdminP2P
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortAdminP2P
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortAdminP2P (INT4 i4FsMIMstCistPort,
                               INT4 *pi4RetValFsMIMstCistPortAdminP2P)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortAdminP2P ((INT4) u2LocalPortId,
                                            pi4RetValFsMIMstCistPortAdminP2P);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortOperP2P
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortOperP2P
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortOperP2P (INT4 i4FsMIMstCistPort,
                              INT4 *pi4RetValFsMIMstCistPortOperP2P)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortOperP2P ((INT4) u2LocalPortId,
                                           pi4RetValFsMIMstCistPortOperP2P);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortAdminEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortAdminEdgeStatus (INT4 i4FsMIMstCistPort,
                                      INT4
                                      *pi4RetValFsMIMstCistPortAdminEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortAdminEdgeStatus ((INT4) u2LocalPortId,
                                                   pi4RetValFsMIMstCistPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortOperEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortOperEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortOperEdgeStatus (INT4 i4FsMIMstCistPort,
                                     INT4
                                     *pi4RetValFsMIMstCistPortOperEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortOperEdgeStatus ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIMstCistPortOperEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortProtocolMigration
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortProtocolMigration (INT4 i4FsMIMstCistPort,
                                        INT4
                                        *pi4RetValFsMIMstCistPortProtocolMigration)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortProtocolMigration ((INT4) u2LocalPortId,
                                                     pi4RetValFsMIMstCistPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortState (INT4 i4FsMIMstCistPort,
                            INT4 *pi4RetValFsMIMstCistPortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortState ((INT4) u2LocalPortId,
                                         pi4RetValFsMIMstCistPortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistForcePortState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistForcePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistForcePortState (INT4 i4FsMIMstCistPort,
                                 INT4 *pi4RetValFsMIMstCistForcePortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistForcePortState ((INT4) u2LocalPortId,
                                              pi4RetValFsMIMstCistForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDot1sFsMstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                retValFsMIDot1sFsMstSetGlobalTrapOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1sFsMstSetGlobalTrapOption (INT4
                                         *pi4RetValFsMIDot1sFsMstSetGlobalTrapOption)
{
    *pi4RetValFsMIDot1sFsMstSetGlobalTrapOption = AST_GLOBAL_TRAP;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortForwardTransitions
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortForwardTransitions (INT4 i4FsMIMstCistPort,
                                         UINT4
                                         *pu4RetValFsMIMstCistPortForwardTransitions)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortForwardTransitions ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIMstCistPortForwardTransitions);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRxMstBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRxMstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRxMstBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortRxMstBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRxMstBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortRxMstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRxRstBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRxRstBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortRxRstBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRxRstBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortRxRstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortRxConfigBpduCount
Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRxConfigBpduCount (INT4 i4FsMIMstCistPort,
                                        UINT4
                                        *pu4RetValFsMIMstCistPortRxConfigBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRxConfigBpduCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIMstCistPortRxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRxTcnBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRxTcnBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortRxTcnBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRxTcnBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortRxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTxMstBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTxMstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTxMstBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortTxMstBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTxMstBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortTxMstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTxRstBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTxRstBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortTxRstBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTxRstBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortTxRstBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTxConfigBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTxConfigBpduCount (INT4 i4FsMIMstCistPort,
                                        UINT4
                                        *pu4RetValFsMIMstCistPortTxConfigBpduCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTxConfigBpduCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIMstCistPortTxConfigBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTxTcnBpduCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTxTcnBpduCount (INT4 i4FsMIMstCistPort,
                                     UINT4
                                     *pu4RetValFsMIMstCistPortTxTcnBpduCount)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTxTcnBpduCount ((INT4) u2LocalPortId,
                                                  pu4RetValFsMIMstCistPortTxTcnBpduCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortInvalidMstBpduRxCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortInvalidMstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortInvalidMstBpduRxCount (INT4 i4FsMIMstCistPort,
                                            UINT4
                                            *pu4RetValFsMIMstCistPortInvalidMstBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortInvalidMstBpduRxCount ((INT4) u2LocalPortId,
                                                         pu4RetValFsMIMstCistPortInvalidMstBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortInvalidRstBpduRxCount (INT4 i4FsMIMstCistPort,
                                            UINT4
                                            *pu4RetValFsMIMstCistPortInvalidRstBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortInvalidRstBpduRxCount ((INT4) u2LocalPortId,
                                                         pu4RetValFsMIMstCistPortInvalidRstBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortInvalidConfigBpduRxCount (INT4 i4FsMIMstCistPort,
                                               UINT4
                                               *pu4RetValFsMIMstCistPortInvalidConfigBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortInvalidConfigBpduRxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIMstCistPortInvalidConfigBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortInvalidTcnBpduRxCount (INT4 i4FsMIMstCistPort,
                                            UINT4
                                            *pu4RetValFsMIMstCistPortInvalidTcnBpduRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortInvalidTcnBpduRxCount ((INT4) u2LocalPortId,
                                                         pu4RetValFsMIMstCistPortInvalidTcnBpduRxCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTransmitSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTransmitSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTransmitSemState (INT4 i4FsMIMstCistPort,
                                       INT4
                                       *pi4RetValFsMIMstCistPortTransmitSemState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTransmitSemState ((INT4) u2LocalPortId,
                                                    pi4RetValFsMIMstCistPortTransmitSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortReceiveSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortReceiveSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortReceiveSemState (INT4 i4FsMIMstCistPort,
                                      INT4
                                      *pi4RetValFsMIMstCistPortReceiveSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortReceiveSemState ((INT4) u2LocalPortId,
                                                   pi4RetValFsMIMstCistPortReceiveSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortProtMigrationSemState
Input       :  The Indices
               FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortProtMigrationSemState
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIMstCistPortProtMigrationSemState (INT4 i4FsMIMstCistPort,
                                            INT4
                                            *pi4RetValFsMIMstCistPortProtMigrationSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortProtMigrationSemState ((INT4) u2LocalPortId,
                                                         pi4RetValFsMIMstCistPortProtMigrationSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistProtocolMigrationCount
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistProtocolMigrationCount (INT4 i4FsMIMstCistPort,
                                         UINT4
                                         *pu4RetValFsMIMstCistProtocolMigrationCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistProtocolMigrationCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIMstCistProtocolMigrationCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortDesignatedCost
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortDesignatedCost (INT4 i4FsMIMstCistPort,
                                     INT4
                                     *pi4RetValFsMIMstCistPortDesignatedCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortDesignatedCost ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIMstCistPortDesignatedCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRegionalRoot
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRegionalRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRegionalRoot (INT4 i4FsMIMstCistPort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstCistPortRegionalRoot)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRegionalRoot ((INT4) u2LocalPortId,
                                                pRetValFsMIMstCistPortRegionalRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRegionalPathCost
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRegionalPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRegionalPathCost (INT4 i4FsMIMstCistPort,
                                       INT4
                                       *pi4RetValFsMIMstCistPortRegionalPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRegionalPathCost ((INT4) u2LocalPortId,
                                                    pi4RetValFsMIMstCistPortRegionalPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistSelectedPortRole
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistSelectedPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistSelectedPortRole (INT4 i4FsMIMstCistPort,
                                   INT4 *pi4RetValFsMIMstCistSelectedPortRole)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistSelectedPortRole ((INT4) u2LocalPortId,
                                                pi4RetValFsMIMstCistSelectedPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistCurrentPortRole
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistCurrentPortRole (INT4 i4FsMIMstCistPort,
                                  INT4 *pi4RetValFsMIMstCistCurrentPortRole)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistCurrentPortRole ((INT4) u2LocalPortId,
                                               pi4RetValFsMIMstCistCurrentPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortInfoSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortInfoSemState (INT4 i4FsMIMstCistPort,
                                   INT4 *pi4RetValFsMIMstCistPortInfoSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortInfoSemState ((INT4) u2LocalPortId,
                                                pi4RetValFsMIMstCistPortInfoSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRoleTransitionSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRoleTransitionSemState (INT4 i4FsMIMstCistPort,
                                             INT4
                                             *pi4RetValFsMIMstCistPortRoleTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortRoleTransitionSemState ((INT4) u2LocalPortId,
                                                          pi4RetValFsMIMstCistPortRoleTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstCistPortStateTransitionSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortStateTransitionSemState (INT4 i4FsMIMstCistPort,
                                              INT4
                                              *pi4RetValFsMIMstCistPortStateTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortStateTransitionSemState ((INT4) u2LocalPortId,
                                                           pi4RetValFsMIMstCistPortStateTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortTopologyChangeSemState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortTopologyChangeSemState (INT4 i4FsMIMstCistPort,
                                             INT4
                                             *pi4RetValFsMIMstCistPortTopologyChangeSemState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortTopologyChangeSemState ((INT4) u2LocalPortId,
                                                          pi4RetValFsMIMstCistPortTopologyChangeSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortHelloTime
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortHelloTime (INT4 i4FsMIMstCistPort,
                                INT4 *pi4RetValFsMIMstCistPortHelloTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortHelloTime ((INT4) u2LocalPortId,
                                             pi4RetValFsMIMstCistPortHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortOperVersion
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortOperVersion (INT4 i4FsMIMstCistPort,
                                  INT4 *pi4RetValFsMIMstCistPortOperVersion)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortOperVersion ((INT4) u2LocalPortId,
                                               pi4RetValFsMIMstCistPortOperVersion);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortEffectivePortState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortEffectivePortState (INT4 i4FsMIMstCistPort,
                                         INT4
                                         *pi4RetValFsMIMstCistPortEffectivePortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortEffectivePortState ((INT4) u2LocalPortId,
                                                      pi4RetValFsMIMstCistPortEffectivePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortAutoEdgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortAutoEdgeStatus (INT4 i4FsMIMstCistPort,
                                     INT4
                                     *pi4RetValFsMIMstCistPortAutoEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortAutoEdgeStatus ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIMstCistPortAutoEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRestrictedRole
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRestrictedRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRestrictedRole (INT4 i4FsMIMstCistPort,
                                     INT4
                                     *pi4RetValFsMIMstCistPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRestrictedRole
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRestrictedTCN
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRestrictedTCN (INT4 i4FsMIMstCistPort,
                                    INT4 *pi4RetValFsMIMstCistPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRestrictedTCN
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRestrictedTCN);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortAdminPathCost
 Input       :  The Indices
                FsMIMstCistPort
 
                The Object 
                retValFsMIMstCistPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIMstCistPortAdminPathCost (INT4 i4FsMIMstCistPort,
                                    INT4 *pi4RetValFsMIMstCistPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortAdminPathCost
        (u2LocalPortId, pi4RetValFsMIMstCistPortAdminPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortEnableBPDURx (INT4 i4FsMIMstCistPort,
                                   INT4 *pi4RetValFsMIMstCistPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortEnableBPDURx ((INT4) u2LocalPortId,
                                         pi4RetValFsMIMstCistPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortEnableBPDUTx (INT4 i4FsMIMstCistPort,
                                   INT4 *pi4RetValFsMIMstCistPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortEnableBPDUTx ((INT4) u2LocalPortId,
                                         pi4RetValFsMIMstCistPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortPseudoRootId
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortPseudoRootId (INT4 i4FsMIMstCistPort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstCistPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortPseudoRootId ((INT4) u2LocalPortId,
                                         pRetValFsMIMstCistPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortIsL2Gp
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortIsL2Gp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortIsL2Gp (INT4 i4FsMIMstCistPort,
                             INT4 *pi4RetValFsMIMstCistPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortIsL2Gp ((INT4) u2LocalPortId,
                                   pi4RetValFsMIMstCistPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortLoopGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                retValFsMIMstCistPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortLoopGuard (INT4 i4FsMIMstCistPort,
                                INT4 *pi4RetValFsMIMstCistPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortLoopGuard
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRcvdEvent
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRcvdEvent (INT4 i4FsMIMstCistPort,
                                INT4 *pi4RetValFsMIMstCistPortRcvdEvent)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRcvdEvent
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRcvdEvent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRcvdEventSubType
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRcvdEventSubType (INT4 i4FsMIMstCistPort,
                                       INT4
                                       *pi4RetValFsMIMstCistPortRcvdEventSubType)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRcvdEventSubType
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRcvdEventSubType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortRcvdEventTimeStamp
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortRcvdEventTimeStamp (INT4 i4FsMIMstCistPort,
                                         UINT4
                                         *pu4RetValFsMIMstCistPortRcvdEventTimeStamp)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRcvdEventTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortRcvdEventTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistLoopInconsistentState
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                retValFsMIMstCistLoopInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMIMstCistLoopInconsistentState (INT4 i4FsMIMstCistPort,
                                        INT4
                                        *pi4RetValFsMIMstCistLoopInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = 0;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsMstCistPortLoopInconsistentState ((INT4) u2LocalPortId,
                                                  pi4RetValFsMIMstCistLoopInconsistentState);

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortPathCost
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortPathCost (INT4 i4FsMIMstCistPort,
                               INT4 i4SetValFsMIMstCistPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortPathCost ((INT4) u2LocalPortId,
                                            i4SetValFsMIMstCistPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortPriority
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortPriority (INT4 i4FsMIMstCistPort,
                               INT4 i4SetValFsMIMstCistPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortPriority ((INT4) u2LocalPortId,
                                            i4SetValFsMIMstCistPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortAdminP2P
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortAdminP2P
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortAdminP2P (INT4 i4FsMIMstCistPort,
                               INT4 i4SetValFsMIMstCistPortAdminP2P)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortAdminP2P ((INT4) u2LocalPortId,
                                            i4SetValFsMIMstCistPortAdminP2P);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhSetFsMIMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortAdminEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortAdminEdgeStatus (INT4 i4FsMIMstCistPort,
                                      INT4
                                      i4SetValFsMIMstCistPortAdminEdgeStatus)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortAdminEdgeStatus ((INT4) u2LocalPortId,
                                                   i4SetValFsMIMstCistPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortProtocolMigration
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortProtocolMigration (INT4 i4FsMIMstCistPort,
                                        INT4
                                        i4SetValFsMIMstCistPortProtocolMigration)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortProtocolMigration ((INT4) u2LocalPortId,
                                                     i4SetValFsMIMstCistPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistForcePortState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistForcePortState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistForcePortState (INT4 i4FsMIMstCistPort,
                                 INT4 i4SetValFsMIMstCistForcePortState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistForcePortState ((INT4) u2LocalPortId,
                                              i4SetValFsMIMstCistForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIDot1sFsMstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                setValFsMIDot1sFsMstSetGlobalTrapOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1sFsMstSetGlobalTrapOption (INT4
                                         i4SetValFsMIDot1sFsMstSetGlobalTrapOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1sFsMstSetGlobalTrapOption,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 0, SNMP_SUCCESS);

    AST_GLOBAL_TRAP = (UINT4) i4SetValFsMIDot1sFsMstSetGlobalTrapOption;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i ",
                      i4SetValFsMIDot1sFsMstSetGlobalTrapOption));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortHelloTime
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortHelloTime (INT4 i4FsMIMstCistPort,
                                INT4 i4SetValFsMIMstCistPortHelloTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortHelloTime ((INT4) u2LocalPortId,
                                             i4SetValFsMIMstCistPortHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortAutoEdgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortAutoEdgeStatus (INT4 i4FsMIMstCistPort,
                                     INT4 i4SetValFsMIMstCistPortAutoEdgeStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortAutoEdgeStatus ((INT4) u2LocalPortId,
                                                  i4SetValFsMIMstCistPortAutoEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortRestrictedRole
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortRestrictedRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortRestrictedRole (INT4 i4FsMIMstCistPort,
                                     INT4 i4SetValFsMIMstCistPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortRestrictedRole ((INT4) u2LocalPortId,
                                           i4SetValFsMIMstCistPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortRestrictedTCN
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortRestrictedTCN (INT4 i4FsMIMstCistPort,
                                    INT4 i4SetValFsMIMstCistPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortRestrictedTCN ((INT4) u2LocalPortId,
                                          i4SetValFsMIMstCistPortRestrictedTCN);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortAdminPathCost
 Input       :  The Indices
                FsMIMstCistPort
 
                The Object 
                setValFsMIMstCistPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIMstCistPortAdminPathCost (INT4 i4FsMIMstCistPort,
                                    INT4 i4SetValFsMIMstCistPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsMstCistPortAdminPathCost
        (u2LocalPortId, i4SetValFsMIMstCistPortAdminPathCost);
    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortEnableBPDURx (INT4 i4FsMIMstCistPort,
                                   INT4 i4SetValFsMIMstCistPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortEnableBPDURx ((INT4) u2LocalPortId,
                                         i4SetValFsMIMstCistPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortEnableBPDUTx (INT4 i4FsMIMstCistPort,
                                   INT4 i4SetValFsMIMstCistPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortEnableBPDUTx ((INT4) u2LocalPortId,
                                         i4SetValFsMIMstCistPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortPseudoRootId
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortPseudoRootId (INT4 i4FsMIMstCistPort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMIMstCistPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortPseudoRootId ((INT4) u2LocalPortId,
                                         pSetValFsMIMstCistPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortIsL2Gp
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortIsL2Gp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortIsL2Gp (INT4 i4FsMIMstCistPort,
                             INT4 i4SetValFsMIMstCistPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortIsL2Gp ((INT4) u2LocalPortId,
                                          i4SetValFsMIMstCistPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortLoopGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                setValFsMIMstCistPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortLoopGuard (INT4 i4FsMIMstCistPort,
                                INT4 i4SetValFsMIMstCistPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortLoopGuard ((INT4) u2LocalPortId,
                                      i4SetValFsMIMstCistPortLoopGuard);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortPathCost
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortPathCost (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                  INT4 i4TestValFsMIMstCistPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortPathCost (pu4ErrorCode, (INT4) u2LocalPortId,
                                        i4TestValFsMIMstCistPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortPriority
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                  INT4 i4TestValFsMIMstCistPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortPriority (pu4ErrorCode, (INT4) u2LocalPortId,
                                        i4TestValFsMIMstCistPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortAdminP2P
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortAdminP2P
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortAdminP2P (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                  INT4 i4TestValFsMIMstCistPortAdminP2P)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortAdminP2P (pu4ErrorCode, (INT4) u2LocalPortId,
                                        i4TestValFsMIMstCistPortAdminP2P);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortAdminEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortAdminEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortAdminEdgeStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIMstCistPort,
                                         INT4
                                         i4TestValFsMIMstCistPortAdminEdgeStatus)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortAdminEdgeStatus (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsMIMstCistPortAdminEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortProtocolMigration
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortProtocolMigration (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIMstCistPort,
                                           INT4
                                           i4TestValFsMIMstCistPortProtocolMigration)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortProtocolMigration (pu4ErrorCode,
                                                 (INT4) u2LocalPortId,
                                                 i4TestValFsMIMstCistPortProtocolMigration);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistForcePortState
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistForcePortState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistForcePortState (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                    INT4 i4TestValFsMIMstCistForcePortState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistForcePortState (pu4ErrorCode, (INT4) u2LocalPortId,
                                          i4TestValFsMIMstCistForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1sFsMstSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                testValFsMIDot1sFsMstSetGlobalTrapOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1sFsMstSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsMIDot1sFsMstSetGlobalTrapOption)
{

    if ((i4TestValFsMIDot1sFsMstSetGlobalTrapOption < 0) ||
        (i4TestValFsMIDot1sFsMstSetGlobalTrapOption > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortHelloTime
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortHelloTime (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                   INT4 i4TestValFsMIMstCistPortHelloTime)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortHelloTime (pu4ErrorCode, (INT4) u2LocalPortId,
                                         i4TestValFsMIMstCistPortHelloTime);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortAutoEdgeStatus
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortAutoEdgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortAutoEdgeStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIMstCistPort,
                                        INT4
                                        i4TestValFsMIMstCistPortAutoEdgeStatus)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortAutoEdgeStatus (pu4ErrorCode,
                                              (INT4) u2LocalPortId,
                                              i4TestValFsMIMstCistPortAutoEdgeStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortRestrictedRole
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortRestrictedRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortRestrictedRole (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIMstCistPort,
                                        INT4
                                        i4TestValFsMIMstCistPortRestrictedRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortRestrictedRole (pu4ErrorCode,
                                              (INT4) u2LocalPortId,
                                              i4TestValFsMIMstCistPortRestrictedRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortRestrictedTCN
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortRestrictedTCN
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortRestrictedTCN (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIMstCistPort,
                                       INT4
                                       i4TestValFsMIMstCistPortRestrictedTCN)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortRestrictedTCN (pu4ErrorCode,
                                             (INT4) u2LocalPortId,
                                             i4TestValFsMIMstCistPortRestrictedTCN);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortAdminPathCost
 Input       :  The Indices
                FsMIMstCistPort
 
                The Object 
                testValFsMIMstCistPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortAdminPathCost (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIMstCistPort,
                                       INT4
                                       i4TestValFsMIMstCistPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstCistPortAdminPathCost
        (pu4ErrorCode, u2LocalPortId, i4TestValFsMIMstCistPortAdminPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortEnableBPDURx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortEnableBPDURx (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIMstCistPort,
                                      INT4 i4TestValFsMIMstCistPortEnableBPDURx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortEnableBPDURx (pu4ErrorCode, (INT4) u2LocalPortId,
                                            i4TestValFsMIMstCistPortEnableBPDURx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortEnableBPDUTx
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortEnableBPDUTx (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIMstCistPort,
                                      INT4 i4TestValFsMIMstCistPortEnableBPDUTx)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortEnableBPDUTx (pu4ErrorCode, (INT4) u2LocalPortId,
                                            i4TestValFsMIMstCistPortEnableBPDUTx);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortPseudoRootId
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortPseudoRootId (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIMstCistPort,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMIMstCistPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortPseudoRootId (pu4ErrorCode, (INT4) u2LocalPortId,
                                            pTestValFsMIMstCistPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortIsL2Gp
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortIsL2Gp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortIsL2Gp (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                INT4 i4TestValFsMIMstCistPortIsL2Gp)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortIsL2Gp (pu4ErrorCode, (INT4) u2LocalPortId,
                                      i4TestValFsMIMstCistPortIsL2Gp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortLoopGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                testValFsMIMstCistPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                                     ****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortLoopGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                   INT4 i4TestValFsMIMstCistPortLoopGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortLoopGuard (pu4ErrorCode,
                                         (INT4) u2LocalPortId,
                                         i4TestValFsMIMstCistPortLoopGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhTestv2FsMIMstPortBpduGuardAction
Input       :  The Indices
           FsMIMstCistPort

           The Object 
           testValFsMIMstPortBpduGuardAction
Output      :  The Test Low Lev Routine Take the Indices &
           Test whether that Value is Valid Input for Set.
           Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
           SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
           SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
           SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
           SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
           SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstPortBpduGuardAction (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIMstCistPort,
                                     INT4 i4TestValFsMIMstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstPortBpduGuardAction (pu4ErrorCode, (INT4) u2LocalPortId,
                                           i4TestValFsMIMstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstCistPortTable
 Input       :  The Indices
                FsMIMstCistPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstCistPortTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstMstiPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstMstiPortTable
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstMstiPortTable (INT4 i4FsMIMstMstiPort,
                                              INT4 i4FsMIMstInstanceIndex)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsMstMstiPortTable ((INT4) u2LocalPortId,
                                                           i4FsMIMstInstanceIndex);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstMstiPortTable
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstMstiPortTable (INT4 *pi4FsMIMstMstiPort,
                                      INT4 *pi4FsMIMstInstanceIndex)
{
    return (nmhGetNextIndexFsMIMstMstiPortTable (0, pi4FsMIMstMstiPort,
                                                 0, pi4FsMIMstInstanceIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstMstiPortTable
 Input       :  The Indices
                FsMIMstMstiPort
                nextFsMIMstMstiPort
                FsMIMstInstanceIndex
                nextFsMIMstInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstMstiPortTable (INT4 i4FsMIMstMstiPort,
                                     INT4 *pi4NextFsMIMstMstiPort,
                                     INT4 i4FsMIMstInstanceIndex,
                                     INT4 *pi4NextFsMIMstInstanceIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    tAstPerStInfo      *pAstPerStInfo = NULL;

    UINT2               u2LocalPortId;
    UINT2               u2InstIndex;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIMstMstiPort);

    if (pAstPortEntry == NULL)
    {
        /* Get the next valid port if the given port is not valid */
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if (pAstPortEntry->u4IfIndex >= (UINT4) i4FsMIMstMstiPort)
            {
                /* For a new port start searching for the Instance ID from the
                 * beginning */
                i4FsMIMstInstanceIndex = 0;
                break;
            }
        }

        if (pAstPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    do
    {
        if (AstSelectContext (AST_IFENTRY_CONTEXT_ID (pAstPortEntry))
            != RST_SUCCESS)
        {
            /* For a new port start searching for the Instance ID from the
             * beginning */
            i4FsMIMstInstanceIndex = 0;
            continue;
        }

        if (!AST_IS_MST_STARTED ())
        {
            /* For a new port start searching for the Instance ID from the
             * beginning */
            i4FsMIMstInstanceIndex = 0;

            AstReleaseContext ();
            continue;
        }

        u2LocalPortId = AST_IFENTRY_LOCAL_PORT (pAstPortEntry);

        u2InstIndex = (UINT2) (i4FsMIMstInstanceIndex + 1);
        AST_GET_NEXT_BUF_INSTANCE (u2InstIndex, pAstPerStInfo)
        {
            if (u2InstIndex == MST_CIST_CONTEXT)
            {
                continue;
            }

            if (pAstPerStInfo == NULL)
            {
                continue;
            }

            pAstPerStPortInfo =
                AST_GET_PERST_PORT_INFO (u2LocalPortId, u2InstIndex);

            if (pAstPerStPortInfo != NULL)
            {
                *pi4NextFsMIMstMstiPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                *pi4NextFsMIMstInstanceIndex = pAstPerStInfo->u2InstanceId;

                AstReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        /* Next Instance is TE_MSTID if it has created for current context */
        if (AST_IS_TE_MSTID_VALID (i4FsMIMstInstanceIndex))
        {
            *pi4NextFsMIMstMstiPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
            *pi4NextFsMIMstInstanceIndex = AST_TE_MSTID;
            return SNMP_SUCCESS;
        }
        /* For a new port start searching for the Instance ID from the
         * beginning */
        i4FsMIMstInstanceIndex = 0;

        AstReleaseContext ();
    }
    while ((pAstPortEntry = AstGetNextIfIndexEntry (pAstPortEntry)) != NULL);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortPathCost (INT4 i4FsMIMstMstiPort,
                               INT4 i4FsMIMstInstanceIndex,
                               INT4 *pi4RetValFsMIMstMstiPortPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortPathCost ((INT4) u2LocalPortId,
                                     i4FsMIMstInstanceIndex,
                                     pi4RetValFsMIMstMstiPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortPriority
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortPriority (INT4 i4FsMIMstMstiPort,
                               INT4 i4FsMIMstInstanceIndex,
                               INT4 *pi4RetValFsMIMstMstiPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortPriority ((INT4) u2LocalPortId,
                                     i4FsMIMstInstanceIndex,
                                     pi4RetValFsMIMstMstiPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortDesignatedRoot
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortDesignatedRoot (INT4 i4FsMIMstMstiPort,
                                     INT4 i4FsMIMstInstanceIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIMstMstiPortDesignatedRoot)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortDesignatedRoot ((INT4) u2LocalPortId,
                                           i4FsMIMstInstanceIndex,
                                           pRetValFsMIMstMstiPortDesignatedRoot);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortDesignatedBridge
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortDesignatedBridge (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIMstMstiPortDesignatedBridge)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortDesignatedBridge ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pRetValFsMIMstMstiPortDesignatedBridge);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortDesignatedPort
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortDesignatedPort (INT4 i4FsMIMstMstiPort,
                                     INT4 i4FsMIMstInstanceIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIMstMstiPortDesignatedPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortDesignatedPort ((INT4) u2LocalPortId,
                                           i4FsMIMstInstanceIndex,
                                           pRetValFsMIMstMstiPortDesignatedPort);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortState (INT4 i4FsMIMstMstiPort, INT4 i4FsMIMstInstanceIndex,
                            INT4 *pi4RetValFsMIMstMstiPortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortState ((INT4) u2LocalPortId, i4FsMIMstInstanceIndex,
                                  pi4RetValFsMIMstMstiPortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiForcePortState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiForcePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiForcePortState (INT4 i4FsMIMstMstiPort,
                                 INT4 i4FsMIMstInstanceIndex,
                                 INT4 *pi4RetValFsMIMstMstiForcePortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiForcePortState ((INT4) u2LocalPortId,
                                       i4FsMIMstInstanceIndex,
                                       pi4RetValFsMIMstMstiForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortForwardTransitions
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortForwardTransitions (INT4 i4FsMIMstMstiPort,
                                         INT4 i4FsMIMstInstanceIndex,
                                         UINT4
                                         *pu4RetValFsMIMstMstiPortForwardTransitions)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiPortForwardTransitions ((INT4) u2LocalPortId,
                                                      i4FsMIMstInstanceIndex,
                                                      pu4RetValFsMIMstMstiPortForwardTransitions);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortReceivedBPDUs
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortReceivedBPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortReceivedBPDUs (INT4 i4FsMIMstMstiPort,
                                    INT4 i4FsMIMstInstanceIndex,
                                    UINT4
                                    *pu4RetValFsMIMstMstiPortReceivedBPDUs)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortReceivedBPDUs ((INT4) u2LocalPortId,
                                          i4FsMIMstInstanceIndex,
                                          pu4RetValFsMIMstMstiPortReceivedBPDUs);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortTransmittedBPDUs
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortTransmittedBPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTransmittedBPDUs (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortTransmittedBPDUs)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortTransmittedBPDUs ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortTransmittedBPDUs);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortInvalidBPDUsRcvd
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortInvalidBPDUsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortInvalidBPDUsRcvd (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortInvalidBPDUsRcvd)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortInvalidBPDUsRcvd ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortInvalidBPDUsRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortDesignatedCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortDesignatedCost (INT4 i4FsMIMstMstiPort,
                                     INT4 i4FsMIMstInstanceIndex,
                                     INT4
                                     *pi4RetValFsMIMstMstiPortDesignatedCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortDesignatedCost ((INT4) u2LocalPortId,
                                           i4FsMIMstInstanceIndex,
                                           pi4RetValFsMIMstMstiPortDesignatedCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiSelectedPortRole
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiSelectedPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiSelectedPortRole (INT4 i4FsMIMstMstiPort,
                                   INT4 i4FsMIMstInstanceIndex,
                                   INT4 *pi4RetValFsMIMstMstiSelectedPortRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiSelectedPortRole ((INT4) u2LocalPortId,
                                         i4FsMIMstInstanceIndex,
                                         pi4RetValFsMIMstMstiSelectedPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiCurrentPortRole
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiCurrentPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiCurrentPortRole (INT4 i4FsMIMstMstiPort,
                                  INT4 i4FsMIMstInstanceIndex,
                                  INT4 *pi4RetValFsMIMstMstiCurrentPortRole)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiCurrentPortRole ((INT4) u2LocalPortId,
                                        i4FsMIMstInstanceIndex,
                                        pi4RetValFsMIMstMstiCurrentPortRole);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortInfoSemState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortInfoSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortInfoSemState (INT4 i4FsMIMstMstiPort,
                                   INT4 i4FsMIMstInstanceIndex,
                                   INT4 *pi4RetValFsMIMstMstiPortInfoSemState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortInfoSemState ((INT4) u2LocalPortId,
                                         i4FsMIMstInstanceIndex,
                                         pi4RetValFsMIMstMstiPortInfoSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortRoleTransitionSemState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortRoleTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortRoleTransitionSemState (INT4 i4FsMIMstMstiPort,
                                             INT4 i4FsMIMstInstanceIndex,
                                             INT4
                                             *pi4RetValFsMIMstMstiPortRoleTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortRoleTransitionSemState ((INT4) u2LocalPortId,
                                                   i4FsMIMstInstanceIndex,
                                                   pi4RetValFsMIMstMstiPortRoleTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortStateTransitionSemState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortStateTransitionSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortStateTransitionSemState (INT4 i4FsMIMstMstiPort,
                                              INT4 i4FsMIMstInstanceIndex,
                                              INT4
                                              *pi4RetValFsMIMstMstiPortStateTransitionSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiPortStateTransitionSemState ((INT4) u2LocalPortId,
                                                           i4FsMIMstInstanceIndex,
                                                           pi4RetValFsMIMstMstiPortStateTransitionSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIMstMstiPortTopologyChangeSemState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortTopologyChangeSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTopologyChangeSemState (INT4 i4FsMIMstMstiPort,
                                             INT4 i4FsMIMstInstanceIndex,
                                             INT4
                                             *pi4RetValFsMIMstMstiPortTopologyChangeSemState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstMstiPortTopologyChangeSemState ((INT4) u2LocalPortId,
                                                          i4FsMIMstInstanceIndex,
                                                          pi4RetValFsMIMstMstiPortTopologyChangeSemState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortEffectivePortState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortEffectivePortState (INT4 i4FsMIMstMstiPort,
                                         INT4 i4FsMIMstInstanceIndex,
                                         INT4
                                         *pi4RetValFsMIMstMstiPortEffectivePortState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortEffectivePortState ((INT4) u2LocalPortId,
                                               i4FsMIMstInstanceIndex,
                                               pi4RetValFsMIMstMstiPortEffectivePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 
                The Object 
                retValFsMIMstMstiPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortAdminPathCost (INT4 i4FsMIMstMstiPort,
                                    INT4 i4FsMIMstInstanceIndex,
                                    INT4 *pi4RetValFsMIMstMstiPortAdminPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsMstMstiPortAdminPathCost (u2LocalPortId,
                                          i4FsMIMstInstanceIndex,
                                          pi4RetValFsMIMstMstiPortAdminPathCost);
    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortPseudoRootId (INT4 i4FsMIMstMstiPort,
                                   INT4 i4FsMIMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIMstMstiPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortPseudoRootId ((INT4) u2LocalPortId,
                                         i4FsMIMstInstanceIndex,
                                         pRetValFsMIMstMstiPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortStateChangeTimeStamp
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortStateChangeTimeStamp (INT4 i4FsMIMstMstiPort,
                                           INT4 i4FsMIMstInstanceIndex,
                                           UINT4
                                           *pu4RetValFsMIMstMstiPortStateChangeTimeStamp)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortStateChangeTimeStamp ((INT4) u2LocalPortId,
                                                 i4FsMIMstInstanceIndex,
                                                 pu4RetValFsMIMstMstiPortStateChangeTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiPortPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMstiPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiPortPathCost (INT4 i4FsMIMstMstiPort,
                               INT4 i4FsMIMstInstanceIndex,
                               INT4 i4SetValFsMIMstMstiPortPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstMstiPortPathCost ((INT4) u2LocalPortId,
                                     i4FsMIMstInstanceIndex,
                                     i4SetValFsMIMstMstiPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhSetFsMIMstMstiPortPriority
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMstiPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiPortPriority (INT4 i4FsMIMstMstiPort,
                               INT4 i4FsMIMstInstanceIndex,
                               INT4 i4SetValFsMIMstMstiPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstMstiPortPriority ((INT4) u2LocalPortId,
                                     i4FsMIMstInstanceIndex,
                                     i4SetValFsMIMstMstiPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiForcePortState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMstiForcePortState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiForcePortState (INT4 i4FsMIMstMstiPort,
                                 INT4 i4FsMIMstInstanceIndex,
                                 INT4 i4SetValFsMIMstMstiForcePortState)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstMstiForcePortState ((INT4) u2LocalPortId,
                                       i4FsMIMstInstanceIndex,
                                       i4SetValFsMIMstMstiForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 
                The Object 
                setValFsMIMstMstiPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIMstMstiPortAdminPathCost (INT4 i4FsMIMstMstiPort,
                                    INT4 i4FsMIMstInstanceIndex,
                                    INT4 i4SetValFsMIMstMstiPortAdminPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetFsMstMstiPortAdminPathCost (u2LocalPortId,
                                          i4FsMIMstInstanceIndex,
                                          i4SetValFsMIMstMstiPortAdminPathCost);
    AstReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMstiPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiPortPseudoRootId (INT4 i4FsMIMstMstiPort,
                                   INT4 i4FsMIMstInstanceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMIMstMstiPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstMstiPortPseudoRootId ((INT4) u2LocalPortId,
                                         i4FsMIMstInstanceIndex,
                                         pSetValFsMIMstMstiPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsMIMstMstiPortPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMstiPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiPortPathCost (UINT4 *pu4ErrorCode, INT4 i4FsMIMstMstiPort,
                                  INT4 i4FsMIMstInstanceIndex,
                                  INT4 i4TestValFsMIMstMstiPortPathCost)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstMstiPortPathCost (pu4ErrorCode, (INT4) u2LocalPortId,
                                        i4FsMIMstInstanceIndex,
                                        i4TestValFsMIMstMstiPortPathCost);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiPortPriority
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMstiPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiPortPriority (UINT4 *pu4ErrorCode, INT4 i4FsMIMstMstiPort,
                                  INT4 i4FsMIMstInstanceIndex,
                                  INT4 i4TestValFsMIMstMstiPortPriority)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstMstiPortPriority (pu4ErrorCode, (INT4) u2LocalPortId,
                                        i4FsMIMstInstanceIndex,
                                        i4TestValFsMIMstMstiPortPriority);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiForcePortState
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMstiForcePortState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiForcePortState (UINT4 *pu4ErrorCode, INT4 i4FsMIMstMstiPort,
                                    INT4 i4FsMIMstInstanceIndex,
                                    INT4 i4TestValFsMIMstMstiForcePortState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstMstiForcePortState (pu4ErrorCode, (INT4) u2LocalPortId,
                                          i4FsMIMstInstanceIndex,
                                          i4TestValFsMIMstMstiForcePortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiPortAdminPathCost
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 
                The Object 
                testValFsMIMstMstiPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIMstMstiPortAdminPathCost (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       INT4
                                       i4TestValFsMIMstMstiPortAdminPathCost)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;
    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhTestv2FsMstMstiPortAdminPathCost (pu4ErrorCode,
                                             u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             i4TestValFsMIMstMstiPortAdminPathCost);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiPortPseudoRootId
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMstiPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiPortPseudoRootId (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIMstMstiPort,
                                      INT4 i4FsMIMstInstanceIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMIMstMstiPortPseudoRootId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstMstiPortPseudoRootId (pu4ErrorCode, (INT4) u2LocalPortId,
                                            i4FsMIMstInstanceIndex,
                                            pTestValFsMIMstMstiPortPseudoRootId);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstMstiPortTable
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstMstiPortTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstPortExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstPortExtTable
 Input       :  The Indices
                FsMIMstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstPortExtTable (INT4 i4FsMIMstPort)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsMstPortExtTable ((INT4) u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstPortExtTable
 Input       :  The Indices
                FsMIMstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstPortExtTable (INT4 *pi4FsMIMstPort)
{
    return nmhGetNextIndexFsMIMstPortExtTable (0, pi4FsMIMstPort);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstPortExtTable
 Input       :  The Indices
                FsMIMstPort
                nextFsMIMstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIMstPortExtTable (INT4 i4FsMIMstPort,
                                    INT4 *pi4NextFsMIMstPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIMstPort);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsMIMstPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIMstPort < (INT4) AST_IFENTRY_IFINDEX (pAstPortEntry))
                && (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                              (pAstPortEntry))))
            {
                *pi4NextFsMIMstPort = AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstPortRowStatus
 Input       :  The Indices
                FsMIMstPort

                The Object 
                retValFsMIMstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPortRowStatus (INT4 i4FsMIMstPort,
                            INT4 *pi4RetValFsMIMstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortRowStatus ((INT4) u2LocalPortId,
                                         pi4RetValFsMIMstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstPortRowStatus
 Input       :  The Indices
                FsMIMstPort

                The Object 
                setValFsMIMstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstPortRowStatus (INT4 i4FsMIMstPort,
                            INT4 i4SetValFsMIMstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIMstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstPortRowStatus ((INT4) u2LocalPortId,
                                         i4SetValFsMIMstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstPortRowStatus
 Input       :  The Indices
                FsMIMstPort

                The Object 
                testValFsMIMstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIMstPort,
                               INT4 i4TestValFsMIMstPortRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstVcmGetContextInfoFromIfIndex (i4FsMIMstPort,
                                         &u4ContextId,
                                         &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstPortRowStatus (pu4ErrorCode, (INT4) u2LocalPortId,
                                     i4TestValFsMIMstPortRowStatus);

    AstReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIMstPortExtTable
 Input       :  The Indices
                FsMIMstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIMstPortExtTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1sFsMstTrapsControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1sFsMstTrapsControlTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1sFsMstTrapsControlTable (INT4
                                                         i4FsMIDot1sFutureMstContextId)
{
    if (AST_IS_VC_VALID (i4FsMIDot1sFutureMstContextId) != RST_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1sFsMstTrapsControlTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1sFsMstTrapsControlTable (INT4
                                                 *pi4FsMIDot1sFutureMstContextId)
{

    UINT4               u4ContextId;

    if (AstGetFirstActiveContext (&u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIDot1sFutureMstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1sFsMstTrapsControlTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                nextFsMIDot1sFutureMstContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1sFsMstTrapsControlTable (INT4
                                                i4FsMIDot1sFutureMstContextId,
                                                INT4
                                                *pi4NextFsMIDot1sFutureMstContextId)
{

    UINT4               u4ContextId;

    if (AstGetNextActiveContext ((UINT4) i4FsMIDot1sFutureMstContextId,
                                 &u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIDot1sFutureMstContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1sFsMstSetGlobalTrapOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1sFsMstSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstSetTraps
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstSetTraps (INT4 i4FsMIDot1sFutureMstContextId,
                       INT4 *pi4RetValFsMIMstSetTraps)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstSetTraps (pi4RetValFsMIMstSetTraps);
    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstGlobalErrTrapType
 Input       :  The Indices

                The Object 
                retValFsMIMstErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstGlobalErrTrapType (INT4 *pi4RetValFsMIMstGlobalErrTrapType)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFsMstErrTrapType (pi4RetValFsMIMstGlobalErrTrapType);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIMstSetTraps
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstSetTraps (INT4 i4FsMIDot1sFutureMstContextId,
                       INT4 i4SetValFsMIMstSetTraps)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstSetTraps (i4SetValFsMIMstSetTraps);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIMstSetTraps
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstSetTraps (UINT4 *pu4ErrorCode,
                          INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 i4TestValFsMIMstSetTraps)
{

    INT1                i1RetVal;
    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsMstSetTraps (pu4ErrorCode, i4TestValFsMIMstSetTraps);
    AstReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1sFsMstTrapsControlTable
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1sFsMstTrapsControlTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIMstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstPortTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIMstPortTrapNotificationTable (INT4
                                                          i4FsMIMstPortTrapIndex)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsMstPortTrapNotificationTable ((INT4)
                                                                u2LocalPortId);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstPortTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIMstPortTrapNotificationTable (INT4 *pi4FsMIMstPortTrapIndex)
{
    return nmhGetNextIndexFsMIMstPortTrapNotificationTable (0,
                                                            pi4FsMIMstPortTrapIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstPortTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
                nextFsMIMstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIMstPortTrapNotificationTable (INT4 i4FsMIMstPortTrapIndex,
                                                 INT4
                                                 *pi4NextFsMIMstPortTrapIndex)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;
    tAstPortEntry      *pTempPortEntry = NULL;

    pAstPortEntry = AstGetIfIndexEntry (i4FsMIMstPortTrapIndex);

    if (pAstPortEntry != NULL)
    {
        while ((pNextPortEntry = AstGetNextIfIndexEntry (pAstPortEntry))
               != NULL)
        {
            pAstPortEntry = pNextPortEntry;

            if (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                          (pAstPortEntry)))
            {
                *pi4NextFsMIMstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        RB_OFFSET_SCAN (AST_GLOBAL_IFINDEX_TREE (), pAstPortEntry,
                        pTempPortEntry, tAstPortEntry *)
        {
            if ((i4FsMIMstPortTrapIndex < (INT4) pAstPortEntry->u4IfIndex)
                && (AstIsMstStartedInContext (AST_IFENTRY_CONTEXT_ID
                                              (pAstPortEntry))))
            {
                *pi4NextFsMIMstPortTrapIndex =
                    AST_IFENTRY_IFINDEX (pAstPortEntry);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstPortMigrationType
 Input       :  The Indices
                FsMIMstPortTrapIndex

                The Object 
                retValFsMIMstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPortMigrationType (INT4 i4FsMIMstPortTrapIndex,
                                INT4 *pi4RetValFsMIMstPortMigrationType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortMigrationType ((INT4) u2LocalPortId,
                                             pi4RetValFsMIMstPortMigrationType);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstPktErrType
 Input       :  The Indices
                FsMIMstPortTrapIndex

                The Object 
                retValFsMIMstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPktErrType (INT4 i4FsMIMstPortTrapIndex,
                         INT4 *pi4RetValFsMIMstPktErrType)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPktErrType ((INT4) u2LocalPortId,
                                      pi4RetValFsMIMstPktErrType);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstPktErrVal
 Input       :  The Indices
                FsMIMstPortTrapIndex

                The Object 
                retValFsMIMstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPktErrVal (INT4 i4FsMIMstPortTrapIndex,
                        INT4 *pi4RetValFsMIMstPktErrVal)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPktErrVal ((INT4) u2LocalPortId,
                                     pi4RetValFsMIMstPktErrVal);

    AstReleaseContext ();

    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FsMIMstPortRoleTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIMstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
                FsMIMstMstiInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMIMstPortRoleTrapNotificationTable
    (INT4 i4FsMIMstPortTrapIndex, INT4 i4FsMIMstMstiInstanceIndex)
{
    /* This table holds the Port role information necessary to *
     * generate trap during role changes. Here, we directly   *
     * call the corresponding Validate, getfirst and Getnext   *
     * routine for another table, having the same Indices.     */
    INT1                i1RetVal;

    i1RetVal = nmhValidateIndexInstanceFsMIMstMstiPortTable
        (i4FsMIMstPortTrapIndex, i4FsMIMstMstiInstanceIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIMstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
                FsMIMstMstiInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsMIMstPortRoleTrapNotificationTable
    (INT4 *pi4FsMIMstPortTrapIndex, INT4 *pi4FsMIMstMstiInstanceIndex)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFirstIndexFsMIMstMstiPortTable
        (pi4FsMIMstPortTrapIndex, pi4FsMIMstMstiInstanceIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIMstPortRoleTrapNotificationTable
 Input       :  The Indices
                FsMIMstPortTrapIndex
                nextFsMIMstPortTrapIndex
                FsMIMstMstiInstanceIndex
                nextFsMIMstMstiInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsMIMstPortRoleTrapNotificationTable
    (INT4 i4FsMIMstPortTrapIndex, INT4 *pi4NextFsMIMstPortTrapIndex,
     INT4 i4FsMIMstMstiInstanceIndex, INT4 *pi4NextFsMIMstMstiInstanceIndex)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetNextIndexFsMIMstMstiPortTable (i4FsMIMstPortTrapIndex,
                                                    pi4NextFsMIMstPortTrapIndex,
                                                    i4FsMIMstMstiInstanceIndex,
                                                    pi4NextFsMIMstMstiInstanceIndex);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIMstPortRoleType
 Input       :  The Indices
                FsMIMstPortTrapIndex
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstPortRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPortRoleType (INT4 i4FsMIMstPortTrapIndex,
                           INT4 i4FsMIMstMstiInstanceIndex,
                           INT4 *pi4RetValFsMIMstPortRoleType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortRoleType ((INT4) u2LocalPortId,
                                        i4FsMIMstMstiInstanceIndex,
                                        pi4RetValFsMIMstPortRoleType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstOldRoleType
 Input       :  The Indices
                FsMIMstPortTrapIndex
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstOldRoleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstOldRoleType (INT4 i4FsMIMstPortTrapIndex,
                          INT4 i4FsMIMstMstiInstanceIndex,
                          INT4 *pi4RetValFsMIMstOldRoleType)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstPortTrapIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstOldPortRoleType ((INT4) u2LocalPortId,
                                           i4FsMIMstMstiInstanceIndex,
                                           pi4RetValFsMIMstOldRoleType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstGenTrapType
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstGenTrapType (INT4 i4FsMIDot1sFutureMstContextId,
                          INT4 *pi4RetValFsMIMstGenTrapType)
{
    INT1                i1RetVal;

    if (AstSelectContext (i4FsMIDot1sFutureMstContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstGenTrapType (pi4RetValFsMIMstGenTrapType);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  MstMiValidateInstanceEntry
 Input       :  i4FsMstInstanceIndex
                                                         
 Output      :  None.                                           
 Returns     :  MST_SUCCESS or MST_FAILURE
****************************************************************************/
INT4
MstMiValidateInstanceEntry (UINT4 u4ContextId, INT4 i4FsMstInstanceIndex)
{
    INT4                i4RetVal;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return MST_FAILURE;
    }

    i4RetVal = MstValidateInstanceEntry (i4FsMstInstanceIndex);

    AstReleaseContext ();

    return i4RetVal;
}

/* MSTP clear statistics feature*/
/****************************************************************************
 Function    :  nmhGetFsMIMstClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                retValFsMIMstClearBridgeStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstClearBridgeStats (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 *pi4RetValFsMIMstClearBridgeStats)
{
    tAstBridgeEntry    *pBrgEntry = NULL;

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    /* Obtain the existing value of bBridgeClearStats  */
    pBrgEntry = AST_GET_BRGENTRY ();

    if (NULL == pBrgEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIMstClearBridgeStats = pBrgEntry->bBridgeClearStats;

    /*Switch back context */
    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                setValFsMIMstClearBridgeStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstClearBridgeStats (INT4 i4FsMIDot1sFutureMstContextId,
                               INT4 i4SetValFsMIMstClearBridgeStats)
{
    tAstBridgeEntry    *pBrgEntry = NULL;

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    pBrgEntry = AST_GET_BRGENTRY ();
    if (NULL == pBrgEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((MST_DISABLED == pBrgEntry->bBridgeClearStats)
        && (MST_ENABLED == i4SetValFsMIMstClearBridgeStats))
    {
        pBrgEntry->bBridgeClearStats = MST_ENABLED;
        MstResetCounters ();
        pBrgEntry->bBridgeClearStats = MST_DISABLED;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId

                The Object 
                testValFsMIMstClearBridgeStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstClearBridgeStats (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIDot1sFutureMstContextId,
                                  INT4 i4TestValFsMIMstClearBridgeStats)
{

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_MST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((MST_DISABLED != i4TestValFsMIMstClearBridgeStats) &&
        (MST_ENABLED != i4TestValFsMIMstClearBridgeStats))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortClearStats
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstCistPortClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortClearStats (INT4 i4FsMIMstCistPort,
                                 INT4 *pi4RetValFsMIMstCistPortClearStats)
{

    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AstReleaseContext ();
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (u2LocalPortId);
    if (NULL == pPortEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIMstCistPortClearStats = pPortEntry->bPortClearStats;
    AstReleaseContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortClearStats
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstCistPortClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortClearStats (INT4 i4FsMIMstCistPort,
                                 INT4 i4SetValFsMIMstCistPortClearStats)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AstReleaseContext ();
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (u2LocalPortId);

    if (NULL == pPortEntry)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((MST_FALSE == pPortEntry->bPortClearStats)
        && (MST_TRUE == i4SetValFsMIMstCistPortClearStats))
    {
        pPortEntry->bPortClearStats = MST_TRUE;
        MstResetPortCounters (u2LocalPortId);
        pPortEntry->bPortClearStats = MST_FALSE;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortClearStats
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                testValFsMIMstCistPortClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortClearStats (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIMstCistPort,
                                    INT4 i4TestValFsMIMstCistPortClearStats)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((MST_FALSE != i4TestValFsMIMstCistPortClearStats) &&
        (MST_TRUE != i4TestValFsMIMstCistPortClearStats))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                retValFsMIMstMstiClearBridgeStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiClearBridgeStats (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4FsMIMstMstiInstanceIndex,
                                   INT4 *pi4RetValFsMIMstMstiClearBridgeStats)
{
    tAstPerStBridgeInfo *pAstPerStBrgInfo = NULL;

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    pAstPerStBrgInfo = AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);
    if (NULL == pAstPerStBrgInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIMstMstiClearBridgeStats =
        pAstPerStBrgInfo->bMstiBridgeClearStats;

    AstReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                setValFsMIMstMstiClearBridgeStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiClearBridgeStats (INT4 i4FsMIDot1sFutureMstContextId,
                                   INT4 i4FsMIMstMstiInstanceIndex,
                                   INT4 i4SetValFsMIMstMstiClearBridgeStats)
{
    tAstPerStBridgeInfo *pAstPerStBrgInfo = NULL;

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    pAstPerStBrgInfo = AST_GET_PER_ST_BRG_INFO_PTR (i4FsMIMstMstiInstanceIndex);
    if (NULL == pAstPerStBrgInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((MST_FALSE == pAstPerStBrgInfo->bMstiBridgeClearStats) &&
        (MST_TRUE == i4SetValFsMIMstMstiClearBridgeStats))
    {
        pAstPerStBrgInfo->bMstiBridgeClearStats = MST_TRUE;
        MstResetPerStCounters ((UINT2) i4FsMIMstMstiInstanceIndex);
        pAstPerStBrgInfo->bMstiBridgeClearStats = MST_FALSE;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiClearBridgeStats
 Input       :  The Indices
                FsMIDot1sFutureMstContextId
                FsMIMstMstiInstanceIndex

                The Object 
                testValFsMIMstMstiClearBridgeStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiClearBridgeStats (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIDot1sFutureMstContextId,
                                      INT4 i4FsMIMstMstiInstanceIndex,
                                      INT4 i4TestValFsMIMstMstiClearBridgeStats)
{

    /*Switch to associated context */
    if (MST_SUCCESS != AstSelectContext (i4FsMIDot1sFutureMstContextId))
    {
        return SNMP_FAILURE;
    }

    if (!AST_IS_MST_STARTED ())
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMIMstMstiInstanceIndex) != MST_SUCCESS)
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_INVALID_INSTANCE_ID_ERR);

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((MST_FALSE != i4TestValFsMIMstMstiClearBridgeStats) &&
        (MST_TRUE != i4TestValFsMIMstMstiClearBridgeStats))
    {
        AstReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIMstMstiPortClearStats
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                retValFsMIMstMstiPortClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortClearStats (INT4 i4FsMIMstMstiPort,
                                 INT4 i4FsMIMstInstanceIndex,
                                 INT4 *pi4RetValFsMIMstMstiPortClearStats)
{

    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AstReleaseContext ();
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    if (NULL == AST_GET_PERST_INFO (i4FsMIMstInstanceIndex))
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId,
                                              i4FsMIMstInstanceIndex);
    if (NULL == pPerStPortInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIMstMstiPortClearStats = pPerStPortInfo->bMstiPortClearStats;
    AstReleaseContext ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIMstMstiPortClearStats
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                setValFsMIMstMstiPortClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstMstiPortClearStats (INT4 i4FsMIMstMstiPort,
                                 INT4 i4FsMIMstInstanceIndex,
                                 INT4 i4SetValFsMIMstMstiPortClearStats)
{

    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        AstReleaseContext ();
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    if (NULL == AST_GET_PERST_INFO (i4FsMIMstInstanceIndex))
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId,
                                              i4FsMIMstInstanceIndex);
    if (NULL == pPerStPortInfo)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((MST_FALSE == pPerStPortInfo->bMstiPortClearStats)
        && (MST_TRUE == i4SetValFsMIMstMstiPortClearStats))
    {
        pPerStPortInfo->bMstiPortClearStats = MST_TRUE;
        MstResetPerStPortCounters ((UINT2) i4FsMIMstInstanceIndex,
                                   u2LocalPortId);
        pPerStPortInfo->bMstiPortClearStats = MST_FALSE;
    }
    else
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstMstiPortClearStats
 Input       :  The Indices
                FsMIMstMstiPort
                FsMIMstInstanceIndex

                The Object 
                testValFsMIMstMstiPortClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstMstiPortClearStats (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIMstMstiPort,
                                    INT4 i4FsMIMstInstanceIndex,
                                    INT4 i4TestValFsMIMstMstiPortClearStats)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstSelectContext (u4ContextId))
    {
        return SNMP_FAILURE;
    }

    if (MST_SUCCESS != AstValidatePortEntry (u2LocalPortId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (MstValidateInstanceEntry (i4FsMIMstInstanceIndex) != MST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP:Such a Instance Does not exist!\n");
        AstReleaseContext ();
        return (INT1) SNMP_FAILURE;
    }

    if ((MST_FALSE != i4TestValFsMIMstMstiPortClearStats) &&
        (MST_TRUE != i4TestValFsMIMstMstiPortClearStats))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    AstReleaseContext ();
    return SNMP_SUCCESS;
}

/* MSTP clear statistics feature*/

/****************************************************************************
 Function    :  nmhGetFsMIMstCistPortBpduGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                retValFsMIMstCistPortBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortBpduGuard (INT4 i4FsMIMstCistPort,
                                INT4 *pi4RetValFsMIMstCistPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortBpduGuard ((INT4) u2LocalPortId,
                                             pi4RetValFsMIMstCistPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhGetFsMIMstCistPortRootGuard
  Input       :  The Indices
                 FsMIMstCistPort

                 The Object
                 retValFsMIMstCistPortRootGuard
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIMstCistPortRootGuard (INT4 i4FsMIMstCistPort,
                                INT4 *pi4RetValFsMIMstCistPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRootGuard
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhGetFsMIMstCistPortRootInconsistentState
  Input       :  The Indices
                 FsMIMstCistPort

                 The Object
                 retValFsMIMstCistPortRootInconsistentState
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIMstCistPortRootInconsistentState (INT4 i4FsMIMstCistPort,
                                            INT4
                                            *pi4RetValFsMIMstCistPortRootInconsistentState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstCistPortRootInconsistentState
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortRootInconsistentState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhGetFsMIMstMstiPortRootInconsistentState
  Input       :  The Indices
          FsMIMstMstiPort
                 FsMIMstInstanceIndex

                 The Object
                 retValFsMIMstMstiPortRootInconsistentState
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstMstiPortRootInconsistentState (INT4 i4FsMIMstMstiPort,
                                            INT4 i4FsMIMstInstanceIndex,
                                            INT4
                                            *pi4RetValFsMIMstMstiPortRootInconsistentState)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortRootInconsistentState ((INT4) u2LocalPortId,
                                                  i4FsMIMstInstanceIndex,
                                                  pi4RetValFsMIMstMstiPortRootInconsistentState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstCistPortBpduGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                setValFsMIMstCistPortBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstCistPortBpduGuard (INT4 i4FsMIMstCistPort,
                                INT4 i4SetValFsMIMstCistPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortBpduGuard ((INT4) u2LocalPortId,
                                             i4SetValFsMIMstCistPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhSetFsMIMstCistPortRootGuard
  Input       :  The Indices
                 FsMIMstCistPort

                 The Object
                 setValFsMIMstCistPortRootGuard
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIMstCistPortRootGuard (INT4 i4FsMIMstCistPort,
                                INT4 i4SetValFsMIMstCistPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsMstCistPortRootGuard ((INT4) u2LocalPortId,
                                      i4SetValFsMIMstCistPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIMstPortBpduGuardAction
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                setValFsMIMstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIMstPortBpduGuardAction (INT4 i4FsMIMstCistPort,
                                  INT4 i4SetValFsMIMstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstPortBpduGuardAction ((INT4) u2LocalPortId,
                                               i4SetValFsMIMstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIMstCistPortBpduGuard
 Input       :  The Indices
                FsMIMstCistPort

                The Object
                testValFsMIMstCistPortBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIMstCistPortBpduGuard (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIMstCistPort,
                                   INT4 i4TestValFsMIMstCistPortBpduGuard)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex (i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext ((INT4) u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortBpduGuard (pu4ErrorCode, (INT4) u2LocalPortId,
                                         i4TestValFsMIMstCistPortBpduGuard);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhTestv2FsMIMstCistPortRootGuard
  Input       :  The Indices
                 FsMIMstCistPort

                 The Object
                 testValFsMIMstCistPortRootGuard
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*************************************************************************/
INT1
nmhTestv2FsMIMstCistPortRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsMIMstCistPort,
                                   INT4 i4TestValFsMIMstCistPortRootGuard)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortRootGuard (pu4ErrorCode,
                                         (INT4) u2LocalPortId,
                                         i4TestValFsMIMstCistPortRootGuard);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
   Function    :  nmhGetFsMIMstCistPortErrorRecovery
   Input       :  The Indices
                  FsMIMstCistPort

                  The Object
                  retValFsMIMstCistPortErrorRecovery
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstCistPortErrorRecovery (INT4 i4FsMIMstCistPort,
                                    INT4 *pi4RetValFsMIMstCistPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstCistPortErrorRecovery ((INT4) u2LocalPortId,
                                                 pi4RetValFsMIMstCistPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;
}

/**************************************************************************** 
*      Function    :  nmhGetFsMIMstPortBpduInconsistentState 
*      Input       :  The Indices 
*                     FsMIMstCistPort 
*      
*                     The Object 
*                     retValFsMIMstPortBpduInconsistentState 
*      Output      :  The Get Low Lev Routine Take the Indices & 
*                     store the Value requested in the Return val. 
*      Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
* ****************************************************************************/
INT1
nmhGetFsMIMstPortBpduInconsistentState (INT4 i4FsMIMstCistPort,
                                        INT4
                                        *pi4RetValFsMIMstPortBpduInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortBpduInconsistentState ((INT4) u2LocalPortId,
                                                     pi4RetValFsMIMstPortBpduInconsistentState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIMstPortBpduGuardAction
 Input       :  The Indices
                FsMIMstCistPort

                The Object 
                retValFsMIMstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIMstPortBpduGuardAction (INT4 i4FsMIMstCistPort,
                                  INT4 *pi4RetValFsMIMstPortBpduGuardAction)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMstPortBpduGuardAction ((INT4) u2LocalPortId,
                                               pi4RetValFsMIMstPortBpduGuardAction);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
   Function    :  nmhSetFsMIMstCistPortErrorRecovery
   Input       :  The Indices
                  FsMIMstCistPort

                  The Object
                  setValFsMIMstCistPortErrorRecovery
   Output      :  The Set Low Lev Routine Take the Indices &
                  Sets the Value accordingly.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************************/
INT1
nmhSetFsMIMstCistPortErrorRecovery (INT4 i4FsMIMstCistPort,
                                    INT4 i4SetValFsMIMstCistPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsMstCistPortErrorRecovery ((INT4) u2LocalPortId,
                                                 i4SetValFsMIMstCistPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
  Function    :  nmhTestv2FsMIMstCistPortErrorRecovery
  Input       :  The Indices
                 FsMIMstCistPort

                 The Object
                 testValFsMIMstCistPortErrorRecovery
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
                                                                                                                      ***************************************************************************/
INT1
nmhTestv2FsMIMstCistPortErrorRecovery (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIMstCistPort,
                                       INT4
                                       i4TestValFsMIMstCistPortErrorRecovery)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsMstCistPortErrorRecovery (pu4ErrorCode, (INT4) u2LocalPortId,
                                             i4TestValFsMIMstCistPortErrorRecovery);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortTCDetectedCount
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortTCDetectedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortTCDetectedCount (INT4 i4FsMIMstCistPort,
                                      UINT4
                                      *pu4RetValFsMIMstCistPortTCDetectedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;
    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortTCDetectedCount
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortTCDetectedCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortTCReceivedCount
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortTCReceivedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortTCReceivedCount (INT4 i4FsMIMstCistPort,
                                      UINT4
                                      *pu4RetValFsMIMstCistPortTCReceivedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortTCReceivedCount
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortTCReceivedCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortTCDetectedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortTCDetectedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortTCDetectedTimeStamp (INT4 i4FsMIMstCistPort,
                                          UINT4
                                          *pu4RetValFsMIMstCistPortTCDetectedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortTCDetectedTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortTCDetectedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortTCReceivedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortTCReceivedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortTCReceivedTimeStamp (INT4 i4FsMIMstCistPort,
                                          UINT4
                                          *pu4RetValFsMIMstCistPortTCReceivedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortTCReceivedTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortTCReceivedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortRcvInfoWhileExpCount
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortRcvInfoWhileExpCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortRcvInfoWhileExpCount (INT4 i4FsMIMstCistPort,
                                           UINT4
                                           *pu4RetValFsMIMstCistPortRcvInfoWhileExpCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortRcvInfoWhileExpCount
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortRcvInfoWhileExpCount);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortRcvInfoWhileExpTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortRcvInfoWhileExpTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortRcvInfoWhileExpTimeStamp (INT4 i4FsMIMstCistPort,
                                               UINT4
                                               *pu4RetValFsMIMstCistPortRcvInfoWhileExpTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortRcvInfoWhileExpTimeStamp
        ((INT4) u2LocalPortId,
         pu4RetValFsMIMstCistPortRcvInfoWhileExpTimeStamp);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortProposalPktsSent
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortProposalPktsSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortProposalPktsSent (INT4 i4FsMIMstCistPort,
                                       UINT4
                                       *pu4RetValFsMIMstCistPortProposalPktsSent)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortProposalPktsSent
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortProposalPktsSent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortProposalPktsRcvd
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortProposalPktsRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortProposalPktsRcvd (INT4 i4FsMIMstCistPort,
                                       UINT4
                                       *pu4RetValFsMIMstCistPortProposalPktsRcvd)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortProposalPktsRcvd
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortProposalPktsRcvd);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortProposalPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortProposalPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortProposalPktSentTimeStamp (INT4 i4FsMIMstCistPort,
                                               UINT4
                                               *pu4RetValFsMIMstCistPortProposalPktSentTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortProposalPktSentTimeStamp
        ((INT4) u2LocalPortId,
         pu4RetValFsMIMstCistPortProposalPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortProposalPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortProposalPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortProposalPktRcvdTimeStamp (INT4 i4FsMIMstCistPort,
                                               UINT4
                                               *pu4RetValFsMIMstCistPortProposalPktRcvdTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortProposalPktRcvdTimeStamp
        ((INT4) u2LocalPortId,
         pu4RetValFsMIMstCistPortProposalPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortAgreementPktSent
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortAgreementPktSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortAgreementPktSent (INT4 i4FsMIMstCistPort,
                                       UINT4
                                       *pu4RetValFsMIMstCistPortAgreementPktSent)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortAgreementPktSent
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortAgreementPktSent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortAgreementPktRcvd
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortAgreementPktRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortAgreementPktRcvd (INT4 i4FsMIMstCistPort,
                                       UINT4
                                       *pu4RetValFsMIMstCistPortAgreementPktRcvd)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortAgreementPktRcvd
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortAgreementPktRcvd);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortAgreementPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortAgreementPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortAgreementPktSentTimeStamp (INT4 i4FsMIMstCistPort,
                                                UINT4
                                                *pu4RetValFsMIMstCistPortAgreementPktSentTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortAgreementPktSentTimeStamp
        ((INT4) u2LocalPortId,
         pu4RetValFsMIMstCistPortAgreementPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortAgreementPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortAgreementPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortAgreementPktRcvdTimeStamp (INT4 i4FsMIMstCistPort,
                                                UINT4
                                                *pu4RetValFsMIMstCistPortAgreementPktRcvdTimeStamp)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortAgreementPktRcvdTimeStamp
        ((INT4) u2LocalPortId,
         pu4RetValFsMIMstCistPortAgreementPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortImpStateOccurCount
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortImpStateOccurCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortImpStateOccurCount (INT4 i4FsMIMstCistPort,
                                         UINT4
                                         *pu4RetValFsMIMstCistPortImpStateOccurCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortImpStateOccurCount
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortImpStateOccurCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortImpStateOccurTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortImpStateOccurTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortImpStateOccurTimeStamp (INT4 i4FsMIMstCistPort,
                                             UINT4
                                             *pu4RetValFsMIMstCistPortImpStateOccurTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortImpStateOccurTimeStamp
        ((INT4) u2LocalPortId, pu4RetValFsMIMstCistPortImpStateOccurTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstCistPortOldPortState
 * * Input       :  The Indices
 * *                FsMIMstCistPort
 * *
 * *                The Object
 * *                retValFsMIMstCistPortOldPortState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstCistPortOldPortState (INT4 i4FsMIMstCistPort,
                                   INT4 *pi4RetValFsMIMstCistPortOldPortState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstCistPort, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsMstCistPortOldPortState
        ((INT4) u2LocalPortId, pi4RetValFsMIMstCistPortOldPortState);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortLoopInconsistentState
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortLoopInconsistentState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortLoopInconsistentState (INT4 i4FsMIMstMstiPort,
                                            INT4 i4FsMIMstInstanceIndex,
                                            INT4
                                            *pi4RetValFsMIMstMstiPortLoopInconsistentState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortLoopInconsistentState ((INT4) u2LocalPortId,
                                                  i4FsMIMstInstanceIndex,
                                                  pi4RetValFsMIMstMstiPortLoopInconsistentState);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * *Function    :  nmhGetFsMIMstMstiPortTCDetectedCount
 * *Input       :  The Indices
 * *               FsMIMstMstiPort
 * *               FsMIMstInstanceIndex
 * *
 * *               The Object
 * *               retValFsMIMstMstiPortTCDetectedCount
 * *Output      :  The Get Low Lev Routine Take the Indices &
 * *               store the Value requested in the Return val.
 * *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTCDetectedCount (INT4 i4FsMIMstMstiPort,
                                      INT4 i4FsMIMstInstanceIndex,
                                      UINT4
                                      *pu4RetValFsMIMstMstiPortTCDetectedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortTCDetectedCount ((INT4) u2LocalPortId,
                                            i4FsMIMstInstanceIndex,
                                            pu4RetValFsMIMstMstiPortTCDetectedCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortTCReceivedCount
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortTCReceivedCount
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTCReceivedCount (INT4 i4FsMIMstMstiPort,
                                      INT4 i4FsMIMstInstanceIndex,
                                      UINT4
                                      *pu4RetValFsMIMstMstiPortTCReceivedCount)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortTCReceivedCount ((INT4) u2LocalPortId,
                                            i4FsMIMstInstanceIndex,
                                            pu4RetValFsMIMstMstiPortTCReceivedCount);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortTCDetectedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortTCDetectedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTCDetectedTimeStamp (INT4 i4FsMIMstMstiPort,
                                          INT4 i4FsMIMstInstanceIndex,
                                          UINT4
                                          *pu4RetValFsMIMstMstiPortTCDetectedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortTCDetectedTimeStamp ((INT4) u2LocalPortId,
                                                i4FsMIMstInstanceIndex,
                                                pu4RetValFsMIMstMstiPortTCDetectedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortTCReceivedTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortTCReceivedTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortTCReceivedTimeStamp (INT4 i4FsMIMstMstiPort,
                                          INT4 i4FsMIMstInstanceIndex,
                                          UINT4
                                          *pu4RetValFsMIMstMstiPortTCReceivedTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortTCReceivedTimeStamp ((INT4) u2LocalPortId,
                                                i4FsMIMstInstanceIndex,
                                                pu4RetValFsMIMstMstiPortTCReceivedTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortProposalPktsSent
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortProposalPktsSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortProposalPktsSent (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortProposalPktsSent)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortProposalPktsSent ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortProposalPktsSent);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortProposalPktsRcvd
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortProposalPktsRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortProposalPktsRcvd (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortProposalPktsRcvd)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortProposalPktsRcvd ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortProposalPktsRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortProposalPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortProposalPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortProposalPktSentTimeStamp (INT4 i4FsMIMstMstiPort,
                                               INT4 i4FsMIMstInstanceIndex,
                                               UINT4
                                               *pu4RetValFsMIMstMstiPortProposalPktSentTimeStamp)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortProposalPktSentTimeStamp ((INT4) u2LocalPortId,
                                                     i4FsMIMstInstanceIndex,
                                                     pu4RetValFsMIMstMstiPortProposalPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortProposalPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortProposalPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortProposalPktRcvdTimeStamp (INT4 i4FsMIMstMstiPort,
                                               INT4 i4FsMIMstInstanceIndex,
                                               UINT4
                                               *pu4RetValFsMIMstMstiPortProposalPktRcvdTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortProposalPktRcvdTimeStamp ((INT4) u2LocalPortId,
                                                     i4FsMIMstInstanceIndex,
                                                     pu4RetValFsMIMstMstiPortProposalPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortAgreementPktSent
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortAgreementPktSent
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortAgreementPktSent (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortAgreementPktSent)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortAgreementPktSent ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortAgreementPktSent);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortAgreementPktRcvd
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortAgreementPktRcvd
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortAgreementPktRcvd (INT4 i4FsMIMstMstiPort,
                                       INT4 i4FsMIMstInstanceIndex,
                                       UINT4
                                       *pu4RetValFsMIMstMstiPortAgreementPktRcvd)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortAgreementPktRcvd ((INT4) u2LocalPortId,
                                             i4FsMIMstInstanceIndex,
                                             pu4RetValFsMIMstMstiPortAgreementPktRcvd);

    AstReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortAgreementPktSentTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortAgreementPktSentTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortAgreementPktSentTimeStamp (INT4 i4FsMIMstMstiPort,
                                                INT4 i4FsMIMstInstanceIndex,
                                                UINT4
                                                *pu4RetValFsMIMstMstiPortAgreementPktSentTimeStamp)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortAgreementPktSentTimeStamp ((INT4) u2LocalPortId,
                                                      i4FsMIMstInstanceIndex,
                                                      pu4RetValFsMIMstMstiPortAgreementPktSentTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortAgreementPktRcvdTimeStamp
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortAgreementPktRcvdTimeStamp
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortAgreementPktRcvdTimeStamp (INT4 i4FsMIMstMstiPort,
                                                INT4 i4FsMIMstInstanceIndex,
                                                UINT4
                                                *pu4RetValFsMIMstMstiPortAgreementPktRcvdTimeStamp)
{

    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortAgreementPktRcvdTimeStamp ((INT4) u2LocalPortId,
                                                      i4FsMIMstInstanceIndex,
                                                      pu4RetValFsMIMstMstiPortAgreementPktRcvdTimeStamp);

    AstReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 * * Function    :  nmhGetFsMIMstMstiPortOldPortState
 * * Input       :  The Indices
 * *                FsMIMstMstiPort
 * *                FsMIMstInstanceIndex
 * *
 * *                The Object
 * *                retValFsMIMstMstiPortOldPortState
 * * Output      :  The Get Low Lev Routine Take the Indices &
 * *                store the Value requested in the Return val.
 * * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsMIMstMstiPortOldPortState (INT4 i4FsMIMstMstiPort,
                                   INT4 i4FsMIMstInstanceIndex,
                                   INT4 *pi4RetValFsMIMstMstiPortOldPortState)
{
    UINT4               u4ContextId = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT1                i1RetVal = AST_INIT_VAL;

    if (AstGetContextInfoFromIfIndex ((UINT4) i4FsMIMstMstiPort,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsMstMstiPortOldPortState ((INT4) u2LocalPortId,
                                         i4FsMIMstInstanceIndex,
                                         pi4RetValFsMIMstMstiPortOldPortState);

    AstReleaseContext ();

    return i1RetVal;
}
