/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mstpcli.c,v 1.173.2.1 2018/03/02 13:16:51 siva Exp $
*
* Description:  Function Definition for CLI MSTP Commands
*********************************************************************/
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : mstpcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : MSTP                                             |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Function Definition for CLI MSTP Commands        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 * |   1    | 3rd Nov 2004    |    Original                                    |
 * |        |                 |                                                |
 *  ---------------------------------------------------------------------------
 *   
 */
#ifdef MSTP_WANTED
#include "astminc.h"
#include "fsrstlow.h"
#include "stpcli.h"
#include "stpclipt.h"
#include "astpbcli.h"
#include "fsmprslw.h"
#include "fspvrslw.h"

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : MstCliSetSystemShut                               */
/*                                                                          */
/*     DESCRIPTION      : This function sets the system control to shut     */
/*                                                                          */
/*     INPUT            :  CliHandle- Handle to the Cli Context             */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/

INT4
MstCliSetSystemShut (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstSystemControl (&u4ErrCode, MST_SNMP_SHUTDOWN) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstSystemControl (MST_SNMP_SHUTDOWN) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetSystemControl                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the system control (start )     */
/*                         of the MSTP after Shutting down the RSTP          */
/*                          and  also enables the MSTP Module.               */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to CLI Context                    */
/*                        u4SystemControl- System Control Value              */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliSetSystemCtrl (tCliHandle CliHandle)
{

    UINT4               u4ErrCode = 0;

    if (AST_IS_I_COMPONENT () == RST_TRUE)
    {
        CliPrintf (CliHandle, "\rMSTP cannot be enabled on I-Component\r\n");
        return CLI_SUCCESS;
    }

    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        CliPrintf (CliHandle,
                   "\r%%MSTP cannot be enabled in Transparent Mode\r\n");
        return CLI_FAILURE;
    }

    if (AST_IS_RST_STARTED ())
    {
        CliPrintf (CliHandle,
                   "\rSpanning Tree enabled protocol is RSTP, now RSTP is being"
                   " shutdown and MSTP is being enabled\r\n");

        /* Shutting Down RSTP */

        if (nmhTestv2FsRstSystemControl (&u4ErrCode, RST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsRstSystemControl (RST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (AST_IS_PVRST_STARTED ())
    {
        CliPrintf (CliHandle,
                   "\rSpanning Tree enabled protocol is PVRST, now PVRST"
                   " is being shutdown and MSTP is being enabled\r\n");

        /* Shutting Down RSTP */
#ifdef PVRST_WANTED
        if (nmhTestv2FsPvrstSystemControl (&u4ErrCode, PVRST_SNMP_SHUTDOWN) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsPvrstSystemControl (PVRST_SNMP_SHUTDOWN) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
#endif
    }

    if (nmhTestv2FsMstSystemControl (&u4ErrCode, MST_SNMP_START) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstSystemControl (MST_SNMP_START) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*Enable MSTP Module is started and enabled */

    MstCliSetModStatus (CliHandle, MST_ENABLED);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetModuleStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the module status (             */
/*                        Enabled/Disabled) of the MSTP Module.              */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*     u4MstModStatus-- MSTP Module Status.                                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliSetModStatus (tCliHandle CliHandle, UINT4 u4MstModStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstModuleStatus (&u4ErrCode, u4MstModStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsMstModuleStatus (u4MstModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetFwdTime                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Forward Time for            */
/*                        MSTP module                                        */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                        u4FwdTime-ForwardDelay                             */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/

INT4
MstCliSetFwdTime (tCliHandle CliHandle, UINT4 u4FwdTime)
{
    UINT4               u4ErrCode = 0;

    u4FwdTime = AST_SYS_TO_MGMT (u4FwdTime);

    if (nmhTestv2FsMstCistBridgeForwardDelay (&u4ErrCode, u4FwdTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstCistBridgeForwardDelay (u4FwdTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetHelloTime                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        MSTP module                                        */
/*                                                                           */
/*     INPUT            : tCLiHandle --Handle to the CLI Context             */
/*                        u4HelloTime-HelloTime                              */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/

INT4
MstCliSetHelloTime (tCliHandle CliHandle, UINT4 u4HelloTime)
{
    UINT4               u4ErrCode = 0;

    u4HelloTime = AST_SYS_TO_MGMT (u4HelloTime);

    if (nmhTestv2FsMstCistBridgeHelloTime (&u4ErrCode, u4HelloTime) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstCistBridgeHelloTime (u4HelloTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetMaxAge                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maximum Age for             */
/*                        MSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                          u4MaxAge--MaximumAge                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliSetMaxAge (tCliHandle CliHandle, UINT4 u4MaxAge)
{
    UINT4               u4ErrCode = 0;

    u4MaxAge = AST_SYS_TO_MGMT (u4MaxAge);

    if (nmhTestv2FsMstCistBridgeMaxAge (&u4ErrCode, u4MaxAge) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstCistBridgeMaxAge (u4MaxAge) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetTxHoldCount                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets and rests the Transmit Hold     */
/*                        Count for the MSTP Module                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context.             */
/*                        u4TxHoldCount- TransmitHoldCount                   */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetBridgeTxHoldCount (tCliHandle CliHandle, UINT4 u4TxHoldCount)
{

    UINT4               u4ErrCode = 0;

    /* Transmit Hold Count */
    if (nmhTestv2FsMstTxHoldCount (&u4ErrCode, u4TxHoldCount) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstTxHoldCount (u4TxHoldCount) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetMaxHops                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets and resets the Max number of    */
/*                          Hops permitted in the MST                        */
/*                        .                                                  */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to the CLI Context                */
/*                          u4MaxHops- Maximum Hops                          */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetMaxHops (tCliHandle CliHandle, UINT4 u4MaxHops)
{
    UINT4               u4ErrCode = 0;

    /* Maximum Hops */
    if (nmhTestv2FsMstMaxHopCount (&u4ErrCode, u4MaxHops) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMstMaxHopCount (u4MaxHops) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetBrgPriority                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Bridge Priority for the     */
/*                        MSTP module.                                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                          u4BrgPriority-BridgePriority                     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetBrgPriority (tCliHandle CliHandle, UINT4 u4InstanceId,
                      UINT4 u4Priority)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();
    INT4                i4RootVal = 0;

    if (u4InstanceId == MST_CIST_CONTEXT)
    {
        if (nmhTestv2FsMstCistBridgePriority (&u4ErrCode, (INT4) u4Priority) ==
            SNMP_FAILURE)
        {
            if (AST_IS_PVRST_STARTED ())
            {
                CliPrintf (CliHandle,
                           "\r%% Bridge Priority for PVRST cannot be set here\r\n");
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "\r%% Bridge Priority must be in increments of 4096"
                       " and can be upto 61440\r\n");
            CliPrintf (CliHandle, "\r%% Allowed values are:\r\n"
                       "0     4096  8192  12288 16384 20480 24576 28672\r\n"
                       "32768 36864 40960 45056 49152 53248 57344 61440\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMstCistBridgePriority ((INT4) u4Priority) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIMstMstiBridgePriority
            (&u4ErrCode, (INT4) u4CurrContextId, (INT4) u4InstanceId,
             (INT4) u4Priority) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }

        if (nmhGetFsMIMstiRootPriority
            ((INT4) u4CurrContextId, (INT4) u4InstanceId,
             &i4RootVal) != SNMP_FAILURE)
        {
            if (i4RootVal != CLI_STP_ROOT_DEFAULT)
            {
                if (nmhSetFsMIMstiRootPriority
                    ((INT4) u4CurrContextId, (INT4) u4InstanceId,
                     CLI_STP_ROOT_DEFAULT) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

        if (nmhSetFsMIMstMstiBridgePriority
            ((INT4) u4CurrContextId, (INT4) u4InstanceId,
             (INT4) u4Priority) == SNMP_FAILURE)
        {
            if (nmhSetFsMIMstiRootPriority
                ((INT4) u4CurrContextId, (INT4) u4InstanceId,
                 i4RootVal) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetRootPriority                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Root Priority for the       */
/*                        MSTP module.                                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                        u4InstanceId - Instance ID                         */
/*                          u4BrgPriority-BridgePriority                     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetRootPriority (tCliHandle CliHandle, UINT4 u4InstanceId,
                       UINT4 u4Priority)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIMstiRootPriority
        (&u4ErrCode, (INT4) u4CurrContextId, (INT4) u4InstanceId,
         (INT4) u4Priority) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIMstiRootPriority
        ((INT4) u4CurrContextId, (INT4) u4InstanceId,
         (INT4) u4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetBrgVersion                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the version to STP              */
/*                          Compatible or RSTP Compatible or MSTP Compatible */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                        u4Version - STP Version                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetBrgVersion (tCliHandle CliHandle, UINT4 u4Version)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstForceProtocolVersion (&u4ErrCode, u4Version) ==
        SNMP_FAILURE)
    {

        return (CLI_FAILURE);

    }
    if (nmhSetFsMstForceProtocolVersion ((INT4) u4Version) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetMaxInstanceNumber                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the  Maximun Instance value     */
/*                        for the Context.                                   */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        i4ValFsMstMaxMstInstanceNumber - Maximun Instance  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetMaxInstanceNumber (tCliHandle CliHandle,
                            INT4 i4ValFsMstMaxMstInstanceNumber)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstMaxMstInstanceNumber (&u4ErrCode,
                                            i4ValFsMstMaxMstInstanceNumber) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMstMaxMstInstanceNumber (i4ValFsMstMaxMstInstanceNumber) ==
        SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetInstanceMapping                           */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables the  MSTP Instance  */
/*                        Port Mapping.                                      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4ValFsMstInstanceMap - enable/disable             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetInstanceMapping (tCliHandle CliHandle, UINT4 u4ValFsMstInstanceMap)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIMstInstPortsMap (&u4ErrCode,
                                      (INT4) u4CurrContextId,
                                      (INT4) u4ValFsMstInstanceMap) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMIMstInstPortsMap ((INT4) u4CurrContextId,
                                   (INT4) u4ValFsMstInstanceMap) ==
        SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  MstCliSetPathCostCalculation                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathcostCalc-PathCostCalcMethod                  */
/*                        (Dynamic/Manual)                                   */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc)
{
    UINT4               u4ErrCode = 0;

    if (u4PathcostCalc == AST_PATHCOST_CALC_DYNAMIC)
    {
        u4PathcostCalc = AST_SNMP_TRUE;
    }
    else if (u4PathcostCalc == AST_PATHCOST_CALC_MANUAL)
    {
        u4PathcostCalc = AST_SNMP_FALSE;
    }
    if (nmhTestv2FsMstCistDynamicPathcostCalculation (&u4ErrCode,
                                                      u4PathcostCalc) ==
        SNMP_FAILURE)
    {

        return (CLI_FAILURE);

    }
    if (nmhSetFsMstCistDynamicPathcostCalculation (u4PathcostCalc)
        == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :  MstCliSetLaggPathCostCalculation                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the PathCost calculation method */
/*                        for Link Aggregated ports                          */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4PathcostCalc-Enable/Disable                      */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetLaggPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsMstCalcPortPathCostOnSpeedChg (&u4ErrCode,
                                                  u4PathcostCalc) ==
        SNMP_FAILURE)
    {

        return (CLI_FAILURE);

    }
    nmhSetFsMstCalcPortPathCostOnSpeedChg (u4PathcostCalc);
    return CLI_SUCCESS;

}

/*MST Configrn Funcn*/

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetRegionName                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the   Region                    */
/*                        name  for the Mst Region.                          */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to the CLI Context                */
/*                        au1RegionName-Region Name                          */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstSetRegnName (tCliHandle CliHandle, UINT1 *pu1RegionName)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tAstBridgeEntry    *pBrgInfo = NULL;
    UINT4               u4ErrCode = 0;
    UINT1               au1DefName[MST_CONFIG_NAME_LEN];
    UINT1               au1ConfName[MST_CONFIG_NAME_LEN];
    UINT1              *pu1ConfigName = au1ConfName;
    UINT1               u1Indx = 0;

    MEMSET (au1DefName, 0, MST_CONFIG_NAME_LEN);
    Name.pu1_OctetList = au1DefName;

    pBrgInfo = AST_GET_BRGENTRY ();

    if (STRLEN (pu1RegionName) != 0)
    {
        /*When  Setting the RegionName */
        Name.i4_Length = STRLEN (pu1RegionName);
        if (Name.i4_Length > MST_CONFIG_NAME_LEN)
        {
            return CLI_FAILURE;
        }

        STRNCPY (Name.pu1_OctetList, pu1RegionName, Name.i4_Length);
    }
    else
    {
        MEMSET (au1ConfName, 0, MST_CONFIG_NAME_LEN);
        /* When Resetting the Region Name */
        for (u1Indx = 0; u1Indx < AST_MAC_ADDR_SIZE; u1Indx++)
        {
            SPRINTF ((CHR1 *) pu1ConfigName, "%02x:",
                     pBrgInfo->BridgeAddr[u1Indx]);
            pu1ConfigName += 3;
        }
        pu1ConfigName -= 1;
        *pu1ConfigName = '\0';

        Name.i4_Length = MST_CONFIG_NAME_LEN;
        STRNCPY (Name.pu1_OctetList, au1ConfName, Name.i4_Length);
    }

    /* Region Name */

    if (nmhTestv2FsMstMstiRegionName (&u4ErrCode, &Name) == SNMP_FAILURE)
    {

        return (CLI_FAILURE);
    }
    if (nmhSetFsMstMstiRegionName (&Name) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetRegionVersion--Revision                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the  RegionVersion              */
/*                        for the Mst Region.                                */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the CLI Context               */
/*                        u4RegionVersion- RegionVersion                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetRegionVersion (tCliHandle CliHandle, UINT4 u4RegionVersion)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstMstiRegionVersion (&u4ErrCode, u4RegionVersion) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMstMstiRegionVersion (u4RegionVersion) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetVlanInstanceMapping                       */
/*                                                                           */
/*     DESCRIPTION      : This function maps or unmaps the specified vlan to */
/*                        the specified Spanning Tree Instance.              */
/*                                                                           */
/*     INPUT            :  tCliHandle -Handle to the CLI Context             */
/*                         u4InstanceId-Instance Id                          */
/*                         *pau1MstVlanList- Vlan List                       */
/*                         u1Action- Set/No Command                          */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetVlanInstanceMapping (tCliHandle CliHandle, UINT4 u4InstanceId,
                              UINT1 *pau1MstVlanList, UINT1 u1Action)
{
    tSNMP_OCTET_STRING_TYPE *pMstVlanListAll = NULL;
    tSNMP_OCTET_STRING_TYPE *pMstVlanList = NULL;
    UINT4               u4ErrCode = 0;
    UINT1               au1VlanMapList[MST_VLANMAP_LIST_SIZE];
    UINT1              *pu1ListPtr = NULL;

    /* If Vlan Range is passed.. */
    if ((pMstVlanListAll =
         allocmem_octetstring (MST_VLANMAP_LIST_SIZE)) == NULL)
    {
        return (CLI_FAILURE);
    }
    if ((pMstVlanList = allocmem_octetstring (MST_VLANMAP_LIST_SIZE)) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        return (CLI_FAILURE);
    }
    MEMSET (pMstVlanListAll->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
    MEMSET (pMstVlanList->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);

    pMstVlanListAll->i4_Length = MST_VLAN_LIST_SIZE;
    pMstVlanList->i4_Length = MST_VLANMAP_LIST_SIZE;

    if (pau1MstVlanList != NULL)
    {
        CLI_MEMCPY (pMstVlanListAll->pu1_OctetList, pau1MstVlanList,
                    pMstVlanListAll->i4_Length);
    }

    switch (u1Action)
    {
        case MST_SET_CMD:

            if (nmhTestv2FsMstSetVlanList
                (&u4ErrCode, u4InstanceId, pMstVlanListAll) == SNMP_FAILURE)
            {
                free_octetstring (pMstVlanListAll);
                free_octetstring (pMstVlanList);
                return (CLI_FAILURE);

            }

            if (nmhSetFsMstSetVlanList (u4InstanceId, pMstVlanListAll) ==
                SNMP_FAILURE)
            {

                CliPrintf (CliHandle,
                           "\r%% Mapping of Instance to VLAN ID Failed. \r\n");
                free_octetstring (pMstVlanListAll);
                free_octetstring (pMstVlanList);
                return CLI_FAILURE;
            }

            break;

        case MST_NO_CMD:

            if (MstValidateInstanceEntry (u4InstanceId) == MST_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");
                free_octetstring (pMstVlanListAll);
                free_octetstring (pMstVlanList);
                return (CLI_FAILURE);
            }

            if (pau1MstVlanList == NULL)
            {
                /* Delete the Instance */

                /* Get the VLAN mappings and reset all the vlan mapped
                 * to this instance
                 */
                pu1ListPtr = pMstVlanListAll->pu1_OctetList;

                MEMSET (au1VlanMapList, 0, MST_VLANMAP_LIST_SIZE);
                nmhGetFsMstInstanceVlanMapped ((UINT2) u4InstanceId,
                                               pMstVlanList);
                MEMCPY (pu1ListPtr, au1VlanMapList, MST_VLANMAP_LIST_SIZE);
                pu1ListPtr += MST_VLANMAP_LIST_SIZE;

                MEMSET (au1VlanMapList, 0, MST_VLANMAP_LIST_SIZE);
                nmhGetFsMstInstanceVlanMapped2k ((UINT2) u4InstanceId,
                                                 pMstVlanList);
                MEMCPY (pu1ListPtr, au1VlanMapList, MST_VLANMAP_LIST_SIZE);
                pu1ListPtr += MST_VLANMAP_LIST_SIZE;

                MEMSET (au1VlanMapList, 0, MST_VLANMAP_LIST_SIZE);
                nmhGetFsMstInstanceVlanMapped3k ((UINT2) u4InstanceId,
                                                 pMstVlanList);
                MEMCPY (pu1ListPtr, au1VlanMapList, MST_VLANMAP_LIST_SIZE);
                pu1ListPtr += MST_VLANMAP_LIST_SIZE;

                MEMSET (au1VlanMapList, 0, MST_VLANMAP_LIST_SIZE);
                nmhGetFsMstInstanceVlanMapped4k ((UINT2) u4InstanceId,
                                                 pMstVlanList);
                MEMCPY (pu1ListPtr, au1VlanMapList, MST_VLANMAP_LIST_SIZE);

                /* If there is no vlan mapped to this specific instance,
                 * reset vlan list need not called. In this case, the instance
                 * can be directly deleted
                 */
                MEMSET (pMstVlanList->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
                if ((MEMCMP
                     (pMstVlanListAll->pu1_OctetList,
                      pMstVlanList->pu1_OctetList, MST_VLAN_LIST_SIZE)) != 0)
                {
                    if (nmhTestv2FsMstResetVlanList
                        (&u4ErrCode, u4InstanceId, pMstVlanListAll)
                        == SNMP_FAILURE)
                    {
                        free_octetstring (pMstVlanListAll);
                        free_octetstring (pMstVlanList);
                        return (CLI_FAILURE);

                    }

                    if (nmhSetFsMstResetVlanList (u4InstanceId,
                                                  pMstVlanListAll)
                        == SNMP_FAILURE)
                    {

                        free_octetstring (pMstVlanListAll);
                        free_octetstring (pMstVlanList);
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    /* If there is null vlan list mapped to a specific instance,
                     * reset vlan list need not called. Instead, that instance can be
                     *  deleted directly */

                    if (MstDeleteInstance ((UINT2) u4InstanceId) != MST_SUCCESS)
                    {
                        AST_TRC_ARG1 (AST_ALL_FAILURE_TRC |
                                      AST_CONTROL_PATH_TRC,
                                      "API:  instance %d deletion failed.. \n",
                                      u4InstanceId);
                        AST_DBG_ARG1 (AST_ALL_FAILURE_DBG | AST_MGMT_DBG |
                                      AST_EVENT_HANDLING_DBG,
                                      "API: instance %d deletion failed.. \n",
                                      u4InstanceId);
                        free_octetstring (pMstVlanListAll);
                        free_octetstring (pMstVlanList);
                        return (CLI_FAILURE);
                    }
                }

            }

            else
            {

                if (nmhTestv2FsMstResetVlanList
                    (&u4ErrCode, u4InstanceId, pMstVlanListAll) == SNMP_FAILURE)
                {
                    free_octetstring (pMstVlanListAll);
                    free_octetstring (pMstVlanList);
                    return (CLI_FAILURE);

                }

                if (nmhSetFsMstResetVlanList (u4InstanceId, pMstVlanListAll) ==
                    SNMP_FAILURE)
                {

                    CLI_FATAL_ERROR (CliHandle);
                    free_octetstring (pMstVlanListAll);
                    free_octetstring (pMstVlanList);
                    return CLI_FAILURE;
                }
            }

            break;

        default:
            free_octetstring (pMstVlanListAll);
            free_octetstring (pMstVlanList);
            return CLI_FAILURE;
    }

    free_octetstring (pMstVlanListAll);
    free_octetstring (pMstVlanList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetPortErrorRecovery                            */
/*                                                                           */
/*     DESCRIPTION      : This function set the Error Recovery Time for an   */
/*                   Interface                            */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle   -Handle to the CLI context             */
/*                        u4PortNum   - Port Number                          */
/*                        u4ErrorRecovery - ErrorRecove                      */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetPortErrorRecovery (tCliHandle CliHandle, UINT4 u4PortNum,
                            UINT4 u4ErrorRecovery)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4TmpErrorRecovery = 0;

    u4TmpErrorRecovery = AST_SYS_TO_MGMT (u4ErrorRecovery);

    if (nmhTestv2FsMstCistPortErrorRecovery
        (&u4ErrCode, (INT4) u4PortNum,
         (INT4) u4TmpErrorRecovery) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMstCistPortErrorRecovery
        ((INT4) u4PortNum, (INT4) u4TmpErrorRecovery) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetPortHellotime                                */
/*                                                                           */
/*     DESCRIPTION      : This function set the HelloTime for an Interface   */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle   -Handle to the CLI context             */
/*                        u4PortNum   - Port Number                          */
/*                        u4HelloTime - HelloTime                            */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetPortHelloTime (tCliHandle CliHandle, UINT4 u4PortNum,
                        UINT4 u4HelloTime)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4TmpHelloTime = 0;

    u4TmpHelloTime = AST_SYS_TO_MGMT (u4HelloTime);

    if (nmhTestv2FsMstCistPortHelloTime (&u4ErrCode, u4PortNum, u4TmpHelloTime)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMstCistPortHelloTime (u4PortNum, u4TmpHelloTime) ==
        SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/* This function validates the given pi1ModeName 
 * and returns the prompt in pi1DispStr if valid. 
 * Returns TRUE if given pi1ModeName is valid.
 * Returns FALSE if the given pi1ModeName is not valid. 
 * pi1ModeName is NULL to display the mode tree with
 * mode name and prompt string. */

INT1
MstpGetMstpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_MST);
    if (STRNCMP (pi1ModeName, CLI_MODE_MST, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    STRNCPY (pi1DispStr, "(config-mst)#", STRLEN ("(config-mst)#"));
    pi1DispStr[STRLEN ("(config-mst)#")] = '\0';
    return TRUE;
}

/* This function validates the given pi1ModeName 
 * and returns the prompt in pi1DispStr if valid. 
 * Returns TRUE if given pi1ModeName is valid.
 * Returns FALSE if the given pi1ModeName is not valid. 
 * pi1ModeName is NULL to display the mode tree with
 * mode name and prompt string. */

INT1
MstpGetVcmMstpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_MST);
    if (STRNCMP (pi1ModeName, CLI_MODE_MST, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    STRNCPY (pi1DispStr, "(config-switch-mst)#",
             STRLEN ("(config-switch-mst)#"));
    pi1DispStr[STRLEN ("(config-switch-mst)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCistSetPortProp                                 */
/*                                                                           */
/*     DESCRIPTION      : This function set the properties for an Interface  */
/*                         of MSTP                                           */
/*                                                                           */
/*     INPUT            : tCliHandle- Handle to the CLI Context              */
/*                        u4PortNum - PortNumber                             */
/*                        u1Flag    - Flag To Check which property to be set */
/*                        u4Val     - MstProperty                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCistSetPortProp (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4CmdType,
                    UINT4 u4Val)
{

    UINT4               u4ErrCode = 0;
    UINT1               u1OperStatus;
    /* Validating Parameters and setting PortPathCost */
    if (u4CmdType == STP_PORT_COST)
    {
        if (nmhTestv2FsMstCistPortAdminPathCost (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }

        /*Setting PathCost for a Port */
        if (nmhSetFsMstCistPortAdminPathCost (u4PortNum, u4Val) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* When the path cost is cleared, reset the pathcost configured variable
       for the port.And also calculate the pathcost dynamically */
    else if (u4CmdType == STP_NO_PORT_COST)
    {
        if (AstPathcostConfiguredFlag ((UINT2) u4PortNum, MST_CIST_CONTEXT)
            == RST_TRUE)
        {

            if (nmhTestv2FsMstCistPortAdminPathCost (&u4ErrCode, u4PortNum, 0)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;

            }

            /*Setting PathCost for a Port */
            if (nmhSetFsMstCistPortAdminPathCost (u4PortNum, 0) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
    }
    else if (u4CmdType == AST_PORT_STATE_DISABLED)
    {
        if (nmhTestv2FsMstCistForcePortState (&u4ErrCode, u4PortNum, u4Val) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMstCistForcePortState (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4CmdType == STP_LINK_TYPE)
    {
        if (nmhTestv2FsMstCistPortAdminP2P (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMstCistPortAdminP2P (u4PortNum, u4Val) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else if (u4CmdType == STP_PORTFAST)
    {

        if (nmhTestv2FsMstCistPortAdminEdgeStatus (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMstCistPortAdminEdgeStatus (u4PortNum, u4Val) ==
            SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u4Val != AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle,
                       "\rWarning: portfast should only be enabled on ports "
                       "connected to a single host.\r\nConnecting hubs, concentrators,"
                       "switches, bridges, etc... to this interface\r\nwhen portfast "
                       "is enabled, can cause temporary bridging loops.\r\n"
                       "Use with CAUTION\r\n\r\n");
            AstL2IwfGetPortOperStatus (STP_MODULE,
                                       AST_GET_IFINDEX (u4PortNum),
                                       &u1OperStatus);
            if (u1OperStatus != CFA_IF_DOWN)
            {

                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will"
                           "have effect \r\nonly when the interface is shutdown\r\n");
            }
        }
        else if (u4Val == AST_SNMP_FALSE)
        {
            AstL2IwfGetPortOperStatus (STP_MODULE,
                                       AST_GET_IFINDEX (u4PortNum),
                                       &u1OperStatus);
            if (u1OperStatus != CFA_IF_DOWN)
            {

                CliPrintf (CliHandle,
                           "\rWarning:Portfast has been configured on this port but will"
                           "have effect  \r\nonly when the interface is shutdown\r\n");
            }

        }

    }
    else if (u4CmdType == STP_PORT_PRIORITY)
    {

        if (nmhTestv2FsMstCistPortPriority (&u4ErrCode, u4PortNum,
                                            u4Val) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /*Setting PortPriority */
        if (nmhSetFsMstCistPortPriority (u4PortNum, u4Val) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if ((u4CmdType == CLI_AST_ENABLED) || (u4CmdType == CLI_AST_DISABLED))
    {

        if (nmhTestv2FsMstPortRowStatus (&u4ErrCode, u4PortNum, u4Val)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rUnable to create/delete MSTP port.\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMstPortRowStatus (u4PortNum, u4Val) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetPortProperties                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Properties for the MSTP     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle          -     Handle to the CLI Context */
/*                        u4PortNum          -     PortNumber                */
/*                        u4InstanceId       -     InstanceId                */
/*                        u4CmdType          -     COST / PRIORITY / DISABLE */
/*                        u1Value            -     Flag                      */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstSetPortProperties (tCliHandle CliHandle, UINT4 u4PortNum,
                      UINT4 u4InstanceId, UINT4 u4CmdType, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    /* Validating Parameters and setting PortPathCost */

    if (u4CmdType == STP_PORT_COST)
    {
        if (nmhTestv2FsMstMstiPortAdminPathCost
            (&u4ErrCode, u4PortNum, u4InstanceId, u4Value) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /*Setting PortPathCost for an Instance */
        if (nmhSetFsMstMstiPortAdminPathCost
            (u4PortNum, u4InstanceId, u4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);

        }

    }
    /* When the path cost is cleared, reset the pathcost configured variable
       for the port.And also calculate the pathcost dynamically */
    else if (u4CmdType == STP_NO_PORT_COST)
    {
        if (MstValidateInstanceEntry (u4InstanceId) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Invalid Instance Specified\r\n");
            return (CLI_FAILURE);
        }
        if (AstPathcostConfiguredFlag ((UINT2) u4PortNum, (UINT2) u4InstanceId)
            == RST_TRUE)
        {

            if (nmhTestv2FsMstMstiPortAdminPathCost
                (&u4ErrCode, u4PortNum, u4InstanceId, 0) == SNMP_FAILURE)
            {
                return CLI_FAILURE;

            }
            /*Setting PathCost for a Port */
            if (nmhSetFsMstMstiPortAdminPathCost (u4PortNum, u4InstanceId,
                                                  0) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
    }

    /* Validating Parameters and Setting the PortPriority */
    else if (u4CmdType == STP_PORT_PRIORITY)
    {
        /* Instances from 1-64 */

        if ((nmhTestv2FsMstMstiPortPriority
             (&u4ErrCode, u4PortNum, u4InstanceId, u4Value)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMstMstiPortPriority
            (u4PortNum, u4InstanceId, u4Value) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Validating Parameters and Setting the Port disable for Particular
     * Instance */
    else if (u4CmdType == AST_PORT_STATE_DISABLED)
    {
        /* Instances from 1-64 */

        if ((nmhTestv2FsMstMstiForcePortState
             (&u4ErrCode, u4PortNum, u4InstanceId, u4Value)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMstMstiForcePortState
            (u4PortNum, u4InstanceId, u4Value) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPotocolMigration                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port Protocol Migration i.e.*/
/*                        mcheck variable on all ports.                      */
/*                                                                           */
/*     INPUT            : tCliHandle   -Handle to the CLI context            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetProtocolMigration (tCliHandle CliHandle)
{

    INT4                i4CurPort = 0;
    INT4                i4NextPort = 0;
    UINT4               u4TempContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT4                i4OutCome = 0;
    UINT1               u1OperStatus = 0;

    i4OutCome = AstGetFirstPortInCurrContext (&i4NextPort);

    if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
    {
        CLI_SET_ERR (CLI_STP_SISP_PROTO_MIG_ERR);
        return CLI_FAILURE;
    }

    if (i4OutCome == RST_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No PortEntry from the table \r\n");
        return CLI_FAILURE;
    }

    do
    {
        if (AstGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4TempContextId,
                                          &u2LocalPortId) != RST_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%Retrieving Context ID fails\r\n");
            return CLI_FAILURE;
        }
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);
        if (CFA_IF_DOWN == u1OperStatus)
        {
            i4CurPort = i4NextPort;
            continue;
        }

        if (MstCliSetPortProtocolMigration (CliHandle,
                                            (INT4) u2LocalPortId) !=
            CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%Setting Protocol Migration on Port fails\r\n");
            return CLI_FAILURE;
        }

        i4CurPort = i4NextPort;
    }
    while (AstGetNextPortInCurrContext (i4CurPort, &i4NextPort) != RST_FAILURE);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPortProtocolMigration                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Protocol Migration on       */
/*                        a single port   i.e. mcheck variable on one port   */
/*                                                                           */
/*     INPUT            : tCliHandle   -  Handle to the CLiContext           */
/*                        u4IfIndex    -  Port Number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliSetPortProtocolMigration (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsMstCistPortProtocolMigration
        (&u4ErrCode, u4IfIndex, AST_SNMP_TRUE) != SNMP_FAILURE)
    {
        if (nmhSetFsMstCistPortProtocolMigration
            (u4IfIndex, AST_SNMP_TRUE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstSetDebug                                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the provides the debugging      */
/*                          supportfor the MSTP                              */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle -Handle to the Cli Context              */
/*                        u4MstTrace - Trace Value                           */
/*                        u1Action   - set/no command                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                     .                                     */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstSetDebug (tCliHandle CliHandle, UINT4 u4MstDbg, UINT1 u1Action)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4DbgVal = 0;
    UNUSED_PARAM (CliHandle);

    if (u1Action == MST_NO_CMD)
    {
        /* For Debug Disable, Get the existing Debug Value 
         * and negate that value
         */
        nmhGetFsMstDebug ((INT4 *) &u4DbgVal);
        u4DbgVal = u4DbgVal & (~u4MstDbg);
    }
    else if (u1Action == MST_SET_CMD)
    {
        u4DbgVal = u4MstDbg;
    }
    if (nmhTestv2FsMstDebug (&u4ErrCode, (INT4) u4DbgVal) == SNMP_FAILURE)
    {

        return CLI_FAILURE;

    }
    nmhSetFsMstDebug ((INT4) u4DbgVal);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : MstSetPortAutoEdge                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the auto edge status for a port.*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        u4Status - enable/disable                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MstSetPortAutoEdge (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortAutoEdgeStatus (&u4ErrCode, u4PortNum, u4Status)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsMstCistPortAutoEdgeStatus (u4PortNum, u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPortRestrictedRole                        */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Restricted Role on port*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local Port Number of the port.       */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliSetPortRestrictedRole (tCliHandle CliHandle, UINT2 u2LocalPort,
                             INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortRestrictedRole (&u4ErrCode, (INT4) u2LocalPort,
                                              i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsMstCistPortRestrictedRole ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPortRestrictedTcn                         */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Restricted Tcn on port*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local Port Number of the port.       */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliSetPortRestrictedTcn (tCliHandle CliHandle, UINT2 u2LocalPort,
                            INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortRestrictedTCN (&u4ErrCode, (INT4) u2LocalPort,
                                             i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsMstCistPortRestrictedTCN ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPortRootGuard                             */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Restricted Role on port*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u2LocalPort - Local Port Number of the port.       */
/*                        i4Value - True / False                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliSetPortRootGuard (tCliHandle CliHandle, UINT2 u2LocalPort, INT4 i4Value)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortRootGuard (&u4ErrCode, (INT4) u2LocalPort,
                                         i4Value) == SNMP_SUCCESS)
    {
        if (nmhSetFsMstCistPortRootGuard ((INT4) u2LocalPort, i4Value)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowSpanningTreeMethod                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Spanning Tree method    */
/*                        for RSTP either 16bit(short) or 32bitpathcost(Long)*/
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliShowSpanningTreeMethod (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\r\nSpanning Tree port pathcost method is Long\r\n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowSpanningTreeDetail                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Information for         */
/*                        "Show Spanning Tree Detail" Command                */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MstCliShowSpanningTreeDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Active)
{
    INT4                i4CurrentPort;
    INT4                i4NextPort;
    INT4                i4PortState;
    INT4                i4PortRole;
    INT4                i4OutCome;
    INT4                i4PortOutCome;
    INT4                i4NextInst;
    INT4                i4PrevInst;
    INT4                i4PrevInstMsti;
    INT4                i4PrevPortMsti = 0;
    INT4                i4NextPortMsti;
    INT4                i4NextPortInstance;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;
    UINT1               u1OperStatus;
    UINT1               u1Flag = 0;
    UINT1               u1Detail = 1;
    INT1                i1InstOutCome;
    UINT4               u4FlushCount = AST_INIT_VAL;
    INT4                i4FlushThreshold = AST_INIT_VAL;

    /* Display the Bridge Info in the CIST Context */
    MstCliDisplayBridgeDetails (CliHandle, u4ContextId, u1Detail);
    CliPrintf (CliHandle, "\r\n");

    /* Get the Current Port frm the Table */
    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while (i4OutCome != RST_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                u1Flag = 0;

                nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);
                nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);

                if (((i4PortState == AST_PORT_STATE_LEARNING) ||
                     (i4PortState == AST_PORT_STATE_FORWARDING))
                    && (i4PortRole != MST_PORT_ROLE_DISABLED))
                {
                    /* Only active ports need to be displayed if the command 
                     * executed is "show spanning-tree active . This flag is 
                     * used to qualify if a certain port's information needs
                     * to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {

                /* Display Port Details  in the Cist Context */
                MstCliDisplayPortDetails (CliHandle, u4ContextId, i4NextPort);

                u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }
            }
            /*Get the next Port from the Table */
        }
        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);
    }

    i1InstOutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           0, &i4NextInst);

    while ((i1InstOutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        if (i4NextInst == AST_TE_MSTID)
        {
            break;
        }

        CliPrintf (CliHandle, "\r\nMST%02d is executing mstp compatible"
                   " spanning-tree protocol\r\n", i4NextInst);

        nmhGetFsMIMstMstiTotalFlushCount (i4ContextId, i4NextInst,
                                          &u4FlushCount);
        nmhGetFsMIMstMstiFlushIndicationThreshold (i4ContextId, i4NextInst,
                                                   &i4FlushThreshold);
        CliPrintf (CliHandle, "Flush Invocations %d \r\n", u4FlushCount);
        CliPrintf (CliHandle, "Flush Indication threshold %d \r\n",
                   i4FlushThreshold);

        i4PortOutCome = AstGetNextMstiPortTableIndex (u4ContextId,
                                                      0, &i4NextPortMsti,
                                                      i4NextInst,
                                                      &i4NextPortInstance);

        while (i4PortOutCome != RST_FAILURE)
        {
            if (i4NextPortInstance == i4NextInst)
            {
                AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPortMsti,
                                           &u1OperStatus);

                if (u1OperStatus != CFA_IF_DOWN)
                {
                    if (u4Active == STP_ACTIVE_PORTS)
                    {
                        u1Flag = 0;
                        nmhGetFsMIMstMstiCurrentPortRole (i4NextPort,
                                                          i4NextInst,
                                                          &i4PortRole);
                        nmhGetFsMIMstMstiPortState (i4NextPortMsti,
                                                    i4NextPortInstance,
                                                    &i4PortState);
                        if (((i4PortState == AST_PORT_STATE_LEARNING)
                             || (i4PortState == AST_PORT_STATE_FORWARDING))
                            && ((i4PortRole != MST_PORT_ROLE_DISABLED)))
                        {
                            /* Only active ports need to be displayed if the command executed
                             * is "show spanning-tree active . This flag is used to qualify 
                             * if a certain port's information needs to be displayed. */

                            u1Flag = 1;
                        }
                    }

                    else
                    {
                        u1Flag = 1;
                    }

                    if (u1Flag)
                    {
                        if (MST_TRUE == MstIsPortMemberOfInst (i4NextPortMsti,
                                                               i4NextPortInstance))
                        {

                            MstCliDisplayMstiPortDetails (CliHandle,
                                                          u4ContextId,
                                                          i4NextPortInstance,
                                                          i4NextPortMsti);
                        }

                        MstCliDisplayMstiPortDetails (CliHandle,
                                                      u4ContextId,
                                                      i4NextPortInstance,
                                                      i4NextPortMsti);
                        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
                        if (u4PagingStatus == CLI_FAILURE)
                        {
                            break;
                            /* User Presses 'q' at the prompt and so quits */
                        }
                    }
                }
            }
            i4PrevPortMsti = i4NextPortMsti;

            i4PrevInstMsti = i4NextPortInstance;

            i4PortOutCome = AstGetNextMstiPortTableIndex (u4ContextId,
                                                          i4PrevPortMsti,
                                                          &i4NextPortMsti,
                                                          i4PrevInstMsti,
                                                          &i4NextPortInstance);
        }
        i4PrevInst = i4NextInst;
        i1InstOutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId,
                                                   i4PrevInst, &i4NextInst);
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliDisplayBridgeDetails                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Bridge Details         */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliDisplayBridgeDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 u1Detail)
{
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4Priority;
    INT4                i4ModStatus;
    INT4                i4Version = 0;
    INT4                i4Intf = CLI_FAILURE;
    INT4                i4RootPort;
    INT4                i4RootCost;
    INT4                i4HoldCount;
    INT4                i4HelloTime;
    INT4                i4Flag;
    UINT4               u4TopChange;
    UINT4               u4TopTime;
    tAstMacAddr         RootAddr;
    tAstMacAddr         MacAddress;
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Intf = NULL;
    UINT2               u2Prio;
    tSNMP_OCTET_STRING_TYPE CistRoot;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4DynamicPathcostCalc;
    INT4                i4FlushInterval = AST_INIT_VAL;
    INT4                i4FlushThreshold = AST_INIT_VAL;
    UINT4               u4FlushCount = AST_INIT_VAL;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;

    CistRoot.pu1_OctetList = &au1BrgIdBuf[0];
    CistRoot.i4_Length = CLI_RSTP_MAX_BRIDGEID_BUFFER;

    /*Spanning Tree Bridge Version */

    nmhGetFsMIMstModuleStatus (i4ContextId, &i4ModStatus);
    if (i4ModStatus != MST_DISABLED)
    {
        nmhGetFsMIMstForceProtocolVersion (i4ContextId, &i4Version);
    }

    nmhGetFsMIMstBpduGuard (i4ContextId, &i4BpduGuard);
    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard enable\r\n");
    }

    /* Bridge Priority */

    nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);

    /*Bridge Address */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
    PrintMacAddress (MacAddress, au1BrgAddr);
    /*Max Age */

    nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Hello Time */

    nmhGetFsMIMstCistBridgeHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Dynamic Pathcost calculation */
    nmhGetFsMIMstCistDynamicPathcostCalculation (i4ContextId,
                                                 &i4DynamicPathcostCalc);

    /* Flush Interval */
    nmhGetFsMIMstFlushInterval (i4ContextId, &i4FlushInterval);
    nmhGetFsMIMstCistTotalFlushCount (i4ContextId, &u4FlushCount);
    nmhGetFsMIMstCistFlushIndicationThreshold (i4ContextId, &i4FlushThreshold);

    MstCliPrintStpModuleStatus (CliHandle, u4ContextId, i4ModStatus, i4Version);

    CliPrintf (CliHandle, "Bridge Identifier has Priority %d, ", i4Priority);
    CliPrintf (CliHandle, "Address %s \r\n", au1BrgAddr);
    CliPrintf (CliHandle, "Configured  Max age %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "  Forward delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "Configured Hello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    if (i4DynamicPathcostCalc == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Dynamic Path Cost Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Dynamic Path Cost Disabled\r\n");
    }
    CliPrintf (CliHandle, "Flush Interval %d centi-sec,", i4FlushInterval);
    CliPrintf (CliHandle, " Flush Invocations %d \r\n", u4FlushCount);
    CliPrintf (CliHandle, "Flush Indication threshold %d \r\n",
               i4FlushThreshold);

    /* To Check  if this  Bridge is a ROOT Bridge */
    if (u1Detail)
    {
        /* Check if the Context is valid after printing the above */
        if (AST_CONTEXT_INFO (u4ContextId) == NULL)
        {
            return;
        }

        AST_MEMSET (au1BrgIdBuf, 0, CLI_RSTP_MAX_BRIDGEID_BUFFER);
        nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

        nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);
        pu1Intf = &au1IntfName[0];
        AST_MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        i4Intf = AstCfaCliGetIfName ((UINT4) i4RootPort, (INT1 *) pu1Intf);

        nmhGetFsMIMstCistRootCost (i4ContextId, &i4RootCost);

        /* Topology Changes */

        nmhGetFsMIMstCistTopChanges (i4ContextId, &u4TopChange);

        /* Time Since Topology Changes */

        nmhGetFsMIMstCistTimeSinceTopologyChange (i4ContextId, &u4TopTime);
        /*AST_MGMT_TO_SYS converts centi seconds to seconds */
        u4TopTime = AST_MGMT_TO_SYS (u4TopTime);
        u4TopTime = u4TopTime / AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

        /*Hold Time */
        nmhGetFsMIMstTxHoldCount (i4ContextId, &i4HoldCount);
        /*root  Bridge MaxAge */

        nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

        /* Root Bridge Forward Delay */

        nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

        /* Root Bridge Hello Time */
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            return;
        }

        if (AST_FORCE_VERSION != AST_VERSION_3)
        {
            AstReleaseContext ();
            i4Flag = 1;
            nmhGetFsMIMstCistHelloTime (i4ContextId, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);
        }
        else
        {
            AstReleaseContext ();
            i4Flag = 0;
        }

        AST_MEMSET (&RootAddr, 0, sizeof (tAstMacAddr));
        AST_MEMCPY (RootAddr, (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                    AST_MAC_ADDR_SIZE);

        if (AST_MEMCMP (RootAddr, MacAddress, AST_MAC_ADDR_SIZE) == 0)
        {
            CliPrintf (CliHandle, "We are root of the spanning tree\r\n");

        }

        /*Root Bridge Priority */

        u2Prio = AstGetBrgPrioFromBrgId (CistRoot);

        /* First 2 bytes contain the bridge priority and the
         * next 6 bytes contain the Bridge MAC*/
        PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1RootAddr);

        CliPrintf (CliHandle, "Current Root has priority %d, ", u2Prio);
        CliPrintf (CliHandle, "address  %s\r\n", au1RootAddr);

        /* Get the Interface Name for the Port */
        if (i4Intf == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%PortName for index %d  is not found \r\n",
                       i4Intf);
        }

        CliPrintf (CliHandle, "cost of root path is %d\r\n", i4RootCost);

        CliPrintf (CliHandle, "Number of Topology Changes %d, ", u4TopChange);

        CliPrintf (CliHandle, "Time since topology Change %d seconds ago \r\n",
                   u4TopTime);
        MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        i4RetVal =
            AstCfaCliGetIfName (gu4RecentTopoChPort, (INT1 *) au1IntfName);

        CliPrintf (CliHandle, "Port which caused last topology change : %s\r\n",
                   au1IntfName);

        CliPrintf (CliHandle, "Transmit Hold-Count %d \r\n", i4HoldCount);
        CliPrintf (CliHandle,
                   "Root Times : Max age %d sec %d cs \t Forward delay %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4MaxAge),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge),
                   AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
        if (i4Flag == 1)
        {
            CliPrintf (CliHandle, "Hello Time %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
        }

    }
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliDisplayPortDetails                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Port Details           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4NextPort- Port Index                             */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliDisplayPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4NextPort)
{
    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;
    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;

    UINT4               u4TxBpduCount = 0;
    UINT4               u4RxBpduCount = 0;
    UINT2               u2Prio;
    UINT2               u2RootPrio;
    UINT2               u2PortPrio;
    UINT2               u2Val;
    INT4                i4Version;
    INT4                i4PathCost;
    INT4                i4PortPriority;
    INT4                i4DesigCost;
    INT4                i4LinkType;
    INT4                i4FwdDelay;
    INT4                i4HelloTime;
    INT4                i4RootHelloTime = AST_INIT_VAL;
    INT4                i4MaxAge;
    INT4                i4Intf;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4FwdTransition;
    INT4                i4PortFast;
    INT4                i4BPDUTxState;
    INT4                i4BPDURxState;
    INT4                i4IsL2Gp;
    UINT4               u4HelloTmr = 0;
    UINT4               u4FwdWhileTmr = 0;
    UINT4               u4TCWhileTmr = 0;
    INT4                i4RestricRole = AST_SNMP_FALSE;
    INT4                i4RootGuard = AST_SNMP_FALSE;
    INT4                i4RestricTCN = AST_SNMP_FALSE;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_MSTP_MAX_PORTID_BUFFER];
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortEntry = NULL;
    INT4                i4ContextId = (INT4) u4ContextId;
    UINT2               u2PseudoRootPrio = 0;
    UINT1               au1PseudoRootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    INT4                i4LoopGuard = AST_INIT_VAL;
    INT4                i4AutoEdge = 0;
    INT4                i4OperEdge = 0;
    INT4                i4ErrorRecovery = AST_DEFAULT_ERROR_RECOVERY;
    UINT4               u4ImpStateOccTimeStamp = 0;
    UINT4               u4ImpossibleStateOcc = 0;
    UINT4               u4RcvInfoCount = 0;
    UINT4               u4RcvInfoTimeStamp = 0;
    UINT4               u4NumTCDetectedCount = 0;
    UINT4               u4NumTCRxdCount = 0;
    UINT4               u4NumProposalPktsSent = 0;
    UINT4               u4NumProposalPktsRcvd = 0;
    UINT4               u4NumAgreementPktSent = 0;
    UINT4               u4NumAgreementPktRcvd = 0;
    UINT4               u4TCDetectedTimeStamp = 0;
    UINT4               u4TCReceivedTimeStamp = 0;
    UINT4               u4ProposalPktSentTimeStamp = 0;
    UINT4               u4ProposalPktRcvdTimeStamp = 0;
    UINT4               u4AgreementPktSentTimeStamp = 0;
    UINT4               u4AgreementPktRcvdTimeStamp = 0;
    UINT1               au1Date[AST_ARRAY_TIMESTAMP] = { 0 };

    MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetRootBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1PortIdBuf, 0, CLI_MSTP_MAX_PORTID_BUFFER);
    RetPortId.pu1_OctetList = &au1PortIdBuf[0];
    RetPortId.i4_Length = CLI_MSTP_MAX_PORTID_BUFFER;

    MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    /* Displaying Port info */

    /* Get the interface name for this Port Number */

    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4Intf = AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

    if (i4Intf == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\rPort Name for Index %d is not  found \r\n",
                   i4NextPort);

        return;

    }

    /* forward delay */
    nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* max age */
    nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /* Port Roles */

    nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);

    /* PORT STATES */

    nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);

    AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT, &i4PortStateStatus);

    /* Immediate Superior's Hello Time
     * This impliese hello time should be displayed only for Root,
     * Alternate and Backup ports, so that the user can know about
     * the received info while timer
     * */
    if ((i4PortRole == AST_PORT_ROLE_ROOT) ||
        (i4PortRole == AST_PORT_ROLE_ALTERNATE) ||
        (i4PortRole == AST_PORT_ROLE_BACKUP))
    {
        AstCliGetPortInfoHelloTime (i4NextPort, &i4RootHelloTime);
    }

    /*OperationalVersion of Protocol on Port */

    nmhGetFsMIMstForceProtocolVersion (i4ContextId, &i4Version);

    /*PortPathCost */

    nmhGetFsMIMstCistPortPathCost (i4NextPort, &i4PathCost);

    /* Port Priority */
    nmhGetFsMIMstCistPortPriority (i4NextPort, &i4PortPriority);

    /* Port Identifier-Port Prio.Nbr */
    nmhGetFsMIMstCistPortPriority (i4NextPort, &i4PortPriority);

    /* Port HelloTime */
    nmhGetFsMIMstCistPortHelloTime (i4NextPort, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Designated Root */

    nmhGetFsMIMstCistPortDesignatedRoot (i4NextPort, &RetRootBridgeId);

    /*Designated Bridge  */

    nmhGetFsMIMstCistPortDesignatedBridge (i4NextPort, &RetBridgeId);

    /*Designated Port */

    /* Port ID */
    MEMSET (au1PortIdBuf, 0, CLI_MSTP_MAX_PORTID_BUFFER);
    nmhGetFsMIMstCistPortDesignatedPort (i4NextPort, &RetPortId);

    /*Designated Port PathCost */

    nmhGetFsMIMstCistPortDesignatedCost (i4NextPort, &i4DesigCost);

    /* Forward Transitions */
    nmhGetFsMIMstCistPortForwardTransitions (i4NextPort,
                                             (UINT4 *) &i4FwdTransition);

    /* Auto-Edge */

    nmhGetFsMIMstCistPortAutoEdgeStatus (i4NextPort, &i4AutoEdge);

    /* PortFast Feature */

    nmhGetFsMIMstCistPortAdminEdgeStatus (i4NextPort, &i4PortFast);

    /* Oper-Edge */

    nmhGetFsMIMstCistPortOperEdgeStatus (i4NextPort, &i4OperEdge);

    /* Link Type */
    nmhGetFsMIMstCistPortOperP2P (i4NextPort, &i4LinkType);

    /*RestrictedRole */
    nmhGetFsMIMstCistPortRestrictedRole (i4NextPort, &i4RestricRole);

    /*RestrictedTCN */
    nmhGetFsMIMstCistPortRestrictedTCN (i4NextPort, &i4RestricTCN);

    /*port bpdu transmit status */
    nmhGetFsMIMstCistPortEnableBPDUTx (i4NextPort, &i4BPDUTxState);

    /*port bpdu receive status */
    nmhGetFsMIMstCistPortEnableBPDURx (i4NextPort, &i4BPDURxState);

    /*port bpdu l2gp status */
    nmhGetFsMIMstCistPortIsL2Gp (i4NextPort, &i4IsL2Gp);

    /*CIST PseudoRootId */
    nmhGetFsMIMstCistPortPseudoRootId (i4NextPort, &PseudoRootId);

    /* Root Guard */
    nmhGetFsMIMstCistPortRootGuard (i4NextPort, &i4RootGuard);

    /* Loop Guard */
    nmhGetFsMIMstCistPortLoopGuard (i4NextPort, &i4LoopGuard);

    PrintMacAddress (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1PseudoRootAddr);

    u2PseudoRootPrio = AstGetBrgPrioFromBrgId (PseudoRootId);

    /*Error Disable Recovery Timer */
    nmhGetFsMIMstCistPortErrorRecovery (i4NextPort, &i4ErrorRecovery);

    u4TxBpduCount = 0;
    u4RxBpduCount = 0;

    /* Get the Transmitted and Received BPUDs count on the port */
    MstCliPortTxRxBpduCount (i4NextPort, &u4TxBpduCount, &u4RxBpduCount);

    /*Topology change detected */

    nmhGetFsMIMstCistPortTCDetectedCount (i4NextPort, &u4NumTCDetectedCount);

    /*Topology changes received */

    nmhGetFsMIMstCistPortTCReceivedCount (i4NextPort, &u4NumTCRxdCount);

    /*Topology change detected time stamp */

    nmhGetFsMIMstCistPortTCDetectedTimeStamp (i4NextPort,
                                              &u4TCDetectedTimeStamp);

    /*Topology changes received time stamp */

    nmhGetFsMIMstCistPortTCReceivedTimeStamp (i4NextPort,
                                              &u4TCReceivedTimeStamp);

    /*Proposal packets sent */

    nmhGetFsMIMstCistPortProposalPktsSent (i4NextPort, &u4NumProposalPktsSent);

    /*Proposal packets received */

    nmhGetFsMIMstCistPortProposalPktsRcvd (i4NextPort, &u4NumProposalPktsRcvd);

    /*Proposal packets sent time stamp */

    nmhGetFsMIMstCistPortProposalPktSentTimeStamp (i4NextPort,
                                                   &u4ProposalPktSentTimeStamp);

    /*Proposal packets received time stamp */

    nmhGetFsMIMstCistPortProposalPktRcvdTimeStamp (i4NextPort,
                                                   &u4ProposalPktRcvdTimeStamp);

    /*Agreement packtes sent */

    nmhGetFsMIMstCistPortAgreementPktSent (i4NextPort, &u4NumAgreementPktSent);

    /*Agreement packets received */

    nmhGetFsMIMstCistPortAgreementPktRcvd (i4NextPort, &u4NumAgreementPktRcvd);

    /*Agreement packtes sent time stamp */

    nmhGetFsMIMstCistPortAgreementPktSentTimeStamp (i4NextPort,
                                                    &u4AgreementPktSentTimeStamp);

    /*Agreement packets received time stamp */

    nmhGetFsMIMstCistPortAgreementPktRcvdTimeStamp (i4NextPort,
                                                    &u4AgreementPktRcvdTimeStamp);

    /*Impossible State count */

    nmhGetFsMIMstCistPortImpStateOccurCount (i4NextPort, &u4ImpossibleStateOcc);

    /*Impossible State Timestamp */

    nmhGetFsMIMstCistPortImpStateOccurTimeStamp (i4NextPort,
                                                 &u4ImpStateOccTimeStamp);

    /*RcvInfoWhile Count */

    nmhGetFsMIMstCistPortRcvInfoWhileExpCount (i4NextPort, &u4RcvInfoCount);

    /* Rcvinfowhile Timestamp */

    nmhGetFsMIMstCistPortRcvInfoWhileExpTimeStamp (i4NextPort,
                                                   &u4RcvInfoTimeStamp);

    if (AstSelectContext (u4ContextId) == RST_SUCCESS)
    {
        if ((pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4NextPort)) == NULL)
        {
            AstReleaseContext ();
            CliPrintf (CliHandle, "\rPort %d deleted \r\n", i4NextPort);
            return;
        }

        if (AstGetRemainingTime ((VOID *) pAstPortEntry, AST_TMR_TYPE_HELLOWHEN,
                                 &u4HelloTmr) == RST_FAILURE)
        {
            u4HelloTmr = 0;
        }

        pAstPerStRstPortEntry =
            AST_GET_PERST_RST_PORT_INFO (AST_IFENTRY_LOCAL_PORT (pAstPortEntry),
                                         MST_CIST_CONTEXT);
        if (pAstPerStRstPortEntry != NULL)
        {

            if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                     AST_TMR_TYPE_FDWHILE,
                                     &u4FwdWhileTmr) == RST_FAILURE)
            {
                u4FwdWhileTmr = 0;
            }
            if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                     AST_TMR_TYPE_TCWHILE,
                                     &u4TCWhileTmr) == RST_FAILURE)
            {
                u4TCWhileTmr = 0;
            }

        }

        AstReleaseContext ();
    }

    CliPrintf (CliHandle, "Port %d [%s] of MST00 is ", i4NextPort, au1IntfName);

    StpCliDisplayMstPortRole (CliHandle, u4ContextId, i4PortRole);
    CliPrintf (CliHandle, ", ");

    StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
    CliPrintf (CliHandle, "\r\n");

    if (i4Version == AST_VERSION_0)
    {
        CliPrintf (CliHandle, "%s is operating in STP Compatible Mode \r\n",
                   au1IntfName);
    }
    else if (i4Version == AST_VERSION_2)
    {
        CliPrintf (CliHandle, "%s  is operating in the RSTP Mode\r\n ",
                   au1IntfName);
    }
    else if (i4Version == AST_VERSION_3)
    {
        CliPrintf (CliHandle, "%s is operating in the MSTP Mode \r\n",
                   au1IntfName);
    }

    CliPrintf (CliHandle, "Port path cost  %d, ", i4PathCost);
    CliPrintf (CliHandle, "Port priority  %d, \r\n", i4PortPriority);
    CliPrintf (CliHandle, "Port Identifier  %d.%d. ", i4PortPriority,
               i4NextPort);
    CliPrintf (CliHandle, "Port HelloTime  %d sec %d cs,\r\n",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
    CliPrintf (CliHandle,
               "Timers: Hello - %d, Forward Delay - %d, Topology Change - %d\r\n",
               u4HelloTmr, u4FwdWhileTmr, u4TCWhileTmr);
    CliPrintf (CliHandle, "Error Disable Recovery Interval %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4ErrorRecovery),
               AST_PROT_TO_BPDU_CENTI_SEC (i4ErrorRecovery));

    u2RootPrio = 0;
    u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);
    CliPrintf (CliHandle, "Designated root has priority %d,", u2RootPrio);

    AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
    PrintMacAddress (RetRootBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1RootBrgAddr);
    CliPrintf (CliHandle, " address %s \r\n", au1RootBrgAddr);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1BrgAddr);
    u2Prio = 0;
    u2Prio = AstGetBrgPrioFromBrgId (RetBridgeId);
    CliPrintf (CliHandle, "Designated Bridge has priority %d,", u2Prio);
    CliPrintf (CliHandle, " address %s \r\n", au1BrgAddr);

    u2PortPrio = 0;
    u2Val = 0;
    u2PortPrio = (UINT2) (RetPortId.pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
    u2Val = AstCliGetPortIdFromOctetList (RetPortId);
    u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);

    CliPrintf (CliHandle, "Designated Port Id is %d.%d, ", u2PortPrio, u2Val);
    CliPrintf (CliHandle, "Designated pathcost is %d\r\n", i4DesigCost);

    MstDisplayOperBridgeValues (CliHandle, i4MaxAge, i4FwdDelay);

    if (i4RootHelloTime != AST_INIT_VAL)
    {
        /* HelloTime info have been filled up
         * */
        CliPrintf (CliHandle, "Received Hello Time %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4RootHelloTime),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4RootHelloTime));
    }

    CliPrintf (CliHandle, "Number of Transitions to forwarding State : %d\r\n",
               i4FwdTransition);

    if (i4AutoEdge == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Auto-Edge is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Auto-Edge is disabled\r\n");
    }

    if (i4PortFast == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "PortFast is enabled, ");
    }
    else
    {
        CliPrintf (CliHandle, "PortFast is disabled, ");
    }

    if (i4OperEdge == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Oper-Edge is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Oper-Edge is disabled\r\n");
    }
    if (i4LinkType == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Link type is point to Point\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Link Type is Shared\r\n");
    }

    /*BPDUS TRANSMITTED */
    CliPrintf (CliHandle, "BPDUs : sent %d,", u4TxBpduCount);

    /* BPDUs RECIEVED */
    CliPrintf (CliHandle, " received %d\r\n", u4RxBpduCount);

    if (i4RestricRole == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Restricted Role is enabled.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Restricted Role is disabled.\r\n");
    }

    if (i4RestricTCN == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Restricted TCN is enabled.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Restricted TCN is disabled.\r\n");
    }

    if (i4IsL2Gp == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s is operating as Layer Two Gateway Port \r\n",
                   au1IntfName);

        CliPrintf (CliHandle,
                   "MST00 PseudoRootId Priority %d, MacAddress %s\r\n",
                   u2PseudoRootPrio, au1PseudoRootAddr);
    }

    CliPrintf (CliHandle, "%s", "bpdu-transmit ");

    if (i4BPDUTxState == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "disabled");
    }

    CliPrintf (CliHandle, "%s", "bpdu-receive ");

    if (i4BPDURxState == RST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "disabled");
    }
    if (i4RootGuard == MST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "Root Guard is enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "Root Guard is disabled");
    }
    if (i4LoopGuard == MST_TRUE)
    {
        CliPrintf (CliHandle, "%s\r\n", "Loop Guard is enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%s\r\n", "Loop Guard is disabled");
    }
    CliPrintf (CliHandle, "Port Specific\r\n");
    CliPrintf (CliHandle, "-------------\r\n");
    CliPrintf (CliHandle, "RcvInfoWhile expiry count        : %d  \r\n",
               u4RcvInfoCount);
    if (u4RcvInfoTimeStamp == 0)

    {
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4RcvInfoTimeStamp, au1Date);
        CliPrintf (CliHandle, "RcvInfoWhile expiry Timestamp    : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Impossible state occurence count : %d \r\n",
               u4ImpossibleStateOcc);
    if (u4ImpStateOccTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Impossible state Timestamp       : -\r\n");

    }
    else
    {
        MstUtilTicksToDate (u4ImpStateOccTimeStamp, au1Date);
        CliPrintf (CliHandle, "Impossible State Timestamp       : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Instance Specific\r\n");
    CliPrintf (CliHandle, "-------------\r\n");
    CliPrintf (CliHandle, "TC detected count                : %d \r\n",
               u4NumTCDetectedCount);
    if (u4TCDetectedTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC detected Timestamp            : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4TCDetectedTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC detected Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "TC received count                : %d \r\n",
               u4NumTCRxdCount);
    if (u4TCReceivedTimeStamp == 0)
    {
        CliPrintf (CliHandle, "TC received Timestamp            : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4TCReceivedTimeStamp, au1Date);
        CliPrintf (CliHandle, "TC received Timestamp            : %s \r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Proposal BPDUs\r\n");
    CliPrintf (CliHandle, "---------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4NumProposalPktsSent);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4NumProposalPktsRcvd);
    if (u4ProposalPktSentTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4ProposalPktSentTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4ProposalPktRcvdTimeStamp == 0)
    {

        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4ProposalPktRcvdTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }
    CliPrintf (CliHandle, "Agreement BPDUs\r\n");
    CliPrintf (CliHandle, "----------------\r\n");
    CliPrintf (CliHandle, "Tx count                         : %d \r\n",
               u4NumAgreementPktSent);
    CliPrintf (CliHandle, "Rx count                         : %d \r\n",
               u4NumAgreementPktRcvd);
    if (u4AgreementPktSentTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Tx Timestamp                     : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4AgreementPktSentTimeStamp, au1Date);
        CliPrintf (CliHandle, "Tx Timestamp                     : %s\r\n",
                   au1Date);
    }

    if (u4AgreementPktRcvdTimeStamp == 0)
    {
        CliPrintf (CliHandle, "Rx Timestamp                     : -\r\n");
    }
    else
    {
        MstUtilTicksToDate (u4AgreementPktRcvdTimeStamp, au1Date);
        CliPrintf (CliHandle, "Rx Timestamp                     : %s\r\n",
                   au1Date);
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliDisplayMstiPortDetails                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Port Details           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                          i4NextPort- Port Index                           */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliDisplayMstiPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                              INT4 i4NextInst, INT4 i4NextPort)
{

    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;
    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;

    UINT4               u4TxBpduCount = 0;
    UINT4               u4RxBpduCount = 0;

    UINT2               u2PortPrio;
    UINT2               u2BrgPrio;
    UINT2               u2Val;
    UINT2               u2RootPrio;
    INT4                i4PortCost;
    INT4                i4PortPriority;
    INT4                i4Intf;
    INT4                i4DesigCost;
    INT4                i4FwdTransition;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4FwdDelay;
    INT4                i4HelloTime;
    INT4                i4MaxAge;
    INT4                i4IsL2Gp;

    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];

    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_MSTP_MAX_PORTID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];

    INT4                i4TempInst = i4NextInst;
    INT4                i4PortOutCome;
    INT4                i4CurrentPort;
    UINT1               u1Flag = 0;
    UINT4               u4HelloTmr = 0;
    UINT4               u4FwdWhileTmr = 0;
    UINT4               u4TCWhileTmr = 0;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStRstPortInfo *pAstPerStRstPortEntry = NULL;
    INT4                i4ContextId = (INT4) u4ContextId;
    UINT2               u2Prio;
    UINT1               au1PseudoRootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    UINT4               u4NumTCDetectedCount = 0;
    UINT4               u4TCDetectedTimeStamp = 0;
    UINT4               u4TCReceivedTimeStamp = 0;
    UINT4               u4NumProposalPktsSent = 0;
    UINT4               u4NumProposalPktsRcvd = 0;
    UINT4               u4NumAgreementPktSent = 0;
    UINT4               u4NumAgreementPktRcvd = 0;
    UINT4               u4ProposalPktSentTimeStamp = 0;
    UINT4               u4ProposalPktRcvdTimeStamp = 0;
    UINT4               u4AgreementPktSentTimeStamp = 0;
    UINT4               u4AgreementPktRcvdTimeStamp = 0;
    UINT1               au1Date[AST_ARRAY_TIMESTAMP] = { 0 };
    UINT4               u4NumTCRxdCount = 0;

    tSNMP_OCTET_STRING_TYPE PseudoRootId;

    MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    if (i4NextPort == 0)
    {
        i4PortOutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, 0, &i4NextPort,
                                          0, &i4NextInst);
    }
    else
    {
        u1Flag = 1;
        i4PortOutCome = RST_SUCCESS;
    }

    /* forward delay */
    nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* max age */
    nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    while (i4PortOutCome != RST_FAILURE)
    {
        if (i4NextInst == AST_TE_MSTID)
        {
            break;
        }

        if (i4NextInst == i4TempInst)
        {

            MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
            RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
            RetBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

            MEMSET (au1PortIdBuf, 0, CLI_MSTP_MAX_PORTID_BUFFER);
            RetPortId.pu1_OctetList = &au1PortIdBuf[0];
            RetPortId.i4_Length = CLI_MSTP_MAX_PORTID_BUFFER;

            MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
            RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
            RetRootBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

            /* Displaying Port info */

            /* Get the interface name for this Port Number */

            MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            i4Intf = AstCfaCliGetIfName ((UINT4) i4NextPort,
                                         (INT1 *) au1IntfName);

            if (i4Intf == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\rPort Name for Index %d is not  found \r\n",
                           i4NextPort);
                return;

            }
            /* Port Roles */

            nmhGetFsMIMstMstiCurrentPortRole (i4NextPort, i4NextInst,
                                              &i4PortRole);

            /* PORT STATES */

            nmhGetFsMIMstMstiPortState (i4NextPort, i4NextInst, &i4PortState);
            AstGetHwPortStateStatus (i4NextPort, i4NextInst,
                                     &i4PortStateStatus);

            /*PortPathCost */

            nmhGetFsMIMstMstiPortPathCost (i4NextPort, i4NextInst, &i4PortCost);

            /* Port Priority */
            nmhGetFsMIMstMstiPortPriority (i4NextPort, i4NextInst,
                                           &i4PortPriority);

            /* Port HelloTime */
            nmhGetFsMIMstCistPortHelloTime (i4NextPort, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

            /*Designated Root */

            nmhGetFsMIMstMstiPortDesignatedRoot (i4NextPort, i4NextInst,
                                                 &RetRootBridgeId);

            /*Designated Bridge  */

            nmhGetFsMIMstMstiPortDesignatedBridge (i4NextPort, i4NextInst,
                                                   &RetBridgeId);

            /*Designated Port */

            /* Port ID */
            nmhGetFsMIMstMstiPortDesignatedPort (i4NextPort, i4NextInst,
                                                 &RetPortId);

            /*Designated Port PathCost */

            nmhGetFsMIMstMstiPortDesignatedCost (i4NextPort, i4NextInst,
                                                 &i4DesigCost);
            /* Forward Transitions */

            nmhGetFsMIMstMstiPortForwardTransitions (i4NextPort, i4NextInst,
                                                     (UINT4 *)
                                                     &i4FwdTransition);

            MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
            nmhGetFsMIMstMstiPortPseudoRootId (i4NextPort,
                                               i4NextInst, &PseudoRootId);

            PrintMacAddress (PseudoRootId.pu1_OctetList +
                             AST_BRG_PRIORITY_SIZE, au1PseudoRootAddr);

            u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

            nmhGetFsMIMstCistPortIsL2Gp (i4NextPort, &i4IsL2Gp);

            if (AstSelectContext (u4ContextId) != RST_FAILURE)
            {
                u4TxBpduCount = 0;
                u4RxBpduCount = 0;

                if ((pAstPortEntry =
                     AstGetIfIndexEntry ((UINT4) i4NextPort)) == NULL)
                {
                    AstReleaseContext ();
                    CliPrintf (CliHandle, "\rPort %d deleted \r\n", i4NextPort);
                    return;
                }

                if (AstGetRemainingTime ((VOID *) pAstPortEntry,
                                         AST_TMR_TYPE_HELLOWHEN,
                                         &u4HelloTmr) == RST_FAILURE)
                {
                    u4HelloTmr = 0;
                }

                pAstPerStRstPortEntry =
                    AST_GET_PERST_RST_PORT_INFO (AST_IFENTRY_LOCAL_PORT
                                                 (pAstPortEntry), i4NextInst);

                if (pAstPerStRstPortEntry != NULL)
                {
                    if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                             AST_TMR_TYPE_FDWHILE,
                                             &u4FwdWhileTmr) == RST_FAILURE)
                    {
                        u4FwdWhileTmr = 0;
                    }
                    if (AstGetRemainingTime ((VOID *) pAstPerStRstPortEntry,
                                             AST_TMR_TYPE_TCWHILE,
                                             &u4TCWhileTmr) == RST_FAILURE)
                    {
                        u4TCWhileTmr = 0;
                    }
                }

                AstReleaseContext ();
            }

            nmhGetFsMIMstMstiPortTransmittedBPDUs (i4NextPort, i4NextInst,
                                                   &u4TxBpduCount);

            nmhGetFsMIMstMstiPortReceivedBPDUs (i4NextPort, i4NextInst,
                                                &u4RxBpduCount);
            /*Topology changes received */

            nmhGetFsMIMstMstiPortTCReceivedCount (i4NextPort, i4NextInst,
                                                  &u4NumTCRxdCount);

            /*Topology changes received time stamp */

            nmhGetFsMIMstMstiPortTCReceivedTimeStamp (i4NextPort, i4NextInst,
                                                      &u4TCReceivedTimeStamp);

            /*Topology change detected */

            nmhGetFsMIMstMstiPortTCDetectedCount (i4NextPort, i4NextInst,
                                                  &u4NumTCDetectedCount);

            /*Topology change detected time stamp */

            nmhGetFsMIMstMstiPortTCDetectedTimeStamp (i4NextPort, i4NextInst,
                                                      &u4TCDetectedTimeStamp);

            /*Proposal packets sent */

            nmhGetFsMIMstMstiPortProposalPktsSent (i4NextPort, i4NextInst,
                                                   &u4NumProposalPktsSent);

            /*Proposal packets received */

            nmhGetFsMIMstMstiPortProposalPktsRcvd (i4NextPort, i4NextInst,
                                                   &u4NumProposalPktsRcvd);

            /*Proposal packets sent time stamp */

            nmhGetFsMIMstMstiPortProposalPktSentTimeStamp (i4NextPort,
                                                           i4NextInst,
                                                           &u4ProposalPktSentTimeStamp);

            /*Proposal packets received time stamp */

            nmhGetFsMIMstMstiPortProposalPktRcvdTimeStamp (i4NextPort,
                                                           i4NextInst,
                                                           &u4ProposalPktRcvdTimeStamp);

            /*Agreement packtes sent */

            nmhGetFsMIMstMstiPortAgreementPktSent (i4NextPort, i4NextInst,
                                                   &u4NumAgreementPktSent);

            /*Agreement packets received */

            nmhGetFsMIMstMstiPortAgreementPktRcvd (i4NextPort, i4NextInst,
                                                   &u4NumAgreementPktRcvd);

            /*Agreement packtes sent time stamp */

            nmhGetFsMIMstMstiPortAgreementPktSentTimeStamp (i4NextPort,
                                                            i4NextInst,
                                                            &u4AgreementPktSentTimeStamp);

            /*Agreement packets received time stamp */

            nmhGetFsMIMstMstiPortAgreementPktRcvdTimeStamp (i4NextPort,
                                                            i4NextInst,
                                                            &u4AgreementPktRcvdTimeStamp);

            CliPrintf (CliHandle, "\r\nPort %d [%s]of MST%02ld is ",
                       i4NextPort, au1IntfName, i4NextInst);

            StpCliDisplayMstPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, ", ");

            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "Port cost  %d, ", i4PortCost);
            CliPrintf (CliHandle, "Port priority  %d,\r\n", i4PortPriority);

            CliPrintf (CliHandle, "Port Identifier  %d.%d. ", i4PortPriority,
                       i4NextPort);

            CliPrintf (CliHandle, "Port HelloTime  %d sec %d cs,\r\n",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            CliPrintf (CliHandle,
                       "Timers: Hello - %d,Forward Delay - %d, Topology Change - %d\r\n",
                       u4HelloTmr, u4FwdWhileTmr, u4TCWhileTmr);

            /*Print The Root Bridge MacAddress & Priority */
            u2RootPrio = 0;
            u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);
            AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
            PrintMacAddress (RetRootBridgeId.pu1_OctetList +
                             AST_BRG_PRIORITY_SIZE, au1RootBrgAddr);
            CliPrintf (CliHandle, "Designated Root has priority %d,",
                       u2RootPrio);
            CliPrintf (CliHandle, " address %s \r\n", au1RootBrgAddr);

            /*Print The  Designated Bridge MacAddress & Priority */
            u2BrgPrio = 0;
            u2BrgPrio = AstGetBrgPrioFromBrgId (RetBridgeId);
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (RetBridgeId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                             au1BrgAddr);
            CliPrintf (CliHandle, "Designated Bridge has priority %d,",
                       u2BrgPrio);
            CliPrintf (CliHandle, " address %s \r\n", au1BrgAddr);

            u2Val = 0;
            u2PortPrio = 0;
            u2PortPrio =
                (UINT2) (RetPortId.pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
            u2Val = AstCliGetPortIdFromOctetList (RetPortId);
            u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);

            CliPrintf (CliHandle, "Designated Port Id is %d.%d, ", u2PortPrio,
                       u2Val);

            CliPrintf (CliHandle, "Designated pathcost is %d\r\n", i4DesigCost);

            MstDisplayOperBridgeValues (CliHandle, i4MaxAge, i4FwdDelay);

            CliPrintf (CliHandle,
                       "Number of Transitions to forwarding State : %d\r\n",
                       i4FwdTransition);

            /*BPDUS TRANSMITTED */
            CliPrintf (CliHandle, "BPDUs : sent %d,", u4TxBpduCount);
            /* BPDUs RECIEVED */
            CliPrintf (CliHandle, " received %d\r\n", u4RxBpduCount);

            if (i4IsL2Gp == RST_TRUE)
            {
                CliPrintf (CliHandle,
                           "MST%02ld PseudoRootId Priority %d, MacAddress %s\r\n",
                           i4NextInst, u2Prio, au1PseudoRootAddr);
            }

            CliPrintf (CliHandle, "TC detected count                : %d \r\n",
                       u4NumTCDetectedCount);
            if (u4TCDetectedTimeStamp == 0)
            {
                CliPrintf (CliHandle,
                           "TC detected Timestamp            : -\r\n");
            }
            else
            {
                MstUtilTicksToDate (u4TCDetectedTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "TC detected Timestamp            : %s \r\n",
                           au1Date);
            }
            CliPrintf (CliHandle, "TC received count                : %d \r\n",
                       u4NumTCRxdCount);
            if (u4TCReceivedTimeStamp == 0)
            {
                CliPrintf (CliHandle,
                           "TC received Timestamp            : -\r\n");
            }
            else
            {
                MstUtilTicksToDate (u4TCReceivedTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "TC received Timestamp            : %s \r\n",
                           au1Date);
            }
            CliPrintf (CliHandle, "Proposal BPDUs\r\n");
            CliPrintf (CliHandle, "---------------\r\n");
            CliPrintf (CliHandle, "Tx count                         : %d \r\n",
                       u4NumProposalPktsSent);
            CliPrintf (CliHandle, "Rx count                         : %d \r\n",
                       u4NumProposalPktsRcvd);
            if (u4ProposalPktSentTimeStamp == 0)
            {
                CliPrintf (CliHandle,
                           "Tx Timestamp                     : -\r\n");
            }
            else
            {
                MstUtilTicksToDate (u4ProposalPktSentTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "Tx Timestamp                     : %s\r\n",
                           au1Date);
            }

            if (u4ProposalPktRcvdTimeStamp == 0)
            {

                CliPrintf (CliHandle,
                           "Rx Timestamp                     : -\r\n");
            }
            else
            {

                MstUtilTicksToDate (u4ProposalPktRcvdTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "Rx Timestamp                     : %s\r\n",
                           au1Date);
            }
            CliPrintf (CliHandle, "Agreement BPDUs\r\n");
            CliPrintf (CliHandle, "----------------\r\n");
            CliPrintf (CliHandle, "Tx count                         : %d \r\n",
                       u4NumAgreementPktSent);
            CliPrintf (CliHandle, "Rx count                         : %d \r\n",
                       u4NumAgreementPktRcvd);
            if (u4AgreementPktSentTimeStamp == 0)
            {
                CliPrintf (CliHandle,
                           "Tx Timestamp                     : -\r\n");
            }
            else
            {
                MstUtilTicksToDate (u4AgreementPktSentTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "Tx Timestamp                     : %s\r\n",
                           au1Date);
            }

            if (u4AgreementPktRcvdTimeStamp == 0)
            {
                CliPrintf (CliHandle,
                           "Rx Timestamp                     : -\r\n");
            }
            else
            {
                MstUtilTicksToDate (u4AgreementPktRcvdTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "Rx Timestamp                     : %s\r\n",
                           au1Date);
            }
        }
        i4CurrentPort = i4NextPort;
        if (u1Flag == 1)
        {
            break;
        }
        i4PortOutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, i4CurrentPort,
                                          &i4NextPort, i4NextInst, &i4TempInst);
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliPortTxRxBpduCount                            */
/*                                                                           */
/*     DESCRIPTION      : This function gets the number of received and      */
/*                        transmitted BPDU s count                           */
/*                                                                           */
/*     INPUT            : i4Port - PortIndex                                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : pu4TxBpduCount - Transmitted BPDUs Count           */
/*                      : pu4RxBpduCount - Received BPUDs Count              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliPortTxRxBpduCount (INT4 i4Port, UINT4 *pu4TxBpduCount,
                         UINT4 *pu4RxBpduCount)
{
    /* Transmitted BPDUs Count in CIST Context on a port */
    *pu4TxBpduCount = MstCistPortTxBpduCount (i4Port);

    /* Received BPUDs count in CIST context on a port */
    *pu4RxBpduCount = MstCistPortRxBpduCount (i4Port);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowSpanningTreeRoot                         */
/*                                                                           */
/*     DESCRIPTION      : This function  dispalys the root bridge parameters */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Val-Info to be displayed                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliShowSpanningTreeRoot (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Val)
{

    INT4                i4RootCost = 0;
    INT4                i4RootPort = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4MaxAge = 0;
    UINT2               u2Prio = 0;
    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];

    tSNMP_OCTET_STRING_TYPE CistRoot;
    UINT1               u1Flag = 0;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4ContextId = (INT4) u4ContextId;

    AST_MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    CistRoot.pu1_OctetList = &au1BrgIdBuf[0];
    CistRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    switch (u4Val)
    {
        case STP_ADDRESS:

            nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

            break;
            /* CIST Root COST */
        case STP_ROOT_COST:

            nmhGetFsMIMstCistRegionalRootCost (i4ContextId, &i4RootCost);

            break;
        case STP_SHOW_DETAIL:

            /*Root Bridge Priority */
            nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

            /* First 2 bytes of Root Id contain the priority information,
             * in which the second byte is always 00. Thus multiplying 
             * the first byte value by (maximum 1 byte hex value + 1)
             * gives the the priority value.
             */

            /*Cost */
            nmhGetFsMIMstCistRootCost (i4ContextId, &i4RootCost);

            /*Root Port */
            nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);
            MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);

            /*Forward Delay */
            nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            /*MaxAge */

            nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            break;

        case STP_FORWARD_TIME:

            nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            break;
        case STP_ID:

            /*Root Bridge ID */
            nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

            break;
        case STP_MAX_AGE:

            nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            break;

        case STP_ROOT_PORT:
            MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);
            AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);
            break;

        case STP_PRIORITY:

            nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);
            break;
        default:

            /* Displays all Root Bridge info in the Table Format */
            MstCliShowRootBridgeTable (CliHandle, u4ContextId);
            u1Flag = 1;
            break;
    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been grouped under the below
     *  switch-case */

    switch (u4Val)
    {

        case STP_ADDRESS:

            /* First 2 bytes contain the bridge priority and the
             * next 6 bytes contain the Bridge MAC*/
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                             au1BrgAddr);
            CliPrintf (CliHandle, "\r\nMST00    %s\r\n", au1BrgAddr);

            break;

        case STP_ROOT_COST:

            CliPrintf (CliHandle, "\r\nMST00     %-10d\r\n", i4RootCost);
            break;

        case STP_SHOW_DETAIL:

            CliPrintf (CliHandle, "\r\nMST00 \r\n");

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                             au1BrgAddr);

            u2Prio = AstGetBrgPrioFromBrgId (CistRoot);

            CliPrintf (CliHandle, "\rRoot ID     Priority %d \r\n", u2Prio);

            CliPrintf (CliHandle, "Address     %s\r\n", au1BrgAddr);

            CliPrintf (CliHandle, "            Cost %d\r\n", i4RootCost);
            CliPrintf (CliHandle, "            Port %d [%s]\r\n", i4RootPort,
                       au1IntfName);
            CliPrintf (CliHandle, "            Forward Delay %d sec %d cs, ",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            CliPrintf (CliHandle, "Max Age %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

            break;

        case STP_FORWARD_TIME:

            CliPrintf (CliHandle, "\r\nForward Delay  %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            break;

        case STP_ID:

            AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
            CliOctetToStr (CistRoot.pu1_OctetList, CistRoot.i4_Length,
                           au1BrgId, AST_BRG_ID_DIS_LEN);
            CliPrintf (CliHandle, "\r\nMST00     %s \r\n", au1BrgId);

            break;
        case STP_MAX_AGE:

            CliPrintf (CliHandle, "\r\nMST00     %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            break;
        case STP_ROOT_PORT:
            if (AST_INIT_VAL != i4RootPort)
            {
                CliPrintf (CliHandle, "\r\nMST00 %d[%s]\r\n", i4RootPort,
                           au1IntfName);
            }
            break;

        case STP_PRIORITY:

            u2Prio = AstGetBrgPrioFromBrgId (CistRoot);
            CliPrintf (CliHandle, "\r\nMST00     %5d \r\n", u2Prio);
            break;

        default:

            break;

    }

    CliPrintf (CliHandle, "\r\n");

    if (u1Flag != 1)
    {
        MstCliShowSpanningTreeMstiRoot (CliHandle, u4ContextId, u4Val);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                            */
/*     FUNCTION NAME    : MstCliShowSpanningTreeMstiRoot                      */
/*                                                                            */
/*     DESCRIPTION      : This function  dispalys the root bridge parameters  */
/*                                                                            */
/*     INPUT            : tCliHandle-Handle to the Cli Context                */
/*                        u4Val-Info to be displayed                          */
/*                                                                            */
/*     OUTPUT           : NONE                                                */
/*                                                                            */
/*                                                                            */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             */
/*****************************************************************************/

INT4
MstCliShowSpanningTreeMstiRoot (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4Val)
{
    tAstMacAddr         MacAddress;
    INT4                i4RootCost = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4RootPort = 0;

    UINT2               u2RootPrio = 0;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE CistRoot;
    INT1                i1OutCome;
    INT4                i4NextInst;
    INT4                i4PrevInst;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    AST_MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    CistRoot.pu1_OctetList = &au1BrgIdBuf[0];
    CistRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);
    /* To get the instance */
    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {

        switch (u4Val)
        {
            case STP_ADDRESS:

                nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                                     &CistRoot);

                break;
                /* CIST Root COST */
            case STP_ROOT_COST:

                nmhGetFsMIMstMstiRootCost (i4ContextId, i4NextInst,
                                           &i4RootCost);

                break;
            case STP_SHOW_DETAIL:

                /*Root Bridge Priority */
                nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                                     &CistRoot);

                nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
                /*Root MaxAge */

                nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
                i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

                /*Root ForwardTime */

                nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
                i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

                break;
            case STP_ID:

                /*Root Bridge ID */
                nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                                     &CistRoot);

                break;
            case STP_ROOT_PORT:
                MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                nmhGetFsMIMstMstiRootPort (i4ContextId, i4NextInst,
                                           &i4RootPort);
                AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);
                break;

            case STP_PRIORITY:

                nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                                     &CistRoot);
                break;
        }

        /*  Since the Lock needs to be released before printing all the above
         *  information, all the CliPrintfs have been grouped under the below
         *  switch-case */

        switch (u4Val)
        {

            case STP_ADDRESS:
                AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
                PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                                 au1BrgAddr);

                CliPrintf (CliHandle, "MST%02d     %s\r\n", i4NextInst,
                           au1BrgAddr);
                break;

            case STP_ROOT_COST:
                CliPrintf (CliHandle, "MST%02d     %d\r\n", i4NextInst,
                           i4RootCost);

                break;
            case STP_SHOW_DETAIL:

                CliPrintf (CliHandle, "MST%02d\r\n", i4NextInst);

                u2RootPrio = AstGetBrgPrioFromBrgId (CistRoot);
                CliPrintf (CliHandle,
                           "\rRoot ID     Priority %d \r\n", u2RootPrio);

                AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
                PrintMacAddress ((CistRoot.pu1_OctetList +
                                  AST_BRG_PRIORITY_SIZE), au1BrgAddr);
                CliPrintf (CliHandle, "            Address    %s\r\n ",
                           au1BrgAddr);

                if (AST_MEMCMP (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                                MacAddress, AST_MAC_ADDR_SIZE) == 0)
                {
                    CliPrintf (CliHandle,
                               "           This bridge is the root\r\n");
                }

                CliPrintf (CliHandle, "            Max age %d sec %d cs,",
                           AST_PROT_TO_BPDU_SEC (i4MaxAge),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

                CliPrintf (CliHandle, " Forward delay %d sec %d cs\r\n",
                           AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

                break;
            case STP_ID:

                AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
                CliOctetToStr (CistRoot.pu1_OctetList, CistRoot.i4_Length,
                               au1BrgId, AST_BRG_ID_DIS_LEN);
                CliPrintf (CliHandle, "MST%02d     %s\r\n", i4NextInst,
                           au1BrgId);

                break;

            case STP_ROOT_PORT:
                if (AST_INIT_VAL != i4RootPort)
                {
                    CliPrintf (CliHandle, "MST%02d     %d[%s]\r\n", i4NextInst,
                               i4RootPort, au1IntfName);
                }
                break;

            case STP_PRIORITY:

                u2RootPrio = AstGetBrgPrioFromBrgId (CistRoot);
                CliPrintf (CliHandle, "MST%02d     %5d\r\n", i4NextInst,
                           u2RootPrio);
                break;

        }

        i4PrevInst = i4NextInst;
        i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
        CliFlush (CliHandle);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowSpanningTreeBridge                       */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  Bridge information    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Val- Bridge Info to be displayed                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliShowSpanningTreeBridge (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Val)
{

    tAstMacAddr         MacAddress;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4PrevInst = 0;
    INT4                i4NextInst;
    INT1                i1OutCome;
    INT1                i1InstOutCome;
    INT4                i4Priority = 0;
    INT4                i4CistPriority = 0;
    INT4                i4MaxAge = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4HelloTime;
    INT4                au4TempInst[AST_MAX_MST_INSTANCES];
    INT4                au4FwdDelay[AST_MAX_MST_INSTANCES];
    INT4                au4MaxAge[AST_MAX_MST_INSTANCES];
    INT4                au4Priority[AST_MAX_MST_INSTANCES];

    INT4                i4Count = 0;
    INT4                i4InstCount = 0;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    MEMSET (&au4Priority, 0, sizeof (INT4) * AST_MAX_MST_INSTANCES);
    MEMSET (&au4TempInst, 0, sizeof (INT4) * AST_MAX_MST_INSTANCES);
    MEMSET (&au4FwdDelay, 0, sizeof (INT4) * AST_MAX_MST_INSTANCES);
    MEMSET (&au4MaxAge, 0, sizeof (INT4) * AST_MAX_MST_INSTANCES);

    switch (u4Val)
    {
        case STP_ADDRESS:

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);

            i1InstOutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                                   &i4NextContextId,
                                                                   0,
                                                                   &i4NextInst);

            while ((i1InstOutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {

                i4PrevInst = i4NextInst;

                au4TempInst[i4Count] = i4NextInst;
                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;

            }
            i4InstCount = i4Count - 1;

            break;

        case STP_SHOW_DETAIL:

            nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);

            i4CistPriority = i4Priority;

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);

            nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            i1InstOutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                                   &i4NextContextId,
                                                                   0,
                                                                   &i4NextInst);
            while ((i1InstOutCome != SNMP_FAILURE)
                   && (i4ContextId == i4NextContextId))
            {
                /* Bridge Priority */
                nmhGetFsMIMstMstiBridgePriority (i4ContextId, i4NextInst,
                                                 &i4Priority);
                i4PrevInst = i4NextInst;

                au4Priority[i4Count] = i4Priority;
                au4TempInst[i4Count] = i4NextInst;

                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;
            }

            i4InstCount = i4Count - 1;

            break;
        case STP_FORWARD_TIME:

            nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);
            i1InstOutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);
            while ((i1InstOutCome != SNMP_FAILURE)
                   && (i4ContextId == i4NextContextId))
            {

                i4PrevInst = i4NextInst;
                au4FwdDelay[i4Count] = i4FwdDelay;
                au4TempInst[i4Count] = i4NextInst;
                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;

            }
            i4InstCount = i4Count - 1;

            break;
        case STP_HELLO_TIME:

            if (AstSelectContext (u4ContextId) == RST_FAILURE)
            {
                return CLI_FAILURE;
            }

            nmhGetFsMIMstCistBridgeHelloTime (i4ContextId, &i4HelloTime);
            i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);
            AstReleaseContext ();

            break;
        case STP_ID:

            /* Bridge ID */

            nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);
            i4Priority = i4Priority / 0x100;
            i4CistPriority = i4Priority;
            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);

            i1InstOutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1InstOutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {

                i4PrevInst = i4NextInst;
                au4TempInst[i4Count] = i4NextInst;
                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;
            }

            i4InstCount = i4Count - 1;
            break;
        case STP_MAX_AGE:

            nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
            i1InstOutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1InstOutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {

                i4PrevInst = i4NextInst;
                au4MaxAge[i4Count] = i4MaxAge;
                au4TempInst[i4Count] = i4NextInst;
                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;
            }

            i4InstCount = i4Count - 1;

            break;

        case STP_PROTOCOL:

            i1InstOutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1InstOutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {

                i4PrevInst = i4NextInst;
                au4TempInst[i4Count] = i4NextInst;
                i1InstOutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);

                i4Count++;
            }
            i4InstCount = i4Count - 1;

            break;
        case STP_PRIORITY:

            nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);

            i4CistPriority = i4Priority;
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                nmhGetFsMIMstMstiBridgePriority (i4ContextId, i4NextInst,
                                                 &i4Priority);

                i4PrevInst = i4NextInst;

                au4Priority[i4Count] = i4Priority;
                au4TempInst[i4Count] = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);

                i4Count++;
            }

            i4InstCount = i4Count - 1;

            break;

        default:

            /*Bridge ID */
            nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);
            i4Priority = i4Priority / 0x100;

            i4CistPriority = i4Priority;

            /* Valid values of priorities in hex are 0x1000(4096), 0x2000 (8192)
             * ,..0xF000(61440).
             * This needs to be displayed as 10:00, 20:00..F0:00 respectively.
             * The second byte is always 00
             * The first byte can be computed as shown- 
             * 0x100 = 256
             * 4096/256 = 16 = 0x10 , 8192/256 = 0x20.., 61440/256 =0xFF
             */

            AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
            nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
            /* MaxAge */
            nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
            i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

            /* Forward Delay */
            nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
            i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                nmhGetFsMIMstMstiBridgePriority (i4ContextId, i4NextInst,
                                                 &i4Priority);
                i4Priority = i4Priority / 0x100;

                i4PrevInst = i4NextInst;
                au4TempInst[i4Count] = i4NextInst;
                au4Priority[i4Count] = i4Priority;

                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                i4Count++;

            }

            i4InstCount = i4Count - 1;

            break;

    }

    /*  Since the Lock needs to be released before printing all the above
     *  information, all the CliPrintfs have been gruped under the below
     *  switch-case */

    switch (u4Val)

    {

        case STP_ADDRESS:

            i4Count = 0;
            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (MacAddress, au1BrgAddr);
            CliPrintf (CliHandle, "\r\nMST00     %s\r\n", au1BrgAddr);

            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     %s\r\n",
                           au4TempInst[i4Count], au1BrgAddr);
                i4Count++;
            }

            break;

        case STP_HELLO_TIME:

            CliPrintf (CliHandle, "\r\nHello Time  %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
            break;

        case STP_FORWARD_TIME:

            i4Count = 0;
            CliPrintf (CliHandle, "\r\nMST00     %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     %d sec %d cs\r\n",
                           au4TempInst[i4Count],
                           AST_PROT_TO_BPDU_SEC (au4FwdDelay[i4Count]),
                           AST_PROT_TO_BPDU_CENTI_SEC (au4FwdDelay[i4Count]));

                i4Count++;
            }
            break;

        case STP_MAX_AGE:

            i4Count = 0;
            CliPrintf (CliHandle, "\r\nMST00     %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     %d sec %d cs\r\n",
                           au4TempInst[i4Count],
                           AST_PROT_TO_BPDU_SEC (au4MaxAge[i4Count]),
                           AST_PROT_TO_BPDU_CENTI_SEC (au4MaxAge[i4Count]));

                i4Count++;
            }

            break;
        case STP_PRIORITY:

            i4Count = 0;
            CliPrintf (CliHandle, "\r\nMST00     %5d\r\n", i4CistPriority);
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     %d\r\n",
                           au4TempInst[i4Count], au4Priority[i4Count]);

                i4Count++;
            }
            break;

        case STP_PROTOCOL:

            i4Count = 0;
            CliPrintf (CliHandle, "\r\nMST00     mstp\r\n");
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     mstp\r\n",
                           au4TempInst[i4Count]);

                i4Count++;
            }

            break;

        case STP_ID:

            i4Count = 0;

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (MacAddress, au1BrgAddr);
            CliPrintf (CliHandle, "\r\nMST00     %x:00:%s\r\n",
                       i4CistPriority, au1BrgAddr);

            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\rMST%02d     %x:00:%s\r\n",
                           au4TempInst[i4Count], i4Priority, au1BrgAddr);

                i4Count++;
            }

            break;

        case STP_SHOW_DETAIL:

            CliPrintf (CliHandle, "\r\nMST00\r\n");

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (MacAddress, au1BrgAddr);
            CliPrintf (CliHandle, "Bridge Id     Priority %d\r\n",
                       i4CistPriority);
            CliPrintf (CliHandle, "              Address  %s\r\n", au1BrgAddr);
            CliPrintf (CliHandle, "              Max age is %d sec %d cs,",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            CliPrintf (CliHandle, " forward delay is %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

            /*Display Bridge Details */

            i4Count = 0;
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "\r\nMST%02d\r\n", au4TempInst[i4Count]);
                CliPrintf (CliHandle, "Bridge Id     Priority %d\r\n",
                           au4Priority[i4Count]);

                CliPrintf (CliHandle, "              Address  %s\r\n",
                           au1BrgAddr);
                CliPrintf (CliHandle, "              Max age is %d sec %d cs,",
                           AST_PROT_TO_BPDU_SEC (i4MaxAge),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
                CliPrintf (CliHandle, " forward delay is %d sec %d cs\r\n",
                           AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

                i4Count++;
            }

            break;

        default:

            CliPrintf (CliHandle,
                       "\r\n%-13s%-25s%-13s%-15s%-8s\r\n",
                       "MST Instance", " Bridge ID", "  MaxAge", " FwdDly",
                       "Protocol");
            CliPrintf (CliHandle,
                       "%-13s%-25s%-13s%-15s%-8s\r\n",
                       "---------", " ---------", "   ------", "   ------",
                       "--------");

            CliPrintf (CliHandle, "MST00      ");
            CliPrintf (CliHandle, "  %-2x:00:", i4CistPriority);

            AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
            PrintMacAddress (MacAddress, au1BrgAddr);
            CliPrintf (CliHandle, "%s", au1BrgAddr);
            CliPrintf (CliHandle, "%d sec %d cs  ",
                       AST_PROT_TO_BPDU_SEC (i4MaxAge),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
            CliPrintf (CliHandle, "%-2d s %d cs ",
                       AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
            /* Protocol Display */
            CliPrintf (CliHandle, " %s\r\n", "  mstp");

            i4Count = 0;
            while (i4Count <= i4InstCount)
            {

                CliPrintf (CliHandle, "MST%02d      ", au4TempInst[i4Count]);
                CliPrintf (CliHandle, "  %-2x:00:", au4Priority[i4Count]);

                CliPrintf (CliHandle, "%s", au1BrgAddr);
                CliPrintf (CliHandle, "%d sec %d cs  ",
                           AST_PROT_TO_BPDU_SEC (i4MaxAge),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
                CliPrintf (CliHandle, "%-2d s %-2d cs ",
                           AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                           AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

                CliPrintf (CliHandle, " %s\r\n", "  mstp");

                i4Count++;
            }

            break;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  MSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstpShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Module)
{
    UINT1               u1HeadFlag = AST_FALSE;
    CliRegisterLock (CliHandle, AstLock, AstUnLock);
    AST_LOCK ();

    if (MstpShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag) ==
        CLI_SUCCESS)
    {
        MstpShowRunningConfigTables (CliHandle, u4ContextId);

        if (u1HeadFlag == AST_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }

        if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG)
        {
            MstpShowRunningConfigInterface (CliHandle, u4ContextId);
        }
    }

    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Mstp      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
MstpShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT1 *pu1HeadFlag)
{

    tSNMP_OCTET_STRING_TYPE Name;
    UINT4               u4SysMode = (UINT4) AST_INIT_VAL;
    UINT1               au1ContextName[AST_SWITCH_ALIAS_LEN];
    UINT1               au1ConfigName[MST_CONFIG_NAME_LEN + 3];
    UINT1               au1DefName[MST_CONFIG_NAME_LEN + 1];
    UINT1              *pu1ConfigName = au1ConfigName;
    UINT1               u1Index;
    UINT1               u1PrintName = AST_SNMP_FALSE;

    INT4                i4ModStatus;
    INT4                i4SystemControl;
    INT4                i4Version = 0;
    INT4                i4HelloTime;
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4Priority;
    INT4                i4HoldCount;
    INT4                i4MaxHops;
    INT4                i4RegionVersion;
    INT4                i4DynamicPathcostCalc = AST_SNMP_FALSE;
    INT4                i4DynPathCostCalcSpeedChng = AST_SNMP_FALSE;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4MstMaxInstanceNumber;
    tAstBridgeEntry    *pBrgInfo = NULL;
    INT4                i4FlushInterval = AST_INIT_VAL;
    INT4                i4Threshold = AST_INIT_VAL;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    UINT1               u1ModeFlag = AST_FALSE;
    INT4                i4InstPortsMap = 0;
    u4SysMode = AstVcmGetSystemModeExt (AST_PROTOCOL_ID);

    MEMSET (au1DefName, 0, MST_CONFIG_NAME_LEN + 1);
    Name.pu1_OctetList = &au1DefName[0];
    Name.i4_Length = MST_CONFIG_NAME_LEN;

    /*Getting the Bridge Address */
    pBrgInfo = AST_GET_BRGENTRY ();

    /*SystemControl */
    nmhGetFsMIMstSystemControl (i4ContextId, &i4SystemControl);

    if (i4SystemControl == MST_SNMP_SHUTDOWN)
    {
        return CLI_FAILURE;
    }

    nmhGetFsMIMstModuleStatus (i4ContextId, &i4ModStatus);
    /*StpVersion */
    nmhGetFsMIMstForceProtocolVersion (i4ContextId, &i4Version);

    /*ForwardDelay */
    nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);

    /*HelloTime */
    nmhGetFsMIMstCistBridgeHelloTime (i4ContextId, &i4HelloTime);

    /*Max Age */
    nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);

    /*MaxHops */
    nmhGetFsMIMstMaxHopCount (i4ContextId, &i4MaxHops);

    /*TransmitHoldCount */
    nmhGetFsMIMstTxHoldCount (i4ContextId, &i4HoldCount);

    /*Cist Priority */
    nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);

    /* Mst Maximum Instance */
    nmhGetFsMIMstMaxMstInstanceNumber (i4ContextId, &i4MstMaxInstanceNumber);

    /*Dynamic Pathcost calculation */
    nmhGetFsMIMstCistDynamicPathcostCalculation (i4ContextId,
                                                 &i4DynamicPathcostCalc);
    nmhGetFsMIMstCalcPortPathCostOnSpeedChg (i4ContextId,
                                             &i4DynPathCostCalcSpeedChng);
    /*Mst Region Version */
    nmhGetFsMIMstMstiRegionVersion (i4ContextId, &i4RegionVersion);

    /* Mst Flush interval duration */
    nmhGetFsMIMstFlushInterval (i4ContextId, &i4FlushInterval);

    /* Cist Flush indication threshold */
    nmhGetFsMIMstCistFlushIndicationThreshold (i4ContextId, &i4Threshold);

    nmhGetFsMIMstBpduGuard (i4ContextId, &i4BpduGuard);

    /* Mst Instance Port Mapping */
    nmhGetFsMIMstInstPortsMap (i4ContextId, &i4InstPortsMap);

    /*Get Mst Region Name and Check if it is not equal to BridgeAddress */
    AST_MEMSET (Name.pu1_OctetList, 0, MST_CONFIG_NAME_LEN + 1);

    nmhGetFsMIMstMstiRegionName (i4ContextId, &Name);
    if (i4ContextId != AST_DEFAULT_CONTEXT)
    {
        if (i4SystemControl == MST_SNMP_START)
        {
            u1ModeFlag = AST_TRUE;
        }
    }

    for (u1Index = 0; u1Index < AST_MAC_ADDR_SIZE; u1Index++)
    {
        SPRINTF ((CHR1 *) pu1ConfigName, "%02x:",
                 pBrgInfo->BridgeAddr[u1Index]);
        pu1ConfigName += 3;
    }

    pu1ConfigName--;;
    *pu1ConfigName = '\0';

    /* Following commands name and revision no. should be configured only
     * under the mode "spanning-tree mst configuration"
     * */

    if (((STRCMP (au1ConfigName, Name.pu1_OctetList) != 0) &&
         (STRCMP (Name.pu1_OctetList, "") != 0)) ||
        (i4RegionVersion != MST_DEFAULT_CONFIG_LEVEL))
    {
        u1PrintName = AST_SNMP_TRUE;
    }
    if (u4SysMode == VCM_MI_MODE)
    {
        if ((u1ModeFlag != AST_FALSE) || (i4ModStatus != MST_ENABLED) ||
            (i4Version != AST_VERSION_3)
            || (i4FwdDelay != MST_DEFAULT_BRG_FWD_DELAY)
            || (i4HelloTime != MST_DEFAULT_BRG_HELLO_TIME)
            || (i4MaxAge != MST_DEFAULT_BRG_MAX_AGE)
            || (i4MaxHops != (MST_DEFAULT_MAX_HOPCOUNT * AST_CENTI_SECONDS))
            || (i4HoldCount != MST_DEFAULT_BRG_TX_LIMIT)
            || (i4Priority != MST_DEFAULT_BRG_PRIORITY)
            || (i4MstMaxInstanceNumber != MST_DEFAULT_MAX_INST)
            || (i4DynamicPathcostCalc != AST_SNMP_FALSE)
            || (i4DynPathCostCalcSpeedChng != AST_SNMP_FALSE)
            || (i4FlushInterval != MST_DEFAULT_FLUSH_INTERVAL)
            || (i4Threshold != MST_DEFAULT_FLUSH_IND_THRESHOLD)
            || (i4BpduGuard != AST_BPDUGUARD_DISABLE)
            || (u1PrintName != AST_SNMP_FALSE)
            || (i4InstPortsMap == MST_INST_MAP_ENABLED))
        {

            AST_MEMSET (au1ContextName, 0, AST_SWITCH_ALIAS_LEN);

            AstVcmGetAliasName (u4ContextId, au1ContextName);
            if (STRLEN (au1ContextName) != 0)
            {
                CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
                *pu1HeadFlag = AST_TRUE;
            }
        }
        else
        {
            return CLI_SUCCESS;
        }
    }
    if (u1ModeFlag != AST_FALSE)
    {
        CliPrintf (CliHandle, "spanning-tree mode mst\r\n");
    }

    /*Module Status */
    if (i4ModStatus != MST_ENABLED)
    {
        CliPrintf (CliHandle, "no spanning-tree\r\n");

    }
    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((AST_BRIDGE_MODE () == AST_PROVIDER_EDGE_BRIDGE_MODE) ||
        (AST_BRIDGE_MODE () == AST_PROVIDER_CORE_BRIDGE_MODE))
    {
        RstpPbPrintModuleStatus (CliHandle, u4ContextId);
    }

    AstReleaseContext ();

    if (i4Version != AST_VERSION_3)
    {
        if (i4Version == AST_VERSION_2)
        {
            CliPrintf (CliHandle, "spanning-tree compatibility rst\r\n");
        }
        else if (i4Version == AST_VERSION_0)
        {
            CliPrintf (CliHandle, "spanning-tree compatibility stp\r\n");
        }
    }

    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    if (i4FwdDelay != MST_DEFAULT_BRG_FWD_DELAY)
    {
        CliPrintf (CliHandle, "spanning-tree forward-time %d \r\n",
                   (i4FwdDelay / AST_CENTI_SECONDS));
    }

    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    if (i4HelloTime != MST_DEFAULT_BRG_HELLO_TIME)
    {
        CliPrintf (CliHandle, "spanning-tree hello-time %d \r\n",
                   (i4HelloTime / AST_CENTI_SECONDS));
    }

    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);
    if (i4MaxAge != MST_DEFAULT_BRG_MAX_AGE)
    {
        CliPrintf (CliHandle, "spanning-tree max-age %d \r\n",
                   (i4MaxAge / AST_CENTI_SECONDS));
    }

    i4MaxHops = AST_MGMT_TO_SYS (i4MaxHops);

    if (i4MaxHops != (MST_DEFAULT_MAX_HOPCOUNT * AST_CENTI_SECONDS))
    {
        CliPrintf (CliHandle, "spanning-tree mst max-hops %d \r\n",
                   (i4MaxHops / AST_CENTI_SECONDS));
    }

    if (i4HoldCount != MST_DEFAULT_BRG_TX_LIMIT)
    {
        CliPrintf (CliHandle, "spanning-tree transmit hold-count %d \r\n",
                   i4HoldCount);
    }

    if (i4Priority != MST_DEFAULT_BRG_PRIORITY)
    {
        CliPrintf (CliHandle, "spanning-tree priority %d\r\n", i4Priority);
    }

    if (i4MstMaxInstanceNumber != MST_DEFAULT_MAX_INST)
    {
        CliPrintf (CliHandle, "spanning-tree mst max-instance %d\r\n",
                   i4MstMaxInstanceNumber);
    }

    /*PathCost */

    if (i4DynamicPathcostCalc != AST_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic\r\n");
    }

    if (i4DynPathCostCalcSpeedChng != AST_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "spanning-tree pathcost dynamic lag-speed\r\n");
    }

    if (i4FlushInterval != MST_DEFAULT_FLUSH_INTERVAL)
    {
        CliPrintf (CliHandle, "spanning-tree flush-interval %d\r\n",
                   i4FlushInterval);
    }

    if (i4Threshold != MST_DEFAULT_FLUSH_IND_THRESHOLD)
    {
        CliPrintf (CliHandle, "spanning-tree flush-indication-threshold %d\r\n",
                   i4Threshold);
    }

    if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
    {
        CliPrintf (CliHandle, "spanning-tree portfast bpduguard default\r\n");
    }
    if (i4InstPortsMap == MST_INST_MAP_DISABLED)
    {
        CliPrintf (CliHandle, "mst instance-port-mapping disable\r\n");
    }
    /* The below 2 objects are configured in the MSTP configuration mode */

    /*Get Mst Region Name and Check if it is not equal to BridgeAddress */

    AST_MEMSET (Name.pu1_OctetList, 0, MST_CONFIG_NAME_LEN + 1);

    nmhGetFsMIMstMstiRegionName (i4ContextId, &Name);

    for (u1Index = 0; u1Index < AST_MAC_ADDR_SIZE; u1Index++)
    {
        SPRINTF ((CHR1 *) pu1ConfigName, "%02x:",
                 pBrgInfo->BridgeAddr[u1Index]);
        pu1ConfigName += 3;
    }
    pu1ConfigName--;;
    *pu1ConfigName = '\0';

    /* Following commands name and revision no. should be configured only
     ** under the mode "spanning-tree mst configuration"
     ***/

    if (((STRCMP (au1ConfigName, Name.pu1_OctetList) != 0) &&
         (STRCMP (Name.pu1_OctetList, "") != 0)) ||
        (i4RegionVersion != MST_DEFAULT_CONFIG_LEVEL))
    {
        CliPrintf (CliHandle, "spanning-tree mst configuration\r\n");

        if ((STRCMP (au1ConfigName, Name.pu1_OctetList) != 0) &&
            (STRCMP (Name.pu1_OctetList, "") != 0))
        {
            CliPrintf (CliHandle, "name  %s\r\n", Name.pu1_OctetList);
        }

        if (i4RegionVersion != MST_DEFAULT_CONFIG_LEVEL)
        {
            CliPrintf (CliHandle, "revision %d\r\n", i4RegionVersion);
        }

        CliPrintf (CliHandle, "!\r\n");
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays instance to vlan mapping    */
/*                        objects scanning the Mstp table                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
MstpShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE VlanMap;
    tSNMP_OCTET_STRING_TYPE *pMstVlanListAll = NULL;
    tSNMP_OCTET_STRING_TYPE *pNullBuf = NULL;
    tAstBoolean         bFlag;
    tAstBoolean         bTmpFlag;
    INT4                i4NextInst;
    INT4                i4CurrentInst;
    INT4                i4NextContextId;
    INT4                i4Priority;
    INT4                i4CommaCount = 0;
    INT1                i1OutCome;
    UINT1               au1VlanMapBuf[MST_VLANMAP_LIST_SIZE];
    UINT1               au1NullMapBuf[MST_VLANMAP_LIST_SIZE];
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4Threshold = AST_INIT_VAL;

    MEMSET (au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    MEMSET (au1NullMapBuf, 0, MST_VLANMAP_LIST_SIZE);

    VlanMap.pu1_OctetList = &au1VlanMapBuf[0];
    VlanMap.i4_Length = MST_VLANMAP_LIST_SIZE;

    if ((pMstVlanListAll = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        return;
    }
    pMstVlanListAll->i4_Length = MST_VLAN_LIST_SIZE;

    if ((pNullBuf = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        return;
    }
    pNullBuf->i4_Length = MST_VLAN_LIST_SIZE;
    /*Instance Specific Priority */

    i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        nmhGetFsMIMstMstiBridgePriority (i4NextContextId, i4NextInst,
                                         &i4Priority);
        if (i4Priority != MST_DEFAULT_BRG_PRIORITY)
        {
            CliPrintf (CliHandle,
                       "spanning-tree mst %d priority %d\r\n",
                       i4NextInst, i4Priority);

        }

        nmhGetFsMIMstMstiFlushIndicationThreshold (i4NextContextId, i4NextInst,
                                                   &i4Threshold);

        if (i4Threshold != MST_DEFAULT_FLUSH_IND_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "spanning-tree mst %d flush-indication-threshold %d\r\n",
                       i4NextInst, i4Threshold);
        }

        i4CurrentInst = i4NextInst;

        i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4CurrentInst,
                                                           &i4NextInst);
    }

    /* walk through the vlan to instance mapping table */

    i1OutCome = nmhGetNextIndexFsMIMstVlanInstanceMappingTable (i4ContextId,
                                                                &i4NextContextId,
                                                                0, &i4NextInst);

    /* Instance to vlan mapping command should be configured only
     * under the mode "spanning-tree mst configuration"
     * */
    if (i1OutCome == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "spanning-tree mst configuration\r\n");
    }
    else
    {
        free_octetstring (pMstVlanListAll);
        free_octetstring (pNullBuf);
        return;
    }

    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        bFlag = MST_FALSE;
        bTmpFlag = MST_FALSE;

        /* Instance Vlan Mapped 1K */
        CliPrintf (CliHandle, "instance %d ", i4NextInst);
        CLI_MEMSET (VlanMap.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped (i4NextContextId, i4NextInst, &VlanMap);

        if (AST_MEMCMP
            (VlanMap.pu1_OctetList, au1NullMapBuf, MST_VLANMAP_LIST_SIZE) != 0)
        {
            bTmpFlag = MST_TRUE;
            CliPrintf (CliHandle, "vlan ");
            bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap.pu1_OctetList,
                                           VlanMap.i4_Length,
                                           MST_VLANS_INSTANCE_1K, bFlag,
                                           &i4CommaCount);
        }

        /* Instance Vlan Mapped2k */
        CLI_MEMSET (VlanMap.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped2k (i4NextContextId, i4NextInst,
                                           &VlanMap);
        if (AST_MEMCMP
            (VlanMap.pu1_OctetList, au1NullMapBuf, MST_VLANMAP_LIST_SIZE) != 0)
        {
            if (bTmpFlag != MST_TRUE)
            {
                CliPrintf (CliHandle, "vlan ");
                bTmpFlag = MST_TRUE;
            }
            bFlag =
                MstDisplayOctetToVlan (CliHandle, VlanMap.pu1_OctetList,
                                       VlanMap.i4_Length, MST_VLANS_INSTANCE_2K,
                                       bFlag, &i4CommaCount);
        }

        /* Msti Instance Vlan Mapped3k */
        CLI_MEMSET (VlanMap.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped3k (i4NextContextId, i4NextInst,
                                           &VlanMap);
        if (AST_MEMCMP
            (VlanMap.pu1_OctetList, au1NullMapBuf, MST_VLANMAP_LIST_SIZE) != 0)
        {
            if (bTmpFlag != MST_TRUE)
            {
                CliPrintf (CliHandle, "vlan ");
                bTmpFlag = MST_TRUE;
            }
            bFlag =
                MstDisplayOctetToVlan (CliHandle, VlanMap.pu1_OctetList,
                                       VlanMap.i4_Length, MST_VLANS_INSTANCE_3K,
                                       bFlag, &i4CommaCount);
        }

        /* Msti Instance Vlan Mapped4k */
        CLI_MEMSET (VlanMap.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped4k (i4NextContextId, i4NextInst,
                                           &VlanMap);
        if (AST_MEMCMP
            (VlanMap.pu1_OctetList, au1NullMapBuf, MST_VLANMAP_LIST_SIZE) != 0)
        {
            if (bTmpFlag != MST_TRUE)
            {
                CliPrintf (CliHandle, "vlan ");
                bTmpFlag = MST_TRUE;
            }
            bFlag =
                MstDisplayOctetToVlan (CliHandle, VlanMap.pu1_OctetList,
                                       VlanMap.i4_Length, MST_VLANS_INSTANCE_4K,
                                       bFlag, &i4CommaCount);
        }
        /* Msti Instance Vlan Mapped > 4k */
        CLI_MEMSET (pMstVlanListAll->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        CLI_MEMSET (pNullBuf->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        nmhGetFsMIMstSetVlanList (i4NextContextId, i4NextInst, pMstVlanListAll);
        if (AST_MEMCMP (pMstVlanListAll->pu1_OctetList,
                        pNullBuf->pu1_OctetList, MST_VLAN_LIST_SIZE) != 0)
        {
            if (bTmpFlag != MST_TRUE)
            {
                CliPrintf (CliHandle, "vlan ");
                bTmpFlag = MST_TRUE;
            }
            bFlag =
                MstDisplayOctetToVlan (CliHandle,
                                       pMstVlanListAll->pu1_OctetList,
                                       pMstVlanListAll->i4_Length,
                                       MST_VLANS_INSTANCE_8K, bFlag,
                                       &i4CommaCount);
        }
        CliPrintf (CliHandle, "\r\n");

        i4CurrentInst = i4NextInst;

        i1OutCome =
            nmhGetNextIndexFsMIMstVlanInstanceMappingTable (i4ContextId,
                                                            &i4NextContextId,
                                                            i4CurrentInst,
                                                            &i4NextInst);
    }

    CliPrintf (CliHandle, "!\r\n");
    free_octetstring (pMstVlanListAll);
    free_octetstring (pNullBuf);

    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface table for  Mstp   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
MstpShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4NextPort;
    INT4                i4CurrentPort;
    INT4                i4OutCome;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               pu1Flag;
    UINT4               u4IcclIfIndex = AST_INIT_VAL;

    AST_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    AstIcchGetIcclIfIndex (&u4IcclIfIndex);

    while ((i4OutCome != RST_FAILURE)
           && (i4NextPort <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
    {
        /* Relinquish the control to the task which waits for AST Lock */
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        pu1Flag = AST_FALSE;

        if (u4IcclIfIndex == (UINT4) i4NextPort)
        {
            i4CurrentPort = i4NextPort;
            i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                                 &i4NextPort);
            continue;
        }
        MstpShowRunningConfigInterfaceDetails (CliHandle, i4NextPort, &pu1Flag);
        MstpShowRunningConfigInstInterface (CliHandle, i4NextPort, &pu1Flag);

        CliRegisterLock (CliHandle, AstLock, AstUnLock);
        AST_LOCK ();
        if (pu1Flag == AST_TRUE)
        {
            CliPrintf (CliHandle, "! \r\n");
            pu1Flag = AST_FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface objects in    */
/*                        Mstp                                               */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
MstpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex,
                                       UINT1 *pu1Flag)
{

    INT4                i4EdgePort = AST_SNMP_FALSE;
    INT4                i4HelloTime;
    INT4                i4AutoEdge;
    INT4                i4PortPriority;
    INT4                i4PortStatus;
    INT4                i4LinkType = AST_AUTO;
    INT4                i4PortPathCost;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4ContextId;
    INT4                i4RestricRole = (INT4) AST_INIT_VAL;
    INT4                i4RootGuard = AST_SNMP_FALSE;
    INT4                i4RestricTCN;
    INT4                i4LocalPort = 0;
    UINT2               u2LocalPortId;
    UINT1               u1PbPortType = VLAN_INVALID_PROVIDER_PORT;
    INT4                i4EnableBpduRx;
    INT4                i4EnableBpduTx;
    INT4                i4IsL2Gp;
    INT4                i4BrgPriority;
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    INT4                i4BpduGuardAction = AST_INIT_VAL;
    UINT2               u2Prio;
    UINT1               au1PseudoRootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               u1IsPathCostSet = AST_SNMP_FALSE;
    UINT1               u1IsRestRoleEnabled = AST_FALSE;
    UINT1               u1IsRestTCNEnabled = AST_FALSE;
    UINT1               u1PrintPseudoRootId = AST_SNMP_FALSE;

    tAstMacAddr         BrgMacAddr;
    tSNMP_OCTET_STRING_TYPE PseudoRootId;

    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    /* Accessing MI nmhGet and nmhGetNext routines slows down the 
     * SRC output. Hence it is better to access the SI nmhGet and nmhGetNext
     * routines. So from the given IfIndex do the following:
     *            - get the Context id and Local port number.
     *            - Select the context
     *            - call SI nmh routines with local port number.
     * 
     * If we register AstLock and AstUnLock only with CliPrintf, then we
     * may face the following issue:
     *         - if the SRC output crosses a page, 
     *                 - the CLI will call AstUnLock.
     *                 - if the user presses any key other than 'q', the CLI
     *                   will call AstLock function.
     *                   As AstLock selects the default context, and this
     *                   may be same as the context selected in this 
     *                   show running config function. This will result in
     *                   wrong SRC out put.
     * To over come the above problem,  the following are added:
     *                - A global variable (gu4StpCliContext) - to store the 
     *                  Context required by SRC function.
     *                - and register the following 2 functions:
     *                - AstLockAndSelCliContext and AstUnLock.
     * For SRC functions that uses SI nmh routines the following to be done:
     *        - register with CLI the above mentioned function and AstUnLock. 
     *        - After selecting the context, set the gu4StpCliContext to
     *          the required context.
     * By this way, once the paging happens the CLI will call 
     * AstLockAndSelCliContext, when the user want to see more output.
     * And this AstLockAndSelCliContext will take the RSTP sem and will
     * also select the proper context.
     *
     */

    MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (BrgMacAddr, AST_INIT_VAL, AST_MAC_ADDR_SIZE);

    /* Get the Bridge Port-type */
    AstL2IwfGetPbPortType (i4IfIndex, &u1PbPortType);

    /* Needed this check when called from Cfa */
    CliRegisterLock (CliHandle, AstLockAndSelCliContext, AstUnLock);
    AST_LOCK ();

    if (AstGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    i4LocalPort = (INT4) u2LocalPortId;

    if (!AstIsMstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Select the context here as we call SI nmh routines below */
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing AST-LOCK in CliPrintf */

    gu4StpCliContext = u4ContextId;

    if (nmhValidateIndexInstanceFsMstCistPortTable (i4LocalPort)
        == SNMP_SUCCESS)
    {
        pAstPortEntry = AST_GET_PORTENTRY (i4LocalPort);

        if (pAstPortEntry == NULL)
        {
            gu4StpCliContext = 0;
            AST_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        /* Following properties are not applicable for SISP logical 
         * interfaces
         * 1) Auto-Edge
         * 2) Admin-Edge
         * 3) Admin-P2P
         * Hence, these configurations should not be shown
         * */
        nmhGetFsMstCistPortAutoEdgeStatus (i4LocalPort, &i4AutoEdge);
        /*EdgePort */
        nmhGetFsMstCistPortAdminEdgeStatus (i4LocalPort, &i4EdgePort);
        /*LinkType */
        nmhGetFsMstCistPortAdminP2P (i4LocalPort, &i4LinkType);
        /*PortPriority */
        nmhGetFsMstCistPortPriority (i4LocalPort, &i4PortPriority);
        /*PortStatus */
        nmhGetFsMstCistForcePortState (i4LocalPort, &i4PortStatus);
        /*PortHelloTime */
        nmhGetFsMstCistPortHelloTime (i4LocalPort, &i4HelloTime);
        i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

        nmhGetFsMstCistPortEnableBPDURx (i4LocalPort, &i4EnableBpduRx);
        nmhGetFsMstCistPortEnableBPDUTx (i4LocalPort, &i4EnableBpduTx);
        nmhGetFsMstCistPortIsL2Gp (i4LocalPort, &i4IsL2Gp);

        if (AstIsPathCostSet (u2LocalPortId, MST_CIST_CONTEXT) == RST_TRUE)
        {
            u1IsPathCostSet = AST_SNMP_TRUE;
        }

        nmhGetFsMstCistPortBpduGuard (i4LocalPort, &i4BpduGuard);

        nmhGetFsMstCistPortRootGuard (i4LocalPort, &i4RootGuard);

        /*RestrictedRole */
        nmhGetFsMstCistPortRestrictedRole (i4LocalPort, &i4RestricRole);
        /*RestrictedTCN */
        nmhGetFsMstCistPortRestrictedTCN (i4LocalPort, &i4RestricTCN);

        if (u1PbPortType == VLAN_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_CUSTOMER_BRIDGE_PORT)
        {
            /*RestrictedRole */
            if (i4RestricRole == AST_SNMP_TRUE)
            {
                u1IsRestRoleEnabled = AST_SNMP_TRUE;
            }
            /*RestrictedTCN */
            if (i4RestricTCN == AST_SNMP_TRUE)
            {
                u1IsRestTCNEnabled = AST_SNMP_TRUE;
            }
        }
        else
        {
            /*RestrictedRole */
            if (i4RestricRole == AST_SNMP_FALSE)
            {
                u1IsRestRoleEnabled = AST_SNMP_FALSE;
            }
            /*RestrictedTCN */
            if (i4RestricTCN == AST_SNMP_FALSE)
            {
                u1IsRestTCNEnabled = AST_SNMP_FALSE;
            }
        }
        nmhGetFsMstCistPortPseudoRootId (i4LocalPort, &PseudoRootId);

        RstGetBridgeAddr (&BrgMacAddr);

        nmhGetFsMstCistBridgePriority (&i4BrgPriority);

        PrintMacAddress (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1PseudoRootAddr);

        u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

        /*Do not display, if Pseudo RootId is same as Birdge Id */
        if ((AST_MEMCMP (BrgMacAddr, PseudoRootId.pu1_OctetList +
                         AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE) != 0) ||
            (u2Prio != i4BrgPriority))
        {
            u1PrintPseudoRootId = AST_SNMP_TRUE;
        }

        if (((i4AutoEdge != AST_DEFAULT_AUTOEDGE_VALUE) ||
             (i4EdgePort != AST_SNMP_FALSE) ||
             (i4LinkType != AST_AUTO) ||
             (i4PortPriority != MST_DEFAULT_PORT_PRIORITY) ||
             (i4PortStatus != MST_FORCE_STATE_ENABLED) ||
             (i4HelloTime != MST_DEFAULT_BRG_HELLO_TIME) ||
             (i4EnableBpduRx != AST_SNMP_TRUE)
             || (pAstPortEntry->bLoopGuard != RST_FALSE)
             || (i4EnableBpduTx != AST_SNMP_TRUE)
             || (i4IsL2Gp != AST_SNMP_FALSE)
             || (u1IsPathCostSet != AST_SNMP_FALSE)
             || (i4BpduGuard != AST_BPDUGUARD_NONE)
             || (i4RootGuard != AST_SNMP_FALSE)
             || (u1IsRestRoleEnabled != AST_FALSE)
             || (u1IsRestTCNEnabled != AST_FALSE)
             || (u1PrintPseudoRootId != AST_SNMP_FALSE))
            && (*pu1Flag != AST_TRUE))
        {
            CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            *pu1Flag = AST_TRUE;
        }

        if (!((i4IfIndex >= AST_MIN_SISP_LOG_INDEX) &&
              (i4IfIndex <= AST_MAX_SISP_LOG_INDEX)))
        {

            if (i4AutoEdge != AST_DEFAULT_AUTOEDGE_VALUE)
            {
                CliPrintf (CliHandle, "no spanning-tree auto-edge\r\n");
            }

            if (i4EdgePort != AST_SNMP_FALSE)
            {
                CliPrintf (CliHandle, "spanning-tree portfast\r\n");

            }

            if (i4LinkType != AST_AUTO)
            {
                if (i4LinkType == AST_FORCE_TRUE)
                {
                    CliPrintf (CliHandle,
                               "spanning-tree link-type point-to-point\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               " spanning-tree link-type shared\r\n");
                }
            }
            if (pAstPortEntry->bLoopGuard != RST_FALSE)
            {
                CliPrintf (CliHandle, "spanning-tree loop-guard\r\n");
            }

        }

        /*PortPathCost */
        if (u1IsPathCostSet != AST_SNMP_FALSE)
        {
            nmhGetFsMstCistPortAdminPathCost (i4LocalPort, &i4PortPathCost);
            CliPrintf (CliHandle, "spanning-tree cost %d\r\n", i4PortPathCost);
        }
        if (u1IsRestRoleEnabled == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree restricted-role\r\n");
        }
        else if (u1IsRestRoleEnabled == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "no spanning-tree restricted-role\r\n");
        }
        if (u1IsRestTCNEnabled == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree restricted-tcn\r\n");
        }
        else if (u1IsRestTCNEnabled == AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "no spanning-tree restricted-tcn\r\n");
        }

        if (i4PortPriority != MST_DEFAULT_PORT_PRIORITY)
        {
            CliPrintf (CliHandle, "spanning-tree port-priority %d\r\n",
                       i4PortPriority);

        }

        if (i4PortStatus != MST_FORCE_STATE_ENABLED)
        {
            CliPrintf (CliHandle, "spanning-tree disable\r\n");
        }

        if (i4HelloTime != MST_DEFAULT_BRG_HELLO_TIME)
        {
            CliPrintf (CliHandle,
                       " spanning-tree mst hello-time %d\r\n",
                       (i4HelloTime / AST_CENTI_SECONDS));
        }

        if (i4EnableBpduRx != AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-receive disabled\r\n");
        }

        if (i4EnableBpduTx != AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "spanning-tree bpdu-transmit disabled\r\n");
        }

        if (i4IsL2Gp != AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree layer2-gateway-port\r\n");
        }

        if (u1PrintPseudoRootId != AST_SNMP_FALSE)
        {
            CliPrintf
                (CliHandle,
                 "spanning-tree pseudoRootId priority %d mac-address %s\r\n",
                 u2Prio, au1PseudoRootAddr);

        }

        if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
        {
            nmhGetFsMIMstPortBpduGuardAction (i4IfIndex, &i4BpduGuardAction);
            if (i4BpduGuardAction == AST_PORT_ADMIN_DOWN)
            {
                CliPrintf (CliHandle,
                           "spanning-tree bpduguard enable admin-down\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "spanning-tree bpduguard enable\r\n");
            }
        }

        if (i4RootGuard != AST_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "spanning-tree guard root\r\n");
        }

    }
    gu4StpCliContext = 0;
    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigInstInterface                 */
/*                                                                           */
/*     DESCRIPTION      : This function scans the Instance specific interface */
/*                        table  in mstp                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
MstpShowRunningConfigInstInterface (tCliHandle CliHandle, INT4 i4IfIndex,
                                    UINT1 *pu1Flag)
{
    UINT4               u4ContextId;
    INT4                i4NextInstPort;
    INT4                i4NextInst;
    INT4                i4CurrentInst = 0;
    INT4                i4CurrentInstPort;
    INT4                i4LocalPort = 0;
    UINT2               u2LocalPortId;

    /* Accessing MI nmhGet and nmhGetNext routines slows down the 
     * SRC output. Hence it is better to access the SI nmhGet and nmhGetNext
     * routines. So from the given IfIndex do the following:
     *            - get the Context id and Local port number.
     *            - Select the context
     *            - call SI nmh routines with local port number.
     * 
     * If we register AstLock and AstUnLock only with CliPrintf, then we
     * may face the following issue:
     *         - if the SRC output crosses a page, 
     *                 - the CLI will call AstUnLock.
     *                 - if the user presses any key other than 'q', the CLI
     *                   will call AstLock function.
     *                   As AstLock selects the default context, and this
     *                   may be same as the context selected in this 
     *                   show running config function. This will result in
     *                   wrong SRC out put.
     * To over come the above problem,  the following are added:
     *                - A global variable (gu4StpCliContext) - to store the 
     *                  Context required by SRC function.
     *                - and register the following 2 functions:
     *                - AstLockAndSelCliContext and AstUnLock.
     * For SRC functions that uses SI nmh routines the following to be done:
     *        - register with CLI the above mentioned function and AstUnLock. 
     *        - After selecting the context, set the gu4StpCliContext to
     *          the required context.
     * By this way, once the paging happens the CLI will call 
     * AstLockAndSelCliContext, when the user want to see more output.
     * And this AstLockAndSelCliContext will take the RSTP sem and will
     * also select the proper context.
     *
     */

    CliRegisterLock (CliHandle, AstLockAndSelCliContext, AstUnLock);
    AST_LOCK ();

    if (AstGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == RST_FAILURE)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    i4LocalPort = (INT4) u2LocalPortId;

    if (!AstIsMstStartedInContext (u4ContextId))
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Select the context here as we call SI nmh routines below */
    if (AstSelectContext (u4ContextId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing AST-LOCK in CliPrintf */

    gu4StpCliContext = u4ContextId;

    i4CurrentInstPort = i4LocalPort;

    while (nmhGetNextIndexFsMstMstiPortTable (i4CurrentInstPort,
                                              &i4NextInstPort,
                                              i4CurrentInst,
                                              &i4NextInst) != SNMP_FAILURE)
    {
        if (i4LocalPort != i4NextInstPort)
        {
            break;
        }

        if (i4NextInst != AST_TE_MSTID)
        {
            MstpShowRunningConfigInstInterfaceDetails (CliHandle,
                                                       i4NextInstPort,
                                                       i4NextInst, pu1Flag);
        }

        i4CurrentInstPort = i4NextInstPort;
        i4CurrentInst = i4NextInst;
    }

    gu4StpCliContext = 0;
    AST_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstpShowRunningConfigInstInterfaceDetails          */
/*                                                                           */
/*     DESCRIPTION      : This function displays Instance specific interface */
/*                        objects in mstp                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
MstpShowRunningConfigInstInterfaceDetails (tCliHandle CliHandle,
                                           INT4 i4InstPort, INT4 i4Inst,
                                           UINT1 *pu1Flag)
{
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    tAstMacAddr         BrgMacAddr;
    UINT1               au1PseudoRootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4PortPriority;
    INT4                i4PortStatus;
    INT4                i4PortPathCost;
    INT4                i4BrgPriority = 0;
    UINT2               u2Prio;
    UINT1               u1IsPathCostSet = AST_SNMP_FALSE;
    UINT1               u1PrintPseudoRootId = AST_SNMP_FALSE;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (BrgMacAddr, AST_INIT_VAL, AST_MAC_ADDR_SIZE);

    /*PortPathCost */
    if (AstIsPathCostSet ((UINT2) i4InstPort, (UINT2) i4Inst) == RST_TRUE)
    {
        u1IsPathCostSet = AST_SNMP_TRUE;
    }

    /*PortPriority */

    nmhGetFsMstMstiPortPriority (i4InstPort, i4Inst, &i4PortPriority);

    /*PortStatus */
    nmhGetFsMstMstiForcePortState (i4InstPort, i4Inst, &i4PortStatus);

    nmhGetFsMstMstiPortPseudoRootId (i4InstPort, i4Inst, &PseudoRootId);

    RstGetBridgeAddr (&BrgMacAddr);

    nmhGetFsMstMstiBridgePriority (i4Inst, &i4BrgPriority);

    PrintMacAddress (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                     au1PseudoRootAddr);

    u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

    /*Do not display, if Pseudo RootId is same as Bridge Id */
    if ((AST_MEMCMP (BrgMacAddr, PseudoRootId.pu1_OctetList +
                     AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE) != 0) ||
        ((u2Prio != i4BrgPriority) && (u2Prio != MST_DEFAULT_BRG_PRIORITY)))
    {
        u1PrintPseudoRootId = AST_SNMP_TRUE;
    }

    if (((u1IsPathCostSet != AST_SNMP_FALSE)
         || (i4PortPriority != MST_DEFAULT_PORT_PRIORITY)
         || (i4PortStatus != MST_FORCE_STATE_ENABLED)
         || (u1PrintPseudoRootId != AST_SNMP_FALSE)) && (*pu1Flag != AST_TRUE))
    {
        CfaCliConfGetIfName ((UINT4) i4InstPort, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        *pu1Flag = AST_TRUE;
    }

    if (u1IsPathCostSet != AST_SNMP_FALSE)
    {
        nmhGetFsMstMstiPortAdminPathCost (i4InstPort, i4Inst, &i4PortPathCost);
        CliPrintf (CliHandle, "spanning-tree mst %d cost %ld\r\n", i4Inst,
                   i4PortPathCost);
    }

    if (i4PortPriority != MST_DEFAULT_PORT_PRIORITY)
    {
        CliPrintf (CliHandle,
                   " spanning-tree mst %d  port-priority %d\r\n",
                   i4Inst, i4PortPriority);
    }

    if (i4PortStatus != MST_FORCE_STATE_ENABLED)
    {
        CliPrintf (CliHandle, "spanning-tree mst %d disable\r\n", i4Inst);
    }

    if (u1PrintPseudoRootId != AST_SNMP_FALSE)
    {
        CliPrintf
            (CliHandle,
             "spanning-tree mst %d pseudoRootId priority %d mac-address %s\r\n",
             i4Inst, u2Prio, au1PseudoRootAddr);

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliDisplayInterfaceDetails                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface Details      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Value-Interface Info to be displayed             */
/*                        u4Index-Interface Index                            */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliDisplayInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Index, UINT4 u4Val)
{

    INT1                i1OutCome;
    INT4                i4PrevInst;
    INT4                i4NextInst;
    INT4                i4PortPriority;
    INT4                i4PortFast;
    INT4                i4PathCost;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4RootCost = 0;
    INT4                i4FwdTransition;
    INT4                i4RxBpdu;
    INT4                i4TxBpdu;
    INT4                i4InvalidBpdu;
    INT4                i4MigrnCount;
    INT4                i4PortRole;
    INT4                i4LinkType;

    UINT4               u4PagingStatus = CLI_SUCCESS;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4NextContextId;
    INT4                i4RestricRole;
    INT4                i4RestricTCN;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4LoopIncState = AST_FALSE;
    INT4                i4RootIncState = AST_FALSE;
    INT4                i4BpduIncState = AST_FALSE;

    if (u4Index != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4Index);
        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }
    switch (u4Val)
    {
        case STP_PORT_COST:

            nmhGetFsMIMstCistPortPathCost (u4Index, &i4PathCost);

            CliPrintf (CliHandle, "\r\nMST00     %-9d\r\n", i4PathCost);

            /* Take every instance one by one */
            i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                               &i4NextContextId,
                                                               0, &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (MstMiValidateMstiPortEntry
                    (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                {
                    if (nmhGetFsMIMstMstiPortPathCost (u4Index, i4NextInst,
                                                       &i4PathCost) ==
                        SNMP_SUCCESS)
                    {

                        CliPrintf (CliHandle, "MST%02d     %-9d\r\n",
                                   i4NextInst, i4PathCost);
                    }
                }
                i4PrevInst = i4NextInst;
                i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                                   &i4NextContextId,
                                                                   i4PrevInst,
                                                                   &i4NextInst);
            }

            break;
        case STP_PORT_PRIORITY:

            nmhGetFsMIMstCistPortPriority (u4Index, &i4PortPriority);

            CliPrintf (CliHandle, "\r\nMST00     %d\r\n", i4PortPriority);

            /* Take every instance one by one */
            i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                               &i4NextContextId,
                                                               0, &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (MstMiValidateMstiPortEntry
                    (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                {
                    if (nmhGetFsMIMstMstiPortPriority (u4Index, i4NextInst,
                                                       &i4PortPriority) ==
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "MST%02d     %-9d\r\n",
                                   i4NextInst, i4PortPriority);
                    }
                }
                i4PrevInst = i4NextInst;
                i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                                   &i4NextContextId,
                                                                   i4PrevInst,
                                                                   &i4NextInst);
                u4PagingStatus = CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }

            }

            break;

        case STP_PORTFAST:

            nmhGetFsMIMstCistPortAdminEdgeStatus (u4Index, &i4PortFast);

            if (i4PortFast == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nMST00     enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nMST00     disabled\r\n");
            }

            /* Take every instance one by one */
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {

                if (MstMiValidateMstiPortEntry
                    (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                {
                    if (i4PortFast == AST_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "\rMST%02d     enabled\r\n",
                                   i4NextInst);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\rMST%02d     disabled\r\n",
                                   i4NextInst);
                    }
                }
                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                u4PagingStatus = CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }
            }

            break;

        case STP_RESTRICTED_ROLE:
            nmhGetFsMIMstCistPortRestrictedRole (u4Index, &i4RestricRole);

            if (i4RestricRole == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nRestricted Role is Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRestricted Role is Disabled\r\n");
            }
            break;

        case STP_RESTRICTED_TCN:
            nmhGetFsMIMstCistPortRestrictedTCN (u4Index, &i4RestricTCN);

            if (i4RestricTCN == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "\r\nRestricted TCN is Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRestricted TCN is Disabled\r\n");
            }

            break;

        case STP_SHOW_DETAIL:

            CliPrintf (CliHandle, "\r\n");

            MstCliDisplayPortDetails (CliHandle, u4ContextId, u4Index);

            u4PagingStatus = CliPrintf (CliHandle, "\r");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
                /* User Presses 'q' at the prompt and so quits */
            }

            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (MST_TRUE == MstIsPortMemberOfInst ((INT4) u4Index,
                                                       i4NextInst))
                {

                    MstCliDisplayMstiPortDetails (CliHandle, u4ContextId,
                                                  i4NextInst, (INT4) u4Index);

                }
                u4PagingStatus = CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }

                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
            }

            break;
        case STP_PORT_STATE:

            nmhGetFsMIMstCistPortState (u4Index, &i4PortState);
            AstGetHwPortStateStatus (u4Index, MST_CIST_CONTEXT,
                                     &i4PortStateStatus);

            CliPrintf (CliHandle, "\r\nMST00     ");
            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "\r\n");

            /* Take every instance one by one */
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);
            while ((i1OutCome != SNMP_FAILURE)
                   && (i4ContextId == i4NextContextId))
            {

                if (MstMiValidateMstiPortEntry
                    (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                {
                    nmhGetFsMIMstMstiPortState (u4Index,
                                                i4NextInst, &i4PortState);
                    AstGetHwPortStateStatus (u4Index, i4NextInst,
                                             &i4PortStateStatus);

                    CliPrintf (CliHandle, "MST%02d     ", i4NextInst);
                    StpCliDisplayPortState (CliHandle, i4PortState,
                                            i4PortStateStatus);
                    CliPrintf (CliHandle, "\r\n");
                }

                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
            }

            break;

        case STP_ROOT_COST:

            nmhGetFsMIMstCistRootCost (i4ContextId, &i4RootCost);
            CliPrintf (CliHandle, "\r\nMST00     %d\r\n", i4RootCost);

            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (i4NextInst == AST_TE_MSTID)
                {
                    break;
                }

                nmhGetFsMIMstMstiRootCost (i4ContextId, i4NextInst,
                                           &i4RootCost);

                CliPrintf (CliHandle, "MST%02d     %d\r\n", i4NextInst,
                           i4RootCost);

                u4PagingStatus = CliPrintf (CliHandle, "\r");
                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }

                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);

            }

            break;

        case STP_PORT_STATS:

            /* Cist Port Forward Transitions */
            nmhGetFsMIMstCistPortForwardTransitions ((INT4) u4Index,
                                                     (UINT4 *)
                                                     &i4FwdTransition);

            /* Cist Port Received BPDUs */
            i4RxBpdu = MstCistPortRxBpduCount (u4Index);

            /* Cist Port Transmitted BPDUs */
            i4TxBpdu = MstCistPortTxBpduCount (u4Index);

            /* Cist Port Invalid BPDUs Received */
            nmhGetFsMIMstCistPortInvalidMstBpduRxCount ((INT4) u4Index,
                                                        (UINT4 *)
                                                        &i4InvalidBpdu);

            /* Cist Port Protocol Migration Count */
            nmhGetFsMIMstCistProtocolMigrationCount ((INT4) u4Index,
                                                     (UINT4 *) &i4MigrnCount);

            CliPrintf (CliHandle, "\r\nMST00 Statistics for Port  %d\r\n",
                       u4Index);
            CliPrintf (CliHandle,
                       "Port Fwd Transitions          : %d\r\n",
                       i4FwdTransition);
            CliPrintf (CliHandle, "Port Received BPDUs           : %d\r\n",
                       i4RxBpdu);

            CliPrintf (CliHandle,
                       "Port Transmitted BPDUs        : %d\r\n", i4TxBpdu);

            CliPrintf (CliHandle,
                       "Port Invalid BPDUs Received   : %d\r\n", i4InvalidBpdu);

            CliPrintf (CliHandle,
                       "Port Protocol Migration Count : %d\r\n", i4MigrnCount);

            /* Take every instance one by one */
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (MstMiValidateMstiPortEntry
                    (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                {
                    nmhGetFsMIMstMstiPortForwardTransitions (u4Index,
                                                             i4NextInst,
                                                             (UINT4 *)
                                                             &i4FwdTransition);

                    /* Cist Port Received BPDUs */
                    nmhGetFsMIMstMstiPortReceivedBPDUs (u4Index,
                                                        i4NextInst,
                                                        (UINT4 *) &i4RxBpdu);

                    /* Cist Port Transmitted BPDUs */
                    nmhGetFsMIMstMstiPortTransmittedBPDUs (u4Index,
                                                           i4NextInst,
                                                           (UINT4 *) &i4TxBpdu);

                    /* Cist Port Invalid BPDUs Received */
                    nmhGetFsMIMstMstiPortInvalidBPDUsRcvd (u4Index,
                                                           i4NextInst,
                                                           (UINT4 *)
                                                           &i4InvalidBpdu);

                    CliPrintf (CliHandle,
                               "\r\nMST%02d Statistics for Port  %d\r\n",
                               i4NextInst, u4Index);

                    CliPrintf (CliHandle,
                               "Port Fwd Transitions          : %d\r\n",
                               i4FwdTransition);

                    CliPrintf (CliHandle,
                               "Port Received BPDUs           : %d\r\n",
                               i4RxBpdu);

                    CliPrintf (CliHandle,
                               "Port Transmitted BPDUs        : %d\r\n",
                               i4TxBpdu);

                    CliPrintf (CliHandle,
                               "Port Invalid BPDUs Received   : %d\r\n",
                               i4InvalidBpdu);
                }

                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
            }

            break;

        default:

            /* Display the Port Role,PortState,PortCost,PortPriority,PortLinkType
             * in the Table Format
             * */
            /*PortRole */
            nmhGetFsMIMstCistCurrentPortRole (u4Index, &i4PortRole);

            /*Port State */
            nmhGetFsMIMstCistPortState (u4Index, &i4PortState);
            AstGetHwPortStateStatus (u4Index, MST_CIST_CONTEXT,
                                     &i4PortStateStatus);
            /*Port Cost */
            nmhGetFsMIMstCistPortPathCost (u4Index, &i4PathCost);
            /*PortID-Prio.Nbr */
            nmhGetFsMIMstCistPortPriority (u4Index, &i4PortPriority);
            /* Link Type */

            nmhGetFsMIMstCistPortOperP2P (u4Index, &i4LinkType);

            CliPrintf (CliHandle, "\r\n%-10s%-13s%-13s%-9s%-9s%-8s\r\n",
                       "Instance", "Role", "State", "Cost", "Prio", "type");
            CliPrintf (CliHandle, "\r\n%-10s%-13s%-13s%-9s%-9s%-8s\r\n",
                       "--------", "----", "-----", "----", "----", "----");

            CliPrintf (CliHandle, "%-10s", "MST00");

            StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");

            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "%-3s", " ");

            CliPrintf (CliHandle, "%-9d", i4PathCost);

            CliPrintf (CliHandle, "%-3d   ", i4PortPriority);

            if (i4LinkType == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "   %-8s", "P2P");
            }
            else
            {
                CliPrintf (CliHandle, "%-9s", "SharedLan");
            }
            MstGetInstPortLoopInconsistentState ((UINT2) u4Index,
                                                 (UINT2) MST_CIST_CONTEXT,
                                                 &i4LoopIncState);

            nmhGetFsMIMstCistPortRootInconsistentState ((UINT2) u4Index,
                                                        &i4RootIncState);
            nmhGetFsMIMstPortBpduInconsistentState ((UINT2) u4Index,
                                                    &i4BpduIncState);

            if (i4LoopIncState == MST_TRUE)
            {
                CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
            }
            else if (i4RootIncState == MST_TRUE)
            {
                CliPrintf (CliHandle, "(*Root-Inc)\r\n");
            }
            else if (i4BpduIncState == MST_TRUE)
            {
                CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");

            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);

            while ((i1OutCome != SNMP_FAILURE) &&
                   (i4ContextId == i4NextContextId))
            {
                if (MST_TRUE == MstIsPortMemberOfInst ((INT4) u4Index,
                                                       i4NextInst))
                {
                    if (MstMiValidateMstiPortEntry
                        (u4ContextId, u4Index, i4NextInst) == MST_SUCCESS)
                    {
                        /*PortRole */
                        nmhGetFsMIMstMstiCurrentPortRole (u4Index,
                                                          i4NextInst,
                                                          &i4PortRole);

                        /*Port State */
                        nmhGetFsMIMstMstiPortState (u4Index,
                                                    i4NextInst, &i4PortState);
                        AstGetHwPortStateStatus (u4Index, i4NextInst,
                                                 &i4PortStateStatus);
                        /*Port Cost */
                        nmhGetFsMIMstMstiPortPathCost (u4Index,
                                                       i4NextInst, &i4PathCost);
                        /*PortID-Prio.Nbr */
                        nmhGetFsMIMstMstiPortPriority (u4Index,
                                                       i4NextInst,
                                                       &i4PortPriority);
                        nmhGetFsMIMstCistPortOperP2P (u4Index, &i4LinkType);

                        CliPrintf (CliHandle, "MST%02d     ", i4NextInst);
                        StpCliDisplayPortRole (CliHandle, u4ContextId,
                                               i4PortRole);
                        CliPrintf (CliHandle, "%-3s", " ");
                        StpCliDisplayPortState (CliHandle, i4PortState,
                                                i4PortStateStatus);
                        CliPrintf (CliHandle, "%-3s", " ");

                        CliPrintf (CliHandle, "%-9d", i4PathCost);

                        CliPrintf (CliHandle, "%-3d   ", i4PortPriority);

                        if (i4LinkType == AST_SNMP_TRUE)
                        {
                            CliPrintf (CliHandle, "   %-8s", "P2P");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-9s", "SharedLan");
                        }
                        MstGetInstPortLoopInconsistentState ((UINT2) u4Index,
                                                             (UINT2) i4NextInst,
                                                             &i4LoopIncState);

                        nmhGetFsMIMstCistPortRootInconsistentState ((UINT2)
                                                                    u4Index,
                                                                    &i4RootIncState);
                        nmhGetFsMIMstPortBpduInconsistentState ((UINT2) u4Index,
                                                                &i4BpduIncState);

                        if (i4LoopIncState == MST_TRUE)
                        {
                            CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                        }
                        else if (i4RootIncState == MST_TRUE)
                        {
                            CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                        }
                        else if (i4BpduIncState == MST_TRUE)
                        {
                            CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");

                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r\n");
                        }

                    }
                }
                i4PrevInst = i4NextInst;
                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
                u4PagingStatus = CliPrintf (CliHandle, "\r");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }
                CliFlush (CliHandle);
            }

            break;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowSpanningTree                             */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the RootBridge information,*/
/*                        the bridge details and also the  information of    */
/*                        all the active ports                               */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Val-Info to be displayed                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Active)
{
    INT4                i4NextContextId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrentPort, i4NextPort;
    INT4                i4CurrentPortMsti, i4NextPortMsti;
    INT4                i4NextPortInstance;
    INT4                i4CurrentPortInstance;
    tSNMP_OCTET_STRING_TYPE RegRoot;
    INT4                i4OutCome;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = 0;
    UINT1               u1OperStatus;
    INT4                i4NextInst;
    INT4                i4PrevInst;
    INT4                i4TmpInst;
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4HelloTime = AST_INIT_VAL;
    INT4                i4RootMaxAge;
    INT4                i4RootFwdDelay;
    INT4                i4RootHelloTime = AST_INIT_VAL;
    INT4                i4Priority;
    INT4                i4PortPriority;
    INT4                i4Version = 0;
    INT4                i4PortRole;
    INT4                i4ModuleStatus;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4PathCost;
    INT4                i4PortCost;
    INT4                i4LinkType;
    INT1                i1InstOutCome;
    INT4                i4PortOutCome;
    UINT2               u2RootPrio;
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    tAstMacAddr         MacAddress;
    tAstMacAddr         RootMacAddress;
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4DynamicPathCost;
    INT4                i4DynamicPathCostLag;
    INT4                i4LoopIncState = AST_FALSE;
    INT4                i4RootIncState = AST_FALSE;
    INT4                i4BpduIncState = AST_FALSE;

    MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RegRoot.pu1_OctetList = &au1BrgIdBuf[0];
    RegRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    nmhGetFsMIMstModuleStatus (i4ContextId, &i4ModuleStatus);

    if (i4ModuleStatus != MST_DISABLED)
    {
        nmhGetFsMIMstForceProtocolVersion (i4ContextId, &i4Version);
    }

    /*Display Root Bridge Details */
    MstCliShowRootBridgeInfo (CliHandle, u4ContextId);

    /*Display Bridge Details */

    /* Bridge Priority */

    nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);
    /*Bridge Address */

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
    /*Max Age */

    nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Forward Delay */

    nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Hello Time */

    nmhGetFsMIMstCistBridgeHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    /*Dynamic Path Cost */

    nmhGetFsMIMstCistDynamicPathcostCalculation (i4ContextId,
                                                 &i4DynamicPathCost);
    nmhGetFsMIMstCalcPortPathCostOnSpeedChg (i4ContextId,
                                             &i4DynamicPathCostLag);

    CliPrintf (CliHandle, "\r\nMST00\r\n");

    MstCliPrintStpModuleStatus (CliHandle, u4ContextId, i4ModuleStatus,
                                i4Version);
    CliPrintf (CliHandle, "Bridge Id       Priority  %d\r\n", i4Priority);

    AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
    PrintMacAddress (MacAddress, au1BrgAddr);
    CliPrintf (CliHandle, "%-16sAddress  %s\r\n", " ", au1BrgAddr);
    CliPrintf (CliHandle, "%-16sMax age is %d sec %d cs,", " ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, " forward delay is %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "\t\tHello Time is %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

    if (i4DynamicPathCost == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\t\tDynamic Path Cost is Disabled\r\n");
    }

    if (i4DynamicPathCostLag == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\t\tDynamic Path Cost Lag-Speed Change is Disabled\r\n");
    }

    /* Display the Port Role,PortState,PortCost,PortPriority,PortLinkType */

    CliPrintf (CliHandle, "%-18s%-13s%-13s%-9s%-7s%-9s\r\n",
               "Name", "Role", "State", "Cost", "Prio", "Type");
    CliPrintf (CliHandle, "%-18s%-13s%-13s%-9s%-7s%-9s\r\n",
               "----", "----", "-----", "----", "----", "------");

    /* Get The Current Port */

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);
    while (i4OutCome != RST_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            if (u4Active == STP_ACTIVE_PORTS)
            {
                u1Flag = 0;
                nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);
                nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
                if (((i4PortState == AST_PORT_STATE_LEARNING) ||
                     (i4PortState == AST_PORT_STATE_FORWARDING))
                    && (i4PortRole != MST_PORT_ROLE_DISABLED))
                {
                    /* Only active ports need to be displayed if the command executed
                     * is "show spanning-tree active . This flag is used to qualify 
                     * if a certain port's information needs to be displayed. */

                    u1Flag = 1;
                }
            }

            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

                /*PortRole */
                nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);

                /*Port State */
                nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
                AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT,
                                         &i4PortStateStatus);

                /*Port Cost */
                nmhGetFsMIMstCistPortPathCost (i4NextPort, &i4PathCost);
                /*PortPriority */
                nmhGetFsMIMstCistPortPriority (i4NextPort, &i4PortPriority);
                /* Link Type */
                nmhGetFsMIMstCistPortOperP2P (i4NextPort, &i4LinkType);

                CliPrintf (CliHandle, "%-18s", au1IntfName);

                StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");

                StpCliDisplayPortState (CliHandle, i4PortState,
                                        i4PortStateStatus);

                CliPrintf (CliHandle, "%-3s", " ");
                CliPrintf (CliHandle, "%-9d", i4PathCost);
                CliPrintf (CliHandle, "%-7d", i4PortPriority);

                if (i4LinkType == AST_SNMP_TRUE)
                {
                    u4PagingStatus = CliPrintf (CliHandle, "%-9s", "P2P");
                }
                else
                {
                    u4PagingStatus = CliPrintf (CliHandle, "%-9s", "SharedLan");
                }
                nmhGetFsMIMstCistLoopInconsistentState (i4NextPort,
                                                        &i4LoopIncState);

                nmhGetFsMIMstCistPortRootInconsistentState (i4NextPort,
                                                            &i4RootIncState);
                nmhGetFsMIMstPortBpduInconsistentState (i4NextPort,
                                                        &i4BpduIncState);

                if (i4LoopIncState == MST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                }
                else if (i4RootIncState == MST_TRUE)
                {
                    CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                }
                else if (i4BpduIncState == MST_TRUE)
                {
                    CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
                }

                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                /* Get the Next Port from the Table */

            }
        }
        u1Flag = 0;
        i4CurrentPort = i4NextPort;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt and so quits */
            break;
        }

        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);

    }                            /*while */
    i1InstOutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId, 0,
                                                           &i4NextInst);

    while ((i1InstOutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        i4TmpInst = i4NextInst;

        if (i4TmpInst == AST_TE_MSTID)
        {
            break;
        }

        /* Root BridgeInfo */

        /*Get the Bridge MacAddress */
        AST_MEMSET (&RootMacAddress, 0, sizeof (tAstMacAddr));
        nmhGetFsMIMstBrgAddress (i4ContextId, &RootMacAddress);

        /*Root Id */
        nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst, &RegRoot);

        /*Root MaxAge */

        nmhGetFsMIMstCistMaxAge (i4ContextId, &i4RootMaxAge);
        i4RootMaxAge = AST_MGMT_TO_SYS (i4RootMaxAge);

        /*Root ForwardTime */

        nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4RootFwdDelay);
        i4RootFwdDelay = AST_MGMT_TO_SYS (i4RootFwdDelay);

        /*Root HelloTime */
        i4RootHelloTime = AST_INIT_VAL;
        nmhGetFsMIMstCistHelloTime (i4ContextId, &i4RootHelloTime);
        i4RootHelloTime = AST_MGMT_TO_SYS (i4RootHelloTime);

        /*Get Bridge Details */

        /* Bridge Priority */

        nmhGetFsMIMstMstiBridgePriority (i4ContextId, i4NextInst, &i4Priority);
        /*Bridge Address */

        AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
        PrintMacAddress (MacAddress, au1BrgAddr);

        /*Max Age */

        nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

        /*Forward Delay */

        nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

        /*Hello Time */

        i4HelloTime = AST_INIT_VAL;
        nmhGetFsMIMstCistBridgeHelloTime (i4ContextId, &i4HelloTime);
        i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

        /*Displays the Root Id,Root Timers,RootCost,RootPort */
        CliPrintf (CliHandle, "\r\nMST%02d\r\n", i4NextInst);

        u2RootPrio = AstGetBrgPrioFromBrgId (RegRoot);

        /*Root Bridge Priority */
        CliPrintf (CliHandle, "\r\nRoot Id         Priority   %d\r\n",
                   u2RootPrio);

        /* First 2 bytes contain the bridge priority and the
         *      * next 6 bytes contain the Bridge MAC*/

        /*RootBridge Address */

        AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
        PrintMacAddress ((RegRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                         au1RootBrgAddr);
        CliPrintf (CliHandle, "\t\tAddress    %s\r\n ", au1RootBrgAddr);

        if (AST_MEMCMP
            (RegRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE, RootMacAddress,
             AST_MAC_ADDR_SIZE) == 0)
        {
            CliPrintf (CliHandle, "\t\tThis bridge is the root \r\n");

        }

        CliPrintf (CliHandle, "\t\tMax age %d sec %d cs,",
                   AST_PROT_TO_BPDU_SEC (i4RootMaxAge),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4RootMaxAge));
        CliPrintf (CliHandle, " forward delay %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4RootFwdDelay),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4RootFwdDelay));
        CliPrintf (CliHandle, "\t\tHello Time is %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4RootHelloTime),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4RootHelloTime));

        /* Bridge Details */
        CliPrintf (CliHandle, "Bridge Id       Priority  %d\r\n", i4Priority);
        CliPrintf (CliHandle, "%-16sAddress  %s\r\n", " ", au1BrgAddr);
        CliPrintf (CliHandle, "%-16sMax age is %d sec %d cs,", " ",
                   AST_PROT_TO_BPDU_SEC (i4MaxAge),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
        CliPrintf (CliHandle, " forward delay is %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
        CliPrintf (CliHandle, "\t\tHello Time is %d sec %d cs\r\n",
                   AST_PROT_TO_BPDU_SEC (i4HelloTime),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

        CliPrintf (CliHandle, "%-18s%-13s%-13s%-9s%-7s%-9s\r\n",
                   "Name", "Role", "State", "Cost", "Prio", "type");
        CliPrintf (CliHandle, "%-18s%-13s%-13s%-9s%-7s%-9s\r\n",
                   "----", "----", "-----", "----", "----", "------");

        i4PortOutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, 0, &i4NextPortMsti,
                                          0, &i4NextPortInstance);

        while (i4PortOutCome != RST_FAILURE)
        {

            /* display only if the matching instance is 
             * returned in this FsMstMstiPortTable as index
             */
            if (i4NextInst == i4NextPortInstance)
            {

                AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPortMsti,
                                           &u1OperStatus);

                if (u1OperStatus != CFA_IF_DOWN)
                {
                    if (u4Active == STP_ACTIVE_PORTS)
                    {
                        u1Flag = 0;
                        nmhGetFsMIMstMstiCurrentPortRole (i4NextPortMsti,
                                                          i4NextInst,
                                                          &i4PortRole);
                        nmhGetFsMIMstMstiPortState (i4NextPortMsti, i4NextInst,
                                                    &i4PortState);
                        if (((i4PortState == AST_PORT_STATE_LEARNING)
                             || (i4PortState == AST_PORT_STATE_FORWARDING))
                            && (i4PortRole != MST_PORT_ROLE_DISABLED))
                        {
                            /* Only active ports need to be displayed if the 
                             * command executed  is "show spanning-tree active .
                             * This flag is used to qualify if a certain port's
                             * information needs to be displayed. */

                            u1Flag = 1;
                        }
                    }

                    else
                    {
                        u1Flag = 1;
                    }

                    if (u1Flag)
                    {
                        MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        AstCfaCliGetIfName ((UINT4) i4NextPortMsti,
                                            (INT1 *) au1IntfName);

                        if (MST_TRUE == MstIsPortMemberOfInst (i4NextPortMsti,
                                                               i4NextPortInstance))
                        {

                            /*PortRole */
                            nmhGetFsMIMstMstiCurrentPortRole (i4NextPortMsti,
                                                              i4NextInst,
                                                              &i4PortRole);

                            /*Port State */
                            nmhGetFsMIMstMstiPortState (i4NextPortMsti,
                                                        i4NextInst,
                                                        &i4PortState);
                            AstGetHwPortStateStatus (i4NextPortMsti, i4NextInst,
                                                     &i4PortStateStatus);
                            /*Port Cost */
                            nmhGetFsMIMstMstiPortPathCost (i4NextPortMsti,
                                                           i4NextInst,
                                                           &i4PortCost);
                            /*PortPriority */
                            nmhGetFsMIMstMstiPortPriority (i4NextPortMsti,
                                                           i4NextInst,
                                                           &i4PortPriority);
                            /* Link Type */

                            nmhGetFsMIMstCistPortOperP2P (i4NextPortMsti,
                                                          &i4LinkType);

                            CliPrintf (CliHandle, "%-18s", au1IntfName);

                            StpCliDisplayPortRole (CliHandle, u4ContextId,
                                                   i4PortRole);
                            CliPrintf (CliHandle, "%-3s", " ");

                            StpCliDisplayPortState (CliHandle, i4PortState,
                                                    i4PortStateStatus);
                            CliPrintf (CliHandle, "%-3s", " ");

                            CliPrintf (CliHandle, "%-9d", i4PortCost);
                            CliPrintf (CliHandle, "%-7d", i4PortPriority);
                            if (i4LinkType == AST_SNMP_TRUE)
                            {
                                CliPrintf (CliHandle, "%-9s", "P2P");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-9s", "SharedLan");
                            }
                            MstGetInstPortLoopInconsistentState ((UINT2)
                                                                 i4NextPortMsti,
                                                                 (UINT2)
                                                                 i4NextInst,
                                                                 &i4LoopIncState);

                            nmhGetFsMIMstMstiPortRootInconsistentState
                                (i4NextPortMsti, i4NextInst, &i4RootIncState);

                            nmhGetFsMIMstPortBpduInconsistentState
                                (i4NextPortMsti, &i4BpduIncState);

                            if (i4RootIncState == MST_TRUE)
                            {
                                u4PagingStatus =
                                    CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                            }
                            else if (i4LoopIncState == MST_TRUE)
                            {
                                u4PagingStatus =
                                    CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                            }
                            else if (i4BpduIncState == MST_TRUE)
                            {
                                u4PagingStatus =
                                    CliPrintf (CliHandle,
                                               "(*bpduguard-Inc)\r\n");
                            }
                            else
                            {
                                u4PagingStatus = CliPrintf (CliHandle, "\r\n");
                            }

                        }
                        /* Get the Next Port from the Table */
                    }
                }
                u1Flag = 0;

                if (u4PagingStatus == CLI_FAILURE)
                {
                    break;
                    /* User Presses 'q' at the prompt and so quits */
                }
            }

            i4CurrentPortMsti = i4NextPortMsti;
            i4CurrentPortInstance = i4NextPortInstance;

            i4PortOutCome =
                AstGetNextMstiPortTableIndex (u4ContextId, i4CurrentPortMsti,
                                              &i4NextPortMsti,
                                              i4CurrentPortInstance,
                                              &i4NextPortInstance);
        }

        i4PrevInst = i4NextInst;
        i1InstOutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, i4PrevInst,
                                                   &i4NextInst);
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowRootBridgeInfo                           */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliShowRootBridgeInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4RootCost;
    INT4                i4RootPort;
    INT4                i4MaxAge;
    INT4                i4FwdDelay;
    INT4                i4HelloTime = AST_INIT_VAL;

    UINT2               u2RootPrio = 0;
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    tSNMP_OCTET_STRING_TYPE CistRoot;
    tAstMacAddr         MacAddress;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4ModuleStatus = 0;

    MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    CistRoot.pu1_OctetList = &au1RootBrgIdBuf[0];
    CistRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    nmhGetFsMIMstModuleStatus (i4ContextId, &i4ModuleStatus);

    AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
    nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);

    nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

    /* Root Cost */
    nmhGetFsMIMstCistRootCost (i4ContextId, &i4RootCost);

    /* Root Port */
    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);
    AstCfaCliGetIfName (i4RootPort, (INT1 *) au1IntfName);

    /*Root MaxAge */

    nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*Root HelloTime */

    nmhGetFsMIMstCistHelloTime (i4ContextId, &i4HelloTime);
    i4HelloTime = AST_MGMT_TO_SYS (i4HelloTime);

    u2RootPrio = AstGetBrgPrioFromBrgId (CistRoot);
    CliPrintf (CliHandle, "\rRoot Id         Priority   %d\r\n", u2RootPrio);

    /* First 2 bytes contain the bridge priority and the
     * next 6 bytes contain the Bridge MAC*/

    AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
    PrintMacAddress ((CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                     au1RootBrgAddr);
    CliPrintf (CliHandle, "\t\tAddress    %s\r\n ", au1RootBrgAddr);

    CliPrintf (CliHandle, "\t\tCost       %d\r\n", i4RootCost);
    CliPrintf (CliHandle, "\t\tPort       %d [%s]\r\n", i4RootPort,
               au1IntfName);

    if (AST_MEMCMP
        ((CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE), MacAddress,
         AST_MAC_ADDR_SIZE) == 0)
    {
        CliPrintf (CliHandle, "\t\tThis bridge is the root \r\n");

    }

    CliPrintf (CliHandle, "\t\tMax age %d sec %d cs,",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, " forward delay %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "\t\tHello Time %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4HelloTime),
               AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliShowRootBridgeTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the root bridge        */
/*                        Information in the TableFormat                     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
MstCliShowRootBridgeTable (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT1               au1BrgId[AST_BRG_ID_DIS_LEN];
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE DesigRoot;
    INT1                i1OutCome;
    INT4                i4NextInst;
    INT4                i4PrevInst;
    INT4                i4FwdDelay;
    INT4                i4RootCost;
    INT4                i4RootPort;
    INT4                i4MaxAge;
    INT4                i4Intf;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    AST_MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    DesigRoot.pu1_OctetList = &au1BrgIdBuf[0];
    DesigRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    /*Root Bridge Id */
    nmhGetFsMIMstCistRoot (i4ContextId, &DesigRoot);

    /*RootCost */

    nmhGetFsMIMstCistRegionalRootCost (i4ContextId, &i4RootCost);

    /*Root MaxAge */

    nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    /*Root ForwardTime */

    nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /*RootPort */

    nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);

    i4Intf = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) au1IntfName);

    /*Displays the Root Id,Root Timers,RootCost,RootPort  in the table format */

    CliPrintf (CliHandle,
               "\r\n%s %s   %s %s %s %s\r\n",
               "Instance    ", "Root ID                ", "RootCost  ",
               "MaxAge   ", "FwdDly  ", "   RootPort");
    CliPrintf (CliHandle, "\r\n%s %s   %s %s %s %s\r\n", "------------",
               "-----------------------", "--------", "-----------",
               "--------", "   --------");

    CliPrintf (CliHandle, "%s", "MST00       ");

    AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
    CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length, au1BrgId,
                   AST_BRG_ID_DIS_LEN);
    CliPrintf (CliHandle, " %s", au1BrgId);

    CliPrintf (CliHandle, "   %-9d", i4RootCost);
    CliPrintf (CliHandle, " %d sec %d cs",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, " %d sec %d cs",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

    /* Get the Interface Name for the Port */
    if (i4Intf == CLI_FAILURE)
    {
        CliPrintf (CliHandle, " %%PortName for index %d  is not found \r\n",
                   i4Intf);
    }
    CliPrintf (CliHandle, " %s\r\n", au1IntfName);

    i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId, 0,
                                                       &i4NextInst);
    /* To get the instance */
    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        /*Root Bridge Id */
        AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
        nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                             &DesigRoot);

        nmhGetFsMIMstMstiRootCost (i4ContextId, i4NextInst, &i4RootCost);
        /*Root MaxAge */

        nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

        /*Root ForwardTime */

        nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

        /*RootPort */

        nmhGetFsMIMstMstiRootPort (i4ContextId, i4NextInst, &i4RootPort);
        i4Intf = AstCfaCliGetIfName ((UINT4) (i4RootPort),
                                     (INT1 *) au1IntfName);

        CliPrintf (CliHandle, "%-3s%02d     ", "MST", i4NextInst);

        AST_MEMSET (au1BrgId, 0, AST_BRG_ID_DIS_LEN);
        CliOctetToStr (DesigRoot.pu1_OctetList, DesigRoot.i4_Length,
                       au1BrgId, AST_BRG_ID_DIS_LEN);
        CliPrintf (CliHandle, "   %s", au1BrgId);

        CliPrintf (CliHandle, "   %d        ", i4RootCost);

        CliPrintf (CliHandle, " %d sec %d cs",
                   AST_PROT_TO_BPDU_SEC (i4MaxAge),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));

        CliPrintf (CliHandle, " %d sec %d cs",
                   AST_PROT_TO_BPDU_SEC (i4FwdDelay),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));

        /* Get the Interface Name for the Port */
        if (i4Intf == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       " %%PortName for index %d  is not found \r\n", i4Intf);
        }

        CliPrintf (CliHandle, " %s\r\n", au1IntfName);

        i4PrevInst = i4NextInst;
        i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId,
                                                           i4PrevInst,
                                                           &i4NextInst);
        CliFlush (CliHandle);
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliDisplayBlockedPorts                          */
/*                                                                           */
/*     DESCRIPTION      : This function  displays all the BlockedPorts       */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliDisplayBlockedPorts (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tCfaIfInfo          CfaIfInfo;
    INT4                i4CurrentPort = 0, i4NextPort;
    INT4                i4OutCome;
    INT4                i4PortOutCome;
    UINT1               i1InstOutCome;
    INT4                i4NextInst;
    INT4                i4PrevInst;
    INT4                i4CistPortState;
    INT4                i4MstiPortState;

    UINT4               u4BlockedPort = 0;
    UINT1               u1IsFirstPort = TRUE;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4NextPortInstance;
    INT4                i4CurrPortInstance;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    MEMSET (&CfaIfInfo, 0, sizeof (CfaIfInfo));

    CliPrintf (CliHandle, "\r\nBlocked Interfaces List for MST00\r\n");

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while (i4OutCome != RST_FAILURE)
    {

        nmhGetFsMIMstCistPortState (i4NextPort, &i4CistPortState);
        if (AstCfaGetIfInfo ((UINT4) i4NextPort, &CfaIfInfo) != CFA_SUCCESS)
        {
            AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                          "SNMP: Cannot Get CFA Information for Port %s\n",
                          AST_GET_IFINDEX_STR (i4NextPort));
        }
        /*Ignore ports which are admin down while displaying blocked ports */
        if ((i4CistPortState == AST_PORT_STATE_DISCARDING) &&
            (CfaIfInfo.u1IfAdminStatus != AST_PORT_ADMIN_DOWN))
        {
            MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);

            AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

            if (u1IsFirstPort == TRUE)
            {
                CliPrintf (CliHandle, "%s", au1IntfName);
                u1IsFirstPort = FALSE;
            }
            else
            {
                CliPrintf (CliHandle, ",%s", au1IntfName);
            }

            u4BlockedPort++;
        }
        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);
    }

    CliPrintf (CliHandle, "\b");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "The Number of Blocked Ports in the system for MST00 is :%d\r\n",
               u4BlockedPort);

    i1InstOutCome =
        nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId, &i4NextContextId, 0,
                                               &i4NextInst);

    while ((i1InstOutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        u4BlockedPort = 0;

        CliPrintf (CliHandle, "\r\nBlocked Interfaces List for MST%02d\r\n",
                   i4NextInst);

        i4PortOutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, 0, &i4NextPort,
                                          0, &i4NextPortInstance);

        while (i4PortOutCome != RST_FAILURE)
        {

            if (i4NextPortInstance == i4NextInst)
            {
                /* Port Role */
                nmhGetFsMIMstMstiPortState (i4NextPort, i4NextInst,
                                            &i4MstiPortState);

                if (i4MstiPortState == AST_PORT_STATE_DISCARDING)
                {
                    MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                    AstCfaCliGetIfName ((UINT4) i4NextPort,
                                        (INT1 *) au1IntfName);
                    CliPrintf (CliHandle, "%s,", au1IntfName);

                    u4BlockedPort++;

                }
            }

            i4CurrentPort = i4NextPort;
            i4CurrPortInstance = i4NextPortInstance;

            i4PortOutCome = AstGetNextMstiPortTableIndex
                (u4ContextId, i4CurrentPort, &i4NextPort, i4CurrPortInstance,
                 &i4NextPortInstance);
        }

        CliPrintf (CliHandle, "\b");
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle,
                   "The Number of Blocked Ports in the system for MST%02d is :%d\r\n",
                   i4NextInst, u4BlockedPort);

        i4PrevInst = i4NextInst;
        i1InstOutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, i4PrevInst,
                                                   &i4NextInst);
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMst                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays info for an mst             */
/*                         configuration                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MstShowSpanningTreeMst (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4Instance, UINT1 u1Detail)
{
    tAstMacAddr         MacAddress;
    INT4                i4NextPort;
    INT4                i4RootPort;
    INT4                i4RootCost;
    INT4                i4RegionalRootCost;
    INT4                i4Intf = CLI_FAILURE;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4PathCost;
    INT4                i4Priority;
    INT4                i4PortPriority;
    UINT1               u1OperStatus;
    INT4                i4VlanMapping;
    INT4                i4LinkType;
    INT4                i4CurrentPort;
    INT4                i4NextPortInst;
    INT4                i4CurrentPortInst;
    INT4                i4NextInst;
    INT4                i4CurrentInst;
    INT4                i4CommaCount = 0;
    INT4                i4FwdDelay;
    INT4                i4MaxAge;
    INT4                i4MaxHops;
    INT4                i4OutCome;
    INT4                i4PortOutCome;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1IfName = NULL;
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RegionalBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];

    tAstMacAddr         RootBrgAddr;
    tAstMacAddr         RegionalBrgAddr;

    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RegionalBrgAddr[AST_BRG_ADDR_DIS_LEN];

    UINT1               u1Flag;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE CistRoot;
    tSNMP_OCTET_STRING_TYPE CistRegionalRoot;

    UINT2               u2Prio;
    UINT2               u2RegionalPrio;
    UINT4               u4Index;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT1                i1OutCome;

    INT4                i4LoopIncState = AST_FALSE;
    INT4                i4RootIncState = AST_FALSE;
    INT4                i4BpduIncState = AST_FALSE;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, " %% Bridge is not in MST mode\r\n");
        return CLI_FAILURE;
    }

    AstReleaseContext ();

    MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    CistRoot.pu1_OctetList = &au1BrgIdBuf[0];
    CistRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RegionalBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    CistRegionalRoot.pu1_OctetList = &au1RegionalBrgIdBuf[0];
    CistRegionalRoot.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    nmhGetFsMIMstMaxHopCount (i4ContextId, &i4MaxHops);
    i4MaxHops = AST_MGMT_TO_SYS (i4MaxHops);

    if (u4Instance == 0)
    {
        u1Flag = 0;
        /* Bridge address */
        AST_MEMSET (au1BrgAddr, 0, sizeof (au1BrgAddr));
        AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
        nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
        nmhGetFsMIMstCistBridgePriority (i4ContextId, &i4Priority);

        /* RootBridge ID is obtained */
        nmhGetFsMIMstCistRoot (i4ContextId, &CistRoot);

        nmhGetFsMIMstCistRootPort (i4ContextId, &i4RootPort);

        pu1IfName = &au1IntfName[0];
        AST_MEMSET (au1IntfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        i4Intf = AstCfaCliGetIfName ((UINT4) (i4RootPort), (INT1 *) pu1IfName);

        nmhGetFsMIMstCistRootCost (i4ContextId, &i4RootCost);

        nmhGetFsMIMstCistRegionalRoot (i4ContextId, &CistRegionalRoot);

        AST_MEMCPY (RegionalBrgAddr, (CistRegionalRoot.pu1_OctetList +
                                      AST_BRG_PRIORITY_SIZE),
                    AST_MAC_ADDR_SIZE);

        nmhGetFsMIMstCistRegionalRootCost (i4ContextId, &i4RegionalRootCost);

        CliPrintf (CliHandle, "\r\n ## MST%02d  ", MST_CIST_CONTEXT);
        CliPrintf (CliHandle, "\r\n");

        PrintMacAddress (MacAddress, au1BrgAddr);
        CliPrintf (CliHandle, "Bridge     Address %s ", au1BrgAddr);
        CliPrintf (CliHandle, "Priority %d\r\n", i4Priority);
        /*RootBridge Priority */

        u2Prio = AstGetBrgPrioFromBrgId (CistRoot);

        /* First 2 bytes contain the bridge priority and the
         * next 6 bytes contain the Bridge MAC*/

        AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
        PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1RootBrgAddr);

        CliPrintf (CliHandle, "Root       Address %s ", au1RootBrgAddr);
        CliPrintf (CliHandle, "Priority %d\r\n", u2Prio);

        /* To check if this bridge is root bridge */
        AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
        PrintMacAddress (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1RootBrgAddr);

        AST_MEMCPY (RootBrgAddr,
                    (CistRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE),
                    AST_MAC_ADDR_SIZE);

        if (AST_MEMCMP (RootBrgAddr, MacAddress, AST_MAC_ADDR_SIZE) == 0)
        {
            CliPrintf (CliHandle, "           We are the Root for CST\r\n");
        }

        if (i4Intf == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%PortName for index %d  is not found \r\n",
                       i4Intf);
        }
        CliPrintf (CliHandle, "           Port %-9s, ", pu1IfName);

        CliPrintf (CliHandle, " path cost %d\r\n", i4RootCost);
        u2RegionalPrio = AstGetBrgPrioFromBrgId (CistRegionalRoot);

        AST_MEMSET (au1RegionalBrgAddr, 0, sizeof (au1RegionalBrgAddr));
        PrintMacAddress (CistRegionalRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1RegionalBrgAddr);

        CliPrintf (CliHandle, "IST Root   Address %s ", au1RegionalBrgAddr);
        CliPrintf (CliHandle, "Priority  %d \r\n", u2RegionalPrio);
        CliPrintf (CliHandle, "           Path cost %d\r\n",
                   i4RegionalRootCost);

        /* configured values of forward delay, max-age,HelloTime and max-hops */
        nmhGetFsMIMstCistBridgeMaxAge (i4ContextId, &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

        /* forward delay */
        nmhGetFsMIMstCistBridgeForwardDelay (i4ContextId, &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

        MstDisplayConfiguredBridgeValues (CliHandle, i4MaxAge, i4FwdDelay,
                                          i4MaxHops);

        /*opreational value of  max age */
        i4MaxAge = 0;
        nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
        i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

        /* forward delay */
        i4FwdDelay = 0;
        nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
        i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

        MstDisplayOperBridgeValues (CliHandle, i4MaxAge, i4FwdDelay);

        if (u1Detail == 0)
        {

            CliPrintf (CliHandle,
                       "\r\n%-10s%-13s%-13s%-9s%-9s%-8s\r\n", "Interface",
                       "Role", "Sts", "Cost", "Prio.Nbr", "Type");
            CliPrintf (CliHandle, "%-10s%-13s%-13s%-9s%-9s%-8s\r\n",
                       "---------", "----", "---", "----", "--------", "----");

            /* Cist Port Info */

            i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);
            while (i4OutCome != RST_FAILURE)
            {
                /* get the interface name for this port number */

                AstL2IwfGetPortOperStatus (STP_MODULE,
                                           (UINT4) i4NextPort, &u1OperStatus);

                if (u1OperStatus != CFA_IF_DOWN)
                {
                    i4Intf =
                        AstCfaCliGetIfName ((UINT4) i4NextPort,
                                            (INT1 *) au1IntfName);

                    if (i4Intf == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   " Port name for index %d is not found\r\n",
                                   i4NextPort);

                        return CLI_FAILURE;
                    }
                    /* port role */
                    nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);
                    /* port state */
                    nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
                    AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT,
                                             &i4PortStateStatus);
                    /* cost */
                    nmhGetFsMIMstCistPortPathCost (i4NextPort, &i4PathCost);
                    /* Prio.Nbr */
                    nmhGetFsMIMstCistPortPriority (i4NextPort, &i4PortPriority);
                    /* Port Type */
                    nmhGetFsMIMstCistPortOperP2P (i4NextPort, &i4LinkType);

                    CliPrintf (CliHandle, "%-10s", au1IntfName);
                    StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                    CliPrintf (CliHandle, "%-3s", " ");
                    StpCliDisplayPortState (CliHandle, i4PortState,
                                            i4PortStateStatus);
                    CliPrintf (CliHandle, "%-3s", " ");
                    CliPrintf (CliHandle, "%-9d", i4PathCost);
                    CliPrintf (CliHandle, "%3d.%-4d   ", i4PortPriority,
                               i4NextPort);
                    if (i4LinkType == AST_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "%-20s", "P2P");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-20s", "SharedLan");
                    }

                    MstGetInstPortLoopInconsistentState ((UINT2) i4NextPort,
                                                         (UINT2)
                                                         MST_CIST_CONTEXT,
                                                         &i4LoopIncState);
                    nmhGetFsMIMstCistPortBpduGuard (i4NextPort,
                                                    &i4BpduIncState);

                    nmhGetFsMIMstCistPortRootInconsistentState (i4NextPort,
                                                                &i4RootIncState);

                    if (i4LoopIncState == MST_TRUE)
                    {
                        CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                    }
                    else if (i4RootIncState == MST_TRUE)
                    {
                        CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                    }
                    else if (i4BpduIncState == MST_TRUE)
                    {
                        CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
                    }

                    else
                    {
                        CliPrintf (CliHandle, "\r\n");
                    }
                    u4PagingStatus = CliPrintf (CliHandle, "\r");

                    if (u4PagingStatus == CLI_FAILURE)
                    {
                        /* User Presses 'q' at the prompt, so break! */

                        break;
                    }
                }
                i4CurrentPort = i4NextPort;
                i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                                     &i4NextPort);
            }
        }
        else
        {
            u4Index = 0;
            MstShowSpanningTreeMstInstDetail (CliHandle, u4ContextId,
                                              u4Instance, u4Index);
            /*Display interface Detail for CIST ports */
        }

        /* display the information for other instances(from 1 to 64) */
        /* walk through msti bridge table */

        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, 0,
                                                   &i4NextInst);
        /* To get the instance */
    }
    else
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");

            return (CLI_FAILURE);
        }
        i4NextInst = u4Instance;
        i1OutCome = SNMP_SUCCESS;
        u1Flag = 1;

        i4NextContextId = i4ContextId;

        /* This flag ,at value 1, indicates that the MST instance id 
         * has been specified by the user */
    }

    i4CommaCount = 1;

    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        CliPrintf (CliHandle, "\r\n ## MST%02d  \r\n", i4NextInst);
        CliPrintf (CliHandle, "Vlans mapped:    ");

        if (MstMiValidateInstanceEntry (u4ContextId, i4NextInst) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n MST%02d no longer valid\r\n",
                       i4NextInst);

            i4CurrentInst = i4NextInst;
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId,
                                                       i4CurrentInst,
                                                       &i4NextInst);
            continue;
        }

        /* vlans mapping */
        i4VlanMapping =
            MstFillInstanceVlanMapping (CliHandle, u4ContextId, i4NextInst,
                                        &i4CommaCount);

        if (i4VlanMapping == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (MstMiValidateInstanceEntry (u4ContextId, i4NextInst) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n MST%02d no longer valid\r\n",
                       i4NextInst);

            i4CurrentInst = i4NextInst;
            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId,
                                                       i4CurrentInst,
                                                       &i4NextInst);
            continue;
        }

        /* bridge address */
        AST_MEMSET (&MacAddress, 0, sizeof (tAstMacAddr));
        AST_MEMSET (au1BrgAddr, AST_INIT_VAL, sizeof (au1BrgAddr));
        nmhGetFsMIMstBrgAddress (i4ContextId, &MacAddress);
        PrintMacAddress (MacAddress, au1BrgAddr);
        nmhGetFsMIMstMstiBridgePriority (i4ContextId, i4NextInst, &i4Priority);

        MEMSET (au1RegionalBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
        nmhGetFsMIMstMstiBridgeRegionalRoot (i4ContextId, i4NextInst,
                                             &CistRegionalRoot);

        /*RootBridge Address */

        u2Prio = AstGetBrgPrioFromBrgId (CistRegionalRoot);
        AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
        PrintMacAddress (CistRegionalRoot.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                         au1RootBrgAddr);

        /* To check if this bridge is root bridge */

        AST_MEMCPY (RootBrgAddr, (CistRegionalRoot.pu1_OctetList +
                                  AST_BRG_PRIORITY_SIZE), AST_MAC_ADDR_SIZE);

        /* To check if this bridge is root bridge */

        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Bridge     Address %s", au1BrgAddr);
        CliPrintf (CliHandle, " Priority %d\r\n", i4Priority);
        CliPrintf (CliHandle, "Root       Address %s ", au1RootBrgAddr);
        CliPrintf (CliHandle, "Priority %d \r\n", u2Prio);

        if (AST_MEMCMP (RootBrgAddr, MacAddress, AST_MAC_ADDR_SIZE) == 0)
        {
            CliPrintf (CliHandle,
                       "Root       this switch for MST%02d\r\n", i4NextInst);
        }

        if (u1Detail == 0)
        {

            CliPrintf (CliHandle,
                       "\r\n%-10s%-13s%-13s%-9s%-9s%-8s\r\n", "Interface",
                       "Role", "Sts", "Cost", "Prio.Nbr", "Type");
            CliPrintf (CliHandle, "%-10s%-13s%-13s%-9s%-9s%-8s\r\n",
                       "---------", "----", "---", "----", "--------", "----");

            /* walk through msti port table */
            i4PortOutCome = AstGetNextMstiPortTableIndex (u4ContextId, 0,
                                                          &i4NextPort,
                                                          0, &i4NextPortInst);
            while (i4PortOutCome != RST_FAILURE)
            {
                if (i4NextInst == i4NextPortInst)
                {
                    AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                               &u1OperStatus);

                    if (u1OperStatus != CFA_IF_DOWN)
                    {
                        /* get the interface name for this port number */

                        i4Intf =
                            AstCfaCliGetIfName ((UINT4) i4NextPort,
                                                (INT1 *) au1IntfName);

                        if (i4Intf == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       " Port name for index %d is not found\r\n",
                                       i4NextPort);

                            return CLI_FAILURE;
                        }
                        if (MST_TRUE == MstIsPortMemberOfInst (i4NextPort,
                                                               i4NextInst))
                        {

                            /* port role */
                            nmhGetFsMIMstMstiCurrentPortRole (i4NextPort,
                                                              i4NextInst,
                                                              &i4PortRole);
                            /* port state */
                            nmhGetFsMIMstMstiPortState (i4NextPort, i4NextInst,
                                                        &i4PortState);
                            AstGetHwPortStateStatus (i4NextPort, i4NextInst,
                                                     &i4PortStateStatus);
                            /* cost */
                            nmhGetFsMIMstMstiPortPathCost (i4NextPort,
                                                           i4NextInst,
                                                           &i4PathCost);

                            /* Prio.Nbr */
                            nmhGetFsMIMstMstiPortPriority (i4NextPort,
                                                           i4NextInst,
                                                           &i4PortPriority);
                            /* Port Type */
                            nmhGetFsMIMstCistPortOperP2P (i4NextPort,
                                                          &i4LinkType);

                            CliPrintf (CliHandle, "%-10s", au1IntfName);

                            StpCliDisplayPortRole (CliHandle, u4ContextId,
                                                   i4PortRole);
                            CliPrintf (CliHandle, "%-3s", " ");

                            StpCliDisplayPortState (CliHandle, i4PortState,
                                                    i4PortStateStatus);
                            CliPrintf (CliHandle, "%-3s", " ");

                            CliPrintf (CliHandle, "%-9d", i4PathCost);
                            CliPrintf (CliHandle, "%3d.%-4d  ", i4PortPriority,
                                       i4NextPort);

                            /* Port Type */
                            if (i4LinkType == AST_SNMP_TRUE)
                            {
                                CliPrintf (CliHandle, "%-20s", "P2P");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-20s", "SharedLan");
                            }

                            MstGetInstPortLoopInconsistentState ((UINT2)
                                                                 i4NextPort,
                                                                 (UINT2)
                                                                 i4NextInst,
                                                                 &i4LoopIncState);
                            nmhGetFsMIMstMstiPortRootInconsistentState
                                (i4NextPort, i4NextInst, &i4RootIncState);
                            nmhGetFsMIMstPortBpduInconsistentState (i4NextPort,
                                                                    &i4BpduIncState);

                            if (i4LoopIncState == MST_TRUE)
                            {
                                CliPrintf (CliHandle, "(*Loop-Inc)\r\n");
                            }
                            else if (i4RootIncState == MST_TRUE)
                            {
                                CliPrintf (CliHandle, "(*Root-Inc)\r\n");
                            }
                            else if (i4BpduIncState == MST_TRUE)
                            {
                                CliPrintf (CliHandle, "(*bpduguard-Inc)\r\n");
                            }

                            else
                            {
                                CliPrintf (CliHandle, "\r\n");
                            }
                        }
                    }
                }
                i4CurrentPort = i4NextPort;
                i4CurrentPortInst = i4NextPortInst;
                i4PortOutCome =
                    AstGetNextMstiPortTableIndex (u4ContextId, i4CurrentPort,
                                                  &i4NextPort,
                                                  i4CurrentPortInst,
                                                  &i4NextPortInst);
            }
        }
        else
        {
            u4Index = 0;
            MstShowSpanningTreeMstInstDetail (CliHandle, u4ContextId,
                                              i4NextInst, u4Index);
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentInst = i4NextInst;    /* Instance id stored in temp var */
        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId,
                                                   i4CurrentInst, &i4NextInst);

        CliFlush (CliHandle);
        if (u1Flag == 1)
        {
            break;
        }

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstDisplayConfiguredBridgeValues                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays the configured values of    */
/*                        forward delay ,max-age and max-hops for a bridge.  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
VOID
MstDisplayConfiguredBridgeValues (tCliHandle CliHandle, INT4 i4MaxAge,
                                  INT4 i4FwdDelay, INT4 i4MaxHops)
{

    CliPrintf (CliHandle, "Configured Forward delay %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "Max age %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    CliPrintf (CliHandle, "Max hops %d\r\n", AST_PROT_TO_BPDU_SEC (i4MaxHops));

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstDisplayOperBridgeValues                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the configured values of    */
/*                        forward delay ,max-age and max-hops for a bridge.  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
VOID
MstDisplayOperBridgeValues (tCliHandle CliHandle,
                            INT4 i4MaxAge, INT4 i4FwdDelay)
{
    CliPrintf (CliHandle, "Operational Forward delay %d sec %d cs, ",
               AST_PROT_TO_BPDU_SEC (i4FwdDelay),
               AST_PROT_TO_BPDU_CENTI_SEC (i4FwdDelay));
    CliPrintf (CliHandle, "Max age %d sec %d cs\r\n",
               AST_PROT_TO_BPDU_SEC (i4MaxAge),
               AST_PROT_TO_BPDU_CENTI_SEC (i4MaxAge));
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstFillInstanceVlanMapping                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays instance to vlan mapping.   */
/*                                                                           */
/*     INPUT            : CliHandle  - Handle to the cli context             */
/*                        u4Instance - Instance                              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MstFillInstanceVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Instance, INT4 *pi4CommaCount)
{

    tSNMP_OCTET_STRING_TYPE VlanMap1;
    tSNMP_OCTET_STRING_TYPE VlanMap2;
    tSNMP_OCTET_STRING_TYPE VlanMap3;
    tSNMP_OCTET_STRING_TYPE VlanMap4;
    tSNMP_OCTET_STRING_TYPE *pMstVlanListAll = NULL;

    tAstVlanList        VlanList1;
    tAstVlanList        VlanList2;
    tAstVlanList        VlanList3;
    tAstVlanList        VlanList4;

    tAstBoolean         bFlag;

    tMstVlanMap        *pVlanMapBuf1 = NULL;
    tMstVlanMap        *pVlanMapBuf2 = NULL;
    tMstVlanMap        *pVlanMapBuf3 = NULL;
    tMstVlanMap        *pVlanMapBuf4 = NULL;

    /* allocating memory for octet strings */
    INT4                i4ContextId = (INT4) u4ContextId;

    if ((pMstVlanListAll = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        return CLI_FAILURE;
    }
    pMstVlanListAll->i4_Length = MST_VLAN_LIST_SIZE;

    if (AST_ALLOC_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return CLI_FAILURE;
    }

    if (AST_ALLOC_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf2) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return CLI_FAILURE;
    }
    if (AST_ALLOC_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf3) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf2)
            AST_INCR_MEMORY_FAILURE_COUNT ();
        return CLI_FAILURE;
    }
    if (AST_ALLOC_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf4) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf2);
        AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf3);
        AST_INCR_MEMORY_FAILURE_COUNT ();
        return CLI_FAILURE;
    }
    MEMSET (pVlanMapBuf1->au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    VlanMap1.pu1_OctetList = &(pVlanMapBuf1->au1VlanMapBuf[0]);
    VlanMap1.i4_Length = MST_VLANMAP_LIST_SIZE;

    MEMSET (pVlanMapBuf2->au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    VlanMap2.pu1_OctetList = &(pVlanMapBuf2->au1VlanMapBuf[0]);
    VlanMap2.i4_Length = MST_VLANMAP_LIST_SIZE;

    MEMSET (pVlanMapBuf3->au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    VlanMap3.pu1_OctetList = &(pVlanMapBuf3->au1VlanMapBuf[0]);
    VlanMap3.i4_Length = MST_VLANMAP_LIST_SIZE;

    MEMSET (pVlanMapBuf4->au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    VlanMap4.pu1_OctetList = &(pVlanMapBuf4->au1VlanMapBuf[0]);
    VlanMap4.i4_Length = MST_VLANMAP_LIST_SIZE;

    bFlag = MST_FALSE;

    if (u4Instance == MST_CIST_CONTEXT)
    {
        if (AstSelectContext (u4ContextId) == RST_FAILURE)
        {
            AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1);
            AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf2);
            AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf3);
            AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf4);
            free_octetstring (pMstVlanListAll);
            return CLI_FAILURE;
        }

        /* Vlan Mapped 1K */
        AST_MEMSET (VlanList1, 0, MST_VLANMAP_LIST_SIZE);
        MstGetVlanList ((UINT2) u4Instance, &VlanList1[0],
                        MST_VLANS_INSTANCE_1K);

        /* Vlan Mapped2k */
        AST_MEMSET (VlanList2, 0, MST_VLANMAP_LIST_SIZE);
        MstGetVlanList ((UINT2) u4Instance, &VlanList2[0],
                        MST_VLANS_INSTANCE_2K);
        /* Vlan Mapped3k */
        AST_MEMSET (VlanList3, 0, MST_VLANMAP_LIST_SIZE);
        MstGetVlanList ((UINT2) u4Instance, &VlanList3[0],
                        MST_VLANS_INSTANCE_3K);
        /* Vlan Mapped4k */
        AST_MEMSET (VlanList4, 0, MST_VLANMAP_LIST_SIZE);
        MstGetVlanList ((UINT2) u4Instance, &VlanList4[0],
                        MST_VLANS_INSTANCE_4K);
        /* Vlan Mapped8k */
        AST_MEMSET (pMstVlanListAll->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        MstGetVfiVlanList ((UINT2) u4Instance, pMstVlanListAll->pu1_OctetList);

        AstReleaseContext ();

    }
    else
    {

        /* Msti Instance Vlan Mapped */
        AST_MEMSET (VlanMap1.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped (i4ContextId, u4Instance, &VlanMap1);
        /* Msti Instance Vlan Mapped2k */
        AST_MEMSET (VlanMap2.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped2k (i4ContextId, u4Instance, &VlanMap2);
        /* Msti Instance Vlan Mapped3k */
        AST_MEMSET (VlanMap3.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped3k (i4ContextId, u4Instance, &VlanMap3);
        /* Msti Instance Vlan Mapped4k */
        AST_MEMSET (VlanMap4.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped4k (i4ContextId, u4Instance, &VlanMap4);
        /* Msti Instance Vlan Mapped8k */
        AST_MEMSET (pMstVlanListAll->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        nmhGetFsMIMstSetVlanList (i4ContextId, u4Instance, pMstVlanListAll);
    }

    if (u4Instance == MST_CIST_CONTEXT)
    {

        bFlag = MstDisplayOctetToVlan (CliHandle, &VlanList1[0],
                                       MST_VLANMAP_LIST_SIZE,
                                       MST_VLANS_INSTANCE_1K, bFlag,
                                       pi4CommaCount);

        bFlag = MstDisplayOctetToVlan (CliHandle, &VlanList2[0],
                                       MST_VLANMAP_LIST_SIZE,
                                       MST_VLANS_INSTANCE_2K, bFlag,
                                       pi4CommaCount);

        bFlag =
            MstDisplayOctetToVlan (CliHandle, &VlanList3[0],
                                   MST_VLANMAP_LIST_SIZE, MST_VLANS_INSTANCE_3K,
                                   bFlag, pi4CommaCount);

        bFlag =
            MstDisplayOctetToVlan (CliHandle, &VlanList4[0],
                                   MST_VLANMAP_LIST_SIZE, MST_VLANS_INSTANCE_4K,
                                   bFlag, pi4CommaCount);
        bFlag =
            MstDisplayOctetToVlan (CliHandle, pMstVlanListAll->pu1_OctetList,
                                   MST_VLAN_LIST_SIZE, MST_VLANS_INSTANCE_8K,
                                   bFlag, pi4CommaCount);
    }
    else

    {

        bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap1.pu1_OctetList,
                                       VlanMap1.i4_Length,
                                       MST_VLANS_INSTANCE_1K, bFlag,
                                       pi4CommaCount);

        bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap2.pu1_OctetList,
                                       VlanMap2.i4_Length,
                                       MST_VLANS_INSTANCE_2K, bFlag,
                                       pi4CommaCount);

        bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap3.pu1_OctetList,
                                       VlanMap3.i4_Length,
                                       MST_VLANS_INSTANCE_3K, bFlag,
                                       pi4CommaCount);

        bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap4.pu1_OctetList,
                                       VlanMap4.i4_Length,
                                       MST_VLANS_INSTANCE_4K, bFlag,
                                       pi4CommaCount);
        bFlag =
            MstDisplayOctetToVlan (CliHandle, pMstVlanListAll->pu1_OctetList,
                                   pMstVlanListAll->i4_Length,
                                   MST_VLANS_INSTANCE_8K, bFlag, pi4CommaCount);
    }

    free_octetstring (pMstVlanListAll);
    AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf1);
    AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf2);
    AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf3);
    AST_RELEASE_MST_VLANMAP_MEM_BLOCK (pVlanMapBuf4);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMstConfig                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the mst configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MstShowSpanningTreeMstConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE VlanMap1;
    tSNMP_OCTET_STRING_TYPE VlanMap2;
    tSNMP_OCTET_STRING_TYPE VlanMap3;
    tSNMP_OCTET_STRING_TYPE VlanMap4;
    tSNMP_OCTET_STRING_TYPE *pMstVlanListAll = NULL;
    tSNMP_OCTET_STRING_TYPE *pNullBuf = NULL;
    INT4                i4NextContextId;

    tAstBoolean         bFlag;
    INT4                i4Version;
    INT4                i4CistContext = MST_CIST_CONTEXT;
    INT4                i4NextInst;
    INT4                i4CurrentInst;
    INT1                i1OutCome;

    UINT1               au1MstName[CLI_MSTP_MAX_NAME_BUFFER];
    UINT1               au1VlanMapBuf[MST_VLANMAP_LIST_SIZE];
    UINT1               au2VlanMapBuf[MST_VLANMAP_LIST_SIZE];
    UINT1               au3VlanMapBuf[MST_VLANMAP_LIST_SIZE];
    UINT1               au4VlanMapBuf[MST_VLANMAP_LIST_SIZE];
    UINT1               au1EmptyBuf[MST_VLANMAP_LIST_SIZE];

    INT4                i4MstMaxInstanceNumber = AST_INIT_VAL;
    INT4                i4CommaCount = 0;
    INT4                i4ContextId = (INT4) u4ContextId;

    if ((pMstVlanListAll = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        return (CLI_FAILURE);
    }
    pMstVlanListAll->i4_Length = MST_VLAN_LIST_SIZE;
    if ((pNullBuf = allocmem_octetstring (MST_VLAN_LIST_SIZE)) == NULL)
    {
        free_octetstring (pMstVlanListAll);
        return (CLI_FAILURE);
    }
    pNullBuf->i4_Length = MST_VLAN_LIST_SIZE;

    MEMSET (au1MstName, 0, CLI_MSTP_MAX_NAME_BUFFER);
    Name.pu1_OctetList = &au1MstName[0];
    Name.i4_Length = CLI_MSTP_MAX_NAME_BUFFER;

    MEMSET (au1VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    MEMSET (au2VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    MEMSET (au3VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    MEMSET (au4VlanMapBuf, 0, MST_VLANMAP_LIST_SIZE);
    MEMSET (au1EmptyBuf, 0, MST_VLANMAP_LIST_SIZE);

    VlanMap1.pu1_OctetList = &au1VlanMapBuf[0];
    VlanMap1.i4_Length = MST_VLANMAP_LIST_SIZE;

    VlanMap2.pu1_OctetList = &au2VlanMapBuf[0];
    VlanMap2.i4_Length = MST_VLANMAP_LIST_SIZE;

    VlanMap3.pu1_OctetList = &au3VlanMapBuf[0];
    VlanMap3.i4_Length = MST_VLANMAP_LIST_SIZE;

    VlanMap4.pu1_OctetList = &au4VlanMapBuf[0];
    VlanMap4.i4_Length = MST_VLANMAP_LIST_SIZE;

    /* Msti Region Name */
    AST_MEMSET (Name.pu1_OctetList, 0, CLI_MSTP_MAX_NAME_BUFFER);
    nmhGetFsMIMstMstiRegionName (i4ContextId, &Name);

    /* Msti Revision */
    nmhGetFsMIMstMstiRegionVersion (i4ContextId, &i4Version);
    /* Displaying CIST instances mapping */

    /* Mst Maximum Instance */
    nmhGetFsMIMstMaxMstInstanceNumber (i4ContextId, &i4MstMaxInstanceNumber);

    /* Instance Index */
    bFlag = MST_FALSE;

    CliPrintf (CliHandle, "\r\nName            [%s]\r\n", Name.pu1_OctetList);

    CliPrintf (CliHandle, "Revision        %d\r\n", i4Version);

    CliPrintf (CliHandle, "Max-Instance    %d\r\n", i4MstMaxInstanceNumber);

    CliPrintf (CliHandle, "Instance        Vlans mapped\r\n");

    CliPrintf (CliHandle,
               " --------      ------------------------------------------\r\n");

    CliPrintf (CliHandle, "  %-2d             ", i4CistContext);

    MstFillInstanceVlanMapping (CliHandle, u4ContextId, MST_CIST_CONTEXT,
                                &i4CommaCount);

    CliPrintf (CliHandle, "\r\n");

    /* displaying other instances vlan mapping */

    i4CommaCount = 0;

    i1OutCome = nmhGetNextIndexFsMIMstVlanInstanceMappingTable (i4ContextId,
                                                                &i4NextContextId,
                                                                0, &i4NextInst);
    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        bFlag = MST_FALSE;

        MEMSET (pMstVlanListAll->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        MEMSET (pNullBuf->pu1_OctetList, 0, MST_VLAN_LIST_SIZE);
        nmhGetFsMIMstSetVlanList (i4ContextId, i4NextInst, pMstVlanListAll);

        /* Msti Instance Vlan Mapped */
        AST_MEMSET (VlanMap1.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped (i4ContextId, i4NextInst, &VlanMap1);

        /* Msti Instance Vlan Mapped2k */
        AST_MEMSET (VlanMap2.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped2k (i4ContextId, i4NextInst, &VlanMap2);

        /* Msti Instance Vlan Mapped3k */
        AST_MEMSET (VlanMap3.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped3k (i4ContextId, i4NextInst, &VlanMap3);

        /* Msti Instance Vlan Mapped4k */
        AST_MEMSET (VlanMap4.pu1_OctetList, 0, MST_VLANMAP_LIST_SIZE);
        nmhGetFsMIMstInstanceVlanMapped4k (i4ContextId, i4NextInst, &VlanMap4);

        /* Instance Index */
        CliPrintf (CliHandle, "  %-2d             ", i4NextInst);

        if ((AST_MEMCMP (au1EmptyBuf, VlanMap1.pu1_OctetList,
                         MST_VLANMAP_LIST_SIZE) == 0) &&
            (AST_MEMCMP (au1EmptyBuf, VlanMap2.pu1_OctetList,
                         MST_VLANMAP_LIST_SIZE) == 0) &&
            (AST_MEMCMP (au1EmptyBuf, VlanMap3.pu1_OctetList,
                         MST_VLANMAP_LIST_SIZE) == 0) &&
            (AST_MEMCMP (au1EmptyBuf, VlanMap4.pu1_OctetList,
                         MST_VLANMAP_LIST_SIZE) == 0) &&
            (AST_MEMCMP (pNullBuf->pu1_OctetList,
                         pMstVlanListAll->pu1_OctetList,
                         MST_VLAN_LIST_SIZE) == 0))
        {

            CliPrintf (CliHandle, "-");
        }
        else
        {
            bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap1.pu1_OctetList,
                                           VlanMap1.i4_Length,
                                           MST_VLANS_INSTANCE_1K, bFlag,
                                           &i4CommaCount);

            bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap2.pu1_OctetList,
                                           VlanMap2.i4_Length,
                                           MST_VLANS_INSTANCE_2K, bFlag,
                                           &i4CommaCount);

            bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap3.pu1_OctetList,
                                           VlanMap3.i4_Length,
                                           MST_VLANS_INSTANCE_3K, bFlag,
                                           &i4CommaCount);

            bFlag = MstDisplayOctetToVlan (CliHandle, VlanMap4.pu1_OctetList,
                                           VlanMap4.i4_Length,
                                           MST_VLANS_INSTANCE_4K, bFlag,
                                           &i4CommaCount);
            bFlag =
                MstDisplayOctetToVlan (CliHandle,
                                       pMstVlanListAll->pu1_OctetList,
                                       pMstVlanListAll->i4_Length,
                                       MST_VLANS_INSTANCE_8K, bFlag,
                                       &i4CommaCount);
        }

        CliPrintf (CliHandle, "\r\n");

        i4CurrentInst = i4NextInst;
        i1OutCome =
            nmhGetNextIndexFsMIMstVlanInstanceMappingTable (i4ContextId,
                                                            &i4NextContextId,
                                                            i4CurrentInst,
                                                            &i4NextInst);
    }

    CliPrintf (CliHandle,
               " -------------------------------------------------------\r\n\r\n");
    free_octetstring (pMstVlanListAll);
    free_octetstring (pNullBuf);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMstInstDetail                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays detailed info for an        */
/*                        instance.                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Instance - Instance id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstShowSpanningTreeMstInstDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT4 u4Instance, UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE RetBridgeId;
    tSNMP_OCTET_STRING_TYPE RetRootBridgeId;
    tSNMP_OCTET_STRING_TYPE RetMasterBridgeId;
    tSNMP_OCTET_STRING_TYPE RetPortId;
    INT4                i4CurrentPort;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortPriority;
    INT4                i4PortStateStatus;
    INT4                i4PathCost;
    INT4                i4RegionalPathCost;
    INT4                i4DesigCost;
    INT4                i4FwdDelay;
    INT4                i4MaxAge;
    INT4                i4MaxHops;
    UINT1               u1OperStatus;
    UINT4               u4Intf;
    INT4                i4NextPort;
    INT4                i4OutCome;
    INT4                i4NextInst;
    INT4                i4CurrPortInstance;
    UINT2               u2Prio;
    UINT2               u2PortPrio;
    UINT2               u2RootPrio;
    UINT2               u2MasterPrio;
    UINT2               u2Val;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1MasterBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4TxBpduCount;
    UINT4               u4RxBpduCount;
    UINT1               au1BrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1RootBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1MasterBrgIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PortIdBuf[CLI_MSTP_MAX_PORTID_BUFFER];
    UINT1               u1PortFlag = 0;
    UINT1               u1Continue = 0;
    INT4                i4ContextId = (INT4) u4ContextId;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (u4Index != 0)
    {
        u1PortFlag = 1;
        /* This flag indicates that the user has specified the port for which
         * details need to be displayed
         */
        pAstPortEntry = AstGetIfIndexEntry (u4Index);
        if (pAstPortEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }
    MEMSET (au1BrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RetBridgeId.pu1_OctetList = &au1BrgIdBuf[0];
    RetBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1RootBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RetRootBridgeId.pu1_OctetList = &au1RootBrgIdBuf[0];
    RetRootBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1MasterBrgIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    RetMasterBridgeId.pu1_OctetList = &au1MasterBrgIdBuf[0];
    RetMasterBridgeId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    MEMSET (au1PortIdBuf, 0, CLI_MSTP_MAX_PORTID_BUFFER);
    RetPortId.pu1_OctetList = &au1PortIdBuf[0];
    RetPortId.i4_Length = CLI_MSTP_MAX_PORTID_BUFFER;

    /* forward delay */
    nmhGetFsMIMstCistForwardDelay (i4ContextId, &i4FwdDelay);
    i4FwdDelay = AST_MGMT_TO_SYS (i4FwdDelay);

    /* max age */
    nmhGetFsMIMstCistMaxAge (i4ContextId, &i4MaxAge);
    i4MaxAge = AST_MGMT_TO_SYS (i4MaxAge);

    nmhGetFsMIMstMaxHopCount (i4ContextId, &i4MaxHops);
    i4MaxHops = AST_MGMT_TO_SYS (i4MaxHops);

    if (u4Instance == MST_CIST_CONTEXT)
    {
        /* walk through cist port table and display ports info */
        i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);
        while (i4OutCome != RST_FAILURE)
        {
            if (MST_TRUE !=
                MstIsPortMemberOfInst (i4NextPort, (INT4) u4Instance))
            {
                i4CurrentPort = i4NextPort;
                i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                                     &i4NextPort);
                continue;
            }

            if (u1PortFlag == 1)
            {
                if (u4Index == (UINT4) i4NextPort)
                {
                    u1Continue = 1;
                }
            }
            else
            {
                u1Continue = 1;
            }
            if (u1Continue)
            {
                AstL2IwfGetPortOperStatus (STP_MODULE,
                                           (UINT4) i4NextPort, &u1OperStatus);

                if (u1OperStatus != CFA_IF_DOWN)
                {
                    /* get the interface name for this port number */
                    u4Intf =
                        AstCfaCliGetIfName ((UINT4) i4NextPort,
                                            (INT1 *) au1IntfName);
                    if (u4Intf == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%%Port name for index %d is not found\r\n",
                                   i4NextPort);
                        return CLI_FAILURE;
                    }
                    /* port role */
                    nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);
                    /* port state */
                    nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
                    AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT,
                                             &i4PortStateStatus);
                    /* port info */
                    nmhGetFsMIMstCistPortPriority (i4NextPort, &i4PortPriority);
                    /* cost */
                    nmhGetFsMIMstCistPortPathCost (i4NextPort, &i4PathCost);
                    /* designated root */
                    AST_MEMSET (au1RootBrgAddr, AST_INIT_VAL,
                                sizeof (au1RootBrgAddr));
                    nmhGetFsMIMstCistPortDesignatedRoot (i4NextPort,
                                                         &RetRootBridgeId);
                    PrintMacAddress (RetRootBridgeId.pu1_OctetList +
                                     AST_BRG_PRIORITY_SIZE, au1RootBrgAddr);

                    u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);

                    nmhGetFsMIMstCistPortDesignatedCost (i4NextPort,
                                                         &i4DesigCost);

                    /* Designated ist master */
                    u2MasterPrio = AST_INIT_VAL;
                    AST_MEMSET (au1MasterBrgAddr, AST_INIT_VAL,
                                sizeof (au1MasterBrgAddr));
                    nmhGetFsMIMstCistPortRegionalRoot (i4NextPort,
                                                       &RetMasterBridgeId);
                    PrintMacAddress (RetMasterBridgeId.pu1_OctetList +
                                     AST_BRG_PRIORITY_SIZE, au1MasterBrgAddr);
                    u2MasterPrio = AstGetBrgPrioFromBrgId (RetMasterBridgeId);
                    /* cost */
                    nmhGetFsMIMstCistPortRegionalPathCost (i4NextPort,
                                                           &i4RegionalPathCost);

                    /* designated bridge */
                    u2Prio = AST_INIT_VAL;
                    AST_MEMSET (au1BrgAddr, AST_INIT_VAL, sizeof (au1BrgAddr));
                    nmhGetFsMIMstCistPortDesignatedBridge (i4NextPort,
                                                           &RetBridgeId);
                    PrintMacAddress (RetBridgeId.pu1_OctetList +
                                     AST_BRG_PRIORITY_SIZE, au1BrgAddr);
                    u2Prio = AstGetBrgPrioFromBrgId (RetBridgeId);
                    /* designated port id */
                    u2PortPrio = AST_INIT_VAL;
                    u2Val = AST_INIT_VAL;
                    nmhGetFsMIMstCistPortDesignatedPort (i4NextPort,
                                                         &RetPortId);
                    u2PortPrio =
                        (UINT2) (RetPortId.
                                 pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
                    u2Val = AstCliGetPortIdFromOctetList (RetPortId);
                    u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);

                    u4TxBpduCount = MstCistPortTxBpduCount (i4NextPort);
                    /* BPDUs received */

                    u4RxBpduCount = MstCistPortRxBpduCount (i4NextPort);

                    CliPrintf (CliHandle, "\r\n%s ", au1IntfName);
                    CliPrintf (CliHandle, "of MST%02d is ", u4Instance);

                    StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                    CliPrintf (CliHandle, ", ");

                    StpCliDisplayPortState (CliHandle, i4PortState,
                                            i4PortStateStatus);
                    CliPrintf (CliHandle, "\r\n");

                    CliPrintf (CliHandle, "Port info             port id ");
                    CliPrintf (CliHandle, "%-3d.%-2d            ",
                               i4PortPriority, i4NextPort);

                    CliPrintf (CliHandle, "priority %-5d ", i4PortPriority);
                    CliPrintf (CliHandle, "cost %d\r\n", i4PathCost);

                    CliPrintf (CliHandle, "Designated root       address ");
                    CliPrintf (CliHandle, "%s", au1RootBrgAddr);
                    CliPrintf (CliHandle, "priority %-5d ", u2RootPrio);
                    CliPrintf (CliHandle, "cost %d\r\n", i4DesigCost);
                    CliPrintf (CliHandle, "Designated ist master address %s",
                               au1MasterBrgAddr);
                    CliPrintf (CliHandle, "priority %-5d ", u2MasterPrio);
                    CliPrintf (CliHandle, "cost %d\r\n", i4RegionalPathCost);

                    CliPrintf (CliHandle, "Designated bridge     address ");
                    CliPrintf (CliHandle, "%s", au1BrgAddr);
                    CliPrintf (CliHandle, "priority %-5d ", u2Prio);

                    CliPrintf (CliHandle, "portid %d.%d\r\n", u2PortPrio,
                               u2Val);
                    MstDisplayConfiguredBridgeValues (CliHandle, i4MaxAge,
                                                      i4FwdDelay, i4MaxHops);

                    MstDisplayOperBridgeValues (CliHandle,
                                                i4MaxAge, i4FwdDelay);

                    CliPrintf (CliHandle, "Bpdus sent %d , ", u4TxBpduCount);
                    CliPrintf (CliHandle, "Received %d \r\n", u4RxBpduCount);

                }
                if (u1PortFlag == 1)
                {
                    break;
                }
            }
            i4CurrentPort = i4NextPort;
            i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                                 &i4NextPort);

        }

    }

    else
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {

            CliPrintf (CliHandle,
                       "%% No mst information available for instance %d\r\n",
                       u4Instance);

            return CLI_FAILURE;
        }

        /* walk through Msti Port table and display ports info if
         * port belongs to given instance                       */
        u1Continue = 0;
        i4OutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, 0, &i4NextPort, 0,
                                          &i4NextInst);
        while (i4OutCome != RST_FAILURE)
        {
            if (MST_TRUE != MstIsPortMemberOfInst (i4NextPort, i4NextInst))
            {
                i4CurrentPort = i4NextPort;
                i4CurrPortInstance = i4NextInst;
                i4OutCome =
                    AstGetNextMstiPortTableIndex (u4ContextId, i4CurrentPort,
                                                  &i4NextPort,
                                                  i4CurrPortInstance,
                                                  &i4NextInst);
                continue;
            }

            if (u1PortFlag == 1)
            {
                if (u4Index == (UINT4) i4NextPort)
                {
                    u1Continue = 1;
                }
            }
            else
            {
                u1Continue = 1;
            }
            if (u1Continue)
            {

                if (i4NextInst == (INT4) u4Instance)
                {
                    AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                               &u1OperStatus);

                    if (u1OperStatus != CFA_IF_DOWN)
                    {
                        /* get the interface name for this port number */
                        u4Intf =
                            AstCfaCliGetIfName ((UINT4) i4NextPort,
                                                (INT1 *) au1IntfName);

                        /* port role */
                        nmhGetFsMIMstMstiCurrentPortRole (i4NextPort,
                                                          u4Instance,
                                                          &i4PortRole);
                        /* port state */
                        nmhGetFsMIMstMstiPortState (i4NextPort, u4Instance,
                                                    &i4PortState);
                        AstGetHwPortStateStatus (i4NextPort, u4Instance,
                                                 &i4PortStateStatus);
                        /* port info */

                        nmhGetFsMIMstMstiPortPriority (i4NextPort, u4Instance,
                                                       &i4PortPriority);

                        nmhGetFsMIMstMstiPortPriority (i4NextPort, u4Instance,
                                                       &i4PortPriority);
                        /* cost */
                        nmhGetFsMIMstMstiPortPathCost (i4NextPort, u4Instance,
                                                       &i4PathCost);
                        /* designated root */
                        u2RootPrio = AST_INIT_VAL;
                        AST_MEMSET (au1BrgAddr, AST_INIT_VAL,
                                    sizeof (au1RootBrgAddr));
                        nmhGetFsMIMstMstiPortDesignatedRoot (i4NextPort,
                                                             u4Instance,
                                                             &RetRootBridgeId);
                        PrintMacAddress (RetRootBridgeId.pu1_OctetList +
                                         AST_BRG_PRIORITY_SIZE, au1RootBrgAddr);

                        u2RootPrio = AstGetBrgPrioFromBrgId (RetRootBridgeId);

                        /* Desig Cost */
                        nmhGetFsMIMstMstiPortDesignatedCost (i4NextPort,
                                                             u4Instance,
                                                             &i4DesigCost);
                        /* designated bridge */
                        u2Prio = AST_INIT_VAL;
                        AST_MEMSET (au1BrgAddr, AST_INIT_VAL,
                                    sizeof (au1BrgAddr));
                        nmhGetFsMIMstMstiPortDesignatedBridge (i4NextPort,
                                                               u4Instance,
                                                               &RetBridgeId);

                        PrintMacAddress (RetBridgeId.pu1_OctetList +
                                         AST_BRG_PRIORITY_SIZE, au1BrgAddr);

                        u2Prio = AstGetBrgPrioFromBrgId (RetBridgeId);
                        /* designated port id */
                        u2PortPrio = AST_INIT_VAL;
                        u2Val = AST_INIT_VAL;
                        AST_MEMSET (RetPortId.pu1_OctetList, 0,
                                    CLI_MSTP_MAX_PORTID_BUFFER);
                        nmhGetFsMIMstMstiPortDesignatedPort (i4NextPort,
                                                             u4Instance,
                                                             &RetPortId);
                        u2PortPrio =
                            (UINT2) (RetPortId.
                                     pu1_OctetList[0] & AST_PORTPRIORITY_MASK);
                        u2Val = AstCliGetPortIdFromOctetList (RetPortId);
                        u2Val = (UINT2) (u2Val & AST_PORTNUM_MASK);

                        if (u4Intf == CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }

                        CliPrintf (CliHandle, "\r\n%s ", au1IntfName);
                        CliPrintf (CliHandle, "of MST%02d is ", u4Instance);

                        StpCliDisplayPortRole (CliHandle, u4ContextId,
                                               i4PortRole);
                        CliPrintf (CliHandle, ", ");

                        StpCliDisplayPortState (CliHandle, i4PortState,
                                                i4PortStateStatus);
                        CliPrintf (CliHandle, "\r\n");

                        CliPrintf (CliHandle, "Port info         port id ");
                        CliPrintf (CliHandle, "%-3d.%-2d            ",
                                   i4PortPriority, i4NextPort);
                        CliPrintf (CliHandle, "priority %-5d ", i4PortPriority);
                        CliPrintf (CliHandle, "cost %d\r\n", i4PathCost);
                        CliPrintf (CliHandle, "Designated root   address ");
                        CliPrintf (CliHandle, "%s", au1RootBrgAddr);
                        CliPrintf (CliHandle, "priority %-5d ", u2RootPrio);
                        CliPrintf (CliHandle, "cost %d\r\n", i4DesigCost);
                        CliPrintf (CliHandle, "Designated bridge address ");
                        CliPrintf (CliHandle, "%s", au1BrgAddr);
                        CliPrintf (CliHandle, "priority %-5d ", u2Prio);
                        CliPrintf (CliHandle, "port id %-3d.%-2d\r\n",
                                   u2PortPrio, u2Val);

                    }
                    u4PagingStatus = CliPrintf (CliHandle, "\r");

                    if (u4PagingStatus == CLI_FAILURE)
                    {
                        /* User Presses 'q' at the prompt, so break! */
                        break;
                    }

                    if (u1PortFlag == 1)
                    {
                        break;
                    }
                }
            }
            i4CurrentPort = i4NextPort;
            i4CurrPortInstance = i4NextInst;
            i4OutCome =
                AstGetNextMstiPortTableIndex (u4ContextId, i4CurrentPort,
                                              &i4NextPort,
                                              i4CurrPortInstance, &i4NextInst);

        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeInterfaceDetails                */
/*                                                                           */
/*     DESCRIPTION      : This function displays info for a port.            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Index - port index                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstShowSpanningTreeMstInterfaceDetails (tCliHandle CliHandle,
                                        UINT4 u4ContextId, UINT4 u4Index,
                                        UINT4 u4Instance)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4NextInst;
    INT4                i4CurrentInst;
    INT4                i4PortRole;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4Intf;
    INT4                i4EdgeStatus;
    INT4                i4HelloTime;
    INT4                i4PortPriority;
    INT4                i4PathCost;
    INT4                i4LinkType;
    INT1                i1OutCome;
    UINT4               u4TxBpduCount;
    UINT4               u4RxBpduCount;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, " %% Bridge is not in MST mode\r\n");
        return CLI_FAILURE;
    }

    AstReleaseContext ();

    pAstPortEntry = AstGetIfIndexEntry (u4Index);

    if (u4Index != 0)
    {
        if (pAstPortEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }
    if (u4Instance != 0)
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4Instance != MST_CIST_CONTEXT)
    {
        u1Flag = 1;
        if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, u4Instance)
            == MST_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% No mst information available for instance %d on "
                       "interface %d\r\n", u4Instance, u4Index);
            return CLI_FAILURE;
        }

    }

    if (u4Instance == MST_CIST_CONTEXT)
    {
        if (MST_TRUE ==
            MstIsPortMemberOfInst ((INT4) u4Index, (INT4) u4Instance))
        {

            /* get the interface name for this port number */
            i4Intf = AstCfaCliGetIfName (u4Index, (INT1 *) au1IntfName);
            if (i4Intf == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Port name for index %d is not found\r\n",
                           u4Index);
                return CLI_FAILURE;
            }

            /* Port role */
            nmhGetFsMIMstCistCurrentPortRole (u4Index, &i4PortRole);
            /* port state */
            nmhGetFsMIMstCistPortState (u4Index, &i4PortState);

            AstGetHwPortStateStatus (u4Index, MST_CIST_CONTEXT,
                                     &i4PortStateStatus);

            /* edge port */
            i1OutCome =
                nmhGetFsMIMstCistPortOperEdgeStatus (u4Index, &i4EdgeStatus);
            /* link type */
            nmhGetFsMIMstCistPortOperP2P (u4Index, &i4LinkType);

            nmhGetFsMIMstCistPortHelloTime (u4Index, &i4HelloTime);

            /* BPDUs transmitted */
            u4TxBpduCount = MstCistPortTxBpduCount (u4Index);

            /* BPDUs received */
            u4RxBpduCount = MstCistPortRxBpduCount (u4Index);
            /* cost */
            nmhGetFsMIMstCistPortPathCost (u4Index, &i4PathCost);
            /* priority.number */
            nmhGetFsMIMstCistPortPriority (u4Index, &i4PortPriority);

            CliPrintf (CliHandle, "\r\n%s ", au1IntfName);

            CliPrintf (CliHandle, "of MST%02d is ", MST_CIST_CONTEXT);

            StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, ", ");

            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "\r\n");

            if (i1OutCome == SNMP_SUCCESS)
            {
                if (i4EdgeStatus == AST_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, "Edge port: yes\r\n");
                }
                else if (i4EdgeStatus == AST_SNMP_FALSE)
                {
                    CliPrintf (CliHandle, "Edge port: no\r\n");
                }
            }

            if (i4LinkType == AST_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "Link type: P2P\r\n");
            }
            else if (i4LinkType == AST_SNMP_FALSE)
            {
                CliPrintf (CliHandle, "Link type: SharedLan\r\n");
            }

            CliPrintf (CliHandle, "Port Hello Timer: %d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

            CliPrintf (CliHandle, "Bpdus sent %d , ", u4TxBpduCount);
            CliPrintf (CliHandle, "Received %d \r\n", u4RxBpduCount);

            /* compose header */
            CliPrintf (CliHandle, "\r\n%-10s%-13s%-13s%-9s%-9s\r\n",
                       "Instance", "Role", "Sts", "Cost", "Prio.Nbr");

            CliPrintf (CliHandle, "%-10s%-13s%-13s%-9s%-9s\r\n",
                       "--------", "----", "---", "----", "--------");
            CliPrintf (CliHandle, "%-10d", MST_CIST_CONTEXT);

            StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");
            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "%-3s", " ");
            CliPrintf (CliHandle, "%-9d", i4PathCost);
            CliPrintf (CliHandle, "%3d.%-4d ", i4PortPriority, u4Index);

            CliPrintf (CliHandle, "\r\n");
        }
        /* displaying other Instances information */
        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, 0,
                                                   &i4NextInst);

    }
    else
    {
        i4NextInst = u4Instance;
        i1OutCome = SNMP_SUCCESS;

        i4NextContextId = i4ContextId;

        /* compose header */
        CliPrintf (CliHandle, "\r\n%-10s%-13s%-13s%-9s%-9s\r\n",
                   "Instance", "Role", "Sts", "Cost", "Prio.Nbr");

        CliPrintf (CliHandle, "%-10s%-13s%-13s%-9s%-9s\r\n",
                   "--------", "----", "---", "----", "--------");

    }
    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        if (MST_TRUE == MstIsPortMemberOfInst ((INT4) u4Index, i4NextInst))
        {

            if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, i4NextInst)
                == MST_SUCCESS)
            {

                /* port role */
                nmhGetFsMIMstMstiCurrentPortRole (u4Index, i4NextInst,
                                                  &i4PortRole);

                /* port state */
                nmhGetFsMIMstMstiPortState (u4Index, i4NextInst, &i4PortState);

                AstGetHwPortStateStatus (u4Index, i4NextInst,
                                         &i4PortStateStatus);

                /* cost */
                nmhGetFsMIMstMstiPortPathCost (u4Index, i4NextInst,
                                               &i4PathCost);

                /* priority.number */
                nmhGetFsMIMstMstiPortPriority (u4Index, i4NextInst,
                                               &i4PortPriority);

                CliPrintf (CliHandle, "%-10d", i4NextInst);
                StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                CliPrintf (CliHandle, "%-3s", " ");
                StpCliDisplayPortState (CliHandle, i4PortState,
                                        i4PortStateStatus);
                CliPrintf (CliHandle, "%-3s", " ");
                CliPrintf (CliHandle, "%-9d", i4PathCost);
                CliPrintf (CliHandle, "%3d.%-4d ", i4PortPriority, u4Index);

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }
            }
        }

        i4CurrentInst = i4NextInst;

        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId,
                                                   i4CurrentInst, &i4NextInst);

        if (u1Flag == 1)
        {
            break;
        }
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMstInterfaceStats                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays detailed info for a port.   */
/*                                                                           */
/*     INPUT            : CliHandle - pointer to the context                 */
/*                        u4Index - port index                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstShowSpanningTreeMstInterfaceStats (tCliHandle CliHandle, UINT4 u4ContextId,
                                      UINT4 u4Instance, UINT4 u4Index)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4CurrentInst;
    INT4                i4NextInst;
    INT1                i1OutCome;
    UINT4               u4TxBpduCount = 0;
    UINT4               u4RxBpduCount = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, " %% Bridge is not in MST mode\r\n");
        return CLI_FAILURE;
    }

    AstReleaseContext ();

    if (u4Index != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4Index);
        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");

            return CLI_FAILURE;
        }
    }
    if (u4Instance == 0)
    {
        i4NextInst = MST_CIST_CONTEXT;

        u4TxBpduCount = MstCistPortTxBpduCount (u4Index);

        /* BPDUs received */
        u4RxBpduCount = MstCistPortRxBpduCount (u4Index);

        /* BPDUs transmitted */
        CliPrintf (CliHandle, "\r\nMST%02d     ", i4NextInst);
        CliPrintf (CliHandle, "Bpdus sent %d,", u4TxBpduCount);
        CliPrintf (CliHandle, " Received %d\r\n", u4RxBpduCount);

        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, 0,
                                                   &i4NextInst);
    }
    else
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");

            return (CLI_FAILURE);
        }
        i1OutCome = SNMP_SUCCESS;
        i4NextInst = u4Instance;
        i4NextContextId = i4ContextId;

        CliPrintf (CliHandle, "\r\n");

    }

    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, i4NextInst)
            == MST_SUCCESS)
        {

            /* BPDUs transmitted */
            nmhGetFsMIMstMstiPortTransmittedBPDUs (u4Index, i4NextInst,
                                                   &u4TxBpduCount);
            /* BPDUs received */
            nmhGetFsMIMstMstiPortReceivedBPDUs (u4Index, i4NextInst,
                                                &u4RxBpduCount);

            CliPrintf (CliHandle, "MST%02d     ", i4NextInst);
            CliPrintf (CliHandle, "Bpdus sent %d,", u4TxBpduCount);
            CliPrintf (CliHandle, " Received %d\r\n", u4RxBpduCount);
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentInst = i4NextInst;

        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId,
                                                   i4CurrentInst, &i4NextInst);
        if (u4Instance != 0)
        {
            break;
        }
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstDisplayOctetToVlan                              */
/*                                                                           */
/*     DESCRIPTION      : This function converts the octet list containing   */
/*                        vlans and displays it in suitable form .           */
/*                                                                           */
/*     INPUT            : CliHandle  - Handle to the cli context             */
/*                        pOctet - Vlan Octet list                           */
/*                        OctetLen - octet list length                       */
/*                        i4Flag - Flag specifies 1K range                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : bSeprtr                                            */
/*                                                                           */
/*****************************************************************************/

tAstBoolean
MstDisplayOctetToVlan (tCliHandle CliHandle, UINT1 *pOctet, UINT4 u4OctetLen,
                       INT4 i4Flag, tAstBoolean bSeprtr, INT4 *pi4CommaCount)
{
    tAstBoolean         bOutCome = MST_FALSE;
    tAstBoolean         bFlag = MST_TRUE;
    tAstVlanList        VlanList;
    tAstMstVlanList     AstMstVlanList;
    UINT2               u2VlanIdStart = AST_INIT_VAL;
    UINT2               u2VlanIdEnd = AST_INIT_VAL;
    UINT2               u2VlanId = AST_INIT_VAL;
    UINT2               u2Temp = 0;
    UINT2               u2LastPrint = 0;
    UINT2               u2TempVlanId = 0;

    AST_MEMSET (VlanList, AST_INIT_VAL, sizeof (tAstVlanList));
    AST_MEMSET (AstMstVlanList, AST_INIT_VAL, sizeof (tAstMstVlanList));

    if (i4Flag == MST_VLANS_INSTANCE_8K)
    {
        AST_MEMCPY (AstMstVlanList, pOctet, u4OctetLen);
    }
    else
    {
        AST_MEMCPY (VlanList, pOctet, u4OctetLen);
    }

    switch (i4Flag)
    {

        case MST_VLANS_INSTANCE_1K:
            u2VlanIdStart = 1;
            u2VlanIdEnd = 1024;
            break;

        case MST_VLANS_INSTANCE_2K:
            u2VlanIdStart = 1025;
            u2VlanIdEnd = 2048;
            break;

        case MST_VLANS_INSTANCE_3K:
            u2VlanIdStart = 2049;
            u2VlanIdEnd = 3072;
            break;

        case MST_VLANS_INSTANCE_4K:
            u2VlanIdStart = 3073;
            u2VlanIdEnd = 4094;
            break;

        case MST_VLANS_INSTANCE_8K:
            u2VlanIdStart = 4095;
            u2VlanIdEnd = VLAN_DEV_MAX_VLAN_ID;
            break;

        default:
            break;
    }

    for (u2VlanId = u2VlanIdStart; u2VlanId <= u2VlanIdEnd; u2VlanId++)
    {
        if (u2VlanId > MST_VLANS_RANGE_4K)
        {
            u2TempVlanId = u2VlanId;
        }
        else if (u2VlanId > MST_VLANS_RANGE_3K)
        {
            u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_3K);
        }
        else if (u2VlanId > MST_VLANS_RANGE_2K)
        {
            u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_2K);
        }
        else if (u2VlanId > MST_VLANS_RANGE_1K)
        {
            u2TempVlanId = (UINT2) (u2VlanId - MST_VLANS_RANGE_1K);
        }
        else
        {
            u2TempVlanId = u2VlanId;
        }
        if ((u2TempVlanId / MST_PORTORVLAN_PER_BYTE) <= MST_VLANMAP_LIST_SIZE)
        {
            MST_IS_MEMBER_VLAN (VlanList, u2TempVlanId, bOutCome);
        }
        else
        {
            MST_IS_MEMBER_VLAN (AstMstVlanList, u2TempVlanId, bOutCome);
        }
        if (bOutCome == MST_TRUE)
        {
            if (bFlag != MST_TRUE)
            {
                if ((u2VlanId - u2Temp) != 1)
                {
                    if (u2LastPrint != u2Temp)
                    {
                        CliPrintf (CliHandle, "-");
                        CliPrintf (CliHandle, "%d", u2Temp);
                    }
                    (*pi4CommaCount)++;
                    MstCliPrintVlanList (CliHandle, *pi4CommaCount, u2VlanId);

                    u2LastPrint = u2VlanId;

                }
            }
            else
            {

                if (bSeprtr == MST_TRUE)
                {
                    (*pi4CommaCount)++;
                    MstCliPrintVlanList (CliHandle, *pi4CommaCount, u2VlanId);
                }
                else
                {
                    (*pi4CommaCount)++;
                    CliPrintf (CliHandle, "%d", u2VlanId);
                }
                u2LastPrint = u2VlanId;
                bFlag = MST_FALSE;
            }
            bSeprtr = MST_TRUE;
            u2Temp = u2VlanId;
        }
    }
    if (u2Temp > MST_VLANS_RANGE_4K)
    {
        u2TempVlanId = u2Temp;
    }
    else if (u2Temp > MST_VLANS_RANGE_3K)
    {
        u2TempVlanId = (UINT2) (u2Temp - MST_VLANS_RANGE_3K);
    }
    else if (u2Temp > MST_VLANS_RANGE_2K)
    {
        u2TempVlanId = (UINT2) (u2Temp - MST_VLANS_RANGE_2K);
    }
    else if (u2Temp > MST_VLANS_RANGE_1K)
    {
        u2TempVlanId = (UINT2) (u2Temp - MST_VLANS_RANGE_1K);
    }
    else
    {
        u2TempVlanId = u2Temp;
    }
    if ((u2TempVlanId / MST_PORTORVLAN_PER_BYTE) <= MST_VLANMAP_LIST_SIZE)
    {
        MST_IS_MEMBER_VLAN (VlanList, u2TempVlanId, bOutCome);
    }
    else
    {
        MST_IS_MEMBER_VLAN (AstMstVlanList, u2TempVlanId, bOutCome);
    }
    if (bOutCome == MST_TRUE)
    {
        if (u2LastPrint != u2Temp)
        {
            CliPrintf (CliHandle, "-");
            CliPrintf (CliHandle, "%d", u2Temp);
        }
    }

    return bSeprtr;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliPrintVlanList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of vlan in vlan list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

VOID
MstCliPrintVlanList (tCliHandle CliHandle, INT4 i4CommaCount, UINT2 VlanId)
{

    INT4                i4Times;
    INT4                i4Loop;
    INT4                i4Count = 1;
    i4Times = VLAN_DEV_MAX_VLAN_ID;

    if (i4CommaCount == i4Count)
    {
        CliPrintf (CliHandle, "%d", VlanId);
        return;
    }
    for (i4Loop = 1; i4Loop < i4Times; i4Loop++)
    {
        /* Only 4 commas per line will be displayed */
        if (i4CommaCount <= (4 * i4Loop))
        {
            if (i4CommaCount == (4 * (i4Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, ",");
                CliPrintf (CliHandle, "%d", VlanId);
            }
            else
            {
                CliPrintf (CliHandle, ",%d", VlanId);
            }
            break;
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstCistPortTxBpduCount                             */
/*                                                                           */
/*     DESCRIPTION      : This function gives the number of Bpdus transmitted*/
/*                        on a given port  in the CIST Context               */
/*                                                                           */
/*     INPUT            : i4Port - port index                                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : u4TxBpduCount - CIST Transmitted BPDU count        */
/*****************************************************************************/
UINT4
MstCistPortTxBpduCount (INT4 i4Port)
{
    UINT4               u4Count;
    UINT4               u4TxBpduCount;

    nmhGetFsMIMstCistPortTxMstBpduCount (i4Port, &u4Count);
    u4TxBpduCount = u4Count;
    nmhGetFsMIMstCistPortTxRstBpduCount (i4Port, &u4Count);
    u4TxBpduCount += u4Count;
    nmhGetFsMIMstCistPortTxConfigBpduCount (i4Port, &u4Count);
    u4TxBpduCount += u4Count;
    nmhGetFsMIMstCistPortTxTcnBpduCount (i4Port, &u4Count);
    u4TxBpduCount += u4Count;
    return u4TxBpduCount;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstCistPortRxBpduCount                             */
/*                                                                           */
/*     DESCRIPTION      : This function gives the number of received Bpdus   */
/*                        on a given port in the CIST Context                */
/*                                                                           */
/*     INPUT            : i4NextPort - port index                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : u4RxBpduCount - CIST received BPDU count           */
/*****************************************************************************/
UINT4
MstCistPortRxBpduCount (INT4 i4NextPort)
{
    UINT4               u4Count;
    UINT4               u4RxBpduCount;

    nmhGetFsMIMstCistPortRxMstBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount = u4Count;
    nmhGetFsMIMstCistPortRxRstBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount += u4Count;
    nmhGetFsMIMstCistPortRxConfigBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount += u4Count;
    nmhGetFsMIMstCistPortRxTcnBpduCount (i4NextPort, &u4Count);
    u4RxBpduCount += u4Count;

    return u4RxBpduCount;
}

/*****************************************************************************/
/* Function Name      : MstDisplayPortType                                   */
/*                                                                           */
/* Description        : fills the port type of the port                      */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context                */
/*                      i4Port    - port number.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/

VOID
MstDisplayPortType (tCliHandle CliHandle, INT4 i4LinkType)
{
    if (i4LinkType == AST_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "P2P\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "SharedLan\r\n");
    }
}

/*****************************************************************************/
/* Function Name      : MstCliShowSUmmary                                    */
/*                                                                           */
/* Description        : Displays the spanningtree port states and  port roles*/
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/

INT4
MstCliShowSummary (tCliHandle CliHandle, UINT4 u4ContextId)
{

    INT4                i4OutCome = 0;
    INT1                i1InstOutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurPort = 0;
    INT4                i4PortRole = 0;
    INT4                i4PortState = 0;
    INT4                i4PortStateStatus = 0;

    INT4                i4PortStatus = 0;
    INT4                i4NextInst = 0;
    INT4                i4PrevInst = 0;
    INT4                i4NextPortInstance = 0;
    INT4                i4CurrPortInstance = 0;
    INT4                i4NextContextId = 0;
    INT4                i4ContextId = (INT4) u4ContextId;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IntfName, 0, sizeof (au1IntfName));

    /* Displays the Spanning Tree PathCost Method ,PortStates 
       and Roles */

    CliPrintf (CliHandle, "Spanning tree enabled protocol is MSTP\r\n");
    MstCliShowSpanningTreeMethod (CliHandle);
    CliPrintf (CliHandle, "\r\nMST00 Port Roles and States\r\n");
    CliPrintf (CliHandle,
               "Port-Index  Port-Role    Port-State     Port-Status\r\n");
    CliPrintf (CliHandle,
               "----------  ---------    ----------     -----------\r\n");

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    while (i4OutCome != RST_FAILURE)
    {
        if (MST_TRUE == MstIsPortMemberOfInst (i4NextPort, i4NextInst))
        {

            /* Port Role */
            nmhGetFsMIMstCistCurrentPortRole (i4NextPort, &i4PortRole);

            /* Port State */
            nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
            AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT,
                                     &i4PortStateStatus);

            /* Port Status */
            nmhGetFsMIMstCistForcePortState (i4NextPort, &i4PortStatus);

            AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);
            CliPrintf (CliHandle, "%-12s", au1IntfName);

            StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
            CliPrintf (CliHandle, "%-3s", " ");

            StpCliDisplayPortState (CliHandle, i4PortState, i4PortStateStatus);
            CliPrintf (CliHandle, "%-3s", " ");

            CliPrintf (CliHandle, "\t");
            if (i4PortStatus == MST_FORCE_STATE_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Disabled\r\n");
            }
        }
        i4CurPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurPort,
                                             &i4NextPort);
    }

    i1InstOutCome =
        nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId, &i4NextContextId, 0,
                                               &i4NextInst);

    while ((i1InstOutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        if (i4NextInst == AST_TE_MSTID)
        {
            break;
        }

        CliPrintf (CliHandle,
                   "\r\nMST%02d Port Roles and States\r\n", i4NextInst);
        CliPrintf (CliHandle,
                   "Port-Index  Port-Role    Port-State     Port-Status\r\n");
        CliPrintf (CliHandle,
                   "----------  ---------    ----------     -----------\r\n");

        if (MstMiValidateInstanceEntry (u4ContextId, i4NextInst) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n MST%02d no longer valid\r\n",
                       i4NextInst);
            i4PrevInst = i4NextInst;
            i1InstOutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId,
                                                       i4PrevInst, &i4NextInst);

            continue;
        }

        i4OutCome =
            AstGetNextMstiPortTableIndex (u4ContextId, 0, &i4NextPort,
                                          0, &i4NextPortInstance);

        while (i4OutCome != RST_FAILURE)
        {

            if (MST_TRUE == MstIsPortMemberOfInst (i4NextPort,
                                                   i4NextPortInstance))
            {

                if (i4NextPortInstance == i4NextInst)
                {

                    /* Port Role */
                    nmhGetFsMIMstMstiCurrentPortRole (i4NextPort,
                                                      i4NextPortInstance,
                                                      &i4PortRole);

                    /* Port State */
                    nmhGetFsMIMstMstiPortState (i4NextPort, i4NextPortInstance,
                                                &i4PortState);
                    AstGetHwPortStateStatus (i4NextPort, i4NextPortInstance,
                                             &i4PortStateStatus);
                    /*PortStatus */
                    nmhGetFsMIMstMstiForcePortState (i4NextPort,
                                                     i4NextPortInstance,
                                                     &i4PortStatus);

                    AstCfaCliGetIfName ((UINT4) i4NextPort,
                                        (INT1 *) au1IntfName);
                    CliPrintf (CliHandle, "%-12s", au1IntfName);

                    StpCliDisplayPortRole (CliHandle, u4ContextId, i4PortRole);
                    CliPrintf (CliHandle, "%-3s", " ");

                    StpCliDisplayPortState (CliHandle, i4PortState,
                                            i4PortStateStatus);
                    CliPrintf (CliHandle, "\t ");

                    if (i4PortStatus == MST_FORCE_STATE_ENABLED)
                    {
                        CliPrintf (CliHandle, "Enabled\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "Disabled\r\n");
                    }
                }
            }

            i4CurPort = i4NextPort;
            i4CurrPortInstance = i4NextPortInstance;

            i4OutCome = AstGetNextMstiPortTableIndex (u4ContextId, i4CurPort,
                                                      &i4NextPort,
                                                      i4CurrPortInstance,
                                                      &i4NextPortInstance);
        }
        i4PrevInst = i4NextInst;
        i1InstOutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, i4PrevInst,
                                                   &i4NextInst);
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMstInstIntfDetail               */
/*                                                                           */
/*     DESCRIPTION      : This function displays detailed info for a port.   */
/*                                                                           */
/*     INPUT            : CliHandle - pointer to the context                 */
/*                        u4Index - port index                               */
/*                        u4Instance - instance id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstShowSpanningTreeMstInstIntfDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                                      UINT4 u4Instance, UINT4 u4Index)
{

    INT4                i4NextInst = u4Instance;
    INT4                i4CurInst;
    INT1                i1OutCome;
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, " %% Bridge is not in MST mode\r\n");
        return CLI_FAILURE;
    }

    AstReleaseContext ();

    if (u4Index != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4Index);
        if (pAstPortEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");

            return (CLI_FAILURE);
        }
    }

    if (u4Instance != 0)
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");

            return (CLI_FAILURE);
        }
        /* check if Instance-interface relation exists */
        if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, u4Instance)
            == MST_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Instance %d does not exist for this port \r\n",
                       u4Instance);
            return CLI_FAILURE;
        }
        MstShowSpanningTreeMstInstDetail (CliHandle, u4ContextId, u4Instance,
                                          u4Index);
    }
    else
    {
        /* if Instance is not specified, scan through all the configured MSTi
         * instances */
        MstShowSpanningTreeMstInstDetail (CliHandle, u4ContextId,
                                          MST_CIST_CONTEXT, u4Index);

        i1OutCome = nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId, 0,
                                                           &i4NextInst);

        while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
        {

            if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, i4NextInst)
                == MST_SUCCESS)
            {
                MstShowSpanningTreeMstInstDetail (CliHandle, u4ContextId,
                                                  i4NextInst, u4Index);
            }
            i4CurInst = i4NextInst;

            i1OutCome =
                nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                       &i4NextContextId,
                                                       i4CurInst, &i4NextInst);
            CliFlush (CliHandle);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstShowSpanningTreeMstInterfaceHelloTime*/
/*                                                                           */
/*     DESCRIPTION      : This function displays detailed info for a port.   */
/*                                                                           */
/*     INPUT            : CliHandle - pointer to the context                 */
/*                        u4Index - port index                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstShowSpanningTreeMstInterfaceHelloTime (tCliHandle CliHandle,
                                          UINT4 u4ContextId,
                                          UINT4 u4Instance, UINT4 u4Index)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    INT4                i4CurrentInst;
    INT4                i4NextInst;
    INT4                i4HelloTime;
    INT1                i1OutCome;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId;
    INT4                i4ContextId = (INT4) u4ContextId;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (AST_FORCE_VERSION != AST_VERSION_3)
    {
        AstReleaseContext ();
        CliPrintf (CliHandle, " %% Bridge is not in MST mode\r\n");
        return CLI_FAILURE;
    }

    AstReleaseContext ();

    if (u4Index == 0)
    {
        return CLI_FAILURE;
    }

    pAstPortEntry = AstGetIfIndexEntry (u4Index);
    if (pAstPortEntry == NULL)
    {

        CliPrintf (CliHandle,
                   "\r%% Cannot Set Spanning Tree Properties on a port "
                   "that belongs to a PortChannel\r\n");

        return CLI_FAILURE;
    }
    if (u4Instance == 0)

    {
        i4NextInst = MST_CIST_CONTEXT;

        /* BPDUs transmitted */

        nmhGetFsMIMstCistPortHelloTime (u4Index, &i4HelloTime);

        CliPrintf (CliHandle, "\r\nMST%02d     ", i4NextInst);
        CliPrintf (CliHandle, "%d sec %d cs \r\n",
                   AST_PROT_TO_BPDU_SEC (i4HelloTime),
                   AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));

        /* walk through the msti port table to find the given port index
         * details */

        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId, 0,
                                                   &i4NextInst);
    }
    else
    {
        if (MstMiValidateInstanceEntry (u4ContextId, u4Instance) == MST_FAILURE)
        {

            CliPrintf (CliHandle, "\r%% Invalid Instance Specified\r\n");

            return (CLI_FAILURE);
        }

        CliPrintf (CliHandle, "\r\n");

        i4NextContextId = i4ContextId;

        i1OutCome = SNMP_SUCCESS;
        i4NextInst = u4Instance;
        nmhGetFsMIMstCistPortHelloTime (u4Index, &i4HelloTime);
    }

    while ((i1OutCome != SNMP_FAILURE) && (i4ContextId == i4NextContextId))
    {
        if (MstMiValidateMstiPortEntry (u4ContextId, u4Index, i4NextInst)
            == MST_SUCCESS)
        {

            CliPrintf (CliHandle, "MST%02d     ", i4NextInst);
            CliPrintf (CliHandle, "%d sec %d cs\r\n",
                       AST_PROT_TO_BPDU_SEC (i4HelloTime),
                       AST_PROT_TO_BPDU_CENTI_SEC (i4HelloTime));
        }
        else if (u4Instance != 0)
        {
            CliPrintf (CliHandle, "\r%% No such Port for this instance\r\n");

            return (CLI_FAILURE);
        }

        if (u4Instance != 0)
        {
            break;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentInst = i4NextInst;
        i1OutCome =
            nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                   &i4NextContextId,
                                                   i4CurrentInst, &i4NextInst);
        CliFlush (CliHandle);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

INT4
MstMiValidateMstiPortEntry (UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4MstInst)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return CLI_FAILURE;
    }

    pPortEntry = AstGetIfIndexEntry (u4IfIndex);

    if (pPortEntry == NULL)
    {
        AstReleaseContext ();
        return MST_FAILURE;
    }

    if (i4MstInst == AST_TE_MSTID)
    {
        AstReleaseContext ();
        return MST_SUCCESS;
    }

    pAstPerStPortInfo = AST_GET_PERST_PORT_INFO (AST_IFENTRY_LOCAL_PORT
                                                 (pPortEntry), i4MstInst);

    if (pAstPerStPortInfo != NULL)
    {
        AstReleaseContext ();
        return MST_SUCCESS;
    }

    AstReleaseContext ();
    return MST_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : AstGetNextMstiPortTableIndex                     */
/*                                                                           */
/*    Description         : This function is used to get the index of the    */
/*                          MSTP Port table.                                 */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                          i4Instance - Current Instance.                   */
/*                                                                           */
/*    Output(s)           : pi4NextIfIndex - Next IfIndex.                   */
/*                          pi4NextInstance - Next Instance.                 */
/*                                                                           */
/*    Global Variables Referred : gpAstContextInfo->PerContextIfIndexTable   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : RST_SUCCESS/RST_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
AstGetNextMstiPortTableIndex (UINT4 u4ContextId, INT4 i4IfIndex,
                              INT4 *pi4NextIfIndex,
                              INT4 i4Instance, INT4 *pi4NextInstance)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPortEntry      *pNextPortEntry = NULL;
    INT4                i4NextPort;
    INT4                i4NextInstance;

    if (AstSelectContext (u4ContextId) == RST_FAILURE)
    {
        return RST_FAILURE;
    }

    RB_OFFSET_SCAN (AST_PER_CONTEXT_IFINDEX_TREE (), pPortEntry,
                    pNextPortEntry, tAstPortEntry *)
    {
        if (i4IfIndex <= (INT4) pPortEntry->u4IfIndex)
        {
            if (i4IfIndex != (INT4) pPortEntry->u4IfIndex)
            {
                i4Instance = 0;
            }

            if (nmhGetNextIndexFsMstMstiPortTable (AST_IFENTRY_LOCAL_PORT
                                                   (pPortEntry), &i4NextPort,
                                                   i4Instance, &i4NextInstance)
                == SNMP_SUCCESS)
            {
                if ((INT4) AST_IFENTRY_LOCAL_PORT (pPortEntry) == i4NextPort)
                {
                    *pi4NextIfIndex = AST_IFENTRY_IFINDEX (pPortEntry);
                    *pi4NextInstance = i4NextInstance;

                    AstReleaseContext ();

                    return RST_SUCCESS;
                }
            }
        }
    }

    AstReleaseContext ();

    return RST_FAILURE;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstConfigPseudoRootId                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Pseudo RootId for a port of */
/*                        given instance.                                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        u4InstanceId - Instance Id                         */
/*                        MacAddress - Mac Address of Pseudo RootId          */
/*                        u2Priority - Priority of Pseudo RootId             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstConfigPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum,
                       UINT4 u4InstanceId, tMacAddr MacAddress,
                       UINT2 u2Priority)
{
    UINT1               au1RootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    UINT4               u4ErrCode;

    PseudoRootId.pu1_OctetList = &au1RootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    AST_MEMSET (au1RootIdBuf, AST_INIT_VAL, CLI_MSTP_MAX_BRIDGEID_BUFFER);

    PseudoRootId.pu1_OctetList[0] = (UINT1) ((u2Priority & 0xff00) >> 8);
    PseudoRootId.pu1_OctetList[1] = (UINT1) (u2Priority & 0x00ff);

    AST_MEMCPY (PseudoRootId.pu1_OctetList + AST_BRG_PRIORITY_SIZE,
                MacAddress, AST_MAC_ADDR_SIZE);

    if (u4InstanceId == MST_CIST_CONTEXT)
    {
        if (nmhTestv2FsMstCistPortPseudoRootId (&u4ErrCode, (INT4) u4PortNum,
                                                &PseudoRootId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMstCistPortPseudoRootId ((INT4) u4PortNum, &PseudoRootId)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMstMstiPortPseudoRootId (&u4ErrCode, (INT4) u4PortNum,
                                                (INT4) u4InstanceId,
                                                &PseudoRootId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMstMstiPortPseudoRootId ((INT4) u4PortNum,
                                             (INT4) u4InstanceId,
                                             &PseudoRootId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstSetPortL2gp                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable l2gp status for  */
/*                        a port.                                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableL2gp - L2Gp Status of port                  */
/*                                      TRUE - to configure port as L2gp port*/
/*                                      FALSE- to remove L2gp status of port */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstSetPortL2gp (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableL2gp)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortIsL2Gp (&u4ErrCode, (INT4) u4PortNum,
                                      i4EnableL2gp) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMstCistPortIsL2Gp ((INT4) u4PortNum, i4EnableL2gp)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstSetBpduRx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu receive     */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduRx - bpdu receive status of give port   */
/*                                      TRUE - bpdu can be received on port  */
/*                                      FALSE- received bpdus will be ignored*/
/*                                             on this port.                 */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduRx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortEnableBPDURx (&u4ErrCode, (INT4) u4PortNum,
                                            i4EnableBpduRx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMstCistPortEnableBPDURx ((INT4) u4PortNum, i4EnableBpduRx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstSetBpduTx                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu transmit    */
/*                        status for a port.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduTx - bpdu transmit status of give port  */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum, INT4 i4EnableBpduTx)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMstCistPortEnableBPDUTx (&u4ErrCode, (INT4) u4PortNum,
                                            i4EnableBpduTx) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMstCistPortEnableBPDUTx ((INT4) u4PortNum, i4EnableBpduTx)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstSetBpduFilter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets enable/disable bpdu transmit    */
/*                        or receive status for a port.                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                        i4EnableBpduFilter - bpdu transmit status of give   */
/*                                            port                           */
/*                                      TRUE - bpdu can be transmitted from  */
/*                                             this port.                    */
/*                                      FALSE- bpdus will not be transmitted */
/*                                             from this port.               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                  INT4 i4EnableBpduFilter)
{

    if (MstSetBpduTx (CliHandle, u4PortNum, i4EnableBpduFilter) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (MstSetBpduRx (CliHandle, u4PortNum, i4EnableBpduFilter) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstCliShowSpanningTreePortL2gp                       */
/*                                                                           */
/* Description        : Displays l2gp specific information of port.          */
/*                                                                           */
/* Input(s)           : CliHandle   - Handle to the cli context              */
/*                      u4ContextId - Context Id.                            */
/*                      u4Index     - Port Index.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/

INT4
MstCliShowSpanningTreePortL2gp (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE PseudoRootId;
    UINT4               u4PagingStatus;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4NextContextId;
    INT4                i4PrevInst;
    INT4                i4NextInst;
    INT4                i4NextPort;
    INT4                i4OutCome;
    INT4                i4IsL2Gp;
    INT4                i4PortState;
    INT4                i4PortStateStatus;
    INT4                i4CurrentPort;
    UINT2               u2Prio;
    UINT1               au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PseudoRootIdBuf[CLI_MSTP_MAX_BRIDGEID_BUFFER];
    UINT1               au1PseudoRootAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               u1OperStatus;
    INT1                i1OutCome;

    MEMSET (au1PseudoRootIdBuf, 0, CLI_MSTP_MAX_BRIDGEID_BUFFER);
    PseudoRootId.pu1_OctetList = &au1PseudoRootIdBuf[0];
    PseudoRootId.i4_Length = CLI_MSTP_MAX_BRIDGEID_BUFFER;

    i4OutCome = AstGetFirstPortInContext (u4ContextId, &i4NextPort);

    if (u4Index != 0)
    {
        i4NextPort = u4Index;
    }

    while (i4OutCome != RST_FAILURE)
    {
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT4) i4NextPort,
                                   &u1OperStatus);

        if (u1OperStatus != CFA_IF_DOWN)
        {
            nmhGetFsMIMstCistPortIsL2Gp (i4NextPort, &i4IsL2Gp);

            if (i4IsL2Gp == RST_TRUE)
            {
                nmhGetFsMIMstCistPortPseudoRootId (i4NextPort, &PseudoRootId);

                PrintMacAddress (PseudoRootId.pu1_OctetList +
                                 AST_BRG_PRIORITY_SIZE, au1PseudoRootAddr);

                u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

                nmhGetFsMIMstCistPortState (i4NextPort, &i4PortState);
                AstGetHwPortStateStatus (i4NextPort, MST_CIST_CONTEXT,
                                         &i4PortStateStatus);
                /* Get the interface name for this Port Number */
                AstCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1IntfName);

                CliPrintf (CliHandle, "\r\n Port %s \r\n", au1IntfName);
                CliPrintf (CliHandle, "%22s%s", " ", "PseudoRootId");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, " %s", "Instance");
                CliPrintf (CliHandle, "%6s", " ");
                CliPrintf (CliHandle, "%s%9s%-17s  %s", "Priority", " ",
                           "MacAddress", "State");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, "%s %13s   %s  %s", "----------",
                           "----------", "-------------------", "----------");
                CliPrintf (CliHandle, "\r\n");

                CliPrintf (CliHandle, " MST00     ");
                CliPrintf (CliHandle, "     %d", u2Prio);
                CliPrintf (CliHandle, "%28s", au1PseudoRootAddr);

                StpCliDisplayPortState (CliHandle, i4PortState,
                                        i4PortStateStatus);

                CliPrintf (CliHandle, "\r\n");

                i1OutCome =
                    nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                           &i4NextContextId, 0,
                                                           &i4NextInst);

                while ((i1OutCome != SNMP_FAILURE) &&
                       (i4ContextId == i4NextContextId))
                {
                    if (MstMiValidateMstiPortEntry
                        (u4ContextId, i4NextPort, i4NextInst) == MST_SUCCESS)
                    {

                        MEMSET (au1PseudoRootIdBuf, 0,
                                CLI_MSTP_MAX_BRIDGEID_BUFFER);
                        nmhGetFsMIMstMstiPortPseudoRootId (i4NextPort,
                                                           i4NextInst,
                                                           &PseudoRootId);

                        PrintMacAddress (PseudoRootId.pu1_OctetList +
                                         AST_BRG_PRIORITY_SIZE,
                                         au1PseudoRootAddr);

                        u2Prio = AstGetBrgPrioFromBrgId (PseudoRootId);

                        nmhGetFsMIMstMstiPortState (i4NextPort,
                                                    i4NextInst, &i4PortState);
                        AstGetHwPortStateStatus (i4NextPort, i4NextInst,
                                                 &i4PortStateStatus);

                        CliPrintf (CliHandle, " MST%02d     ", i4NextInst);

                        CliPrintf (CliHandle, "     %d", u2Prio);
                        CliPrintf (CliHandle, "%28s", au1PseudoRootAddr);

                        StpCliDisplayPortState (CliHandle, i4PortState,
                                                i4PortStateStatus);

                        CliPrintf (CliHandle, "\r\n");

                    }

                    i4PrevInst = i4NextInst;
                    i1OutCome =
                        nmhGetNextIndexFsMIMstMstiBridgeTable (i4ContextId,
                                                               &i4NextContextId,
                                                               i4PrevInst,
                                                               &i4NextInst);
                    u4PagingStatus = CliPrintf (CliHandle, "\r");

                    if (u4PagingStatus == CLI_FAILURE)
                    {
                        break;
                        /* User Presses 'q' at the prompt and so quits */
                    }
                    CliFlush (CliHandle);
                }
            }

        }

        if (u4Index != 0)
        {
            break;
        }

        i4CurrentPort = i4NextPort;
        i4OutCome = AstGetNextPortInContext (u4ContextId, i4CurrentPort,
                                             &i4NextPort);
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstResetPseudoRootId                               */
/*                                                                           */
/*     DESCRIPTION      : This function Resets the Pseudo RootId for a port. */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4PortNum  -  port number                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*****************************************************************************/

INT4
MstResetPseudoRootId (tCliHandle CliHandle, UINT4 u4InstanceId, UINT4 u4PortNum)
{
    tAstMacAddr         MacAddress;
    INT4                i4Priority;

    AST_MEMSET (MacAddress, AST_INIT_VAL, AST_MAC_ADDR_SIZE);

    RstGetBridgeAddr (&MacAddress);

    if (u4InstanceId == MST_CIST_CONTEXT)
    {
        if (nmhGetFsMstCistBridgePriority (&i4Priority) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetFsMstMstiBridgePriority ((INT4) u4InstanceId,
                                           &i4Priority) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    return (MstConfigPseudoRootId (CliHandle, u4PortNum, u4InstanceId,
                                   MacAddress, (UINT2) i4Priority));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetPortLoopGuard                             */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the Loop Guard on port    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliSetPortLoopGuard (tCliHandle CliHandle, UINT2 u2LocalPort, INT4 i4Value)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstCistPortLoopGuard (&u4ErrCode, u2LocalPort, i4Value)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsMstCistPortLoopGuard (u2LocalPort, i4Value) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);

            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

#endif

/* MSTP clear statistics feature*/
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliResetBridgeCounters                          */
/*                                                                           */
/*     DESCRIPTION      : This function resets all the bridge and interface  */
/*                          associated  statistical counters                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliResetBridgeCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    i1Return =
        nmhTestv2FsMIMstClearBridgeStats (&u4ErrCode, u4CurrContextId,
                                          MST_ENABLED);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }

    i1Return = nmhSetFsMIMstClearBridgeStats ((INT4) u4CurrContextId,
                                              MST_ENABLED);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliResetPortCounters                            */
/*                                                                           */
/*     DESCRIPTION      : This function resets all the interface             */
/*                          associated  statistical counters                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
MstCliResetPortCounters (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsMIMstCistPortClearStats (&u4ErrCode, (INT4) u4IfIndex,
                                            MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }

    i1Return = nmhSetFsMIMstCistPortClearStats ((INT4) u4IfIndex, MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliResetPerStBridgeCounters                     */
/*                                                                           */
/*     DESCRIPTION      : This function resets all the bridge and interface  */
/*                          associated  statistical counters                 */
/*                          for the given instance                           */
/*                                                                           */
/*     INPUT            : u2InstIndex -Instance Index                        */
/*                        CliHandle - Handle to the CLI context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliResetPerStBridgeCounters (tCliHandle CliHandle, UINT2 u2InstIndex)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    i1Return =
        nmhTestv2FsMIMstMstiClearBridgeStats (&u4ErrCode, u4CurrContextId,
                                              (INT4) u2InstIndex, MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }

    i1Return =
        nmhSetFsMIMstMstiClearBridgeStats ((INT4) u4CurrContextId,
                                           (INT4) u2InstIndex, MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliResetPerStPortCounters                       */
/*                                                                           */
/*     DESCRIPTION      : This function resets all the interface             */
/*                          associated  statistical counters                 */
/*                          for the given instance                           */
/*                                                                           */
/*     INPUT            : u2InstIndex -Instance Index                        */
/*                        u2PortNum - Port Number.                           */
/*                        CliHandle - Handle to the CLI context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstCliResetPerStPortCounters (tCliHandle CliHandle,
                              UINT2 u2InstIndex, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsMIMstMstiPortClearStats (&u4ErrCode, (INT4) u4IfIndex,
                                            (INT4) u2InstIndex, MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CliPrintf (CliHandle, "\n Test failed on input\n");
        return CLI_FAILURE;
    }

    i1Return =
        nmhSetFsMIMstMstiPortClearStats ((INT4) u4IfIndex, (INT4) u2InstIndex,
                                         MST_TRUE);
    if (SNMP_FAILURE == i1Return)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/* MSTP clear statistics feature*/
/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : MstCliShowPerformanceDataInstIface                */
/*                                                                          */
/*     DESCRIPTION      : This function displays performance data           */
/*                        specific to the given instance and port           */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4IfIndex - Interface Index                       */
/*                        u4Instance - InstanceIndex                        */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/*****************************************************************************/
INT4
MstCliShowPerformanceDataInstIface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                    UINT4 u4Instance)
{
    UINT4               u4EventTimeStamp = 0;
    UINT4               u4StateChangeTimeStamp = 0;
    UINT4               u4ContextId = 0;
    INT4                i4Event = 0;
    INT4                i4EventSubType = 0;
    UINT2               u2LocalPortId = 0;
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;

    if (u4IfIndex != 0)
    {
        pAstPortEntry = AstGetIfIndexEntry (u4IfIndex);

        if (pAstPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% This Port is not participating in the"
                       " spanning-tree\r\n");
            return (CLI_FAILURE);
        }
    }
    pPerStInfo = AST_GET_PERST_INFO ((UINT2) u4Instance);

    if (pPerStInfo == NULL)
    {
        CliPrintf (CliHandle, "\r%% Instance not created\r\n");
        return (CLI_FAILURE);
    }
    if (AstGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPortId) == RST_FAILURE)
    {
        return RST_FALSE;
    }
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2LocalPortId, u4Instance);

    if (pPerStPortInfo == NULL)
    {
        CliPrintf (CliHandle, "\r%% There is no such instance \r\n");
        return CLI_FAILURE;
    }
    nmhGetFsMIMstCistPortRcvdEventTimeStamp (u4IfIndex, &u4EventTimeStamp);
    nmhGetFsMIMstCistPortRcvdEvent (u4IfIndex, &i4Event);
    nmhGetFsMIMstCistPortRcvdEventSubType (u4IfIndex, &i4EventSubType);
    nmhGetFsMIMstMstiPortStateChangeTimeStamp (u4IfIndex, u4Instance,
                                               &u4StateChangeTimeStamp);
    CliPrintf (CliHandle,
               "\r\n STP Performance data for instance %d at Port %d \r\n",
               u4Instance, u4IfIndex);
    CliPrintf (CliHandle,
               "\r\n =================================================\r\n");

    CliPrintf (CliHandle, "\r Rcvd Event Time Stamp"
               "(In millisecs)           : %d\r\n", u4EventTimeStamp);
    if (i4EventSubType == AST_EXT_PORT_UP)
    {
        CliPrintf (CliHandle, "\r Rcvd Event           "
                   "                         : PORT_UP\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r Rcvd Event           "
                   "                         : PORT_DOWN\r\n");
    }
    CliPrintf (CliHandle, "\r Port State Change Time Stamp(In millisecs)  "
               "  : %d\r\n", u4StateChangeTimeStamp);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : MstCliSetFlushInterval                            */
/*                                                                          */
/*     DESCRIPTION      : This function configures the flush trigger        */
/*                        interval value for the context                    */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4FlushInterval - Flush interval in centi-secs    */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/****************************************************************************/
INT4
MstCliSetFlushInterval (tCliHandle CliHandle, UINT4 u4FlushInterval)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (nmhTestv2FsMIMstFlushInterval (&u4ErrCode, u4CurrContextId,
                                       (INT4) u4FlushInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIMstFlushInterval (u4CurrContextId, (INT4) u4FlushInterval)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : MstCliSetFlushIndThreshold                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the flush indication     */
/*                        threshold for the instance                        */
/*                                                                          */
/*     INPUT            : CliHandle - Handle to the CLI context             */
/*                        u4Instance - MST Instance ID                      */
/*                        u4Threshold - Flush indication threshold          */
/*                                                                          */
/*     OUTPUT           :  NONE                                             */
/*                                                                          */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/****************************************************************************/
INT4
MstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Instance,
                            UINT4 u4Threshold)
{
    UINT4               u4ErrCode = AST_INIT_VAL;
    UINT4               u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (u4Instance == MST_CIST_CONTEXT)
    {
        if (nmhTestv2FsMIMstCistFlushIndicationThreshold (&u4ErrCode,
                                                          u4CurrContextId,
                                                          (INT4) u4Threshold)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIMstCistFlushIndicationThreshold (u4CurrContextId,
                                                       (INT4) u4Threshold)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMIMstMstiFlushIndicationThreshold (&u4ErrCode,
                                                          u4CurrContextId,
                                                          u4Instance,
                                                          (INT4) u4Threshold)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIMstMstiFlushIndicationThreshold (u4CurrContextId,
                                                       u4Instance,
                                                       (INT4) u4Threshold)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstBpduguardEnable                                 */
/*                                                                           */
/*     DESCRIPTION      : This function enables bpdu-guard on the given      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstBpduguardEnable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,
                    UINT1 u1BpduGuardAction)
{
    UINT4               u4ErrCode = 0;

    if (u1BpduGuardAction == AST_INIT_VAL)
    {
        /*Default BPDU Guard Action is Disabling Spanning-tree */
        u1BpduGuardAction = AST_PORT_STATE_DISABLED;
    }

    if (nmhTestv2FsMstCistPortBpduGuard (&u4ErrCode, i4Index,
                                         u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2FsMstPortBpduGuardAction (&u4ErrCode, i4Index,
                                           (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMstCistPortBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetFsMstPortBpduGuardAction (i4Index, (INT4) u1BpduGuardAction) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstBpduguardDisable                                */
/*                                                                           */
/*     DESCRIPTION      : This function disables bpdu-guard on the given     */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index -   Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MstBpduguardDisable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsMstCistPortBpduGuard (&u4ErrCode,
                                         i4Index, u4Value) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsMstCistPortBpduGuard (i4Index, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetBpduGuardStatus                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the global status (             */
/*                        Enabled/Disabled) of the BPDU Guard Feature.       */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4GblBpduGuardStatus- MSTP Module Status.          */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliSetBpduGuardStatus (tCliHandle CliHandle, UINT4 u4GblBpduGuardStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstBpduGuard (&u4ErrCode, u4GblBpduGuardStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsMstBpduGuard (u4GblBpduGuardStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MstCliSetStpPerformanceStatus                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the global status (             */
/*                        Enabled/Disabled) of the STP Performance Feature.  */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4GblBpduGuardStatus- MSTP Module Status.          */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
MstCliSetStpPerformanceStatus (tCliHandle CliHandle,
                               UINT4 u4GblStpPerformanceStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsMstStpPerfStatus
        (&u4ErrCode, (INT4) u4GblStpPerformanceStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMstStpPerfStatus ((INT4) u4GblStpPerformanceStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% nmhSetFsMstStpPerfStatus failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstDisplayInterfaceBpduGuard                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays Bpdu Guard Status on the    */
/*                        Interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
MstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4BpduGuard = (INT4) AST_INIT_VAL;
    tAstPortEntry      *pAstPortEntry = NULL;

    if (nmhValidateIndexInstanceFsMIMstCistPortTable (i4Index) == SNMP_SUCCESS)
    {
        pAstPortEntry = AstGetIfIndexEntry ((UINT4) i4Index);
        if (pAstPortEntry == NULL)
        {
            return;
        }

        nmhGetFsMIMstCistPortBpduGuard (i4Index, &i4BpduGuard);
        /*Bpdu Guard  */
        if (i4BpduGuard == AST_BPDUGUARD_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is Enabled \r\n");
        }
        else if (i4BpduGuard == AST_BPDUGUARD_DISABLE)
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is Disabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nBpdu Guard  is None \r\n");
        }

    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MstDisplayPortInconsistency                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays port-level inconsistency    */
/*                        due to root guard or loop guard.                   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : i4Index  - Interface Number                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
MstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index)
{
    tAstPerStInfo      *pPerStInfo = NULL;
    UINT4               u4ContextId = AST_INIT_VAL;
    INT4                i4Inconsistency = AST_INIT_VAL;
    UINT2               u2LocalPortId = AST_INIT_VAL;
    INT2                i2InstanceId = AST_INIT_VAL;
    INT4                i4LoopIncState = AST_INIT_VAL;

    if (nmhGetFsMIMstCistPortRootInconsistentState (i4Index,
                                                    &i4Inconsistency) !=
        SNMP_FAILURE)
    {
        if (i4Inconsistency == AST_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "MST00\tRoot Inconsistent\r\n");
        }
    }
    if (nmhGetFsMIMstCistLoopInconsistentState (i4Index,
                                                &i4Inconsistency) !=
        SNMP_FAILURE)
    {
        if (i4Inconsistency == MST_TRUE)
        {
            CliPrintf (CliHandle, "MST00\tLoop Inconsistent\r\n");
        }
    }

    if (AstGetContextInfoFromIfIndex ((UINT4) i4Index,
                                      &u4ContextId,
                                      &u2LocalPortId) != MST_SUCCESS)
    {
        return;
    }

    if (AstSelectContext (u4ContextId) != MST_SUCCESS)
    {
        return;
    }

    i2InstanceId = 1;
    AST_GET_NEXT_BUF_INSTANCE (i2InstanceId, pPerStInfo)
    {
        if (pPerStInfo != NULL)
        {
            if (nmhGetFsMstMstiPortRootInconsistentState (i4Index,
                                                          (INT4) i2InstanceId,
                                                          &i4Inconsistency) !=
                SNMP_FAILURE)
            {
                if (i4Inconsistency == AST_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, "MST%2d\tRoot Inconsistent\r\n",
                               i2InstanceId);
                }
            }
            if (MstGetInstPortLoopInconsistentState
                ((UINT2) i4Index, (UINT2) i2InstanceId,
                 &i4LoopIncState) != MST_FAILURE)
            {
                if (i4LoopIncState == MST_TRUE)
                {
                    CliPrintf (CliHandle, "MST%2d\tLoop Inconsistent\r\n",
                               i2InstanceId);
                }

            }

            if (AstSelectContext (u4ContextId) != MST_SUCCESS)
            {
                return;
            }
        }
    }

    AstReleaseContext ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StpCliDisplayMstPortRole                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the MSTP Port Roles        */
/*                       and this will print the MSTP cases without space    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Context Identifier                   */
/*                        i4Val- Integer Value for the Role                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
StpCliDisplayMstPortRole (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val)
{

    if (AstIsMstStartedInContext (u4ContextId))
    {

        switch (i4Val)
        {
            case MST_PORT_ROLE_DISABLED:
                CliPrintf (CliHandle, "Disabled");
                break;
            case MST_PORT_ROLE_ALTERNATE:
                CliPrintf (CliHandle, "Alternate");
                break;
            case MST_PORT_ROLE_BACKUP:
                CliPrintf (CliHandle, "Back Up");
                break;
            case MST_PORT_ROLE_ROOT:
                CliPrintf (CliHandle, "Root");
                break;
            case MST_PORT_ROLE_DESIGNATED:
                CliPrintf (CliHandle, "Designated");
                break;
            case MST_PORT_ROLE_MASTER:
                CliPrintf (CliHandle, "Master");
                break;
            default:
                CliPrintf (CliHandle, "Unknown");
                break;
        }

    }
    return;
}
